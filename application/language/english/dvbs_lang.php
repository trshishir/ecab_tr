<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * Author: Raghu Veer K (rgh.veer@gmail.com)
 * Script: en_lang.php
 * English translation file
 *
 * Last edited:
 * 
 *
 * Project:
 * Digital Vehicle Booking System v1.0 (DVBS)
 */



/* * ********************** COMMON WORDS ********************* */

$lang['hello'] = "Hello";
$lang['id'] = "ID";
$lang['hi'] = "Hi";
$lang['age'] = "Age";
$lang['cv'] = "CV";
$lang['letter'] = "Letter";
$lang['welcome'] = "Welcome to";
$lang['site'] = "Site";
$lang['home'] = "Home";
$lang['logo'] = "Logo";
$lang['page_title'] = "Page Title";
$lang['header_title'] = "Header Title";
$lang['header'] = "Header";
$lang['footer'] = "Footer";
$lang['status'] = "Status";
$lang['statut'] = "Statut";
$lang['contact_us'] = "Contact Us";
$lang['about_us'] = "About Us";
$lang['site_map'] = "Site Map";
$lang['map'] = "Map";
$lang['settings'] = "Settings";
$lang['reports'] = "Reports";
$lang['logout'] = "Logout";
$lang['login'] = "Login";
$lang['access_denied'] = "Access denied!";
$lang['error'] = "Error!";
$lang['forgot_pw'] = "Forgot Password?";
$lang['remember_me'] = "Remember me";
$lang['back_to_login'] = "Back to Login Page";
$lang['search'] = "Search";
$lang['notifications'] = "Notifications";
$lang['password'] = "Password";
$lang['change_password'] = "Change Password";
$lang['current_password'] = "Current Password";
$lang['new_password'] = "New Password (at least 8 characters long)";
$lang['confirm_password'] = "Confirm Password";
$lang['profile'] = "Profile";
$lang['title'] = "Title";
$lang['content'] = "Content";
// $lang['type'] = "Type";
$lang['type'] = "Category";
$lang['name'] = "Name";
$lang['disabled_in_demo'] = "This feature is disabled in Demo";
$lang['first_name'] = "First Name";
$lang['last_name'] = "Last Name";
$lang['pw'] = "Password";
$lang['old_pw'] = "Old Password";
$lang['new_pw'] = "New Password";
$lang['confirm_pw'] = "Confirm Password";
$lang['code'] = "Code";
$lang['dob'] = "DOB";
$lang['image'] = "Image";
$lang['photo'] = "Photo";
$lang['note'] = "Note";
$lang['upload_file'] = "Upload File";
$lang['email'] = "Email";
$lang['email_address'] = "Email Address";
$lang['phone'] = "Phone";
$lang['office'] = "Office";
$lang['company'] = "Company";
$lang['website'] = "Website";
$lang['doj'] = "DOJ";
$lang['fax'] = "Fax";
$lang['contact'] = "Contact";
$lang['experience'] = "Experience";
$lang['location'] = "Location";
$lang['location_id'] = "Location Id";
$lang['address'] = "Address";
$lang['city'] = "City";
$lang['state'] = "State";
$lang['country'] = "Country";
$lang['zip_code'] = "Zip code";
$lang['about'] = "About";
$lang['description'] = "Description";
$lang['comment']="Comment";
$lang['time'] = "Time";
$lang['time_zone'] = "Time Zone"; //new
$lang['date'] = "Date";
$lang['from'] = "From";
$lang['to'] = "To";
$lang['cost'] = "Cost";
$lang['price'] = "Price";
$lang['rate'] = "Rate";
$lang['amount'] = "Amount";
$lang['total'] = "Total";
$lang['start_date'] = "Start Date";
$lang['end_date'] = "End Date";
$lang['size'] = "Size";
$lang['header_logo'] = "Header Logo";
$lang['login_logo'] = "Login Logo";
$lang['theme'] = "Theme";
$lang['menus'] = "Menus";
$lang['help'] = "Help";
$lang['yes'] = "Yes";
$lang['no'] = "No";
$lang['documentation'] = "Documentation";
$lang['first'] = "First";
$lang['last'] = "Last";
$lang['next'] = "Next";
$lang['previous'] = "Previous";
$lang['category'] = "Category";
$lang['category_id'] = "Category Id";
$lang['sub_category'] = "Sub Category";
$lang['sub_category_id'] = "Sub Category Id";
$lang['actions'] = "Actions";
$lang['operations'] = "Operations";
$lang['create'] = "Create";
$lang['add'] = "Add";
$lang['edit'] = "Edit";
$lang['update'] = "Update";
$lang['save'] = "Save";
$lang['submit'] = "Submit";
$lang['reset'] = "Reset";
$lang['delete'] = "Delete";
$lang['feature'] = "Feature";
$lang['create_success'] = "Created Successfully";
$lang['add_success'] = "Added Successfully";
$lang['save_success'] = "Saved Successfully";
$lang['update_success'] = "Updated Successfully";
$lang['delete_success'] = "Deleted Successfully";
$lang['status_change_success'] = "Status Changed Successfully";
$lang['make_active'] = "Make Active";
$lang['make_inactive'] = "Make Inactive";
$lang['record'] = "Record";
$lang['not_exist'] = "Does Not Exist";
$lang['session_expired'] = "Session Expired!";
$lang['enable'] = "Enable";
$lang['disable'] = "Disable";
$lang['flat'] = "Flat";
$lang['incremental'] = "Incremental";
$lang['please'] = "Please";
$lang['select'] = "Select";
$lang['value'] = "Value";
$lang['in'] = "in";
$lang['mins'] = "Mins";
$lang['in_mins'] = "in Mins";
$lang['please_edit_record'] = "Please Edit Record You want to Update";
$lang['valid_url_req'] = "Please enter a valid URL.";
$lang['valid_image'] = "Only jpg / jpeg / png images are accepted.";
$lang['confirm_delete'] = "Are you sure you want to delete this record?";
// $lang['confirm']						= "Are you sure?";
$lang['is'] = 'is';
$lang['unable'] = "Unable";
$lang['telugu'] = "Telugu";
$lang['english'] = "English";
$lang['hindi'] = "Hindi";
$lang['route_map'] = "Route Map";
$lang['question'] = "Question";
$lang['answer'] = "Answer";
$lang['car'] = "Car";
$lang['theme_settings'] = "Theme Settings";
$lang['change_theme'] = "Switch Theme";
$lang['change_site_theme'] = "Change Site Theme";
$lang['view_today_bookings'] = "View Today Bookings";
$lang['close'] = "Close";
$lang['warning'] = "Warning";
$lang['sure_delete'] = "Are you sure to delete?";
$lang['alert'] = "Alert";
$lang['info'] = "Info";
$lang['delete_vehicle'] = "Vehicles are available in this Category. If you want to delete this, delete all the vehicles of this category first.";
$lang['welcome_vbs'] = "Welcome To Digi Vehicle Booking System  ( VBS )";
$lang['thanx'] = "Thank you for your interest for registering with VBS. It is very helpfull for Vehicle Booking";
$lang['bottom_message'] = "Copyright ©  2014  Digi Vehicle Booking System (VBS)  All rights reserved.";
$lang['login_heading'] = "LOGIN";
$lang['forgot_pwd_msg'] = "Get your forgot password here. Forgot Password";
$lang['plane'] = "Plane";
$lang['saloon'] = "SALOON";
$lang['estate'] = "ESTATE";
$lang['instruction_to_driver'] = "Instructions to Driver";
$lang['next_your_details'] = "Next Your Details";
$lang['left_side_elements'] = "LEFT SIDE ELEMENTS ";
$lang['land_line'] = "Land Line";
$lang['contact_map'] = "Contact Map"; //new

/* admin header */
$lang['booking_system'] = "Booking System";
$lang['CAB_admin'] = "CAB Admin";
$lang['view_unread_bookings'] = "View Unread Bookings";
$lang['log_out'] = "Log Out";
$lang['read_all_new_messages'] = "Read All New Messages";
$lang['our_car_booking'] = "Our Car Booking";


/* admin navigation */
$lang['dashboard'] = "Dashboard";
$lang['bookings'] = "Bookings";
$lang['vehicle_settings'] = "Vehicle Settings";
$lang['list_vehicles'] = "List Vehicles";
$lang['add_vehicle'] = "Add Vehicle";
$lang['airport_settings'] = "Airport Settings";
$lang['list_airports'] = "List Airports";
$lang['users'] = "Users";
$lang['list_customers'] = "List Customers";
$lang['list_executives'] = "List Executives";
$lang['master_settings'] = "Master Settings";
$lang['hourly_package_settings'] = "Package Settings";
$lang['dynamic_pages'] = "Dynamic Pages";
$lang['app_settings'] = "App Settings"; //new
$lang['android'] = "Android"; //new
$lang['ios'] = "Ios"; //new
$lang['poi_settings'] = "POI";

//**dashboard**//
$lang['add_booking'] = "Add Booking";
$lang['search_bookings'] = "Search Bookings";
$lang['today_bookings'] = "Today Bookings";
$lang['latest_users'] = "Latest Users";
$lang['reg_date'] = "Reg-Date";
$lang['view_details'] = "View Details";
$lang['view_all'] = "View All";
$lang['booking_details'] = "Booking Details";
$lang['no_users'] = "No Users Available.";
$lang['no_recent_bookings'] = "No Recent Bookings Available.";



/* * * Bookings ** */
$lang['add_booking'] = "Add Booking";
$lang['book'] = "Book";
$lang['pick_date'] = "Date of Pick-up";
$lang['pick_time'] = "Time of Pick-up";
$lang['drop_date'] = "Date of Drop-of";
$lang['drop_time'] = "Time of Drop-of";
// $lang['source'] = "Source";
// $lang['destination'] = "Destination";
$lang['destination'] = "Destination Department";
$lang['payment_type'] = "Payment Type";
$lang['successful'] = "Successful";
$lang['unable_to_book'] = "Unable to Book";
$lang['search_bookings'] = "Search Bookings";
$lang['customer_name'] = "Customer Name";
$lang['customer_phone'] = "Customer Phone";
$lang['customer_email'] = "Customer Email";
$lang['canceled'] = "Cancelled";
$lang['recent_bookings'] = "Recent Bookings";
$lang['total_bookings'] = "Total Bookings";
$lang['date_of_booking'] = "Date of Booking";
$lang['booking_ref'] = "Booking Ref No";
$lang['return_journey'] = "Return journey required";
$lang['alert_change_site_theme'] = "Do you want to change the Site Theme to ";
$lang['unread_bookings'] = "Unread Bookings";



/* * Today Bookings* */
$lang['today_bookings'] = "Today Bookings";
$lang['all_bookings'] = "All Bookings";
$lang['book_date'] = "Book Date";
$lang['pick_point'] = "Pick Point";
$lang['drop_point'] = "Drop Point";
$lang['vehicle_selected'] = "Vehicle Selected";
$lang['cost_of_journey'] = "Cost of Journey";
$lang['payment_received'] = "Payment Received";
$lang['confirm'] = "Confirm";
$lang['cancel'] = "Cancel";
$lang['booking_reference_no'] = "Booking Reference Number";
$lang['booking_no'] = "Booking Number";

/** Search Bookings* */
$lang['from_date'] = "From Date";
$lang['to_date'] = "To Date";
$lang['enter_from_date'] = "Enter From Date";
$lang['enter_to_date'] = "Enter To Date";


/* * * Vehicle ** */
$lang['vehicles'] = "Vehicles";
$lang['passenger_capacity'] = "Passengers Capacity";
$lang['large_luggage_capacity'] = "Large Luggage Capacity";
$lang['small_luggage_capacity'] = "Small Luggage Capacity";
$lang['cost_type'] = "Cost Type";
$lang['vehicle'] = "Vehicle";
$lang['model'] = "Model";
$lang['fuel_type'] = "Fuel Type";
$lang['total_vehicles'] = "Total Vehicles";
$lang['starting'] = "Starting";
$lang['day'] = "DAY";
$lang['night'] = "NIGHT";
$lang['car_name'] = "Car Name";
$lang['car_selected'] = "Car Selected";
$lang['in_kgs'] = "In kg's";
$lang['eg_car_conditions_etc'] = "eg: car conditions etc";
$lang['total_number_of_vehicles_available'] = "Total no of vehicles available";
$lang['minimum_day_distance'] = "Minimum day distance";
$lang['minimum_day_cost'] = "Minimum day cost";
$lang['minimum_night_distance'] = "Minimum night distance";
$lang['minimum_night_cost'] = "Minimum night cost";
$lang['day_flat_cost'] = "Day flat cost";
$lang['night_flat_cost'] = "Night flat cost";
$lang['from_in_kms'] = "From in KMs";
$lang['to_in_kms'] = "To in KMs";
$lang['day_cost'] = "Day Cost";
$lang['night_cost'] = "Night Cost";
$lang['vehicle_cost_details'] = "Vehicle & Cost Details";
$lang['vehicle_name'] = "Vehicle Name";
$lang['location_address'] = "Location Address";
$lang['pick_date_vehicles'] = "Pick Date";
$lang['pick_time_vehicles'] = "Pick Time";
$lang['overall_vehicles'] = "Overall Vehicles";



/* * * Vehicle Categories ** */
$lang['vehicle_categories'] = "Vehicle Categories";
$lang['vehicle_category'] = "Vehicle Category";
$lang['add_vehicle_category'] = "Add Vehicle Category";
$lang['edit_vehicle_category'] = "Edit Vehicle Category";
$lang['eg_premimum_general_etc'] = "Eg: Premimum,General etc";


/* * * Vehicle Features ** */
$lang['vehicle_features'] = "Vehicle Features";
$lang['vehicle_feature'] = "Vehicle Feature";
$lang['add_vehicle_feature'] = "Add Vehicle Feature";
$lang['edit_vehicle_feature'] = "Edit Vehicle Feature";
$lang['eg_ac_non_ac_video_coach_etc'] = "Eg:AC, Non AC, Video coach etc";


/* * * Airports ** */
$lang['airports'] = "Airports";
$lang['airport_name'] = "Airport Name";
$lang['airport_address'] = "Airport Address";
$lang['airport'] = "Airport";
$lang['add_airport'] = "Add Airport";
$lang['edit_airport'] = "Edit Airport";
$lang['enter_airport_name'] = "Enter Airport Name";

//$lang['airport_id']						= "Airport Id";


/* * * Customers ** */
$lang['customers'] = "Customers";
$lang['index_active'] = "Block";
$lang['index_inactive'] = "Unblock";
$lang['user_name'] = "User Name";
$lang['active'] = "Active";
$lang['inactive'] = "Inactive";


//**add customer**//
$lang['add_customer'] = "Add Customer";


/* * * Executives ** */
$lang['executives'] = "Executives";
$lang['add_executive'] = "Add Executive";
$lang['edit_executive'] = "Edit Executive";


/* * * Site Settings ** */
$lang['site_settings'] = "Site Settings";
$lang['site_url'] = "Site URL";
$lang['address'] = "Address";
$lang['city'] = "City";
$lang['state'] = "State";
$lang['country'] = "Country";
$lang['zip_code'] = "Zip Code";
$lang['phone'] = "Phone";
$lang['fax'] = "Fax";
$lang['contact_email'] = "Contact Email";
$lang['currency_code'] = "Currency Code";
$lang['currency_symbol'] = "Currency Symbol";
$lang['distance_type'] = "Distance Type";
$lang['waiting_time'] = "Waiting Time";
$lang['airports'] = "Airports";
$lang['day_start_time'] = "Day Start Time";
$lang['day_end_time'] = "Day End Time";
$lang['night_start_time'] = "Night Start Time";
$lang['night_end_time'] = "Night End Time";
$lang['site_theme'] = "Site Theme";
$lang['email_type'] = "Email Type";
$lang['design_by'] = "Powered by";
$lang['rights_reserved'] = "Rights Reserved";
$lang['unable_to_update'] = "Unable To Update";
$lang['value_should_be_in_24hrs_format'] = "Value should be in 24hrs format";
$lang['faqs'] = "FAQ's";
$lang['faq'] = "FAQ";
$lang['payment_method'] = "Payment method";
$lang['date_format'] = "Date format";
$lang['app_settings'] = "App Settings";




/* * * Testimonials  Settings ** */
$lang['testimonial_settings'] = "Testimonial Settings";
$lang['testimonials'] = "Testimonials";
$lang['testimonials_of_our_clients'] = "Testimonials of our clients";
$lang['author'] = "Author";
$lang['action'] = "Action";
$lang['add_testimony'] = "Add Testimony";
$lang['unable_to_add'] = "Unable To Add";
$lang['location_name'] = "Location Name";
$lang['invalid'] = "Invalid";
$lang['operation'] = "Operation";
$lang['unable_to_delete'] = "Unable To Delete";
$lang['edit_testimony'] = "Edit Testimony";


/* * * Email Settings ** */
$lang['email_settings'] = "Email Settings";
$lang['host'] = "Host";
$lang['port'] = "Port";
$lang['host_name'] = "Host Name";


/* * * Paypal Settings ** */
$lang['paypal_settings'] = "Paypal Settings";
$lang['paypal_email'] = "Paypal Email";
$lang['currency'] = "Currency";
$lang['account_type'] = "Account Type";
$lang['logo_image'] = "Logo Image";
$lang['paypal'] = "Paypal";
$lang['payer_name'] = "Payer Name";
$lang['payer_email'] = "Payer Email";
$lang['booking_date'] = "Booking Date";
$lang['booking_status'] = "Booking Status";


//***Package Settings ***/
$lang['package_settings'] = "Package Settings";
$lang['packages'] = "Packages";
$lang['hours'] = "Hours";
$lang['distance'] = "Distance";
$lang['min_cost'] = "Min Cost";
$lang['charge_distance'] = "Charge/distance";
$lang['charge_hour'] = "Charge/hour";
$lang['terms_conditions'] = "Terms of Conditions";
$lang['add_package'] = "Add Package";
$lang['edit_package_setting'] = "Edit Package Setting";
$lang['in_hours'] = "In hour(s)";
$lang['distance_in_km'] = "Distance in km";
$lang['charge_per_km'] = "Charge per km";
$lang['charge_per_hour'] = "Charge per hour";
$lang['package_details'] = "Package Details";
$lang['package_extras'] = "Extra Charges";
$lang['load_more'] = "Load more";
$lang['show_less'] = "Show less";

// ***Waitings***//
$lang['waiting_time_settings'] = "Waiting Time Settings";
$lang['waiting_times'] = "Waiting Times";
$lang['add_waiting_time'] = "Add Waiting Time";
$lang['edit_waiting'] = "Edit Waiting";


//***Reasons to book with us***//
$lang['reasons_to_book_with_us'] = "Reasons To Book With Us";
$lang['reasons_to_book'] = "Reasons To Book";
$lang['instructions'] = "Instructions";


//***Social Network Settings***//
$lang['social_network_settings'] = "Social Network Settings";
$lang['facebook'] = "Facebook";
$lang['twitter'] = "Twitter";
$lang['linked_in'] = "Linked in";
$lang['google_plus'] = "Google Plus";
$lang['social_networks'] = "Social Networks";
$lang['url_order'] = "eg: https://your url";


//**SEO settings***//
$lang['seo_settings'] = "SEO Settings";
$lang['add_seo_setting'] = "	Add SEO Setting";
$lang['edit_seo_setting'] = "Edit SEO Setting";
$lang['site_keywords'] = "Site Keywords";
$lang['google_analytics'] = "Google Analytics";
$lang['site_title'] = "Site Title";
$lang['site_description'] = "Site Description";


//**Dynamic Pages**//
$lang['pages'] = "Pages";
$lang['meta_tag'] = "Meta title";
$lang['meta_description'] = "Meta Description";
$lang['seo_keywords'] = "SEO Keywords";
$lang['is_bottom'] = "is Bottom";
$lang['sort_order'] = "Sort Order";
$lang['parent_id'] = "Parent ID";
$lang['sort_order'] = "Sort Order";
$lang['bottom'] = "Bottom";
$lang['under_category'] = "Under Category";
$lang['add_page'] = "Add Page";
$lang['meta_tag_keywords'] = "Meta Description";
$lang['edit_page'] = "Edit Page";

//**Theme Settings**//
$lang['select_your_theme'] = "Select Your Theme";
$lang['yellow_theme'] = "Yellow Theme";
$lang['red_theme'] = "Red Theme";

//homepage//
//**header**//
$lang['welcome_to_DVBS'] = "Welcome to Navetteo";
$lang['create_account'] = "Create account";
$lang['lang'] = "Lang";
$lang['digi_advanced_cab_booking_system'] = "Digi Advanced Cab Booking System";

//**navigation**//
$lang['toggle_navigation'] = "Toggle Navigation";
$lang['home_page'] = "Home";
$lang['online_booking'] = "Online Booking";
$lang['book_online'] = "Booking";
$lang['FAQs'] = "FAQ";
$lang['downloads'] = "Downloads";
$lang['testimonials'] = "Testimonials";
$lang['my_account'] = "My Account";
$lang['my_profile'] = "My Profile";
$lang['my_bookings'] = "My Bookings";
$lang['contact_page'] = "Contact";
$lang['ourprice_page'] = "Price";
$lang['tarrifs'] = "Prices";
$lang['testimonial_page'] = "Testimonials";
$lang['usefullinks_page'] = "Useful Links";

//home page index
$lang['find_your_perfect_trip'] = "Book Your Journey Now";
$lang['pick_up_location'] = "Pick-Up Location";
$lang['drop_of_location'] = "Drop-Of Location";
$lang['pick_up_date'] = "Pick-Up Date";
$lang['pick_up_time'] = "Pick-Up Time";
$lang['drop_of_date'] = "Drop-Of Date";
$lang['drop_of_time'] = "Drop-Of Time";
$lang['cars'] = "CARS";
$lang['starts_from'] = "Starts From";

//footer
$lang['careers'] = "Careers";
$lang['privacy_policy'] = "Privacy Policy";
$lang['our_company'] = "Our Company";
$lang['our_fleet'] = "Our Fleet";
$lang['news_letter'] = "News Letter";
$lang['we_never_send_spam'] = "We Never Send Spam";
$lang['digi_advanced_cab_booking_system_2015'] = "Digi Advanced Cab Booking System 2015.";
$lang['all_rights_reserved'] = "All rights reserved.";
$lang['cards_we_accept'] = "Cards We Accept";


//create_account
$lang['register'] = "Register";
$lang['user_email'] = "User Email";
$lang['date_of_registration'] = "Date of Registration";
$lang['register_here'] = "Register Here";
$lang['saloon_cars'] = "Saloon Cars";
$lang['cities'] = "CITYS";
$lang['hyderabad'] = "Hyderabad";

//login
$lang['login_forgot_password'] = "Forgot your password?";

//online booking
$lang['online_booking'] = "Online Booking";
$lang['journey_details'] = "Journey Details";
$lang['passenger_details'] = "Passenger Details";
$lang['payment_details'] = "Payment Details";
$lang['enter_your_pickup_and_destination_details'] = "Enter Your Pickup and Destination Details";
$lang['date_time'] = "Date & Time";
$lang['click_and_select_the_date_and_time_you_would_like_to_be_picked_up'] = "Click & Select the date and time you'd like to picked up";
$lang['select_date'] = "Select Date";
$lang['select_time'] = "Select Time";
$lang['wait_and_return_journey_required'] = "Wait & Return Journey required";
$lang['total_journey_time'] = "TOTAL JOURNEY TIME";
$lang['total_journey_distance'] = "TOTAL JOURNEY DISTANCE";
$lang['total_cost'] = "TOTAL COST";
$lang['personal_details'] = "Personal Details";
$lang['confirmed_bookings'] = "Confirmed Bookings";
$lang['cancelled_bookings'] = "Cancelled Bookings";
$lang['pending_bookings'] = "Pending Bookings";
$lang['local_address'] = "Local Address";

//passenger details
$lang['guest_check_out'] = "Guest Check Out";
$lang['passengers_details'] = "Passenger(s) Details";
$lang['phone_number'] = "Phone Number";
$lang['information_to_driver'] = "Information To Driver";
$lang['back_to_journey_details'] = "Back To Journey Details";
$lang['next_payment_details'] = "Next Payment Details";
$lang['booking_summary'] = "Booking Summary";
$lang['booking_reference'] = "Booking Reference";
$lang['journey_type'] = "Journey Type";
$lang['two_way'] = "Two Way";
$lang['two_way_fare'] = "Two Way Fare";
$lang['one_way'] = "One Way";
$lang['one_way_fare'] = "One Way Fare";
$lang['pick_up'] = "Pick Up";
$lang['drop_of'] = "Drop of";
$lang['no_of_passengers'] = "No. of Passengers";
$lang['no_of_wheelchairs'] = "No. of Wheelchairs";
$lang['journey_miles_and_time'] = "Journey Miles & Time";
$lang['car_type'] = "Car Type";
$lang['booking_confirmation'] = "Booking Confirmation";
$lang['payment'] = "Payment";
$lang['credit_card'] = "Credit Card";
$lang['cash'] = "Cash";
$lang['pay_with_credit_card'] = "Pay With Credit Card";
$lang['pay_with_paypal'] = "Pay With Paypal";
$lang['pay_with_cash'] = "Pay With Cash";
$lang['back_to_your_details'] = "Back To Your Details";
$lang['continue'] = "Continue";


/** Packages* */
$lang['sno'] = "Sno";
$lang['package'] = "Package";
$lang['fare'] = "Fare";
$lang['after_distance'] = "After distance";
$lang['per_km'] = "per Km";
$lang['after_time'] = "After time";
$lang['per_hr'] = "per Hr";
$lang['book_now'] = "Book Now";
$lang['learn_more'] = "Learn More";

/** Package booking * */
$lang['package_booking'] = "Package Booking";
$lang['select_your_car'] = "Select Your Car";
$lang['select_the_car_that_suits_to_your_requirement'] = "Select the Car that suits to your requirement";
$lang['package_details_caps'] = "PACKAGE DETAILS";
$lang['total_time'] = "Total Time";
$lang['cost_details'] = "COST DETAILS";
$lang['package_cost'] = "Package Cost";
$lang['extra_distance'] = "Extra distance";
$lang['extra_time'] = "Extra time";

/** Contact * */
$lang['mobile'] = "Mobile";
$lang['message'] = "Message";
$lang['email_sent_successfully_we_will_contact_you_as_soon_as_possible'] = "Email sent successfully... We will contact you as soon as possible.";
$lang['unable_to_send_email'] = "Unable to send email.";

/** My account * */
$lang['mobile_number'] = "Mobile Number";


/* * change password * */
$lang['old_password'] = "Old Password";
$lang['password_changed_success'] = "Password Changed Successfully.";

/** myBookings * */
$lang['one_way_journey_details'] = "One Way Journey Details";
$lang['go_back_journey_details'] = "Two Way Journey Details";
$lang['status_canceled'] = "Cancelled";
$lang['unable_to_cancel_status'] = "Unable To Cancel Status";
$lang['status_confirm'] = "Confirm";

/* * logout* */
$lang['success_logout'] = "Successfully logged out";




















//PREVIOUS//
/* * * User ** */
// $lang['user'] = "User";
$lang['user'] = "Destination User";
$lang['new_user'] = "New User";
$lang['user_id'] = "User Id";
$lang['user_create_successful'] = "User Created Successfully";
$lang['user_update_successful'] = "User Updated Successfully";
$lang['no_of_users'] = "No. of Users";
$lang['active_executives'] = "Active Executives";
$lang['inactive_executives'] = "Inactive Executives";
$lang['chart_of_users'] = "Chart of Users";
$lang['chart_of_recent_bookings'] = "Chart of Recent Bookings";


/* * * Admin ** */
$lang['admin'] = "Admin";


//
$lang['new_executive'] = "New Executive";


///


$lang['new_customer'] = "New Customer";
$lang['customer_id'] = "Customer Id";


///
$lang['new_vehicle'] = "New Vehicle";
$lang['vehicle_id'] = "Vehicle Id";
$lang['luggage'] = "Luggage";
$lang['fuel'] = "Fuel";
$lang['petrol'] = "Petrol";
$lang['diesel'] = "Diesel";
$lang['gas'] = "Gas";



////////
$lang['booking_id'] = "Booking Id";
$lang['pick_place'] = "Place of Pick-up";
$lang['allotted_driver_id'] = "Allotted Driver Id";


//***waiting times
$lang['from_time'] = "From Time";
$lang['to_time'] = "To Time";


/* * * Booking System Settings ** */
$lang['distance_type'] = "Distance Type";
$lang['max_advance_booking_days'] = "Max. Advance Booking Days";
$lang['system'] = "System";


/* * * Cost Type Values ** */
$lang['cost_type_values'] = "Cost Type Values";
$lang['day_flat_rate'] = "Day flat Rate";
$lang['night_flat_rate'] = "Night flat Rate";
$lang['day_from_value'] = "Day From Value";
$lang['day_to_value'] = "Day To Value";
$lang['night_from_value'] = "Night From Value";
$lang['night_to_value'] = "Night To Value";


/* * * Airport Locations ** */
$lang['airport_location'] = "Airport Location";
$lang['airport_locations'] = "Airport Locations";


/* * *Locations* */
$lang['locations'] = "Locations";
$lang['list_locations'] = "List Locations";
$lang['add_location'] = "Add Location";
$lang['enter_location_name'] = "Enter Location Name";

/* * *Services* */
$lang['services'] = "Services";
$lang['legals'] = "Legals";
$lang['news'] = "News";
$lang['prices'] = "Prices";
$lang['fleet'] = "Fleet";
$lang['slideshows'] = "Slideshows";
$lang['banners'] = "Banners";
$lang['tutorials'] = "Tutorials";
$lang['documentations'] = "Documentations";
$lang['udapts'] = "Udapts";
$lang['list_services'] = "List Services";
$lang['add_service'] = "Add Service";
$lang['service'] = "Service";


/* * * Airport Cars ** */
$lang['airport_car'] = "Airport Car";
$lang['airport_cars'] = "Airport Cars";


/* * * Payments ** */
$lang['payments'] = "Payments";
$lang['payment_amount'] = "Payment Amount";
$lang['transaction_id'] = "Transaction Id";
$lang['transaction_status'] = "Transaction Status";
$lang['booking_successful'] = "Booking Successfully Completed";
$lang['booking_thanx'] = "Thanks for booking with us.";
$lang['cancel_booking'] = "If you wish to cancel the booking after confirmation you will need to inform us by calling us on +040 - 00 333 000 or email us at";
$lang['waiting_cost'] = "Waiting Cost";



/* * * Drivers ** */
$lang['driver'] = "Driver";
$lang['drivers'] = "Drivers";
$lang['driver_id'] = "Driver Id";
$lang['licence_no'] = "Licence No.";
$lang['rides'] = "Rides";
$lang['timesheet'] = "Timesheet";
$lang['wages'] = "Wages";
$lang['requests'] = "Requests";
$lang['infractions'] = "Infractions";


/* * * Language Settings ** */
$lang['language_settings'] = "Language Settings";
$lang['language'] = "Language";
$lang['languages'] = "Languages";
$lang['language_code'] = "Language Code";
$lang['language_name'] = "Language Name";

// Themes Sittings
$lang['themes'] = "Themes";
$lang['add theme'] = "Add Theme";
// unities
$lang['unitys'] = "Unitys";
$lang['unity'] = "Unity";
$lang['add unity'] = "Add Unity";
// currency module
$lang['currencies'] = "Currencies";
$lang['add currency'] = "Add Currency";
// for doucmnts module
$lang['documents'] = "Documents";
$lang['add document'] = "Add Document";
//
$lang['library'] = "Library";
$lang['add library'] = "Add Library";
// config module
$lang['accounting configurations'] = "Accounting Configurations";
/* * * Days & Months ** */
$lang['monday'] = "Monday";
$lang['tuesday'] = "Tuesday";
$lang['wednesday'] = "Wednesday";
$lang['thursday'] = "Thursday";
$lang['friday'] = "Friday";
$lang['saturday'] = "Saturday";
$lang['sunday'] = "Sunday";
$lang['january'] = "January";
$lang['february'] = "February";
$lang['march'] = "March";
$lang['april'] = "April";
$lang['may'] = "May";
$lang['june'] = "June";
$lang['july'] = "July";
$lang['august'] = "August";
$lang['september'] = "September";
$lang['october'] = "October";
$lang['november'] = "November";
$lang['december'] = "December";


//CodeIgniter
// Errors
$lang['error_csrf'] = 'This form post did not pass our security checks.';

// Login
$lang['login_heading'] = 'Login';
$lang['login_subheading'] = 'Please login with your email/username and password below.';
$lang['login_identity_label'] = 'Email/Username:';
$lang['login_password_label'] = 'Password:';
$lang['login_remember_label'] = 'Remember Me:';
$lang['login_submit_btn'] = 'Login';



// Index
$lang['index_heading'] = 'Users';
$lang['index_subheading'] = 'Below is a list of the users.';
$lang['index_fname_th'] = 'First Name';
$lang['index_lname_th'] = 'Last Name';
$lang['index_email_th'] = 'Email';
$lang['index_groups_th'] = 'Groups';
$lang['index_status_th'] = 'Status';
$lang['index_action_th'] = 'Action';
$lang['index_active_link'] = 'Active';
$lang['index_inactive_link'] = 'Inactive';
$lang['index_create_user_link'] = 'Create a new user';
$lang['index_create_group_link'] = 'Create a new group';

// Deactivate User
$lang['deactivate_heading'] = 'Deactivate User';
$lang['deactivate_subheading'] = 'Are you sure you want to deactivate the user \'%s\'';
$lang['deactivate_confirm_y_label'] = 'Yes:';
$lang['deactivate_confirm_n_label'] = 'No:';
$lang['deactivate_submit_btn'] = 'Submit';
$lang['deactivate_validation_confirm_label'] = 'confirmation';
$lang['deactivate_validation_user_id_label'] = 'user ID';

// Create User
$lang['create_user_heading'] = 'Create User';
$lang['create_user_subheading'] = 'Please enter the user\'s information below.';
$lang['create_user_fname_label'] = 'First Name';
$lang['create_user_lname_label'] = 'Last Name';
$lang['create_gender_label'] = 'Gender  : ';
$lang['create_dob_label'] = 'Date Of Birth  ';
$lang['create_user_desired_location_label'] = 'Desired Location';
$lang['create_user_desired_job_type_label'] = 'Desired Job Type';
$lang['create_user_open_for_contract_label'] = 'Open For Contract';
$lang['create_user_pay_rate_label'] = 'Pay Rate';
$lang['create_current_salary_label'] = 'Current Salary';
$lang['create_city_label'] = 'City';
$lang['create_state_label'] = 'State';
$lang['create_country_label'] = 'Country';
$lang['create_fax_label'] = 'Fax';
$lang['create_industry_label'] = 'Industry';

$lang['create_Zipcode_label'] = 'Zip Code';
$lang['create_willing_relocate_label'] = 'Willing to Relocate : ';
$lang['create_user_company_label'] = 'Company Name:';
$lang['company_profile'] = 'Company Profile:';
$lang['create_user_email_label'] = 'Email';
$lang['create_user_primary_email_label'] = 'Primary Email';
$lang['create_user_secondary_email_label'] = 'Secondary Email';
$lang['create_user_phone_label'] = 'Phone';

$lang['create_user_primary_phone_label'] = 'Primary Phone';
$lang['create_user_secondary_phone_label'] = 'Secondary Phone';
$lang['create_user_password_label'] = 'Password:';
$lang['create_user_password_confirm_label'] = 'Confirm Password:';
$lang['create_user_submit_btn'] = 'Create User';
$lang['create_user_validation_fname_label'] = 'First Name';
$lang['create_user_validation_lname_label'] = 'Last Name';
$lang['create_user_validation_email_label'] = 'Email Address';
$lang['create_user_validation_phone1_label'] = 'First Part of Phone';
$lang['create_user_validation_phone2_label'] = 'Second Part of Phone';
$lang['create_user_validation_phone3_label'] = 'Third Part of Phone';
$lang['create_user_validation_company_label'] = 'Company Name';
$lang['create_user_validation_password_label'] = 'Password';
$lang['create_user_validation_password_confirm_label'] = 'Password Confirmation';
$lang['create_user_validation_address_label'] = 'Address';

// Edit User
$lang['edit_user_heading'] = 'Edit User';
$lang['edit_user_subheading'] = 'Please enter the user\'s information below.';
$lang['edit_user_fname_label'] = 'First Name:';
$lang['edit_user_lname_label'] = 'Last Name:';
$lang['edit_user_company_label'] = 'Company Name:';
$lang['edit_user_email_label'] = 'Email:';
$lang['edit_user_phone_label'] = 'Phone:';
$lang['edit_user_password_label'] = 'Password: (if changing password)';
$lang['edit_user_password_confirm_label'] = 'Confirm Password: (if changing password)';
$lang['edit_user_groups_heading'] = 'Member of groups';
$lang['edit_user_submit_btn'] = 'Save User';
$lang['edit_user_validation_fname_label'] = 'First Name';
$lang['edit_user_validation_lname_label'] = 'Last Name';
$lang['edit_user_validation_email_label'] = 'Email Address';
$lang['edit_user_validation_phone1_label'] = 'First Part of Phone';
$lang['edit_user_validation_phone2_label'] = 'Second Part of Phone';
$lang['edit_user_validation_phone3_label'] = 'Third Part of Phone';
$lang['edit_user_validation_company_label'] = 'Company Name';
$lang['edit_user_validation_groups_label'] = 'Groups';
$lang['edit_user_validation_password_label'] = 'Password';
$lang['edit_user_validation_password_confirm_label'] = 'Password Confirmation';

// Create Group
$lang['create_group_title'] = 'Create Group';
$lang['create_group_heading'] = 'Create Group';
$lang['create_group_subheading'] = 'Please enter the group information below.';
$lang['create_group_name_label'] = 'Group Name:';
$lang['create_group_desc_label'] = 'Description:';
$lang['create_group_submit_btn'] = 'Create Group';
$lang['create_group_validation_name_label'] = 'Group Name';
$lang['create_group_validation_desc_label'] = 'Description';

// Edit Group
$lang['edit_group_title'] = 'Edit Group';
$lang['edit_group_saved'] = 'Group Saved';
$lang['edit_group_heading'] = 'Edit Group';
$lang['edit_group_subheading'] = 'Please enter the group information below.';
$lang['edit_group_name_label'] = 'Group Name:';
$lang['edit_group_desc_label'] = 'Description:';
$lang['edit_group_submit_btn'] = 'Save Group';
$lang['edit_group_validation_name_label'] = 'Group Name';
$lang['edit_group_validation_desc_label'] = 'Description';

// Change Password
$lang['change_password_heading'] = 'Change Password';
$lang['change_password_old_password_label'] = 'Old Password:';
$lang['change_password_new_password_label'] = 'New Password (at least %s characters long):';
$lang['change_password_new_password_confirm_label'] = 'Confirm New Password:';
$lang['change_password_submit_btn'] = 'Change';
$lang['change_password_validation_old_password_label'] = 'Old Password';
$lang['change_password_validation_new_password_label'] = 'New Password';
$lang['change_password_validation_new_password_confirm_label'] = 'Confirm New Password';

// Forgot Password
$lang['forgot_password_heading'] = 'Forgot Password';
$lang['forgot_password_subheading'] = 'Please enter your %s so we can send you an email to reset your password.';
$lang['forgot_password_email_label'] = '%s:';
$lang['forgot_password_submit_btn'] = 'Submit';
$lang['forgot_password_validation_email_label'] = 'Email Address';
$lang['forgot_password_username_identity_label'] = 'Username';
$lang['forgot_password_email_identity_label'] = 'Email';
$lang['forgot_password_email_not_found'] = 'No record of that email address.';

// Reset Password
$lang['reset_password_heading'] = 'Change Password';
$lang['reset_password_new_password_label'] = 'New Password (at least %s characters long):';
$lang['reset_password_new_password_confirm_label'] = 'Confirm New Password:';
$lang['reset_password_submit_btn'] = 'Change';
$lang['reset_password_validation_new_password_label'] = 'New Password';
$lang['reset_password_validation_new_password_confirm_label'] = 'Confirm New Password';


//New Kalyan start

$lang['failed'] = 'Failed';



//New Kalyan end
//New Raghu Start
$lang['in_kms'] = 'in KMs';

//New Raghu End
// start
$lang['currency_code_alpha'] = "Currency Code Alpha";
$lang['currency_name'] = "Currency Name";
$lang['user file'] = "User File";
$lang['user name'] = "User Name";
$lang['account_deactivated'] = "Account Deactivated";
$lang['Day'] = "Day";
$lang['url'] = "URL";
$lang['symbol'] = "Symbol";
$lang['start'] = "Start";
$lang['end'] = "End";
$lang['Night'] = "Night";
$lang['not_working_day'] = "Not Working Day";
$lang['minimum_fee'] = "Minimum Fee";




//

$lang['added'] = "Added";
$lang['time_from'] = "Time From (mins)";
$lang['time_to'] = "Time To (mins)";
$lang['email_received'] = "Email received, we will contact you as soon as possible.";
$lang['select_waiting_time'] = "Waiting Time";
$lang['type_your_address_here'] = 'Type your address here';
$lang['copyright'] = 'Copyright &copy; 2016 · Navetteo SAS. All rights reserved.';
$lang['bookmark_us'] = 'Bookmark Us';
$lang['type_ur_address'] = 'Type your address here';
$lang['terms_of_service'] = 'Terms of Service';
$lang['privacy_policy'] = 'Privacy Policy';
$lang['report_abuse'] = 'Report Abuse';
$lang['legal_notice'] = 'Legal';

$lang['partner_agreement'] = 'Partner Agreement';
$lang['driver_agreement'] = 'Driver Agreement Notice';
$lang['refund_policy'] = 'Refund Policy';

$lang['page_fleet'] = "Fleet";
$lang['vehicle_details_caps'] = 'Vehicule Details';
$lang['start_from'] = "Start From";
$lang['our_commitments'] = "Our Commitments";
$lang['benefits'] = "Benefits";
$lang['benefits_txt'] = "Why Choose %s for your transfers ?";
$lang['value_for_money'] = "Value for Money";
$lang['value_for_money_txt'] = "Quality Journeys at discount prices";
$lang['customer_service'] = "Customer Service";
$lang['customer_service_txt'] = "Quality Client Service to assist you by phone, by email or by live chat support.";
$lang['support'] = "Support";
$lang['supports'] = "Supports";
$lang['jobs'] = "Job Applications";
$lang['jobseekers'] = "Jobseekers";
$lang['calls'] = "Calls";
$lang['easy_of_use'] = "Ease of Use";
$lang['easy_of_use_txt'] = "4 steps only easy booking, client area to follow your bookings online";
$lang['punctuality'] = "Punctuality";
$lang['security'] = "Security";
$lang['quality'] = "Quality";
$lang['economy'] = "Economy";

$lang['services_pages'] = "Services Pages";
$lang['see_more'] = "see more";
$lang['live_traffic'] = 'Live Traffic';
$lang['map'] = 'Map';
$lang['track_flight'] = 'Track Flight';
$lang['flight_id'] = 'Flight ID';
$lang['track'] = 'Track';
$lang['loading_flight_status'] = 'Please wait while loading Flight status...';
$lang['EUR'] = '&euro;';
$lang['confirmation_approval'] = 'Confirmation email will be sent on approval';
$lang['passenger'] = "Passenger";
$lang['booking_email_label'] = 'You will find below a new booking online ID: ';
//  $lang['booking_email_note'] = 'Note: Your reservation is in status "Waiting Confirmation" You will receive another confirmation email when the status will change to "Confirmed" for any questions thank you to call us by phone during our opening hours.';
$lang['booking_email_note'] = 'Note: For further information thank you to contact us by phone during the opening hours of our offices.';
$lang['status_pending'] = 'Pending';
$lang['regards'] = 'Regards';
$lang['incl_waiting_time_return_journey'] = 'Incl Waiting Time & Return Journey';
$lang['passengers'] = 'Passengers';
$lang['luggages'] = 'Luggages';
$lang['wheelchair'] = 'Wheelchair';
$lang['baby_seat'] = 'Baby Seat';
$lang['pet'] = 'Pet';

$lang['train'] = 'Train';
$lang['hotel'] = 'Hotel';
$lang['park'] = 'Park';


/* Station - Gare  */
$lang['station_settings'] = "Station Settings";
$lang['list_stations'] = "List Stations";

$lang['stations'] = "Stations";
$lang['station_name'] = "Station Name";
$lang['station_address'] = "Station Address";
$lang['station'] = "Station";
$lang['add_station'] = "Add Station";
$lang['edit_station'] = "Edit Station";
$lang['enter_station_name'] = "Enter Station Name";

/* Hotel */
$lang['hotel_settings'] = "Hotel Settings";
$lang['list_hotels'] = "List Hotels";

$lang['hotels'] = "Hotels";
$lang['hotel_name'] = "Hotel Name";
$lang['hotel_address'] = "Hotel Address";
$lang['hotel'] = "Hotel";
$lang['add_hotel'] = "Add Hotel";
$lang['edit_hotel'] = "Edit Hotel";
$lang['enter_hotel_name'] = "Enter Hotel Name";

/* Park */
$lang['park_settings'] = "Park Settings";
$lang['list_parks'] = "List Parks";

$lang['parks'] = "Parks";
$lang['park_name'] = "Park Name";
$lang['park'] = "Park";
$lang['add_park'] = "Add Park";
$lang['edit_park'] = "Edit Park";
$lang['enter_park_name'] = "Enter Park Name";

$lang['billing_address'] = "Billing Address";

$lang['one_time'] = 'One Time';
$lang['regular'] = 'Regular';

$lang['go_back'] = 'Go and Back';

$lang['start_time'] = "Start Time";
$lang['end_time'] = "End Time";

$lang['go'] = 'Go';
$lang['back'] = 'Back';


// MAIN MENU
$lang['transfer_airport'] = 'Airport Transfer';
$lang['transfer_dorly'] = "Airport D'Orly Transfer";
$lang['transfer_cdg'] = "Airport CDG Transfer";
$lang['transfer_beauvais'] = "Airport Beauvais Transfer";
$lang['transfer_lebourget'] = "Airport Le Bourget Transfer";
$lang['transfer_parisvatry'] = "Airport Paris Vatry Transfer";

$lang['transfer_railways'] = 'Railways Transfer';
$lang['transfer_dunord'] = 'Du Nord Transfer';
$lang['transfer_montparnasse'] = 'Montparnasse Transfer';
$lang['transfer_saintlazard'] = 'Saint Lazard Transfer';
$lang['transfer_delyon'] = 'De Lyon Transfer';
$lang['transfer_delest'] = "De l'Est Transfer";

$lang['transfer_parkgarden'] = 'Park Garden Transfer';
$lang['transfer_disneyland'] = 'Parc DisneyLand Transfer';
$lang['transfer_asterix'] = 'Parc Asterix Transfer';
$lang['transfer_paristour'] = 'Paris Tour';
$lang['transfer_versailles'] = 'Versailles Transfer';

$lang['add_extra_stop'] = 'Add Extra Stop';
$lang['remove_extra_stop'] = "Remove Extra Stop";

$lang['partner_access'] = 'Partner Access';
$lang['driver_login'] = 'Driver Login';
$lang['client_login'] = 'Client Login';

$lang['quote'] = 'Quote';
$lang['quotes'] = 'Quotes';
$lang['name_address'] = "Name/Complement Address";
$lang['address_postcode_city'] = "Address, Postcode, City";
$lang['payment'] = "Payment";

$lang["booking_source"] = 'Source';
$lang["booking_destination"] = 'Destination';

$lang['our_packages'] = 'Our Packages';
$lang['package_price'] = 'Package Price';
$lang['zones_page'] = "Zones";
$lang['waiting'] = "Waiting";
$lang['contact_us_button'] = "Contact Us";

$lang['identification'] = "Identification";
$lang['pending'] = 'Pending';
$lang['confirm'] = 'Confirm';
$lang['cancelled'] = 'Cancelled';
$lang['action_confirm'] = "Confirm";
$lang['action_cancel'] = "Cancel";


$lang['pickup_location'] = "Pick-up Address";
$lang['drop_location'] = "Drop-of Address";

$lang['already_client'] = 'Already Client ?';
$lang['new_client'] = 'New Client ?';

$lang['drop2_address_label'] = 'Add Address';
$lang['drop2_airport_label'] = 'Select Airport';
$lang['drop2_train_label'] = 'Select Train';
$lang['drop2_hotel_label'] = 'Select Hotel';
$lang['drop2_park_label'] = 'Select Park';
$lang['extra_stop_label'] = 'Extra Stop';

$lang['number'] = 'Number';

$lang['cash_to_driver'] = "Cash to Driver";
$lang['total_price_to_pay_now'] = "Total Price to Pay Now";

$lang['my_quotes'] = 'My Quotes';
$lang['my_invoices'] = 'My Invoices';
$lang['my_support_tickets'] = 'My Support Tickets';
$lang['reservation'] = 'Reservation';




$lang['newsletter'] = 'Newsletter';
$lang['send_time'] = 'Send time';
$lang['user_type'] = 'User Type';
$lang['user_status'] = 'Status of user';
$lang['subject'] = 'Subject';
$lang['send_date'] = 'Send Date';
$lang['old'] = 'Remaining';

$lang['filemanagement'] = 'Files';
$lang['nature'] = 'Kind';
$lang['filename']='File Name';
$lang['priority'] = 'Priority';
$lang['alert'] = 'Alert';
$lang['delay_date'] = 'Delay';
$lang['selection'] = 'Selection';
$lang['name_selection'] = 'Name of document';
$lang['note'] = 'Note';
$lang['file'] = 'File';


$lang['task'] = 'Tasks';
$lang['config'] = 'Config';
$lang['booking'] = 'Booking';
$lang['date'] = 'Date';
$lang['date_hour'] = '';
$lang['date_minute'] = '';
$lang['added_by_firstname'] = 'Added By First Name';
$lang['added_by_lastname'] = 'Added By Last Name';
$lang['affected_department'] = 'Department';
$lang['departments'] = 'Departments';
$lang['affected_user'] = 'User';
//$lang['priority'] = 'File';
$lang['date2'] = 'Date';
$lang['date2_hour'] = '';
$lang['date2_minute'] = '';
$lang['files'] = 'Files';
$lang['note'] = 'Note';
$lang['note2'] = 'Note';
$lang['task_time'] = 'Time';
$lang['added_by'] = 'Added by';
$lang['from_department'] = 'From department';
$lang['affected_to'] = 'Affected to';
$lang['delay_date'] = 'Delay date';
$lang['delay_time'] = 'Delay time';
$lang['to_be_done_before'] = 'To be done before';
$lang['add_files'] = 'Add files';
$lang['task_description'] = 'Task Description';


//
$lang['configurations'] = 'Configurations';
$lang['module'] = 'Module';
$lang['message_sentence'] = 'First Message Sentence';
$lang['name_of_sender'] = 'Name of sender';
$lang['sender_name'] = 'Sender Name';
$lang['sender_user'] = 'Sender User';
$lang['sender_department'] = 'Sender Department';
$lang['smtp'] = 'SMTP';
$lang['popups'] = 'Popups';
$lang['select_all'] = 'All';
$lang['add_note'] = 'Add note';
$lang['quick_replies'] = 'Quick Replies';
$lang['callback'] = 'Callback';
$lang['smtp_text'] = 'SMTP';
$lang['job_applications'] = 'Job Applications';
$lang['support_tickets'] = 'Support Tickets';
$lang['quote_requests'] = 'Quote Requests';
$lang['CRM'] = 'CRM';
$lang['CMS'] = 'CMS';
$lang['accounting'] = 'Accounting';
$lang['sent_by'] = 'Sent By';
$lang['attachment'] = 'Attachment';
$lang['send_copy'] = 'Send Copy';
$lang['reminders'] = "Reminders";
$lang['reminders1'] = "Driver Reminders";
$lang['view'] = "View";
$lang['documents'] = "Documents";

$lang['price_KM_of_Approach_Course'] = 'Price / KM of Parcours D\'approche';
$lang['night_surcharge'] = 'Majoration de nuit';
$lang['packed_lunch'] = 'Panier Repas';
$lang['price_KM_ROUTE'] = 'Price / KM TRAJET';
$lang['holiday_day_supplement'] = 'Supplement jour Ferie';
$lang['compensatory_rest'] = 'Repos Compensateur';
$lang['price_KM_path_Return'] = 'Price / KM TRAJET Retour';
$lang['supplement_may_1_and_december_25'] = 'Supplement 1er mai';
$lang['rest'] = 'Repos';
$lang['pet'] = 'Animal de Cmpanie';
$lang['waiting'] = 'Attente';
$lang['passenger'] = 'Passager';
$lang['car_seat_baby_seat'] = 'Siege BEBE';
$lang['accompanying_person'] = 'Accompagnateur';
$lang['armchair'] = 'Fauteuil';
$lang['baggage'] = 'Bagages';
$lang['baby_seat_rental'] = 'Location De Siege BEBE';
$lang['armchair_rental'] = 'Location Fauteuil';
$lang['pickup_fee'] = 'Pickup Fee';
$lang['minimum_fee'] = 'Minimum Fee';
$lang['persons'] = 'Persons';
$lang['pickup_fee'] = 'Pickup Fee';

$lang['commissions'] = "Commissions";
$lang['contracts'] = "Contracts";
$lang['messages'] = "Messages";
$lang['contacts'] = "Contacts";
$lang['recourses'] = "Recourses";
$lang['application'] = "Application";
$lang['document'] = "Document";

$lang['admin_login'] = "Admin login";
$lang['google_api'] = "Google API";

$lang['file_date'] = "File Date";
$lang['quote_id'] = "Quote ID";
$lang['sales_operator'] = "Sales Operator";
$lang['recall_day'] = "Callback Day";
$lang['recall_time'] = "Callback Time";
$lang['address1'] = "Address";
$lang['address2'] = "Address 2";
$lang['situation'] = "Situation";