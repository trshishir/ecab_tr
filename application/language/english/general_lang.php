<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
                /**
                *
                * Created:  2014-05-31 by CubesCode
                *
                * Description:  english language file for general views
                *
                */



$lang['CRM'] = "CRM ";
$lang['accounting'] = "Accounting  ";
$lang['CMS'] = "CMS ";
$lang['job_applications'] = "job_applications ";
$lang['calls'] = "Calls";
$lang['CRM'] = "CRM";
$lang['newsletter'] = "Newsletter";
$lang['promotion'] = "Promotion";
$lang['sales'] = "Sales";
$lang['bills'] = "Bills";
$lang['bank'] = "Bank";
$lang['factoring'] = "Factoring";
$lang['reports'] = "Reports";
$lang['language'] = "Languages";
$lang['job_applications'] = "Job Applications";
$lang['calls'] = "Calls";
$lang['quote_requests'] = "Quote Requests";
$lang['files'] = "Files";
$lang['tasks'] = "Tasks";
$lang['configurations'] = "Configurations";
$lang['accounting'] = "Accounting";
$lang['quick_replies'] = "Quick Replies";
$lang['smtp_configurations'] = "SMTP Configurations";
$lang['notifications'] = "Notifications";
$lang['popups_settings'] = "Popups Settings";
$lang['setting'] = "Setting";
$lang['help'] = "Help";
$lang['company_profile'] = "Company Profile";
$lang['departments'] = "Departments";
$lang['supplier'] = "Supplier";
$lang['users'] = "Users";
$lang['log_out'] = "Log out";
$lang['calls'] = "Calls";
$lang['job_applications'] = "Job Applications";
$lang['bookings'] = "Bookings";
$lang['invoices'] = "Invoices";
$lang['clients'] = "Clients";
$lang['cars'] = "Cars";
$lang['drivers'] = "Drivers";
$lang['id'] = "ID";
$lang['date'] = "Date";
$lang['time'] = "Time";
$lang['civility'] = "Civility";
$lang['first_name'] = "First Name";
$lang['last_name'] = "Last Name";
$lang['email'] = "Email";
$lang['phone'] = "Phone";
$lang['age'] = "Age";
$lang['dob'] = "DOB";
$lang['zip_code'] = "Zip Code";
$lang['job_offer'] = "Job Offer";
$lang['telephone'] = "Telephone";
$lang['date_of_birth'] = "Date of Birth";
$lang['postal_code'] = "Postal Code";
$lang['cv'] = "CV";
$lang['cover_letter'] = "Cover Letter";
$lang['offer'] = "Offer";
$lang['status'] = "Status";
$lang['subject'] = "Subject";
$lang['communication'] = "Communication";
$lang['presentation'] = "Presentation";
$lang['driving'] = "Driving";
$lang['look'] = "Look";
$lang['experience'] = "Experience";
$lang['add_note'] = "Add note";
$lang['civility'] = "Civility";
$lang['languages'] = "Languages";
$lang['xyzab'] = "xyzab";
$lang['prename'] = "Prename";
$lang['translation'] = "Translation";
$lang['library'] = "Library";