<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
                /**
                *
                * Created:  2014-05-31 by CubesCode
                *
                * Description:  french language file for general views
                *
                */



$lang['CMS'] = "Cms test";
$lang['CRM'] = "CrmTEST";
$lang['accounting'] = "Comptabilité";
$lang['job_applications'] = " demandes d'emploi";
$lang['calls'] = "Appels";
$lang['newsletter'] = "Bulletin";
$lang['promotion'] = "Promotion";
$lang['sales'] = "Ventes";
$lang['bills'] = "Factures";
$lang['bills'] = "Factures";
$lang['bank'] = " banque";
$lang['factoring'] = "Affacturage";
$lang['reports'] = " rapports";
$lang['language'] = "Langue";
$lang['quote_requests'] = "Quote Requests";
$lang['files'] = "des dossiers";
$lang['tasks'] = " Tâches";
$lang['configurations'] = " configurations";
$lang['quick_replies'] = "réponses_ rapides";
$lang['smtp_configurations'] = "smtp_configurations";
$lang['notifications'] = "notifications";
$lang['popups_settings'] = "popups_settings";
$lang['setting'] = " réglage";
$lang['help'] = "Aidez-Moi";
$lang['company_profile'] = "Profil de la société";
$lang['departments'] = "départements";
$lang['supplier'] = "fournisseuse";
$lang['users'] = "utilisatrices";
$lang['log_out'] = " Se déconnecter";
$lang['bookings'] = "réservations";
$lang['invoices'] = "factures";
$lang['clients'] = "clientes";
$lang['cars'] = "voitures";
$lang['drivers'] = "conducteurs";
$lang['email'] = "email";
$lang['id'] = "id";
$lang['date'] = "Date";
$lang['time'] = "temps";
$lang['civility'] = "civilité";
$lang['first_name'] = "Prénom";
$lang['last_name'] = "nom de famille";
$lang['phone'] = "téléphone";
$lang['age'] = "âge";
$lang['dob'] = "dob";
$lang['zip_code'] = " code postal";
$lang['job_offer'] = " offre d'emploi";
$lang['telephone'] = "Téléphone";
$lang['date_of_birth'] = "date de naissance";
$lang['postal_code'] = "code postal";
$lang['cv'] = "CV";
$lang['cover_letter'] = " lettre de motivation";
$lang['offer'] = "offre";
$lang['status'] = "statut";
$lang['subject'] = "matière";
$lang['communication'] = " la communication";
$lang['presentation'] = "présentation";
$lang['driving'] = "conduite";
$lang['look'] = "Regardez";
$lang['experience'] = "expérience";
$lang['add_note'] = " ajouter une note";
$lang['prename'] = "Pre name";
$lang['languages'] = "langues";
$lang['translation'] = "translation ";
$lang['translation'] = "Traduction";
$lang['xyzab'] = "Xyzab";
$lang['xyzab'] = "Xyzab";
$lang['xyzab'] = "Xyzab";
