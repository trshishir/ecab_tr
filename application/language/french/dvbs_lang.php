<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
* Author: Raghu Veer K (rgh.veer@gmail.com)
* Script: en_lang.php
* English translation file
*
* Last edited:
* 
*
* Project:
* Digital Vehicle Booking System v1.0 (DVBS)
*/



/************************ COMMON WORDS **********************/
$lang['hello']	 						= "Bonjour";
$lang['hi']		 						= "Salut";
$lang['age']		 					= "Age";
$lang['cv']		 					    = "CV";
$lang['letter']		 					= "Letter";
$lang['welcome'] 						= "Bienvenue à";
$lang['jobs'] = "Job Applications";
$lang['calls'] = "Calls";
$lang['id'] = "ID";
$lang['site']	 						= "Site";
$lang['home'] 							= "Maison";
$lang['logo'] 							= "Logo";
$lang['page_title']						= "Titre de la page";
$lang['header_title']					= "Header Titre";
$lang['header']		   					= "Header";
$lang['footer']		   					= "Pied de page";
$lang['status'] 						= "Statut";
$lang['contact_us']						= "Contactez-Nous";
$lang['about_us'] 						= "À Propos De Nous";
$lang['site_map'] 						= "Plan du site";
$lang['map'] 							= "Carte";
$lang['settings'] 						= "Paramètres";
$lang['reports'] 						= "Rapports";
$lang['logout'] 						= "Déconnexion";
$lang['login'] 							= "S'identifier";
$lang['access_denied'] 					= "Accès refusé!";
$lang['error']		 					= "Erreur!";
$lang['forgot_pw'] 						= "Mot De Passe Oublié?";
$lang['remember_me'] 					= "Souviens-toi de moi";
$lang['back_to_login'] 					= "Retour à la page Connexion";
$lang['search'] 						= "Recherche";
$lang['notifications'] 					= "Notifications";
$lang['password']		 				= "Mot de passe";
$lang['change_password'] 				= "Changer Le Mot De Passe";
$lang['current_password'] 				= "Mot De Passe Actuel";
$lang['new_password'] 					= "Nouveau mot de passe (au moins 8 caractères)";
$lang['confirm_password'] 				= "Confirmez Le Mot De Passe";
$lang['profile'] 						= "Profil";
$lang['title'] 							= "Titre";
$lang['content'] 						= "Content";
$lang['type']	 						= "Type";
$lang['name'] 							= "Nom";
$lang['disabled_in_demo'] 				= "Cette fonctionnalité est désactivée dans Démo";
$lang['since'] 					        = "Since";
$lang['job_offer'] 					    = "Job Offer";
$lang['civility'] 					    = "Civility";
$lang['first_name'] 					= "Prénom";
$lang['last_name'] 						= "Nom";
$lang['pw'] 							= "Mot de passe";
$lang['old_pw'] 						= "Ancien Mot De Passe";
$lang['new_pw'] 						= "Nouveau Mot De Passe";
$lang['confirm_pw'] 					= "Confirmez Le Mot De Passe";
$lang['code'] 							= "Code";
$lang['dob'] 							= "DOB";
$lang['image'] 							= "Image";
$lang['photo'] 							= "photo";
$lang['note'] 							= "Remarque";
$lang['upload_file'] 					= "Télécharger le fichier";
$lang['email'] 							= "Email";
$lang['email_address'] 					= "Adresse e-mail";
$lang['phone'] 							= "Téléphone";
$lang['office'] 						= "Bureau";
$lang['company'] 						= "Société";
$lang['website'] 						= "Site Web";
$lang['doj'] 							= "MJ";
$lang['fax'] 							= "Fax";
$lang['contact'] 						= "Contact";
$lang['experience'] 					= "Expérience";
$lang['location']						= "Emplacement";
$lang['location_id']					= "Lieu Id";
$lang['address'] 						= "Adresse";
$lang['city'] 							= "Ville";
$lang['state'] 							= "état";
$lang['country'] 						= "Pays";
$lang['zip_code'] 						= "Code postal";
$lang['about']		 					= "Sur";
$lang['description'] 					= "Description";
$lang['time'] 							= "Temps";
$lang['date'] 							= "Date";
$lang['from'] 							= "à partir de";
$lang['to'] 							= "à";
$lang['cost'] 							= "Coût";
$lang['price'] 							= "Prix";
$lang['rate'] 							= "Taux";
$lang['amount'] 						= "Montant";
$lang['total'] 							= "Total";
$lang['start_date']						= "Date De Début";
$lang['end_date'] 						= "Date de fin";
$lang['size'] 							= "Taille";
$lang['header_logo'] 					= "Header Logo";
$lang['login_logo'] 					= "Connexion Logo";
$lang['theme'] 							= "Thème";
$lang['menus'] 							= "Menus";
$lang['help'] 							= "Aidez-Moi";
$lang['yes'] 							= "Oui";
$lang['no'] 							= "Aucun";
$lang['documentation'] 					= "Documentation";
$lang['first'] 							= "Première";
$lang['last'] 							= "Dernier";
$lang['next'] 							= "Suivant";
$lang['previous'] 						= "Précédent";
$lang['category']						= "Catégorie";
$lang['category_id']					= "Catégorie Id";
$lang['sub_category']					= "Sous catégorie";
$lang['sub_category_id']				= "Sous catégorie Id";
$lang['replied']				        = "Replied";
$lang['new']				            = "New";
$lang['closed']				            = "Closed";
$lang['subject']				        = "Subject";
$lang['meeting']				        = "Meeting";
$lang['accepted']				        = "Accepted";
$lang['denied']				            = "Denied";
$lang['actions'] 						= "Actes";
$lang['operations'] 					= "Opérations";
$lang['create']							= "Créer";
$lang['add']							= "Ajouter";
$lang['edit']							= "éditer";
$lang['update']							= "Mettre à jour";
$lang['save']							= "Enregistrer";
$lang['submit']							= "Soumettre";
$lang['reset']							= "Remettre";
$lang['delete']							= "Effacer";
$lang['feature']						= "Caractéristique";
$lang['create_success']					= "Créé avec succès";
$lang['add_success']					= "Ajouté avec succès";
$lang['save_success']					= "Enregistré terminée";
$lang['update_success']					= "Mise à jour avec succès";
$lang['delete_success']					= "Supprimé avec succès";
$lang['status_change_success']			= "Statut changé avec succès";
$lang['make_active']					= "Assurez-active";
$lang['make_inactive']					= "Assurez inactif";
$lang['record']							= "Record";
$lang['not_exist']						= "Ne existe pas";
$lang['session_expired']				= "Session a expiré!";
$lang['enable']							= "Permettre";
$lang['disable']						= "Désactiver";
$lang['flat']							= "Plat";
$lang['incremental']					= "Incrémental";
$lang['please']							= "Se il vous plaît";
$lang['select']							= "Sélectionner";
$lang['value']							= "Valeur";
$lang['in']								= "En";
$lang['mins']							= "Mins";
$lang['in_mins']						= "En minutes";
$lang['please_edit_record']				= "Veuillez modifier enregistrement que vous souhaitez mettre à jour";
$lang['valid_url_req']					= "Se il vous plaît entrer une URL valide.";
$lang['valid_image']					= "Seules les images jpg / jpeg / png sont acceptées.";
$lang['select_vehicle_valid'] 			= 'Se il vous plaît Sélectionnez véhicule';
$lang['confirm_delete']					= "Êtes-vous sûr de vouloir supprimer cet album?";
$lang['confirm']						= "Êtes-vous sûr?";
$lang['is'] 							= 'Est';
$lang['unable'] 						= "Incapable";
$lang['telugu']							= "Telugu";
$lang['english']						= "Anglais";
$lang['hindi']							= "Hindi";
$lang['route_map']						= "Route Map";
$lang['question']						= "Question";
$lang['answer']						    = "Réponse";
$lang['no_of_users'] 					= "Nombre de utilisateurs";
$lang['active_executives'] 				= "Les cadres actifs";
$lang['inactive_executives'] 			= "Executives inactifs";
$lang['chart_of_users'] 				= "Tableau des utilisateurs";
$lang['chart_of_recent_bookings'] 		= "Tableau des réservations récentes";
$lang['active'] 						= "actif";
$lang['inactive'] 						= "inactif";
$lang['theme_settings']					= "Paramètres thème";
$lang['change_theme']					= "Passer Thème";
$lang['change_site_theme']				= "Change Site Thème";
$lang['view_today_bookings']			= "Voir Aujourd'hui Réservations";
$lang['close']							= "près";
$lang['warning']						= "avertissement";
$lang['sure_delete']					= "Êtes-vous sûr de vouloir supprimer?";
$lang['alert']							= "alerte";
$lang['info']							= "infos";
$lang['delete_vehicle']					= "Les véhicules sont disponibles dans cette catégorie. Si vous voulez supprimer ce problème, supprimez tous les véhicules de cette catégorie premier.";
$lang['welcome_vbs']					= "Bienvenue Pour système de réservation de véhicule Digi  ( VBS )";
$lang['thanx']							= "Nous vous remercions de votre intérêt pour l'inscription auprès VBS. Il est très utile pour les réservations de véhicules";
$lang['bottom_message']							= "droit d'auteur ©  2014  Système de réservation de véhicule Digi (VBS)  Tous les droits sont réservés.";
$lang['login_heading']					= "S'IDENTIFIER";
$lang['forgot_pwd_msg']					= "Obtenez votre mot de passe oublié ici. Mot De Passe Oublié";
$lang['plane']							= "plan";
$lang['saloon']							= "SALOON";
$lang['estate']							= "ESTATE";
$lang['instruction_to_driver']			= "Instructions aux Pilote";
$lang['next_your_details']				= "Suivant Vos détails";
$lang['left_side_elements']				= "GAUCHE ELEMENTS ";
$lang['land_line']						= "ligne terrestre";
$lang['alert_change_site_theme']        = "Voulez-vous changer le thème du site de ";

/*admin header*/
$lang['booking_system']					= "Système de réservation";
$lang['CAB_admin']						= "CAB admin";
$lang['view_unread_bookings']			= "Visualiser documents non lus Réservations";
$lang['log_out']						= "Déconnexion";
$lang['read_all_new_messages']			= "Lire tous les nouveaux messages";
$lang['our_car_booking']				= "Notre réservation de voitures";



/*admin navigation*/
$lang['dashboard']						= "Tableau de bord";
$lang['bookings']						= "Réservations";
$lang['vehicle_settings']				= "Paramètres de véhicules";
$lang['list_vehicles']					= "Liste des véhicules";
$lang['add_vehicle']					= "Ajouter véhicule";
$lang['airport_settings']				= "Paramètres de l'aéroport";
$lang['list_airports']					= "Liste Aéroports";
$lang['users'] 							= "Utilisateurs";
$lang['list_customers'] 				= "Liste clients";
$lang['list_executives']				= "Liste Executives";
$lang['master_settings']				= "Paramétrage des maîtres";
$lang['hourly_package_settings']		= "Paramètres de l'emballage horaires";
$lang['dynamic_pages']					= "Pages dynamiques";
$lang['app_settings']					= "Paramètres App"; //new
$lang['android']						= "Android"; //new
$lang['ios']							= "Ios"; //new
$lang['poi_settings']				= "POI";

//**dashboard**//
$lang['add_booking']					= "Ajouter réservation";
$lang['search_bookings']				= "Rechercher Réservations";
$lang['today_bookings']					= "Aujourd'hui Réservations";
$lang['latest_users']					= "Derniers utilisateurs";
$lang['reg_date']						= "Reg-Date";
$lang['view_details']					= "Voir les détails";
$lang['view_all']						= "Voir tous";
$lang['booking_details']				= "Réservation Détails";
$lang['no_users']						= "Aucun Utilisateurs disponibles.";
$lang['no_recent_bookings']				= "Pas de réservations récentes disponibles.";



/*** Bookings ***/
$lang['add_booking']					= "Ajouter réservation";
$lang['book']							= "Livre";
$lang['pick_date']						= "Date de Pick-up";
$lang['pick_time']						= "Temps de Pick-up";
$lang['source'] 						= "Source";
$lang['destination']                    = "Destination";
$lang['payment_type']					= "Type de paiement";
$lang['successful']                     = "Réussi";
$lang['unable_to_book']					= "Impossible de réserver";
$lang['search_bookings']				= "Rechercher Réservations";
$lang['customer_name']					= "Nom du client";
$lang['customer_phone']					= "Clientèle Téléphone";
$lang['customer_email']					= "Client Email";
$lang['canceled']						= "Annulé";
$lang['recent_bookings']				= "Réservations récents";
$lang['total_bookings']					= "total des réservations";
$lang['date_of_booking']				= "Date de réservation";
$lang['booking_ref']					= "Réservation Ref N°";
$lang['return_journey']					= "Trajet Retour";


/**Today Bookings**/
$lang['today_bookings']					= "Aujourd'hui Réservations";
$lang['all_bookings']					= "Toutes les réservations";
$lang['book_date']						= "Date de livre";
$lang['pick_point']						= "Choisissez le point";
$lang['drop_point']						= "Point de goutte";
$lang['vehicle_selected']				= "Véhicule sélectionné";
$lang['cost_of_journey']				= "Coût de Trajet";
$lang['payment_received']				= "Paiement reçu";
$lang['Confirm']						= "Confirmer";
$lang['cancel']							= "Annuler";
$lang['booking_reference_no']			= "Réservation Numéro de référence";
$lang['unread_bookings']        		= "Réservations non lus";
$lang['booking_no']			= "Réservation Numéro";
	
/** Search Bookings**/
$lang['from_date']						= "date d'entrée en";
$lang['to_date']						= "à Ce Jour";
$lang['enter_from_date']				= "Entrez De Date de";
$lang['enter_to_date']					= "Entrer date";


/*** Vehicle ***/
$lang['vehicles'] 						= "véhicules";
$lang['passenger_capacity']				= "Capacité passagers";
$lang['large_luggage_capacity']			= "Grande Capacité de bagages";
$lang['small_luggage_capacity']			= "petit bagages Capacité";
$lang['cost_type']						= "Type de coût";
$lang['vehicle'] 						= "Véhicule";
$lang['model'] 							= "Modèle";
$lang['fuel_type']						= "Type de carburant";
$lang['total_vehicles']					= "Nombre total de véhicules";
$lang['no_of_passengers']				= "Nombre de passagers";
$lang['no_of_wheelchairs']				= "Nombre de Fauteuil Roulant";
$lang['starting']						= "Départ";
$lang['day']							= "JOUR";
$lang['night']							= "NUIT";
$lang['car_name']						= "Nom de voitures";
$lang['car_selected'] 					= "Car sélectionné";
$lang['in_kgs']							= "Dans kg de";
$lang['eg_car_conditions_etc']			= "Par exemple: conditions de voitures etc.";
$lang['total_number_of_vehicles_available']	= "Nombre total de véhicules disponibles";
$lang['minimum_day_distance']			= "La distance minimale de jour";
$lang['minimum_day_cost']				= "Le coût d'un jour minimum";
$lang['minimum_night_distance']			= "La distance minimum de nuits";
$lang['minimum_night_cost']				= "Le coût minimum de nuits";
$lang['day_flat_cost']					= "Coût fixe Jour";
$lang['night_flat_cost']				= "Coût fixe Nuit";
$lang['from_in_kms']					= "De dans KMs";
$lang['to_in_kms']						= "Pour en KMs";
$lang['day_cost']						= "Coût jour";
$lang['night_cost']						= "Nuit Coût";
$lang['vehicle_cost_details']			= "Véhicule & Détails des coûts";
$lang['vehicle_name']					= "Nom de véhicule";
$lang['location_address']				= "Adresse civique";
$lang['pick_date_vehicles']				= "Date de Prise";	
$lang['pick_time_vehicles']				= "Choisissez Temps";
$lang['overall_vehicles']				= "Véhicules Total";
$lang['total_bookings']					= "Total des réservations";
$lang['recent_bookings']				= "Réservations récents";
$lang['date_of_booking']				= "Date de réservation";



/*** Vehicle Categories ***/
$lang['vehicle_categories']				= "Catégories de véhicules";
$lang['vehicle_category']				= "Catégorie de véhicule";
$lang['add_vehicle_category']			= "Ajouter Catégorie de véhicule";
$lang['edit_vehicle_category']			= "Modifier Catégorie de véhicule";
$lang['eg_premimum_general_etc']		= "Par exemple: Premimum, le général etc";


/*** Vehicle Features ***/
$lang['vehicle_features']				= "Caractéristiques du véhicule";
$lang['vehicle_feature']				= "Caractéristique de véhicule";
$lang['add_vehicle_feature']			= "Ajouter l'option de véhicule";
$lang['edit_vehicle_feature']			= "Caractéristique Modifier véhicule";
$lang['eg_ac_non_ac_video_coach_etc']	= "Par exemple:AC, non AC, entraîneur vidéo etc";


/*** Airports ***/
$lang['airports']						= "Aéroports";
$lang['airport_name']					= "Nom de l'aéroport";
$lang['airport_address']                                = "Addresse l'aéroport";
$lang['airport']						= "Aéroport";
$lang['add_airport']					= "Ajoutez l'aéroport";
$lang['edit_airport']					= "Modifier l'aéroport";
$lang['enter_airport_name']				= "Entrez Nom de l'aéroport";
$lang['airport_id']						= "Aéroport Id";


/*** Customers ***/
$lang['customers'] 						= "Clientèle";
$lang['index_active']       			= "Bloc";
$lang['index_inactive']    				= "Débloquer";
$lang['user_name'] 						= "Nom d'utilisateur";


//**add customer**//
$lang['add_customer'] 					= "Ajouter clientèle";


/*** Executives ***/
$lang['executives'] 					= "Executives";
$lang['add_executive']					= "Ajouter exécutif";
$lang['edit_executive'] 				= "Modifier exécutif";


/*** Site Settings ***/
$lang['site_settings']					= "Paramètres du site"; 
$lang['site_url']						= "URL du site";	
$lang['address']						= "Adresse";
$lang['city']							= "Ville";
$lang['state']							= "état";
$lang['country']						= "Pays";
$lang['zip_code']						= "Code postal";
$lang['phone']							= "Téléphone";
$lang['fax']							= "Fax";
$lang['contact_email']					= "Contact Email";
$lang['currency_code']					= "Code de devise";
$lang['currency_symbol']				= "Symbole monétaire";
$lang['distance_type']					= "Type de Distance";
$lang['waiting_time']					= "Temps d'attente";
$lang['airports']						= "Aéroports";
$lang['day_start_time']					= "Jour Heure de début";
$lang['day_end_time']					= "Jour Heure de fin";
$lang['night_start_time']				= "Nuit Start Time";
$lang['night_end_time']					= "Nuit Fin des Temps";
$lang['site_theme']						= "Thème du site";
$lang['email_type']						= "Courriel Type";
$lang['design_by']						= "Design by";
$lang['rights_reserved']				= "Droits réservés";
$lang['unable_to_update']				= "Pas mettre à jour";
$lang['value_should_be_in_24hrs_format']= "La valeur doit être au format 24 heures";
$lang['faqs'] 							= "FAQ's";
$lang['faq'] 							= "FAQ";
$lang['payment_method']					= "Mode de paiement";
$lang['date_format']					= "Format de date";



/*** Testimonials  Settings ***/
$lang['testimonial_settings']			= "Paramètres témoignage";
$lang['testimonials']					= "Témoignages";
$lang['testimonials_of_our_clients']					= "Témoignages de nos clients";
$lang['author'] 						= "Auteur";
$lang['action']							= "Action";
$lang['add_testimony']					= "Ajouter témoignage";
$lang['unable_to_add']					= "Impossible d'ajouter";
$lang['location_name']					= "Lieu Nom";
$lang['invalid'] 						= "Invalide";
$lang['operation']						= "Opération";
$lang['unable_to_delete']				= "Impossible de supprimer";
$lang['edit_testimony']					= "Modifier témoignage";


/*** Email Settings ***/
$lang['email_settings'] 				= "Paramètres de messagerie";
$lang['host']							= "Hôte";
$lang['port']							= "Port";
$lang['host_name']						= "Nom d'hôte";


/*** Paypal Settings ***/
$lang['paypal_settings']				= "Paramètres paypal";
$lang['paypal_email']					= "Paypal Email";
$lang['currency']						= "Monnaie";
$lang['account_type']					= "Type de compte";
$lang['logo_image']						= "Logo image";
$lang['paypal']							= "Paypal";


//***Package Settings ***/
$lang['package_settings']				= "Paramètres de l'emballage";
$lang['packages']						= "Forfaits";
$lang['hours']							= "Heures";
$lang['distance']						= "Distance";
$lang['min_cost']						= "Coût min";
$lang['charge_distance']				= "Charge Distance";
$lang['charge_hour']					= "Charger ";
$lang['terms_conditions']				= "Termes & conditions";
$lang['add_package']					= "Ajouter un package";
$lang['edit_package_setting']			= "Modifier un paquetage de réglage";
$lang['in_hours']						= "En heure (s)";
$lang['distance_in_km']					= "Distance en km";
$lang['charge_per_km']					= "Frais par km";
$lang['charge_per_hour']				= "Frais par heure";
$lang['package_details']				= "Détails du forfait";
$lang['package_extras']					= "Frais supplémentaires";
$lang['load_more']						= "charger plus";
$lang['show_less']						= "Voir moins";


// ***Waitings***//
$lang['waiting_time_settings']			= "Temps d'attente Paramètres";
$lang['waiting_times']					= "Temps d'attente";
$lang['add_waiting_time']				= "Ajouter Temps d'attente";
$lang['edit_waiting']					= "Modifier attente";


//***Reasons to book with us***//
$lang['reasons_to_book_with_us']		= "Raisons de réserver avec nous";
$lang['reasons_to_book']				= "Raisons de réserver";
$lang['instructions']					= "Instructions";


//***Social Network Settings***//
$lang['social_network_settings']		= "Paramètres réseau social";
$lang['facebook']						= "Facebook";
$lang['twitter']						= "Gazouillement";
$lang['linked_in']						= "Linked in";
$lang['google_plus']					= "Google Plus";
$lang['social_networks']				= "Réseaux sociaux";
$lang['url_order']					    = "eg: https://your url";


//**SEO settings***//
$lang['seo_settings']					= "Paramètres de référencement";
$lang['add_seo_setting']				= "Ajouter SEO Cadre";
$lang['edit_seo_setting']				= "Modifier SEO Cadre";
$lang['site_keywords']					= "Site Mots-clés";
$lang['google_analytics']				= "Google Analytics";
$lang['site_title']						= "Titre du site";
$lang['site_description']				= "Description du site";


//**Dynamic Pages**//
$lang['pages']                          = "Pages";
$lang['meta_tag'] 						= "Meta Tag";
$lang['meta_keywords']					= "Meta Keywords";
$lang['seo_keywords']					= "Mots-clés SEO";
$lang['is_bottom'] 						= "Est bas";
$lang['sort_order'] 					= "l'ordre de tri";
$lang['parent_id'] 						= "parent ID";
$lang['sort_order'] 					= "l'ordre de tri";
$lang['bottom']							= "Bas";
$lang['under_category'] 			    = "Dans la catégorie";
$lang['add_page']						= "Ajouter une page";
$lang['meta_tag_keywords']				= "Meta Tag Mots-clés";
$lang['edit_page']						= "Modifier la Page";

//homepage//
//**header**//
$lang['welcome_to_DVBS']				= "Bienvenue à " . APP_NAME;
$lang['create_account']					= "Créer un compte";
$lang['lang']							= "Lang";
$lang['digi_advanced_cab_booking_system']= "Digi avancée Cab système de réservation";

//**navigation**//
$lang['toggle_navigation']				= "Basculer la navigation";
$lang['home_page']				= "Accueil";
$lang['online_booking']					= "Réservation en ligne";
$lang['book_online']					= "Réservation";
$lang['FAQs']							= "FAQ";
$lang['my_account']						= "Mon Compte";
$lang['my_profile']						= "Mon profil";
$lang['my_bookings']					= "Mes réservations";
$lang['contact_page']					= "Contact";
$lang['ourprice_page']					= "Nos Tarifs";
$lang['tarrifs']					= "Tarifs";
$lang['testimonial_page']					= "Témoignages";
$lang['usefullinks_page']					= "Liens Utiles";

//home page index
$lang['find_your_perfect_trip']			= "Reserver Votre Trajet Maintenant";
//  $lang['pick_up_location']				= "Pick-Up Localisation";
$lang['pick_up_location']				= "Adresse de Départ";
//  $lang['drop_of_location']				= "Drop-de situation";
$lang['drop_of_location']				= "Adresse de Destination";
//  $lang['pick_up_date']					= "Pick-Up Date de";
$lang['pick_up_date']					= "Date de Départ";
//  $lang['pick_up_time']					= "Pick-Up Time";
$lang['pick_up_time']					= "Heure de Départ";
$lang['drop_of_date']					= "Date de retour";
$lang['drop_of_time']					= "Heure de retour";

$lang['cars']							= "VOITURES";
$lang['starts_from']					= "Commence à partir de";

//footer
$lang['careers']						= "Carrières";
$lang['privacy_policy']					= "Politique de confidentialité";
$lang['our_company']					= "Notre Société";
$lang['our_fleet']						= "Notre flotte";
$lang['news_letter']					= "Nouvelles Lettre";
$lang['we_never_send_spam']				= "Nous ne envoyons jamais de Spam";
$lang['digi_advanced_cab_booking_system_2015']="Digi avancée Cab système de réservation en 2015.";
$lang['all_rights_reserved']			= "Tous les droits sont réservés.";
$lang['design_by_conquerors_technologies']="Design by conquérants Technologies";
$lang['cards_we_accept']                = "Cartes acceptées";


//create_account
$lang['register'] 						= "Se enregistrer";
$lang['user_email']						= "E-mail";
$lang['date_of_registration']			= "Date d'enregistrement";
$lang['register_here']					= "Inscrivez-vous ici";
$lang['saloon_cars']    				= "Saloon Cars";
$lang['cities']							= "CITYS";
$lang['hyderabad']						= "Hyderabad";

//login
$lang['login_forgot_password'] 			= "Mot de passe oublié?";
$lang['login_reset_password'] 			= "Reset Password";

//online booking
$lang['online_booking']							= "Réservation en ligne";
$lang['journey_details']						= "Details du Trajet";
$lang['passenger_details']						= "Détails du Client";
$lang['payment_details']						= "Détails du paiement";
$lang['enter_your_pickup_and_destination_details']= "Entrez votre pick-up et Destination Détails";
$lang['date_time']								= "Date and Time";
//$lang['date_time']								= "Date et heure";
$lang['click_and_select_the_date_and_time_you_would_like_to_be_picked_up']="Click & Sélectionnez la date et l'heure aimerais ramassé";
$lang['select_date']							= "Sélectionnez Date";
$lang['select_time']							= "Sélectionnez Heure";
$lang['wait_and_return_journey_required']		= "Trajet Retour";
$lang['total_journey_time']						= "TEMPS TOTAL VOYAGE";
$lang['total_journey_distance']						= "DISTANCE TOTALE VOYAGE";
$lang['total_cost']								= "COÛT TOTAL";
$lang['personal_details']						= "Détails personnels";
$lang['confirmed_bookings']						= "Réservations confirmées";
$lang['cancelled_bookings']						= "Réservations annulées";
$lang['pending_bookings']						= "En attendant Réservations";
$lang['local_address']							= "Adresse locale";

//passenger details
$lang['guest_check_out']						= "Réserver comme invité";
$lang['passengers_details']						= "Détails du Cliént";
$lang['phone_number']							= "Numéro de téléphone";
$lang['information_to_driver']					= "Pour information Pilote";
$lang['back_to_journey_details']				= "Retour Pour Trajet Détails";
$lang['next_payment_details']					= "Suivant Détails de paiement";
$lang['booking_summary']						= "Détails de la Réservation";
$lang['booking_reference']						= "Référence de réservation";
$lang['journey_type']							= "Type de Trajet";
$lang['two_way']								= "Two Way";
$lang['two_way_fare']							= "Two Way Fare";
$lang['one_way']								= "Aller Simple";
$lang['one_way_fare']							= "Tarif Aller Simple ";
$lang['pick_up']								= "Adresse de prise en charge"; // Ramasser
$lang['drop_of']								= "Adresse de Destination";
$lang['journey_miles_and_time']					= "Distance et Temps du Trajet";
$lang['car_type']								= "Type de voiture";
$lang['booking_confirmation']					= "Réservation Confirmation";
$lang['payment']								= "Paiement";
$lang['credit_card']							= "Carte de crédit";
$lang['cash']									= "Espèces";
$lang['pay_with_credit_card']					= "Payer par carte de crédit";
$lang['pay_with_paypal']						= "Payer avec PayPal";
$lang['pay_with_cash']							= "Payez espèces";
$lang['back_to_your_details']					= "Retour à vos données";
$lang['continue']								= "Continuer";


/** Packages**/
$lang['sno']									= "Sno";
$lang['package']								= "Paquet";
$lang['fare']									= "Aller";
$lang['after_distance']							= "Après la distance";
$lang['per_km'] 								= "Par kilomètre";
$lang['after_time']								= "Après le temps";
$lang['per_hr']									= "par Hr";
$lang['book_now']								= "Réserver Maintenant";
$lang['learn_more']								= "En savoir plus";

/** Package booking **/
$lang['package_booking']						= "Réservation de l'emballage";
$lang['select_your_car']						= "Sélectionnez votre voiture";
$lang['select_the_car_that_suits_to_your_requirement']	= "Sélectionnez la voiture qui convient à vos besoins";
$lang['package_details_caps']						= "PRÉCISIONS SUR LE FORFAIT";
$lang['total_time']								= "Temps total";
$lang['cost_details']							= "DÉTAILS DE COÛTS";
$lang['package_cost']							= "Forfait Coût";
$lang['extra_distance']							= "Distance supplémentaire";
$lang['extra_time']								= "Le temps supplémentaire";

/** Contact **/
$lang['mobile']									= "Mobile";
$lang['message']								= "Message";
$lang['email_sent_successfully_we_will_contact_you_as_soon_as_possible']="Courriel envoyé avec succès ... Nous vous contacterons dès que possible.";
$lang['unable_to_send_email']					= "Impossible d'envoyer un courriel.";

/** My account **/
$lang['mobile_number']							= "Numéro De Portable";


/**change password **/
$lang['old_password']							= "Ancien Mot De Passe";
$lang['password_changed_success']				= "Mot de passe changé avec succès.";

/** myBookings **/
$lang['one_way_journey_details']				= "Aller Simple Trajet Détails";
$lang['go_back_journey_details']				= "Aller Retour Trajet Détails";
$lang['status_canceled']						= "Annuléé";
$lang['unable_to_cancel_status']				= "Impossible d'annuler la Condition";
$lang['status_confirm']						= "Confirmé";

/**logout**/
$lang['success_logout']							= "Bien déconnecté";




















//PREVIOUS//
/*** User ***/
$lang['user'] 							= "Utilisateur";
$lang['new_user'] 						= "Nouvel utilisateur";
$lang['user_id'] 						= "Id De L'Utilisateur";
$lang['user_create_successful'] 		= "Utilisateur créé avec succès";
$lang['user_update_successful'] 		= "Utilisateur correctement mis à jour";


/*** Admin ***/
$lang['admin'] 							= "Admin";


//
$lang['new_executive'] 					= "New exécutif";


///


$lang['new_customer'] 					= "Nouveau client";
$lang['customer_id'] 					= "Client Id";


///
$lang['new_vehicle'] 					= "Nouveau véhicule";
$lang['vehicle_id'] 					= "Véhicule Id";
$lang['luggage']						= "Bagage";
$lang['fuel']							= "Carburant";
$lang['petrol']							= "Essence";
$lang['diesel']							= "Diesel";
$lang['gas']							= "Gaz";



////////
$lang['booking_id']						= "Réservation id";
$lang['pick_place']						= "Lieu de Pick-up";
$lang['allotted_driver_id']				= "Pilote alloué Id";


//***waiting times
$lang['from_time']						= "From Time";
$lang['to_time']						= "pour l'heure";


/*** Booking System Settings ***/
$lang['distance_type']					= "Type de Distance";
$lang['max_advance_booking_days']		= "Max. Réservez à l'avance Days";
$lang['system']							= "Système";


/*** Cost Type Values ***/
$lang['cost_type_values']				= "Type de coût valeurs";
$lang['day_flat_rate']					= "Jour forfaitaire";
$lang['night_flat_rate']				= "Nuit forfaitaire";
$lang['day_from_value']					= "Jour de la valeur";
$lang['day_to_value']					= "Day To Value";
$lang['night_from_value']				= "Nuit de la valeur";
$lang['night_to_value']					= "Nuit To Value";


/*** Airport Locations ***/
$lang['airport_location']				= "Aéroport Emplacement";
$lang['airport_locations']				= "Emplacements des Aéroport";


/*** Airport Cars ***/
$lang['airport_car']					= "Car l'aéroport";
$lang['airport_cars']					= "Airport Cars";


/*** Payments ***/
$lang['payments']						= "Paiements";
$lang['payment_amount']					= "Montant du paiement";
$lang['transaction_id']					= "Transaction Id";
$lang['transaction_status']				= "Transaction Status";
$lang['payer_name']						= "Nom Payer";
$lang['payer_email']					= "Payer Email";
$lang['booking_date']					= "Date de réservation";
$lang['booking_status']					= "Statut de réservation";
$lang['booking_successful']				= "Réservation Réussi";
$lang['booking_thanx']					= "Merci pour la réservation avec nous.";
$lang['cancel_booking']					= "Si vous souhaitez annuler la réservation après confirmation vous aurez besoin de nous informer en appelant nous sur +040 - 00 333 000 ou écrivez-nous à";
$lang['waiting_cost']					= "Coût d'attente";


/*** Drivers ***/
$lang['driver']							= "Conducteur";
$lang['drivers']						= "Pilotes";
$lang['driver_id']						= "Id pilote";
$lang['licence_no']						= "Licence No.";


/*** Language Settings ***/
$lang['language_settings']				= "Paramètres de langue";
$lang['language']						= "Langue";
$lang['language_code']					= "Code de langue";
$lang['language_name']					= "Nom de la langue";


/*** Days & Months ***/
$lang['monday'] 						= "Lundi";
$lang['tuesday'] 						= "Mardi";
$lang['wednesday'] 						= "Mercredi";
$lang['thursday'] 						= "Jeudi";
$lang['friday'] 						= "Vendredi";
$lang['saturday'] 						= "Samedi";
$lang['sunday'] 						= "Dimanche";
$lang['january']   			 			= "Janvier";
$lang['february']   					= "Février";
$lang['march']     					 	= "Mars";
$lang['april']      					= "Avril";
$lang['may']      					 	= "Mai";
$lang['june']       					= "Juin";
$lang['july']       					= "Juillet";
$lang['august']     					= "Août";
$lang['september']  					= "Septembre";
$lang['october']    					= "Octobre";
$lang['november']   					= "Novembre";
$lang['december']   					= "Décembre";


//CodeIgniter
// Errors
$lang['error_csrf'] = 'Cette forme post ne est pas passé nos contrôles de sécurité.';

// Login
$lang['login_heading']         = 'S\'identifier';
$lang['login_subheading']      = 'Se il vous plaît vous connecter avec votre email / nom d\'utilisateur et mot de passe ci-dessous.';
$lang['login_identity_label']  = 'Email';
$lang['login_password_label']  = 'Mot de passe:';
$lang['login_remember_label']  = 'Souviens-Toi De Moi:';
$lang['login_submit_btn']      = 'S\'identifier';



// Index
$lang['index_heading']           = 'Utilisateurs';
$lang['index_subheading']        = 'Voici une liste des utilisateurs.';
$lang['index_fname_th']          = 'Prénom';
$lang['index_lname_th']          = 'Nom De Famille';
$lang['index_email_th']          = 'Email';
$lang['index_groups_th']         = 'Groupes';
$lang['index_status_th']         = 'Statut';
$lang['index_action_th']         = 'Action';
$lang['index_active_link']       = 'Actif';
$lang['index_inactive_link']     = 'Inactif';
$lang['index_create_user_link']  = 'Créez un nouvel utilisateur';
$lang['index_create_group_link'] = 'Créer un nouveau groupe';

// Deactivate User
$lang['deactivate_heading']                  = 'Désactiver l\'utilisateur';
$lang['deactivate_subheading']               = "Etes-vous sûr que vous souhaitez désactiver l'utilisateur \ '% s \'";
$lang['deactivate_confirm_y_label']          = 'Oui:';
$lang['deactivate_confirm_n_label']          = 'No:';
$lang['deactivate_submit_btn']               = 'Soumettre'; 
$lang['deactivate_validation_confirm_label'] = 'Confirmation';
$lang['deactivate_validation_user_id_label'] = 'ID de l\'utilisateur';

// Create User
$lang['support'] = "Support";
$lang['create_user_heading']                           = 'Créer un utilisateur';
$lang['create_user_subheading']                        = "Se il vous plaît entrez l'utilisateur s \ 'informations ci-dessous.";
$lang['create_user_fname_label']                       = 'Prénom';
$lang['create_user_lname_label']                       = 'Nom De Famille';
$lang['create_gender_label']                       	   = 'Sexe : ';
$lang['create_dob_label']                       	   = 'Date De Naissance  ';
$lang['create_user_desired_location_label']			   = 'Lieu souhaité';
$lang['create_user_desired_job_type_label']			   = 'Type d\'emploi désiré';
$lang['create_user_open_for_contract_label']		   = 'Ouvert contrat';
$lang['create_user_pay_rate_label']		   			   = 'Taux de rémunération';
$lang['create_current_salary_label']		   		   = 'Salaire actuel';
$lang['create_city_label']							   = 'Ville';
$lang['create_state_label']							   = 'état';
$lang['create_country_label']						   = 'Pays';
$lang['create_fax_label']						   	   = 'Fax';
$lang['create_industry_label']						   = 'Industrie';

$lang['create_Zipcode_label']						   = 'Code postal';
$lang['create_willing_relocate_label']                 = 'Disposé à déménager: ';
$lang['create_user_company_label']                     = 'Nom de l\'entreprise:';
$lang['create_user_email_label']                       = 'Email';
$lang['create_user_primary_email_label']               = 'Courriel primaire';
$lang['create_user_secondary_email_label']             = 'Email secondaire';
$lang['create_user_phone_label']                       = 'Téléphone';

$lang['create_user_primary_phone_label']               = 'Téléphone primaire';
$lang['create_user_secondary_phone_label']             = 'Téléphone secondaire';
$lang['create_user_password_label']                    = 'Mot de passe:';
$lang['create_user_password_confirm_label']            = 'Confirmez Le Mot De Passe:';
$lang['create_user_submit_btn']                        = 'Créer un utilisateur';
$lang['create_user_validation_fname_label']            = 'Prénom';
$lang['create_user_validation_lname_label']            = 'Nom De Famille';
$lang['create_user_validation_email_label']            = 'Adresse e-mail';
$lang['create_user_validation_phone1_label']           = 'Première partie de Téléphone';
$lang['create_user_validation_phone2_label']           = 'Deuxième partie de Téléphone';
$lang['create_user_validation_phone3_label']           = 'Troisième partie de Téléphone';
$lang['create_user_validation_company_label']          = 'Nom de l\'entreprise';
$lang['create_user_validation_password_label']         = 'Mot de passe';
$lang['create_user_validation_password_confirm_label'] = 'Mot de passe Confirmation';
$lang['create_user_validation_address_label'] = 'Adresse';

// Edit User
$lang['edit_user_heading']                           = 'Modifier l\'utilisateur';
$lang['edit_user_subheading']                        = "Se il vous plaît entrez l'utilisateur s \ 'informations ci-dessous.";
$lang['edit_user_fname_label']                       = 'Prénom:';
$lang['edit_user_lname_label']                       = 'Nom De Famille:';
$lang['edit_user_company_label']                     = 'Nom de l\'entreprise:';
$lang['edit_user_email_label']                       = 'Email:';
$lang['edit_user_phone_label']                       = 'Téléphone:';
$lang['edit_user_password_label']                    = 'Mot de passe: (si le changement de passe)';
$lang['edit_user_password_confirm_label']            = 'Confirmez Le Mot De Passe: (si le changement de passe)';
$lang['edit_user_groups_heading']                    = 'Membre des groupes';
$lang['edit_user_submit_btn']                        = 'Enregistrer utilisateur';
$lang['edit_user_validation_fname_label']            = 'Prénom';
$lang['edit_user_validation_lname_label']            = 'Nom De Famille';
$lang['edit_user_validation_email_label']            = 'Adresse e-mail';
$lang['edit_user_validation_phone1_label']           = 'Première partie de Téléphone';
$lang['edit_user_validation_phone2_label']           = 'Deuxième partie de Téléphone';
$lang['edit_user_validation_phone3_label']           = 'Troisième partie de Téléphone';
$lang['edit_user_validation_company_label']          = 'Nom de l\'entreprise';
$lang['edit_user_validation_groups_label']           = 'Groupes';
$lang['edit_user_validation_password_label']         = 'Mot de passe';
$lang['edit_user_validation_password_confirm_label'] = 'Mot de passe Confirmation';

// Create Group
$lang['create_group_title']                  = 'Créer un groupe';
$lang['create_group_heading']                = 'Créer un groupe';
$lang['create_group_subheading']             = 'Se il vous plaît saisir les informations de groupe ci-dessous.';
$lang['create_group_name_label']             = 'Nom du groupe:';
$lang['create_group_desc_label']             = 'Description:';
$lang['create_group_submit_btn']             = 'Créer un groupe';
$lang['create_group_validation_name_label']  = 'Nom du groupe';
$lang['create_group_validation_desc_label']  = 'Description';

// Edit Group
$lang['edit_group_title']                  = 'Modifier le groupe';
$lang['edit_group_saved']                  = 'Groupe enregistrées';
$lang['edit_group_heading']                = 'Modifier le groupe';
$lang['edit_group_subheading']             = 'Se il vous plaît saisir les informations de groupe ci-dessous.';
$lang['edit_group_name_label']             = 'Nom du groupe:';
$lang['edit_group_desc_label']             = 'Description:';
$lang['edit_group_submit_btn']             = 'Enregistrer le groupe';
$lang['edit_group_validation_name_label']  = 'Nom du groupe';
$lang['edit_group_validation_desc_label']  = 'Description';

// Change Password
$lang['change_password_heading']                               = 'Changer Le Mot De Passe';
$lang['change_password_old_password_label']                    = 'Ancien Mot De Passe:';
$lang['change_password_new_password_label']                    = 'Nouveau Mot De Passe (les caractères d\'au moins% à long):';
$lang['change_password_new_password_confirm_label']            = 'Confirmer le nouveau mot de passe:';
$lang['change_password_submit_btn']                            = 'Changement';
$lang['change_password_validation_old_password_label']         = 'Ancien Mot De Passe';
$lang['change_password_validation_new_password_label']         = 'Nouveau Mot De Passe';
$lang['change_password_validation_new_password_confirm_label'] = 'Confirmer le nouveau mot de passe';

// Forgot Password
$lang['forgot_password_heading']                 = 'Mot De Passe Oublié';
$lang['forgot_password_subheading']              = 'Se il vous plaît entrez votre% s afin que nous puissions vous envoyer un e-mail pour réinitialiser votre mot de passe.';
$lang['forgot_password_email_label']             = '% s:';
$lang['forgot_password_submit_btn']              = 'Soumettre';
$lang['forgot_password_validation_email_label']  = 'Adresse e-mail';
$lang['forgot_password_username_identity_label'] = 'Nom d\'utilisateur';
$lang['forgot_password_email_identity_label']    = 'Email';
$lang['forgot_password_email_not_found']         = 'Aucune trace de cette adresse e-mail.';

// Reset Password
$lang['reset_password_heading']                               = 'Changer Le Mot De Passe';
$lang['reset_password_new_password_label']                    = 'Nouveau Mot De Passe (les caractères d\'au moins% à long):';
$lang['reset_password_new_password_confirm_label']            = 'Confirmer le nouveau mot de passe:';
$lang['reset_password_submit_btn']                            = 'Changement';
$lang['reset_password_validation_new_password_label']         = 'Nouveau Mot De Passe';
$lang['reset_password_validation_new_password_confirm_label'] = 'Confirmer le nouveau mot de passe';


//New Kalyan start

$lang['failed']												  = 'Manqué';



//New Kalyan end


//New Raghu Start
$lang['in_kms'] 											  = 'Dans KMs';

//New Raghu End



// start
$lang['currency_code_alpha']			= "Code Currency Alpha";
$lang['currency_name']					= "Nom de la devise";
$lang['user file']						= "Fichier de l'utilisateur";
$lang['user name']						= "Nom d'utilisateur";
$lang['account_deactivated']			= "Compte Désactivé";
$lang['Day']							= "Jour";
$lang['url']							= "URL";
$lang['symbol']							= "Symbole";
$lang['start']							= "Début";
$lang['end']							= "Fin";
$lang['Night']							= "Nuit";
$lang['not_working_day']                                        = "Jour Ferie";
$lang['minimum_fee']                                        = "Tarif Minimum";




//

$lang['added'] 							= "Ajouté";
$lang['time_from']						= "De temps (minutes)";
$lang['time_to']						= "Time To (minutes)";
$lang['email_received']					= "Email reçu , nous vous contacterons dès que possible.";
$lang['select_waiting_time']			= "Temps d'attente";
$lang['type_your_address_here'] = 'Tapez votre adresse ici';
$lang['copyright'] = 'Copyright &copy; 2020 · ' . APP_NAME . ' SAS Tous les droits réservés.';
$lang['bookmark_us'] = 'Ajouter aux favoris';
$lang['type_ur_address'] = 'Tapez votre adresse içi';
$lang['terms_conditions'] = 'Conditions Generales de vente';
$lang['privacy_policy'] = 'Politique de vie privée';
$lang['report_abuse'] = 'Reporter un abus';
$lang['legal_notice'] = 'Mentions Légales';
$lang['page_fleet'] = "Parc";
$lang['vehicle_details_caps'] = 'Les détails du véhicule';
$lang['start_from'] = "à partir de";
$lang['our_commitments'] = "Nos Engagements";
$lang['benefits'] = "Avantages";
$lang['benefits_txt'] = "Pourquoi choisir " . APP_NAME . "Pour vos transferts?";
$lang['value_for_money'] = "Le rapport qualité prix";
$lang['value_for_money_txt'] = "Un Transfert de qualité à des prix abordables";
$lang['customer_service'] = "Service Clients";
$lang['customer_service_txt'] = "Un service client de qualité, pour vous assister par téléphone, par email, ou par chat en ligne.";
$lang['easy_of_use'] = "Facilité d'utilisation";
$lang['easy_of_use_txt'] = "Une Réservation facile en 4 étapes seulement, et un espace client pour suivre vos réservations en ligne.";
$lang['punctuality'] = "Ponctualite";
$lang['security'] = "Securite";
$lang['quality'] = "Qualite";
$lang['economy'] = "Economie";

/***Services**/
$lang['services_pages']						= "Services Pages";
$lang['see_more']						= "voir plus";
$lang['live_traffic'] = 'Info Trafic';
$lang['map'] = 'Carte';
$lang['car'] = 'Véhicule';
$lang['track_flight'] = 'Suivre le vol';
$lang['flight_id'] = 'Vol ID';
$lang['track'] = 'Piste';
$lang['loading_flight_status'] = "S'il vous plaît patienter pendant le chargement état de vol...";
$lang['EUR'] = '&euro;';
$lang['confirmation_approval'] = 'Un email de confirmation vous sera envoyé quand la réservation sera confirmée';
$lang['passenger'] = "Passager";
$lang['booking_email_label'] = 'Vous trouverez ci-dessous une nouvelle Réservation en ligne N°: ';
//  $lang['booking_email_note'] = 'Note : Votre réservation est au statut "En Attente de Confirmation" Vous allez recevoir un autre email de confirmation quand son statut passera à "Confirmée", pour toute autre question merci de nous appeler par téléphone pendant nos horaires d\'ouverture.';
$lang['booking_email_note'] = 'Remarque: Pour plus d\'informations merci de nous contacter par téléphone pendant les heures d\'ouverture de nos bureaux.';
$lang['status_pending'] = 'En Attente';
$lang['regards'] = 'Bien cordialement';
$lang['incl_waiting_time_return_journey'] = "Incl Temps d'attente et de retour Journey";
$lang['passengers'] = 'Passagers';
$lang['luggages'] = 'Bagages';
$lang['wheelchair'] = 'Fauteuil Roulant';
$lang['baby_seat'] = 'Siége bébé';
$lang['train'] = 'Gare';
$lang['hotel'] = 'Hôtel';
$lang['park'] = 'Parc';
$lang['pet'] = 'Animal de Companie';


/* Station - Gare  */
$lang['station_settings']				= "Paramètres de Gare";
$lang['list_stations']					= "Liste Garés";

$lang['stations']                                       = "Garés";
$lang['station_name']					= "Nom de l'garé";
$lang['station_address']				= "Garé Adresse";
$lang['station']					= "Gare";
$lang['add_station']					= "Ajoutez l'garé";
$lang['edit_station']					= "Modifier l'garé";
$lang['enter_station_name']				= "Entrez Nom de Garé";

/* Hotel */
$lang['hotel_settings']				= "Paramètres de Hôtel";
$lang['list_hotels']					= "Liste Hôtels";

$lang['hotels']                                     = "Hôtels";
$lang['hotel_name']                                 = "Hôtel Name";
$lang['hotel_address']                                 = "Hôtel Adresse";
$lang['hotel']                                      = "Hôtel";
$lang['add_hotel']					= "Ajoutez Hôtel";
$lang['edit_hotel']					= "Modifier Hôtel";
$lang['enter_hotel_name']				= "Entrez Nom de Hôtel";

/* Park */
$lang['park_settings']				= "Park Settings";
$lang['list_parks']					= "Liste Pacs";

$lang['parks']                                     = "Pacs";
$lang['park_name']                                 = "Nom de Pacs";
$lang['add_park']					= "Ajoutez Pacs";
$lang['edit_park']					= "Modifier Pacs";
$lang['enter_park_name']				= "Entrez Nom de Pacs";

$lang['billing_address'] = "Adresse de Facturation";

$lang['one_time'] = 'Occasionnel';
$lang['regular'] = 'Regulier';
$lang['services'] = 'Services';

$lang['go_back'] = 'Aller Retour';

$lang['start_time']					= "Heure de début";
$lang['end_time']					= "Heure de fin";

$lang['go'] = 'Aller';
$lang['back'] = 'Retour';

// MAIN MENU
$lang['transfer_airport'] = 'Transfert Aeroport';
$lang['transfer_dorly'] = "Transfert Aeroport D'Orly";
$lang['transfer_cdg'] = "Transfert Aeroport CDG ";
$lang['transfer_beauvais'] = "Transfert Aeroport Beauvais";
$lang['transfer_lebourget'] = "Transfert Aéroport Le Bourget";
$lang['transfer_parisvatry'] = "Transfert Aéroport Paris Vatry";

$lang['transfer_railways'] = 'Transfert Gare';
$lang['transfer_dunord'] = 'Transfert Gare Du Nord';
$lang['transfer_montparnasse'] = 'Transfert Gare Montparnasse';
$lang['transfer_saintlazard'] = 'Transfert Gare Saint Lazard';
$lang['transfer_delyon'] = 'Transfert Gare De Lyon';
$lang['transfer_delest'] = "Transfert Gare De l'Est";

$lang['transfer_parkgarden'] = 'Transfert Parc';
$lang['transfer_disneyland'] = 'Transfert Parc DisneyLand';
$lang['transfer_asterix'] = 'Transfert Parc Asterix';
$lang['transfer_paristour'] = 'Paris Tour';
$lang['transfer_versailles'] = 'Transfert Versailles';

$lang['add_extra_stop'] = 'Ajouter un Arrêt';
$lang['remove_extra_stop'] = "Supprimer l'Arrêt";

$lang['partner_access'] = 'Acces Partenaire';
$lang['partner_login'] = 'Identification Partenaire';
$lang['driver_login'] = 'Identification Chauffeur';
$lang['client_login'] = 'Identification Client';

$lang['quote'] = 'DEVIS';
$lang['request'] = 'Quote Request';
$lang['requests'] = 'Quote Requests';
$lang['name_address'] = "Nom ou complement d'adresse";
$lang['address_postcode_city'] = "Adresse, Code Postal, City";
$lang['payment'] = "Paiement";

$lang["booking_source"] = 'Addresse de prise en charge';
$lang["booking_destination"] = 'Addresse de destination';
$lang['our_packages'] = 'Nos Forfaits';
$lang['package_price'] = 'Prix Forfait';
$lang['zones_page'] = "Zones";
$lang['waiting'] = "Attente";
$lang['contact_us_button'] = "Envoyer";

$lang['identification'] = "Identification";
$lang['pending'] = 'Attente';
$lang['pending'] = 'Pending';
$lang['confirm'] = 'Confirmée';
$lang['cancelled'] = 'Annulée';
$lang['action_confirm'] = "Confirmée";
$lang['action_cancel']	= "Annulée";

$lang['pickup_location']	= "Adresse de prise en charge";
$lang['drop_location']	= "Adresse de destination";

$lang['already_client'] = 'Deja Client ?';
$lang['new_client'] = 'Nouveau Client ?';

$lang['drop2_address_label'] = 'Adresse de Départ';
$lang['drop2_airport_label'] = 'Sélectionner un Aéroport';
$lang['drop2_train_label'] = 'Sélectionner une Gare';
$lang['drop2_hotel_label'] = 'Sélectionner un Hotel';
$lang['drop2_park_label'] = 'Sélectionner un Parc';
$lang['extra_stop_label'] = 'Un Arrét';

$lang['number'] = 'Numéro';

$lang['cash_to_driver'] = "Espéce au Chauffeur";
$lang['total_price_to_pay_now'] = "Total à régler maintenant";

$lang['my_quotes'] = 'Mes Devis';
$lang['my_invoices'] = 'Mes Factures';
$lang['my_support_tickets'] = 'Mes Tickets Support';
$lang['reservation'] = 'Réservation';



$lang['newsletter'] = 'Bulletin';
$lang['send_time'] = 'Heure d\'envoi';
$lang['user_type'] = 'Type d\'utilisateur';
$lang['user_status'] = 'Statut de l\'utilisateur';
$lang['subject'] = 'Matière';
$lang['send_date'] = 'Envoyer la date';
$lang['old'] = 'Vieille';



$lang['filemanagement'] = 'Files';
$lang['nature'] = 'Nature';
$lang['priority'] = 'Priority';
$lang['alert'] = 'Alert';
$lang['delay_date'] = 'Delay';
$lang['selection'] = 'Selection';
$lang['name_selection'] = 'Name';
$lang['note'] = 'Note';
$lang['file'] = 'File';


$lang['task'] = 'Tasks';
$lang['date'] = 'Date';
$lang['date_hour'] = '';
$lang['date_minute'] = '';
$lang['added_by_firstname'] = 'Added By First Name';
$lang['added_by_lastname'] = 'Added By Last Name';
$lang['affected_department'] = 'Department';
$lang['affected_user'] = 'Users';
//$lang['priority'] = 'File';
$lang['date2'] = 'Date';
$lang['date2_hour'] = '';
$lang['date2_minute'] = '';
$lang['files'] = 'Files';
$lang['note'] = 'Note';
$lang['note2'] = 'Note';
$lang['task_time'] = 'Time';
$lang['added_by'] = 'Added by';
$lang['from_department'] = 'From department';
$lang['affected_to'] = 'Affected to';
$lang['delay_date'] = 'Delay date';
$lang['delay_time'] = 'Delay time';
$lang['to_be_done_before'] = 'To be done before';
$lang['add_files'] = 'Add files';
$lang['task_description'] = 'Task Description';

//
$lang['configurations'] = 'Configurations';
$lang['module'] = 'Module';
$lang['message_sentence'] = 'Phrase du premier message';
$lang['sender_name'] = 'Sender Name';
$lang['smtp'] = 'Configurations SMTP';
$lang['popups'] = 'Configurations des fenêtres contextuelles';
$lang['select_all'] = 'All';
$lang['add_note'] = 'Add note';
$lang['quick_replies'] = 'Réponses rapides';
$lang['callback'] = 'Configurations de rappel';
$lang['company_profile'] = 'Profil de la société';
$lang['departments'] = 'Départements';
$lang['supplier'] = 'fournisseuse';
$lang['smtp_text'] = 'SMTP';
$lang['job_applications'] = "Demandes d'emploi";
$lang['quote_requests'] = 'Demandes de devis';
$lang['CRM'] = 'CRM';
$lang['CMS'] = 'CMS';
$lang['accounting'] = 'Comptabilité';
$lang['sent_by'] = 'Envoyée par';
$lang['attachment'] = 'Attachment';
$lang['send_copy'] = 'Envoyer une copie';
$lang['reminders'] = "Rappels";
$lang['reminders1'] = "Rappels aux conducteurs";
$lang['view'] = "Vue";
$lang['documents'] = "Les Documents";

//booking_config
$lang['name_of_poi'] = "Nom du POI";
$lang['name_of_vat'] = "Nom de vat";
$lang['name_of_discount'] = "Nom de remise";
$lang['discount'] = "Remise";
$lang['name_of_category'] = "Nom de Catégorie";
$lang['category'] = "Catégorie";
$lang['vat'] = "TVA";
$lang['price_calculation_method'] = "Prix calcul méthode";
$lang['booking_form'] = "Réservation forme";

$lang['name_of_service'] = "Nom de service";
$lang['service_category'] = "Service catégorie";

$lang['client_Category_name'] = "cliente catégorie Nom";
$lang['invoice_Period'] = "facture d'achat Période";

$lang['price_KM_distance'] = "Price / Km";
$lang['night_fee'] = "Night Fee";
$lang['driver_meal_cost'] = "Driver Meal Cost";
$lang['price_minute_time'] = "Price / Minute";
$lang['not_working_day_fee'] = "Not working day fee";
$lang['driver_rest_cost'] = "Driver Rest Cost";
$lang['approche_rate'] = "Approche Rate";
$lang['mai_december_holiday'] = "1st Mai and 25 December Fee";
$lang['driver_bonus_cost'] = "Driver Bonus Cost";
$lang['waiting_time_price_minute'] = "Waiting Time (PRICE / Minute)";
$lang['night_time_sch'] = "Night Time";
$lang['no_working_day_dates'] = "Not working day";
$lang['driver_meal_sch'] = "Driver Meal";
$lang['driver_rest_sch'] = "Driver Rest";
$lang['driver_bonus_fee'] = "Driver Bonus";
$lang['booking_discount'] = "Discount";

$lang['price_KM_of_Approach_Course'] = "Price / KM of Parcours D'approche";
$lang['price_KM_ROUTE'] = "Price / KM TRAJET";
$lang['price_KM_path_Return'] = "Price / KM TRAJET Retour";
// $lang['waiting'] = "Attente";
$lang['car_seat_baby_seat'] = "Siege BEBE";
$lang['baggage'] = "Bagages";
$lang['pickup_fee'] = "Pickup Fee";
$lang['night_surcharge'] = "Majoration de nuit";
$lang['holiday_day_supplement'] = "Supplement jour ferie ";
$lang['supplement_may_1_and_december_25'] = "Supplement 1er mai et 25 decembre";
$lang['pet'] = "Animal de Compagnie";
$lang['accompanying_person'] = "Accompagnateur";
$lang['baby_seat_rental'] = "Location De Siege BEBE";
$lang['minimum_fee'] = "Minimum Fee";
$lang['packed_lunch'] = "Panier Repas";
$lang['compensatory_rest'] = "Repos Compensateur";
$lang['rest'] = "Repos";
$lang['passenger'] = "Passager";
$lang['armchair'] = "Fauteuil";
$lang['armchair_rental'] = "Location Fauteuil";
$lang['persons'] = "Persons";


$lang['type_name'] = "Nom du type";
$lang['adaptation'] = "Adaptation";
$lang['passengers'] = "Passagers";
$lang['luggages'] = "Lugages";
$lang['weelchairs'] = "Fauteuils";
$lang['baby_sets'] = "Ensembles bébé";
$lang['animals'] = "Animaux";
$lang['quote_id'] = "ID de devis";
$lang['sales_operator'] = "Opérateur de vente";
$lang['recall_day'] = "Jours de Rappel";
$lang['recall_time'] = "Temps de Rappel";
$lang['address1'] = "Adresse";
$lang['address2'] = "Adresse 2";
$lang['situation'] = "Situation";