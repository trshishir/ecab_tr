<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * Author: Raghu Veer K (rgh.veer@gmail.com)
 * Script: en_lang.php
 * English translation file
 *
 * Last edited:
 * 
 *
 * Project:
 * Digital Vehicle Booking System v1.0 (DVBS)
 */



/* * ********************** COMMON WORDS ********************* */
$lang['hello'] = "Olá";
$lang['hi'] = "oi";
$lang['welcome'] = "Bem-vindo ao";
$lang['site'] = "local";
$lang['home'] = "casa";
$lang['logo'] = "logotipo";
$lang['page_title'] = "Título da página";
$lang['header_title'] = "Cabeçalho Título";
$lang['header'] = "cabeçalho";
$lang['footer'] = "rodapé";
$lang['status'] = "estado";
$lang['contact_us'] = "Entre Em Contato Conosco";
$lang['about_us'] = "Sobre Nós";
$lang['site_map'] = "Site Map";
$lang['map'] = "mapa";
$lang['settings'] = "Definições";
$lang['reports'] = "relatórios";
$lang['logout'] = "Sair";
$lang['login'] = "Conecte-Se";
$lang['access_denied'] = "Acesso negado!";
$lang['error'] = "Erro!";
$lang['forgot_pw'] = "Esqueceu Sua Senha?";
$lang['remember_me'] = "lembre de mim";
$lang['back_to_login'] = "Voltar à página de login";
$lang['search'] = "pesquisa";
$lang['notifications'] = "Notificações";
$lang['password'] = "senha";
$lang['change_password'] = "Alterar A Senha";
$lang['current_password'] = "Senha Atual";
$lang['new_password'] = "nova Senha (pelo menos 8 caracteres)";
$lang['confirm_password'] = "confirme Sua Senha";
$lang['profile'] = "perfil";
$lang['title'] = "título";
$lang['content'] = "conteúdo";
$lang['type'] = "tipo";
$lang['name'] = "nome";
$lang['disabled_in_demo'] = "Esse recurso é desativado em demonstração";
$lang['first_name'] = "primeiro Nome";
$lang['last_name'] = "Sobrenome";
$lang['pw'] = "senha";
$lang['old_pw'] = "Senha Antiga";
$lang['new_pw'] = "nova Senha";
$lang['confirm_pw'] = "confirme Sua Senha";
$lang['code'] = "código";
$lang['dob'] = "DOB";
$lang['image'] = "imagem";
$lang['photo'] = "foto";
$lang['note'] = "nota";
$lang['upload_file'] = "Upload de arquivo";
$lang['email'] = "Email";
$lang['email_address'] = "Endereço De Email";
$lang['phone'] = "telefone";
$lang['office'] = "escritório";
$lang['company'] = "companhia";
$lang['website'] = "site";
$lang['doj'] = "DOJ";
$lang['fax'] = "fax";
$lang['contact'] = "contato";
$lang['experience'] = "experiência";
$lang['location'] = "localização";
$lang['location_id'] = "Localização Id";
$lang['address'] = "endereço";
$lang['city'] = "cidade";
$lang['state'] = "estado";
$lang['country'] = "país";
$lang['zip_code'] = "cEP";
$lang['about'] = "sobre";
$lang['description'] = "descrição";
$lang['time'] = "tempo";
$lang['date'] = "data";
$lang['from'] = "a partir de";
$lang['to'] = "para";
$lang['cost'] = "custo";
$lang['price'] = "preço";
$lang['rate'] = "taxa";
$lang['amount'] = "quantidade";
$lang['total'] = "total";
$lang['start_date'] = "Data De Início";
$lang['end_date'] = "Data de Término";
$lang['size'] = "tamanho";
$lang['header_logo'] = "Header logo";
$lang['login_logo'] = "Entrar logo";
$lang['theme'] = "tema";
$lang['menus'] = "menus";
$lang['help'] = "Socorro";
$lang['yes'] = "sim";
$lang['no'] = "não";
$lang['documentation'] = "documentação";
$lang['first'] = "primeiro";
$lang['last'] = "último";
$lang['next'] = "Next";
$lang['previous'] = "anterior";
$lang['category'] = "categoria";
$lang['category_id'] = "categoria Id";
$lang['sub_category'] = "Sub Categoria";
$lang['sub_category_id'] = "Sub Categoria Id";
$lang['actions'] = "ações";
$lang['operations'] = "operações";
$lang['create'] = "criar";
$lang['add'] = "adicionar";
$lang['edit'] = "editar";
$lang['update'] = "atualizar";
$lang['save'] = "Salvar";
$lang['submit'] = "submeter";
$lang['reset'] = "restabelecer";
$lang['delete'] = "excluir";
$lang['feature'] = "característica";
$lang['create_success'] = "Criado com sucesso";
$lang['add_success'] = "Adicionado com sucesso";
$lang['save_success'] = "salvo com sucesso";
$lang['update_success'] = "Atualizado com sucesso";
$lang['delete_success'] = "excluído com sucesso";
$lang['status_change_success'] = "Status Alterado com sucesso";
$lang['make_active'] = "Make Active";
$lang['make_inactive'] = "Faça Inativo";
$lang['record'] = "registro";
$lang['not_exist'] = "Não existe";
$lang['session_expired'] = "Sessão expirou!";
$lang['enable'] = "possibilitar";
$lang['disable'] = "incapacitar";
$lang['flat'] = "apartamento";
$lang['incremental'] = "incremental";
$lang['please'] = "Por Favor";
$lang['select'] = "selecionar";
$lang['value'] = "valor";
$lang['in'] = "em";
$lang['mins'] = "mins";
$lang['in_mins'] = "em Mins";
$lang['please_edit_record'] = "Por favor edite a ficha que você deseja atualizar";
$lang['valid_url_req'] = "Digite uma URL válida.";
$lang['valid_image'] = "Apenas as imagens jpg / jpeg / png são aceitos.";
$lang['confirm_delete'] = "Tem certeza de que deseja excluir este registro?";
// $lang['confirm']						= "Você tem certeza?";
$lang['is'] = 'é';
$lang['unable'] = "incapaz";
$lang['telugu'] = "Telugu";
$lang['english'] = "Inglês";
$lang['hindi'] = "não";
$lang['route_map'] = "Mapa da Rota";
$lang['question'] = "pergunta";
$lang['answer'] = "resposta";
$lang['theme_settings'] = "Definições de Tema";
$lang['change_theme'] = "Mudar Tema";
$lang['change_site_theme'] = "Alterar local do tema";
$lang['view_today_bookings'] = "Ver Hoje Reservas";
$lang['close'] = "perto";
$lang['warning'] = "aviso";
$lang['sure_delete'] = "Tem certeza que deseja deletar?";
$lang['alert'] = "alerta";
$lang['info'] = "Informações";
$lang['delete_vehicle'] = "Veículos estão disponíveis nesta categoria. Se você deseja excluir esta, excluir todos os veículos desta categoria primeiro.";
$lang['welcome_vbs'] = "Welcome To Digi Veículo Sistema de Reserva  ( VBS )";
$lang['thanx'] = "Obrigado pelo seu interesse em registrar com VBS. É muito útil para a reserva do veículo";
$lang['bottom_message'] = "direitos autorais ©  2014  Digi Veículo Sistema de Reserva (VBS)  Todos os direitos reservados.";
$lang['login_heading'] = "CONECTE-SE";
$lang['forgot_pwd_msg'] = "Obter sua senha esqueci aqui. Esqueceu Sua Senha";
$lang['plane'] = "avião";
$lang['saloon'] = "BAR";
$lang['estate'] = "BENS";
$lang['instruction_to_driver'] = "Instruções aos Motorista";
$lang['next_your_details'] = "Próximo Seus Detalhes";
$lang['left_side_elements'] = "LADO ESQUERDO ELEMENTS ";
$lang['land_line'] = "Linha terra";
$lang['alert_change_site_theme'] = "Você quer mudar o tema do site para ";



/* admin header */
$lang['booking_system'] = "Sistema de Reservas";
$lang['CAB_admin'] = "CAB Administrador";
$lang['view_unread_bookings'] = "Ver Unread Reservas";
$lang['log_out'] = "Sair";
$lang['read_all_new_messages'] = "Leia todas as mensagens novas";
$lang['our_car_booking'] = "Nossa reserva de carro";


/* admin navigation */
$lang['dashboard'] = "painel de instrumentos";
$lang['bookings'] = "reservas";
$lang['vehicle_settings'] = "Configurações de veículos";
$lang['list_vehicles'] = "Lista Veículos";
$lang['add_vehicle'] = "Adicionar Vehicle";
$lang['airport_settings'] = "Configurações Aeroporto";
$lang['list_airports'] = "lista de aeroportos";
$lang['users'] = "usuários";
$lang['list_customers'] = "Lista Clientes";
$lang['list_executives'] = "Lista Executives";
$lang['master_settings'] = "Configurações de Mestre";
$lang['hourly_package_settings'] = "Configurações do pacote de hora em hora";
$lang['dynamic_pages'] = "As páginas dinâmicas";


//**dashboard**//
$lang['add_booking'] = "Adicionar Booking";
$lang['search_bookings'] = "pesquisa Reservas";
$lang['today_bookings'] = "Hoje Reservas";
$lang['latest_users'] = "Últimos Usuários";
$lang['reg_date'] = "Reg-Date";
$lang['view_details'] = "Ver detalhes";
$lang['view_all'] = "Ver Todos";
$lang['booking_details'] = "Detalhes da Reserva";
$lang['no_users'] = "Nenhum usuário Disponíveis.";
$lang['no_recent_bookings'] = "Sem reservas recente disponível.";



/* * * Bookings ** */
$lang['add_booking'] = "Adicionar Booking";
$lang['book'] = "livro";
$lang['pick_date'] = "Data de Pick-up";
$lang['pick_time'] = "Tempo de Pick-up";
$lang['source'] = "fonte";
$lang['destination'] = "destino";
$lang['payment_type'] = "Tipo de pagamento";
$lang['successful'] = "bem sucedido";
$lang['unable_to_book'] = "Incapaz Reservar";
$lang['search_bookings'] = "pesquisa Reservas";
$lang['customer_name'] = "Nome do cliente";
$lang['customer_phone'] = "de Atendimento ao Cliente";
$lang['customer_email'] = "cliente Email";
$lang['canceled'] = "Cancelado";
$lang['recent_bookings'] = "Reservas recentes";
$lang['total_bookings'] = "total de Reservas";
$lang['date_of_booking'] = "Data da Reserva";
$lang['booking_ref'] = "Booking Ref Não";
$lang['return_journey'] = "Viagem de retorno exigida";



/* * Today Bookings* */
$lang['today_bookings'] = "Hoje Reservas";
$lang['all_bookings'] = "todas as reservas";
$lang['book_date'] = "livro Data";
$lang['pick_point'] = "Local de pick";
$lang['drop_point'] = "ponto de gota";
$lang['vehicle_selected'] = "veículo seleccionado";
$lang['cost_of_journey'] = "Custo de Journey";
$lang['payment_received'] = "Pagamento Recebido";
$lang['confirm'] = "confirmar";
$lang['cancel'] = "cancelar";
$lang['booking_reference_no'] = "Booking Número de Referência";
$lang['unread_bookings'] = "Reservas não lidas";

/** Search Bookings* */
$lang['enter_from_date'] = "Digite De Data";
$lang['enter_to_date'] = "Digite To Date";
$lang['from_date'] = "A partir do dia";
$lang['to_date'] = "a Data";


/* * * Vehicle ** */
$lang['vehicles'] = "veículos";
$lang['passenger_capacity'] = "passageiros Capacidade";
$lang['large_luggage_capacity'] = "Grande capacidade de bagagem";
$lang['small_luggage_capacity'] = "Pequeno capacidade de bagagem";
$lang['cost_type'] = "Tipo de Custo";
$lang['vehicle'] = "veículo";
$lang['model'] = "modelo";
$lang['fuel_type'] = "Tipo de combustível";
$lang['total_vehicles'] = "total de Veículos";
$lang['starting'] = "começando";
$lang['day'] = "DIA";
$lang['night'] = "NOITE";
$lang['car_name'] = "Nome Car";
$lang['car_selected'] = "carro Selecionado";
$lang['in_kgs'] = "Em kg de";
$lang['eg_car_conditions_etc'] = "eg: condições carro etc";
$lang['total_number_of_vehicles_available'] = "N.º total de veículos disponíveis";
$lang['minimum_day_distance'] = "A distância mínima dia";
$lang['minimum_day_cost'] = "Custo mínimo dia";
$lang['minimum_night_distance'] = "A distância mínima noite";
$lang['minimum_night_cost'] = "Custo mínimo noite";
$lang['day_flat_cost'] = "Custo fixo Dia";
$lang['night_flat_cost'] = "Custo fixo Noite";
$lang['from_in_kms'] = "A partir de em KMs";
$lang['to_in_kms'] = "Para nos KMs";
$lang['day_cost'] = "Custo Day";
$lang['night_cost'] = "Custo noite";
$lang['vehicle_cost_details'] = "Vehicle & Cost Details";
$lang['vehicle_name'] = "Nome do veículo";
$lang['location_address'] = "Localização de endereços";
$lang['pick_date_vehicles'] = "Escolher Data";
$lang['pick_time_vehicles'] = "Escolha Tempo";




/* * * Vehicle Categories ** */
$lang['vehicle_categories'] = "Categorias de veículos";
$lang['vehicle_category'] = "Categoria do veículo";
$lang['add_vehicle_category'] = "Adicionar categoria Veículo";
$lang['edit_vehicle_category'] = "Categoria Editar Vehicle";
$lang['eg_premimum_general_etc'] = "eg: Premimum,General etc";


/* * * Vehicle Features ** */
$lang['vehicle_features'] = "Características do veículo";
$lang['vehicle_feature'] = "Característica veículo";
$lang['add_vehicle_feature'] = "Incluir Recurso Veículo";
$lang['edit_vehicle_feature'] = "Recurso de edição de Veículo";
$lang['eg_ac_non_ac_video_coach_etc'] = "eg:AC, não AC, treinador Vídeo etc";


/* * * Airports ** */
$lang['airports'] = "aeroportos";
$lang['airport_name'] = "Nome Airport";
$lang['airport'] = "aeroporto";
$lang['add_airport'] = "Adicionar Aeroporto";
$lang['edit_airport'] = "Editar Aeroporto";
$lang['enter_airport_name'] = "Digite o Nome do aeroporto";

//$lang['airport_id']						= "Airport Id";


/* * * Customers ** */
$lang['customers'] = "clientes";
$lang['index_active'] = "bloco";
$lang['index_inactive'] = "desobstruir";
$lang['user_name'] = "Nome de Usuário";


//**add customer**//
$lang['add_customer'] = "Adicionar ao Cliente";


/* * * Executives ** */
$lang['executives'] = "executivos";
$lang['add_executive'] = "Adicionar Executivo";
$lang['edit_executive'] = "Editar Executivo";


/* * * Site Settings ** */
$lang['site_settings'] = "Configurações do Site";
$lang['site_url'] = "URL do site";
$lang['address'] = "endereço";
$lang['city'] = "cidade";
$lang['state'] = "estado";
$lang['country'] = "país";
$lang['zip_code'] = "Cep";
$lang['phone'] = "telefone";
$lang['fax'] = "fax";
$lang['contact_email'] = "Contato e-mail";
$lang['currency_code'] = "Código da Moeda";
$lang['currency_symbol'] = "Símbolo de moeda";
$lang['distance_type'] = "Distância Tipo";
$lang['waiting_time'] = "Tempo de espera";
$lang['airports'] = "aeroportos";
$lang['day_start_time'] = "Dia Start Time";
$lang['day_end_time'] = "Dia End Time";
$lang['night_start_time'] = "Noite Start Time";
$lang['night_end_time'] = "Noite End Time";
$lang['site_theme'] = "Tema do Site";
$lang['email_type'] = "Email Tipo";
$lang['design_by'] = "design by";
$lang['rights_reserved'] = "direitos Reservados";
$lang['unable_to_update'] = "Não é possível atualizar";
$lang['value_should_be_in_24hrs_format'] = "O valor deve estar no formato de 24 horas";
$lang['faqs'] = "FAQ's";
$lang['faq'] = "FAQ";
$lang['payment_method'] = "método de pagamento";
$lang['date_format'] = "formato da data";




/* * * Testimonials  Settings ** */
$lang['testimonial_settings'] = "Configurações Apreciação";
$lang['testimonials'] = "Depoimentos";
$lang['author'] = "autor";
$lang['action'] = "ação";
$lang['add_testimony'] = "Adicionar Depoimento";
$lang['unable_to_add'] = "Não é possível adicionar";
$lang['location_name'] = "Localização Nome";
$lang['invalid'] = "inválido";
$lang['operation'] = "operação";
$lang['unable_to_delete'] = "Não é possível excluir";
$lang['edit_testimony'] = "Editar Testemunho";


/* * * Email Settings ** */
$lang['email_settings'] = "Configurações de e-mail";
$lang['host'] = "hospedeiro";
$lang['port'] = "porto";
$lang['host_name'] = "Nome do host";


/* * * Paypal Settings ** */
$lang['paypal_settings'] = "Configurações paypal";
$lang['paypal_email'] = "Paypal Email";
$lang['currency'] = "moeda";
$lang['account_type'] = "Tipo de Conta";
$lang['logo_image'] = "logo imagem";
$lang['paypal'] = "Paypal";


//***Package Settings ***/
$lang['package_settings'] = "Configurações da Embalagem";
$lang['packages'] = "pacotes";
$lang['hours'] = "horas";
$lang['distance'] = "distância";
$lang['min_cost'] = "min Custo";
$lang['charge_distance'] = "carga Distância";
$lang['charge_hour'] = "cobrar ";
$lang['terms_conditions'] = "Terms & Conditions";
$lang['add_package'] = "Adicionar Package";
$lang['edit_package_setting'] = "Ambiente Editar Package";
$lang['in_hours'] = "Na hora(s)";
$lang['distance_in_km'] = "Distância em km";
$lang['charge_per_km'] = "Custo por km";
$lang['charge_per_hour'] = "Custo por hora";
$lang['package_details'] = "Detalhes do pacote";
$lang['package_extras'] = "Custos Adicionais";
$lang['load_more'] = "Coloque mais";
$lang['show_less'] = "mostrar menos";


// ***Waitings***//
$lang['waiting_time_settings'] = "Configurações de tempo de espera";
$lang['waiting_times'] = "tempos de espera";
$lang['add_waiting_time'] = "Adicionar Tempo de Espera";
$lang['edit_waiting'] = "Editar Waiting";


//***Reasons to book with us***//
$lang['reasons_to_book_with_us'] = "Razões para reservar connosco";
$lang['reasons_to_book'] = "Razões para reservar";
$lang['instructions'] = "instruções";


//***Social Network Settings***//
$lang['social_network_settings'] = "Configurações de Rede Social";
$lang['facebook'] = "Facebook";
$lang['twitter'] = "chilro";
$lang['linked_in'] = "Linked in";
$lang['google_plus'] = "Google Plus";
$lang['social_networks'] = "Redes Sociais";
$lang['url_order'] = "eg: https://your url";


//**SEO settings***//
$lang['seo_settings'] = "Configurações de SEO";
$lang['add_seo_setting'] = "	Adicionar SEO Ambiente";
$lang['edit_seo_setting'] = "Editar SEO Ambiente";
$lang['site_keywords'] = "site Palavras-chave";
$lang['google_analytics'] = "Google Analytics";
$lang['site_title'] = "Título do site";
$lang['site_description'] = "Descrição do Site";


//**Dynamic Pages**//
$lang['pages'] = "páginas";
$lang['meta_tag'] = "Tag meta";
$lang['meta_keywords'] = "meta Keywords";
$lang['seo_keywords'] = "expressões SEO";
$lang['is_bottom'] = "é inferior";
$lang['sort_order'] = "Ordem de classificação";
$lang['parent_id'] = "ID Parent";
$lang['sort_order'] = "Ordem de classificação";
$lang['bottom'] = "fundo";
$lang['under_category'] = "Em Categoria";
$lang['add_page'] = "Adicionar Página";
$lang['meta_tag_keywords'] = "Meta Tag Palavras-chave";
$lang['edit_page'] = "Editar Página";

//homepage//
//**header**//
$lang['welcome_to_DVBS'] = "Bem-vindo ao DVBS";
$lang['create_account'] = "criar uma conta";
$lang['lang'] = "Lang";
$lang['digi_advanced_cab_booking_system'] = "Digi avançada Cab Sistema de Reserva";

//**navigation**//
$lang['toggle_navigation'] = "Alternar Navigation";
$lang['online_booking'] = "Reservas Online";
$lang['FAQs'] = "FAQ's";
$lang['my_account'] = "minha Conta";
$lang['my_profile'] = "Meu Perfil";
$lang['my_bookings'] = "As minhas reservas";

//home page index
$lang['find_your_perfect_trip'] = "Encontre sua viagem perfeita";
$lang['pick_up_location'] = "Pick-Up Localização";
$lang['drop_of_location'] = "Drop-de Localização";
$lang['pick_up_date'] = "Pick-Up Date";
$lang['pick_up_time'] = "Pick-Up Time";
$lang['cars'] = "CARS";
$lang['starts_from'] = "começa a partir de";

//footer
$lang['careers'] = "Carreiras";
$lang['privacy_policy'] = "Política De Privacidade";
$lang['our_company'] = "nossa Empresa";
$lang['our_fleet'] = "nossa Frota";
$lang['news_letter'] = "news Letter";
$lang['we_never_send_spam'] = "We Never Send Spam";
$lang['digi_advanced_cab_booking_system_2015'] = "Digi avançada Cab Sistema de Reserva de 2015.";
$lang['all_rights_reserved'] = "Todos os direitos reservados.";
$lang['design_by_conquerors_technologies'] = "Design by conquistadores Technologies";
$lang['cards_we_accept'] = "Cartões Aceitos";


//create_account
$lang['register'] = "Cadastre-se";
$lang['user_email'] = "Usuário Email";
$lang['date_of_registration'] = "Data de Registro";
$lang['register_here'] = "Registe-se aqui";
$lang['saloon_cars'] = "Saloon Cars";
$lang['cities'] = "CITYS";
$lang['hyderabad'] = "Hyderabad";

//login
$lang['login_forgot_password'] = "Esqueceu sua senha?";

//online booking
$lang['online_booking'] = "Reservas Online";
$lang['journey_details'] = "Journey Detalhes";
$lang['passenger_details'] = "Detalhes do Passageiro";
$lang['payment_details'] = "Detalhes do pagamento";
$lang['enter_your_pickup_and_destination_details'] = "Digite seu captador e Detalhes do Destino";
$lang['date_time'] = "data&tempo";
$lang['click_and_select_the_date_and_time_you_would_like_to_be_picked_up'] = "Clique e Selecione a data e hora que você gostaria de apanhados";
$lang['select_date'] = "Selecionar Data";
$lang['select_time'] = "Select Tempo";
$lang['wait_and_return_journey_required'] = "Espere & Return Journey necessária";
$lang['total_journey_time'] = "Tempo total de viagem";
$lang['total_cost'] = "CUSTO TOTAL";
$lang['personal_details'] = "Detalhes pessoais";
$lang['confirmed_bookings'] = "As reservas confirmadas";
$lang['cancelled_bookings'] = "Reservas canceladas";
$lang['pending_bookings'] = "Enquanto se aguarda Reservas";
$lang['local_address'] = "Endereço local";

//passenger details
$lang['guest_check_out'] = "Guest Check Out";
$lang['passengers_details'] = "passageiro(s) Detalhes";
$lang['phone_number'] = "Número De Telefone";
$lang['information_to_driver'] = "Informações Para Motorista";
$lang['back_to_journey_details'] = "Back To Journey Detalhes";
$lang['next_payment_details'] = "Próxima Detalhes do pagamento";
$lang['booking_summary'] = "Resumo Booking";
$lang['booking_reference'] = "reserva de Referência";
$lang['journey_type'] = "Journey Tipo";
$lang['two_way'] = "Two Way";
$lang['two_way_fare'] = "Two Way Fare";
$lang['one_way'] = "Mão Única";
$lang['one_way_fare'] = "One Way Fare";
$lang['pick_up'] = "pegar";
$lang['drop_of'] = "Drop of";
$lang['journey_miles_and_time'] = "Journey Miles & Tempo";
$lang['car_type'] = "Tipo de carro";
$lang['booking_confirmation'] = "confirmação de reserva";
$lang['payment'] = "pagamento";
$lang['credit_card'] = "Cartão De Crédito";
$lang['cash'] = "dinheiro";
$lang['pay_with_cardsave'] = "Pague com Cardsave";
$lang['pay_with_paypal'] = "Pague com PayPal";
$lang['pay_with_cash'] = "Pagar com dinheiro";
$lang['back_to_your_details'] = "Voltar aos seus dados";
$lang['continue'] = "continuar";


/** Packages* */
$lang['sno'] = "Sno";
$lang['package'] = "pacote";
$lang['fare'] = "tarifa";
$lang['after_distance'] = "depois de distância";
$lang['per_km'] = "por Km";
$lang['after_time'] = "depois de um tempo";
$lang['per_hr'] = "por Hr";
$lang['book_now'] = "Book Now";

/** Package booking * */
$lang['package_booking'] = "Booking Package";
$lang['select_your_car'] = "Escolha o seu carro";
$lang['select_the_car_that_suits_to_your_requirement'] = "Escolha o carro que se adapte a sua exigência";
$lang['package_details_caps'] = "Detalhes do Pacote";
$lang['total_time'] = "Tempo Total";
$lang['cost_details'] = "DETALHES DE CUSTOS";
$lang['package_cost'] = "Custo Package";
$lang['extra_distance'] = "distância extra";
$lang['extra_time'] = "O tempo extra";

/** Contact * */
$lang['mobile'] = "móvel";
$lang['message'] = "mensagem";
$lang['email_sent_successfully_we_will_contact_you_as_soon_as_possible'] = "E-mail enviado com sucesso ... Entraremos em contato o mais breve possível.";
$lang['unable_to_send_email'] = "Não é possível enviar e-mail.";

/** My account * */
$lang['mobile_number'] = "Número De Telemóvel";


/* * change password * */
$lang['old_password'] = "Senha Antiga";
$lang['password_changed_success'] = "Senha alterada com sucesso.";

/** myBookings * */
$lang['one_way_journey_details'] = "One Way Journey Detalhes";
$lang['status_canceled'] = "Estado Cancelado";
$lang['unable_to_cancel_status'] = "Possível cancelar Estado";


/* * logout* */
$lang['success_logout'] = "Conectado com sucesso";




















//PREVIOUS//
/* * * User ** */
$lang['user'] = "usuário";
$lang['new_user'] = "novo Usuário";
$lang['user_id'] = "Id Do Usuário";
$lang['user_create_successful'] = "Usuário criado com sucesso";
$lang['user_update_successful'] = "Usuário atualizado com sucesso";


/* * * Admin ** */
$lang['admin'] = "Administrador";


//
$lang['new_executive'] = "New Executive";


///


$lang['new_customer'] = "novo cliente";
$lang['customer_id'] = "Id cliente";


///
$lang['new_vehicle'] = "New Vehicle";
$lang['vehicle_id'] = "identificação do veículo";
$lang['luggage'] = "bagagem";
$lang['fuel'] = "combustível";
$lang['petrol'] = "gasolina";
$lang['diesel'] = "diesel";
$lang['gas'] = "gás";



////////
$lang['booking_id'] = "Booking Id";
$lang['pick_place'] = "Local de Pick-up";
$lang['allotted_driver_id'] = "Motorista atribuído Id";


//***waiting times
$lang['from_time'] = "From Time";
$lang['to_time'] = "para Tempo";


/* * * Booking System Settings ** */
$lang['distance_type'] = "Distância Tipo";
$lang['max_advance_booking_days'] = "Max. Reserva antecipada Dias";
$lang['system'] = "sistema";


/* * * Cost Type Values ** */
$lang['cost_type_values'] = "Tipo de custo benefício";
$lang['day_flat_rate'] = "Dia da tarifa única";
$lang['night_flat_rate'] = "Noite da tarifa única";
$lang['day_from_value'] = "Dia Do Valor";
$lang['day_to_value'] = "Day To Valor";
$lang['night_from_value'] = "Noite Do Valor";
$lang['night_to_value'] = "Night To Valor";


/* * * Airport Locations ** */
$lang['airport_location'] = "aeroporto Localização";
$lang['airport_locations'] = "Locais Aeroporto";


/* * * Airport Cars ** */
$lang['airport_car'] = "aeroporto de carro";
$lang['airport_cars'] = "Carros Aeroporto";


/* * * Payments ** */
$lang['payments'] = "pagamentos";
$lang['payment_amount'] = "Valor do Pagamento";
$lang['transaction_id'] = "ID da transação";
$lang['transaction_status'] = "status da Transação";
$lang['booking_successful'] = "Reservas concluída com êxito";
$lang['booking_thanx'] = "Obrigado por reserva conosco.";
$lang['cancel_booking'] = "Se você deseja cancelar a reserva após a confirmação, você terá de informar-nos por nos chamar em 000000000 ou e-mail para enquiries@dvbs.com";
$lang['waiting_cost'] = "Custo de espera";


/* * * Drivers ** */
$lang['driver'] = "motorista";
$lang['drivers'] = "Drivers";
$lang['driver_id'] = "motorista Id";
$lang['licence_no'] = "Licence No.";


/* * * Language Settings ** */
$lang['language_settings'] = "Definições de idioma";
$lang['language'] = "língua";
$lang['language_code'] = "Código idioma";
$lang['language_name'] = "Nome do Idioma";


/* * * Days & Months ** */
$lang['monday'] = "segunda-feira";
$lang['tuesday'] = "terça-feira";
$lang['wednesday'] = "quarta-feira";
$lang['thursday'] = "quinta-feira";
$lang['friday'] = "sexta-feira";
$lang['saturday'] = "sábado";
$lang['sunday'] = "domingo";
$lang['january'] = "janeiro";
$lang['february'] = "fevereiro";
$lang['march'] = "março";
$lang['april'] = "abril";
$lang['may'] = "maio";
$lang['june'] = "Junho";
$lang['july'] = "Julho";
$lang['august'] = "Agosto";
$lang['september'] = "setembro";
$lang['october'] = "outubro";
$lang['november'] = "novembro";
$lang['december'] = "dezembro";


//CodeIgniter
// Errors
$lang['error_csrf'] = 'Este formulário de postagem não passou no teste de segurança.';

// Login
$lang['login_heading'] = 'Conecte-Se';
$lang['login_subheading'] = 'Por favor, faça o login com seu e-mail / nome de usuário e senha abaixo.';
$lang['login_identity_label'] = 'Email / Nome de Usuário:';
$lang['login_password_label'] = 'senha:';
$lang['login_remember_label'] = 'Lembre De Mim:';
$lang['login_submit_btn'] = 'Conecte-Se';



// Index
$lang['index_heading'] = 'usuários';
$lang['index_subheading'] = 'Abaixo está uma lista de usuários.';
$lang['index_fname_th'] = 'primeiro Nome';
$lang['index_lname_th'] = 'Sobrenome';
$lang['index_email_th'] = 'Email';
$lang['index_groups_th'] = 'grupos';
$lang['index_status_th'] = 'estado';
$lang['index_action_th'] = 'ação';
$lang['index_active_link'] = 'ativo';
$lang['index_inactive_link'] = 'inativo';
$lang['index_create_user_link'] = 'Criar um novo usuário';
$lang['index_create_group_link'] = 'Criar um novo grupo';

// Deactivate User
$lang['deactivate_heading'] = 'Desativar Usuário';
$lang['deactivate_subheading'] = "Tem certeza de que deseja desativar o usuário '% s '";
$lang['deactivate_confirm_y_label'] = 'sim:';
$lang['deactivate_confirm_n_label'] = 'não:';
$lang['deactivate_submit_btn'] = 'submeter';
$lang['deactivate_validation_confirm_label'] = 'confirmação';
$lang['deactivate_validation_user_id_label'] = 'ID do usuário';

// Create User
$lang['create_user_heading'] = 'Criar Usuário';
$lang['create_user_subheading'] = 'Por favor, insira o usuário informações \'s abaixo.';
$lang['create_user_fname_label'] = 'primeiro Nome';
$lang['create_user_lname_label'] = 'Sobrenome';
$lang['create_gender_label'] = 'gênero  : ';
$lang['create_dob_label'] = 'Data De Nascimento  ';
$lang['create_user_desired_location_label'] = 'Localização pretendido';
$lang['create_user_desired_job_type_label'] = 'Pretendido Tipo de trabalho';
$lang['create_user_open_for_contract_label'] = 'Open For Contrato';
$lang['create_user_pay_rate_label'] = 'Pay Taxa';
$lang['create_current_salary_label'] = 'Salário atual';
$lang['create_city_label'] = 'cidade';
$lang['create_state_label'] = 'estado';
$lang['create_country_label'] = 'país';
$lang['create_fax_label'] = 'fax';
$lang['create_industry_label'] = 'indústria';

$lang['create_Zipcode_label'] = 'Cep';
$lang['create_willing_relocate_label'] = 'Disposto a mudar: ';
$lang['create_user_company_label'] = 'Nome Da Empresa:';
$lang['create_user_email_label'] = 'Email';
$lang['create_user_primary_email_label'] = 'Primary Email';
$lang['create_user_secondary_email_label'] = 'secundária Email';
$lang['create_user_phone_label'] = 'telefone';

$lang['create_user_primary_phone_label'] = 'Telefone Principal';
$lang['create_user_secondary_phone_label'] = 'Telefone Secundário';
$lang['create_user_password_label'] = 'senha:';
$lang['create_user_password_confirm_label'] = 'confirme Sua Senha:';
$lang['create_user_submit_btn'] = 'Criar Usuário';
$lang['create_user_validation_fname_label'] = 'primeiro Nome';
$lang['create_user_validation_lname_label'] = 'Sobrenome';
$lang['create_user_validation_email_label'] = 'Endereço De Email';
$lang['create_user_validation_phone1_label'] = 'Primeira parte de Telefone';
$lang['create_user_validation_phone2_label'] = 'Segunda parte de Telefone';
$lang['create_user_validation_phone3_label'] = 'Terceira Parte do Telefone';
$lang['create_user_validation_company_label'] = 'Nome Da Empresa';
$lang['create_user_validation_password_label'] = 'senha';
$lang['create_user_validation_password_confirm_label'] = 'confirmação de Senha';

// Edit User
$lang['edit_user_heading'] = 'Editar Usuário';
$lang['edit_user_subheading'] = 'Por favor, insira o usuário informações \'s abaixo.';
$lang['edit_user_fname_label'] = 'primeiro Nome:';
$lang['edit_user_lname_label'] = 'Sobrenome:';
$lang['edit_user_company_label'] = 'Nome Da Empresa:';
$lang['edit_user_email_label'] = 'Email:';
$lang['edit_user_phone_label'] = 'telefone:';
$lang['edit_user_password_label'] = 'senha: (se alterar a senha)';
$lang['edit_user_password_confirm_label'] = 'confirme Sua Senha: (se alterar a senha)';
$lang['edit_user_groups_heading'] = 'Membro de grupos';
$lang['edit_user_submit_btn'] = 'Salvar Usuário';
$lang['edit_user_validation_fname_label'] = 'primeiro Nome';
$lang['edit_user_validation_lname_label'] = 'Sobrenome';
$lang['edit_user_validation_email_label'] = 'Endereço De Email';
$lang['edit_user_validation_phone1_label'] = 'Primeira parte de Telefone';
$lang['edit_user_validation_phone2_label'] = 'Segunda parte de Telefone';
$lang['edit_user_validation_phone3_label'] = 'Terceira Parte do Telefone';
$lang['edit_user_validation_company_label'] = 'Nome Da Empresa';
$lang['edit_user_validation_groups_label'] = 'grupos';
$lang['edit_user_validation_password_label'] = 'senha';
$lang['edit_user_validation_password_confirm_label'] = 'confirmação de Senha';

// Create Group
$lang['create_group_title'] = 'Criar Grupo';
$lang['create_group_heading'] = 'Criar Grupo';
$lang['create_group_subheading'] = 'Digite as informações do grupo abaixo.';
$lang['create_group_name_label'] = 'Nome do grupo:';
$lang['create_group_desc_label'] = 'descrição:';
$lang['create_group_submit_btn'] = 'Criar Grupo';
$lang['create_group_validation_name_label'] = 'Nome do grupo';
$lang['create_group_validation_desc_label'] = 'descrição';

// Edit Group
$lang['edit_group_title'] = 'Editar Grupo';
$lang['edit_group_saved'] = 'grupo Saved';
$lang['edit_group_heading'] = 'Editar Grupo';
$lang['edit_group_subheading'] = 'Digite as informações do grupo abaixo.';
$lang['edit_group_name_label'] = 'Nome do grupo:';
$lang['edit_group_desc_label'] = 'descrição:';
$lang['edit_group_submit_btn'] = 'Salvar grupo';
$lang['edit_group_validation_name_label'] = 'Nome do grupo';
$lang['edit_group_validation_desc_label'] = 'descrição';

// Change Password
$lang['change_password_heading'] = 'Alterar A Senha';
$lang['change_password_old_password_label'] = 'Senha Antiga:';
$lang['change_password_new_password_label'] = 'nova Senha (caracteres, pelo menos,%s longa):';
$lang['change_password_new_password_confirm_label'] = 'Confirmar nova senha:';
$lang['change_password_submit_btn'] = 'mudança';
$lang['change_password_validation_old_password_label'] = 'Senha Antiga';
$lang['change_password_validation_new_password_label'] = 'nova Senha';
$lang['change_password_validation_new_password_confirm_label'] = 'Confirmar nova senha';

// Forgot Password
$lang['forgot_password_heading'] = 'Esqueceu Sua Senha';
$lang['forgot_password_subheading'] = 'Digite seu% s para que possamos enviar-lhe um e-mail para redefinir sua senha.';
$lang['forgot_password_email_label'] = '%s:';
$lang['forgot_password_submit_btn'] = 'submeter';
$lang['forgot_password_validation_email_label'] = 'Endereço De Email';
$lang['forgot_password_username_identity_label'] = 'Nome de Usuário';
$lang['forgot_password_email_identity_label'] = 'Email';
$lang['forgot_password_email_not_found'] = 'Não há registro de que o endereço de e-mail.';

// Reset Password
$lang['reset_password_heading'] = 'Alterar A Senha';
$lang['reset_password_new_password_label'] = 'nova Senha (caracteres, pelo menos,% s longa):';
$lang['reset_password_new_password_confirm_label'] = 'Confirmar nova senha:';
$lang['reset_password_submit_btn'] = 'mudança';
$lang['reset_password_validation_new_password_label'] = 'nova Senha';
$lang['reset_password_validation_new_password_confirm_label'] = 'Confirmar nova senha';


//New Kalyan start

$lang['failed'] = 'fracassado';



//New Kalyan end
//New Raghu Start
$lang['in_kms'] = 'em KMs';

//New Raghu End
// start
$lang['currency_code_alpha'] = "Moeda Código Alpha";
$lang['currency_name'] = "Nome da Moeda";
$lang['user file'] = "Arquivo de usuário";
$lang['user name'] = "Nome de Usuário";
$lang['account_deactivated'] = "conta Desactivado";
$lang['Day'] = "dia";
$lang['url'] = "URL";
$lang['symbol'] = "símbolo";
$lang['start'] = "começo";
$lang['end'] = "fim";
$lang['Night'] = "noite";





//

$lang['added'] = "adicionado";
$lang['time_from'] = "tempo de (mins)";
$lang['time_to'] = "Time To (mins)";
$lang['email_received'] = "E-mail recebido, nós entraremos em contato o mais breve possível.";
$lang['select_waiting_time'] = "Selecione Tempo de Espera";



$lang['newsletter'] = 'Boletim de Notícias';
$lang['send_time'] = 'Hora de envio';
$lang['user_type'] = 'Tipo de usuário';
$lang['user_status'] = 'Status do usuário';
$lang['subject'] = 'Sujeita';
$lang['send_date'] = 'Data de envio';
$lang['old'] = 'Velha';


$lang['filemanagement'] = 'Files';
$lang['nature'] = 'Nature';
$lang['priority'] = 'Priority';
$lang['alert'] = 'Alert';
$lang['delay_date'] = 'Delay';
$lang['selection'] = 'Selection';
$lang['name_selection'] = 'Name';
$lang['note'] = 'Note';
$lang['file'] = 'File';


$lang['task'] = 'Tasks';
$lang['date'] = 'Date';
$lang['date_hour'] = '';
$lang['date_minute'] = '';
$lang['added_by_firstname'] = 'Added By First Name';
$lang['added_by_lastname'] = 'Added By Last Name';
$lang['affected_department'] = 'Department';
$lang['affected_user'] = 'Users';
//$lang['priority'] = 'File';
$lang['date2'] = 'Date';
$lang['date2_hour'] = '';
$lang['date2_minute'] = '';
$lang['files'] = 'Files';
$lang['note'] = 'Note';
$lang['note2'] = 'Note';
$lang['task_time'] = 'Time';
$lang['added_by'] = 'Added by';
$lang['from_department'] = 'From department';
$lang['affected_to'] = 'Affected to';
$lang['delay_date'] = 'Delay date';
$lang['delay_time'] = 'Delay time';
$lang['to_be_done_before'] = 'To be done before';
$lang['add_files'] = 'Add files';
$lang['task_description'] = 'Task Description';


//

$lang['configurations'] = 'Configurações';
$lang['module'] = 'Módulo';
$lang['message_sentence'] = 'Primeira Mensagem Sentença';
$lang['sender_name'] = 'Sender Name';

$lang['smtp'] = 'Configurações SMTP';
$lang['popups'] = 'Configurações de pop-ups';
$lang['select_all'] = 'All';
$lang['add_note'] = 'Add note';
$lang['quick_replies'] = 'Respostas rápidas';
$lang['callback'] = 'Configurações de retorno de chamada';
$lang['company_profile'] = 'Perfil da companhia';
$lang['departments'] = 'Departamentos';
$lang['supplier'] = 'Fornecedor';
$lang['smtp_text'] = 'SMTP';
$lang['job_applications'] = 'Candidaturas a emprego';
$lang['quote_requests'] = 'Solicitações de cotação';
$lang['CMS'] = 'CMS';
$lang['CRM'] = 'CRM';
$lang['accounting'] = 'Contabilidade';
$lang['sent_by'] = 'Enviado por';
$lang['attachment'] = 'Anexo';
$lang['send_copy'] = 'Enviar cópia';
$lang['reminders'] = "Lembretes";
$lang['reminders1'] = "Lembretes de driver";
$lang['view'] = "Visão";
$lang['documents'] = "Documentos";
$lang['quote_id'] = "ID da cotação";
$lang['sales_operator'] = "Operador de Vendas";