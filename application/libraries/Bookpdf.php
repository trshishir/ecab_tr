<?php
require_once(dirname(__FILE__).'/tcpdf/tcpdf.php');

class Bookpdf extends TCPDF {

   protected $info;

    public function setInfo($info){
        $this->info = $info;
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-13);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $addLine="<hr>";
         $this->writeHTML($addLine, $ln=false, $fill=false, $reseth=false, $cell=false, $align='');
        $pagenumber='Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages();
        $this->setCellPaddings( $left = 0, $top = 0, $right =0, $bottom = 0);
         $this->setCellMargins($left = 0, $top = 1, $right = 0, $bottom = 0);
       // $this->cell(202,6,$this->info,1,1,'C');
        //$this->cell(202,6,trim($pagenumber),1,1,'R');
        $this->writeHTML($this->info, $ln=true, $fill=false, $reseth=true, $cell=false, $align='C');
         $this->setCellPaddings( $left = 0, $top = 0, $right =0, $bottom = 0);
         $this->setCellMargins($left = 0, $top = 1, $right = 0, $bottom = 0);
        $this->cell(202,0,trim($pagenumber),0,1,'R');
         //$this->writeHTML(strip_tags($pagenumber), $ln=true, $fill=false, $reseth=true, $cell=false, $align='R');
        
    }
}