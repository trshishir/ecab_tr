<?php


class Mail_bookingsender
{
    private $config = [];
    private $operators;

    public function __construct()
    {
        $this->config = get_email_configuration();
        $this->operators = get_operator_emails();
    }

    public function __get($var){
        return get_instance()->$var;
    }

    public function sendMail($data){
        
       
        require_once(APPPATH."third_party/phpmailer/Exception.php");
        require_once(APPPATH."third_party/phpmailer/PHPMailer.php");
        require_once(APPPATH."third_party/phpmailer/SMTP.php");

        $mail = new \PHPMailer\PHPMailer\PHPMailer(true);

        try {
            $mail->isSMTP();
            $mail->Host       = $this->config['SMTP_HOST'];
            $mail->SMTPAuth   = true;
            $mail->CharSet    = 'UTF-8';
            $mail->Username   = $this->config['SMTP_USERNAME'];
            $mail->Password   = $this->config["SMTP_PASSWORD"];
            $mail->SMTPSecure = $this->config["SMTP_TYPE"];
            $mail->Port       = $this->config['SMTP_PORT'];
            //Recipients
        
            $mail->setFrom($this->config['SMTP_USERNAME'], APP_NAME);
            //send email to client
            $mail->addAddress($data['email']);
            //send email to operator
             foreach ($this->operators as $item) {
                $mail->addAddress($item->email);
             }

            $mail->addReplyTo($this->config['SMTP_USERNAME'], APP_NAME);
            //Content
            if($data['firstpdffile']){
                $mail->addStringAttachment($data['firstpdffile'],$data['firstpdffilename']);
            }
          
            if($data['secondpdffile']){
                $mail->addStringAttachment($data['secondpdffile'],$data['secondpdffilename']);
            }
           
            $mail->isHTML(true);
            $mail->Subject = $data['subject'];
            $mail->Body    = $data['message'];
            

            return $mail->send();
        } catch (\PHPMailer\PHPMailer\Exception $e) {
           return false;
        }
    }
}