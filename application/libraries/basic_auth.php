<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Basic_auth {

    public function __construct() {

        $this->load->library('email');
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('user_model');
    }

    public function __get($var) {

        return get_instance()->$var;
    }

    public function generateRememberMeToken($user) {
        return md5($user->password . time());
    }

    public function setRememberMeToken($user) {

        $token = $this->generateRememberMeToken($user);
        $this->user_model->update(['remember_code' => $token], $user->id);
        $cookie = array(
            'name' => 'remember_me_token',
            'value' => $token,
            'expire' => '1209600', // Two weeks
            'path' => '/'
        );

        set_cookie($cookie);
    }

    public function login($username, $password, $remember) {
        $user = $this->user_model->login($username, $password, "email");

        if ($user != false && $user->active == 1) {
            $this->session->set_userdata("user", $user);

            $user = $this->ion_auth->user()->row();
            $usergroup = $this->ion_auth->get_users_groups($user->id)->row();

            $user->group = $usergroup;
            $this->session->set_userdata("user_id", $user->id);
            $this->session->set_userdata("user", $user);
            $this->session->set_userdata("usergroup", $usergroup);

            if ($remember != false) {
                $this->setRememberMeToken($user);
            }
            // sleep(1);
            return $user;
        }

        return false;
    }
    
    public function admin_login($username, $password, $remember) {
        $user = $this->user_model->admin_login($username, $password, "email");
         
        if (!empty($user) && $user->active == 1) {
            // $user = $this->ion_auth->user()->row();
            // echo "<PRE>";print_r($user); echo "</PRE>";exit;
            // $usergroup = $this->ion_auth->get_users_groups($user->id)->row();
            // $user->group = $usergroup;
            
            $this->session->set_userdata("user_id", $user->id);
            $this->session->set_userdata("user", $user);
            $this->session->set_userdata("usergroup", $user->group_name);

            if ($remember != false) {
                $this->setRememberMeToken($user);
            }
            // sleep(1);
            return $user;
        }

        return false;
    }
    
    public function client_login($username, $password, $remember) {
        $user = $this->user_model->client_login($username, $password, "email");
        // echo "<PRE>";print_r($user); echo "</PRE>";exit;
        if (!empty($user) && $user->active == 1) {
            // $user = $this->ion_auth->user()->row();
            // echo "<PRE>";print_r($user); echo "</PRE>";exit;
            // $usergroup = $this->ion_auth->get_users_groups($user->id)->row();
            // $user->group = $usergroup;
            
            $this->session->set_userdata("user_id", $user->id);
            $this->session->set_userdata("user", $user);
            $this->session->set_userdata("usergroup", $user->group_name);

            if ($remember != false) {
                $this->setRememberMeToken($user);
            }
            // sleep(1);
            return $user;
        }

        return false;
    }
    public function client_login_checks($username, $password, $remember) {

        $users = $this->user_model->client_login_check($username, $password, "email");
     
        // echo "<PRE>";print_r($user); echo "</PRE>";exit;
        if (!empty($users) && $users->active == 1) {
            // $user = $this->ion_auth->user()->row();
            // echo "<PRE>";print_r($user); echo "</PRE>";exit;
            // $usergroup = $this->ion_auth->get_users_groups($user->id)->row();
            // $user->group = $usergroup;
           
            $this->session->set_userdata("user_id", $users->id);
            $this->session->set_userdata("user", $users);
            $this->session->set_userdata("usergroup", $users->group_name);

            if ($remember != false) {
                $this->setRememberMeToken($users);
            }
            // sleep(1);
            
            return $users;
            
        }

        return false;
    }
    public function partner_login($username, $password, $remember) {
        $user = $this->user_model->partner_login($username, $password, "email");
        // echo "<PRE>";print_r($user); echo "</PRE>";exit;
        if (!empty($user) && $user->active == 1) {
            // $user = $this->ion_auth->user()->row();
            // $usergroup = $this->ion_auth->get_users_groups($user->id)->row();
            // $user->group = $usergroup;
            
            $this->session->set_userdata("user_id", $user->id);
            $this->session->set_userdata("user", $user);
            $this->session->set_userdata("usergroup", $user->group_name);

            if ($remember != false) {
                $this->setRememberMeToken($user);
            }
            // sleep(1);
            return $user;
        }

        return false;
    }
    
    public function driver_login($username, $password, $remember) {
        $user = $this->user_model->driver_login($username, $password, "email");
        // echo "<PRE>";print_r($user); echo "</PRE>";exit;
        if (!empty($user) && $user->active == 1) {

            // $user = $this->ion_auth->user()->row();
            // $usergroup = $this->ion_auth->get_users_groups($user->id)->row();
            // $user->group = $usergroup;
            $this->session->set_userdata("user_id", $user->id);
            $this->session->set_userdata("user", $user);
            $this->session->set_userdata("usergroup", $user->group_name);

            if ($remember != false) {
                $this->setRememberMeToken($user);
            }
            // sleep(1);
            return $user;
        }

        return false;
    }
    
    public function jobseeker_login($username, $password, $remember) {
        $user = $this->user_model->jobseeker_login($username, $password, "email");

        if (!empty($user) && $user->active == 1) {
            // $user = $this->ion_auth->user()->row();
            // $usergroup = $this->ion_auth->get_users_groups($user->id)->row();
            // $user->group = $usergroup;
            
            $this->session->set_userdata("user_id", $user->id);
            $this->session->set_userdata("user", $user);
            $this->session->set_userdata("usergroup", $user->group_name);

            if ($remember != false) {
                $this->setRememberMeToken($user);
            }
            // sleep(1);
            return $user;
        }

        return false;
    }
    
    public function affiliate_login($username, $password, $remember) {
        $user = $this->user_model->affiliate_login($username, $password, "email");
        // echo "<PRE>";print_r($user); echo "</PRE>";exit;
        if (!empty($user) && $user->active == 1) {
            // $user = $this->ion_auth->user()->row();
            // $usergroup = $this->ion_auth->get_users_groups($user->id)->row();
            // $user->group = $usergroup;
            
            $this->session->set_userdata("user_id", $user->id);
            $this->session->set_userdata("user", $user);
            $this->session->set_userdata("usergroup", $user->group_name);

            if ($remember != false) {
                $this->setRememberMeToken($user);
            }
            // sleep(1);
            return $user;
        }

        return false;
    }

    public function is_login() {
        $check = $this->session->userdata("user") !== false;
        
        if ($check == false && isset($_COOKIE['remember_me_token']) && !empty($_COOKIE['remember_me_token'])) {
            $user = $this->user_model->get(['remember_code' => $_COOKIE['remember_me_token']]);
            if ($user != false && $user->active == 1) {
                $this->session->set_userdata("user", $user);
                $this->setRememberMeToken($user);
                $check = true;
            }
        }
        return $check;
    }
    
    /**
     * is_client
     *
     * @return bool
     * @author Saravanan R
     * */
    public function is_client() {
        $check = $this->session->userdata("user") !== false;
        $status = false;
        $user = $this->session->userdata("user");
        if ($check) {
            $status = $this->user_model->isClient($user->id);
        } 
        return $status;
    }
    
    /**
     * is_driver
     *
     * @return bool
     * @author Saravanan R
     * */
    public function is_driver() {
        $check = $this->session->userdata("user") !== false;
        $status = false;
        $user = $this->session->userdata("user");
        if ($check) {
            $status = $this->user_model->isDriver($user->id);
        } 
        return $status;
    }
    
    /**
     * is_partner
     *
     * @return bool
     * @author Saravanan R
     * */
    public function is_partner() {
        $check = $this->session->userdata("user") !== false;
        $status = false;
        $user = $this->session->userdata("user");
        if ($check) {
            $status = $this->user_model->isPartner($user->id);
        } 
        return $status;
    }
    
    /**
     * is_jobseeker
     *
     * @return bool
     * @author Saravanan R
     * */
    public function is_jobseeker() {
        $check = $this->session->userdata("user") !== false;
        $status = false;
        $user = $this->session->userdata("user");
        if ($check) {
            $status = $this->user_model->isJobseeker($user->id);
        } 
        return $status;
    }
    
    /**
     * is_affiliate
     *
     * @return bool
     * @author Saravanan R
     * */
    public function is_affiliate() {
        $check = $this->session->userdata("user") !== false;
        $status = false;
        $user = $this->session->userdata("user");
        if ($check) {
            $status = $this->user_model->isAffiliate($user->id);
        } 
        return $status;
    }
    
    public function user() {
        return $this->session->userdata("user");
    }

    public function logout() {
        if ($this->is_login()) {
            $user = $this->session->userdata("user");
            $this->user_model->update(['remember_code' => ''], $user->id);
            $this->session->unset_userdata("usergroup");
            $this->session->unset_userdata("user");
            $this->session->userdata = array();
            return true;
        }

        return true;
    }

    public function resetPasswordKey($user, $counter = 3) {

        $ifExist = $this->user_model->getResetPassword(['user_id' => $user->id]);
        if ($ifExist != false) {
            return $ifExist->verification_code;
        } else {
            $key = md5($user->id . "-" . time());
            $insert = [
                'user_id' => $user->id,
                'verification_code' => $key,
                'counter' => $counter
            ];
            $this->user_model->addResetPassword($insert);
            return $key;
        }
    }

    public function verifyAccount($key) {
        return $this->user_model->verifyAccount($key);
    }

    public function messages() {
        $_output = '';
        foreach ($this->messages as $message) {
            $messageLang = $this->lang->line($message) ? $this->lang->line($message) : '##' . $message . '##';
            $_output .= $this->message_start_delimiter . $messageLang . $this->message_end_delimiter;
        }
        return $_output;
    }

}
