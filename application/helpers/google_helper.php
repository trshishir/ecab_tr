<?php


function googlePermission($permission){
    $defaultPermission = [
        'front'     => ['live' => 1, 'position' => 1, 'status' => 1],
        'area'      => ['live' => 1, 'position' => 1, 'status' => 1],
        'profile'   => ['live' => 1, 'position' => 1, 'status' => 1],
        'map_view'  => ['live' => 1, 'position' => 1, 'status' => 1],
        'booking'   => ['live' => 1, 'position' => 1, 'status' => 1],
    ];

    if(!empty($permission))
        $permission = json_decode(is_string($permission) ? $permission : json_encode($permission), true);
    else $permission = [];

    $arr = [];
    foreach ($defaultPermission as $key => $permit){
        $arr[$key]['status'] = isset($permission[$key]['status']) && in_array($permission[$key]['status'], [0,1]) ?
            $permission[$key]['status'] : $permit['status'];
        $arr[$key]['position'] = isset($permission[$key]['position']) && in_array($permission[$key]['position'], [0,1]) ?
            $permission[$key]['position'] : $permit['status'];
        $arr[$key]['live'] = isset($permission[$key]['live']) && in_array($permission[$key]['live'], [0,1]) ?
            $permission[$key]['live'] : $permit['live'];
    }

    return json_decode(json_encode($arr));
}


function get_google_api(){
    $CI =& get_instance();
    $CI->load->model('google_api_model');
    $googleApi = $CI->google_api_model->getFirst();
    $googleApi->permission = googlePermission(@$googleApi->permission);
    //var_dump($googleApi);exit();
    return $googleApi;
}