<?php
function get_jobs_category($id)
{
    $CI =& get_instance();
    $CI->load->model('sitemodel');
    $getJob_fornt = $CI->sitemodel->getJob_fornt($id);
    return $getJob_fornt;
}

/**
 * @param $image_path
 * @return bool|mixed
 */
function get_image_mime_type($image_path)
{
    $mimes  = array(
        IMAGETYPE_GIF => "image/gif",
        IMAGETYPE_JPEG => "image/jpg",
        IMAGETYPE_PNG => "image/png",
        IMAGETYPE_SWF => "image/swf",
        IMAGETYPE_PSD => "image/psd",
        IMAGETYPE_BMP => "image/bmp",
        IMAGETYPE_TIFF_II => "image/tiff",
        IMAGETYPE_TIFF_MM => "image/tiff",
        IMAGETYPE_JPC => "image/jpc",
        IMAGETYPE_JP2 => "image/jp2",
        IMAGETYPE_JPX => "image/jpx",
        IMAGETYPE_JB2 => "image/jb2",
        IMAGETYPE_SWC => "image/swc",
        IMAGETYPE_IFF => "image/iff",
        IMAGETYPE_WBMP => "image/wbmp",
        IMAGETYPE_XBM => "image/xbm",
        IMAGETYPE_ICO => "image/ico");

    if (($image_type = exif_imagetype($image_path))
        && (array_key_exists($image_type ,$mimes)))
    {
        return $mimes[$image_type];
    }
    else
    {
        return FALSE;
    }
}



function create_unique_slug($table, $field, $str, $id=0, $where=''){
    $slug = get_slug($str);
    $CI =& get_instance();
    $query = "SELECT $field as slug FROM $table WHERE  $field  LIKE '$slug%' $where";
    if($id > 0){
        $query .= " AND id <> '$id'";
    }

    $results = $CI->base_model->run_query($query);
    $slugs = [];
    if($results != false)
        foreach ($results as $row){
            $slugs[] = $row->slug;
        }
    $counter = 0;
    if(in_array($slug, $slugs)) {
        $original = $slug;
        while (true) {
            if (in_array($slug, $slugs)) {
                $counter++;
                $slug = $original . "-" . $counter;
            } else {
                break;
            }
        }
    }
    return $slug;
}

function random_string($length = 10) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}

function get_slug($str, $delimiter = '-'){
    return strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
}

if ( ! function_exists('get_phrase'))
{
    function get_phrase($phrase = '')
    {
        $CI	=&	get_instance();
        $CI->load->database();
        $current_language	=	$CI->session->userdata('current_language');

        if($current_language	==	'')
        {
            $current_language	=	'english';
            $CI->session->set_userdata('current_language' , $current_language);
        }


        $query	=	$CI->db->get_where('vbs_languages' , array('phrase' => $phrase));
        die(var_dump($CI->db->last_query()));
        $row   	=	$query->row();

        //return $row->$current_language;
        if(isset($row->$current_language) && $row->$current_language !="")
            return $row->$current_language;
        else
            return ucwords(str_replace('_',' ',$phrase));
    }
}
?>