<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('get_configuration'))
{
    function get_configuration()
    {
        $data = [];
        $CI =& get_instance();
        $CI->load->model('config_model');
        $configData = $CI->config_model->getAll();
        if($configData != false){
            foreach($configData as $item)
                $data[$item->key] = $item->value;
        }
        return $data;
    }
        //code by s_a
     function get_email_configuration()
    { 

        $data = [];
        $CI =& get_instance();
        $CI->load->model('config_model');
        $configData = $CI->config_model->getEmailConfiguration('vbs_email_settings');
        $data['SMTP_USERNAME']=$configData->smtp_user;
        $data['SMTP_PASSWORD']=$configData->smtp_password;
        $data['SMTP_HOST']=$configData->smtp_host;
        $data['SMTP_PORT']=$configData->smtp_port;
        $data['SMTP_TYPE']=$configData->smtp_type;
        
        return $data;
    }

    function get_operator_emails()
    { 
        $role=10;
        $department=4;
        $CI =& get_instance();
        $CI->load->model('config_model');
        $data = $CI->config_model->get_operator_emails('vbs_users',['role_id'=>$role,'department_id'=>$department]);
        return $data;
    }
    //code by s_a
}
 
   