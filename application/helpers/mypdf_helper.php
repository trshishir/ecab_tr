<?php
 /* PDF FOR QUOTE MODULE START*/
 function createstringquotepdf($booking_id){
        
        $CI = get_instance();
        $status="1";
        
        $CI->load->model('bookings_config_model');
        $CI->load->model('quotes_model');
        $CI->load->model('invoice_model');
        $CI->load->model('bookings_model');

    
       $dbquotesdata = $CI->bookings_config_model->getpricestatusdata('vbs_price_status');
 
       $dbbookingdata=$CI->bookings_model->getdbbookingrecord('vbs_bookings',array('id'=>$booking_id));
      
       $dbdiscountandfeedata=$CI->bookings_model->getdbbookingrecord('vbs_bookings_pricevalues',array('booking_id'=>$booking_id));

       $quotes=$CI->bookings_model->getdbbookingrecord('vbs_quotes',array('booking_id'=>$booking_id));
      
       $dbtermservices=$CI->bookings_model->getdbbookingrecord('cms_page_post',array('language'=>'en','status'=>$status));

       $companydata=$CI->bookings_model->getcompanystatus();

       $companycity=$CI->quotes_model->getsinglerecord('vbs_cities',['id'=>$companydata->city])->name;

       $client=$CI->bookings_model->getclientdatabyid($dbbookingdata->user_id);
       $clientcity=$CI->quotes_model->getsinglerecord('vbs_cities',['id'=>$client->city])->name;

       $car_name=$CI->bookings_model->getcarname($dbbookingdata->car_id);

       $CI->load->model('cms_model');

       $page_name = "Terms of Service";

       $page_content = $CI->cms_model->getCMSPage('legals',$page_name);

        //create pdf
      $CI->load->library('bookpdf');
      $pdf = new bookpdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

      $address2=(!empty($companydata->address2))?trim($companydata->address2).', ' :'';

      $companydatainfo=strtoupper($companydata->name).' '.strtoupper($companydata->type).', '.$companydata->address.', '.$address2.$companydata->zip_code.', '.$companycity.', Capital : '.$companydata->capital.', License N : '.$companydata->licence.', SIRET : '.$companydata->sirft.', VAT N<sup>o</sup> : '.$companydata->numero_tva;
      $pdf->setInfo($companydatainfo);
$fileName='Quotes '.create_timestampdmy_uid($quotes->date,$quotes->id).' '.$client->civility.' '.$client->first_name.' '.$client->last_name;
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle($fileName.".PDF");
$pdf->SetSubject($fileName.".PDF");
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setPrintHeader(false);
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

$pdf->addPage();
$pdf->AddFont('fontawesome', '');
$pdf->SetFont('fontawesome', '', 14, '', true);


// $pdf->writeHTMLCell("&#xf0a4;&#xf01d;&#xf03e;"); // three icons

$customimage=base_url().$dbquotesdata->customicon;
$customimage = '<img src="'.$customimage.'" />';


/* Company information */
$cominfo1="<span><b>".strtoupper($companydata->name).' '.strtoupper($companydata->type)."</b></span><br><span>".$companydata->address."</span><br>";
if(!empty($companydata->address2)){
  $cominfo1.="<span>".$companydata->address2."</span><br>";
}
$cominfo1.="<span>".$companydata->zip_code.' '.$companycity."</span><br><span>Phone&nbsp;&nbsp;&nbsp;&nbsp; : ".$companydata->phone."</span><br><span>Fax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ".$companydata->fax."</span><br><span>Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: ".$companydata->email."</span><br><span>Website&nbsp;&nbsp;: ".$companydata->website."</span>";
/* Company information */


/* Client information */
$clientinfo="<b>&nbsp;".$client->civility.' '.$client->first_name.' '.$client->last_name."</b><br>&nbsp;".$client->address."<br>&nbsp;";
if(!empty($client->address1)){
  $clientinfo.=$client->address1."<br>&nbsp;";
}
$clientinfo.=$client->zipcode.'  '.$clientcity;
/* Client information */

/* Quote information */
$date=$quotes->date;
$quoteDate="Date : ".from_unix_date($quotes->date);
$quoteReference="Reference : ".$quotes->reference;
$quoteAvlDate="Available till date : ".date('d/m/Y', strtotime($date. ' + '.$dbquotesdata->quotedelay.' days'));
/* Quote information */

/* Custom Logo */
$customimage=base_url().$dbquotesdata->customicon;
$customimage = '<img src="'.$customimage.'"  width="'.$dbquotesdata->customiconwidth.'" height="'.$dbquotesdata->customiconheight.'"/>';
/* Custom Logo */

/* Pending/ Approved Logo */
$pendingapprovedicon='';
if($quotes->status == 0){
$pendingapprovedicon=base_url().$dbquotesdata->statutpendingicon;
$pendingapprovedicon = '<img src="'.$pendingapprovedicon.'" width="'.$dbquotesdata->pendingiconwidth.'" height="'.$dbquotesdata->pendingiconheight.'" />';
}
elseif($quotes->status == 1){
  $pendingapprovedicon=base_url().$dbquotesdata->statutapprovedicon;
  $pendingapprovedicon = '<img src="'.$pendingapprovedicon.'" width="'.$dbquotesdata->approvediconwidth.'" height="'.$dbquotesdata->approvediconheight.'" />';
}
elseif($quotes->status == 2){
  $pendingapprovedicon=base_url().$dbquotesdata->statutdeniedicon;
  $pendingapprovedicon = '<img src="'.$pendingapprovedicon.'" width="'.$dbquotesdata->deniediconwidth.'" height="'.$dbquotesdata->deniediconheight.'" />';

}
elseif($quotes->status == 3){
  $pendingapprovedicon=base_url().$dbquotesdata->statutcancelledicon;
  $pendingapprovedicon = '<img src="'.$pendingapprovedicon.'" width="'.$dbquotesdata->cancellediconwidth.'" height="'.$dbquotesdata->cancellediconheight.'" />';

}
else{
$pendingapprovedicon=base_url().$dbquotesdata->statutpendingicon;
$pendingapprovedicon = '<img src="'.$pendingapprovedicon.'" width="'.$dbquotesdata->pendingiconwidth.'" height="'.$dbquotesdata->pendingiconheight.'" />';
}

/* Pending/ Approved Logo */

/* Company Logo */
$companylogo=base_url().'uploads/company/'.$companydata->logo;
$companylogo = '<img src="'.$companylogo.'"/>';
/* Company Logo */

/* Set Company/Client/Quote Information in Table */
$html='<table cellspacing="0" cellpadding="0"  >
<tr>
          <td style="width:33.3%;">'.$companylogo.'</td>
          <td style="width:33.3%;"></td>
          <td style="width:33.3%;"></td>
        </tr></table>';
$html .='<table cellspacing="0" cellpadding="0" >
        

        <tr>
           <td style="width:33.3%;">'.$cominfo1.'</td>
          <td style="width:33.3%;"></td>
          <td style="width:33.3%;"><div class="test" style="page-break-inside: avoid;"><table cellspacing="0" cellpadding="6" ><tr><td>'.$clientinfo.'</td></tr></table></div>


          <style>   
              div.test {
                
                  border:0.1em solid black;  

              }
          </style></td>

            
        </tr> 
         <tr>
          <td></td>
          <td>'.$customimage.'</td>
          <td>'.$pendingapprovedicon.'</td>      
        </tr> 
        <tr>
          <td>'.$quoteDate.'</td>
          <td>'.$quoteReference.'</td>
          <td style="text-align:right;">'.$quoteAvlDate.'</td>      
        </tr>        
     </table>
     <style>
        table{
             border-collapse:collapse;
             table-layout: auto;
             width: 100%;    
         }
      
         th{
            border:1px solid #000000;
            text-align:center;
            font-size:10px !important;
         }
        
         td{
              
               text-align:left;
          
         
         }

       
        
     </style>
';
/* Set Company/Client/Quote Information in Table */
$totalhtprice;
$fullbookinginformation;
$bookdetailarray=getbookingdetails($booking_id,$CI);
$totalhtprice=$bookdetailarray['totalhtprice'];
$fullbookinginformation=$bookdetailarray['fullbookinginformation'];

$pdf->setFont('FontAwesome','',20);
/* Set Booking information in table*/
$pdf->setCellMargins($left = 0, $top =0, $right = 0, $bottom = 0);
$pdf->setCellPaddings( $left = 0, $top = 2, $right = 0, $bottom = 2);
$suptext="<sup>o</sup>";
$qutnum="QUOTE N".$suptext." : ".create_timestampdmy_uid($quotes->date,$quotes->id);
$pdf->setFont('helveticaB','',13);
$pdf->Cell(30,10,'',0,0,'C');
$pdf->writeHTMLCell(132,'','','',$qutnum,1,0,0,true,'C',true);
$pdf->Cell(30,10,'',0,1,'C');
$pdf->Ln(4);

$pdf->setCellPaddings( $left = 0, $top = 0, $right = 0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 3);
$pdf->setFont('helvetica','',11);
$pdf->writeHTMLCell(190,0,10,'',$html,0,1);
//$pdf->writeHTML($html, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');


$pdf->setFont('helvetica','',9);
$pdf->setCellPaddings( $left = 0, $top = 0, $right = 0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 3);
$pdf->writeHTMLCell(190,0,10,'',$fullbookinginformation,0,1);

// ---------------------------------------------------------
$totaldiscountfee=($dbdiscountandfeedata->welcomediscountfee + $dbdiscountandfeedata->rewarddiscountfee + $dbdiscountandfeedata->perioddiscountfee + $dbdiscountandfeedata->promocodediscountfee);
$totaldiscount=($dbdiscountandfeedata->welcomediscount + $dbdiscountandfeedata->rewarddiscount + $dbdiscountandfeedata->perioddiscount + $dbdiscountandfeedata->promocodediscount);
$pricevaluessection='<table cellspacing="0" cellpadding="4" border="1" class="discountpaymenttable">
        <tr>
          <td style="text-align:left;width:73%;"><b>TOTAL HT</b></td>
          <td style="width:27%;text-align:center;">'.round($totalhtprice,2).' €</td>    
        </tr>';
         if($dbdiscountandfeedata->welcomediscount!=0){
          $totalhtprice -= $dbdiscountandfeedata->welcomediscountfee;
          $pricevaluessection.='<tr>
                      <td style="text-align:left;width:73%;"><b>Welcome Discount ('.$dbdiscountandfeedata->welcomediscount.'%)</b></td>
                      <td style="width:27%;text-align:center;" > '.$dbdiscountandfeedata->welcomediscountfee.' €</td>    
                    </tr>';
         }
            
          
           if($dbdiscountandfeedata->rewarddiscount!=0){
            $totalhtprice -= $dbdiscountandfeedata->rewarddiscountfee;
            $pricevaluessection.='<tr>
                      <td style="text-align:left;width:73%;"><b>Reward Discount ('.$dbdiscountandfeedata->rewarddiscount.'%)</b></td>
                      <td style="width:27%;text-align:center;">'.$dbdiscountandfeedata->rewarddiscountfee.' €</td>    
                    </tr>';
           }
            
           if($dbdiscountandfeedata->perioddiscount!=0){
           $totalhtprice -= $dbdiscountandfeedata->perioddiscountfee;
            $pricevaluessection.='<tr>
                      <td style="text-align:left;width:73%;"><b>Period Discount ('.$dbdiscountandfeedata->perioddiscount.'%)</b></td>
                      <td style="width:27%;text-align:center;">'.$dbdiscountandfeedata->perioddiscountfee.' €</td>    
                    </tr>';
           }
              if($dbdiscountandfeedata->promocodediscount!=0){
                 $totalhtprice -= $dbdiscountandfeedata->promocodediscountfee;
            $pricevaluessection.='<tr>
                      <td style="text-align:left;width:73%;"><b>Discount Code ('.$dbdiscountandfeedata->promocodediscount.'%)</b></td>
                      <td style="width:27%;text-align:center;">'.$dbdiscountandfeedata->promocodediscountfee.' €</td>    
                    </tr>';
          
                  }
            if($totaldiscount == 0){
                 $pricevaluessection.='<tr>
                      <td style="text-align:left;width:73%;"><b>DISCOUNT (0%)</b></td>
                      <td style="width:27%;text-align:center;"> 0 €</td>    
                    </tr>';
            }
              $pricevaluessection.='<tr>
                      <td style="text-align:left;width:73%;"><b>TOTAL HT</b></td>
                      <td style="width:27%;text-align:center;">'.round($totalhtprice,2).' €</td>    
                    </tr>';
          $pricevaluessection.='<tr>
                      <td style="text-align:left;width:73%;"><b>VAT ('.$dbdiscountandfeedata->vatdiscount.'%)</b></td>
                      <td style="width:27%;text-align:center;">'.$dbdiscountandfeedata->vatfee.' €</td>    
                    </tr>';
          $pricevaluessection.='<tr>
                      <td style="text-align:left;width:73%;"><b>TOTAL TTC</b></td>
                      <td style="width:27%;text-align:center;">'.round(($totalhtprice + $dbdiscountandfeedata->vatfee),2).' €</td>    
                    </tr>';
     $pricevaluessection.='</table>
     <style>
        table.discountpaymenttable{
            border-collapse:collapse;
             border:1px solid #000000;
             table-layout: auto;
             width: 100%;  
           
         }
      
        
        
        table.discountpaymenttable tr td{          
          border:1px solid #000000;
          text-align:right;   
          font-size:8px;    
         }

       
        
     </style>
';
$customText='<table cellspacing="0" cellpadding="4"  class="insidetext">
<tr>
<td class="insidetexttd" >'.$dbquotesdata->customtext.'</td>
</tr>

</table>
    <style>
        table.insidetext{
          border:none;
        }
        td.insidetexttd{
           border-left:none;
           border-right:none;
           border-top:none;
           border-bottom:none;
           font-size:9px;
        }

      </style>  
';
$commentSection='<table cellspacing="0" cellpadding="1" border="0"  class="commenttable">
<tr>
<td style="width:100%;height:70px;">'.$customText.'</td>
</tr>

<tr>
<td style="width:100%;border:none;"><br><br>Done on : '.(($quotes->status == 1)?from_unix_date($quotes->approveddate):"-- -- ----").' '.
'&nbsp;&nbsp;&nbsp; At : '.$clientcity.'</td>
</tr>
<tr>
<td style="width:100%;border:none;">Signature  (and Stamp if you are a company) : </td>
</tr>
<tr>
<td style="width:100%;border:none;">Write ! Agree here : <br></td>
</tr>

<tr>
<td style="width:54%;height:70px;"></td>
</tr>
</table>
   <style>
        table.commenttable{
             
             
             table-layout: auto;
             width: 100%;  
           
         }
      
        table.commenttable tr td{          
          border:1px solid #000000;
           text-align:left;
           font-size:9px;
          
         
         }
      </style>   
';

$combinedSection='<table  class="combinedtable">
<tr>
<td style="width:62.7%;">'.$commentSection.'</td>
<td style="width:8%;"></td>
<td style="width:28.8%;">'.$pricevaluessection.'</td>
</tr>
</table>
  <style>
        table.combinedtable{
             
             
             table-layout: auto;
             width: 100%;  
             border:none;

           
         }
      
        table.combinedtable tr td{          
          border:none;
         
         }
      </style>  
';

$pdf->setFont('helvetica','',9);
$pdf->writeHTMLCell(191,'','','',$combinedSection,0,1);


//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->addPage();
$pdf->setCellPaddings( $left = 0, $top = 0, $right = 0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 10);
$pdf->setFont('helveticaB','',13);
$pdf->cell(30,5,'',0,0,'C');
$pdf->cell(132,10,'TERMS OF SERVICE',1,0,'C');
$pdf->cell(30,5,'',0,1,'C');

$pdf->setFont('helvetica','',10);
$termservices=$page_content->description;;
$pdf->writeHTMLCell(190,0,'','',$termservices,0,1);

//

//$quotepdffile=$pdf->Output(strtolower($filename).".pdf", 'S');
//$quotepdfarray=array('quotepdffile'=>$quotepdffile,'filename'=>strtolower($filename).".pdf");
return array('pdf' => $pdf,'fileName' => $fileName);
        //create pdf
}


function createnotificationpdf($id,$booking_id){
  $CI = get_instance();
    $CI->load->model('bookings_config_model');
    $CI->load->model('quotes_model');
    $CI->load->model('invoice_model');
    $CI->load->model('bookings_model');

  $quotenotificationsdata=$CI->quotes_model->getsinglerecord('vbs_quotes_notification',['id' => $id]);

  $notificationsdata = $CI->quotes_model->getremindersnotification('vbs_notifications',['id' => $quotenotificationsdata->notification_id]);

  $dbbookingdata=$CI->bookings_model->getdbbookingrecord('vbs_bookings',array('id'=>$booking_id));

  $quotes=$CI->bookings_model->getdbbookingrecord('vbs_quotes',array('booking_id'=>$booking_id));

  $companydata=$CI->bookings_model->getcompanystatus();

  $companycity=$CI->quotes_model->getsinglerecord('vbs_cities',['id'=>$companydata->city])->name;

  $client=$CI->bookings_model->getclientdatabyid($dbbookingdata->user_id);
  $clientcity=$CI->quotes_model->getsinglerecord('vbs_cities',['id'=>$client->city])->name;

  $user = $CI->basic_auth->user();
  $CI->load->model('userx_model');
  $user_data = $CI->userx_model->get(['user.id' => $user->id]);
  //notification data
  $CI->load->library('bookpdf');

  $pdf = new bookpdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

   $address2=(!empty($companydata->address2))?trim($companydata->address2).', ' :'';
$companydatainfo=strtoupper($companydata->name).' '.strtoupper($companydata->type).', '.$companydata->address.', '.$address2.$companydata->zip_code.', '.$companycity.', Capital : '.$companydata->capital.', License N : '.$companydata->licence.', SIRET : '.$companydata->sirft.', VAT N<sup>o</sup> : '.$companydata->numero_tva;
$pdf->setInfo($companydatainfo);
$fileName='Pending Quote Notification '.create_timestampdmy_uid($quotes->date,$quotes->id).' '.$client->civility.' '.$client->first_name.' '.$client->last_name;
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle($fileName.".PDF");
$pdf->SetSubject($fileName.".PDF");
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setPrintHeader(false);
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);



$pdf->addPage();



/* Company information */
$cominfo1="<span><b>".strtoupper($companydata->name).' '.strtoupper($companydata->type)."</b></span><br><span>".$companydata->address."</span><br>";
if(!empty($companydata->address2)){
  $cominfo1.="<span>".$companydata->address2."</span><br>";
}
$cominfo1.="<span>".$companydata->zip_code.' '.$companycity."</span><br><span>Phone&nbsp;&nbsp;&nbsp;&nbsp; : ".$companydata->phone."</span><br><span>Fax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ".$companydata->fax."</span><br><span>Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: ".$companydata->email."</span><br><span>Website&nbsp;&nbsp;: ".$companydata->website."</span>";
/* Company information */


/* Client information */
$clientinfo="<b>&nbsp;".$client->civility.' '.$client->first_name.' '.$client->last_name."</b><br>&nbsp;".$client->address."<br>&nbsp;";
if(!empty($client->address1)){
  $clientinfo.=$client->address1."<br>&nbsp;";
}
$clientinfo.=$client->zipcode.'  '.$clientcity;
/* Client information */







/* Company Logo */
$companylogo=base_url().'uploads/company/'.$companydata->logo;
$companylogo = '<img src="'.$companylogo.'" width="110" height="95"/>';
/* Company Logo */

/* Set Company/Client/Quote Information in Table */
$html='<table cellspacing="0" cellpadding="0" >
<tr>
          <td style="width:33.3%;">'.$companylogo.'</td>
          <td style="width:33.3%;"></td>
          <td style="width:33.3%;"></td>
        </tr></table>';
$html .='<table cellspacing="0" cellpadding="0" >
       

        <tr>
          <td style="width:33.3%;">'.$cominfo1.'</td>
          <td style="width:33.3%;"></td>
         <td style="width:33.3%;"><div class="test" style="page-break-inside: avoid;"><table cellspacing="0" cellpadding="6" ><tr><td>'.$clientinfo.'</td></tr></table></div>


          <style>   
              div.test {
                
                  border:0.1em solid black;  

              }
          </style></td> 
        </tr> 

        <tr>
          <td></td>
          <td></td>
          <td>Done at : '.$companycity.'&nbsp; on : '.from_unix_date($quotenotificationsdata->date).'</td>     
        </tr>        
     </table>
     <style>
        table{
             border-collapse:collapse;
             table-layout: auto;
             width: 100%;    
         }
      
         th{
            border:1px solid #000000;
            text-align:center;
            font-size:10px !important;
         }
        
         td{
           
           text-align:left;
          
         
         }

       
        
     </style>
';
/* Set Company/Client/Quote Information in Table */
$message=str_replace("{client_civility}",$client->civility,$notificationsdata->message);
$message=str_replace("{client_firstname}",$client->first_name,$message);
$message=str_replace("{client_lastname}",$client->last_name,$message);
$message=str_replace("{quote_id}",create_timestampdmy_uid($quotes->date,$quotes->id),$message);
$message=str_replace("{quote_date}",from_unix_date($quotes->date),$message);

$message="<h4>Subject : ".$notificationsdata->subject."</h4>".$message;
    
$notificationtitle="PENDING QUOTE NOTIFICATION";
$pdf->setFont('helveticaB','',12);
$pdf->cell(30,10,'',0,0,'C');
$pdf->cell(132,10,$notificationtitle,1,0,'C');
$pdf->cell(30,10,'',0,1,'C');
$pdf->Ln(4);
//Set Reminder Message


 

$pdf->setFont('helvetica','',11);
$pdf->setCellPaddings( $left = 1, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 3);
$pdf->writeHTMLCell(190,0,10,'',$html,0,1);




//Set Notification Message
$pdf->setCellPaddings( $left = 1, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 0);
$pdf->setFont('helvetica','',11);

$pdf->writeHTMLCell(190,'','','',$message,0,1);
 $positiony = $pdf->GetY();
//$pdf->cell(192,2,$positiony,1,1,'C');

//signature box

$pdf->setCellPaddings( $left = 0, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 0);
/*admin logo */
$adminsignaturelogo=base_url().'uploads/user/'.$user_data['document_signature'];
$adminsignaturelogo = '<img src="'.$adminsignaturelogo.'" width="110" height="95"/>';
/*admin logo*/
$userdeparment=$user_data['department'];
$username='<span>'.$user_data['civility'].' '.$user_data['first_name'].' '.$user_data['last_name'].'</span>';
$stampsection='<table cellspacing="2" cellpadding="1" border="0" nobr="true">
<tr>
<th style="width:67%;"></th>
<th style="width:33%;"><b>'.$userdeparment.' Department</b><br>'.$username.'<br>Signature and Stamp</th>
</tr>

<tr>
<td style="border:none;"></td>
<td  style="border-top:none;">'.$adminsignaturelogo.'</td>
</tr>
</table>
   <style>
        table{
             border-collapse:collapse;
             table-layout: auto;
             width: 100%;  
         }
         th{
           border:none; 
           text-align:left;
         }
         td{   
          border:0.1em  solid #000000;
          text-align:center; 
         }
      </style>   
';

$pdf->setFont('helvetica','',10);
if($positiony < 223){
  $pdf->SetY(-75);
  $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}
elseif($positiony < 263){
   $pdf->Ln(45);
   $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}
else{
   $pdf->Ln(5);
   $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}

//signature box


return array('pdf' => $pdf,'fileName' => $fileName);
}



function createreminderpdf($id,$level,$booking_id){
  $CI = get_instance();
    $CI->load->model('bookings_config_model');
    $CI->load->model('quotes_model');
    $CI->load->model('invoice_model');
    $CI->load->model('bookings_model');


  $quotereminderndata=$CI->quotes_model->getsinglerecord('vbs_quotes_reminder',['id' => $id]);
  $remindersdata = $CI->quotes_model->getremindersnotification('vbs_reminders',['id' => $quotereminderndata->reminder_id]);
  $dbbookingdata=$CI->bookings_model->getdbbookingrecord('vbs_bookings',array('id'=>$booking_id));
  $quotes=$CI->bookings_model->getdbbookingrecord('vbs_quotes',array('booking_id'=>$booking_id));
  $companydata=$CI->bookings_model->getcompanystatus();
  $companycity=$CI->quotes_model->getsinglerecord('vbs_cities',['id'=>$companydata->city])->name;
  $client=$CI->bookings_model->getclientdatabyid($dbbookingdata->user_id);
  $clientcity=$CI->bookings_model->getsinglerecord('vbs_cities',['id'=>$client->city])->name;
  $level=$level;
  $user = $CI->basic_auth->user();
  $CI->load->model('userx_model');
  $user_data = $CI->userx_model->get(['user.id' => $user->id]);

    $CI->load->library('bookpdf');
    $pdf = new bookpdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    $address2=(!empty($companydata->address2))?trim($companydata->address2).', ' :'';
    $companydatainfo=strtoupper($companydata->name).' '.strtoupper($companydata->type).', '.$companydata->address.', '.$address2.$companydata->zip_code.', '.$companycity.', Capital : '.$companydata->capital.', License N : '.$companydata->licence.', SIRET : '.$companydata->sirft.', VAT N<sup>o</sup> : '.$companydata->numero_tva;

$pdf->setInfo($companydatainfo);
if($level == 1){
  $remindertype="First";
}
elseif($level == 2){
  $remindertype="Second";
}
elseif($level == 3){
  $remindertype="Third";
}
$fileName='Pending Quote '.$remindertype.' Reminder '.create_timestampdmy_uid($quotes->date,$quotes->id).' '.$client->civility.' '.$client->first_name.' '.$client->last_name;
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle($fileName.".PDF");
$pdf->SetSubject($fileName.".PDF");
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setPrintHeader(false);
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);



$pdf->addPage();



/* Company information */
$cominfo1="<span><b>".strtoupper($companydata->name).' '.strtoupper($companydata->type)."</b></span><br><span>".$companydata->address."</span><br>";
if(!empty($companydata->address2)){
  $cominfo1.="<span>".$companydata->address2."</span><br>";
}
$cominfo1.="<span>".$companydata->zip_code.' '.$companycity."</span><br><span>Phone&nbsp;&nbsp;&nbsp;&nbsp; : ".$companydata->phone."</span><br><span>Fax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ".$companydata->fax."</span><br><span>Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: ".$companydata->email."</span><br><span>Website&nbsp;&nbsp;: ".$companydata->website."</span>";
/* Company information */


/* Client information */
$clientinfo="<b>&nbsp;".$client->civility.' '.$client->first_name.' '.$client->last_name."</b><br>&nbsp;".$client->address."<br>&nbsp;";
if(!empty($client->address1)){
  $clientinfo.=$client->address1."<br>&nbsp;";
}
$clientinfo.=$client->zipcode.'  '.$clientcity;
/* Client information */







/* Company Logo */
$companylogo=base_url().'uploads/company/'.$companydata->logo;
$companylogo = '<img src="'.$companylogo.'" width="110" height="95"/>';
/* Company Logo */

/* Set Company/Client/Quote Information in Table */
$html='<table cellspacing="0" cellpadding="0" >
<tr>
          <td style="width:35%;">'.$companylogo.'</td>
          <td style="width:30%;"></td>
          <td style="width:35%;"></td>
        </tr></table>';
$html .='<table cellspacing="0" cellpadding="0" >
       

        <tr>
          <td style="width:35%;">'.$cominfo1.'</td>
          <td style="width:30%;"></td>
        <td style="width:33.3%;"><div class="test" style="page-break-inside: avoid;"><table cellspacing="0" cellpadding="6" ><tr><td>'.$clientinfo.'</td></tr></table></div>


          <style>   
              div.test {
                
                  border:0.1em  solid #000000;

              }
          </style></td>   
        </tr> 

        <tr>
          <td></td>
          <td></td>
          <td>Done at : '.$companycity.'&nbsp; on : '.from_unix_date($quotereminderndata->date).'</td>     
        </tr>        
     </table>
     <style>
        table{
             border-collapse:collapse;
             table-layout: auto;
             width: 100%;    
         }
      
         th{
            border:1px solid #000000;
            text-align:center;
            font-size:10px !important;
         }
        
         td{
           
           text-align:left;
          
         
         }

       
        
     </style>
';
/* Set Company/Client/Quote Information in Table */
$message=str_replace("{client_civility}",$client->civility,$remindersdata->message);
$message=str_replace("{client_firstname}",$client->first_name,$message);
$message=str_replace("{client_lastname}",$client->last_name,$message);
$message=str_replace("{quote_id}",create_timestampdmy_uid($quotes->date,$quotes->id),$message);
$message=str_replace("{quote_date}",from_unix_date($quotes->date),$message);
//Set Reminder Message 
$message="<h4>Subject : ".$remindersdata->subject."</h4>".$message;
    
              
$remindertitle="PENDING QUOTE ".strtoupper($remindertype)." REMINDER";
//$pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
$pdf->setFont('helveticaB','',12);
$pdf->cell(30,10,'',0,0,'C');
$pdf->cell(132,10,$remindertitle,1,0,'C');
$pdf->cell(30,10,'',0,1,'C');
$pdf->Ln(4);
//Set Reminder Message


 

$pdf->setFont('helvetica','',11);
$pdf->setCellPaddings( $left = 1, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 3);
$pdf->writeHTMLCell(190,0,10,'',$html,0,1);




//Set Notification Message
$pdf->setCellPaddings( $left = 1, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 0);
$pdf->setFont('helvetica','',11);

$pdf->writeHTMLCell(190,'','','',$message,0,1);
 $positiony = $pdf->GetY();
//$pdf->cell(192,2,$positiony,1,1,'C');

//signature box

$pdf->setCellPaddings( $left = 0, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 0);
/*admin logo */
$adminsignaturelogo=base_url().'uploads/user/'.$user_data['document_signature'];
$adminsignaturelogo = '<img src="'.$adminsignaturelogo.'" width="110" height="95"/>';
/*admin logo*/
$userdeparment=$user_data['department'];
$username='<span>'.$user_data['civility'].' '.$user_data['first_name'].' '.$user_data['last_name'].'</span>';
$stampsection='<table cellspacing="2" cellpadding="1" border="0" nobr="true">
<tr>
<th style="width:67%;"></th>
<th style="width:33%;"><b>'.$userdeparment.' Department</b><br>'.$username.'<br>Signature and Stamp</th>
</tr>

<tr>
<td style="border:none;"></td>
<td  style="border-top:none;">'.$adminsignaturelogo.'</td>
</tr>
</table>
   <style>
        table{
             border-collapse:collapse;
             table-layout: auto;
             width: 100%;  
         }
         th{
           border:none; 
           text-align:left;
         }
         td{   
          border:0.1em  solid #000000;
          text-align:center; 
         }
      </style>   
';

$pdf->setFont('helvetica','',10);
if($positiony < 223){
  $pdf->SetY(-75);
  $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}
elseif($positiony < 263){
   $pdf->Ln(45);
   $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}
else{
   $pdf->Ln(5);
   $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}

//signature box




return array('pdf' => $pdf,'fileName' => $fileName);
 }
 /* PDF FOR QUOTE MODULE END*/



 /* PDF FOR INVOICE MODULE START*/

 function createinvoicepdf($booking_id){

      $CI = get_instance();
        $status="1";

        //booking add record
        $CI->load->model('bookings_config_model');
        $CI->load->model('quotes_model');
        $CI->load->model('invoice_model');
        $CI->load->model('bookings_model');
        $dbinvoicesdata = $CI->bookings_config_model->getpricestatusdata('vbs_invoices_config');
        
        $dbbookingdata=$CI->bookings_model->getdbbookingrecord('vbs_bookings',array('id'=>$booking_id));
      
        $dbpickairportdata=$CI->bookings_model->getdbbookingrecord('vbs_booking_airport',array('booking_id'=>$booking_id,'type'=>'pickup'));
        $dbpicktraindata=$CI->bookings_model->getdbbookingrecord('vbs_booking_train',array('booking_id'=>$booking_id,'type'=>'pickup'));
        $dbpickaddressdata=$CI->bookings_model->getdbbookingrecord('vbs_booking_address',array('booking_id'=>$booking_id,'type'=>'pickup'));

         $dbdropairportdata=$CI->bookings_model->getdbbookingrecord('vbs_booking_airport',array('booking_id'=>$booking_id,'type'=>'dropoff'));
        $dbdroptraindata=$CI->bookings_model->getdbbookingrecord('vbs_booking_train',array('booking_id'=>$booking_id,'type'=>'dropoff'));
        $dbdropaddressdata=$CI->bookings_model->getdbbookingrecord('vbs_booking_address',array('booking_id'=>$booking_id,'type'=>'dropoff'));
        $dbpickregulardata=$CI->bookings_model->getdbmultiplebookingrecord('vbs_booking_regschedule',array('booking_id'=>$booking_id,'type'=>'regular'));
        $dbreturndata=$CI->bookings_model->getdbmultiplebookingrecord('vbs_booking_regschedule',array('booking_id'=>$booking_id,'type'=>'return'));
        $dbstopsdata=$CI->bookings_model->getdbmultiplebookingrecord('vbs_booking_stops',array('booking_id'=>$booking_id));
        $dbpassengers=$CI->bookings_model->getdbpassengersbookingrecord(array('booking_id'=>$booking_id));
        $dbdiscountandfeedata=$CI->bookings_model->getdbbookingrecord('vbs_bookings_pricevalues',array('booking_id'=>$booking_id));
        $dbcreditcarddata=$CI->bookings_model->getdbbookingrecord('vbs_booking_creditcard',array('booking_id'=>$booking_id));
        $dbdebitbankdata=$CI->bookings_model->getdbbookingrecord('vbs_booking_directdebit',array('booking_id'=>$booking_id));
  
         $car_configuration_data= $CI->bookings_model->booking_Configuration('vbs_car_configuration',['car_id' => $dbbookingdata->car_id]);
        $servicecategory= $CI->bookings_model->getservicesbyid('vbs_u_category_service',$dbbookingdata->service_category_id);
        $servicecategoryname= $servicecategory->category_name;

        $service= $CI->bookings_model->getservicesbyid('vbs_u_service', $dbbookingdata->service_id);
        $servicename= $service->service_name;
        $invoices=$CI->bookings_model->getdbbookingrecord('vbs_invoices',array('booking_id'=>$booking_id));
       $paymentmethod=$CI->bookings_model->getdbbookingrecord('vbs_u_payment_method',array('id'=>$dbbookingdata->payment_method_id));
       $custompayments=$CI->invoice_model->getmultiplerecord('vbs_invoices_custompayments',['invoice_id'=> $invoices->id]);
      
        $dbtermservices=$CI->bookings_model->getdbbookingrecord('cms_page_post',array('language'=>'en','status'=>$status));

          
        $companydata=$CI->bookings_model->getcompanystatus();
        $companycity=$CI->invoice_model->getsinglerecord('vbs_cities',['id'=>$companydata->city])->name;
        $client=$CI->bookings_model->getclientdatabyid($dbbookingdata->user_id);
        $clientcity=$CI->bookings_model->getsinglerecord('vbs_cities',['id'=>$client->city])->name;
        $car_name=$CI->bookings_model->getcarname($dbbookingdata->car_id);

        $CI->load->library('bookpdf');
        $pdf = new bookpdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

           $address2=(!empty($companydata->address2))?trim($companydata->address2).', ' :'';
        $companydatainfo=strtoupper($companydata->name).' '.strtoupper($companydata->type).', '.$companydata->address.', '.$address2.$companydata->zip_code.', '.$companycity.', Capital : '.$companydata->capital.', License N : '.$companydata->licence.', SIRET : '.$companydata->sirft.', VAT N<sup>o</sup> : '.$companydata->numero_tva;
        $pdf->setInfo($companydatainfo);
      $fileName='Invoices '.create_timestampdmy_uid($invoices->date,$invoices->id).' '.$client->civility.' '.$client->first_name.' '.$client->last_name;
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle($fileName.".PDF");
        $pdf->SetSubject($fileName.".PDF");
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // set default header data
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setPrintHeader(false);
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        // set margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);



        $pdf->addPage();
        $pdf->AddFont('fontawesome', '');
        $pdf->SetFont('fontawesome', '', 14, '', true);


        // $pdf->writeHTMLCell("&#xf0a4;&#xf01d;&#xf03e;"); // three icons

        $customimage=base_url().$dbinvoicesdata->customicon;
        $customimage = '<img src="'.$customimage.'" />';


        /* Company information */
        $cominfo1="<span><b>".strtoupper($companydata->name).' '.strtoupper($companydata->type)."</b></span><br><span>".$companydata->address."</span><br>";
        if(!empty($companydata->address2)){
          $cominfo1.="<span>".$companydata->address2."</span><br>";
        }
        $cominfo1.="<span>".$companydata->zip_code.' '.$companycity."</span><br><span>Phone&nbsp;&nbsp;&nbsp;&nbsp; : ".$companydata->phone."</span><br><span>Fax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ".$companydata->fax."</span><br><span>Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: ".$companydata->email."</span><br><span>Website&nbsp;&nbsp;: ".$companydata->website."</span>";
        /* Company information */


        /* Client information */
        $clientinfo="<b>&nbsp;".$client->civility.' '.$client->first_name.' '.$client->last_name."</b><br>&nbsp;".$client->address."<br>&nbsp;";
        if(!empty($client->address1)){
          $clientinfo.=$client->address1."<br>&nbsp;";
        }
        $clientinfo.=$client->zipcode.'  '.$clientcity;
        /* Client information */

        /* Invoice information */
        $date=$invoices->date;
        $invoiceDate="Date : ".from_unix_date($invoices->date);
        $invoiceReference="Reference : ".$invoices->reference;
        $invoiceAvlDate="Must be paid beford : ".date('d/m/Y', strtotime($date. ' + '.$dbinvoicesdata->invoicedelay.' days'));
        /* Invoice information */
        /* Custom Logo */
        $customimage=base_url().$dbinvoicesdata->customicon;
        $customimage = '<img src="'.$customimage.'"  width="'.$dbinvoicesdata->customiconwidth.'" height="'.$dbinvoicesdata->customiconheight.'"/>';
        /* Custom Logo */

        /* Pending/ Approved Logo */
        $pendingapprovedicon='';
        if($invoices->status == 0){
        $pendingapprovedicon=base_url().$dbinvoicesdata->statutpendingicon;
        $pendingapprovedicon = '<img src="'.$pendingapprovedicon.'" width="'.$dbinvoicesdata->pendingiconwidth.'" height="'.$dbinvoicesdata->pendingiconheight.'"/>';
        }
        elseif($invoices->status == 1){
          $pendingapprovedicon=base_url().$dbinvoicesdata->statutpaidicon;
          $pendingapprovedicon = '<img src="'.$pendingapprovedicon.'" width="'.$dbinvoicesdata->paidiconwidth.'" height="'.$dbinvoicesdata->paidiconheight.'"/>';
        }
        elseif($invoices->status == 2){
          $pendingapprovedicon=base_url().$dbinvoicesdata->statutcancelledicon;
          $pendingapprovedicon = '<img src="'.$pendingapprovedicon.'" width="'.$dbinvoicesdata->cancellediconwidth.'" height="'.$dbinvoicesdata->cancellediconheight.'"/>';

        }
        else{
        $pendingapprovedicon=base_url().$dbinvoicesdata->statutpendingicon;
        $pendingapprovedicon = '<img src="'.$pendingapprovedicon.'" width="'.$dbinvoicesdata->pendingiconwidth.'" height="'.$dbinvoicesdata->pendingiconheight.'"/>';
        }

        /* Pending/ Approved Logo */

        /* Company Logo */
        $companylogo=base_url().'uploads/company/'.$companydata->logo;
        $companylogo = '<img src="'.$companylogo.'"/>';
        /* Company Logo */

        /* Set Company/Client/Quote Information in Table */
        $html='<table cellspacing="0" cellpadding="0" >
        <tr>
                  <td style="width:33.3%;">'.$companylogo.'</td>
                  <td style="width:33.3%;"></td>
                  <td style="width:33.3%;"></td>
                </tr></table>';
        $html .='<table cellspacing="0" cellpadding="0" >
                

                <tr>
                   <td style="width:33.3%;">'.$cominfo1.'</td>
                  <td style="width:33.3%;"></td>
                  <td style="width:33.3%;"><div class="test" style="page-break-inside: avoid;"><table cellspacing="0" cellpadding="6" ><tr><td>'.$clientinfo.'</td></tr></table></div>


                  <style>   
                      div.test {
                        
                          border:0.1em solid black;  

                      }
                  </style></td>

                    
                </tr> 
                 <tr>
                  <td></td>
                  <td>'.$customimage.'</td>
                  <td>'.$pendingapprovedicon.'</td>      
                </tr> 
                <tr>
                  <td>'.$invoiceDate.'</td>
                  <td>'.$invoiceReference.'</td>
                  <td style="text-align:right;">'.$invoiceAvlDate.'</td>      
                </tr>        
             </table>
             <style>
                table{
                     border-collapse:collapse;
                     table-layout: auto;
                     width: 100%;    
                 }
              
                 th{
                    border:1px solid #000000;
                    text-align:center;
                    font-size:10px !important;
                 }
                
                 td{
                      
                       text-align:left;
                  
                 
                 }

               
                
             </style>
        ';
        /* Set Company/Client/Quote Information in Table */


        $totalhtprice;
        $fullbookinginformation;
        $bookdetailarray=getbookingdetails($booking_id,$CI);
        $totalhtprice=$bookdetailarray['totalhtprice'];
        $fullbookinginformation=$bookdetailarray['fullbookinginformation'];
        
        $pdf->setFont('FontAwesome','',20);
        /* Set Booking information in table*/
        $pdf->setCellMargins($left = 0, $top =0, $right = 0, $bottom = 0);
        $pdf->setCellPaddings( $left = 0, $top = 2, $right = 0, $bottom = 2);
        $suptext="<sup>o</sup>";
        $invTitle="INVOICE N".$suptext." : ".create_timestampdmy_uid($invoices->date,$invoices->id);
        $pdf->setFont('helveticaB','',13);
        $pdf->Cell(30,10,'',0,0,'C');
        $pdf->writeHTMLCell(132,'','','',$invTitle,1,0,0,true,'C',true);
        $pdf->Cell(30,10,'',0,1,'C');
        $pdf->Ln(4);

        $pdf->setCellPaddings( $left = 0, $top = 0, $right = 0, $bottom = 0);
        $pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 3);
        $pdf->setFont('helvetica','',11);
        $pdf->writeHTMLCell(190,0,10,'',$html,0,1);
        //$pdf->writeHTML($html, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');


        $pdf->setFont('helvetica','',9);
        $pdf->setCellPaddings( $left = 0, $top = 0, $right = 0, $bottom = 0);
        $pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 3);
        $pdf->writeHTMLCell(190,0,10,'',$fullbookinginformation,0,1);

        // ---------------------------------------------------------
        $totaldiscountfee=($dbdiscountandfeedata->welcomediscountfee + $dbdiscountandfeedata->rewarddiscountfee + $dbdiscountandfeedata->perioddiscountfee + $dbdiscountandfeedata->promocodediscountfee);
        $totaldiscount=($dbdiscountandfeedata->welcomediscount + $dbdiscountandfeedata->rewarddiscount + $dbdiscountandfeedata->perioddiscount + $dbdiscountandfeedata->promocodediscount);
        $pricevaluessection='<table cellspacing="0" cellpadding="4" border="1" class="discountpaymenttable">
                <tr>
                  <td style="text-align:left;width:73%;"><b>TOTAL HT</b></td>
                  <td style="width:27%;text-align:center;">'.round($totalhtprice,2).' €</td>    
                </tr>';
                 if($dbdiscountandfeedata->welcomediscount!=0){
                  $totalhtprice -= $dbdiscountandfeedata->welcomediscountfee;
                  $pricevaluessection.='<tr>
                              <td style="text-align:left;width:73%;"><b>Welcome Discount ('.$dbdiscountandfeedata->welcomediscount.'%)</b></td>
                              <td style="width:27%;text-align:center;"> '.$dbdiscountandfeedata->welcomediscountfee.' €</td>    
                            </tr>';
                 }
                    
                  
                   if($dbdiscountandfeedata->rewarddiscount!=0){
                    $totalhtprice -= $dbdiscountandfeedata->rewarddiscountfee;
                    $pricevaluessection.='<tr>
                              <td style="text-align:left;width:73%;"><b>Reward Discount ('.$dbdiscountandfeedata->rewarddiscount.'%)</b></td>
                              <td style="width:27%;text-align:center;">'.$dbdiscountandfeedata->rewarddiscountfee.' €</td>    
                            </tr>';
                   }
                    
                   if($dbdiscountandfeedata->perioddiscount!=0){
                   $totalhtprice -= $dbdiscountandfeedata->perioddiscountfee;
                    $pricevaluessection.='<tr>
                              <td style="text-align:left;width:73%;"><b>Period Discount ('.$dbdiscountandfeedata->perioddiscount.'%)</b></td>
                              <td style="width:27%;text-align:center;"> '.$dbdiscountandfeedata->perioddiscountfee.' €</td>    
                            </tr>';
                   }
                      if($dbdiscountandfeedata->promocodediscount!=0){
                         $totalhtprice -= $dbdiscountandfeedata->promocodediscountfee;
                    $pricevaluessection.='<tr>
                              <td style="text-align:left;width:73%;"><b>Discount Code ('.$dbdiscountandfeedata->promocodediscount.'%)</b></td>
                              <td style="width:27%;text-align:center;"> '.$dbdiscountandfeedata->promocodediscountfee.' €</td>    
                            </tr>';
                  
                          }
                    if($totaldiscount == 0){
                         $pricevaluessection.='<tr>
                              <td style="text-align:left;width:73%;"><b>DISCOUNT (0%)</b></td>
                              <td style="width:27%;text-align:center;"> 0 €</td>    
                            </tr>';
                    }
                      $pricevaluessection.='<tr>
                              <td style="text-align:left;width:73%;"><b>TOTAL HT</b></td>
                              <td style="width:27%;text-align:center;">'.round($totalhtprice,2).' €</td>    
                            </tr>';
                  $pricevaluessection.='<tr>
                              <td style="text-align:left;width:73%;"><b>VAT ('.$dbdiscountandfeedata->vatdiscount.'%)</b></td>
                              <td style="width:27%;text-align:center;"> '.$dbdiscountandfeedata->vatfee.' €</td>    
                            </tr>';
                  $pricevaluessection.='<tr>
                              <td style="text-align:left;width:73%;"><b>TOTAL TTC</b></td>
                              <td style="width:27%;text-align:center;">'.round(($totalhtprice + $dbdiscountandfeedata->vatfee),2).' €</td>    
                            </tr>';
                   $pricevaluessection.='<tr>
                  <td style="text-align:left;width:73%;"><b>TOTAL PAID</b></td>
                  <td style="width:27%;text-align:center;">'.$dbbookingdata->payment_received.' €</td>    
                  </tr>';
                  $totalvtht=$totalhtprice + $dbdiscountandfeedata->vatfee;
                 $pricevaluessection.='<tr>
                  <td style="text-align:left;width:73%;"><b>REST DUE</b></td>
                  <td style="width:27%;text-align:center;">'.bcsub(($totalhtprice + $dbdiscountandfeedata->vatfee) , $dbbookingdata->payment_received,2).' €</td>    
                  </tr>';
             $pricevaluessection.='</table>
             <style>
                table.discountpaymenttable{
                    border-collapse:collapse;
                     border:1px solid #000000;
                     table-layout: auto;
                     width: 100%;  
                   
                 }
              
                
                
                table.discountpaymenttable tr td{          
                  border:1px solid #000000;
                  text-align:right;   
                  font-size:8px;    
                 }

               
                
             </style>
        ';
        $customText='<table cellspacing="0" cellpadding="4"  class="insidetext">
        <tr>
        <td class="insidetexttd" >'.$dbinvoicesdata->customtext.'</td>
        </tr>

        </table>
            <style>
                table.insidetext{
                  border:none;
                }
                td.insidetexttd{
                   border-left:none;
                   border-right:none;
                   border-top:none;
                   border-bottom:none;
                   font-size:9px;
                }

              </style>  
        ';
        $customPaymentstxt='<table cellspacing="0" cellpadding="1"  class="insidetext">
        <tr>
        <td class="insidetexttd" style="width:25%;text-align:left;"><b>Date</b></td>
        <td class="insidetexttd" style="width:25%;text-align:left;"><b>Amount</b></td>
        <td class="insidetexttd" style="width:25%;text-align:left;"><b>Payment Methode</b></td>
        <td class="insidetexttd" style="width:25%;text-align:right;"><b>Reference</b></td>
        </tr>';
        if($custompayments){
                        foreach ($custompayments as $payment) {
                            $customPaymentstxt.='<tr>
                              <td class="insidetexttd" style="width:25%;text-align:left;">'.from_unix_date($payment->date).'</td>
                              <td class="insidetexttd" style="width:25%;text-align:left;">'.$payment->amount.' €</td>
                              <td class="insidetexttd" style="width:25%;text-align:left;">'.$payment->method.'</td>
                              <td class="insidetexttd" style="width:25%;text-align:right;">'.$payment->reference.'</td>  
                            </tr>';
                        }
                   }

         $customPaymentstxt.='</table>
            <style>
                table.insidetext{
                  border:none;
                  width:100%;
                }
                td.insidetexttd{
                   border-left:none;
                   border-right:none;
                   border-top:none;
                   border-bottom:none;
                   font-size:9px;

                }

              </style>  
        ';
        $commentSection='<table cellspacing="0" cellpadding="1" border="0"  class="commenttable">
        <tr>
        <td style="width:100%;height:70px;">'.$customText.'</td>
        </tr>
        <tr>
        <td style="width:100%;border:none;"></td>
        </tr>
        <tr>
        <td style="width:100%;border:none;"><b>Payments : </b></td>
        </tr>
        <tr>
        <td style="width:100%;text-align:center;border:none;">'.$customPaymentstxt.'</td>
        </tr>
        </table>
           <style>
                table.commenttable{
                     
                     
                     table-layout: auto;
                     width: 100%;  
                   
                 }
              
                table.commenttable tr td{          
                  border:1px solid #000000;
                   text-align:left;
                   font-size:9px;
                  
                 
                 }
              </style>   
        ';

        $combinedSection='<table  class="combinedtable">
        <tr>
        <td style="width:62.7%;">'.$commentSection.'</td>
        <td style="width:8%;"></td>
        <td style="width:28.8%;">'.$pricevaluessection.'</td>
        </tr>
        </table>
          <style>
                table.combinedtable{
                     table-layout: auto;
                     width: 100%;  
                     border:none;
                   
                 }
              
                table.combinedtable tr td{          
                  border:none;
                 
                 }
              </style>  
        ';
      
        $pdf->setFont('helvetica','',9);
        $pdf->writeHTMLCell(191,'','','',$combinedSection,0,1);

        //
        
        return array('pdf' => $pdf,'fileName' => $fileName);
 }



function createinvoicenotificationpdf($id,$booking_id){

$CI = get_instance();
$CI->load->model('bookings_config_model');
$CI->load->model('quotes_model');
$CI->load->model('invoice_model');
$CI->load->model('bookings_model');

 $invoicenotificationsdata=$CI->invoice_model->getsingleremindernotification('vbs_invoices_notification',['id' => $id]);
 $notificationsdata = $CI->invoice_model->getremindersnotification('vbs_notifications',['id' => $invoicenotificationsdata->notification_id]);
  
 $dbbookingdata=$CI->bookings_model->getdbbookingrecord('vbs_bookings',array('id'=>$booking_id));
 $invoices=$CI->bookings_model->getdbbookingrecord('vbs_invoices',array('booking_id'=>$booking_id));
 $companydata=$CI->bookings_model->getcompanystatus();
 $companycity=$CI->invoice_model->getsinglerecord('vbs_cities',['id'=>$companydata->city])->name;
 $client=$CI->bookings_model->getclientdatabyid($dbbookingdata->user_id);
 $clientcity=$CI->bookings_model->getsinglerecord('vbs_cities',['id'=>$client->city])->name;
 $user = $CI->basic_auth->user();
 $CI->load->model('userx_model');
 $user_data = $CI->userx_model->get(['user.id' => $user->id]);
$CI->load->library('bookpdf');
$pdf = new bookpdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
  $address2=(!empty($companydata->address2))?trim($companydata->address2).', ' :'';
$companydatainfo=strtoupper($companydata->name).' '.strtoupper($companydata->type).', '.$companydata->address.', '.$address2.$companydata->zip_code.', '.$companycity.', Capital : '.$companydata->capital.', License N : '.$companydata->licence.', SIRET : '.$companydata->sirft.', VAT N<sup>o</sup> : '.$companydata->numero_tva;
$pdf->setInfo($companydatainfo);
$fileName='Upaid Invoice Notification '.create_timestampdmy_uid($invoices->date,$invoices->id).' '.$client->civility.' '.$client->first_name.' '.$client->last_name;
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle($fileName.".PDF");
$pdf->SetSubject($fileName.".PDF");
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setPrintHeader(false);
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);



$pdf->addPage();



/* Company information */
$cominfo1="<span><b>".strtoupper($companydata->name).' '.strtoupper($companydata->type)."</b></span><br><span>".$companydata->address."</span><br>";
if(!empty($companydata->address2)){
  $cominfo1.="<span>".$companydata->address2."</span><br>";
}
$cominfo1.="<span>".$companydata->zip_code.' '.$companycity."</span><br><span>Phone&nbsp;&nbsp;&nbsp;&nbsp; : ".$companydata->phone."</span><br><span>Fax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ".$companydata->fax."</span><br><span>Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: ".$companydata->email."</span><br><span>Website&nbsp;&nbsp;: ".$companydata->website."</span>";
/* Company information */


/* Client information */
$clientinfo="<b>&nbsp;".$client->civility.' '.$client->first_name.' '.$client->last_name."</b><br>&nbsp;".$client->address."<br>&nbsp;";
if(!empty($client->address1)){
  $clientinfo.=$client->address1."<br>&nbsp;";
}
$clientinfo.=$client->zipcode.'  '.$clientcity;
/* Client information */







/* Company Logo */
$companylogo=base_url().'uploads/company/'.$companydata->logo;
$companylogo = '<img src="'.$companylogo.'" width="110" height="95"/>';
/* Company Logo */

/* Set Company/Client/Invoice Information in Table */
$html='<table cellspacing="0" cellpadding="0" >
<tr>
          <td style="width:33.3%;">'.$companylogo.'</td>
          <td style="width:33.3%;"></td>
          <td style="width:33.3%;"></td>
        </tr></table>';
$html .='<table cellspacing="0" cellpadding="0" >
       

        <tr>
          <td style="width:33.3%;">'.$cominfo1.'</td>
          <td style="width:33.3%;"></td>
         <td style="width:33.3%;"><div class="test" style="page-break-inside: avoid;"><table cellspacing="0" cellpadding="6" ><tr><td>'.$clientinfo.'</td></tr></table></div>


          <style>   
              div.test {
                
                  border:0.1em  solid #000000;  

              }
          </style></td> 
        </tr> 

        <tr>
          <td></td>
          <td></td>
          <td>Done at : '.$companycity.'&nbsp; on : '.from_unix_date($invoicenotificationsdata->date).'</td>     
        </tr>        
     </table>
     <style>
        table{
             border-collapse:collapse;
             table-layout: auto;
             width: 100%;    
         }
      
         th{
            border:1px solid #000000;
            text-align:center;
            font-size:10px !important;
         }
        
         td{
           
           text-align:left;
          
         
         }

       
        
     </style>
';
/* Set Company/Client/Invoice Information in Table */
$message=str_replace("{client_civility}",$client->civility,$notificationsdata->message);
$message=str_replace("{client_firstname}",$client->first_name,$message);
$message=str_replace("{client_lastname}",$client->last_name,$message);
$message=str_replace("{invoice_id}",create_timestampdmy_uid($invoices->date,$invoices->id),$message);
$message=str_replace("{invoice_date}",from_unix_date($invoices->date),$message);




$message="<h4>Subject : ".$notificationsdata->subject."</h4>".$message ;
    
$notificationtitle="UPAID INVOICE NOTIFICATION";
$pdf->setFont('helveticaB','',12);
$pdf->cell(30,10,'',0,0,'C');
$pdf->cell(132,10,$notificationtitle,1,0,'C');
$pdf->cell(30,10,'',0,1,'C');
$pdf->Ln(4);
//Set Reminder Message


 

$pdf->setFont('helvetica','',11);
$pdf->setCellPaddings( $left = 1, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 3);
$pdf->writeHTMLCell(190,0,10,'',$html,0,1);




//Set Notification Message
$pdf->setCellPaddings( $left = 1, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 0);
$pdf->setFont('helvetica','',11);

$pdf->writeHTMLCell(190,'','','',$message,0,1);
 $positiony = $pdf->GetY();
//$pdf->cell(192,2,$positiony,1,1,'C');

//signature box

$pdf->setCellPaddings( $left = 0, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 0);
/*admin logo */
$adminsignaturelogo=base_url().'uploads/user/'.$user_data['document_signature'];
$adminsignaturelogo = '<img src="'.$adminsignaturelogo.'" width="110" height="95"/>';
/*admin logo*/
$userdeparment=$user_data['department'];
$username='<span>'.$user_data['civility'].' '.$user_data['first_name'].' '.$user_data['last_name'].'</span>';
$stampsection='<table cellspacing="2" cellpadding="1" border="0" nobr="true">
<tr>
<th style="width:67%;"></th>
<th style="width:33%;"><b>'.$userdeparment.' Department</b><br>'.$username.'<br>Signature and Stamp</th>
</tr>

<tr>
<td style="border:none;"></td>
<td  style="border-top:none;">'.$adminsignaturelogo.'</td>
</tr>
</table>
   <style>
        table{
             border-collapse:collapse;
             table-layout: auto;
             width: 100%;  
         }
         th{
           border:none; 
           text-align:left;
         }
         td{   
         border:0.1em  solid #000000; 
          text-align:center; 
         }
      </style>   
';

$pdf->setFont('helvetica','',10);
if($positiony < 223){
  $pdf->SetY(-75);
  $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}
elseif($positiony < 263){
   $pdf->Ln(45);
   $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}
else{
   $pdf->Ln(5);
   $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}

//signature box





return array('pdf' => $pdf,'fileName' => $fileName);
}

function createinvoicereminderpdf($id,$level,$booking_id){
      $CI = get_instance();
        $CI->load->model('bookings_config_model');
        $CI->load->model('quotes_model');
        $CI->load->model('invoice_model');
        $CI->load->model('bookings_model');

        $invoicereminderndata=$CI->invoice_model->getsingleremindernotification('vbs_invoices_reminder',['id' => $id]);
        $remindersdata = $CI->invoice_model->getremindersnotification('vbs_reminders',['id' => $invoicereminderndata->reminder_id]);
        $dbbookingdata=$CI->bookings_model->getdbbookingrecord('vbs_bookings',array('id'=>$booking_id));
        $invoices=$CI->bookings_model->getdbbookingrecord('vbs_invoices',array('booking_id'=>$booking_id));
        $companydata=$CI->bookings_model->getcompanystatus();
        $companycity=$CI->invoice_model->getsinglerecord('vbs_cities',['id'=>$companydata->city])->name;
        $client=$CI->bookings_model->getclientdatabyid($dbbookingdata->user_id);
        $clientcity=$CI->bookings_model->getsinglerecord('vbs_cities',['id'=>$client->city])->name;
        $level=$level;
        $user = $CI->basic_auth->user();
        $CI->load->model('userx_model');
        $user_data = $CI->userx_model->get(['user.id' => $user->id]);

        $CI->load->library('bookpdf');
        $pdf = new bookpdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        //create pdf
        $address2=(!empty($companydata->address2))?trim($companydata->address2).', ' :'';
$companydatainfo=strtoupper($companydata->name).' '.strtoupper($companydata->type).', '.$companydata->address.', '.$address2.$companydata->zip_code.', '.$companycity.', Capital : '.$companydata->capital.', License N : '.$companydata->licence.', SIRET : '.$companydata->sirft.', VAT N<sup>o</sup> : '.$companydata->numero_tva;
$pdf->setInfo($companydatainfo);
 if($level == 1){
  $remindertype="First";
}
elseif($level == 2){
  $remindertype="Second";
}
elseif($level == 3){
  $remindertype="Third";
}

$fileName='Upaid Invoice '.$remindertype.' Reminder '.create_timestampdmy_uid($invoices->date,$invoices->id).' '.$client->civility.' '.$client->first_name.' '.$client->last_name;
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle($fileName.".PDF");
$pdf->SetSubject($fileName.".PDF");
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setPrintHeader(false);
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);



$pdf->addPage();



/* Company information */
$cominfo1="<span><b>".strtoupper($companydata->name).' '.strtoupper($companydata->type)."</b></span><br><span>".$companydata->address."</span><br>";
if(!empty($companydata->address2)){
  $cominfo1.="<span>".$companydata->address2."</span><br>";
}
$cominfo1.="<span>".$companydata->zip_code.' '.$companycity."</span><br><span>Phone&nbsp;&nbsp;&nbsp;&nbsp; : ".$companydata->phone."</span><br><span>Fax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ".$companydata->fax."</span><br><span>Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: ".$companydata->email."</span><br><span>Website&nbsp;&nbsp;: ".$companydata->website."</span>";
/* Company information */


/* Client information */
$clientinfo="<b>&nbsp;".$client->civility.' '.$client->first_name.' '.$client->last_name."</b><br>&nbsp;".$client->address."<br>&nbsp;";
if(!empty($client->address1)){
  $clientinfo.=$client->address1."<br>&nbsp;";
}
$clientinfo.=$client->zipcode.'  '.$clientcity;
/* Client information */







/* Company Logo */
$companylogo=base_url().'uploads/company/'.$companydata->logo;
$companylogo = '<img src="'.$companylogo.'" width="110" height="95"/>';
/* Company Logo */

/* Set Company/Client/Invoices Information in Table */
$html='<table cellspacing="0" cellpadding="0" >
<tr>
          <td style="width:35%;">'.$companylogo.'</td>
          <td style="width:30%;"></td>
          <td style="width:35%;"></td>
        </tr></table>';
$html .='<table cellspacing="0" cellpadding="0" >
       

        <tr>
          <td style="width:35%;">'.$cominfo1.'</td>
          <td style="width:30%;"></td>
        <td style="width:33.3%;"><div class="test" style="page-break-inside: avoid;"><table cellspacing="0" cellpadding="6" ><tr><td>'.$clientinfo.'</td></tr></table></div>


          <style>   
              div.test {
                
                   border:0.1em  solid #000000; 

              }
          </style></td>   
        </tr> 

        <tr>
          <td></td>
          <td></td>
          <td>Done at : '.$companycity.'&nbsp; on : '.from_unix_date($invoicereminderndata->date).'</td>     
        </tr>        
     </table>
     <style>
        table{
             border-collapse:collapse;
             table-layout: auto;
             width: 100%;    
         }
      
         th{
            border:1px solid #000000;
            text-align:center;
            font-size:10px !important;
         }
        
         td{
           
           text-align:left;
          
         
         }

       
        
     </style>
';
/* Set Company/Client/Invoices Information in Table */
$message=str_replace("{client_civility}",$client->civility,$remindersdata->message);
$message=str_replace("{client_firstname}",$client->first_name,$message);
$message=str_replace("{client_lastname}",$client->last_name,$message);
$message=str_replace("{invoice_id}",create_timestampdmy_uid($invoices->date,$invoices->id),$message);
$message=str_replace("{invoice_date}",from_unix_date($invoices->date),$message);
//Set Reminder Message 
$message="<h4>Subject : ".$remindersdata->subject."</h4>".$message;
    
              
$remindertitle="UPAID INVOICE ".strtoupper($remindertype)." REMINDER";
$pdf->setFont('helveticaB','',12);
$pdf->cell(30,10,'',0,0,'C');
$pdf->cell(132,10,$remindertitle,1,0,'C');
$pdf->cell(30,10,'',0,1,'C');
$pdf->Ln(4);
//Set Reminder Message


 

$pdf->setFont('helvetica','',11);
$pdf->setCellPaddings( $left = 1, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 3);
$pdf->writeHTMLCell(190,0,10,'',$html,0,1);




//Set Notification Message
$pdf->setCellPaddings( $left = 1, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 0);
$pdf->setFont('helvetica','',11);

$pdf->writeHTMLCell(190,'','','',$message,0,1);
 $positiony = $pdf->GetY();
//$pdf->cell(192,2,$positiony,1,1,'C');

//signature box

$pdf->setCellPaddings( $left = 0, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 0);
/*admin logo */
$adminsignaturelogo=base_url().'uploads/user/'.$user_data['document_signature'];
$adminsignaturelogo = '<img src="'.$adminsignaturelogo.'" width="110" height="95"/>';
/*admin logo*/
$userdeparment=$user_data['department'];
$username='<span>'.$user_data['civility'].' '.$user_data['first_name'].' '.$user_data['last_name'].'</span>';
$stampsection='<table cellspacing="2" cellpadding="1" border="0" nobr="true">
<tr>
<th style="width:67%;"></th>
<th style="width:33%;"><b>'.$userdeparment.' Department</b><br>'.$username.'<br>Signature and Stamp</th>
</tr>

<tr>
<td style="border:none;"></td>
<td  style="border-top:none;">'.$adminsignaturelogo.'</td>
</tr>
</table>
   <style>
        table{
             border-collapse:collapse;
             table-layout: auto;
             width: 100%;  
         }
         th{
           border:none; 
           text-align:left;
         }
         td{   
        border:0.1em  solid #000000; 
          text-align:center; 
         }
      </style>   
';

$pdf->setFont('helvetica','',10);
if($positiony < 223){
  $pdf->SetY(-75);
  $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}
elseif($positiony < 263){
   $pdf->Ln(45);
   $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}
else{
   $pdf->Ln(5);
   $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}

//signature box



        //create pdf

 return array('pdf' => $pdf,'fileName' => $fileName);
}


 /* PDF FOR INVOICE MODULE END*/

 /* PDF FOR BOOKING MODULE START */
 function createbookingnotificationpdf($id,$booking_id){
  $CI = get_instance();
  $CI->load->model('bookings_config_model');
  $CI->load->model('quotes_model');
  $CI->load->model('invoice_model');
  $CI->load->model('bookings_model');

   $dbquotesdata = $CI->bookings_config_model->getpricestatusdata('vbs_price_status');

  $bookingnotificationsdata=$CI->bookings_model->getnotification('vbs_bookings_notification',['id' => $id]);

  $notificationsdata = $CI->bookings_model->getnotification('vbs_notifications',['id' => $bookingnotificationsdata->notification_id]);
  
  $dbbookingdata=$CI->bookings_model->getdbbookingrecord('vbs_bookings',array('id'=>$booking_id));
 
  $companydata=$CI->bookings_model->getcompanystatus();

  $companycity=$CI->bookings_model->getsinglerecord('vbs_cities',['id'=>$companydata->city])->name;

  $client=$CI->bookings_model->getclientdatabyid($dbbookingdata->user_id);
  $clientcity=$CI->bookings_model->getsinglerecord('vbs_cities',['id'=>$client->city])->name;

  $user = $CI->basic_auth->user();

  $CI->load->model('userx_model');
  $user_data = $CI->userx_model->get(['user.id' => $user->id]);

  $CI->load->library('bookpdf');
  $pdf = new bookpdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

     $address2=(!empty($companydata->address2))?trim($companydata->address2).', ' :'';
  $companydatainfo=strtoupper($companydata->name).' '.strtoupper($companydata->type).', '.$companydata->address.', '.$address2.$companydata->zip_code.', '.$companycity.', Capital : '.$companydata->capital.', License N : '.$companydata->licence.', SIRET : '.$companydata->sirft.', VAT N<sup>o</sup> : '.$companydata->numero_tva;
  $pdf->setInfo($companydatainfo);
$fileName=create_timestampdmy_uid($dbbookingdata->bookdate,$dbbookingdata->id).' '.$client->civility.' '.$client->first_name.' '.$client->last_name.' Notification';
  // set document information
  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('Nicola Asuni');
  $pdf->SetTitle($fileName.".PDF");
  $pdf->SetSubject($fileName.".PDF");
  $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

  // set default header data
  //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

  // set header and footer fonts
  $pdf->setPrintHeader(false);
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
  // set margins
  //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
  $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
  $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);



  $pdf->addPage();



  /* Company information */
  $cominfo1="<span><b>".strtoupper($companydata->name).' '.strtoupper($companydata->type)."</b></span><br><span>".$companydata->address."</span><br>";
  if(!empty($companydata->address2)){
    $cominfo1.="<span>".$companydata->address2."</span><br>";
  }
  $cominfo1.="<span>".$companydata->zip_code.' '.$companycity."</span><br><span>Phone&nbsp;&nbsp;&nbsp;&nbsp; : ".$companydata->phone."</span><br><span>Fax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ".$companydata->fax."</span><br><span>Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: ".$companydata->email."</span><br><span>Website&nbsp;&nbsp;: ".$companydata->website."</span>";
  /* Company information */


  /* Client information */
  $clientinfo="<b>&nbsp;".$client->civility.' '.$client->first_name.' '.$client->last_name."</b><br>&nbsp;".$client->address."<br>&nbsp;";
  if(!empty($client->address1)){
    $clientinfo.=$client->address1."<br>&nbsp;";
  }
  $clientinfo.=$client->zipcode.'  '.$clientcity;
  /* Client information */


/* Pending/ confirmed Logo */
$pendingconfirmedicon='';
if($dbbookingdata->is_conformed == 0){
$pendingconfirmedicon=base_url().$dbquotesdata->bookingstatutpendingicon;
$pendingconfirmedicon = '<img src="'.$pendingconfirmedicon.'" width="'.$dbquotesdata->bookingpendingiconwidth.'" height="'.$dbquotesdata->bookingpendingiconheight.'" />';
}
elseif($dbbookingdata->is_conformed == 1){
  $pendingconfirmedicon=base_url().$dbquotesdata->bookingstatutconfirmedicon;
  $pendingconfirmedicon = '<img src="'.$pendingconfirmedicon.'" width="'.$dbquotesdata->bookingconfirmediconwidth.'" height="'.$dbquotesdata->bookingconfirmediconheight.'" />';
}

elseif($dbbookingdata->is_conformed == 2){
  $pendingconfirmedicon=base_url().$dbquotesdata->bookingstatutcancelledicon;
  $pendingconfirmedicon = '<img src="'.$pendingconfirmedicon.'" width="'.$dbquotesdata->bookingcancellediconwidth.'" height="'.$dbquotesdata->bookingcancellediconheight.'" />';

}
else{
$pendingconfirmedicon=base_url().$dbquotesdata->bookingstatutpendingicon;
$pendingconfirmedicon = '<img src="'.$pendingconfirmedicon.'" width="'.$dbquotesdata->bookingpendingiconwidth.'" height="'.$dbquotesdata->bookingpendingiconheight.'" />';
}

/* Pending/ confirmed Logo */




  /* Company Logo */
  $companylogo=base_url().'uploads/company/'.$companydata->logo;
  $companylogo = '<img src="'.$companylogo.'" width="110" height="95"/>';
  /* Company Logo */


  $html='<table cellspacing="0" cellpadding="0" >
  <tr>
            <td style="width:33.3%;">'.$companylogo.'</td>
            <td style="width:33.3%;"></td>
            <td style="width:33.3%;"></td>
          </tr></table>';
  $html .='<table cellspacing="0" cellpadding="0" >
         

          <tr>
            <td style="width:33.3%;">'.$cominfo1.'</td>
            <td style="width:33.3%;"></td>
           <td style="width:33.3%;"><div class="test" style="page-break-inside: avoid;"><table cellspacing="0" cellpadding="6" ><tr><td>'.$clientinfo.'</td></tr></table></div>


            <style>   
                div.test {
                  
                    border:0.1em solid black;  

                }
            </style>
            <table cellspacing="0" cellpadding="1" ><tr><td>'.$pendingconfirmedicon.'</td></tr></table>
            </td> 
          </tr> 
          <tr>
            <td></td>
            <td></td>
            <td>Done at : '.$companycity.'&nbsp; on : '.from_unix_date($bookingnotificationsdata->date).'</td>     
          </tr>        
       </table>
       <style>
          table{
               border-collapse:collapse;
               table-layout: auto;
               width: 100%;    
           }
        
           th{
              border:1px solid #000000;
              text-align:center;
              font-size:10px !important;
           }
          
           td{
             
             text-align:left;
            
           
           }

         
          
       </style>
  ';

$message=str_replace("{client_civility}",$client->civility,$notificationsdata->message);
$message=str_replace("{client_firstname}",$client->first_name,$message);
$message=str_replace("{client_lastname}",$client->last_name,$message);
$message=str_replace("{booking_id}",create_timestampdmy_uid($dbbookingdata->bookdate,$dbbookingdata->id),$message);
$message=str_replace("{booking_date}",from_unix_date($dbbookingdata->bookdate),$message);

  //Set Reminder Message 
  $message="<h4>Subject : ".$notificationsdata->subject."</h4>".$message;
      
  $notificationtitle="BOOKING NOTIFICATION";
  $pdf->setFont('helveticaB','',12);
  $pdf->cell(30,10,'',0,0,'C');
  $pdf->cell(132,10,$notificationtitle,1,0,'C');
  $pdf->cell(30,10,'',0,1,'C');
  $pdf->Ln(4);
  //Set Reminder Message


   

  $pdf->setFont('helvetica','',11);
  $pdf->setCellPaddings( $left = 1, $top = 0, $right =0, $bottom = 0);
  $pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 3);
  $pdf->writeHTMLCell(190,0,10,'',$html,0,1);




  //Set Notification Message
  $pdf->setCellPaddings( $left = 1, $top = 0, $right =0, $bottom = 0);
  $pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 3);
  $pdf->setFont('helvetica','',11);

  $pdf->writeHTMLCell(190,'','','',$message,0,1);

$bookdetailarray=getbookingdetails($booking_id,$CI);
$bookinginformation=$bookdetailarray['bookinginformation'];

$fullbookinginformation=getbookingnotificationdetails($bookinginformation,$booking_id,$CI);

$pdf->setFont('helvetica','',9);
$pdf->setCellPaddings( $left = 0, $top = 0, $right = 0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 6);
$pdf->writeHTMLCell(190,0,10,'',$fullbookinginformation,0,1);

   $positiony = $pdf->GetY();
  //$pdf->cell(192,2,$positiony,1,1,'C');

  //signature box

  $pdf->setCellPaddings( $left = 0, $top = 0, $right =0, $bottom = 0);
  $pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 0);
  /*admin logo */
  $adminsignaturelogo=base_url().'uploads/user/'.$user_data['document_signature'];
  $adminsignaturelogo = '<img src="'.$adminsignaturelogo.'" width="110" height="95"/>';
  /*admin logo*/
  $userdeparment=$user_data['department'];
  $username='<span>'.$user_data['civility'].' '.$user_data['first_name'].' '.$user_data['last_name'].'</span>';
  $stampsection='<table cellspacing="2" cellpadding="1" border="0" nobr="true">
  <tr>
  <th style="width:67%;"></th>
  <th style="width:33%;"><b>'.$userdeparment.' Department</b><br>'.$username.'<br>Signature and Stamp</th>
  </tr>

  <tr>
  <td style="border:none;"></td>
  <td  style="border-top:none;">'.$adminsignaturelogo.'</td>
  </tr>
  </table>
     <style>
          table{
               border-collapse:collapse;
               table-layout: auto;
               width: 100%;  
           }
           th{
             border:none; 
             text-align:left;
           }
           td{   
            border:0.1em  solid #000000;
            text-align:center; 
           }
        </style>   
  ';

  $pdf->setFont('helvetica','',10);
  if($positiony < 223){
    $pdf->SetY(-75);
    $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
  }
  elseif($positiony < 263){
     $pdf->Ln(45);
     $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
  }
  else{
     $pdf->Ln(5);
     $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
  }

  //signature box




  

  return array('pdf' => $pdf,'fileName' => $fileName);
 }



 function getbookingdetails($booking_id,$CI){

  $CI->load->model('bookings_config_model');
  $CI->load->model('quotes_model');
  $CI->load->model('invoice_model');
  $CI->load->model('bookings_model');

$dbbookingdata=$CI->bookings_model->getdbbookingrecord('vbs_bookings',array('id'=>$booking_id));

$dbpickairportdata=$CI->bookings_model->getdbbookingrecord('vbs_booking_airport',array('booking_id'=>$booking_id,'type'=>'pickup'));
$dbpicktraindata=$CI->bookings_model->getdbbookingrecord('vbs_booking_train',array('booking_id'=>$booking_id,'type'=>'pickup'));
$dbpickaddressdata=$CI->bookings_model->getdbbookingrecord('vbs_booking_address',array('booking_id'=>$booking_id,'type'=>'pickup'));

 $dbdropairportdata=$CI->bookings_model->getdbbookingrecord('vbs_booking_airport',array('booking_id'=>$booking_id,'type'=>'dropoff'));
$dbdroptraindata=$CI->bookings_model->getdbbookingrecord('vbs_booking_train',array('booking_id'=>$booking_id,'type'=>'dropoff'));
$dbdropaddressdata=$CI->bookings_model->getdbbookingrecord('vbs_booking_address',array('booking_id'=>$booking_id,'type'=>'dropoff'));
$dbpickregulardata=$CI->bookings_model->getdbmultiplebookingrecord('vbs_booking_regschedule',array('booking_id'=>$booking_id,'type'=>'regular'));
$dbreturndata=$CI->bookings_model->getdbmultiplebookingrecord('vbs_booking_regschedule',array('booking_id'=>$booking_id,'type'=>'return'));
$dbstopsdata=$CI->bookings_model->getdbmultiplebookingrecord('vbs_booking_stops',array('booking_id'=>$booking_id));
$dbpassengers=$CI->bookings_model->getdbpassengersbookingrecord(array('booking_id'=>$booking_id));
$dbdiscountandfeedata=$CI->bookings_model->getdbbookingrecord('vbs_bookings_pricevalues',array('booking_id'=>$booking_id));
$dbcreditcarddata=$CI->bookings_model->getdbbookingrecord('vbs_booking_creditcard',array('booking_id'=>$booking_id));
$dbdebitbankdata=$CI->bookings_model->getdbbookingrecord('vbs_booking_directdebit',array('booking_id'=>$booking_id));

  $car_configuration_data= $CI->bookings_model->booking_Configuration('vbs_car_configuration',['car_id' =>$dbbookingdata->car_id]);
 $servicecategory= $CI->bookings_model->getservicesbyid('vbs_u_category_service',$dbbookingdata->service_category_id);
$servicecategoryname= $servicecategory->category_name;

 $service= $CI->bookings_model->getservicesbyid('vbs_u_service',$dbbookingdata->service_id);
$servicename= $service->service_name;


/* Set Booking Information in Table */
$ridedate='';
$ridetime='';
$start_date=date('d/m/Y');
$end_date=date('d/m/Y');
$start_time=date('h : i');
$end_time=date('h : i');
$ispickmonday='No Ride';
$ispicktuesday='No Ride';
$ispickwednesday='No Ride'; 
$ispickthursday='No Ride';
$ispickfriday='No Ride';
$ispicksaturday='No Ride';
$ispicksunday='No Ride';

$isreturnmonday='No Ride';
$isreturntuesday='No Ride';
$isreturnwednesday='No Ride'; 
$isreturnthursday='No Ride';
$isreturnfriday='No Ride';
$isreturnsaturday='No Ride';
$isreturnsunday='No Ride';



//waiting time
$waiting_time='';
     if(!empty($dbbookingdata->waiting_time) || $dbbookingdata->waiting_time != null){   
       $waiting_time= $dbbookingdata->waiting_time;
     }
       if($dbbookingdata->returncheck == 1 && $dbbookingdata->regular != 1 ){
                    $waitarray=explode(":", $waiting_time);
                 if($waitarray[0] > 1 && $waitarray[1] > 1){
                  $waiting_time = $waitarray[0]." hours and ".$waitarray[1]." minutes ";
                 }
                 elseif($waitarray[0] > 1 && $waitarray[1] <= 1){
                    $waiting_time = $waitarray[0]." hours and ".$waitarray[1]." minute ";
                 }
                  elseif($waitarray[0] <= 1 && $waitarray[1] > 1){
                    $waiting_time = $waitarray[0]." hour and ".$waitarray[1]." minutes ";
                 }
                 else{
                   $waiting_time = $waitarray[0]." hour and ".$waitarray[1]." minute ";
                 }
       }
//waiting time
//return date and time
       $return_date=date('d/m/Y');
       $return_time=date('h : i');
        if(!empty($dbbookingdata->return_time) || $dbbookingdata->return_time != null){  
          $return_time= $dbbookingdata->return_time;
         }
       if(!empty($dbbookingdata->return_date) || $dbbookingdata->return_date != null){ 
         $time = strtotime($dbbookingdata->return_date);
         $newformat = date('d/m/Y',$time);
         $return_date= $newformat;
       }
//return date and time
if($dbbookingdata->regular == 1){
 
       if(!empty($dbbookingdata->start_date) || $dbbookingdata->start_date != null){ 
         $time = strtotime($dbbookingdata->start_date);
         $newformat = date('d/m/Y',$time);
         $start_date= $newformat;
       }
       if(!empty($dbbookingdata->end_date) || $dbbookingdata->end_date != null){ 
         $time = strtotime($dbbookingdata->end_date);
         $newformat = date('d/m/Y',$time);
         $end_date= $newformat;
       }
       if(!empty($dbbookingdata->start_time) || $dbbookingdata->start_time != null){   
       $start_time= $dbbookingdata->start_time;
      }
       if(!empty($dbbookingdata->end_time) || $dbbookingdata->end_time != null){  
       $end_time= $dbbookingdata->end_time;
      }
        foreach($dbpickregulardata as $regular){
        if(strtolower($regular->day)=='monday'){
             $ispickmonday=$regular->time;
           }
          elseif(strtolower($regular->day)=='tuesday'){
             $ispicktuesday=$regular->time;
           }
          elseif(strtolower($regular->day)=='wednesday'){
             $ispickwednesday=$regular->time;
           }
          elseif(strtolower($regular->day)=='thursday'){
             $ispickthursday=$regular->time;
           }
          elseif(strtolower($regular->day)=='friday'){
             $ispickfriday=$regular->time;
           }
          elseif(strtolower($regular->day)=='saturday'){
             $ispicksaturday=$regular->time;
           }
          elseif(strtolower($regular->day)=='sunday'){
             $ispicksunday=$regular->time;
           }         
      }

  if($dbbookingdata->returncheck ==1){
    foreach($dbreturndata as $return){
        if(strtolower($return->day)=='monday'){
             $isreturnmonday=$return->time;
           }
          elseif(strtolower($return->day)=='tuesday'){
             $isreturntuesday=$return->time;
           }
          elseif(strtolower($return->day)=='wednesday'){
             $isreturnwednesday=$return->time;
           }
          elseif(strtolower($return->day)=='thursday'){
             $isreturnthursday=$return->time;
           }
          elseif(strtolower($return->day)=='friday'){
             $isreturnfriday=$return->time;
           }
          elseif(strtolower($return->day)=='saturday'){
             $isreturnsaturday=$return->time;
           }
          elseif(strtolower($return->day)=='sunday'){
             $isreturnsunday=$return->time;
           }         
      }
  }
}
 if(!empty($dbbookingdata->pick_date)){
 $pickdate=from_unix_date($dbbookingdata->pick_date);
}
if(!empty($dbbookingdata->pick_time)){
  $picktime=$dbbookingdata->pick_time;
}
$pick_addr=str_replace($dbbookingdata->pickcity, '', $dbbookingdata->pick_point);
$pick_addr=str_replace($dbbookingdata->pickzipcode, '', $pick_addr);
$pick_addr=str_replace(',', '', $pick_addr);
$pick_addr=trim($pick_addr);
$pick_addr=$pick_addr.', '.$dbbookingdata->pickzipcode.', '.$dbbookingdata->pickcity;

$drop_addr=str_replace($dbbookingdata->dropcity, '', $dbbookingdata->drop_point);
$drop_addr=str_replace($dbbookingdata->dropzipcode, '', $drop_addr);
$drop_addr=str_replace(',', '', $drop_addr);
$drop_addr=trim($drop_addr);
$drop_addr=$drop_addr.', '.$dbbookingdata->dropzipcode.', '.$dbbookingdata->dropcity;
 

$pick_point='<span>'.$pick_addr.'</span>';
$drop_point='<span>'.$drop_addr.'</span>';
$services='<br><span><b>Service Category : </b>'.$servicecategoryname.'</span><span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     <b>Service : </b>'.$servicename.'</span>';
$passengers="<b>Passengers : </b><br><br>";
    foreach($dbpassengers as $p){
                       $passengers.='<span style="font-family:FontAwesome;">&#xf007;</span> <span>'.$p->civility.' '.$p->fname.' '.$p->lname.'</span>, <span style="font-family:FontAwesome;">&#xf10b;</span> <span>'.$p->mobilephone.'</span>, <span style="font-family:FontAwesome;">&#xf095;</span> <span>'.$p->homephone.'</span>, <span style="font-family:FontAwesome;">&#xf193;</span> <span>'.$p->name_of_disablecategory.'</span><br><br>';
                      
            
                     }
                     $passengers=preg_replace('/(<br>)+$/', '', $passengers);
$stops='<b>Stops : </b><br><br>';
foreach ($dbstopsdata as $stop){
    $waittime='';
            $waitarray = explode(":", $stop->stopwaittime);
             if($waitarray[0] > 1 && $waitarray[1] > 1){
               $waittime= $waitarray[0]." hours ".$waitarray[1]." minutes";
             }
             elseif($waitarray[0] < 2 && $waitarray[1] < 2){
               $waittime= $waitarray[0]." hour ".$waitarray[1]." minute";
             }
             elseif($waitarray[0] > 1 && $waitarray[1] < 2){
               $waittime= $waitarray[0]." hours ".$waitarray[1]." minute";
             }
             elseif($waitarray[0] < 2 && $waitarray[1] > 1){
               $waittime= $waitarray[0]." hour ".$waitarray[1]." minutes";
             }
  $stops.='<span><b>Addresse : </b>'.$stop->stopaddress.'</span><span>&nbsp;&nbsp;&nbsp; <b>Waiting Time : </b>'.$waittime.'</span><br><br>';
}
 $stops=preg_replace('/(<br>)+$/', '', $stops);

$cars='<b>Car Category : </b>'.$car_name."<br><br>";
 $configno=0;
   foreach ($car_configuration_data as $config){
     $configno++;
     $cars.='<span><b>Configuration '.$configno.' : </b></span>
     <span>'.$config->passengers.' Passengers,</span>
     <span>'.$config->luggage.' Luggage,</span>
     <span>'.$config->wheelchairs.' Wheelchairs,</span>
     <span>'.$config->baby.' Babys,</span>
     <span>'.$config->animals.' Animals</span><br>';
   }
   $cars=preg_replace('/(<br>)+$/', '', $cars);

$isregular= ($dbbookingdata->regular == 1)?"Yes":'No';
$isreturn= ($dbbookingdata->returncheck ==1)?"Yes":'No';
$iswheerchair= ($dbbookingdata->wheelchair == 1)?"Yes":'No';
$isquantity= ($dbbookingdata->returncheck ==1)?"2":'1';
if($dbbookingdata->regular == 1){
  $isquantity="1";
}
/* Set Booking Information in Table */



$htprice=($dbbookingdata->price/$isquantity);
$totalhtprice=$dbbookingdata->price;


//Inside booking table
$bookinginformation='<table cellspacing="0" cellpadding="3"  class="outside">
       
        <tr>
           <td class="outsidetd" style="text-align:left;"><b>Booking Date : </b>'.from_unix_date($dbbookingdata->bookdate).'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <b>Booking Time : </b>'.$dbbookingdata->booktime.'</td>     
        </tr>
         <tr>
          <td class="outsidetd" style="text-align:left;">'.$services.'</td>
                
        </tr>
        <tr>
          <td class="outsidetd" style="text-align:left;"><b>Recurrent : </b>'.$isregular.'&nbsp;&nbsp;&nbsp; <b>Return : </b>'.$isreturn.'&nbsp;&nbsp;&nbsp; <b>Wheelchair : </b>'.$iswheerchair.'</td>
                 
        </tr>
         <tr>
          <td class="outsidetd" style="text-align:left;">'.$passengers.'</td>
                 
        </tr>
       <tr>
          <td class="outsidetd" style="text-align:left;"><b>Pickup : </b></td>
                 
        </tr>
         <tr>
          <td class="outsidetd" style="text-align:left;"><table cellspacing="0" cellpadding="0"  class="outside">
<tr>
<td class="outsidetd" style="text-align:left;width:46%;">'.$pick_point.'</td>
<td class="outsidetd" style="text-align:left;width:54%;">';
   if($dbbookingdata->regular != 1 ){
      $bookinginformation.='<b>Pick Date : </b>'.$pickdate.'&nbsp;&nbsp; <b>Pick Time : </b>'.$picktime;
     }else{
      $bookinginformation.='<b>Start Date : </b>'.$start_date.'&nbsp;&nbsp; <b>Start Time : </b>'.$start_time;
     }
$bookinginformation.='</td>
</tr>
</table>
      <style>
        table.inside{
          border:none;
        }
        td.insidetd{
          border-left:none;
           border-right:none;
           font-size:8px;
        }

      </style></td>
                 
        </tr>
        <tr>
          <td class="outsidetd" style="text-align:left;"><b>Dropoff : </b></td>
                 
        </tr>
         <tr>
          <td class="outsidetd" style="text-align:left;"><table cellspacing="0" cellpadding="0"  class="outside">
<tr>
<td class="outsidetd" style="text-align:left;width:46%;">'.$drop_point.'</td>
<td class="outsidetd" style="text-align:left;width:54%;">';
     if($dbbookingdata->regular != 1 ){
      if($dbbookingdata->returncheck == 1){
      $bookinginformation.='<b>Return Date : </b>'.$return_date.'&nbsp;&nbsp; <b>Return Time : </b>'.$return_time;
      }
     }else{
      $bookinginformation.='<b>End Date : </b>'.$end_date.'&nbsp;&nbsp; <b>End Time : </b>'.$end_time;
     }
$bookinginformation.='</td>
</tr>
</table>
      <style>
        table.inside{
          border:none;
        }
        td.insidetd{
          border-left:none;
           border-right:none;
           font-size:8px;
        }

      </style></td>
                 
        </tr>
         <tr>
          <td class="outsidetd" style="text-align:left;">'.$stops.'</td>
                 
        </tr>';
      
        if($dbbookingdata->regular == 1){
          $bookinginformation.='<tr>
          <td class="outsidetd" style="text-align:left;"><table  cellspacing="0" cellpadding="0" class="inside">
        <tr>
          <td style="width:17%;" class="insidetd"><b></b></td>
          <td style="width:10%;" class="insidetd"><b>Monday</b></td>
          <td style="width:12%;" class="insidetd"><b>Tuesday</b></td>
          <td style="width:16%;" class="insidetd"><b>Wednesday</b></td>
          <td style="width:13%;" class="insidetd"><b>Thursday</b></td>
          <td style="width:10%;" class="insidetd"><b>Friday</b></td>
          <td style="width:12%;" class="insidetd"><b>Saturday</b></td>
          <td style="width:10%;" class="insidetd"><b>Sunday</b></td>
        </tr>
        <tr>
           <td style="width:17%;" class="insidetd"></td>
           <td style="width:10%;" class="insidetd"></td>
           <td style="width:12%;" class="insidetd"></td>
           <td style="width:16%;" class="insidetd"></td>
           <td style="width:13%;" class="insidetd"></td>
           <td style="width:10%;" class="insidetd"></td>
           <td style="width:12%;" class="insidetd"></td>
           <td style="width:10%;" class="insidetd"></td>
         </tr>
        <tr>
          <td style="text-align:left;"  class="insidetd"><b>PickUp Time</b></td>
          <td class="insidetd">'.$ispickmonday.'</td>
          <td class="insidetd">'.$ispicktuesday.'</td>
          <td class="insidetd">'.$ispickwednesday.'</td>
          <td class="insidetd">'.$ispickthursday.'</td>
          <td class="insidetd">'.$ispickfriday.'</td>
          <td class="insidetd">'.$ispicksaturday.'</td>
          <td class="insidetd">'.$ispicksunday.'</td>
        </tr>';
        if($dbbookingdata->returncheck ==1){
           $bookinginformation.='<tr>
          <td style="width:17%;" class="insidetd"></td>
          <td style="width:10%;" class="insidetd"></td>
          <td style="width:12%;" class="insidetd"></td>
          <td style="width:16%;" class="insidetd"></td>
          <td style="width:13%;" class="insidetd"></td>
          <td style="width:10%;" class="insidetd"></td>
          <td style="width:12%;" class="insidetd"></td>
          <td style="width:10%;" class="insidetd"></td>
        </tr>
        <tr>
          <td style="text-align:left;" class="insidetd"><b>Return Time</b></td>
          <td class="insidetd">'.$isreturnmonday.'</td>
          <td class="insidetd">'.$isreturntuesday.'</td>
          <td class="insidetd">'.$isreturnwednesday.'</td>
          <td class="insidetd">'.$isreturnthursday.'</td>
          <td class="insidetd">'.$isreturnfriday.'</td>
          <td class="insidetd">'.$isreturnsaturday.'</td>
          <td class="insidetd">'.$isreturnsunday.'</td>
        </tr>';
        }
      $bookinginformation.='</table>
      <style>
        table.inside{
          border:none;
        }
        td.insidetd{
          border-left:none;
           border-right:none;
           font-size:8px;
        }

      </style>
      </td>
                
        </tr>';
       }
          $bookinginformation.='<tr>
          <td class="outsidetd" style="text-align:left;">'.$cars.'</td>
                 
        </tr>
         <tr>
          <td class="outsidetd" style="text-align:left;"><b>Estimated Distance : </b>'.$dbbookingdata->distance.'&nbsp;&nbsp;&nbsp; <b>Estimated Time : </b>'.$dbbookingdata->time_of_journey.'</td>
                 
        </tr>';
        
     $bookinginformation.='</table>
      <style>
        table.outside{
          border:none;
        }
        td.outsidetd{
          border-left:none;
           border-right:none;
           font-size:8px;
        }

      </style>
';


//inside booking table
/* Set Booking information in table*/
$fullbookinginformation='<table cellspacing="0" cellpadding="2" >
        <tr>
          <th style="width:63%;"><b>Description</b></th>
          <th style="width:9%;"><b>Price HT</b></th>
          <th style="width:9%;"><b>Quantity</b></th>
          <th style="width:9%;"><b>Total HT</b></th>
          <th style="width:10%;"><b>Total TTC</b></th>
        </tr>

        <tr>
           <td style="text-align:left;border:none;">'.$bookinginformation.'</td>
          <td><table cellspacing="0" cellpadding="3"  class="insidefees">';
            $fullbookinginformation.='<tr>
                 <td class="insidefeestd" style="text-align:center;">';
             $fullbookinginformation.=round($htprice, 2).' €';
             $fullbookinginformation.='</td></tr>';
            $fullbookinginformation.='</table><style>
              table.insidefees{
                border:none;
              }
              td.insidefeestd{
                border-left:none;
                 border-right:none;
                 font-size:8px;
              }
            </style>';
              $fullbookinginformation.='</td>
          <td><table cellspacing="0" cellpadding="3"  class="insidefees">';
            $fullbookinginformation.='<tr>
                 <td class="insidefeestd" style="text-align:center;">';
                $fullbookinginformation.=$isquantity;
             $fullbookinginformation.='</td></tr>';
            $fullbookinginformation.='</table><style>
              table.insidefees{
                border:none;
              }
              td.insidefeestd{
                border-left:none;
                 border-right:none;
                 font-size:8px;
              }
            </style>';
              $fullbookinginformation.='</td>
          <td><table cellspacing="0" cellpadding="3"  class="insidefees">';
            $fullbookinginformation.='<tr>
                 <td class="insidefeestd" style="text-align:center;">';
                $fullbookinginformation.=round($totalhtprice, 2).' €';
             $fullbookinginformation.='</td></tr>';
            $fullbookinginformation.='</table><style>
              table.insidefees{
                border:none;
              }
              td.insidefeestd{
                border-left:none;
                 border-right:none;
                 font-size:8px;
              }
            </style>';
              $fullbookinginformation.='</td>
          <td><table cellspacing="0" cellpadding="3"  class="insidefees">';
            $fullbookinginformation.='<tr>
                 <td class="insidefeestd" style="text-align:center;">';
                $fullbookinginformation.=round(( $totalhtprice + $dbdiscountandfeedata->vatfee ), 2).' €';
             $fullbookinginformation.='</td></tr>';
            $fullbookinginformation.='</table><style>
              table.insidefees{
                border:none;
              }
              td.insidefeestd{
                border-left:none;
                 border-right:none;
                 font-size:8px;
              }
            </style>';
              $fullbookinginformation.='</td>      
        </tr>';

$fullbookinginformation.='<tr>
           <td style="text-align:left;border:none;"><table cellspacing="0" cellpadding="3"  class="insidefees">';
            $fullbookinginformation.='<tr>
                 <td class="insidefeestd" style="text-align:left;"><b>Fees : </b></td></tr>';
                 $fullbookinginformation.='</table>
            <style>
              table.insidefees{
                border:none;
              }
              td.insidefeestd{
                border-left:none;
                 border-right:none;
                 font-size:8px;
              }
            </style>
          </td>
           <td></td> 
           <td></td> 
           <td></td> 
           <td></td>   
        </tr>';
 
   
   $fullbookinginformation.='<tr>
          <td style="text-align:left;border:none;"><table cellspacing="0" cellpadding="3"  class="insidefees">';
            $fullbookinginformation.='<tr>
                 <td class="insidefeestd" style="text-align:left;">';
            
               
                if($dbdiscountandfeedata->nighttimefee!=0){
                  $fullbookinginformation.='<br>Night Fee ';
                }
                 if($dbdiscountandfeedata->notworkingdayfee!=0){
                  $fullbookinginformation.='<br>Not Working Day Fee ';
                }
                 if($dbdiscountandfeedata->maydecdayfee!=0){
                  $fullbookinginformation.='<br>1st Mai And 25 December Fee';
                }
                 if($dbdiscountandfeedata->drivermealfee!=0){
                  $fullbookinginformation.='<br>Driver Meal Fee';
                }
                 if($dbdiscountandfeedata->driverrestfee!=0){
                  $fullbookinginformation.='<br>Driver Rest Fee';
                }

               

               $fullbookinginformation.='</td></tr>';
            
            $fullbookinginformation.='</table>
            <style>
              table.insidefees{
                border:none;
              }
              td.insidefeestd{
                border-left:none;
                 border-right:none;
                 font-size:8px;
              }
            </style>
          </td>
          <td  style="text-align:center;border:none;"><table cellspacing="0" cellpadding="3"  class="insidefees">';
            $fullbookinginformation.='<tr>
                 <td class="insidefeestd" style="text-align:center;">';
               
                if($dbdiscountandfeedata->nighttimefee!=0){
              $isquantity=($dbdiscountandfeedata->isnighttimetype == 1 && $dbdiscountandfeedata->isreturnnighttimetype == 1) ? '2' : '1';
              $nighttimefee=($dbdiscountandfeedata->nighttimefee/$isquantity);
                  $fullbookinginformation.='<br>'.$nighttimefee.' €';
                }
                 if($dbdiscountandfeedata->notworkingdayfee!=0){
              $isquantity=($dbdiscountandfeedata->isnotworkingdaytype == 1 && $dbdiscountandfeedata->isreturnnotworkingdaytype == 1) ? '2' : '1';
              $notworkingdayfee=($dbdiscountandfeedata->notworkingdayfee/$isquantity);
                  $fullbookinginformation.='<br>'.$notworkingdayfee.' €';
                }
              if($dbdiscountandfeedata->maydecdayfee!=0){
              $isquantity=($dbdiscountandfeedata->ismydecembertype == 1 && $dbdiscountandfeedata->isreturnmydecembertype == 1) ? '2' : '1';
              $maydecdayfee=($dbdiscountandfeedata->maydecdayfee/$isquantity);
                  $fullbookinginformation.='<br>'.$maydecdayfee.' €';
                }
                  if($dbdiscountandfeedata->drivermealfee!=0){
              $isquantity=($dbdiscountandfeedata->isdrivermealtype == 1 && $dbdiscountandfeedata->isreturndrivermealtype == 1) ? '2' : '1';
              $drivermealfee=($dbdiscountandfeedata->drivermealfee/$isquantity);
                  $fullbookinginformation.='<br>'.$drivermealfee.' €';
                }
                  if($dbdiscountandfeedata->driverrestfee!=0){
              $isquantity=($dbdiscountandfeedata->isdriverresttype == 1 && $dbdiscountandfeedata->isreturndriverresttype == 1) ? '2' : '1';
              $driverrestfee=($dbdiscountandfeedata->driverrestfee/$isquantity);
                  $fullbookinginformation.='<br>'.$driverrestfee.' €';
                }
                $fullbookinginformation.='</td></tr>';
            $fullbookinginformation.='</table><style>
              table.insidefees{
                border:none;
              }
              td.insidefeestd{
                border-left:none;
                 border-right:none;
                 font-size:8px;
              }
            </style>';
           $fullbookinginformation.='</td>
         <td  style="text-align:center;border:none;"><table cellspacing="0" cellpadding="3"  class="insidefees">';
            $fullbookinginformation.='<tr>
                 <td class="insidefeestd" style="text-align:center;">';
               
                if($dbdiscountandfeedata->nighttimefee!=0){
              $isquantity=($dbdiscountandfeedata->isnighttimetype == 1 && $dbdiscountandfeedata->isreturnnighttimetype == 1) ? '2' : '1';
                  $fullbookinginformation.= '<br>'.$isquantity;
                }
                 if($dbdiscountandfeedata->notworkingdayfee!=0){
              $isquantity=($dbdiscountandfeedata->isnotworkingdaytype == 1 && $dbdiscountandfeedata->isreturnnotworkingdaytype == 1) ? '2' : '1';
                  $fullbookinginformation.= '<br>'.$isquantity;
                }
                  if($dbdiscountandfeedata->maydecdayfee!=0){
              $isquantity=($dbdiscountandfeedata->ismydecembertype == 1 && $dbdiscountandfeedata->isreturnmydecembertype == 1) ? '2' : '1';
                  $fullbookinginformation.= '<br>'.$isquantity;
                }
                  if($dbdiscountandfeedata->drivermealfee!=0){
              $isquantity=($dbdiscountandfeedata->isdrivermealtype == 1 && $dbdiscountandfeedata->isreturndrivermealtype == 1) ? '2' : '1';
                  $fullbookinginformation.= '<br>'.$isquantity;
                }
                  if($dbdiscountandfeedata->driverrestfee!=0){
              $isquantity=($dbdiscountandfeedata->isdriverresttype == 1 && $dbdiscountandfeedata->isreturndriverresttype == 1) ? '2' : '1';
                  $fullbookinginformation.= '<br>'.$isquantity;
                }
                 $fullbookinginformation.='</td></tr>';
            $fullbookinginformation.='</table><style>
              table.insidefees{
                border:none;
              }
              td.insidefeestd{
                border-left:none;
                 border-right:none;
                 font-size:8px;
              }
            </style>';
           $fullbookinginformation.='</td>
            <td  style="text-align:center;border:none;"><table cellspacing="0" cellpadding="3"  class="insidefees">';
            $fullbookinginformation.='<tr>
                 <td class="insidefeestd" style="text-align:center;">';
               
                if($dbdiscountandfeedata->nighttimefee!=0){
                  $totalhtprice+=$dbdiscountandfeedata->nighttimefee;
                    $nighttimeht=$totalhtprice;
                  $fullbookinginformation.='<br>'.round($nighttimeht,2).' €';
                }
                 if($dbdiscountandfeedata->notworkingdayfee!=0){
                   $totalhtprice+=$dbdiscountandfeedata->notworkingdayfee;
                    $notworkingdayht=$totalhtprice;
                 $fullbookinginformation.='<br>'.round($notworkingdayht,2).' €';
                }
                 if($dbdiscountandfeedata->maydecdayfee!=0){
                    $totalhtprice+=$dbdiscountandfeedata->maydecdayfee;
                    $maydecdayht=$totalhtprice;
                   $fullbookinginformation.='<br>'.round($maydecdayht,2).' €';
                }
                 if($dbdiscountandfeedata->drivermealfee!=0){
    
                  $totalhtprice+=$dbdiscountandfeedata->drivermealfee;
                    $drivermealht=$totalhtprice;
                   $fullbookinginformation.='<br>'.round($drivermealht,2).' €';
                }
                 if($dbdiscountandfeedata->driverrestfee!=0){
    
                $totalhtprice+=$dbdiscountandfeedata->driverrestfee;
                    $driverrestht=$totalhtprice;
                   $fullbookinginformation.='<br>'.round($driverrestht,2).' €';
                }
                 $fullbookinginformation.='</td></tr>';
            $fullbookinginformation.='</table><style>
              table.insidefees{
                border:none;
              }
              td.insidefeestd{
                border-left:none;
                 border-right:none;
                 font-size:8px;
              }
            </style>';
           $fullbookinginformation.='</td>
           <td  style="text-align:center;border:none;"><table cellspacing="0" cellpadding="3"  class="insidefees">';
            $fullbookinginformation.='<tr>
                 <td class="insidefeestd" style="text-align:center;">';
               
                if($dbdiscountandfeedata->nighttimefee!=0){
                  $fullbookinginformation.='<br>'.round(($nighttimeht + $dbdiscountandfeedata->vatfee),2).' €';
                }
                 if($dbdiscountandfeedata->notworkingdayfee!=0){
                  $fullbookinginformation.='<br>'.round(($notworkingdayht + $dbdiscountandfeedata->vatfee),2).' €';
                }
                  if($dbdiscountandfeedata->maydecdayfee!=0){
                  $fullbookinginformation.='<br>'.round(($maydecdayht + $dbdiscountandfeedata->vatfee),2).' €';
                }
                  if($dbdiscountandfeedata->drivermealfee!=0){
                  $fullbookinginformation.='<br>'.round(($drivermealht + $dbdiscountandfeedata->vatfee),2).' €';
                }
                  if($dbdiscountandfeedata->driverrestfee!=0){
                  $fullbookinginformation.='<br>'.round(($driverrestht + $dbdiscountandfeedata->vatfee),2).' €';
                }
                 $fullbookinginformation.='</td></tr>';
            $fullbookinginformation.='</table><style>
              table.insidefees{
                border:none;
              }
              td.insidefeestd{
                border-left:none;
                 border-right:none;
                 font-size:8px;
              }
            </style>';
           $fullbookinginformation.='</td>      
        </tr>';
        


 
 $fullbookinginformation.='<tr>
          <td style="text-align:left;border:none;"><table cellspacing="0" cellpadding="3"  class="insidefees">';
            $fullbookinginformation.='<tr>
                 <td class="insidefeestd" style="text-align:left;">';
                  $fullbookinginformation.='<b>Note :</b><br><br>';
                  if(!empty($dbbookingdata->booking_adminnote)){
                  $fullbookinginformation.= $dbbookingdata->booking_adminnote;  
                  }              
               $fullbookinginformation.='</td></tr>';
            $fullbookinginformation.='</table><style>
              table.insidefees{
                border:none;
              }
              td.insidefeestd{
                border-left:none;
                 border-right:none;
                 font-size:8px;
              }
            </style>
           
          </td>
          <td></td>
           <td></td>
            <td></td>
             <td></td>
          </tr>';
        
  
$fullbookinginformation.='</table>
     <style>
        table{
            border-collapse:collapse;
             border:1px solid #000000;
             table-layout: auto;
             width: 100%;  
           
         }
      
         th{
            border:1px solid #000000;
            text-align:center;
            font-size:10px !important;
         }
        
         td{
           border-left:1px solid #000000;
           border-right:1px solid #000000;
           border-bottom: none;
           text-align:center;
            font-size:8px !important;
          
         
         }

       
        
     </style>
';
return array('bookinginformation'=>$bookinginformation,'fullbookinginformation'=>$fullbookinginformation,'totalhtprice'=>$totalhtprice);
 }

 function getbookingnotificationdetails($bookinginformation,$booking_id,$CI){

  $CI->load->model('bookings_config_model');
  $CI->load->model('quotes_model');
  $CI->load->model('invoice_model');
  $CI->load->model('bookings_model');

$dbbookingdata=$CI->bookings_model->getdbbookingrecord('vbs_bookings',array('id'=>$booking_id));
$dbdiscountandfeedata=$CI->bookings_model->getdbbookingrecord('vbs_bookings_pricevalues',array('booking_id'=>$booking_id));
$isquantity= ($dbbookingdata->returncheck ==1)?"2":'1';
if($dbbookingdata->regular == 1){
  $isquantity="1";
}

$fullbookinginformation='<table cellspacing="0" cellpadding="2" >
        <tr>
          <th style="width:90%;"><b>Description</b></th>
          <th style="width:10%;"><b>Quantity</b></th>
        </tr>

        <tr>
           <td style="text-align:left;border:none;">'.$bookinginformation.'</td>
           <td><table cellspacing="0" cellpadding="3"  class="insidefees">';
                 $fullbookinginformation.='<tr>
                 <td class="insidefeestd" style="text-align:center;">';
                 $fullbookinginformation.=$isquantity;
                 $fullbookinginformation.='</td></tr>';
                 $fullbookinginformation.='</table><style>
               table.insidefees{
                  border:none;
               }
               td.insidefeestd{
                  border-left:none;
                  border-right:none;
                  font-size:8px;
                 }
            </style>';
            $fullbookinginformation.='</td>    
        </tr>';

$fullbookinginformation.='<tr>
           <td style="text-align:left;border:none;"><table cellspacing="0" cellpadding="3"  class="insidefees">';
            $fullbookinginformation.='<tr>
                 <td class="insidefeestd" style="text-align:left;"><b>Fees : </b></td></tr>';
                 $fullbookinginformation.='</table>
            <style>
              table.insidefees{
                border:none;
              }
              td.insidefeestd{
                border-left:none;
                 border-right:none;
                 font-size:8px;
              }
            </style>
          </td>
           <td></td> 
           <td></td> 
           <td></td> 
           <td></td>   
        </tr>';
 
   
   $fullbookinginformation.='<tr>
           <td style="text-align:left;border:none;"><table cellspacing="0" cellpadding="3"  class="insidefees">';
                 $fullbookinginformation.='<tr>
                 <td class="insidefeestd" style="text-align:left;">';
            
                 if($dbdiscountandfeedata->nighttimefee!=0){
                  $fullbookinginformation.='<br>Night Fee ';
                }
                 if($dbdiscountandfeedata->notworkingdayfee!=0){
                  $fullbookinginformation.='<br>Not Working Day Fee ';
                }
                 if($dbdiscountandfeedata->maydecdayfee!=0){
                  $fullbookinginformation.='<br>1st Mai And 25 December Fee';
                }
                 if($dbdiscountandfeedata->drivermealfee!=0){
                  $fullbookinginformation.='<br>Driver Meal Fee';
                }
                 if($dbdiscountandfeedata->driverrestfee!=0){
                  $fullbookinginformation.='<br>Driver Rest Fee';
                }

               $fullbookinginformation.='</td></tr>';
            
               $fullbookinginformation.='</table>
               <style>
               table.insidefees{
                border:none;
               }
               td.insidefeestd{
                 border-left:none;
                 border-right:none;
                 font-size:8px;
              }
            </style>
          </td>
          <td  style="text-align:center;border:none;"><table cellspacing="0" cellpadding="3"  class="insidefees">';
            $fullbookinginformation.='<tr>
                 <td class="insidefeestd" style="text-align:center;">';
               
                if($dbdiscountandfeedata->nighttimefee!=0){
              $isquantity=($dbdiscountandfeedata->isnighttimetype == 1 && $dbdiscountandfeedata->isreturnnighttimetype == 1) ? '2' : '1';
                  $fullbookinginformation.= '<br>'.$isquantity;
                }
                 if($dbdiscountandfeedata->notworkingdayfee!=0){
              $isquantity=($dbdiscountandfeedata->isnotworkingdaytype == 1 && $dbdiscountandfeedata->isreturnnotworkingdaytype == 1) ? '2' : '1';
                  $fullbookinginformation.= '<br>'.$isquantity;
                }
                  if($dbdiscountandfeedata->maydecdayfee!=0){
              $isquantity=($dbdiscountandfeedata->ismydecembertype == 1 && $dbdiscountandfeedata->isreturnmydecembertype == 1) ? '2' : '1';
                  $fullbookinginformation.= '<br>'.$isquantity;
                }
                  if($dbdiscountandfeedata->drivermealfee!=0){
              $isquantity=($dbdiscountandfeedata->isdrivermealtype == 1 && $dbdiscountandfeedata->isreturndrivermealtype == 1) ? '2' : '1';
                  $fullbookinginformation.= '<br>'.$isquantity;
                }
                  if($dbdiscountandfeedata->driverrestfee!=0){
              $isquantity=($dbdiscountandfeedata->isdriverresttype == 1 && $dbdiscountandfeedata->isreturndriverresttype == 1) ? '2' : '1';
                  $fullbookinginformation.= '<br>'.$isquantity;
                }
                 $fullbookinginformation.='</td></tr>';
            $fullbookinginformation.='</table><style>
              table.insidefees{
                border:none;
              }
              td.insidefeestd{
                border-left:none;
                 border-right:none;
                 font-size:8px;
              }
            </style>';
           $fullbookinginformation.='</td>    
        </tr>';
        


 
 $fullbookinginformation.='<tr>
          <td style="text-align:left;border:none;"><table cellspacing="0" cellpadding="3"  class="insidefees">';
            $fullbookinginformation.='<tr>
                 <td class="insidefeestd" style="text-align:left;">';
                  $fullbookinginformation.='<b>Comments :</b><br><br>';
                  if(!empty($dbbookingdata->booking_comment)){
                  $fullbookinginformation.= $dbbookingdata->booking_comment;  
                  }              
               $fullbookinginformation.='</td></tr>';
            $fullbookinginformation.='</table><style>
              table.insidefees{
                border:none;
              }
              td.insidefeestd{
                border-left:none;
                 border-right:none;
                 font-size:8px;
              }
            </style>
           
          </td>
          <td></td>
          </tr>';
        
  
$fullbookinginformation.='</table>
     <style>
        table{
            border-collapse:collapse;
             border:1px solid #000000;
             table-layout: auto;
             width: 100%;  
           
         }
      
         th{
            border:1px solid #000000;
            text-align:center;
            font-size:10px !important;
         }
        
         td{
           border-left:1px solid #000000;
           border-right:1px solid #000000;
           border-bottom: none;
           text-align:center;
            font-size:8px !important;
          
         
         }

       
        
     </style>
';
return $fullbookinginformation;
 }

 //driver notification
 function createdrivernotificationpdf($drivernotificationid,$driver_id,$columnnumber){
    $CI = get_instance();
    $CI->load->model('drivers_model');
    $CI->load->model('bookings_model');

  $drivernotificationsdata=$CI->drivers_model->getSingleRecord('vbs_drivers_notification',['id' => $drivernotificationid]);
  $columnname="";
  $columnexpireddate="";
  if($columnnumber == 1){
     $columnname="Identity Card";
      $columnexpireddate=from_unix_date($drivernotificationsdata->typeiddate);
  }
  elseif($columnnumber == 2){
     $columnname="Driver License";
      $columnexpireddate=from_unix_date($drivernotificationsdata->permitnumberdate);
  }
  elseif($columnnumber == 3){
     $columnname="Medical Certificate";
      $columnexpireddate=from_unix_date($drivernotificationsdata->medicalcertificatedate);
  }
  elseif($columnnumber == 4){
     $columnname="PSC1";
      $columnexpireddate=from_unix_date($drivernotificationsdata->psc1date);
  }
  elseif($columnnumber == 5){
     $columnname="Work Medecine";
      $columnexpireddate=from_unix_date($drivernotificationsdata->medicineoftravaidate);
  }

  $notificationsdata = $CI->drivers_model->getSingleRecord('vbs_notifications',['id' => $drivernotificationsdata->notification_id]);

  
  $driver=$CI->drivers_model->getSingleRecord('vbs_driverprofile',array('id'=>$driver_id));

  $companydata=$CI->bookings_model->getcompanystatus();

  $companycity=$CI->drivers_model->getSingleRecord('vbs_cities',['id'=>$companydata->city])->name;

  $client=$CI->drivers_model->getclientdatabyid($driver->userid);
  $clientcity=$CI->drivers_model->getSingleRecord('vbs_cities',['id'=>$client->city])->name;

  $user = $CI->basic_auth->user();
  $CI->load->model('userx_model');
  $user_data = $CI->userx_model->get(['user.id' => $user->id]);
  //notification data
  $CI->load->library('bookpdf');

  $pdf = new bookpdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

   $address2=(!empty($companydata->address2))?trim($companydata->address2).', ' :'';
$companydatainfo=strtoupper($companydata->name).' '.strtoupper($companydata->type).', '.$companydata->address.', '.$address2.$companydata->zip_code.', '.$companycity.', Capital : '.$companydata->capital.', License N : '.$companydata->licence.', SIRET : '.$companydata->sirft.', VAT N<sup>o</sup> : '.$companydata->numero_tva;
$pdf->setInfo($companydatainfo);

$fileName='Expired '.$columnname.'  Notification '.date("dmY", strtotime($driver->created_at)) .'0000'. $driver->id.' '.$client->civility.' '.$client->first_name.' '.$client->last_name;
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle($fileName.".PDF");
$pdf->SetSubject($fileName.".PDF");
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setPrintHeader(false);
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);



$pdf->addPage();



/* Company information */
$cominfo1="<span><b>".strtoupper($companydata->name).' '.strtoupper($companydata->type)."</b></span><br><span>".$companydata->address."</span><br>";
if(!empty($companydata->address2)){
  $cominfo1.="<span>".$companydata->address2."</span><br>";
}
$cominfo1.="<span>".$companydata->zip_code.' '.$companycity."</span><br><span>Phone&nbsp;&nbsp;&nbsp;&nbsp; : ".$companydata->phone."</span><br><span>Fax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ".$companydata->fax."</span><br><span>Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: ".$companydata->email."</span><br><span>Website&nbsp;&nbsp;: ".$companydata->website."</span>";
/* Company information */


/* Client information */
$clientinfo="<b>&nbsp;".$client->civility.' '.$client->first_name.' '.$client->last_name."</b><br>&nbsp;".$client->address."<br>&nbsp;";
if(!empty($client->address1)){
  $clientinfo.=$client->address1."<br>&nbsp;";
}
$clientinfo.=$client->zipcode.'  '.$clientcity;
/* Client information */







/* Company Logo */
$companylogo=base_url().'uploads/company/'.$companydata->logo;
$companylogo = '<img src="'.$companylogo.'" width="110" height="95"/>';
/* Company Logo */

/* Set Company/Client/Quote Information in Table */
$html='<table cellspacing="0" cellpadding="0" >
<tr>
          <td style="width:37%;">'.$companylogo.'</td>
          <td style="width:26%;"></td>
          <td style="width:37%;"></td>
        </tr></table>';
$html .='<table cellspacing="0" cellpadding="0" >
       

        <tr>
          <td style="width:37%;">'.$cominfo1.'</td>
          <td style="width:26%;"></td>
         <td style="width:37%;"><div class="test" style="page-break-inside: avoid;"><table cellspacing="0" cellpadding="6" ><tr><td>'.$clientinfo.'</td></tr></table></div>


          <style>   
              div.test {
                
                  border:0.1em solid black;  

              }
          </style></td> 
        </tr> 

        <tr>
          <td></td>
          <td></td>
          <td>Done at : '.$companycity.'&nbsp; Expired on : '.$columnexpireddate.'</td>     
        </tr>        
     </table>
     <style>
        table{
             border-collapse:collapse;
             table-layout: auto;
             width: 100%;    
         }
      
         th{
            border:1px solid #000000;
            text-align:center;
            font-size:10px !important;
         }
        
         td{
           
           text-align:left;
          
         
         }

       
        
     </style>
';
/* Set Company/Client/Quote Information in Table */
$subject=str_replace("{driver_column_name}",$columnname,$notificationsdata->subject);

$message=str_replace("{user_civility}",$client->civility,$notificationsdata->message);
$message=str_replace("{user_firstname}",$client->first_name,$message);
$message=str_replace("{user_lastname}",$client->last_name,$message);
$message=str_replace("{driver_column_name}",$columnname,$message);
$message=str_replace("{driver_column_expire_date}",$columnexpireddate,$message);

$message="<h4>Subject : ".strtoupper($subject)." </h4>".$message;
    
$notificationtitle="EXPIRED ".strtoupper($columnname)."  NOTIFICATION";
$pdf->setFont('helveticaB','',12);
$pdf->cell(30,10,'',0,0,'C');
$pdf->cell(132,10,$notificationtitle,1,0,'C');
$pdf->cell(30,10,'',0,1,'C');
$pdf->Ln(4);
//Set Reminder Message


 

$pdf->setFont('helvetica','',11);
$pdf->setCellPaddings( $left = 1, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 3);
$pdf->writeHTMLCell(190,0,10,'',$html,0,1);




//Set Notification Message
$pdf->setCellPaddings( $left = 1, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 0);
$pdf->setFont('helvetica','',11);

$pdf->writeHTMLCell(190,'','','',$message,0,1);
 $positiony = $pdf->GetY();
//$pdf->cell(192,2,$positiony,1,1,'C');

//signature box

$pdf->setCellPaddings( $left = 0, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 0);
/*admin logo */
$adminsignaturelogo=base_url().'uploads/user/'.$user_data['document_signature'];
$adminsignaturelogo = '<img src="'.$adminsignaturelogo.'" width="110" height="95"/>';
/*admin logo*/
$userdeparment=$user_data['department'];
$username='<span>'.$user_data['civility'].' '.$user_data['first_name'].' '.$user_data['last_name'].'</span>';
$stampsection='<table cellspacing="2" cellpadding="1" border="0" nobr="true">
<tr>
<th style="width:67%;"></th>
<th style="width:33%;"><b>'.$userdeparment.' Department</b><br>'.$username.'<br>Signature and Stamp</th>
</tr>

<tr>
<td style="border:none;"></td>
<td  style="border-top:none;">'.$adminsignaturelogo.'</td>
</tr>
</table>
   <style>
        table{
             border-collapse:collapse;
             table-layout: auto;
             width: 100%;  
         }
         th{
           border:none; 
           text-align:left;
         }
         td{   
          border:0.1em  solid #000000;
          text-align:center; 
         }
      </style>   
';

$pdf->setFont('helvetica','',10);
if($positiony < 223){
  $pdf->SetY(-75);
  $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}
elseif($positiony < 263){
   $pdf->Ln(45);
   $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}
else{
   $pdf->Ln(5);
   $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}

//signature box


return array('pdf' => $pdf,'fileName' => $fileName);
}

 //driver reminder
 function createdriverreminderpdf($driverreminderid,$driver_id,$columnnumber){
    $CI = get_instance();
    $CI->load->model('drivers_model');
     $CI->load->model('bookings_model');

  $driverremindersdata=$CI->drivers_model->getSingleRecord('vbs_drivers_reminders',['id' => $driverreminderid]);
  $columnname="";
  $columnexpireddate="";
  if($columnnumber == 1){
     $columnname="Identity Card";
      $columnexpireddate=from_unix_date($driverremindersdata->typeiddate);
  }
  elseif($columnnumber == 2){
     $columnname="Driver License";
      $columnexpireddate=from_unix_date($driverremindersdata->permitnumberdate);
  }
  elseif($columnnumber == 3){
     $columnname="Medical Certificate";
      $columnexpireddate=from_unix_date($driverremindersdata->medicalcertificatedate);
  }
  elseif($columnnumber == 4){
     $columnname="PSC1";
      $columnexpireddate=from_unix_date($driverremindersdata->psc1date);
  }
  elseif($columnnumber == 5){
     $columnname="Work Medecine";
      $columnexpireddate=from_unix_date($driverremindersdata->medicineoftravaidate);
  }

  $reminderdata = $CI->drivers_model->getSingleRecord('vbs_reminders',['id' => $driverremindersdata->reminder_id]);

  
  $driver=$CI->drivers_model->getSingleRecord('vbs_driverprofile',array('id'=>$driver_id));

  $companydata=$CI->bookings_model->getcompanystatus();

  $companycity=$CI->drivers_model->getSingleRecord('vbs_cities',['id'=>$companydata->city])->name;

  $client=$CI->drivers_model->getclientdatabyid($driver->userid);
  $clientcity=$CI->drivers_model->getSingleRecord('vbs_cities',['id'=>$client->city])->name;

  $user = $CI->basic_auth->user();
  $CI->load->model('userx_model');
  $user_data = $CI->userx_model->get(['user.id' => $user->id]);
  //reminder data
  $CI->load->library('bookpdf');

  $pdf = new bookpdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

   $address2=(!empty($companydata->address2))?trim($companydata->address2).', ' :'';
$companydatainfo=strtoupper($companydata->name).' '.strtoupper($companydata->type).', '.$companydata->address.', '.$address2.$companydata->zip_code.', '.$companycity.', Capital : '.$companydata->capital.', License N : '.$companydata->licence.', SIRET : '.$companydata->sirft.', VAT N<sup>o</sup> : '.$companydata->numero_tva;
$pdf->setInfo($companydatainfo);

$fileName='Expired '.$columnname.'  Reminder '.date("dmY", strtotime($driver->created_at)) .'0000'. $driver->id.' '.$client->civility.' '.$client->first_name.' '.$client->last_name;
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle($fileName.".PDF");
$pdf->SetSubject($fileName.".PDF");
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setPrintHeader(false);
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);



$pdf->addPage();



/* Company information */
$cominfo1="<span><b>".strtoupper($companydata->name).' '.strtoupper($companydata->type)."</b></span><br><span>".$companydata->address."</span><br>";
if(!empty($companydata->address2)){
  $cominfo1.="<span>".$companydata->address2."</span><br>";
}
$cominfo1.="<span>".$companydata->zip_code.' '.$companycity."</span><br><span>Phone&nbsp;&nbsp;&nbsp;&nbsp; : ".$companydata->phone."</span><br><span>Fax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ".$companydata->fax."</span><br><span>Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: ".$companydata->email."</span><br><span>Website&nbsp;&nbsp;: ".$companydata->website."</span>";
/* Company information */


/* Client information */
$clientinfo="<b>&nbsp;".$client->civility.' '.$client->first_name.' '.$client->last_name."</b><br>&nbsp;".$client->address."<br>&nbsp;";
if(!empty($client->address1)){
  $clientinfo.=$client->address1."<br>&nbsp;";
}
$clientinfo.=$client->zipcode.'  '.$clientcity;
/* Client information */







/* Company Logo */
$companylogo=base_url().'uploads/company/'.$companydata->logo;
$companylogo = '<img src="'.$companylogo.'" width="110" height="95"/>';
/* Company Logo */

/* Set Company/Client/Quote Information in Table */
$html='<table cellspacing="0" cellpadding="0" >
<tr>
          <td style="width:37%;">'.$companylogo.'</td>
          <td style="width:26%;"></td>
          <td style="width:37%;"></td>
        </tr></table>';
$html .='<table cellspacing="0" cellpadding="0" >
       

        <tr>
          <td style="width:37%;">'.$cominfo1.'</td>
          <td style="width:26%;"></td>
         <td style="width:37%;"><div class="test" style="page-break-inside: avoid;"><table cellspacing="0" cellpadding="6" ><tr><td>'.$clientinfo.'</td></tr></table></div>


          <style>   
              div.test {
                
                  border:0.1em solid black;  

              }
          </style></td> 
        </tr> 

        <tr>
          <td></td>
          <td></td>
          <td>Done at : '.$companycity.'&nbsp; Expired on : '.$columnexpireddate.'</td>     
        </tr>        
     </table>
     <style>
        table{
             border-collapse:collapse;
             table-layout: auto;
             width: 100%;    
         }
      
         th{
            border:1px solid #000000;
            text-align:center;
            font-size:10px !important;
         }
        
         td{
           
           text-align:left;
          
         
         }

       
        
     </style>
';
/* Set Company/Client/Quote Information in Table */
$subject=str_replace("{driver_column_name}",$columnname,$reminderdata->subject);

$message=str_replace("{user_civility}",$client->civility,$reminderdata->message);
$message=str_replace("{user_firstname}",$client->first_name,$message);
$message=str_replace("{user_lastname}",$client->last_name,$message);
$message=str_replace("{driver_column_name}",$columnname,$message);
$message=str_replace("{driver_column_expire_date}",$columnexpireddate,$message);

$message="<h4>Subject : ".strtoupper($subject)." </h4>".$message;
    
$remindertitle="EXPIRED ".strtoupper($columnname)."  REMINDER";
$pdf->setFont('helveticaB','',12);
$pdf->cell(30,10,'',0,0,'C');
$pdf->cell(132,10,$remindertitle,1,0,'C');
$pdf->cell(30,10,'',0,1,'C');
$pdf->Ln(4);
//Set Reminder Message


 

$pdf->setFont('helvetica','',11);
$pdf->setCellPaddings( $left = 1, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 3);
$pdf->writeHTMLCell(190,0,10,'',$html,0,1);




//Set Reminder Message
$pdf->setCellPaddings( $left = 1, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 0);
$pdf->setFont('helvetica','',11);

$pdf->writeHTMLCell(190,'','','',$message,0,1);
 $positiony = $pdf->GetY();
//$pdf->cell(192,2,$positiony,1,1,'C');

//signature box

$pdf->setCellPaddings( $left = 0, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 0);
/*admin logo */
$adminsignaturelogo=base_url().'uploads/user/'.$user_data['document_signature'];
$adminsignaturelogo = '<img src="'.$adminsignaturelogo.'" width="110" height="95"/>';
/*admin logo*/
$userdeparment=$user_data['department'];
$username='<span>'.$user_data['civility'].' '.$user_data['first_name'].' '.$user_data['last_name'].'</span>';
$stampsection='<table cellspacing="2" cellpadding="1" border="0" nobr="true">
<tr>
<th style="width:67%;"></th>
<th style="width:33%;"><b>'.$userdeparment.' Department</b><br>'.$username.'<br>Signature and Stamp</th>
</tr>

<tr>
<td style="border:none;"></td>
<td  style="border-top:none;">'.$adminsignaturelogo.'</td>
</tr>
</table>
   <style>
        table{
             border-collapse:collapse;
             table-layout: auto;
             width: 100%;  
         }
         th{
           border:none; 
           text-align:left;
         }
         td{   
          border:0.1em  solid #000000;
          text-align:center; 
         }
      </style>   
';

$pdf->setFont('helvetica','',10);
if($positiony < 223){
  $pdf->SetY(-75);
  $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}
elseif($positiony < 263){
   $pdf->Ln(45);
   $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}
else{
   $pdf->Ln(5);
   $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}

//signature box


return array('pdf' => $pdf,'fileName' => $fileName);
}

 //partner notification
 function createpartnernotificationpdf($partnernotificationid,$partner_id,$columnnumber){
    $CI = get_instance();
    $CI->load->model('partners_model');
    $CI->load->model('bookings_model');

  $partnernotificationsdata=$CI->partners_model->getSingleRecord('vbs_partners_notification',['id' => $partnernotificationid]);
  $columnname="";
  $columnexpireddate="";
  if($columnnumber == 1){
     $columnname="Identity Card";
      $columnexpireddate=from_unix_date($partnernotificationsdata->typeiddate);
  }
  elseif($columnnumber == 2){
     $columnname="Partner License";
      $columnexpireddate=from_unix_date($partnernotificationsdata->permitnumberdate);
  }
  elseif($columnnumber == 3){
     $columnname="Medical Certificate";
      $columnexpireddate=from_unix_date($partnernotificationsdata->medicalcertificatedate);
  }
  elseif($columnnumber == 4){
     $columnname="PSC1";
      $columnexpireddate=from_unix_date($partnernotificationsdata->psc1date);
  }
  elseif($columnnumber == 5){
     $columnname="Work Medecine";
      $columnexpireddate=from_unix_date($partnernotificationsdata->medicineoftravaidate);
  }

  $notificationsdata = $CI->partners_model->getSingleRecord('vbs_notifications',['id' => $partnernotificationsdata->notification_id]);

  
  $partner=$CI->partners_model->getSingleRecord('vbs_partners',array('id'=>$partner_id));

  $companydata=$CI->bookings_model->getcompanystatus();

  $companycity=$CI->partners_model->getSingleRecord('vbs_cities',['id'=>$companydata->city])->name;

  $client=$CI->partners_model->getclientdatabyid($partner->userid);
  $clientcity=$CI->partners_model->getSingleRecord('vbs_cities',['id'=>$client->city])->name;

  $user = $CI->basic_auth->user();
  $CI->load->model('userx_model');
  $user_data = $CI->userx_model->get(['user.id' => $user->id]);
  //notification data
  $CI->load->library('bookpdf');

  $pdf = new bookpdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

   $address2=(!empty($companydata->address2))?trim($companydata->address2).', ' :'';
$companydatainfo=strtoupper($companydata->name).' '.strtoupper($companydata->type).', '.$companydata->address.', '.$address2.$companydata->zip_code.', '.$companycity.', Capital : '.$companydata->capital.', License N : '.$companydata->licence.', SIRET : '.$companydata->sirft.', VAT N<sup>o</sup> : '.$companydata->numero_tva;
$pdf->setInfo($companydatainfo);

$fileName='Expired '.$columnname.'  Notification '.date("dmY", strtotime($partner->created_at)) .'0000'. $partner->id.' '.$client->civility.' '.$client->first_name.' '.$client->last_name;
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle($fileName.".PDF");
$pdf->SetSubject($fileName.".PDF");
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setPrintHeader(false);
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);



$pdf->addPage();



/* Company information */
$cominfo1="<span><b>".strtoupper($companydata->name).' '.strtoupper($companydata->type)."</b></span><br><span>".$companydata->address."</span><br>";
if(!empty($companydata->address2)){
  $cominfo1.="<span>".$companydata->address2."</span><br>";
}
$cominfo1.="<span>".$companydata->zip_code.' '.$companycity."</span><br><span>Phone&nbsp;&nbsp;&nbsp;&nbsp; : ".$companydata->phone."</span><br><span>Fax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ".$companydata->fax."</span><br><span>Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: ".$companydata->email."</span><br><span>Website&nbsp;&nbsp;: ".$companydata->website."</span>";
/* Company information */


/* Client information */
$clientinfo="<b>&nbsp;".$client->civility.' '.$client->first_name.' '.$client->last_name."</b><br>&nbsp;".$client->address."<br>&nbsp;";
if(!empty($client->address1)){
  $clientinfo.=$client->address1."<br>&nbsp;";
}
$clientinfo.=$client->zipcode.'  '.$clientcity;
/* Client information */







/* Company Logo */
$companylogo=base_url().'uploads/company/'.$companydata->logo;
$companylogo = '<img src="'.$companylogo.'" width="110" height="95"/>';
/* Company Logo */

/* Set Company/Client/Quote Information in Table */
$html='<table cellspacing="0" cellpadding="0" >
<tr>
          <td style="width:37%;">'.$companylogo.'</td>
          <td style="width:26%;"></td>
          <td style="width:37%;"></td>
        </tr></table>';
$html .='<table cellspacing="0" cellpadding="0" >
       

        <tr>
          <td style="width:37%;">'.$cominfo1.'</td>
          <td style="width:26%;"></td>
         <td style="width:37%;"><div class="test" style="page-break-inside: avoid;"><table cellspacing="0" cellpadding="6" ><tr><td>'.$clientinfo.'</td></tr></table></div>


          <style>   
              div.test {
                
                  border:0.1em solid black;  

              }
          </style></td> 
        </tr> 

        <tr>
          <td></td>
          <td></td>
          <td>Done at : '.$companycity.'&nbsp; Expired on : '.$columnexpireddate.'</td>     
        </tr>        
     </table>
     <style>
        table{
             border-collapse:collapse;
             table-layout: auto;
             width: 100%;    
         }
      
         th{
            border:1px solid #000000;
            text-align:center;
            font-size:10px !important;
         }
        
         td{
           
           text-align:left;
          
         
         }

       
        
     </style>
';
/* Set Company/Client/Quote Information in Table */
$subject=str_replace("{partner_column_name}",$columnname,$notificationsdata->subject);

$message=str_replace("{user_civility}",$client->civility,$notificationsdata->message);
$message=str_replace("{user_firstname}",$client->first_name,$message);
$message=str_replace("{user_lastname}",$client->last_name,$message);
$message=str_replace("{partner_column_name}",$columnname,$message);
$message=str_replace("{partner_column_expire_date}",$columnexpireddate,$message);

$message="<h4>Subject : ".strtoupper($subject)." </h4>".$message;
    
$notificationtitle="EXPIRED ".strtoupper($columnname)."  NOTIFICATION";
$pdf->setFont('helveticaB','',12);
$pdf->cell(30,10,'',0,0,'C');
$pdf->cell(132,10,$notificationtitle,1,0,'C');
$pdf->cell(30,10,'',0,1,'C');
$pdf->Ln(4);
//Set Reminder Message


 

$pdf->setFont('helvetica','',11);
$pdf->setCellPaddings( $left = 1, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 3);
$pdf->writeHTMLCell(190,0,10,'',$html,0,1);




//Set Notification Message
$pdf->setCellPaddings( $left = 1, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 0);
$pdf->setFont('helvetica','',11);

$pdf->writeHTMLCell(190,'','','',$message,0,1);
 $positiony = $pdf->GetY();
//$pdf->cell(192,2,$positiony,1,1,'C');

//signature box

$pdf->setCellPaddings( $left = 0, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 0);
/*admin logo */
$adminsignaturelogo=base_url().'uploads/user/'.$user_data['document_signature'];
$adminsignaturelogo = '<img src="'.$adminsignaturelogo.'" width="110" height="95"/>';
/*admin logo*/
$userdeparment=$user_data['department'];
$username='<span>'.$user_data['civility'].' '.$user_data['first_name'].' '.$user_data['last_name'].'</span>';
$stampsection='<table cellspacing="2" cellpadding="1" border="0" nobr="true">
<tr>
<th style="width:67%;"></th>
<th style="width:33%;"><b>'.$userdeparment.' Department</b><br>'.$username.'<br>Signature and Stamp</th>
</tr>

<tr>
<td style="border:none;"></td>
<td  style="border-top:none;">'.$adminsignaturelogo.'</td>
</tr>
</table>
   <style>
        table{
             border-collapse:collapse;
             table-layout: auto;
             width: 100%;  
         }
         th{
           border:none; 
           text-align:left;
         }
         td{   
          border:0.1em  solid #000000;
          text-align:center; 
         }
      </style>   
';

$pdf->setFont('helvetica','',10);
if($positiony < 223){
  $pdf->SetY(-75);
  $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}
elseif($positiony < 263){
   $pdf->Ln(45);
   $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}
else{
   $pdf->Ln(5);
   $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}

//signature box


return array('pdf' => $pdf,'fileName' => $fileName);
}

 //partner reminder
 function createpartnerreminderpdf($partnerreminderid,$partner_id,$columnnumber){
    $CI = get_instance();
    $CI->load->model('partners_model');
     $CI->load->model('bookings_model');

  $partnerremindersdata=$CI->partners_model->getSingleRecord('vbs_partners_reminders',['id' => $partnerreminderid]);
  $columnname="";
  $columnexpireddate="";
  if($columnnumber == 1){
     $columnname="Identity Card";
      $columnexpireddate=from_unix_date($partnerremindersdata->typeiddate);
  }
  elseif($columnnumber == 2){
     $columnname="Partner License";
      $columnexpireddate=from_unix_date($partnerremindersdata->permitnumberdate);
  }
  elseif($columnnumber == 3){
     $columnname="Medical Certificate";
      $columnexpireddate=from_unix_date($partnerremindersdata->medicalcertificatedate);
  }
  elseif($columnnumber == 4){
     $columnname="PSC1";
      $columnexpireddate=from_unix_date($partnerremindersdata->psc1date);
  }
  elseif($columnnumber == 5){
     $columnname="Work Medecine";
      $columnexpireddate=from_unix_date($partnerremindersdata->medicineoftravaidate);
  }

  $reminderdata = $CI->partners_model->getSingleRecord('vbs_reminders',['id' => $partnerremindersdata->reminder_id]);

  
  $partner=$CI->partners_model->getSingleRecord('vbs_partners',array('id'=>$partner_id));

  $companydata=$CI->bookings_model->getcompanystatus();

  $companycity=$CI->partners_model->getSingleRecord('vbs_cities',['id'=>$companydata->city])->name;

  $client=$CI->partners_model->getclientdatabyid($partner->userid);
  $clientcity=$CI->partners_model->getSingleRecord('vbs_cities',['id'=>$client->city])->name;

  $user = $CI->basic_auth->user();
  $CI->load->model('userx_model');
  $user_data = $CI->userx_model->get(['user.id' => $user->id]);
  //reminder data
  $CI->load->library('bookpdf');

  $pdf = new bookpdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

   $address2=(!empty($companydata->address2))?trim($companydata->address2).', ' :'';
$companydatainfo=strtoupper($companydata->name).' '.strtoupper($companydata->type).', '.$companydata->address.', '.$address2.$companydata->zip_code.', '.$companycity.', Capital : '.$companydata->capital.', License N : '.$companydata->licence.', SIRET : '.$companydata->sirft.', VAT N<sup>o</sup> : '.$companydata->numero_tva;
$pdf->setInfo($companydatainfo);

$fileName='Expired '.$columnname.'  Reminder '.date("dmY", strtotime($partner->created_at)) .'0000'. $partner->id.' '.$client->civility.' '.$client->first_name.' '.$client->last_name;
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle($fileName.".PDF");
$pdf->SetSubject($fileName.".PDF");
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setPrintHeader(false);
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);



$pdf->addPage();



/* Company information */
$cominfo1="<span><b>".strtoupper($companydata->name).' '.strtoupper($companydata->type)."</b></span><br><span>".$companydata->address."</span><br>";
if(!empty($companydata->address2)){
  $cominfo1.="<span>".$companydata->address2."</span><br>";
}
$cominfo1.="<span>".$companydata->zip_code.' '.$companycity."</span><br><span>Phone&nbsp;&nbsp;&nbsp;&nbsp; : ".$companydata->phone."</span><br><span>Fax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ".$companydata->fax."</span><br><span>Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: ".$companydata->email."</span><br><span>Website&nbsp;&nbsp;: ".$companydata->website."</span>";
/* Company information */


/* Client information */
$clientinfo="<b>&nbsp;".$client->civility.' '.$client->first_name.' '.$client->last_name."</b><br>&nbsp;".$client->address."<br>&nbsp;";
if(!empty($client->address1)){
  $clientinfo.=$client->address1."<br>&nbsp;";
}
$clientinfo.=$client->zipcode.'  '.$clientcity;
/* Client information */







/* Company Logo */
$companylogo=base_url().'uploads/company/'.$companydata->logo;
$companylogo = '<img src="'.$companylogo.'" width="110" height="95"/>';
/* Company Logo */

/* Set Company/Client/Quote Information in Table */
$html='<table cellspacing="0" cellpadding="0" >
<tr>
          <td style="width:37%;">'.$companylogo.'</td>
          <td style="width:26%;"></td>
          <td style="width:37%;"></td>
        </tr></table>';
$html .='<table cellspacing="0" cellpadding="0" >
       

        <tr>
          <td style="width:37%;">'.$cominfo1.'</td>
          <td style="width:26%;"></td>
         <td style="width:37%;"><div class="test" style="page-break-inside: avoid;"><table cellspacing="0" cellpadding="6" ><tr><td>'.$clientinfo.'</td></tr></table></div>


          <style>   
              div.test {
                
                  border:0.1em solid black;  

              }
          </style></td> 
        </tr> 

        <tr>
          <td></td>
          <td></td>
          <td>Done at : '.$companycity.'&nbsp; Expired on : '.$columnexpireddate.'</td>     
        </tr>        
     </table>
     <style>
        table{
             border-collapse:collapse;
             table-layout: auto;
             width: 100%;    
         }
      
         th{
            border:1px solid #000000;
            text-align:center;
            font-size:10px !important;
         }
        
         td{
           
           text-align:left;
          
         
         }

       
        
     </style>
';
/* Set Company/Client/Quote Information in Table */
$subject=str_replace("{partner_column_name}",$columnname,$reminderdata->subject);

$message=str_replace("{user_civility}",$client->civility,$reminderdata->message);
$message=str_replace("{user_firstname}",$client->first_name,$message);
$message=str_replace("{user_lastname}",$client->last_name,$message);
$message=str_replace("{partner_column_name}",$columnname,$message);
$message=str_replace("{partner_column_expire_date}",$columnexpireddate,$message);

$message="<h4>Subject : ".strtoupper($subject)." </h4>".$message;
    
$remindertitle="EXPIRED ".strtoupper($columnname)."  REMINDER";
$pdf->setFont('helveticaB','',12);
$pdf->cell(30,10,'',0,0,'C');
$pdf->cell(132,10,$remindertitle,1,0,'C');
$pdf->cell(30,10,'',0,1,'C');
$pdf->Ln(4);
//Set Reminder Message


 

$pdf->setFont('helvetica','',11);
$pdf->setCellPaddings( $left = 1, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 3);
$pdf->writeHTMLCell(190,0,10,'',$html,0,1);




//Set Reminder Message
$pdf->setCellPaddings( $left = 1, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 0);
$pdf->setFont('helvetica','',11);

$pdf->writeHTMLCell(190,'','','',$message,0,1);
 $positiony = $pdf->GetY();
//$pdf->cell(192,2,$positiony,1,1,'C');

//signature box

$pdf->setCellPaddings( $left = 0, $top = 0, $right =0, $bottom = 0);
$pdf->setCellMargins($left = 0, $top = 0, $right = 0, $bottom = 0);
/*admin logo */
$adminsignaturelogo=base_url().'uploads/user/'.$user_data['document_signature'];
$adminsignaturelogo = '<img src="'.$adminsignaturelogo.'" width="110" height="95"/>';
/*admin logo*/
$userdeparment=$user_data['department'];
$username='<span>'.$user_data['civility'].' '.$user_data['first_name'].' '.$user_data['last_name'].'</span>';
$stampsection='<table cellspacing="2" cellpadding="1" border="0" nobr="true">
<tr>
<th style="width:67%;"></th>
<th style="width:33%;"><b>'.$userdeparment.' Department</b><br>'.$username.'<br>Signature and Stamp</th>
</tr>

<tr>
<td style="border:none;"></td>
<td  style="border-top:none;">'.$adminsignaturelogo.'</td>
</tr>
</table>
   <style>
        table{
             border-collapse:collapse;
             table-layout: auto;
             width: 100%;  
         }
         th{
           border:none; 
           text-align:left;
         }
         td{   
          border:0.1em  solid #000000;
          text-align:center; 
         }
      </style>   
';

$pdf->setFont('helvetica','',10);
if($positiony < 223){
  $pdf->SetY(-75);
  $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}
elseif($positiony < 263){
   $pdf->Ln(45);
   $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}
else{
   $pdf->Ln(5);
   $pdf->writeHTML($stampsection, $ln=true, $fill=false, $reseth=true, $cell=false, $align='');
}

//signature box


return array('pdf' => $pdf,'fileName' => $fileName);
}