<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('general');
        $this->load->helper('language');
        $this->load->helper('validate');
        if (!$this->basic_auth->is_login())
            redirect("admin", 'refresh');
        else
        $this->data['user'] = $this->basic_auth->user();
        $this->load->model('clients_model');  
        $this->load->model('drivers_model');  
        $this->load->model('request_model');
        $this->load->model('cms_model');
        $this->load->model('base_model');
        $this->load->model('jobs_model');
        $this->load->model('support_model');
        $this->load->model('calls_model');
        $this->load->model('notes_model');
        $this->load->model('notifications_model');
          $this->load->model('userx_model');
        $this->data['configuration'] = get_configuration();

    }

    
    public function index(){


        $this->data['user'] = $this->basic_auth->user();
        $this->data['css_type']     = array("form","booking_datatable","datatable");
        $this->data['active_class'] = "clients";
        $this->data['gmaps']        = false;
        $this->data['title']        = 'Clients';
        $this->data['title_link']   = base_url('admin/clients');
        $this->data['content']      = 'admin/clients/profile';

        $data = [];
        $this->data['countries'] = $this->cms_model->get_all_countries();
        $this->data['client_status_data'] = $this->clients_model->getAllData('vbs_clientstatus');
        $this->data['client_civilite_data'] = $this->clients_model->getAllData('vbs_clientcivilite');
        $this->data['client_delay_data'] = $this->clients_model->getAllData('vbs_clientdelay');
        $this->data['client_payment_data'] = $this->clients_model->getAllData('vbs_clientpayment');
        $this->data['client_type_data'] = $this->clients_model->getAllData('vbs_clienttype');
        $this->data['client_profile_data'] = $this->clients_model->getAllData('vbs_client');
    
        $this->_render_page('templates/admin_template', $this->data);
    }
 public function profileAdd(){

      if ($_SERVER['REQUEST_METHOD'] === 'POST') {
          $this->profileStore();
      }else{
          return redirect("admin/clients");
      }
  }
  public function profileStore(){

          if (empty($error)) {   
               
               $username     = $this->input->post('pre_nom') . ' ' . $this->input->post('nom');
               $email        = strtolower($this->input->post('email'));
               $password     = date("dmY", strtotime($_POST['birthday']));
               $user_role    = 6;
               $isemailexist = $this->clients_model->isemailalreayexist(['email'=>$email],'vbs_users');

               if($isemailexist){
                       $this->session->set_flashdata('alert', [
                           'message' => ": Email is already exist in users.",
                           'class' => "alert-danger",
                           'type' => "Error"
                       ]);
                       redirect('admin/clients', 'refresh');
               }


               $user_group = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('groups') . " WHERE name = 'clients'");

               $group_id        = array($user_group[0]->id);
               $additional_data = array(
                   'civility'     => $this->clients_model->getSingleRecord('vbs_clientcivilite',['id'=>$this->input->post('civilite')])->civilite,
                   'first_name'   => $this->input->post('pre_nom'),
                   'last_name'    => $this->input->post('nom'),
                   'phone'        => $this->input->post('phone'),
                   'country'      => $this->input->post('country'),
                   'region'       => $this->input->post('region'),
                   'city'         => $this->input->post('ville'),
                   'zipcode'      => $this->input->post('code_postal'),
                   'address'      => $this->input->post('address1'),
                   'address1'     => $this->input->post('address2'),
                   'image'        => $this->do_user_upload_image($_FILES['client_image'],'client_image')
               );

               if ($this->ion_auth->register($user_role, $username, $password, $email, $additional_data, $group_id)) {
                   $user_rec = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('users') . " WHERE email = '" . $email . "'");

              $user = $this->basic_auth->user();
              $user_id=$user->id;
              //date converation
             
             $birthdate=str_replace('/', '-', $_POST['birthday']);
             $birthdate=strtotime($birthdate);
             $birthdate = date('Y-m-d',$birthdate);
              //date converation
            
              $id = $this->clients_model->createRecord('vbs_client',[
                  'status'           => $_POST['status'],
                  'civilite'         => $_POST['civilite'],
                  'nom'              => trim($_POST['nom']),
                  'pre_nom'          => trim($_POST['pre_nom']),
                  'company'          => trim($_POST['company']),
                  'address1'         => trim($_POST['address1']),
                  'address2'         => trim($_POST['address2']),
                  'email'            => trim($_POST['email']),
                  'phone'            => trim($_POST['phone']),
                  'mobile'           => trim($_POST['mobile']),
                  'fax'              => trim($_POST['fax']),
                  'code_postal'      => trim($_POST['code_postal']),
                  'ville'            => $_POST['ville'],
                  'region'           => $_POST['region'],
                  'country'          => $_POST['country'],
                  'client_image'     => $this->do_upload_image($_FILES['client_image'],'client_image'),
                  'client_type'      => $_POST['client_type'],
                  'birthday'         => $birthdate,
                  'disabled'         => $_POST['disabled'],
                  'payment_method'   => $_POST['payment_method'],
                  'payment_delay'    => $_POST['payment_delay'],
                  'note'             => trim($_POST['note']),
                  'user_id'          => $user_id,
                  'client_id'        => $user_rec[0]->id
              ]);
          
              $this->session->set_flashdata('alert', [
                  'message'   => ": Successfully added a client profile .",
                  'class'     => "alert-success",
                  'status_id' => "1",
                  'type'      => "Success"
              ]);
              redirect('admin/clients');
            }
          } else {
              $this->data['alert'] = [
                  'message' => 'Please try again',
                  'class'   => "alert-danger",
                  'status_id' => "1",
                  'type'    => "Error"
              ];
              redirect('admin/clients');
          }
  }


    public function profileEdit(){
      

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $id   = $this->input->post('profile_id');
            if (!empty($id)) {

                $client        = $this->clients_model->getSingleRecord('vbs_client',['id'=>$id]);
                $userprofile   = $this->clients_model->getSingleRecord('vbs_users',['id'=>$client->client_id]);

                $client_user_image = '';
                $client_image      = '';
               
                if(!empty($_FILES['client_image']['name']))
                {
                  $client_user_image   = $this->do_user_upload_image($_FILES['client_image'],'client_image');
                }else{
                  $client_user_image   = $userprofile->image;
                }
                if(!empty($_FILES['client_image']['name']))
                {
                  $client_image  = $this->do_upload_image($_FILES['client_image'],'client_image');
                }else{
                  $client_image  = $client->client_image;
                }

   /* $email=$this->input->post('email');
    $isemailexist=$this->clients_model->isemailalreayexist(['id !='=> $userprofile->id,'email'=>$email],'vbs_users');
                if($isemailexist){
                    $this->session->set_flashdata('alert', [
                            'message' => ": Email is already exist.",
                            'class' => "alert-danger",
                            'type' => "Error"
                        ]);
                        redirect('admin/clients', 'refresh');
                }

            $password='';
            if(empty($this->input->post('password'))){
                $password=$userprofile->password;
            }else{
                $password= md5($this->input->post('password'));
            }*/
                 $password=md5(date("dmY", strtotime($_POST['birthday'])));
             $db_array = array(
                    'civility'     => $this->clients_model->getSingleRecord('vbs_clientcivilite',['id'=>$this->input->post('civilite')])->civilite,
                    'first_name'=> $this->input->post('pre_nom'),
                    'last_name' => $this->input->post('nom'),
                    'phone'     => $this->input->post('phone'),
                    'country'   => $this->input->post('country'),
                    'region'    => $this->input->post('region'),
                    'city'      => $this->input->post('ville'),
                    'zipcode'   => $this->input->post('code_postal'),
                    'address'   => $this->input->post('address1'),
                    'address1'  => $this->input->post('address2'),
                    'password'  => $password,
                    'image'     => $client_user_image,
                );

                $updateuserprofile=$this->userx_model->update($db_array,$userprofile->id);
               
                 //date converation
             $birthdate=str_replace('/', '-', $_POST['birthday']);
             $birthdate=strtotime($birthdate);
             $birthdate = date('Y-m-d',$birthdate);
              //date converation
                if($updateuserprofile){
                      $update=$this->clients_model->createUpdate('vbs_client',[
                              'status'           => $_POST['status'],
                              'civilite'         => $_POST['civilite'],
                              'nom'              => trim($_POST['nom']),
                              'pre_nom'          => trim($_POST['pre_nom']),
                              'company'          => trim($_POST['company']),
                              'address1'         => trim($_POST['address1']),
                              'address2'         => trim($_POST['address2']),
                              'phone'            => trim($_POST['phone']),
                              'mobile'           => trim($_POST['mobile']),
                              'fax'              => trim($_POST['fax']),
                              'code_postal'      => trim($_POST['code_postal']),
                              'ville'            => $_POST['ville'],
                              'region'           => $_POST['region'],
                              'country'          => $_POST['country'],
                              'client_image'     => $client_image,
                              'client_type'      => $_POST['client_type'],
                              'birthday'         => $birthdate,
                              'disabled'         => $_POST['disabled'],
                              'payment_method'   => $_POST['payment_method'],
                              'payment_delay'    => $_POST['payment_delay'],
                              'note'             => trim($_POST['note'])
                      ], $id);
                      if ($update){
                          $this->session->set_flashdata('alert', [
                              'message' => ": Successfully updated a client profile.",
                              'class' => "alert-success",
                              'status_id' => "1",
                              'type' => "Success"
                          ]);
                          redirect('admin/clients');
                      }else {
                        $this->data['alert'] = [
                       'message' => 'Please try again',
                       'class' => "alert-danger",
                       'status_id' => "1",
                       'type' => "Error"
                       ];
                       redirect('admin/clients');
                    }
                }else {
                    $this->data['alert'] = [
                    'message' => 'Please try again',
                    'class' => "alert-danger",
                    'status_id' => "1",
                    'type' => "Error"
                     ];
                     redirect('admin/clients');
                }
       
        }else{
           redirect('admin/clients');
          }
     }else{
           redirect('admin/clients');
          }
        
    }
    public function profileDelete(){
        
        $id=$_POST['delete_profile_id'];
        $del=$this->clients_model->createDelete('vbs_client',['id'=>$id]);
        if ($del==true) {
        $this->session->set_flashdata('alert', [
                'message' => "Successfully deleted.",
                'class' => "alert-success",
                'status_id' => "1",
                'type' => "Success"
        ]);
        redirect('admin/clients');
        }
        else {
            $this->session->set_flashdata('alert', [
                    'message' => "Please try again.",
                    'class' => "alert-danger",
                    'status_id' => "1",
                    'type' => "Error"
            ]);
            redirect('admin/clients');
        }
    }
    public function get_ajax_profile_data(){
        $id=(int)$_GET['profile_id'];
       

        if($id>0){

                $data = [];
               
                $this->data['countries'] = $this->cms_model->get_all_countries();
                $this->data['client_contact_data'] = $this->clients_model->getAllData('vbs_client_contact',['profile_id'=>$id]);
                $this->data['client_status_data'] = $this->clients_model->getAllData('vbs_clientstatus');
                $this->data['client_civilite_data'] = $this->clients_model->getAllData('vbs_clientcivilite');
                $this->data['client_delay_data'] = $this->clients_model->getAllData('vbs_clientdelay');
                $this->data['client_payment_data'] = $this->clients_model->getAllData('vbs_clientpayment');
                $this->data['client_type_data'] = $this->clients_model->getAllData('vbs_clienttype');
                $this->data['client'] = $this->clients_model->getSingleRecord('vbs_client',['id'=>$id]);
                $this->data['regions'] = $this->drivers_model->ajax_get_region_listing($this->data['client']->country);
                $this->data['cities'] = $this->drivers_model->ajax_get_cities_listings($this->data['client']->region);
                $this->data['profile_id']=$id;
                $result=$this->load->view('admin/clients/index', $this->data, TRUE);
                echo $result;
                exit();
        }else{
            $result="Please select Record!";
        }
        echo $result;
        exit;
    }
  /*contacts*/
    public function contactAdd(){

      if ($_SERVER['REQUEST_METHOD'] === 'POST') {
          $this->contactStore();
      }else{
          return redirect("admin/clients");
      }
  }
  public function contactStore(){
          

              $user = $this->basic_auth->user();
              $user_id=$user->id;

              $id = $this->clients_model->createRecord('vbs_client_contact',[
                  'status'           => $_POST['status'],
                  'civilite'         => $_POST['civilite'],
                  'nom'              => trim($_POST['nom']),
                  'pre_nom'          => trim($_POST['pre_nom']),
                  'department'       => trim($_POST['department']),
                  'job'              => trim($_POST['job']),
                  'address1'         => trim($_POST['address1']),
                  'address2'         => trim($_POST['address2']),
                  'email'            => trim($_POST['email']),
                  'phone'            => trim($_POST['phone']),
                  'mobile'           => trim($_POST['mobile']),
                  'fax'              => trim($_POST['fax']),
                  'code_postal'      => trim($_POST['code_postal']),
                  'ville'            => $_POST['ville'],
                  'region'           => $_POST['region'],
                  'country'          => $_POST['country'],
                  'user_id'          => $user_id,
                  'profile_id'       => $_POST['profile_id']
              ]);
          if($id){
            $this->data['client_contact_data']=$this->clients_model->getAllData('vbs_client_contact',['profile_id'=>$_POST['profile_id']]);
            $record=$this->load->view('admin/clients/contact/list', $this->data, TRUE);
            $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully added a client contact.",'messageType'=>'Success','className'=>'alert-success'];
            header('Content-Type: application/json'); 
            echo json_encode($data);
            }
           else {
              $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
              header('Content-Type: application/json'); 
              echo json_encode($data);
          }
         
  }


    public function contactEdit(){
      

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $id   = $this->input->post('contact_id');
            if(!empty($id)) {
                $contact = $this->clients_model->getSingleRecord('vbs_client_contact',['id'=>$id]);              
                $update=$this->clients_model->createUpdate('vbs_client_contact',[
                          'status'           => $_POST['status'],
                          'civilite'         => $_POST['civilite'],
                          'nom'              => trim($_POST['nom']),
                          'pre_nom'          => trim($_POST['pre_nom']),
                          'department'       => trim($_POST['department']),
                          'job'              => trim($_POST['job']),
                          'address1'         => trim($_POST['address1']),
                          'address2'         => trim($_POST['address2']),
                          'email'            => trim($_POST['email']),
                          'phone'            => trim($_POST['phone']),
                          'mobile'           => trim($_POST['mobile']),
                          'fax'              => trim($_POST['fax']),
                          'code_postal'      => trim($_POST['code_postal']),
                          'ville'            => $_POST['ville'],
                          'region'           => $_POST['region'],
                          'country'          => $_POST['country']
                      ], $id);
                      if ($update){
                        $this->data['client_contact_data']=$this->clients_model->getAllData('vbs_client_contact',['profile_id'=>$contact->profile_id]);
                          $record=$this->load->view('admin/clients/contact/list', $this->data, TRUE);
                          $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully updated a client contact.",'messageType'=>'Success','className'=>'alert-success'];
                          header('Content-Type: application/json'); 
                          echo json_encode($data);
                      }else {
                        $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                        header('Content-Type: application/json'); 
                        echo json_encode($data);
                    }
             
                  }else {
                    $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
                    }
                }else{
                    $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
                }
    }
    public function contactDelete(){
        
        $id=$_POST['delete_contact_id'];
        $contact = $this->clients_model->getSingleRecord('vbs_client_contact',['id'=>$id]);  
        $del=$this->clients_model->createDelete('vbs_client_contact',['id'=>$id]);
        if ($del==true) {
        $this->data['client_contact_data']=$this->clients_model->getAllData('vbs_client_contact',['profile_id'=>$contact->profile_id]);
          $record=$this->load->view('admin/clients/contact/list', $this->data, TRUE);
          $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully deleted a client contact.",'messageType'=>'Success','className'=>'alert-success'];
          header('Content-Type: application/json'); 
          echo json_encode($data);
        }
        else {
         $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
         header('Content-Type: application/json'); 
         echo json_encode($data);
        }
    }
    public function get_ajax_contact_data(){
        $id=(int)$_GET['contact_id'];

        if($id>0){

                $data = [];
                $this->data['countries'] = $this->cms_model->get_all_countries();
                $this->data['client_status_data'] = $this->clients_model->getAllData('vbs_clientstatus');
                $this->data['client_civilite_data'] = $this->clients_model->getAllData('vbs_clientcivilite');
                $this->data['client_delay_data'] = $this->clients_model->getAllData('vbs_clientdelay');
                $this->data['client_payment_data'] = $this->clients_model->getAllData('vbs_clientpayment');
                $this->data['client_type_data'] = $this->clients_model->getAllData('vbs_clienttype');
                $this->data['contact'] = $this->clients_model->getSingleRecord('vbs_client_contact',['id'=>$id]);
                $this->data['regions'] = $this->drivers_model->ajax_get_region_listing($this->data['contact']->country);
                $this->data['cities'] = $this->drivers_model->ajax_get_cities_listings($this->data['contact']->region);
                $result=$this->load->view('admin/clients/contact/edit', $this->data, TRUE);
                echo $result;
                exit();
        }else{
            $result="Please select Record!";
        }
        echo $result;
        exit;
    }
    /*contacts*/
    function do_upload_image($filename,$name) {
        
        $config['upload_path'] = './uploads/clients/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['max_size'] = '10240';
        $config['file_name'] = str_replace(' ', '_', $filename['name']);
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        
        if (!$this->upload->do_upload($name)) {
            return NULL;
        } else {
            $avatar = $this->upload->data();
            $avatar = $avatar['file_name'];
        
            return $avatar;
        }
        
    }
  


        function do_user_upload_image($filename,$name) {
        
        $config['upload_path'] = './uploads/user/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['max_size'] = '10240';
        $config['file_name'] = str_replace(' ', '_', $filename['name']);
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        
        if (!$this->upload->do_upload($name)) {
            return NULL;
        } else {
            $avatar = $this->upload->data();
            $avatar = $avatar['file_name'];
        
            return $avatar;
        }
        
    }
}
