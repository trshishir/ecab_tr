<?php
class Documents extends MY_Controller{
public function __construct()
    {
        parent::__construct();
        // load form and url helpers
        $data = [];
        $this->load->model('my_model');
        $this->load->model('general_model');
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('general');
        $this->load->helper('language');
        $this->load->helper('validate');
        if (!$this->basic_auth->is_login())
            redirect("admin", 'refresh');
        else
        $data['user'] = $this->basic_auth->user();
        $this->load->model('bookings_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');
        $this->load->model('support_model');
        $this->load->model('calls_model');
        $this->load->model('notes_model');
        $this->load->model('notifications_model');
        $this->load->model('cms_model');
        $this->load->model('userx_model');
        $this->load->model('filemanagement_model');
        $this->load->model('currency_model');
        $this->data['configuration'] = get_configuration();
    }
    public function index()
    {
        $this->data['user'] = $this->basic_auth->user();
        $this->data['css_type']   = array("form","datatable");
        $this->data['active_class'] = "documents";
        $this->data['gmaps']      = false;
        $this->data['title']      = $this->lang->line("documents");
        $this->data['modules'] = $this->general_model->get('vbs_modules');
        $this->data['title_link']     = base_url('admin/documents');
        $this->data['documents'] = $this->my_model->get_table_row_query("SELECT vbs_documents.*,vbs_modules.name as module_name,vbs_users.civility,vbs_users.first_name,vbs_users.last_name FROM vbs_documents INNER JOIN vbs_modules ON vbs_modules.id =vbs_documents.module INNER JOIN vbs_users ON vbs_users.id = vbs_documents.added_by");
        $this->data['content']  = 'admin/documents/index';
        $this->_render_page('templates/admin_template',$this->data);
    }
    public function add_document()
    {
        $this->data['css_type']   = array("form");
        $this->data['active_class'] = "documents";
        $this->data['gmaps']      = false;
        $this->data['title']        = $this->lang->line("documents");
        $this->data['title_link']     = base_url('admin/documents');
        $this->data['subtitle']     = ' > ' .$this->lang->line("add document");
        $this->data['user'] = $this->basic_auth->user();
        $this->data['content']  = 'admin/documents/add_document';
        $this->data['company_data'] = $this->general_model->get('vbs_company');
        $this->data['company_data'] = $this->data['company_data'][0];
        $this->data['modules'] = $this->general_model->get('vbs_modules');
        $this->_render_page('templates/admin_template',$this->data);
    }
    public function store_document()
    {
        extract($_POST);
        $data=array(
            'doc_statut'=>$doc_statut,
            'name'=>$name,
            'module'=>$module,
            'doc_name'=>$doc_name,
            'doc_date'=>$doc_date,
            'custom_department'=>$custom_department,
            'custom_user'=>$custom_user,
            'added_by'=>$this->session->userdata('user_id'),
            'created_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->general_model->add('vbs_documents',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                    'message' => "Document Added Successfully.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
            redirect('admin/documents');
        }
    }
    public function edit_document($id)
    {
        $this->data['css_type']   = array("form","datatable");
        $this->data['active_class'] = "dashboard";
        $this->data['gmaps']      = false;
        $this->data['title']        = $this->lang->line("documents");
        $this->data['title_link']     = base_url('admin/documents');
        $this->data['user'] = $this->basic_auth->user();
        $this->data['document'] = $this->my_model->get_table_row_query("SELECT * FROM vbs_documents where id = '".$id."'");
        $this->data['document'] = $this->data['document'][0];
        $this->data['subtitle']     = '>'.' '.create_timestamp_uid($data['document']['created_at'],$id);
        $this->data['company_data'] = $this->general_model->get('vbs_company');
        $this->data['company_data'] = $this->data['company_data'][0];
        $this->data['content']  = 'admin/documents/edit_document';
        $this->_render_page('templates/admin_template',$this->data);
    }
    public function update_document()
    {
        extract($_POST);
        $data=array(
            'doc_statut'=>$doc_statut,
            'name'=>$name,
            'module'=>$module,
            'doc_name'=>$doc_name,
            'doc_date'=>$doc_date,
            'custom_department'=>$custom_department,
            'custom_user'=>$custom_user,
            'added_by'=>$this->session->userdata('user_id'),
            'updated_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->my_model->update_table(array('id'=>$id),'vbs_documents',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                    'message' => "Document Updated Successfully.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
            redirect('admin/documents');
        }
    }
    public function view_document($id){
        $this->data['css_type']   = array("form","datatable");
        $this->data['active_class'] = "dashboard";
        $this->data['gmaps']      = false;
        $this->data['title']        = $this->lang->line("documents");
        $this->data['title_link']     = base_url('admin/documents');
        $this->data['document'] = $this->my_model->get_table_row_query("SELECT * FROM vbs_documents where id = '".$id."'");
        $this->data['document'] = $this->data['document'][0];
        $this->data['subtitle']     = '>'.' '.create_timestamp_uid($this->data['document']['created_at'],$id);
        $this->data['company_data'] = $this->general_model->get('vbs_company');
        $this->data['company_data'] = $this->data['company_data'][0];
        $this->data['content']  = 'admin/documents/view_document';
        $this->_render_page('templates/admin_template',$this->data);
    }
    public function get_users_against_category($id){
        extract($_POST);
        // $allUser = $this->general_model->get_bywhere_array('',)
    }
    
}
?>