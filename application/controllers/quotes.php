<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quotes extends MY_Controller
{

  function __construct()

  {

    parent::__construct();



    $this->load->library('form_validation');
    $this->load->helper('url');
    $this->load->library('session');
    $this->lang->load('auth');
    $this->lang->load('general');
    $this->load->helper('language');
    $this->load->helper('validate');
    $this->load->helper('mypdf');

    if (!$this->basic_auth->is_login())

      redirect("admin", 'refresh');

    else

      $this->data['user'] = $this->basic_auth->user();


        $this->load->model('quotes_model');
        $this->load->model('invoice_model');

    $this->load->model('bookings_model');

    $this->load->model('bookings_config_model');

    $this->load->model('request_model');

    $this->load->model('jobs_model');

    $this->load->model('support_model');

    $this->load->model('calls_model');

        $this->load->model('notes_model');

        $this->load->model('notifications_model');



    $this->data['configuration'] = get_configuration();

  }
public function index(){
  $this->data['css_type']   = array("form","booking_datatable","datatable");

    $this->data['active_class'] = "quotes";

    $this->data['gmaps']    = false;

    $this->data['title']    = $this->lang->line("quotes");

    $this->data['title_link']   = base_url('admin/quotes');

    $this->data['content']    = 'admin/quotes/index';

    $this->load->model('calls_model');

    $this->load->model('request_model');

    $this->load->model('jobs_model');

    $data = [];
        $this->data['googleapikey']=$this->bookings_model->getgoogleapikey("vbs_googleapi");
    $this->data['jobs']    = $this->jobs_model->getAll();
 $flashbookdata =  $this->session->flashdata('bookingsearchdata');
      if(isset($flashbookdata['record']) ){
           $this->data['quotes'] = $flashbookdata['record'];
           $this->data['search_keyword']=$flashbookdata['fields']['search_keyword'];
           $this->data['search_otherkeyword']=$flashbookdata['fields']['search_otherkeyword'];
           $this->data['search_from_period']=$flashbookdata['fields']['search_from_period'];
           $this->data['search_to_period']=$flashbookdata['fields']['search_to_period'];
           $this->data['search_service']=$flashbookdata['fields']['search_service'];
           $this->data['search_methods']=$flashbookdata['fields']['search_methods'];
           $this->data['search_regular']=$flashbookdata['fields']['search_regular'];
           $this->data['search_status']=$flashbookdata['fields']['search_status'];
      }else{
          $this->data['quotes'] = $this->quotes_model->getquotesallrecords();
      }
  
      $this->data['status'] = $this->bookings_model->booking_Config_getAll('vbs_u_bookingstatut');
      $this->data['poi_data'] = $this->bookings_model->booking_Config_getAll('vbs_u_poi');
      $this->data['poi_package'] = $this->bookings_model->booking_Config_getAll('vbs_u_package');
        $this->data['paymentmethods'] = $this->bookings_model->booking_Config_getAll(' vbs_u_payment_method');
         
      $this->data['service_cat'] = $this->bookings_model->booking_Config_getAll('vbs_u_category_service');
      $this->data['service'] = $this->bookings_model->booking_Config_getAll('vbs_u_service');
      $this->data['car_data'] = $this->bookings_model->getcarsdata();
      $this->data['wheelcar_data'] = $this->bookings_model->getwheelcarsdata();
      $this->data['statut_data'] = $this->bookings_config_model->getAllStatut();
      $this->data['clients_data']=$this->bookings_model->getusersbyrole("clients");
      $this->data['drivers_data']=$this->bookings_model->getusersbyrole("drivers");
       
        //booking add record
         
        $this->data['poi_data'] = $this->bookings_model->booking_Config_getAll('vbs_u_poi');
        $this->data['poi_package'] = $this->bookings_model->booking_Config_getAll('vbs_u_package');
        $this->data['service_cat'] = $this->bookings_model->booking_Config_getAll('vbs_u_category_service');
        $this->data['service'] = $this->bookings_model->booking_Config_getAll('vbs_u_service');
        $this->data['car_data'] = $this->bookings_model->getcarsdata();
        $this->data['wheelcar_data'] = $this->bookings_model->getwheelcarsdata();
        $this->data['googleapikey']=$this->bookings_model->getgoogleapikey("vbs_googleapi");
       //remove prev session
         if($this->session->userdata['passengers']){
         $this->session->unset_userdata('passengers');
      }
       if($this->session->userdata['bookingdata']){
         $this->session->unset_userdata('bookingdata');
      }
       if($this->session->userdata['bookingpickregular']){
         $this->session->unset_userdata('bookingpickregular');
      }
       if($this->session->userdata['bookingreturnregular']){
         $this->session->unset_userdata('bookingreturnregular');
      }
       if($this->session->userdata['mapdatainfo']){
         $this->session->unset_userdata('mapdatainfo');
      }

       //remove prev session
     
    $this->_render_page('templates/admin_template', $this->data);
}

public function delete_quotes_record(){
  
  $bookingid=$_POST['delet_booking_id'];
  $quote=$this->bookings_model->getdbbookingrecord('vbs_quotes',array('booking_id'=>$bookingid));
   $invoices=$this->bookings_model->getdbbookingrecord('vbs_invoices',array('booking_id'=>$bookingid));
   $del=$this->quotes_model->deletequotedata('vbs_quotes',['id'=>$quote->id]);
   $invoicedel=$this->quotes_model->deletequotedata('vbs_invoices',['booking_id'=>$bookingid]);
   $reminderdel=$this->invoice_model->deleteinvoicedata('vbs_invoices_reminder',['invoice_id'=>$invoices->id]);
  $notificationdel=$this->invoice_model->deleteinvoicedata('vbs_invoices_notification',['invoice_id'=>$invoices->id]);
  $customreindersdel=$this->invoice_model->deleteinvoicedata('vbs_invoices_customreminders',['invoice_id'=>$invoices->id]);
  $custompaymentsdel=$this->invoice_model->deleteinvoicedata('vbs_invoices_custompayments',['invoice_id'=>$invoices->id]);
   $quotereminderdel=$this->quotes_model->deletequotedata('vbs_quotes_reminder',['quote_id'=>$quote->id]);
   $quotenotificationdel=$this->quotes_model->deletequotedata('vbs_quotes_notification',['quote_id'=>$quote->id]);
   $quotecustomreminderdel=$this->quotes_model->deletequotedata('vbs_quotes_customreminders',['quote_id'=>$quote->id]);
  if($del){
 $this->session->set_flashdata('alert', [
        'message' => "Successfully deleted.",
        'class' => "alert-success",
        'type' => "Success"
    ]);
   redirect('admin/quotes');
  }else{
     $this->session->set_flashdata('alert', [
        'message' => "Please try again.",
        'class' => "alert-danger",
        'type' => "Error"
    ]);
    redirect('admin/quotes');
  }
}
//Edit Section
 public function get_ajax_quotes(){
  $booking_id=$_GET['booking_id'];
   $notificationpendingstatus="1";
   $reminderpendingstatus="2";

  //add index function data
        $this->data['css_type']   = array("form","booking_datatable","datatable");

    $this->data['active_class'] = "quotes";

    $this->data['gmaps']    = false;

    $this->data['title']    = $this->lang->line("quotes");

    $this->data['title_link']   = base_url('admin/quotes');

    $this->data['content']    = 'admin/quotes/index';

        $data = [];

        $this->data['clients_data']=$this->bookings_model->getusersbyrole("clients");
        $this->data['drivers_data']=$this->bookings_model->getusersbyrole("drivers");
       
        //booking add record
         
        $this->data['poi_data'] = $this->bookings_model->booking_Config_getAll('vbs_u_poi');
        $this->data['poi_package'] = $this->bookings_model->booking_Config_getAll('vbs_u_package');
        $this->data['service_cat'] = $this->bookings_model->booking_Config_getAll('vbs_u_category_service');
        $this->data['service'] = $this->bookings_model->booking_Config_getAll('vbs_u_service');
        $this->data['car_data'] = $this->bookings_model->getcarsdata();
        $this->data['wheelcar_data'] = $this->bookings_model->getwheelcarsdata();
        $this->data['googleapikey']=$this->bookings_model->getgoogleapikey("vbs_googleapi");
      
        $this->data['quotes']=$this->bookings_model->getdbbookingrecord('vbs_quotes',array('booking_id'=>$booking_id));
        $this->data['dbbookingdata']=$this->bookings_model->getdbbookingrecord('vbs_bookings',array('id'=>$booking_id));
     
        $this->data['dbpickairportdata']=$this->bookings_model->getdbbookingrecord('vbs_booking_airport',array('booking_id'=>$booking_id,'type'=>'pickup'));
        $this->data['dbpicktraindata']=$this->bookings_model->getdbbookingrecord('vbs_booking_train',array('booking_id'=>$booking_id,'type'=>'pickup'));
        $this->data['dbpickaddressdata']=$this->bookings_model->getdbbookingrecord('vbs_booking_address',array('booking_id'=>$booking_id,'type'=>'pickup'));

         $this->data['dbdropairportdata']=$this->bookings_model->getdbbookingrecord('vbs_booking_airport',array('booking_id'=>$booking_id,'type'=>'dropoff'));
        $this->data['dbdroptraindata']=$this->bookings_model->getdbbookingrecord('vbs_booking_train',array('booking_id'=>$booking_id,'type'=>'dropoff'));
        $this->data['dbdropaddressdata']=$this->bookings_model->getdbbookingrecord('vbs_booking_address',array('booking_id'=>$booking_id,'type'=>'dropoff'));
        $this->data['dbpickregulardata']=$this->bookings_model->getdbmultiplebookingrecord('vbs_booking_regschedule',array('booking_id'=>$booking_id,'type'=>'regular'));
        $this->data['dbreturndata']=$this->bookings_model->getdbmultiplebookingrecord('vbs_booking_regschedule',array('booking_id'=>$booking_id,'type'=>'return'));
        $dbpassengers=$this->bookings_model->getdbpassengersbookingrecord(array('booking_id'=>$booking_id));
       $this->data['dbstopsdata']=$this->bookings_model->getdbmultiplebookingrecord('vbs_booking_stops',array('booking_id'=>$booking_id));
        $this->data['dbdiscountandfeedata']=$this->bookings_model->getdbbookingrecord('vbs_bookings_pricevalues',array('booking_id'=>$booking_id));
        $this->data['dbcreditcarddata']=$this->bookings_model->getdbbookingrecord('vbs_booking_creditcard',array('booking_id'=>$booking_id));
        $this->data['dbdebitbankdata']=$this->bookings_model->getdbbookingrecord('vbs_booking_directdebit',array('booking_id'=>$booking_id));
        $this->data["paymentmethods"]=$this->bookings_model->paymentmethods();
        $this->data['companydata']=$this->bookings_model->getcompanystatus();
      
        $this->data['quotenotifications']=$this->quotes_model->getmultiplerecord("vbs_quotes_notification",['quote_id'=>$this->data['quotes']->id]);
         $this->data['quotefirstreminders']=$this->quotes_model->getmultiplerecord("vbs_quotes_reminder",['quote_id'=>$this->data['quotes']->id,'type'=>'1']);
          $this->data['quotesecondreminders']=$this->quotes_model->getmultiplerecord("vbs_quotes_reminder",['quote_id'=>$this->data['quotes']->id,'type'=>'2']);
           $this->data['quotethirdreminders']=$this->quotes_model->getmultiplerecord("vbs_quotes_reminder",['quote_id'=>$this->data['quotes']->id,'type'=>'3']);
         $module='2';
         $statuspending='2';
         $this->data['reminders']  = $this->quotes_model->getmultiplerecord('vbs_reminders',['module' => $module,'status'=> $statuspending]);

          $this->data['quotecustomreminders']=$this->quotes_model->getmultiplerecord('vbs_quotes_customreminders',['quote_id'=> $this->data['quotes']->id]);
        if($this->session->userdata['passengers']){
          $this->session->unset_userdata('passengers');
        }
       $bookingstore['passengers']=array();
        foreach($dbpassengers as $p){
         $bookingstore['passengers'][$p->id]= array(
                   'id'          => $p->id,
                   'name'        => $p->civility.' '.$p->fname.' '.$p->lname,
                   'phone'       => $p->mobilephone,
                   'home'        => $p->homephone,
                   'disable'     => $p->name_of_disablecategory,
                   'disableicon' => 'fa fa-check'

              );
        
        }
      
        $this->session->set_userdata($bookingstore);
        
  $result=$this->load->view('admin/quotes/edit_booking_box', $this->data, TRUE);
  echo $result;
  exit();
 }
  public function update_booking_data(){
    $id=$this->input->post('booking_id');
    if(empty($id) || $id == '0'){
       return redirect("admin/quotes");
    }
  
    //declare variable 
       //pickup category variables
       $pickupcategory="";$pickaddressfield="";$pickotheraddressfield="";
       $pickzipcodefield="";$pickcityfield="";
       $pickpackagefield="";$pickpackagename="";
       $pickairportfield="";$pickairportname="";$pickairportflightnumberfield="";$pickairportarrivaltimefield="";
       $picktrainfield="";$picktrainname="";$picktrainnumberfield="";$picktrainarrivaltimefield="";
       $pickhotelfield="";$pickhotelname="";
       $pickparkfield="";$pickparkname="";
       $pickmondaytype=0;$pickthuesdaytype=0;$pickwednesdaytype=0;$pickthursdaytype=0;$pickfridaytype=0;$picksaturdaytype=0;
       $picksundaytype=0;
       $pickmonday="";$pickthuesday="";$pickwednesday="";$pickthursday="";$pickfriday="";$picksaturday="";$picksunday="";
       $pickdatefield="";$picktimefield="";$waitingtimefield="";$returndatefield="";$returntimefield="";
     
       //dropoff category variables
       $dropoffcategory="";$dropaddressfield="";$dropotheraddressfield="";$dropzipcodefield="";$dropcityfield="";
       $droppackagefield="";$droppackagename="";
       $dropairportfield="";$dropairportname="";$dropairportflightnumberfield="";$dropairportarrivaltimefield="";
       $droptrainfield="";$droptrainname="";$droptrainnumberfield="";$droptrainarrivaltimefield="";
       $drophotelfield="";$drophotelname="";
       $dropparkfield="";$dropparkname="";
       $returnmondaytype=0;$returnthuesdaytype=0;$returnwednesdaytype=0;$returnthursdaytype=0;$returnfridaytype=0;$returnsaturdaytype=0;
       $returnsundaytype=0;
       $returnmonday="";$returnthuesday="";$returnwednesday="";$returnthursday="";$returnfriday="";$returnsaturday="";$returnsunday="";
       $service="";$servicecategory="";
      

       //other vairable  
       $regularcheckfield=0;$returncheckfield=0;$wheelcarcheckfield=0;
       $starttimefield="";$startdatefield="";$endtimefield="";$enddatefield="";
       $carfield="";      
       $mapaddress="";$dropmapaddress="";
      //end declare variable
        $date=date("Y-m-d");

   //stopper section
        $stopsdata=array();
        if(isset($_POST['stopaddress'])){
          
               $stopaddressfield=$this->input->post('stopaddress');
               $stopwaittimefield=$this->input->post('stopwaittime');
               $stopcount=count($stopaddressfield);

               if($stopcount > 0){
                 $stopno=0;
                 for($i=0;$i<count($stopaddressfield);$i++){
                 if(!empty($stopaddressfield[$i])){
                 $stopsdata[$stopno]= array(
                   'stopaddress'  => $stopaddressfield[$i],
                   'stopwaittime' => $stopwaittimefield[$i]

                 );
                      $stopno++;
                 }
                }               
              }
           
        }
  //stopper section
//get last booking id from booking table 
$bookingid=$this->bookings_model->getlastbookingid();
//if booking id is not exist then give id to 1
  if(!$bookingid){
    $bookingid=1;
   }    

 $service=$this->input->post('pickservice');
 $servicecategory=$this->input->post('pickservicecategory');
 $booking_status=$this->input->post('booking_status');
 $client_id=$this->input->post('client_field');
 $booking_comment=$this->input->post('booking_comment');
 $booking_adminnote=$this->input->post('booking_adminnote');
 $pickupcategory=$this->input->post("pickupcategory");
 $dropoffcategory=$this->input->post("dropoffcategory");


 if(!empty($this->input->post("regularcheckfield"))){
    $regularcheckfield=1;          
 }
 if(!empty($this->input->post("returncheckfield"))){
    $returncheckfield=1;    
 }
 if(!empty($this->input->post("wheelcarcheckfield"))){
    $wheelcarcheckfield=1;    
 }

if(empty($this->input->post("regularcheckfield"))){     
    $picktimefield=$this->input->post("picktimefield");
    $pickdatefield=$this->input->post("pickdatefield");
 }
if(!empty($this->input->post("returncheckfield")) && empty($this->input->post("regularcheckfield"))){
     $waitingtimefield=$this->input->post("waitingtimefield");
     $returntimefield=$this->input->post("returntimefield");
     $returndatefield=$this->input->post("returndatefield");
}
if(!empty($this->input->post("regularcheckfield")) ){
   $startdatefield=$this->input->post("startdatefield");
   $starttimefield=$this->input->post("starttimefield");
   $enddatefield=$this->input->post("enddatefield");
   $endtimefield=$this->input->post("endtimefield");
}



//get pickup fields by category
 if($pickupcategory=="address"){
         
    $pickaddressfield=$this->input->post("pickaddressfield");
    $pickotheraddressfield=$this->input->post("pickotheraddressfield");
    $pickzipcodefield=$this->input->post("pickzipcodefield");
    $pickcityfield=$this->input->post("pickcityfield");
    $mapaddress= $pickaddressfield;
}
elseif($pickupcategory=="package"){
    
    $pickpackagefield=$this->input->post("pickpackagefield");
    $packagerecord=$this->bookings_model->poidatarecord('vbs_u_package',$pickpackagefield);
    $poirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$packagerecord['departure']);
    $pickpackagename=$packagerecord['name'];
    $pickzipcodefield=$poirecord['postal'];
    $pickcityfield=$poirecord['ville'];
    $mapaddress= $poirecord['address'].', '.$poirecord['ville'];
}
elseif($pickupcategory=="airport"){
   
    $pickairportfield=$this->input->post("pickairportfield");
    $pickairportflightnumberfield=$this->input->post("pickairportflightnumberfield");
    $pickairportarrivaltimefield=$this->input->post("pickairportarrivaltimefield");
    $airportrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$pickairportfield);
    $pickairportname=$airportrecord['name'];
    $pickzipcodefield=$airportrecord['postal'];
    $pickcityfield=$airportrecord['ville'];
     $mapaddress=  $airportrecord['address'].', '.$airportrecord['ville'];
}
elseif($pickupcategory=="train"){
  
    $picktrainfield=$this->input->post("picktrainfield");
    $picktrainnumberfield=$this->input->post("picktrainnumberfield");
    $picktrainarrivaltimefield=$this->input->post("picktrainarrivaltimefield");
    $trainrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$picktrainfield);
    $picktrainname=$trainrecord['name'];
    $pickzipcodefield=$trainrecord['postal'];
    $pickcityfield=$trainrecord['ville'];
    $mapaddress=  $trainrecord['address'].', '.$trainrecord['ville'];
}
elseif($pickupcategory=="hotel"){
    
    $pickhotelfield=$this->input->post("pickhotelfield");
    $hotelrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$pickhotelfield);
    $pickhotelname=$hotelrecord['name'];
    $pickzipcodefield=$hotelrecord['postal'];
    $pickcityfield=$hotelrecord['ville'];
    $mapaddress=  $hotelrecord['address'].', '.$hotelrecord['ville'];
}
elseif($pickupcategory=="park"){
    
    $pickparkfield=$this->input->post("pickparkfield");
    $parkrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$pickparkfield);
    $pickparkname=$parkrecord['name'];
    $pickzipcodefield=$parkrecord['postal'];
    $pickcityfield=$parkrecord['ville'];  
    $mapaddress=  $parkrecord['address'].', '.$parkrecord['ville'];       
}

//end pickup fields by category

//get dropoff fields by category
if($dropoffcategory=="address"){
  
    $dropaddressfield=$this->input->post("dropaddressfield");
    $dropotheraddressfield=$this->input->post("dropotheraddressfield");
    $dropzipcodefield=$this->input->post("dropzipcodefield");
    $dropcityfield=$this->input->post("dropcityfield");
    $dropmapaddress=  $dropaddressfield;
}
elseif($dropoffcategory=="package"){
   
    $droppackagefield=$this->input->post("droppackagefield");
    $packagerecord=$this->bookings_model->poidatarecord('vbs_u_package',$droppackagefield);
    $droppackagename=$packagerecord['name'];
    $poirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$packagerecord['destination']);
    $dropzipcodefield=$poirecord['postal'];
    $dropcityfield=$poirecord['ville'];   
    $dropmapaddress= $poirecord['address'].', '.$poirecord['ville'];       
}
elseif($dropoffcategory=="airport"){
    
    $dropairportfield=$this->input->post("dropairportfield");
    $dropairportflightnumberfield=$this->input->post("dropairportflightnumberfield");
    $dropairportarrivaltimefield=$this->input->post("dropairportarrivaltimefield");
    $airportrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$dropairportfield);
    $dropairportname=$airportrecord['name'];
    $dropzipcodefield=$airportrecord['postal'];
    $dropcityfield=$airportrecord['ville'];
    $dropmapaddress=  $airportrecord['address'].', '.$airportrecord['ville'];
}
elseif($dropoffcategory=="train"){
    
    $droptrainfield=$this->input->post("droptrainfield");
    $droptrainnumberfield=$this->input->post("droptrainnumberfield");
    $droptrainarrivaltimefield=$this->input->post("droptrainarrivaltimefield");
    $trainrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$droptrainfield);
    $droptrainname=$trainrecord['name'];
    $dropzipcodefield=$trainrecord['postal'];
    $dropcityfield=$trainrecord['ville'];
    $dropmapaddress=  $trainrecord['address'].', '.$trainrecord['ville'];
}
elseif($dropoffcategory=="hotel"){
    
    $drophotelfield=$this->input->post("drophotelfield");
    $hotelrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$drophotelfield);
    $drophotelname=$hotelrecord['name'];
    $dropzipcodefield=$hotelrecord['postal'];
    $dropcityfield=$hotelrecord['ville'];
    $dropmapaddress=  $hotelrecord['address'].', '.$hotelrecord['ville'];
}
elseif($dropoffcategory=="park"){
     
    $dropparkfield=$this->input->post("dropparkfield");
    $parkrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$dropparkfield);
    $dropparkname=$parkrecord['name'];
    $dropzipcodefield=$parkrecord['postal'];
    $dropcityfield=$parkrecord['ville'];
    $dropmapaddress=  $parkrecord['address'].', '.$parkrecord['ville'];

}
//end dropoff fields by category

// regular day and time fields
if(!empty($this->input->post("pickmondaycheckfield"))){
$pickmonday=$this->input->post("pickmondayfield");
$pickmondaytype=1;
}
if(!empty($this->input->post("picktuesdaycheckfield"))){
$pickthuesday=$this->input->post("picktuesdayfield");
$pickthuesdaytype=1;
}
if(!empty($this->input->post("pickwednesdaycheckfield"))){
$pickwednesday=$this->input->post("pickwednesdayfield");
$pickwednesdaytype=1;
}
if(!empty($this->input->post("pickthursdaycheckfield"))){
$pickthursday=$this->input->post("pickthursdayfield");
$pickthursdaytype=1;
}
if(!empty($this->input->post("pickfridaycheckfield"))){
$pickfriday=$this->input->post("pickfridayfield");
$pickfridaytype=1;
}
if(!empty($this->input->post("picksaturdaycheckfield"))){
$picksaturday=$this->input->post("picksaturdayfield");
$picksaturdaytype=1;
}
if(!empty($this->input->post("picksundaycheckfield"))){
$picksunday=$this->input->post("picksundayfield");
$picksundaytype=1;
}

$bookingpickregular=array(
   array("day"=>"Monday","type"=>$pickmondaytype,"time"=>$pickmonday),
   array("day"=>"Tuesday","type"=>$pickthuesdaytype,"time"=>$pickthuesday),
   array("day"=>"Wednesday","type"=>$pickwednesdaytype,"time"=>$pickwednesday),
   array("day"=>"Thursday","type"=>$pickthursdaytype,"time"=>$pickthursday),
   array("day"=>"Friday","type"=>$pickfridaytype,"time"=>$pickfriday),
   array("day"=>"Saturday","type"=>$picksaturdaytype,"time"=>$picksaturday),
   array("day"=>"Sunday","type"=>$picksundaytype,"time"=>$picksunday),
);

//return day and time check fields
if(!empty($this->input->post("returnmondaycheckfield"))){
$returnmonday=$this->input->post("returnmondayfield");
$returnmondaytype=1;
}
if(!empty($this->input->post("returntuesdaycheckfield"))){
$returnthuesday=$this->input->post("returntuesdayfield");
$returnthuesdaytype=1;
}
if(!empty($this->input->post("returnwednesdaycheckfield"))){
$returnwednesday=$this->input->post("returnwednesdayfield");
$returnwednesdaytype=1;
}
if(!empty($this->input->post("returnthursdaycheckfield"))){
$returnthursday=$this->input->post("returnthursdayfield");
$returnthursdaytype=1;
}
if(!empty($this->input->post("returnfridaycheckfield"))){
$returnfriday=$this->input->post("returnfridayfield");
$returnfridaytype=1;
}
if(!empty($this->input->post("returnsaturdaycheckfield"))){
$returnsaturday=$this->input->post("returnsaturdayfield");
$returnsaturdaytype=1;
}
if(!empty($this->input->post("returnsundaycheckfield"))){
$returnsunday=$this->input->post("returnsundayfield");
$returnsundaytype=1;
}
$bookingreturnregular=array(
   array("day"=>"Monday","type"=>$returnmondaytype,"time"=>$returnmonday),
   array("day"=>"Tuesday","type"=>$returnthuesdaytype,"time"=>$returnthuesday),
   array("day"=>"Wednesday","type"=>$returnwednesdaytype,"time"=>$returnwednesday),
   array("day"=>"Thursday","type"=>$returnthursdaytype,"time"=>$returnthursday),
   array("day"=>"Friday","type"=>$returnfridaytype,"time"=>$returnfriday),
   array("day"=>"Saturday","type"=>$returnsaturdaytype,"time"=>$returnsaturday),
   array("day"=>"Sunday","type"=>$returnsundaytype,"time"=>$returnsunday),
);
//return day and time section

//get cars by car type if car is wheelchair or not
$cartype=$this->input->post('carstypes');
if($cartype=="1"){
  $carfield=$this->input->post('wheelcarfield');
}else{
   $carfield=$this->input->post('carfield');
}
$car_name=$this->bookings_model->getcarname($carfield);

$customprice=$_POST['customprice'];
$bookingvalues=$this->bookings_model->getdbbookingrecord('vbs_bookings_pricevalues',array('booking_id'=>$id));

$arraysofbooking=array('pickdatefield'=>$pickdatefield,'picktimefield'=>$picktimefield,'startdatefield'=>$startdatefield,'starttimefield'=>$starttimefield,'returndatefield'=>$returndatefield,'returntimefield'=>$returntimefield,'enddatefield'=>$enddatefield,'endtimefield'=>$endtimefield,'waitingtimefield'=>$waitingtimefield,'mapaddress'=>$mapaddress,'dropmapaddress'=>$dropmapaddress,'carfield'=>$carfield,'regularcheckfield'=>$regularcheckfield,'returncheckfield'=>$returncheckfield,'service'=>$service,'client_id'=>$client_id,'customprice'=>$customprice,'promocodediscount'=>$bookingvalues->promocodediscount);

$bookingpricedata=$this->calculateBookingPrice($arraysofbooking,$stopsdata);

if($bookingpricedata){
      //Payment method and insert section

  $payment_method=$this->input->post("payment_method");
  $paymentstatus=$this->input->post('paymentstatus');
  $methods=$this->bookings_model->getpaymentmethod('vbs_u_payment_method',$payment_method);
  $clientdata=$this->bookings_model->getclientdatabyid($client_id);
  if($payment_method){
        $pickupdate;$returndate;$startdate;$enddate;
          if(!empty($pickdatefield)){
             $pickupdate=str_replace('/', '-', $pickdatefield);
             $pickupdate=strtotime($pickupdate);
             $pickupdate = date('Y-m-d',$pickupdate);
          }
          if(!empty($returndatefield)){
             $returndate=str_replace('/', '-', $returndatefield);
             $returndate=strtotime($returndate);
             $returndate = date('Y-m-d',$returndate);
          }
          if(!empty($startdatefield)){
             $startdate=str_replace('/', '-', $startdatefield);
             $startdate=strtotime($startdate);
             $startdate = date('Y-m-d',$startdate);
          }
          if(!empty($enddatefield)){
             $enddate=str_replace('/', '-', $enddatefield);
             $enddate=strtotime($enddate);
             $enddate = date('Y-m-d',$enddate);
          }


          if(!empty($pickdatefield)){
               $bookingdate=$pickupdate;
          }
          else{
                $bookingdate=$startdate;
              }

      //inser data into database
          $dataupdate = $this->bookings_model->updatebookingrecord('vbs_bookings',[
    'user_id'            => $client_id,
    'pick_date'          => $pickupdate,
    'pick_time'          => $picktimefield,
    'pick_point'         => $this->cleanstr($bookingpricedata['pickupaddress']),
    'drop_point'         => $this->cleanstr($bookingpricedata['dropoffaddress']),
    'totaldiscount'      => $bookingpricedata['totaldiscount'],
    'distance'           => $bookingpricedata['distance'],
    'vehicle_selected'   => $carfield,
    'cost_of_journey'    => $bookingpricedata['price'],
    'price'              => $bookingpricedata['price'],
    'totalprice'         => $bookingpricedata['totalprice'],
    'registered_name'    => $clientdata->username,
    'phone'              => $clientdata->phone,
    'email'              => $clientdata->email,
    'payer_id'           => 0,
    'package_type'       => "Null",
    'info_to_drivers'    => "Null",
    'payer_name'         => "Null",
    'is_new'             => 0,
    'unread'             => 0,
    'time_of_journey'    => $bookingpricedata['time'],
    'pickupcategory'     => $pickupcategory,
    'dropoffcategory'    => $dropoffcategory,
    'waiting_time'       => $waitingtimefield,
    'return_time'        => $returntimefield,
    'return_date'        => $returndate,
    'start_time'         => $starttimefield,
    'start_date'         => $startdate,
    'end_time'           => $endtimefield,
    'end_date'           => $enddate,
    'car_id'             => $carfield,
    'pickuppackage_id'   => $pickpackagefield,
    'droppackage_id'     => $droppackagefield,
    'pickuphotel_id'     => $pickhotelfield,
    'drophotel_id'       => $drophotelfield,
    'pickuppark_id'      => $pickparkfield,
    'droppark_id'        => $dropparkfield,
    'regular'            => $regularcheckfield,
    'returncheck'        => $returncheckfield,
    'wheelchair'         => $wheelcarcheckfield,
    'payment_method_id'  => $payment_method,
    'service_id'         => $service,
    'service_category_id'=> $servicecategory,
    'booking_comment'    => $booking_comment,
    'booking_adminnote'    => $booking_adminnote, 
    'ridedate'           => $bookingdate,
    'realrideprice'      => $bookingpricedata['realrideprice'],
    'realridetotalprice' => $bookingpricedata['realridetotalprice'],
    'customprice'        => (isset($_POST['customprice']) ? $_POST['customprice'] : '0'),
    'pickzipcode'        => $pickzipcodefield,
    'pickcity'           => $pickcityfield,
    'dropzipcode'        => $dropzipcodefield,
    'dropcity'           => $dropcityfield

  ],['id'=>$id]);
         
                
    if($dataupdate){
               
                $delprevdata=$this->delprevbookingdata($id);
     
              //update to quotes
                $approveddate=date("Y-m-d");
                $quote_id= $_POST['quote_id'];
                $quotes=$this->bookings_model->getdbbookingrecord('vbs_quotes',array('id'=>$quote_id));
               
              //add custom comment
            /*$customreminderdatefield=$this->input->post('customreminderdatefield');
            $customremindertimefield=$this->input->post("customremindertimefield");
            $customremindertitlefield=$this->input->post("customremindertitlefield");
            $customremindercommentfield=$this->input->post("customremindercommentfield");

            $del=$this->quotes_model->deletequotedata('vbs_quotes_customreminders',['quote_id'=>$quote_id]);
          
                for($i=0;$i<count($customreminderdatefield);$i++){
                  if(!empty($customremindertitlefield[$i])){
                     $date=$customreminderdatefield[$i];
                     $date=str_replace('/', '-', $date);
                     $date=strtotime($date);
                     $date = date('Y-m-d',$date);
                      $crid = $this->quotes_model->addrecord("vbs_quotes_customreminders",[
                        'date'     => $date,
                        'time'     => $customremindertimefield[$i],
                        'title'    => $customremindertitlefield[$i],
                        'comment'  => $customremindercommentfield[$i],
                        'quote_id' => $quote_id
                        ]);
                    }
                  }*/
              //add custom comment
                $invoiceexist= $this->bookings_model->checkdataexist('vbs_invoices',[
                      'quote_id'       =>  $quote_id
                    ]);
               
                if($_POST['quote_status']=='1'){
               
                  if($quotes->status == "1"){
                     $approveddate=$quotes->approveddate;
                  }
                
                    $quoteupdate = $this->bookings_model->updatebookingrecord('vbs_quotes',[
                    'status'            => $_POST['quote_status'],
                    'reference'         => $_POST['reference'],
                    'approveddate'      => $approveddate
                  ],['id'=>$quote_id]); 
              
                    if(!$invoiceexist){
                       $invoiceid = $this->bookings_model->addbookingrecord('vbs_invoices',[
                        'date'            => @date('Y-m-d'),
                        'time'            => @date('h : i'),
                        'booking_id'      =>  $quotes->booking_id,
                        'quote_id'        =>  $quote_id
                      ]);
                      $module='4';
                      $statuspending='2';
                      $notificationstatuspending='1';
                       $remindersdata   = $this->quotes_model->getmultiplerecord('vbs_reminders',['module' => $module,'status'=> $statuspending]);
                     
                     if($remindersdata){
                      $remindertype=1;
                      foreach ($remindersdata as $reminder) {
                            $reminders = json_decode($reminder->reminders);
                            $date         = date('Y-m-d');
                            $days         = $reminders[0]->reminder_days;
                            $reminderdate = date('Y-m-d', strtotime($date. ' + '.$days.' days'));
                            $remindertime = date('h : i');
                             
                             $reminderid = $this->quotes_model->addrecord('vbs_invoices_reminder',[
                                             'date'            => $reminderdate,
                                             'time'            => $remindertime,
                                             'invoice_id'      => $invoiceid,
                                             'reminder_id'     => $reminder->id,
                                             'type'            => $remindertype
                                            ]); 
                             $remindertype++;

                      }
                    }
                     $notificationdata     = $this->quotes_model->getremindersnotification('vbs_notifications',['department' => $module,'status'=>$notificationstatuspending]);
                     if($notificationdata){
                     $notificationid = $this->quotes_model->addrecord('vbs_invoices_notification',[
                       'date'            => date('Y-m-d'),
                       'time'            => date('h : i'),
                       'invoice_id'      => $invoiceid,
                       'notification_id' => $notificationdata->id
                      ]);
                       }
                    } 

                }else{
                   $quoteupdate = $this->bookings_model->updatebookingrecord('vbs_quotes',[
                    'status'            => $_POST['quote_status'],
                    'reference'         => $_POST['reference'],
                  ],['id'=>$quote_id]);  
                  if($invoiceexist){
                     $invoice=$this->invoice_model->getsinglerecord('vbs_invoices',['booking_id'=>$id]);
                    $del=$this->invoice_model->deleteinvoicedata('vbs_invoices',['quote_id '=>$quote_id]);
                    $reminderdel=$this->invoice_model->deleteinvoicedata('vbs_invoices_reminder',['invoice_id'=>$invoice->id]);
                    $notificationdel=$this->invoice_model->deleteinvoicedata('vbs_invoices_notification',['invoice_id'=>$invoice->id]);
                    $customreindersdel=$this->invoice_model->deleteinvoicedata('vbs_invoices_customreminders',['invoice_id'=>$invoice->id]);
                    $custompaymentsdel=$this->invoice_model->deleteinvoicedata('vbs_invoices_custompayments',['invoice_id'=>$invoice->id]);
                  }
                }
              
               //update to quotes
             //add stops
            foreach($stopsdata as $stop){
                $stopid=$this->bookings_model->addbookingrecord('vbs_booking_stops',[
                  'stopaddress'          => $stop['stopaddress'],
                  'stopwaittime'         => $stop['stopwaittime'],
                  'booking_id'           =>  @$id,
                ]);
            }
               //add stops
         //add discount and fee and tax in vbs_booking_pricevalues
        $pricevalueid = $this->bookings_model->addbookingrecord('vbs_bookings_pricevalues',[
            'nighttimefee'         => $bookingpricedata['nighttimefee'],
            'notworkingdayfee'     => $bookingpricedata['notworkingdayfee'],
            'maydecdayfee'         => $bookingpricedata['maydecdayfee'],
            'drivermealfee'        => $bookingpricedata['drivermealfee'],
            'driverrestfee'        => $bookingpricedata['driverrestfee'],
            'nighttimediscount'    => $bookingpricedata['nighttimediscount'],
            'notworkingdaydiscount'=> $bookingpricedata['notworkingdaydiscount'],
            'maydecdaydiscount'    => $bookingpricedata['maydecdaydiscount'],
            'welcomediscount'      => $bookingpricedata['welcomediscount'],
            'perioddiscount'       => $bookingpricedata['perioddiscount'],
            'rewarddiscount'       => $bookingpricedata['rewarddiscount'],
            'promocodediscount'    => $bookingpricedata['promocodediscount'],
            'welcomediscountfee'   => $bookingpricedata['welcomediscountfee'],
            'perioddiscountfee'    => $bookingpricedata['perioddiscountfee'],
            'rewarddiscountfee'    => $bookingpricedata['rewarddiscountfee'],
            'promocodediscountfee' => $bookingpricedata['promocodediscountfee'],
            'vatdiscount'          => $bookingpricedata['vatdiscount'],
            'vatfee'               => $bookingpricedata['vatfee'],
            'isnighttimetype'      => $bookingpricedata['isnighttimetype'],
            'isnotworkingdaytype'  => $bookingpricedata['isnotworkingdaytype'],
            'ismydecembertype'     => $bookingpricedata['ismydecembertype'],
            'isdrivermealtype'     => $bookingpricedata['isdrivermealtype'],
            'isdriverresttype'     => $bookingpricedata['isdriverresttype'],
            'isreturnnighttimetype'=> $bookingpricedata['isreturnnighttimetype'],
            'isreturnnotworkingdaytype'  => $bookingpricedata['isreturnnotworkingdaytype'],
            'isreturnmydecembertype'     => $bookingpricedata['isreturnmydecembertype'],
            'isreturndrivermealtype'     => $bookingpricedata['isreturndrivermealtype'],
            'isreturndriverresttype'     => $bookingpricedata['isreturndriverresttype'],
            'booking_id'           =>  @$id,
            
        ]);
         //end discount and fee and tax  section

       if(!empty($pickairportfield)){
        $airportid = $this->bookings_model->addbookingrecord('vbs_booking_airport',[
          'flightnumber'     =>  $this->cleanstr($pickairportflightnumberfield),
          'arrivaltime'      =>  $this->cleanstr($pickairportarrivaltimefield),
          'poi_id'           =>  $pickairportfield,
          'booking_id'       =>  $id,
          'type'             =>  "pickup"
      ]);
       }
        if(!empty($dropairportfield)){
        $dropairportid = $this->bookings_model->addbookingrecord('vbs_booking_airport',[
          'flightnumber'     =>  $this->cleanstr($dropairportflightnumberfield),
          'arrivaltime'      =>  $this->cleanstr($dropairportarrivaltimefield),
          'poi_id'           =>  $dropairportfield,
          'booking_id'       =>  $id,
          'type'             =>  "dropoff"
      ]);
       }
        if(!empty($picktrainfield)){
        $trainid = $this->bookings_model->addbookingrecord('vbs_booking_train',[
          'trainnumber'     =>  $this->cleanstr($picktrainnumberfield),
          'arrivaltime'     =>  $this->cleanstr($picktrainarrivaltimefield),
          'poi_id'          =>  $picktrainfield,
          'booking_id'      =>  $id,
          'type'            =>  "pickup",

      ]);
       }
        if(!empty($droptrainfield)){
        $droptrainid = $this->bookings_model->addbookingrecord('vbs_booking_train',[
          'trainnumber'     => $this->cleanstr($droptrainnumberfield),
          'arrivaltime'     => $this->cleanstr($droptrainarrivaltimefield),
          'poi_id'          => $droptrainfield,
          'booking_id'      => $id,
          'type'           => "dropoff"
      ]);
       }
        if(!empty($pickaddressfield)){
        $addressid = $this->bookings_model->addbookingrecord('vbs_booking_address',[
          'address'          =>  $this->cleanstr($pickaddressfield),
          'otheraddress'     =>  $this->cleanstr($pickotheraddressfield),
          'city'             =>  $this->cleanstr($pickcityfield),
          'zipcode'         =>   $this->cleanstr($pickzipcodefield),
          'booking_id'       =>  $id,
          'type'             =>  "pickup"
      ]);
       }
        if(!empty($dropaddressfield)){
        $dropaddressid = $this->bookings_model->addbookingrecord('vbs_booking_address',[
          'address'          =>  $this->cleanstr($dropaddressfield),
          'otheraddress'     =>  $this->cleanstr($dropotheraddressfield),
          'city'             =>  $this->cleanstr($dropcityfield),
          'zipcode'          =>  $this->cleanstr($dropzipcodefield),
          'booking_id'       =>  $id,
          'type'             =>  "dropoff"
      ]);
       }
       if($this->session->userdata['passengers']){
        $passengers=$this->session->userdata['passengers'];
        foreach($passengers as $passenger){
             $passengerid = $this->bookings_model->addbookingrecord('vbs_booking_passengers',[
             'passenger_id'     => @$passenger["id"],
             'booking_id '      => @$id
      ]);
        }
       }
       
       
        foreach($bookingpickregular as $regular){
          if($regular["type"]==1){
            $regularid = $this->bookings_model->addbookingrecord('vbs_booking_regschedule',[
             'day'             => @$regular["day"],
             'time'            => @$regular["time"],
             'type'            => "regular",
             'booking_id'     => @$id
      ]);
          }
             
        }
      
       
        
        foreach($bookingreturnregular as $return){
          if($return["type"]==1){
            $returnid = $this->bookings_model->addbookingrecord('vbs_booking_regschedule',[
             'day'         => @$return["day"],
             'time'        => @$return["time"],
             'type'        => "return",
             'booking_id'  => @$id
      ]);
          }
             
        }
       

           $method = strtolower($methods->name);
           $method = str_replace(" ", "",  $method);
        
             if($method == "creditcard"){
              $creditcardid = $this->bookings_model->addbookingrecord('vbs_booking_creditcard',[
             'name_credit'         =>  $this->cleanstr($_POST["name_credit"]),
             'card_num_credit'     =>  $this->cleanstr($_POST["card_num_credit"]),
             'exp_month_credit'    =>  $this->cleanstr($_POST["exp_month_credit"]),
             'exp_year_credit'     =>  $this->cleanstr($_POST["exp_year_credit"]),
             'cvc_credit'          =>  $this->cleanstr($_POST["cvc_credit"]),
             'booking_id'         =>   @$id
              ]);
              }
             elseif($method == "bankdirectdebit"){
              $directdebitid = $this->bookings_model->addbookingrecord('vbs_booking_directdebit',[
             'civility_debit '     => $this->cleanstr($_POST["civility_debit"]),
             'fname_debit'         => $this->cleanstr($_POST["fname_debit"]),
             'lname_debit'         => $this->cleanstr($_POST["lname_debit"]),
             'bankname_debit'      => $this->cleanstr($_POST["bankname_debit"]),
             'agencyname_debit'    => $this->cleanstr($_POST["agencyname_debit"]),
             'agencyaddr_debit'    => $this->cleanstr($_POST["agencyaddr_debit"]),
             'otheraddr_debit'     => $this->cleanstr($_POST["otheraddr_debit"]),
             'agencyzipcode_debit' => $this->cleanstr($_POST["agencyzipcode_debit"]),
             'agencycity_debit'    => $this->cleanstr($_POST["agencycity_debit"]),
             'ibnnum_debit'        => $this->cleanstr($_POST["ibnnum_debit"]),
             'swiftcode_debit'     => $this->cleanstr($_POST["swiftcode_debit"]),
             'booking_id'          => @$id
              ]);
             }
          
         
           $this->session->set_flashdata('alert', [
                  'message' => " :Quotes record is updated.",
                  'class' => "alert-success",
                  'type' => "Congratulation"
              ]);
           return redirect("admin/quotes");
   
    }else{
    $this->session->set_flashdata('alert', [
                  'message' => " :something went wrong. please try again",
                  'class' => "alert-danger",
                  'type' => "Error"
              ]);
           return redirect("admin/quotes");
       }
      //insert data into database//
   }
   //Payment method and insert section
}else{
     $this->session->set_flashdata('alert', [
                  'message' => " :something went wrong. please try again",
                  'class' => "alert-danger",
                  'type' => "Error"
              ]);
           return redirect("admin/quotes");
}
 }
public function calculateBookingPrice($arraysofbooking,$bookingstops){
      //declare variables
       $perioddiscount=0;$welcomediscount=0;$rewarddiscount=0;$promocodediscount=0;
       $perioddiscountfee=0;$welcomediscountfee=0;$rewarddiscountfee=0;$promocodediscountfee=0;
       $isnighttime=false;$ismydecember=false;$isnotworkingday=false;
       $nighttimefee=0;$maydecdayfee=0;$notworkingdayfee=0;
       $nighttimediscount=0;$maydecdaydiscount=0;$notworkingdaydiscount=0;
       $isdrivermeal=false;$drivermealfee=0;
       $isdriverrest=false;$driverrestfee=0;
       $vatname='';$vatdiscount=0;$vatfee=0;
       $totalprice=0;
       $isreturnnighttime=false;$isreturnmydecember=false;$isreturnnotworkingday=false;
       $isreturndrivermeal=false;$isreturndriverrest=false;
       $distance=0;
       $realrideprice=0;$realridetotalprice=0;
       //end variables
 
           
      
          $pickdatefield=$arraysofbooking['pickdatefield'];
          $picktimefield=$arraysofbooking['picktimefield'];
          $startdatefield=$arraysofbooking['startdatefield'];
          $starttimefield=$arraysofbooking['starttimefield']; 
          $returndatefield=$arraysofbooking['returndatefield'];
          $returntimefield=$arraysofbooking['returntimefield'];
          $enddatefield=$arraysofbooking['enddatefield'];
          $endtimefield=$arraysofbooking['endtimefield'];  
          $waitingtimefield=$arraysofbooking['waitingtimefield'];
          $mapaddress=$arraysofbooking['mapaddress'];
          $dropmapaddress=$arraysofbooking['dropmapaddress'];
          $carfield=$arraysofbooking['carfield'];
          $regularcheckfield=$arraysofbooking['regularcheckfield'];
          $returncheckfield=$arraysofbooking['returncheckfield'];
          $service=$arraysofbooking['service'];
          $client_id=$arraysofbooking['client_id'];
          $customprice=$arraysofbooking['customprice'];
          $servicedata= $this->bookings_model->getservicesbyid('vbs_u_service', $service);
          $vatdata= $this->bookings_model->getservicesbyid('vbs_u_vat', $servicedata->tva);
          $vatname=$vatdata->name_of_var;
          $vatdiscount=$vatdata->vat;
          $promocodediscount=$arraysofbooking['promocodediscount'];

         $result=$this->get_coordinates($mapaddress,$dropmapaddress,$bookingstops);
    
   

        if($result){
          if($carfield){
           $pricedata=$this->bookings_model->getpricebycarid($carfield);
            
           if($pricedata){
            
            if($result['distance'] > 0){
              $dis=str_replace(',', '', $result['distance']);
                $distance=round(floatval($dis) * 1.609344 , 2);
             
            }
           
            $time=$result['time'];
            $minutes=$result['minutes'];
            $hours=$minutes/60;
            $waitingminutes=0;
            $discount=0;
            

            
            if($regularcheckfield==0){
                    $pickdate=$pickdatefield;
                      $picktime=$picktimefield;
                      $perioddiscounts=$this->bookings_model->getdiscountbydate($pickdate,$picktime);              
                  if($perioddiscounts){
                    $perioddiscount=$perioddiscounts->discount;
                  }
                   //check booking date is a 1may or 25 december
                   $is1mayordecember=$this->bookings_model->getdateofmaydec($pickdate);
                   if($is1mayordecember){
                      $ismydecember=true;
                    
                   }
        
                   //check picktime is night time
                  $isnighttime=$this->checknighttime($pricedata,$picktime);
                 
                   //check driver meal
                   $isdrivermeal=$this->checkdrivermealtime($pricedata,$picktime);
                   //end driver meal
                 
                  //booking date in  not working days and driver rest in not working day and more than 3 hours ride
                    $notworkingdayscheck=$this->bookings_model->getnotworkingdate($pricedata->id,$pickdate);
                    $todayissunday=$this->isWeekend($pickdate) ;
                 
                    if($notworkingdayscheck || $todayissunday){
                         $isnotworkingday=true;
                         if($hours > 3){
                           $isdriverrest=true;
                         }
                        
                    }


               //give the values if user select return ride
               if($returncheckfield!=0){
                $returndate=$returndatefield;
                        $returntime=$returntimefield;
                       
                       //period discount
                         if(!$perioddiscounts){
                       $returnperioddiscounts=$this->bookings_model->getdiscountbydate($returndate,$returntime);
                        if($returnperioddiscounts){
                        $perioddiscount=$returnperioddiscounts->discount;
                      }
                     }
        
                   //check returntime is night time
                  $isreturnnighttime=$this->checknighttime($pricedata,$returntime);
                 
                   //check driver meal
                   $isreturndrivermeal=$this->checkdrivermealtime($pricedata,$returntime);
                   //end driver meal
                 
                 

                    if($pickdate != $returndate){
                       //check booking date is a 1may or 25 december
                     $isreturn1mayordecember=$this->bookings_model->getdateofmaydec($returndate);
                     if($isreturn1mayordecember){
                        $isreturnmydecember=true;
                      
                     }
                              //booking date in  not working days and driver rest in not working day and more than 3 hours ride
                      $notreturnworkingdayscheck=$this->bookings_model->getnotworkingdate($pricedata->id,$returndate);
                      $todayissunday=$this->isWeekend($returndate) ;
                   
                      if($notreturnworkingdayscheck || $todayissunday){
                           $isreturnnotworkingday=true;
                           if($hours > 3){
                             $isreturndriverrest=true;
                           }
                          
                      }
                    }
               }

            
            }

                //check client is already exist in booking table for the bonus welcome
                $clientexistcount=$this->bookings_model->clientexist($client_id);
               
                if(!$clientexistcount){
                     $welcomediscounts=$this->bookings_model->getwelcomediscount();
                      $welcomediscount=$welcomediscounts->discount;
                    
                }else{
                  
                     $rewarddiscounts=$this->bookings_model->getrewarddiscount();
                  
                     if( $clientexistcount >= $rewarddiscounts->no_of_booking){
                      $rewarddiscount=$rewarddiscounts->discount;
                   
                     }
                }

             
                if(!empty($waitingtimefield)){
                        $waitarray=explode(":", $waitingtimefield);
                        $waitingminutes= (($waitarray[0]*60) + $waitarray[1]);
                }
                 foreach ($bookingstops as $stop) {
                  $waitarray = explode(":", $stop['stopwaittime']);
                  $waitingminutes += (($waitarray[0]*60) + $waitarray[1]);
                }

        
            $discount=$welcomediscount+$rewarddiscount+$perioddiscount+$promocodediscount;
           
           
           
            $price=($pricedata->pickup_fee + ($pricedata->price_km_approche * $distance)+($pricedata->price_trajet * $minutes) + ($pricedata->attente * $waitingminutes));
            if($price <  $pricedata->minimum_fee){
              $price=$pricedata->minimum_fee;
            }
                //calculate Real Ride Price
            if($pricedata->car_place==0){
         $companydata=$this->bookings_model->getcompanystatus();
       $realridepickdistancevalue=$this->get_other_coordinates($companydata->address,$mapaddress);
       $dis1=str_replace(',', '', $realridepickdistancevalue['distance']);
             $realridepickdistance=round(floatval($dis1) * 1.609344 , 2);
       $realridedropdistancevalue=$this->get_other_coordinates($companydata->address,$dropmapaddress);
             $dis2=str_replace(',', '', $realridedropdistancevalue['distance']);
             $realridedropdistance=round(floatval($dis2) * 1.609344 , 2);
       $realridepickprice=($pricedata->price_trajet_retour * $realridepickdistance);
       $realridedropprice=($pricedata->return_rate * $realridedropdistance);
       $realrideprice=$realridepickprice + $realridedropprice;
       $realridetotalprice=$price + $realrideprice;
       $realrideprice=round($realrideprice, 2);
       $realridetotalprice=round($realridetotalprice, 2);
      
        }
            //calculate Real Ride Price
            if($customprice != 0 || $customprice !=''){
              $price=$customprice;
        }
            if($regularcheckfield == 0){
              if($returncheckfield !=0){
                   $price=($price * 2);
                  $totalprice=$price;

              }else{
                 $totalprice=$price;
              }
            }else{
               $totalprice=$price;
            }
           
             if($isnighttime){
              if($pricedata->nightfeetype==1){
               $totalprice=$totalprice + $pricedata->majoration_de_nuit;
               $nighttimefee=$pricedata->majoration_de_nuit;
               
              }else{
                  $totalprice=$totalprice + ($price * ($pricedata->majoration_de_nuit/100));
                   $nighttimefee=($price * ($pricedata->majoration_de_nuit/100));
                   $nighttimediscount=$pricedata->majoration_de_nuit;
              }
         
            }
            if($isnotworkingday){
               if($pricedata->notworkfeetype==1){
               $totalprice=$totalprice + $pricedata->supplement_jour_ferie;
               $notworkingdayfee=$pricedata->supplement_jour_ferie;

              }else{
                  $totalprice=$totalprice + ($price * ($pricedata->supplement_jour_ferie/100));
                   $notworkingdayfee=($price * ($pricedata->supplement_jour_ferie/100));
                   $notworkingdaydiscount=$pricedata->supplement_jour_ferie;
              }
            
            }
            if($ismydecember){
               if($pricedata->decfeetype==1){
               $totalprice=$totalprice + $pricedata->supplement_1er;
               $maydecdayfee=$pricedata->supplement_1er;
               
              }else{
                  $totalprice=$totalprice + ($price * ($pricedata->supplement_1er/100));
                  $maydecdayfee=($price * ($pricedata->supplement_1er/100));
                  $maydecdaydiscount=$pricedata->supplement_1er;
              }
             
            }
            if($isdrivermeal){
                  $totalprice=$totalprice + $pricedata->panier_repas;
                  $drivermealfee=$pricedata->panier_repas;
            }
             if($isdriverrest){
                  $totalprice=$totalprice + $pricedata->repos_compensateur;
                  $driverrestfee=$pricedata->repos_compensateur;

            }
            //return ride add values
               
                if($isreturnnighttime){
              if($pricedata->nightfeetype==1){
               $totalprice=$totalprice + $pricedata->majoration_de_nuit;
               $nighttimefee +=$pricedata->majoration_de_nuit;
               
              }else{
                  $totalprice=$totalprice + ($price * ($pricedata->majoration_de_nuit/100));
                   $nighttimefee +=($price * ($pricedata->majoration_de_nuit/100));
                   $nighttimediscount +=$pricedata->majoration_de_nuit;
              }
         
            }
            if($isreturnnotworkingday){
               if($pricedata->notworkfeetype==1){
               $totalprice=$totalprice + $pricedata->supplement_jour_ferie;
               $notworkingdayfee +=$pricedata->supplement_jour_ferie;

              }else{
                  $totalprice=$totalprice + ($price * ($pricedata->supplement_jour_ferie/100));
                   $notworkingdayfee +=($price * ($pricedata->supplement_jour_ferie/100));
                   $notworkingdaydiscount +=$pricedata->supplement_jour_ferie;
              }
            
            }
            if($isreturnmydecember){
               if($pricedata->decfeetype==1){
               $totalprice=$totalprice + $pricedata->supplement_1er;
               $maydecdayfee +=$pricedata->supplement_1er;
               
              }else{
                  $totalprice=$totalprice + ($price * ($pricedata->supplement_1er/100));
                  $maydecdayfee +=($price * ($pricedata->supplement_1er/100));
                  $maydecdaydiscount +=$pricedata->supplement_1er;
              }
             
            }
            if($isreturndrivermeal){
                  $totalprice=$totalprice + $pricedata->panier_repas;
                  $drivermealfee +=$pricedata->panier_repas;
            }
             if($isreturndriverrest){
                  $totalprice=$totalprice + $pricedata->repos_compensateur;
                  $driverrestfee +=$pricedata->repos_compensateur;

            }
            //end return ride add values
          
            if($welcomediscount!=0){
               $totalprice=$totalprice - ($price * ($welcomediscount/100));
               $welcomediscountfee=($price * ($welcomediscount/100));
            }
            if($rewarddiscount!=0){
               $totalprice=$totalprice - ($price * ($rewarddiscount/100));
               $rewarddiscountfee= ($price * ($rewarddiscount/100));
            }
            if($perioddiscount!=0){
             
               $totalprice=$totalprice - ($price * ($perioddiscount/100));
               $perioddiscountfee=($price * ($perioddiscount/100));
            }
             if($promocodediscount!=0){
             
               $totalprice=$totalprice - ($price * ($promocodediscount/100));
               $promocodediscountfee=($price * ($promocodediscount/100));
            }
            if($vatdiscount!=0){
             
               $totalprice=$totalprice + ($price * ($vatdiscount/100));
               $vatfee=($price * ($vatdiscount/100));
            }
          
            
            
             $nighttimefee=round($nighttimefee, 2);
             $notworkingdayfee=round($notworkingdayfee, 2);
             $maydecdayfee=round($maydecdayfee, 2);
             $drivermealfee=round($drivermealfee, 2);
             $driverrestfee=round($driverrestfee, 2);
             $welcomediscountfee=round($welcomediscountfee, 2);
             $rewarddiscountfee=round($rewarddiscountfee, 2);
             $perioddiscountfee=round($perioddiscountfee, 2);
             $promocodediscountfee=round($promocodediscountfee, 2);
             $totalprice=round($totalprice, 2);
             $price=round($price, 2);

             //price and distance double in return ride
               if($regularcheckfield==0){
                if($returncheckfield!=0){
                  
                  $minutes = $minutes * 2;
                  $distance=round($distance * 2, 2);
                 }
               }

            $caltimehour=intdiv($minutes, 60);
            $caltimeminutes=$minutes % 60;

            if($caltimehour > 1){
              $caltimehour= $caltimehour." "."hours";
            }elseif($caltimehour == 1){
              $caltimehour= $caltimehour." "."hour";
            }else{
               $caltimehour= "";
            }
            if($caltimeminutes > 1){
              $caltimeminutes= $caltimeminutes." "."mins";
            }elseif($caltimeminutes == 1){
               $caltimeminutes= $caltimeminutes." "."min";
            }else{
               $caltimeminutes= "";
            }
               
            $time=$caltimehour." ".$caltimeminutes;
             //end calculate
         

         //is ride fee pick and return
             $isnighttimetype = 0;
             $isnotworkingdaytype = 0;
             $ismydecembertype = 0;
             $isdrivermealtype = 0;
             $isdriverresttype = 0;

             $isreturnnighttimetype = 0;
             $isreturnnotworkingdaytype = 0;
             $isreturnmydecembertype = 0;
             $isreturndrivermealtype = 0;
             $isreturndriverresttype = 0;

             if($isnighttime){
               $isnighttimetype = 1;
             }
             if($isnotworkingday){
               $isnotworkingdaytype = 1;
             }
             if($ismydecember){
               $ismydecembertype = 1;
             }
             if($isdrivermeal){
               $isdrivermealtype = 1;
             }
             if($isdriverrest){
               $isdriverresttype = 1;
             }
             if($isreturnnighttime){
               $isreturnnighttimetype = 1;
             }
             if($isreturnnotworkingday){
               $isreturnnotworkingdaytype = 1;
             }
             if($isreturnmydecember){
               $isreturnmydecembertype = 1;
             }
             if($isreturndrivermeal){
               $isreturndrivermealtype = 1;
             }
             if($isreturndriverrest){
               $isreturndriverresttype = 1;
             }
         //is ride fee pick and return
           
            $bookingmapandpricedata=array('distance'=> $distance,'time'=>$time,'price'=>$price,'totalprice'=>$totalprice,'nighttimefee'=>round($nighttimefee, 2),'notworkingdayfee'=>round($notworkingdayfee, 2),'maydecdayfee'=>round($maydecdayfee, 2),'nighttimediscount'=>$nighttimediscount,'notworkingdaydiscount'=>$notworkingdaydiscount,'maydecdaydiscount'=>$maydecdaydiscount,'pickupaddress'=>$mapaddress,'dropoffaddress'=>$dropmapaddress,'welcomediscount'=>$welcomediscount,'rewarddiscount'=>$rewarddiscount,'perioddiscount'=>$perioddiscount,'promocodediscount'=>$promocodediscount,'totaldiscount'=>$discount,'welcomediscountfee'=>round($welcomediscountfee, 2),'rewarddiscountfee'=>round($rewarddiscountfee, 2),'perioddiscountfee'=>round($perioddiscountfee, 2),'promocodediscountfee'=>round($promocodediscountfee, 2),'vatname'=>$vatname,'vatdiscount'=>$vatdiscount,'vatfee'=>round($vatfee, 2),'drivermealfee'=>round($drivermealfee, 2),'driverrestfee'=>round($driverrestfee, 2),'realrideprice'=>round($realrideprice, 2),'realridetotalprice'=>round($realridetotalprice, 2),'isnighttimetype' => $isnighttimetype,'isnotworkingdaytype' => $isnotworkingdaytype,'ismydecembertype' => $ismydecembertype,'isdrivermealtype' => $isdrivermealtype,'isdriverresttype' => $isdriverresttype,'isreturnnighttimetype' => $isreturnnighttimetype,'isreturnnotworkingdaytype' => $isreturnnotworkingdaytype,'isreturnmydecembertype' => $isreturnmydecembertype,'isreturndrivermealtype' => $isreturndrivermealtype,'isreturndriverresttype' => $isreturndriverresttype);
                return $bookingmapandpricedata;
           }else{
             return false;
               }

          }else{
                return false;
          }

        }else{
            return false;
        }
}
 public function delprevbookingdata($id){


  
  $airport=$this->bookings_model->deletebookingdata('vbs_booking_airport',['booking_id'=>$id]);
  $train=$this->bookings_model->deletebookingdata('vbs_booking_train',['booking_id'=>$id]);
  $address=$this->bookings_model->deletebookingdata('vbs_booking_address',['booking_id'=>$id]);
  $passengers=$this->bookings_model->deletebookingdata('vbs_booking_passengers',['booking_id'=>$id]);
  $regular=$this->bookings_model->deletebookingdata('vbs_booking_regschedule',['booking_id'=>$id]);
  $bookings_pricevalues=$this->bookings_model->deletebookingdata('vbs_bookings_pricevalues',['booking_id'=>$id]);
  $booking_creditcard=$this->bookings_model->deletebookingdata('vbs_booking_creditcard',['booking_id'=>$id]);
  $booking_directdebit=$this->bookings_model->deletebookingdata('vbs_booking_directdebit',['booking_id'=>$id]);
  $booking_stopper=$this->bookings_model->deletebookingdata('vbs_booking_stops',['booking_id'=>$id]);
  return true;
  
 }
 //End Edit Section
 //Edit Booking
 //Booking section
 //booking validation , payment calculation,etc.

public function booking_save(){
       //declare variable 
       //pickup category variables
       $pickupcategory="";$pickaddressfield="";$pickotheraddressfield="";
       $pickzipcodefield="";$pickcityfield="";
       $pickpackagefield="";$pickpackagename="";
       $pickairportfield="";$pickairportname="";$pickairportflightnumberfield="";$pickairportarrivaltimefield="";
       $picktrainfield="";$picktrainname="";$picktrainnumberfield="";$picktrainarrivaltimefield="";
       $pickhotelfield="";$pickhotelname="";
       $pickparkfield="";$pickparkname="";
       $pickmondaytype=0;$pickthuesdaytype=0;$pickwednesdaytype=0;$pickthursdaytype=0;$pickfridaytype=0;$picksaturdaytype=0;
       $picksundaytype=0;
       $pickmonday="";$pickthuesday="";$pickwednesday="";$pickthursday="";$pickfriday="";$picksaturday="";$picksunday="";
       $pickdatefield="";$picktimefield="";$waitingtimefield="";$returndatefield="";$returntimefield="";
     
       //dropoff category variables
       $dropoffcategory="";$dropaddressfield="";$dropotheraddressfield="";$dropzipcodefield="";$dropcityfield="";
       $droppackagefield="";$droppackagename="";
       $dropairportfield="";$dropairportname="";$dropairportflightnumberfield="";$dropairportarrivaltimefield="";
       $droptrainfield="";$droptrainname="";$droptrainnumberfield="";$droptrainarrivaltimefield="";
       $drophotelfield="";$drophotelname="";
       $dropparkfield="";$dropparkname="";
       $returnmondaytype=0;$returnthuesdaytype=0;$returnwednesdaytype=0;$returnthursdaytype=0;$returnfridaytype=0;$returnsaturdaytype=0;
       $returnsundaytype=0;
       $returnmonday="";$returnthuesday="";$returnwednesday="";$returnthursday="";$returnfriday="";$returnsaturday="";$returnsunday="";
       $service="";$servicecategory="";

      

       //other vairable  
       $regularcheckfield=0;$returncheckfield=0;$wheelcarcheckfield=0;
       $starttimefield="";$startdatefield="";$endtimefield="";$enddatefield="";
       $carfield="";      
       $mapaddress="";$dropmapaddress="";
      //end declare variable
        $date=date("Y-m-d");
      //stopper section
        $bookingstore['stops']=array();
        if(isset($_POST['stopaddress'])){
          
               $stopaddressfield=$this->input->post('stopaddress');
               $stopwaittimefield=$this->input->post('stopwaittime');
               $stopcount=count($stopaddressfield);

               if($stopcount > 0){
                $stopno=0;
                 for($i=0;$i<count($stopaddressfield);$i++){
                  if(!empty($stopaddressfield[$i])){
                     $bookingstore['stops'][$stopno]= array(
                   'stopaddress'  => $stopaddressfield[$i],
                   'stopwaittime' => $stopwaittimefield[$i]
                  
                 );
                      $stopno++;
                  }
                
                }            
              }
           
        }

  //stopper section

     

     //get last booking id from booking table 
      $bookingid=$this->bookings_model->getlastbookingid();
      //if booking id is not exist then give id to 1
      if(!$bookingid){
        $bookingid=1;
      }

        //regular,return,wheelchair field is checked or not then set value 0 or 1
        if(!empty($this->input->post("regularcheckfield"))){
            $regularcheckfield=1;          
         }
         if(!empty($this->input->post("returncheckfield"))){
             $returncheckfield=1;            
         }
         if(!empty($this->input->post("wheelcarcheckfield"))){
             $wheelcarcheckfield=1;            
         }
        // end regular , return and wheelchair fields


        $booking_status=$this->input->post('booking_status');
        $client_id=$this->input->post('client_field');
        $service=$this->input->post('pickservice');
        $servicecategory=$this->input->post('pickservicecategory');
        $booking_comment=$this->input->post('booking_comment');
        $pickupcategory=$this->input->post("pickupcategory");
        $dropoffcategory=$this->input->post("dropoffcategory");
     
        if(empty($this->input->post("regularcheckfield"))){     
            $picktimefield=$this->input->post("picktimefield");
            $pickdatefield=$this->input->post("pickdatefield");
         }
        if(!empty($this->input->post("returncheckfield")) && empty($this->input->post("regularcheckfield"))){
             $waitingtimefield=$this->input->post("waitingtimefield");
             $returntimefield=$this->input->post("returntimefield");
             $returndatefield=$this->input->post("returndatefield");
        }
        if(!empty($this->input->post("regularcheckfield")) ){
           $startdatefield=$this->input->post("startdatefield");
           $starttimefield=$this->input->post("starttimefield");
           $enddatefield=$this->input->post("enddatefield");
           $endtimefield=$this->input->post("endtimefield");
        }

        //get pickup fields by category
         if($pickupcategory=="address"){
                 
            $pickaddressfield=$this->input->post("pickaddressfield");
            $pickotheraddressfield=$this->input->post("pickotheraddressfield");
            $pickzipcodefield=$this->input->post("pickzipcodefield");
            $pickcityfield=$this->input->post("pickcityfield");
            $mapaddress= $pickaddressfield;
        }
        elseif($pickupcategory=="package"){
            
            $pickpackagefield=$this->input->post("pickpackagefield");
            $packagerecord=$this->bookings_model->poidatarecord('vbs_u_package',$pickpackagefield);
            $poirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$packagerecord['departure']);
            $pickpackagename=$packagerecord['name'];
            $pickzipcodefield=$poirecord['postal'];
            $pickcityfield=$poirecord['ville'];
            $mapaddress= $poirecord['address'].', '.$poirecord['ville'];
        }
        elseif($pickupcategory=="airport"){
           
            $pickairportfield=$this->input->post("pickairportfield");
            $pickairportflightnumberfield=$this->input->post("pickairportflightnumberfield");
            $pickairportarrivaltimefield=$this->input->post("pickairportarrivaltimefield");
            $airportrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$pickairportfield);
            $pickairportname=$airportrecord['name'];
            $pickzipcodefield=$airportrecord['postal'];
            $pickcityfield=$airportrecord['ville'];
             $mapaddress=  $airportrecord['address'].', '.$airportrecord['ville'];
        }
        elseif($pickupcategory=="train"){
          
            $picktrainfield=$this->input->post("picktrainfield");
            $picktrainnumberfield=$this->input->post("picktrainnumberfield");
            $picktrainarrivaltimefield=$this->input->post("picktrainarrivaltimefield");
            $trainrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$picktrainfield);
            $picktrainname=$trainrecord['name'];
            $pickzipcodefield=$trainrecord['postal'];
            $pickcityfield=$trainrecord['ville'];
            $mapaddress=  $trainrecord['address'].', '.$trainrecord['ville'];
        }
        elseif($pickupcategory=="hotel"){
            
            $pickhotelfield=$this->input->post("pickhotelfield");
            $hotelrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$pickhotelfield);
            $pickhotelname=$hotelrecord['name'];
            $pickzipcodefield=$hotelrecord['postal'];
            $pickcityfield=$hotelrecord['ville'];
            $mapaddress=  $hotelrecord['address'].', '.$hotelrecord['ville'];
        }
        elseif($pickupcategory=="park"){
            
            $pickparkfield=$this->input->post("pickparkfield");
            $parkrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$pickparkfield);
            $pickparkname=$parkrecord['name'];
            $pickzipcodefield=$parkrecord['postal'];
            $pickcityfield=$parkrecord['ville'];  
            $mapaddress=  $parkrecord['address'].', '.$parkrecord['ville'];       
        }

        //end pickup fields by category

        //get dropoff fields by category
        if($dropoffcategory=="address"){
          
            $dropaddressfield=$this->input->post("dropaddressfield");
            $dropotheraddressfield=$this->input->post("dropotheraddressfield");
            $dropzipcodefield=$this->input->post("dropzipcodefield");
            $dropcityfield=$this->input->post("dropcityfield");
            $dropmapaddress=  $dropaddressfield;
        }
        elseif($dropoffcategory=="package"){
           
            $droppackagefield=$this->input->post("droppackagefield");
            $packagerecord=$this->bookings_model->poidatarecord('vbs_u_package',$droppackagefield);
            $droppackagename=$packagerecord['name'];
            $poirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$packagerecord['destination']);
            $dropzipcodefield=$poirecord['postal'];
            $dropcityfield=$poirecord['ville'];   
            $dropmapaddress= $poirecord['address'].', '.$poirecord['ville'];       
        }
        elseif($dropoffcategory=="airport"){
            
            $dropairportfield=$this->input->post("dropairportfield");
            $dropairportflightnumberfield=$this->input->post("dropairportflightnumberfield");
            $dropairportarrivaltimefield=$this->input->post("dropairportarrivaltimefield");
            $airportrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$dropairportfield);
            $dropairportname=$airportrecord['name'];
            $dropzipcodefield=$airportrecord['postal'];
            $dropcityfield=$airportrecord['ville'];
            $dropmapaddress=  $airportrecord['address'].', '.$airportrecord['ville'];
        }
        elseif($dropoffcategory=="train"){
            
            $droptrainfield=$this->input->post("droptrainfield");
            $droptrainnumberfield=$this->input->post("droptrainnumberfield");
            $droptrainarrivaltimefield=$this->input->post("droptrainarrivaltimefield");
            $trainrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$droptrainfield);
            $droptrainname=$trainrecord['name'];
            $dropzipcodefield=$trainrecord['postal'];
            $dropcityfield=$trainrecord['ville'];
            $dropmapaddress=  $trainrecord['address'].', '.$trainrecord['ville'];
        }
        elseif($dropoffcategory=="hotel"){
            
            $drophotelfield=$this->input->post("drophotelfield");
            $hotelrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$drophotelfield);
            $drophotelname=$hotelrecord['name'];
            $dropzipcodefield=$hotelrecord['postal'];
            $dropcityfield=$hotelrecord['ville'];
            $dropmapaddress=  $hotelrecord['address'].', '.$hotelrecord['ville'];
        }
        elseif($dropoffcategory=="park"){
             
            $dropparkfield=$this->input->post("dropparkfield");
            $parkrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$dropparkfield);
            $dropparkname=$parkrecord['name'];
            $dropzipcodefield=$parkrecord['postal'];
            $dropcityfield=$parkrecord['ville'];
            $dropmapaddress=  $parkrecord['address'].', '.$parkrecord['ville'];

        }
       //end dropoff fields by category


        //get cars by car type if car is wheelchair or not
        $cartype=$this->input->post('carstypes');
        if($cartype=="1"){
          $carfield=$this->input->post('wheelcarfield');
        }else{
           $carfield=$this->input->post('carfield');
        }

    //convert these values into array and after that we store in session
     $bookingstore["bookingdata"]=array("categorytype"=>$pickupcategory,'address'=>$pickaddressfield,'otheraddress'=>$pickotheraddressfield,
      'zipcode'=>$pickzipcodefield,'city'=>$pickcityfield,'package'=>$pickpackagefield,'pickpackagename'=>$pickpackagename,
      'airport'=>$pickairportfield,'pickairportname'=>$pickairportname,'flightnumber'=>$pickairportflightnumberfield,
      'flightarrivaltime'=>$pickairportarrivaltimefield,'train'=>$picktrainfield,
            'picktrainname'=>$picktrainname,'trainnumber'=>$picktrainnumberfield,'trainarrivalnumber'=>$picktrainarrivaltimefield,
            'hotel'=>$pickhotelfield,'pickhotelname'=>$pickhotelname,'pickparkname'=>$pickparkname,'park'=>$pickparkfield,
            'pickupdate'=>$pickdatefield,'pickuptime'=>$picktimefield,'waitingtime'=>$waitingtimefield,'returndate'=>$returndatefield,
            'returntime'=>$returntimefield,'regularcheck'=>$regularcheckfield,'returncheck'=>$returncheckfield,'wheelchaircheck'=>$wheelcarcheckfield,
            'starttime'=>$starttimefield,'startdate'=>$startdatefield,'endtime'=>$endtimefield,'enddate'=>$enddatefield,
            'carfield'=>$carfield,"dropcategorytype"=>$dropoffcategory,'dropaddress'=>$dropaddressfield,'dropotheraddress'=>$dropotheraddressfield,
            'dropzipcode'=>$dropzipcodefield,'dropcity'=>$dropcityfield,'droppackage'=>$droppackagefield,'droppackagename'=>$droppackagename,
            'dropairport'=>$dropairportfield,'dropairportname'=>$dropairportname,'dropflightnumber'=>$dropairportflightnumberfield,
            'dropflightarrivaltime'=>$dropairportarrivaltimefield,'droptrain'=>$droptrainfield,'droptrainname'=>$droptrainname,
            'droptrainnumber'=>$droptrainnumberfield,'droptrainarrivalnumber'=>$droptrainarrivaltimefield,'drophotel'=>$drophotelfield,'drophotelname'=>$drophotelname,'droppark'=>$dropparkfield,'dropparkname'=>$dropparkname,'bookingid'=>$bookingid,'servicecategory'=>$servicecategory,'service'=>$service,'booking_comment'=>$booking_comment,'mapaddress'=>$mapaddress,'dropmapaddress'=>$dropmapaddress);

    // regular day and time fields
    if(!empty($this->input->post("pickmondaycheckfield"))){
        $pickmonday=$this->input->post("pickmondayfield");
        $pickmondaytype=1;
        }
        if(!empty($this->input->post("picktuesdaycheckfield"))){
        $pickthuesday=$this->input->post("picktuesdayfield");
        $pickthuesdaytype=1;
       }
        if(!empty($this->input->post("pickwednesdaycheckfield"))){
        $pickwednesday=$this->input->post("pickwednesdayfield");
        $pickwednesdaytype=1;
       }
        if(!empty($this->input->post("pickthursdaycheckfield"))){
        $pickthursday=$this->input->post("pickthursdayfield");
        $pickthursdaytype=1;
       }
        if(!empty($this->input->post("pickfridaycheckfield"))){
        $pickfriday=$this->input->post("pickfridayfield");
        $pickfridaytype=1;
       }
        if(!empty($this->input->post("picksaturdaycheckfield"))){
        $picksaturday=$this->input->post("picksaturdayfield");
        $picksaturdaytype=1;
       }
        if(!empty($this->input->post("picksundaycheckfield"))){
        $picksunday=$this->input->post("picksundayfield");
        $picksundaytype=1;
       }

        $bookingstore['bookingpickregular']=array(
           array("day"=>"Monday","type"=>$pickmondaytype,"time"=>$pickmonday),
           array("day"=>"Tuesday","type"=>$pickthuesdaytype,"time"=>$pickthuesday),
           array("day"=>"Wednesday","type"=>$pickwednesdaytype,"time"=>$pickwednesday),
           array("day"=>"Thursday","type"=>$pickthursdaytype,"time"=>$pickthursday),
           array("day"=>"Friday","type"=>$pickfridaytype,"time"=>$pickfriday),
           array("day"=>"Saturday","type"=>$picksaturdaytype,"time"=>$picksaturday),
           array("day"=>"Sunday","type"=>$picksundaytype,"time"=>$picksunday),
        );

       //return day and time check fields
        if(!empty($this->input->post("returnmondaycheckfield"))){
        $returnmonday=$this->input->post("returnmondayfield");
        $returnmondaytype=1;
       }
        if(!empty($this->input->post("returntuesdaycheckfield"))){
        $returnthuesday=$this->input->post("returntuesdayfield");
        $returnthuesdaytype=1;
       }
        if(!empty($this->input->post("returnwednesdaycheckfield"))){
        $returnwednesday=$this->input->post("returnwednesdayfield");
        $returnwednesdaytype=1;
       }
        if(!empty($this->input->post("returnthursdaycheckfield"))){
        $returnthursday=$this->input->post("returnthursdayfield");
        $returnthursdaytype=1;
       }
        if(!empty($this->input->post("returnfridaycheckfield"))){
        $returnfriday=$this->input->post("returnfridayfield");
        $returnfridaytype=1;
       }
        if(!empty($this->input->post("returnsaturdaycheckfield"))){
        $returnsaturday=$this->input->post("returnsaturdayfield");
        $returnsaturdaytype=1;
       }
        if(!empty($this->input->post("returnsundaycheckfield"))){
        $returnsunday=$this->input->post("returnsundayfield");
        $returnsundaytype=1;
       }
       $bookingstore['bookingreturnregular']=array(
           array("day"=>"Monday","type"=>$returnmondaytype,"time"=>$returnmonday),
           array("day"=>"Tuesday","type"=>$returnthuesdaytype,"time"=>$returnthuesday),
           array("day"=>"Wednesday","type"=>$returnwednesdaytype,"time"=>$returnwednesday),
           array("day"=>"Thursday","type"=>$returnthursdaytype,"time"=>$returnthursday),
           array("day"=>"Friday","type"=>$returnfridaytype,"time"=>$returnfriday),
           array("day"=>"Saturday","type"=>$returnsaturdaytype,"time"=>$returnsaturday),
           array("day"=>"Sunday","type"=>$returnsundaytype,"time"=>$returnsunday),
        );
      $bookingstore['bookingextravalues']=array(
                                  'booking_status'=>$booking_status,
                                  'client_id'     =>$client_id
                                 );

       $arraysofbooking=array('pickdatefield'=>$pickdatefield,'picktimefield'=>$picktimefield,'startdatefield'=>$startdatefield,'starttimefield'=>$starttimefield,'returndatefield'=>$returndatefield,'returntimefield'=>$returntimefield,'enddatefield'=>$enddatefield,'endtimefield'=>$endtimefield,'waitingtimefield'=>$waitingtimefield,'mapaddress'=>$mapaddress,'dropmapaddress'=>$dropmapaddress,'carfield'=>$carfield,'regularcheckfield'=>$regularcheckfield,'returncheckfield'=>$returncheckfield,'service'=>$service,'client_id'=>$client_id,'customprice'=>'','promocodediscount'=>'0');

        $bookingstore['mapdatainfo']=$this->calculateBookingPrice($arraysofbooking,$bookingstore['stops']);
        $this->session->set_userdata($bookingstore);
        $this->data["car_name"]=$this->bookings_model->getcarname($carfield);
        $map= $this->session->userdata['mapdatainfo'];
        $this->data['pickupaddress']=$map['pickupaddress'];
        $this->data['dropoffaddress']=$map['dropoffaddress'];
        $this->data['googleapikey']=$this->bookings_model->getgoogleapikey("vbs_googleapi");
        $this->data["client"]=$this->bookings_model->getclientdatabyid($client_id);
        $this->data["paymentmethods"]=$this->bookings_model->paymentmethods();
        $result=$this->load->view('admin/quotes/add_booking_quote', $this->data, TRUE);
  echo $result;
  exit();
    }

    public function save_booking_confirm(){

    $booking=$this->session->userdata['bookingdata'];
    $mapinfodata=$this->session->userdata['mapdatainfo']; 
     $stopsdata=$this->session->userdata['stops'];
    $bookingextravalues=$this->session->userdata['bookingextravalues']; 
    $payment_method=$this->input->post("payment_method");
    $methods=$this->bookings_model->getpaymentmethod('vbs_u_payment_method',$payment_method);
  
     
      if($payment_method){
         if($this->session->userdata['bookingdata']){
            $client_id=$bookingextravalues['client_id'];
            $clientdata=$this->bookings_model->getclientdatabyid($client_id);
            $car_name=$this->bookings_model->getcarname($booking["carfield"]);          
            $pickupdate;$returndate;$startdate;$enddate;
          if(!empty($booking['pickupdate'])){
             $pickupdate=str_replace('/', '-', $booking['pickupdate']);
             $pickupdate=strtotime($pickupdate);
             $pickupdate = date('Y-m-d',$pickupdate);
          }
          if(!empty($booking['returndate'])){
             $returndate=str_replace('/', '-', $booking['returndate']);
             $returndate=strtotime($returndate);
             $returndate = date('Y-m-d',$returndate);
          }
          if(!empty($booking['startdate'])){
             $startdate=str_replace('/', '-', $booking['startdate']);
             $startdate=strtotime($startdate);
             $startdate = date('Y-m-d',$startdate);
          }
          if(!empty($booking['enddate'])){
             $enddate=str_replace('/', '-', $booking['enddate']);
             $enddate=strtotime($enddate);
             $enddate = date('Y-m-d',$enddate);
          }


           if(!empty($booking['pickupdate'])){
               $bookingdate=$pickupdate;
              }
              else{
                $bookingdate=$startdate;
              }
          $id = $this->bookings_model->addbookingrecord('vbs_bookings',[
          'user_id'            => $client_id,
          'booking_ref'        => create_timestampdmy_uid($bookingdate,($booking['bookingid']+1)),
          'pick_date'          => $pickupdate,
          'pick_time'          => $booking['pickuptime'],
          'pick_point'         => $this->cleanstr($mapinfodata['pickupaddress']),
          'drop_point'         => $this->cleanstr($mapinfodata['dropoffaddress']),
          'totaldiscount'      => $mapinfodata['totaldiscount'],
          'distance'           => $mapinfodata['distance'],
          'vehicle_selected'   => $booking['carfield'],
          'cost_of_journey'    => $mapinfodata['price'],
          'price'              => $mapinfodata['price'],
          'totalprice'         => $mapinfodata['totalprice'],
          'bookdate'           => @date('Y-m-d'),
          'booktime'           => @date('h : i'),
          'registered_name'    => $clientdata->username,
          'phone'              => $clientdata->phone,
          'email'              => $clientdata->email,
          'transaction_id'     => ($booking['bookingid']+1),
          'payer_id'           => 0,
          'package_type'       => "Null",
          'info_to_drivers'    => "Null",
          'payer_name'         => "Null",
          'is_new'             => 0,
          'unread'             => 0,
          'time_of_journey'    => $mapinfodata['time'],
          'pickupcategory'     => $booking['categorytype'],
          'dropoffcategory'    => $booking['dropcategorytype'],
          'waiting_time'       => $booking['waitingtime'],
          'return_time'        => $booking['returntime'],
          'return_date'        => $returndate,
          'start_time'         => $booking['starttime'],
          'start_date'         => $startdate,
          'end_time'           => $booking['endtime'],
          'end_date'           => $enddate,
          'car_id'             => $booking['carfield'],
          'pickuppackage_id'   => $booking['package'],
          'droppackage_id'     => $booking['droppackage'],
          'pickuphotel_id'     => $booking['hotel'],
          'drophotel_id'       => $booking['drophotel'],
          'pickuppark_id'      => $booking['park'],
          'droppark_id'        => $booking['droppark'],
          'regular'            => $booking['regularcheck'],
          'returncheck'        => $booking['returncheck'],
          'wheelchair'         => $booking['wheelchaircheck'],
          'payment_method_id'  => $payment_method,
          'service_id'         => $booking['service'],
          'service_category_id'=> $booking['servicecategory'],
          'booking_comment'    => $booking['booking_comment'],
          'ridedate'           => $bookingdate,
          'is_conformed'       => $bookingextravalues['booking_status'],
          'realrideprice'      => $mapinfodata['realrideprice'],
          'realridetotalprice' => $mapinfodata['realridetotalprice'],
          'customprice'        => (isset($_POST['customprice']) ? $_POST['customprice'] : '0'),
          'pickzipcode'        => $booking['zipcode'],
          'pickcity'           => $booking['city'],
          'dropzipcode'        => $booking['dropzipcode'],
          'dropcity'           => $booking['dropcity'],

        ]);
          if($id){
            //add quotes
            if($bookingextravalues['booking_status']==1){
              $quotesid = $this->bookings_model->addbookingrecord('vbs_quotes',[
                'date'           => @date('Y-m-d'),
                'time'           => @date('h : i'),
                'booking_id'       =>  $id
            ]);
           }

            //add quotes
                //add stops
            foreach($stopsdata as $stop){
                $stopid=$this->bookings_model->addbookingrecord('vbs_booking_stops',[
                  'stopaddress'          => $stop['stopaddress'],
                  'stopwaittime'         => $stop['stopwaittime'],
                  'booking_id'           =>  @$id,
                ]);
            }
               //add stops

               //add discount and fee and tax in vbs_booking_pricevalues
              $pricevalueid = $this->bookings_model->addbookingrecord('vbs_bookings_pricevalues',[
                  'nighttimefee'         => $mapinfodata['nighttimefee'],
                  'notworkingdayfee'     => $mapinfodata['notworkingdayfee'],
                  'maydecdayfee'         => $mapinfodata['maydecdayfee'],
                  'drivermealfee'        => $mapinfodata['drivermealfee'],
                  'driverrestfee'        => $mapinfodata['driverrestfee'],
                  'nighttimediscount'    => $mapinfodata['nighttimediscount'],
                  'notworkingdaydiscount'=> $mapinfodata['notworkingdaydiscount'],
                  'maydecdaydiscount'    => $mapinfodata['maydecdaydiscount'],
                  'welcomediscount'      => $mapinfodata['welcomediscount'],
                  'perioddiscount'       => $mapinfodata['perioddiscount'],
                  'rewarddiscount'       => $mapinfodata['rewarddiscount'],
                  'promocodediscount'    => $mapinfodata['promocodediscount'],
                  'welcomediscountfee'   => $mapinfodata['welcomediscountfee'],
                  'perioddiscountfee'    => $mapinfodata['perioddiscountfee'],
                  'rewarddiscountfee'    => $mapinfodata['rewarddiscountfee'],
                  'promocodediscountfee' => $mapinfodata['promocodediscountfee'],
                  'vatdiscount'          => $mapinfodata['vatdiscount'],
                  'vatfee'               => $mapinfodata['vatfee'],
                  'booking_id'           =>  @$id,
                  
              ]);
               //end discount and fee and tax  section

             if(!empty($booking["airport"])){
              $airportid = $this->bookings_model->addbookingrecord('vbs_booking_airport',[
                'flightnumber'     =>  $this->cleanstr($booking["flightnumber"]),
                'arrivaltime'      =>  $this->cleanstr($booking["flightarrivaltime"]),
                'poi_id'           =>  @$booking["airport"],
                'booking_id'       =>  @$id,
                'type'             =>  "pickup"
            ]);
             }
              if(!empty($booking["dropairport"])){
              $dropairportid = $this->bookings_model->addbookingrecord('vbs_booking_airport',[
                'flightnumber'     =>  $this->cleanstr($booking["dropflightnumber"]),
                'arrivaltime'      =>  $this->cleanstr($booking["dropflightarrivaltime"]),
                'poi_id'           =>  @$booking["dropairport"],
                'booking_id'       =>  @$id,
                'type'             =>  "dropoff"
            ]);
             }
              if(!empty($booking["train"])){
              $trainid = $this->bookings_model->addbookingrecord('vbs_booking_train',[
                'trainnumber'     =>  $this->cleanstr($booking["trainnumber"]),
                'arrivaltime'     =>  $this->cleanstr($booking["trainarrivalnumber"]),
                'poi_id'          =>  @$booking["train"],
                'booking_id'      =>  @$id,
                'type'            =>  "pickup",

            ]);
             }
              if(!empty($booking["droptrain"])){
              $droptrainid = $this->bookings_model->addbookingrecord('vbs_booking_train',[
                'trainnumber'     => $this->cleanstr($booking["droptrainnumber"]),
                'arrivaltime'     => $this->cleanstr($booking["droptrainarrivalnumber"]),
                'poi_id'          => @$booking["droptrain"],
                'booking_id'      => @$id,
                'type'           => "dropoff"
            ]);
             }
              if(!empty($booking["address"])){
              $addressid = $this->bookings_model->addbookingrecord('vbs_booking_address',[
                'address'          =>  $this->cleanstr($booking["address"]),
                'otheraddress'     =>  $this->cleanstr($booking["otheraddress"]),
                'city'             =>  $this->cleanstr($booking["city"]),
                'zipcode'         =>   $this->cleanstr($booking["zipcode"]),
                'booking_id'       =>  @$id,
                'type'             =>  "pickup"
            ]);
             }
              if(!empty($booking["dropaddress"])){
              $dropaddressid = $this->bookings_model->addbookingrecord('vbs_booking_address',[
                'address'          =>  $this->cleanstr($booking["dropaddress"]),
                'otheraddress'     =>  $this->cleanstr($booking["dropotheraddress"]),
                'city'             =>  $this->cleanstr($booking["dropcity"]),
                'zipcode'          =>  $this->cleanstr($booking["dropzipcode"]),
                'booking_id'       =>  @$id,
                'type'             =>  "dropoff"
            ]);
             }
             if($this->session->userdata['passengers']){
              $passengers=$this->session->userdata['passengers'];
              foreach($passengers as $passenger){
                   $passengerid = $this->bookings_model->addbookingrecord('vbs_booking_passengers',[
                   'passenger_id'     => @$passenger["id"],
                   'booking_id '      => @$id
            ]);
              }
             }
              if($this->session->userdata['bookingpickregular']){
              $pickregular=$this->session->userdata['bookingpickregular'];
              foreach($pickregular as $regular){
                if($regular["type"]==1){
                  $regularid = $this->bookings_model->addbookingrecord('vbs_booking_regschedule',[
                   'day'             => @$regular["day"],
                   'time'            => @$regular["time"],
                   'type'            => "regular",
                   'booking_id'     => @$id
            ]);
                }
                   
              }
             }
              if($this->session->userdata['bookingreturnregular']){
              $pickreturn=$this->session->userdata['bookingreturnregular'];
              foreach($pickreturn as $return){
                if($return["type"]==1){
                  $returnid = $this->bookings_model->addbookingrecord('vbs_booking_regschedule',[
                   'day'         => @$return["day"],
                   'time'        => @$return["time"],
                   'type'        => "return",
                   'booking_id'  => @$id
            ]);
                }
                   
              }
             }
              
                  if(strtolower($methods->name)=="credit card"){
                    $creditcardid = $this->bookings_model->addbookingrecord('vbs_booking_creditcard',[
                   'name_credit'         =>  $this->cleanstr($_POST["name_credit"]),
                   'card_num_credit'     =>  $this->cleanstr($_POST["card_num_credit"]),
                   'exp_month_credit'    =>  $this->cleanstr($_POST["exp_month_credit"]),
                   'exp_year_credit'     =>  $this->cleanstr($_POST["exp_year_credit"]),
                   'cvc_credit'          =>  $this->cleanstr($_POST["cvc_credit"]),
                   'booking_id'         =>   @$id
                    ]);
                    }
                  elseif(strtolower($methods->name)=="bank direct debit"){
                    $directdebitid = $this->bookings_model->addbookingrecord('vbs_booking_directdebit',[
                   'civility_debit '     => $this->cleanstr($_POST["civility_debit"]),
                   'fname_debit'         => $this->cleanstr($_POST["fname_debit"]),
                   'lname_debit'         => $this->cleanstr($_POST["lname_debit"]),
                   'bankname_debit'      => $this->cleanstr($_POST["bankname_debit"]),
                   'agencyname_debit'    => $this->cleanstr($_POST["agencyname_debit"]),
                   'agencyaddr_debit'    => $this->cleanstr($_POST["agencyaddr_debit"]),
                   'otheraddr_debit'     => $this->cleanstr($_POST["otheraddr_debit"]),
                   'agencyzipcode_debit' => $this->cleanstr($_POST["agencyzipcode_debit"]),
                   'agencycity_debit'    => $this->cleanstr($_POST["agencycity_debit"]),
                   'ibnnum_debit'        => $this->cleanstr($_POST["ibnnum_debit"]),
                   'swiftcode_debit'     => $this->cleanstr($_POST["swiftcode_debit"]),
                   'booking_id'          => @$id
                    ]);
                   }
                
               
                 $this->session->set_flashdata('alert', [
                        'message' => " :Booking is added.",
                        'class' => "alert-success",
                        'type' => "Success"
                    ]);
                 return redirect("admin/quotes");
         
          }
       else{
          $this->session->set_flashdata('alert', [
                        'message' => " :something went wrong. please try again",
                        'class' => "alert-danger",
                        'type' => "Error"
                    ]);
            return redirect("admin/quotes");
      }
    
      }
  }
      else{
               $this->session->set_flashdata('alert', [
                        'message' => " :something is wrong. please try again",
                        'class' => "alert-danger",
                        'type' => "Error"
                    ]);
            return redirect("admin/quotes");
      }
    }

function get_other_coordinates($pickupaddress,$dropoffaddress){
 //replace string
 $pickupaddress=str_replace(", ,", "+", $pickupaddress);
 $pickupaddress=str_replace(" , ", "+", $pickupaddress);
 $pickupaddress=str_replace(", ", "+", $pickupaddress);
 $pickupaddress=str_replace(" ,", "+", $pickupaddress);
 $pickupaddress=str_replace(",", "+", $pickupaddress);
 $pickupaddress=str_replace(" - ", "+", $pickupaddress);
 $pickupaddress=str_replace("- ", "+", $pickupaddress);
 $pickupaddress=str_replace(" -", "+", $pickupaddress);
 $pickupaddress=str_replace("-", "+", $pickupaddress);
 $pickupaddress=str_replace(" ", "+", $pickupaddress);

 
 $dropoffaddress=str_replace(", ,", "+", $dropoffaddress);
 $dropoffaddress=str_replace(" , ", "+", $dropoffaddress);
 $dropoffaddress=str_replace(", ", "+", $dropoffaddress);
 $dropoffaddress=str_replace(" ,", "+", $dropoffaddress);
 $dropoffaddress=str_replace(",", "+", $dropoffaddress);
 $dropoffaddress=str_replace(" - ", "+", $dropoffaddress);
 $dropoffaddress=str_replace("- ", "+", $dropoffaddress);
 $dropoffaddress=str_replace(" -", "+", $dropoffaddress);
 $dropoffaddress=str_replace("-", "+", $dropoffaddress);
 $dropoffaddress=str_replace(" ", "+", $dropoffaddress);

//replace string

$googlekey=$this->bookings_model->getgoogleapikey("vbs_googleapi");

  $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".$pickupaddress."&destinations=".$dropoffaddress."&key=".$googlekey->api_key;
  //echo $url;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch,CURLOPT_POST,false);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
      
    if($response_a != null){

      $status= $response_a['rows'][0]['elements'][0]['status'];
         if ( $status == 'ZERO_RESULTS' ){
          return false;
         }else{
           $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
            $time = $response_a['rows'][0]['elements'][0]['duration']['text'];
             $seconds = $response_a['rows'][0]['elements'][0]['duration']['value'];
             $minutes=round($seconds/60);
            
            return array('distance'=>$dist,'time'=>$time,'minutes'=>$minutes);
         }

     
    }else{
      return false;
    }
    
}
    
 function get_coordinates($pickupaddress,$dropoffaddress,$bookingstops)
{
  $googlekey=$this->bookings_model->getgoogleapikey("vbs_googleapi");  
  $stopcounts=count($bookingstops);

  if($stopcounts > 0){
   
    $dist;
    $time;
    $seconds;
    $minutes;
  //pickupaddress to first stoper
 
    $pickupaddress=str_replace(", ,", "+", $pickupaddress);
    $pickupaddress=str_replace(" , ", "+", $pickupaddress);
    $pickupaddress=str_replace(", ", "+", $pickupaddress);
    $pickupaddress=str_replace(" ,", "+", $pickupaddress);
    $pickupaddress=str_replace(",", "+", $pickupaddress);
    $pickupaddress=str_replace(" - ", "+", $pickupaddress);
    $pickupaddress=str_replace("- ", "+", $pickupaddress);
    $pickupaddress=str_replace(" -", "+", $pickupaddress);
    $pickupaddress=str_replace("-", "+", $pickupaddress);
    $pickupaddress=str_replace(" ", "+", $pickupaddress);

 //replace string
   $stopaddress=str_replace(", ,", "+", $bookingstops[0]['stopaddress']);
   $stopaddress=str_replace(" , ", "+", $bookingstops[0]['stopaddress']);
   $stopaddress=str_replace(", ", "+",  $bookingstops[0]['stopaddress']);
   $stopaddress=str_replace(" ,", "+",  $bookingstops[0]['stopaddress']);
   $stopaddress=str_replace(",", "+",   $bookingstops[0]['stopaddress']);
   $stopaddress=str_replace(" - ", "+", $bookingstops[0]['stopaddress']);
   $stopaddress=str_replace("- ", "+",  $bookingstops[0]['stopaddress']);
   $stopaddress=str_replace(" -", "+",  $bookingstops[0]['stopaddress']);
   $stopaddress=str_replace("-", "+",   $bookingstops[0]['stopaddress']);
   $stopaddress=str_replace(" ", "+",   $bookingstops[0]['stopaddress']);
    
   $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".$pickupaddress."&destinations=".$stopaddress."&key=".$googlekey->api_key;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch,CURLOPT_POST,false);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
      
    if($response_a != null){

      $status= $response_a['rows'][0]['elements'][0]['status'];
         if ( $status == 'ZERO_RESULTS' ){
          return false;
         }
         else{
            $dist     = $response_a['rows'][0]['elements'][0]['distance']['text'];
            $time     = $response_a['rows'][0]['elements'][0]['duration']['text'];
            $seconds  = $response_a['rows'][0]['elements'][0]['duration']['value'];
            $minutes  = round($seconds/60);
         }

     
    }else{
      return false;
    }
 
  //pickupaaddress to first stoper

  //first stoper to next stoper
    if($stopcounts > 1){
    for($i=0;$i < ($stopcounts-1);$i++){
     
    $stopaddress1=str_replace(", ,", "+", $bookingstops[$i]['stopaddress']);
    $stopaddress1=str_replace(" , ", "+", $bookingstops[$i]['stopaddress']);
    $stopaddress1=str_replace(", ", "+", $bookingstops[$i]['stopaddress']);
    $stopaddress1=str_replace(" ,", "+", $bookingstops[$i]['stopaddress']);
    $stopaddress1=str_replace(",", "+", $bookingstops[$i]['stopaddress']);
    $stopaddress1=str_replace(" - ", "+", $bookingstops[$i]['stopaddress']);
    $stopaddress1=str_replace("- ", "+", $bookingstops[$i]['stopaddress']);
    $stopaddress1=str_replace(" -", "+", $bookingstops[$i]['stopaddress']);
    $stopaddress1=str_replace("-", "+", $bookingstops[$i]['stopaddress']);
    $stopaddress1=str_replace(" ", "+", $bookingstops[$i]['stopaddress']);

 //replace string
   $stopaddress2=str_replace(", ,", "+", $bookingstops[$i+1]['stopaddress']);
   $stopaddress2=str_replace(" , ", "+", $bookingstops[$i+1]['stopaddress']);
   $stopaddress2=str_replace(", ", "+",  $bookingstops[$i+1]['stopaddress']);
   $stopaddress2=str_replace(" ,", "+",  $bookingstops[$i+1]['stopaddress']);
   $stopaddress2=str_replace(",", "+",   $bookingstops[$i+1]['stopaddress']);
   $stopaddress2=str_replace(" - ", "+", $bookingstops[$i+1]['stopaddress']);
   $stopaddress2=str_replace("- ", "+",  $bookingstops[$i+1]['stopaddress']);
   $stopaddress2=str_replace(" -", "+",  $bookingstops[$i+1]['stopaddress']);
   $stopaddress2=str_replace("-", "+",   $bookingstops[$i+1]['stopaddress']);
   $stopaddress2=str_replace(" ", "+",   $bookingstops[$i+1]['stopaddress']);
    
   $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".$stopaddress1."&destinations=".$stopaddress2."&key=".$googlekey->api_key;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch,CURLOPT_POST,false);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
      
    if($response_a != null){

      $status= $response_a['rows'][0]['elements'][0]['status'];
         if ( $status == 'ZERO_RESULTS' ){
          return false;
         }
         else{
            $dist     += $response_a['rows'][0]['elements'][0]['distance']['text'];
            $time     = $response_a['rows'][0]['elements'][0]['duration']['text'];
            $seconds  = $response_a['rows'][0]['elements'][0]['duration']['value'];
            $minutes  += round($seconds/60);
         }

     
    }else{
      return false;
    }
   
  } 
}
  //first stoper to next stoper

 //last stoper to dropoff
    $dropoffaddress=str_replace(", ,", "+", $dropoffaddress);
    $dropoffaddress=str_replace(" , ", "+", $dropoffaddress);
    $dropoffaddress=str_replace(", ", "+", $dropoffaddress);
    $dropoffaddress=str_replace(" ,", "+", $dropoffaddress);
    $dropoffaddress=str_replace(",", "+", $dropoffaddress);
    $dropoffaddress=str_replace(" - ", "+", $dropoffaddress);
    $dropoffaddress=str_replace("- ", "+", $dropoffaddress);
    $dropoffaddress=str_replace(" -", "+", $dropoffaddress);
    $dropoffaddress=str_replace("-", "+", $dropoffaddress);
    $dropoffaddress=str_replace(" ", "+", $dropoffaddress);

 //replace string
   $stopaddress=str_replace(", ,", "+", $bookingstops[$stopcounts-1]['stopaddress']);
   $stopaddress=str_replace(" , ", "+", $bookingstops[$stopcounts-1]['stopaddress']);
   $stopaddress=str_replace(", ", "+",  $bookingstops[$stopcounts-1]['stopaddress']);
   $stopaddress=str_replace(" ,", "+",  $bookingstops[$stopcounts-1]['stopaddress']);
   $stopaddress=str_replace(",", "+",   $bookingstops[$stopcounts-1]['stopaddress']);
   $stopaddress=str_replace(" - ", "+", $bookingstops[$stopcounts-1]['stopaddress']);
   $stopaddress=str_replace("- ", "+",  $bookingstops[$stopcounts-1]['stopaddress']);
   $stopaddress=str_replace(" -", "+",  $bookingstops[$stopcounts-1]['stopaddress']);
   $stopaddress=str_replace("-", "+",   $bookingstops[$stopcounts-1]['stopaddress']);
   $stopaddress=str_replace(" ", "+",   $bookingstops[$stopcounts-1]['stopaddress']);
    
   $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".$stopaddress."&destinations=".$dropoffaddress."&key=".$googlekey->api_key;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch,CURLOPT_POST,false);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
      
    if($response_a != null){

      $status= $response_a['rows'][0]['elements'][0]['status'];
         if ( $status == 'ZERO_RESULTS' ){
          return false;
         }
         else{
            $dist     += $response_a['rows'][0]['elements'][0]['distance']['text'];
            $time     = $response_a['rows'][0]['elements'][0]['duration']['text'];
            $seconds  = $response_a['rows'][0]['elements'][0]['duration']['value'];
            $minutes  += round($seconds/60);
         }

     
    }else{
      return false;
    }
 //last stoper to dropoff
  return array('distance'=>$dist,'time'=>$time,'minutes'=>$minutes);
 }
 else{

  //if stoper is null
  //replace string
 $pickupaddress=str_replace(", ,", "+", $pickupaddress);
 $pickupaddress=str_replace(" , ", "+", $pickupaddress);
 $pickupaddress=str_replace(", ", "+", $pickupaddress);
 $pickupaddress=str_replace(" ,", "+", $pickupaddress);
 $pickupaddress=str_replace(",", "+", $pickupaddress);
 $pickupaddress=str_replace(" - ", "+", $pickupaddress);
 $pickupaddress=str_replace("- ", "+", $pickupaddress);
 $pickupaddress=str_replace(" -", "+", $pickupaddress);
 $pickupaddress=str_replace("-", "+", $pickupaddress);
 $pickupaddress=str_replace(" ", "+", $pickupaddress);

 
 $dropoffaddress=str_replace(", ,", "+", $dropoffaddress);
 $dropoffaddress=str_replace(" , ", "+", $dropoffaddress);
 $dropoffaddress=str_replace(", ", "+", $dropoffaddress);
 $dropoffaddress=str_replace(" ,", "+", $dropoffaddress);
 $dropoffaddress=str_replace(",", "+", $dropoffaddress);
 $dropoffaddress=str_replace(" - ", "+", $dropoffaddress);
 $dropoffaddress=str_replace("- ", "+", $dropoffaddress);
 $dropoffaddress=str_replace(" -", "+", $dropoffaddress);
 $dropoffaddress=str_replace("-", "+", $dropoffaddress);
 $dropoffaddress=str_replace(" ", "+", $dropoffaddress);

//replace string

$googlekey=$this->bookings_model->getgoogleapikey("vbs_googleapi");

  $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".$pickupaddress."&destinations=".$dropoffaddress."&key=".$googlekey->api_key;
  //echo $url;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch,CURLOPT_POST,false);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
      
    if($response_a != null){

      $status= $response_a['rows'][0]['elements'][0]['status'];
         if ( $status == 'ZERO_RESULTS' ){
          return false;
         }else{
          
           $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
            $time = $response_a['rows'][0]['elements'][0]['duration']['text'];
            $seconds = $response_a['rows'][0]['elements'][0]['duration']['value'];
            $minutes=round($seconds/60);
               
        
            return array('distance'=>$dist,'time'=>$time,'minutes'=>$minutes);

         }

     
    }else{
      return false;
    }
   
  //if stoper is null
 }

    
}
function checknighttime($pricedata,$picktime){
  $isnighttime=false;
   if($pricedata->night_time_from >= "12 : 00" && $pricedata->night_time_to >= "12 : 00"  && $picktime >= "12 : 00"){
                  if($picktime >= $pricedata->night_time_from &&  $picktime <= $pricedata->night_time_to){
                      $isnighttime=true;
                     
                  }
            
             }

             elseif($pricedata->night_time_from <= "12 : 00" && $pricedata->night_time_to <= "12 : 00"  && $picktime <= "12 : 00"){
                  if($picktime >= $pricedata->night_time_from &&  $picktime <= $pricedata->night_time_to){
                      $isnighttime=true;
                     
                  }
            
             }
              elseif($pricedata->night_time_from >= "12 : 00" && $pricedata->night_time_to <= "12 : 00"  && $picktime >= "12 : 00"){
                  if($picktime >= $pricedata->night_time_from){
                      $isnighttime=true;
                    
                  }
            
             }
              elseif($pricedata->night_time_from <= "12 : 00" && $pricedata->night_time_to >= "12 : 00"  && $picktime >= "12 : 00"){
                  if($picktime <= $pricedata->night_time_to){
                      $isnighttime=true;
                      
                  }
            
             }
              elseif($pricedata->night_time_from >= "12 : 00" && $pricedata->night_time_to <= "12 : 00"  && $picktime <= "12 : 00"){
                  if($picktime <= $pricedata->night_time_to){
                      $isnighttime=true;
                    
                  }
            
             }
              elseif($pricedata->night_time_from <= "12 : 00" && $pricedata->night_time_to >= "12 : 00"  && $picktime <= "12 : 00"){
                  if($picktime >= $pricedata->night_time_from){
                      $isnighttime=true;
                    
                  }
            
             }
             return $isnighttime;
}
//driver meal time function return true or false
 function checkdrivermealtime($pricedata,$picktime){
        //check picktime is driver meal
           //driver first meal check
              $isdrivermeal=false;
             if($pricedata->driver_meal_from >= "12 : 00" && $pricedata->driver_meal_to >= "12 : 00"  && $picktime >= "12 : 00"){
                  if($picktime >= $pricedata->driver_meal_from &&  $picktime <= $pricedata->driver_meal_to){
                      $isdrivermeal=true;
                     
                  }
            
             }

             elseif($pricedata->driver_meal_from <= "12 : 00" && $pricedata->driver_meal_to <= "12 : 00"  && $picktime <= "12 : 00"){
                  if($picktime >= $pricedata->driver_meal_from &&  $picktime <= $pricedata->driver_meal_to){
                      $isdrivermeal=true;
                     
                  }
            
             }
              elseif($pricedata->driver_meal_from >= "12 : 00" && $pricedata->driver_meal_to <= "12 : 00"  && $picktime >= "12 : 00"){
                  if($picktime >= $pricedata->driver_meal_from){
                      $isdrivermeal=true;
                    
                  }
            
             }
              elseif($pricedata->driver_meal_from <= "12 : 00" && $pricedata->driver_meal_to >= "12 : 00"  && $picktime >= "12 : 00"){
                  if($picktime <= $pricedata->driver_meal_to){
                      $isdrivermeal=true;
                      
                  }
            
             }
              elseif($pricedata->driver_meal_from >= "12 : 00" && $pricedata->driver_meal_to <= "12 : 00"  && $picktime <= "12 : 00"){
                  if($picktime <= $pricedata->driver_meal_to){
                      $isdrivermeal=true;
                    
                  }
            
             }
              elseif($pricedata->driver_meal_from <= "12 : 00" && $pricedata->driver_meal_to >= "12 : 00"  && $picktime <= "12 : 00"){
                  if($picktime >= $pricedata->driver_meal_from){
                      $isdrivermeal=true;
                    
                  }
            
             }
             //end first meal
             //driver second meal
                 if($pricedata->driver_othermeal_from >= "12 : 00" && $pricedata->driver_othermeal_to >= "12 : 00"  && $picktime >= "12 : 00"){
                  if($picktime >= $pricedata->driver_othermeal_from &&  $picktime <= $pricedata->driver_othermeal_to){
                      $isdrivermeal=true;
                     
                  }
            
             }

             elseif($pricedata->driver_othermeal_from <= "12 : 00" && $pricedata->driver_othermeal_to <= "12 : 00"  && $picktime <= "12 : 00"){
                  if($picktime >= $pricedata->driver_othermeal_from &&  $picktime <= $pricedata->driver_othermeal_to){
                      $isdrivermeal=true;
                     
                  }
            
             }
              elseif($pricedata->driver_othermeal_from >= "12 : 00" && $pricedata->driver_othermeal_to <= "12 : 00"  && $picktime >= "12 : 00"){
                  if($picktime >= $pricedata->driver_othermeal_from){
                      $isdrivermeal=true;
                    
                  }
            
             }
              elseif($pricedata->driver_othermeal_from <= "12 : 00" && $pricedata->driver_othermeal_to >= "12 : 00"  && $picktime >= "12 : 00"){
                  if($picktime <= $pricedata->driver_othermeal_to){
                      $isdrivermeal=true;
                      
                  }
            
             }
              elseif($pricedata->driver_othermeal_from >= "12 : 00" && $pricedata->driver_othermeal_to <= "12 : 00"  && $picktime <= "12 : 00"){
                  if($picktime <= $pricedata->driver_othermeal_to){
                      $isdrivermeal=true;
                    
                  }
            
             }
              elseif($pricedata->driver_othermeal_from <= "12 : 00" && $pricedata->driver_othermeal_to >= "12 : 00"  && $picktime <= "12 : 00"){
                  if($picktime >= $pricedata->driver_othermeal_from){
                      $isdrivermeal=true;
                    
                  }
            
             }
             //end second meal
            return  $isdrivermeal;
  //end driver meal           
    }
    function isWeekend($date) {
  
   $date=str_replace('/', '-', $date);
    $weekDay = date('w', strtotime($date));
   
     if ($weekDay == 0){
      return true;
    }
    else{
      return false;
    }
}
 public function cleanstr($str){
  $str = preg_replace("/[^A-Za-z0-9 ]/", '', $str);
  return $str;
 }
//end booking validation ,payment calculation etc.
//Detail Section
 public function get_ajax_detail_bookings(){
  $booking_id=$_GET['booking_id'];
  $notificationpendingstatus="1";
  $reminderpendingstatus="2";
  //add index function data
        $this->data['css_type']   = array("form","booking_datatable","datatable");

    $this->data['active_class'] = "quotes";

    $this->data['gmaps']    = false;

    $this->data['title']    = $this->lang->line("quotes");

    $this->data['title_link']   = base_url('admin/quotes');

    $this->data['content']    = 'admin/quotes/index';

        $data = [];

        $this->data['clients_data']=$this->bookings_model->getusersbyrole("clients");
        $this->data['drivers_data']=$this->bookings_model->getusersbyrole("drivers");
       
        //booking add record
         
        $this->data['poi_data'] = $this->bookings_model->booking_Config_getAll('vbs_u_poi');
        $this->data['poi_package'] = $this->bookings_model->booking_Config_getAll('vbs_u_package');
        $this->data['service_cat'] = $this->bookings_model->booking_Config_getAll('vbs_u_category_service');
        $this->data['service'] = $this->bookings_model->booking_Config_getAll('vbs_u_service');
        $this->data['car_data'] = $this->bookings_model->getcarsdata();
        $this->data['wheelcar_data'] = $this->bookings_model->getwheelcarsdata();
        $this->data['googleapikey']=$this->bookings_model->getgoogleapikey("vbs_googleapi");
        
        $this->data['dbbookingdata']=$this->bookings_model->getdbbookingrecord('vbs_bookings',array('id'=>$booking_id));

        //print_r($this->data['dbbookingdata']);
       // exit();
        $this->data['dbpickairportdata']=$this->bookings_model->getdbbookingrecord('vbs_booking_airport',array('booking_id'=>$booking_id,'type'=>'pickup'));
        $this->data['dbpicktraindata']=$this->bookings_model->getdbbookingrecord('vbs_booking_train',array('booking_id'=>$booking_id,'type'=>'pickup'));
        $this->data['dbpickaddressdata']=$this->bookings_model->getdbbookingrecord('vbs_booking_address',array('booking_id'=>$booking_id,'type'=>'pickup'));

         $this->data['dbdropairportdata']=$this->bookings_model->getdbbookingrecord('vbs_booking_airport',array('booking_id'=>$booking_id,'type'=>'dropoff'));
        $this->data['dbdroptraindata']=$this->bookings_model->getdbbookingrecord('vbs_booking_train',array('booking_id'=>$booking_id,'type'=>'dropoff'));
        $this->data['dbdropaddressdata']=$this->bookings_model->getdbbookingrecord('vbs_booking_address',array('booking_id'=>$booking_id,'type'=>'dropoff'));
        $this->data['dbpickregulardata']=$this->bookings_model->getdbmultiplebookingrecord('vbs_booking_regschedule',array('booking_id'=>$booking_id,'type'=>'regular'));
        $this->data['dbreturndata']=$this->bookings_model->getdbmultiplebookingrecord('vbs_booking_regschedule',array('booking_id'=>$booking_id,'type'=>'return'));
        $this->data['dbstopsdata']=$this->bookings_model->getdbmultiplebookingrecord('vbs_booking_stops',array('booking_id'=>$booking_id));
        $dbpassengers=$this->bookings_model->getdbpassengersbookingrecord(array('booking_id'=>$booking_id));
        $this->data['dbdiscountandfeedata']=$this->bookings_model->getdbbookingrecord('vbs_bookings_pricevalues',array('booking_id'=>$booking_id));
        $this->data['dbcreditcarddata']=$this->bookings_model->getdbbookingrecord('vbs_booking_creditcard',array('booking_id'=>$booking_id));
        $this->data['dbdebitbankdata']=$this->bookings_model->getdbbookingrecord('vbs_booking_directdebit',array('booking_id'=>$booking_id));
        $this->data["paymentmethods"]=$this->bookings_model->paymentmethods();
         $this->data['companydata']=$this->bookings_model->getcompanystatus();
          $this->data['quotedata']=$this->bookings_model->getdbbookingrecord('vbs_quotes',array('booking_id'=>$booking_id));

       $this->data['quotenotifications']=$this->quotes_model->getmultiplerecord("vbs_quotes_notification",['quote_id'=>$this->data['quotedata']->id]);
         $this->data['quotefirstreminders']=$this->quotes_model->getmultiplerecord("vbs_quotes_reminder",['quote_id'=>$this->data['quotedata']->id,'type'=>'1']);
          $this->data['quotesecondreminders']=$this->quotes_model->getmultiplerecord("vbs_quotes_reminder",['quote_id'=>$this->data['quotedata']->id,'type'=>'2']);
           $this->data['quotethirdreminders']=$this->quotes_model->getmultiplerecord("vbs_quotes_reminder",['quote_id'=>$this->data['quotedata']->id,'type'=>'3']);          $this->data['quotecustomreminders']=$this->quotes_model->getmultiplerecord('vbs_quotes_customreminders',['quote_id'=> $this->data['quotedata']->id]);
        if($this->session->userdata['passengers']){
          $this->session->unset_userdata('passengers');
        }
       $bookingstore['passengers']=array();
        foreach($dbpassengers as $p){
         $bookingstore['passengers'][$p->id]= array(
                   'id'          => $p->id,
                   'name'        => $p->civility.' '.$p->fname.' '.$p->lname,
                   'phone'       => $p->mobilephone,
                   'home'        => $p->homephone,
                   'disable'     => $p->name_of_disablecategory,
                   'disableicon' => 'fa fa-check'

              );
        
        }
      
        $this->session->set_userdata($bookingstore);
        

  //end section
  //$this->data['booking_id']=$_GET['booking_id'];
  $result=$this->load->view('admin/quotes/booking_box_detail', $this->data, TRUE);
  echo $result;
  exit();
 }
//Detail Section
 //Booking Section
 public function quotes_pdf($booking_id){
  
  $pdfArrays=createstringquotepdf($booking_id);  
  $this->data['pdf']=$pdfArrays['pdf']; 
  $this->data['fileName']=$pdfArrays['fileName'];    
  $this->load->view('admin/quotes/quotepdf',$this->data);
 }

 public function get_bookings_bysearch(){

  $keyword=$this->input->post('keyword');
  $otherkeyword=$this->input->post('otherkeyword');
  $from_period=$this->input->post('from_period');
  $to_period=$this->input->post('to_period');
  $service=$this->input->post('service');
  $paymentmethod=$this->input->post('paymentmethod');
  $regular=$this->input->post('regular');
  $statut=$this->input->post('statut');

  $search=array();
    
     if(!empty($from_period)){
         $from_period=str_replace('/', '-', $from_period);
         $from_period=strtotime($from_period);
         $from_period = date('Y-m-d',$from_period);
         $search["DATE_FORMAT(qt.date,'%Y-%m-%d') >="]=$from_period;
         $from_period = strtotime($from_period);
         $from_period = date('d/m/Y',$from_period);
     }
     if(!empty($to_period)){
           $to_period=str_replace('/', '-', $to_period);
           $to_period=strtotime($to_period);
           $to_period = date('Y-m-d',$to_period);
           $search["DATE_FORMAT(qt.date,'%Y-%m-%d') <="]=$to_period;
           $to_period = strtotime($to_period);
           $to_period = date('d/m/Y',$to_period);
     }
     if(!empty($service)){
      $search['booking.service_id']=$service;
     }
     if(!empty($paymentmethod)){
      $search['booking.payment_method_id']=$paymentmethod;
     }
     if(!empty($regular)){
      $search['booking.service_category_id']=$regular;
     }
     if($statut != ''){
      $search['qt.status']=$statut;
     }
    
  
     $data=$this->quotes_model->getquotesallrecordsbysearch($search,$keyword,$otherkeyword);
 
    
    
       
        $this->session->set_flashdata('bookingsearchdata', [
              'record' => $data,  
              'fields' =>  array("search_keyword"=>$keyword,"search_otherkeyword"=>$otherkeyword,"search_from_period"=>$from_period,"search_to_period"=>$to_period,"search_service"=>$service,"search_methods"=>$paymentmethod,"search_regular"=>$regular,"search_status"=>$statut),
                                                              
                                                           ]);
    
  
          return redirect("admin/quotes");
  
}
public function notification_pdf($id,$booking_id){
  if(empty($id) || $id=='0'){
     $this->session->set_flashdata('alert', [
                        'message' => " :something is wrong. please try again",
                        'class' => "alert-danger",
                        'type' => "Error"
                    ]);
            return redirect("admin/quotes");
  }

  $pdfArrays=createnotificationpdf($id,$booking_id);  
  $this->data['pdf']=$pdfArrays['pdf']; 
  $this->data['fileName']=$pdfArrays['fileName'];  

  $this->load->view('admin/quotes/notificationpdf',$this->data);
}
public function reminder_pdf($id,$level,$booking_id){
 
 $pdfArrays=createreminderpdf($id,$level,$booking_id);  
 $this->data['pdf']=$pdfArrays['pdf']; 
 $this->data['fileName']=$pdfArrays['fileName'];  

 //get user 
 $this->load->view('admin/quotes/reminderpdf',$this->data);
}

//update price by cutom price
public function update_booking_customprice(){
       
       $isnighttime=false;
       $ismydecember=false;
       $isnotworkingday=false;
       $isdrivermeal=false;
       $isdriverrest=false;
       $isreturnnighttime=false;
       $isreturnmydecember=false;
       $isreturnnotworkingday=false;
       $isreturndrivermeal=false;
       $isreturndriverrest=false;

       $nighttimediscount=0;
       $maydecdaydiscount=0;
       $notworkingdaydiscount=0;


       $nighttimefee=0;
       $maydecdayfee=0;
       $notworkingdayfee=0;
       $drivermealfee=0;
       $driverrestfee=0;
       
       $perioddiscountfee=0;
       $welcomediscountfee=0;
       $rewarddiscountfee=0;
       $promocodediscountfee=0;
       $vatfee=0;

        


        $customprice = $_GET['customprice'];
        $booking_id  = $_GET['bookingid'];

        $bookingdata=$this->bookings_model->getdbbookingrecord('vbs_bookings',array('id'=>$booking_id));
        $bookingpricedata=$this->bookings_model->getdbbookingrecord('vbs_bookings_pricevalues',array('booking_id'=>$booking_id));
        


       //driver meal,driver rest, night fee, not working day fee, may or december fee
            if($bookingdata->regular==0){
                      $pricedata=$this->bookings_model->getpricebycarid($bookingdata->car_id);
                      $pickdate=$bookingdata->pick_date;
                      $picktime=$bookingdata->pick_time;
                    
                   //check booking date is a 1may or 25 december
                   $is1mayordecember=$this->bookings_model->getdateofmaydec($pickdate);
                   if($is1mayordecember){
                      $ismydecember=true;
                   }
        
                   //check picktime is night time
                  $isnighttime=$this->checknighttime($pricedata,$picktime);
                 
                   //check driver meal
                   $isdrivermeal=$this->checkdrivermealtime($pricedata,$picktime);
                   //end driver meal
                 
                  //booking date in  not working days and driver rest in not working day and more than 3 hours ride
                    $notworkingdayscheck=$this->bookings_model->getnotworkingdate($pricedata->id,$pickdate);
                    $todayissunday=$this->isWeekend($pickdate) ;
                 
                    if($notworkingdayscheck || $todayissunday){
                         $isnotworkingday=true;
                         if($bookingpricedata->driverrestfee != 0){
                           $isdriverrest=true;
                         }
                        
                    }


               //give the values if user select return ride
               if($bookingdata->returncheck!=0){
                        $returndate=$bookingdata->return_date;
                        $returntime=$bookingdata->return_time;
                       
                       //period discount
                  if(!$perioddiscounts){
                       $returnperioddiscounts=$this->bookings_model->getdiscountbydate($returndate,$returntime);
                        if($returnperioddiscounts){
                        $perioddiscount=$returnperioddiscounts->discount;
                      }
                     }
        
                   //check returntime is night time
                  $isreturnnighttime=$this->checknighttime($pricedata,$returntime);
                 
                   //check driver meal
                   $isreturndrivermeal=$this->checkdrivermealtime($pricedata,$returntime);
                   //end driver meal
                 
                 

                    if($pickdate != $returndate){
                       //check booking date is a 1may or 25 december
                     $isreturn1mayordecember=$this->bookings_model->getdateofmaydec($returndate);
                     if($isreturn1mayordecember){
                        $isreturnmydecember=true;
                      
                     }
                              //booking date in  not working days and driver rest in not working day and more than 3 hours ride
                      $notreturnworkingdayscheck=$this->bookings_model->getnotworkingdate($pricedata->id,$returndate);
                      $todayissunday=$this->isWeekend($returndate) ;
                   
                      if($notreturnworkingdayscheck || $todayissunday){
                           $isreturnnotworkingday=true;
                           if($bookingpricedata->driverrestfee != 0){
                             $isreturndriverrest=true;
                           }     
                      }
                    }
               }

            
            }
           //driver meal,driver rest, night fee, not working day fee, may or december fee
           
           
            $price=$customprice;
            $totalprice=$price;

           
             if($isnighttime){
              if($pricedata->nightfeetype==1){
               $totalprice=$totalprice + $pricedata->majoration_de_nuit;
               $nighttimefee=$pricedata->majoration_de_nuit;
               
              }else{
                   $totalprice=$totalprice + ($price * ($pricedata->majoration_de_nuit/100));
                   $nighttimefee=($price * ($pricedata->majoration_de_nuit/100));
                   $nighttimediscount=$pricedata->majoration_de_nuit;
              }
         
            }
            if($isnotworkingday){
               if($pricedata->notworkfeetype==1){
               $totalprice=$totalprice + $pricedata->supplement_jour_ferie;
               $notworkingdayfee=$pricedata->supplement_jour_ferie;

              }else{
                  $totalprice=$totalprice + ($price * ($pricedata->supplement_jour_ferie/100));
                   $notworkingdayfee=($price * ($pricedata->supplement_jour_ferie/100));
                   $notworkingdaydiscount=$pricedata->supplement_jour_ferie;
              }
            
            }
            if($ismydecember){
               if($pricedata->decfeetype==1){
               $totalprice=$totalprice + $pricedata->supplement_1er;
               $maydecdayfee=$pricedata->supplement_1er;
               
              }else{
                  $totalprice=$totalprice + ($price * ($pricedata->supplement_1er/100));
                  $maydecdayfee=($price * ($pricedata->supplement_1er/100));
                  $maydecdaydiscount=$pricedata->supplement_1er;
              }
             
            }
            if($isdrivermeal){
                  $totalprice=$totalprice + $pricedata->panier_repas;
                  $drivermealfee=$pricedata->panier_repas;
            }
             if($isdriverrest){
                  $totalprice=$totalprice + $pricedata->repos_compensateur;
                  $driverrestfee=$pricedata->repos_compensateur;

            }
            //return ride add values
               
             if($isreturnnighttime){
              if($pricedata->nightfeetype==1){
               $totalprice=$totalprice + $pricedata->majoration_de_nuit;
               $nighttimefee +=$pricedata->majoration_de_nuit;
               
              }else{
                  $totalprice=$totalprice + ($price * ($pricedata->majoration_de_nuit/100));
                   $nighttimefee +=($price * ($pricedata->majoration_de_nuit/100));
                   $nighttimediscount +=$pricedata->majoration_de_nuit;
              }
         
            }
            if($isreturnnotworkingday){
               if($pricedata->notworkfeetype==1){
               $totalprice=$totalprice + $pricedata->supplement_jour_ferie;
               $notworkingdayfee +=$pricedata->supplement_jour_ferie;

              }else{
                  $totalprice=$totalprice + ($price * ($pricedata->supplement_jour_ferie/100));
                   $notworkingdayfee +=($price * ($pricedata->supplement_jour_ferie/100));
                   $notworkingdaydiscount +=$pricedata->supplement_jour_ferie;
              }
            
            }
            if($isreturnmydecember){
               if($pricedata->decfeetype==1){
               $totalprice=$totalprice + $pricedata->supplement_1er;
               $maydecdayfee +=$pricedata->supplement_1er;
               
              }else{
                  $totalprice=$totalprice + ($price * ($pricedata->supplement_1er/100));
                  $maydecdayfee +=($price * ($pricedata->supplement_1er/100));
                  $maydecdaydiscount +=$pricedata->supplement_1er;
              }
             
            }
            if($isreturndrivermeal){
                  $totalprice=$totalprice + $pricedata->panier_repas;
                  $drivermealfee +=$pricedata->panier_repas;
            }
             if($isreturndriverrest){
                  $totalprice=$totalprice + $pricedata->repos_compensateur;
                  $driverrestfee +=$pricedata->repos_compensateur;

            }
            //end return ride add values
          
            if($bookingpricedata->welcomediscount != 0){
               $totalprice=$totalprice - ($price * ($bookingpricedata->welcomediscount/100));
               $welcomediscountfee=($price * ($bookingpricedata->welcomediscount/100));
            }
            if($bookingpricedata->rewarddiscount != 0){
               $totalprice=$totalprice - ($price * ($bookingpricedata->rewarddiscount/100));
               $rewarddiscountfee= ($price * ($bookingpricedata->rewarddiscount/100));
            }
            if($bookingpricedata->perioddiscount != 0){
             
               $totalprice=$totalprice - ($price * ($bookingpricedata->perioddiscount/100));
               $perioddiscountfee=($price * ($bookingpricedata->perioddiscount/100));
            }
             if($bookingpricedata->promocodediscount!=0){
             
               $totalprice=$totalprice - ($price * ($bookingpricedata->promocodediscount/100));
               $promocodediscountfee=($price * ($bookingpricedata->promocodediscount/100));
            }
            if($bookingpricedata->vatdiscount!=0){
             
               $totalprice=$totalprice + ($price * ($bookingpricedata->vatdiscount/100));
               $vatfee=($price * ($bookingpricedata->vatdiscount/100));
            }
          
            
            
             $nighttimefee=round($nighttimefee, 2);
             $notworkingdayfee=round($notworkingdayfee, 2);
             $maydecdayfee=round($maydecdayfee, 2);
             $drivermealfee=round($drivermealfee, 2);
             $driverrestfee=round($driverrestfee, 2);
             $welcomediscountfee=round($welcomediscountfee, 2);
             $rewarddiscountfee=round($rewarddiscountfee, 2);
             $perioddiscountfee=round($perioddiscountfee, 2);
             $promocodediscountfee=round($promocodediscountfee, 2);
             $totalprice=round($totalprice, 2);
             $price=round($price, 2);



        

             $bookdataupdate = $this->bookings_model->updatebookingrecord('vbs_bookings',[
             'customprice'        => $cutomprice,
             'cost_of_journey'    => $price,
             'price'              => $price,
             'totalprice'         => $totalprice,
              ],['id'=>$booking_id]);

             if($bookdataupdate){
              $bookingpricevalueupdate = $this->bookings_model->updatebookingrecord('vbs_bookings_pricevalues',[
              'nighttimefee'         => $nighttimefee,
              'notworkingdayfee'     => $notworkingdayfee,
              'maydecdayfee'         => $maydecdayfee,
              'drivermealfee'        => $drivermealfee,
              'driverrestfee'        => $driverrestfee,
              'nighttimediscount'    => $nighttimediscount,
              'notworkingdaydiscount'=> $notworkingdaydiscount,
              'maydecdaydiscount'    => $maydecdaydiscount,
              'welcomediscountfee'   => $welcomediscountfee,
              'perioddiscountfee'    => $perioddiscountfee,
              'rewarddiscountfee'    => $rewarddiscountfee,
              'promocodediscountfee' => $promocodediscountfee,
              'vatfee'               => $vatfee    
               ],['booking_id'=>$booking_id]);

               if($bookdataupdate){
                   $this->data['dbdiscountandfeedata']=$this->bookings_model->getdbbookingrecord('vbs_bookings_pricevalues',array('booking_id'=>$booking_id));
                   $priceview=$this->load->view('admin/quotes/update_price_values', $this->data, TRUE);
                    $data = ['price' => $price,'totalprice' => $totalprice,'priceview'=>$priceview,'result'=>"200"];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
             }
             else{
                    $data = ['result'=>"201"];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
             }
        }else{
                    $data = ['result'=>"201"];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
        }
            
}

//update price by custom price   
   public function add_quote_notification(){
   $booking_id=$_GET['bookingid'];
   $notification_date=$_GET['date'];
   $notification_time=$_GET['time'];

   $issent=0;

    $module='2';
    $notificationstatuspending='1';
    $notificationdata = $this->quotes_model->getremindersnotification('vbs_notifications',['department' => $module,'status'=>$notificationstatuspending]);

    $quotes=$this->quotes_model->getsinglerecord('vbs_quotes',array('booking_id'=>$booking_id));

   $currentdatetime=date("d/m/Y H : i");
   $notificationdatetime=$notification_date.' '.$notification_time;
   if($currentdatetime >= $notificationdatetime){
      //Email Send
      $format_notification_date=str_replace('/', '-', $notification_date);
      $format_notification_date=strtotime($format_notification_date);
      $format_notification_date = date('Y-m-d',$format_notification_date);

      $notificationsentstatus= "Sent";
      $issent=1;
      $quotenotificationid = $this->quotes_model->addrecord('vbs_quotes_notification',[
                        'date'            => $format_notification_date,
                        'time'            => $notification_time,
                        'quote_id'        => $quotes->id,
                        'notification_id' => $notificationdata->id,
                        'issent'          => $issent
                      ]);

            if($quotenotificationid){
                   //Email Send
                     $isemailsent=$this->sendemailnotification($quotenotificationid,$booking_id);
                      if($isemailsent){
                            $data = ['date' => $_GET['date'],'time' => $_GET['time'],'status'=>$notificationsentstatus,'result'=>"200",'id'=>$quotenotificationid,'bookingid'=>$booking_id];
                            header('Content-Type: application/json'); 
                            echo json_encode($data);
                      }else{
                            $delqnt=$this->quotes_model->deletequotedata('vbs_quotes_notification',['id'=>$quotenotificationid]);
                             $data = ['result'=>"202",'error'=>"Email could not be sent."];
                             header('Content-Type: application/json'); 
                             echo json_encode($data);
                      }
                     //Email Send         
                }else{
                   $data = ['result'=>"201",'error'=>'something went wrong.'];
                   header('Content-Type: application/json'); 
                   echo json_encode($data);
                 }
     //Email Send
   }else{
      $notification_date=str_replace('/', '-', $notification_date);
      $notification_date=strtotime($notification_date);
      $notification_date = date('Y-m-d',$notification_date);
      
      $notificationsentstatus= "Pending";
      $quotenotificationid = $this->quotes_model->addrecord('vbs_quotes_notification',[
                'date'            => $notification_date,
                'time'            => $notification_time,
                'quote_id'        => $quotes->id,
                'notification_id' => $notificationdata->id,
                'issent'          => $issent
               ]);
      
            if($quotenotificationid){
             
                   $data = ['date' => $_GET['date'],'time' => $_GET['time'],'status'=>$notificationsentstatus,'result'=>"200",'id'=>$quotenotificationid,'bookingid'=>$booking_id];
                   header('Content-Type: application/json'); 
                   echo json_encode($data);
            }
            else{
              
                   $data = ['result'=>"201",'error'=>'something went wrong.'];
                   header('Content-Type: application/json'); 
                   echo json_encode($data);
            }
   }

  

}
 public function add_quote_reminder(){
  $booking_id=$_GET['bookingid'];
  $reminderid=$_GET['reminderid'];
  $reminder_date=$_GET['date'];
  $reminder_time=$_GET['time'];
  $level=$_GET['level'];

  $issent=0;

  

   $quotes=$this->quotes_model->getsinglerecord('vbs_quotes',array('booking_id'=>$booking_id));

  $currentdatetime=date("d/m/Y H : i");
  $reminderdatetime=$reminder_date.' '.$reminder_time;
  if($currentdatetime >= $reminderdatetime){
     //Email Send
     $format_reminder_date=str_replace('/', '-', $reminder_date);
     $format_reminder_date=strtotime($format_reminder_date);
     $format_reminder_date = date('Y-m-d',$format_reminder_date);

     $remindersentstatus= "Sent";
     $issent=1;
     $quotereminderid = $this->quotes_model->addrecord('vbs_quotes_reminder',[
                       'date'            => $format_reminder_date,
                       'time'            => $reminder_time,
                       'quote_id'        => $quotes->id,
                       'reminder_id'     => $reminderid,
                       'issent'          => $issent,
                       'type'            => $level
                     ]);

           if($quotereminderid){
                  //Email Send
                    $isemailsent=$this->sendemailreminder($quotereminderid,$level,$booking_id);
                     if($isemailsent){
                           $data = ['date' => $_GET['date'],'time' => $_GET['time'],'status'=>$remindersentstatus,'result'=>"200",'id'=>$quotereminderid,'bookingid'=>$booking_id,'level'=>$level];
                           header('Content-Type: application/json'); 
                           echo json_encode($data);
                     }else{
                           $airport=$this->quotes_model->deletequotedata('vbs_quotes_reminder',['id'=>$quotereminderid]);
                            $data = ['result'=>"202",'error'=>"Email could not be sent."];
                            header('Content-Type: application/json'); 
                            echo json_encode($data);
                     }
                    //Email Send         
               }else{
                  $data = ['result'=>"201",'error'=>'something went wrong.'];
                  header('Content-Type: application/json'); 
                  echo json_encode($data);
                }
    //Email Send
  }else{
     $reminder_date=str_replace('/', '-', $reminder_date);
     $reminder_date=strtotime($reminder_date);
     $reminder_date = date('Y-m-d',$reminder_date);
     
     $remindersentstatus= "Pending";
     $quotereminderid = $this->quotes_model->addrecord('vbs_quotes_reminder',[
               'date'            => $reminder_date,
               'time'            => $reminder_time,
               'quote_id'        => $quotes->id,
               'reminder_id'     => $reminderid,
               'issent'          => $issent,
               'type'            => $level
              ]);
     
           if($quotereminderid){
            
                  $data = ['date' => $_GET['date'],'time' => $_GET['time'],'status'=>$remindersentstatus,'result'=>"200",'id'=>$quotereminderid,'bookingid'=>$booking_id,'level'=>$level];
                  header('Content-Type: application/json'); 
                  echo json_encode($data);
           }
           else{
             
                  $data = ['result'=>"201",'error'=>'something went wrong.'];
                  header('Content-Type: application/json'); 
                  echo json_encode($data);
           }
  }

  

}
public function getbookingsbyclient(){
   $client_id=(int)$_GET['client_id'];
   $isconformed='0';
    if($client_id>0){
       $bookings=$this->quotes_model->getmultiplerecord("vbs_bookings",['user_id'=>$client_id,'is_conformed'=>$isconformed]);
     
       $result='<option value="">&#xf02d; Bookings</option>';
               foreach($bookings as $item){
                $result.='<option  value="'.$item->id.'">&#xf02d; '.create_timestampdmy_uid($item->bookdate,$item->id).'</option>';
             }
      }else{
       $result='<option value="">&#xf02d; Bookings</option>';
    }
    echo $result;
    exit;  
}
public function sendemailreminder($quotereminderid,$level,$booking_id){
$pdfReminderArrays=createreminderpdf($quotereminderid,$level,$booking_id); 
$reminderpdffile=$pdfReminderArrays['pdf']->Output(strtolower($pdfReminderArrays['fileName']).".pdf", 'S');
$filename=$pdfReminderArrays['fileName'];


$pdfArrays=createstringquotepdf($booking_id);   
$quotepdffile=$pdfArrays['pdf']->Output(strtolower($pdfArrays['fileName']).".pdf", 'S');
$quotepdffilename=$pdfArrays['fileName'];

$dbbookingdata=$this->bookings_model->getdbbookingrecord('vbs_bookings',array('id'=>$booking_id));
$client=$this->bookings_model->getclientdatabyid($dbbookingdata->user_id);
  //Email Sent

$content="Hi ".$client->civility." ".$client->first_name." ".$client->last_name.",<br>";
$content.="You 've received reminder letter from ECAB LLC. Please check these files.";
                    $this->load->library('mail_bookingsender');
                    $mailData['email']   =  $client->email;
                    $mailData['subject'] = "You've received messages from ECAB LLC";
                    $mailData['message']    = $content;
                    $mailData['firstpdffile']    = $reminderpdffile;
                    $mailData['secondpdffile']    = $quotepdffile;
                    $mailData['firstpdffilename']    = strtolower($filename).".pdf";
                    $mailData['secondpdffilename']    = strtolower($quotepdffilename).".pdf";
                


                    $sent = $this->mail_bookingsender->sendMail($mailData);
                    return $sent;

    //create pdf
}

public function sendemailnotification($quotenotificationid,$booking_id){

$pdfNotificationArrays=createnotificationpdf($quotenotificationid,$booking_id); 
$pdffile=$pdfNotificationArrays['pdf']->Output(strtolower($pdfNotificationArrays['fileName']).".pdf", 'S');
$filename=$pdfNotificationArrays['fileName'];


$pdfArrays=createstringquotepdf($booking_id);   
$quotepdffile=$pdfArrays['pdf']->Output(strtolower($pdfArrays['fileName']).".pdf", 'S');
$quotepdffilename=$pdfArrays['fileName'];

$dbbookingdata=$this->bookings_model->getdbbookingrecord('vbs_bookings',array('id'=>$booking_id));
$client=$this->bookings_model->getclientdatabyid($dbbookingdata->user_id);
  //Email Sent
$content="Hi ".$client->civility." ".$client->first_name." ".$client->last_name.",<br>";
$content.="You 've received notification letter from ECAB LLC. Please check these files.";
                     $this->load->library('mail_bookingsender');
                    $mailData['email']   = $client->email;
                    $mailData['subject'] = "You've received messages from ECAB LLC";
                    $mailData['message']    = $content;
                    $mailData['firstpdffile']    = $pdffile;
                    $mailData['secondpdffile']    = $quotepdffile;
                    $mailData['firstpdffilename']    = strtolower($filename).".pdf";
                    $mailData['secondpdffilename']    = strtolower($quotepdffilename).".pdf";
                


                    $sent = $this->mail_bookingsender->sendMail($mailData);
                    return $sent;
                    /*if($sent === true) {
                       echo "message sent";
                    }else{
                       echo "message not sent";
                    }*/
       
  //Email sent   
 
        //exit();
}

public function addcustomreminders(){
  
  $quoteid=$_GET['quoteid'];
  $title=$_GET['title'];
  $date=$_GET['date'];
  $time=$_GET['time'];
  $comment=$_GET['comment'];

  //add custom comment
          
          
               
                     $date=$date;
                     $date=str_replace('/', '-', $date);
                     $date=strtotime($date);
                     $date = date('Y-m-d',$date);
                      $crid = $this->quotes_model->addrecord("vbs_quotes_customreminders",[
                        'date'     => $date,
                        'time'     => $time,
                        'title'    => $title,
                        'comment'  => $comment,
                        'quote_id' => $quoteid
                        ]);
                  
             if($crid){
                    $record=array(
                        'date'     => $_GET['date'],
                        'time'     => $time,
                        'title'    => $title,
                        'comment'  => $comment,
                        'quote_id' => $quoteid,
                        'customreminderid'=>$crid
                    );
                    $data = ['result'=>"200",'record'=>$record];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
             }
             else{
                    $data = ['result'=>"201",'error'=>'something went wrong.'];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
             }
}
public function removecustomreminders(){
  $customreminderid=$_GET['customreminderid'];
  $del=$this->quotes_model->deletequotedata('vbs_quotes_customreminders',['id'=>$customreminderid]);
  if($del){
                    $data = ['result'=>"200"];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
  }else{
                    $data = ['result'=>"201",'error'=>'something went wrong.'];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
  }
}

public function addquoteinvoicerecord(){
  $bookingid=$_POST['bookingid'];
  $quotestatus=$_POST['quotestatus'];
  $approveddate=date("Y-m-d");
  $dataupdate = $this->quotes_model->updaterecord('vbs_quotes',[
                 'status'       => $quotestatus,
                 'approveddate' => $approveddate
                ],['booking_id' => $bookingid]);
  
 if($dataupdate){
  $quotes=$this->bookings_model->getdbbookingrecord('vbs_quotes',array('booking_id'=>$bookingid));
            if($quotestatus==1){
               $invoiceid = $this->bookings_model->addbookingrecord('vbs_invoices',[
                        'date'            => @date('Y-m-d'),
                        'time'            => @date('h : i'),
                        'booking_id'      =>  $bookingid,
                        'quote_id'        =>  $quotes->id
                      ]);

                  $module='4';
                  $statuspending='2';
                  $notificationstatuspending='1';
                  $remindersdata   = $this->quotes_model->getmultiplerecord('vbs_reminders',['module' => $module,'status'=> $statuspending]);
          if($remindersdata){
            $remindertype=1;
           foreach ($remindersdata as $reminder) {
                 $reminders = json_decode($reminder->reminders);
                 $date         = date('Y-m-d');
                 $days         = $reminders[0]->reminder_days;
                 $reminderdate = date('Y-m-d', strtotime($date. ' + '.$days.' days'));
                 $remindertime = date('h : i');
                 
                  $reminderid = $this->quotes_model->addrecord('vbs_invoices_reminder',[
                                  'date'            => $reminderdate,
                                  'time'            => $remindertime,
                                  'invoice_id'      => $invoiceid,
                                  'reminder_id'     => $reminder->id,
                                   'type'            => $remindertype
                                 ]); 
                  $remindertype++;

           }
         }
            $notificationdata     = $this->quotes_model->getremindersnotification('vbs_notifications',['department' => $module,'status'=>$notificationstatuspending]);
                     if($notificationdata){
                     $notificationid = $this->quotes_model->addrecord('vbs_invoices_notification',[
                       'date'            => date('Y-m-d'),
                       'time'            => date('h : i'),
                       'invoice_id'      => $invoiceid,
                       'notification_id' => $notificationdata->id
                      ]);
                       }

                         $this->session->set_flashdata('alert', [
                            'message' => " :Invoice record is added.",
                            'class' => "alert-success",
                            'type' => "Congratulation"
                        ]);
                     return redirect("admin/invoices");  
              }else{
              $this->session->set_flashdata('alert', [
                  'message' => " :something went wrong. please try again",
                  'class' => "alert-danger",
                  'type' => "Error"
              ]);
           return redirect("admin/invoices");
} 
}else{
   $this->session->set_flashdata('alert', [
                  'message' => " :something went wrong. please try again",
                  'class' => "alert-danger",
                  'type' => "Error"
              ]);
           return redirect("admin/invoices");
}
}
/*----------------------------------------------Esport Excel-----------------------------------------------*/
  //export excel file
  public function export_excel_file(){
  $this->load->model("bookings_model");
  $this->load->model("bookings_config_model");
  
  $user = $this->basic_auth->user();
  $user_id=$user->id;
  $user=$this->bookings_model->getclientdatabyid($user_id);
  $usercity=$this->bookings_model->getsinglerecord('vbs_cities',['id'=>$user->city])->name;

  $companydata=$this->bookings_model->getcompanystatus();
  $companycity=$this->bookings_model->getsinglerecord('vbs_cities',['id'=>$companydata->city])->name;

  $companylogo=base_url().'uploads/company/'.$companydata->logo;
  /* Company information */
$cominfo1="\n".$companydata->address."\n";
if(!empty($companydata->address2)){
  $cominfo1.=$companydata->address2."\n";
}
$cominfo1.=$companydata->zip_code.' '.$companycity."\nPhone      :  ".$companydata->phone."\nFax            :  ".$companydata->fax."\nEmail       :  ".$companydata->email."\nWebsite :  ".$companydata->website;
/* Company information */
  /* Client information */
 $userinfo="";
   if(!empty($user->address)){
    $userinfo.="\n".$user->address."\n";
  }
  if(!empty($user->address1)){
    $userinfo.=$user->address1."\n";
  }
  $userinfo.=$user->zipcode.'  '.$usercity."\nPhone   :  ".$user->phone."\nFax         :  ".$user->fax."\nEmail     :  ".$user->email;
  /* Client information */


  $this->load->library("excel");
  $object = new PHPExcel();

  $object->setActiveSheetIndex(0);

  //table data
  $column = 0;
  $colEng='A';
  $table_columns = array("Id", "Date","Time", "Name","Service Category", "Service", "Notification", "Reminder 1", "Reminder 2", "Reminder 3", "Price", "Payment","Statut","Since");
  foreach($table_columns as $field)
  {
   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 11, $field);
   $column++;
  }
  for($i=1;$i<$column;$i++){
    $colEng++;
  }
  
  $quotes_data = $this->quotes_model->getquotesallrecords();
  $excel_row = 12;

 $notificationpendingstatus="1";
 $reminderpendingstatus="2";
  foreach($quotes_data as $item)
  {
     
    //get data
     $userclient=$this->bookings_config_model->getuser($item->user_id);
     $userclient=str_replace(".", " ", $userclient);

     $quotereminderdata=$this->quotes_model->getquotereminders(['quote_id'=> $item->id,'rm.status'=> $reminderpendingstatus]);
     $quotenotificationdata=$this->quotes_model->getquotenotification(['quote_id'=> $item->id,'nt.status'=>$notificationpendingstatus]);

      $notificationdatetime='-';
      $reminderdatetime='-';
      $status="-";

     if($quotenotificationdata){
            $notificationdatetime=from_unix_date($quotenotificationdata->date).' '.$quotenotificationdata->time;
     }
    
      if($item->quotestatus== 0){
        $status="Pending";
       }
     elseif($item->quotestatus== 1){
       $status="Accepted";
     }
    elseif($item->quotestatus== 2){
      $status="Denied";
      }
      elseif($item->quotestatus== 3){
      $status="Cancelled";
      }

    //get data
   $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, create_timestampdmy_uid($item->quotedate,$item->id));
   $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, from_unix_date($item->quotedate));
   $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $item->quotetime);
   $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $userclient);
   $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $item->servicecat_name);
   $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $item->service_name);
   $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $notificationdatetime);
   if($quotereminderdata){
    $count=0;
    $num=0;
        foreach ($quotereminderdata as $reminder){
         if($count < 3){
         $object->getActiveSheet()->setCellValueByColumnAndRow(7+$num, $excel_row, from_unix_date($reminder->date).' '.$reminder->time);
        }
        $count++;
        $num++;
      } 
   }else{
    $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, "-");
    $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, "-");
    $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, "-");
   }
 
   $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $item->totalprice);
   $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $item->payment_name);
   $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $status);
   $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $item->created_at);

  
   $excel_row++;
  }
  //table data
 
//$object->getActiveSheet()->setCellValue('A34', $colEng);
  
 $object->getActiveSheet()->mergeCells('A1:B4');
 $object->getActiveSheet()->mergeCells('A5:B7');
 $object->getActiveSheet()->mergeCells('C1:L2');
 $object->getActiveSheet()->mergeCells('C3:L7');
 $object->getActiveSheet()->mergeCells('M1:N4');
 $object->getActiveSheet()->mergeCells('M5:N7');
 $object->getActiveSheet()->mergeCells('A9:B9');
 $object->getActiveSheet()->mergeCells('A'.($excel_row+1).':'.$colEng.($excel_row+1));

//style
$style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );
 $object->getDefaultStyle()->applyFromArray($style);
 $object->getActiveSheet()->getDefaultColumnDimension()->setWidth(20);

$object->getActiveSheet()
    ->getStyle("A12:A".($excel_row-1))
    ->getNumberFormat()
    ->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

$object->getActiveSheet()
        ->getStyle("C1:L2")
        ->applyFromArray(
          array(
            'font' => array(
                      'bold' => true,
                      'size' => 24
                  ),
            "borders" =>  array(
                            'allborders'     =>array(
                                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                                        'color' => array('rgb'=>'000000')
                                        )
                          ),
          ));
$object->getActiveSheet()
        ->getStyle("A5:B7")
        ->applyFromArray(
          array(
            "borders" =>  array(
                            'allborders'     =>array(
                                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                                        'color' => array('rgb'=>'000000')
                                        )
                          ),
          ));
$object->getActiveSheet()
        ->getStyle("M5:N7")
        ->applyFromArray(
          array(
            "borders" =>  array(
                            'allborders'     =>array(
                                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                                        'color' => array('rgb'=>'000000')
                                        )
                          ),
          ));
$object->getActiveSheet()
        ->getStyle("A9:B9")
        ->applyFromArray(
          array(
            "borders" =>  array(
                            'allborders'     =>array(
                                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                                        'color' => array('rgb'=>'000000')
                                        )
                          ),
          ));

$object->getActiveSheet()
        ->getStyle('A'.($excel_row+1).':'.$colEng.($excel_row+1))
        ->applyFromArray(
          array(
            "borders" =>  array(
                            'allborders'     =>array(
                                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                                        'color' => array('rgb'=>'000000')
                                        )
                          ),
          ));

$object->getActiveSheet()
        ->getStyle('A11'.':'.$colEng.'11')
        ->applyFromArray(
          array(
            'font' => array(
                      'bold' => true,
                  ),
            "borders" =>  array(
                            'allborders'     =>array(
                                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                                        'color' => array('rgb'=>'A9A9A9')
                                        )
                          ),
               'fill' => array(
                          'type' => PHPExcel_Style_Fill::FILL_SOLID,
                          'color' => array('rgb' => 'f1f1f1')
                      )
          ));  
$object->getActiveSheet()
        ->getStyle('A12'.':'.$colEng.($excel_row-1))
        ->applyFromArray(
          array(
            "borders" => array(
                            'allborders'   =>array(
                                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                                        'color' => array('rgb'=>'A9A9A9')
                                        )
                          ),
          ));

//style   

  $title_data="QUOTES";
  $object->getActiveSheet()->setCellValue("C1", $title_data);

  //company information start
  $objRichText = new PHPExcel_RichText();
  $objBold = $objRichText->createTextRun(strtoupper($companydata->name).' '.strtoupper($companydata->type));
  $objBold->getFont()->setBold(true);
  $objRichText->createText($cominfo1);

  $object->getActiveSheet()->getCell('A5')->setValue($objRichText);
  $object->getActiveSheet()->getStyle('A5')->getAlignment()->setWrapText(true);
  $object->getActiveSheet()->getStyle('A5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
  //company information close

   //client information start
  $objRichText2 = new PHPExcel_RichText();
  $objBold = $objRichText2->createTextRun($user->civility.' '.$user->first_name.' '.$user->last_name);
  $objBold->getFont()->setBold(true);
  $objRichText2->createText($userinfo);

  $object->getActiveSheet()->getCell('M5')->setValue($objRichText2);
  $object->getActiveSheet()->getStyle('M5')->getAlignment()->setWrapText(true);
  $object->getActiveSheet()->getStyle('M5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
  //client information close
 
 //Download Date Time
   $download_datetime=date("d/m/Y").' '.date("h:i:sa");
   $object->getActiveSheet()->setCellValue("A9", $download_datetime);
   $object->getActiveSheet()->getStyle('A9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
  //Download Date Time

   //Footer Info
  $address2=(!empty($companydata->address2))?trim($companydata->address2).', ' :'';
  $companydatainfo=strtoupper($companydata->name).' '.strtoupper($companydata->type).', '.$companydata->address.', '.$address2.$companydata->zip_code.', '.$companycity.', Capital : '.$companydata->capital.', License N : '.$companydata->licence.', SIRET : '.$companydata->sirft.', VAT N';

    $objRichText3 = new PHPExcel_RichText();
    $objRichText3->createText($companydatainfo);
    $objBold = $objRichText3->createTextRun('o');
    $objBold->getFont()->setSuperScript(true);
    $objRichText3->createText(' : '.$companydata->numero_tva);
    

   $object->getActiveSheet()->setCellValue('A'.($excel_row+1), $objRichText3);
   $object->getActiveSheet()->getStyle('A'.($excel_row+1))->getAlignment()->setWrapText(true);
   $object->getActiveSheet()->getStyle('A'.($excel_row+1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  //Footer Info
  
  //Company Logo 
    $objDrawing = new PHPExcel_Worksheet_Drawing();

    $objDrawing->setName('company_logo');
    $objDrawing->setDescription('company_logo');
    $objDrawing->setPath($companylogo);                  
    //setOffsetX works properly
    $objDrawing->setOffsetX(8); 
    $objDrawing->setOffsetY(4); 
    $objDrawing->setCoordinates('A1');                
    //set width, height
    $objDrawing->setWidth(350); 
    $objDrawing->setHeight(70); 
    $objDrawing->setWorksheet($object->getActiveSheet());
  //Company Logo



  $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
  header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Quotes.xls"');
  $object_writer->save('php://output');
    }
 /*---------------------------------------Export Excel--------------------------------------------------*/  
} 
