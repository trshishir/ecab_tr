<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Accounting_config extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->lang->load('auth');
		$this->lang->load('general');
		$this->load->helper('language');
		if (!$this->basic_auth->is_login())
			redirect("admin", 'refresh');
		else
			$this->data['user'] = $this->basic_auth->user();
		$this->data['configuration'] = get_configuration();
        $session = $this->session->userdata('user');
        $this->department =  $session->department_id;
	}
	public function index(){
		$this->data['css_type'] 	= array("form","datatable");
		$this->data['active_class'] = "Config";
		$this->data['gmaps'] 		= false;
		$this->data['title'] 		= $this->lang->line("accounting configurations");
		$this->data['title_link'] 	= base_url('admin/accounting_config.php');

		$this->data['acc_statuts'] = $this->my_model->get_table_row_query("SELECT vbs_accounting_statuts.*,vbs_users.civility,vbs_users.first_name,vbs_users.last_name,vbs_departments.name as department_name FROM vbs_accounting_statuts LEFT JOIN vbs_departments on vbs_accounting_statuts.from_department_id = vbs_departments.id LEFT JOIN vbs_users ON vbs_users.id = vbs_accounting_statuts.added_by");
        // for VAT
        $this->data['vat'] = $this->my_model->get_table_row_query("SELECT vbs_account_vat.*,vbs_users.civility,vbs_users.first_name,vbs_users.last_name,vbs_departments.name as department_name FROM vbs_account_vat LEFT JOIN vbs_departments on vbs_account_vat.from_department_id = vbs_departments.id LEFT JOIN vbs_users ON vbs_users.id = vbs_account_vat.added_by");
        // for payment method tab
        $this->data['payment_methode'] = $this->my_model->get_table_row_query("SELECT vbs_accounting_payment_methodes.*,vbs_users.civility,vbs_users.first_name,vbs_users.last_name,vbs_departments.name as department_name FROM vbs_accounting_payment_methodes  LEFT JOIN vbs_departments on vbs_accounting_payment_methodes.from_department_id = vbs_departments.id LEFT JOIN vbs_users ON vbs_users.id = vbs_accounting_payment_methodes.added_by");
        // for Garantee Fund
        $this->data['garantee_fund'] = $this->my_model->get_table_row_query("SELECT vbs_account_garantee_fund.*,vbs_users.civility,vbs_users.first_name,vbs_users.last_name,vbs_departments.name as department_name FROM vbs_account_garantee_fund  LEFT JOIN vbs_departments on vbs_account_garantee_fund.from_department_id = vbs_departments.id LEFT JOIN vbs_users ON vbs_users.id = vbs_account_garantee_fund.added_by");
        // for Garantee Fund
        $this->data['financement_delay'] = $this->my_model->get_table_row_query("SELECT vbs_account_financement_delay.*,vbs_users.civility,vbs_users.first_name,vbs_users.last_name,vbs_departments.name as department_name FROM vbs_account_financement_delay  LEFT JOIN vbs_departments on vbs_account_financement_delay.from_department_id = vbs_departments.id LEFT JOIN vbs_users ON vbs_users.id = vbs_account_financement_delay.added_by");
        
        // For payments delays
        $this->data['payment_delay'] = $this->my_model->get_table_row_query("SELECT vbs_accounting_payment_delays.*,vbs_users.civility,vbs_users.first_name,vbs_users.last_name,vbs_departments.name as department_name FROM vbs_accounting_payment_delays  LEFT JOIN vbs_departments on vbs_accounting_payment_delays.from_department_id = vbs_departments.id LEFT JOIN vbs_users ON vbs_users.id = vbs_accounting_payment_delays.added_by");
        // For Bill Category
        $this->data['bill_category'] = $this->my_model->get_table_row_query("SELECT vbs_account_bill_category.*,vbs_users.civility,vbs_users.first_name,vbs_users.last_name,vbs_departments.name as department_name, vbs_account_vat.vat,vbs_accounting_payment_methodes.payment_methode FROM vbs_account_bill_category  LEFT JOIN vbs_departments on vbs_account_bill_category.from_department_id = vbs_departments.id LEFT JOIN vbs_users ON vbs_users.id = vbs_account_bill_category.added_by LEFT JOIN vbs_account_vat on vbs_account_vat.id = vbs_account_bill_category.vat_id LEFT JOIN vbs_accounting_payment_methodes on vbs_accounting_payment_methodes.id = vbs_account_bill_category.payment_methode_id ");
        // For Bill Category
        $this->data['sales_category'] = $this->my_model->get_table_row_query("SELECT vbs_account_sales_category.*,vbs_users.civility,vbs_users.first_name,vbs_users.last_name,vbs_departments.name as department_name,vbs_account_vat.vat,vbs_accounting_payment_methodes.payment_methode,vbs_accounting_payment_delays.payment_delay FROM vbs_account_sales_category  LEFT JOIN vbs_departments on vbs_account_sales_category.from_department_id = vbs_departments.id LEFT JOIN vbs_users ON vbs_users.id = vbs_account_sales_category.added_by LEFT JOIN vbs_account_vat on vbs_account_sales_category.vat_id = vbs_account_vat.id LEFT JOIN vbs_accounting_payment_methodes on vbs_account_sales_category.payment_methode_id = vbs_accounting_payment_methodes.id LEFT JOIN vbs_accounting_payment_delays on vbs_account_sales_category.payment_delay_id = vbs_accounting_payment_delays.id ");
        // For client Category
        $this->data['client_category'] = $this->my_model->get_table_row_query("SELECT vbs_account_client_category.*,vbs_users.civility,vbs_users.first_name,vbs_users.last_name,vbs_departments.name as department_name,vbs_accounting_payment_methodes.payment_methode FROM vbs_account_client_category  LEFT JOIN vbs_departments on vbs_account_client_category.from_department_id = vbs_departments.id LEFT JOIN vbs_users ON vbs_users.id = vbs_account_client_category.added_by LEFT JOIN vbs_accounting_payment_methodes ON vbs_accounting_payment_methodes.id = vbs_account_client_category.payment_methode_id");

        // For Supplier accounting_organization_add 
        $this->data['supplier'] = $this->my_model->get_table_row_query("SELECT vbs_account_supplier.*,vbs_users.civility,vbs_users.first_name,vbs_users.last_name,vbs_departments.name as department_name,vbs_accounting_payment_delays.payment_delay,vbs_account_suppliers_category.supplier_category FROM vbs_account_supplier  LEFT JOIN vbs_departments on vbs_account_supplier.from_department_id = vbs_departments.id LEFT JOIN vbs_users ON vbs_users.id = vbs_account_supplier.added_by LEFT JOIN vbs_accounting_payment_delays ON vbs_accounting_payment_delays.id = vbs_account_supplier.payment_delay_id LEFT JOIN vbs_account_suppliers_category ON vbs_account_suppliers_category.id = vbs_account_supplier.supplier_category_id");
        // For Supplier category 
        $this->data['supplier_category'] = $this->my_model->get_table_row_query("SELECT vbs_account_suppliers_category.*,vbs_users.civility,vbs_users.first_name,vbs_users.last_name,vbs_departments.name as department_name FROM vbs_account_suppliers_category  LEFT JOIN vbs_departments on vbs_account_suppliers_category.from_department_id = vbs_departments.id LEFT JOIN vbs_users ON vbs_users.id = vbs_account_suppliers_category.added_by");
        // For organization
        $this->data['organization'] = $this->my_model->get_table_row_query("SELECT vbs_account_organization.*,vbs_users.civility,vbs_users.first_name,vbs_users.last_name,vbs_departments.name as department_name,vbs_account_organization_category.organization_category FROM vbs_account_organization  LEFT JOIN vbs_departments on vbs_account_organization.from_department_id = vbs_departments.id LEFT JOIN vbs_users ON vbs_users.id = vbs_account_organization.added_by LEFT JOIN vbs_account_organization_category ON vbs_account_organization_category.id = vbs_account_organization.org_category_id");
        // For organization category
        $this->data['organization_category'] = $this->my_model->get_table_row_query("SELECT vbs_account_organization_category.*,vbs_users.civility,vbs_users.first_name,vbs_users.last_name,vbs_departments.name as department_name FROM vbs_account_organization_category  LEFT JOIN vbs_departments on vbs_account_organization_category.from_department_id = vbs_departments.id LEFT JOIN vbs_users ON vbs_users.id = vbs_account_organization_category.added_by");
        // For banks
        $this->data['banks'] = $this->my_model->get_table_row_query("SELECT vbs_account_banks.*,vbs_users.civility,vbs_users.first_name,vbs_users.last_name,vbs_departments.name as department_name FROM vbs_account_banks  LEFT JOIN vbs_departments on vbs_account_banks.from_department_id = vbs_departments.id LEFT JOIN vbs_users ON vbs_users.id = vbs_account_banks.added_by");
        // For Factors
        $this->data['factors'] = $this->my_model->get_table_row_query("SELECT vbs_account_factors.*,vbs_users.civility,vbs_users.first_name,vbs_users.last_name,vbs_departments.name as department_name,vbs_account_financement_delay.financement_delay,vbs_account_garantee_fund.garantee_fund FROM vbs_account_factors  LEFT JOIN vbs_departments on vbs_account_factors.from_department_id = vbs_departments.id LEFT JOIN vbs_users ON vbs_users.id = vbs_account_factors.added_by LEFT JOIN vbs_account_financement_delay ON vbs_account_financement_delay.id = vbs_account_factors.finacement_delay_id LEFT JOIN vbs_account_garantee_fund ON vbs_account_garantee_fund.id = vbs_account_factors.garantee_fund_id");
        // For Factors
        $this->data['client_kind'] = $this->my_model->get_table_row_query("SELECT vbs_accounting_client_kind.*,vbs_users.civility,vbs_users.first_name,vbs_users.last_name,vbs_departments.name as department_name,vbs_accounting_payment_delays.payment_delay FROM vbs_accounting_client_kind  LEFT JOIN vbs_departments on vbs_accounting_client_kind.from_department_id = vbs_departments.id LEFT JOIN vbs_users ON vbs_users.id = vbs_accounting_client_kind.added_by LEFT JOIN vbs_accounting_payment_delays on vbs_accounting_payment_delays.id = vbs_accounting_client_kind.payment_delay_id");

		$this->data['content'] 		= 'admin/accounting_config/index';
		$this->data['grid_page'] 		= 'admin/accounting_config/grid_js';
		$this->_render_page('templates/admin_template_accountingConfig',$this->data);

	}
	public function accounting_statuts_add(){
		extract($_POST);
		$data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'status'=>$status,
            'statuts'=>$statuts,
            'from_department_id'=>$this->department,
            'created_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->general_model->add('vbs_accounting_statuts',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                'message' => "Statuts Added Successfully.",
                'class' => "alert-success",
                'type' => "Success",
            ]);
            redirect('admin/accounting_config');
        }
	}
	public function accounting_statuts_update(){
		extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'status'=>$status,
            'statuts'=>$statuts,
            'from_department_id'=>$this->department,
            'updated_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->my_model->update_table(array('id'=>$id),'vbs_accounting_statuts',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                    'message' => "Statuts Updated Successfully.",
                    'class' => "alert-success",
                    'type' => "Success",
                ]);
            redirect('admin/accounting_config');
        }
	}
    public function accounting_payment_methode_add(){
        extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'payment_methode'=>$payment_methode,
            'created_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->general_model->add('vbs_accounting_payment_methodes',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                'message' => "Payment Methode Added Successfully.",
                'class' => "alert-success",
                'type' => "Success",
                'active_tab' => "payment_methode"
            ]);
            $this->session->set_flashdata('active_tab', 'pay_methode');
            redirect('admin/accounting_config');
        } 
    }
    public function accounting_payment_methode_update(){
        extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'payment_methode'=>$payment_methode,
            'updated_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->my_model->update_table(array('id'=>$id),'vbs_accounting_payment_methodes',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                'message' => "Payment Methode Updated Successfully.",
                'class' => "alert-success",
                'type' => "Success"
            ]);
            $this->session->set_flashdata('active_tab', 'pay_methode');
            redirect('admin/accounting_config');
        }
    }
    public function accounting_payment_delay_add(){
        extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'payment_delay'=>$payment_delay,
            'created_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->general_model->add('vbs_accounting_payment_delays',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                'message' => "Payment Delay Added Successfully.",
                'class' => "alert-success",
                'type' => "Success"
            ]);
            $this->session->set_flashdata('active_tab', 'pay_delay');
            redirect('admin/accounting_config');
        } 
    }
    public function accounting_payment_delay_update(){
        extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'payment_delay'=>$payment_delay,
            'updated_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->my_model->update_table(array('id'=>$id),'vbs_accounting_payment_delays',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                'message' => "Payment Delay Updated Successfully.",
                'class' => "alert-success",
                'type' => "Success"
            ]);
            $this->session->set_flashdata('active_tab', 'pay_delay');
            redirect('admin/accounting_config');
        } 
    }
    public function accounting_bill_category_add(){
       extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'bill_category'=>$bill_category,
            'vat_id'=>$vat_id,
            'payment_methode_id'=>$payment_methode_id,
            'created_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->general_model->add('vbs_account_bill_category',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                'message' => "Bill Category Added Successfully.",
                'class' => "alert-success",
                'type' => "Success"
            ]);
            $this->session->set_flashdata('active_tab', 'bill_cat');
            redirect('admin/accounting_config');
        } 
    }
    public function accounting_bill_category_update(){
        extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'bill_category'=>$bill_category,
            'vat_id'=>$vat_id,
            'payment_methode_id'=>$payment_methode_id,
            'updated_at'=>date('Y-m-d H:i:s')
        );

        $lastid=$this->my_model->update_table(array('id'=>$id),'vbs_account_bill_category',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                    'message' => "Bill Category Updated Successfully.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
            $this->session->set_flashdata('active_tab', 'bill_cat');
            redirect('admin/accounting_config');
        }
    }
    public function accounting_sales_category_add()
    {
        extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'sales_category'=>$sales_category,
            'vat_id'=>$vat_id,
            'created_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->general_model->add('vbs_account_sales_category',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                'message' => "Sales Category Added Successfully.",
                'class' => "alert-success",
                'type' => "Success"
            ]);
            $this->session->set_flashdata('active_tab', 'sales_cat');
            redirect('admin/accounting_config');
        }
    }
    public function accounting_sales_category_update()
    {
        extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'sales_category'=>$sales_category,
            'vat_id'=>$vat_id,
            'updated_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->my_model->update_table(array('id'=>$id),'vbs_account_sales_category',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                    'message' => "sales Category Updated Successfully.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
            $this->session->set_flashdata('active_tab', 'sales_cat');
            redirect('admin/accounting_config');
        }
    }
    public function accounting_client_category_add()
    {
       extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'client_category'=>$client_category,
            'payment_methode_id'=>$payment_methode_id,
            'created_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->general_model->add('vbs_account_client_category',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                'message' => "client Category Added Successfully.",
                'class' => "alert-success",
                'type' => "Success"
            ]);
            $this->session->set_flashdata('active_tab', 'client_cat');
            redirect('admin/accounting_config');
        } 
    }
    public function accounting_client_category_update()
    {
        extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'client_category'=>$client_category,
            'payment_methode_id'=>$payment_methode_id,
            'updated_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->my_model->update_table(array('id'=>$id),'vbs_account_client_category',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                    'message' => "Client Category Updated Successfully.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
            $this->session->set_flashdata('active_tab', 'client_cat');
            redirect('admin/accounting_config');
        } 
    }
    public function accounting_supplier_add()
    {
       extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'payment_delay_id'=>$payment_delay_id,
            'statut'=>$statut,
            'supplier_name'=>$supplier_name,
            'supplier_category_id'=>$supplier_category_id,
            'phone'=>$phone,
            'email'=>$email,
            'address'=>$address,
            'address2'=>$address2,
            'sup_zip_code'=>$sup_zip_code,
            'sup_city'=>$sup_city,
            'sup_country'=>$sup_country,
            'sup_contact_name'=>$sup_contact_name,
            'created_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->general_model->add('vbs_account_supplier',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                'message' => "Supplier Added Successfully.",
                'class' => "alert-success",
                'type' => "Success"
            ]);
            $this->session->set_flashdata('active_tab', 'supplier');
            redirect('admin/accounting_config');
        } 
    }
    public function accounting_supplier_update()
    {
       extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'payment_delay_id'=>$payment_delay_id,
            'supplier_category_id'=>$supplier_category_id,
            'statut'=>$statut,
            'supplier_name'=>$supplier_name,
            'phone'=>$phone,
            'email'=>$email,
            'address'=>$address,
            'address2'=>$address2,
            'sup_zip_code'=>$sup_zip_code,
            'sup_city'=>$sup_city,
            'sup_country'=>$sup_country,
            'sup_contact_name'=>$sup_contact_name,
            'created_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->my_model->update_table(array('id'=>$id),'vbs_account_supplier',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                    'message' => "Supplier Updated Successfully.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
            $this->session->set_flashdata('active_tab', 'supplier');
            redirect('admin/accounting_config');
        }
    }
    public function accounting_organization_add()
    { 
       extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'org_category_id'=>$org_category_id,
            'statut'=>$statut,
            'org_name'=>$org_name,
            'org_phone'=> $org_phone,
            'org_fax'=> $org_fax,
            'org_email'=> $org_email,
            'org_contact_name'=> $org_contact_name,
            'org_reference_number'=> $org_reference_number,
            'payment_delay_id'=> $payment_delay_id,
            'org_address'=> $org_address,
            'org_address2'=> $org_address2,
            'org_zip_code'=> $org_zip_code,
            'org_city'=> $org_city,
            'org_country'=> $org_country,
            'created_at'=> date('Y-m-d H:i:s')
        );
        $lastid=$this->general_model->add('vbs_account_organization',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                'message' => "Organization Added Successfully.",
                'class' => "alert-success",
                'type' => "Success"
            ]);
            $this->session->set_flashdata('active_tab', 'organization');
            redirect('admin/accounting_config');
        }  
    }
    public function accounting_organization_update()
    {
        extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'org_category_id'=>$org_category_id,
            'statut'=>$statut,
            'org_name'=>$org_name,
            'org_phone'=>$org_phone,
            'org_fax'=>$org_fax,
            'org_email'=>$org_email,
            'org_contact_name'=>$org_contact_name,
            'org_reference_number'=>$org_reference_number,
            'payment_delay_id'=>$payment_delay_id,
            'org_address'=>$org_address,
            'org_address2'=> $org_address2,
            'org_zip_code'=>$org_zip_code,
            'org_city'=>$org_city,
            'org_country'=> $org_country,
            'created_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->my_model->update_table(array('id'=>$id),'vbs_account_organization',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                    'message' => "Organization Updated Successfully.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
            $this->session->set_flashdata('active_tab', 'organization');
            redirect('admin/accounting_config');
        }
    }
    public function accounting_bank_add()
    {
        extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'bank_name'=>$bank_name,
            'bank_phone'=> $bank_phone,
            'bank_fax'=> $bank_fax,
            'bank_email'=> $bank_email,
            'bank_contact_name'=> $bank_contact_name,
            'bank_reference_number'=> $bank_reference_number,
            'bank_address'=> $bank_address,
            'bank_address2'=> $bank_address2,
            'bank_zip_code'=> $bank_zip_code,
            'bank_city'=> $bank_city,
            'bank_country'=> $bank_country,
            'created_at'=> date('Y-m-d H:i:s')
        );
        $lastid=$this->general_model->add('vbs_account_banks',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                'message' => "Bank Added Successfully.",
                'class' => "alert-success",
                'type' => "Success"
            ]);
            $this->session->set_flashdata('active_tab', 'bank');
            redirect('admin/accounting_config');
        }
    }
    public function accounting_bank_update()
    {
        extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'bank_name'=>$bank_name,
            'bank_phone'=> $bank_phone,
            'bank_fax'=> $bank_fax,
            'bank_email'=> $bank_email,
            'bank_contact_name'=> $bank_contact_name,
            'bank_reference_number'=> $bank_reference_number,
            'bank_address'=> $bank_address,
            'bank_address2'=> $bank_address2,
            'bank_zip_code'=> $bank_zip_code,
            'bank_city'=> $bank_city,
            'bank_country'=> $bank_country,
            'updated_at'=> date('Y-m-d H:i:s')
        );
        $lastid=$this->my_model->update_table(array('id'=>$id),'vbs_account_banks',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                    'message' => "Bank Updated Successfully.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
            $this->session->set_flashdata('active_tab', 'bank');
            redirect('admin/accounting_config');
        }
    }
    public function accounting_factor_add()
    {
        extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'finacement_delay_id'=>$finacement_delay_id,
            'garantee_fund_id'=>$garantee_fund_id,
            'statut'=>$statut,
            'factor_name'=>$factor_name,
            'factor_phone'=> $factor_phone,
            'factor_fax'=> $factor_fax,
            'factor_email'=> $factor_email,
            'factor_contact_name'=> $factor_contact_name,
            'factor_reference_number'=> $factor_reference_number,
            'factor_address'=> $factor_address,
            'factor_address2'=> $factor_address2,
            'factor_zip_code'=> $factor_zip_code,
            'factor_city'=> $factor_city,
            'factor_country'=> $factor_country,
            'created_at'=> date('Y-m-d H:i:s')
        );
        $lastid=$this->general_model->add('vbs_account_factors',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                'message' => "Factor Added Successfully.",
                'class' => "alert-success",
                'type' => "Success"
            ]);
            $this->session->set_flashdata('active_tab', 'factors');
            redirect('admin/accounting_config');
        }
    }
    public function accounting_factor_update()
    {
        extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'finacement_delay_id'=>$finacement_delay_id,
            'garantee_fund_id'=>$garantee_fund_id,
            'statut'=>$statut,
            'factor_name'=>$factor_name,
            'factor_phone'=> $factor_phone,
            'factor_fax'=> $factor_fax,
            'factor_email'=> $factor_email,
            'factor_contact_name'=> $factor_contact_name,
            'factor_reference_number'=> $factor_reference_number,
            'factor_address'=> $factor_address,
            'factor_address2'=> $factor_address2,
            'factor_zip_code'=> $factor_zip_code,
            'factor_city'=> $factor_city,
            'factor_country'=> $factor_country,
            'updated_at'=> date('Y-m-d H:i:s')
        );
        $lastid=$this->my_model->update_table(array('id'=>$id),'vbs_account_factors',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                    'message' => "Factor Updated Successfully.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
            $this->session->set_flashdata('active_tab', 'factors');
            redirect('admin/accounting_config');
        }
    }
    public function accounting_supplier_category_add()
    {
       extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'supplier_category'=>$supplier_category,
            'created_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->general_model->add('vbs_account_suppliers_category',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                'message' => "Supplier Category Added Successfully.",
                'class' => "alert-success",
                'type' => "Success"
            ]);
            $this->session->set_flashdata('active_tab', 'sup_cat');
            redirect('admin/accounting_config');
        } 
    }
    public function accounting_supplier_category_update()
    {
       extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'supplier_category'=>$supplier_category,
            'updated_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->my_model->update_table(array('id'=>$id),'vbs_account_suppliers_category',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                    'message' => "Supplier Category Updated Successfully.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
            $this->session->set_flashdata('active_tab', 'sup_cat');
            redirect('admin/accounting_config');
        } 
    }
    public function accounting_organization_category_add()
    {
        extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'organization_category'=>$organization_category,
            'created_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->general_model->add('vbs_account_organization_category',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                'message' => "Organization Category Added Successfully.",
                'class' => "alert-success",
                'type' => "Success"
            ]);
            $this->session->set_flashdata('active_tab', 'org_cat');
            redirect('admin/accounting_config');
        } 
    }
    public function accounting_organization_category_update()
    {
       extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'organization_category'=>$organization_category,
            'created_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->my_model->update_table(array('id'=>$id),'vbs_account_organization_category',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                    'message' => "Organization Category Updated Successfully.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
            $this->session->set_flashdata('active_tab', 'org_cat');
            redirect('admin/accounting_config');
        } 
    }
    public function accounting_vat_add()
    {
        extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'vat'=>$vat,
            'created_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->general_model->add('vbs_account_vat',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                'message' => "VAT Added Successfully.",
                'class' => "alert-success",
                'type' => "Success"
            ]);
            $this->session->set_flashdata('active_tab', 'vat');
            redirect('admin/accounting_config');
        } 
    }
    public function accounting_vat_update()
    {
        extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'vat'=>$vat,
            'updated_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->my_model->update_table(array('id'=>$id),'vbs_account_vat',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                    'message' => "VAT Updated Successfully.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
            $this->session->set_flashdata('active_tab', 'vat');
            redirect('admin/accounting_config');
        }
    }
    public function accounting_client_kind_add()
    {
        extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'client_kind'=>$client_kind,
            'payment_delay_id'=>$payment_delay_id,
            'created_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->general_model->add('vbs_accounting_client_kind',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                'message' => "client Kind Added Successfully.",
                'class' => "alert-success",
                'type' => "Success"
            ]);
            $this->session->set_flashdata('active_tab', 'client_kind');
            redirect('admin/accounting_config');
        }
    }
    public function accounting_client_kind_update()
    {
        extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'client_kind'=>$client_kind,
            'payment_delay_id'=>$payment_delay_id,
            'updated_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->my_model->update_table(array('id'=>$id),'vbs_accounting_client_kind',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                    'message' => "Client Kind Updated Successfully.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
            $this->session->set_flashdata('active_tab', 'client_kind');
            redirect('admin/accounting_config');
        }
    }
    public function accounting_garantee_fund_add()
    {
        extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'garantee_fund'=>$garantee_fund,
            'created_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->general_model->add('vbs_account_garantee_fund',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                'message' => "Garantee Added Successfully.",
                'class' => "alert-success",
                'type' => "Success"
            ]);
            $this->session->set_flashdata('active_tab', 'garantee_fund');
            redirect('admin/accounting_config');
        } 
    }
    public function accounting_garantee_fund_update()
    {
        extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'garantee_fund'=>$garantee_fund,
            'updated_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->my_model->update_table(array('id'=>$id),'vbs_account_garantee_fund',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                    'message' => "Garantee Fund Updated Successfully.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
            $this->session->set_flashdata('active_tab', 'garantee_fund');
            redirect('admin/accounting_config');
        } 
    }
    public function accounting_financement_delay_add()
    {
        extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'financement_delay'=>$financement_delay,
            'created_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->general_model->add('vbs_account_financement_delay',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                'message' => "Financement Delay Added Successfully.",
                'class' => "alert-success",
                'type' => "Success"
            ]);
            $this->session->set_flashdata('active_tab', 'financement_delay');
            redirect('admin/accounting_config');
        } 
    }
    public function accounting_financement_delay_update()
    {
        extract($_POST);
        $data=array(
            'added_by'=>$this->session->userdata('user_id'),
            'from_department_id'=>$this->department,
            'statut'=>$statut,
            'financement_delay'=>$financement_delay,
            'updated_at'=>date('Y-m-d H:i:s')
        );
        $lastid=$this->my_model->update_table(array('id'=>$id),'vbs_account_financement_delay',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                    'message' => "Financement Delay Updated Successfully.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
            $this->session->set_flashdata('active_tab', 'financement_delay');
            redirect('admin/accounting_config');
        } 
    }
    public function delete_config_record()
    {
        extract($_POST);
        $query = $this->general_model->delete($table,'id',$_POST[id][0]);
        // echo json_encode($query);
        $data['data'] = 'True';
        echo json_encode($data);
    }
}