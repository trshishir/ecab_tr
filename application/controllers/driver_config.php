<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Driver_config extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->lang->load('auth');
		$this->lang->load('general');
		$this->load->helper('language');
		$this->load->helper('validate');
		if (!$this->basic_auth->is_login())
			redirect("admin", 'refresh');
		else
		$this->data['user'] = $this->basic_auth->user();
		// $this->load->model('drivers_model');
		$this->load->model('driver_config_model');
		$this->load->model('drivers_model');
		$this->load->model('request_model');
		$this->load->model('jobs_model');
		$this->load->model('support_model');
		$this->load->model('calls_model');
		$this->load->model('notes_model');
		$this->load->model('notifications_model');
		$this->data['configuration'] = get_configuration();

	}

	public static $table = "vehicle_categories";
	public function index(){

		$this->data['user'] = $this->basic_auth->user();
		$this->data['css_type'] 	= array("form","booking_datatable","datatable");
		$this->data['active_class'] = "driver";
		$this->data['gmaps'] 		= false;
		$this->data['title'] 		= 'Driver Configurations';
		$this->data['title_link'] 	= base_url('admin/driver_config');
		$this->data['content'] 		= 'admin/driver_config/index';
   		$data = [];
		$this->data['driver_status_data'] = $this->driver_config_model->getDriverStatus();
		$this->data['civilite_data'] = $this->driver_config_model->getAllCivilite();
		$this->data['post_data'] = $this->driver_config_model->getAllData('vbs_driverpost');
		$this->data['payment_method'] = $this->driver_config_model->getAllData('vbs_u_payment_method');
		$this->data['pattern_data'] = $this->driver_config_model->getAllData('vbs_driverpattern');
		$this->data['contract_data'] = $this->driver_config_model->getAllData('vbs_drivercontract');
		$this->data['nature_data'] = $this->driver_config_model->getAllData('vbs_drivernature');
		$this->data['hours_data'] = $this->driver_config_model->getAllData('vbs_driverhours');
		$this->data['type_data'] = $this->driver_config_model->getAllData('vbs_drivertype');
		$data['drivers'] = $this->driver_config_model->getAll();
		$data['jobs'] 	 = $this->jobs_model->getAll();
		$data['calls']   = $this->calls_model->getAll();
		$this->data['data'] = $this->driver_config_model->getAll();
		$this->_render_page('templates/admin_template', $this->data);
	}
	public function driverStatusadd(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->driverStatusstore();
		}else{
			redirect('admin/driver_config');
		}
		

	}
	public function driverStatusstore(){
			// $error = request_validate();
			if (empty($error)) {
				//$dob = to_unix_date(@$_POST['dob']);
				$user = $this->basic_auth->user();
			 	$user_id=$user->id;

				$id = $this->driver_config_model->addDriverStatus([
					'status' 		=> @$_POST['status'],
					'statut' 		=> @$_POST['statut'],
					'user_id' =>$user_id
				]);
			
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a driver statut .",
					'class' => "alert-success",
					'status_id' => "1",
					'type' => "Success"
				]);
				redirect('admin/driver_config');
			} else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "1",
					'type' => "Error"
				];
				redirect('admin/driver_config');
			}
	}
	public function driverStatusEdit(){
		$this->load->model('driver_config_model');
		$this->data['css_type'] 	= array("form","datatable");
		$this->data['active_class'] = "driver_config";
		$this->data['title'] 		= $this->lang->line("drivers");
		$this->data['title_link'] 	= base_url('admin/driver_config');
		$this->data['content'] 		= 'admin/driver_config/index';
		$this->data['active_status'] 		= '1';
		$this->load->model('calls_model');
		$this->load->model('request_model');
		$this->load->model('jobs_model');
		$data = [];
		$data['drivers'] = $this->driver_config_model->getAll();
		$data['jobs'] 	 = $this->jobs_model->getAll();
		$data['calls']   = $this->calls_model->getAll();
		$this->data['data'] = $this->driver_config_model->getAll();
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['driver_status_id'];
			if (!empty($id)) {
			$update=$this->driver_config_model->driverStatusUpdate([
				'status' 		=> @$_POST['status'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a driver statut.",
					'class' => "alert-success",
					'status_id' => "1",
					'type' => "Success"
				]);
				redirect('admin/driver_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "1",
					'type' => "Error"
				];
					redirect('admin/driver_config');
			}
		}
	}
		redirect('admin/driver_config');
	}
	public function deleteDriverStatus(){
		$this->load->model('driver_config_model');
		$id=$_POST['delet_driver_status_id'];
		$del=$this->driver_config_model->deleteDriverStatus($id);
		if ($del==true) {
		$this->session->set_flashdata('alert', [
				'message' => "Successfully deleted.",
				'class' => "alert-success",
				'status_id' => "1",
				'type' => "Success"
		]);
		redirect('admin/driver_config');
		}
		else {
			$this->session->set_flashdata('alert', [
					'message' => "Please try again.",
					'class' => "alert-danger",
					'status_id' => "1",
					'type' => "Error"
			]);
			redirect('admin/driver_config');
		}
	}
	public function get_ajax_driverStatus(){
		$id=(int)$_GET['driver_status_id'];

		if($id>0){
			$this->load->model('driver_config_model');
			$driver_status_data = $this->driver_config_model->driver_Config_getAll('vbs_driverstatus',['id' => $id]);
			
			foreach($driver_status_data as $key => $driverStatus){
				$result='<input type="hidden" name="driver_status_id" value="'.$driverStatus->id.'">
			 <div class="col-md-12">  
			 	<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($driverStatus->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($driverStatus->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div> 
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Status</span>
					<input type="text" class="form-control" name="status" placeholder="" value="'.$driverStatus->status.'" required>
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}

	public function civiliteAdd(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->civilitestore();
		}else show_404();
	}
	public function civilitestore(){
		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_drivercivilite';
			$id = $this->driver_config_model->driver_Config_Add($table,[
				'civilite' 		=> @$_POST['civilite'],
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a civilite .",
				'class' => "alert-success",
				'status_id' => "2",
				'type' => "Success"
			]);
			redirect('admin/driver_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'status_id' => "2",
				'type' => "Error"
			];
			redirect('admin/driver_config');
		}
	}
	public function civiliteEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['civilite_id'];
			if (!empty($id)) {
			$table='vbs_drivercivilite';
			$update=$this->driver_config_model->driver_Config_Update($table,[
				'civilite' 		=> @$_POST['civilite'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Civilite.",
					'class' => "alert-success",
					'status_id' => "2",
					'type' => "Success"
				]);
				redirect('admin/driver_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "2",
					'type' => "Error"
				];
				redirect('admin/driver_config');
			}
		}
	}else show_404();

	}
	public function civiliteDelete(){
	$this->load->model('driver_config_model');
	$id=$_POST['delet_civilite_id'];
	$table = "vbs_drivercivilite";
	$del=$this->driver_config_model->driver_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "2",
			'type' => "Success"
	]);
	redirect('admin/driver_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "2",
				'type' => "Error"
		]);
		redirect('admin/driver_config');
	}
	 show_404();
	}
	public function get_ajax_civilite(){
		$id=(int)$_GET['civilite_id'];

		if($id>0){
			$this->load->model('driver_config_model');
			$civilite_data = $this->driver_config_model->driver_Config_getAll('vbs_drivercivilite',['id' => $id]);
			
			foreach($civilite_data as $key => $civilite){
				$result='<input type="hidden" name="civilite_id" value="'.$civilite->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($civilite->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($civilite->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Civilite</span>
					<input type="text" class="form-control" name="civilite" placeholder="" value="'.$civilite->civilite.'">
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}

	// post 
	public function postAdd(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->poststore();
		}else show_404();
	}
	public function poststore(){
		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_driverpost';
			$id = $this->driver_config_model->driver_Config_Add($table,[
				'post' 		=> @$_POST['post'],
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a poste .",
				'class' => "alert-success",
				'status_id' => "3",
				'type' => "Success"
			]);
			redirect('admin/driver_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'status_id' => "3",
				'type' => "Error"
			];
			redirect('admin/driver_config');
		}
	}
	public function postEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['post_id'];
			if (!empty($id)) {
			$table='vbs_driverpost';
			$update=$this->driver_config_model->driver_Config_Update($table,[
				'post' 		=> @$_POST['post'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Poste.",
					'class' => "alert-success",
					'status_id' => "3",
					'type' => "Success"
				]);
				redirect('admin/driver_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "3",
					'type' => "Error"
				];
				redirect('admin/driver_config');
			}
		}
	}else show_404();

	}
	public function postDelete(){
	$this->load->model('driver_config_model');
	$id=$_POST['delet_post_id'];
	$table = "vbs_driverpost";
	$del=$this->driver_config_model->driver_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "3",
			'type' => "Success"
	]);
	redirect('admin/driver_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "3",
				'type' => "Error"
		]);
		redirect('admin/driver_config');
	}
	 show_404();
	}
	public function get_ajax_post(){
		$id=(int)$_GET['post_id'];

		if($id>0){
			$this->load->model('driver_config_model');
			$post_data = $this->driver_config_model->driver_Config_getAll('vbs_driverpost',['id' => $id]);
			
			foreach($post_data as $key => $post){
				$result='<input type="hidden" name="post_id" value="'.$post->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($post->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($post->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Poste</span>
					<input type="text" class="form-control" name="post" placeholder="" value="'.$post->post.'">
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	
	//pattern
	public function patternAdd(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->patternstore();
		}else show_404();
	}
	public function patternstore(){
		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_driverpattern';
			$id = $this->driver_config_model->driver_Config_Add($table,[
				'pattern' 		=> @$_POST['pattern'],
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a motif .",
				'class' => "alert-success",
				'status_id' => "4",
				'type' => "Success"
			]);
			redirect('admin/driver_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'status_id' => "4",
				'type' => "Error"
			];
			redirect('admin/driver_config');
		}
	}
	public function patternEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['pattern_id'];
			if (!empty($id)) {
			$table='vbs_driverpattern';
			$update=$this->driver_config_model->driver_Config_Update($table,[
				'pattern' 		=> @$_POST['pattern'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Motif.",
					'class' => "alert-success",
					'status_id' => "4",
					'type' => "Success"
				]);
				redirect('admin/driver_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "4",
					'type' => "Error"
				];
				redirect('admin/driver_config');
			}
		}
	}else show_404();

	}
	public function patternDelete(){
	$this->load->model('driver_config_model');
	$id=$_POST['delet_pattern_id'];
	$table = "vbs_driverpattern";
	$del=$this->driver_config_model->driver_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "4",
			'type' => "Success"
	]);
	redirect('admin/driver_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "4",
				'type' => "Error"
		]);
		redirect('admin/driver_config');
	}
	 show_404();
	}
	public function get_ajax_pattern(){
		$id=(int)$_GET['pattern_id'];

		if($id>0){
			$this->load->model('driver_config_model');
			$pattern_data = $this->driver_config_model->driver_Config_getAll('vbs_driverpattern',['id' => $id]);
			
			foreach($pattern_data as $key => $pattern){
				$result='<input type="hidden" name="pattern_id" value="'.$pattern->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($pattern->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($pattern->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Motif</span>
					<input type="text" class="form-control" name="pattern" placeholder="" value="'.$pattern->pattern.'">
			      </div>
				</div>
	          </div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}

	// contract 
	public function contractAdd(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->contractstore();
		}else show_404();
	}
	public function contractstore(){
		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_drivercontract';
			$id = $this->driver_config_model->driver_Config_Add($table,[
				'contract' 		=> @$_POST['contract'],
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a contrat .",
				'class' => "alert-success",
				'status_id' => "5",
				'type' => "Success"
			]);
			redirect('admin/driver_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'status_id' => "5",
				'type' => "Error"
			];
			redirect('admin/driver_config');
		}
	}
	public function contractEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['contract_id'];
			if (!empty($id)) {
			$table='vbs_drivercontract';
			$update=$this->driver_config_model->driver_Config_Update($table,[
				'contract' 		=> @$_POST['contract'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Contrat.",
					'class' => "alert-success",
					'status_id' => "5",
					'type' => "Success"
				]);
				redirect('admin/driver_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "5",
					'type' => "Error"
				];
				redirect('admin/driver_config');
			}
		}
	}else show_404();

	}
	public function contractDelete(){
	$this->load->model('driver_config_model');
	$id=$_POST['delet_contract_id'];
	$table = "vbs_drivercontract";
	$del=$this->driver_config_model->driver_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "5",
			'type' => "Success"
	]);
	redirect('admin/driver_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "5",
				'type' => "Error"
		]);
		redirect('admin/driver_config');
	}
	 show_404();
	}
	public function get_ajax_contract(){
		$id=(int)$_GET['contract_id'];
		$result = '';
		if($id>0){
			$this->load->model('driver_config_model');
			$contract_data = $this->driver_config_model->driver_Config_getAll('vbs_drivercontract',['id' => $id]);
		
			
			foreach($contract_data as $key => $contract){
				$result='<input type="hidden" name="contract_id" value="'.$contract->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($contract->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($contract->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Type Contrat</span>
					<input type="text" class="form-control" name="contract" placeholder="" value="'.$contract->contract.'">
				 </div>
				</div>
		      </div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}

	// nature 
	public function natureAdd(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->naturestore();
		}else show_404();
	}
	public function naturestore(){
		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_drivernature';
			$id = $this->driver_config_model->driver_Config_Add($table,[
				'nature' 		=> @$_POST['nature'],
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a nature .",
				'class' => "alert-success",
				'status_id' => "6",
				'type' => "Success"
			]);
			redirect('admin/driver_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'status_id' => "6",
				'type' => "Error"
			];
			redirect('admin/driver_config');
		}
	}
	public function natureEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['nature_id'];
			if (!empty($id)) {
			$table='vbs_drivernature';
			$update=$this->driver_config_model->driver_Config_Update($table,[
				'nature' 		=> @$_POST['nature'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Nature.",
					'class' => "alert-success",
					'status_id' => "6",
					'type' => "Success"
				]);
				redirect('admin/driver_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "6",
					'type' => "Error"
				];
				redirect('admin/driver_config');
			}
		}
	}else show_404();

	}
	public function natureDelete(){
	$this->load->model('driver_config_model');
	$id=$_POST['delet_nature_id'];
	$table = "vbs_drivernature";
	$del=$this->driver_config_model->driver_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "6",
			'type' => "Success"
	]);
	redirect('admin/driver_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "6",
				'type' => "Error"
		]);
		redirect('admin/driver_config');
	}
	 show_404();
	}
	public function get_ajax_nature(){
		$id=(int)$_GET['nature_id'];

		if($id>0){
			$this->load->model('driver_config_model');
			$nature_data = $this->driver_config_model->driver_Config_getAll('vbs_drivernature',['id' => $id]);
			
			foreach($nature_data as $key => $nature){
				$result='<input type="hidden" name="nature_id" value="'.$nature->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($nature->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($nature->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Nature du Contrat</span>
					<input type="text" class="form-control" name="nature" placeholder="" value="'.$nature->nature.'">
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}

	// hours 
	public function hoursAdd(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->hoursstore();
		}else show_404();
	}
	public function hoursstore(){
		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_driverhours';
			$id = $this->driver_config_model->driver_Config_Add($table,[
				'hours' 		=> @$_POST['hours'],
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a hours .",
				'class' => "alert-success",
				'status_id' => "7",
				'type' => "Success"
			]);
			redirect('admin/driver_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'status_id' => "7",
				'type' => "Error"
			];
			redirect('admin/driver_config');
		}
	}
	public function hoursEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['hours_id'];
			if (!empty($id)) {
			$table='vbs_driverhours';
			$update=$this->driver_config_model->driver_Config_Update($table,[
				'hours' 		=> @$_POST['hours'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Hours.",
					'class' => "alert-success",
					'status_id' => "7",
					'type' => "Success"
				]);
				redirect('admin/driver_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "7",
					'type' => "Error"
				];
				redirect('admin/driver_config');
			}
		}
	}else show_404();

	}
	public function hoursDelete(){
	$this->load->model('driver_config_model');
	$id=$_POST['delet_hours_id'];
	$table = "vbs_driverhours";
	$del=$this->driver_config_model->driver_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "7",
			'type' => "Success"
	]);
	redirect('admin/driver_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "7",
				'type' => "Error"
		]);
		redirect('admin/driver_config');
	}
	 show_404();
	}
	public function get_ajax_hours(){
		$id=(int)$_GET['hours_id'];

		if($id>0){
			$this->load->model('driver_config_model');
			$hours_data = $this->driver_config_model->driver_Config_getAll('vbs_driverhours',['id' => $id]);
			
			foreach($hours_data as $key => $hours){
				$result='<input type="hidden" name="hours_id" value="'.$hours->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($hours->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($hours->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			  </div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Nombre Dheure Mensuel</span>
					<input type="text" class="form-control" name="hours" placeholder="" value="'.$hours->hours.'">
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}

	// type 
	public function typeAdd(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->typestore();
		}else show_404();
	}
	public function typestore(){
		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_drivertype';
			$id = $this->driver_config_model->driver_Config_Add($table,[
				'type' 		=> @$_POST['type'],
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a type .",
				'class' => "alert-success",
				'status_id' => "8",
				'type' => "Success"
			]);
			redirect('admin/driver_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'status_id' => "8",
				'type' => "Error"
			];
			redirect('admin/driver_config');
		}
	}
	public function typeEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['type_id'];
			if (!empty($id)) {
			$table='vbs_drivertype';
			$update=$this->driver_config_model->driver_Config_Update($table,[
				'type' 		=> @$_POST['type'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Type.",
					'class' => "alert-success",
					'status_id' => "8",
					'type' => "Success"
				]);
				redirect('admin/driver_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "8",
					'type' => "Error"
				];
				redirect('admin/driver_config');
			}
		}
	}else show_404();

	}
	public function typeDelete(){
	$this->load->model('driver_config_model');
	$id=$_POST['delet_type_id'];
	$table = "vbs_drivertype";
	$del=$this->driver_config_model->driver_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "8",
			'type' => "Success"
	]);
	redirect('admin/driver_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "8",
				'type' => "Error"
		]);
		redirect('admin/driver_config');
	}
	 show_404();
	}
	public function get_ajax_type(){
		$id=(int)$_GET['type_id'];

		if($id>0){
			$this->load->model('driver_config_model');
			$type_data = $this->driver_config_model->driver_Config_getAll('vbs_drivertype',['id' => $id]);
			
			foreach($type_data as $key => $type){
				$result='<input type="hidden" name="type_id" value="'.$type->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($type->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($type->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Type de Piece didentite</span>
					<input type="text" class="form-control" name="type" placeholder="" value="'.$type->type.'">
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
}
