<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cars extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('general');
        $this->load->helper('language');
        $this->load->helper('validate');
        if (!$this->basic_auth->is_login())
            redirect("admin", 'refresh');
        else
        $this->data['user'] = $this->basic_auth->user(); 
        $this->load->model('cars_model');  
        $this->load->model('request_model');
        $this->load->model('cms_model');
        $this->load->model('base_model');
        $this->load->model('jobs_model');
        $this->load->model('support_model');
        $this->load->model('calls_model');
        $this->load->model('notes_model');
        $this->load->model('notifications_model');
        $this->load->model('userx_model');
        $this->data['configuration'] = get_configuration();

    }

    
    public function index(){



        $this->data['user'] = $this->basic_auth->user();
        $this->data['css_type']     = array("form","booking_datatable","datatable");
        $this->data['active_class'] = "cars";
        $this->data['gmaps']        = false;
        $this->data['title']        = 'Cars';
        $this->data['title_link']   = base_url('admin/cars');
        $this->data['content']      = 'admin/cars/profile';

        $data = [];
        $this->data['countries']    = $this->cms_model->get_all_countries();
        $this->data['car_status']   = $this->cars_model->getAllData('vbs_carstatut');
        $this->data['car_brands']   = $this->cars_model->getAllData('vbs_carmarque');
        $this->data['car_models']   = $this->cars_model->getAllData('vbs_carmodele');
        $this->data['car_series']   = $this->cars_model->getAllData('vbs_carserie');
        $this->data['car_garages']  = $this->cars_model->getAllData('vbs_carage');
        $this->data['car_gearboxes']= $this->cars_model->getAllData('vbs_carboite');
        $this->data['car_fuels']    = $this->cars_model->getAllData('vbs_carcarburant');
        $this->data['car_colors']   = $this->cars_model->getAllData('vbs_carcouleur');
        $this->data['car_belts']    = $this->cars_model->getAllData('vbs_carcourroie');
        $this->data['car_nature']   = $this->cars_model->getAllData('vbs_carnature');
        $this->data['car_types']    = $this->cars_model->getAllData('vbs_cartype');
        $this->data['civilite_data']= $this->cars_model->getAllData('vbs_carcivilite');
        //$this->data['car_data']     = $this->cars_model->getAllData('vbs_carsadded');
        $this->data['driver_data']  = $this->cars_model->getAllData('vbs_driverprofile');
        $this->data['googleapikey'] = $this->cars_model->getuniquerecord("vbs_googleapi");
        $flashbookdata =  $this->session->flashdata('searchdata');
        if(isset($flashbookdata['record']) ){
          $this->data['car_data'] = $flashbookdata['record'];
          $this->data['search_from_period']=$flashbookdata['fields']['search_from_period'];
          $this->data['search_to_period']=$flashbookdata['fields']['search_to_period'];
          $this->data['search_status']=$flashbookdata['fields']['search_status'];
          $this->data['search_burant']=$flashbookdata['fields']['search_burant'];
          $this->data['search_immatriculation']=$flashbookdata['fields']['search_immatriculation'];
          $this->data['search_access']=$flashbookdata['fields']['search_access'];
         }else{
           $this->data['car_data'] = $this->cars_model->getAllData('vbs_carsadded');
         }

        $this->_render_page('templates/admin_template', $this->data);
    }
 public function profileAdd(){

      if ($_SERVER['REQUEST_METHOD'] === 'POST') {
          $this->profileStore();
      }else{
          return redirect("admin/cars");
      }
  }
  public function profileStore(){
            
                
          if (empty($error)) {   
               
              $date_immatriculation=$this->input->post('date_immatriculation');
              $date_dentree=$this->input->post('date_dentree');
              $date_de_sortie=$this->input->post('date_de_sortie');
              $date_expired=$this->input->post('date_expired');

              $date_immatriculation=str_replace('/', '-', $date_immatriculation);
              $date_immatriculation=strtotime($date_immatriculation);
              $date_immatriculation = date('Y-m-d',$date_immatriculation);

              $date_dentree=str_replace('/', '-', $date_dentree);
              $date_dentree=strtotime($date_dentree);
              $date_dentree = date('Y-m-d',$date_dentree);

              $date_de_sortie=str_replace('/', '-', $date_de_sortie);
              $date_de_sortie=strtotime($date_de_sortie);
              $date_de_sortie = date('Y-m-d',$date_de_sortie);

           
              $user = $this->basic_auth->user();
              $user_id=$user->id;
              
            
              $id = $this->cars_model->createRecord('vbs_carsadded',[
                 
                        'statut'                 =>  $this->input->post('statut'),
                        'immatriculation'        =>  $this->input->post('immatriculation'),
                        'car_image'              =>  $this->do_upload_image($_FILES['car_image'],'car_image'),
                        'marque'                 =>  $this->input->post('marque'),
                        'modele'                 =>  $this->input->post('modele'),
                        'date_immatriculation'   =>  $date_immatriculation,
                        'car_old'                =>  $this->input->post('car_old'),
                        'car_access'             =>  $this->input->post('car_access'),
                        'car_options'            =>  $this->input->post('car_options'),
                        'serie'                  =>  $this->input->post('serie'),
                        'boite'                  =>  $this->input->post('boite'),
                        'carburant'              =>  $this->input->post('carburant'),
                        'courroie'               =>  $this->input->post('courroie'),
                        'couleur'                =>  $this->input->post('couleur'),
                        'nature'                 =>  $this->input->post('nature'),
                        'type'                   =>  $this->input->post('type'),
                        'date_dentree'           =>  $date_dentree,
                        'time_dentree'           =>  $this->input->post('time_dentree'),
                        'kilometrage_dentree'    =>  $this->input->post('kilometrage_dentree'),
                        'prix_dachat'            =>  $this->input->post('prix_dachat'),
                        'currency'               =>  $this->input->post('currency'),
                        'image1'                 =>  $this->do_upload_image($_FILES['image1'],'image1'),
                        'seller_civilite'        =>  $this->input->post('seller_civilite'),
                        'seller_nom'             =>  $this->input->post('seller_nom'),
                        'seller_prenom'          =>  $this->input->post('seller_prenom'),
                        'seller_societe'         =>  $this->input->post('seller_societe'),
                        'image2'                 =>  $this->do_upload_image($_FILES['image2'],'image2'),
                        'seller_address'         =>  $this->input->post('seller_address'),
                        'seller_address2'        =>  $this->input->post('seller_address2'),
                        'seller_postal_code'     =>  $this->input->post('seller_postal_code'),
                        'ville'                  =>  $this->input->post('ville'),
                        'date_de_sortie'         =>  $date_de_sortie,
                        'buyer_kilometrage'      =>  $this->input->post('buyer_kilometrage'),
                        'buyer_prix_dachat'      =>  $this->input->post('buyer_prix_dachat'),
                        'buyer_currency'         =>  $this->input->post('buyer_currency'),
                        'image3'                 =>  $this->do_upload_image($_FILES['image3'],'image3'),
                        'buyer_civilite'         =>  $this->input->post('buyer_civilite'),
                        'buyer_nom'              =>  $this->input->post('buyer_nom'),
                        'buyer_prenom'           =>  $this->input->post('buyer_prenom'),
                        'buyer_societe'          =>  $this->input->post('buyer_societe'),
                        'image4'                 =>  $this->do_upload_image($_FILES['image4'],'image4'),
                        'buyer_address'          =>  $this->input->post('buyer_address'),
                        'buyer_address2'         =>  $this->input->post('buyer_address2'),
                        'buyer_postal_code'      =>  $this->input->post('buyer_postal_code'),
                        'buyer_ville'            =>  $this->input->post('buyer_ville'),
                        'user_id'                =>  $user_id
              ]);
              if($id){
                //store car extra images
                if(isset($_POST['car_extra_images'])){
                 $totalimages= count($_POST['car_extra_images']);
                 for($i=0;$i<$totalimages;$i++){
                  $carimgid = $this->cars_model->createRecord('vbs_car_images',[
                    'image'  => $_POST['car_extra_images'][$i],
                    'car_id' => $id
                  ]);
                 }
                }
                //store car extra images

                //store car configuration
                            $passengercap=$this->input->post('passengercap');
                            $lugagescap=$this->input->post("lugagescap");
                            $wheelchairscap=$this->input->post("wheelchairscap");
                            $babycap=$this->input->post("babycap");
                            $animalscap=$this->input->post("animalscap");
                            for($i=0;$i<count($passengercap);$i++){
                                $cid = $this->cars_model->createRecord("vbs_maincar_configuration",[
                                'passengers' => $passengercap[$i],
                                'baby'       => $babycap[$i],
                                'wheelchairs'=> $wheelchairscap[$i],
                                'luggage'    => $lugagescap[$i],
                                'animals'    => $animalscap[$i],
                                'car_id'     => $id
                                ]);
                            }
                //store car configuration
                //store reminder and notificaiton of car expiry date
                 $exp_array=array(
                  'date_expired' =>   $date_expired,
                 );

                 $isremindercreate     = $this->reminder_store($id,$exp_array);
                 $isnotificationcreate = $this->notification_store($id,$exp_array);
                 if($isremindercreate == true && $isnotificationcreate == true){
                          $this->session->set_flashdata('alert', [
                           'message'   => ": Successfully added a car profile .",
                           'class'     => "alert-success",
                           'status_id' => "1",
                           'type'      => "Success"
                            ]);
                         redirect('admin/cars');
                 }else{
                      $this->data['alert'] = [
                        'message' => "Car's notification or reminder missing in notification or reminder page. Please add these and try again.",
                        'class'   => "alert-danger",
                        'status_id' => "1",
                        'type'    => "Error"
                          ];
                          redirect('admin/cars');
                      }
                 //store reminder and notification of car expiry date       
            }else{
               $this->data['alert'] = [
                  'message' => 'Please try again',
                  'class'   => "alert-danger",
                  'status_id' => "1",
                  'type'    => "Error"
              ];
              redirect('admin/cars');
            }
              
            }
           else {
              $this->data['alert'] = [
                  'message' => 'Please try again',
                  'class'   => "alert-danger",
                  'status_id' => "1",
                  'type'    => "Error"
              ];
              redirect('admin/cars');
          }
  }


    public function profileEdit(){
      

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $id   = $this->input->post('profile_id');
            if (!empty($id)) {

               
                $car = $this->cars_model->getsinglerecord('vbs_carsadded',['id'=>$id]);
                

                $car_image='';
                $image1='';
                $image2='';
                $image3='';
                $image4='';
                

    if(!empty($_FILES['car_image']['name']))
    {
      $car_image=$this->do_upload_image($_FILES['car_image'],'car_image');
    }else{
      if($_POST['car_image_pos'] == '1'){
        $car_image=$car->car_image;
      }else{
        $car_image=NULL;
      }
      
    }
    
    if(!empty($_FILES['image1']['name']))
    {
      $image1=$this->do_upload_image($_FILES['image1'],'image1');
    }else{
        $image1=$car->image1;
    }
    if(!empty($_FILES['image2']['name']))
    {
      $image2=$this->do_upload_image($_FILES['image2'],'image2');
    }else{
        $image2=$car->image2;
    }
    if(!empty($_FILES['image3']['name']))
    {
      $image3=$this->do_upload_image($_FILES['image3'],'image3');
    }else{
        $image3=$car->image3;
    }
    if(!empty($_FILES['image4']['name']))
    {
      $image4=$this->do_upload_image($_FILES['image4'],'image4');
    }else{
        $image4=$car->image4;
    }

                          
    //date converation
    $date_immatriculation=$this->input->post('date_immatriculation');
    $date_dentree=$this->input->post('date_dentree');
    $date_de_sortie=$this->input->post('date_de_sortie');
    $date_expired=$this->input->post('date_expired');
    
    $date_immatriculation=str_replace('/', '-', $date_immatriculation);
    $date_immatriculation=strtotime($date_immatriculation);
    $date_immatriculation = date('Y-m-d',$date_immatriculation);

    $date_dentree=str_replace('/', '-', $date_dentree);
    $date_dentree=strtotime($date_dentree);
    $date_dentree = date('Y-m-d',$date_dentree);

    $date_de_sortie=str_replace('/', '-', $date_de_sortie);
    $date_de_sortie=strtotime($date_de_sortie);
    $date_de_sortie = date('Y-m-d',$date_de_sortie);

    $date_expired=str_replace('/', '-', $date_expired);
    $date_expired=strtotime($date_expired);
    $date_expired = date('Y-m-d',$date_expired);
    
    //date converation
                
                      $update=$this->cars_model->createUpdate('vbs_carsadded',[
                            'statut'               =>  $this->input->post('statut'),
                            'immatriculation'      =>  $this->input->post('immatriculation'),
                            'car_image'            =>  $car_image,
                            'marque'               =>  $this->input->post('marque'),
                            'modele'               =>  $this->input->post('modele'),
                            'date_immatriculation' =>  $date_immatriculation,
                            'car_old'              =>  $this->input->post('car_old'),
                            'car_access'           =>  $this->input->post('car_access'),
                            'car_options'          =>  $this->input->post('car_options'),
                            'serie'                =>  $this->input->post('serie'),
                            'boite'                =>  $this->input->post('boite'),
                            'carburant'            =>  $this->input->post('carburant'),
                            'courroie'             =>  $this->input->post('courroie'),
                            'couleur'              =>  $this->input->post('couleur'),
                            'nature'               =>  $this->input->post('nature'),
                            'type'                 =>  $this->input->post('type'),
                            'date_dentree'         =>  $date_dentree,
                            'time_dentree'         =>  $this->input->post('time_dentree'),
                            'kilometrage_dentree'  =>  $this->input->post('kilometrage_dentree'),
                            'prix_dachat'          =>  $this->input->post('prix_dachat'),
                            'currency'             =>  $this->input->post('currency'),
                            'image1'               =>  $image1,
                            'seller_civilite'      =>  $this->input->post('seller_civilite'),
                            'seller_nom'           =>  $this->input->post('seller_nom'),
                            'seller_prenom'        =>  $this->input->post('seller_prenom'),
                            'seller_societe'       =>  $this->input->post('seller_societe'),
                            'image2'               =>  $image2,
                            'seller_address'       =>  $this->input->post('seller_address'),
                            'seller_address2'      =>  $this->input->post('seller_address2'),
                            'seller_postal_code'   =>  $this->input->post('seller_postal_code'),
                            'ville'                =>  $this->input->post('ville'),
                            'date_de_sortie'       =>  $date_de_sortie,
                            'buyer_kilometrage'    =>  $this->input->post('buyer_kilometrage'),
                            'buyer_prix_dachat'    =>  $this->input->post('buyer_prix_dachat'),
                            'buyer_currency'       =>  $this->input->post('buyer_currency'),
                            'image3'               =>  $image3,
                            'buyer_civilite'       =>  $this->input->post('buyer_civilite'),
                            'buyer_nom'            =>  $this->input->post('buyer_nom'),
                            'buyer_prenom'         =>  $this->input->post('buyer_prenom'),
                            'buyer_societe'        =>  $this->input->post('buyer_societe'),
                            'image4'               =>  $image4,
                            'buyer_address'        =>  $this->input->post('buyer_address'),
                            'buyer_address2'       =>  $this->input->post('buyer_address2'),
                            'buyer_postal_code'    =>  $this->input->post('buyer_postal_code'),
                            'buyer_ville'          =>  $this->input->post('buyer_ville')
                      ], $id);
                      if ($update){
                        /* delete car images and store images*/
                        $delcarimages=$this->cars_model->createDelete('vbs_car_images',['car_id'=>$id]);
                        if($delcarimages){
                        if(isset($_POST['car_extra_images'])){
                             $totalimages= count($_POST['car_extra_images']);
                             for($i=0;$i<$totalimages;$i++){
                              $carimgid = $this->cars_model->createRecord('vbs_car_images',[
                                'image'  => $_POST['car_extra_images'][$i],
                                'car_id' => $id
                              ]);
                             }
                          }
                        }
                        /* delete car images and store images*/
                        //delete and store car configuration
                           $deletecarconfiguration=$this->cars_model->createDelete('vbs_maincar_configuration',['car_id'=>$id]);
                           if($deletecarconfiguration){
                                    $passengercap=$this->input->post('passengercap');
                                    $lugagescap=$this->input->post("lugagescap");
                                    $wheelchairscap=$this->input->post("wheelchairscap");
                                    $babycap=$this->input->post("babycap");
                                    $animalscap=$this->input->post("animalscap");
                                    for($i=0;$i<count($passengercap);$i++){
                                        $cid = $this->cars_model->createRecord("vbs_maincar_configuration",[
                                        'passengers' => $passengercap[$i],
                                        'baby'       => $babycap[$i],
                                        'wheelchairs'=> $wheelchairscap[$i],
                                        'luggage'    => $lugagescap[$i],
                                        'animals'    => $animalscap[$i],
                                        'car_id'     => $id
                                        ]);
                                    }
                             }       
                        //delete and store car configuration
                          //store reminder and notificaiton of car expiry date
                           $exp_array=array(
                            'date_expired' =>   $date_expired,
                           );

                           $isreminderupdate     = $this->reminder_update($id,$exp_array);
                           $isnotificationupdate = $this->notification_update($id,$exp_array);
                           if($isreminderupdate == true && $isnotificationupdate == true){
                                    $this->session->set_flashdata('alert', [
                                     'message'   => ": Successfully updated a car profile .",
                                     'class'     => "alert-success",
                                     'status_id' => "1",
                                     'type'      => "Success"
                                      ]);
                                   redirect('admin/cars');
                           }else{
                                $this->data['alert'] = [
                                  'message' => "Car's notification or reminder missing in notification or reminder page. Please add these and try again.",
                                  'class'   => "alert-danger",
                                  'status_id' => "1",
                                  'type'    => "Error"
                                    ];
                                    redirect('admin/cars');
                                }
                           //store reminder and notification of car expiry date 
                       
                      }else {
                        $this->data['alert'] = [
                       'message' => 'Please try again',
                       'class' => "alert-danger",
                       'status_id' => "1",
                       'type' => "Error"
                       ];
                       redirect('admin/cars');
                    }
                }}else {
                    $this->data['alert'] = [
                    'message' => 'Please try again',
                    'class' => "alert-danger",
                    'status_id' => "1",
                    'type' => "Error"
                     ];
                     redirect('admin/cars');
                }
       
     
        
    }
    public function profileDelete(){
        
        $id=$_POST['delete_profile_id'];
        $del=$this->cars_model->createDelete('vbs_carsadded',['id'=>$id]);
        if ($del==true) {
        $this->session->set_flashdata('alert', [
                'message' => "Successfully deleted.",
                'class' => "alert-success",
                'status_id' => "1",
                'type' => "Success"
        ]);
        redirect('admin/cars');
        }
        else {
            $this->session->set_flashdata('alert', [
                    'message' => "Please try again.",
                    'class' => "alert-danger",
                    'status_id' => "1",
                    'type' => "Error"
            ]);
            redirect('admin/cars');
        }
    }
    public function get_ajax_profile_data(){
        $id=(int)$_GET['profile_id'];
       

        if($id>0){

                $data = [];
        $this->data['car'] =$this->cars_model->getsinglerecord('vbs_carsadded',['id'=>$id]);
        $this->data['carimages'] =$this->cars_model->getAllData('vbs_car_images',['car_id'=>$this->data['car']->id]);
        $this->data['car_configuration_data'] = $this->cars_model->getAllData('vbs_maincar_configuration ',['car_id'=>$this->data['car']->id]);
        $this->data['driver'] = $this->cars_model->getsinglerecord('vbs_driverprofile',['car_id'=>$id]);
        //config data
        $this->data['car_status']   = $this->cars_model->getAllData('vbs_carstatut');
        $this->data['car_brands']   = $this->cars_model->getAllData('vbs_carmarque');
        $this->data['car_models']   = $this->cars_model->getAllData('vbs_carmodele');
        $this->data['car_series']   = $this->cars_model->getAllData('vbs_carserie');
        $this->data['car_garages']  = $this->cars_model->getAllData('vbs_carage');
        $this->data['car_gearboxes']= $this->cars_model->getAllData('vbs_carboite');
        $this->data['car_fuels']    = $this->cars_model->getAllData('vbs_carcarburant');
        $this->data['car_colors']   = $this->cars_model->getAllData('vbs_carcouleur');
        $this->data['car_belts']    = $this->cars_model->getAllData('vbs_carcourroie');
        $this->data['car_nature']   = $this->cars_model->getAllData('vbs_carnature');
        $this->data['car_types']    = $this->cars_model->getAllData('vbs_cartype');
        $this->data['civilite_data']= $this->cars_model->getAllData('vbs_carcivilite');
        $this->data['driver_data']  = $this->cars_model->getAllData('vbs_driverprofile');
        //config data
        //car controller technique
        $this->data['technique_data']=$this->cars_model->getAllData('vbs_car_controllertechnique',['car_id'=> $this->data['car']->id]);
        $this->data['assurance_data']=$this->cars_model->getAllData('vbs_car_assurance',['car_id'=> $this->data['car']->id]);
        $this->data['entretien_data']=$this->cars_model->getAllData('vbs_car_entretien',['car_id'=> $this->data['car']->id]);
        $this->data['reparation_data']=$this->cars_model->getAllData('vbs_car_reparation',['car_id'=> $this->data['car']->id]);
        $this->data['sinistre_data']=$this->cars_model->getAllData('vbs_car_sinistre',['car_id'=> $this->data['car']->id]);
        $this->data['cout_data']=$this->cars_model->getAllData('vbs_car_cout',['car_id'=> $this->data['car']->id]);
        

        //$this->data['car'] = $this->cars_model->getsinglerecord('vbs_carprofile',['id'=>$id]);
        //$this->data['carprofile'] = $this->cars_model->getsinglerecord('vbs_users',['id'=>$this->data['car']->userid]);
        $this->data['countries'] = $this->cms_model->get_all_countries();
        $this->data['regions'] = $this->cars_model->ajax_get_region_listing($this->data['car']->paysdenaissance);
        $this->data['cities'] = $this->cars_model->ajax_get_cities_listings($this->data['car']->region);
       $this->data['carcustomreminders']=$this->cars_model->getMultipleRecord('vbs_cars_customreminders',['car_id'=> $this->data['car']->id]);
       $this->data['carnotifications']=$this->cars_model->getMultipleRecord("vbs_cars_notification",['car_id'=>$this->data['car']->id]);
       $this->data['carreminders']=$this->cars_model->getMultipleRecord("vbs_cars_reminders",['car_id'=>$this->data['car']->id]);
         
        $result=$this->load->view('admin/cars/index', $this->data, TRUE);
        echo $result;
        exit();
        }else{
            $result="Please select Record!";
        }
        echo $result;
        exit;
    }
    /* --------------------------------- Car Reminders Add Section start -------------------------------------------- */
    function reminder_store($car_id,$data){
      $currenttime = date('h : i');
      $module='8';
      $statusexpired='1';
      $remindersdata   = $this->cars_model->getMultipleRecord('vbs_reminders',['module' => $module,'status'=> $statusexpired]);
                           
      if($remindersdata){
        $remindertype=1;

        foreach ($remindersdata as $reminder) {
              $reminders   =    json_decode($reminder->reminders);
              $days        =    $reminders[0]->reminder_days;

          //day add into date
          $new_date_expired  = date('Y-m-d', strtotime($data['date_expired']. ' - '.$days.' days'));
          //day add into date

          $reminderid = $this->cars_model->createRecord('vbs_cars_reminders',[
                                 'date'                        => $new_date_expired,
                                 'time'                        => $currenttime,
                                 'car_id'                      => $car_id,
                                 'reminder_id'                 => $reminder->id,
                                 'type'                        => $remindertype
                                ]); 
               $remindertype++;
        }
        return true;
      }else{
        return false;
      }
    }
/* --------------------------------- Car Reminders Add Section close -------------------------------------------- */
/* --------------------------------- Car notification Add Section start -------------------------------------------- */
function notification_store($car_id,$data){

  $currenttime = date('h : i');
  $department='9';
  $statusexpired='1';
  $notificationdata  = $this->cars_model->getSingleRecord('vbs_notifications',['department' => $department,'status'=>$statusexpired]);
                       
  if($notificationdata){
 
         $notificationid = $this->cars_model->createRecord('vbs_cars_notification',[
                            'date'                       => $data['date_expired'],
                            'time'                       => $currenttime,
                            'car_id'                     => $car_id,
                            'notification_id'            => $notificationdata->id,
                        ]); 
         
    return true;     
  }else{
    return false;
  }
} 
/* --------------------------------- Car notification Add Section close -------------------------------------------- */
/* --------------------------------- Driver Reminders Update Section start -------------------------------------------- */
function reminder_update($car_id,$data){
  
  $car_reminder_prev_data=$this->cars_model->getOneRecord('vbs_cars_reminders',['car_id' => $car_id]);
  $reminderdata   = $this->cars_model->getOneRecord('vbs_reminders',['id' => $car_reminder_prev_data->reminder_id]);

  if($reminderdata){
    
          $reminders = json_decode($reminderdata->reminders);
          $days      = $reminders[0]->reminder_days;

      //day add into date
      $new_date_expired  = date('Y-m-d', strtotime($data['date_expired']. ' - '.$days.' days'));
      //day add into date

           $reminderid = $this->cars_model->createUpdate('vbs_cars_reminders',[
                           'date'  => $new_date_expired
                          ],$car_reminder_prev_data->id); 
          if($reminderid){return true;}
          else{return false;} 
  }else{
    return false;
  }
}

/* --------------------------------- Driver Reminders Update Section close -------------------------------------------- */

/* --------------------------------- Driver notification Update Section start -------------------------------------------- */
function notification_update($car_id,$data){

 $car_notification_prev_data=$this->cars_model->getOneRecord('vbs_cars_notification',['car_id' => $car_id]);
          
  if($car_notification_prev_data){
         $notificationid = $this->cars_model->createUpdate('vbs_cars_notification',[
                         'date'                => $data['date_expired']
                        ],$car_notification_prev_data->id);     
     if($notificationid){return true;}
     else{return false;} 
  }else{
    return false;
  }
} 
/* --------------------------------- Driver notification Update Section close -------------------------------------------- */
 /* --------------------------------- Car notification pdf  Section start -------------------------------------------- */

    public function notification_pdf($carnotificationid,$car_id){

     $pdfArrays=createcarnotificationpdf($carnotificationid,$car_id);  
      $this->data['pdf']=$pdfArrays['pdf']; 
      $this->data['fileName']=$pdfArrays['fileName'];  

      $this->load->view('admin/cars/profile/notificationpdf',$this->data);
    }
   /* --------------------------------- Car notification pdf  Section close -------------------------------------------- */
 /* --------------------------------- Car reminder pdf  Section start -------------------------------------------- */

     public function reminder_pdf($carreminderid,$car_id,$columnnumber){

     $pdfArrays=createcarreminderpdf($carreminderid,$car_id,$columnnumber,$level);  
      $this->data['pdf']=$pdfArrays['pdf']; 
      $this->data['fileName']=$pdfArrays['fileName'];  

      $this->load->view('admin/cars/profile/reminderpdf',$this->data);
    }
     /* --------------------------------- Car reminder pdf  Section close -------------------------------------------- */
        /* --------------------------------- Driver custom reminder  Section start -------------------------------------------- */

       public function addcustomreminders(){
       
       $car_id = $_GET['car_id'];
       $title  = $_GET['title'];
       $date   = $_GET['date'];
       $time   = $_GET['time'];
       $comment= $_GET['comment'];
       $subject= $_GET['subject'];

       //add custom comment
               
                          $date=$date;
                          $date=str_replace('/', '-', $date);
                          $date=strtotime($date);
                          $date = date('Y-m-d',$date);
                           $crid = $this->cars_model->createRecord("vbs_cars_customreminders",[
                             'date'     => $date,
                             'time'     => $time,
                             'title'    => $title,
                             'comment'  => $comment,
                             'subject'  => $subject,
                             'car_id'   => $car_id
                             ]);
                       
                  if($crid){
                         $record=array(
                             'date'     => $_GET['date'],
                             'time'     => $time,
                             'title'    => $title,
                             'comment'  => $comment,
                             'subject'  => $subject,
                             'car_id'   => $car_id,
                             'customreminderid'=>$crid
                         );
                         $data = ['result'=>"200",'record'=>$record];
                         header('Content-Type: application/json'); 
                         echo json_encode($data);
                  }
                  else{
                         $data = ['result'=>"201",'error'=>'something went wrong.'];
                         header('Content-Type: application/json'); 
                         echo json_encode($data);
                  }
     }


     public function removecustomreminders(){
       $customreminderid=$_GET['customreminderid'];
       $del=$this->cars_model->createDelete('vbs_cars_customreminders',['id'=>$customreminderid]);
       if($del){
                         $data = ['result'=>"200"];
                         header('Content-Type: application/json'); 
                         echo json_encode($data);
       }else{
                         $data = ['result'=>"201",'error'=>'something went wrong.'];
                         header('Content-Type: application/json'); 
                         echo json_encode($data);
       }
     }
     /* --------------------------------- Driver custom reminder  Section close -------------------------------------------- */
     /* add car notification */
     public function addcarnotification(){

    
       $date    = $_POST['date'];
       $time    = $_POST['time'];
       $car_id  = $_POST['car_id'];
       $department    = '9';
       $statusexpired = '1';
       $notificationdata  = $this->cars_model->getSingleRecord('vbs_notifications',['department' => $department,'status'=>$statusexpired]);

       //convert into date format
       $date=str_replace('/', '-', $date);
       $date=strtotime($date);
       $date = date('Y-m-d',$date);
       //convert into date format
       $notificationid = $this->cars_model->createRecord('vbs_cars_notification',[
                                'date'                     => $date,
                                'time'                     => $time,
                                'car_id'                   => $car_id,
                                'notification_id'          => $notificationdata->id,
                             ]); 
           $record=array('notification_id'=>$notificationdata->id,'status'=>'Pending');
           $data = ['result'=>"200",'record'=>$record];
           header('Content-Type: application/json'); 
           echo json_encode($data);

     }
     /* add car notification */
     /* add car reminder */
     public function addcarreminder(){

     
       $date=$_POST['date'];
       $time=$_POST['time'];
       $car_id=$_POST['car_id'];
       $module='8';
       $statusexpired='1';
       $remindersdata = $this->cars_model->getSingleRecord('vbs_reminders',['module' => $module,'status'=> $statusexpired]);

       //convert into date format
         $date=str_replace('/', '-', $date);
         $date=strtotime($date);
         $date = date('Y-m-d',$date);
       //convert into date format
       
       $reminderid = $this->cars_model->createRecord('vbs_cars_reminders',[
                                'date'                     => $date,
                                'time'                     => $time,
                                'car_id'                   => $car_id,
                                'reminder_id'              => $remindersdata->id,
                             ]); 
           $record=array('reminder_id'=>$reminderid,'status'=>'Pending');
           $data = ['result'=>"200",'record'=>$record];
           header('Content-Type: application/json'); 
           echo json_encode($data);

     }
     /* add car reminder */
     //search
     public function get_cars_bysearch(){
       
       $from_period = $this->input->post('from_period');
       $to_period   = $this->input->post('to_period');
       $statut      = $this->input->post('statut');
       $carburant   = $this->input->post('carburant');
       $car_access   = $this->input->post('car_access');
       $immatriculation   = $this->input->post('immatriculation');

     //$from_period='26/12/2020';
     //$to_period='28/12/2020';

       $search=array();
         
          if(!empty($from_period)){

            $from_period=str_replace('/', '-', $from_period);
            $from_period=strtotime($from_period);
            $from_period = date('Y-m-d',$from_period);

            $search["DATE_FORMAT(created_at,'%Y-%m-%d') >="]=$from_period;
            $from_period = strtotime($from_period);
            $from_period = date('d/m/Y',$from_period);

          }
          if(!empty($to_period)){
              $to_period=str_replace('/', '-', $to_period);
              $to_period=strtotime($to_period);
              $to_period = date('Y-m-d',$to_period);

              $search["DATE_FORMAT(created_at,'%Y-%m-%d') <="]=$to_period;
              $to_period = strtotime($to_period);
              $to_period = date('d/m/Y',$to_period);
          }
           if(!empty($immatriculation)){
             $search['immatriculation']=$immatriculation;
           }
          if($carburant != ''){
            $search['carburant']=$carburant;
          }
          if($car_access != ''){
            $search['car_access']=$car_access;
          }
          if($statut != ''){
           $search['statut']=$statut;
          }
         
       
          $data=$this->cars_model->getAllData('vbs_carsadded',$search);
          $this->session->set_flashdata('searchdata', [
                   'record' =>  $data,  
                   'fields' =>  array("search_from_period"=>$from_period,"search_to_period"=>$to_period,"search_status"=>$statut,"search_burant"=>$carburant,'search_immatriculation'=>$immatriculation,'search_access'=>$car_access)
                                                                   
                 ]);
       
               return redirect("admin/cars");
             
     }
     //search
  /*assurance*/
    public function techniqueAdd(){

      if ($_SERVER['REQUEST_METHOD'] === 'POST') {
          $this->techniqueStore();
      }else{
          return redirect("admin/cars");
      }
  }
  public function techniqueStore(){
          
    $start_date = $this->input->post('start_date');
    $end_date   = $this->input->post('end_date');

    $start_date=str_replace('/', '-', $start_date);
    $start_date=strtotime($start_date);
    $start_date = date('Y-m-d',$start_date);

    $end_date=str_replace('/', '-', $end_date);
    $end_date=strtotime($end_date);
    $end_date = date('Y-m-d',$end_date);


              

              $id = $this->cars_model->createRecord('vbs_car_controllertechnique',[
                  'statut'           => $_POST['statut'],
                  'start_date'       => $start_date,
                  'end_date'         => $end_date,
                  'start_age'        => trim($_POST['start_age']),
                  'end_age'          => trim($_POST['end_age']),
                  'note'             => trim($_POST['note']),
                  'car_id'           => $_POST['profile_id']
              ]);
          if($id){
           
        $this->data['technique_data']=$this->cars_model->getAllData('vbs_car_controllertechnique',['car_id'=> $_POST['profile_id']]);
            $record=$this->load->view('admin/cars/technique/list', $this->data, TRUE);
            $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully added a Techniqal Controle.",'messageType'=>'Success','className'=>'alert-success'];
            header('Content-Type: application/json'); 
            echo json_encode($data);
            }
           else {
              $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
              header('Content-Type: application/json'); 
              echo json_encode($data);
          }
         
  }


    public function techniqueEdit(){
      

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
          $start_date = $this->input->post('start_date');
          $end_date   = $this->input->post('end_date');

          $start_date=str_replace('/', '-', $start_date);
          $start_date=strtotime($start_date);
          $start_date = date('Y-m-d',$start_date);

          $end_date=str_replace('/', '-', $end_date);
          $end_date=strtotime($end_date);
          $end_date = date('Y-m-d',$end_date);

          $id   = $this->input->post('technique_id');

            if(!empty($id)) {
                $technique = $this->cars_model->getSingleRecord('vbs_car_controllertechnique',['id'=>$id]);              
                $update=$this->cars_model->createUpdate('vbs_car_controllertechnique',[
                     'statut'           => $_POST['statut'],
                     'start_date'       => $start_date,
                     'end_date'         => $end_date,
                     'start_age'        => trim($_POST['start_age']),
                     'end_age'          => trim($_POST['end_age']),
                     'note'             => trim($_POST['note'])
                      ], $id);
                      if ($update){
                      //car controller technique
                      $this->data['technique_data']=$this->cars_model->getAllData('vbs_car_controllertechnique',['car_id'=> $technique->car_id]);

                      $record=$this->load->view('admin/cars/technique/list', $this->data, TRUE);
                      $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully updated a Techniqal Controle.",'messageType'=>'Success','className'=>'alert-success'];
                      header('Content-Type: application/json'); 
                      echo json_encode($data);
                      }else {
                        $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                        header('Content-Type: application/json'); 
                        echo json_encode($data);
                    }
             
              }else {
                    $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
                    }
                }else{
                    $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
                }
    }
    public function techniqueDelete(){
        
        $id=$_POST['delete_technique_id'];
        $technique = $this->cars_model->getSingleRecord('vbs_car_controllertechnique',['id'=>$id]);  
        $del=$this->cars_model->createDelete('vbs_car_controllertechnique',['id'=>$id]);
        if ($del==true) {
         $this->data['technique_data']=$this->cars_model->getAllData('vbs_car_controllertechnique',['car_id'=> $technique->car_id]);
          $record=$this->load->view('admin/cars/technique/list', $this->data, TRUE);
          $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully deleted a Techniqal Controle.",'messageType'=>'Success','className'=>'alert-success'];
          header('Content-Type: application/json'); 
          echo json_encode($data);
        }
        else {
         $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
         header('Content-Type: application/json'); 
         echo json_encode($data);
        }
    }
    public function techniqueAjax(){
        $id=(int)$_GET['technique_id'];

        if($id>0){

                $data = [];
                $this->data['technique'] = $this->cars_model->getSingleRecord('vbs_car_controllertechnique',['id'=>$id]);
                $result=$this->load->view('admin/cars/technique/edit', $this->data, TRUE);
                echo $result;
                exit();
        }else{
            $result="Please select Record!";
        }
        echo $result;
        exit;
    }
   /*assurance*/
     public function assuranceAdd(){

       if ($_SERVER['REQUEST_METHOD'] === 'POST') {
           $this->assuranceStore();
       }else{
           return redirect("admin/cars");
       }
   }
   public function assuranceStore(){
           
     $start_date = $this->input->post('start_date');
     $end_date   = $this->input->post('end_date');

     $start_date=str_replace('/', '-', $start_date);
     $start_date=strtotime($start_date);
     $start_date = date('Y-m-d',$start_date);

     $end_date=str_replace('/', '-', $end_date);
     $end_date=strtotime($end_date);
     $end_date = date('Y-m-d',$end_date);


               

               $id = $this->cars_model->createRecord('vbs_car_assurance',[
                   'statut'           => $_POST['statut'],
                   'start_date'       => $start_date,
                   'end_date'         => $end_date,
                   'price'            => trim($_POST['price']),
                   'tva'              => $_POST['tva'],
                   'contract_number'  => trim($_POST['contract_number']),
                   'insurance_company'=> trim($_POST['insurance_company']),
                   'brie_glasse'      => trim($_POST['brie_glasse']),
                   'franchise'        => trim($_POST['franchise']),
                   'car_id'           => $_POST['profile_id']
               ]);
           if($id){
             
         $this->data['assurance_data']=$this->cars_model->getAllData('vbs_car_assurance',['car_id'=> $_POST['profile_id']]);
             $record=$this->load->view('admin/cars/assurance/list', $this->data, TRUE);
             $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully added a Techniqal Controle.",'messageType'=>'Success','className'=>'alert-success'];
             header('Content-Type: application/json'); 
             echo json_encode($data);
             }
            else {
               $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
               header('Content-Type: application/json'); 
               echo json_encode($data);
           }
          
   }


     public function assuranceEdit(){
       

         if ($_SERVER['REQUEST_METHOD'] === 'POST') {
           $start_date = $this->input->post('start_date');
           $end_date   = $this->input->post('end_date');

           $start_date=str_replace('/', '-', $start_date);
           $start_date=strtotime($start_date);
           $start_date = date('Y-m-d',$start_date);

           $end_date=str_replace('/', '-', $end_date);
           $end_date=strtotime($end_date);
           $end_date = date('Y-m-d',$end_date);

           $id   = $this->input->post('assurance_id');

             if(!empty($id)) {
                 $assurance = $this->cars_model->getSingleRecord('vbs_car_assurance',['id'=>$id]);              
                 $update=$this->cars_model->createUpdate('vbs_car_assurance',[
                         'statut'           => $_POST['statut'],
                         'start_date'       => $start_date,
                         'end_date'         => $end_date,
                         'price'            => trim($_POST['price']),
                         'tva'              => $_POST['tva'],
                         'contract_number'  => trim($_POST['contract_number']),
                         'insurance_company'=> trim($_POST['insurance_company']),
                         'brie_glasse'      => trim($_POST['brie_glasse']),
                         'franchise'        => trim($_POST['franchise'])
                       ], $id);
                       if ($update){
                       //car controller assurance
                       $this->data['assurance_data']=$this->cars_model->getAllData('vbs_car_assurance',['car_id'=> $assurance->car_id]);

                       $record=$this->load->view('admin/cars/assurance/list', $this->data, TRUE);
                       $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully updated a assurance.",'messageType'=>'Success','className'=>'alert-success'];
                       header('Content-Type: application/json'); 
                       echo json_encode($data);
                       }else {
                         $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                         header('Content-Type: application/json'); 
                         echo json_encode($data);
                     }
              
               }else {
                     $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                     header('Content-Type: application/json'); 
                     echo json_encode($data);
                     }
                 }else{
                     $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                     header('Content-Type: application/json'); 
                     echo json_encode($data);
                 }
     }
     public function assuranceDelete(){
         
         $id=$_POST['delete_assurance_id'];
         $assurance = $this->cars_model->getSingleRecord('vbs_car_assurance',['id'=>$id]);  
         $del=$this->cars_model->createDelete('vbs_car_assurance',['id'=>$id]);
         if ($del==true) {
          $this->data['assurance_data']=$this->cars_model->getAllData('vbs_car_assurance',['car_id'=> $assurance->car_id]);
           $record=$this->load->view('admin/cars/assurance/list', $this->data, TRUE);
           $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully deleted a Techniqal Controle.",'messageType'=>'Success','className'=>'alert-success'];
           header('Content-Type: application/json'); 
           echo json_encode($data);
         }
         else {
          $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
          header('Content-Type: application/json'); 
          echo json_encode($data);
         }
     }
     public function assuranceAjax(){
         $id=(int)$_GET['assurance_id'];

         if($id>0){

                 $data = [];
                 $this->data['assurance'] = $this->cars_model->getSingleRecord('vbs_car_assurance',['id'=>$id]);
                 $result=$this->load->view('admin/cars/assurance/edit', $this->data, TRUE);
                 echo $result;
                 exit();
         }else{
             $result="Please select Record!";
         }
         echo $result;
         exit;
     }
     /*assurance*/

     /*entretien*/
       public function entretienAdd(){

         if ($_SERVER['REQUEST_METHOD'] === 'POST') {
             $this->entretienStore();
         }else{
             return redirect("admin/cars");
         }
     }
     public function entretienStore(){
             
       $start_date = $this->input->post('start_date');
       $end_date   = $this->input->post('end_date');

       $start_date=str_replace('/', '-', $start_date);
       $start_date=strtotime($start_date);
       $start_date = date('Y-m-d',$start_date);

       $end_date=str_replace('/', '-', $end_date);
       $end_date=strtotime($end_date);
       $end_date = date('Y-m-d',$end_date);


                 

                 $id = $this->cars_model->createRecord('vbs_car_entretien',[
                     'statut'           => $_POST['statut'],
                     'start_date'       => $start_date,
                     'end_date'         => $end_date,
                     'price'            => trim($_POST['price']),
                     'tva'              => $_POST['tva'],
                     'start_age'        => trim($_POST['start_age']),
                     'end_age'          => trim($_POST['end_age']),
                     'entretien'        => trim($_POST['entretien']),
                     'car_id'           => $_POST['profile_id']
                 ]);
             if($id){
               //$this->data['entretien_data']=$this->cars_model->getAllData('vbs_car_entretien',['car_id'=>$_POST['profile_id']]);
           $this->data['entretien_data']=$this->cars_model->getAllData('vbs_car_entretien',['car_id'=> $_POST['profile_id']]);
               $record=$this->load->view('admin/cars/entretien/list', $this->data, TRUE);
               $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully added a Entretien.",'messageType'=>'Success','className'=>'alert-success'];
               header('Content-Type: application/json'); 
               echo json_encode($data);
               }
              else {
                 $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                 header('Content-Type: application/json'); 
                 echo json_encode($data);
             }
            
     }


       public function entretienEdit(){
         

           if ($_SERVER['REQUEST_METHOD'] === 'POST') {
             $start_date = $this->input->post('start_date');
             $end_date   = $this->input->post('end_date');

             $start_date=str_replace('/', '-', $start_date);
             $start_date=strtotime($start_date);
             $start_date = date('Y-m-d',$start_date);

             $end_date=str_replace('/', '-', $end_date);
             $end_date=strtotime($end_date);
             $end_date = date('Y-m-d',$end_date);

             $id   = $this->input->post('entretien_id');

               if(!empty($id)) {
                   $entretien = $this->cars_model->getSingleRecord('vbs_car_entretien',['id'=>$id]);              
                   $update=$this->cars_model->createUpdate('vbs_car_entretien',[
                             'statut'           => $_POST['statut'],
                             'start_date'       => $start_date,
                             'end_date'         => $end_date,
                             'price'            => trim($_POST['price']),
                             'tva'              => $_POST['tva'],
                             'start_age'        => trim($_POST['start_age']),
                             'end_age'          => trim($_POST['end_age']),
                             'entretien'        => trim($_POST['entretien'])
                         ], $id);
                         if ($update){
                         //car controller entretien
                         $this->data['entretien_data']=$this->cars_model->getAllData('vbs_car_entretien',['car_id'=> $entretien->car_id]);

                         $record=$this->load->view('admin/cars/entretien/list', $this->data, TRUE);
                         $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully updated a Entretien.",'messageType'=>'Success','className'=>'alert-success'];
                         header('Content-Type: application/json'); 
                         echo json_encode($data);
                         }else {
                           $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                           header('Content-Type: application/json'); 
                           echo json_encode($data);
                       }
                
                 }else {
                       $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                       header('Content-Type: application/json'); 
                       echo json_encode($data);
                       }
                   }else{
                       $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                       header('Content-Type: application/json'); 
                       echo json_encode($data);
                   }
       }
       public function entretienDelete(){
           
           $id=$_POST['delete_entretien_id'];
           $entretien = $this->cars_model->getSingleRecord('vbs_car_entretien',['id'=>$id]);  
           $del=$this->cars_model->createDelete('vbs_car_entretien',['id'=>$id]);
           if ($del==true) {
            $this->data['entretien_data']=$this->cars_model->getAllData('vbs_car_entretien',['car_id'=> $entretien->car_id]);
             $record=$this->load->view('admin/cars/entretien/list', $this->data, TRUE);
             $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully deleted a Entretien.",'messageType'=>'Success','className'=>'alert-success'];
             header('Content-Type: application/json'); 
             echo json_encode($data);
           }
           else {
            $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
            header('Content-Type: application/json'); 
            echo json_encode($data);
           }
       }
       public function entretienAjax(){
           $id=(int)$_GET['entretien_id'];

           if($id>0){

                   $data = [];
                   $this->data['entretien'] = $this->cars_model->getSingleRecord('vbs_car_entretien',['id'=>$id]);
                   $result=$this->load->view('admin/cars/entretien/edit', $this->data, TRUE);
                   echo $result;
                   exit();
           }else{
               $result="Please select Record!";
           }
           echo $result;
           exit;
       }
       /*entretien*/     

       /*reparation*/
         public function reparationAdd(){

           if ($_SERVER['REQUEST_METHOD'] === 'POST') {
               $this->reparationStore();
           }else{
               return redirect("admin/cars");
           }
       }
       public function reparationStore(){
               
         $start_date = $this->input->post('start_date');
       

         $start_date=str_replace('/', '-', $start_date);
         $start_date=strtotime($start_date);
         $start_date = date('Y-m-d',$start_date);

       

                   $id = $this->cars_model->createRecord('vbs_car_reparation',[
                      'statut'           => $_POST['statut'],
                      'start_date'       => $start_date,
                      'price'            => trim($_POST['price']),
                      'tva'              => $_POST['tva'],
                      'start_age'        => trim($_POST['start_age']),
                      'reparation'       => trim($_POST['reparation']),
                      'car_id'           => $_POST['profile_id']
                   ]);
               if($id){
               
             $this->data['reparation_data']=$this->cars_model->getAllData('vbs_car_reparation',['car_id'=> $_POST['profile_id']]);
                 $record=$this->load->view('admin/cars/reparation/list', $this->data, TRUE);
                 $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully added a repair data.",'messageType'=>'Success','className'=>'alert-success'];
                 header('Content-Type: application/json'); 
                 echo json_encode($data);
                 }
                else {
                   $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                   header('Content-Type: application/json'); 
                   echo json_encode($data);
               }
              
       }


         public function reparationEdit(){
           

             if ($_SERVER['REQUEST_METHOD'] === 'POST') {
               $start_date = $this->input->post('start_date');
              

               $start_date=str_replace('/', '-', $start_date);
               $start_date=strtotime($start_date);
               $start_date = date('Y-m-d',$start_date);

             

               $id   = $this->input->post('reparation_id');

                 if(!empty($id)) {
                     $reparation = $this->cars_model->getSingleRecord('vbs_car_reparation',['id'=>$id]);              
                     $update=$this->cars_model->createUpdate('vbs_car_reparation',[
                               'statut'           => $_POST['statut'],
                               'start_date'       => $start_date,
                               'price'            => trim($_POST['price']),
                               'tva'              => $_POST['tva'],
                               'start_age'        => trim($_POST['start_age']),
                               'reparation'       => trim($_POST['reparation'])
                           ], $id);
                           if ($update){
                           //car controller reparation
                           $this->data['reparation_data']=$this->cars_model->getAllData('vbs_car_reparation',['car_id'=> $reparation->car_id]);

                           $record=$this->load->view('admin/cars/reparation/list', $this->data, TRUE);
                           $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully updated a repair data.",'messageType'=>'Success','className'=>'alert-success'];
                           header('Content-Type: application/json'); 
                           echo json_encode($data);
                           }else {
                             $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                             header('Content-Type: application/json'); 
                             echo json_encode($data);
                         }
                  
                   }else {
                         $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                         header('Content-Type: application/json'); 
                         echo json_encode($data);
                         }
                     }else{
                         $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                         header('Content-Type: application/json'); 
                         echo json_encode($data);
                     }
         }
         public function reparationDelete(){
             
             $id=$_POST['delete_reparation_id'];
             $reparation = $this->cars_model->getSingleRecord('vbs_car_reparation',['id'=>$id]);  
             $del=$this->cars_model->createDelete('vbs_car_reparation',['id'=>$id]);
             if ($del==true) {
              $this->data['reparation_data']=$this->cars_model->getAllData('vbs_car_reparation',['car_id'=> $reparation->car_id]);
               $record=$this->load->view('admin/cars/reparation/list', $this->data, TRUE);
               $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully deleted a repair data.",'messageType'=>'Success','className'=>'alert-success'];
               header('Content-Type: application/json'); 
               echo json_encode($data);
             }
             else {
              $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
              header('Content-Type: application/json'); 
              echo json_encode($data);
             }
         }
         public function reparationAjax(){
             $id=(int)$_GET['reparation_id'];

             if($id>0){

                     $data = [];
                     $this->data['reparation'] = $this->cars_model->getSingleRecord('vbs_car_reparation',['id'=>$id]);
                     $result=$this->load->view('admin/cars/reparation/edit', $this->data, TRUE);
                     echo $result;
                     exit();
             }else{
                 $result="Please select Record!";
             }
             echo $result;
             exit;
         }
         /*reparation*/

         /*sinistre*/
           public function sinistreAdd(){

             if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                 $this->sinistreStore();
             }else{
                 return redirect("admin/cars");
             }
         }
         public function sinistreStore(){
                 
           $sinistre_date = $this->input->post('sinistre_date');
         

           $sinistre_date = str_replace('/', '-', $sinistre_date);
           $sinistre_date = strtotime($sinistre_date);
           $sinistre_date = date('Y-m-d',$sinistre_date);

         

                     $id = $this->cars_model->createRecord('vbs_car_sinistre',[
                        'statut'           => $_POST['statut'],
                        'sinistre_date'    => $sinistre_date,
                        'garage'           => trim($_POST['garage']),
                        'responsability'   => trim($_POST['responsability']),
                        'constat'          => $this->do_upload_image($_FILES['constat'],'constat'),
                        'report'           => $this->do_upload_image($_FILES['report'],'report'),
                        'invoice'          => $this->do_upload_image($_FILES['invoice'],'invoice'),
                        'car_id'           => $_POST['profile_id']
                     ]);
                 if($id){
                 
                  $this->data['sinistre_data']=$this->cars_model->getAllData('vbs_car_sinistre',['car_id'=> $_POST['profile_id']]);
                   $record=$this->load->view('admin/cars/sinistre/list', $this->data, TRUE);
                   $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully added a sinistre data.",'messageType'=>'Success','className'=>'alert-success'];
                   header('Content-Type: application/json'); 
                   echo json_encode($data);
                   }
                  else {
                     $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                     header('Content-Type: application/json'); 
                     echo json_encode($data);
                 }
                
         }


           public function sinistreEdit(){
             

               if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                 $start_date = $this->input->post('start_date');
                

                 $start_date=str_replace('/', '-', $start_date);
                 $start_date=strtotime($start_date);
                 $start_date = date('Y-m-d',$start_date);

               

                 $id   = $this->input->post('sinistre_id');

                   if(!empty($id)) {
                       $sinistre = $this->cars_model->getSingleRecord('vbs_car_sinistre',['id'=>$id]);

                        $constat='';
                        $report='';
                        $invoice='';
                       if(!empty($_FILES['constat']['name']))
                          {
                            $constat=$this->do_upload_image($_FILES['constat'],'constat');
                          }else{
                              $constat=$sinistre->constat;
                          }
                          if(!empty($_FILES['report']['name']))
                          {
                            $report=$this->do_upload_image($_FILES['report'],'report');
                          }else{
                              $report=$sinistre->report;
                          }
                          if(!empty($_FILES['invoice']['name']))
                          {
                            $invoice=$this->do_upload_image($_FILES['invoice'],'invoice');
                          }else{
                              $invoice=$sinistre->invoice;
                          }              
                       $update=$this->cars_model->createUpdate('vbs_car_sinistre',[
                               'statut'           => $_POST['statut'],
                               'sinistre_date'    => $sinistre_date,
                               'garage'           => trim($_POST['garage']),
                               'responsability'   => trim($_POST['responsability']),
                               'constat'          => $constat,
                               'report'           => $report,
                               'invoice'          => $invoice
                             ], $id);
                             if ($update){
                             //car controller sinistre
                             $this->data['sinistre_data']=$this->cars_model->getAllData('vbs_car_sinistre',['car_id'=> $sinistre->car_id]);

                             $record=$this->load->view('admin/cars/sinistre/list', $this->data, TRUE);
                             $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully updated a sinistre data.",'messageType'=>'Success','className'=>'alert-success'];
                             header('Content-Type: application/json'); 
                             echo json_encode($data);
                             }else {
                               $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                               header('Content-Type: application/json'); 
                               echo json_encode($data);
                           }
                    
                     }else {
                           $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                           header('Content-Type: application/json'); 
                           echo json_encode($data);
                           }
                       }else{
                           $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                           header('Content-Type: application/json'); 
                           echo json_encode($data);
                       }
           }
           public function sinistreDelete(){
               
               $id=$_POST['delete_sinistre_id'];
               $sinistre = $this->cars_model->getSingleRecord('vbs_car_sinistre',['id'=>$id]);  
               $del=$this->cars_model->createDelete('vbs_car_sinistre',['id'=>$id]);
               if ($del==true) {
                $this->data['sinistre_data']=$this->cars_model->getAllData('vbs_car_sinistre',['car_id'=> $sinistre->car_id]);
                 $record=$this->load->view('admin/cars/sinistre/list', $this->data, TRUE);
                 $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully deleted a sinistre data.",'messageType'=>'Success','className'=>'alert-success'];
                 header('Content-Type: application/json'); 
                 echo json_encode($data);
               }
               else {
                $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                header('Content-Type: application/json'); 
                echo json_encode($data);
               }
           }
           public function sinistreAjax(){
               $id=(int)$_GET['sinistre_id'];

               if($id>0){

                       $data = [];
                       $this->data['sinistre'] = $this->cars_model->getSingleRecord('vbs_car_sinistre',['id'=>$id]);
                       $result=$this->load->view('admin/cars/sinistre/edit', $this->data, TRUE);
                       echo $result;
                       exit();
               }else{
                   $result="Please select Record!";
               }
               echo $result;
               exit;
           }
           /*sinistre*/
           /*cout*/
              public function coutAdd(){

                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    $this->coutStore();
                }else{
                    return redirect("admin/cars");
                }
            }
            public function coutStore(){
                
                        $id = $this->cars_model->createRecord('vbs_car_cout',[
                            'statut'           => $_POST['statut'],
                            'cout'             => trim($_POST['cout']),
                            'car_id'           => $_POST['profile_id']
                        ]);
                    if($id){
                     
                  $this->data['cout_data']=$this->cars_model->getAllData('vbs_car_cout',['car_id'=> $_POST['profile_id']]);
                      $record=$this->load->view('admin/cars/cout/list', $this->data, TRUE);
                      $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully added a Cost.",'messageType'=>'Success','className'=>'alert-success'];
                      header('Content-Type: application/json'); 
                      echo json_encode($data);
                      }
                     else {
                        $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                        header('Content-Type: application/json'); 
                        echo json_encode($data);
                    }
                   
            }


              public function coutEdit(){
                

                  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                  

                    $id   = $this->input->post('cout_id');

                      if(!empty($id)) {
                          $cout = $this->cars_model->getSingleRecord('vbs_car_cout',['id'=>$id]);              
                          $update=$this->cars_model->createUpdate('vbs_car_cout',[
                              'statut'           => $_POST['statut'],
                              'cout'             => trim($_POST['cout']),
                                ], $id);
                                if ($update){
                                //car controller cout
                                $this->data['cout_data']=$this->cars_model->getAllData('vbs_car_cout',['car_id'=> $cout->car_id]);

                                $record=$this->load->view('admin/cars/cout/list', $this->data, TRUE);
                                $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully updated a Cost.",'messageType'=>'Success','className'=>'alert-success'];
                                header('Content-Type: application/json'); 
                                echo json_encode($data);
                                }else {
                                  $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                                  header('Content-Type: application/json'); 
                                  echo json_encode($data);
                              }
                       
                        }else {
                              $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                              header('Content-Type: application/json'); 
                              echo json_encode($data);
                              }
                          }else{
                              $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                              header('Content-Type: application/json'); 
                              echo json_encode($data);
                          }
              }
              public function coutDelete(){
                  
                  $id=$_POST['delete_cout_id'];
                  $cout = $this->cars_model->getSingleRecord('vbs_car_cout',['id'=>$id]);  
                  $del=$this->cars_model->createDelete('vbs_car_cout',['id'=>$id]);
                  if ($del==true) {
                   $this->data['cout_data']=$this->cars_model->getAllData('vbs_car_cout',['car_id'=> $cout->car_id]);
                    $record=$this->load->view('admin/cars/cout/list', $this->data, TRUE);
                    $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully deleted a Cost.",'messageType'=>'Success','className'=>'alert-success'];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
                  }
                  else {
                   $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                   header('Content-Type: application/json'); 
                   echo json_encode($data);
                  }
              }
              public function coutAjax(){
                  $id=(int)$_GET['cout_id'];

                  if($id>0){

                          $data = [];
                          $this->data['cout'] = $this->cars_model->getSingleRecord('vbs_car_cout',['id'=>$id]);
                          $result=$this->load->view('admin/cars/cout/edit', $this->data, TRUE);
                          echo $result;
                          exit();
                  }else{
                      $result="Please select Record!";
                  }
                  echo $result;
                  exit;
              }
             /*cout*/
    function ajax_get_cities_listing(){
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            
            $result = $this->cars_model->ajax_get_cities_listings($_POST['region_id']);
            echo json_encode($result);
        }
    }
    function ajax_get_region_listing(){
       if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            
            $result = $this->cars_model->ajax_get_region_listing($_POST['country_id']);
            echo json_encode($result);
        } 
    }

    function do_upload_image($filename,$name) {
        
        $config['upload_path'] = './uploads/cars/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['max_size'] = '10240';
        $config['file_name'] = str_replace(' ', '_', $filename['name']);
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        
        if (!$this->upload->do_upload($name)) {
            return NULL;
        } else {
            $avatar = $this->upload->data();
            $avatar = $avatar['file_name'];
        
            return $avatar;
        }
        
    }
  


        function do_user_upload_image($filename,$name) {
        
        $config['upload_path'] = './uploads/user/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['max_size'] = '10240';
        $config['file_name'] = str_replace(' ', '_', $filename['name']);
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        
        if (!$this->upload->do_upload($name)) {
            return NULL;
        } else {
            $avatar = $this->upload->data();
            $avatar = $avatar['file_name'];
        
            return $avatar;
        }
        
    }

    public function add_car_image(){
     if(!empty($_FILES['file']['name']))
        {
         $file=$this->do_upload_image($_FILES['file'],'file');
           $data = ['result'=>"200",'file'=>$file];
            header('Content-Type: application/json'); 
            echo json_encode($data);
        }else{
           $data = ['result'=>"201"];
            header('Content-Type: application/json'); 
            echo json_encode($data);
        }
       
    }
}
