<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Filemanagement extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('general');
        $this->load->helper('language');
        if (!$this->basic_auth->is_login())
            redirect("admin", 'refresh');
        else
            $this->data['user'] = $this->basic_auth->user();
        $this->load->model('filemanagement_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');
        $this->load->model('calls_model');
        $this->load->model('userx_model');
        $this->load->model('notes_model');
        $this->load->model('support_model');
        $this->load->model('department_model');
        $this->load->model('comment_model');

        $this->data['configuration'] = get_configuration();
    }

    public function index() {
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "files";
        $this->data['gmaps'] = false;
        $this->data['title'] = $this->lang->line("filemanagement");
        $this->data['title_link'] = base_url('admin/files');
        $this->data['content'] = 'admin/files/index';

        $loggedInUserName = $this->session->userdata('user')->username;

        $value = $this->filemanagement_model->getUserFiles($loggedInUserName);

        for($i = 0; $i < count($value); $i++) {
            $addedByName = $value[$i]['added_by_lastname'];
            $senderUserDept = $this->userx_model->get(["username" => strtolower($addedByName)]);
            $value[$i]["sender_department"] = $senderUserDept['department'];
        }

        $this->data['data'] = $value;

        $this->_render_page('templates/admin_template', $this->data);
    }

    public function add() {
        $this->data['css_type'] = array("form");
        $this->data['active_class'] = "files";
        $this->data['gmaps'] = false;
        $this->data['title'] = $this->lang->line("filemanagement");
        $this->data['title_link'] = base_url('admin/files');
        $this->data['subtitle'] = "Add";
        $this->data['subtitle_link'] = base_url('admin/files/add');
        $this->data['content'] = 'admin/files/add';
        $this->data['data'] = $this->filemanagement_model->getAll();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->store();
        }
        $this->data['departments'] = $this->userx_model->departments();
        $this->data['users'] = $this->userx_model->getAll();
        $this->data['department']=$this->department_model->getAll();

        $loggedInUserDept = $this->department_model->get(["id" => $this->session->userdata('user')->department_id]);
        $this->data['loggedInUserDept'] = $loggedInUserDept['name'];
        
        $senderNamesResult = $this->filemanagement_model->getAllSenderNames();
        $senderNames = array();

        for ($i=0; $i < count($senderNamesResult); $i++) { 
            array_push($senderNames, $senderNamesResult[$i]['selection']);
        }

        $this->data['senderNames'] = $senderNames;

        $this->_render_page('templates/admin_template', $this->data);
    }

    public function search_user() {
//        error_reporting(E_ALL);
//        ini_set('display_errors', 1);

        $search = !empty($_GET['query']) ? $_GET['query'] : "";
//        $query = "SELECT GROUP_CONCAT(name_selection) FROM `vbs_filemanagement` WHERE name_selection LIKE '%$search%' ";
        $query = "SELECT selection FROM `vbs_filemanagement` WHERE selection LIKE '%$search%'";
        
        $data = $this->filemanagement_model->run_query($query);
        
        $return_data = array();
        
        foreach ($data as $key => $value) {
            $return_data[] = $value->selection;
        }

//        echo '<pre>';
//        print_r($return_data);
//        die;
//        return $return_data;
        echo json_encode($return_data);exit;
    }

    public function fileSearchData() {  

        $data = $this->input->get('data');
        $searchFilter['type'] = $data[0];
        $searchFilter['name'] = $data[1];
        $searchFilter['alert'] = $data[2];
        $searchFilter['nature'] = $data[3];
        $searchFilter['department'] = $data[4];
        $searchFilter['priority'] = $data[5];
        $searchFilter['status'] = $data[6];
        $searchFilter['date_from'] = $data[7];
        $searchFilter['date_to'] = $data[8];

        $data = $this->filemanagement_model->search_files($searchFilter);

        if (!empty($data)) {
            $count = 1;
            foreach ($data as $key => $value) {

                $checkbox = "<input type='checkbox' name='files[]' class='ccheckbox' value='" . $value['id'] . "' />";
                $registered_date = date('d/m/Y', strtotime($value['created_at']));
                // $since = timeDiff($value['created_at']);
                $edit_link = "<a href='" . base_url() . "admin/files/" . $value['id'] . "/edit.php" . "'>" . create_timestamp_uid($value['created_at'], $count) . "</a>";

                $count++;
                
                $file_url = base_url()."uploads/files/".$value['file'];
                $file = "<a  href='".$file_url. "' class='btn btn-default' target='blank'>View</a>";

                $status = "<div data-tag='" . ucfirst($value['status'])."' class='status'><span>" . ucfirst($value['status']). " </span></div>";

                $alert_before_day = $value['alert_before_day'] == null ? 0 : $value['alert_before_day'];

                $last_action = "<div style='padding: 10px; white-space: nowrap; font-weight: bold;". dayDiff($value['date'],$value['delay_date'],$value['alert_before_day'],$value['alert']) . "'>". timeDiff($value['last_action'], $value['delay_date']) . "</div>";

                $alert = strtolower($value['alert']);
                $alert = $alert == "on" ? $value['alert_before_day'] . " Days" : $value['alert'];

                $result['data'][$key] = array(
                    $checkbox,
                    $edit_link,
                    $registered_date,
                    from_unix_time($value['created_at']),
                    $value['type'],
                    $value['nature'],
                    ucfirst($value['name_selection']),
                    from_unix_date($value['date']),
                    from_unix_date($value['delay_date']),
                    ucfirst($alert),
                    ucfirst('test sender destination'),
                    strtolower($value['selection']) == 'admin' ? "Mr Super " . $value['selection'] : ucfirst($value['selection']),
                    ucfirst($value['destination']),
                    ucfirst($value['name']),
                    ucfirst($value['priority']),
                    $file,
                    $status,
                    $last_action

                );

            }
        }
        echo json_encode($result);
    }

    public function store() {
//        createFileManagementFilesPath();
//        $error = request_validate();
        $comment= @$_POST['note2'];
        $comments=$comment[0];
        $dest_id = @$_POST['department'];
        $dept_name;
        $dept_data= $this->department_model->getAll();
        foreach ($dept_data as $key => $dep) {
           # code...
            if($dep['id']==$dest_id){
                $dept_name=$dep['name'];
            }
        }


        //$dept_data =  $this->department_model->get(["department_id"=>$dest_id]);

        $error = $this->filemanagement_model->validate();
        $filenames = $this->uploadFiles($error);

        if (empty($error)) {
            //$dob = to_unix_date(@$_POST['dob']);

            $id = $this->filemanagement_model->create([
                'file' => !empty($filenames) && count($filenames) > 0 ? $filenames[0] : "",
                'files' => json_encode($filenames),
                'added_by_firstname' => @$_POST['added_by_firstname'],
                'added_by_lastname' => @$_POST['added_by_lastname'],
                'send_date' => to_unix_date(@$_POST['send_date']),
                'send_date_hour' => @$_POST['send_date_hour'],
                'date_minute' => @$_POST['date_minute'],
                'status' => @$_POST['status'],
                'type' => @$_POST['type'],
                'name' => @$_POST['name'],
                'nature' => @$_POST['nature'],
                'priority' => @$_POST['priority'],
                'alert' => @$_POST['alert'],
                'destination' => $dept_name,
                'alert_before_day' => !empty($_POST['alert_before_day']) ? $_POST['alert_before_day'] : NULL,
                'note' => $comments,
                'description' => @$_POST['description'],

                'selection' => @$_POST['q'],
//                'selection' => @$_POST['selection'],
                'name_selection' => @$_POST['name_selection'],
                'date' => to_unix_date(@$_POST['date']),
                'delay_date' => to_unix_date(@$_POST['delay_date']),
                'ip_address' => $this->input->ip_address()
            ]);

            $notes = $this->notes_model->createNotesAddedByArray($id, 'files');
            if (!empty($notes)) {
                $this->notes_model->bulkInsert($notes);
            }

            $user = $this->userx_model->get([
                "username" => @$_POST['name']
            ]);
            $user_id = $user['id'];

            $file_id = $id;

            $comments= @$_POST['note2'];

            foreach ($comments as $comment) {
                if(!empty($comment)) {
                    print_r($this->comment_model->create([
                        'comment' => $comment,
                        'user_id' => $user_id,
                        'file_id' => $file_id,
                        'first_name' => @$_POST['added_by_firstname'],
                        'last_name' => @$_POST['added_by_lastname'],
                        'department' => @$_POST['department'],
                        'date' => to_unix_date(@$_POST['send_date']),
                        'time' => @$_POST['send_date_hour']
                    ]));
                    echo("<br>");
                }

            }

            $this->session->set_flashdata('alert', [
                'message' => "Successfully Created.",
                'class' => "alert-success",
                'type' => "Success"
            ]);
            redirect('admin/files');
        } else {
            $this->data['alert'] = [
                'message' => @$error[0],
                'class' => "alert-danger",
                'type' => "Error"
            ];
        }
    }

    public function update($id) {

        $comment= @$_POST['note2'];
        $comments=$comment[0];
        $dest_id = @$_POST['department'];
        $dept_name;
        $dept_data= $this->department_model->getAll();
        foreach ($dept_data as $key => $dep) {
           # code...
            if($dep['id']==$dest_id){
                $dept_name=$dep['name'];
            }
        }

        $error = $this->filemanagement_model->validate();
        $filenames = $this->uploadFiles($error);

        print_r($filenames);

        if (empty($error)) {

            $result = $this->filemanagement_model->update([
                'file' => !empty($filenames) && count($filenames) > 0 ? $filenames[0] : "",
                'files' => json_encode($filenames),
                'added_by_firstname' => @$_POST['added_by_firstname'],
                'added_by_lastname' => @$_POST['added_by_lastname'],
                'send_date' => to_unix_date(@$_POST['send_date']),
                'send_date_hour' => @$_POST['send_date_hour'],
                'date_minute' => @$_POST['date_minute'],
                'status' => @$_POST['status'],
                'type' => @$_POST['type'],
                'name' => @$_POST['name'],
                'nature' => @$_POST['nature'],
                'priority' => @$_POST['priority'],
                'alert' => @$_POST['alert'],
                'destination' => $dept_name,
                'alert_before_day' => !empty($_POST['alert_before_day']) ? $_POST['alert_before_day'] : NULL,
                'note' => $comments,
                'description' => @$_POST['description'],
                'selection' => @$_POST['q'],
                'name_selection' => @$_POST['name_selection'],
                'date' => to_unix_date(@$_POST['date']),
                'delay_date' => to_unix_date(@$_POST['delay_date']),
                'ip_address' => $this->input->ip_address()
            ], $id);

            $notes = $this->notes_model->createNotesAddedByArray($id, 'files');
            if (!empty($notes)) {
                $this->notes_model->bulkInsert($notes);
            }

            $user = $this->userx_model->get([
                "username" => @$_POST['name']
            ]);
            $user_id = $user['id'];

            $file_id = $id;

            $comments= @$_POST['note2'];

            foreach ($comments as $comment) {
                if(!empty($comment)) {
                    print_r($this->comment_model->create([
                        'comment' => $comment,
                        'user_id' => $user_id,
                        'file_id' => $file_id,
                        'first_name' => @$_POST['added_by_firstname'],
                        'last_name' => @$_POST['added_by_lastname'],
                        'department' => @$_POST['department'],
                        'date' => to_unix_date(@$_POST['send_date']),
                        'time' => @$_POST['send_date_hour']
                    ]));
                    echo("<br>");
                }

            }

            $this->session->set_flashdata('alert', [
                'message' => "Successfully Created.",
                'class' => "alert-success",
                'type' => "Success"
            ]);
            redirect('admin/files');
        } else {
            $this->data['alert'] = [
                'message' => @$error[0],
                'class' => "alert-danger",
                'type' => "Error"
            ];
        }
    }

    public function uploadJobFiles($FILES, &$error) {

        $this->load->library('upload');
        $finename = '';
        if (isset($_FILES['files']['name']) && !empty($_FILES['files']['name'])) {
            $config['upload_path'] = './uploads/files/';
            $config['allowed_types'] = '*';
            $config['max_size'] = '10240';
            $finename = $config['file_name'] = str_replace(' ', '_', time() . $_FILES['files']['name']);

            $this->upload->initialize($config);
            if (!$this->upload->do_upload('file')) {
                $error[] = $this->upload->display_errors();
            } else {
                $file = $this->upload->data();
                $file['upload_path'] = 'uploads/files/' . $file['file_name'];
            }
        }
        return $finename;
    }

    public function uploadFiles(&$error) {
        $this->load->library('upload');

        $files = $_FILES;

        $fileNames = [];
        $cpt = count($_FILES['files']['name']);
        for ($i = 0; $i < $cpt; $i++) {

//            $finename = $_FILES['file']['name'] = rand()."_".time() . '_' . $files['files']['name'][$i];
            $finename = $_FILES['file']['name'] = rand() . "_" . time() . '.' . pathinfo($files['files']['name'][$i], PATHINFO_EXTENSION);
//            echo $finename;die;
            $_FILES['file']['type'] = $files['files']['type'][$i];
            $_FILES['file']['tmp_name'] = $files['files']['tmp_name'][$i];
            $_FILES['file']['error'] = $files['files']['error'][$i];
            $_FILES['file']['size'] = $files['files']['size'][$i];

            $this->upload->initialize($this->set_upload_options());

            if (!$this->upload->do_upload('file')) {
                $error[] = $this->upload->display_errors();
            } else {
                $fileNames[] = $finename;
            }
        }
        return $fileNames;
    }

    private function set_upload_options() {
        //upload an image options
        $config = array();
        $config['upload_path'] = './uploads/files/';
        $config['allowed_types'] = '*';
        $config['max_size'] = '0';
        $config['overwrite'] = FALSE;

        return $config;
    }

    public function delete() {
        $ids = $this->input->get('ids');
        for($count = 0; $count < count($ids); $count++){
            $this->filemanagement_model->delete($ids[$count]);
        }

        $this->session->set_flashdata('alert', [
            'message' => "Successfully deleted.",
            'class' => "alert-success",
            'type' => "Success"
        ]);

    }

    public function edit($id) {

        $strlength = strlen($id);
        $fileId = substr($id, $strlength-2, $strlength);

        $this->data['data'] = $this->filemanagement_model->get(['id' => $fileId]);

        $this->data['css_type'] = array("form");
        $this->data['active_class'] = "files";
        $this->data['gmaps'] = false;
        $this->data['title'] = $this->lang->line("filemanagement");
        $this->data['title_link'] = base_url('admin/files');
        $this->data['subtitle'] = $id;
        $this->data['subtitle_link'] = base_url('admin/files/edit');
        $this->data['content'] = 'admin/files/edit';

        $this->data['departments'] = $this->userx_model->departments();
        $this->data['users'] = $this->userx_model->getAll();
        $this->data['department']=$this->department_model->getAll();
        $this->data['file'] = $this->filemanagement_model->getFile(["id"=>$fileId]);
        $this->data['comment'] = $this->comment_model->getAll($fileId);

        $file = $this->data['file'][0];
        $addedByName = $file['added_by_lastname'];

        $senderUserDept = $this->userx_model->get(["username" => strtolower($addedByName)]);
        $this->data['senderUserDept'] = $senderUserDept['department'];

        $senderNamesResult = $this->filemanagement_model->getAllSenderNames();
        $senderNames = array();

        for ($i=0; $i < count($senderNamesResult); $i++) { 
            array_push($senderNames, $senderNamesResult[$i]['selection']);
        }

        $this->data['senderNames'] = $senderNames;

        $this->_render_page('templates/admin_template', $this->data);
    }

    public function delete_comment() {
        $id = $this->input->get('id');
        echo $this->comment_model->delete($id);
    }

    public function get_user_by_destination() {
        $selected = $this->input->get('selected');
        $data = $this->userx_model->getUser(["department_id"=>$selected]);
        // $username = array();
        $username = array();
        foreach ($data as $row) {
            array_push($username, $row["username"]);
        }
        echo json_encode($username);
    }

    public function add_sender() {
        $val = $this->input->get('data');
        $result = $this->filemanagement_model->add_sender(["selection" => $val]);
        print_r($result);
    } 

    public function delete_sender() {
        $val = $this->input->get('data');
        $result = $this->filemanagement_model->delete_sender($val);
        print_r($result);
    }  

}
