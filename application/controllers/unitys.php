<?php
class Unitys extends MY_Controller{
public function __construct()
    {
        parent::__construct();
        // load form and url helpers
        $data = [];
        $this->load->model('my_model');
        $this->load->model('general_model');
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('general');
        $this->load->helper('language');
        $this->load->helper('validate');
        if (!$this->basic_auth->is_login())
            redirect("admin", 'refresh');
        else
        $data['user'] = $this->basic_auth->user();
        $this->load->model('bookings_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');
        $this->load->model('support_model');
        $this->load->model('calls_model');
        $this->load->model('notes_model');
        $this->load->model('notifications_model');
        $this->load->model('cms_model');
        $this->load->model('userx_model');
        $this->load->model('filemanagement_model');
        $this->load->model('currency_model');
        $this->data['configuration'] = get_configuration();
    }
    public function index()
    {
        $this->data['user'] = $this->basic_auth->user();
        $this->data['css_type']   = array("form","datatable");
        $this->data['active_class'] = "unitys";
        $this->data['gmaps']      = false;
        $this->data['title']      = $this->lang->line("unitys");
        $this->data['title_link']     = base_url('admin/unitys');
        $this->data['company_data'] = $this->general_model->get('vbs_company');
        $this->data['company_data'] = $this->data['company_data'][0];
        $this->data['unitys'] = $this->my_model->get_table_row_query("SELECT vbs_unitys.*,vbs_users.civility,vbs_users.first_name,vbs_users.last_name FROM vbs_unitys INNER JOIN vbs_users ON vbs_users.id = vbs_unitys.added_by");
        $this->data['content']  = 'admin/unitys/index';
        $this->_render_page('templates/theme_template',$this->data);
    }
    public function add_unity()
    {
        $this->data['css_type']   = array("form");
        $this->data['active_class'] = "dashboard";
        $this->data['gmaps']      = false;
        $this->data['title']        = $this->lang->line("unitys");
        $this->data['title_link']     = base_url('admin/unitys');
        $this->data['subtitle']     = '>' .$this->lang->line("add unity");
        $this->data['user'] = $this->basic_auth->user();
        $this->data['company_data'] = $this->general_model->get('vbs_company');
        $this->data['company_data'] = $this->data['company_data'][0];
        $this->data['content']  = 'admin/unitys/add_unity';
        $this->load->view('templates/theme_template',$this->data);
    }
    public function store_unity()
    {
        extract($_POST);
        $data=array(
            'statut'=>$statut,
            'unity_name'=>$unity_name,
            'unity_code'=>$unity_code,
            'conversion'=>$conversion,
            'symbole'=>$symbole,
            'added_by'=>$this->session->userdata('user_id'),
            'default_status'=>$default_status,
            'allow_front_change'=>$allow_front_change,
            'allow_admin_change'=>$allow_admin_change,
            'allow_areas_change'=>$allow_areas_change,
            'created_at'=>date('Y-m-d H:i:s'),
        );
        $lastid=$this->general_model->add('vbs_unitys',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                    'message' => "Unity Added Successfully.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
            redirect('admin/unitys');
        }
    }
    public function edit_unity($id)
    {
        $this->data['css_type']   = array("form","datatable");
        $this->data['active_class'] = "dashboard";
        $this->data['gmaps']      = false;
        $this->data['title']        = $this->lang->line("unitys");
        $this->data['title_link']     = base_url('admin/unitys');
        $this->data['user'] = $this->basic_auth->user();
        $this->data['unity'] = $this->my_model->get_table_row_query("SELECT * FROM vbs_unitys where id = '".$id."'");
        $this->data['unity'] = $this->data['unity'][0];
        $this->data['subtitle']     = '>'.' '.create_timestamp_uid($data['unity']['created_at'],$id);
        $this->data['company_data'] = $this->general_model->get('vbs_company');
        $this->data['company_data'] = $this->data['company_data'][0];
        $this->data['content']  = 'admin/unitys/edit_unity';
        $this->load->view('templates/theme_template',$this->data);
    }
    public function update_unity()
    {
        extract($_POST);
        $data=array(
            'statut'=>$statut,
            'unity_name'=>$unity_name,
            'unity_code'=>$unity_code,
            'conversion'=>$conversion,
            'symbole'=>$symbole,
            'added_by'=>$this->session->userdata('user_id'),
            'default_status'=>$default_status,
            'allow_front_change'=>$allow_front_change,
            'allow_admin_change'=>$allow_admin_change,
            'allow_areas_change'=>$allow_areas_change,
            'updated_at'=>date('Y-m-d H:i:s'),
        );
        $lastid=$this->my_model->update_table(array('id'=>$id),'vbs_unitys',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                    'message' => "theme Updated Successfully.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
            redirect('admin/unitys');
        }
    }
    public function view_unity($id){
        $this->data['css_type']   = array("form","datatable");
        $this->data['active_class'] = "dashboard";
        $this->data['gmaps']      = false;
        $this->data['title']        = $this->lang->line("unitys");
        $this->data['title_link']     = base_url('admin/unitys');
        $this->data['unity'] = $this->my_model->get_table_row_query("SELECT * FROM vbs_unitys where id = '".$id."'");
        $this->data['unity'] = $this->data['unity'][0];
        $this->data['subtitle']     = '>'.' '.create_timestamp_uid($data['unity'][0]['created_at'],$id);
        $this->data['company_data'] = $this->general_model->get('vbs_company');
        $this->data['company_data'] = $this->data['company_data'][0];
        $this->data['content']  = 'admin/unitys/view_unity';
        $this->load->view('templates/theme_template',$this->data);
    }
    
}
?>