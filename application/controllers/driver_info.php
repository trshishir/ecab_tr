<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Driver_info extends MY_Controller
{
	
	function __construct()
	{

		parent::__construct();
		
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->lang->load('auth');
		$this->lang->load('general');
		$this->load->helper('language');
		$this->load->helper('validate');
		if (!$this->basic_auth->is_login())
			redirect("admin", 'refresh');
		else
		$this->data['user'] = $this->basic_auth->user();
		
		$this->load->model('drivers_model');
		$this->load->model('request_model');
		$this->load->model('jobs_model');
		$this->load->model('support_model');
		$this->load->model('calls_model');
		$this->load->model('notes_model');
		$this->load->model('notifications_model');
		$this->data['configuration'] = get_configuration();

	}

	public static $table = "driverprofile";
	public function index(){

		$this->data['user'] = $this->basic_auth->user();
		$this->data['css_type'] 	= array("form","datatable");
		$this->data['active_class'] = "driver";
		$this->data['gmaps'] 		= false;
		$this->data['title'] 		= 'Drivers';
		$this->data['title_link'] 	= base_url('admin/driver_info');
		$this->data['content'] 		= 'admin/driver_info/index';
   		$data = [];
		$this->data['driver_status_data'] = $this->drivers_model->getAllData('driverprofile');
		// log_message('debug', print_r($this->drivers_model->getAllData('driverprofile'),TRUE));
		// $this->data['post_data'] = $this->drivers_model->getAllData('vbs_driverpost');
		// $this->data['payment_method'] = $this->drivers_model->getAllData('vbs_u_payment_method');
		// $this->data['pattern_data'] = $this->drivers_model->getAllData('vbs_driverpattern');
		// $this->data['contract_data'] = $this->drivers_model->getAllData('vbs_drivercontract');
		// $this->data['nature_data'] = $this->drivers_model->getAllData('vbs_drivernature');
		// $this->data['hours_data'] = $this->drivers_model->getAllData('vbs_driverhours');
		// $this->data['type_data'] = $this->drivers_model->getAllData('vbs_drivertype');
		// $data['drivers'] = $this->drivers_model->getAll();
		$data['jobs'] 	 = $this->jobs_model->getAll();
		$data['calls']   = $this->calls_model->getAll();
		$this->_render_page('templates/admin_template_v2', $this->data);
	}
	public function driverProfileadd(){
		$this->load->model('drivers_model');
		$this->data['driver_status_data'] = $this->drivers_model->getAll();
		$this->data['css_type'] 	= array("form","datatable");
		$this->data['active_class'] = "driver_info";
		$this->data['title'] 		= 'Drivers Management';
		$this->data['title_link'] 	= base_url('admin/driver_info');
		$this->data['content'] 		= 'admin/driver_info/index';
		$this->data['active_status'] 		= '1';
		$this->load->model('calls_model');
		$this->load->model('request_model');
		$this->load->model('jobs_model');
		$data = [];
		$data['drivers'] = $this->drivers_model->getAll();
		$data['jobs'] 	 = $this->jobs_model->getAll();
		$data['calls']   = $this->calls_model->getAll();
		$this->data['data'] = $this->drivers_model->getAll();
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->driverProfilestore();
		}
		$this->_render_page('templates/admin_template_v2', $this->data);

	}
	public function driverProfilestore(){
		$this->form_validation->set_rules('address', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
		$this->load->library('form_validation');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == true) {
			$user = $this->basic_auth->user();
			 $user_id=$user->id;
				$driver_data = [];
				$driver_data['status'] =  $this->input->post('status');
				$driver_data['civilite'] =  $this->input->post('civilite');
				$driver_data['nom'] =  $this->input->post('nom');
				$driver_data['prenom'] =  $this->input->post('prenom');
				$driver_data['address'] =  $this->input->post('address');
				$driver_data['postalcode'] =  $this->input->post('postalcode');
				$driver_data['ville'] =  $this->input->post('ville');
				$driver_data['driverImg'] = $this->do_upload_image($_FILES['driverImg'],'driverImg');
				$driver_data['dateenaissance'] =  $this->input->post('dateenaissance');
				$driver_data['villedenaissance'] =  $this->input->post('villedenaissance');
				$driver_data['paysdenaissance'] =  $this->input->post('paysdenaissance');
				$driver_data['poste'] =  $this->input->post('poste');
				$driver_data['datedentree'] =  $this->input->post('datedentree');
				$driver_data['datedesortie'] =  $this->input->post('datedesortie');
				$driver_data['motif'] =  $this->input->post('motif');
				$driver_data['typecontrat'] =  $this->input->post('typecontrat');
				$driver_data['natureducontrat'] =  $this->input->post('natureducontrat');
				$driver_data['nombredheuremensuel'] =  $this->input->post('nombredheuremensuel');
				$driver_data['typedepiecedidentite'] =  $this->input->post('typedepiecedidentite');
				$driver_data['typedepiecedidentite'] =  $this->input->post('typedepiecedidentite');
				$driver_data['numerropiecedidentite'] =  $this->input->post('numerropiecedidentite');
				$driver_data['datedexpiration'] =  $this->input->post('datedexpiration');
				$driver_data['pumeropermis'] =  $this->input->post('pumeropermis');
				$driver_data['Upload1'] = $this->do_upload_image($_FILES['Upload1'],'Upload1');
				$driver_data['Upload2'] = $this->do_upload_image($_FILES['Upload2'],'Upload2');
				$driver_data['datedelivrance1'] =  $this->input->post('datedelivrance1');
				$driver_data['datedexpiration2'] =  $this->input->post('datedexpiration2');
				$driver_data['certificatmedicate'] = $this->do_upload_image($_FILES['certificatmedicate'],'certificatmedicate');
				$driver_data['datedelivrance2'] = $this->input->post('datedelivrance2');
				$driver_data['datedexpiration3'] =  $this->input->post('datedexpiration3');
				$driver_data['PSC1'] = $this->do_upload_image($_FILES['PSC1'],'PSC1');
				$driver_data['datedexpiration4'] =  $this->input->post('datedexpiration4');
				$driver_data['datedelivrance3'] =  $this->input->post('datedelivrance3');
				$driver_data['medecinedetravai'] = $this->do_upload_image($_FILES['medecinedetravai'],'medecinedetravai');
				$driver_data['datedelivrance4'] =  $this->input->post('datedelivrance4');
				$driver_data['datedexpiration5'] =  $this->input->post('datedexpiration5');
				$driver_data['autrdeiplome1'] = $this->do_upload_image($_FILES['autrdeiplome1'],'autrdeiplome1');
				$driver_data['autrediplome2'] = $this->do_upload_image($_FILES['autrediplome2'],'autrediplome2');
				$driver_data['autrediplome3'] = $this->do_upload_image($_FILES['autrediplome3'],'autrediplome3');
				$driver_data['userid'] =  $user_id;
				
			$table_name = "driverprofile";
			$status = $this->base_model->insert_operation($driver_data, $table_name);
		
			
			if ($status) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a driver status .",
					'class' => "alert-success",
					'status_id' => "1",
					'type' => "Success"
				]);
				redirect('admin/driver_info');
			} else {
				$this->session->set_flashdata('error', $this->ion_auth->messages());
				$this->_render_page('templates/admin_template_v2', $this->data);
			}
		}
	}
	public function driverProfileEdit(){
		$this->load->model('drivers_model');
		$this->data['css_type'] 	= array("form","datatable");
		$this->data['active_class'] = "driver_info";
		$this->data['title'] 		= $_SERVER['REQUEST_METHOD'] === 'POST' ? 'Drivers :'.@$_POST['driver_status_id'] : 'Drivers';
		$this->data['title_link'] 	= base_url('admin/driver_info');
		$this->data['content'] 		= 'admin/driver_info/index';
		$this->data['active_status'] 		= '1';
		$this->load->model('calls_model');
		$this->load->model('request_model');
		$this->load->model('jobs_model');
		$data = [];
		$data['drivers'] = $this->drivers_model->getAll();
		$data['jobs'] 	 = $this->jobs_model->getAll();
		$data['calls']   = $this->calls_model->getAll();
		$this->data['data'] = $this->drivers_model->getAll();
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['driver_status_id'];
			if (!empty($id)) {
			$update=$this->drivers_model->driverProfileUpdate([
				$driver_data['status'] => $this->input->post('status'),
				$driver_data['civilite'] => $this->input->post('civilite'),
				$driver_data['nom'] => $this->input->post('nom'),
				$driver_data['prenom'] => $this->input->post('prenom'),
				$driver_data['address'] => $this->input->post('address'),
				$driver_data['postalcode'] => $this->input->post('postalcode'),
				$driver_data['ville'] => $this->input->post('ville'),
				$driver_data['dateenaissance'] => $this->input->post('dateenaissance'),
				$driver_data['villedenaissance'] => $this->input->post('villedenaissance'),
				$driver_data['paysdenaissance'] => $this->input->post('paysdenaissance'),
				$driver_data['poste'] => $this->input->post('poste'),
				$driver_data['datedentree'] => $this->input->post('datedentree'),
				$driver_data['datedesortie'] => $this->input->post('datedesortie'),
				$driver_data['motif'] => $this->input->post('motif'),
				$driver_data['typecontrat'] => $this->input->post('typecontrat'),
				$driver_data['natureducontrat'] => $this->input->post('natureducontrat'),
				$driver_data['nombredheuremensuel'] => $this->input->post('nombredheuremensuel'),
				$driver_data['typedepiecedidentite'] => $this->input->post('typedepiecedidentite'),
				$driver_data['typedepiecedidentite'] => $this->input->post('typedepiecedidentite'),
				$driver_data['numerropiecedidentite'] => $this->input->post('numerropiecedidentite'),
				$driver_data['datedexpiration'] => $this->input->post('datedexpiration'),
				$driver_data['pumeropermis'] => $this->input->post('pumeropermis'),
				$driver_data['datedelivrance1'] => $this->input->post('datedelivrance1'),
				$driver_data['datedexpiration2'] => $this->input->post('datedexpiration2'),
				$driver_data['datedelivrance2'] =>$this->input->post('datedelivrance2'),
				$driver_data['datedexpiration3'] => $this->input->post('datedexpiration3'),
				$driver_data['PSC1'] =>$this->do_upload_image($_FILES['PSC1'],'PSC1'),
				$driver_data['datedexpiration4'] => $this->input->post('datedexpiration4'),
				$driver_data['datedelivrance3'] => $this->input->post('datedelivrance3'),
				$driver_data['datedelivrance4'] => $this->input->post('datedelivrance4'),
				$driver_data['datedexpiration5'] => $this->input->post('datedexpiration5')
				

				
				
			], $id);
			// isset($_FILES['driverImg']){
			// 	$driver_data['driverImg'] => $this->do_upload_image($_FILES['driverImg'],'driverImg'),
			// }
			// isset($_FILES['Upload1']){
			// 	$driver_data['Upload1'] => $this->do_upload_image($_FILES['Upload1'],'Upload1'),
			// }
			// isset($_FILES['Upload2']){
			// 	$driver_data['Upload2'] => $this->do_upload_image($_FILES['Upload2'],'Upload2'),
			// }
			// isset($_FILES['certificatmedicate']){
			// 	$driver_data['certificatmedicate'] => $this->do_upload_image($_FILES['certificatmedicate'],'certificatmedicate'),
			// }
			// isset($_FILES['medecinedetravai']){
			// 	$driver_data['medecinedetravai'] => $this->do_upload_image($_FILES['medecinedetravai'],'medecinedetravai'),
			// }
			
			// isset($_FILES['autrdeiplome1']){
			// 	$driver_data['autrdeiplome1'] => $this->do_upload_image($_FILES['autrdeiplome1'],'autrdeiplome1'),
			// }
			// isset($_FILES['autrediplome2']){
			// 	$driver_data['autrediplome2'] => $this->do_upload_image($_FILES['autrediplome2'],'autrediplome2'),
			// }
			// isset($_FILES['autrediplome3']){
			// 	$driver_data['autrediplome3'] => $this->do_upload_image($_FILES['autrediplome3'],'autrediplome3'),
			// }

			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a driver status.",
					'class' => "alert-success",
					'status_id' => "1",
					'type' => "Success"
				]);
				redirect('admin/driver_info');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
			}
		}
	}
		$this->data['driver_status_data'] = $this->drivers_model->getDriverProfile();
		$this->_render_page('templates/admin_template_v2', $this->data);
	}
	public function deleteDriverProfile(){
		$this->load->model('drivers_model');
		$id=$_POST['delet_driver_status_id'];
		$del=$this->drivers_model->deleteDriverProfile($id);
		if ($del==true) {
		$this->session->set_flashdata('alert', [
				'message' => "Successfully deleted.",
				'class' => "alert-success",
				'status_id' => "1",
				'type' => "Success"
		]);
		redirect('admin/driver_info');
		}
		else {
			$this->session->set_flashdata('alert', [
					'message' => "Please try again.",
					'class' => "alert-danger",
					'status_id' => "1",
					'type' => "Error"
			]);
			redirect('admin/driver_info');
		}
	}
	public function get_ajax_driverProfile(){
		$id=(int)$_GET['driver_status_id'];
		$this->data['title'] 		= 'Drivers :'.$id;
		if($id>0){
			$this->load->model('drivers_model');
			$driver_status_data = $this->drivers_model->driver_Config_getAll('vbs_driverprofile',['id' => $id]);
			?>
			<style media="screen">
			select {
			padding: 5px !important;
			background: url(<?php echo base_url();?>/assets/arrow.png) no-repeat right  !important;
			-webkit-appearance: none;} </style>
			<?php
			foreach($driver_status_data as $key => $driverProfile){
				$imgurl = base_url() .'/uploads/drivers/'. $driverProfile->driverImg;
				$result='<div class="row">
				<div class="col-md-6" >
                                
				<div class="row" style="margin-top: 10px;">

						<div class="col-md-6">
							<div class="form-group">
								<div class="col-md-4" style="margin-top: 10px;">
									<span style="font-weight: bold;">Statut </span>
								</div>
								<div class="col-md-8" style="padding: 0px;">
									<select class="form-control" value="'.$driverProfile->status.'" name="status">
										<option value="DISPONIBLE">DISPONIBLE</option>
										<option value="EN CONGE">EN CONGE</option>
										<option value="EN ARRET MALADIE">EN ARRET MALADIE</option>
										<option value="ABSENT">ABSENT</option>
										<option value="INACTIF">INACTIF</option>
									</select>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<div class="col-md-4" style="margin-top: 10px;">
									<span style="font-weight: bold;">Civilite</span>
								</div>
								<div class="col-md-3" style="padding: 0px;">
									<select class="form-control" name="civilite">
											<option value="Mr">Mr</option>
											<option value="Miss">Miss</option>
											<option value="Mme">Mme</option>
									</select>
								</div>

							</div>
						</div>
				</div>
				<div class="row" style="margin-top: 10px;">
					<div class="col-md-6">
							<div class="form-group">
								<div class="col-md-4" style="margin-top: 10px;">
									<span style="font-weight: bold;">Nom</span>
								</div>
								<div class="col-md-8" style="padding: 0px;">
									<input name="nom" value="'.$driverProfile->nom.'" class="form-control" type="text">
								</div>

							</div>
						</div>


						<div class="col-md-6">
							<div class="form-group">
								<div class="col-md-4" style="margin-top: 10px;">
									<span style="font-weight: bold;">Prenom</span>
								</div>
								<div class="col-md-8" style="padding: 0px;">
									<input name="prenom" value="'.$driverProfile->prenom.'" class="form-control" type="text">
								</div>
							</div>
						</div>
				</div>

				<div class="row" style="margin-top: 10px;">
				<div class="col-md-6">
					<div class="form-group">
						<div class="col-md-4" style="margin-top: 10px;">
							<span style="font-weight: bold;">Address</span>
						</div>
						<div class="col-md-8" style="padding: 0px;">
							<input name="address" value="'.$driverProfile->address.'" class="form-control" type="text">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<div class="col-md-4" style="margin-top: 10px;">
							<span style="font-weight: bold;">Address</span>
						</div>
						<div class="col-md-8" style="padding: 0px;">
							<input name="address" value="'.$driverProfile->address.'" class="form-control" type="text">
						</div>
					</div>
				</div>
			</div>

				<div class="row" style="margin-top: 10px;">
				<div class="col-md-6">
					<div class="form-group">
						<div class="col-md-4" style="padding: 0px;margin-top: 10px;">
							<span style="font-weight: bold;">Code Postal</span>
						</div>
						<div class="col-md-8" style="padding: 0px;">
							<input name="postalcode" value="'.$driverProfile->postalcode.'" class="form-control" type="text">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<div class="col-md-4" style="margin-top: 10px;">
							<span style="font-weight: bold;">Ville</span>
						</div>
						<div class="col-md-8" style="padding: 0px;">
							<input name="ville" value="'.$driverProfile->ville.'" class="form-control" type="text">
						</div>
					</div>
				</div>

			   
			</div>

			 </div>
			 <div class="col-md-6" style="padding: 0px;">
			 <div class="row" style="margin-top: 10px;">
				 
				 
				 
				 <div class="col-md-6">
					 <div class="form-group">
						 <div class="col-md-4" style="margin-top: 10px;">
							 <span style="font-weight: bold;">Driver Image</span>
						 </div>
						 <div class="col-md-8" style="padding: 0px;">
							 <input class="driver_image" name="driverImg" type="file">
						 </div>

					 </div>
				 </div>
				 <div class="col-md-6" style="text-align: center;">
								 <img id="preview_driver" alt="Preview Logo"
									 src="'.$imgurl.'"
									 style="border: 1px solid;cursor: pointer;height: 210px;width: 210px;position: relative;z-index: 10;">
							 </div>
			 </div>	
		</div>
			
			
			<div style="clear: both"></div>

			<div class="row" style="margin-top: 10px;">

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;">
								<span style="font-weight: bold;">Date de Naissance</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input name="dateenaissance" required=""
									   class="form-control datepicker" value="'.$driverProfile->dateenaissance.'" type="text">
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;">
								<span style="font-weight: bold;">Ville de Naissance</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input name="villedenaissance" value="'.$driverProfile->villedenaissance.'" required="" class="form-control"
									   type="text">
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;">
								<span style="font-weight: bold;">Pays de Naissance</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input name="paysdenaissance" value="'.$driverProfile->paysdenaissance.'" class="form-control" type="text">
							</div>

						</div>
					</div>

			</div>


			<div class="row" style="margin-top: 10px;">

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 10px;">
								<span style="font-weight: bold;">Poste </span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<select class="form-control" name="poste">
									<option value="Chauffeur Accompagnateur">Chauffeur Accompagnateur</option>
									<option value="Chauffeur">Chauffeur</option>
									<option value="Secretaire">Secretaire</option>
									<option value="Comptable">Comptable</option>
									<option value="Regulateur">Regulateur</option>
									<option value="Commercial">Commercial</option>
									<option value="Responsable Parc">Responsable Parc</option>
									<option value="Auxiliaire de vie">Auxiliaire de vie</option>
								</select>
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 10px;">
								<span style="font-weight: bold;">Date Dentree</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input required="" name="datedentree" value="'.$driverProfile->datedentree.'"
									   class="form-control datepicker" type="text">
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;">
								<span style="font-weight: bold;">Date de Sortie</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input required="" name="datedesortie" value="'.$driverProfile->datedesortie.'"
									   class="form-control datepicker" type="text">
							</div>

						</div>
					</div>


					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 10px;">
								<span style="font-weight: bold;">Motif</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<select class="form-control" name="motif">
									<option value="">Select</option>
								</select>
							</div>
						</div>
					</div>

			</div>

			<div class="row" style="margin-top: 10px;">

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 10px;">
								<span style="font-weight: bold;">Availability Contrat</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<select class="form-control" name="typecontrat">
									<option value="CDI">CDI</option>
									<option value="CDD">CDD</option>
									<option value="SAISONNIERE">SAISONNIERE</option>
								</select>
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;">
								<span style="font-weight: bold;">Nature du Contrat</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<select class="form-control" name="natureducontrat">
									<option value="Temps Plein">Temps Plein</option>
									<option value="Temps Partiel">Temps Partiel</option>
									<option value="Scolaire">Scolaire</option>
								</select>
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;padding-right: 0px;">
								<span style="font-weight: bold;">Nombre Dheure Mensuel</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<select class="form-control" name="nombredheuremensuel">
									<option value="104">104</option>
									<option value="65">65</option>
									<option value="151,67">151,67</option>
								</select>
							</div>

						</div>
					</div>

			</div>


			<div class="row" style="margin-top: 10px;">

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;">
								<span style="font-weight: bold;">Availability de Piece didentite</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<select class="form-control" name="typedepiecedidentite">
									<option value="CIN">CIN</option>
									<option value="Titre de sejour">Titre de sejour</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 10px;">
								<span style="font-weight: bold;">Upload</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input required="" name="upload1" type="file">
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;padding-right: 0px;">
								<span style="font-weight: bold;">Numerro Piece Didentite</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input required="" value="'.$driverProfile->numerropiecedidentite.'" name="numerropiecedidentite" class="form-control"
									   type="text">
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;">
								<span style="font-weight: bold;">Date dexpiration</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input required=""  value="'.$driverProfile->datedexpiration.'" name="datedexpiration"
									   class="form-control datepicker" type="text">
							</div>

						</div>
					</div>

			</div>


			<div class="row" style="margin-top: 10px;">

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;">
								<span style="font-weight: bold;">Numero Permis</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input required=""  value="'.$driverProfile->pumeropermis.'" name="pumeropermis" class="form-control"
									   type="text">
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 10px;">
								<span style="font-weight: bold;">Upload</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input required=""  name="Upload2" type="file">
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;">
								<span style="font-weight: bold;">Date Delivrance</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input required="" value="'.$driverProfile->datedelivrance1.'" name="datedelivrance1"
									   class="form-control datepicker" type="text">
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;">
								<span style="font-weight: bold;">Date Dexpiration</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input required="" value="'.$driverProfile->datedexpiration2.'" name="datedexpiration2"
									   class="form-control datepicker" type="text">
							</div>

						</div>
					</div>

			</div>


			<div class="row" style="margin-top: 10px;">

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;">
								<span style="font-weight: bold;">Certificat Medicate</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input required="" name="certificatmedicate" type="file">
							</div>
						</div>
					</div>


					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;">
								<span style="font-weight: bold;">Date Delivrance</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input required="" value="'.$driverProfile->datedelivrance2.'" name="datedelivrance2"
									   class="form-control datepicker" type="text">
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;">
								<span style="font-weight: bold;">Date Dexpiration</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input required="" value="'.$driverProfile->datedexpiration3.'" name="datedexpiration3"
									   class="form-control datepicker" type="text">
							</div>

						</div>
					</div>

			</div>


			<div class="row" style="margin-top: 10px;">

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 10px;">
								<span style="font-weight: bold;">PSC1</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input required="" name="PSC1" type="file">
							</div>
						</div>
					</div>


					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;">
								<span style="font-weight: bold;">Date Delivrance</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input required="" value="'.$driverProfile->datedelivrance3.'" name="datedelivrance3"
									   class="form-control datepicker" type="text">
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;">
								<span style="font-weight: bold;">Date Dexpiration</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input required="" value="'.$driverProfile->datedexpiration4.'" name="datedexpiration4"
									   class="form-control datepicker" type="text">
							</div>

						</div>
					</div>

			</div>


			<div class="row" style="margin-top: 10px;">

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;">
								<span style="font-weight: bold;">Medecine de Travai</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input required=""  name="medecinedetravai" type="file">
							</div>
						</div>
					</div>


					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;">
								<span style="font-weight: bold;">Date Delivrance</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input required="" name="datedelivrance4" value="'.$driverProfile->datedelivrance4.'"
									   class="form-control datepicker" type="text">
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;">
								<span style="font-weight: bold;">Date Dexpiration</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input required="" name="datedexpiration5" value="'.$driverProfile->datedexpiration5.'"
									   class="form-control  datepicker" type="text">
							</div>

						</div>
					</div>

			</div>

			<div class="row" style="margin-top: 10px;">

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;">
								<span style="font-weight: bold;">Autre Diplome1</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input required="" name="autrdeiplome1" type="file">
							</div>
						</div>
					</div>


					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;">
								<span style="font-weight: bold;">Autre Diplome2</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input required="" name="autrediplome2" type="file">
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<div class="col-md-4" style="margin-top: 5px;">
								<span style="font-weight: bold;">Autre Diplome3</span>
							</div>
							<div class="col-md-8" style="padding: 0px;">
								<input required="" name="autrediplome3" type="file">
							</div>

						</div>
					</div>

			</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}

	public function civiliteAdd(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->civilitestore();
		}else show_404();
	}
	public function civilitestore(){
		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_drivercivilite';
			$id = $this->drivers_model->driver_Config_Add($table,[
				'civilite' 		=> @$_POST['civilite']
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a civilite .",
				'class' => "alert-success",
				'status_id' => "2",
				'type' => "Success"
			]);
			redirect('admin/driver_info');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'type' => "Error"
			];
			redirect('admin/driver_info');
		}
	}
	public function civiliteEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['civilite_id'];
			if (!empty($id)) {
			$table='vbs_drivercivilite';
			$update=$this->drivers_model->driver_Config_Update($table,[
				'civilite' 		=> @$_POST['civilite'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Civilite.",
					'class' => "alert-success",
					'status_id' => "2",
					'type' => "Success"
				]);
				redirect('admin/driver_info');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/driver_info');
			}
		}
	}else show_404();

	}
	public function civiliteDelete(){
	$this->load->model('drivers_model');
	$id=$_POST['delet_civilite_id'];
	$table = "vbs_drivercivilite";
	$del=$this->drivers_model->driver_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "2",
			'type' => "Success"
	]);
	redirect('admin/driver_info');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "2",
				'type' => "Error"
		]);
		redirect('admin/driver_info');
	}
	 show_404();
	}
	public function get_ajax_civilite(){
		$id=(int)$_GET['civilite_id'];

		if($id>0){
			$this->load->model('drivers_model');
			$civilite_data = $this->drivers_model->driver_Config_getAll('vbs_drivercivilite',['id' => $id]);
			?>
			<style media="screen">
			select {
			padding: 5px !important;
			background: url(<?php echo base_url();?>/assets/arrow.png) no-repeat right  !important;
			-webkit-appearance: none;} </style>
			<?php
			foreach($civilite_data as $key => $civilite){
				$result='<input type="hidden" name="civilite_id" value="'.$civilite->id.'">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Civilite</span>
					<input type="text" class="form-control" name="civilite" placeholder="" value="'.$civilite->civilite.'">
				</div>
				</div>
				<div class="row" style="margin-top: 5px;">
			
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
    function do_upload_image($filename,$name) {
        
        $config['upload_path'] = './uploads/drivers/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['max_size'] = '10240';
        $config['file_name'] = str_replace(' ', '_', $filename['name']);
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        
        if (!$this->upload->do_upload($name)) {
            return NULL;
        } else {
            $avatar = $this->upload->data();
            $avatar = $avatar['file_name'];
        
            return $avatar;
        }
        
    }

}
