<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('general');
        $this->load->helper('language');
        $this->load->library('session');
        $this->load->model('user_model');
        $this->load->model('invoice_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');
        $this->load->model('support_model');
        $this->load->model('calls_model');
        $this->load->model('notes_model');
        $this->load->model('notifications_model');
        $this->load->model('cars_model');
        $this->load->model('company_model');
        $this->load->model('popups_model');
        $this->load->model('smtp_model');
        $this->load->model('userx_model');


        $this->data['title'] = "Client Dashboard";
        $this->data['css_type'] = array("form", "datatable");
        $this->data['user'] = $this->session->userdata('user');

        $this->data['configuration'] = get_configuration();
    }

    /* Added by Saravanan.R
      STARTS
     */

    public function signup() {
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "Client";
        $this->data['gmaps'] = false;
        $this->data['title'] = 'Client Signup';
        $this->data['subtitle'] = 'Client';
        $this->data['title_link'] = '#';
        $this->load->library('form_validation');
        $this->data['company_name'] = $this->company_model->getCompanyName();


        $this->data['civility'] = array('name' => 'civility', 'class' => 'form-control', 'placeholder' => 'Civility', 'id' => 'civility', 'type' => 'text', 'value' => $this->form_validation->set_value('civility'));
        $this->data['first_name'] = array('name' => 'first_name', 'class' => 'form-control', 'placeholder' => 'First name', 'id' => 'first_name', 'type' => 'text', 'value' => $this->form_validation->set_value('first_name'));
        $this->data['last_name'] = array('name' => 'last_name', 'class' => 'form-control', 'placeholder' => 'Last name', 'id' => 'last_name', 'type' => 'text', 'value' => $this->form_validation->set_value('last_name'));
        $this->data['email'] = array('name' => 'email', 'class' => 'form-control', 'placeholder' => 'User Email', 'id' => 'email', 'type' => 'text', 'value' => $this->form_validation->set_value('email'));
        $this->data['phone'] = array('name' => 'phone', 'class' => 'form-control', 'placeholder' => 'phone', 'id' => 'phone', 'type' => 'text', 'maxlength' => '11', 'value' => $this->form_validation->set_value('phone'));
        $this->data['mobile'] = array('name' => 'mobile', 'class' => 'form-control', 'placeholder' => 'Mobile', 'id' => 'mobile', 'type' => 'text', 'maxlength' => '15', 'value' => $this->form_validation->set_value('mobile'));
        $this->data['fax'] = array('name' => 'fax', 'class' => 'form-control', 'placeholder' => 'Fax', 'id' => 'fax', 'type' => 'text', 'maxlength' => '15', 'value' => $this->form_validation->set_value('fax'));
        $this->data['password'] = array('name' => 'password', 'class' => 'form-control', 'placeholder' => 'Password', 'id' => 'password', 'type' => 'password', 'value' => $this->form_validation->set_value('password'));
        $this->data['confirm_password'] = array('name' => 'confirm_password', 'class' => 'form-control', 'placeholder' => 'Confirm Password', 'id' => 'confirm_password', 'type' => 'password', 'value' => $this->form_validation->set_value('confirm_password'));
        $this->data['company'] = array('name' => 'company', 'class' => 'form-control', 'placeholder' => 'Company', 'id' => 'company', 'type' => 'text', 'maxlength' => '150', 'value' => $this->form_validation->set_value('company'));
        $this->data['address'] = array('name' => 'address', 'class' => 'form-control', 'placeholder' => 'adresse', 'rows' => '1', 'cols' => '40', 'id' => 'address', 'type' => 'text', 'value' => $this->form_validation->set_value('address'));
        $this->data['address1'] = array('name' => 'address1', 'class' => 'form-control', 'placeholder' => '', 'rows' => '1', 'cols' => '40', 'id' => 'address1', 'type' => 'text', 'value' => $this->form_validation->set_value('address'));
        $this->data['city'] = array('name' => 'city', 'class' => 'form-control', 'placeholder' => 'City', 'id' => 'city', 'type' => 'text', 'value' => $this->form_validation->set_value('city'));
        $this->data['zipcode'] = array('name' => 'zipcode', 'class' => 'form-control', 'placeholder' => 'Zipcode', 'id' => 'zipcode', 'type' => 'text', 'value' => $this->form_validation->set_value('zipcode'));
        $this->data['content'] = 'site/client_signup';
        $this->data['tos'] = '';//strip_tags(file_get_contents($tos_file))
        $this->data['privacy'] = '';


        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // validate form input
            $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required|xss_clean');
            $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required|xss_clean');
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[vbs_users.email]');
            $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone1_label'), 'xss_clean|integer|min_length[8]|max_length[10]');
            $this->form_validation->set_rules('mobile', $this->lang->line('create_user_validation_phone1_label'), 'required|xss_clean|integer|min_length[10]|max_length[10]');
            $this->form_validation->set_rules('address', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
            $this->form_validation->set_rules('address1', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
            $this->form_validation->set_rules('city', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
            $this->form_validation->set_rules('zipcode', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean|min_length[5]');
            $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']'); // |matches[confirm_password] 
            $this->load->library('form_validation');

            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            if ($this->form_validation->run() == true) {
                $username = $this->input->post('first_name') . '' . $this->input->post('last_name');
                $email = strtolower($this->input->post('email'));
                $password = $this->input->post('password');
                $user_role = 6;
                $user_group = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('groups') . " WHERE name = 'clients'");
                $group_id = array( $user_group[0]->id);
                $additional_data = array(
                    'civility' => $this->input->post('civility'),
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'phone' => $this->input->post('phone'),
                    'city' => $this->input->post('city'),
                    'zipcode' => $this->input->post('zipcode'),
                    'address' => $this->input->post('address'),
                    'date_of_registration' => date('Y-m-d')
                );

                if ($this->form_validation->run() == true && $this->ion_auth->register($user_role, $username, $password, $email, $additional_data,$group_id)) {
                    $user_rec = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('users') . " WHERE email = '" . $email . "'");
                    $inputdata['user_id'] = $user_rec[0]->id;
                    $inputdata['address1'] = $this->input->post('address');
                    $inputdata['address2'] = $this->input->post('city');
                    $inputdata['fax_no'] = $this->input->post('fax');
                    $inputdata['company_name'] = $this->input->post('company');
                    $table_name = "users_details";

                    $status = $this->base_model->insert_operation($inputdata, $table_name);
                    echo "status:".$status."<br/>";
                    // $user = $this->basic_auth->login($username, $password);

                    if ($status) {

                        // $this->session->set_flashdata('message', $this->ion_auth->messages());
                        // check to see if we are creating the user
                        // redirect them back to the admin page
                        $this->session->set_flashdata('messages', $this->ion_auth->messages());
                        redirect('client/login', 'refresh');
                    } else {
                        $this->session->set_flashdata('error', $this->ion_auth->messages());
                        $this->_render_page('templates/site_template', $this->data);
                    }
                } else {
                    $this->session->set_flashdata('error', $this->ion_auth->messages());
                    $this->_render_page('templates/site_template', $this->data);
                }
            }
        } else {


            $this->_render_page('templates/site_template', $this->data);
        }
        //$this->load->view('site/common/header',$this->data); 
        //$this->load->view('site/common/navigation',$this->data); 
        // $this->load->view('site/driver_registration',$this->data); 
        // $this->load->view('site/common/footer',$this->data); 
    }

    public function login() {

        $this->data['alert'] = "";
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "Client";
        $this->data['gmaps'] = false;
        $this->data['title'] = 'Client Login';
        $this->data['subtitle'] = 'Client Login';
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[2]|valid_email|max_length[50]|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[2]|max_length[50]|xss_clean');

            if ($this->form_validation->run() !== false) {
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                $remember = $this->input->post('remember_me');
                $curl = $this->input->post('curl');

                $user = $this->basic_auth->client_login($username, $password, $remember);

                if ($user != false) {
                    $this->session->set_userdata("user", $user);
                    $this->session->set_flashdata('message', $this->ion_auth->messages());
					if(empty($curl)){
						redirect("client/dashboard", 'refresh');
					}else if($curl=='contact'){
						redirect("contact", 'refresh');
					}else if($curl=='popup'){
						redirect(site_url(), 'refresh');
					}
                } else
                    
                $this->data['alert'] = [
                    'message' => "Authentication failed.",
                    'class' => "alert-danger",
                    'type' => "Error"
                ];
            } else {
                $this->data['alert'] = [
                    'message' => "Invalid email or password.",
                    'class' => "alert-danger",
                    'type' => "Error"
                ];
            }
        }
        $this->data['company'] = $this->company_model->getFirst();
        $this->data['content'] = 'site/client_login';
        $this->data['username'] = array(
            'name' => 'username',
            'id' => 'identity',
            'type' => 'text',
            'class' => 'form-control',
            'placeholder' => $this->lang->line('login_identity_label'),
            'value' => $this->form_validation->set_value('username'),
        );

        $this->data['password'] = array(
            'name' => 'password',
            'id' => 'password',
            'type' => 'password',
            'placeholder' => $this->lang->line('password'),
            'class' => 'form-control'
        );

        $this->data['forgot_form'] = false;

        $this->_render_page('templates/site_template', $this->data);
    }
	
	public function popup_login() {
		$username = $_GET['username'];
		$password = $_GET['password'];
		$remember = $_GET['remember_me'];

		$user = $this->basic_auth->client_login($username, $password, $remember);

		if ($user != false) {
			$this->session->set_userdata("user", $user);
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			echo json_encode($user);
		} else{
			echo json_encode([
                    'message' => "Invalid email or password.",
                    'type' => "Error"
                ]);
		}
    }

    public function logout() {
        $this->basic_auth->logout();
        // redirect them to the login page
        //$this->session->set_flashdata('message', $this->basic_auth->messages());
        $this->session->userdata = array();
       redirect('client/identification', 'refresh');
    }

    public function dashboard() {
        if (!(bool) $this->basic_auth->is_login() || !$this->basic_auth->is_client()) {
            redirect('client/identification', 'refresh');
        }
        else {
            $this->data['user'] = $this->basic_auth->user();
        }
	 if(isset($this->session->userdata['current_booking_url'])){
        
            $current_url=$this->session->userdata['current_booking_url'];
            $this->session->unset_userdata('current_booking_url');
          
            redirect($current_url);
        }
        
        $this->data['css'] = array('form');
        $this->data['bread_crumb'] = true;
        $this->data['heading'] = 'Client Dashboard';
        $this->data['content'] = 'users/dashboard';
        $this->data['forgot_form'] = false;
		
		$data = [];

        $this->data['total_clients'] = $this->user_model->getClientsTotal();
        
		if($this->session->userdata('user')->email == 'client@ecab.app'){
			$data['request'] = $this->request_model->getAll();
		}else{
			$data['request'] = $this->request_model->getAll(array('user_id'=>$this->session->userdata('user')->id));
		}

        
//        for bar chart Qoute Request
        $record = $this->request_model->QouteChartCount();

        foreach ($record as $row1) {
            $data1['label'][] = $row1->month_name;
            $data1['data'][] = (int) $row1->count;
        }
        $this->data['chart_data'] = json_encode($data1);


        //        for line chart Qoute request
        $QouteLine = $this->request_model->QouteLineChart();
//        print_r($QouteLine);
        foreach ($QouteLine as $line) {
            $data4['day'][] = $line->y;
            $data4['count'][] = $line->a;
        }
        $this->data['qoute_line_data'] = json_encode($data4);


        foreach ($data as $key => $d) {
            if ($d != false) {
                foreach ($d as $i) {
                    if (!empty($i->status))
                        $this->data[$key][strtolower($i->status)] = isset($this->data[$key][strtolower($i->status)]) ? $this->data[$key][strtolower($i->status)] + 1 : 1;
                }
            }
        }
        
        $this->_render_page('templates/client_template', $this->data);
    }

    /* ENDS by Saravanan.R */
	
	public function request(){
		$this->data['css_type'] 	= array("form","datatable");
		$this->data['active_class'] = "request";
		$this->data['gmaps'] 		= false;
		$this->data['title'] 		= $this->lang->line("request");
		$this->data['title_link'] 	= base_url('client/request');
		$this->data['content'] 		= 'users/index';

        $this->load->model('calls_model');
        //$this->load->model('request_model');
        $this->load->model('jobs_model');

        $data = [];
		
		if($this->session->userdata('user')->email == 'client@ecab.app'){
			$this->data['data'] = $this->request_model->getAll();
		}else{
			$this->data['data'] = $this->request_model->getAll(array('user_id'=>$this->session->userdata('user')->id));
		}
		
       
        $this->data['popups'] = $this->popups_model->get(array('id'=>1));
        $this->data['company_data'] = $this->userx_model->get_company();
		$this->data['MAIL'] = $this->smtp_model->get(array('id' => 1));
		$this->_render_page('templates/client_template', $this->data);
	}

	public function add(){
		$this->data['css_type'] 	= array("form");
		$this->data['active_class'] = "request";
		$this->data['gmaps'] 	= false;
		$this->data['title'] 	= $this->lang->line("request");
		$this->data['title_link'] = base_url('client/request');
		$this->data['subtitle'] = "Add";
		$this->data['content']  = 'users/add';
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->store();
		}
		$this->_render_page('templates/client_template', $this->data);
	}

	public function store(){
			$error = request_validate();
			if (empty($error)) {
				//$dob = to_unix_date(@$_POST['dob']);
				$id = $this->request_model->create([
					'user_id' 		=> $this->session->userdata('user')->id,
					'civility' 		=> @$_POST['civility'],
					'first_name' 	=> @$_POST['name'],
					'last_name' 	=> @$_POST['prename'],
					'company' 		=> @$_POST['company'],
					'email' 		=> @$_POST['email'],
					'telephone' 	=> @$_POST['tel'],
					//'dob' 			=> @$dob,
					'message' 		=> @$_POST['message'],
					'status' 		=> @$_POST['status'],
					'msg_subject' 	=> @$_POST['msg_subject'],
					'ip_address'	=> $this->input->ip_address()
				]);

				$this->session->set_flashdata('alert', [
					'message' => "Successfully Created.",
					'class' => "alert-success",
					'type' => "Success"
				]);
				redirect('client/request/'.$id.'/edit');
			} else {
				$this->data['alert'] = [
					'message' => @$error[0],
					'class' => "alert-danger",
					'type' => "Error"
				];
			}
	}
    
    public function filterby() {
        // $msg = json_encode($_GET);
        // {"modules":"Default","year_lists":"2020","month_lists":"jan","from_period":"","to_period":""}
        $year = $_GET['year_lists'];
        $modules = $_GET['modules'];
        $month = $_GET['month_lists'];
        $from_date = empty($_GET['from_period']) ? 1 : $_GET['from_period'];
        $to_date = empty($_GET['to_period']) ? 31 : $_GET['to_period'];
        // 2020-12-31
        $from = "$year-$month-$from_date";
        $to = "$year-$month-$to_date";
        $req1 = $this->request_model->getRowsCount("(created_at BETWEEN '$from' AND '$to') AND status='New'");
        $req2 = $this->request_model->getRowsCount("(created_at BETWEEN '$from' AND '$to') AND status='Pending'");
        $req3 = $this->request_model->getRowsCount("(created_at BETWEEN '$from' AND '$to') AND status='Replied'");
        $req4 = $this->request_model->getRowsCount("(created_at BETWEEN '$from' AND '$to') AND status='Closed'");
        
        $cal1 = $this->calls_model->getRowsCount("(created_at BETWEEN '$from' AND '$to') AND status='New'");
        $cal2 = $this->calls_model->getRowsCount("(created_at BETWEEN '$from' AND '$to') AND status='Pending'");
        $cal3 = $this->calls_model->getRowsCount("(created_at BETWEEN '$from' AND '$to') AND status='Replied'");
        $cal4 = $this->calls_model->getRowsCount("(created_at BETWEEN '$from' AND '$to') AND status='Closed'");
        
        $job1 = $this->jobs_model->getRowsCount("(created_at BETWEEN '$from' AND '$to') AND status='New'");
        $job2 = $this->jobs_model->getRowsCount("(created_at BETWEEN '$from' AND '$to') AND status='Pending'");
        $job3 = $this->jobs_model->getRowsCount("(created_at BETWEEN '$from' AND '$to') AND status='Meeting'");
        $job4 = $this->jobs_model->getRowsCount("(created_at BETWEEN '$from' AND '$to') AND status='Accepted'");
        $job5 = $this->jobs_model->getRowsCount("(created_at BETWEEN '$from' AND '$to') AND status='Denied'");
        
        $sup1 = $this->support_model->getRowsCount("(created_on BETWEEN '$from' AND '$to') AND status='New'");
        $sup2 = $this->support_model->getRowsCount("(created_on BETWEEN '$from' AND '$to') AND status='Pending'");
        $sup3 = $this->support_model->getRowsCount("(created_on BETWEEN '$from' AND '$to') AND status='Replied'");
        $sup4 = $this->support_model->getRowsCount("(created_on BETWEEN '$from' AND '$to') AND status='Closed'");
        
        $msg = "from $from to $to";
        $res = [
            'chart1' => [$req1,$req2,$req3,$req4],
            'chart2' => [$cal1,$cal2,$cal3,$cal4],
            'chart3' => [$job1,$job2,$job3,$job4,$job5],
            'chart4' => [$sup1,$sup2,$sup3,$sup4],
            'msg' => $msg
        ];
        echo json_encode($res);
    }
	public function edit($id){

		$this->data['data'] 		= $this->request_model->get(['id' => $id]);
		if($this->data['data'] != false) {
			$this->data['css_type'] = array("form");
			$this->data['active_class'] = "request";
			$this->data['gmaps'] = false;
			$this->data['title'] 	= $this->lang->line("request");
			$this->data['title_link'] = base_url('client/request');
			$this->data['subtitle'] = create_timestamp_uid($this->data['data']->created_at,$id);
			$this->data['content']  = 'users/edit';
			
			$this->load->model('quick_replies_model');
			$this->data['quick_replies']  = $this->quick_replies_model->getAll(array('delete_bit' => 0, 'status' => 1, 'module' => 2));
			
			if($this->data['data']->unread != 0)
				$this->request_model->update(['unread' => 0], $id);

			if($this->data['data']->status == "New")
				$this->request_model->update(array('Status' => "Pending", 'reminder_update_date' => "", 'reminder_count' => 0), $id);
			
			//$this->data['replies'] = $this->base_model->get("request_replies", array('request_id'=>$id));
			$this->data['replies'] = $this->base_model->get_replies($id, 2);
			$this->data['company_data'] = $this->userx_model->get_company();
			$this->_render_page('templates/client_template', $this->data);
		} else show_404();
	}

	public function update($id){
		$request = $this->request_model->get(['id' => $id]);
		if($request != false) {
			//$error = request_validate();
			if (empty($error)) {
			    $check = 1;
				if(@$_POST['status'] == "New"){
					$check = 1;
				}else if(@$_POST['status'] == "Pending"){
					$check = 2;
				}else if(@$_POST['status'] == "Replied"){
					$check = 3;
				}else{
					$check = 4;
				}
				$notifications = $this->notifications_model->get(array('status'=>$check, 'department'=>2, 'notification_status'=>1));
				if($notifications != null && !empty($notifications)){
					$MAIL = $this->smtp_model->get(array('id' => 1));
					$company_data = $this->userx_model->get_company();
					$request_reply = $this->request_model->get_reply($request->id);
					$message .= $notifications->message;
					$subject = $notifications->subject;
					$subject = str_replace("{quote_request_subject}","Demande de Devis ecab.app",$subject);	
					if(!empty($request_reply)){
						$message = str_replace("{last_quote_request_user_reply}",$request_reply[0]->message,$message);	
					}else{
						$message = str_replace("Reply : {last_quote_request_user_reply}","",$message);	
					}
					$message = str_replace("{quote_request_sender_email}",$request->email,$message);
					$message = str_replace("{quote_request_date}",from_unix_date($request->created_at),$message);
					$message = str_replace("{quote_request_time}",from_unix_time($request->created_at),$message);
					$message = str_replace("{quote_request_civility}",$request->civility,$message);
					$message = str_replace("{quote_request_first_name}",$request->first_name,$message);
					$message = str_replace("{quote_request_last_name}",$request->last_name,$message);
					$message = str_replace("{quote_request_company_name}",$request->company,$message);
					$message = str_replace("Subject : {quote_request_subject}","",$message);
					$message = str_replace("{quote_request_message}",$request->message,$message);
					if(@$_POST['status'] == "New"){
						$message .= '<div class="row section-company-info" style=" background: -webkit-linear-gradient(#efefef, #ECECEC, #CECECE);margin: 10px 0px;max-height: 600px;border: 2px solid #a4a8ab;padding:10px;">
						<div class="col-md-5">
							<div class="text_company">';
						if(isset($company_data['name']) && !empty($company_data['name'])){
							$message .= '<p><span>'.$company_data["name"].'</span></p>';
						}
						if(isset($company_data['email']) && !empty($company_data['email'])){
							$message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Email:</span><span>'.$company_data['email'].'</span></p>';
						}
						if(isset($company_data['phone']) && !empty($company_data['phone'])){
							$message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Phone:</span><span>'.$company_data['phone'].'</span></p>';
						}
						if(isset($company_data['fax']) && !empty($company_data['fax'])){
							$message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Fax:</span><span>'.$company_data['fax'].'</span></p>';
						}
						if(isset($company_data['website']) && !empty($company_data['website'])){
							$message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Website:</span><span>'.$company_data['website'].'</span></p>';
						}
						if(isset($company_data['city']) && !empty($company_data['city'])){
							$message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Address:</span><span>'.$company_data['city'].' '.$company_data['country'].'</span></p>';
						}
						$message .= '<p class="social_icons">';
							if(isset($company_data['facebook_link']) && !empty($company_data['facebook_link'])){
								$message .= '<a style="font-size: 12px;display: inline-block;height: 25px;width: 25px;background: #fff;border-radius: 50%;text-align: center;line-height: 25px;margin-right: 15px;" href="'.$company_data['facebook_link'].'" target="_blank"><i class="fa fa-facebook"></i></a>';
							}
							if(isset($company_data['youtube_link']) && !empty($company_data['youtube_link'])){
								$message .= '<a style="font-size: 12px;display: inline-block;height: 25px;width: 25px;background: #fff;border-radius: 50%;text-align: center;line-height: 25px;margin-right: 15px;" href="'.$company_data['youtube_link'].'" target="_blank"><i class="fa fa-youtube"></i></a>';
							}
							if(isset($company_data['instagram_link']) && !empty($company_data['instagram_link'])){
								$message .= '<a style="font-size: 12px;display: inline-block;height: 25px;width: 25px;background: #fff;border-radius: 50%;text-align: center;line-height: 25px;margin-right: 15px;" href="'.$company_data['instagram_link'].'" target="_blank"><i class="fa fa-instagram"></i></a>';
							}
						$message .= '</p>
						</div>
							</div>
							<div class="col-md-7" style="margin-top: 15px;">
								<div class="profile_image">';
						$message .= '<a href="" class="company_image"><img style="width: 100%;max-width: 300px;max-height: 370px;"';
						if(isset($company_data['logo']) && !empty($company_data['logo'])){
							$message .= 'src="'.base_url('uploads/company').'/'.$company_data['logo'].'"';
						}
						$message .= 'alt=""></a>';
						$message .= '</div>
							</div>
						</div>';
					}
					$check = sendReply($request,$subject,$message,"",$MAIL,array(@$_POST['status']));
				}
				//$dob = to_unix_date(@$_POST['dob']);
				if($_POST['status'] == "Replied"){
					$this->request_model->update([
	/*						'civility' 		=> @$_POST['civility'],
							'first_name' 	=> @$_POST['name'],
							'last_name' 	=> @$_POST['prename'],
							'email' 		=> @$_POST['email'],
							'telephone' 	=> @$_POST['tel'],
							//'dob' 			=> @$dob,
							'message' 		=> @$_POST['message'],*/
							'status' 		=> @$_POST['status'],
							'reminder_update_date' 		=> date('Y-m-d H:i:s'),
							'reminder_count' 		=> 0,
					], $id);
				}else{
					$this->request_model->update([
	/*						'civility' 		=> @$_POST['civility'],
							'first_name' 	=> @$_POST['name'],
							'last_name' 	=> @$_POST['prename'],
							'email' 		=> @$_POST['email'],
							'telephone' 	=> @$_POST['tel'],
							//'dob' 			=> @$dob,
							'message' 		=> @$_POST['message'],*/
							'status' 		=> @$_POST['status'],
							'reminder_update_date' 		=> "",
							'reminder_count' 		=> 0,
					], $id);
				}

				$this->session->set_flashdata('alert', [
						'message' => "Successfully Updated.",
						'class' => "alert-success",
						'type' => "Success"
				]);
			} else {

				$this->session->set_flashdata('alert', [
						'message' => @$error[0],
						'class' => "alert-danger",
						'type' => "Error"
				]);
			}
			redirect('client/request/'.$id.'/edit');
		} else show_404();
	}

	public function reply($id){
		$call = $this->request_model->get(['id' => $id]);
		if($call != false) {
			$this->form_validation->set_rules('reply_subject', 'Subject', 'trim|xss_clean|min_length[0]|max_length[200]');
			$this->form_validation->set_rules('reply_message', 'Message', 'trim|xss_clean|min_length[0]|max_length[5000]');
			if ($this->form_validation->run() !== false) {

				$subject = isset($_POST['reply_subject']) ? $_POST['reply_subject'] : '';
				$message = isset($_POST['reply_message']) ? $_POST['reply_message'] : '';
				$msg = $message;
				$MAIL = $this->smtp_model->get(array('id' => 1));
				$company_data = $this->userx_model->get_company();
				$message .= '<div class="row section-company-info" style=" background: -webkit-linear-gradient(#efefef, #ECECEC, #CECECE);margin: 10px 0px;max-height: 600px;border: 2px solid #a4a8ab;padding:10px;">
				<div class="col-md-5">
					<div class="text_company">';
				if(isset($company_data['name']) && !empty($company_data['name'])){
					$message .= '<p><span>'.$company_data["name"].'</span></p>';
				}
				if(isset($company_data['email']) && !empty($company_data['email'])){
					$message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Email:</span><span>'.$company_data['email'].'</span></p>';
				}
				if(isset($company_data['phone']) && !empty($company_data['phone'])){
					$message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Phone:</span><span>'.$company_data['phone'].'</span></p>';
				}
				if(isset($company_data['fax']) && !empty($company_data['fax'])){
					$message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Fax:</span><span>'.$company_data['fax'].'</span></p>';
				}
				if(isset($company_data['website']) && !empty($company_data['website'])){
					$message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Website:</span><span>'.$company_data['website'].'</span></p>';
				}
				if(isset($company_data['city']) && !empty($company_data['city'])){
					$message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Address:</span><span>'.$company_data['city'].' '.$company_data['country'].'</span></p>';
				}
				$message .= '<p class="social_icons">';
					if(isset($company_data['facebook_link']) && !empty($company_data['facebook_link'])){
						$message .= '<a style="font-size: 12px;display: inline-block;height: 25px;width: 25px;background: #fff;border-radius: 50%;text-align: center;line-height: 25px;margin-right: 15px;" href="'.$company_data['facebook_link'].'" target="_blank"><i class="fa fa-facebook"></i></a>';
					}
					if(isset($company_data['youtube_link']) && !empty($company_data['youtube_link'])){
						$message .= '<a style="font-size: 12px;display: inline-block;height: 25px;width: 25px;background: #fff;border-radius: 50%;text-align: center;line-height: 25px;margin-right: 15px;" href="'.$company_data['youtube_link'].'" target="_blank"><i class="fa fa-youtube"></i></a>';
					}
					if(isset($company_data['instagram_link']) && !empty($company_data['instagram_link'])){
						$message .= '<a style="font-size: 12px;display: inline-block;height: 25px;width: 25px;background: #fff;border-radius: 50%;text-align: center;line-height: 25px;margin-right: 15px;" href="'.$company_data['instagram_link'].'" target="_blank"><i class="fa fa-instagram"></i></a>';
					}
				$message .= '</p>
				</div>
					</div>
					<div class="col-md-7" style="margin-top: 15px;">
						<div class="profile_image">';
				$message .= '<a href="" class="company_image"><img style="width: 100%;max-width: 300px;max-height: 370px;"';
				if(isset($company_data['logo']) && !empty($company_data['logo'])){
					$message .= 'src="'.base_url('uploads/company').'/'.$company_data['logo'].'"';
				}
				$message .= 'alt=""></a>';
				$message .= '</div>
					</div>
				</div>';
				
				if ($_FILES['attachment']['name']) {
					$cart = array();
					$cpt = count($_FILES['attachment']['name']);
					for($i=0; $i<$cpt; $i++)
					{
						if(!empty($_FILES['attachment']['name'][$i])){
							$_FILES['file']['name'] = $_FILES['attachment']['name'][$i];
							$_FILES['file']['type'] = $_FILES['attachment']['type'][$i];
							$_FILES['file']['tmp_name'] = $_FILES['attachment']['tmp_name'][$i];
							$_FILES['file']['error'] = $_FILES['attachment']['error'][$i];
							$_FILES['file']['size'] = $_FILES['attachment']['size'][$i];
							// $profile_image = time() ."-" . preg_replace('/\s+/', '', $_FILES['attachment']['name'][$i]);
							$path = $_FILES['attachment']['name'][$i];
							$ext = pathinfo($path, PATHINFO_EXTENSION);
							$profile_image = time() ."_" . rand(100,1000) .$i.'.'.$ext;
							$config['file_name'] = $profile_image;
							$config['upload_path']          = './uploads/attachment/';
							$config['allowed_types']        = 'gif|jpg|png|jpeg|docx|doc|pdf|txt|mp3|wav|zip|csv|sql|xml|psd|svg|ico|html|php|ppt|xls|xlsx|mp4|mpg|mpeg|wmv|mov|3gp|mkv';
							$this->load->library('upload', $config);
							$this->upload->initialize($config);
							if ( ! $this->upload->do_upload('file'))
							{
								$message = array('error' => $this->upload->display_errors());
								$this->session->set_flashdata('message', $message);
								redirect($_SERVER['HTTP_REFERER']);
							}
							else
							{
								$dataa = array('upload_data' => $this->upload->data());
								array_push($cart, $profile_image);
							}
						}
					}
					$attachment['attachment'] = implode(",",$cart);
				}
				$check = sendReply($call,$subject,$message,"",$MAIL,array(),array(),$attachment['attachment']);

				if($check['status'] != false) {
					$this->request_model->update(array('last_action' => date('Y-m-d H:i:s'), 'status'=>'Replied', 'reminder_update_date' => date('Y-m-d H:i:s'), 'reminder_count' => 0), $id);
					$data = array(
						'subject' => $subject,
						'message' => $msg,
						'request_id' => $id,
						'type' => 2,
						'addedBy' => $this->session->userdata('user')->id,
						'created_at' => date('Y-m-d H:i:s'),
					);
					$this->base_model->SaveForm('vbs_request_replies', $data);
					$lastid = $this->base_model->get_last_record('vbs_request_replies');	
					if($attachment['attachment'] != ""){
						$data = array(
							'request_reply_id' => $lastid->id,
							'attachments' => $attachment['attachment'],
						);
						$this->base_model->SaveForm('vbs_request_attachments', $data);
					}
					$this->session->set_flashdata('alert', [
						'message' => "Successfully Reply Sent.",
						'class' => "alert-success",
						'type' => "Success"
					]);
				} else
					$this->session->set_flashdata('alert', [
							'message' => $check['message'],
							'class' => "alert-danger",
							'type' => "Danger"
					]);
			} else {
				$validator['messages'] = "";
				foreach ($_POST as $key => $inp) {
					if(form_error($key) != false){
						$this->session->set_flashdata('alert', [
								'message' => form_error($key,"<span>","</span>"),
								'class' => "alert-danger",
								'type' => "Danger"
						]);
						break;
					}
				}
			}

			redirect('client/request/'.$id.'/edit');
		} else show_404();
	}

	public function delete($id){
		$this->request_model->delete($id);
		$this->session->set_flashdata('alert', [
				'message' => "Successfully deleted.",
				'class' => "alert-success",
				'type' => "Success"
		]);
		redirect('client/request');
	}
	
	public function profile(){
            $user_id = $this->session->userdata('user')->id;
            $this->data['alert'] = "";
            $this->data['css_type'] = array("form", "datatable");
            $this->data['active_class'] = "Client";
            $this->data['gmaps'] = false;
            $this->data['title'] = 'Client Edit';
            $this->load->library('form_validation');
            $user_data = $this->user_model->getUserDetail($user_id);
            $this->data['user_data'] = $user_data;
            $statut = ($user_data->company_name != "") ? "2" : "1";
            $this->data['statut'] = $statut;
            $this->data['civility'] = array('name' => 'civility', 'class' => 'user form-control', 'placeholder' => 'Civility', 'id' => 'civility', 'type' => 'text', 'value' => $user_data->civility);
            $this->data['first_name'] = array('name' => 'first_name', 'class' => 'user form-control', 'placeholder' => 'First name', 'id' => 'first_name', 'type' => 'text', 'value' =>  $user_data->first_name);
            $this->data['last_name'] = array('name' => 'last_name', 'class' => 'user form-control', 'placeholder' => 'Last name', 'id' => 'last_name', 'type' => 'text', 'value' =>  $user_data->last_name);
            $this->data['email'] = array('name' => 'email', 'class' => 'user-name form-control', 'placeholder' => 'User Email', 'id' => 'email', 'type' => 'text', 'value' =>  $user_data->email);
            $this->data['phone'] = array('name' => 'phone', 'class' => 'phone1 form-control', 'placeholder' => 'phone', 'id' => 'phone', 'type' => 'text', 'maxlength' => '11', 'value' =>  $user_data->phone);
            $this->data['mobile'] = array('name' => 'mobile', 'class' => 'phone1 form-control', 'placeholder' => 'Mobile', 'id' => 'mobile', 'type' => 'text', 'maxlength' => '15', 'value' =>  $user_data->mobile_no);
            $this->data['fax'] = array('name' => 'fax', 'class' => 'phone1 form-control', 'placeholder' => 'Fax', 'id' => 'fax', 'type' => 'text', 'maxlength' => '15', 'value' =>  $user_data->fax_no);
            $this->data['password'] = array('name' => 'password', 'class' => 'password form-control', 'placeholder' => 'Password', 'id' => 'password', 'type' => 'password', 'value' => $this->form_validation->set_value('password'));
            $this->data['confirm_password'] = array('name' => 'confirm_password', 'class' => 'password form-control', 'placeholder' => 'Confirm Password', 'id' => 'confirm_password', 'type' => 'password', 'value' => $this->form_validation->set_value('confirm_password'));
            $this->data['company'] = array('name' => 'company', 'class' => 'user form-control', 'placeholder' => 'Company', 'id' => 'company', 'type' => 'text', 'maxlength' => '150', 'value' =>  $user_data->company_name);
            $this->data['address'] = array('name' => 'address', 'class' => 'user form-control', 'placeholder' => 'adresse', 'rows' => '1', 'cols' => '40', 'id' => 'address', 'type' => 'text', 'value' =>  $user_data->address1);
            $this->data['address1'] = array('name' => 'address1', 'class' => 'user form-control', 'placeholder' => '', 'rows' => '1', 'cols' => '40', 'id' => 'address1', 'type' => 'text', 'value' =>  $user_data->address2);
            $this->data['city'] = array('name' => 'city', 'class' => 'user form-control', 'placeholder' => 'City', 'id' => 'city', 'type' => 'text', 'value' =>  $user_data->city);
            $this->data['zipcode'] = array('name' => 'zipcode', 'class' => 'user form-control', 'placeholder' => 'Zipcode', 'id' => 'zipcode', 'type' => 'text', 'value' =>  $user_data->zipcode);
            $this->data['content'] 		= 'users/client-profile';
            
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required|xss_clean');
                    $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required|xss_clean');
                    $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
                    $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone1_label'), 'xss_clean|integer|min_length[8]|max_length[10]');
                    $this->form_validation->set_rules('mobile', $this->lang->line('create_user_validation_phone1_label'), 'xss_clean|integer|min_length[10]|max_length[10]');
                    $this->form_validation->set_rules('address', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
                    $this->form_validation->set_rules('address1', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
                    $this->form_validation->set_rules('city', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
                    $this->form_validation->set_rules('zipcode', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean|min_length[5]');
                    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
                    if ($this->input->post('password') != "") {
                            $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[confirm_password]'); //  
                    }

                    if ($this->form_validation->run() == true) {
                        $username = $this->input->post('first_name') . '' . $this->input->post('last_name');
                        if ($this->input->post('password') == "") {
                                $userData = array(
                                        'username' => strtolower($username),
                                        'civility' => $this->input->post('civility'),
                                        'first_name' => $this->input->post('first_name'),
                                        'last_name' => $this->input->post('last_name'),
                                        'company_name' => $this->input->post('company'),
                                        'email' => $this->input->post('email'),
                                        'phone' => $this->input->post('phone'),
                                        'city' => $this->input->post('city'),
                                        'zipcode' => $this->input->post('zipcode')
                                );
                        }
                        else {
                                $userData = array(
                                        'username' => strtolower($username),
                                        'password' => md5($this->input->post('password')),
                                        'civility' => $this->input->post('civility'),
                                        'first_name' => $this->input->post('first_name'),
                                        'last_name' => $this->input->post('last_name'),
                                        'company_name' => $this->input->post('company'),
                                        'email' => $this->input->post('email'),
                                        'phone' => $this->input->post('phone'),
                                        'city' => $this->input->post('city'),
                                        'zipcode' => $this->input->post('zipcode')
                                );
                        }
                        $additionalData = array('address1' => $this->input->post('address'),
                                'address2' => $this->input->post('address1'),
                                'company_name' => $this->input->post('company'),
                                'fax_no' => $this->input->post('fax')
                        );
                        if (!empty($this->input->post('mobile'))) {
                                $additionalData['mobile_no'] = $this->input->post('mobile');
                        }
                        //  $status = $this->base_model->update_operation($userData, 'users');
                        $status = $this->user_model->updateUser($userData, $additionalData, $user_id);
                        if ($status) {
                                $this->session->set_flashdata('alert', ['message' => "Successfully Updated.",'class' => "alert-success",'type' => "Success"]);
                        }
                        else {
                                $this->session->set_flashdata('alert', ['message' => @$error[0],'class' => "alert-danger",'type' => "Error"]);
                        }
                    }
            }
		
            $this->_render_page('templates/client_template', $this->data);
	}
	 /* Start by Sultan*/

    public function identification() {
        $this->load->library('form_validation');
       $this->data['alert'] = "";
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "Client";
        $this->data['gmaps'] = false;
        $this->data['title'] = 'Authentication';
        $this->data['subtitle'] = 'Authentication';
        $this->data['company'] = $this->company_model->getFirst();
        $this->data['content'] = 'site/authentication';
        $this->data['username'] = array(
            'name' => 'username',
            'id' => 'identity',
            'type' => 'text',
            'class' => 'user',
            'placeholder' => $this->lang->line('login_identity_label'),
            'value' => $this->form_validation->set_value('username'),
        );

        $this->data['passwords'] = array(
            'name' => 'password',
            'id' => 'passwords',
            'type' => 'password',
            'placeholder' => $this->lang->line('password'),
            'class' => 'password'
        );

        $this->data['forgot_form'] = false;
        //added signup crendials
         $this->data['civility'] = array('name' => 'civility', 'class' => 'user form-control', 'placeholder' => 'Civility', 'id' => 'civility', 'type' => 'text', 'value' => $this->form_validation->set_value('civility'));
        $this->data['first_name'] = array('name' => 'first_name', 'class' => 'user form-control', 'placeholder' => 'First name', 'id' => 'first_name', 'type' => 'text', 'value' => $this->form_validation->set_value('first_name'));
        $this->data['last_name'] = array('name' => 'last_name', 'class' => 'user form-control', 'placeholder' => 'Last name', 'id' => 'last_name', 'type' => 'text', 'value' => $this->form_validation->set_value('last_name'));
        $this->data['email'] = array('name' => 'email', 'class' => 'user-name form-control', 'placeholder' => 'User Email', 'id' => 'email', 'type' => 'text', 'value' => $this->form_validation->set_value('email'));
        $this->data['phone'] = array('name' => 'phone', 'class' => 'phone1 form-control', 'placeholder' => 'phone', 'id' => 'phone', 'type' => 'text', 'maxlength' => '11', 'value' => $this->form_validation->set_value('phone'));
        $this->data['mobile'] = array('name' => 'mobile', 'class' => 'phone1 form-control', 'placeholder' => 'Mobile', 'id' => 'mobile', 'type' => 'text', 'maxlength' => '15', 'value' => $this->form_validation->set_value('mobile'));
        $this->data['fax'] = array('name' => 'fax', 'class' => 'phone1 form-control', 'placeholder' => 'Fax', 'id' => 'fax', 'type' => 'text', 'maxlength' => '15', 'value' => $this->form_validation->set_value('fax'));
        $this->data['password'] = array('name' => 'password', 'class' => 'password form-control', 'placeholder' => 'Password', 'id' => 'password', 'type' => 'password', 'value' => $this->form_validation->set_value('password'));
        $this->data['confirm_password'] = array('name' => 'confirm_password', 'class' => 'password form-control', 'placeholder' => 'Confirm Password', 'id' => 'confirm_password', 'type' => 'password', 'value' => $this->form_validation->set_value('confirm_password'));
        $this->data['company'] = array('name' => 'company', 'class' => 'user form-control', 'placeholder' => 'Company', 'id' => 'company', 'type' => 'text', 'maxlength' => '150', 'value' => $this->form_validation->set_value('company'));
        $this->data['address'] = array('name' => 'address', 'class' => 'user form-control', 'placeholder' => 'adresse', 'rows' => '1', 'cols' => '40', 'id' => 'address', 'type' => 'text', 'value' => $this->form_validation->set_value('address'));
        $this->data['address1'] = array('name' => 'address1', 'class' => 'user form-control', 'placeholder' => '', 'rows' => '1', 'cols' => '40', 'id' => 'address1', 'type' => 'text', 'value' => $this->form_validation->set_value('address'));
        $this->data['city'] = array('name' => 'city', 'class' => 'user form-control', 'placeholder' => 'City', 'id' => 'city', 'type' => 'text', 'value' => $this->form_validation->set_value('city'));
        $this->data['zipcode'] = array('name' => 'zipcode', 'class' => 'user form-control', 'placeholder' => 'Zipcode', 'id' => 'zipcode', 'type' => 'text', 'value' => $this->form_validation->set_value('zipcode'));
      

        $this->_render_page('templates/site_template', $this->data);

    }
public function login_validate(){
        $this->data['alert'] = "";
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "Client";
        $this->data['gmaps'] = false;
        $this->data['title'] = 'Login/Register';
        $this->data['subtitle'] = 'Login/Register';
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[2]|valid_email|max_length[50]|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[2]|max_length[50]|xss_clean');

            if ($this->form_validation->run() !== false) {
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                $remember = $this->input->post('remember_me');
                $curl = $this->input->post('curl');

                $user = $this->basic_auth->client_login_checks($username, $password, $remember);

                if ($user != false) {
                    $this->session->set_userdata("user", $user);
                    $this->session->set_flashdata('message', $this->ion_auth->messages());
                    if(empty($curl)){
                        redirect("client/dashboard", 'refresh');
                    }else if($curl=='contact'){
                        redirect("contact", 'refresh');
                    }else if($curl=='popup'){
                        redirect(site_url(), 'refresh');
                    }
                } else{
                   $this->session->set_flashdata('alert', [
                    'message' => " :Invalid email or password.",
                    'class' => "alert-danger",
                    'type' => "Error"
                   ]);
                     
                     redirect('client/identification');
                }
                    
               
            } else {
               
                $this->session->set_flashdata('alert', [
                    'message' => " :Authentication failed.",
                    'class' => "alert-danger",
                    'type' => "Error"
                   ]);
                     
                     redirect('client/identification');
            }
        }else {
              
                $this->session->set_flashdata('alert', [
                    'message' => " :Authentication failed.",
                    'class' => "alert-danger",
                    'type' => "Error"
                   ]);
                     
                     redirect('client/identification');
            }
       
}
public function signup_validate(){
  $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "Client";
        $this->data['gmaps'] = false;
        $this->data['title'] = 'Login/Resiter';
        $this->data['subtitle'] = 'Login/Resiter';
        $this->data['title_link'] = '#';
        $this->load->library('form_validation');
           $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required|xss_clean');
            $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required|xss_clean');
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[vbs_users.email]');
            $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone1_label'), 'xss_clean|integer|min_length[8]|max_length[10]');
            $this->form_validation->set_rules('mobile', $this->lang->line('create_user_validation_phone1_label'), 'required|xss_clean|integer|min_length[10]|max_length[10]');
            $this->form_validation->set_rules('address', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
            $this->form_validation->set_rules('address1', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
            $this->form_validation->set_rules('city', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
            $this->form_validation->set_rules('zipcode', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean|min_length[5]');
            $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']');
 
 

        $this->data['civility'] = array('name' => 'civility', 'class' => 'user form-control', 'placeholder' => 'Civility', 'id' => 'civility', 'type' => 'text', 'value' => $this->input->post('civility'));
        $this->data['first_name'] = array('name' => 'first_name', 'class' => 'user form-control', 'placeholder' => 'First name', 'id' => 'first_name', 'type' => 'text', 'value' => $this->input->post('first_name'));
        $this->data['last_name'] = array('name' => 'last_name', 'class' => 'user form-control', 'placeholder' => 'Last name', 'id' => 'last_name', 'type' => 'text', 'value' => $this->input->post('last_name'));
        $this->data['email'] = array('name' => 'email', 'class' => 'user-name form-control', 'placeholder' => 'User Email', 'id' => 'email', 'type' => 'text', 'value' => $this->input->post('email'));
        $this->data['phone'] = array('name' => 'phone', 'class' => 'phone1 form-control', 'placeholder' => 'phone', 'id' => 'phone', 'type' => 'text', 'maxlength' => '11', 'value' => $this->input->post('phone'));
        $this->data['mobile'] = array('name' => 'mobile', 'class' => 'phone1 form-control', 'placeholder' => 'Mobile', 'id' => 'mobile', 'type' => 'text', 'maxlength' => '15', 'value' => $this->input->post('mobile'));
        $this->data['fax'] = array('name' => 'fax', 'class' => 'phone1 form-control', 'placeholder' => 'Fax', 'id' => 'fax', 'type' => 'text', 'maxlength' => '15', 'value' => $this->input->post('fax'));
        $this->data['password'] = array('name' => 'password', 'class' => 'password form-control', 'placeholder' => 'Password', 'id' => 'password', 'type' => 'password', 'value' => $this->input->post('password'));
        $this->data['confirm_password'] = array('name' => 'confirm_password', 'class' => 'password form-control', 'placeholder' => 'Confirm Password', 'id' => 'confirm_password', 'type' => 'password', 'value' => $this->input->post('confirm_password'));
        $this->data['company'] = array('name' => 'company', 'class' => 'user form-control', 'placeholder' => 'Company', 'id' => 'company', 'type' => 'text', 'maxlength' => '150', 'value' => $this->input->post('company'));
        $this->data['address'] = array('name' => 'address', 'class' => 'user form-control', 'placeholder' => 'adresse', 'rows' => '1', 'cols' => '40', 'id' => 'address', 'type' => 'text', 'value' => $this->input->post('address'));
        $this->data['address1'] = array('name' => 'address1', 'class' => 'user form-control', 'placeholder' => '', 'rows' => '1', 'cols' => '40', 'id' => 'address1', 'type' => 'text', 'value' => $this->input->post('address1'));
        $this->data['city'] = array('name' => 'city', 'class' => 'user form-control', 'placeholder' => 'City', 'id' => 'city', 'type' => 'text', 'value' => $this->input->post('city'));
        $this->data['zipcode'] = array('name' => 'zipcode', 'class' => 'user form-control', 'placeholder' => 'Zipcode', 'id' => 'zipcode', 'type' => 'text', 'value' => $this->input->post('zipcode'));
        $this->data['content'] = 'site/authentication';


        if ($_SERVER['REQUEST_METHOD'] === 'POST'){
            // validate form input
          // |matches[confirm_password] 
            $this->load->library('form_validation');

            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            if ($this->form_validation->run() == true) {
                $username = $this->input->post('first_name') . ' ' . $this->input->post('last_name');
                $email = strtolower($this->input->post('email'));
                $password = $this->input->post('password');
                $user_role = 6;
            $user_group = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('groups') . " WHERE name = 'clients'");
                $group_id = array( $user_group[0]->id);
                $additional_data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'phone' => $this->input->post('phone'),
		    'mobile' => $this->input->post('mobile'),
                    'date_of_registration' => date('Y-m-d'),
                    'address'=>$this->input->post('address'),
                    'address2'=>$this->input->post('address1'),
                    'city'=>$this->input->post('city'),
                    'zipcode'=>$this->input->post('zipcode'),
                    'civility'=>$this->input->post('civility'),
                    'company_name'=>$this->input->post('company')
                );

                if ($this->form_validation->run() == true && $this->ion_auth->register($user_role, $username, $password, $email, $additional_data,$group_id)) {
                    $user_rec = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('users') . " WHERE email = '" . $email . "'");
                    $inputdata['user_id'] = $user_rec[0]->id;
                    $inputdata['address1'] = $this->input->post('address');
                    $inputdata['address2'] = $this->input->post('city');
                    $inputdata['fax_no'] = $this->input->post('fax');
                    $inputdata['company_name'] = $this->input->post('company');
                    $table_name = "users_details";

                    $status = $this->base_model->insert_operation($inputdata, $table_name);

                    // $user = $this->basic_auth->login($username, $password);

                    if ($status) {

                        // $this->session->set_flashdata('message', $this->ion_auth->messages());
                        // check to see if we are creating the user
                        // redirect them back to the admin page
                        $this->session->set_flashdata('alert', [
                            'message' =>" :Account created",
                            'class' => "alert-success",
                            'type' => "Successfully"
                           ]);
                        redirect('client/identification', 'refresh');
                    } else {
                       
                         $this->session->set_flashdata('alert', [
                            'message' => " :Something went wrong",
                            'class' => "alert-danger",
                            'type' => "Error"
                           ]);
                        $this->_render_page('templates/site_template', $this->data);
                    }
                } else {
                      $this->session->set_flashdata('alert', [
                            'message' => " :Validation Faild",
                            'class' => "alert-danger",
                            'type' => "Error"
                           ]);
                    $this->_render_page('templates/site_template', $this->data);
                }
            }
            else {
            $this->_render_page('templates/site_template', $this->data);
           }
        }
         else {
            $this->_render_page('templates/site_template', $this->data);
        }
    
 }
}
