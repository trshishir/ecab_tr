<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dispatch extends MY_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('general');
        $this->load->helper('language');
        $this->load->helper('validate');
        if (!$this->basic_auth->is_login())
            redirect("admin", 'refresh');
        else
            $this->data['user'] = $this->basic_auth->user();
        $this->load->model('dispatch_model');
        $this->load->model('clients_model');
        $this->load->model('drivers_model');
        $this->load->model('request_model');
        $this->load->model('cms_model');
        $this->load->model('base_model');
        $this->load->model('jobs_model');
        $this->load->model('support_model');
        $this->load->model('calls_model');
        $this->load->model('notes_model');
        $this->load->model('notifications_model');
        $this->load->model('userx_model');
        $this->load->model('sitemodel');
        $this->data['configuration'] = get_configuration();
    }

    public function index(){

        $this->data['user'] = $this->basic_auth->user();
        $this->data['css_type']     = array("form","booking_datatable","datatable");
        $this->data['active_class'] = "Dispatch";
        $this->data['gmaps']        = false;
        $this->data['title']        = 'Dispatch Planning';
        $this->data['title_link']   = base_url('admin/dispatch_planning');
        $this->data['content']      = 'admin/dispatch/index';

        $driversdata=$this->dispatch_model->getAllData('vbs_driverprofile');

        $this->data['display_type'] = "drivers";
        $this->data['drivers'] = $this->getDrivers($driversdata);

        $this->data['cars']        = $this->getDrivers($driversdata);
        $this->data['clients']     = $this->getDrivers($driversdata);
        $this->data['mod_cars']    = $this->handleDriver($this->data['cars']);
        $this->data['rides']       = '';

        $this->data['mod_drivers'] = $this->handleDriver($this->data['drivers']);




        $this->_render_page('templates/admin_template', $this->data);
    }

    function getDrivers($driversdata){

        foreach ($driversdata as $item) {
            $result[] = array(
                'driver' => $this->dispatch_model->getsinglerecord('vbs_drivercivilite',['id'=>$item->civilite])->civilite.' '.$item->prenom.' '.$item->nom,
                'image' => $item->driverImg
            );
        }
        return $result;
    }
    function handleDriver($drivers)
    {
        foreach ($drivers as $item) {
            $result[] = array(
                'key' => $item['driver'],
                'label' => "<img src='". base_url() .'/uploads/drivers/'. $item['image']. "' class='driver-icn' /> <span class='driver-name'>" . $item['driver'] . "</span>"
            );
        }
        return $result;
    }

    function dispatch_statistics()
    {
        if ($this->session->userdata('login_type')  != 'admin'){redirect(base_url() . 'login', 'refresh');}
        $page_data['content']  = 'admin/dispatch/dispatch_statistics';
        $page_data['user_name']  = get_phrase('Mr. ' . $this->session->userdata('login_as'));
        $page_data['page_title'] = "Dispatch Statistics"; //get_phrase($this->session->userdata('login_as').'_dashboard');
        $this->load->view('admin', $page_data);
    }    /** Accounting Statistics **/

    //driver_name, client_name
    public function dispatch_timesheet()
    {
        $this->load->library('pagination');
        $this->load->library('table');
//        if ($this->session->userdata('login_type')  != 'admin'){redirect(base_url() . 'login', 'refresh');}
        //civility, first/last name, email, phone, hire_date, job, address, city, zip_code, status.
        if (empty($_POST) || $_POST['type'] == "search") {
            $page_data['content']      = 'admin/dispatch/dispatch_timesheet';
            $page_data['user_name']    = 'Mr. ' . $this->session->userdata('login_as');
            $page_data['page_title']   = "Timesheet"; //get_phrase($this->session->userdata('login_as').'_dashboard');
            $page_data['board1']       = $this->db->get('vbs_first_board')->result_array();
            //Must be last to avoid query interruption
            if ($_POST['type'] == "search") {
                $this->db->select('*');
                $search_word = $this->input->post('search-word');
                $from_period = str_replace("/", "-", $this->input->post('from_period'));
                $to_period   = str_replace("/", "-", $this->input->post('to_period'));
                $status      = $this->input->post('status');
                $nature      = $this->input->post('nature');
                $ridetype       = $this->input->post('ridetype
                    ');

                if ($search_word != "" && $search_word != "Search keyword") {
                    $this->db->like('client', $search_word)->or_like('driver', $search_word)->or_like('origin', $search_word)->or_like('destination', $search_word);
                }
                $this->db->where('start_time >=', $from_period);
                $this->db->where('stop_time <=', $to_period);


                if ($status != "All status") {
                    $this->db->where('status', $status);
                }
                if ($nature != "All nature") {
                    $this->db->where('nature', $nature);
                }
                if ($ridetype != "All type") {
                    $this->db->where('ridetype', $ridetype);
                }/*                $page_data['searched_word'] = $search_word;
                $page_data['searched_from'] = $from_period;
                $page_data['searched_to'] = $to_period;
                $page_data['searched_status'] = $status;
                $page_data['searched_nature'] = $nature;
                $page_data['searched_ridetype'] = $ridetype;*/
            }
            $page_data['drivers'] = [];
            $page_data['rides'] = $this->db->get('vbs_rides')->result_array();
            $this->_render_page('templates/admin_template', $page_data);
            //die(var_dump($this->_render_page('templates/admin_template', $page_data)));
        }else {
            $type = $this->input->post('type');
            $data = array(
                "client" => $this->input->post('client'),
                "driver" => $this->input->post('driver'),
                "car" => $this->input->post('car'),
                "start_time" => str_replace("/", "-", $this->input->post('start_time')),
                "stop_time" => str_replace("/", "-", $this->input->post('stop_time')),
                "nature" => $this->input->post('nature'),
                "ridetype" => $this->input->post('ridetype'),
                "service_name" => $this->input->post('service_name'),
                "person_count" => $this->input->post('person_count'),
                "wheelchair" => $this->input->post('wheelchair'),
                "origin" => $this->input->post('origin'),
                "destination" => $this->input->post('destination'),
                "origin_cord" => $this->input->post('origin_cord'),
                "destination_cord" => $this->input->post('destination_cord'),
                "status" => $this->input->post('status')

            );

            if ($type == "add") {
                $this->db->insert('vbs_rides', $data);
            } else if ($type == "delete") {
                $this->db->where('id', $this->input->post('id'))->delete('vbs_rides');
            } else if ($type == "edit") {
                $this->db->where('id', $this->input->post('id'))->update('vbs_rides', $data);
            }
            redirect('admin/dispatch_timesheet');
        }
    }    //driver_name, client_name'

    function dispatch_reports()
    {
        $this->load->library('pagination');
        $this->load->library('table');

//        if ($this->session->userdata('login_type')  != 'admin'){redirect(base_url() . 'login', 'refresh');}
        //civility, first/last name, email, phone, hire_date, job, address, city, zip_code, status.
        if (empty($_POST) || $_POST['type'] == "search") {
            $page_data['content']    = 'admin/dispatch/dispatch_reports';

            $page_data['user_name']    = 'Mr. ' . $this->session->userdata('login_as');
            $page_data['page_title']   = "Reports"; //get_phrase($this->session->userdata('login_as').'_dashboard');
            $page_data['board1']       = $this->db->get('first_board')->result_array();
            //Must be last to avoid query interruption
            if ($_POST['type'] == "search") {
                $this->db->select('*');
                $search_word = $this->input->post('search-word');
                $from_period = str_replace("/", "-", $this->input->post('from_period'));
                $to_period   = str_replace("/", "-", $this->input->post('to_period'));
                $status      = $this->input->post('status');
                $nature      = $this->input->post('nature');
                $ridetype       = $this->input->post('ridetype
                    ');

                if ($search_word != "" && $search_word != "Search keyword") {
                    $this->db->like('client', $search_word)->or_like('driver', $search_word)->or_like('origin', $search_word)->or_like('destination', $search_word);
                }
                $this->db->where('start_time >=', $from_period);
                $this->db->where('stop_time <=', $to_period);


                if ($status != "All status") {
                    $this->db->where('status', $status);
                }
                if ($nature != "All nature") {
                    $this->db->where('nature', $nature);
                }
                if ($ridetype != "All type") {
                    $this->db->where('ridetype', $ridetype);
                }/*                $page_data['searched_word'] = $search_word;
                $page_data['searched_from'] = $from_period;
                $page_data['searched_to'] = $to_period;
                $page_data['searched_status'] = $status;
                $page_data['searched_nature'] = $nature;
                $page_data['searched_ridetype'] = $ridetype;*/
            };
            $page_data['content']      = "admin/dispatch/dispatch_reports";
            $page_data['rides'] = $this->db->get('vbs_rides')->result_array();
            $this->_render_page('templates/admin_template', $page_data);
        }
        else {

            $type = $this->input->post('type');
            $data = array(
                "client" => $this->input->post('client'),
                "driver" => $this->input->post('driver'),
                "car" => $this->input->post('car'),
                "start_time" => str_replace("/", "-", $this->input->post('start_time')),
                "stop_time" => str_replace("/", "-", $this->input->post('stop_time')),
                "nature" => $this->input->post('nature'),
                "ridetype" => $this->input->post('ridetype'),
                "service_name" => $this->input->post('service_name'),
                "person_count" => $this->input->post('person_count'),
                "wheelchair" => $this->input->post('wheelchair'),
                "origin" => $this->input->post('origin'),
                "destination" => $this->input->post('destination'),
                "origin_cord" => $this->input->post('origin_cord'),
                "destination_cord" => $this->input->post('destination_cord'),
                "status" => $this->input->post('status')

            );

            if ($type == "add") {
                $this->db->insert('vbs_rides', $data);
            } else if ($type == "delete") {
                $this->db->where('id', $this->input->post('id'))->delete('vbs_rides');
            } else if ($type == "edit") {
                $this->db->where('id', $this->input->post('id'))->update('vbs_rides', $data);
            }
            redirect('admin/dispatch_reports');
        }
    }

    /** Dispatch Map Views **/
    function dispatch_map_views()
    {
        if ($this->session->userdata('login_type')  != 'admin'){redirect(base_url() . 'login', 'refresh');}
        $page_data['content']  = 'admin/dispatch/dispatch_map_views';
        $page_data['rides']      = $this->db->get('vbs_rides')->result_array();
        $page_data['drivers']      = $this->db->where('job', 'driver')->get('handi_employees')->result_array();
        $page_data['clients']      = $this->db->get('handi_clients')->result_array();
        $page_data['cars']      = $this->db->get('handi_cars')->result_array();
        $page_data['user_name']  = get_phrase('Mr. ' . $this->session->userdata('login_as'));
        $page_data['page_title'] = "Dispatch Map Views"; //get_phrase($this->session->userdata('login_as').'_dashboard');
        $page_data['board1']     = $this->db->get('first_board')->result_array();
        $this->_render_page('templates/admin_template', $page_data);
    }

    /** Dispatch Clients Planning **/
    function dispatch_clients_planning()
    {
        if ($this->session->userdata('login_type')  != 'admin'){redirect(base_url() . 'login', 'refresh');}
        $page_data['content']  = 'admin/dispatch/dispatch_clients_planning';
        $page_data['user_name']  = get_phrase('Mr. ' . $this->session->userdata('login_as'));
        $page_data['page_title'] = "Dispatch Clients Planning"; //get_phrase($this->session->userdata('login_as').'_dashboard');
        $page_data['board1']     = $this->db->get('first_board')->result_array();
        $this->_render_page('templates/admin_template', $page_data);
    }
    /** Dispatch Cars Availability **/
    function dispatch_cars_availability()
    {
        if ($this->session->userdata('login_type')  != 'admin'){redirect(base_url() . 'login', 'refresh');}
        $page_data['content']  = 'admin/dispatch/dispatch_cars_availability';
        $page_data['user_name']  = get_phrase('Mr. ' . $this->session->userdata('login_as'));
        $page_data['page_title'] = "Dispatch Cars Availability"; //get_phrase($this->session->userdata('login_as').'_dashboard');
        $page_data['board1']     = $this->db->get('first_board')->result_array();
        $this->_render_page('templates/admin_template', $page_data);
    }
    /** Dispatch Drivers Availability **/
    function dispatch_drivers_availability()
    {
        if ($this->session->userdata('login_type')  != 'admin'){redirect(base_url() . 'login', 'refresh');}
        $page_data['content']  = 'admin/dispatch/dispatch_drivers_availability';
        $page_data['user_name']  = get_phrase('Mr. ' . $this->session->userdata('login_as'));
        $page_data['page_title'] = "Dispatch Drivers Availability"; //get_phrase($this->session->userdata('login_as').'_dashboard');
        $page_data['board1']     = $this->db->get('first_board')->result_array();
        $this->_render_page('templates/admin_template', $page_data);
    }
    /** Dispatch Planning **/
    function dispatch_planning()
    {
        if ($this->session->userdata('login_type')  != 'admin'){redirect(base_url() . 'login', 'refresh');}
        $page_data['display_type'] = "drivers";
        $rides                     = $this->db->get('vbs_rides')->result_array();
        if (!empty($_POST)) {
            $cat                       = $this->input->post('cat');
            $page_data['display_type'] = $cat;
            $type                      = $this->input->post('type');
            if ($type == "search") {
                $word = $this->input->post('search_word');
                if ($word != "") {
                    $rides = $this->db->like('driver', $word)->or_like('car', $word)->get('vbs_rides')->result_array();
                }
                $page_data['searched_word'] = $word;
            } else {
                $driver = $this->input->post('select_driver');
                $car    = $this->input->post('select_car');
                $client = $this->input->post('select_client');
                $status = $this->input->post('select_status');

                $page_data['searched_car'] = $car;
                $page_data['searched_driver'] = $driver;
                $page_data['searched_client'] = $client;
                $page_data['searched_status'] = $status;
                $page_data['display_type'] = $cat;

                if ($cat == "cars") {
                    if ($car != "All cars") {
                        $this->db->where('car', $car);
                    }
                } else {
                    if ($driver != "All drivers") {
                        $this->db->where('driver', $driver);
                    }
                }
                if ($client != "All clients") {
                    $this->db->where('client', $client);
                }
                if ($status != "All status") {
                    $this->db->where('status', $status);
                }
                $rides = $this->db->get('vbs_rides')->result_array();
            }
        }
        $page_data['drivers']     = $this->getDrivers($rides);
        $page_data['cars']        = $this->getCars($rides);
        $page_data['clients']     = $this->getClients($rides);
        $page_data['mod_drivers'] = $this->handleDriver($page_data['drivers']);
        $page_data['mod_cars']    = $this->handleCar($page_data['cars']);
        $page_data['rides']       = $this->handleRide($rides);
        $page_data['content']     = 'admin/dispatch/dispatch_planning';
        //$page_data['drivers'] = $this->db->where('job', 'driver')->get('handi_employees')->result_array();
        $page_data['user_name']  = get_phrase('Mr. ' . $this->session->userdata('login_as'));
        $page_data['page_title'] = "Dispatch Planning"; //get_phrase($this->session->userdata('login_as').'_dashboard');
        $page_data['board1']     = $this->db->get('first_board')->result_array();
        $this->_render_page('templates/admin_template', $page_data);
        $this->_render_page('templates/admin_template', $page_data);
    }
    /** Dispatch Time Reports **/
    function dispatch_time_reports()
    {
        if ($this->session->userdata('login_type')  != 'admin'){redirect(base_url() . 'login', 'refresh');}
        $page_data['content']    = 'admin/dispatch/dispatch_time_reports';
        $page_data['user_name']  = get_phrase('Mr. ' . $this->session->userdata('login_as'));
        $page_data['page_title'] = "Dispatch Time Reports"; //get_phrase($this->session->userdata('login_as').'_dashboard');
        $page_data['board1']     = $this->db->get('first_board')->result_array();
        $this->_render_page('templates/admin_template', $page_data);
    }
    /** Dispatch Rides Reports **/
    function dispatch_rides_reports()
    {
        if ($this->session->userdata('login_type')  != 'admin'){redirect(base_url() . 'login', 'refresh');}
        $page_data['content']  = 'admin/dispatch/dispatch_rides_reports';
        $page_data['user_name']  = get_phrase('Mr. ' . $this->session->userdata('login_as'));
        $page_data['page_title'] = "Dispatch Rides Reports"; //get_phrase($this->session->userdata('login_as').'_dashboard');
        $page_data['board1']     = $this->db->get('first_board')->result_array();
        $this->_render_page('templates/admin_template', $page_data);
    }
    /** Dispatch Support **/
    function dispatch_support()
    {
        if ($this->session->userdata('login_type')  != 'admin'){redirect(base_url() . 'login', 'refresh');}
        $page_data['content']  = 'admin/dispatch/dispatch_support';
        $page_data['user_name']  = get_phrase('Mr. ' . $this->session->userdata('login_as'));
        $page_data['page_title'] = "Dispatch Support"; //get_phrase($this->session->userdata('login_as').'_dashboard');
        $page_data['board1']     = $this->db->get('first_board')->result_array();
        $this->_render_page('templates/admin_template', $page_data);
    }

    function gross_bills()
    {
        $this->load->library('pagination');
        $this->load->library('table');
//        if ($this->session->userdata('login_type')  != 'admin'){redirect(base_url() . 'login', 'refresh');}
        $page_data['content']    = 'admin/dispatch/gross_bills';
        $page_data['account_statut'] = $this->sitemodel->getConfigAccounting('account_statut');
        $page_data['account_modereglement'] = $this->sitemodel->getConfigAccounting('account_modereglement');
        $page_data['account_periode'] = $this->sitemodel->getConfigAccounting('account_periode');
        $page_data['servicecategory'] = $this->sitemodel->getConfigAccounting('servicecategory');
        $page_data['account_produit'] = $this->sitemodel->getAccountingProduit();
        $page_data['user_name']    = 'Mr. ' . $this->session->userdata('login_as');
        $page_data['title']   = "Gross Bills";
        $this->_render_page('templates/admin_template', $page_data);
    }

    function factoring()
    {
        $this->load->library('pagination');
        $this->load->library('table');
//        if ($this->session->userdata('login_type')  != 'admin'){redirect(base_url() . 'login', 'refresh');}
        $page_data['content']    = 'admin/dispatch/factoring';
        $page_data['account_statut'] = $this->sitemodel->getConfigAccounting('account_statut');
        $page_data['account_modereglement'] = $this->sitemodel->getConfigAccounting('account_modereglement');
        $page_data['account_periode'] = $this->sitemodel->getConfigAccounting('account_periode');
        $page_data['servicecategory'] = $this->sitemodel->getConfigAccounting('servicecategory');
        $page_data['account_produit'] = $this->sitemodel->getAccountingProduit();
        $page_data['user_name']    = get_phrase('Mr. ' . $this->session->userdata('login_as'));
        $page_data['page_title']   = "Factoring";
        $this->_render_page('templates/admin_template', $page_data);
    }

}