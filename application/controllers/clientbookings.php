<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ClientBookings extends MY_Controller
{
	function __construct()
	{
		 parent::__construct();
        
  if (!(bool) $this->basic_auth->is_login() || !$this->basic_auth->is_client()) {
            redirect("client/login");
        }
        else {
            $this->data['user'] = $this->basic_auth->user();
        }
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('general');
        $this->load->helper('validate');
        $this->load->helper('language');
        $this->load->library('session');
        $this->load->model('user_model');
        $this->load->model('invoice_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');
        $this->load->model('support_model');
        $this->load->model('calls_model');
        $this->load->model('notes_model');
        $this->load->model('notifications_model');
        $this->load->model('cars_model');
        $this->load->model('company_model');
        $this->load->model('popups_model');
        $this->load->model('smtp_model');
        $this->load->model('userx_model');
        $this->load->model('bookings_model');

        $this->data['title'] = "Bookings";
        $this->data['css_type'] = array("form", "datatable");
        $this->data['user'] = $this->session->userdata('user');

        $this->data['configuration'] = get_configuration();

       
      

      

		
     
	}
	public function index(){
 

 		 $this->data['css'] = array('form');
        $this->data['bread_crumb'] = true;
        $this->data['heading'] = 'Client Dashboard';
        $this->data['content'] = 'users/bookings/index';
        $this->data['forgot_form'] = false;
		//
       
        $this->data['active_class'] = "bookings";
        $this->data['gmaps'] = false;
        $this->data['title_link'] = base_url('client/bookings');
      

   
       $data = [];
      
        $this->data['total_clients'] = $this->user_model->getClientsTotal();
        
		if($this->session->userdata('user')->email == 'client@ecab.app'){
			$data['request'] = $this->request_model->getAll();
		}else{
			$data['request'] = $this->request_model->getAll(array('user_id'=>$this->session->userdata('user')->id));
		}
	$clientid= $this->session->userdata('user')->id;


		 
		 $this->data['bookings'] = $this->bookings_model->getbookingallrecordsbyclientid($clientid);
		 $this->_render_page('templates/client_templatev2', $this->data);
	}
	
		
}
