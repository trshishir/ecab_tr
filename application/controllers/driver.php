<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Driver extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('general');
        $this->load->helper('language');
        $this->load->library('session');
        $this->load->model('user_model');
        $this->load->model('invoice_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');
        $this->load->model('support_model');
        $this->load->model('calls_model');
        $this->load->model('notes_model');
        $this->load->model('notifications_model');
        $this->load->model('cars_model');
        $this->load->model('company_model');

        $this->data['configuration'] = get_configuration();
        $this->data['title'] = "Driver Dashboard";
        $this->data['css_type'] = array("form", "datatable");
        $this->data['user'] = $this->session->userdata('user');
    }

    /*
      public function index()
      {
      $this->data['css_type'] = array("form", "datatable");
      $this->data['active_class'] = "cars";
      $this->data['gmaps'] = false;
      $this->data['title'] = $this->lang->line("cars");
      $this->data['title_link'] = base_url('admin/cars');
      $this->data['content'] = 'admin/cars/index';

      $this->load->model('calls_model');
      $this->load->model('request_model');
      $this->load->model('jobs_model');

      $data = [];
      $data['request'] = $this->request_model->getAll();
      $data['jobs'] = $this->jobs_model->getAll();
      $data['calls'] = $this->calls_model->getAll();

      $this->data['data'] = $this->request_model->getAll();
      $this->_render_page('templates/admin_template', $this->data);
      } */

    public function configurations() {
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "Config Drivers";
        $this->data['gmaps'] = false;
        $this->data['title'] = $this->lang->line("configurations");
        $this->data['subtitle'] = 'Config Drivers';
        $this->data['title_link'] = '#';
        $this->data['content'] = 'admin/drivers/configurations';
        $data = [];
        $this->data['data'] = $this->notifications_model->getAll();
        $this->_render_page('templates/admin_template', $this->data);
    }

    /* Added by Saravanan.R
      STARTS
     */

    public function index($param=null) {

        if($param==null) {
            $rec =$this->cms_model->getCmsList_by_status('news',1,'driver');
            $this->data['content'] = 'site/news';
            $this->data['news'] = $rec;
        } else{
            $slug = explode(".",$param)[0];
            $rec =$this->cms_model->getCmsListing_id(array('post_type' =>'news', 'link_url'=>$slug), $this->lang->lang());
            $this->data['sub_heading']          = $rec->name;
            $this->data['details']              = $rec;
            $this->data['content']              = 'site/news_details';
            $this->data['author'] = $this->user_model->get(array('user.id'=>$rec->created_by));
        }

        $this->data['title'] = 'Driver';
        $this->data['subtitle'] = 'Driver';
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "driver";
        $this->data['gmaps'] = false;

//        var_dump($this->data['slider']);exit;
        $this->data['title_link'] = '#';
        $this->_render_page('templates/site_template', $this->data);
    }

    public function signup() {
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "Driver";
        $this->data['gmaps'] = false;
        $this->data['title'] = 'Driver Signup';
        $this->data['subtitle'] = 'Driver';
        $this->data['title_link'] = '#';
        $this->load->library('form_validation');
        $this->data['company_name'] = $this->company_model->getCompanyName();

        $this->data['civility'] = array('name' => 'civility', 'class' => 'form-control', 'placeholder' => 'Civility', 'id' => 'civility', 'type' => 'text', 'value' => $this->form_validation->set_value('civility'));
        $this->data['first_name'] = array('name' => 'first_name', 'class' => 'form-control', 'placeholder' => 'First name', 'id' => 'first_name', 'type' => 'text', 'value' => $this->form_validation->set_value('first_name'));
        $this->data['last_name'] = array('name' => 'last_name', 'class' => 'form-control', 'placeholder' => 'Last name', 'id' => 'last_name', 'type' => 'text', 'value' => $this->form_validation->set_value('last_name'));
        $this->data['email'] = array('name' => 'email', 'class' => 'form-control', 'placeholder' => 'User Email', 'id' => 'email', 'type' => 'text', 'value' => $this->form_validation->set_value('email'));
        $this->data['phone'] = array('name' => 'phone', 'class' => 'form-control', 'placeholder' => 'phone', 'id' => 'phone', 'type' => 'text', 'maxlength' => '11', 'value' => $this->form_validation->set_value('phone'));
        $this->data['mobile'] = array('name' => 'mobile', 'class' => 'form-control', 'placeholder' => 'Mobile', 'id' => 'mobile', 'type' => 'text', 'maxlength' => '15', 'value' => $this->form_validation->set_value('mobile'));
        $this->data['fax'] = array('name' => 'fax', 'class' => 'form-control', 'placeholder' => 'Fax', 'id' => 'fax', 'type' => 'text', 'maxlength' => '15', 'value' => $this->form_validation->set_value('fax'));
        $this->data['password'] = array('name' => 'password', 'class' => 'form-control', 'placeholder' => 'Password', 'id' => 'password', 'type' => 'password', 'value' => $this->form_validation->set_value('password'));
        $this->data['confirm_password'] = array('name' => 'confirm_password', 'class' => 'form-control', 'placeholder' => 'Confirm Password', 'id' => 'confirm_password', 'type' => 'password', 'value' => $this->form_validation->set_value('confirm_password'));
        $this->data['company'] = array('name' => 'company', 'class' => 'form-control', 'placeholder' => 'Company', 'id' => 'company', 'type' => 'text', 'maxlength' => '150', 'value' => $this->form_validation->set_value('company'));
        $this->data['address'] = array('name' => 'address', 'class' => 'form-control', 'placeholder' => 'adresse', 'rows' => '1', 'cols' => '40', 'id' => 'address', 'type' => 'text', 'value' => $this->form_validation->set_value('address'));
        $this->data['address1'] = array('name' => 'address1', 'class' => 'form-control', 'placeholder' => '', 'rows' => '1', 'cols' => '40', 'id' => 'address1', 'type' => 'text', 'value' => $this->form_validation->set_value('address'));
        $this->data['city'] = array('name' => 'city', 'class' => 'form-control', 'placeholder' => 'City', 'id' => 'city', 'type' => 'text', 'value' => $this->form_validation->set_value('city'));
        $this->data['zipcode'] = array('name' => 'zipcode', 'class' => 'form-control', 'placeholder' => 'Zipcode', 'id' => 'zipcode', 'type' => 'text', 'value' => $this->form_validation->set_value('zipcode'));
        $this->data['content'] = 'site/driver_signup';
        $driver_agreement = $this->cms_model->getCMSPage('legals','Driver Agreement');
        $this->data['driver_agreement'] = $driver_agreement->description;
        
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // validate form input
            $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required|xss_clean');
            $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required|xss_clean');
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[vbs_users.email]');
            $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone1_label'), 'xss_clean|integer|min_length[8]|max_length[10]');
            $this->form_validation->set_rules('mobile', $this->lang->line('create_user_validation_phone1_label'), 'required|xss_clean|integer|min_length[10]|max_length[10]');
            $this->form_validation->set_rules('address', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
            $this->form_validation->set_rules('address1', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
            $this->form_validation->set_rules('city', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
            $this->form_validation->set_rules('zipcode', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean|min_length[5]');
            $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']'); // |matches[confirm_password] 
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            if ($this->form_validation->run() == true) {
                $username = $this->input->post('first_name') . ' ' . $this->input->post('last_name');
                $email = strtolower($this->input->post('email'));
                $password = $this->input->post('password');
                $user_role = 7;
                $user_group = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('groups') . " WHERE name = 'drivers'");
                $group_id = array( $user_group[0]->id);
                $additional_data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'phone' => $this->input->post('phone'),
                    'city' => $this->input->post('city'),
                    'zipcode' => $this->input->post('zipcode'),
                    'address' => $this->input->post('address'),
                    'date_of_registration' => date('Y-m-d')
                );

                if ($this->form_validation->run() == true && $this->ion_auth->register($user_role, $username, $password, $email, $additional_data,$group_id)) {
                    $user_rec = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('users') . " WHERE email = '" . $email . "'");
                    $inputdata['user_id'] = $user_rec[0]->id;
                    $inputdata['address1'] = $this->input->post('address');
                    $inputdata['address2'] = $this->input->post('address1') . ' ' . $this->input->post('city');
                    $inputdata['fax_no'] = $this->input->post('fax');
                    $inputdata['company_name'] = $this->input->post('company');
                    $table_name = "users_details";

                    $status = $this->base_model->insert_operation($inputdata, $table_name);

                    // $user = $this->basic_auth->login($username, $password);

                    if ($status) {
                        // $this->session->set_flashdata('message', $this->ion_auth->messages());
                        // check to see if we are creating the user
                        // redirect them back to the admin page
                        $this->session->set_flashdata('messages', $this->ion_auth->messages());
                        redirect('driver/login', 'refresh');
                    } else {
                        $this->session->set_flashdata('error', $this->ion_auth->messages());
                        $this->_render_page('templates/site_template', $this->data);
                    }
                } else {
                    $this->session->set_flashdata('error', $this->ion_auth->messages());
                    $this->_render_page('templates/site_template', $this->data);
                }
            }
        } else {
            $this->_render_page('templates/site_template', $this->data);
        }
    }

    public function login() {
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "Drivers";
        $this->data['gmaps'] = false;
        $this->data['title'] = 'Driver Login';
        $this->data['subtitle'] = 'Driver Login';

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[2]|valid_email|max_length[50]|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[2]|max_length[50]|xss_clean');

            if ($this->form_validation->run() !== false) {
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                $remember = $this->input->post('remember_me');
				$curl = $this->input->post('curl');

                $user = $this->basic_auth->driver_login($username, $password, $remember);

                if ($user != false) {
                    $this->session->set_userdata("user", $user);
                    $this->session->set_flashdata('message', $this->ion_auth->messages());
					if(empty($curl)){
						redirect("driver/dashboard", 'refresh');
					}else if($curl=='contact'){
						redirect("contact", 'refresh');
					}else if($curl=='popup'){
						redirect(site_url(), 'refresh');
					}
                } else
                    $this->data['alert'] = [
                        'message' => "Authentication failed.",
                        'class' => "alert-danger",
                        'type' => "Error"
                    ];
            } else {
                $this->data['alert'] = [
                    'message' => "Invalid email or password.",
                    'class' => "alert-danger",
                    'type' => "Error"
                ];
            }
        }
        
        $this->data['company'] = $this->company_model->getFirst();
        $this->data['content'] = 'site/driver_login';
        $this->data['username'] = array(
            'name' => 'username',
            'id' => 'identity',
            'type' => 'text',
            'class' => 'form-control',
            'placeholder' => $this->lang->line('login_identity_label'),
            'value' => $this->form_validation->set_value('username'),
        );

        $this->data['password'] = array(
            'name' => 'password',
            'id' => 'password',
            'type' => 'password',
            'placeholder' => $this->lang->line('password'),
            'class' => 'form-control'
        );

        $this->data['forgot_form'] = false;

        $this->_render_page('templates/site_template', $this->data);
    }
	
	public function popup_login() {
        $username = $_GET['username'];
		$password = $_GET['password'];
		$remember = $_GET['remember_me'];

		$user = $this->basic_auth->driver_login($username, $password, $remember);

		if ($user != false) {
			$this->session->set_userdata("user", $user);
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			echo json_encode($user);
		} else{
			echo json_encode([
                    'message' => "Invalid email or password.",
                    'type' => "Error"
                ]);
		}
    }

    public function logout() {
        $this->basic_auth->logout();
        // redirect them to the login page
        // $this->session->set_flashdata('message', $this->ion_auth->messages());
        $this->session->userdata = array();
        redirect('driver/login', 'refresh');
    }

    public function dashboard() {
        if (!$this->basic_auth->is_login() || !$this->basic_auth->is_driver()) {
            redirect("driver/login", 'refresh');
        } else {
            $this->data['user'] = $this->basic_auth->user();
        }

        $this->data['css'] = array('form');
        $this->data['bread_crumb'] = true;
        $this->data['heading'] = 'Driver Dashboard';
        $this->data['content'] = 'driver/dashboard';
        $this->data['forgot_form'] = false;
        $data = [];

        $this->data['total_clients'] = $this->user_model->getClientsTotal();
        
		if($this->session->userdata('user')->email == 'driver@ecab.app'){
			$data['request'] = $this->request_model->getAll();
		}else{
			$data['request'] = $this->request_model->getAll(array('user_id'=>$this->session->userdata('user')->id));
		}

        
//        for bar chart Qoute Request
        $record = $this->request_model->QouteChartCount();

        foreach ($record as $row1) {
            $data1['label'][] = $row1->month_name;
            $data1['data'][] = (int) $row1->count;
        }
        $this->data['chart_data'] = json_encode($data1);


        //        for line chart Qoute request
        $QouteLine = $this->request_model->QouteLineChart();
//        print_r($QouteLine);
        foreach ($QouteLine as $line) {
            $data4['day'][] = $line->y;
            $data4['count'][] = $line->a;
        }
        $this->data['qoute_line_data'] = json_encode($data4);


        foreach ($data as $key => $d) {
            if ($d != false) {
                foreach ($d as $i) {
                    if (!empty($i->status))
                        $this->data[$key][strtolower($i->status)] = isset($this->data[$key][strtolower($i->status)]) ? $this->data[$key][strtolower($i->status)] + 1 : 1;
                }
            }
        }
        $this->_render_page('templates/driver_template', $this->data);
    }

    /* ENDS by Saravanan.R */

    public function addstatusprocess() {
        extract($_GET);
        //print_r($_GET);
        //die();
        $values = [
            'status' => $statut,
            'is_delete' => 1,
            'create_date' => date("Y-m-d H:i:s")
        ];


        // print_r($values);

        $insert = $this->cars_model->insertData('vbs_driverStatus', $values, 'status', $statut);

        if ($insert == 1) {
            echo "success";
        }
    }

    public function addciviliteprocess() {
        extract($_GET);
        //print_r($_GET);
        //die();
        $values = [
            'civilite' => $civilite,
            'is_delete' => 1,
            'create_date' => date("Y-m-d H:i:s")
        ];


        // print_r($values);

        $insert = $this->cars_model->insertData('vbs_driverCivilite', $values, 'civilite', $civilite);

        if ($insert == 1) {
            echo "success";
        }
    }

    public function addpostprocess() {
        extract($_GET);
        //print_r($_GET);
        //die();
        $values = [
            'post' => $post,
            'is_delete' => 1,
            'create_date' => date("Y-m-d H:i:s")
        ];


        // print_r($values);

        $insert = $this->cars_model->insertData('vbs_driverPost', $values, 'post', $post);

        if ($insert == 1) {
            echo "success";
        }
    }

    public function addpatternprocess() {
        extract($_GET);
        //print_r($_GET);
        //die();
        $values = [
            'pattern' => $pattern,
            'is_delete' => 1,
            'create_date' => date("Y-m-d H:i:s")
        ];


        // print_r($values);

        $insert = $this->cars_model->insertData('vbs_driverPattern', $values, 'pattern', $pattern);

        if ($insert == 1) {
            echo "success";
        }
    }

    public function addageprocess() {
        extract($_GET);
        //print_r($_GET);
        //die();
        $values = [
            'contract' => $age,
            'is_delete' => 1,
            'create_date' => date("Y-m-d H:i:s")
        ];


        // print_r($values);

        $insert = $this->cars_model->insertData('vbs_driverContract', $values, 'contract', $age);

        if ($insert == 1) {
            echo "success";
        }
    }

    public function addseriesprocess() {
        extract($_GET);
        //print_r($_GET);
        //die();
        $values = [
            'nature' => $series,
            'is_delete' => 1,
            'create_date' => date("Y-m-d H:i:s")
        ];


        // print_r($values);

        $insert = $this->cars_model->insertData('vbs_driverNature', $values, 'nature', $series);

        if ($insert == 1) {
            echo "success";
        }
    }

    public function addboiteprocess() {
        extract($_GET);
        //print_r($_GET);
        //die();
        $values = [
            'hours' => $boite,
            'is_delete' => 1,
            'create_date' => date("Y-m-d H:i:s")
        ];


        // print_r($values);

        $insert = $this->cars_model->insertData('vbs_driverHours', $values, 'hours', $boite);

        if ($insert == 1) {
            echo "success";
        }
    }

    public function addfuelprocess() {
        extract($_GET);
        //print_r($_GET);
        //die();
        $values = [
            'type' => $fuel,
            'is_delete' => 1,
            'create_date' => date("Y-m-d H:i:s")
        ];


        // print_r($values);

        $insert = $this->cars_model->insertData('vbs_driverType', $values, 'type', $fuel);

        if ($insert == 1) {
            echo "success";
        }
    }

    public function addmailprocess() {
        extract($_GET);
        //print_r($_GET);
        //die();
        $values = [
            'courroie' => $mail,
            'is_delete' => 1,
            'create_date' => date("Y-m-d H:i:s")
        ];


        // print_r($values);

        $insert = $this->cars_model->insertData('vbs_carCourroie', $values, 'courroie', $mail);

        if ($insert == 1) {
            echo "success";
        }
    }

    public function addcolorprocess() {
        extract($_GET);
        //print_r($_GET);
        //die();
        $values = [
            'couleur' => $color,
            'is_delete' => 1,
            'create_date' => date("Y-m-d H:i:s")
        ];


        // print_r($values);

        $insert = $this->cars_model->insertData('vbs_carCouleur', $values, 'couleur', $color);

        if ($insert == 1) {
            echo "success";
        }
    }

    public function addnatureprocess() {
        extract($_GET);
        //print_r($_GET);
        //die();
        $values = [
            'nature' => $nature,
            'is_delete' => 1,
            'create_date' => date("Y-m-d H:i:s")
        ];


        // print_r($values);

        $insert = $this->cars_model->insertData('vbs_carNature', $values, 'nature', $nature);

        if ($insert == 1) {
            echo "success";
        }
    }

    public function getstatusprocess() {
        extract($_GET);
        $statut = $this->cars_model->getStaut('vbs_driverStatus', 'is_delete', '1', $name, $from, $to, 'status');
        $tbody = '';
        if (!empty($statut)) {
            $i = 1;
            foreach ($statut as $stat) {
                $tbody .= '
                <tr>
                <td align="center">
	             <input type="checkbox" name="check" class="StautClass"  id="check" value="' . $stat->id . '" >
	             <input type="hidden" id="update_statut" value="' . $stat->status . '">
	             </td>
                 <td align="center">' . $i . '</td>
                 <td align="center">' . $stat->status . '</td>
                 <td align="center">' . $stat->create_date . '</td>
                </tr>
                ';
                $i++;
            }
        } else {
            $tbody .= '<tr align="center"><td colspan="4"><span class="text-danger text-center">No Record Found</span></td></tr>';
        }

        echo $tbody;
    }

    public function getcivilityprocess() {
        extract($_GET);
        $statut = $this->cars_model->getStaut('vbs_driverCivilite', 'is_delete', '1', $name, $from, $to, 'civilite');
        $tbody = '';
        if (!empty($statut)) {
            $i = 1;
            foreach ($statut as $stat) {
                $tbody .= '
                <tr>
                <td align="center">
	             <input type="checkbox" name="CivilityCheck" class="CivilityClass"  id="CivilityCheck" value="' . $stat->id . '" >
	             <input type="hidden" id="update_civility" value="' . $stat->civilite . '">
	             </td>
                 <td align="center">' . $i . '</td>
                 <td align="center">' . $stat->civilite . '</td>
                 <td align="center">' . $stat->create_date . '</td>
                </tr>
                ';
                $i++;
            }
        } else {
            $tbody .= '<tr align="center"><td colspan="4"><span class="text-danger text-center">No Record Found</span></td></tr>';
        }

        echo $tbody;
    }

    public function getpostprocess() {
        extract($_GET);
        $statut = $this->cars_model->getStaut('vbs_driverPost', 'is_delete', '1', $name, $from, $to, 'post');
        $tbody = '';
        if (!empty($statut)) {
            $i = 1;
            foreach ($statut as $stat) {
                $tbody .= '
                <tr>
                <td align="center">
	             <input type="checkbox" name="PostCheck" class="PostClass"  id="postCheck" value="' . $stat->id . '" >
	             <input type="hidden" id="update_post" value="' . $stat->post . '">
	             </td>
                 <td align="center">' . $i . '</td>
                 <td align="center">' . $stat->post . '</td>
                 <td align="center">' . $stat->create_date . '</td>
                </tr>
                ';
                $i++;
            }
        } else {
            $tbody .= '<tr align="center"><td colspan="4"><span class="text-danger text-center">No Record Found</span></td></tr>';
        }

        echo $tbody;
    }

    public function getpatternprocess() {
        extract($_GET);
        $statut = $this->cars_model->getStaut('vbs_driverPattern', 'is_delete', '1', $name, $from, $to, 'pattern');
        $tbody = '';
        if (!empty($statut)) {
            $i = 1;
            foreach ($statut as $stat) {
                $tbody .= '
                <tr>
                <td align="center">
	             <input type="checkbox" name="PatternCheck" class="PatternClass"  id="PatternCheck" value="' . $stat->id . '" >
	             <input type="hidden" id="update_pattern" value="' . $stat->pattern . '">
	             </td>
                 <td align="center">' . $i . '</td>
                 <td align="center">' . $stat->pattern . '</td>
                 <td align="center">' . $stat->create_date . '</td>
                </tr>
                ';
                $i++;
            }
        } else {
            $tbody .= '<tr align="center"><td colspan="4"><span class="text-danger text-center">No Record Found</span></td></tr>';
        }

        echo $tbody;
    }

    public function getageprocess() {
        extract($_GET);
        $statut = $this->cars_model->getStaut('vbs_driverContract', 'is_delete', '1', $name, $from, $to, 'contract');
        $tbody = '';
        if (!empty($statut)) {
            $i = 1;
            foreach ($statut as $stat) {
                $tbody .= '
                <tr>
                <td align="center">
	             <input type="checkbox" name="AgeCheck" class="AgeClass"  id="AgeCheck" value="' . $stat->id . '" >
	             <input type="hidden" id="update_age" value="' . $stat->contract . '">
	             </td>
                 <td align="center">' . $i . '</td>
                 <td align="center">' . $stat->contract . '</td>
                 <td align="center">' . $stat->create_date . '</td>
                </tr>
                ';
                $i++;
            }
        } else {
            $tbody .= '<tr align="center"><td colspan="4"><span class="text-danger text-center">No Record Found</span></td></tr>';
        }

        echo $tbody;
    }

    public function getseriesprocess() {
        extract($_GET);
        $statut = $this->cars_model->getStaut('vbs_driverNature', 'is_delete', '1', $name, $from, $to, 'nature');
        $tbody = '';
        if (!empty($statut)) {
            $i = 1;
            foreach ($statut as $stat) {
                $tbody .= '
                <tr>
                <td align="center">
	             <input type="checkbox" name="SeriesCheck" class="SeriesClass"  id="SeriesCheck" value="' . $stat->id . '" >
	             <input type="hidden" id="update_series" value="' . $stat->nature . '">
	             </td>
                 <td align="center">' . $i . '</td>
                 <td align="center">' . $stat->nature . '</td>
                 <td align="center">' . $stat->create_date . '</td>
                </tr>
                ';
                $i++;
            }
        } else {
            $tbody .= '<tr align="center"><td colspan="4"><span class="text-danger text-center">No Record Found</span></td></tr>';
        }

        echo $tbody;
    }

    public function getboiteprocess() {
        extract($_GET);
        $statut = $this->cars_model->getStaut('vbs_driverHours', 'is_delete', '1', $name, $from, $to, 'hours');
        $tbody = '';
        if (!empty($statut)) {
            $i = 1;
            foreach ($statut as $stat) {
                $tbody .= '
                <tr>
                <td align="center">
                 <input type="checkbox" name="BoiteCheck" class="BoiteClass"  id="BoiteCheck" value="' . $stat->id . '" >
                 <input type="hidden" id="update_boite" value="' . $stat->hours . '">
                 </td>
                 <td align="center">' . $i . '</td>
                 <td align="center">' . $stat->hours . '</td>
                 <td align="center">' . $stat->create_date . '</td>
                </tr>
                ';
                $i++;
            }
        } else {
            $tbody .= '<tr align="center"><td colspan="4"><span class="text-danger text-center">No Record Found</span></td></tr>';
        }

        echo $tbody;
    }

    public function getfuelprocess() {
        extract($_GET);
        $statut = $this->cars_model->getStaut('vbs_driverType', 'is_delete', '1', $name, $from, $to, 'type');
        $tbody = '';
        if (!empty($statut)) {
            $i = 1;
            foreach ($statut as $stat) {
                $tbody .= '
                <tr>
                <td align="center">
                 <input type="checkbox" name="FuelCheck" class="FuelClass"  id="FuelCheck" value="' . $stat->id . '" >
                 <input type="hidden" id="update_fuel" value="' . $stat->type . '">
                 </td>
                 <td align="center">' . $i . '</td>
                 <td align="center">' . $stat->type . '</td>
                 <td align="center">' . $stat->create_date . '</td>
                </tr>
                ';
                $i++;
            }
        } else {
            $tbody .= '<tr align="center"><td colspan="4"><span class="text-danger text-center">No Record Found</span></td></tr>';
        }

        echo $tbody;
    }

    public function getmailprocess() {
        extract($_GET);
        $statut = $this->cars_model->getStaut('vbs_carCourroie', 'is_delete', '1', $name, $from, $to, 'courroie');
        $tbody = '';
        if (!empty($statut)) {
            $i = 1;
            foreach ($statut as $stat) {
                $tbody .= '
                <tr>
                <td align="center">
                 <input type="checkbox" name="MailCheck" class="MailClass"  id="MailCheck" value="' . $stat->id . '" >
                 <input type="hidden" id="update_mail" value="' . $stat->courroie . '">
                 </td>
                 <td align="center">' . $i . '</td>
                 <td align="center">' . $stat->courroie . '</td>
                 <td align="center">' . $stat->create_date . '</td>
                </tr>
                ';
                $i++;
            }
        } else {
            $tbody .= '<tr align="center"><td colspan="4"><span class="text-danger text-center">No Record Found</span></td></tr>';
        }

        echo $tbody;
    }

    public function getcolorprocess() {
        extract($_GET);
        $statut = $this->cars_model->getStaut('vbs_carCouleur', 'is_delete', '1', $name, $from, $to, 'couleur');
        $tbody = '';
        if (!empty($statut)) {
            $i = 1;
            foreach ($statut as $stat) {
                $tbody .= '
                <tr>
                <td align="center">
                 <input type="checkbox" name="ColorCheck" class="ColorClass"  id="ColorCheck" value="' . $stat->id . '" >
                 <input type="hidden" id="update_color" value="' . $stat->couleur . '">
                 </td>
                 <td align="center">' . $i . '</td>
                 <td align="center">' . $stat->couleur . '</td>
                 <td align="center">' . $stat->create_date . '</td>
                </tr>
                ';
                $i++;
            }
        } else {
            $tbody .= '<tr align="center"><td colspan="4"><span class="text-danger text-center">No Record Found</span></td></tr>';
        }

        echo $tbody;
    }

    public function getnatureprocess() {
        extract($_GET);
        $statut = $this->cars_model->getStaut('vbs_carNature', 'is_delete', '1', $name, $from, $to, 'nature');
        $tbody = '';
        if (!empty($statut)) {
            $i = 1;
            foreach ($statut as $stat) {
                $tbody .= '
                <tr>
                <td align="center">
                 <input type="checkbox" name="NatureCheck" class="NatureClass"  id="NatureCheck" value="' . $stat->id . '" >
                 <input type="hidden" id="update_nature" value="' . $stat->nature . '">
                 </td>
                 <td align="center">' . $i . '</td>
                 <td align="center">' . $stat->nature . '</td>
                 <td align="center">' . $stat->create_date . '</td>
                </tr>
                ';
                $i++;
            }
        } else {
            $tbody .= '<tr align="center"><td colspan="4"><span class="text-danger text-center">No Record Found</span></td></tr>';
        }

        echo $tbody;
    }

    public function updatestatusprocess() {
        extract($_GET);

        /* print_r($_GET);
          die(); */
        $values = [
            'status' => $statut
        ];
        $update = $this->cars_model->updateStatut($id, $statut, $values, 'vbs_driverStatus', 'status');

        if ($update == 1) {
            echo "success";
        } else {
            echo "failed";
        }
    }

    public function updateciviliteprocess() {
        extract($_GET);

        /* print_r($_GET);
          die(); */
        $values = [
            'civilite' => $civilite
        ];
        $update = $this->cars_model->updateStatut($id, $civilite, $values, 'vbs_driverCivilite', 'civilite');

        if ($update == 1) {
            echo "success";
        } else {
            echo "failed";
        }
    }

    public function updatepostprocess() {
        extract($_GET);

        /* print_r($_GET);
          die(); */
        $values = [
            'post' => $post
        ];
        $update = $this->cars_model->updateStatut($id, $post, $values, 'vbs_driverPost', 'post');

        if ($update == 1) {
            echo "success";
        } else {
            echo "failed";
        }
    }

    public function updatepatternprocess() {
        extract($_GET);

        /* print_r($_GET);
          die(); */
        $values = [
            'pattern' => $pattern
        ];
        $update = $this->cars_model->updateStatut($id, $pattern, $values, 'vbs_driverPattern', 'pattern');

        if ($update == 1) {
            echo "success";
        } else {
            echo "failed";
        }
    }

    public function updateageprocess() {
        extract($_GET);

        /* print_r($_GET);
          die(); */
        $values = [
            'contract' => $age
        ];
        $update = $this->cars_model->updateStatut($id, $age, $values, 'vbs_driverContract', 'contract');

        if ($update == 1) {
            echo "success";
        } else {
            echo "failed";
        }
    }

    public function updateseriesprocess() {
        extract($_GET);

        /* print_r($_GET);
          die(); */
        $values = [
            'nature' => $series
        ];
        $update = $this->cars_model->updateStatut($id, $series, $values, 'vbs_driverNature', 'nature');

        if ($update == 1) {
            echo "success";
        } else {
            echo "failed";
        }
    }

    public function updateboiteprocess() {
        extract($_GET);

        /* print_r($_GET);
          die(); */
        $values = [
            'hours' => $boite
        ];
        $update = $this->cars_model->updateStatut($id, $boite, $values, 'vbs_driverHours', 'hours');

        if ($update == 1) {
            echo "success";
        } else {
            echo "failed";
        }
    }

    public function updatefuelprocess() {
        extract($_GET);

        /* print_r($_GET);
          die(); */
        $values = [
            'type' => $fuel
        ];
        $update = $this->cars_model->updateStatut($id, $fuel, $values, 'vbs_driverType', 'type');

        if ($update == 1) {
            echo "success";
        } else {
            echo "failed";
        }
    }

    public function updatemailprocess() {
        extract($_GET);

        /* print_r($_GET);
          die(); */
        $values = [
            'courroie' => $mail
        ];
        $update = $this->cars_model->updateStatut($id, $mail, $values, 'vbs_carCourroie', 'courroie');

        if ($update == 1) {
            echo "success";
        } else {
            echo "failed";
        }
    }

    public function updatecolorprocess() {
        extract($_GET);

        /* print_r($_GET);
          die(); */
        $values = [
            'couleur' => $color
        ];
        $update = $this->cars_model->updateStatut($id, $color, $values, 'vbs_carCouleur', 'couleur');

        if ($update == 1) {
            echo "success";
        } else {
            echo "failed";
        }
    }

    public function updatenatureprocess() {
        extract($_GET);

        /* print_r($_GET);
          die(); */
        $values = [
            'nature' => $nature
        ];
        $update = $this->cars_model->updateStatut($id, $nature, $values, 'vbs_carNature', 'nature');

        if ($update == 1) {
            echo "success";
        } else {
            echo "failed";
        }
    }

    public function deletestatusprocess() {
        extract($_GET);

        //$delete =  $this->cars_model->deleteDelete('vbs_carStatut',$id);
        $values = [
            'is_delete' => '0'
        ];
        $update = $this->cars_model->deactive($values, $id, 'vbs_driverStatus');
        if ($update == 1) {
            echo "success";
        }
    }

    public function deleteciviliteprocess() {
        extract($_GET);

        //$delete =  $this->cars_model->deleteDelete('vbs_carStatut',$id);
        $values = [
            'is_delete' => '0'
        ];
        $update = $this->cars_model->deactive($values, $id, 'vbs_driverCivilite');
        if ($update == 1) {
            echo "success";
        }
    }

    public function deletepostprocess() {
        extract($_GET);

        //$delete =  $this->cars_model->deleteDelete('vbs_carStatut',$id);
        $values = [
            'is_delete' => '0'
        ];
        $update = $this->cars_model->deactive($values, $id, 'vbs_driverPost');
        if ($update == 1) {
            echo "success";
        }
    }

    public function deletepatternprocess() {
        extract($_GET);

        //$delete =  $this->cars_model->deleteDelete('vbs_carStatut',$id);
        $values = [
            'is_delete' => '0'
        ];
        $update = $this->cars_model->deactive($values, $id, 'vbs_driverPattern');
        if ($update == 1) {
            echo "success";
        }
    }

    public function deleteageprocess() {
        extract($_GET);

        //$delete =  $this->cars_model->deleteDelete('vbs_carStatut',$id);
        $values = [
            'is_delete' => '0'
        ];
        $update = $this->cars_model->deactive($values, $id, 'vbs_driverContract');
        if ($update == 1) {
            echo "success";
        }
    }

    public function deleteseriesprocess() {
        extract($_GET);

        //$delete =  $this->cars_model->deleteDelete('vbs_carStatut',$id);
        $values = [
            'is_delete' => '0'
        ];
        $update = $this->cars_model->deactive($values, $id, 'vbs_driverNature');
        if ($update == 1) {
            echo "success";
        }
    }

    public function deleteboiteprocess() {
        extract($_GET);

        //$delete =  $this->cars_model->deleteDelete('vbs_carStatut',$id);
        $values = [
            'is_delete' => '0'
        ];
        $update = $this->cars_model->deactive($values, $id, 'vbs_driverHours');
        if ($update == 1) {
            echo "success";
        }
    }

    public function deletefuelprocess() {
        extract($_GET);

        //$delete =  $this->cars_model->deleteDelete('vbs_carStatut',$id);
        $values = [
            'is_delete' => '0'
        ];
        $update = $this->cars_model->deactive($values, $id, 'vbs_driverType');
        if ($update == 1) {
            echo "success";
        }
    }

    public function deletemailprocess() {
        extract($_GET);

        //$delete =  $this->cars_model->deleteDelete('vbs_carStatut',$id);
        $values = [
            'is_delete' => '0'
        ];
        $update = $this->cars_model->deactive($values, $id, 'vbs_carCourroie');
        if ($update == 1) {
            echo "success";
        }
    }

    public function deletecolorprocess() {
        extract($_GET);

        //$delete =  $this->cars_model->deleteDelete('vbs_carStatut',$id);
        $values = [
            'is_delete' => '0'
        ];
        $update = $this->cars_model->deactive($values, $id, 'vbs_carCouleur');
        if ($update == 1) {
            echo "success";
        }
    }

    public function deletenatureprocess() {
        extract($_GET);

        //$delete =  $this->cars_model->deleteDelete('vbs_carStatut',$id);
        $values = [
            'is_delete' => '0'
        ];
        $update = $this->cars_model->deactive($values, $id, 'vbs_carNature');
        if ($update == 1) {
            echo "success";
        }
    }

}
