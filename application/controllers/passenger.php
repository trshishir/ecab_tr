<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Passenger extends MY_Controller
{
	function __construct()
	{
		 parent::__construct();
        
  if (!(bool) $this->basic_auth->is_login() || !$this->basic_auth->is_client()) {
            redirect("client/login");
        }
        else {
            $this->data['user'] = $this->basic_auth->user();
        }
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('general');
        $this->load->helper('validate');
        $this->load->helper('language');
        $this->load->library('session');
        $this->load->model('user_model');
        $this->load->model('invoice_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');
        $this->load->model('support_model');
        $this->load->model('calls_model');
        $this->load->model('notes_model');
        $this->load->model('notifications_model');
        $this->load->model('cars_model');
        $this->load->model('company_model');
        $this->load->model('popups_model');
        $this->load->model('smtp_model');
        $this->load->model('userx_model');
        $this->load->model('passenger_model');

        $this->data['title'] = "Passengers";
        $this->data['css_type'] = array("form", "datatable");
        $this->data['user'] = $this->session->userdata('user');

        $this->data['configuration'] = get_configuration();

       
      

      

		
     
	}
	public function index(){
 

 		 $this->data['css'] = array('form');
        $this->data['bread_crumb'] = true;
        $this->data['heading'] = 'Client Dashboard';
        $this->data['content'] = 'users/passengers/index';
        $this->data['forgot_form'] = false;
		//
       
        $this->data['active_class'] = "passengers";
        $this->data['gmaps'] = false;
        $this->data['title_link'] = base_url('client/passengers');
      

   
       $data = [];

        $this->data['total_clients'] = $this->user_model->getClientsTotal();
        
		if($this->session->userdata('user')->email == 'client@ecab.app'){
			$data['request'] = $this->request_model->getAll();
		}else{
			$data['request'] = $this->request_model->getAll(array('user_id'=>$this->session->userdata('user')->id));
		}
	$clientid= $this->session->userdata('user')->id;


		 $this->data['passenger_data']=$this->passenger_model->getpassengers($clientid);
		 $this->data['disablecategories']=$this->passenger_model->getdisablecategories('vbs_u_disablecategory');
		  $this->_render_page('templates/client_templatev2', $this->data);
	}
	public function passengerAdd(){
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		  $this->form_validation->set_rules('civility', 'Civility', 'trim|required');
		  $this->form_validation->set_rules('disablecatid', 'Disable Category', 'trim|required');
		  $this->form_validation->set_rules('fname', 'First Name', 'trim|required|max_length[50]');
		  $this->form_validation->set_rules('lname', 'Last Name', 'trim|required|max_length[50]');
		  $this->form_validation->set_rules('mobilephone', 'Mobile Phone', 'trim|required|max_length[50]');
		  $this->form_validation->set_rules('homephone', 'Home Phone', 'trim|required|max_length[50]');
		  if ($this->form_validation->run() == FALSE){
               $this->session->set_flashdata('alert', [
				'message' => ': Something went wrong! please try again.',
				'class' => "alert-danger",
				'type' => "Error"
		    ]);
			redirect('client/passengers');
		  }


		$this->passengerstore();
	}else show_404();
   }

   public function passengerstore(){

	
		if (empty($error)) {
			        
					$user_id =$this->session->userdata('user')->id;
					$table='vbs_passengers';
					$id = $this->passenger_model->passengers_Add($table,[
						'civility' 		=> @$_POST['civility'],
						'fname' 		=> @$_POST['fname'],
						'lname' 		=> @$_POST['lname'],
						'mobilephone' 		=> @$_POST['mobilephone'],
						'homephone' 		=> @$_POST['homephone'],
						'disablecatid' => @$_POST['disablecatid'],
						'created_at' 		=> @date('Y-m-d'),
						'time' 		=> @date("H : i"),
						'clientid' 		=> $user_id,
					]);

	
					
					if ($id) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a Passenger.",
					'class' => "alert-success",
					'type' => "Success"
				]);
				redirect('client/passengers');
			}else {
				$this->session->set_flashdata('alert', [
				'message' => ': Please try again',
				'class' => "alert-danger",
				'type' => "Error"
			    ]);
				redirect('client/passengers');
			}

		} else {
			$this->session->set_flashdata('alert', [
				'message' => ': Please try again',
				'class' => "alert-danger",
				'type' => "Error"
		    ]);
			redirect('client/passengers');
		}
	}
	public function passengerEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			 $this->form_validation->set_rules('civility', 'Civility', 'trim|required');
		  $this->form_validation->set_rules('disablecatid', 'Disable Category', 'trim|required');
		  $this->form_validation->set_rules('fname', 'First Name', 'trim|required|max_length[50]');
		  $this->form_validation->set_rules('lname', 'Last Name', 'trim|required|max_length[50]');
		  $this->form_validation->set_rules('mobilephone', 'Mobile Phone', 'trim|required|max_length[50]');
		  $this->form_validation->set_rules('homephone', 'Home Phone', 'trim|required|max_length[50]');
		  if ($this->form_validation->run() == FALSE){
               $this->session->set_flashdata('alert', [
				'message' => ': Something went wrong! please try again.',
				'class' => "alert-danger",
				'type' => "Error"
		    ]);
			redirect('client/passengers');
		  }
			$id=@$_POST['passenger_id'];
			if (!empty($id)) {
				
			$table='vbs_passengers';
			$update=$this->passenger_model->passengers_Update($table,[
					   'civility' 		=> @$_POST['civility'],
						'fname' 		=> @$_POST['fname'],
						'lname' 		=> @$_POST['lname'],
						'mobilephone' 		=> @$_POST['mobilephone'],
						'homephone' 		=> @$_POST['homephone'],
						'disablecatid' => @$_POST['disablecatid'],
			], $id);
            
           




			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Passenger.",
					'class' => "alert-success",
					'type' => "Success"
				]);
				redirect('client/passengers');
			}else {
				$this->session->set_flashdata('alert', [
				'message' => ': Please try again',
				'class' => "alert-danger",
				'type' => "Error"
			    ]);
				redirect('client/passengers');
			}
		}else {
				$this->session->set_flashdata('alert', [
				'message' => ': Please try again',
				'class' => "alert-danger",
				'type' => "Error"
			    ]);
				redirect('client/passengers');
			}

	}else {
				$this->session->set_flashdata('alert', [
				'message' => ': Please try again',
				'class' => "alert-danger",
				'type' => "Error"
			    ]);
				redirect('client/passengers');
			}

	}
	public function passengerDelete(){
	
	$id=$_POST['delet_passenger_id'];
	$table = "vbs_passengers";
	$del=$this->passenger_model->passengers_Delete($table,$id);
	
	if ($del) {
         	$this->session->set_flashdata('alert', [
					'message' => ": Successfully Deleted a Passenger.",
					'class' => "alert-success",
					'type' => "Success"
				]);
				redirect('client/passengers');
	}
	else {
		      $this->session->set_flashdata('alert', [
				'message' => ': Please try again',
				'class' => "alert-danger",
				'type' => "Error"
			    ]);
				redirect('client/passengers');
	}
	
	}

public function get_ajax_passenger(){
		$id=(int)$_GET['passenger_id'];

		if($id>0){
			$this->load->model('passenger_model');
			$passenger = $this->passenger_model->getpassengerrecord($id);
			$disablecategories=$this->passenger_model->getdisablecategories('vbs_u_disablecategory');
           
		
			
				$result='<input type="hidden" name="passenger_id" value="'.$passenger->id.'">

			<div class="col-md-12">
				<div class="col-md-3" style="margin-top: 5px;">
				    <div class="form-group"> 
				        <label> Civility </label>                                
				        <select class="form-control" name="civility">
				           	<option '; if ($passenger->civility == "Mr") {$result.='selected="selected" '; }
								$result.=' value="Mr">Mr</option>
								<option '; if ($passenger->civility == "Mrs") {$result.='selected="selected" '; }
								$result.=' value="Mrs">Mrs</option>
				        </select> 
				    </div>
				 </div>
				   <div class="col-md-3" style="margin-top: 5px;">
					    <div class="form-group">
					      <span>First Name</span>
					        <input type="text" class="form-control" name="fname" placeholder="" value="'.$passenger->fname.'" required>
					    </div>
					  </div>
					  <div class="col-md-3" style="margin-top: 5px;">
					    <div class="form-group">
					      <span>Last Name</span>
					        <input type="text" class="form-control" name="lname" placeholder="" value="'.$passenger->lname.'" required>
					    </div>
					  </div>	
		    	</div>
		    	<div class="col-md-12">
				   <div class="col-md-3" style="margin-top: 5px;">
				    <div class="form-group">
				      <span>Mobile Phone</span>
				        <input type="text" class="form-control" name="mobilephone" placeholder="" value="'.$passenger->mobilephone.'" required>
				    </div>
				  </div>
				   <div class="col-md-3" style="margin-top: 5px;">
				    <div class="form-group">
				      <span>Home Phone</span>
				        <input type="text" class="form-control" name="homephone" placeholder="" value="'.$passenger->homephone.'" required>
				    </div>
				  </div>

				  <div class="col-md-3" style="margin-top: 5px;">
				    <div class="form-group">
				      <span>Category of Disable</span>
				      <select class="form-control"  name="disablecatid" required >
				        <option value="">Select</option>';
				         foreach ($disablecategories as $disable){
				        	$result.='<option '; if ($passenger->disablecatid  == $disable->id) {$result.='selected="selected" '; }
								$result.=' value="'.$disable->id.'">'.$disable->name_of_disablecategory.'</option>';
				        }

				      $result.='</select>
				    </div>
				  </div>
				</div>';
		    	}
		    	else{
		     	$result="Please select Record!";
		       }
		echo $result;
		exit;
	    
		}
		
}
