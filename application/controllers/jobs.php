<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jobs extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('general');
        $this->load->helper('language');
        $this->load->library('session');
        $this->load->model('user_model');
        $this->load->model('invoice_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');
        $this->load->model('support_model');
        $this->load->model('calls_model');
        $this->load->model('notes_model');
        $this->load->model('notifications_model');
        $this->load->model('cars_model');
        $this->load->model('company_model');
        $this->load->model('smtp_model');
        $this->load->model('userx_model');
        $this->load->model('popups_model');
        $this->load->model('sitemodel');

        $this->data['configuration'] = get_configuration();
        $this->data['title'] = "Jobseeker Dashboard";
        $this->data['css_type'] = array("form", "datatable");
        $this->data['user'] = $this->session->userdata('user');
    }

    /* Added by Saravanan.R
      STARTS
     */

    public function home() {
        /*
          if($this->session->userdata('role_id')!='8'){
          $this->session->set_flashdata('error', 'Invalide detail');
          redirect('jobseeker/login.php');
          }
          redirect('job');
         */

        if (!(bool) $this->basic_auth->is_login() || !$this->basic_auth->is_jobseeker()) {
            redirect("jobs/login");
        } else {
            $this->data['user'] = $this->basic_auth->user();
        }

        $this->data['css'] = array('form');
		$this->data['active_class'] = "dashboard";
        $this->data['bread_crumb'] = true;
        $this->data['heading'] = 'Jobseeker Dashboard';
        $this->data['content'] = 'job/dashboard';
        $this->data['forgot_form'] = false;
		
		$data = [];

        $this->data['total_jobseekers'] = $this->user_model->getJobseekersTotal();
        
		if($this->session->userdata('user')->email == 'jobseeker@ecab.app'){
			$data['jobs'] = $this->jobs_model->getAll();
		}else{
			$data['jobs'] = $this->jobs_model->getAll(array('user_id'=>$this->session->userdata('user')->id));
		}
        
//        for bar chart Job request
        $JobsRecord = $this->jobs_model->JobsChartCount();
        foreach ($JobsRecord as $row3) {
            $data3['label'][] = $row3->month_name;
            $data3['data'][] = (int) $row3->count;
        }
        $this->data['jobs_chart_data'] = json_encode($data3);


//        for line chart jobs
        $QouteLine = $this->jobs_model->JobsLineChart();
//        print_r($QouteLine);
        foreach ($QouteLine as $line) {
            $data6['day'][] = $line->y;
            $data6['count'][] = $line->a;
        }
        $this->data['jobs_line_data'] = json_encode($data6);
         $CallRecord = $this->calls_model->CallChartCount();
        foreach ($CallRecord as $row2) {
            $data2['label'][] = $row2->month_name;
            $data2['data'][] = (int) $row2->count;
        }
        $this->data['call_chart_data'] = json_encode($data2);

        foreach ($data as $key => $d) {
            if ($d != false) {
                foreach ($d as $i) {
                    if (!empty($i->status))
                        $this->data[$key][strtolower($i->status)] = isset($this->data[$key][strtolower($i->status)]) ? $this->data[$key][strtolower($i->status)] + 1 : 1;
                }
            }
        }

        $this->_render_page('templates/jobseeker_template', $this->data);
    }
	
	public function applications() {
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "application";
        $this->data['gmaps'] = false;
        $this->data['title'] = 'Job Applications';
        $this->data['title_link'] = base_url('jobseeker/applications');
        $this->data['content'] = 'job/index';


        //$this->load->model('jobs_model');

        $data = [];
        $data['request'] = $this->request_model->getAll();
        $data['jobs'] = $this->jobs_model->getAll();
        $data['calls'] = $this->calls_model->getAll();


        $this->data['data'] = $this->jobs_model->getAll();
       
        $this->_render_page('templates/jobseeker_template', $this->data);
    }

    public function fetchJobseekersData() {
        $result = array('data' => array());
		
		if($this->session->userdata('user')->email == 'jobseeker@ecab.app'){
			$data = $this->jobs_model->getAll();
		}else{
			$data = $this->jobs_model->getAll(array('user_id'=>$this->session->userdata('user')->id));
		}
        foreach ($data as $key => $value) {

            $status = $value->status;
            $checkbox = "<input type='checkbox' name='applications[]' class='ccheckbox' value='" . $value->id . "' />";
            $registered_date = date('d/m/Y', strtotime($value->created_at));
            $registered_time = date('H:i:s', strtotime($value->created_at));
            $since = timeDiff($value->created_at);
            $edit_link = "<a href='" . base_url() . "jobseeker/" . $value->id . "/edit'" . ">" . create_timestamp_uid($value->created_at,$value->id) . "</a>";
            $result['data'][$key] = array(
                $checkbox,
                $edit_link,
                $registered_date,
                $registered_time,
                $value->civility . ' ' . $value->first_name . ' ' . $value->last_name,
                $value->email,
                $value->telephone,
                $status,
                $since
            );
        } // /foreach

        echo json_encode($result);
    }

    public function jobseekerSearchData() {
        $result = array('data' => array());
        
        $searchFilter['search_name'] = $_POST['search_name'];
        $searchFilter['search_email'] = $_POST['search_email'];
        $searchFilter['search_phone'] = $_POST['search_phone'];
        $searchFilter['date_from'] = $_POST['date_from'];
        $searchFilter['date_to'] = $_POST['date_to'];
        $searchFilter['status'] = $_POST['status'];
        $data = $this->jobs_model->searchJobseeker($searchFilter);
        if (!empty($data)) {
            foreach ($data as $key => $value) {

                $status = $value['status'];
                $checkbox = "<input type='checkbox' name='applications[]' class='ccheckbox' value='" . $value['id'] . "' />";
                $registered_date = date('d/m/Y', strtotime($value['created_at']));
                $registered_time = date('H:i:s', strtotime($value['created_at']));
                $since = timeDiff($value['created_at']);
                $edit_link = "<a href='" . base_url() . "jobseeker/" . $value['id'] . "/edit'" . ">" . create_timestamp_uid($value['created_at'],$value['id']) . "</a>";
                $result['data'][$key] = array(
                    $checkbox,
                    $edit_link,
                    $registered_date,
                    $registered_time,
                    $value['civility'] . ' ' . $value['first_name'] . ' ' . $value['last_name'],
                    $value['email'],
                    $value['telephone'],
                    $status,
                    $since
                );
            } // /foreach
        }
        echo json_encode($result);
    }

    public function index() {
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "Jobseeker";
        $this->data['gmaps'] = false;
        $this->data['title'] = 'Jobseeker';
        $this->data['subtitle'] = 'Jobseeker';
        $this->data['title_link'] = '#';

        $this->data['civility'] = array('name' => 'civility', 'class' => 'user form-control', 'placeholder' => 'Civility', 'id' => 'civility', 'type' => 'text', 'value' => $this->form_validation->set_value('civility'));
        $this->data['first_name'] = array('name' => 'first_name', 'class' => 'user form-control', 'placeholder' => 'First name', 'id' => 'first_name', 'type' => 'text', 'value' => $this->form_validation->set_value('first_name'));
        $this->data['last_name'] = array('name' => 'last_name', 'class' => 'user form-control', 'placeholder' => 'Last name', 'id' => 'last_name', 'type' => 'text', 'value' => $this->form_validation->set_value('last_name'));
        $this->data['email'] = array('name' => 'email', 'class' => 'email form-control', 'placeholder' => 'User Email', 'id' => 'email', 'type' => 'text', 'value' => $this->form_validation->set_value('email'));
        $this->data['phone'] = array('name' => 'phone', 'class' => 'phone1 form-control', 'placeholder' => 'phone', 'id' => 'phone', 'type' => 'text', 'maxlength' => '11', 'value' => $this->form_validation->set_value('phone'));
        $this->data['password'] = array('name' => 'password', 'class' => 'password form-control', 'placeholder' => 'Password', 'id' => 'password', 'type' => 'password', 'value' => $this->form_validation->set_value('password'));
        $this->data['confirm_password'] = array('name' => 'confirm_password', 'class' => 'password form-control', 'placeholder' => 'Confirm Password', 'id' => 'confirm_password', 'type' => 'password', 'value' => $this->form_validation->set_value('confirm_password'));
        $this->data['company'] = array('name' => 'company', 'class' => 'user form-control', 'placeholder' => 'Company', 'id' => 'company', 'type' => 'text', 'maxlength' => '150', 'value' => $this->form_validation->set_value('company'));
        $this->data['address'] = array('name' => 'address', 'class' => 'form-control', 'placeholder' => 'adresse', 'rows' => '1', 'cols' => '40', 'id' => 'address', 'type' => 'text', 'value' => $this->form_validation->set_value('address'));
        $this->data['address1'] = array('name' => 'address1', 'class' => 'form-control', 'placeholder' => '', 'rows' => '1', 'cols' => '40', 'id' => 'address1', 'type' => 'text', 'value' => $this->form_validation->set_value('address'));
        $this->data['city'] = array('name' => 'city', 'class' => 'form-control', 'placeholder' => 'City', 'id' => 'city', 'type' => 'text', 'value' => $this->form_validation->set_value('city'));
        $this->data['zipcode'] = array('name' => 'zipcode', 'class' => 'form-control', 'placeholder' => 'Zipcode', 'id' => 'zipcode', 'type' => 'text', 'value' => $this->form_validation->set_value('zipcode'));
        $this->data['content'] = 'site/jobseekers';

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // validate form input
            $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required|xss_clean');
            $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required|xss_clean');
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[vbs_users.email]');
            $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone1_label'), 'xss_clean|integer|min_length[8]|max_length[10]');
            $this->form_validation->set_rules('address', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
            $this->form_validation->set_rules('address1', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
            $this->form_validation->set_rules('city', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
            $this->form_validation->set_rules('zipcode', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean|min_length[5]');
            $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']'); // |matches[confirm_password] 

            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run() == true) {
                $username = $this->input->post('first_name') . ' ' . $this->input->post('last_name');
                $email = strtolower($this->input->post('email'));
                $password = $this->input->post('password');
                $user_role = 8;
                $additional_data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'phone' => $this->input->post('phone'),
                    'date_of_registration' => date('Y-m-d')
                );
                $group = array('6');
                if ($this->form_validation->run() == true && $this->ion_auth->register($user_role, $username, $password, $email, $additional_data,$group)) {
                    $user_rec = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('users') . " WHERE email = '" . $email . "'");
                    $inputdata['user_id'] = $user_rec[0]->id;
                    $inputdata['address1'] = $this->input->post('address');
                    $inputdata['address2'] = $this->input->post('address1') . ' ' . $this->input->post('city');
                    $inputdata['fax_no'] = ''; // $this->input->post('fax');
                    $inputdata['company_name'] = $this->input->post('company');
                    $table_name = "users_details";

                    $status = $this->base_model->insert_operation($inputdata, $table_name);

                    // $user = $this->basic_auth->login($username, $password);

                    if ($status) {

                        // $this->session->set_flashdata('message', $this->ion_auth->messages());
                        // check to see if we are creating the user
                        // redirect them back to the admin page
                        $this->session->set_flashdata('messages', $this->ion_auth->messages());
                        redirect('jobseeker/login', 'refresh');
                    } else {
                        $this->session->set_flashdata('error', $this->ion_auth->messages());
                        $this->_render_page('templates/site_template', $this->data);
                    }
                } else {
                    $this->session->set_flashdata('error', $this->ion_auth->messages());
                    $this->_render_page('templates/site_template', $this->data);
                }
            }
        } else {
            $this->_render_page('templates/site_template', $this->data);
        }
    }

    public function signup() {
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "Jobseeker Signup";
        $this->data['gmaps'] = false;
        $this->data['title'] = 'Jobseeker Signup';
        $this->data['subtitle'] = 'Jobseeker Signup';
        $this->data['title_link'] = '#';
        $this->data['company_name'] = $this->company_model->getCompanyName();
        
        $this->data['civility'] = array('name' => 'civility', 'class' => 'form-control', 'placeholder' => 'Civility', 'id' => 'civility', 'type' => 'text', 'value' => $this->form_validation->set_value('civility'));
        $this->data['first_name'] = array('name' => 'first_name', 'class' => 'form-control', 'placeholder' => 'First name', 'id' => 'first_name', 'type' => 'text', 'value' => $this->form_validation->set_value('first_name'));
        $this->data['last_name'] = array('name' => 'last_name', 'class' => 'form-control', 'placeholder' => 'Last name', 'id' => 'last_name', 'type' => 'text', 'value' => $this->form_validation->set_value('last_name'));
        $this->data['email'] = array('name' => 'email', 'class' => 'form-control', 'placeholder' => 'User Email', 'id' => 'email', 'type' => 'text', 'value' => $this->form_validation->set_value('email'));
        $this->data['phone'] = array('name' => 'phone', 'class' => 'form-control', 'placeholder' => 'phone', 'id' => 'phone', 'type' => 'text', 'maxlength' => '11', 'value' => $this->form_validation->set_value('phone'));
        $this->data['mobile'] = array('name' => 'mobile', 'class' => 'form-control', 'placeholder' => 'Mobile', 'id' => 'mobile', 'type' => 'text', 'maxlength' => '15', 'value' => $this->form_validation->set_value('mobile'));
        $this->data['fax'] = array('name' => 'fax', 'class' => 'form-control', 'placeholder' => 'Fax', 'id' => 'fax', 'type' => 'text', 'maxlength' => '15', 'value' => $this->form_validation->set_value('fax'));
        $this->data['password'] = array('name' => 'password', 'class' => 'form-control', 'placeholder' => 'Password', 'id' => 'password', 'type' => 'password', 'value' => $this->form_validation->set_value('password'));
        $this->data['confirm_password'] = array('name' => 'confirm_password', 'class' => 'form-control', 'placeholder' => 'Confirm Password', 'id' => 'confirm_password', 'type' => 'password', 'value' => $this->form_validation->set_value('confirm_password'));
        $this->data['company'] = array('name' => 'company', 'class' => 'form-control', 'placeholder' => 'Company', 'id' => 'company', 'type' => 'text', 'maxlength' => '150', 'value' => $this->form_validation->set_value('company'));
        $this->data['address'] = array('name' => 'address', 'class' => 'form-control', 'placeholder' => 'adresse', 'rows' => '1', 'cols' => '40', 'id' => 'address', 'type' => 'text', 'value' => $this->form_validation->set_value('address'));
        $this->data['address1'] = array('name' => 'address1', 'class' => 'form-control', 'placeholder' => '', 'rows' => '1', 'cols' => '40', 'id' => 'address1', 'type' => 'text', 'value' => $this->form_validation->set_value('address'));
        $this->data['city'] = array('name' => 'city', 'class' => 'form-control', 'placeholder' => 'City', 'id' => 'city', 'type' => 'text', 'value' => $this->form_validation->set_value('city'));
        $this->data['zipcode'] = array('name' => 'zipcode', 'class' => 'form-control', 'placeholder' => 'Zipcode', 'id' => 'zipcode', 'type' => 'text', 'value' => $this->form_validation->set_value('zipcode'));
        $this->data['content'] = 'site/jobseeker_signup';
        $this->data['tos'] = '';
        $this->data['privacy'] = '';
        
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required|xss_clean');
            $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required|xss_clean');
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[vbs_users.email]');
            $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone1_label'), 'xss_clean|integer|min_length[8]|max_length[10]');
            $this->form_validation->set_rules('mobile', $this->lang->line('create_user_validation_phone1_label'), 'required|xss_clean|integer|min_length[10]|max_length[10]');
            $this->form_validation->set_rules('address', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
            $this->form_validation->set_rules('address1', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
            $this->form_validation->set_rules('city', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
            $this->form_validation->set_rules('zipcode', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean|min_length[5]');
            $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']'); // |matches[confirm_password] 
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            
            if ($this->form_validation->run() == true) {
                $username = $this->input->post('first_name') . ' ' . $this->input->post('last_name');
                $email = strtolower($this->input->post('email'));
                $password = $this->input->post('password');
                $user_role = 8;
                $user_group = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('groups') . " WHERE name = 'jobseekers'");
                $group_id = array( $user_group[0]->id);
                $additional_data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'phone' => $this->input->post('phone'),
                    'city' => $this->input->post('city'),
                    'zipcode' => $this->input->post('zipcode'),
                    'address' => $this->input->post('address'),
                    'date_of_registration' => date('Y-m-d')
                );
                if ($this->form_validation->run() == true && $this->ion_auth->register($user_role, $username, $password, $email, $additional_data,$group_id)) {
                    $user_rec = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('users') . " WHERE email = '" . $email . "'");
                    $inputdata['user_id'] = $user_rec[0]->id;
                    $inputdata['address1'] = $this->input->post('address');
                    $inputdata['address2'] = $this->input->post('address1') . ' ' . $this->input->post('city');
                    $inputdata['fax_no'] = $this->input->post('fax');
                    $inputdata['company_name'] = $this->input->post('company');
                    $table_name = "users_details";

                    $status = $this->base_model->insert_operation($inputdata, $table_name);

                    // $user = $this->basic_auth->login($username, $password);

                    if ($status) {
                        // $this->session->set_flashdata('message', $this->ion_auth->messages());
                        // check to see if we are creating the user
                        // redirect them back to the admin page
                        $this->session->set_flashdata('messages', $this->ion_auth->messages());
                        redirect('jobseeker/login', 'refresh');
                    } else {
                        $this->session->set_flashdata('error', $this->ion_auth->messages());
                        $this->_render_page('templates/site_template', $this->data);
                    }
                } else {
                    $this->session->set_flashdata('error', $this->ion_auth->messages());
                    $this->_render_page('templates/site_template', $this->data);
                }
            }
        } else {
            $this->_render_page('templates/site_template', $this->data);
        }
    }

    public function login() {
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "Jobseeker";
        $this->data['gmaps'] = false;
        $this->data['title'] = 'Jobseeker Login';
        $this->data['subtitle'] = 'Jobseeker Login';
        
         if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[2]|valid_email|max_length[50]|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[2]|max_length[50]|xss_clean');

            if ($this->form_validation->run() !== false) {
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                $remember = $this->input->post('remember_me');
				$curl = $this->input->post('curl');

                $user = $this->basic_auth->jobseeker_login($username, $password, $remember);

                if ($user != false) {
                    $this->session->set_userdata("user", $user);
                    $this->session->set_flashdata('message', $this->ion_auth->messages());
					if(empty($curl)){
						redirect("jobs/dashboard", 'refresh');
					}else if($curl=='contact'){
						redirect("contact", 'refresh');
					}else if($curl=='popup'){
						redirect(site_url(), 'refresh');
					}
                } else
                    $this->data['alert'] = [
                        'message' => "Authentication failed.",
                        'class' => "alert-danger",
                        'type' => "Error"
                    ];
            } else {
                $this->data['alert'] = [
                    'message' => "Invalid email or password.",
                    'class' => "alert-danger",
                    'type' => "Error"
                ];
            }
        }
        
        $this->data['company'] = $this->company_model->getFirst();
        $this->data['content'] = 'site/jobseeker_login';
        $this->data['username'] = array(
            'name' => 'username',
            'id' => 'identity',
            'type' => 'text',
            'class' => 'form-control',
            'placeholder' => $this->lang->line('login_identity_label'),
            'value' => $this->form_validation->set_value('username'),
        );

        $this->data['password'] = array(
            'name' => 'password',
            'id' => 'password',
            'type' => 'password',
            'placeholder' => $this->lang->line('password'),
            'class' => 'form-control'
        );

        $this->data['forgot_form'] = false;

        $this->_render_page('templates/site_template', $this->data);
    }
	
    public function popup_login() {
        $username = $_GET['username'];
		$password = $_GET['password'];
		$remember = $_GET['remember_me'];

		$user = $this->basic_auth->jobseeker_login($username, $password, $remember);

		if ($user != false) {
			$this->session->set_userdata("user", $user);
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			echo json_encode($user);
		} else{
			echo json_encode([
                    'message' => "Invalid email or password.",
                    'type' => "Error"
                ]);
		}
    }
    
    public function logout() {
        $this->basic_auth->logout();
        // redirect them to the login page
        //$this->session->set_flashdata('message', $this->basic_auth->messages());
        $this->session->userdata = array();
        redirect('jobs/login', 'refresh');
    }

    function validateUserDetail() {


        $this->load->model('jobseeker_model');
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $result = $this->jobseeker_model->validateDetail('vbs_users', array('username' => $username, 'password' => $password));

        if ($result->num_rows() > 0) {
            $jobseeker_data = $result->row();
            if (false) {
                $this->form_validation->set_message('validateUserDetail', 'Your Email is not verify.');
                return false;
            } else {

                $this->session->set_userdata('UserId', $jobseeker_data->id);
                $this->session->set_userdata('username', $jobseeker_data->username);
                $this->session->set_userdata('role_id', $jobseeker_data->role_id);
                $this->session->set_userdata('first_name', $jobseeker_data->first_name);
                /* $this->session->set_userdata('id', $jobseeker_data->id);
                  $this->session->set_userdata('email', $jobseeker_data->email);
                  $this->session->set_userdata('name', $jobseeker_data->real_name);
                  $this->session->set_userdata('access_level', $jobseeker_data->access_level); */
                return true;
            }
        } else {

            if ($password == '') {
                $this->form_validation->set_message('validateUserDetail', 'The Password field is required.');
            } else {
                $this->form_validation->set_message('validateUserDetail', 'Your email or password is incorrect.');
            }
            return false;
        }
    }
    
    public function view($id) {
        
        $jobDetails = $this->sitemodel->jobDetailByTitle($id);
        $job = $jobDetails->row();
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "jobs";
        $this->data['gmaps'] = false;
        $this->data['title'] = $job->job . " Job";
        $this->data['subtitle'] = $job->job;
        $this->data['title_link'] = '#';
         
        $this->data['job_detail'] = $jobDetails;
        $this->data['content'] = 'site/job_view';
        $this->_render_page('templates/site_template', $this->data);
    }
	
	public function add() {
        $this->data['css_type'] = array("form");
        $this->data['active_class'] = "jobs";
        $this->data['gmaps'] = false;
        $this->data['title'] = $this->lang->line("jobs");
        $this->data['title_link'] = base_url('jobseeker/applications');
        $this->data['subtitle'] = "Add";
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->store();
        }
		
		$this->data['content'] = 'job/add';
		$this->_render_page('templates/jobseeker_template', $this->data);
    }

    public function addJob() {
        $this->data['css_type'] = array("form");
        $this->data['active_class'] = "jobs";
        $this->data['gmaps'] = false;
        $this->data['title'] = $this->lang->line("jobs");
        $this->data['title_link'] = base_url('jobseeker/applications');
        $this->data['subtitle'] = "Add";
        $this->data['content'] = 'job/add';
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->store();
        }

        $this->_render_page('templates/jobseeker_template', $this->data);
    }
    
    public function store() {
        $error = job_validate(true);
        $CV = $Letter = false;

        createJobFilesPath();
        $this->uploadJobFiles($CV, $Letter, $error);

        if (empty($error)) {
            $evaluation = [
                'communication' => $this->input->post('communication'),
                'presentation' => $this->input->post('presentation'),
                'driving' => $this->input->post('driving'),
                'look' => $this->input->post('look'),
                'experience' => $this->input->post('experience'),
            ];

            $dob = to_unix_date(@$_POST['dob']);
            $id = $this->jobs_model->create([
                'user_id' => $this->session->userdata('user')->id,
                'civility' => @$_POST['civility'],
                'first_name' => @$_POST['name'],
                'last_name' => @$_POST['prename'],
                'email' => @$_POST['email'],
                'telephone' => @$_POST['tel'],
                'address1' => @$_POST['address1'],
                'address2' => @$_POST['address2'],
                'postal_code' => @$_POST['postal_code'],
                'city' => @$_POST['city'],
                'dob' => $dob,
                'age' => calculateAge($dob),
                'status' => @$_POST['status'],
                'situation' => @$_POST['situation'],
                'offer' => @$_POST['offer'],
                'msg_subject' => @$_POST['msg_subject'],
                'cv' => $CV != false ? json_encode($CV) : '',
                'letter' => $Letter != false ? json_encode($Letter) : '',
                'evaluation' => json_encode($evaluation),
                'ip_address' => $this->input->ip_address(),
            ]);

            $notes = $this->notes_model->createNotesArray($id, 'job');
            if (!empty($notes))
                $this->notes_model->bulkInsert($notes);

            $this->session->set_flashdata('alert', [
                'message' => "Successfully Created.",
                'class' => "alert-success",
                'type' => "Success"
            ]);
            redirect('jobs/' . $id . '/edit');
        } else {
            $this->data['alert'] = [
                'message' => @$error[0],
                'class' => "alert-danger",
                'type' => "Error"
            ];
        }
    }

    public function edit($id) {

        $this->data['data'] = $this->jobs_model->get(['id' => $id]);
        if ($this->data['data'] != false) {
            $this->data['css_type'] = array("form");
            $this->data['active_class'] = "jobs";
            $this->data['gmaps'] = false;
            $this->data['title'] = $this->lang->line("jobs");
            $this->data['title_link'] = base_url('jobseeker/applications');
            $this->data['subtitle'] = create_timestamp_uid($this->data['data']->created_at, $id);
            $this->data['content'] = 'job/edit';
            $this->data['evaluation'] = json_decode($this->data['data']->evaluation, true);
            $this->load->model('quick_replies_model');
            $this->data['quick_replies'] = $this->quick_replies_model->getAll(array('delete_bit' => 0, 'status' => 1, 'module' => 1));
            if ($this->data['data']->unread != 0)
                $this->jobs_model->update(['unread' => 0], $id);

            if ($this->data['data']->status == "New")
                $this->jobs_model->update(['Status' => "Pending", 'reminder_update_date' => "", 'reminder_count' => 0], $id);

            $this->data['notes'] = $this->notes_model->getAll(['type' => 'job', 'type_id' => $id]);
            $this->data['replies'] = $this->base_model->get_replies($id, 1);
            $this->data['company_data'] = $this->userx_model->get_company();
            $this->_render_page('templates/jobseeker_template', $this->data);
        } else
            show_404();
    }

    public function update($id) {
        $job = $this->jobs_model->get(['id' => $id]);
        if ($job != false) {
//			$error = job_validate(false);
//			$CV = $Letter = false;
//			createJobFilesPath();
//			$this->uploadJobFiles($CV, $Letter, $error);

            if (empty($error)) {

                $evaluation = [
                    'communication' => $this->input->post('communication'),
                    'presentation' => $this->input->post('presentation'),
                    'driving' => $this->input->post('driving'),
                    'look' => $this->input->post('look'),
                    'experience' => $this->input->post('experience'),
                ];

//				$dob = to_unix_date(@$_POST['dob']);
                $check = 1;
                if (@$_POST['status'] == "New") {
                    $check = 1;
                } else if (@$_POST['status'] == "Pending") {
                    $check = 2;
                } else if (@$_POST['status'] == "Replied") {
                    $check = 3;
                } else {
                    $check = 4;
                }
                $notifications = $this->notifications_model->get(array('status' => $check, 'department' => 1, 'notification_status' => 1));
                if ($notifications != null && !empty($notifications)) {
                    $MAIL = $this->smtp_model->get(array('id' => 1));
                    $company_data = $this->userx_model->get_company();
                    $job_reply = $this->jobs_model->get_reply($job->id);
                    $message .= $notifications->message;
                    $subject = $notifications->subject;
                    $subject = str_replace("{job_request_subject}", "Candidature Chauffeur ecab.app", $subject);
                    if (!empty($job_reply)) {
                        $message = str_replace("{last_job_request_user_reply}", $job_reply[0]->message, $message);
                    } else {
                        $message = str_replace("Reply : {last_job_request_user_reply}", "", $message);
                    }
                    $message = str_replace("{job_request_sender_email}", $job->email, $message);
                    $message = str_replace("{job_request_date}", from_unix_date($job->created_at), $message);
                    $message = str_replace("{job_request_time}", from_unix_time($job->created_at), $message);
                    $message = str_replace("{job_request_civility}", $job->civility, $message);
                    $message = str_replace("{job_request_first_name}", $job->first_name, $message);
                    $message = str_replace("{job_request_last_name}", $job->last_name, $message);
                    $message = str_replace("Company : {job_request_company_name}", "", $message);
                    $message = str_replace("{job_request_subject}", $job->subject, $message);
                    $message = str_replace("{job_request_message}", $job->message, $message);
                    if (@$_POST['status'] == "New") {
                        $message .= '<div class="row section-company-info" style=" background: -webkit-linear-gradient(#efefef, #ECECEC, #CECECE);margin: 10px 0px;max-height: 600px;border: 2px solid #a4a8ab;padding:10px;">
						<div class="col-md-5">
							<div class="text_company">';
                        if (isset($company_data['name']) && !empty($company_data['name'])) {
                            $message .= '<p><span>' . $company_data["name"] . '</span></p>';
                        }
                        if (isset($company_data['email']) && !empty($company_data['email'])) {
                            $message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Email:</span><span>' . $company_data['email'] . '</span></p>';
                        }
                        if (isset($company_data['phone']) && !empty($company_data['phone'])) {
                            $message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Phone:</span><span>' . $company_data['phone'] . '</span></p>';
                        }
                        if (isset($company_data['fax']) && !empty($company_data['fax'])) {
                            $message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Fax:</span><span>' . $company_data['fax'] . '</span></p>';
                        }
                        if (isset($company_data['website']) && !empty($company_data['website'])) {
                            $message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Website:</span><span>' . $company_data['website'] . '</span></p>';
                        }
                        if (isset($company_data['city']) && !empty($company_data['city'])) {
                            $message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Address:</span><span>' . $company_data['city'] . ' ' . $company_data['country'] . '</span></p>';
                        }
                        $message .= '<p class="social_icons">';
                        if (isset($company_data['facebook_link']) && !empty($company_data['facebook_link'])) {
                            $message .= '<a style="font-size: 12px;display: inline-block;height: 25px;width: 25px;background: #fff;border-radius: 50%;text-align: center;line-height: 25px;margin-right: 15px;" href="' . $company_data['facebook_link'] . '" target="_blank"><i class="fa fa-facebook"></i></a>';
                        }
                        if (isset($company_data['youtube_link']) && !empty($company_data['youtube_link'])) {
                            $message .= '<a style="font-size: 12px;display: inline-block;height: 25px;width: 25px;background: #fff;border-radius: 50%;text-align: center;line-height: 25px;margin-right: 15px;" href="' . $company_data['youtube_link'] . '" target="_blank"><i class="fa fa-youtube"></i></a>';
                        }
                        if (isset($company_data['instagram_link']) && !empty($company_data['instagram_link'])) {
                            $message .= '<a style="font-size: 12px;display: inline-block;height: 25px;width: 25px;background: #fff;border-radius: 50%;text-align: center;line-height: 25px;margin-right: 15px;" href="' . $company_data['instagram_link'] . '" target="_blank"><i class="fa fa-instagram"></i></a>';
                        }
                        $message .= '</p>
						</div>
							</div>
							<div class="col-md-7" style="margin-top: 15px;">
								<div class="profile_image">';
                        $message .= '<a href="" class="company_image"><img style="width: 100%;max-width: 300px;max-height: 370px;"';
                        if (isset($company_data['logo']) && !empty($company_data['logo'])) {
                            $message .= 'src="' . base_url('uploads/company') . '/' . $company_data['logo'] . '"';
                        }
                        $message .= 'alt=""></a>';
                        $message .= '</div>
							</div>
						</div>';
                    }
                    $check = sendReply($job, $subject, $message, "job", $MAIL, array(), array(), array(@$_POST['status']));
                }
                if ($_POST['status'] == "Replied") {
                    $this->jobs_model->update([
                        /* 						'civility' 		=> @$_POST['civility'],
                          'first_name' 	=> @$_POST['name'],
                          'last_name' 	=> @$_POST['prename'],
                          'email' 		=> @$_POST['email'],
                          'telephone' 	=> @$_POST['tel'],
                          'postal_code' 	=> @$_POST['postal_code'],
                          'dob' 			=> $dob,
                          'age' 			=> calculateAge($dob), */
                        'status' => @$_POST['status'],
                        'evaluation' => json_encode($evaluation),
                        'reminder_update_date' => date('Y-m-d H:i:s'),
                        'reminder_count' => 0,
                            /* 						'cv' 			=> $CV != false ? json_encode($CV) : $job->cv,
                              'letter' 		=> $Letter != false ? json_encode($Letter) : $job->letter, */
                            ], $id);
                } else {
                    $this->jobs_model->update([
                        /* 						'civility' 		=> @$_POST['civility'],
                          'first_name' 	=> @$_POST['name'],
                          'last_name' 	=> @$_POST['prename'],
                          'email' 		=> @$_POST['email'],
                          'telephone' 	=> @$_POST['tel'],
                          'postal_code' 	=> @$_POST['postal_code'],
                          'dob' 			=> $dob,
                          'age' 			=> calculateAge($dob), */
                        'status' => @$_POST['status'],
                        'evaluation' => json_encode($evaluation),
                        'reminder_update_date' => date('Y-m-d H:i:s'),
                        'reminder_count' => 0,
                            /* 						'cv' 			=> $CV != false ? json_encode($CV) : $job->cv,
                              'letter' 		=> $Letter != false ? json_encode($Letter) : $job->letter, */
                            ], $id);
                }

                $this->notes_model->delete(['type' => 'job', 'type_id' => $id]);
                $notes = $this->notes_model->createNotesArray($id, 'job');
                if (!empty($notes))
                    $this->notes_model->bulkInsert($notes);

                $this->session->set_flashdata('alert', [
                    'message' => "Successfully Updated.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
            } else {

                $this->session->set_flashdata('alert', [
                    'message' => @$error[0],
                    'class' => "alert-danger",
                    'type' => "Error"
                ]);
            }

            redirect('jobseeker/' . $id . '/edit');
        } else
            show_404();
    }

    public function reply($id) {
        $call = $this->jobs_model->get(['id' => $id]);
        if ($call != false) {
            $this->form_validation->set_rules('reply_subject', 'Subject', 'trim|xss_clean|min_length[0]|max_length[200]');
            $this->form_validation->set_rules('reply_message', 'Message', 'trim|xss_clean|min_length[0]|max_length[5000]');
            if ($this->form_validation->run() !== false) {

                $subject = isset($_POST['reply_subject']) ? $_POST['reply_subject'] : '';
                $message = isset($_POST['reply_message']) ? $_POST['reply_message'] : '';
                $msg = $message;
                $MAIL = $this->smtp_model->get(array('id' => 1));
                $company_data = $this->userx_model->get_company();
                $message .= '<div class="row section-company-info" style=" background: -webkit-linear-gradient(#efefef, #ECECEC, #CECECE);margin: 10px 0px;max-height: 600px;border: 2px solid #a4a8ab;padding:10px;">
				<div class="col-md-5">
					<div class="text_company">';
                if (isset($company_data['name']) && !empty($company_data['name'])) {
                    $message .= '<p><span>' . $company_data["name"] . '</span></p>';
                }
                if (isset($company_data['email']) && !empty($company_data['email'])) {
                    $message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Email:</span><span>' . $company_data['email'] . '</span></p>';
                }
                if (isset($company_data['phone']) && !empty($company_data['phone'])) {
                    $message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Phone:</span><span>' . $company_data['phone'] . '</span></p>';
                }
                if (isset($company_data['fax']) && !empty($company_data['fax'])) {
                    $message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Fax:</span><span>' . $company_data['fax'] . '</span></p>';
                }
                if (isset($company_data['website']) && !empty($company_data['website'])) {
                    $message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Website:</span><span>' . $company_data['website'] . '</span></p>';
                }
                if (isset($company_data['city']) && !empty($company_data['city'])) {
                    $message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Address:</span><span>' . $company_data['city'] . ' ' . $company_data['country'] . '</span></p>';
                }
                $message .= '<p class="social_icons">';
                if (isset($company_data['facebook_link']) && !empty($company_data['facebook_link'])) {
                    $message .= '<a style="font-size: 12px;display: inline-block;height: 25px;width: 25px;background: #fff;border-radius: 50%;text-align: center;line-height: 25px;margin-right: 15px;" href="' . $company_data['facebook_link'] . '" target="_blank"><i class="fa fa-facebook"></i></a>';
                }
                if (isset($company_data['youtube_link']) && !empty($company_data['youtube_link'])) {
                    $message .= '<a style="font-size: 12px;display: inline-block;height: 25px;width: 25px;background: #fff;border-radius: 50%;text-align: center;line-height: 25px;margin-right: 15px;" href="' . $company_data['youtube_link'] . '" target="_blank"><i class="fa fa-youtube"></i></a>';
                }
                if (isset($company_data['instagram_link']) && !empty($company_data['instagram_link'])) {
                    $message .= '<a style="font-size: 12px;display: inline-block;height: 25px;width: 25px;background: #fff;border-radius: 50%;text-align: center;line-height: 25px;margin-right: 15px;" href="' . $company_data['instagram_link'] . '" target="_blank"><i class="fa fa-instagram"></i></a>';
                }
                $message .= '</p>
				</div>
					</div>
					<div class="col-md-7" style="margin-top: 15px;">
						<div class="profile_image">';
                $message .= '<a href="" class="company_image"><img style="width: 100%;max-width: 300px;max-height: 370px;"';
                if (isset($company_data['logo']) && !empty($company_data['logo'])) {
                    $message .= 'src="' . base_url('uploads/company') . '/' . $company_data['logo'] . '"';
                }
                $message .= 'alt=""></a>';
                $message .= '</div>
					</div>
				</div>';
                if ($_FILES['attachment']['name']) {
                    $cart = array();
                    $cpt = count($_FILES['attachment']['name']);
                    for ($i = 0; $i < $cpt; $i++) {
                        if (!empty($_FILES['attachment']['name'][$i])) {
                            $_FILES['file']['name'] = $_FILES['attachment']['name'][$i];
                            $_FILES['file']['type'] = $_FILES['attachment']['type'][$i];
                            $_FILES['file']['tmp_name'] = $_FILES['attachment']['tmp_name'][$i];
                            $_FILES['file']['error'] = $_FILES['attachment']['error'][$i];
                            $_FILES['file']['size'] = $_FILES['attachment']['size'][$i];
                            // $profile_image = time() ."-" . preg_replace('/\s+/', '', $_FILES['attachment']['name'][$i]);
                            $path = $_FILES['attachment']['name'][$i];
                            $ext = pathinfo($path, PATHINFO_EXTENSION);
                            $profile_image = time() . "_" . rand(100, 1000) . $i . '.' . $ext;
                            $config['file_name'] = $profile_image;
                            $config['upload_path'] = './uploads/attachment/';
                            $config['allowed_types'] = 'gif|jpg|png|jpeg|docx|doc|pdf|txt|mp3|wav|zip|csv|sql|xml|psd|svg|ico|html|php|ppt|xls|xlsx|mp4|mpg|mpeg|wmv|mov|3gp|mkv';
                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);
                            if (!$this->upload->do_upload('file')) {
                                $message = array('error' => $this->upload->display_errors());
                                $this->session->set_flashdata('message', $message);
                                redirect($_SERVER['HTTP_REFERER']);
                            } else {
                                $dataa = array('upload_data' => $this->upload->data());
                                array_push($cart, $profile_image);
                            }
                        }
                    }
                    $attachment['attachment'] = implode(",", $cart);
                }
                $check = sendReply($call, $subject, $message, "job", $MAIL, $attachment['attachment']);

                if ($check['status'] != false) {
                    $this->jobs_model->update(array('last_action' => date('Y-m-d H:i:s'), 'status' => 'Replied', 'reminder_update_date' => date('Y-m-d H:i:s'), 'reminder_count' => 0), $id);
                    $data = array(
                        'subject' => $subject,
                        'message' => $msg,
                        'request_id' => $id,
                        'type' => 1,
                        'addedBy' => $this->session->userdata('user')->id,
                        'created_at' => date('Y-m-d H:i:s'),
                    );
                    $this->base_model->SaveForm('vbs_request_replies', $data);
                    $lastid = $this->base_model->get_last_record('vbs_request_replies');
                    if ($attachment['attachment'] != "") {
                        $data = array(
                            'request_reply_id' => $lastid->id,
                            'attachments' => $attachment['attachment'],
                        );
                        $this->base_model->SaveForm('vbs_request_attachments', $data);
                    }
                    $this->session->set_flashdata('alert', [
                        'message' => "Successfully Reply Sent.",
                        'class' => "alert-success",
                        'type' => "Success"
                    ]);
                } else
                    $this->session->set_flashdata('alert', [
                        'message' => $check['message'],
                        'class' => "alert-danger",
                        'type' => "Danger"
                    ]);
            } else {
                $validator['messages'] = "";
                foreach ($_POST as $key => $inp) {
                    if (form_error($key) != false) {
                        $this->session->set_flashdata('alert', [
                            'message' => form_error($key, "<span>", "</span>"),
                            'class' => "alert-danger",
                            'type' => "Danger"
                        ]);
                        break;
                    }
                }
            }

            redirect('jobseeker/' . $id . '/edit');
        } else
            show_404();
    }

    public function delete($id) {
        $this->jobs_model->delete($id);

        $this->session->set_flashdata('alert', [
            'message' => "Successfully deleted.",
            'class' => "alert-success",
            'type' => "Success"
        ]);
        redirect('jobseeker/applications');
    }

    public function uploadJobFiles(&$CV, &$Letter, &$error) {

        $this->load->library('upload');
        if (isset($_FILES['cv']['name']) && !empty($_FILES['cv']['name'])) {
            $config['upload_path'] = './uploads/jobs/cv/' . date('Y-m-d') . '/';
            $config['allowed_types'] = 'doc|docx|pdf';
            $config['max_size'] = '10240';
            $config['file_name'] = str_replace(' ', '_', $_FILES['cv']['name']);

            $this->upload->initialize($config);
            if (!$this->upload->do_upload('cv')) {
                $error[] = $this->upload->display_errors();
            } else {
                $CV = $this->upload->data();
                $CV['upload_path'] = 'uploads/jobs/cv/' . date('Y-m-d') . '/' . $CV['file_name'];
            }
        }

        if (isset($_FILES['letter']['name']) && !empty($_FILES['letter']['name'])) {
            $letterConfig['upload_path'] = './uploads/jobs/letter/' . date('Y-m-d') . '/';
            $letterConfig['allowed_types'] = 'doc|docx|pdf';
            $letterConfig['max_size'] = '10240';
            $letterConfig['file_name'] = str_replace('_', '', $_FILES['letter']['name']);
            $this->upload->initialize($letterConfig);
            if (!$this->upload->do_upload('letter')) {
                $error[] = $this->upload->display_errors();
            } else {
                $Letter = $this->upload->data();
                $Letter['upload_path'] = 'uploads/jobs/letter/' . date('Y-m-d') . '/' . $Letter['file_name'];
            }
        }
    }
	
	public function profile(){
		
		if(isset($_POST['submit'])){
			$data = array(
						'username' => $this->input->post('user_name'),
						'email' => $this->input->post('email'),
						'first_name' => $this->input->post('first_name'),
						'last_name' => $this->input->post('last_name'),
						'company_name' => $this->input->post('company_name'),
						'phone' => $this->input->post('phone'),
					);
			
			$this->support_model->updateProfile($data,$this->input->post('user_id'));
		}
		
		$this->data['css_type'] 	= array("form","datatable");
		$this->data['active_class'] = "profile";
		$this->data['gmaps'] 		= false;
		$this->data['title'] 		= $this->lang->line("My_Profile");
		$this->data['content'] 		= 'admin/support/users_data/my_profile';
		$this->data['obj'] 		    = $this;

		$this->data['user_data'] = $this->support_model->getUserData($this->session->userdata('user')->id);
		$this->data['company_data'] = $this->userx_model->get_company();
		$this->data['MAIL'] = $this->smtp_model->get(array('id' => 1));
		$this->_render_page('templates/jobseeker_template', $this->data);
	}

}
