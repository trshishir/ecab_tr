<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partner extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('general');
        $this->load->helper('language');
        $this->load->library('session');
        $this->load->model('user_model');
        $this->load->model('invoice_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');
        $this->load->model('support_model');
        $this->load->model('calls_model');
        $this->load->model('notes_model');
        $this->load->model('notifications_model');
        $this->load->model('cars_model');
        $this->load->model('popups_model');
        $this->load->model('company_model');
        $this->load->model('userx_model');
        $this->load->model('smtp_model');

        $this->data['configuration'] = get_configuration();
        $this->data['title'] = "Partner Dashboard";
        $this->data['css_type'] = array("form", "datatable");
        $this->data['user'] = $this->session->userdata('user');
    }

    /* Added by Saravanan.R
      STARTS
     */


    public function index($param=null) {
        if($param==null) {
            $rec =$this->cms_model->getCmsList_by_status('news',1,'partner');
            $this->data['content'] = 'site/news';
            $this->data['news'] = $rec;
        } else{
            $slug = explode(".",$param)[0];
            $rec =$this->cms_model->getCmsListing_id(array('post_type' =>'news', 'link_url'=>$slug), $this->lang->lang());
            $this->data['sub_heading']          = $rec->name;
            $this->data['details'] =$rec;
            $this->data['content']              = 'site/news_details';
            $this->data['author'] = $this->user_model->get(array('user.id'=>$rec->created_by));
        }

        $this->data['title'] = 'Partner';
        $this->data['subtitle'] = 'Partner';
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "partner";
        $this->data['gmaps'] = false;

        $this->data['title_link'] = '#';
        $this->_render_page('templates/site_template', $this->data);
    }

    public function signup() {
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "Partner";
        $this->data['gmaps'] = false;
        $this->data['title'] = 'Partner Signup';
        $this->data['subtitle'] = 'Start making money';
        $this->data['title_link'] = '#';
        $this->data['company_name'] = $this->company_model->getCompanyName();
        
        $this->data['civility'] = array('name' => 'civility', 'class' => 'form-control', 'placeholder' => 'Civility', 'id' => 'civility', 'type' => 'text', 'value' => $this->form_validation->set_value('civility'));
        $this->data['first_name'] = array('name' => 'first_name', 'class' => 'form-control', 'placeholder' => 'First name', 'id' => 'first_name', 'type' => 'text', 'value' => $this->form_validation->set_value('first_name'));
        $this->data['last_name'] = array('name' => 'last_name', 'class' => 'form-control', 'placeholder' => 'Last name', 'id' => 'last_name', 'type' => 'text', 'value' => $this->form_validation->set_value('last_name'));
        $this->data['email'] = array('name' => 'email', 'class' => 'form-control', 'placeholder' => 'User Email', 'id' => 'email', 'type' => 'text', 'value' => $this->form_validation->set_value('email'));
        $this->data['phone'] = array('name' => 'phone', 'class' => 'form-control', 'placeholder' => 'phone', 'id' => 'phone', 'type' => 'text', 'maxlength' => '11', 'value' => $this->form_validation->set_value('phone'));
        $this->data['mobile'] = array('name' => 'mobile', 'class' => 'form-control', 'placeholder' => 'Mobile', 'id' => 'mobile', 'type' => 'text', 'maxlength' => '15', 'value' => $this->form_validation->set_value('mobile'));
        $this->data['fax'] = array('name' => 'fax', 'class' => 'form-control', 'placeholder' => 'Fax', 'id' => 'fax', 'type' => 'text', 'maxlength' => '15', 'value' => $this->form_validation->set_value('fax'));
        $this->data['password'] = array('name' => 'password', 'class' => 'form-control', 'placeholder' => 'Password', 'id' => 'password', 'type' => 'password', 'value' => $this->form_validation->set_value('password'));
        $this->data['confirm_password'] = array('name' => 'confirm_password', 'class' => 'form-control', 'placeholder' => 'Confirm Password', 'id' => 'confirm_password', 'type' => 'password', 'value' => $this->form_validation->set_value('confirm_password'));
        $this->data['company'] = array('name' => 'company', 'class' => 'form-control', 'placeholder' => 'Company', 'id' => 'company', 'type' => 'text', 'maxlength' => '150', 'value' => $this->form_validation->set_value('company'));
        $this->data['address'] = array('name' => 'address', 'class' => 'form-control', 'placeholder' => 'adresse', 'rows' => '1', 'cols' => '40', 'id' => 'address', 'type' => 'text', 'value' => $this->form_validation->set_value('address'));
        $this->data['address1'] = array('name' => 'address1', 'class' => 'form-control', 'placeholder' => '', 'rows' => '1', 'cols' => '40', 'id' => 'address1', 'type' => 'text', 'value' => $this->form_validation->set_value('address'));
        $this->data['city'] = array('name' => 'city', 'class' => 'form-control', 'placeholder' => 'City', 'id' => 'city', 'type' => 'text', 'value' => $this->form_validation->set_value('city'));
        $this->data['zipcode'] = array('name' => 'zipcode', 'class' => 'form-control', 'placeholder' => 'Zipcode', 'id' => 'zipcode', 'type' => 'text', 'value' => $this->form_validation->set_value('zipcode'));
        $this->data['content'] = 'site/partner_signup';
        $partner_agreement = $this->cms_model->getCMSPage('legals','Partner Agreement');
        $this->data['partner_agreement'] = $partner_agreement->description;

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // validate form input
            $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required|xss_clean');
            $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required|xss_clean');
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[vbs_users.email]');
            $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone1_label'), 'xss_clean|integer|min_length[8]|max_length[10]');
            $this->form_validation->set_rules('mobile', $this->lang->line('create_user_validation_phone1_label'), 'required|xss_clean|integer|min_length[10]|max_length[10]');
            $this->form_validation->set_rules('address', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
            $this->form_validation->set_rules('address1', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
            $this->form_validation->set_rules('city', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
            $this->form_validation->set_rules('zipcode', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean|min_length[5]');
            $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']'); // |matches[confirm_password] 
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            if ($this->form_validation->run() == true) {
                $username = $this->input->post('first_name') . ' ' . $this->input->post('last_name');
                $email = strtolower($this->input->post('email'));
                $password = $this->input->post('password');
                $user_role = 9;
                $user_group = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('groups') . " WHERE name = 'partners'");
                $group_id = array( $user_group[0]->id);
                $additional_data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'phone' => $this->input->post('phone'),
                    'city' => $this->input->post('city'),
                    'zipcode' => $this->input->post('zipcode'),
                    'address' => $this->input->post('address'),
                    'date_of_registration' => date('Y-m-d')
                );

                if ($this->form_validation->run() == true && $this->ion_auth->register($user_role, $username, $password, $email, $additional_data,$group_id)) {
                    $user_rec = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('users') . " WHERE email = '" . $email . "'");
                    $inputdata['user_id'] = $user_rec[0]->id;
                    $inputdata['address1'] = $this->input->post('address');
                    $inputdata['address2'] = $this->input->post('address1') . ' ' . $this->input->post('city');
                    $inputdata['fax_no'] = $this->input->post('fax');
                    $inputdata['company_name'] = $this->input->post('company');
                    $table_name = "users_details";

                    $status = $this->base_model->insert_operation($inputdata, $table_name);

                    // $user = $this->basic_auth->login($username, $password);

                    if ($status) {
                        // $this->session->set_flashdata('message', $this->ion_auth->messages());
                        // check to see if we are creating the user
                        // redirect them back to the admin page
                        $this->session->set_flashdata('messages', $this->ion_auth->messages());
                        redirect('partner/login', 'refresh');
                    } else {
                        $this->session->set_flashdata('error', $this->ion_auth->messages());
                        $this->_render_page('templates/site_template', $this->data);
                    }
                } else {
                    $this->session->set_flashdata('error', $this->ion_auth->messages());
                    $this->_render_page('templates/site_template', $this->data);
                }
            }
        } else {
            $this->_render_page('templates/site_template', $this->data);
        }
    }

    public function login() {
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "Partner";
        $this->data['gmaps'] = false;
        $this->data['title'] = 'Partner Login';
        $this->data['subtitle'] = 'Partner Login';


        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[2]|valid_email|max_length[50]|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[2]|max_length[50]|xss_clean');

            if ($this->form_validation->run() !== false) {
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                $remember = $this->input->post('remember_me');
				$curl = $this->input->post('curl');

                $user = $this->basic_auth->partner_login($username, $password, $remember);

                if ($user != false) {
                    $this->session->set_userdata("user", $user);
                    $this->session->set_flashdata('message', $this->ion_auth->messages());
					if(empty($curl)){
						redirect("partner/home", 'refresh');
					}else if($curl=='contact'){
						redirect("contact", 'refresh');
					}else if($curl=='popup'){
						redirect(site_url(), 'refresh');
					}
                } else
                    $this->data['alert'] = [
                        'message' => "Authentication failed.",
                        'class' => "alert-danger",
                        'type' => "Error"
                    ];
            } else {
                $this->data['alert'] = [
                    'message' => "Invalid email or password.",
                    'class' => "alert-danger",
                    'type' => "Error"
                ];
            }
        }
        
        $this->data['company'] = $this->company_model->getFirst();
        $this->data['content'] = 'site/partner_login';
        $this->data['username'] = array(
            'name' => 'username',
            'id' => 'identity',
            'type' => 'text',
            'class' => 'form-control',
            'placeholder' => $this->lang->line('login_identity_label'),
            'value' => $this->form_validation->set_value('username'),
        );

        $this->data['password'] = array(
            'name' => 'password',
            'id' => 'password',
            'type' => 'password',
            'placeholder' => $this->lang->line('password'),
            'class' => 'form-control'
        );

        $this->data['forgot_form'] = false;

        $this->_render_page('templates/site_template', $this->data);
    }
	
	public function popup_login() {
        $username = $_GET['username'];
		$password = $_GET['password'];
		$remember = $_GET['remember_me'];

		$user = $this->basic_auth->partner_login($username, $password, $remember);

		if ($user != false) {
			$this->session->set_userdata("user", $user);
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			echo json_encode($user);
		} else{
			echo json_encode([
                    'message' => "Invalid email or password.",
                    'type' => "Error"
                ]);
		}
    }

    public function logout() {
        $this->basic_auth->logout();
        // redirect them to the login page
        // $this->session->set_flashdata('message', $this->basic_auth->messages());
        $this->session->userdata = array();
        redirect('partner/login', 'refresh');
    }

    public function home() {
        if (!$this->basic_auth->is_login() || !$this->basic_auth->is_partner()) {
            redirect("partner/login", 'refresh');
        } else {
            $this->data['user'] = $this->basic_auth->user();
        }
		$this->data['active_class'] = "dashboard";
        $this->data['css'] = array('form');
        $this->data['bread_crumb'] = true;
        $this->data['heading'] = 'Partner Dashboard';
        $this->data['content'] = 'partner/dashboard';
        $this->data['forgot_form'] = false;
		
		$data = [];

        $this->data['total_partners'] = $this->user_model->getPartnersTotal();
        
		if($this->session->userdata('user')->email == 'partner@ecab.app'){
			$data['calls'] = $this->calls_model->getAll();
		}else{
			$data['calls'] = $this->calls_model->getAll(array('user_id'=>$this->session->userdata('user')->id));
		}

//print_r( $record);
//        for bar chart Calls request
        $CallRecord = $this->calls_model->CallChartCount();
        foreach ($CallRecord as $row2) {
            $data2['label'][] = $row2->month_name;
            $data2['data'][] = (int) $row2->count;
        }
        $this->data['call_chart_data'] = json_encode($data2);


        //        for line chart Qoute request
        $QouteLine = $this->request_model->QouteLineChart();
//        print_r($QouteLine);
        foreach ($QouteLine as $line) {
            $data4['day'][] = $line->y;
            $data4['count'][] = $line->a;
        }
        $this->data['qoute_line_data'] = json_encode($data4);
//                for line chart calls
        $QouteLine = $this->calls_model->CallsLineChart();

//        print_r($QouteLine);
        foreach ($QouteLine as $line) {
            $data5['day'][] = $line->y;
            $data5['count'][] = $line->a;
        }
        $this->data['calls_line_data'] = json_encode($data5);

        foreach ($data as $key => $d) {
            if ($d != false) {
                foreach ($d as $i) {
                    if (!empty($i->status))
                        $this->data[$key][strtolower($i->status)] = isset($this->data[$key][strtolower($i->status)]) ? $this->data[$key][strtolower($i->status)] + 1 : 1;
                }
            }
        }

        $this->_render_page('templates/partner_template', $this->data);
    }

    public function profile() {
        $this->data['message'] = "";
        if (!$this->basic_auth->is_login()) {
            redirect('partner/login', 'refresh');
        }
        
        if ($this->input->post('submit') == "Update") {
            // FORM VALIDATIONS
            $this->form_validation->set_rules('user_name', 'User Name', 'xss_clean|required');
            $this->form_validation->set_rules('email', 'Email', 'valid_email|xss_clean|required');
            $this->form_validation->set_rules('first_name', 'First Name', 'xss_clean|required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'xss_clean|required');
            $this->form_validation->set_rules('phone', 'Phone', 'xss_clean|required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run() == TRUE) {
                $inputdata['username'] = $this->input->post('user_name');
                $inputdata['email'] = $this->input->post('email');
                $inputdata['first_name'] = $this->input->post('first_name');
                $inputdata['last_name'] = $this->input->post('last_name');
                $inputdata['phone'] = $this->input->post('phone');
                $table_name = "users";
                $where['id'] = $this->input->post('update_rec_id');
                if ($this->base_model->update_operation($inputdata, $table_name, $where)) {
                    $this->prepare_flashmessage("Updated Successfully", 0);
                    redirect('executive/profile', 'refresh');
                } else {
                    $this->prepare_flashmessage("Unable to update", 1);
                    redirect('executive/profile');
                }
            }
        }
        // $this->db->select('*');
        // $this->db->from('users');
        // $this->db->join('users_details','users_details.user_id=users.id');
        // $admin_details = $this->db->where(array('id' => $this->ion_auth->get_user_id()))->get();
        
        $sql = "SELECT * FROM vbs_users INNER JOIN vbs_users_details ON vbs_users_details.user_id = vbs_users.id  WHERE vbs_users.id= ".$this->ion_auth->get_user_id();
         
        $query = $this->db->query($sql, array(0));
        $admin_details = $query->result_array()[0];
        $this->data['admin_details'] = $admin_details;
        $this->data['css_type'] = array(
            'form'
        );
		
		$this->data['active_class'] = "profile";
        $this->data['gmaps'] = "false";
        $this->data['title'] = 'Profile';
        $this->data['content'] = 'partner/profile';
        $this->_render_page('templates/partner_template', $this->data);
    }

    /* ENDS by Saravanan.R */
	
	public function support(){
		$this->data['css_type'] 	= array("form","datatable");
		$this->data['active_class'] = "support";
		$this->data['gmaps'] 		= false;
		$this->data['title'] 		= $this->lang->line("support");
		$this->data['title_link'] 	= base_url('partner/support');
		$this->data['content'] 		= 'partner/index';

        

        
        if($this->session->userdata('user')->email == 'partner@ecab.app'){
			$this->data['data'] = $this->calls_model->getAll();
		}else{
			$this->data['data'] = $this->calls_model->getAll(array('user_id'=>$this->session->userdata('user')->id));
		}

        $this->data['popups'] = $this->popups_model->get(array('id'=>1));
        $this->data['company_data'] = $this->userx_model->get_company();
		$this->data['MAIL'] = $this->smtp_model->get(array('id' => 1));
		$this->_render_page('templates/partner_template', $this->data);
	}

	public function add(){
		$this->data['css_type'] 	= array("form");
		$this->data['active_class'] = "support";
		$this->data['gmaps'] 	= false;
		$this->data['title'] 	= $this->lang->line("support");
		$this->data['title_link'] 	= base_url('partner/support');
		$this->data['subtitle'] = "Add";
		$this->data['content']  = 'partner/add';
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->store();
		}

		$this->_render_page('templates/partner_template', $this->data);
	}

	public function store(){
			$error = call_validate();
			if (empty($error)) {
				$this->db->trans_begin();

				//$dob = to_unix_date(@$_POST['dob']);
				$id = $this->calls_model->create([
					'user_id' 		=> $this->session->userdata('user')->id,
					'civility' 		=> @$_POST['civility'],
					'first_name' 	=> @$_POST['name'],
					'last_name' 	=> @$_POST['prename'],
					'company' 		=> @$_POST['company'],
					'email' 		=> @$_POST['email'],
					'telephone' 	=> @$_POST['num'],
					'subject' 		=> @$_POST['subject'],
					'message' 		=> @$_POST['message'],
					'day' 			=> @$_POST['days'],
					//'dob' 			=> @$dob,
					'from_time' 	=> @$_POST['from_time'],
					'to_time' 		=> @$_POST['to_time'],
					'status' 		=> @$_POST['status'],
					'msg_subject' 	=> @$_POST['msg_subject'],
					'ip_address'	=> $this->input->ip_address()
				]);

				$notes = $this->notes_model->createNotesArray($id, 'call');
				if(!empty($notes)) $this->notes_model->bulkInsert($notes);

				$this->session->set_flashdata('alert', [
					'message' => "Successfully Created.",
					'class' => "alert-success",
					'type' => "Success"
				]);
				redirect('partner/support/'.$id.'/edit');
			} else {
				$this->data['alert'] = [
					'message' => @$error[0],
					'class' => "alert-danger",
					'type' => "Error"
				];
			}
	}

	public function edit($id){

		$this->data['data'] 		= $this->calls_model->get(['id' => $id]);
		if($this->data['data'] != false) {
			$this->data['css_type'] = array("form");
			$this->data['active_class'] = "support";
			$this->data['gmaps'] = false;
			$this->data['title'] 	= $this->lang->line("support");
			$this->data['title_link'] = base_url('partner/support');
			$this->data['subtitle'] = create_timestamp_uid($this->data['data']->created_at,$id);
			$this->data['content']  = 'partner/edit';
			$this->data['notes'] = $this->notes_model->getAll(['type' => 'call','type_id' => $id]);
			$this->load->model('quick_replies_model');
			$this->data['quick_replies']  = $this->quick_replies_model->getAll(array('delete_bit' => 0, 'status' => 1, 'module' => 3));
			// if($this->data['data']->unread != 0)
			// 	$this->calls_model->update(['unread' => 0], $id);

			// if($this->data['data']->status == "New")
			// 	$this->calls_model->update(['Status' => "Pending"], $id);
            $this->data['replies'] = $this->base_model->get_replies($id, 3);
			$this->data['company_data'] = $this->userx_model->get_company();
			$this->_render_page('templates/partner_template', $this->data);
		} else show_404();
	}

	public function update($id){
		$call = $this->calls_model->get(['id' => $id]);
		if($call != false) {
			//$error = call_validate();
			if (empty($error)) {
				$check = 1;
				if(@$_POST['status'] == "New"){
					$check = 1;
				}else if(@$_POST['status'] == "Pending"){
					$check = 2;
				}else if(@$_POST['status'] == "Replied"){
					$check = 3;
				}else{
					$check = 4;
				}
				$notifications = $this->notifications_model->get(array('status'=>$check, 'department'=>3, 'notification_status'=>1));
				if($notifications != null && !empty($notifications)){
					$MAIL = $this->smtp_model->get(array('id' => 1));
					$company_data = $this->userx_model->get_company();
					$call_reply = $this->calls_model->get_reply($call->id);
					$message .= $notifications->message;
					$subject = $notifications->subject;
					$subject = str_replace("{call_request_subject}","Demande de Rappel ecab.app",$subject);	
					if(!empty($call_reply)){
						$message = str_replace("{last_call_request_user_reply}",$call_reply[0]->message,$message);	
					}else{
						$message = str_replace("Reply : {last_call_request_user_reply}","",$message);	
					}
					$message = str_replace("{call_request_sender_email}",$call->email,$message);
					$message = str_replace("{call_request_date}",from_unix_date($call->created_at),$message);
					$message = str_replace("{call_request_time}",from_unix_time($call->created_at),$message);
					$message = str_replace("{call_request_civility}",$call->civility,$message);
					$message = str_replace("{call_request_first_name}",$call->first_name,$message);
					$message = str_replace("{call_request_last_name}",$call->last_name,$message);
					$message = str_replace("{call_request_company_name}",$call->company,$message);
					$message = str_replace("{call_request_subject}",$call->subject,$message);
					$message = str_replace("{call_request_message}",$call->message,$message);
					if(@$_POST['status'] == "New"){
						$message .= '<div class="row section-company-info" style=" background: -webkit-linear-gradient(#efefef, #ECECEC, #CECECE);margin: 10px 0px;max-height: 600px;border: 2px solid #a4a8ab;padding:10px;">
						<div class="col-md-5">
							<div class="text_company">';
						if(isset($company_data['name']) && !empty($company_data['name'])){
							$message .= '<p><span>'.$company_data["name"].'</span></p>';
						}
						if(isset($company_data['email']) && !empty($company_data['email'])){
							$message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Email:</span><span>'.$company_data['email'].'</span></p>';
						}
						if(isset($company_data['phone']) && !empty($company_data['phone'])){
							$message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Phone:</span><span>'.$company_data['phone'].'</span></p>';
						}
						if(isset($company_data['fax']) && !empty($company_data['fax'])){
							$message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Fax:</span><span>'.$company_data['fax'].'</span></p>';
						}
						if(isset($company_data['website']) && !empty($company_data['website'])){
							$message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Website:</span><span>'.$company_data['website'].'</span></p>';
						}
						if(isset($company_data['city']) && !empty($company_data['city'])){
							$message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Address:</span><span>'.$company_data['city'].' '.$company_data['country'].'</span></p>';
						}
						$message .= '<p class="social_icons">';
							if(isset($company_data['facebook_link']) && !empty($company_data['facebook_link'])){
								$message .= '<a style="font-size: 12px;display: inline-block;height: 25px;width: 25px;background: #fff;border-radius: 50%;text-align: center;line-height: 25px;margin-right: 15px;" href="'.$company_data['facebook_link'].'" target="_blank"><i class="fa fa-facebook"></i></a>';
							}
							if(isset($company_data['youtube_link']) && !empty($company_data['youtube_link'])){
								$message .= '<a style="font-size: 12px;display: inline-block;height: 25px;width: 25px;background: #fff;border-radius: 50%;text-align: center;line-height: 25px;margin-right: 15px;" href="'.$company_data['youtube_link'].'" target="_blank"><i class="fa fa-youtube"></i></a>';
							}
							if(isset($company_data['instagram_link']) && !empty($company_data['instagram_link'])){
								$message .= '<a style="font-size: 12px;display: inline-block;height: 25px;width: 25px;background: #fff;border-radius: 50%;text-align: center;line-height: 25px;margin-right: 15px;" href="'.$company_data['instagram_link'].'" target="_blank"><i class="fa fa-instagram"></i></a>';
							}
						$message .= '</p>
						</div>
							</div>
							<div class="col-md-7" style="margin-top: 15px;">
								<div class="profile_image">';
						$message .= '<a href="" class="company_image"><img style="width: 100%;max-width: 300px;max-height: 370px;"';
						if(isset($company_data['logo']) && !empty($company_data['logo'])){
							$message .= 'src="'.base_url('uploads/company').'/'.$company_data['logo'].'"';
						}
						$message .= 'alt=""></a>';
						$message .= '</div>
							</div>
						</div>';
					}
					$check = sendReply($call,$subject,$message,"",$MAIL,array(@$_POST['status']));
				}
				//$dob = to_unix_date(@$_POST['dob']);
				if($call->unread != 0)
					$call->unread = 0;

				@$_POST['status'] = @$_POST['status'];
				if(@$_POST['status'] == "New")
					@$_POST['status'] = "Pending";

				if($_POST['status'] == "Replied"){
					$this->calls_model->update([
	/*						'civility' 		=> @$_POST['civility'],
							'first_name' 	=> @$_POST['name'],
							'last_name' 	=> @$_POST['prename'],
							'email' 		=> @$_POST['email'],
							'telephone' 	=> @$_POST['num'],
							'subject' 		=> @$_POST['subject'],
							'message' 		=> @$_POST['message'],
							'day' 			=> @$_POST['days'],
							//'dob' 			=> @$dob,
							'from_time' 	=> @$_POST['from_time'],
							'to_time' 		=> @$_POST['to_time'],*/
							'unread' => $call->unread,
							'status' 		=> @$_POST['status'],
							'status' 		=> @$_POST['status'],
							'reminder_update_date' 		=> date('Y-m-d H:i:s'),
							'reminder_count' 		=> 0,
					], $id);
				}else{
					$this->calls_model->update([
	/*						'civility' 		=> @$_POST['civility'],
							'first_name' 	=> @$_POST['name'],
							'last_name' 	=> @$_POST['prename'],
							'email' 		=> @$_POST['email'],
							'telephone' 	=> @$_POST['num'],
							'subject' 		=> @$_POST['subject'],
							'message' 		=> @$_POST['message'],
							'day' 			=> @$_POST['days'],
							//'dob' 			=> @$dob,
							'from_time' 	=> @$_POST['from_time'],
							'to_time' 		=> @$_POST['to_time'],*/
							'unread' => $call->unread,
							'status' 		=> @$_POST['status'],
							'status' 		=> @$_POST['status'],
							'reminder_update_date' 		=> "",
							'reminder_count' 		=> 0,
					], $id);
				}

				$this->notes_model->delete(['type' => 'call', 'type_id' => $id]);
				$notes = $this->notes_model->createNotesArray($id, 'call');
				if(!empty($notes)) $this->notes_model->bulkInsert($notes);

				$this->session->set_flashdata('alert', [
						'message' => "Successfully Updated.",
						'class' => "alert-success",
						'type' => "Success"
				]);
			} else {
				$this->session->set_flashdata('alert', [
						'message' => @$error[0],
						'class' => "alert-danger",
						'type' => "Error"
				]);
			}
			redirect('partner/support/'.$id.'/edit');
		} else show_404();
	}

	public function reply($id){
		$call = $this->calls_model->get(['id' => $id]);
		if($call != false) {
			$this->form_validation->set_rules('reply_subject', 'Subject', 'trim|xss_clean|min_length[0]|max_length[200]');
			$this->form_validation->set_rules('reply_message', 'Message', 'trim|xss_clean|min_length[0]|max_length[5000]');
			if ($this->form_validation->run() !== false) {

				$subject = isset($_POST['reply_subject']) ? $_POST['reply_subject'] : '';
				$message = isset($_POST['reply_message']) ? $_POST['reply_message'] : '';
				$msg = $message;
				$MAIL = $this->smtp_model->get(array('id' => 1));
				$company_data = $this->userx_model->get_company();
				$message .= '<div class="row section-company-info" style=" background: -webkit-linear-gradient(#efefef, #ECECEC, #CECECE);margin: 10px 0px;max-height: 600px;border: 2px solid #a4a8ab;padding:10px;">
				<div class="col-md-5">
					<div class="text_company">';
				if(isset($company_data['name']) && !empty($company_data['name'])){
					$message .= '<p><span>'.$company_data["name"].'</span></p>';
				}
				if(isset($company_data['email']) && !empty($company_data['email'])){
					$message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Email:</span><span>'.$company_data['email'].'</span></p>';
				}
				if(isset($company_data['phone']) && !empty($company_data['phone'])){
					$message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Phone:</span><span>'.$company_data['phone'].'</span></p>';
				}
				if(isset($company_data['fax']) && !empty($company_data['fax'])){
					$message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Fax:</span><span>'.$company_data['fax'].'</span></p>';
				}
				if(isset($company_data['website']) && !empty($company_data['website'])){
					$message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Website:</span><span>'.$company_data['website'].'</span></p>';
				}
				if(isset($company_data['city']) && !empty($company_data['city'])){
					$message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Address:</span><span>'.$company_data['city'].' '.$company_data['country'].'</span></p>';
				}
				$message .= '<p class="social_icons">';
					if(isset($company_data['facebook_link']) && !empty($company_data['facebook_link'])){
						$message .= '<a style="font-size: 12px;display: inline-block;height: 25px;width: 25px;background: #fff;border-radius: 50%;text-align: center;line-height: 25px;margin-right: 15px;" href="'.$company_data['facebook_link'].'" target="_blank"><i class="fa fa-facebook"></i></a>';
					}
					if(isset($company_data['youtube_link']) && !empty($company_data['youtube_link'])){
						$message .= '<a style="font-size: 12px;display: inline-block;height: 25px;width: 25px;background: #fff;border-radius: 50%;text-align: center;line-height: 25px;margin-right: 15px;" href="'.$company_data['youtube_link'].'" target="_blank"><i class="fa fa-youtube"></i></a>';
					}
					if(isset($company_data['instagram_link']) && !empty($company_data['instagram_link'])){
						$message .= '<a style="font-size: 12px;display: inline-block;height: 25px;width: 25px;background: #fff;border-radius: 50%;text-align: center;line-height: 25px;margin-right: 15px;" href="'.$company_data['instagram_link'].'" target="_blank"><i class="fa fa-instagram"></i></a>';
					}
				$message .= '</p>
				</div>
					</div>
					<div class="col-md-7" style="margin-top: 15px;">
						<div class="profile_image">';
				$message .= '<a href="" class="company_image"><img style="width: 100%;max-width: 300px;max-height: 370px;"';
				if(isset($company_data['logo']) && !empty($company_data['logo'])){
					$message .= 'src="'.base_url('uploads/company').'/'.$company_data['logo'].'"';
				}
				$message .= 'alt=""></a>';
				$message .= '</div>
					</div>
				</div>';
				if ($_FILES['attachment']['name']) {
					$cart = array();
					$cpt = count($_FILES['attachment']['name']);
					for($i=0; $i<$cpt; $i++)
					{
						if(!empty($_FILES['attachment']['name'][$i])){
							$_FILES['file']['name'] = $_FILES['attachment']['name'][$i];
							$_FILES['file']['type'] = $_FILES['attachment']['type'][$i];
							$_FILES['file']['tmp_name'] = $_FILES['attachment']['tmp_name'][$i];
							$_FILES['file']['error'] = $_FILES['attachment']['error'][$i];
							$_FILES['file']['size'] = $_FILES['attachment']['size'][$i];
							// $profile_image = time() ."-" . preg_replace('/\s+/', '', $_FILES['attachment']['name'][$i]);
							$path = $_FILES['attachment']['name'][$i];
							$ext = pathinfo($path, PATHINFO_EXTENSION);
							$profile_image = time() ."_" . rand(100,1000) .$i.'.'.$ext;
							$config['file_name'] = $profile_image;
							$config['upload_path']          = './uploads/attachment/';
							$config['allowed_types']        = 'gif|jpg|png|jpeg|docx|doc|pdf|txt|mp3|wav|zip|csv|sql|xml|psd|svg|ico|html|php|ppt|xls|xlsx|mp4|mpg|mpeg|wmv|mov|3gp|mkv';
							$this->load->library('upload', $config);
							$this->upload->initialize($config);
							if ( ! $this->upload->do_upload('file'))
							{
								$message = array('error' => $this->upload->display_errors());
								$this->session->set_flashdata('message', $message);
								redirect($_SERVER['HTTP_REFERER']);
							}
							else
							{
								$dataa = array('upload_data' => $this->upload->data());
								array_push($cart, $profile_image);
							}
						}
					}
					$attachment['attachment'] = implode(",",$cart);
				}
				$check = sendReply($call,$subject,$message,"",$MAIL,array(),array(),$attachment['attachment']);

				if($check['status'] != false) {
					$this->calls_model->update(array('last_action' => date('Y-m-d H:i:s'), 'status'=>'Replied', 'reminder_update_date' => date('Y-m-d H:i:s'), 'reminder_count' => 0), $id);
					$data = array(
						'subject' => $subject,
						'message' => $msg,
						'request_id' => $id,
						'type' => 3,
						'addedBy' => $this->session->userdata('user')->id,
						'created_at' => date('Y-m-d H:i:s'),
					);
					$this->base_model->SaveForm('vbs_request_replies', $data);
					$lastid = $this->base_model->get_last_record('vbs_request_replies');
					if($attachment['attachment'] != ""){
						$data = array(
							'request_reply_id' => $lastid->id,
							'attachments' => $attachment['attachment'],
						);
						$this->base_model->SaveForm('vbs_request_attachments', $data);
					}
					$this->session->set_flashdata('alert', [
						'message' => "Successfully Reply Sent.",
						'class' => "alert-success",
						'type' => "Success"
					]);
				} else
					$this->session->set_flashdata('alert', [
							'message' => $check['message'],
							'class' => "alert-danger",
							'type' => "Danger"
					]);
			} else {
				$validator['messages'] = "";
				$counter = 0;
				foreach ($_POST as $key => $inp) {
					if(form_error($key) != false){
						$this->session->set_flashdata('alert', [
								'message' => form_error($key,"<span>","</span>"),
								'class' => "alert-danger",
								'type' => "Danger"
						]);
						break;
					}
				}
			}

			redirect('partner/support/'.$id.'/edit');
		} else show_404();
	}

	public function delete($id){
		$this->calls_model->delete($id);
		$this->session->set_flashdata('alert', [
				'message' => "Successfully deleted.",
				'class' => "alert-success",
				'type' => "Success"
		]);
		redirect('partner/support');
	}
}