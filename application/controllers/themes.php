<?php
class Themes extends MY_Controller{
public function __construct()
    {
        parent::__construct();
        // load form and url helpers
        $data = [];
        $this->load->model('my_model');
        $this->load->model('general_model');
        $this->load->model('language_model');
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('general');
        $this->load->helper('language');
        $this->load->helper('validate');
        if (!$this->basic_auth->is_login())
            redirect("admin", 'refresh');
        else
            $data['user'] = $this->basic_auth->user();
        $this->load->model('bookings_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');
        $this->load->model('support_model');
        $this->load->model('calls_model');
        $this->load->model('notes_model');
        $this->load->model('notifications_model');
        $this->load->model('cms_model');
        $this->load->model('userx_model');
        $this->load->model('filemanagement_model');
        $this->load->model('currency_model');

        $this->data['configuration'] = get_configuration();
    }


    public function index()
    {
        $this->data['user'] = $this->basic_auth->user();
        $this->data['css_type']   = array("form","datatable");
        $this->data['active_class'] = "themes";
        $this->data['gmaps']      = false;
        $this->data['title']      = $this->lang->line("themes");
        $this->data['title_link']     = base_url('admin/themes');
        $this->data['company_data'] = $this->general_model->get('vbs_company');
        $this->data['company_data'] = $this->data['company_data'][0]; 
        $this->data['themes'] = $this->my_model->get_table_row_query("SELECT vbs_themes.*,vbs_users.civility,vbs_users.first_name,vbs_users.last_name FROM vbs_themes INNER JOIN vbs_users ON vbs_users.id = vbs_themes.added_by");
        $this->data['content']  = 'admin/themes/index';
        $this->_render_page('templates/theme_template',$this->data);
    }
    public function add_theme()
    {
        $this->data['css_type']   = array("form");
        $this->data['active_class'] = "dashboard";
        $this->data['gmaps']      = false;
        $this->data['title']        = $this->lang->line("themes");
        $this->data['title_link']     = base_url('admin/themes');
        $this->data['subtitle']     = '>' .$this->lang->line("add theme");
        $this->data['user'] = $this->basic_auth->user();
        $this->data['company_data'] = $this->general_model->get('vbs_company');
        $this->data['company_data'] = $this->data['company_data'][0];
        $this->data['content']  = 'admin/themes/add_theme';
        $this->load->view('templates/theme_template',$this->data);
        //$this->load->view('admin/language/add');
    }
    public function store_theme()
    {
        //extract($_POST);
        $file_name = '';
        if (isset($_FILES['file']) && $_FILES['file']['name']!='') {
            $config['upload_path']          = './assets/system_design/theme_files/';
            $config['allowed_types']        = '*';
            $config['file_name']        = $_POST['theme_name'].'.png';
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('file'))
            {
                $this->session->set_flashdata('alert', [
                    'message' => "Image not uploding.",
                    'class' => "alert-danger",
                    'type' => "Error"
                ]);
                redirect('admin/themes/add');
            }else{
                $data = array('upload_data' => $this->upload->data());
                $file_name = $data['upload_data']['file_name'];
            }
        }else{
            $this->session->set_flashdata('alert', [
                    'message' => "Please select an image.",
                    'class' => "alert-danger",
                    'type' => "Error"
                ]);
            redirect('admin/themes/add');
        }
        $data=array(
            'theme_name'=>$_POST['theme_name'],
            'area'=>$_POST['area'],
            'status'=>$_POST['status'],
            'default_status'=>$_POST['default_status'],
            'allow_change'=>$_POST['allow_change'],
            'added_by'=>$this->session->userdata('user_id'),
            'file'=>$file_name,
            'created_at'=>date('Y-m-d H:i:s'),
            );
        $lastid=$this->general_model->add('vbs_themes',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                    'message' => "Theme Added Successfully.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
            redirect('admin/themes');
        }
    }
    public function edit_theme($id)
    {
        $this->data['css_type']   = array("form","datatable");
        $this->data['active_class'] = "dashboard";
        $this->data['gmaps']      = false;
        $this->data['title']        = $this->lang->line("themes");
        $this->data['title_link']     = base_url('admin/themes');
        $this->data['user'] = $this->basic_auth->user();
        $this->data['theme'] = $this->my_model->get_table_row_query("SELECT * FROM vbs_themes where id = '".$id."'");
        $this->data['subtitle']     = '>'.' '.create_timestamp_uid($data['theme'][0]['created_at'],$id);
        $this->data['company_data'] = $this->general_model->get('vbs_company');
        $this->data['company_data'] = $this->data['company_data'][0];
        $this->data['content']  = 'admin/themes/edit_theme';
        $this->load->view('templates/theme_template',$this->data);
    }
    public function update_theme()
    {
        extract($_POST);
        $file_name = '';
        if(isset($_FILES['file']) && $_FILES['file']['name']!='') {
            $config['upload_path']          = './assets/system_design/theme_files/';
            $config['allowed_types']        = '*';
            $config['file_name']        = $_FILES['file']['name'];
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('file'))
            {
                $data = array('upload_data' => $this->upload->data());
                $file_name = $data['upload_data']['file_name'];
            }
        }else if($file_name ==''){
            $file_name = $this->general_model->get_column('vbs_themes',array('id' => $id),'file');
        }
        $data=array(
            'theme_name'=>$theme_name,
            'area'=>$area,
            'status'=>$status,
            'default_status'=>$default_status,
            'allow_change'=>$allow_change,
            'added_by'=>$this->session->userdata('user_id'),
            'file'=>$file_name,
            'updated_at'=>date('Y-m-d H:i:s'),
            );
        $lastid=$this->my_model->update_table(array('id'=>$id),'themes',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                    'message' => "theme Updated Successfully.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
            redirect('admin/themes');
        }
    }
    public function edit($id){
        $this->data['css_type']   = array("form","datatable");
        $this->data['active_class'] = "dashboard";
        $this->data['gmaps']      = false;
        $this->data['title']        = $this->lang->line("themes");
        $this->data['title_link']     = base_url('admin/themes');
        $this->data['theme'] = $this->my_model->get_table_row_query("SELECT * FROM vbs_themes where id = '".$id."'");
        $this->data['subtitle']     = '>'.' '.create_timestamp_uid($this->data['theme'][0]['created_at'],$id);
        $this->data['company_data'] = $this->general_model->get('vbs_company');
        $this->data['company_data'] = $this->data['company_data'][0];
        $this->data['content']  = 'admin/themes/edit';
        $this->load->view('templates/theme_template',$this->data);
    }
    public function view_theme($id){
        $this->data['css_type']   = array("form","datatable");
        $this->data['active_class'] = "dashboard";
        $this->data['gmaps']      = false;
        $this->data['title']        = $this->lang->line("themes");
        $this->data['title_link']     = base_url('admin/themes');
        $this->data['theme'] = $this->my_model->get_table_row_query("SELECT * FROM vbs_themes where id = '".$id."'");
        $this->data['subtitle']     = '>'.' '.create_timestamp_uid($this->data['theme'][0]['created_at'],$id);
        $this->data['company_data'] = $this->general_model->get('vbs_company');
        $this->data['company_data'] = $this->data['company_data'][0];
        $this->data['content']  = 'admin/themes/view_theme';
        $this->load->view('templates/theme_template',$this->data);
    }
    
}
?>