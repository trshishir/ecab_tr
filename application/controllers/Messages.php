<?php

error_reporting(E_ALL);

defined('BASEPATH') OR exit('No direct script access allowed');

class Messages extends MY_Controller



{

    function __construct()

    {

        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');

        $this->load->helper(array('form', 'url'));

        $this->load->library('session');

        $this->lang->load('auth');

        $this->lang->load('general');

        $this->load->helper('language');
        $this->load->model("chat_model");
        

    

        $this->data['user'] = $this->basic_auth->user();

        $this->load->model('request_model');

        $this->load->model('jobs_model');

        $this->load->model('support_model');

        $this->load->model('calls_model');
            



        $this->data['configuration'] = get_configuration();

    }





    public function index(){

        // echo 123;exit;

        $this->data['css_type']     = array("form","datatable");

        $this->data['active_class'] = "messages";

        $this->data['gmaps']        = false;

        $this->data['title']        = $this->lang->line('messages');

        $this->data['content']      = 'admin/messages';



        $this->load->model('calls_model');

        $this->load->model('request_model');

        $this->load->model('jobs_model');

        $this->load->model('support_model');





        $data = [];

        $data['request'] = $this->request_model->getAll();

        $data['jobs']    = $this->jobs_model->getAll();

        $data['calls']   = $this->calls_model->getAll();

        $data['support']   = $this->support_model->getAll();
        
        

//        for bar chart Qoute Request

        $record = $this->request_model->QouteChartCount();



        foreach($record as $row1) {

            $data1['label'][] = $row1->month_name;

            $data1['data'][] = (int) $row1->count;

        }

        $this->data['chart_data'] = json_encode($data1);

//print_r( $record);

//        for bar chart Calls request

        $CallRecord = $this->calls_model->CallChartCount();

        foreach($CallRecord as $row2) {

            $data2['label'][] = $row2->month_name;

            $data2['data'][] = (int) $row2->count;

        }

        $this->data['call_chart_data'] = json_encode($data2);



//        for bar chart Job request

        $JobsRecord = $this->jobs_model->JobsChartCount();

        foreach($JobsRecord as $row3) {

            $data3['label'][] = $row3->month_name;

            $data3['data'][] = (int) $row3->count;

        }

        $this->data['jobs_chart_data'] = json_encode($data3);

        

//        for bar chart Support request

        $SupportRecord = $this->support_model->SupportChartCount();

        foreach($SupportRecord as $row3) {

            $data31['label'][] = $row3->month_name;

            $data31['data'][] = (int) $row3->count;

        }

        $this->data['support_chart_data'] = json_encode($data31);



        //        for line chart Qoute request

        $QouteLine = $this->request_model->QouteLineChart();

//        print_r($QouteLine);

            foreach ($QouteLine as $line){

                $data4['day'][] = $line->y;

                $data4['count'][] = $line->a;

            }

        $this->data['qoute_line_data'] = json_encode($data4);

//                for line chart calls

        $QouteLine = $this->calls_model->CallsLineChart();



//        print_r($QouteLine);

        foreach ($QouteLine as $line){

            $data5['day'][] = $line->y;

            $data5['count'][] = $line->a;

        }

        $this->data['calls_line_data'] = json_encode($data5);



//        for line chart jobs

        $QouteLine = $this->jobs_model->JobsLineChart();

//        print_r($QouteLine);

        foreach ($QouteLine as $line){

            $data6['day'][] = $line->y;

            $data6['count'][] = $line->a;

        }

        $this->data['jobs_line_data'] = json_encode($data6);

        

//        for line chart Support

        $QouteLine = $this->support_model->SupportLineChart();

//        print_r($QouteLine);

        foreach ($QouteLine as $line){

            $data61['day'][] = $line->y;

            $data61['count'][] = $line->a;

        }

        $this->data['support_line_data'] = json_encode($data61);

        

        foreach($data as $key => $d){

            if($d != false){

                foreach($d as $i){

                    if(!empty($i->status))

                        $this->data[$key][strtolower($i->status)] = isset($this->data[$key][strtolower($i->status)]) ? $this->data[$key][strtolower($i->status)] + 1 : 1;

                }

            }

        }



        $this->_render_page('templates/admin_template', $this->data);

    }

    function changechatboxstatus()
    {
       $this->load->model("chat_model");
       $this->chat_model->changechatingboxsetting($_GET['status']);
        echo "Done";
    }

    function insertchatdata()
    {
       $this->load->model("chat_model");
       $name = $_GET['username'];
       $email = $_GET['email'];
       $telephone = $_GET['telephone'];
       $ipaddress = $_SERVER['REMOTE_ADDR'];
       $pagetimblad = $_GET['page_timblad'];
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ipaddress)); 
   
        // echo 'Country Name: ' . $ipdat->geoplugin_countryName . "\n"; 
        // echo 'City Name: ' . $ipdat->geoplugin_city . "\n"; 
        // echo 'Continent Name: ' . $ipdat->geoplugin_continentName . "\n"; 
        // echo 'Latitude: ' . $ipdat->geoplugin_latitude . "\n"; 
        // echo 'Longitude: ' . $ipdat->geoplugin_longitude . "\n"; 
        // echo 'Currency Symbol: ' . $ipdat->geoplugin_currencySymbol . "\n"; 
        // echo 'Currency Code: ' . $ipdat->geoplugin_currencyCode . "\n"; 
        // echo 'Timezone: ' . $ipdat->geoplugin_timezone; 
       // exit;
        $countryName = $ipdat->geoplugin_countryName;
       $res = $this->chat_model->insertbasicchatdetails($name,$email,$telephone,$ipaddress,$pagetimblad,$countryName);
       // print_r($res);exit;
       if($res['status']==0)
       {
         $this->session->set_userdata("chatuserexist",true);
         $this->session->set_userdata("chatuserid",$res['userid']);
         echo json_encode(array('status'=>'false','userid'=>$res['userid']));
       }
       else
       {
         $this->session->set_userdata("chatuserexist",true);
         $this->session->set_userdata("chatuserid",$res['userid']);
         $message_history = $this->chat_model->getusermessages($res['userid']);
         $messages_html = '';
         foreach($message_history as $val){
            $messages_html .= '<div class="row" style="width:100%"><div class="col-md-2" style="padding: 0;"><img src="'.base_url().'assets/default.jpg" class="circle-rounded"></div><div class="col-md-10" style="padding: 0;"><div class="users-chat-div2"><div class="usercontent-div"><p><strong>';
            if($val->userid_from==1)
            {
                $messages_html .= 'Admin';
            }else
            { 
                $messages_html .= 'Me';
            }
            $messages_html .= '</strong></p><p>'.$val->message_text.'</p><span class="real-chat-badge"><small>'.date('Y-m-d',strtotime($val->dateandtime)).'</small> <i class="fa fa-check" aria-hidden="true"> </i><i class="fa fa-check" aria-hidden="true"></i></span></div></div></div></div>';
         }
         echo json_encode(array('status'=>'true','userid'=>$res['userid'],'message_history'=>$messages_html));
       }
    }

    function insertchatmessagedata()
    {
        $messagetext = $_POST['messagetext'];
        $userid_from = $_POST['userid'];
        // $attachfile = $_FILES['chatfile']['name'];
        $attachfile = trim(str_replace(" ", "_", $_FILES['chatfile']['name']));
        $this->chat_model->changetypinguserdtatus_off($_POST["userid"]);
        $this->load->model("chat_model");
        $this->do_fileupload($attachfile);
        // $messagetext = $_GET['messagetext'];
        $res = $this->chat_model->insertchatmessagedatadetails($userid_from,$messagetext,$attachfile);  
        $this->chat_model->updatesoundstatus(); 

        if($res["attachfile"]==""){
        echo '<div class="row" style="width:100%"><div class="col-md-2" style="padding-left:0;"><img src="'.base_url().'assets/default.jpg" class="circle-rounded"></div><div class="col-md-10">';
        echo '<div class="users-chat-div2">
                   <div class="usercontent-div">';
                  echo'<p><strong>Me</strong></p>';
                   echo'<p>'.$res["message_text"].'</p>
                    <span class="real-chat-badge"><small>'.date('Y-m-d',strtotime($res["dateandtime"])).' '.date('H:i',strtotime($res["dateandtime"])).'</small> <i class="fa fa-check" aria-hidden="true"> </i><i class="fa fa-check" aria-hidden="true"></i></span>
                   </div>
                </div></div></div>';  
        }
        else
        {
            echo '<div class="row" style="width:100%"><div class="col-md-2" style="padding-left:0;"><img src="'.base_url().'assets/default.jpg" class="circle-rounded"></div><div class="col-md-10">';
            echo '<div class="users-chat-div2">
                   <div class="usercontent-div">';
                  echo'<p><strong>Me</strong></p>';
                  echo'<img src="'.base_url().'assets/chat_files/'.$res["attachfile"].'" width="150">';
                   echo'<p>'.$res["message_text"].'</p>
                    <span class="real-chat-badge"><small>'.date('Y-m-d',strtotime($res["dateandtime"])).' '.date('H:i',strtotime($res["dateandtime"])).'</small> <i class="fa fa-check" aria-hidden="true"> </i><i class="fa fa-check" aria-hidden="true"></i></span>
                   </div>
                </div></div></div>'; 
        }
    }

    function getallnewmessages()
    {
        $res = $this->chat_model->getallnewmessageshistory();
        // $sound_res = $this->chat_model->getchat_sound_status();
        // echo "<pre>";
        // print_r($sound_res);exit;
        $html_data = '';
        $time_var = '';
        foreach($res as $val)
        {
            $last_message = $this->chat_model->getlast_message($val->userid_from);
            $count_message = $this->chat_model->getcount_message($val->userid_from);

            $timestamp = strtotime($last_message['dateandtime']);   
            $strTime = array("s", "mn", "h", "d", "m", "y");
            $length = array("60","60","24","30","12","10");

            $currentTime = time();
            if($currentTime >= $timestamp) {
            $diff     = time()- $timestamp;
            for($i = 0; $diff >= $length[$i] && $i < count($length)-1; $i++) {
            $diff = $diff / $length[$i];
            }

            $diff = round($diff);
            
            // return $diff . " " . $strTime[$i] . "(s)";
           }
            
            $html_data .= '</div></div>';
            if($last_message['is_like']==1){
                $color = 'blue;';
                $clickbtn = 'dislikeit('.$last_message["id"].')';
            }
            else{
                $color = ';';
                $clickbtn = 'likeit('.$last_message["id"].')';
            }
            // $time_var = $diff.$strTime[$i];

            if($val->is_user_active==1)
            {
                $bullet_color = 'green';
                $org_bg = 'linear-gradient(to bottom, #fbfbfb 0%, #3e9d389e 39%, #3e9d389e 39%, #3e9d38d9 100%)';
            }
            elseif($val->is_user_active==2)
            {
                $bullet_color = 'orange';
                $org_bg = 'linear-gradient(to bottom, #fbfbfb 0%, #f7b34f 39%, #f7b34f 39%, #f7b350e0 100%)';
            }
            else
            {
                $bullet_color = 'darkgrey';
                $org_bg = '';
            }
            // echo "<pre>";
            // print_r($last_message);
            $name_i = "'".$val->name."'";
            $html_data .= '<div style="position:relative;"><div style="background:'.$org_bg.'" class="users-chat-div hide-chat-box2';if($count_message!=0){ $html_data .= " pending_msg_box";} $html_data .= '" onclick="getspecificuserchathistory('.$val->userid_from.','.$name_i.')">
               <div class="user-image-div">
                   <img src="'.base_url().'assets/default.jpg" class="circle-rounded">
                   <i class="fa fa-circle fa-circle2 status_active'.$val->userid_from.'" aria-hidden="true" style="color:'.$bullet_color.'"></i>
               </div>
               <div class="usercontent-div">';
            $html_data .= '<p data-title="IP: '.$val->ip_address.' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Location: '.$val->location.' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Connected Time: '.$diff.$strTime[$i].'"><strong>'.$val->name.' (Visitor)</strong></p>';
            if($last_message['message_text']=="")
            {
                $html_data .= '<p>File ['.substr($last_message["attachfile"],0,20).']</p>';  
            }
            else
            {
                $html_data .= '<p>'.substr($last_message["message_text"],0,18).'</p>';
            }
            if($count_message==0){

            }
            else
            {
                $html_data .= '<span class="badge badge-danger chat-badge">'.$count_message.'</span>';
            }            
            

            
            $html_data .= '<span class="chat-badge-time">'.$diff.$strTime[$i].'&nbsp;&nbsp; <i class="fa fa-thumbs-o-up likebtn'.$last_message["id"].'" style="
    position: relative; z-index: 9999; font-size: 18px;cursor:pointer;color:'.$color.'" onclick="'.$clickbtn.'" title="Like"></i></span> </div>';
        }
        // echo $html_data;exit;
        // echo json_encode(array("sound_status"=>$sound_res["is_chat_sound_status"],"ischat_open"=>$sound_res["is_chat_open"],"chat_html_data"=>$html_data));
        echo $html_data;
    }

    function checksoundstatusdata()
    {
        $sound_res = $this->chat_model->getchat_sound_status();
        echo json_encode(array("sound_status"=>$sound_res["is_chat_sound_status"],"ischat_open"=>$sound_res["is_chat_open"]));
    }

    function fnctimeago($date) {
       $timestamp = strtotime($date);   
       
       $strTime = array("second", "minute", "hour", "day", "month", "year");
       $length = array("60","60","24","30","12","10");

       $currentTime = time();
       if($currentTime >= $timestamp) {
            $diff     = time()- $timestamp;
            for($i = 0; $diff >= $length[$i] && $i < count($length)-1; $i++) {
            $diff = $diff / $length[$i];
            }

            $diff = round($diff);
            return $diff . " " . $strTime[$i] . "(s) ago ";
       }
    }

    function getallspecificusermessages()
    {
        $quick_replies = $this->chat_model->getquickreplies();
        $res_usersdata = $this->chat_model->getusersnameandotherdata($_GET['userid']);
        $res = $this->chat_model->getallspecificusermessageshistory($_GET['userid']);
        $this->chat_model->updateallspecificusermessageshistory($_GET['userid']);
        echo '<div class="chat-main" style="background-color: #f5f5f5;"><div class="col-md-12 chat-header chat-header'.$_GET['userid'].' rounded-top bg-primary text-white"><div class="row"><div class="col-md-12 hidechatbx hide-chat-box2 username pl-2" id="minimizechatbox'.$_GET['userid'].'" onclick="updateclosespecificchatbox2_off('.$_GET['userid'].')"><i class="fa fa-circle text-success status_active'.$_GET['userid'].'" aria-hidden="false" style="position: relative; top: 0px; left: 0px;"></i> <h6 class="m-0" id="userchatname">'.$res_usersdata["name"].'(Visitor)</h6> <span style="color:#868686">'. date('M d').', '.date('H:i A').'</span></div><div class="options text-right pr-2"><b class="hide-chat-box2" style="font-size: 32px;position: relative;top: 2px;cursor: context-menu;"> - </b><i class="fa fa-times closechatbox" onclick="closechatbox('.$_GET['userid'].')" aria-hidden="false"></i></div></div></div><div class="chat-content'.$_GET['userid'].'"><div class="col-md-12 chats border chatsbox" id="chatsbox'.$_GET['userid'].'"><ul class="p-0 chat_ul'.$_GET['userid'].'" id="chat_ul'.$_GET['userid'].'" style="padding-left: 0px !important; padding-top: 10px;">';
        foreach($res as $val)
        {
            if($val->type==1)
            {
                if($val->attachfile=="")
                {
                    echo '<li class="p-1 rounded mb-1"><div class="receive-msg"><div class="row"><div class="col-md-2" style="padding:0px; padding-left:5px;"><img src="'.base_url().'assets/default.jpg"></div><div class="col-md-10 receive-msg-desc mt-1 ml-1 pl-2 pr-2"><span class="dateandtime_class">'.date("Y-m-d",strtotime($val->dateandtime)).' '.date("H:i",strtotime($val->dateandtime)).'</span><p class="pl-2 pr-2 rounded"><span>'.$val->message_text.'</span></p></div></div></div></li>';
                    // echo '<li class="pl-2 pr-2 bg-primary rounded text-white send-msg mb-1">'.$val->message_text. '</li>';
                }
                else{

                    $ext = pathinfo($val->attachfile, PATHINFO_EXTENSION);
                    if($ext=="png" or $ext=="PNG" or $ext=="jpg" or $ext=="jpeg" or $ext=="gif")
                    {
                        echo '<li class="pl-1 rounded mb-1"><div class="receive-msg"><div class="row"><div class="col-md-2" style="padding:0px;padding-left:5px;"><img src="'.base_url().'assets/default.jpg"></div><div class="col-md-10 receive-msg-desc mt-1 ml-1 pl-2 pr-2"><span class="dateandtime_class">'.date("Y-m-d",strtotime($val->dateandtime)).' '.date("H:i",strtotime($val->dateandtime)).'</span><div class="bgcolor"><div class="text-center" style="padding-top:5px;"><a href="'.base_url().'assets/chat_files/'.$val->attachfile.'" target="_blank"><img src="'.base_url().'assets/chat_files/'.$val->attachfile.'" width="200"><p class="image_name">'.substr($val->attachfile,0,15).'</p></a></div>'.$val->message_text. '</div></div></div></div></li>';
                    }
                    else
                    {
                        echo '<li class="pl-1 rounded mb-1"><div class="receive-msg"><div class="row"><div class="col-md-2" style="padding:0px;padding-left:5px;"><img src="'.base_url().'assets/default.jpg"></div><div class="col-md-10 receive-msg-desc mt-1 ml-1 pl-2 pr-2"><span class="dateandtime_class">'.date("Y-m-d",strtotime($val->dateandtime)).' '.date("H:i",strtotime($val->dateandtime)).'</span><div class="bgcolor"><div class="text-center" style="padding-top:5px;"><a href="'.base_url().'assets/chat_files/'.$val->attachfile.'" target="_blank"><img src="'.base_url().'assets/chat_files/file_upload_image.png" width="200"><p class="image_name">'.substr($val->attachfile,0,15).'</p></a></div>'.$val->message_text. '</div></div></div></div></li>';
                        // echo '<li class="pl-2 pr-2 bg-primary rounded text-white send-msg mb-1"><div class="text-center" style="padding-top:5px;"><a href="'.base_url().'assets/chat_files/'.$val->attachfile.'" target="_blank"><img src="'.base_url().'assets/chat_files/file_upload_image.png" width="200"><p>'.substr($val->attachfile,0,15).'</p></a></div>'.$val->message_text. '</li>';                        
                    }
                    // echo '<li class="pl-2 pr-2 bg-primary rounded text-white send-msg mb-1"><div class="text-center" style="padding-top:5px;"><img src="'.base_url().'assets/chat_files/'.$val->attachfile.'" width="150"></div>'.$val->message_text. '</li>';
                }
            }
            else
            {
                if($val->attachfile=="")
                {
                echo '<li class="p-1 rounded mb-1"><div class="receive-msg"><div class="row"><div class="col-md-2" style="padding:0px;padding-left:5px;"><img src="'.base_url().'assets/default.jpg"></div><div class="col-md-10 receive-msg-desc mt-1 ml-1 pl-2 pr-2"><span class="dateandtime_class">'.date("Y-m-d",strtotime($val->dateandtime)).' '.date("H:i",strtotime($val->dateandtime)).'</span><p class="pl-2 pr-2 rounded"><span>'.$val->message_text.'</span></p></div></div></div></li>';
                }
                else
                {
                    $ext = pathinfo($val->attachfile, PATHINFO_EXTENSION);
                    if($ext=="png" or $ext=="PNG" or $ext=="jpg" or $ext=="jpeg" or $ext=="gif")
                    {
                        echo '<li class="pl-1 rounded mb-1"><div class="receive-msg"><div class="row"><div class="col-md-2" style="padding:0px;padding-left:5px;"><img src="'.base_url().'assets/default.jpg"></div><div class="col-md-10 receive-msg-desc mt-1 ml-1 pl-2 pr-2"><span class="dateandtime_class">'.date("Y-m-d",strtotime($val->dateandtime)).' '.date("H:i",strtotime($val->dateandtime)).'</span><div class="bgcolor"><div class="text-center" style="padding-top:5px;"><a href="'.base_url().'assets/chat_files/'.$val->attachfile.'" target="_blank"><img src="'.base_url().'assets/chat_files/'.$val->attachfile.'" width="200"><p class="image_name">'.substr($val->attachfile,0,15).'</p></a></div>'.$val->message_text. '</div></div></div></div></li>';
                    }
                    else
                    {
                        echo '<li class="pl-1 rounded mb-1"><div class="receive-msg"><div class="row"><div class="col-md-2" style="padding:0px;padding-left:5px;"><img src="'.base_url().'assets/default.jpg"></div><div class="col-md-10 receive-msg-desc mt-1 ml-1 pl-2 pr-2"><span class="dateandtime_class">'.date("Y-m-d",strtotime($val->dateandtime)).' '.date("H:i",strtotime($val->dateandtime)).'</span><div class="bgcolor"><div class="text-center" style="padding-top:5px;"><a href="'.base_url().'assets/chat_files/'.$val->attachfile.'" target="_blank"><img src="'.base_url().'assets/chat_files/file_upload_image.png" width="200"><p class="image_name">'.substr($val->attachfile,0,15).'</p></a></div>'.$val->message_text. '</div></div></div></div></li>';
                    }
                }
            }
        }
        echo '</ul></div><div id="attachfile_div'.$_GET['userid'].'" class="text-center" style="float: right;width: 70px; display: none;"><div id="otherfieattacmentdiv"></div><img src="" id="changeattachfileimage'.$_GET['userid'].'" style="width: 100%;border: 1px solid #ddd; border-radius: 4px; padding: 3px;"><div class="text-center" id="attachfilename'.$_GET['userid'].'" style="line-height: 1;"></div></div><div class="col-md-12 message-box message-box'.$_GET['userid'].' border pl-2 pr-2 border-top-0" onclick="removeextrafunctionality()"><input type="text" name="messagetext" onkeyup="changeadmintypingstatus('.$_GET['userid'].')" id="input-left-position'.$_GET['userid'].'" class="pl-0 pr-0 w-100" placeholder="Type a message..." autocomplete="off" required="required"/><div class="typing-loader typing_loader_none'.$_GET['userid'].'"></div><div class="border-line"></div><input type="hidden" name="userid" class="input_userid" value="'.$_GET['userid'].'"><div class="tools" style="clear: both;"><input type="file" name="chatfile" id="chatfile'.$_GET['userid'].'" style="visibility: hidden;height: 0px;width: 0px;" onchange="addattachfileto_div('.$_GET['userid'].')"><i class="fa fa-paperclip" aria-hidden="true" title="Add file" onclick="addchatfile('.$_GET['userid'].')"></i><i class="fa fa-smile-o" aria-hidden="true" title="Emoticons" onclick="openemojis('.$_GET['userid'].')"></i>';
        echo'<select class="form-control quickreply_selectdd" onchange="quickchatreplyfnc('.$_GET['userid'].')" id="quickchatreply_val'.$_GET['userid'].'">';
        foreach($quick_replies as $replies_val){
        echo '<option value="'.str_replace(array("<pre>","<p>","</p>","<h1>","</h1>","<h2>","</h2>","<h3>","</h3>","<h4>","</h4>","<h5>","</h5>","<h6>","</h6>","<strong>","</strong>"),"",$replies_val->message_sentence).'">'.str_replace(array("<pre>","<p>","</p>","<h1>","</h1>","<h2>","</h2>","<h3>","</h3>","<h4>","</h4>","<h5>","</h5>","<h6>","</h6>","<strong>","</strong>"),"",$replies_val->message_sentence).'</option>';
        }
        echo'</select><button type="submit" name="submit" class="btn btn-success"  style="float: right; padding: 5px; height: inherit !important; margin-bottom: 5px;color: #fff;">Send</button></div></div></div></div>';
       $this->change_is_chat_open_status();
        // print_r($res);
    }

    function getallspecificusermessagesdatatoul()
    {
        $res_usersdata = $this->chat_model->getusersnameandotherdata($_GET['userid']);
        $res = $this->chat_model->getallspecificusermessageshistory($_GET['userid']);
        $this->chat_model->updateallspecificusermessageshistory($_GET['userid']);

        if($res_usersdata['is_user_typing']==1)
        {
            echo '<style>.typing_loader_none'.$_GET['userid'].'{display:block !important}</style>';
        }
        else
        {
                       
        }
        if($res_usersdata['is_user_active']==1)
        {
            $bullet_color = 'green';
            $org_bgcolor = 'linear-gradient(to bottom, rgb(62, 157, 56) 0%,rgba(255,255,255,1) 49%,rgb(62, 157, 56) 100%)';
        }
        elseif($res_usersdata['is_user_active']==2)
        {
            $bullet_color = 'orange';
            $org_bgcolor = 'linear-gradient(to bottom, rgba(255, 177, 0, 0.92) 0%,rgba(255,255,255,1) 49%,rgba(255, 177, 0, 0.92) 100%)';
        }
        else
        {
            $bullet_color = 'darkgrey';
            $org_bgcolor = '';
        }

        echo '<style>.chat-header'.$_GET['userid'].'{background:'.$org_bgcolor.'} .status_active'.$_GET['userid'].'{color:'.$bullet_color.'}</style>';

        foreach($res as $val)
        {
            if($val->type==1)
            {
                if($val->attachfile=="")
                {
                    echo '<li class="p-1 rounded mb-1"><div class="receive-msg"><div class="row"><div class="col-md-2" style="padding:0px; padding-left:5px;"><img src="'.base_url().'assets/default.jpg"></div><div class="col-md-10 receive-msg-desc mt-1 ml-1 pl-2 pr-2"><span class="dateandtime_class">'.date("Y-m-d",strtotime($val->dateandtime)).' '.date("H:i",strtotime($val->dateandtime)).'</span><p class="pl-2 pr-2 rounded" style="background:#767676c2"><span>'.$val->message_text.'</span></p></div></div></div></li>';
                    // echo '<li class="pl-2 pr-2 bg-primary rounded text-white send-msg mb-1">'.$val->message_text. '</li>';
                }
                else{

                    $ext = pathinfo($val->attachfile, PATHINFO_EXTENSION);
                    if($ext=="png" or $ext=="PNG" or $ext=="jpg" or $ext=="jpeg" or $ext=="gif")
                    {
                        echo '<li class="pl-1 rounded mb-1"><div class="receive-msg"><div class="row"><div class="col-md-2" style="padding:0px;padding-left:5px;"><img src="'.base_url().'assets/default.jpg"></div><div class="col-md-10 receive-msg-desc mt-1 ml-1 pl-2 pr-2"><span class="dateandtime_class">'.date("Y-m-d",strtotime($val->dateandtime)).' '.date("H:i",strtotime($val->dateandtime)).'</span><div class="bgcolor" style="background:#767676c2"><div class="text-center" style="padding-top:5px;"><a href="'.base_url().'assets/chat_files/'.$val->attachfile.'" target="_blank"><img src="'.base_url().'assets/chat_files/'.$val->attachfile.'" width="200"><p class="image_name">'.substr($val->attachfile,0,15).'</p></a></div>'.$val->message_text. '</div></div></div></div></li>';
                    }
                    else
                    {
                        echo '<li class="pl-1 rounded mb-1"><div class="receive-msg"><div class="row"><div class="col-md-2" style="padding:0px;padding-left:5px;"><img src="'.base_url().'assets/default.jpg"></div><div class="col-md-10 receive-msg-desc mt-1 ml-1 pl-2 pr-2"><span class="dateandtime_class">'.date("Y-m-d",strtotime($val->dateandtime)).' '.date("H:i",strtotime($val->dateandtime)).'</span><div class="bgcolor" style="background:#767676c2"><div class="text-center" style="padding-top:5px;"><a href="'.base_url().'assets/chat_files/'.$val->attachfile.'" target="_blank"><img src="'.base_url().'assets/chat_files/file_upload_image.png" width="200"><p class="image_name">'.substr($val->attachfile,0,15).'</p></a></div>'.$val->message_text. '</div></div></div></div></li>';
                        // echo '<li class="pl-2 pr-2 bg-primary rounded text-white send-msg mb-1"><div class="text-center" style="padding-top:5px;"><a href="'.base_url().'assets/chat_files/'.$val->attachfile.'" target="_blank"><img src="'.base_url().'assets/chat_files/file_upload_image.png" width="200"><p>'.substr($val->attachfile,0,15).'</p></a></div>'.$val->message_text. '</li>';                        
                    }
                    // echo '<li class="pl-2 pr-2 bg-primary rounded text-white send-msg mb-1"><div class="text-center" style="padding-top:5px;"><img src="'.base_url().'assets/chat_files/'.$val->attachfile.'" width="150"></div>'.$val->message_text. '</li>';
                }
            }
            else
            {
                if($val->attachfile=="")
                {
                echo '<li class="p-1 rounded mb-1"><div class="receive-msg"><div class="row"><div class="col-md-2" style="padding:0px;padding-left:5px;"><img src="'.base_url().'assets/default.jpg"></div><div class="col-md-10 receive-msg-desc mt-1 ml-1 pl-2 pr-2"><span class="dateandtime_class">'.date("Y-m-d",strtotime($val->dateandtime)).' '.date("H:i",strtotime($val->dateandtime)).'</span><p class="pl-2 pr-2 rounded"><span>'.$val->message_text.'</span></p></div></div></div></li>';
                }
                else
                {
                    $ext = pathinfo($val->attachfile, PATHINFO_EXTENSION);
                    if($ext=="png" or $ext=="PNG" or $ext=="jpg" or $ext=="jpeg" or $ext=="gif")
                    {
                        echo '<li class="pl-1 rounded mb-1"><div class="receive-msg"><div class="row"><div class="col-md-2" style="padding:0px;padding-left:5px;"><img src="'.base_url().'assets/default.jpg"></div><div class="col-md-10 receive-msg-desc mt-1 ml-1 pl-2 pr-2"><span class="dateandtime_class">'.date("Y-m-d",strtotime($val->dateandtime)).' '.date("H:i",strtotime($val->dateandtime)).'</span><div class="bgcolor"><div class="text-center" style="padding-top:5px;"><a href="'.base_url().'assets/chat_files/'.$val->attachfile.'" target="_blank"><img src="'.base_url().'assets/chat_files/'.$val->attachfile.'" width="200"><p class="image_name">'.substr($val->attachfile,0,15).'</p></a></div>'.$val->message_text. '</div></div></div></div></li>';
                    }
                    else
                    {
                        echo '<li class="pl-1 rounded mb-1"><div class="receive-msg"><div class="row"><div class="col-md-2" style="padding:0px;padding-left:5px;"><img src="'.base_url().'assets/default.jpg"></div><div class="col-md-10 receive-msg-desc mt-1 ml-1 pl-2 pr-2"><span class="dateandtime_class">'.date("Y-m-d",strtotime($val->dateandtime)).' '.date("H:i",strtotime($val->dateandtime)).'</span><div class="bgcolor"><div class="text-center" style="padding-top:5px;"><a href="'.base_url().'assets/chat_files/'.$val->attachfile.'" target="_blank"><img src="'.base_url().'assets/chat_files/file_upload_image.png" width="200"><p class="image_name">'.substr($val->attachfile,0,15).'</p></a></div>'.$val->message_text. '</div></div></div></div></li>';
                    }
                }
            }
        }
    }


    function insertadminreply()
    {
        // echo 'i am here';
        // var_dump($_POST);
        $attachfile = trim(str_replace(" ", "_", $_FILES['chatfile']['name']));
        // echo $attachfile;exit;
        // $target = base_url()."assets/chat_files/";
        // move_uploaded_file($_FILES["chatfile"]["tmp_name"],$target.$attachfile);
        $this->chat_model->changeadmintypinguserdtatus_off($_POST["userid"]);
        $res = $this->chat_model->insertadminreplydata($_POST['userid'],$_POST['messagetext'],$attachfile);

        $this->do_fileupload($attachfile);
        if($res["attachfile"]=="")
        {
            // echo '<li class="pl-2 pr-2 rounded text-white send-msg mb-1">'.$res["message_text"]. '</li>';
            echo '<li class="p-1 rounded mb-1">
                <div class="receive-msg">
                <div class="row">
                <div class="col-md-2" style="padding:0px;padding-left:5px;">
                    <img src="'.base_url().'assets/default.jpg">
                </div>
                <div class="col-md-10 receive-msg-desc mt-1 ml-1 pl-2 pr-2">
                <span class="dateandtime_class">'.date("Y-m-d",strtotime($res["dateandtime"])).' '.date("H:i",strtotime($res["dateandtime"])).'</span>
                    <p class="pl-2 pr-2 rounded" style="background:#767676c2"><span>'.$res["message_text"].'</span></p>
                </div>
                </div>
                </div>
                </li>';
        }
        else
        {
            $ext = pathinfo($res["attachfile"], PATHINFO_EXTENSION);
            if($ext=="png" or $ext=="PNG" or $ext=="jpg" or $ext=="jpeg" or $ext=="gif")
            {
                // <div class="text-right dateandtime_class">'.date("Y-m-d",strtotime($res["dateandtime"])).' '.date("H:i",strtotime($res["dateandtime"])).'</div>
                // echo '<li class="pl-2 pr-2 bg-primary rounded text-white send-msg mb-1"><div class="text-center" style="padding-top:5px;"><a href="'.base_url().'assets/chat_files/'.$res["attachfile"].'" target="_blank"><img src="'.base_url().'assets/chat_files/'.$res["attachfile"].'" width="200"><p>'.substr($res["attachfile"],0,15).'</p></a></div>'.$res["message_text"]. '</li>';
                echo '<li class="pl-1 rounded mb-1"><div class="receive-msg"><div class="row"><div class="col-md-2" style="padding:0px;padding-left:5px;"><img src="'.base_url().'assets/default.jpg"></div><div class="col-md-10 receive-msg-desc mt-1 ml-1 pl-2 pr-2"><div class="bgcolor" style="background:#767676c2"><div class="text-center" style="padding-top:5px;"><a href="'.base_url().'assets/chat_files/'.$res["attachfile"].'" target="_blank"><img src="'.base_url().'assets/chat_files/'.$res["attachfile"].'" width="200"><p class="image_name">'.substr($res["attachfile"],0,15).'</p></a></div>'.$res["message_text"]. '</div></div></div></div></li>';
            }
            else
            {
                // echo '<li class="pl-2 pr-2 bg-primary rounded text-white send-msg mb-1"><div class="text-center" style="padding-top:5px;"><a href="'.base_url().'assets/chat_files/'.$res["attachfile"].'" target="_blank"><img src="'.base_url().'assets/chat_files/file_upload_image.png" width="200"><p>'.substr($res["attachfile"],0,15).'</p></a></div>'.$res["message_text"]. '</li>';
                echo '<li class="pl-1 rounded mb-1"><div class="receive-msg"><div class="row"><div class="col-md-2" style="padding:0px;padding-left:5px;"><img src="'.base_url().'assets/default.jpg"></div><div class="col-md-10 receive-msg-desc mt-1 ml-1 pl-2 pr-2"><div class="bgcolor" style="background:#767676c2"><div class="text-center" style="padding-top:5px;"><a href="'.base_url().'assets/chat_files/'.$res["attachfile"].'" target="_blank"><img src="'.base_url().'assets/chat_files/file_upload_image.png" width="200"><p class="image_name">'.substr($res["attachfile"],0,15).'</p></a></div>'.$res["message_text"]. '</div></div></div></div></li>';
            }
        }
    }

    public function do_fileupload($attachfile)
    { 
        $config['upload_path']   = 'assets/chat_files';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['overwrite']     = FALSE;
        $config['file_name']     = $attachfile;
        $config['max_size']      = '100000';
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('chatfile')) {
            $error = array(
                'error' => $this->upload->display_errors()
            );

        }      
    }

    function getvisitorschathistory()
    {
        $sound_user_status = $this->chat_model->getuserchatsoundstatus($_GET['userid']);
        $message_history = $this->chat_model->getvisitorchathistory($_GET['userid']);

        // echo "<pre>";
        // print_r($sound_user_status);
        // exit;
        if($sound_user_status['is_admin_typing']==1)
        {
            echo '<style>.typing_loader_none{display:block !important}</style>';
        }
        else{}
        foreach($message_history as $val){ 
            if($val->attachfile==""){
                echo '<div class="row"><div class="col-md-2" style="padding-left:0;"><img src="'.base_url().'assets/default.jpg" class="circle-rounded"></div><div class="col-md-10"><div class="users-chat-div2">
                   <div class="usercontent-div">';
                    echo '<p><strong>';
                    if($val->type==1){
                        echo'Admin';
                    }
                    else{
                        echo 'Me';
                    }
                     echo'</strong></p>';
                    echo '<p>'. $val->message_text.'</p>';
                    echo '<span class="real-chat-badge"><small>'.date('Y-m-d',strtotime($val->dateandtime)).' '.date('H:i',strtotime($val->dateandtime)).'</small> <i class="fa fa-check" aria-hidden="true"> </i><i class="fa fa-check" aria-hidden="true"></i></span>
                   </div>
                </div></div></div>';
             }
             else
             {
                echo '<div class="row"><div class="col-md-2" style="padding-left:0;"><img src="'.base_url().'assets/default.jpg" class="circle-rounded"></div><div class="col-md-10"><div class="users-chat-div2">
                   <div class="usercontent-div">';
                    echo '<p><strong>';
                    if($val->type==1){
                        echo'Admin';
                    }
                    else{
                        echo 'Me';
                    }
                     echo'</strong></p>';
                     echo'<p><img src="'.base_url().'assets/chat_files/'.$val->attachfile.'" width="150"></p>';
                    echo '<p>'. $val->message_text.'</p>';
                    echo '<span class="real-chat-badge"><small>'.date('Y-m-d',strtotime($val->dateandtime)).' '.date('H:i',strtotime($val->dateandtime)).'</small> <i class="fa fa-check" aria-hidden="true"> </i><i class="fa fa-check" aria-hidden="true"></i></span>
                   </div>
                </div></div></div>';
             }
        }

        if($sound_user_status["chat_sound_status"]==1)
        {
            echo "<script> var audio = new Audio('".base_url()."assets/chat_audio_sound.mp3');
                        audio.play(); </script>";
        }
    }

    function getallpendingmessages()
    {
        $res = $this->chat_model->getallpendingmessagescount();
        echo $res;
    }

    function changechatsound_status()
    {
        $res = $this->chat_model->updatechatsound_val();
        echo "Sound Done";
    }

    function change_is_chat_open_status()
    {
        $this->chat_model->updatechat_open_status();
        // echo "Open chat done";
    }

    function change_is_chat_open_status_to_off()
    {
        $this->chat_model->updatechat_open_status_off();
    }

    function change_is_chat_open_status_to_on()
    {
        $this->chat_model->updatechat_open_status_on();
    }

    function updatemessagesoundforuserdatadiv()
    {
        $this->chat_model->updatesoundstatusdivdata($_GET['userid']);
    }

    function changetypingstatus()
    {
        $this->chat_model->changetypinguserdtatus($_GET["userid"]);
        echo 'user typing active';
    }

    function changetypingstatus_off()
    {
        $this->chat_model->changetypinguserdtatus_off($_GET["userid"]);
        echo 'user typing inactive';
    }

    function changeadmintypingstatus()
    {
        $this->chat_model->changeadmintypinguserdtatus($_GET["userid"]);
        echo 'admin typing active';
    }

    function changeadmintypingstatus_off()
    {
         $this->chat_model->changeadmintypinguserdtatus_off($_GET["userid"]);
        echo 'admin typing inactive';
    }

    function changeuseractivestatusdataaway()
    {
        $this->chat_model->changeuserstatusactive($_GET['userid']);
    }

    function chageactivestatusdataoffline()
    {
        $this->chat_model->chageactivestatusdataofflinedata($_GET['userid']);
    }

    function likeamessage()
    {
        $this->chat_model->likeamessage($_GET['msgid']);       
    }

    function dislikeamessage()
    {
        $this->chat_model->dislikeamessage($_GET['msgid']);
    }

}