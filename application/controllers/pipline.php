<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pipline extends MY_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->lang->load('auth');
		$this->load->helper('language');
		if (!$this->basic_auth->is_login())
			redirect("admin", 'refresh');
		else
			$this->data['user'] = $this->basic_auth->user();
		$this->load->model('pipline_model');
		$this->load->model('request_model');
		$this->load->model('jobs_model');
		$this->load->model('calls_model');
		$this->load->model('support_model');
		
		$this->data['configuration'] = get_configuration();
	}

	public function index(){
		$this->data['css_type'] 	= array("form","datatable");
		$this->data['active_class'] = "pipline";
		$this->data['gmaps'] 		= false;
		$this->data['title'] 		= "Piplines";
		$this->data['title_link'] 	= base_url('admin/pipline');
		$this->data['content'] 		= 'admin/pipline/index';
        $data = [];
		$this->data['data'] = $this->pipline_model->getAll(array('delete_bit' => 0));
		$this->_render_page('templates/admin_template', $this->data);
	}

	public function add(){
		$this->data['css_type'] 	= array("form");
		$this->data['active_class'] = "pipline";
		$this->data['gmaps'] 	= false;
		$this->data['title'] 	= "Piplines";
		$this->data['title_link'] 	= base_url('admin/pipline');
		$this->data['subtitle'] = "Add Pipline";
		$this->data['content']  = 'admin/pipline/add';
		$this->data['operators'] = $this->pipline_model->getAllOperators();
		$this->data['quotes'] = $this->pipline_model->getAllQuotes();
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->addPipline();
		}
		$this->_render_page('templates/admin_template', $this->data);
	}

	public function addPipline(){
		$error = $this->pipline_model->pipline_validate(true);
		if (empty($error)) {
			$id = $this->pipline_model->create([
				'status' 	=> @$_POST['status'],
				'civility' 	=> @$_POST['civility'],
				'first_name' 	=> @$_POST['first_name'],
				'last_name' 	=> @$_POST['last_name'],
				'company' => @$_POST['company'],
				'phone' => @$_POST['phone'],
				'fax' => @$_POST['fax'],
				'email' => @$_POST['email'],
				'sales_operator' => @$_POST['sales_operator'],
				'address' => @$_POST['address'],
				'address1' => @$_POST['address1'],
				'zip_code' => @$_POST['zip_code'],
				'city' => @$_POST['city'],
				'description' => @$_POST['description'],
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' 	=> date('Y-m-d H:i:s'),
			]);
			if(!empty(@$_POST['quote_id1'])){
				for ($i=0; $i < count(@$_POST['quote_id1']); $i++) { 
					$note_data['pipline_id'] = $id;
					$note_data['date'] = @$_POST['date1'][$i];
					$note_data['time'] = @$_POST['time1'][$i];
					$note_data['reminder_type'] = @$_POST['reminder_type'][$i];
					$note_data['quote_id'] = @$_POST['quote_id1'][$i];
					$note_data['quote_status'] = @$_POST['quote_status1'][$i];
					$note_data['quote_date'] = @$_POST['quote_date1'][$i];
					$note_data['quote_since'] = @$_POST['quote_since1'][$i];
					$note_data['note'] = @$_POST['note'][$i];
					$this->pipline_model->createQuoteDetails($note_data, "vbs_piplines_notes");
				}
			}
			if(!empty(@$_POST['quote_id2'])){
				for ($i=0; $i < count(@$_POST['quote_id2']); $i++) { 
					$list_data['pipline_id'] = $id;
					$list_data['quote_id'] = @$_POST['quote_id2'][$i];
					$list_data['quote_status'] = @$_POST['quote_status2'][$i];
					$list_data['quote_date'] = @$_POST['quote_date2'][$i];
					$list_data['quote_since'] = @$_POST['quote_since2'][$i];
					$this->pipline_model->createQuoteDetails($list_data, "vbs_piplines_list");
				}
			}
			$this->session->set_flashdata('alert', [
				'message' => "Successfully Created.",
				'class' => "alert-success",
				'type' => "Success"
			]);
			redirect('admin/pipline');
		} else {
			$this->data['alert'] = [
				'message' => @$error[0],
				'class' => "alert-danger",
				'type' => "Error"
			];
		}
	}

	public function edit($id){
		$this->data['data']	= $this->pipline_model->get(['id' => $id]);
		$this->data['notes_data'] = $this->pipline_model->getNotes(['pipline_id' => $id]);
		$this->data['list_data'] = $this->pipline_model->getList(['pipline_id' => $id]);
		if($this->data['data'] != false) {
			$this->data['css_type'] = array("form");
			$this->data['active_class'] = "pipline";
			$this->data['gmaps'] 	= false;
			$this->data['title'] 	= "Piplines";
			$this->data['subtitle'] 	= 'Edit Pipline';
			$this->data['title_link'] 	= base_url('admin/pipline');
			$this->data['content']  = 'admin/pipline/edit';
			$this->data['operators'] = $this->pipline_model->getAllOperators();
			$this->data['quotes'] = $this->pipline_model->getAllQuotes();
			$this->_render_page('templates/admin_template', $this->data);
		} else show_404();
	}

	public function update($id){
		$error = $this->pipline_model->pipline_validate(true);
		$con = $this->pipline_model->get(['id' => $id]);
		if($con != false) {
			if (empty($error)) {
				$this->pipline_model->update([
					'status' 	=> @$_POST['status'],
					'civility' 	=> @$_POST['civility'],
					'first_name' 	=> @$_POST['first_name'],
					'last_name' 	=> @$_POST['last_name'],
					'company' => @$_POST['company'],
					'phone' => @$_POST['phone'],
					'fax' => @$_POST['fax'],
					'email' => @$_POST['email'],
					'sales_operator' => @$_POST['sales_operator'],
					'address' => @$_POST['address'],
					'address1' => @$_POST['address1'],
					'zip_code' => @$_POST['zip_code'],
					'city' => @$_POST['city'],
					'description' => @$_POST['description'],
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' 	=> date('Y-m-d H:i:s'),
				], $id);

				$this->pipline_model->delete1($id, "vbs_piplines_notes");
				if(!empty(@$_POST['quote_id1'])){
					for ($i=0; $i < count(@$_POST['quote_id1']); $i++) { 
						$note_data['pipline_id'] = $id;
						$note_data['date'] = @$_POST['date1'][$i];
						$note_data['time'] = @$_POST['time1'][$i];
						$note_data['reminder_type'] = @$_POST['reminder_type'][$i];
						$note_data['quote_id'] = @$_POST['quote_id1'][$i];
						$note_data['quote_status'] = @$_POST['quote_status1'][$i];
						$note_data['quote_date'] = @$_POST['quote_date1'][$i];
						$note_data['quote_since'] = @$_POST['quote_since1'][$i];
						$note_data['note'] = @$_POST['note'][$i];
						$this->pipline_model->createQuoteDetails($note_data, "vbs_piplines_notes");
					}
				}

				$this->pipline_model->delete1($id, "vbs_piplines_list");
				if(!empty(@$_POST['quote_id2'])){
					for ($i=0; $i < count(@$_POST['quote_id2']); $i++) { 
						$list_data['pipline_id'] = $id;
						$list_data['quote_id'] = @$_POST['quote_id2'][$i];
						$list_data['quote_status'] = @$_POST['quote_status2'][$i];
						$list_data['quote_date'] = @$_POST['quote_date2'][$i];
						$list_data['quote_since'] = @$_POST['quote_since2'][$i];
						$this->pipline_model->createQuoteDetails($list_data, "vbs_piplines_list");
					}
				}

				$this->session->set_flashdata('alert', [
						'message' => "Successfully Updated.",
						'class' => "alert-success",
						'type' => "Success"
				]);
			} else {
				$this->session->set_flashdata('alert', [
						'message' => @$error[0],
						'class' => "alert-danger",
						'type' => "Error"
				]);
			}

			// redirect('admin/pipline/'.$id.'/edit');
			redirect('admin/pipline');
		} else show_404();
	}

	public function delete($id){
		$this->pipline_model->delete($id);
		$this->session->set_flashdata('alert', [
				'message' => "Successfully deleted.",
				'class' => "alert-success",
				'type' => "Success"
		]);
		redirect('admin/pipline');
	}

	public function delete_data(){
		$array= $this->input->get("arr");
		$this->pipline_model->delete_data(implode(',', $array));
		$this->session->set_flashdata('alert', [
				'message' => "User successfully deleted.",
				'class' => "alert-success",
				'type' => "Success"
		]);
		echo json_encode(array('success' => $array));
	}
}