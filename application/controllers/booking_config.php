<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking_config extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->lang->load('auth');
		$this->lang->load('general');
		$this->load->helper('language');
		$this->load->helper('validate');
		if (!$this->basic_auth->is_login())
			redirect("admin", 'refresh');
		else
		$this->data['user'] = $this->basic_auth->user();
		// $this->load->model('bookings_model');
		$this->load->model('bookings_config_model');
		$this->load->model('request_model');
		$this->load->model('jobs_model');
		$this->load->model('support_model');
		$this->load->model('calls_model');
		$this->load->model('notes_model');
		$this->load->model('notifications_model');
		$this->data['configuration'] = get_configuration();

	}

	public static $table = "vehicle_categories";
	public function index(){
		
		$this->data['user'] = $this->basic_auth->user();
		$this->data['css_type'] 	= array("form","booking_datatable","datatable");
		$this->data['active_class'] = "booking";
		$this->data['gmaps'] 		= false;
		$this->data['title'] 		= 'Booking Configurations';
		$this->data['title_link'] 	= base_url('admin/booking_config');
		$this->data['content'] 		= 'admin/booking_config/indexv2';
	//	$this->load->model('calls_model');
	//	$this->load->model('request_model');
	//	$this->load->model('jobs_model');
   		$data = [];
   		
		$this->data['vat_data'] = $this->bookings_config_model->getAllVat();
		$this->data['statut_data'] = $this->bookings_config_model->getAllStatut();
	
		
		$this->data['service_cat'] = $this->bookings_config_model->getAllData('vbs_u_category_service');
		$this->data['payment_method'] = $this->bookings_config_model->getAllData('vbs_u_payment_method');
		$this->data['service'] = $this->bookings_config_model->getAllData('vbs_u_service');

		$this->data['client_category'] = $this->bookings_config_model->getAllData('vbs_u_client_category');
		$this->data['car_category'] = $this->bookings_config_model->getAllData('vbs_u_car_category');
		$this->data['ride_category'] = $this->bookings_config_model->getAllData('vbs_u_ride_category');
		$this->data['price_car'] = $this->bookings_config_model->getAllData('vbs_u_price_car');
		$this->data['price_van'] = $this->bookings_config_model->getAllData('vbs_u_price_van');
		$this->data['price_minibus'] = $this->bookings_config_model->getAllData('vbs_u_price_minibus');
		$this->data['price_car_type'] = $this->bookings_config_model->getAllData('vbs_u_price_car_type');
		$this->data['price_car_tpmr'] = $this->bookings_config_model->getAllData('vbs_u_price_car_tpmr');
		$this->data['price_van_tpmr'] = $this->bookings_config_model->getAllData('vbs_u_price_van_tpmr');
		$this->data['price_minibus_tpmr'] = $this->bookings_config_model->getAllData('vbs_u_price_minibus_tpmr');
		$this->data['poi_data'] = $this->bookings_config_model->booking_Config_getAll('vbs_u_poi');
		$this->data['poi_categories'] = $this->bookings_config_model->booking_Config_getAll('vbs_u_ride_category');
	     $this->data['api_data'] = $this->bookings_config_model->booking_Config_getAll('vbs_u_googleapi');
		 $this->data['disablecat_data'] = $this->bookings_config_model->booking_Config_getAll('vbs_u_disablecategory');
		$this->data['pricestatus'] = $this->bookings_config_model->getpricestatusdata('vbs_price_status');
		$data['bookings'] = $this->bookings_config_model->getAll();
		$data['jobs'] 	 = $this->jobs_model->getAll();
		$data['calls']   = $this->calls_model->getAll();
		$this->data['data'] = $this->bookings_config_model->getAll();
	$this->data['quoteconfig'] = $this->bookings_config_model->getAllData('vbs_quotes_config');
	$this->data['invoice'] = $this->bookings_config_model->getinvoicestatusdata('vbs_invoices_config');


			$this->_render_page('templates/admin_template', $this->data);
	}
		public function get_ajax_statut(){
		$id=(int)$_GET['statut_id'];

		if($id>0){
			
			$statut_data = $this->bookings_config_model->booking_Config_getAll('vbs_u_bookingstatut',['id' => $id]);
			?>
			
			<?php
			foreach($statut_data as $key => $statut){
				$result='<input type="hidden" name="statut_id" value="'.$statut->id.'">
				<div class="col-md-12">
                <div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span>Statut</span>
						<select class="form-control" name="statut" required >
							
							<option '; if ($statut->statut == "1") {$result.='selected="selected" '; }
							$result.=' value="1">Show</option>
							<option ';
							if ($statut->statut == "2") { $result.='selected="selected" '; }
							$result.=' value="2">Hide</option>
						</select>
				</div>
				</div>
				<div class="col-md-3" style="margin-top: 5px;">
				<div class="form-group">
					<span>Name Of Statut</span>
					<input type="text" class="form-control" name="name_of_statut" placeholder="" value="'.$statut->name_of_var.'">
				</div>
				</div></div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}



		public function deleteStatut(){
		$this->load->model('bookings_config_model');
		$id=$_POST['delet_statut_id'];
		$del=$this->bookings_config_model->deletestatut($id);
		if ($del==true) {
		$this->session->set_flashdata('alert', [
				'message' => "Successfully deleted.",
				'class' => "alert-success",
				'status_id' => "13",
				'type' => "Success"
		]);
		redirect('admin/booking_config');
		}
		else {
			$this->session->set_flashdata('alert', [
					'message' => "Please try again.",
					'class' => "alert-danger",
					'status_id' => "13",
					'type' => "Error"
			]);
			redirect('admin/booking_config');
		}
	}



	public function statutadd(){
		
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->statutstore();
		}else show_404();

	}
public function statutstore(){
			// $error = request_validate();
			if (empty($error)) {
				//$dob = to_unix_date(@$_POST['dob']);
				$user = $this->basic_auth->user();
			 	$user_id=$user->id;


				$id = $this->bookings_config_model->addStatut([
					'name_of_var' 		=> @$_POST['name_of_statut'],
					'statut' 		=> @$_POST['statut'],
					'created_at' 		=> @date('Y-m-d'),
					'time' 		=> @date("H : i"),
					'user_id' 		=> $user_id,
				]);
				// $this->data['alert'] = [
				// 	'message' => ': Successfully added a statut.',
				// 	'class' => "alert-success",
				// 	'type' => "Success"
				// ];
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a statut .",
					'class' => "alert-success",
					'status_id' => "13",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			} else {
					$this->session->set_flashdata('alert', [
					'message' => ": Please try again",
					'class' => "alert-danger",
					'status_id' => "13",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
				
			}
	}
	public function statutEdit(){
		
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['statut_id'];
			if (!empty($id)) {
			$update=$this->bookings_config_model->statutUpdate([
				'name_of_var' 		=> @$_POST['name_of_statut'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a statut.",
					'class' => "alert-success",
					'status_id' => "13",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			}else {
				$this->session->set_flashdata('alert', [
					'message' => ": Please try again",
					'class' => "alert-danger",
					'status_id' => "13",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
			}
		}else {
				$this->session->set_flashdata('alert', [
					'message' => ": Please try again",
					'class' => "alert-danger",
					'status_id' => "13",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
			}
	}else {
				$this->session->set_flashdata('alert', [
					'message' => ": Please try again",
					'class' => "alert-danger",
					'status_id' => "13",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
			}
	}
	public function vatadd(){
		$this->load->model('bookings_config_model');
		$this->data['vat_data'] = $this->bookings_config_model->getAllVat();
		$this->data['css_type'] 	= array("form","datatable");
		$this->data['active_class'] = "bookings_config";
		$this->data['title'] 		= $this->lang->line("bookings");
		$this->data['title_link'] 	= base_url('admin/booking_config');
		$this->data['content'] 		= 'admin/booking_config/indexv2';
		$this->data['active_status'] 		= '1';
		$this->load->model('calls_model');
		$this->load->model('request_model');
		$this->load->model('jobs_model');
		$data = [];
		$data['bookings'] = $this->bookings_config_model->getAll();
		$data['jobs'] 	 = $this->jobs_model->getAll();
		$data['calls']   = $this->calls_model->getAll();
		$this->data['data'] = $this->bookings_config_model->getAll();
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->vatstore();
		}
		$this->_render_page('templates/admin_template_v2', $this->data);

	}
	public function vatstore(){
			// $error = request_validate();
			if (empty($error)) {
				//$dob = to_unix_date(@$_POST['dob']);
				$user = $this->basic_auth->user();
			 	$user_id=$user->id;

				$id = $this->bookings_config_model->addVat([
					'name_of_var' 		=> @$_POST['name_of_vat'],
					'vat' 		=> @$_POST['vat'],
					'statut' 		=> @$_POST['vat_statut'],
					'created_at' 		=> @date('Y-m-d'),
					'time' 		=> @date("H : i"),
					'user_id' 		=> $user_id,
				]);
				// $this->data['alert'] = [
				// 	'message' => ': Successfully added a vat.',
				// 	'class' => "alert-success",
				// 	'type' => "Success"
				// ];
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a vat .",
					'class' => "alert-success",
					'status_id' => "1",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			} else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
			}
	}
	public function vatEdit(){
		$this->load->model('bookings_config_model');
		$this->data['css_type'] 	= array("form","datatable");
		$this->data['active_class'] = "bookings_config";
		$this->data['title'] 		= $this->lang->line("bookings");
		$this->data['title_link'] 	= base_url('admin/booking_config');
		$this->data['content'] 		= 'admin/booking_config/indexv2';
		$this->data['active_status'] 		= '1';
		$this->load->model('calls_model');
		$this->load->model('request_model');
		$this->load->model('jobs_model');
		$data = [];
		$data['bookings'] = $this->bookings_config_model->getAll();
		$data['jobs'] 	 = $this->jobs_model->getAll();
		$data['calls']   = $this->calls_model->getAll();
		$this->data['data'] = $this->bookings_config_model->getAll();
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['vat_id'];
			if (!empty($id)) {
			$update=$this->bookings_config_model->vatUpdate([
				'name_of_var' 		=> @$_POST['name_of_vat'],
				'vat' 		=> @$_POST['vat'],
				'statut' 		=> @$_POST['vat_statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a vat.",
					'class' => "alert-success",
					'status_id' => "1",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
			}
		}
	}
		$this->data['vat_data'] = $this->bookings_config_model->getAllVat();
		$this->_render_page('templates/admin_template_v2', $this->data);
	}
	public function deleteVat(){
		$this->load->model('bookings_config_model');
		$id=$_POST['delet_vat_id'];
		$del=$this->bookings_config_model->deletevat($id);
		if ($del==true) {
		$this->session->set_flashdata('alert', [
				'message' => "Successfully deleted.",
				'class' => "alert-success",
				'status_id' => "1",
				'type' => "Success"
		]);
		redirect('admin/booking_config');
		}
		else {
			$this->session->set_flashdata('alert', [
					'message' => "Please try again.",
					'class' => "alert-danger",
					'status_id' => "1",
					'type' => "Error"
			]);
			redirect('admin/booking_config');
		}
	}


	public function poiAdd(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->poistore();
		}else show_404();
	}
	public function poistore(){
			if (empty($error)) {
				$user = $this->basic_auth->user();
				$user_id=$user->id;
				$table='vbs_u_poi';
				$id = $this->bookings_config_model->booking_Config_Add($table,[
					'name' 		=> @$_POST['name'],
					'category_id' 		=> @$_POST['category'],
					'statut' 		=> @$_POST['poi_cat_statut'],
					'address' 		=> @$_POST['address'],
					'postal' 		=> @$_POST['postal'],
					'ville' 		=> @$_POST['ville'],
					'created_at' 		=> @date('Y-m-d'),
					'time' 		=> @date("H : i"),
					'user_id' 		=> @$user_id,
				]);
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a POI.",
					'class' => "alert-success",
					'status_id' => "7",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			} else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "7",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
	}
	public function poiEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['edt_po_id'];
			if (!empty($id)) {
				$table='vbs_u_poi';
				$update=$this->bookings_config_model->booking_Config_Update($table,[
					'name' 		=> @$_POST['name'],
					'category_id' 		=> @$_POST['category'],
					'address' 		=> @$_POST['address'],
					'postal' 		=> @$_POST['postal'],
					'ville' 		=> @$_POST['ville'],
					 'statut' 		=> @$_POST['poi_cat_statut'],
				], $id);
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully updated a POI.",
					'class' => "alert-success",
					'status_id' => "7",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			} else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "7",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
		}else show_404();
	}
	public function poiDel(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['poideleteid'];
			if (!empty($id)) {
				$table = "vbs_u_poi";
				$del=$this->bookings_config_model->booking_Config_Delete($table,$id);
				if ($del==true) {
				$this->session->set_flashdata('alert', [
						'message' => "Successfully record deleted.",
						'class' => "alert-success",
						'status_id' => "7",
						'type' => "Success"
				]);
				redirect('admin/booking_config');
				}
				else {
					$this->session->set_flashdata('alert', [
							'message' => "Please try again.",
							'class' => "alert-danger",
							'status_id' => "7",
							'type' => "Error"
					]);
					redirect('admin/booking_config');
				}
			}
		}else show_404();
	}
	public function get_ajax_poi(){
		$id=(int)$_GET['poi_id'];
		if($id>0){
			$this->load->model('bookings_config_model');
			$poi_data = $this->bookings_config_model->booking_Config_getAll('vbs_u_poi',array('id'=>$id));
			$poi_categories= $this->bookings_config_model->booking_Config_getAll('vbs_u_ride_category');
			foreach($poi_data as $key => $item){
				$result='<input type="hidden" class="edit_po_id" name="edt_po_id" value="'.$item->id.'">
				<div class="row" style="margin-top: 10px;">
				   		<div class="col-md-2" style="margin-top: 5px;">
							<div class="form-group">
								<span>Statut</span>
									<select class="form-control" name="poi_cat_statut" required >
										
										<option '; if ($item->statut == "1") {$result.='selected="selected" '; }
										$result.=' value="1">Show</option>
										<option ';
										if ($item->statut == "2") { $result.='selected="selected" '; }
										$result.=' value="2">Hide</option>
									</select>
						     	</div>
				            </div>
				    <div class="col-md-2" style="margin-top: 5px;">
						<div class="form-group">
							<span>Poi Caregory</span>
							<select class="form-control" name="category" required style="background: #fff !important;">';
                                   foreach ($poi_categories as $key => $cat){
								$result.='<option ';
								if ($item->category_id == $cat->id) {$result.='selected="selected" '; }
								$result.=' value="'.$cat->id.'">'.$cat->ride_cat_name.'</option>';
							}
							
							$result.='</select>
						</div>
					</div>        
					<div class="col-md-2" style="margin-top: 5px;">
						<div class="form-group">
						<span >Name of POI</span>
						<input type="text" class="edit_name form-control" name="name" placeholder="" value="'.$item->name.'" required>
						</div>
					</div>
	
				</div>
				<div class="row" style="margin-top: 10px;">
					<div class="col-md-2" style="margin-top: 5px;">
						<div class="form-group">
						<span >Address</span>
						<input type="text" class="form-control" name="address" placeholder="" value="'.$item->address.'" required>
						</div>
					</div>
					<div class="col-md-2" style="margin-top: 5px;">
						<div class="form-group">
							<span >Zip code</span>
							<input type="text" class="form-control" name="postal" placeholder="" value="'.$item->postal.'" required>
						</div>
					</div>
					<div class="col-md-2" style="margin-top: 5px;">
						<div class="form-group">
							<span >City</span>
							<input type="text" class="form-control" name="ville" placeholder="" value="'.$item->ville.'" required>
						</div>
					</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}

	function packageAdd(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			if (empty($error)) {
				$table='vbs_u_package';
				$user = $this->basic_auth->user();
				$user_id=$user->id;
				$id = $this->bookings_config_model->booking_Config_Add($table,[
					'name' 		=> @$_POST['name'],
					'status' 		=> @$_POST['status'],
					'd_type' 		=> @$_POST['d_type'],
					'departure' 		=> @$_POST['departure'],
					'destination_type' 		=> @$_POST['destination_type'],
					'destination' 		=> @$_POST['destination'],
					'p_car' 		=> @$_POST['p_car'],
					'p_minibus' 		=> @$_POST['p_minibus'],
					'p_van' 		=> @$_POST['p_van'],
					'p_tpmr_car' 		=> @$_POST['p_tpmr_car'],
					'p_tpmr_mini' 		=> @$_POST['p_tpmr_mini'],
					'p_tpmr_van' 		=> @$_POST['p_tpmr_van'],
					'created_at' 		=> @date('Y-m-d'),
					'time' 		=> @date("H : i"),
					'user_id' 		=> @$user_id,
				]);
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a package.",
					'class' => "alert-success",
					'status_id' => "8",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			} else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "8",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
		}else show_404();
	}
	function packageEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['edit_package_id'];
			if (!empty($id)) {
				$table='vbs_u_package';
				$update=$this->bookings_config_model->booking_Config_Update($table,[
					'name' 		=> @$_POST['name'],
					'status' 		=> @$_POST['status'],
					'd_type' 		=> @$_POST['d_type'],
					'departure' 		=> @$_POST['departure'],
					'destination_type' 		=> @$_POST['destination_type'],
					'destination' 		=> @$_POST['destination'],
					'p_car' 		=> @$_POST['p_car'],
					'p_minibus' 		=> @$_POST['p_minibus'],
					'p_van' 		=> @$_POST['p_van'],
					'p_tpmr_car' 		=> @$_POST['p_tpmr_car'],
					'p_tpmr_mini' 		=> @$_POST['p_tpmr_mini'],
					'p_tpmr_van' 		=> @$_POST['p_tpmr_van']
				], $id);
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully updated a POI.",
					'class' => "alert-success",
					'status_id' => "8",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			} else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "8",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
		}else show_404();
	}
	function packageDel(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['packagedeleteid'];
			if (!empty($id)) {
				$table = "vbs_u_package";
				$del=$this->bookings_config_model->booking_Config_Delete($table,$id);
				if ($del==true) {
				$this->session->set_flashdata('alert', [
						'message' => "Successfully record deleted.",
						'class' => "alert-success",
						'status_id' => "8",
						'type' => "Success"
				]);
				redirect('admin/booking_config');
				}
				else {
					$this->session->set_flashdata('alert', [
							'message' => "Please try again.",
							'class' => "alert-danger",
							'status_id' => "8",
							'type' => "Error"
					]);
					redirect('admin/booking_config');
				}
			}
		}else show_404();
	}
	function get_ajax_package(){

		$id=(int)$_GET['package_id'];
		if($id>0){
			$this->load->model('bookings_config_model');
			$package_data = $this->bookings_config_model->booking_Config_getAll('vbs_u_package',array('id'=>$id));
			
			$poi_categories = $this->bookings_config_model->booking_Config_getAll('vbs_u_ride_category');
			foreach($package_data as $key => $item){
				$departurepoi_data = $this->bookings_config_model->booking_Config_getAll('vbs_u_poi',array('category_id'=>$item->d_type));
				$destinationpoi_data = $this->bookings_config_model->booking_Config_getAll('vbs_u_poi',array('category_id'=>$item->destination_type));
				$result='<input type="hidden" class="edit_package_id" name="edit_package_id" value="'.$item->id.'">
					<div class="row" style="margin-top: 10px;">

					 	<div class="col-md-2" style="margin-top: 5px;">
							<div class="form-group">
							<span style="font-weight: bold;">Statut</span>
							<select class="form-control" name="status" required style="background: #fff !important;">
								<option ';
								if ($item->status == "1") {$result.='selected="selected" '; }
								$result.='value="1">Show</option>
								<option ';
								if ($item->status == "0") {$result.='selected="selected" '; }
								$result.='value="0">Hide</option>
							</select>
							</div>
						</div>
					  <div class="col-md-2" style="margin-top: 5px;">
						<div class="form-group">
							<span style="font-weight: bold;">Package Name</span>
							<input type="text" class="form-control" name="name" placeholder="" value="'.$item->name.'" required>
							</div>
						</div>
					    <div class="col-md-2" style="margin-top: 5px;">
			                <div class="form-group">
			                  <span style="font-weight: bold;">Depart Poi Category</span>
			                  <select class="form-control" id="editdeparturetype_id" name="d_type" required style="background: #fff !important;" onchange="editadddeparture()">
			                      <option value="">Select</option>';
			                      foreach($poi_categories as $key => $cat){
			                          $result.='<option ';
								if ($item->d_type ==  $cat->id) {$result.='selected="selected" '; }
								$result.='value="'.$cat->id.'">'.$cat->ride_cat_name.'</option>';
			                      }
			                      
			                  $result.='</select>
			                </div>
			            </div>
						<div class="col-md-2" style="margin-top: 5px;">
							<div class="form-group">
							<span style="font-weight: bold;">Depart Poi</span>
							<select class="form-control" id="editdeparture_id" name="departure" required style="background: #fff !important;">
								<option value="">Select</option>';
								 foreach($departurepoi_data as $key => $poi){
								$result.='<option ';
								if ($item->departure ==  $poi->id) {$result.='selected="selected" '; }
								$result.='value="'.$poi->id.'">'.$poi->name.'</option>';
								}
							$result.='</select>
							</div>
						</div>
						 <div class="col-md-2" style="margin-top: 5px;">
			                <div class="form-group">
			                  <span style="font-weight: bold;">Destination Poi Category</span>
			                  <select class="form-control" id="editdestinationtype_id" name="destination_type" required style="background: #fff !important;" onchange="editadddestination()">
			                      <option value="">Select</option>';
			                      foreach($poi_categories as $key => $cat){
			                          $result.='<option ';
								if ($item->destination_type ==  $cat->id) {$result.='selected="selected" '; }
								$result.='value="'.$cat->id.'">'.$cat->ride_cat_name.'</option>';
			                      }
			                      
			                  $result.='</select>
			                </div>
			            </div>
						<div class="col-md-2" style="margin-top: 5px;">
							<div class="form-group">
							<span style="font-weight: bold;">Destination Poi</span>
							<select class="form-control" id="editdestination_id" name="destination" required style="background: #fff !important;">
								<option value="">Select</option>';
								 foreach($destinationpoi_data as $key => $poi){
								$result.='<option ';
								if ($item->destination ==  $poi->id) {$result.='selected="selected" '; }
								$result.='value="'.$poi->id.'">'.$poi->name.'</option>';
								}
							$result.='</select>
							</div>
						</div>
				</div>
				
				<div class="row" style="margin-top: 10px;">
					<div class="col-md-2" style="margin-top: 5px;">
						<div class="form-group">
							<span style="font-weight: bold;">Price(Car)</span>
							<input type="number" class="form-control" name="p_car" placeholder="" value="'.$item->p_car.'" required>
						</div>
					</div>
					<div class="col-md-2" style="margin-top: 5px;">
						<div class="form-group">
							<span style="font-weight: bold;">Price(Van)</span>
							<input type="number" class="form-control" name="p_van" placeholder="" value="'.$item->p_van.'" required>
						</div>
					</div>
					<div class="col-md-2" style="margin-top: 5px;">
						<div class="form-group">
							<span style="font-weight: bold;">Price(Minibus)</span>
							<input type="number" class="form-control" name="p_minibus" placeholder="" value="'.$item->p_minibus.'" required>
						</div>
					</div>
					
					
				</div>
				<div class="row" style="margin-top: 10px;">
					<div class="col-md-2" style="margin-top: 5px;">
						<div class="form-group">
							<span style="font-weight: bold;">Price(Car Access)</span>
							<input type="number" class="form-control" name="p_tpmr_car" placeholder="" value="'.$item->p_tpmr_car.'" required>
						</div>
					</div>
					<div class="col-md-2" style="margin-top: 5px;">
						<div class="form-group">
							<span style="font-weight: bold;">Price(Van Access)</span>
							<input type="number" class="form-control" name="p_tpmr_van" placeholder="" value="'.$item->p_tpmr_van.'" required>
						</div>
					</div>
					<div class="col-md-2" style="margin-top: 5px;">
						<div class="form-group">
							<span style="font-weight: bold;">Price(Minibus Access)</span>
							<input type="number" class="form-control" name="p_tpmr_mini" placeholder="" value="'.$item->p_tpmr_mini.'" required>
						</div>
					</div>
					
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	
	// Service Category
	public function servicecatAdd(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->service_cat_store();
		}else show_404();
	}
	public function service_cat_store(){
		
		
			// $error = request_validate();
			if (empty($error)) {
				//$dob = to_unix_date(@$_POST['dob']);
				$paymentmethod=$_POST['paymentmethod'];
				$user = $this->basic_auth->user();
				$user_id=$user->id;
				$table='vbs_u_category_service';
				$id = $this->bookings_config_model->booking_Config_Add($table,[
					'category_name' 		=> @$_POST['name_of_category'],
					'statut' 		=> @$_POST['service_cat_statut'],
					'invoice_periode' 		=> @$_POST['invoice_periode'],
					'created_at' 		=> @date('Y-m-d'),
					'time' 		=> @date("H : i"),
					'user_id' 		=> $user_id
				]);
				for($i=0;$i<count($paymentmethod);$i++){				
                 $payment_category_id= $this->bookings_config_model->booking_Config_Add('vbs_servicepayment_category',[
                      'paymentmethod_id'=> $paymentmethod[$i],
                      'servicecategory_id'=>$id

                	]);
                	}
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a service category .",
					'class' => "alert-success",
					'status_id' => "3",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			} else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
	}
	public function servicecatEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['service_id'];
			if (!empty($id)) {
			$paymentmethod=$_POST['paymentmethod'];
			$table='vbs_u_category_service';
			$update=$this->bookings_config_model->booking_Config_Update($table,[
				'category_name' 		=> @$_POST['name_of_category'],
				'statut' 		=> @$_POST['service_cat_statut'],
				'invoice_periode' 		=> @$_POST['invoice_periode']
			], $id);
			$delpaymentmethod=$this->bookings_config_model->booking_catpaymentConfig_Delete('vbs_servicepayment_category',$id);
			for($i=0;$i<count($paymentmethod);$i++){				
                 $payment_category_id= $this->bookings_config_model->booking_Config_Add('vbs_servicepayment_category',[
                      'paymentmethod_id'=> $paymentmethod[$i],
                      'servicecategory_id'=>$id

                	]);
                	}
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Service Category.",
					'class' => "alert-success",
					'status_id' => "3",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
		}
	}else show_404();

	}
	public function servicecatDelete(){
	$this->load->model('bookings_config_model');
	$id=$_POST['delet_service_cat_id'];
	$table = "vbs_u_category_service";
	$del=$this->bookings_config_model->booking_Config_Delete($table,$id);
	$delpaymentmethod=$this->bookings_config_model->booking_catpaymentConfig_Delete('vbs_servicepayment_category',$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "3",
			'type' => "Success"
	]);
	redirect('admin/booking_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "3",
				'type' => "Error"
		]);
		redirect('admin/booking_config');
	}
	show_404();
	}

	// Service
	public function serviceAdd(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->service_store();
		}else show_404();
	}
	public function service_store(){
			// $error = request_validate();
			if (empty($error)) {
				//$dob = to_unix_date(@$_POST['dob']);
				$user = $this->basic_auth->user();
				$user_id=$user->id;
				$table='vbs_u_service';
				$id = $this->bookings_config_model->booking_Config_Add($table,[
					'service_name' 		=> @$_POST['service_name'],
					'service_category' 		=> @$_POST['service_category'],
					'statut' 		=> @$_POST['service_statut'],
					'tva' 		=> @$_POST['tva'],
					'created_at' 		=> @date('Y-m-d'),
					'time' 		=> @date("H : i"),
					'user_id' 		=> @$user_id,
				]);
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a service.",
					'class' => "alert-success",
					'status_id' => "4",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			} else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
	}
	public function serviceEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['service_id'];
			if (!empty($id)) {
			$table='vbs_u_service';
			$update=$this->bookings_config_model->booking_Config_Update($table,[
				'service_name' 		=> @$_POST['service_name'],
				'service_category' 		=> @$_POST['service_category'],
				'statut' 		=> @$_POST['service_statut'],
				'tva' 		=> @$_POST['tva'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Service.",
					'class' => "alert-success",
					'status_id' => "4",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
		}
	}else show_404();

	}
	public function serviceDelete(){
	$this->load->model('bookings_config_model');
	$id=$_POST['delet_service_id'];
	$table = "vbs_u_service";
	$del=$this->bookings_config_model->booking_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "4",
			'type' => "Success"
	]);
	redirect('admin/booking_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "4",
				'type' => "Error"
		]);
		redirect('admin/booking_config');
	}
	show_404();
	}

	// Client Category
	public function clientCatAdd(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->client_cat_store();
		}else show_404();
	}
	public function client_cat_store(){
			// $error = request_validate();
			if (empty($error)) {
				//$dob = to_unix_date(@$_POST['dob']);
				$user = $this->basic_auth->user();
				$user_id=$user->id;
				$table='vbs_u_client_category';
				$id = $this->bookings_config_model->booking_Config_Add($table,[
					'client_cat_name' 		=> @$_POST['client_cat_name'],
					'statut' 		=> @$_POST['client_cat_statut'],
					'delay_of_payment' 		=> @$_POST['delay_of_payment'],
					'created_at' 		=> @date('Y-m-d'),
					'time' 		=> @date("H : i"),
					'user_id' 		=> @$user_id,
				]);
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a client category.",
					'class' => "alert-success",
					'status_id' => "5",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			} else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
	}
	public function clientCatEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['clientcat_id'];
			if (!empty($id)) {
			$table='vbs_u_client_category';
			$update=$this->bookings_config_model->booking_Config_Update($table,[
				'client_cat_name' 		=> @$_POST['client_cat_name'],
				'delay_of_payment' 		=> @$_POST['delay_of_payment'],
				'statut' 		=> @$_POST['client_cat_statut']
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a client category.",
					'class' => "alert-success",
					'status_id' => "5",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
		}
	}else show_404();

	}
	public function clientCatDelete(){
	$this->load->model('bookings_config_model');
	$id=$_POST['delet_client_id'];
	$table = "vbs_u_client_category";
	$del=$this->bookings_config_model->booking_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "5",
			'type' => "Success"
	]);
	redirect('admin/booking_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "5",
				'type' => "Error"
		]);
		redirect('admin/booking_config');
	}
	show_404();
	}
	public function get_ajax_client_cat(){
		$id=(int)$_GET['client_cat_id'];

		if($id>0){
			$this->load->model('bookings_config_model');
			$client_cat_data = $this->bookings_config_model->booking_Config_getAll('vbs_u_client_category',['id' => $id]);
			// print_r($client_cat);
			// foreach($client_cat as $key => $item){
			?>
			
			<?php
			$payment_methods = $this->bookings_config_model->getAllData('vbs_u_payment_method');
			foreach($client_cat_data as $key => $client_cat){
				$result='<input type="hidden" name="clientcat_id" value="'.$client_cat->id.'">
				<div class="col-md-12">
			<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span>Statut</span>
						<select class="form-control" name="client_cat_statut" required>
							
							<option '; 
							if ($client_cat->statut == "1") {$result.='selected="selected" '; }
							$result.=' value="1">Show</option>
							<option ';
							if ($client_cat->statut == "2") { $result.='selected="selected" '; }
							$result.=' value="2">Hide</option>
						</select>
				</div>
			</div>
			<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span>Client Category</span>
					<input type="text" class="form-control" name="client_cat_name" placeholder="" value="'.$client_cat->client_cat_name.'">
				</div>
			</div>
			

			
			<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span >Delay of payment (days) </span>
					  <select class="form-control" name="delay_of_payment" required>
				            <option ';
							if ($client_cat->delay_of_payment == "0") { $result.='selected="selected" '; }
							$result.=' value="0">0</option>
				            <option ';
							if ($client_cat->delay_of_payment == "15") { $result.='selected="selected" '; }
							$result.=' value="15">15</option>
				            <option ';
							if ($client_cat->delay_of_payment == "30") { $result.='selected="selected" '; }
							$result.=' value="30">30</option>
				            <option ';
							if ($client_cat->delay_of_payment == "45") { $result.='selected="selected" '; }
							$result.=' value="45">45</option>
				            <option ';
							if ($client_cat->delay_of_payment == "60") { $result.='selected="selected" '; }
							$result.=' value="60">60</option>
				        </select>
					
				</div>
				</div>
				</div>
			';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	// payment method
	public function paymentmethodAdd(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->payment_method_store();
		}else show_404();
	}
	public function payment_method_store(){
			// $error = request_validate();
			if (empty($error)) {
				//$dob = to_unix_date(@$_POST['dob']);
				$user = $this->basic_auth->user();
				$user_id=$user->id;
				     $card_expirydate=$_POST['card_expirydate'];
				   	 $card_expirydate=str_replace('/', '-', $card_expirydate);
					 $card_expirydate=strtotime($card_expirydate);
					 $card_expirydate = date('Y-m-d',$card_expirydate);
				$table='vbs_u_payment_method';
  

              if($_POST['category_client'] == 1 || $_POST['category_client'] == 2){
               //check this category of payment exist 
              	$isexist=$this->bookings_config_model->booking_Config_getAll($table,['category_client'=>$_POST['category_client']]);
	              	if($isexist){

	                  $this->session->set_flashdata('alert', [
						'message' => ': This category is already exist! Please try again.',
						'class' => "alert-danger",
						'status_id' => "11",
						'type' => "Error"
					]);
					redirect('admin/booking_config');
	              	}
              	$description="";
              }else{
                $description=$_POST['description'];
              }		
                
				$id = $this->bookings_config_model->booking_Config_Add($table,[
					'category_client' 		=> @$_POST['category_client'],
					'status' 		=> @$_POST['payment_method_statut'],
					'name' 		=> @$_POST['name'],
					'description' 		=> $description,
					'created_at' 		=> @date('Y-m-d'),
					'time' 		=> @date("H : i"),
					'user_id' 		=> $user_id,
					'paymentorder'=> @$_POST['paymentorder']
				]);
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a payment method .",
					'class' => "alert-success",
					'status_id' => "11",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			
			} else {
				$this->session->set_flashdata('alert', [
					'message' => ': Please try again',
					'class' => "alert-danger",
					'status_id' => "11",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
			}
	}
	public function paymentmethodEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['service_id'];
			if (!empty($id)) {
                     $card_expirydate=$_POST['card_expirydate'];
				   	 $card_expirydate=str_replace('/', '-', $card_expirydate);
					 $card_expirydate=strtotime($card_expirydate);
					 $card_expirydate = date('Y-m-d',$card_expirydate);
			$table='vbs_u_payment_method';
			 if($_POST['category_client'] == 1 || $_POST['category_client'] == 2){
			 	  //check this category of payment exist 
              	$isexist=$this->bookings_config_model->booking_Config_getAll($table,['category_client'=>$_POST['category_client'],'id !='=> $id]);
	              	if($isexist){

	                  $this->session->set_flashdata('alert', [
						'message' => ': This category is already exist! Please try again.',
						'class' => "alert-danger",
						'status_id' => "11",
						'type' => "Error"
					]);
					redirect('admin/booking_config');
	              	}
              	$description="";
              }else{
                $description=$_POST['description'];
              }	
               
			$update=$this->bookings_config_model->booking_Config_Update($table,[
				'category_client' 		=> @$_POST['category_client'],
					'status' 		=> @$_POST['payment_method_statut'],
					'name' 		=> @$_POST['name'],
					'description' 		=>  $description,
					'paymentorder'=> @$_POST['paymentorder']
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a payment Method.",
					'class' => "alert-success",
					'status_id' => "11",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			}else {
				$this->session->set_flashdata('alert', [
					'message' => ': Please try again',
					'class' => "alert-danger",
					'status_id' => "11",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
			}
		
		}else {
				$this->session->set_flashdata('alert', [
					'message' => ': Please try again',
					'class' => "alert-danger",
					'status_id' => "11",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
			}
	}else show_404();

	}
	public function paymentmethodDelete(){
	$this->load->model('bookings_config_model');
	$id=$_POST['delet_payment_method_id'];
	$table = "vbs_u_payment_method";
	$del=$this->bookings_config_model->booking_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "11",
			'type' => "Success"
	]);
	redirect('admin/booking_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "11",
				'type' => "Error"
		]);
		redirect('admin/booking_config');
	}
	show_404();
	}
	// Car Category
	public function carCatAdd(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->car_cat_store();
		}else show_404();
	}
	public function car_cat_store(){

		   $passengercap=$this->input->post('passengercap');
            $lugagescap=$this->input->post("lugagescap");
            $wheelchairscap=$this->input->post("wheelchairscap");
            $babycap=$this->input->post("babycap");
            $animalscap=$this->input->post("animalscap");
          
           
          
			// $error = request_validate();
			if (empty($error)) {
				//$dob = to_unix_date(@$_POST['dob']);
		        $file=$_FILES['caricon'];
                $fileName=$file['name'];
                $fileExt=explode('.',$fileName);
                $fileActEct=strtolower(end($fileExt));
                $fileName=time().".".$fileActEct;
                $fileNewName='uploads/vehicle_images/'.$fileName;
                $path_to_folder = 'uploads/vehicle_images/';
                $config['upload_path'] = realpath($path_to_folder);
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['max_size']     = '3000000'; // 3mb
                $config['file_name']    = $fileName;
        
                $this->load->library('upload', $config);
              
                if ($this->upload->do_upload('caricon'))
                { 		
                	
				$user = $this->basic_auth->user();
				$user_id=$user->id;
				$table='vbs_u_car_category';
				$id = $this->bookings_config_model->booking_Config_Add($table,[
					'car_cat_name' 		=> @$_POST['car_cat_name'],
					'statut' 		=> @$_POST['car_cat_statut'],
					'caricon' =>$fileNewName,
					'cartype'=>@$_POST['cartype'],
					'created_at' 		=> @date('Y-m-d'),
					'time' 		=> @date("H : i"),
					'user_id' 		=> @$user_id,
				]);
                for($i=0;$i<count($passengercap);$i++){
            			$cid = $this->bookings_config_model->booking_Config_Add("vbs_car_configuration",[
							'passengers' =>$passengercap[$i],
							'baby' 		 =>$babycap[$i],
							'wheelchairs'=>$wheelchairscap[$i],
							'luggage' 	 =>$lugagescap[$i],
							'animals' 	 =>$animalscap[$i],
							'car_id' 	 =>$id
		  				]);
                     }
           
         
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a car category.",
					'class' => "alert-success",
					'status_id' => "6",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			}else{
				
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
			} else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
	}

	public function carCatEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['carcat_id'];
			if (!empty($id)) {
			  if(!empty($_FILES['caricon']['name']))
                 {
			    $file=$_FILES['caricon'];
                $fileName=$file['name'];
                $fileExt=explode('.',$fileName);
                $fileActEct=strtolower(end($fileExt));
                $fileName=time().".".$fileActEct;
                $fileNewName='uploads/vehicle_images/'.$fileName;
                $path_to_folder = 'uploads/vehicle_images/';
                $config['upload_path'] = realpath($path_to_folder);
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['max_size']     = '3000000'; // 3mb
                $config['file_name']    = $fileName;
        
                $this->load->library('upload', $config);
              
                if ($this->upload->do_upload('caricon'))
                { 

			$table='vbs_u_car_category';
			$update=$this->bookings_config_model->booking_Config_Update($table,[
				'car_cat_name' 		=> @$_POST['car_cat_name'],
				'statut' 		=> @$_POST['car_cat_statut'],
				'cartype'=>@$_POST['cartype'],
				'caricon'=>$fileNewName
			], $id);
			}else{
			$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
	      	}
	      }else{
	      	$table='vbs_u_car_category';
			$update=$this->bookings_config_model->booking_Config_Update($table,[
				'car_cat_name' 		=> @$_POST['car_cat_name'],
				'statut' 		=> @$_POST['car_cat_statut'],
				'cartype'=>@$_POST['cartype'],
			
			], $id);
	      }

		
	 
           //add car configuration
			  $configdel=$this->bookings_config_model->booking_Carconfig_Delete("vbs_car_configuration",$id);
         
           
           if($this->input->post("passengercap")){
            $passengercap=$this->input->post('passengercap');
            $lugagescap=$this->input->post("lugagescap");
            $wheelchairscap=$this->input->post("wheelchairscap");
            $babycap=$this->input->post("babycap");
            $animalscap=$this->input->post("animalscap");

              for($i=0;$i<count($passengercap);$i++){
            			$cid = $this->bookings_config_model->booking_Config_Add("vbs_car_configuration",[
							'passengers' =>$passengercap[$i],
							'baby' 		 =>$babycap[$i],
							'wheelchairs'=>$wheelchairscap[$i],
							'luggage' 	 =>$lugagescap[$i],
							'animals' 	 =>$animalscap[$i],
							'car_id' 	 =>$id
		  				]);
                     }
              }       
			//end configuration
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a car category.",
					'class' => "alert-success",
					'status_id' => "6",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
		
	}
	}else show_404();

	}
   //delete configuration
public function delete_car_config(){
	$id=(int)$_GET['config_id'];
	$record=$this->bookings_config_model->getconfigurationrecord('vbs_car_configuration',$id);
     $del=$this->bookings_config_model->booking_Config_Delete('vbs_car_configuration',$id);
	$car_configuration_data= $this->bookings_config_model->booking_Config_getAll('vbs_car_configuration',['car_id' => $record['car_id']]);
    $result="";
     
  			if($car_configuration_data){
        $result.='<div class="col-md-6" style="position:relative;width: 54%;">
       <button type="button" class="btn btn-default"  onclick="editboxes();" style="z-index:10;position:absolute;top: 11px;right: -39px;border: none;cursor:pointer;overflow: hidden;outline:none;font-size: 22px;color: #414441;height: 46px;line-height: 35px;"><i class="fa fa-plus"></i></button>
     </div>';}
     else{
     	   $result.='<div class="col-md-6" style="position:relative;width: 54%;">
       <button type="button" class="btn btn-default"  onclick="editboxes();" style="z-index:10;position:absolute;top: 11px;right: -39px;border: none;cursor:pointer;overflow: hidden;outline:none;font-size: 22px;color: #414441;height: 46px;line-height: 35px;"><i class="fa fa-plus"></i></button>
     </div>';
     $result.=' <div class="capacitybox" >
          <div class="col-md-6" style="margin-top:10px;margin-left: 1.5%;background-image: linear-gradient(to bottom, #fff 0, #e0e0e0 100%);border-color: #ccc;padding: 6px 12px;"> 

             <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6 text-right">
                    <span>Passengers</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="passengercap[]" style="width:100%;" class="form-control" required>
                     
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
                  </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right">
                    <span>Luggage</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
 
                  <select name="lugagescap[]" style="width:100%;" class="form-control" required>
                     
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
                </div>
             </div>
             <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right">
                    <span>Wheelchairs</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="wheelchairscap[]" style="width:100%;" class="form-control" required>
                    
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
                </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right">
                    <span>Babies</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="babycap[]" style="width:100%;" class="form-control" required>
                     
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
              </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right">
                    <span>Animals</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="animalscap[]" style="width:100%;" class="form-control" required>
                    
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
               </div>
             </div>
          
           </div>  
         </div>';
     }
		 	foreach ($car_configuration_data as $key => $car_Config) {
		 			$result.='<div class="capacitybox">
		 			<input type="hidden" name="carconfigid[]" value="'.$car_Config->id.'">
         <div class="col-md-6" style="margin-top:10px;margin-left: 1.5%;background-image: linear-gradient(to bottom, #fff 0, #e0e0e0 100%);border-color: #ccc;padding: 6px 12px;"> 

             <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6 text-right">
                    <span>Passengers</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="passengercap[]" style="width:100%;" class="form-control" required>
                     
                      <option'; if ($car_Config->passengers  == "0") {$result.=' selected="selected" '; }
							$result.=' value="0">0</option>
                      <option'; if ($car_Config->passengers  == "1") {$result.=' selected="selected" '; }
							$result.=' value="1">1</option>
                      <option'; if ($car_Config->passengers  == "2") {$result.=' selected="selected" '; }
							$result.=' value="2">2</option>
                      <option'; if ($car_Config->passengers  == "3") {$result.=' selected="selected" '; }
							$result.=' value="3">3</option>
                      <option'; if ($car_Config->passengers  == "4") {$result.=' selected="selected" '; }
							$result.=' value="4">4</option>
                      <option'; if ($car_Config->passengers  == "5") {$result.=' selected="selected" '; }
							$result.=' value="5">5</option>
                      <option'; if ($car_Config->passengers  == "6") {$result.=' selected="selected" '; }
							$result.=' value="6">6</option>
                      <option'; if ($car_Config->passengers  == "7") {$result.=' selected="selected" '; }
							$result.=' value="7">7</option>
                      <option'; if ($car_Config->passengers  == "8") {$result.=' selected="selected" '; }
							$result.=' value="8">8</option>
                      <option'; if ($car_Config->passengers  == "9") {$result.=' selected="selected" '; }
							$result.=' value="9">9</option>
                      <option'; if ($car_Config->passengers  == "10") {$result.=' selected="selected" '; }
							$result.=' value="10">10</option>
                  </select> 

                </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right">
                    <span>Luggage</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">

                  <select name="lugagescap[]" style="width:100%;" class="form-control" required>
                    
                      <option'; if ($car_Config->luggage   == "0") {$result.=' selected="selected" '; }
							$result.=' value="0">0</option>
                      <option'; if ($car_Config->luggage   == "1") {$result.=' selected="selected" '; }
							$result.=' value="1">1</option>
                      <option'; if ($car_Config->luggage   == "2") {$result.=' selected="selected" '; }
							$result.=' value="2">2</option>
                      <option'; if ($car_Config->luggage   == "3") {$result.=' selected="selected" '; }
							$result.=' value="3">3</option>
                      <option'; if ($car_Config->luggage   == "4") {$result.=' selected="selected" '; }
							$result.=' value="4">4</option>
                      <option'; if ($car_Config->luggage   == "5") {$result.=' selected="selected" '; }
							$result.=' value="5">5</option>
                      <option'; if ($car_Config->luggage   == "6") {$result.=' selected="selected" '; }
							$result.=' value="6">6</option>
                      <option'; if ($car_Config->luggage   == "7") {$result.=' selected="selected" '; }
							$result.=' value="7">7</option>
                      <option'; if ($car_Config->luggage   == "8") {$result.=' selected="selected" '; }
							$result.=' value="8">8</option>
                      <option'; if ($car_Config->luggage   == "9") {$result.=' selected="selected" '; }
							$result.=' value="9">9</option>
                      <option'; if ($car_Config->luggage   == "10") {$result.=' selected="selected" '; }
							$result.=' value="10">10</option>
                  </select> 

                </div>
             </div>
             <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right">
                    <span>Wheelchairs</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="wheelchairscap[]" style="width:100%;" class="form-control" required>
                
                     <option'; if ($car_Config->wheelchairs   == "0") {$result.=' selected="selected" '; }
							$result.=' value="0">0</option>
                      <option'; if ($car_Config->wheelchairs   == "1") {$result.=' selected="selected" '; }
							$result.=' value="1">1</option>
                      <option'; if ($car_Config->wheelchairs   == "2") {$result.=' selected="selected" '; }
							$result.=' value="2">2</option>
                      <option'; if ($car_Config->wheelchairs   == "3") {$result.=' selected="selected" '; }
							$result.=' value="3">3</option>
                      <option'; if ($car_Config->wheelchairs   == "4") {$result.=' selected="selected" '; }
							$result.=' value="4">4</option>
                      <option'; if ($car_Config->wheelchairs   == "5") {$result.=' selected="selected" '; }
							$result.=' value="5">5</option>
                      <option'; if ($car_Config->wheelchairs   == "6") {$result.=' selected="selected" '; }
							$result.=' value="6">6</option>
                      <option'; if ($car_Config->wheelchairs   == "7") {$result.=' selected="selected" '; }
							$result.=' value="7">7</option>
                      <option'; if ($car_Config->wheelchairs   == "8") {$result.=' selected="selected" '; }
							$result.=' value="8">8</option>
                      <option'; if ($car_Config->wheelchairs   == "9") {$result.=' selected="selected" '; }
							$result.=' value="9">9</option>
                      <option'; if ($car_Config->wheelchairs   == "10") {$result.=' selected="selected" '; }
							$result.=' value="10">10</option>
                  </select> 

             </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right">
                    <span>Babies</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="babycap[]" style="width:100%;" class="form-control" required>
                     
                    <option'; if ($car_Config->baby   == "0") {$result.=' selected="selected" '; }
							$result.=' value="0">0</option>
                      <option'; if ($car_Config->baby   == "1") {$result.=' selected="selected" '; }
							$result.=' value="1">1</option>
                      <option'; if ($car_Config->baby   == "2") {$result.=' selected="selected" '; }
							$result.=' value="2">2</option>
                      <option'; if ($car_Config->baby   == "3") {$result.=' selected="selected" '; }
							$result.=' value="3">3</option>
                      <option'; if ($car_Config->baby   == "4") {$result.=' selected="selected" '; }
							$result.=' value="4">4</option>
                      <option'; if ($car_Config->baby   == "5") {$result.=' selected="selected" '; }
							$result.=' value="5">5</option>
                      <option'; if ($car_Config->baby   == "6") {$result.=' selected="selected" '; }
							$result.=' value="6">6</option>
                      <option'; if ($car_Config->baby   == "7") {$result.=' selected="selected" '; }
							$result.=' value="7">7</option>
                      <option'; if ($car_Config->baby   == "8") {$result.=' selected="selected" '; }
							$result.=' value="8">8</option>
                      <option'; if ($car_Config->baby   == "9") {$result.=' selected="selected" '; }
							$result.=' value="9">9</option>
                      <option'; if ($car_Config->baby   == "10") {$result.=' selected="selected" '; }
							$result.=' value="10">10</option>
                  </select> 

              </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right">
                    <span>Animals</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="animalscap[]" style="width:100%;" class="form-control" required>
                    
                      <option'; if ($car_Config->animals   == "0") {$result.=' selected="selected" '; }
							$result.=' value="0">0</option>
                      <option'; if ($car_Config->animals   == "1") {$result.=' selected="selected" '; }
							$result.=' value="1">1</option>
                      <option'; if ($car_Config->animals   == "2") {$result.=' selected="selected" '; }
							$result.=' value="2">2</option>
                      <option'; if ($car_Config->animals   == "3") {$result.=' selected="selected" '; }
							$result.=' value="3">3</option>
                      <option'; if ($car_Config->animals   == "4") {$result.=' selected="selected" '; }
							$result.=' value="4">4</option>
                      <option'; if ($car_Config->animals   == "5") {$result.=' selected="selected" '; }
							$result.=' value="5">5</option>
                      <option'; if ($car_Config->animals   == "6") {$result.=' selected="selected" '; }
							$result.=' value="6">6</option>
                      <option'; if ($car_Config->animals   == "7") {$result.=' selected="selected" '; }
							$result.=' value="7">7</option>
                      <option'; if ($car_Config->animals   == "8") {$result.=' selected="selected" '; }
							$result.=' value="8">8</option>
                      <option'; if ($car_Config->animals   == "9") {$result.=' selected="selected" '; }
							$result.=' value="9">9</option>
                      <option'; if ($car_Config->animals   == "10") {$result.=' selected="selected" '; }
							$result.=' value="10">10</option>
                  </select> 

              </div>
             </div>
                    
           </div>  ';
           if($counts!=0){
           	$result .='<div class="col-md-6" style="position:relative;width: 54%;">
					       <button type="button"  class="btn btn-default"  onclick="deleteconfigbox('.$car_Config->id.');" style="z-index:10;position:absolute;top: -46px;right: -39px;border: none;cursor:pointer;overflow: hidden;outline:none;font-size: 22px;color: #414441;height: 46px;line-height: 35px;"><i class="fa fa-minus"></i></button>
					     </div>';
					    

           }

            $counts++; 
         $result .='</div>';
		 	}
		 	

		 	echo $result;
		 	exit;
		 	
}
	//end delete configuration


	public function carCatDelete(){
	$this->load->model('bookings_config_model');
	$id=$_POST['delete_car_id'];
	$table = "vbs_u_car_category";
	$del=$this->bookings_config_model->booking_Config_Delete($table,$id);
	$configdel=$this->bookings_config_model->booking_Carconfig_Delete("vbs_car_configuration",$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "6",
			'type' => "Success"
	]);
	redirect('admin/booking_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "6",
				'type' => "Error"
		]);
		redirect('admin/booking_config');
	}
	show_404();
	}
	public function get_ajax_car_cat(){
		$counts=0;
		$id=(int)$_GET['car_cat_id'];

		if($id>0){
			$this->load->model('bookings_config_model');
			$car_cat_data = $this->bookings_config_model->booking_Config_getAll('vbs_u_car_category',['id' => $id]);
			$car_configuration_data= $this->bookings_config_model->booking_Config_getAll('vbs_car_configuration',['car_id' => $id]);
			// print_r($client_cat);
			// foreach($client_cat as $key => $item){
			?>
		
			<?php
			foreach($car_cat_data as $key => $car_cat){
				$result='<input type="hidden" name="carcat_id" value="'.$car_cat->id.'">
		<div class="col-md-12">		
		   <div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span>Statut</span>
						<select class="form-control" name="car_cat_statut" required >
							
							<option '; if ($car_cat->statut == "1") {$result.=' selected="selected" '; }
							$result.=' value="1">Show</option>
							<option ';
							if ($car_cat->statut == "0") { $result.='selected="selected" '; }
							$result.=' value="0">Hide</option>
						</select>
				</div>
			</div>
			<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span>Car Category</span>
					<input type="text" class="form-control" name="car_cat_name" placeholder="" value="'.$car_cat->car_cat_name.'" required>
				</div>
			</div>
			 <div class="col-md-2" style="margin-top: 5px;">
	         <span>Wheelchair Access</span>
	           <select class="form-control" name="cartype" required>
	          
	            <option '; if ($car_cat->cartype == "1") {$result.=' selected="selected" '; }
								$result.=' value="1">Yes</option>
	            <option '; if ($car_cat->cartype == "0") {$result.=' selected="selected" '; }
								$result.=' value="0">No</option>
	          </select>
	       </div>
	       <div class="col-md-2" style="margin-top: 5px;">
			';
                if(!empty($car_cat->caricon)){
                    $result.='<div class="col-md-12"> 
		                         <img  id="editcar_logo_preview" src="'.base_url($car_cat->caricon).'" style="width: 200px; height: 150px;" >
		                        </div>';
                       }else{
                       	 $result.='<div class="col-md-12"> 
		                         <img  id="editcar_logo_preview" src="'.base_url('uploads/vehicle_images/default_car.png').'" style="width: 200px; height: 150px;" >
		                        </div>';
                       }
			 $result.='<div class="col-md-12">
                         <div class="form-group">
            <input type="file" name="caricon" class="form-control-file"  onChange="editcarpreview(this)"   style="width:100%;">
            <span>Car Icone</span>
          </div>
       </div>
         

    </div></div>';
			}
			if($car_configuration_data){
        $result.=' <div class="maineditcapacitybox">
     <div class="col-md-6" style="position:relative;width: 54%;">
       <button type="button"   onclick="editboxes();" class="plusgreeniconconfig"><i class="fa fa-plus"></i></button>
     </div>';}
     else{
     	   $result.=' <div class="maineditcapacitybox">
     <div class="col-md-6" style="position:relative;width: 54%;">
       <button type="button"  onclick="editboxes();" class="plusgreeniconconfig"><i class="fa fa-plus"></i></button>
     </div>';
     $result.=' <div class="capacitybox" >
          <div class="col-md-6" style="margin-top:10px;margin-left: 1.5%;background-image: linear-gradient(to bottom, #fff 0, #e0e0e0 100%);border-color: #ccc;padding: 6px 12px;"> 

             <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6 text-right" style="padding-top: 7px;">
                    <span>Passengers</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="passengercap[]" style="width:100%;" class="form-control" required>
                     
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
                  </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Luggage</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
 
                  <select name="lugagescap[]" style="width:100%;" class="form-control" required>
                     
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
                </div>
             </div>
             <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Wheelchairs</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="wheelchairscap[]" style="width:100%;" class="form-control" required>
                    
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
                </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Babys</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="babycap[]" style="width:100%;" class="form-control" required>
                     
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
              </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Animals</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="animalscap[]" style="width:100%;" class="form-control" required>
                    
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
               </div>
             </div>
          
           </div>  
         </div></div>';
     }
		 	foreach ($car_configuration_data as $key => $car_Config) {
		 			$result.='<div class="capacitybox">
		 			<input type="hidden" name="carconfigid[]" value="'.$car_Config->id.'">
         <div class="col-md-6" style="margin-top:10px;margin-left: 1.5%;background-image: linear-gradient(to bottom, #fff 0, #e0e0e0 100%);border-color: #ccc;padding: 6px 12px;"> 

             <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6 text-right" style="padding-top: 7px;">
                    <span>Passengers</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="passengercap[]" style="width:100%;" class="form-control" required>
                     
                      <option'; if ($car_Config->passengers  == "0") {$result.=' selected="selected" '; }
							$result.=' value="0">0</option>
                      <option'; if ($car_Config->passengers  == "1") {$result.=' selected="selected" '; }
							$result.=' value="1">1</option>
                      <option'; if ($car_Config->passengers  == "2") {$result.=' selected="selected" '; }
							$result.=' value="2">2</option>
                      <option'; if ($car_Config->passengers  == "3") {$result.=' selected="selected" '; }
							$result.=' value="3">3</option>
                      <option'; if ($car_Config->passengers  == "4") {$result.=' selected="selected" '; }
							$result.=' value="4">4</option>
                      <option'; if ($car_Config->passengers  == "5") {$result.=' selected="selected" '; }
							$result.=' value="5">5</option>
                      <option'; if ($car_Config->passengers  == "6") {$result.=' selected="selected" '; }
							$result.=' value="6">6</option>
                      <option'; if ($car_Config->passengers  == "7") {$result.=' selected="selected" '; }
							$result.=' value="7">7</option>
                      <option'; if ($car_Config->passengers  == "8") {$result.=' selected="selected" '; }
							$result.=' value="8">8</option>
                      <option'; if ($car_Config->passengers  == "9") {$result.=' selected="selected" '; }
							$result.=' value="9">9</option>
                      <option'; if ($car_Config->passengers  == "10") {$result.=' selected="selected" '; }
							$result.=' value="10">10</option>
                  </select> 

                </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Luggage</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">

                  <select name="lugagescap[]" style="width:100%;" class="form-control" required>
                    
                      <option'; if ($car_Config->luggage   == "0") {$result.=' selected="selected" '; }
							$result.=' value="0">0</option>
                      <option'; if ($car_Config->luggage   == "1") {$result.=' selected="selected" '; }
							$result.=' value="1">1</option>
                      <option'; if ($car_Config->luggage   == "2") {$result.=' selected="selected" '; }
							$result.=' value="2">2</option>
                      <option'; if ($car_Config->luggage   == "3") {$result.=' selected="selected" '; }
							$result.=' value="3">3</option>
                      <option'; if ($car_Config->luggage   == "4") {$result.=' selected="selected" '; }
							$result.=' value="4">4</option>
                      <option'; if ($car_Config->luggage   == "5") {$result.=' selected="selected" '; }
							$result.=' value="5">5</option>
                      <option'; if ($car_Config->luggage   == "6") {$result.=' selected="selected" '; }
							$result.=' value="6">6</option>
                      <option'; if ($car_Config->luggage   == "7") {$result.=' selected="selected" '; }
							$result.=' value="7">7</option>
                      <option'; if ($car_Config->luggage   == "8") {$result.=' selected="selected" '; }
							$result.=' value="8">8</option>
                      <option'; if ($car_Config->luggage   == "9") {$result.=' selected="selected" '; }
							$result.=' value="9">9</option>
                      <option'; if ($car_Config->luggage   == "10") {$result.=' selected="selected" '; }
							$result.=' value="10">10</option>
                  </select> 

                </div>
             </div>
             <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Wheelchairs</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="wheelchairscap[]" style="width:100%;" class="form-control" required>
                
                     <option'; if ($car_Config->wheelchairs   == "0") {$result.=' selected="selected" '; }
							$result.=' value="0">0</option>
                      <option'; if ($car_Config->wheelchairs   == "1") {$result.=' selected="selected" '; }
							$result.=' value="1">1</option>
                      <option'; if ($car_Config->wheelchairs   == "2") {$result.=' selected="selected" '; }
							$result.=' value="2">2</option>
                      <option'; if ($car_Config->wheelchairs   == "3") {$result.=' selected="selected" '; }
							$result.=' value="3">3</option>
                      <option'; if ($car_Config->wheelchairs   == "4") {$result.=' selected="selected" '; }
							$result.=' value="4">4</option>
                      <option'; if ($car_Config->wheelchairs   == "5") {$result.=' selected="selected" '; }
							$result.=' value="5">5</option>
                      <option'; if ($car_Config->wheelchairs   == "6") {$result.=' selected="selected" '; }
							$result.=' value="6">6</option>
                      <option'; if ($car_Config->wheelchairs   == "7") {$result.=' selected="selected" '; }
							$result.=' value="7">7</option>
                      <option'; if ($car_Config->wheelchairs   == "8") {$result.=' selected="selected" '; }
							$result.=' value="8">8</option>
                      <option'; if ($car_Config->wheelchairs   == "9") {$result.=' selected="selected" '; }
							$result.=' value="9">9</option>
                      <option'; if ($car_Config->wheelchairs   == "10") {$result.=' selected="selected" '; }
							$result.=' value="10">10</option>
                  </select> 

             </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Babys</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="babycap[]" style="width:100%;" class="form-control" required>
                     
                    <option'; if ($car_Config->baby   == "0") {$result.=' selected="selected" '; }
							$result.=' value="0">0</option>
                      <option'; if ($car_Config->baby   == "1") {$result.=' selected="selected" '; }
							$result.=' value="1">1</option>
                      <option'; if ($car_Config->baby   == "2") {$result.=' selected="selected" '; }
							$result.=' value="2">2</option>
                      <option'; if ($car_Config->baby   == "3") {$result.=' selected="selected" '; }
							$result.=' value="3">3</option>
                      <option'; if ($car_Config->baby   == "4") {$result.=' selected="selected" '; }
							$result.=' value="4">4</option>
                      <option'; if ($car_Config->baby   == "5") {$result.=' selected="selected" '; }
							$result.=' value="5">5</option>
                      <option'; if ($car_Config->baby   == "6") {$result.=' selected="selected" '; }
							$result.=' value="6">6</option>
                      <option'; if ($car_Config->baby   == "7") {$result.=' selected="selected" '; }
							$result.=' value="7">7</option>
                      <option'; if ($car_Config->baby   == "8") {$result.=' selected="selected" '; }
							$result.=' value="8">8</option>
                      <option'; if ($car_Config->baby   == "9") {$result.=' selected="selected" '; }
							$result.=' value="9">9</option>
                      <option'; if ($car_Config->baby   == "10") {$result.=' selected="selected" '; }
							$result.=' value="10">10</option>
                  </select> 

              </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Animals</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="animalscap[]" style="width:100%;" class="form-control" required>
                    
                      <option'; if ($car_Config->animals   == "0") {$result.=' selected="selected" '; }
							$result.=' value="0">0</option>
                      <option'; if ($car_Config->animals   == "1") {$result.=' selected="selected" '; }
							$result.=' value="1">1</option>
                      <option'; if ($car_Config->animals   == "2") {$result.=' selected="selected" '; }
							$result.=' value="2">2</option>
                      <option'; if ($car_Config->animals   == "3") {$result.=' selected="selected" '; }
							$result.=' value="3">3</option>
                      <option'; if ($car_Config->animals   == "4") {$result.=' selected="selected" '; }
							$result.=' value="4">4</option>
                      <option'; if ($car_Config->animals   == "5") {$result.=' selected="selected" '; }
							$result.=' value="5">5</option>
                      <option'; if ($car_Config->animals   == "6") {$result.=' selected="selected" '; }
							$result.=' value="6">6</option>
                      <option'; if ($car_Config->animals   == "7") {$result.=' selected="selected" '; }
							$result.=' value="7">7</option>
                      <option'; if ($car_Config->animals   == "8") {$result.=' selected="selected" '; }
							$result.=' value="8">8</option>
                      <option'; if ($car_Config->animals   == "9") {$result.=' selected="selected" '; }
							$result.=' value="9">9</option>
                      <option'; if ($car_Config->animals   == "10") {$result.=' selected="selected" '; }
							$result.=' value="10">10</option>
                  </select> 

              </div>
             </div>
                    
           </div>  ';
           if($counts!=0){
           	$result .='<div class="col-md-6" style="position:relative;width: 54%;">
					       <button type="button"   onclick="deleteconfigbox(this);" class="minusrediconconfig"><i class="fa fa-minus"></i></button>
					     </div>';
					    

           }

            $counts++; 
         $result .='</div>';
		 	}
		 	if($car_configuration_data){
		 	$result.='</div>';
		 }
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}

	// Car Category
	public function rideCatAdd(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->ride_cat_store();
		}else show_404();
	}
	public function ride_cat_store(){
			// $error = request_validate();
			if (empty($error)) {
				//$dob = to_unix_date(@$_POST['dob']);
				$user = $this->basic_auth->user();
				$user_id=$user->id;
				$table='vbs_u_ride_category';
				$id = $this->bookings_config_model->booking_Config_Add($table,[
					'ride_cat_name' 		=> @$_POST['ride_cat_name'],
					'statut' 		=> @$_POST['ride_cat_statut'],
					'created_at' 		=> @date('Y-m-d'),
					'time' 		=> @date("H : i"),
					'user_id' 		=> @$user_id,
				]);
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a ride category.",
					'class' => "alert-success",
					'status_id' => "9",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			} else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
	}

	public function rideCatEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['ridecat_id'];
			if (!empty($id)) {
			$table='vbs_u_ride_category';
			$update=$this->bookings_config_model->booking_Config_Update($table,[
				'ride_cat_name' 		=> @$_POST['ride_cat_name'],
				'statut' 		=> @$_POST['ride_cat_statut']
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a ride category.",
					'class' => "alert-success",
					'status_id' => "9",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
		}
	}else show_404();

	}
	public function rideCatDelete(){
	$this->load->model('bookings_config_model');
	$id=$_POST['delete_ride_id'];
	$table = "vbs_u_ride_category";
	$del=$this->bookings_config_model->booking_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "9",
			'type' => "Success"
	]);
	redirect('admin/booking_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "9",
				'type' => "Error"
		]);
		redirect('admin/booking_config');
	}
	show_404();
	}
	public function get_ajax_ride_cat(){
		$id=(int)$_GET['ride_cat_id'];

		if($id>0){
			$this->load->model('bookings_config_model');
			$ride_cat_data = $this->bookings_config_model->booking_Config_getAll('vbs_u_ride_category',['id' => $id]);
			// print_r($client_cat);
			// foreach($client_cat as $key => $item){
			?>
			
			<?php
			foreach($ride_cat_data as $key => $ride_cat){
				$result='<input type="hidden" name="ridecat_id" value="'.$ride_cat->id.'">
		<div class="col-md-12">
		   <div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
						<select class="form-control" name="ride_cat_statut" required >
							
							<option'; if ($ride_cat->statut == "1") {$result.=' selected="selected" '; }
							$result.=' value="1">Show</option>
							<option ';
							if ($ride_cat->statut == "2") { $result.='selected="selected" '; }
							$result.=' value="2">Hide</option>
						</select>
				</div>
			</div>
			<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Poi Category Name</span>
					<input type="text" class="form-control" name="ride_cat_name" placeholder="" value="'.$ride_cat->ride_cat_name.'">
				</div>
			</div>
			</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	public function get_ajax_service(){
		$id=(int)$_GET['service_id'];

		if($id>0){
			$this->load->model('bookings_config_model');
			$service_data = $this->bookings_config_model->booking_Config_getAll('vbs_u_service',['id' => $id]);
			 $vat_data = $this->bookings_config_model->getAllVat();
			?>
		
			<?php
			$all_service = $this->bookings_config_model->getAllData('vbs_u_category_service');
			
				foreach($service_data as $key => $single_service){
				$result='  <input type="hidden" name="service_id" value="'.$single_service->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
					<div class="form-group">
					<span>Statut</span>
							<select class="form-control" name="service_statut" required >
								
								<option '; if ($single_service->statut == "1") {$result.='selected="selected" '; }
								$result.=' value="1">Show</option>
								<option ';
								if ($single_service->statut == "2") { $result.='selected="selected" '; }
								$result.=' value="2">Hide</option>
							</select>
					</div>
				</div>
				<div class="col-md-2" style="margin-top: 5px;">
					<div class="form-group">
					<span>Service Category</span>
							<select class="form-control " name="service_category" required >
							<option value="">Select</option>
							'; foreach($all_service as $value): ; $result.='<option '; if($value->id == $single_service->service_category){ $result.='selected="selected" '; } $result.=' value="'.$value->id.'">'.$value->category_name.'</option>
							'; endforeach; $result.='
							</select>

					</div>
				</div>
				<div class="col-md-2" style="margin-top: 5px;">
					<div class="form-group">
					<span>Service Name</span>
						<input type="text" class="form-control" name="service_name" placeholder="" value="'.$single_service->service_name.'" required>
					</div>
				</div>
                 <div class="col-md-2" style="margin-top: 5px;">
				    <div class="form-group">
				      <span>TVA % </span>
				      <select class="form-control" name="tva" required>
				         <option value="">Select</option>';
				        foreach($vat_data as $key => $item){
				          $result.='<option '; if ($single_service->tva == $item->id) {$result.='selected="selected" '; }
								$result.=' value="'.$item->id.'">'.$item->vat.'</option>';
				        }
				       
				        
				      $result.='</select>
				    </div>
				  </div>
				</div>
				';
				}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	public function get_ajax_service_cat(){
		$id=(int)$_GET['service_cat_id'];

		if($id>0){
			$this->load->model('bookings_config_model');
			$service_cat_data = $this->bookings_config_model->booking_Config_getAll('vbs_u_category_service',['id' => $id]);
				
                 $payment_method = $this->bookings_config_model->getAllData('vbs_u_payment_method');
			?>
			
			<?php
			
			
			foreach($service_cat_data as $key => $service_cat){
				$result='<input type="hidden" name="service_id" value="'.$service_cat->id.'">
				<div class="col-md-12">
				<div class="col-md-3" style="margin-top: 5px;">
				<div class="form-group">
					<span>Statut</span>
						<select class="form-control" name="service_cat_statut" required >
							<option value="">Select</option>
							<option '; if ($service_cat->statut == "1") {$result.='selected="selected" '; }
							$result.=' value="1">Show</option>
							<option ';
							if ($service_cat->statut == "2") { $result.='selected="selected" '; }
							$result.=' value="2">Hide</option>
						</select>
				</div>
				</div>
				<div class="col-md-3" style="margin-top: 5px;">
				<div class="form-group">
					<span>Service Category</span>
					<input type="text" class="form-control" name="name_of_category" placeholder="" value="'.$service_cat->category_name.'">
				</div>
				</div>
				</div>
				<div class="col-md-12">
			
				
				  <div class="col-md-3" style="margin-top: 5px;">
				   <div class="multiselect">
				    <div class="selectBox" onclick="editshowCheckboxes()">
				       <span> Payment Methodes </span>
				      <select class="form-control">
				        <option disabled="disabled" selected="selected" style="display:none;">Payment Methodes</option>
				      </select>
				     
				    </div>
				    <div id="editcheckboxes">';
				         foreach($payment_method as $key => $value){
				     $result.='<label style="background-color:white;margin-bottom:0px;border-bottom: 1px solid #eeeeee;">
				        <input ';
							if ($this->ispaymentmethod($value->id,$id)) { $result.='checked'; }
							$result.=' type="checkbox" name="paymentmethod[]"  value="'.$value->id.'" style="float:none;margin:10px;"/>'.$value->name.'</label>';
				        }
				    $result.='</div>
				  </div>
				  </div>

				  <div class="col-md-3" style="margin-top: 5px;">
			        <div class="form-group">
			          <span> Invoice Periode </span>
			          <select class="form-control" name="invoice_periode" required>
			            <option ';
							if ($service_cat->invoice_periode  == "1") { $result.='selected="selected" '; }
							$result.=' value="1">Instantly</option>
			            <option ';
							if ($service_cat->invoice_periode  == "2") { $result.='selected="selected" '; }
							$result.=' value="2">Monthly</option>
			          </select>
			        </div>
			      </div>
				</div>
				
				
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	public function get_ajax_payment_method(){
		$id=(int)$_GET['payment_method_id'];

		if($id>0){
			$this->load->model('bookings_config_model');
			$payment_method_data = $this->bookings_config_model->booking_Config_getAll('vbs_u_payment_method',['id' => $id]);
			?>
			
			<?php
			foreach($payment_method_data as $key => $payment_method){
			     	$card_expirydate=$payment_method->card_expirydate ;
                     $card_expirydate =str_replace('-', '/', $card_expirydate);
					 $card_expirydate =strtotime($card_expirydate);
					 $card_expirydate = date('d/m/Y',$card_expirydate);
				$result='<input type="hidden" name="service_id" value="'.$payment_method->id.'">
				<div class="col-md-12">
					<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span >Status</span>
						<select class="form-control" name="payment_method_statut" required >
							
							<option '; if ($payment_method->status == "1") {$result.='selected="selected" '; }
							$result.=' value="1">Show</option>
							<option ';
							if ($payment_method->status == "0") { $result.='selected="selected" '; }
							$result.=' value="0">Hide</option>
						</select>
				</div>
				</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span >Client Category</span>
					<select class="form-control" name="category_client" required id="editcategory_client" onchange="editcheckclientCategory()">
					
					<option '; if ($payment_method->category_client == "0") {$result.='selected="selected" '; }
					$result.=' value="0">Others</option>
					<option ';
					if ($payment_method->category_client == "1") { $result.='selected="selected" '; }
					$result.=' value="1">Credit Card</option>
					<option '; if ($payment_method->category_client == "2") {$result.='selected="selected" '; }
					$result.=' value="2">Bank Direct Debit</option>
	
				</select>
				</div>
				</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span > Name </span>
					<input type="text" class="form-control" name="name" placeholder="" value="'.$payment_method->name.'" required>
				</div>
			</div>
			<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span>Order By</span>
					<select class="form-control" name="paymentorder" required>
					
					<option '; if ($payment_method->paymentorder == "1") {$result.='selected="selected" '; }
					$result.=' value="1">1</option>
					<option ';
					if ($payment_method->paymentorder == "2") { $result.='selected="selected" '; }
					$result.=' value="2">2</option>
					<option '; if ($payment_method->paymentorder == "3") {$result.='selected="selected" '; }
					$result.=' value="3">3</option>
					<option '; if ($payment_method->paymentorder == "4") {$result.='selected="selected" '; }
					$result.=' value="4">4</option>
					<option '; if ($payment_method->paymentorder == "5") {$result.='selected="selected" '; }
					$result.=' value="5">5</option>
					<option '; if ($payment_method->paymentorder == "6") {$result.='selected="selected" '; }
					$result.=' value="6">6</option>
	
				</select>
				</div>
				</div>				
			</div>
				
	<div class="col-md-12" style="margin-top: 5px;" id="editdescription_categories">
        <div class="form-group">
          <span >Description</span>
          <textarea id="edittextareacontent" class="form-control" name="description" rows="8" cols="80" >'.$payment_method->description.'</textarea>
        </div>
      </div>
				';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	
	public function get_ajax_vat(){
		$id=(int)$_GET['vat_id'];

		if($id>0){
			$this->load->model('bookings_config_model');
			$vat_data = $this->bookings_config_model->booking_Config_getAll('vbs_u_vat',['id' => $id]);
			?>
			
			<?php
			foreach($vat_data as $key => $vat){
				$result='<input type="hidden" name="vat_id" value="'.$vat->id.'">
				 <div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span>Statut</span>
						<select class="form-control" name="vat_statut" required >
							
							<option '; if ($vat->statut == "1") {$result.='selected="selected" '; }
							$result.=' value="1">Show</option>
							<option ';
							if ($vat->statut == "2") { $result.='selected="selected" '; }
							$result.=' value="2">Hide</option>
						</select>
				</div>
				</div>
				<div class="col-md-3" style="margin-top: 5px;">
				<div class="form-group">
					<span>Name Of Vat</span>
					<input type="text" class="form-control" name="name_of_vat" placeholder="" value="'.$vat->name_of_var.'" required>
				</div>
				</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span>Vat %</span>
					<input type="text" class="form-control" name="vat" placeholder="" value="'.$vat->vat.'" required>
				</div>
				</div>
				</div>
				';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	public function price_car_Add(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->car_store();
		}else show_404();
	}
	public function car_store(){
			// $error = request_validate();

			if (empty($error)) {
				$user = $this->basic_auth->user();
				$user_id=$user->id;
				$table='vbs_u_price_car';
				$id = $this->bookings_config_model->booking_Config_Add($table,[
					 'price_km_approche' => @$_POST['price_km_approche'],
					'price_trajet' 		=> @$_POST['price_trajet'],
					'price_trajet_retour' 		=> @$_POST['price_trajet_retour'],
					'attente' 		=> @$_POST['attente'],
					'pickup_fee' 		=> @$_POST['pickup_fee'],
					'majoration_de_nuit' 		=> @$_POST['majoration_de_nuit'],
					'supplement_jour_ferie' 		=> @$_POST['supplement_jour_ferie'],
					'supplement_1er' 		=> @$_POST['supplement_1er'],
					'minimum_fee' 		=> @$_POST['minimum_fee'],
					'panier_repas' 		=> @$_POST['panier_repas'],
					'repos_compensateur' 		=> @$_POST['repos_compensateur'],
					'statut' => @$_POST['statut'],
					'night_time_from'=>@$_POST['nighttimefrom'],
					'night_time_to'=>@$_POST['nighttimeto'],
					'driver_resttime_hour'=>@$_POST['driverresttimehour'],
					'driver_bonus'=>@$_POST['diverbonusfee'],
					'driver_meal_from'=>@$_POST['drivermealfrom'],
					'driver_meal_to'=>@$_POST['drivermealto'],
					'driver_othermeal_from'=>@$_POST['driverothermealfrom'],
					'driver_othermeal_to'=>@$_POST['driverothermealto'],
					'car_category' => @$_POST['car_category'],
					'nightfeetype' => @$_POST['nightfeetype'],
					'notworkfeetype' => @$_POST['notworkfeetype'],
					'decfeetype' => @$_POST['decfeetype'],
					'driverworkinghour' => @$_POST['driverworkinghour'],
					'car_place' => @$_POST['car_place'],
					'return_rate' => @$_POST['return_rate'],
					'created_at' 		=> @date('Y-m-d'),
					'time' 		=> @date("h : i : A"),
					'user_id' 		=> @$user_id,

				]);
				//add not working days
                   if($this->session->userdata['freedates']){
 	                 $freedates=$this->session->userdata['freedates'];

                     foreach($freedates as  $date){
                     $month=$date["month"];
                     $day=$date["day"];
                   

					switch ($month) {
							  case "January":
							   $month=1;
							    break;
							   case "February":
							   $month=2;
							    break;
							   case "March":
							   $month=3;
							    break;
							    case "April":
							   $month=4;
							    break;
							    case "May":
							   $month=5;
							    break;
							    case "June":
							   $month=6;
							    break;
							    case "July":
							   $month=7;
							    break;
							    case "August":
							   $month=8;
							    break;
							    case "September":
							   $month=9;
							    break;
							    case "October":
							   $month=10;
							    break;
							    case "November":
							   $month=11;
							    break;
							    case "December":
							    $month=12;
							    break;
							    

							  default:
							     $month=1;
							}
							
							$wid = $this->bookings_config_model->booking_Config_Add('vbs_notworking_days',[
											'day' => $day,
										   	'month'=> $month,
											'pirce_car_id' => $id
										
							]);
				  }		

            }
            $this->session->unset_userdata('freedates');
				//end not working days
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a car price.",
					'class' => "alert-success",
					'status_id' => "12",
					'type' => "Success",
					'price_stu_id' => "1"
				]);
				redirect('admin/booking_config');
			} else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
	}
	public function price_car_Delete(){
		$this->load->model('bookings_config_model');
		$id=$_POST['delet_car_id'];
		$table = "vbs_u_price_car";
		$del=$this->bookings_config_model->booking_Config_Delete($table,$id);
		$configdatadel=$this->bookings_config_model->booking_Configworkingdata_Delete("vbs_notworking_days",$id);
		if ($del==true) {
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully deleted record.",
				'class' => "alert-success",
				'status_id' => "6",
				'type' => "Success",
				'price_stu_id' => "1"
			]);
		redirect('admin/booking_config');
		}
		else {
			$this->session->set_flashdata('alert', [
					'message' => "Please try again.",
					'class' => "alert-danger",
					'status_id' => "6",
					'type' => "Error"
			]);
			redirect('admin/booking_config');
		}
		show_404();
	}

	public function price_car_Edit(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['price_car_id'];
			if (!empty($id)) {
			$table='vbs_u_price_car';
			$update=$this->bookings_config_model->booking_Config_Update($table,[
				'price_km_approche' 		=> @$_POST['price_km_approche'],
				'price_trajet' 		=> @$_POST['price_trajet'],
				'price_trajet_retour' 		=> @$_POST['price_trajet_retour'],
				'attente' 		=> @$_POST['attente'],
				'pickup_fee' 		=> @$_POST['pickup_fee'],
				'majoration_de_nuit' 		=> @$_POST['majoration_de_nuit'],
				'supplement_jour_ferie' 		=> @$_POST['supplement_jour_ferie'],
				'supplement_1er' 		=> @$_POST['supplement_1er'],
				'minimum_fee' 		=> @$_POST['minimum_fee'],
				'panier_repas' 		=> @$_POST['panier_repas'],
				'repos_compensateur' 		=> @$_POST['repos_compensateur'],
				'statut' => @$_POST['statut'],
				'night_time_from'=>@$_POST['nighttimefrom'],
				'night_time_to'=>@$_POST['nighttimeto'],
				'driver_resttime_hour'=>@$_POST['driverresttimehour'],
				'driver_bonus'=>@$_POST['diverbonusfee'],
				'driver_meal_from'=>@$_POST['drivermealfrom'],
				'driver_meal_to'=>@$_POST['drivermealto'],
				'driver_othermeal_from'=>@$_POST['driverothermealfrom'],
				'driver_othermeal_to'=>@$_POST['driverothermealto'],
				'car_category' => @$_POST['car_category'],
				 'nightfeetype' => @$_POST['nightfeetype'],
				 'notworkfeetype' => @$_POST['notworkfeetype'],
				 'decfeetype' => @$_POST['decfeetype'],
				 'driverworkinghour' => @$_POST['driverworkinghour'],
				 'car_place' => @$_POST['car_place'],
				'return_rate' => @$_POST['return_rate'],

				 ], $id);
			//not working days
               $configdatadel=$this->bookings_config_model->booking_Configworkingdata_Delete("vbs_notworking_days",$id);
               if($configdatadel){
                         if($this->session->userdata['freedates']){
 	                 $freedates=$this->session->userdata['freedates'];

                     foreach($freedates as  $date){
                     $month=$date["month"];
                     $day=$date["day"];
                   

					switch ($month) {
							  case "January":
							   $month=1;
							    break;
							   case "February":
							   $month=2;
							    break;
							   case "March":
							   $month=3;
							    break;
							    case "April":
							   $month=4;
							    break;
							    case "May":
							   $month=5;
							    break;
							    case "June":
							   $month=6;
							    break;
							    case "July":
							   $month=7;
							    break;
							    case "August":
							   $month=8;
							    break;
							    case "September":
							   $month=9;
							    break;
							    case "October":
							   $month=10;
							    break;
							    case "November":
							   $month=11;
							    break;
							    case "December":
							    $month=12;
							    break;
							    

							  default:
							     $month=1;
							}
							
							$wid = $this->bookings_config_model->booking_Config_Add('vbs_notworking_days',[
											'day' => $day,
										   	'month'=> $month,
											'pirce_car_id' => $id
										
							]);
				  }		

            }
            $this->session->unset_userdata('freedates');
               }
           //end not working days
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a car price.",
					'class' => "alert-success",
					'status_id' => "12",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
		}
	}else show_404();

	}
	public function price_car_type_Edit(){
		// print_r($_POST);
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['price_car_type_id'];
			$no_of_persons=@$_POST['no_of_persons'];
			$no_of_lugages=@$_POST['no_of_lugages'];
			$weelchairs=@$_POST['weelchairs'];
			$no_of_babyseat=@$_POST['no_of_babyseat'];
			$no_of_animal=@$_POST['no_of_animal'];
			foreach ($no_of_persons as $key => $value) {
				$data[]=$no_of_persons[$key];
				$data[]=$no_of_lugages[$key];
				$data[]=$weelchairs[$key];
				$data[]=$no_of_babyseat[$key];
				$data[]=$no_of_animal[$key];
				$otherdata[]=$data;
				$data=array();
			}
			for ($i=0; $i < count($otherdata) ; $i++) {
						$data=implode('#=#',$otherdata[$i]);
						$otherdetail[]=$data;
			}
		  $othersdata=implode('##==##',$otherdetail);
			// exit(0);
			if (!empty($id)) {
			$table='vbs_u_price_car_type';
			$update=$this->bookings_config_model->booking_Config_Update($table,[
				'price_km_approche' 		=> @$_POST['price_km_approche'],
				'price_trajet' 		=> @$_POST['price_trajet'],
				'price_trajet_retour' 		=> @$_POST['price_trajet_retour'],
				'attente' 		=> @$_POST['attente'],
				'siege_bebe' 		=> @$_POST['siege_bebe'],
				'bagages' 		=> @$_POST['bagages'],
				'pickup_fee' 		=> @$_POST['pickup_fee'],
				'majoration_de_nuit' 		=> @$_POST['majoration_de_nuit'],
				'supplement_jour_ferie' 		=> @$_POST['supplement_jour_ferie'],
				'supplement_1er' 		=> @$_POST['supplement_1er'],
				'animal_de_cmpanie' 		=> @$_POST['animal_de_cmpanie'],
				'accompagnateur' 		=> @$_POST['accompagnateur'],
				'location_de_siege_BEBE' 		=> @$_POST['location_de_siege_BEBE'],
				'minimum_fee' 		=> @$_POST['minimum_fee'],
				'panier_repas' 		=> @$_POST['panier_repas'],
				'repos_compensateur' 		=> @$_POST['repos_compensateur'],
				'repos' 		=> @$_POST['repos'],
				'passager' 		=> @$_POST['passager'],
				'fauteuil' 		=> @$_POST['fauteuil'],
				'location_fauteuil' 		=> @$_POST['location_fauteuil'],
				'type_name' 		=> @$_POST['type_name'],
				'udaptation' 		=> @$_POST['udaptation'],
				'others' 		=> @$othersdata
			 ], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a car type price.",
					'class' => "alert-success",
					'status_id' => "6",
					'type' => "Success",
					'price_stu_id' => "2"
				]);
				redirect('admin/booking_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
		}
	}else show_404();

	}
	public function price_van_Add(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->van_store();
		}else show_404();
	}
	public function van_store(){
			// $error = request_validate();
			if (empty($error)) {
				$table='vbs_u_price_van';
				$id = $this->bookings_config_model->booking_Config_Add($table,[
					'price_km_approche' 		=> @$_POST['price_km_approche'],
					'price_trajet' 		=> @$_POST['price_trajet'],
					'price_trajet_retour' 		=> @$_POST['price_trajet_retour'],
					'attente' 		=> @$_POST['attente'],
					'siege_bebe' 		=> @$_POST['siege_bebe'],
					'bagages' 		=> @$_POST['bagages'],
					'pickup_fee' 		=> @$_POST['pickup_fee'],
					'majoration_de_nuit' 		=> @$_POST['majoration_de_nuit'],
					'supplement_jour_ferie' 		=> @$_POST['supplement_jour_ferie'],
					'supplement_1er' 		=> @$_POST['supplement_1er'],
					'animal_de_cmpanie' 		=> @$_POST['animal_de_cmpanie'],
					'accompagnateur' 		=> @$_POST['accompagnateur'],
					'location_de_siege_BEBE' 		=> @$_POST['location_de_siege_BEBE'],
					'minimum_fee' 		=> @$_POST['minimum_fee'],
					'panier_repas' 		=> @$_POST['panier_repas'],
					'repos_compensateur' 		=> @$_POST['repos_compensateur'],
					'repos' 		=> @$_POST['repos'],
					'passager' 		=> @$_POST['passager'],
					'fauteuil' 		=> @$_POST['fauteuil'],
					'location_fauteuil' 		=> @$_POST['location_fauteuil'],
					'created_at' 		=> @date('Y-m-d'),
					'time' 		=> @date("h : i : A"),
				]);
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a van price.",
					'class' => "alert-success",
					'status_id' => "6",
					'type' => "Success",
					'price_stu_id' => "3"
				]);
				redirect('admin/booking_config');
			} else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
	}
	public function price_van_Delete(){
	$this->load->model('bookings_config_model');
	$id=$_POST['delet_van_id'];
	$table = "vbs_u_price_van";
	$del=$this->bookings_config_model->booking_Config_Delete($table,$id);
	if ($del==true) {
		$this->session->set_flashdata('alert', [
			'message' => ": Successfully deleted record.",
			'class' => "alert-success",
			'status_id' => "6",
			'type' => "Success",
			'price_stu_id' => "3"
		]);
	redirect('admin/booking_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "6",
				'type' => "Error"
		]);
		redirect('admin/booking_config');
	}
	 show_404();
	}
	public function price_van_Edit(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['price_van_id'];
			if (!empty($id)) {
			$table='vbs_u_price_van';
			$update=$this->bookings_config_model->booking_Config_Update($table,[
				'price_km_approche' 		=> @$_POST['price_km_approche'],
				'price_trajet' 		=> @$_POST['price_trajet'],
				'price_trajet_retour' 		=> @$_POST['price_trajet_retour'],
				'attente' 		=> @$_POST['attente'],
				'siege_bebe' 		=> @$_POST['siege_bebe'],
				'bagages' 		=> @$_POST['bagages'],
				'pickup_fee' 		=> @$_POST['pickup_fee'],
				'majoration_de_nuit' 		=> @$_POST['majoration_de_nuit'],
				'supplement_jour_ferie' 		=> @$_POST['supplement_jour_ferie'],
				'supplement_1er' 		=> @$_POST['supplement_1er'],
				'animal_de_cmpanie' 		=> @$_POST['animal_de_cmpanie'],
				'accompagnateur' 		=> @$_POST['accompagnateur'],
				'location_de_siege_BEBE' 		=> @$_POST['location_de_siege_BEBE'],
				'minimum_fee' 		=> @$_POST['minimum_fee'],
				'panier_repas' 		=> @$_POST['panier_repas'],
				'repos_compensateur' 		=> @$_POST['repos_compensateur'],
				'repos' 		=> @$_POST['repos'],
				'passager' 		=> @$_POST['passager'],
				'fauteuil' 		=> @$_POST['fauteuil'],
				'location_fauteuil' 		=> @$_POST['location_fauteuil'] ], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a van price.",
					'class' => "alert-success",
					'status_id' => "6",
					'type' => "Success",
					'price_stu_id' => "3"
				]);
				redirect('admin/booking_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
		}
	}else show_404();

	}
	public function price_minibus_Add(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->minibus_store();
		}else show_404();
	}
	public function minibus_store(){
			// $error = request_validate();
			if (empty($error)) {
				$table='vbs_u_price_minibus';
				$id = $this->bookings_config_model->booking_Config_Add($table,[
					'price_km_approche' 		=> @$_POST['price_km_approche'],
					'price_trajet' 		=> @$_POST['price_trajet'],
					'price_trajet_retour' 		=> @$_POST['price_trajet_retour'],
					'attente' 		=> @$_POST['attente'],
					'siege_bebe' 		=> @$_POST['siege_bebe'],
					'bagages' 		=> @$_POST['bagages'],
					'pickup_fee' 		=> @$_POST['pickup_fee'],
					'majoration_de_nuit' 		=> @$_POST['majoration_de_nuit'],
					'supplement_jour_ferie' 		=> @$_POST['supplement_jour_ferie'],
					'supplement_1er' 		=> @$_POST['supplement_1er'],
					'animal_de_cmpanie' 		=> @$_POST['animal_de_cmpanie'],
					'accompagnateur' 		=> @$_POST['accompagnateur'],
					'location_de_siege_BEBE' 		=> @$_POST['location_de_siege_BEBE'],
					'minimum_fee' 		=> @$_POST['minimum_fee'],
					'panier_repas' 		=> @$_POST['panier_repas'],
					'repos_compensateur' 		=> @$_POST['repos_compensateur'],
					'repos' 		=> @$_POST['repos'],
					'passager' 		=> @$_POST['passager'],
					'fauteuil' 		=> @$_POST['fauteuil'],
					'location_fauteuil' 		=> @$_POST['location_fauteuil'],
					'created_at' 		=> @date('Y-m-d'),
					'time' 		=> @date("h : i : A"),
				]);
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a minibus price.",
					'class' => "alert-success",
					'status_id' => "6",
					'type' => "Success",
					'price_stu_id' => "4"
				]);
				redirect('admin/booking_config');
			} else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
	}
	public function price_minibus_Delete(){
	$this->load->model('bookings_config_model');
	$id=$_POST['delet_minibus_id'];
	$table = "vbs_u_price_minibus";
	$del=$this->bookings_config_model->booking_Config_Delete($table,$id);
	if ($del==true) {
		$this->session->set_flashdata('alert', [
			'message' => ": Successfully deleted record.",
			'class' => "alert-success",
			'status_id' => "6",
			'type' => "Success",
			'price_stu_id' => "4"
		]);
	redirect('admin/booking_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "6",
				'type' => "Error"
		]);
		redirect('admin/booking_config');
	}
	 show_404();
	}
	public function price_minibus_Edit(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['price_minibus_id'];
			if (!empty($id)) {
			$table='vbs_u_price_minibus';
			$update=$this->bookings_config_model->booking_Config_Update($table,[
				'price_km_approche' 		=> @$_POST['price_km_approche'],
				'price_trajet' 		=> @$_POST['price_trajet'],
				'price_trajet_retour' 		=> @$_POST['price_trajet_retour'],
				'attente' 		=> @$_POST['attente'],
				'siege_bebe' 		=> @$_POST['siege_bebe'],
				'bagages' 		=> @$_POST['bagages'],
				'pickup_fee' 		=> @$_POST['pickup_fee'],
				'majoration_de_nuit' 		=> @$_POST['majoration_de_nuit'],
				'supplement_jour_ferie' 		=> @$_POST['supplement_jour_ferie'],
				'supplement_1er' 		=> @$_POST['supplement_1er'],
				'animal_de_cmpanie' 		=> @$_POST['animal_de_cmpanie'],
				'accompagnateur' 		=> @$_POST['accompagnateur'],
				'location_de_siege_BEBE' 		=> @$_POST['location_de_siege_BEBE'],
				'minimum_fee' 		=> @$_POST['minimum_fee'],
				'panier_repas' 		=> @$_POST['panier_repas'],
				'repos_compensateur' 		=> @$_POST['repos_compensateur'],
				'repos' 		=> @$_POST['repos'],
				'passager' 		=> @$_POST['passager'],
				'fauteuil' 		=> @$_POST['fauteuil'],
				'location_fauteuil' 		=> @$_POST['location_fauteuil'] ], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a minibus price.",
					'class' => "alert-success",
					'status_id' => "6",
					'type' => "Success",
					'price_stu_id' => "4"
				]);
				redirect('admin/booking_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
		}
	}else show_404();

	}
	public function price_cartpmr_Add(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->cartpmr_store();
		}else show_404();
	}
	public function cartpmr_store(){
			// $error = request_validate();
			if (empty($error)) {
				$table='vbs_u_price_car_tpmr';
				$id = $this->bookings_config_model->booking_Config_Add($table,[
					'price_km_approche' 		=> @$_POST['price_km_approche'],
					'price_trajet' 		=> @$_POST['price_trajet'],
					'price_trajet_retour' 		=> @$_POST['price_trajet_retour'],
					'attente' 		=> @$_POST['attente'],
					'siege_bebe' 		=> @$_POST['siege_bebe'],
					'bagages' 		=> @$_POST['bagages'],
					'pickup_fee' 		=> @$_POST['pickup_fee'],
					'majoration_de_nuit' 		=> @$_POST['majoration_de_nuit'],
					'supplement_jour_ferie' 		=> @$_POST['supplement_jour_ferie'],
					'supplement_1er' 		=> @$_POST['supplement_1er'],
					'animal_de_cmpanie' 		=> @$_POST['animal_de_cmpanie'],
					'accompagnateur' 		=> @$_POST['accompagnateur'],
					'location_de_siege_BEBE' 		=> @$_POST['location_de_siege_BEBE'],
					'minimum_fee' 		=> @$_POST['minimum_fee'],
					'panier_repas' 		=> @$_POST['panier_repas'],
					'repos_compensateur' 		=> @$_POST['repos_compensateur'],
					'repos' 		=> @$_POST['repos'],
					'passager' 		=> @$_POST['passager'],
					'fauteuil' 		=> @$_POST['fauteuil'],
					'location_fauteuil' 		=> @$_POST['location_fauteuil'],
					'created_at' 		=> @date('Y-m-d'),
					'time' 		=> @date("h : i : A"),
				]);
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a car tpmr price.",
					'class' => "alert-success",
					'status_id' => "6",
					'type' => "Success",
					'price_stu_id' => "5"
				]);
				redirect('admin/booking_config');
			} else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
	}
	public function price_cartpmr_Delete(){
	$this->load->model('bookings_config_model');
	$id=$_POST['delet_cartpmr_id'];
	$table = "vbs_u_price_car_tpmr";
	$del=$this->bookings_config_model->booking_Config_Delete($table,$id);
	if ($del==true) {
		$this->session->set_flashdata('alert', [
			'message' => ": Successfully deleted record.",
			'class' => "alert-success",
			'status_id' => "6",
			'type' => "Success",
			'price_stu_id' => "5"
		]);
	redirect('admin/booking_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "6",
				'type' => "Error"
		]);
		redirect('admin/booking_config');
	}
	 show_404();
	}
	public function price_cartpmr_Edit(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['price_car_tpmr_id'];
			if (!empty($id)) {
			$table='vbs_u_price_car_tpmr';
			$update=$this->bookings_config_model->booking_Config_Update($table,[
				'price_km_approche' 		=> @$_POST['price_km_approche'],
				'price_trajet' 		=> @$_POST['price_trajet'],
				'price_trajet_retour' 		=> @$_POST['price_trajet_retour'],
				'attente' 		=> @$_POST['attente'],
				'siege_bebe' 		=> @$_POST['siege_bebe'],
				'bagages' 		=> @$_POST['bagages'],
				'pickup_fee' 		=> @$_POST['pickup_fee'],
				'majoration_de_nuit' 		=> @$_POST['majoration_de_nuit'],
				'supplement_jour_ferie' 		=> @$_POST['supplement_jour_ferie'],
				'supplement_1er' 		=> @$_POST['supplement_1er'],
				'animal_de_cmpanie' 		=> @$_POST['animal_de_cmpanie'],
				'accompagnateur' 		=> @$_POST['accompagnateur'],
				'location_de_siege_BEBE' 		=> @$_POST['location_de_siege_BEBE'],
				'minimum_fee' 		=> @$_POST['minimum_fee'],
				'panier_repas' 		=> @$_POST['panier_repas'],
				'repos_compensateur' 		=> @$_POST['repos_compensateur'],
				'repos' 		=> @$_POST['repos'],
				'passager' 		=> @$_POST['passager'],
				'fauteuil' 		=> @$_POST['fauteuil'],
				'location_fauteuil' 		=> @$_POST['location_fauteuil'] ], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a car TPMR.",
					'class' => "alert-success",
					'status_id' => "6",
					'type' => "Success",
					'price_stu_id' => "5"
				]);
				redirect('admin/booking_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
		}
	}else show_404();

	}
	public function price_vantpmr_Add(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->vantpmr_store();
		}else show_404();
	}
	public function vantpmr_store(){
			// $error = request_validate();
			if (empty($error)) {
				$table='vbs_u_price_van_tpmr';
				$id = $this->bookings_config_model->booking_Config_Add($table,[
					'price_km_approche' 		=> @$_POST['price_km_approche'],
					'price_trajet' 		=> @$_POST['price_trajet'],
					'price_trajet_retour' 		=> @$_POST['price_trajet_retour'],
					'attente' 		=> @$_POST['attente'],
					'siege_bebe' 		=> @$_POST['siege_bebe'],
					'bagages' 		=> @$_POST['bagages'],
					'pickup_fee' 		=> @$_POST['pickup_fee'],
					'majoration_de_nuit' 		=> @$_POST['majoration_de_nuit'],
					'supplement_jour_ferie' 		=> @$_POST['supplement_jour_ferie'],
					'supplement_1er' 		=> @$_POST['supplement_1er'],
					'animal_de_cmpanie' 		=> @$_POST['animal_de_cmpanie'],
					'accompagnateur' 		=> @$_POST['accompagnateur'],
					'location_de_siege_BEBE' 		=> @$_POST['location_de_siege_BEBE'],
					'minimum_fee' 		=> @$_POST['minimum_fee'],
					'panier_repas' 		=> @$_POST['panier_repas'],
					'repos_compensateur' 		=> @$_POST['repos_compensateur'],
					'repos' 		=> @$_POST['repos'],
					'passager' 		=> @$_POST['passager'],
					'fauteuil' 		=> @$_POST['fauteuil'],
					'location_fauteuil' 		=> @$_POST['location_fauteuil'],
					'created_at' 		=> @date('Y-m-d'),
					'time' 		=> @date("h : i : A"),
				]);
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a van tpmr price.",
					'class' => "alert-success",
					'status_id' => "6",
					'type' => "Success",
					'price_stu_id' => "6"
				]);
				redirect('admin/booking_config');
			} else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
	}
	public function price_vantpmr_Delete(){
	$this->load->model('bookings_config_model');
	$id=$_POST['delet_vantpmr_id'];
	$table = "vbs_u_price_van_tpmr";
	$del=$this->bookings_config_model->booking_Config_Delete($table,$id);
	if ($del==true) {
		$this->session->set_flashdata('alert', [
			'message' => ": Successfully deleted record.",
			'class' => "alert-success",
			'status_id' => "6",
			'type' => "Success",
			'price_stu_id' => "6"
		]);
	redirect('admin/booking_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "6",
				'type' => "Error"
		]);
		redirect('admin/booking_config');
	}
	 show_404();
	}

	public function price_vantpmr_Edit(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['price_van_tpmr_id'];
			if (!empty($id)) {
			$table='vbs_u_price_van_tpmr';
			$update=$this->bookings_config_model->booking_Config_Update($table,[
				'price_km_approche' 		=> @$_POST['price_km_approche'],
				'price_trajet' 		=> @$_POST['price_trajet'],
				'price_trajet_retour' 		=> @$_POST['price_trajet_retour'],
				'attente' 		=> @$_POST['attente'],
				'siege_bebe' 		=> @$_POST['siege_bebe'],
				'bagages' 		=> @$_POST['bagages'],
				'pickup_fee' 		=> @$_POST['pickup_fee'],
				'majoration_de_nuit' 		=> @$_POST['majoration_de_nuit'],
				'supplement_jour_ferie' 		=> @$_POST['supplement_jour_ferie'],
				'supplement_1er' 		=> @$_POST['supplement_1er'],
				'animal_de_cmpanie' 		=> @$_POST['animal_de_cmpanie'],
				'accompagnateur' 		=> @$_POST['accompagnateur'],
				'location_de_siege_BEBE' 		=> @$_POST['location_de_siege_BEBE'],
				'minimum_fee' 		=> @$_POST['minimum_fee'],
				'panier_repas' 		=> @$_POST['panier_repas'],
				'repos_compensateur' 		=> @$_POST['repos_compensateur'],
				'repos' 		=> @$_POST['repos'],
				'passager' 		=> @$_POST['passager'],
				'fauteuil' 		=> @$_POST['fauteuil'],
				'location_fauteuil' 		=> @$_POST['location_fauteuil'] ], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a van TPMR.",
					'class' => "alert-success",
					'status_id' => "6",
					'type' => "Success",
					'price_stu_id' => "6"
				]);
				redirect('admin/booking_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
		}
	}else show_404();

	}
	public function price_minibustpmr_Add(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->minibustpmr_store();
		}else show_404();
	}
	public function minibustpmr_store(){
			// $error = request_validate();
			if (empty($error)) {
				$table='vbs_u_price_minibus_tpmr';
				$id = $this->bookings_config_model->booking_Config_Add($table,[
					'price_km_approche' 		=> @$_POST['price_km_approche'],
					'price_trajet' 		=> @$_POST['price_trajet'],
					'price_trajet_retour' 		=> @$_POST['price_trajet_retour'],
					'attente' 		=> @$_POST['attente'],
					'siege_bebe' 		=> @$_POST['siege_bebe'],
					'bagages' 		=> @$_POST['bagages'],
					'pickup_fee' 		=> @$_POST['pickup_fee'],
					'majoration_de_nuit' 		=> @$_POST['majoration_de_nuit'],
					'supplement_jour_ferie' 		=> @$_POST['supplement_jour_ferie'],
					'supplement_1er' 		=> @$_POST['supplement_1er'],
					'animal_de_cmpanie' 		=> @$_POST['animal_de_cmpanie'],
					'accompagnateur' 		=> @$_POST['accompagnateur'],
					'location_de_siege_BEBE' 		=> @$_POST['location_de_siege_BEBE'],
					'minimum_fee' 		=> @$_POST['minimum_fee'],
					'panier_repas' 		=> @$_POST['panier_repas'],
					'repos_compensateur' 		=> @$_POST['repos_compensateur'],
					'repos' 		=> @$_POST['repos'],
					'passager' 		=> @$_POST['passager'],
					'fauteuil' 		=> @$_POST['fauteuil'],
					'location_fauteuil' 		=> @$_POST['location_fauteuil'],
					'created_at' 		=> @date('Y-m-d'),
					'time' 		=> @date("h : i : A"),
				]);
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a minibus tpmr price.",
					'class' => "alert-success",
					'status_id' => "6",
					'type' => "Success",
					'price_stu_id' => "7"
				]);
				redirect('admin/booking_config');
			} else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
	}
	public function price_minibustpmr_Delete(){
	$this->load->model('bookings_config_model');
	$id=$_POST['delet_minibustpmr_id'];
	$table = "vbs_u_price_minibus_tpmr";
	$del=$this->bookings_config_model->booking_Config_Delete($table,$id);
	if ($del==true) {
		$this->session->set_flashdata('alert', [
			'message' => ": Successfully deleted record.",
			'class' => "alert-success",
			'status_id' => "6",
			'type' => "Success",
			'price_stu_id' => "7"
		]);
	redirect('admin/booking_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "6",
				'type' => "Error"
		]);
		redirect('admin/booking_config');
	}
	 show_404();
	}
	public function price_minibustpmr_Edit(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['price_minibus_tpmr_id'];
			if (!empty($id)) {
			$table='vbs_u_price_minibus_tpmr';
			$update=$this->bookings_config_model->booking_Config_Update($table,[
				'price_km_approche' 		=> @$_POST['price_km_approche'],
				'price_trajet' 		=> @$_POST['price_trajet'],
				'price_trajet_retour' 		=> @$_POST['price_trajet_retour'],
				'attente' 		=> @$_POST['attente'],
				'siege_bebe' 		=> @$_POST['siege_bebe'],
				'bagages' 		=> @$_POST['bagages'],
				'pickup_fee' 		=> @$_POST['pickup_fee'],
				'majoration_de_nuit' 		=> @$_POST['majoration_de_nuit'],
				'supplement_jour_ferie' 		=> @$_POST['supplement_jour_ferie'],
				'supplement_1er' 		=> @$_POST['supplement_1er'],
				'animal_de_cmpanie' 		=> @$_POST['animal_de_cmpanie'],
				'accompagnateur' 		=> @$_POST['accompagnateur'],
				'location_de_siege_BEBE' 		=> @$_POST['location_de_siege_BEBE'],
				'minimum_fee' 		=> @$_POST['minimum_fee'],
				'panier_repas' 		=> @$_POST['panier_repas'],
				'repos_compensateur' 		=> @$_POST['repos_compensateur'],
				'repos' 		=> @$_POST['repos'],
				'passager' 		=> @$_POST['passager'],
				'fauteuil' 		=> @$_POST['fauteuil'],
				'location_fauteuil' 		=> @$_POST['location_fauteuil'] ], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a minibus TPMR.",
					'class' => "alert-success",
					'status_id' => "6",
					'type' => "Success",
					'price_stu_id' => "7"
				]);
				redirect('admin/booking_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/booking_config');
			}
		}
	 }else show_404();

	}
	public function get_ajax_price_car(){
	$id=(int)$_GET['car_id'];

		if($id>0){
			$this->load->model('bookings_config_model');
			$value_data = $this->bookings_config_model->booking_Config_getAll('vbs_u_price_car',['id' => $id]);
			$config_data = $this->bookings_config_model->booking_Config_getAll('vbs_notworking_days',['pirce_car_id' => $id]);
		$car_category= $this->bookings_config_model->getAllData('vbs_u_car_category');
			if($config_data){
				$this->storeworkingdays($config_data);
			}
			foreach($value_data as $key => $value){
			?>
			
			<?php
				$result='<div class="row" style="margin-top: 10px;">
                           		<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span >Statut</span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
											
											<select class="form-control" name="statut" required >
												
												<option '; if ($value->statut == "1") {$result.='selected="selected" '; }
												$result.=' value="1">Show</option>
												<option ';
												if ($value->statut == "2") { $result.='selected="selected" '; }
												$result.=' value="2">Hide</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;padding-left: 10px;">
											<span >Car Category</span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<select class="form-control" name="car_category" required="">
												<option value="">Select</option>';
						foreach($car_category as $key => $values){
			$result.='<option '; if($values->id == $value->car_category){$result.='selected="selected"';} $result.='value="'.$values->id.'">'.$values->car_cat_name.'</option>';
												}
												$result.='</select>

																								
										</div>
									</div>
								</div>
                           </div>
                 <div class="row" style="margin-top: 10px;">
                           	<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span >Car Place</span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
											<select class="form-control" name="car_place" required="">
												<option value="">Select</option>
												<option '; if ($value->car_place == "0") {$result.='selected="selected" '; }
												$result.=' value="0">Agency</option>
												<option ';
												if ($value->car_place == "1") { $result.='selected="selected" '; }
												$result.=' value="1">Driver</option>
										  </select>
										</div>
									</div>
								</div>
                           </div>     
				<div class="row" style="margin-top: 10px;">
					<input type="hidden" name="price_car_id" value="'.$value->id.'">
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span>Distance : Price / Km</span>
							</div>
							<div class="col-md-5" style="padding: 0px;">
									<input type="text"  name="price_km_approche" class="form-control" value="'.$value->price_km_approche.'" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span>Night Fee</span>
							</div>
							<div class="col-md-2" style="padding: 0px;">
								<select style="width:100%" class="form-control" name="nightfeetype" required="">
									<option '; if ($value->nightfeetype == "0") {$result.='selected="selected" '; } $result.=' value="0">%</option>
									<option '; if ($value->nightfeetype == "1") {$result.='selected="selected" '; } $result.=' value="1">Fix</option>
								</select>
							</div>
							<div class="col-md-4" style="padding: 0px;">
								<input type="text" name="majoration_de_nuit"value="'.$value->majoration_de_nuit.'"  class="form-control" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span>Driver Meal Cost</span>
							</div>
							<div class="col-md-6" style="padding: 0px;">
								<input type="text" name="panier_repas"value="'.$value->panier_repas.'"  class="form-control" required="">
							</div>
						</div>
					</div>
				</div>
				<div class="row" style="margin-top: 10px;">
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span>Time : Price / Minute</span>
							</div>
							<div class="col-md-5" style="padding: 0px;">
								<input type="text" name="price_trajet"value="'.$value->price_trajet.'"  class="form-control" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span>Sundays and not working day fee</span>
							</div>
							<div class="col-md-2" style="padding: 0px;">
								<select style="width:100%" class="form-control" name="notworkfeetype" required="">
									<option '; if ($value->notworkfeetype == "0") {$result.='selected="selected" '; } $result.=' value="0">%</option>
									<option '; if ($value->notworkfeetype == "1") {$result.='selected="selected" '; } $result.=' value="1">Fix</option>
								</select>
							</div>
							<div class="col-md-4" style="padding: 0px;">
								<input type="text" name="supplement_jour_ferie"value="'.$value->supplement_jour_ferie.'"  class="form-control" required="">
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span>Driver Rest Cost</span>
							</div>
							<div class="col-md-6" style="padding: 0px;">
								<input type="text" id="editdriverrestcost" name="repos_compensateur" value="'.$value->repos_compensateur.'"  class="form-control" required="" readonly>
							</div>
						</div>
					</div>
				</div>
			
				 <div class="row" style="margin-top: 10px;">
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span>Approche Rate : Price / Km</span>
							</div>
							<div class="col-md-5" style="padding: 0px;">
								<input type="text" name="price_trajet_retour" value="'.$value->price_trajet_retour.'" class="form-control" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span>1st Mai and 25 December Fee</span>
							</div>
							<div class="col-md-2" style="padding: 0px;">
								<select style="width:100%" class="form-control" name="decfeetype" required="">
									<option '; if ($value->decfeetype == "0") {$result.='selected="selected" '; } $result.=' value="0">%</option>
									<option '; if ($value->decfeetype == "1") {$result.='selected="selected" '; } $result.=' value="1">Fix</option>
								</select>
							</div>
							<div class="col-md-4"  style="padding: 0px;">
								<input type="text" name="supplement_1er" value="'.$value->supplement_1er.'" class="form-control" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						
					</div>
				</div>
					<div class="row" style="margin-top: 10px;">
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span>Return Rate : Price / Km</span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
											<input type="text" name="return_rate" value="'.$value->return_rate.'" class="form-control" required="">
										</div>
									</div>
								</div>
							</div>
				 <div class="row" style="margin-top: 10px;">
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span>Waiting Time : Price / Minute </span>
							</div>
							<div class="col-md-5" style="padding: 0px;">
								<input type="text" name="attente" value="'.$value->attente.'" class="form-control" required="">
							</div>
						</div>
					</div>
							<div class="col-md-4"  style="margin-top: 5px;">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span >Night Time</span>
										</div>
										<div class="col-md-6" style="padding: 0px;">

											<div class="col-md-2" style="padding:10px 0px 0px;">
										      <label for="nighttimefrom">From</label>
										    </div>
										    <div class="col-md-4" style="padding-left:0px;">
										    <input   id="editnighttimefrom" name="nighttimefrom" type="text" value="'.date($value->night_time_from).'" style="width:120%;" />
										    </div>
										    <div class="col-md-2" style="padding-top:10px;">
										      <label for="nighttimeto">To</label>
										    </div>
										    <div class="col-md-4" style="padding-left:0px;">
										    <input   id="editnighttimeto" name="nighttimeto" type="text" value="'.date($value->night_time_to).'" style="width:120%;" />
										    </div>
										</div>	
										
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span>Driver Rest (Hours)</span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											  
											
											  <div class="col-md-12" style="padding: 0px;">
											  <input type="number" name="driverresttimehour" min="0"  value="'.$value->driver_resttime_hour .'"   class="form-control" id="editdriverresttimehour" required onchange="editcalDriverCost()">
											  	
											  </div>
										</div>
									</div>
								</div>
				
					
				</div>
				 <div class="row" style="margin-top: 10px;">
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span>Pickup Fee</span>
							</div>
							<div class="col-md-5" style="padding: 0px;">
								<input type="text" name="pickup_fee" value="'.$value->pickup_fee.'" class="form-control" required="">
							</div>

						</div>
							</div>
							<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span>Not working days</span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
										   <div class="col-md-4" style="padding: 0px 0px 0px 5px;">
									    	   <select name="notworkingday" id="editnotworkingday" style="width:100%;" class="form-control">
						                               <option value="1">1</option>
						                               <option value="2">2</option>
						                               <option value="3">3</option>
						                               <option value="4">4</option>
						                               <option value="5">5</option>
						                               <option value="6">6</option>
						                               <option value="7">7</option>
						                               <option value="8">8</option>
						                               <option value="9">9</option>
						                               <option value="10">10</option>
						                               <option value="11">11</option>
						                               <option value="12">12</option>
						                               <option value="13">13</option>
						                               <option value="14">14</option>
						                               <option value="15">15</option>
						                               <option value="16">16</option>
						                               <option value="17">17</option>
						                               <option value="18">18</option>
						                               <option value="19">19</option>
						                               <option value="20">20</option>
						                               <option value="21">21</option>
						                               <option value="22">22</option>
						                               <option value="23">23</option>
						                               <option value="24">24</option>
						                               <option value="25">25</option>
						                               <option value="26">26</option>
						                               <option value="27">27</option>
						                               <option value="28">28</option>
						                               <option value="29">29</option>
						                               <option value="30">30</option>
						                               <option value="31">31</option>
						                       </select> 
									    	</div>
											<div class="col-md-5" style="padding: 0px 0px 0px 5px;">
											   <select name="notworkingmonth" id="editnotworkingmonth" style="width:100%;" class="form-control">
					                                <option value="1">January</option>
					                                <option value="2">February</option>
					                                <option value="3">March</option>
					                                <option value="4">April</option>
					                                <option value="5">May</option>
					                                <option value="6">June</option>
					                                <option value="7">July</option>
					                                <option value="8">August</option>
					                                <option value="9">September</option>
					                                <option value="10">October</option>
					                                <option value="11">November</option>
					                                <option value="12">December</option>
					                            </select> 
									    	</div>
									    	
									    	<div class="col-md-3" style="padding: 0px 0px 0px 5px;">
										        <button  type="button" id="editnotworkingbtn" onclick="editfreeworkingdates();" class="btn btn-default btn-md" style="min-width:100%;">Add</button>
										    </div>

										</div>
										<div class="col-md-6 col-md-offset-5">
											<ul id="editnotworkingdaysdiv" style="border:0px;display:block!important;list-style-type:none;">';
											 if($config_data){
											 	$dates=$this->session->userdata['freedates'];
                                                   foreach($dates as $key => $date){
                                                       $result.='<li class="text-left"  style="display:inline-block;margin-right:5px;"><button type="button" style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;outline:none;color:#4ca6cf;" class="dateremovebtn" onclick="editfreedaysremove('. $key.');"> <i class="fa fa-close"></i>
													</button>'.$date["day"].' '. $date["month"].'</li>';
                                                   }
                                                    
											 }
												
													
											  
											  $result.='</ul>
										</div>
									</div>
								</div>
						<div class="col-md-4">
                                   <div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span>Driver Working Hour Cost</span>
										</div>
										
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="diverbonusfee"  value="'.$value->driver_bonus .'"   class="form-control" id="editdriverworkinghourcost" required="" onkeyup="editcalDriverCost()">
										</div>
									</div>
						</div>
				
					
				
				</div>
				<div class="row" style="margin-top: 10px;">
				
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span>Minimum Ride Price HT</span>
							</div>
							<div class="col-md-5" style="padding: 0px;">
								<input type="text" name="minimum_fee"value="'.$value->minimum_fee.'"  class="form-control" required="">
							</div>
						</div>
					</div>
				   	<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span>Driver Meal</span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<div class="col-md-2" style="padding:10px 0px 0px;">
										      <label for="editdrivermealfrom">From</label>
										    </div>
										    <div class="col-md-4" style="padding-left:0px;">
										    <input   id="editdrivermealfrom" name="drivermealfrom" type="text" 
										    value="'.date($value->driver_meal_from).'" style="width:120%;" />
										    </div>
										    <div class="col-md-2" style="padding-top:10px;">
										      <label for="editdrivermealto">To</label>
										    </div>
										    <div class="col-md-4" style="padding-left:0px;">
										    <input   id="editdrivermealto" name="drivermealto" type="text" 
										    value="'.date($value->driver_meal_to).'" style="width:120%;" />
										    </div>
										   
										</div>
										<div class="col-md-6 col-md-offset-5" style="padding: 0px">
											 <div class="col-md-2" style="padding:10px 0px 0px;">
										      <label for="editdriverothermealfrom">From</label>
										    </div>
										    <div class="col-md-4" style="padding-left:0px;">
										    <input   id="editdriverothermealfrom" name="driverothermealfrom" type="text" 
										   value="'.date($value->driver_othermeal_from).'" style="width:120%;" />
										    </div>
										    <div class="col-md-2" style="padding-top:10px;">
										      <label for="editdriverothermealto">To</label>
										    </div>
										    <div class="col-md-4" style="padding-left:0px;">
										    <input   id="editdriverothermealto" name="driverothermealto" type="text" 
										   value="'.date($value->driver_othermeal_to).'" style="width:120%;" />
										    </div>
										</div>
									</div>
								</div>
								  <div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span >Driver Working Hours (sunday or not working day) to earn a rest day</span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											  
											
											  <div class="col-md-12" style="padding: 0px;">
											<input type="number" name="driverworkinghour" min="0"   value="'.$value->driverworkinghour.'"   
											  	 class="form-control" required >
											  	
											  </div>
										</div>
									</div>
								</div>
				</div>';

			 }
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;	
	}
	public function get_ajax_price_vantpmr(){
		$id=(int)$_GET['vantpmr_id'];

		if($id>0){
			$this->load->model('bookings_config_model');
			$value_data = $this->bookings_config_model->booking_Config_getAll('vbs_u_price_van_tpmr',['id' => $id]);
			foreach($value_data as $key => $value){
			?>
			<style media="screen">
			select {
			padding: 5px !important;
			background: url(<?php echo base_url();?>/assets/arrow.png) no-repeat right #fff  !important;
			-webkit-appearance: none;} </style>
			<?php
				$result='<div class="row" style="margin-top: 10px;">
					<input type="hidden" name="price_van_tpmr_id" value="'.$value->id.'">
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span style="font-weight: bold;">Price / KM of Parcours D\'approche</span>
							</div>
							<div class="col-md-5" style="padding: 0px;">
									<input type="text"  name="price_km_approche"class="form-control" value="'.$value->price_km_approche.'" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4	">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span style="font-weight: bold;">Majoration de nuit</span>
							</div>
							<div class="col-md-2" style="padding: 0px;">
								<select style="width:100%" class="form-control" name="" required="">
									<option value="%">%</option>
									<option value="Fix">Fix</option>
								</select>
							</div>
							<div class="col-md-4" style="padding: 0px;">
								<input type="text" name="majoration_de_nuit"value="'.$value->majoration_de_nuit.'"  class="form-control" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span style="font-weight: bold;">Panier Repas</span>
							</div>
							<div class="col-md-6" style="padding: 0px;">
								<input type="text" name="panier_repas"value="'.$value->panier_repas.'"  class="form-control" required="">
							</div>
						</div>
					</div>
				</div>
				<div class="row" style="margin-top: 10px;">
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span style="font-weight: bold;">Price / KM TRAJET</span>
							</div>
							<div class="col-md-5" style="padding: 0px;">
								<input type="text" name="price_trajet"value="'.$value->price_trajet.'"  class="form-control" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span style="font-weight: bold;">Supplement jour Ferie</span>
							</div>
							<div class="col-md-2" style="padding: 0px;">
								<select style="width:100%" class="form-control" name="" required="">
									<option value="%">%</option>
									<option value="Fix">Fix</option>
								</select>
							</div>
							<div class="col-md-4" style="padding: 0px;">
								<input type="text" name="supplement_jour_ferie"value="'.$value->supplement_jour_ferie.'"  class="form-control" required="">
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span style="font-weight: bold;">Repos Compensateur</span>
							</div>
							<div class="col-md-6" style="padding: 0px;">
								<input type="text" name="repos_compensateur"value="'.$value->repos_compensateur.'"  class="form-control" required="">
							</div>
						</div>
					</div>
				</div>
				 <div class="row" style="margin-top: 10px;">
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span style="font-weight: bold;">Price / KM TRAJET Retour</span>
							</div>
							<div class="col-md-5" style="padding: 0px;">
								<input type="text" name="price_trajet_retour" value="'.$value->price_trajet_retour.'" class="form-control" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span style="font-weight: bold;">Supplement 1er mai et 25 decembre</span>
							</div>
							<div class="col-md-2" style="padding: 0px;">
								<select style="width:100%" class="form-control" name="" required="">
									<option value="%">%</option>
									<option value="Fix">Fix</option>
								</select>
							</div>
							<div class="col-md-4"  style="padding: 0px;">
								<input type="text" name="supplement_1er" value="'.$value->supplement_1er.'" class="form-control" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span style="font-weight: bold;">Repos </span>
							</div>
							<div class="col-md-6" style="padding: 0px;">
								<input type="text" name="repos"value="'.$value->repos.'" class="form-control" required="">
							</div>
						</div>
					</div>
				</div>
				 <div class="row" style="margin-top: 10px;">
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span style="font-weight: bold;">Attente</span>
							</div>
							<div class="col-md-5" style="padding: 0px;">
								<input type="text" name="attente" value="'.$value->attente.'" class="form-control" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span style="font-weight: bold;">Animal de Cmpanie</span>
							</div>
							<div class="col-md-6" style="padding: 0px;">
								<input type="text" name="animal_de_cmpanie"value="'.$value->animal_de_cmpanie.'"  class="form-control" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span style="font-weight: bold;">Passager </span>
							</div>
							<div class="col-md-6" style="padding: 0px;">
								<input type="text" name="passager"value="'.$value->passager.'"  class="form-control" required="">
							</div>
						</div>
					</div>
				</div>
				 <div class="row" style="margin-top: 10px;">
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span style="font-weight: bold;">Siege BEBE</span>
							</div>
								<div class="col-md-5" style="padding: 0px;">
								<input type="text" name="siege_bebe"value="'.$value->siege_bebe.'"  class="form-control" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span style="font-weight: bold;">Accompagnateur</span>
							</div>
							<div class="col-md-6" style="padding: 0px;">
								<input type="text" name="accompagnateur"value="'.$value->accompagnateur.'" class="form-control" required="">
							</div>

						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span style="font-weight: bold;">Fauteuil </span>
							</div>
							<div class="col-md-6" style="padding: 0px;">
								<input type="text" name="fauteuil" value="'.$value->fauteuil.'" class="form-control" required="">
							</div>
						</div>
					</div>
				</div>
				<div class="row" style="margin-top: 10px;">
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span style="font-weight: bold;">Bagages</span>
							</div>
							<div class="col-md-5" style="padding: 0px;">
								<input type="text" name="bagages"value="'.$value->bagages.'" class="form-control" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span style="font-weight: bold;">Location De Siege BEBE</span>
							</div>
							<div class="col-md-6" style="padding: 0px;">
								<input type="text" name="location_de_siege_BEBE" value="'.$value->location_de_siege_BEBE.'" class="form-control" required="">
							</div>

						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span style="font-weight: bold;">Location Fauteuil </span>
							</div>
								<div class="col-md-6" style="padding: 0px;">
								<input type="text" name="location_fauteuil" value="'.$value->location_fauteuil.'" class="form-control" required="">
							</div>
						</div>
					</div>
				</div>
				 <div class="row" style="margin-top: 10px;">
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span style="font-weight: bold;">Pickup Fee</span>
							</div>
							<div class="col-md-5" style="padding: 0px;">
								<input type="text" name="pickup_fee" value="'.$value->pickup_fee.'" class="form-control" required="">
							</div>

						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span style="font-weight: bold;">Minimum Fee</span>
							</div>
							<div class="col-md-6" style="padding: 0px;">
								<input type="text" name="minimum_fee"value="'.$value->minimum_fee.'"  class="form-control" required="">
							</div>
						</div>
					</div>

				</div>';

			 }
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	public function get_ajax_price_minibustpmr(){
		$id=(int)$_GET['minibustpmr_id'];

		if($id>0){
			$this->load->model('bookings_config_model');
			$value_data = $this->bookings_config_model->booking_Config_getAll('vbs_u_price_minibus_tpmr',['id' => $id]);
			foreach($value_data as $key => $value){
			?>
			<style media="screen">
			select {
			padding: 5px !important;
			background: url(<?php echo base_url();?>/assets/arrow.png) no-repeat right #fff  !important;
			-webkit-appearance: none;} </style>
			<?php
				$result='<div class="row" style="margin-top: 10px;">
					<input type="hidden" name="price_minibus_tpmr_id" value="'.$value->id.'">
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span style="font-weight: bold;">Price / KM of Parcours D\'approche</span>
							</div>
							<div class="col-md-5" style="padding: 0px;">
									<input type="text"  name="price_km_approche"class="form-control" value="'.$value->price_km_approche.'" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4	">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span style="font-weight: bold;">Majoration de nuit</span>
							</div>
							<div class="col-md-2" style="padding: 0px;">
								<select style="width:100%" class="form-control" name="" required="">
									<option value="%">%</option>
									<option value="Fix">Fix</option>
								</select>
							</div>
							<div class="col-md-4" style="padding: 0px;">
								<input type="text" name="majoration_de_nuit"value="'.$value->majoration_de_nuit.'"  class="form-control" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span style="font-weight: bold;">Panier Repas</span>
							</div>
							<div class="col-md-6" style="padding: 0px;">
								<input type="text" name="panier_repas"value="'.$value->panier_repas.'"  class="form-control" required="">
							</div>
						</div>
					</div>
				</div>
				<div class="row" style="margin-top: 10px;">
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span style="font-weight: bold;">Price / KM TRAJET</span>
							</div>
							<div class="col-md-5" style="padding: 0px;">
								<input type="text" name="price_trajet"value="'.$value->price_trajet.'"  class="form-control" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span style="font-weight: bold;">Supplement jour Ferie</span>
							</div>
							<div class="col-md-2" style="padding: 0px;">
								<select style="width:100%" class="form-control" name="" required="">
									<option value="%">%</option>
									<option value="Fix">Fix</option>
								</select>
							</div>
							<div class="col-md-4" style="padding: 0px;">
								<input type="text" name="supplement_jour_ferie"value="'.$value->supplement_jour_ferie.'"  class="form-control" required="">
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span style="font-weight: bold;">Repos Compensateur</span>
							</div>
							<div class="col-md-6" style="padding: 0px;">
								<input type="text" name="repos_compensateur"value="'.$value->repos_compensateur.'"  class="form-control" required="">
							</div>
						</div>
					</div>
				</div>
				 <div class="row" style="margin-top: 10px;">
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span style="font-weight: bold;">Price / KM TRAJET Retour</span>
							</div>
							<div class="col-md-5" style="padding: 0px;">
								<input type="text" name="price_trajet_retour" value="'.$value->price_trajet_retour.'" class="form-control" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span style="font-weight: bold;">Supplement 1er mai et 25 decembre</span>
							</div>
							<div class="col-md-2" style="padding: 0px;">
								<select style="width:100%" class="form-control" name="" required="">
									<option value="%">%</option>
									<option value="Fix">Fix</option>
								</select>
							</div>
							<div class="col-md-4"  style="padding: 0px;">
								<input type="text" name="supplement_1er" value="'.$value->supplement_1er.'" class="form-control" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span style="font-weight: bold;">Repos </span>
							</div>
							<div class="col-md-6" style="padding: 0px;">
								<input type="text" name="repos"value="'.$value->repos.'" class="form-control" required="">
							</div>
						</div>
					</div>
				</div>
				 <div class="row" style="margin-top: 10px;">
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span style="font-weight: bold;">Attente</span>
							</div>
							<div class="col-md-5" style="padding: 0px;">
								<input type="text" name="attente" value="'.$value->attente.'" class="form-control" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span style="font-weight: bold;">Animal de Cmpanie</span>
							</div>
							<div class="col-md-6" style="padding: 0px;">
								<input type="text" name="animal_de_cmpanie"value="'.$value->animal_de_cmpanie.'"  class="form-control" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span style="font-weight: bold;">Passager </span>
							</div>
							<div class="col-md-6" style="padding: 0px;">
								<input type="text" name="passager"value="'.$value->passager.'"  class="form-control" required="">
							</div>
						</div>
					</div>
				</div>
				 <div class="row" style="margin-top: 10px;">
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span style="font-weight: bold;">Siege BEBE</span>
							</div>
								<div class="col-md-5" style="padding: 0px;">
								<input type="text" name="siege_bebe"value="'.$value->siege_bebe.'"  class="form-control" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span style="font-weight: bold;">Accompagnateur</span>
							</div>
							<div class="col-md-6" style="padding: 0px;">
								<input type="text" name="accompagnateur"value="'.$value->accompagnateur.'" class="form-control" required="">
							</div>

						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span style="font-weight: bold;">Fauteuil </span>
							</div>
							<div class="col-md-6" style="padding: 0px;">
								<input type="text" name="fauteuil" value="'.$value->fauteuil.'" class="form-control" required="">
							</div>
						</div>
					</div>
				</div>
				<div class="row" style="margin-top: 10px;">
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span style="font-weight: bold;">Bagages</span>
							</div>
							<div class="col-md-5" style="padding: 0px;">
								<input type="text" name="bagages"value="'.$value->bagages.'" class="form-control" required="">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span style="font-weight: bold;">Location De Siege BEBE</span>
							</div>
							<div class="col-md-6" style="padding: 0px;">
								<input type="text" name="location_de_siege_BEBE" value="'.$value->location_de_siege_BEBE.'" class="form-control" required="">
							</div>

						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 5px;">
								<span style="font-weight: bold;">Location Fauteuil </span>
							</div>
								<div class="col-md-6" style="padding: 0px;">
								<input type="text" name="location_fauteuil" value="'.$value->location_fauteuil.'" class="form-control" required="">
							</div>
						</div>
					</div>
				</div>
				 <div class="row" style="margin-top: 10px;">
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span style="font-weight: bold;">Pickup Fee</span>
							</div>
							<div class="col-md-5" style="padding: 0px;">
								<input type="text" name="pickup_fee" value="'.$value->pickup_fee.'" class="form-control" required="">
							</div>

						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5" style="margin-top: 10px;">
								<span style="font-weight: bold;">Minimum Fee</span>
							</div>
							<div class="col-md-6" style="padding: 0px;">
								<input type="text" name="minimum_fee"value="'.$value->minimum_fee.'"  class="form-control" required="">
							</div>
						</div>
					</div>

				</div>';

			 }
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}


public function remove_discount_periods(){
	 $index=$_GET['index'];
	 if($this->session->userdata['discountperiods']){
	 	 $periods['discountperiods']=$this->session->userdata['discountperiods'];
	     unset( $periods['discountperiods'][$index]);
          $this->session->set_userdata($periods);
        
	 }
	   $periods=$this->session->userdata['discountperiods'];
        $data = ["periods" => $periods];
        header('Content-Type: application/json'); 
        echo json_encode($data);

}
public function add_discount_periods(){
	
      $discountdatefrom=$_GET['datefrom'];
      $discountdateto=$_GET['dateto'];
      $discounttimefrom=$_GET['timefrom'];
      $discounttimeto=$_GET['timeto'];

	 if($this->session->userdata['discountperiods']){
	 	 $periods['discountperiods']=$this->session->userdata['discountperiods'];
	     $newarray=array("discountdatefrom"=>$discountdatefrom,"discountdateto"=>$discountdateto,"discounttimefrom"=>$discounttimefrom,"discounttimeto"=>$discounttimeto);
	     array_push($periods['discountperiods'], $newarray);

          $this->session->set_userdata($periods);
        
	 }
	 else{
	 	 $periods['discountperiods']=array(
	 	 array("discountdatefrom"=>$discountdatefrom,"discountdateto"=>$discountdateto,"discounttimefrom"=>$discounttimefrom,"discounttimeto"=>$discounttimeto)
	 	 );
            $this->session->set_userdata($periods);
	 }
	 

	    $periods=$this->session->userdata['discountperiods'];
        $data = ["periods" => $periods];
        header('Content-Type: application/json'); 
        echo json_encode($data);
}

public function remove_notworking_days(){
	 $index=$_GET['index'];
	 if($this->session->userdata['freedates']){
	 	 $dates['freedates']=$this->session->userdata['freedates'];
	     unset( $dates['freedates'][$index]);
          $this->session->set_userdata($dates);
        
	 }
	   $dates=$this->session->userdata['freedates'];
        $data = ["dates" => $dates];
        header('Content-Type: application/json'); 
        echo json_encode($data);

}
public function add_notworking_days(){
	//$this->session->unset_userdata('freedates');
      $day=$_GET['day'];
     $month=$_GET['month'];
//	$day=$this->input->post("day");
//	$month=$this->input->post("month");
	switch ($month) {
			  case "1":
			   $month="January";
			    break;
			   case "2":
			   $month="February";
			    break;
			   case "3":
			   $month="March";
			    break;
			    case "4":
			   $month="April";
			    break;
			    case "5":
			   $month="May";
			    break;
			    case "6":
			   $month="June";
			    break;
			    case "7":
			   $month="July";
			    break;
			    case "8":
			   $month="August";
			    break;
			    case "9":
			   $month="September";
			    break;
			    case "10":
			   $month="October";
			    break;
			    case "11":
			   $month="November";
			    break;
			    case "12":
			    $month="December";
			    break;
			    

			  default:
			     $month="January";
			}
	 if($this->session->userdata['freedates']){
	 	 $dates['freedates']=$this->session->userdata['freedates'];
	     $newarray=array("month"=>$month,"day"=>$day);
	     array_push($dates['freedates'], $newarray);

          $this->session->set_userdata($dates);
        
	 }
	 else{
	 	 $dates['freedates']=array(
	 	 	array("month"=>$month,"day"=>$day)
	 	 );
            $this->session->set_userdata($dates);
	 }
	 

	    $dates=$this->session->userdata['freedates'];
        $data = ["dates" => $dates];
        header('Content-Type: application/json'); 
        echo json_encode($data);
}
public function storeworkingdays($configdata){
	$dates['freedates']=array();
	//$id=9;
		//$value_data = $this->bookings_config_model->booking_Config_getAll('vbs_notworking_days',['pirce_car_id' => $id]);
			if($configdata){


			foreach($configdata as $key => $value){
				switch ($value->month) {
			  case "1":
			   $month="January";
			    break;
			   case "2":
			   $month="February";
			    break;
			   case "3":
			   $month="March";
			    break;
			    case "4":
			   $month="April";
			    break;
			    case "5":
			   $month="May";
			    break;
			    case "6":
			   $month="June";
			    break;
			    case "7":
			   $month="July";
			    break;
			    case "8":
			   $month="August";
			    break;
			    case "9":
			   $month="September";
			    break;
			    case "10":
			   $month="October";
			    break;
			    case "11":
			   $month="November";
			    break;
			    case "12":
			    $month="December";
			    break;
			    

			  default:
			     $month="January";
			}
               $dates['freedates'][$key]=array('day'=>$value->day,'month'=>$month);
				

			}
			 $this->session->set_userdata($dates);
			
		
	}
		
}
public function storediscountperiod($perioddata){
	$period['discountperiods']=array();
	
			if($perioddata){


			foreach($perioddata as $key => $value){
			
               $period['discountperiods'][$key]=array("discountdatefrom"=>from_unix_date($value->datefrom),"discountdateto"=>from_unix_date($value->dateto),"discounttimefrom"=>$value->timefrom,"discounttimeto"=>$value->timeto);
				

			}
			 $this->session->set_userdata($period);
			
		
	}
		
}
public function ispaymentmethod($methodid,$id){
	$ismethod=false;
$service_cat_payment = $this->bookings_config_model->booking_Config_getAll('vbs_servicepayment_category ',['servicecategory_id' => $id]);
foreach ($service_cat_payment as $key => $item) {
	if($item->paymentmethod_id==$methodid){
		$ismethod=true;
	}
}
return $ismethod;
}
	public function get_poidata(){
		$id=(int)$_GET['poicategory_id'];

		if($id>0){
			 $poidata = $this->bookings_config_model->booking_Config_getAll('vbs_u_poi',['category_id' => $id]);
			 
			 $result='<option value="">Select</option>';
				       foreach($poidata as $key => $item){
				        $result.='<option  value="'.$item->id.'">'.$item->name.'</option>';
				     }
			}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;	     
}
public function upload_editor_image(){
	            
	            $file=$_FILES['file'];
                $fileName=$file['name'];
                $fileExt=explode('.',$fileName);
                $fileActEct=strtolower(end($fileExt));
                $fileName=time().".".$fileActEct;
                $fileNewName='uploads/paymentimage/'.$fileName;
                $path_to_folder = 'uploads/paymentimage/';
                $config['upload_path'] = realpath($path_to_folder);
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['max_size']     = '3000000'; // 3mb
                $config['file_name']    = $fileName;
               
                $this->load->library('upload', $config);
              
                if ($this->upload->do_upload('file'))
                { 
                	 	  echo json_encode(array(
												        'file_path' => base_url($fileNewName)
												    ));

                }
}
	public function get_ajax_api(){
		$id=(int)$_GET['api_id'];

		if($id>0){
			
			$api_data = $this->bookings_config_model->booking_Config_getAll('vbs_u_googleapi',['id' => $id]);
			?>
			
			<?php
			foreach($api_data as $key => $item){
				$result='<input type="hidden" name="api_id" value="'.$item->id.'">
				<div class="col-md-12">
                <div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span>Statut</span>
						<select class="form-control" name="statut" required >
							
							<option '; if ($item->statut == "1") {$result.='selected="selected" '; }
							$result.=' value="1">Show</option>
							<option ';
							if ($item->statut == "0") { $result.='selected="selected" '; }
							$result.=' value="0">Hide</option>
						</select>
				</div>
				</div>
				<div class="col-md-3" style="margin-top: 5px;">
				<div class="form-group">
					<span>Api Key</span>
					<input type="text" class="form-control" name="api_key" placeholder="" value="'.$item->api_key.'">
				</div>
				</div></div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}



		public function deleteApi(){
		$this->load->model('bookings_config_model');
		$id=$_POST['delet_api_id'];
		$del=$this->bookings_config_model->deleteapi($id);
		if ($del==true) {
		$this->session->set_flashdata('alert', [
				'message' => "Successfully deleted.",
				'class' => "alert-success",
				'status_id' => "10",
				'type' => "Success"
		]);
		redirect('admin/booking_config');
		}
		else {
			$this->session->set_flashdata('alert', [
					'message' => "Please try again.",
					'class' => "alert-danger",
					'status_id' => "10",
					'type' => "Error"
			]);
			redirect('admin/booking_config');
		}
	}



	public function apiadd(){
		
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->apistore();
		}else show_404();

	}
public function apistore(){
			// $error = request_validate();
			if (empty($error)) {
				//$dob = to_unix_date(@$_POST['dob']);
				$user = $this->basic_auth->user();
			 	$user_id=$user->id;


				$id = $this->bookings_config_model->addApi([
					'api_key' 		=> @$_POST['api_key'],
					'statut' 		=> @$_POST['statut'],
					'created_at' 		=> @date('Y-m-d'),
					'time' 		=> @date("H : i"),
					'user_id' 		=> $user_id,
				]);
				// $this->data['alert'] = [
				// 	'message' => ': Successfully added a statut.',
				// 	'class' => "alert-success",
				// 	'type' => "Success"
				// ];
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a Api Key .",
					'class' => "alert-success",
					'status_id' => "10",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			} else {
				$this->session->set_flashdata('alert', [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "10",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
				
			}
	}
	public function apiEdit(){
		
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['api_id'];
			if (!empty($id)) {
			$update=$this->bookings_config_model->apiUpdate([
				'api_key' 		=> @$_POST['api_key'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated Api Key.",
					'class' => "alert-success",
					'status_id' => "10",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			}else {
				$this->session->set_flashdata('alert', [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "10",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
			}
		}else {
				$this->session->set_flashdata('alert', [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "10",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
			}
	}else {
				$this->session->set_flashdata('alert', [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "10",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
			}
	}
	//add price status functions
function do_upload_image($file,$name) {
		     
		        $filefullName            =$file['name'];
                $fileExt                 =explode('.',$filefullName);
                $fileActEct              =strtolower(end($fileExt));
                $fileName                =uniqid().".".$fileActEct;
                echo "file name:".$fileName;
                $fileNewName             ='uploads/custom_images/'.$fileName;
                $path_to_folder          ='uploads/custom_images/';
                $config['upload_path']   =realpath($path_to_folder);
                $config['allowed_types'] ='gif|jpg|jpeg|png';
                $config['max_size']      ='3000000'; // 3mb
                $config['file_name']     =$fileName;
        
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload($name))
                {
                	return $fileNewName;
                }
                else{
                  return NULL;
                }
	}
	public function pricestatusadd(){
		
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->pricestatusstore();
		}else show_404();

	}
//price status store
	public function pricestatusstore(){
			// $error = request_validate();
			if (empty($error)) {
				//$dob = to_unix_date(@$_POST['dob']);
				$user = $this->basic_auth->user();
			 	$user_id=$user->id;
                $pendingicon=$this->do_upload_image($_FILES['statutpendingicon'],'statutpendingicon');
                $approvedicon=$this->do_upload_image($_FILES['statutapprovedicon'],'statutapprovedicon');
                $deniedicon=$this->do_upload_image($_FILES['statutdeniedicon'],'statutdeniedicon');
                $cancelledicon=$this->do_upload_image($_FILES['statutcancelledicon'],'statutcancelledicon');
                $customicon=$this->do_upload_image($_FILES['customicon'],'customicon');

                 $bookingpendingicon=$this->do_upload_image($_FILES['bookingstatutpendingicon'],'bookingstatutpendingicon');
                $bookingconfirmedicon=$this->do_upload_image($_FILES['bookingstatutconfirmedicon'],'bookingstatutconfirmedicon');
                $bookingcancelledicon=$this->do_upload_image($_FILES['bookingstatutcancelledicon'],'bookingstatutcancelledicon');
                $bookingcustomicon=$this->do_upload_image($_FILES['bookingcustomicon'],'bookingcustomicon');
    
				$id = $this->bookings_config_model->addpricestatus([
					'message' 		=> @$_POST['message'],
					'statut' 		=> @$_POST['statut'],
					'pricestatus' =>@$_POST['pricestatus'],
					'statutpendingicon'  => $pendingicon,
					'customicon'  => $customicon,
					'statutapprovedicon'  => $approvedicon,
					'statutdeniedicon'  => $deniedicon,
					'statutcancelledicon'  => $cancelledicon,
					'customiconwidth'  => $_POST['customiconwidth'],
					'customiconheight'  => $_POST['customiconheight'],
					'pendingiconwidth'  => $_POST['pendingiconwidth'],
					'pendingiconheight'  => $_POST['pendingiconheight'],
					'approvediconwidth'  => $_POST['approvediconwidth'],
					'approvediconheight'  => $_POST['approvediconheight'],
					'deniediconwidth'  => $_POST['deniediconwidth'],
					'deniediconheight'  => $_POST['deniediconheight'],
					'cancellediconwidth'  => $_POST['cancellediconwidth'],
					'cancellediconheight'  => $_POST['cancellediconheight'],
					'bookingcustomicon'  => $bookingcustomicon,
					'bookingstatutpendingicon'  => $bookingpendingicon,
					'bookingstatutconfirmedicon'  => $bookingconfirmedicon,
					'bookingstatutcancelledicon'  => $bookingcancelledicon,
					'bookingcustomiconwidth'  => $_POST['bookingcustomiconwidth'],
					'bookingcustomiconheight'  => $_POST['bookingcustomiconheight'],
					'bookingpendingiconwidth'  => $_POST['bookingpendingiconwidth'],
					'bookingpendingiconheight'  => $_POST['bookingpendingiconheight'],
					'bookingconfirmediconwidth'  => $_POST['bookingconfirmediconwidth'],
					'bookingconfirmediconheight'  => $_POST['bookingconfirmediconheight'],
					'bookingcancellediconwidth'  => $_POST['bookingcancellediconwidth'],
					'bookingcancellediconheight'  => $_POST['bookingcancellediconheight'],
					'quotedelay'  =>$_POST['quotedelay'],
					'customtext'  =>$_POST['customtext'],
					'created_at' 		=> @date('Y-m-d'),
					'time' 		=> @date("H : i"),
					'user_id' 		=> $user_id,
				]);
				// $this->data['alert'] = [
				// 	'message' => ': Successfully added a statut.',
				// 	'class' => "alert-success",
				// 	'type' => "Success"
				// ];
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a row.",
					'class' => "alert-success",
					'status_id' => "10",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			} else {
				$this->session->set_flashdata('alert', [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "10",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
				
			}
	}
//price status store

//price status edit
	public function pricestatusEdit(){
		
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['id'];
			if (!empty($id)) {

			$pricestatus = $this->bookings_config_model->getpricestatusdata('vbs_price_status');
                
                
                $customicon='';
                $pendingicon='';
                $approvedicon='';
                $deniedicon='';
                $cancelledicon='';

                if(!empty($_FILES['customicon']['name']))
                {
                  $customicon=$this->do_upload_image($_FILES['customicon'],'customicon');
                }else{
                	$customicon=$pricestatus->customicon;
                }
                if(!empty($_FILES['statutpendingicon']['name']))
                {
                 $pendingicon=$this->do_upload_image($_FILES['statutpendingicon'],'statutpendingicon');
                }else{
                	$pendingicon=$pricestatus->statutpendingicon;
                }
                if(!empty($_FILES['statutapprovedicon']['name']))
                {
                 $approvedicon=$this->do_upload_image($_FILES['statutapprovedicon'],'statutapprovedicon');
                }else{
                 $approvedicon=$pricestatus->statutapprovedicon;
                }
                 if(!empty($_FILES['statutdeniedicon']['name']))
                {
                 $deniedicon=$this->do_upload_image($_FILES['statutdeniedicon'],'statutdeniedicon');
                }else{
                 $deniedicon=$pricestatus->statutdeniedicon;
                }
                 if(!empty($_FILES['statutcancelledicon']['name']))
                {
                 $cancelledicon=$this->do_upload_image($_FILES['statutcancelledicon'],'statutcancelledicon');
                }else{
                 $cancelledicon=$pricestatus->statutcancelledicon;
                }
            //booking icons
                $bookingcustomicon='';
                $bookingpendingicon='';
                $bookingconfirmedicon='';
                $bookingcancelledicon='';

                if(!empty($_FILES['bookingcustomicon']['name']))
                {
                  $bookingcustomicon=$this->do_upload_image($_FILES['bookingcustomicon'],'bookingcustomicon');
                }else{
                	$bookingcustomicon=$pricestatus->bookingcustomicon;
                }
                if(!empty($_FILES['bookingstatutpendingicon']['name']))
                {
                 $bookingpendingicon=$this->do_upload_image($_FILES['bookingstatutpendingicon'],'bookingstatutpendingicon');
                }else{
                	$bookingpendingicon=$pricestatus->bookingstatutpendingicon;
                }
                if(!empty($_FILES['bookingstatutconfirmedicon']['name']))
                {
                 $bookingconfirmedicon=$this->do_upload_image($_FILES['bookingstatutconfirmedicon'],'bookingstatutconfirmedicon');
                }else{
                 $bookingconfirmedicon=$pricestatus->bookingstatutconfirmedicon;
                }
               
                 if(!empty($_FILES['bookingstatutcancelledicon']['name']))
                {
                 $bookingcancelledicon=$this->do_upload_image($_FILES['bookingstatutcancelledicon'],'bookingstatutcancelledicon');
                }else{
                 $bookingcancelledicon=$pricestatus->bookingstatutcancelledicon;
                }
            //booking icons
			$update=$this->bookings_config_model->pricestatusUpdate([
				'message' 		=> @$_POST['message'],
				'pricestatus' =>@$_POST['pricestatus'],
				'statut' 		=> @$_POST['statut'],
				'statutpendingicon'  => $pendingicon,
				'customicon'  => $customicon,
				'statutapprovedicon'  => $approvedicon,
				'statutdeniedicon'  => $deniedicon,
				'statutcancelledicon'  => $cancelledicon,
				'customiconwidth'  => $_POST['customiconwidth'],
				'customiconheight'  => $_POST['customiconheight'],
				'pendingiconwidth'  => $_POST['pendingiconwidth'],
				'pendingiconheight'  => $_POST['pendingiconheight'],
				'approvediconwidth'  => $_POST['approvediconwidth'],
				'approvediconheight'  => $_POST['approvediconheight'],
				'deniediconwidth'  => $_POST['deniediconwidth'],
				'deniediconheight'  => $_POST['deniediconheight'],
				'cancellediconwidth'  => $_POST['cancellediconwidth'],
				'cancellediconheight'  => $_POST['cancellediconheight'],
				'bookingcustomicon'  => $bookingcustomicon,
				'bookingstatutpendingicon'  => $bookingpendingicon,
				'bookingstatutconfirmedicon'  => $bookingconfirmedicon,
				'bookingstatutcancelledicon'  => $bookingcancelledicon,
				'bookingcustomiconwidth'  => $_POST['bookingcustomiconwidth'],
				'bookingcustomiconheight'  => $_POST['bookingcustomiconheight'],
				'bookingpendingiconwidth'  => $_POST['bookingpendingiconwidth'],
				'bookingpendingiconheight'  => $_POST['bookingpendingiconheight'],
				'bookingconfirmediconwidth'  => $_POST['bookingconfirmediconwidth'],
				'bookingconfirmediconheight'  => $_POST['bookingconfirmediconheight'],
				'bookingcancellediconwidth'  => $_POST['bookingcancellediconwidth'],
				'bookingcancellediconheight'  => $_POST['bookingcancellediconheight'],
				'quotedelay'  =>$_POST['quotedelay'],
				'customtext'  =>$_POST['customtext']
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a row.",
					'class' => "alert-success",
					'status_id' => "10",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			}else {
				$this->session->set_flashdata('alert', [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "10",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
			}
		}else {
				$this->session->set_flashdata('alert', [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "10",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
			}
	}else {
				$this->session->set_flashdata('alert', [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "10",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
			}
	}

//price status edit
		public function get_ajax_pricestatus(){
		$id=(int)$_GET['pricestatus_id'];

		if($id>0){
			
			$pricestatus_data = $this->bookings_config_model->booking_Config_getAll('vbs_price_status',['id' => $id]);
			?>
			
			<?php
			foreach($pricestatus_data as $key => $item){
				$result='<input type="hidden" name="id" value="'.$item->id.'">
				<div class="col-md-12">
                <div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span>Statut</span>
						<select class="form-control" name="statut" required>
							
							<option '; if ($item->statut == "1") {$result.='selected="selected" '; }
							$result.=' value="1">Show</option>
							<option ';
							if ($item->statut == "0") { $result.='selected="selected" '; }
							$result.=' value="0">Hide</option>
						</select>
				</div>
				</div>
				  <div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span>Price</span>
						<select class="form-control" name="pricestatus" id="editpricestatus" required onchange="editshowpricemessage()">
							
							<option '; if ($item->pricestatus  == "1") {$result.='selected="selected" '; }
							$result.=' value="1">Show</option>
							<option ';
							if ($item->pricestatus  == "0") { $result.='selected="selected" '; }
							$result.=' value="0">Hide</option>
						</select>
				</div>
				</div>
				<</div>
				 <div class="col-md-12" style="margin-top: 5px;display: none;"  id="editpricemessage">
                                <div class="form-group">
                                  <span>Message</span>
                                  <textarea id="editpricemessagecontent" class="form-control" name="message" rows="8" cols="80">'.$item->message.'</textarea>
                                </div>
                              </div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}



		public function deletepricestatus(){
		$this->load->model('bookings_config_model');
		$id=$_POST['delet_pricestatus_id'];
		$del=$this->bookings_config_model->deletepricestatus($id);
		if ($del==true) {
		$this->session->set_flashdata('alert', [
				'message' => "Successfully deleted.",
				'class' => "alert-success",
				'status_id' => "10",
				'type' => "Success"
		]);
		redirect('admin/booking_config');
		}
		else {
			$this->session->set_flashdata('alert', [
					'message' => "Please try again.",
					'class' => "alert-danger",
					'status_id' => "10",
					'type' => "Error"
			]);
			redirect('admin/booking_config');
		}
	}
       //end price status
		//add disable category
	public function disablecatadd(){
		
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->disablecatstore();
		}else show_404();

	}
public function disablecatstore(){
			// $error = request_validate();
			if (empty($error)) {
				//$dob = to_unix_date(@$_POST['dob']);
				$user = $this->basic_auth->user();
			 	$user_id=$user->id;


				$id = $this->bookings_config_model->addDisablecat([
					'name_of_disablecategory' 		=> @$_POST['name_of_disablecat'],
					'statut' 		=> @$_POST['statut'],
					'created_at' 		=> @date('Y-m-d'),
					'time' 		=> @date("H : i"),
					'user_id' 		=> $user_id,
				]);
				// $this->data['alert'] = [
				// 	'message' => ': Successfully added a disable category.',
				// 	'class' => "alert-success",
				// 	'type' => "Success"
				// ];
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a disable category.",
					'class' => "alert-success",
					'status_id' => "14",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			} else {
					$this->session->set_flashdata('alert', [
					'message' => ": Please try again",
					'class' => "alert-danger",
					'status_id' => "14",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
				
			}
	}
	public function disablecatEdit(){
		
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['disablecat_id'];
			if (!empty($id)) {
			$update=$this->bookings_config_model->disablecatUpdate([
				'name_of_disablecategory' 		=> @$_POST['name_of_disablecat'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a disable category.",
					'class' => "alert-success",
					'status_id' => "14",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			}else {
				$this->session->set_flashdata('alert', [
					'message' => ": Please try again",
					'class' => "alert-danger",
					'status_id' => "14",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
			}
		}else {
				$this->session->set_flashdata('alert', [
					'message' => ": Please try again",
					'class' => "alert-danger",
					'status_id' => "14",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
			}
	}else {
				$this->session->set_flashdata('alert', [
					'message' => ": Please try again",
					'class' => "alert-danger",
					'status_id' => "14",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
			}
	}
	public function get_ajax_disablecat(){
		$id=(int)$_GET['disablecat_id'];

		if($id>0){
			
			$disablecat_data = $this->bookings_config_model->booking_Config_getAll('vbs_u_disablecategory',['id' => $id]);
			?>
			
			<?php
			foreach($disablecat_data as $key => $disablecat){
				$result='<input type="hidden" name="disablecat_id" value="'.$disablecat->id.'">
				<div class="col-md-12">
                <div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span>Statut</span>
						<select class="form-control" name="statut" required >
							
							<option '; if ($disablecat->statut == "1") {$result.='selected="selected" '; }
							$result.=' value="1">Show</option>
							<option ';
							if ($disablecat->statut == "0") { $result.='selected="selected" '; }
							$result.=' value="0">Hide</option>
						</select>
				</div>
				</div>
				<div class="col-md-3" style="margin-top: 5px;">
				<div class="form-group">
					<span>Name Of Disable Category</span>
					<input type="text" class="form-control" name="name_of_disablecat" placeholder="" value="'.$disablecat->name_of_disablecategory.'">
				</div>
				</div></div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}



		public function deleteDisablecat(){
		$this->load->model('bookings_config_model');
		$id=$_POST['delet_disablecat_id'];
		$del=$this->bookings_config_model->deletedisablecat($id);
		if ($del==true) {
		$this->session->set_flashdata('alert', [
				'message' => "Successfully deleted.",
				'class' => "alert-success",
				'status_id' => "14",
				'type' => "Success"
		]);
		redirect('admin/booking_config');
		}
		else {
			$this->session->set_flashdata('alert', [
					'message' => "Please try again.",
					'class' => "alert-danger",
					'status_id' => "14",
					'type' => "Error"
			]);
			redirect('admin/booking_config');
		}
	}

	//end disable category
	//Quote Config Section
	public function quoteconfigAdd(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->quoteconfig_store();
		}else show_404();
	}
	public function quoteconfig_store(){

			// $error = request_validate();
			if (empty($error)) {
				$file=$_FILES['customicon'];
                $fileName=$file['name'];
                $fileExt=explode('.',$fileName);
                $fileActEct=strtolower(end($fileExt));
                $fileName=time().".".$fileActEct;
                $fileNewName='uploads/custom_images/'.$fileName;
                $path_to_folder = 'uploads/custom_images/';
                $config['upload_path'] = realpath($path_to_folder);
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['max_size']     = '3000000'; // 3mb
                $config['file_name']    = $fileName;
        
                $this->load->library('upload', $config);
              
                if ($this->upload->do_upload('customicon'))
                {
				//$dob = to_unix_date(@$_POST['dob']);
				$user = $this->basic_auth->user();
				$user_id=$user->id;
				$table='vbs_quotes_config';
				$id = $this->bookings_config_model->booking_Config_Add($table,[
					'comment' 		=> @$_POST['comment'],
					'servicecategoryid' 		=> @$_POST['servicecategoryid'],
					'statut' 		=> @$_POST['statut'],
					'customicon'    => $fileNewName,
					'availabledate' 		=> @$_POST['availabledate'],
					'created_at' 		=> @date('Y-m-d'),
					'time' 		=> @date("H : i"),
					'user_id' 		=> @$user_id,
				]);
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a quotes.",
					'class' => "alert-success",
					'status_id' => "15",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			} else {
				$this->session->set_flashdata('alert', [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error",
					'status_id' => "15"
				]);
				redirect('admin/booking_config');
			}
               }
			else {
				$this->session->set_flashdata('alert', [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error",
					'status_id' => "15"
				]);
				redirect('admin/booking_config');
			}
	}
	public function quoteconfigEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['quoteconfig_id'];
			if (!empty($id)) {
			 if(!empty($_FILES['customicon']['name']))
             {
                $file=$_FILES['customicon'];
                $fileName=$file['name'];
                $fileExt=explode('.',$fileName);
                $fileActEct=strtolower(end($fileExt));
                $fileName=time().".".$fileActEct;
                $fileNewName='uploads/custom_images/'.$fileName;
                $path_to_folder = 'uploads/custom_images/';
                $config['upload_path'] = realpath($path_to_folder);
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['max_size']     = '3000000'; // 3mb
                $config['file_name']    = $fileName;
        
                $this->load->library('upload', $config);
              
                if ($this->upload->do_upload('customicon'))
                {
				$table='vbs_quotes_config';
				$update=$this->bookings_config_model->booking_Config_Update($table,[
				'comment' 		=> @$_POST['comment'],
				'servicecategoryid' 		=> @$_POST['servicecategoryid'],
				'statut' 		=> @$_POST['statut'],
				'customicon'    => $fileNewName,
				'availabledate' 		=> @$_POST['availabledate']
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a quotes.",
					'class' => "alert-success",
					'status_id' => "15",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			}else {
				$this->session->set_flashdata('alert', [
						'message' => "Please try again.",
						'class' => "alert-danger",
						'status_id' => "15",
						'type' => "Error"
				]);
				redirect('admin/booking_config');
	       }
		}
	}else{
		$table='vbs_quotes_config';
				$update=$this->bookings_config_model->booking_Config_Update($table,[
				'comment' 		=> @$_POST['comment'],
				'servicecategoryid' 		=> @$_POST['servicecategoryid'],
				'statut' 		=> @$_POST['statut'],
				'availabledate' 		=> @$_POST['availabledate']
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Service.",
					'class' => "alert-success",
					'status_id' => "15",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			}else {
				$this->session->set_flashdata('alert', [
						'message' => "Please try again.",
						'class' => "alert-danger",
						'status_id' => "15",
						'type' => "Error"
				]);
				redirect('admin/booking_config');
			}
	}
	}else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "15",
				'type' => "Error"
		]);
		redirect('admin/booking_config');
	}
}else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "15",
				'type' => "Error"
		]);
		redirect('admin/booking_config');
	}
	}
	public function quoteconfigDelete(){
	$this->load->model('bookings_config_model');
	$id=$_POST['delet_quoteconfig_id'];
	$table = "vbs_quotes_config";
	$del=$this->bookings_config_model->booking_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "15",
			'type' => "Success"
	]);
	redirect('admin/booking_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "15",
				'type' => "Error"
		]);
		redirect('admin/booking_config');
	}
	show_404();
	}
public function get_ajax_quoteconfig(){
		$id=(int)$_GET['quoteconfig_id'];

		if($id>0){
			$this->load->model('bookings_config_model');
			$quoteconfig_data = $this->bookings_config_model->booking_Config_getAll('vbs_quotes_config',['id' => $id]);
			
			?>
		
			<?php
			$all_service = $this->bookings_config_model->getAllData('vbs_u_category_service');
			
				foreach($quoteconfig_data as $key => $item){
				$result='  <input type="hidden" name="quoteconfig_id" value="'.$item->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
					<div class="form-group">
					<span>Statut</span>
							<select class="form-control" name="statut" required >
								
								<option '; if ($item->statut == "1") {$result.='selected="selected" '; }
								$result.=' value="1">Show</option>
								<option ';
								if ($item->statut == "0") { $result.='selected="selected" '; }
								$result.=' value="0">Hide</option>
							</select>
					</div>
				</div>
				<div class="col-md-2" style="margin-top: 5px;">
					<div class="form-group">
					<span>Service Category</span>
							<select class="form-control " name="servicecategoryid" required >
							<option value="">Select</option>
							'; foreach($all_service as $value): ; $result.='<option '; if($value->id == $item->servicecategoryid){ $result.='selected="selected" '; } $result.=' value="'.$value->id.'">'.$value->category_name.'</option>
							'; endforeach; $result.='
							</select>

					</div>
				</div>
				<div class="col-md-2" style="margin-top: 5px;">
					<div class="form-group">
					<span>Available Date (days)</span>
						<input type="text" class="form-control" name="availabledate" placeholder="" value="'.$item->availabledate.'" required>
					</div>
				</div>
                <div class="col-md-2" style="margin-top: 5px;">
			';
                if(!empty($item->customicon)){
                    $result.='<div class="col-md-12"> 
		                         <img  id="editcustom_logo_preview" src="'.base_url($item->customicon).'" style="width: 200px; height: 150px;" >
		                        </div>';
                       }else{
                       	 $result.='<div class="col-md-12"> 
		                         <img  id="editcustom_logo_preview" src="'.base_url('uploads/vehicle_images/default_custom.png').'" style="width: 200px; height: 150px;" >
		                        </div>';
                       }
			 $result.='<div class="col-md-12">
                         <div class="form-group">
            <input type="file" name="customicon" class="form-control-file"  onChange="editcustompreview(this)"   style="width:100%;">
            <span>Custom Icon</span>
          </div>
       </div>
         

    </div>
				</div>
				<div class="col-md-12" style="margin-top: 5px;">
        <div class="form-group">
          <span>Comments</span>
          <textarea id="edittextareaquotecontent" class="form-control" name="comment" rows="8" cols="80">'.$item->comment.'</textarea>
        </div>
      </div>
				';
				}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
//Quote Config Section
//invoice config section
public function invoicestatusadd(){
		
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->invoicestatusstore();
		}else show_404();

	}
	//invoice status store
public function invoicestatusstore(){
			// $error = request_validate();
			if (empty($error)) {
				//$dob = to_unix_date(@$_POST['dob']);
				$user = $this->basic_auth->user();
			 	$user_id=$user->id;
                $pendingicon=$this->do_upload_image($_FILES['statutpendingicon'],'statutpendingicon');
                $paidicon=$this->do_upload_image($_FILES['statutpaidicon'],'statutpaidicon');
                $cancelledicon=$this->do_upload_image($_FILES['statutcancelledicon'],'statutcancelledicon');
                $customicon=$this->do_upload_image($_FILES['customicon'],'customicon');
    
				$id = $this->bookings_config_model->addinvoicestatus([
					'statut' 		=> @$_POST['statut'],
					'statutpendingicon'  => $pendingicon,
					'statutpaidicon'  => $paidicon,
					'customicon'  => $customicon,
					'statutcancelledicon'  => $cancelledicon,
					'customiconwidth'  => $_POST['customiconwidth'],
					'customiconheight'  => $_POST['customiconheight'],
					'pendingiconwidth'  => $_POST['pendingiconwidth'],
					'pendingiconheight'  => $_POST['pendingiconheight'],
					'paidiconwidth'  => $_POST['paidiconwidth'],
					'paidiconheight'  => $_POST['paidiconheight'],
					'cancellediconwidth'  => $_POST['cancellediconwidth'],
					'cancellediconheight'  => $_POST['cancellediconheight'],
					'invoicedelay'  =>$_POST['invoicedelay'],
					'customtext'  =>$_POST['customtext'],
					'created_at' 		=> @date('Y-m-d'),
					'time' 		=> @date("H : i"),
					'user_id' 		=> $user_id,
				]);
				
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a row.",
					'class' => "alert-success",
					'status_id' => "15",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			} else {
				$this->session->set_flashdata('alert', [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "15",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
				
			}
	}

//invoice status store
//invoice status edit
public function invoicestatusEdit(){
		
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['id'];
			if (!empty($id)) {

			$invoicestatus = $this->bookings_config_model->getinvoicestatusdata('vbs_invoices_config');
                
                
                $customicon='';
                $pendingicon='';
                $paidicon='';
                $cancelledicon='';

                if(!empty($_FILES['customicon']['name']))
                {
                  $customicon=$this->do_upload_image($_FILES['customicon'],'customicon');
                }else{
                	$customicon=$invoicestatus->customicon;
                }
                if(!empty($_FILES['statutpendingicon']['name']))
                {
                 $pendingicon=$this->do_upload_image($_FILES['statutpendingicon'],'statutpendingicon');
                }else{
                	$pendingicon=$invoicestatus->statutpendingicon;
                }
                 if(!empty($_FILES['statutpaidicon']['name']))
                {
                 $paidicon=$this->do_upload_image($_FILES['statutpaidicon'],'statutpaidicon');
                }else{
                	$paidicon=$invoicestatus->statutpaidicon;
                }
                if(!empty($_FILES['statutcancelledicon']['name']))
                {
                 $cancelledicon=$this->do_upload_image($_FILES['statutcancelledicon'],'statutcancelledicon');
                }else{
                 $cancelledicon=$invoicestatus->statutcancelledicon;
                }


			$update=$this->bookings_config_model->invoicestatusUpdate([
				'statut' 		=> @$_POST['statut'],
				'statutpendingicon'  => $pendingicon,
				'statutpaidicon'  => $paidicon,
				'customicon'  => $customicon,
				'statutcancelledicon'  => $cancelledicon,
				'customiconwidth'  => $_POST['customiconwidth'],
				'customiconheight'  => $_POST['customiconheight'],
				'pendingiconwidth'  => $_POST['pendingiconwidth'],
				'pendingiconheight'  => $_POST['pendingiconheight'],
				'paidiconwidth'  => $_POST['paidiconwidth'],
				'paidiconheight'  => $_POST['paidiconheight'],
				'cancellediconwidth'  => $_POST['cancellediconwidth'],
				'cancellediconheight'  => $_POST['cancellediconheight'],
				'invoicedelay'  =>$_POST['invoicedelay'],
				'customtext'  =>$_POST['customtext']
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a row.",
					'class' => "alert-success",
					'status_id' => "15",
					'type' => "Success"
				]);
				redirect('admin/booking_config');
			}else {
				$this->session->set_flashdata('alert', [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "15",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
			}
		}else {
				$this->session->set_flashdata('alert', [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "15",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
			}
	}else {
				$this->session->set_flashdata('alert', [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "15",
					'type' => "Error"
				]);
				redirect('admin/booking_config');
			}
	}
//invoice status edit

//invoice config section
}
