<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partners extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('general');
        $this->load->helper('language');
        $this->load->helper('validate');
        $this->load->helper('mypdf');
        if (!$this->basic_auth->is_login())
            redirect("admin", 'refresh');
        else
        $this->data['user'] = $this->basic_auth->user(); 
        $this->load->model('partners_model');  
        $this->load->model('request_model');
        $this->load->model('cms_model');
        $this->load->model('base_model');
        $this->load->model('jobs_model');
        $this->load->model('support_model');
        $this->load->model('calls_model');
        $this->load->model('notes_model');
        $this->load->model('notifications_model');
        $this->load->model('userx_model');
        $this->data['configuration'] = get_configuration();

    }

    
    public function index(){

        $this->data['user'] = $this->basic_auth->user();
        $this->data['css_type']     = array("form","booking_datatable","datatable");
        $this->data['active_class'] = "partners";
        $this->data['gmaps']        = false;
        $this->data['title']        = 'Partners';
        $this->data['title_link']   = base_url('admin/partners');
        $this->data['content']      = 'admin/partners/profile';

        $data = [];
        $this->data['countries'] = $this->cms_model->get_all_countries();
        $this->data['partner_status_data'] = $this->partners_model->getAllData('vbs_partnerstatus');
        $this->data['civilite_data'] = $this->partners_model->getAllData('vbs_partnercivilite');
        $this->data['users_data'] = $this->userx_model->getAll(['grp.name'=>'admin']);
        /*$this->data['post_data'] = $this->partners_model->getAllData('vbs_partnerpost');
        $this->data['contract_data'] = $this->partners_model->getAllData('vbs_partnercontract');
        
        $this->data['pattern_data'] = $this->partners_model->getAllData('vbs_partnerpattern');
        $this->data['nature_data'] = $this->partners_model->getAllData('vbs_partnernature');
        $this->data['hours_data'] = $this->partners_model->getAllData('vbs_partnerhours');
        $this->data['type_data'] = $this->partners_model->getAllData('vbs_partnertype');
        $this->data['cars'] = $this->partners_model->getAllData('vbs_carsadded');*/
        $flashbookdata =  $this->session->flashdata('searchdata');
        if(isset($flashbookdata['record']) ){
          $this->data['partner_data'] = $flashbookdata['record'];
          $this->data['search_from_period']=$flashbookdata['fields']['search_from_period'];
          $this->data['search_to_period']=$flashbookdata['fields']['search_to_period'];
          $this->data['search_status']=$flashbookdata['fields']['search_status'];
          $this->data['search_category']=$flashbookdata['fields']['search_category'];
          $this->data['search_hours']=$flashbookdata['fields']['search_hours'];
         }else{
           $this->data['partner_data'] = $this->partners_model->getAllData('vbs_partners');
         }
         $this->data['googleapikey']=$this->partners_model->getuniquerecord("vbs_googleapi");
           
        $this->_render_page('templates/admin_template', $this->data);
    }
 public function profileAdd(){

      if ($_SERVER['REQUEST_METHOD'] === 'POST') {
          $this->profileStore();
      }else{
          return redirect("admin/partners");
      }
  }
  public function profileStore(){

          if (empty($error)) {   
                //date converation
             
            $dob=str_replace('/', '-', $_POST['dob']);
            $dob=strtotime($dob);
            $dob = date('Y-m-d',$dob);

              //date converation
               $username     = $this->input->post('prenom') . ' ' . $this->input->post('nom');
               $email        = strtolower($this->input->post('email'));
               $password     = date("dmY", strtotime($_POST['dob']));
               $user_role    = 8;
               $isemailexist = $this->partners_model->isemailalreayexist(['email'=>$email],'vbs_users');

               if($isemailexist){
                       $this->session->set_flashdata('alert', [
                           'message' => ": Email is already exist in users.",
                           'class' => "alert-danger",
                           'type' => "Error"
                       ]);
                       redirect('admin/partners', 'refresh');
               }


               $user_group = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('groups') . " WHERE name = 'partners'");

               $group_id        = array($user_group[0]->id);
               $additional_data = array(
                   'civility'     => $this->partners_model->getSingleRecord('vbs_partnercivilite',['id'=>$this->input->post('civilite')])->civilite,
                   'first_name'   => $this->input->post('prenom'),
                   'last_name'    => $this->input->post('nom'),
                   'phone'        => $this->input->post('phone'),
                   'country'      => $this->input->post('country'),
                   'region'       => $this->input->post('region'),
                   'city'         => $this->input->post('city'),
                   'zipcode'      => $this->input->post('zipcode'),
                   'address'      => $this->input->post('address'),
                   'address1'     => $this->input->post('address2'),
                   'image'        => $this->do_user_upload_image($_FILES['profile_image'],'profile_image'),
                   'mobile'       => trim($this->input->post('mobile')),
                   'dob'          => $dob
               );

               if ($this->ion_auth->register($user_role, $username, $password, $email, $additional_data, $group_id)) {
                   $user_rec = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('users') . " WHERE email = '" . $email . "'");

              $user = $this->basic_auth->user();
              $user_id=$user->id;
             
            
              $id = $this->partners_model->createRecord('vbs_partners',[
                  'statut'                      =>  $_POST['statut'],
                  'civilite'                    =>  $_POST['civilite'],
                  'nom'                         =>  trim($_POST['nom']),
                  'prenom'                      =>  trim($_POST['prenom']),
                  'address'                     =>  trim($_POST['address']),
                  'address2'                    =>  trim($_POST['address2']),
                  'zipcode'                     =>  trim($_POST['zipcode']),
                  'email'                       =>  trim($_POST['email']),
                  'phone'                       =>  trim($_POST['phone']),
                  'mobile'                      =>  trim($_POST['mobile']),
                  'fax'                         =>  trim($_POST['fax']),
                  'profile_image'               =>  $this->do_upload_image($_FILES['profile_image'],'profile_image'),
                  'dob'                         =>  $dob,
                  'city'                        =>  $_POST['city'],
                  'region'                      =>  $_POST['region'],
                  'country'                     =>  $_POST['country'],
                  'category'                    =>  $_POST['category'],
                  'source'                      =>  $_POST['source'],
                  'advisor'                     =>  $_POST['advisor'],
                  'support'                     =>  $_POST['support'],
                  'company'                     =>  (($_POST['category'] == '1')?trim($_POST['company']):''),
                  'userid'                      =>  $user_rec[0]->id,
                  'added_by_user'               =>  $user_id
              ]);
              
              if($id){
                 $currentdate = date('y-m-d');
                   $exp_array=array(
                    'datedexpiration' =>   $currentdate,
                    'datedexpiration2' =>  $currentdate,
                    'datedexpiration3' =>  $currentdate,
                    'datedexpiration4' =>  $currentdate,
                    'datedexpiration5' =>  $currentdate,
                   );

                   $isremindercreate=$this->reminder_store($id,$exp_array);
                   $isnotificationcreate=$this->notification_store($id,$exp_array);
                   if($isremindercreate == true && $isnotificationcreate == true){
                            $this->session->set_flashdata('alert', [
                             'message'   => ": Successfully added a partner profile .",
                             'class'     => "alert-success",
                             'status_id' => "1",
                             'type'      => "Success"
                              ]);
                           redirect('admin/partners');
                   }else{
                        $this->data['alert'] = [
                          'message' => "Partner's notification or reminder missing in notification or reminder page. Please add these and try again.",
                          'class'   => "alert-danger",
                          'status_id' => "1",
                          'type'    => "Error"
                            ];
                            redirect('admin/partners');
                        }
                   }
              else{
                   $this->data['alert'] = [
                  'message' => 'Please try again',
                  'class'   => "alert-danger",
                  'status_id' => "1",
                  'type'    => "Error"
                    ];
                    redirect('admin/partners');
                  }
            }
          } else {
              $this->data['alert'] = [
                  'message' => 'Please try again',
                  'class'   => "alert-danger",
                  'status_id' => "1",
                  'type'    => "Error"
              ];
              redirect('admin/partners');
          }
  }


    public function profileEdit(){
      

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $id   = $this->input->post('profile_id');
            if (!empty($id)) {

                   //date converation
            $dob=str_replace('/', '-', $_POST['dob']);
            $dob=strtotime($dob);
            $dob = date('Y-m-d',$dob);  
                //date converation

               
                $partner        = $this->partners_model->getSingleRecord('vbs_partners',['id'=>$id]);
                $userprofile   = $this->partners_model->getSingleRecord('vbs_users',['id'=>$partner->userid]);

                $partneruserimg='';
                $profile_image='';
             
                

                if(!empty($_FILES['profile_image']['name']))
                {
                  $partneruserimg=$this->do_user_upload_image($_FILES['profile_image'],'profile_image');
                }else{
                    
                    if($_POST['partner_image_pos'] == '1'){
                      $partneruserimg=$userprofile->image;
                    }else{
                      $partneruserimg=NULL;
                    }
                }
                if(!empty($_FILES['profile_image']['name']))
                {
                  $profile_image=$this->do_upload_image($_FILES['profile_image'],'profile_image');
                }else{
                   
                    if($_POST['partner_image_pos'] == '1'){
                      $profile_image=$partner->profile_image;
                    }else{
                      $profile_image=NULL;
                    }
                }
            
                 $password=md5(date("dmY", strtotime($_POST['dob'])));
             $db_array = array(
                'civility'     => $this->partners_model->getSingleRecord('vbs_partnercivilite',['id'=>$this->input->post('civilite')])->civilite,
                'first_name'   => $this->input->post('prenom'),
                'last_name'    => $this->input->post('nom'),
                'phone'        => $this->input->post('phone'),
                'country'      => $this->input->post('country'),
                'region'       => $this->input->post('region'),
                'city'         => $this->input->post('city'),
                'zipcode'      => $this->input->post('zipcode'),
                'address'      => $this->input->post('address'),
                'address1'     => $this->input->post('address2'),
                'image'        => $partneruserimg,
                'mobile'       => trim($this->input->post('mobile')),
                'dob'          => $dob
                );

                $updateuserprofile=$this->userx_model->update($db_array,$userprofile->id);
               
               
                if($updateuserprofile){
                      $update=$this->partners_model->createUpdate('vbs_partners',[
                      'statut'                      =>  $_POST['statut'],
                      'civilite'                    =>  $_POST['civilite'],
                      'nom'                         =>  trim($_POST['nom']),
                      'prenom'                      =>  trim($_POST['prenom']),
                      'address'                     =>  trim($_POST['address']),
                      'address2'                    =>  trim($_POST['address2']),
                      'zipcode'                     =>  trim($_POST['zipcode']),
                      'email'                       =>  trim($_POST['email']),
                      'phone'                       =>  trim($_POST['phone']),
                      'mobile'                      =>  trim($_POST['mobile']),
                      'fax'                         =>  trim($_POST['fax']),
                      'profile_image'               =>  $profile_image,
                      'dob'                         =>  $dob,
                      'city'                        =>  $_POST['city'],
                      'region'                      =>  $_POST['region'],
                      'country'                     =>  $_POST['country'],
                      'category'                    =>  $_POST['category'],
                      'source'                      =>  $_POST['source'],
                      'advisor'                     =>  $_POST['advisor'],
                      'support'                     =>  $_POST['support'],
                      'company'                     =>  (($_POST['category'] == '1')?trim($_POST['company']):'')
                      ], $id);
                      if ($update){
                        
                         
                                $this->session->set_flashdata('alert', [
                                    'message' => ": Successfully updated a partner profile.",
                                    'class' => "alert-success",
                                    'status_id' => "1",
                                    'type' => "Success"
                                ]);
                                redirect('admin/partners');
                         
                         
                      }else {
                        $this->data['alert'] = [
                       'message' => 'Please try again',
                       'class' => "alert-danger",
                       'status_id' => "1",
                       'type' => "Error"
                       ];
                       redirect('admin/partners');
                    }
                }else {
                    $this->data['alert'] = [
                    'message' => 'Please try again',
                    'class' => "alert-danger",
                    'status_id' => "1",
                    'type' => "Error"
                     ];
                     redirect('admin/partners');
                }
       
        }else{
           redirect('admin/partners');
          }
     }else{
           redirect('admin/partners');
          }
        
    }
    public function profileDelete(){
        
        $id=$_POST['delete_profile_id'];
        $partner=$this->partners_model->getSingleRecord('vbs_partners',['id'=>$id]);
        $del=$this->partners_model->deletePartnerProfile($partner);
        if ($del==true) {
        $this->session->set_flashdata('alert', [
                'message' => "Successfully deleted.",
                'class' => "alert-success",
                'status_id' => "1",
                'type' => "Success"
        ]);
        redirect('admin/partners');
        }
        else {
            $this->session->set_flashdata('alert', [
                    'message' => "Please try again.",
                    'class' => "alert-danger",
                    'status_id' => "1",
                    'type' => "Error"
            ]);
            redirect('admin/partners');
        }
    }
    public function get_ajax_profile_data(){
        $id=(int)$_GET['profile_id'];
       

        if($id>0){

        $data = [];
        $this->data['partner_status_data'] = $this->partners_model->getAllData('vbs_partnerstatus');
        $this->data['civilite_data'] = $this->partners_model->getAllData('vbs_partnercivilite');
        $this->data['type_data'] = $this->partners_model->getAllData('vbs_partnertype');
        $this->data['users_data'] = $this->userx_model->getAll(['grp.name'=>'admin']);
        $this->data['partner'] = $this->partners_model->getSingleRecord('vbs_partners',['id'=>$id]);
       
        $this->data['countries'] = $this->cms_model->get_all_countries();
        $this->data['regions'] = $this->partners_model->ajax_get_region_listing($this->data['partner']->country);
        $this->data['cities'] = $this->partners_model->ajax_get_cities_listings($this->data['partner']->region);
        $this->data['partnercustomreminders']=$this->partners_model->getMultipleRecord('vbs_partners_customreminders',['partner_id'=> $this->data['partner']->id]);
        $this->data['partnernotifications']=$this->partners_model->getMultipleRecord("vbs_partners_notification",['partner_id'=>$this->data['partner']->id]);
        $this->data['partnerreminders']=$this->partners_model->getMultipleRecord("vbs_partners_reminders",['partner_id'=>$this->data['partner']->id]);
        $this->data['partnernotes']=$this->partners_model->getMultipleRecord("vbs_partners_notes",['partner_id'=>$this->data['partner']->id]);

        //tabs record
         $this->data['document_data']=$this->partners_model->getAllData('vbs_partners_documents',['partner_id'=> $this->data['partner']->id]);
        //tabs record
        
        $result=$this->load->view('admin/partners/index', $this->data, TRUE);
        echo $result;
        exit();
        }else{
            $result="Please select Record!";
        }
        echo $result;
        exit;
    }
/* --------------- Partner Reminders Add Section start ---------------------------- */
function reminder_store($partner_id,$data){


  $currenttime = date('h : i');
  $module='9';
  $statusexpired='1';
  $remindersdata   = $this->partners_model->getMultipleRecord('vbs_reminders',['module' => $module,'status'=> $statusexpired]);
                       
  if($remindersdata){
    $remindertype=1;

    foreach ($remindersdata as $reminder) {
          $reminders = json_decode($reminder->reminders);
          $days         = $reminders[0]->reminder_days;

      //day add into date
      $new_datedexpiration  = date('Y-m-d', strtotime($data['datedexpiration']. ' - '.$days.' days'));
      $new_datedexpiration2 = date('Y-m-d', strtotime($data['datedexpiration2']. ' - '.$days.' days'));
      $new_datedexpiration3 = date('Y-m-d', strtotime($data['datedexpiration3']. ' - '.$days.' days'));
      $new_datedexpiration4 = date('Y-m-d', strtotime($data['datedexpiration4']. ' - '.$days.' days'));
      $new_datedexpiration5 = date('Y-m-d', strtotime($data['datedexpiration5']. ' - '.$days.' days'));
      //day add into date

           $reminderid = $this->partners_model->createRecord('vbs_partners_reminders',[
                           'typeiddate'                  => $new_datedexpiration,
                           'typeidtime'                  => $currenttime,
                           'permitnumberdate'            => $new_datedexpiration2,
                           'permitnumbertime'            => $currenttime,
                           'medicalcertificatedate'      => $new_datedexpiration3,
                           'medicalcertificatetime'      => $currenttime,
                           'psc1date'                    => $new_datedexpiration4,
                           'psc1time'                    => $currenttime,
                           'medicineoftravaidate'        => $new_datedexpiration5,
                           'medicineoftravaitime'        => $currenttime,
                           'partner_id'                   => $partner_id,
                           'reminder_id'                 => $reminder->id,
                           'type'                        => $remindertype
                          ]); 
           $remindertype++;
    }
    return true;
  }else{
    return false;
  }
}

/* --------------------------------- Partner Reminders Add Section close -------------------------------------------- */

/* --------------------------------- Partner notification Add Section start -------------------------------------------- */
function notification_store($partner_id,$data){
 

  $currenttime = date('h : i');
  $department='10';
  $statusexpired='1';
  $notificationdata  = $this->partners_model->getSingleRecord('vbs_notifications',['department' => $department,'status'=>$statusexpired]);
                       
  if($notificationdata){
 
         $notificationid = $this->partners_model->createRecord('vbs_partners_notification',[
                         'typeiddate'                  => $data['datedexpiration'],
                         'typeidtime'                  => $currenttime,
                         'permitnumberdate'            => $data['datedexpiration2'],
                         'permitnumbertime'            => $currenttime,
                         'medicalcertificatedate'      => $data['datedexpiration3'],
                         'medicalcertificatetime'      => $currenttime,
                         'psc1date'                    => $data['datedexpiration4'],
                         'psc1time'                    => $currenttime,
                         'medicineoftravaidate'        => $data['datedexpiration5'],
                         'medicineoftravaitime'        => $currenttime,
                         'partner_id'                   => $partner_id,
                         'notification_id'             => $notificationdata->id,
                        ]); 
         
    return true;     
  }else{
    return false;
  }
} 
/* --------------------------------- Partner notification Add Section close -------------------------------------------- */
/* --------------------------------- Partner Reminders Update Section start -------------------------------------------- */
function reminder_update($partner_id,$data){
  
  $partner_reminder_prev_data=$this->partners_model->getOneRecord('vbs_partners_reminders',['partner_id' => $partner_id]);
  $reminderdata   = $this->partners_model->getOneRecord('vbs_reminders',['id' => $partner_reminder_prev_data->reminder_id]);

  if($reminderdata){
    
          $reminders = json_decode($reminderdata->reminders);
          $days      = $reminders[0]->reminder_days;

      //day add into date
      $new_datedexpiration  = date('Y-m-d', strtotime($data['datedexpiration']. ' - '.$days.' days'));
      $new_datedexpiration2 = date('Y-m-d', strtotime($data['datedexpiration2']. ' - '.$days.' days'));
      $new_datedexpiration3 = date('Y-m-d', strtotime($data['datedexpiration3']. ' - '.$days.' days'));
      $new_datedexpiration4 = date('Y-m-d', strtotime($data['datedexpiration4']. ' - '.$days.' days'));
      $new_datedexpiration5 = date('Y-m-d', strtotime($data['datedexpiration5']. ' - '.$days.' days'));
      //day add into date

           $reminderid = $this->partners_model->createUpdate('vbs_partners_reminders',[
                           'typeiddate'                  => $new_datedexpiration,
                           'permitnumberdate'            => $new_datedexpiration2,
                           'medicalcertificatedate'      => $new_datedexpiration3,
                           'psc1date'                    => $new_datedexpiration4,
                           'medicineoftravaidate'        => $new_datedexpiration5
                          ],$partner_reminder_prev_data->id); 
          if($reminderid){return true;}
          else{return false;} 
  }else{
    return false;
  }
}

/* --------------------------------- Partner Reminders Update Section close -------------------------------------------- */

/* --------------------------------- Partner notification Update Section start -------------------------------------------- */
function notification_update($partner_id,$data){
 $partner_notification_prev_data=$this->partners_model->getOneRecord('vbs_partners_notification',['partner_id' => $partner_id]);
              
  if($partner_notification_prev_data){
         $notificationid = $this->partners_model->createUpdate('vbs_partners_notification',[
                         'typeiddate'                  => $data['datedexpiration'],
                         'permitnumberdate'            => $data['datedexpiration2'],
                         'medicalcertificatedate'      => $data['datedexpiration3'],
                         'psc1date'                    => $data['datedexpiration4'],
                         'medicineoftravaidate'        => $data['datedexpiration5']
                        ],$partner_notification_prev_data->id);     
     if($notificationid){return true;}
     else{return false;} 
  }else{
    return false;
  }
} 
/* --------------------------------- Partner notification Update Section close -------------------------------------------- */
  /* --------------------------------- Partner notification pdf  Section start -------------------------------------------- */

    public function notification_pdf($partnernotificationid,$partner_id,$columnnumber){

     $pdfArrays=createpartnernotificationpdf($partnernotificationid,$partner_id,$columnnumber);  
      $this->data['pdf']=$pdfArrays['pdf']; 
      $this->data['fileName']=$pdfArrays['fileName'];  

      $this->load->view('admin/partners/profile/notificationpdf',$this->data);
    }
   /* --------------------------------- Partner notification pdf  Section close -------------------------------------------- */
 /* --------------------------------- Partner reminder pdf  Section start -------------------------------------------- */

     public function reminder_pdf($partnerreminderid,$partner_id,$columnnumber,$leve){

     $pdfArrays=createpartnerreminderpdf($partnerreminderid,$partner_id,$columnnumber);  
      $this->data['pdf']=$pdfArrays['pdf']; 
      $this->data['fileName']=$pdfArrays['fileName'];  

      $this->load->view('admin/partners/profile/reminderpdf',$this->data);
    }
     /* --------------------------------- Partner reminder pdf  Section close -------------------------------------------- */

   /* --------------------------------- Partner custom reminder  Section start -------------------------------------------- */

  public function addcustomreminders(){
  
  $partner_id=$_GET['partner_id'];
  $title=$_GET['title'];
  $date=$_GET['date'];
  $time=$_GET['time'];
  $comment=$_GET['comment'];
  $subject=$_GET['subject'];

  //add custom comment
          
                     $date=$date;
                     $date=str_replace('/', '-', $date);
                     $date=strtotime($date);
                     $date = date('Y-m-d',$date);
                      $crid = $this->partners_model->createRecord("vbs_partners_customreminders",[
                        'date'     => $date,
                        'time'     => $time,
                        'title'    => $title,
                        'comment'  => $comment,
                        'subject'  => $subject,
                        'partner_id' => $partner_id
                        ]);
                  
             if($crid){
                    $record=array(
                        'date'     => $_GET['date'],
                        'time'     => $time,
                        'title'    => $title,
                        'comment'  => $comment,
                        'subject'  => $subject,
                        'partner_id' => $partner_id,
                        'customreminderid'=>$crid
                    );
                    $data = ['result'=>"200",'record'=>$record];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
             }
             else{
                    $data = ['result'=>"201",'error'=>'something went wrong.'];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
             }
}


public function removecustomreminders(){
  $customreminderid=$_GET['customreminderid'];
  $del=$this->partners_model->createDelete('vbs_partners_customreminders',['id'=>$customreminderid]);
  if($del){
                    $data = ['result'=>"200"];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
  }else{
                    $data = ['result'=>"201",'error'=>'something went wrong.'];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
  }
}
/* --------------------------------- Partner custom reminder  Section close -------------------------------------------- */
 /* ------- Partner note  Section start ----------- */

  public function addpartnernote(){
  
  $partner_id=$_GET['partner_id'];
  $note=$_GET['note'];

  //add partner note
          
                     
                  $noteid = $this->partners_model->createRecord("vbs_partners_notes",[                    
                        'note'       => $note,
                        'partner_id' => $partner_id
                    ]);
                  
             if($noteid){
                    $record=array(
                        'note'       => $note,
                        'partner_id' => $partner_id,
                        'noteid'     => $noteid
                    );
                    $data = ['result'=>"200",'record'=>$record];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
             }
             else{
                    $data = ['result'=>"201",'error'=>'something went wrong.'];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
             }
}


public function deletepartnernote(){
  $noteid=$_GET['noteid'];
  $del=$this->partners_model->createDelete('vbs_partners_notes',['id'=>$noteid]);
  if($del){
                    $data = ['result'=>"200"];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
  }else{
                    $data = ['result'=>"201",'error'=>'something went wrong.'];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
  }
}
 /* ------- Partner note  Section close ----------- */
//search
public function get_partners_bysearch(){
  
  $from_period=$this->input->post('from_period');
  $to_period=$this->input->post('to_period');
  $statut=$this->input->post('statut');
  $category=$this->input->post('category');
  $nombredheuremensuel=$this->input->post('nombredheuremensuel');
//$from_period='26/12/2020';
//$to_period='28/12/2020';

  $search=array();
    
     if(!empty($from_period)){

       $from_period=str_replace('/', '-', $from_period);
       $from_period=strtotime($from_period);
       $from_period = date('Y-m-d',$from_period);

       $search["DATE_FORMAT(created_at,'%Y-%m-%d') >="]=$from_period;
       $from_period = strtotime($from_period);
       $from_period = date('d/m/Y',$from_period);

     }
     if(!empty($to_period)){
         $to_period=str_replace('/', '-', $to_period);
         $to_period=strtotime($to_period);
         $to_period = date('Y-m-d',$to_period);

         $search["DATE_FORMAT(created_at,'%Y-%m-%d') <="]=$to_period;
         $to_period = strtotime($to_period);
         $to_period = date('d/m/Y',$to_period);
     }
     
     if($statut != ''){
      $search['statut']=$statut;
     }
     if($category != ''){
      $search['category']=$category;
     }
     if($nombredheuremensuel != ''){
      $search['nombredheuremensuel']=$nombredheuremensuel;
     }
    
  
     $data=$this->partners_model->getAllData('vbs_partners',$search);
     $this->session->set_flashdata('searchdata', [
              'record' =>  $data,  
              'fields' =>  array("search_from_period"=>$from_period,"search_to_period"=>$to_period,"search_status"=>$statut,'search_category'=>$category,'search_hours'=>$nombredheuremensuel)
                                                              
            ]);
  
          return redirect("admin/partners");
        
}
//search
//send access detail
public function sendaccessdetail(){
  $email    = $this->input->post('email');
  $password = $this->input->post('password');
  $id       = $this->input->post('id');

  if(!empty($email) && !empty($password) && !empty($id)){
     $partner=$this->partners_model->getSingleRecord('vbs_partners',['id'=>$id]);
    //sent email
    //Email Sent
     
            $content="Hi ".$this->partners_model->getSingleRecord('vbs_partnercivilite',['id'=>$partner->civilite])->civilite." ".$partner->prenom." ".$partner->nom.",<br>";
            $content.="You 've received access of your account from ECAB LLC."."<br>";
            $content.="You account email address is ".$email." and your password is ".$password;
                     $this->load->library('mail_bookingsender');
                    $mailData['email']   = $email;
                    $mailData['subject'] = "You've received messages from ECAB LLC";
                    $mailData['message']    = $content;
                    $sent = $this->mail_bookingsender->sendMail($mailData);
                    //return $sent;
    //sent email
              if($sent){
                    $data = ['result'=>"200"];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
              }else{
                   $data = ['result'=>"201"];
                   header('Content-Type: application/json'); 
                   echo json_encode($data);
              }
           
        }else{
            $data = ['result'=>"201"];
            header('Content-Type: application/json'); 
            echo json_encode($data);
        }
}
//send access detail
/* add partner notification */
public function addpartnernotification(){

  $column1=$_POST['column1'];
  $column2=$_POST['column2'];
  $date=$_POST['date'];
  $time=$_POST['time'];
  $partner_id=$_POST['partner_id'];
  $department='10';
  $statusexpired='1';
  $notificationdata  = $this->partners_model->getSingleRecord('vbs_notifications',['department' => $department,'status'=>$statusexpired]);

  //convert into date format
  $date=str_replace('/', '-', $date);
  $date=strtotime($date);
  $date = date('Y-m-d',$date);
  //convert into date format
  $notificationid = $this->partners_model->createRecord('vbs_partners_notification',[
                           $column1                     => $date,
                           $column2                     => $time,
                          'partner_id'                  => $partner_id,
                          'notification_id'             => $notificationdata->id,
                        ]); 
      $record=array('notification_id'=>$notificationdata->id,'status'=>'Pending');
      $data = ['result'=>"200",'record'=>$record];
      header('Content-Type: application/json'); 
      echo json_encode($data);

}
/* add partner notification */
/* add partner reminder */
public function addpartnerreminder(){

  $column1=$_POST['column1'];
  $column2=$_POST['column2'];
  $date=$_POST['date'];
  $time=$_POST['time'];
  $partner_id=$_POST['partner_id'];
  $module='9';
  $statusexpired='1';
  $remindersdata = $this->partners_model->getSingleRecord('vbs_reminders',['module' => $module,'status'=> $statusexpired]);

  //convert into date format
    $date=str_replace('/', '-', $date);
    $date=strtotime($date);
    $date = date('Y-m-d',$date);
  //convert into date format
  
  $reminderid = $this->partners_model->createRecord('vbs_partners_reminders',[
                           $column1                     => $date,
                           $column2                     => $time,
                          'partner_id'                   => $partner_id,
                          'reminder_id'                 => $remindersdata->id,
                        ]); 
      $record=array('reminder_id'=>$reminderid,'status'=>'Pending');
      $data = ['result'=>"200",'record'=>$record];
      header('Content-Type: application/json'); 
      echo json_encode($data);

}
/* add partner reminder */
 /*document*/
    public function documentAdd(){

      if ($_SERVER['REQUEST_METHOD'] === 'POST') {
          $this->documentStore();
      }else{
          return redirect("admin/partners");
      }
  }
  public function documentStore(){
          
       //date converation
            
       $datedexpiration=str_replace('/', '-', $_POST['datedexpiration']);
       $datedexpiration=strtotime($datedexpiration);
       $datedexpiration = date('Y-m-d',$datedexpiration);


       $datedexpiration2=str_replace('/', '-', $_POST['datedexpiration2']);
       $datedexpiration2=strtotime($datedexpiration2);
       $datedexpiration2 = date('Y-m-d',$datedexpiration2);


       $datedexpiration3=str_replace('/', '-', $_POST['datedexpiration3']);
       $datedexpiration3=strtotime($datedexpiration3);
       $datedexpiration3 = date('Y-m-d',$datedexpiration3);


       $datedexpiration4=str_replace('/', '-', $_POST['datedexpiration4']);
       $datedexpiration4=strtotime($datedexpiration4);
       $datedexpiration4 = date('Y-m-d',$datedexpiration4);

       $datedexpiration5=str_replace('/', '-', $_POST['datedexpiration5']);
       $datedexpiration5=strtotime($datedexpiration5);
       $datedexpiration5 = date('Y-m-d',$datedexpiration5);


       $datedelivrance1=str_replace('/', '-', $_POST['datedelivrance1']);
       $datedelivrance1=strtotime($datedelivrance1);
       $datedelivrance1 = date('Y-m-d',$datedelivrance1);

       $datedelivrance2=str_replace('/', '-', $_POST['datedelivrance2']);
       $datedelivrance2=strtotime($datedelivrance2);
       $datedelivrance2 = date('Y-m-d',$datedelivrance2);


       $datedelivrance3=str_replace('/', '-', $_POST['datedelivrance3']);
       $datedelivrance3=strtotime($datedelivrance3);
       $datedelivrance3 = date('Y-m-d',$datedelivrance3);

       $datedelivrance4=str_replace('/', '-', $_POST['datedelivrance4']);
       $datedelivrance4=strtotime($datedelivrance4);
       $datedelivrance4 = date('Y-m-d',$datedelivrance4);

       //date converation

 $id = $this->partners_model->createRecord('vbs_partners_documents',[
       'statut'                      => $_POST['statut'],
       'typedepiecedidentite'        =>  $this->input->post('typedepiecedidentite'),
       'numerropiecedidentite'       =>  trim($this->input->post('numerropiecedidentite')),
       'datedexpiration'             =>  $datedexpiration,
       'pumeropermis'                =>  trim($this->input->post('pumeropermis')),
       'upload1'                     =>  $this->do_upload_image($_FILES['upload1'],'upload1'),
       'upload2'                     =>  $this->do_upload_image($_FILES['upload2'],'upload2'),
       'datedelivrance1'             =>  $datedelivrance1,
       'datedexpiration2'            =>  $datedexpiration2,
       'certificatmedicate'          =>  $this->do_upload_image($_FILES['certificatmedicate'],'certificatmedicate'),
       'datedelivrance2'             =>  $datedelivrance2,
       'datedexpiration3'            =>  $datedexpiration3,
       'psc1'                        =>  $this->do_upload_image($_FILES['psc1'],'psc1'),
       'datedexpiration4'            =>  $datedexpiration4,
       'datedelivrance3'             =>  $datedelivrance3,
       'medecinedetravai'            =>  $this->do_upload_image($_FILES['medecinedetravai'],'medecinedetravai'),
       'datedelivrance4'             =>  $datedelivrance4,
       'datedexpiration5'            =>  $datedexpiration5,
       'autrdeiplome1'               =>  $this->do_upload_image($_FILES['autrdeiplome1'],'autrdeiplome1'),
       'autrdeiplome2'               =>  $this->do_upload_image($_FILES['autrdeiplome2'],'autrdeiplome2'),
       'autrdeiplome3'               =>  $this->do_upload_image($_FILES['autrdeiplome3'],'autrdeiplome3'),
       'partner_id'                 => $_POST['profile_id']
 ]);
          if($id){          
            $exp_array=array(
              'datedexpiration' =>   $datedexpiration,
              'datedexpiration2' =>  $datedexpiration2,
              'datedexpiration3' =>  $datedexpiration3,
              'datedexpiration4' =>  $datedexpiration4,
              'datedexpiration5' =>  $datedexpiration5
             );

             $isremindercreate=$this->reminder_update($_POST['profile_id'],$exp_array);
             $isnotificationcreate=$this->notification_update($_POST['profile_id'],$exp_array);
             if($isremindercreate == true && $isnotificationcreate == true){
              $this->data['document_data']=$this->partners_model->getAllData('vbs_partners_documents',['partner_id'=> $_POST['profile_id']]);
              $record=$this->load->view('admin/partners/document/list', $this->data, TRUE);
              $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully added a document.",'messageType'=>'Success','className'=>'alert-success'];
              header('Content-Type: application/json'); 
              echo json_encode($data);
             }
             else{
                $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                header('Content-Type: application/json'); 
                echo json_encode($data);
             }

             
            }
           else {
              $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
              header('Content-Type: application/json'); 
              echo json_encode($data);
          }
         
  }


    public function documentEdit(){
      

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $id   = $this->input->post('document_id');
                     //date converation
                     
                      $datedexpiration=str_replace('/', '-', $_POST['datedexpiration']);
                      $datedexpiration=strtotime($datedexpiration);
                      $datedexpiration = date('Y-m-d',$datedexpiration);


                      $datedexpiration2=str_replace('/', '-', $_POST['datedexpiration2']);
                      $datedexpiration2=strtotime($datedexpiration2);
                      $datedexpiration2 = date('Y-m-d',$datedexpiration2);


                      $datedexpiration3=str_replace('/', '-', $_POST['datedexpiration3']);
                      $datedexpiration3=strtotime($datedexpiration3);
                      $datedexpiration3 = date('Y-m-d',$datedexpiration3);


                      $datedexpiration4=str_replace('/', '-', $_POST['datedexpiration4']);
                      $datedexpiration4=strtotime($datedexpiration4);
                      $datedexpiration4 = date('Y-m-d',$datedexpiration4);

                      $datedexpiration5=str_replace('/', '-', $_POST['datedexpiration5']);
                      $datedexpiration5=strtotime($datedexpiration5);
                      $datedexpiration5 = date('Y-m-d',$datedexpiration5);


                      $datedelivrance1=str_replace('/', '-', $_POST['datedelivrance1']);
                      $datedelivrance1=strtotime($datedelivrance1);
                      $datedelivrance1 = date('Y-m-d',$datedelivrance1);

                      $datedelivrance2=str_replace('/', '-', $_POST['datedelivrance2']);
                      $datedelivrance2=strtotime($datedelivrance2);
                      $datedelivrance2 = date('Y-m-d',$datedelivrance2);


                      $datedelivrance3=str_replace('/', '-', $_POST['datedelivrance3']);
                      $datedelivrance3=strtotime($datedelivrance3);
                      $datedelivrance3 = date('Y-m-d',$datedelivrance3);

                      $datedelivrance4=str_replace('/', '-', $_POST['datedelivrance4']);
                      $datedelivrance4=strtotime($datedelivrance4);
                      $datedelivrance4 = date('Y-m-d',$datedelivrance4);
                      
                        //date converation

                      $document  = $this->partners_model->getSingleRecord('vbs_partners_documents',['id'=>$id]);
                      
                       $upload1='';
                       $upload2=''; 
                       $certificatmedicate='';
                       $psc1='';
                       $medecinedetravai='';
                       $autrdeiplome1='';
                       $autrdeiplome2='';
                       $autrdeiplome3='';
                      
                       
                       if(!empty($_FILES['upload1']['name']))
                       {
                         $upload1=$this->do_upload_image($_FILES['upload1'],'upload1');
                       }else{
                           $upload1=$document->upload1;
                       }
                       if(!empty($_FILES['upload2']['name']))
                       {
                         $upload2=$this->do_upload_image($_FILES['upload2'],'upload2');
                       }else{
                           $upload2=$document->upload2;
                       }
                       if(!empty($_FILES['certificatmedicate']['name']))
                       {
                         $certificatmedicate=$this->do_upload_image($_FILES['certificatmedicate'],'certificatmedicate');
                       }else{
                           $certificatmedicate=$document->certificatmedicate;
                       }
                       if(!empty($_FILES['psc1']['name']))
                       {
                         $psc1=$this->do_upload_image($_FILES['psc1'],'psc1');
                       }else{
                           $psc1=$document->psc1;
                       }
                       if(!empty($_FILES['medecinedetravai']['name']))
                       {
                         $medecinedetravai=$this->do_upload_image($_FILES['medecinedetravai'],'medecinedetravai');
                       }else{
                           $medecinedetravai=$document->medecinedetravai;
                       }
                       if(!empty($_FILES['autrdeiplome1']['name']))
                       {
                         $autrdeiplome1=$this->do_upload_image($_FILES['autrdeiplome1'],'autrdeiplome1');
                       }else{
                           $autrdeiplome1=$document->autrdeiplome1;
                       }
                       if(!empty($_FILES['autrdeiplome2']['name']))
                       {
                         $autrdeiplome2=$this->do_upload_image($_FILES['autrdeiplome2'],'autrdeiplome2');
                       }else{
                           $autrdeiplome2=$document->autrdeiplome2;
                       }
                       if(!empty($_FILES['autrdeiplome3']['name']))
                       {
                         $autrdeiplome3=$this->do_upload_image($_FILES['autrdeiplome3'],'autrdeiplome3');
                       }else{
                           $autrdeiplome3=$document->autrdeiplome3;
                       }

            if(!empty($id)) {
                            
                $update=$this->partners_model->createUpdate('vbs_partners_documents',[
                    'typedepiecedidentite' =>  $this->input->post('typedepiecedidentite'),
                    'numerropiecedidentite' =>  $this->input->post('numerropiecedidentite'),
                    'datedexpiration' =>  $datedexpiration,
                    'pumeropermis' =>  $this->input->post('pumeropermis'),
                    'upload1' => $upload1,
                    'upload2' => $upload2,
                    'datedelivrance1' =>  $datedelivrance1,
                    'datedexpiration2' =>  $datedexpiration2,
                    'certificatmedicate' => $certificatmedicate,
                    'datedelivrance2' => $datedelivrance2,
                    'datedexpiration3' =>  $datedexpiration3,
                    'psc1' => $psc1,
                    'datedexpiration4' =>  $datedexpiration4,
                    'datedelivrance3' =>  $datedelivrance3,
                    'medecinedetravai' => $medecinedetravai,
                    'datedelivrance4' =>  $datedelivrance4,
                    'datedexpiration5' =>  $datedexpiration5,
                    'autrdeiplome1' => $autrdeiplome1,
                    'autrdeiplome2' => $autrdeiplome2,
                    'autrdeiplome3' => $autrdeiplome3
                      ], $id);
                      if ($update){
                        $exp_array=array(
                          'datedexpiration' =>   $datedexpiration,
                          'datedexpiration2' =>  $datedexpiration2,
                          'datedexpiration3' =>  $datedexpiration3,
                          'datedexpiration4' =>  $datedexpiration4,
                          'datedexpiration5' =>  $datedexpiration5
                         );

                         $isremindercreate=$this->reminder_update($document->partner_id,$exp_array);
                         $isnotificationcreate=$this->notification_update($document->partner_id,$exp_array);
                         if($isremindercreate == true && $isnotificationcreate == true){
                               //partner document
                                                 $this->data['document_data']=$this->partners_model->getAllData('vbs_partners_documents',['partner_id'=> $document->partner_id]);

                                                 $record=$this->load->view('admin/partners/document/list', $this->data, TRUE);
                                                 $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully updated a document.",'messageType'=>'Success','className'=>'alert-success'];
                                                 header('Content-Type: application/json'); 
                                                 echo json_encode($data);
                         }else{
                              $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                              header('Content-Type: application/json'); 
                              echo json_encode($data);
                              }
                  
                      }else {
                        $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                        header('Content-Type: application/json'); 
                        echo json_encode($data);
                    }
             
              }else {
                    $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
                    }
                }else{
                    $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
                }
    }
    public function documentDelete(){
        
        $id=$_POST['delete_document_id'];
        $document = $this->partners_model->getSingleRecord('vbs_partners_documents',['id'=>$id]);  
        $del=$this->partners_model->createDelete('vbs_partners_documents',['id'=>$id]);
        if ($del==true) {
        $this->data['document_data']=$this->partners_model->getAllData('vbs_partners_documents',['partner_id'=> $document->partner_id]);
        $record=$this->load->view('admin/partners/document/list', $this->data, TRUE);
        $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully deleted a document.",'messageType'=>'Success','className'=>'alert-success'];
        header('Content-Type: application/json'); 
        echo json_encode($data);
        }
        else {
         $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
         header('Content-Type: application/json'); 
         echo json_encode($data);
        }
    }
    public function documentAjax(){
        $id=(int)$_GET['document_id'];

        if($id>0){
                $data = [];
                $this->data['document'] = $this->partners_model->getSingleRecord('vbs_partners_documents',['id'=>$id]);
                $this->data['type_data'] = $this->partners_model->getAllData('vbs_partnertype');
                $result=$this->load->view('admin/partners/document/edit', $this->data, TRUE);
                echo $result;
                exit();
        }else{
            $result="Please select Record!";
        }
        echo $result;
        exit;
    }
     /*document*/


    function ajax_get_cities_listing(){
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            
            $result = $this->partners_model->ajax_get_cities_listings($_POST['region_id']);
            echo json_encode($result);
        }
    }
    function ajax_get_region_listing(){
       if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            
            $result = $this->partners_model->ajax_get_region_listing($_POST['country_id']);
            echo json_encode($result);
        } 
    }

    function do_upload_image($filename,$name) {
        
        $config['upload_path'] = './uploads/partners/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['max_size'] = '10240';
        $config['file_name'] = str_replace(' ', '_', $filename['name']);
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        
        if (!$this->upload->do_upload($name)) {
            return NULL;
        } else {
            $avatar = $this->upload->data();
            $avatar = $avatar['file_name'];
        
            return $avatar;
        }
        
    }
  


        function do_user_upload_image($filename,$name) {
        
        $config['upload_path'] = './uploads/user/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['max_size'] = '10240';
        $config['file_name'] = str_replace(' ', '_', $filename['name']);
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        
        if (!$this->upload->do_upload($name)) {
            return NULL;
        } else {
            $avatar = $this->upload->data();
            $avatar = $avatar['file_name'];
        
            return $avatar;
        }
        
    }
}
