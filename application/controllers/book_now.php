<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Book_now extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->helper('validate');
        $this->load->helper('language');
        $this->load->library('session');
        $this->data['configuration'] = get_configuration();
        $this->load->model("bookings_model");
        $this->load->model('passenger_model');
    }

    private function addMetaData($param = 1, $data = [])
    {
        $data['meta_keywords'] = "tpmr;transport pmr;pmr transport;transport handicapé;transport handicapés;handicapé transport;transport handicape;transport handicap;transport mobilité réduite;transport adapté;transports handicapés;transport des handicapés;transport enfant handicapé;transports handicapés;transport personne agée;taxi handicapé;handicapé taxi;taxi pmr;taxi avec rampe;transport handicapes;transport de personnes handicapées;transport de personne handicapé;transport personnes âgées;transport personnes handicapées;transport de personnes à mobilité réduite;transport personne mobilité réduite;transport de personnes agées;transport personnes agée;transport personnes handicapées;transport personne handicapée;transport personne agée​";
        $data['meta_description'] = "HANDI EXPRESS Le spécialiste du transport de personnes à mobilité réduite,Transport de personnes handicapées et Transport de personne âgées.Tout Trajet - Toute Distance 24h/24 - 7j/7.";
        switch ($param) {
            case 1:
                $data['title'] = 'Booking Details - HANDI EXPRESS Transport de personnes à mobilité réduite';
                break;
            case 2:
                $data['title'] = 'Register Details - HANDI EXPRESS Transport de personnes à mobilité réduite';
                break;
            case 3:
                $data['title'] = 'Booking Quote - HANDI EXPRESS Transport de personnes à mobilité réduite';
                break;
            case 3:
                $data['title'] = 'Payment Details - HANDI EXPRESS Transport de personnes à mobilité réduite';
                break;
        }
        return $data;
    }

    public function index() {
        $param==1;
        if ($this->lang->lang() == 'fr') {
                $name="Book Now";
        }else if ($this->lang->lang() == 'en') {

        }
        $this->data['service_info'] = $name;
        $this->data['heading']        = $this->lang->line('services');
        $this->data['active_class']     = "book now";
        $this->data['sub_heading']      = $name;
        $this->data['bread_crumb']      = true;
        $this->data['title']        = $name;

        $this->data               = $this->addMetaData($param, $this->data);
        $this->data['content']        = 'site/book_now/book_details';
        $this->_render_page('templates/site_template', $this->data);
    }
       //created by sultan book now
     public function booking(){

      
        $this->data['page_title'] = "Booking";
        $this->data['active_class'] = "booking";
        $this->data['content'] = 'site/booking/booking';
        $this->data['poi_data'] = $this->bookings_model->booking_Config_getAll('vbs_u_poi');
        $this->data['poi_package'] = $this->bookings_model->booking_Config_getAll('vbs_u_package');
        $this->data['service_cat'] = $this->bookings_model->booking_Config_getAll('vbs_u_category_service');
        $this->data['service'] = $this->bookings_model->booking_Config_getAll('vbs_u_service');
        $this->data['car_data'] = $this->bookings_model->getcarsdata();
        $this->data['wheelcar_data'] = $this->bookings_model->getwheelcarsdata();
        $this->data['googleapikey']=$this->bookings_model->getgoogleapikey("vbs_googleapi");

      //  print_r($this->data['car_category']);
       // exit();
        $this->_render_page('templates/site_template', $this->data);
    }
   public function booking_quote() {
 
      if (!(bool) $this->basic_auth->is_login() || !$this->basic_auth->is_client()) {

              $this->session->set_userdata("current_booking_url",'booking_quote');
              redirect('client/identification');
        }


            $booking=$this->session->userdata['bookingdata'];
      
            $client=$this->session->userdata['user'];
            $this->data["car_name"]=$this->bookings_model->getcarname($booking["carfield"]);
            $this->data['pricestatus_data']=$this->bookings_model->getpricestatus();
            $this->data['page_name'] = 'booking_quote';
            $this->data['page_title'] = "Quote";
            $this->data['content'] = 'site/booking/booking_quote';
            $map= $this->session->userdata['mapdatainfo'];
            $this->data['pickupaddress']=$map['pickupaddress'];
            $this->data['dropoffaddress']=$map['dropoffaddress'];
            $this->data['passengers']=$this->bookings_model->booking_passengers('vbs_passengers',$client->id);
            $this->data['googleapikey']=$this->bookings_model->getgoogleapikey("vbs_googleapi");
            $this->data['disablecategories']=$this->passenger_model->getdisablecategories('vbs_u_disablecategory');
            $this->_render_page('templates/site_template', $this->data);
    }
    public function savepassenger(){

      $id=$_GET['passenger_id'];
     // $id=$this->input->post("passengerfield");
      if(!empty($id) ||  $id!=""){
          $passenger=$this->bookings_model->passengerrecord('vbs_passengers',$id);
            $name=$passenger['civility'].' '.$passenger['fname'].' '.$passenger['lname'];
           $disable=$this->passenger_model->getdiscategory('vbs_u_disablecategory',$passenger['disablecatid']);
      if($this->session->userdata['passengers']){
           $getpassenger['passengers']=$this->session->userdata['passengers'];
          
           if(!array_key_exists($id,  $getpassenger['passengers'])){
          
  $getpassenger['passengers'][$id]=array('id'=>$id,'name'=>$name,'phone'=>$passenger['mobilephone'],'home'=>$passenger['homephone'],'disable'=> $disable,'disableicon'=>'fa fa-check');
                 $this->session->set_userdata($getpassenger);
           }
      }
      else{
              
               $bookingstore['passengers']=array(
               $id=>array('id'=>$id,'name'=>$name,'phone'=>$passenger['mobilephone'],'home'=>$passenger['homephone'],'disable'=> $disable,'disableicon'=>'fa fa-check'));
               $this->session->set_userdata($bookingstore);
      }

       $record=$this->session->userdata['passengers'];
        $data = ["passengers" => $record,'result'=>"200"];
        header('Content-Type: application/json'); 
        echo json_encode($data);
      }
      else{
         $data = ['result'=>"203"];
        header('Content-Type: application/json'); 
        echo json_encode($data);
      }
    


   
    }
     public function removepassenger(){

      $id=$_GET['passenger_id'];
     // $id=$this->input->post("passengerfield");
      if(!empty($id) ||  $id!=""){
          $passenger=$this->bookings_model->passengerrecord('vbs_passengers',$id);
    
           $getpassenger['passengers']=$this->session->userdata['passengers'];
              unset($getpassenger['passengers'][$id]); 
              if(count($getpassenger['passengers'])>0){
                      $this->session->set_userdata($getpassenger);
                        $record=$this->session->userdata['passengers'];
                        $data = ["passengers" => $record,'result'=>"200"];
                        header('Content-Type: application/json'); 
                        echo json_encode($data);
              }else{
                    $this->session->unset_userdata('passengers');
                    
                      $data = ['result'=>"201"];
                      header('Content-Type: application/json'); 
                      echo json_encode($data);
              }
          
   
     

     
      }
      else{
         $data = ['result'=>"203"];
        header('Content-Type: application/json'); 
        echo json_encode($data);
      }
    


   
    }


     public function booking_payment() {
      $passengers=$this->session->userdata['passengers'];
      if(count($passengers) < 1){
          $this->session->set_flashdata('passenger_error', [
          'error' =>'Required'       
           ]);
           redirect('booking_quote');
      }
     
      
             if (!(bool) $this->basic_auth->is_login() || !$this->basic_auth->is_client()) {
                    $this->session->set_userdata("current_booking_url",'booking_payment');
                    redirect('client/identification');
              }

            $booking=$this->session->userdata['bookingdata'];
            $client=$this->session->userdata['user'];
            $this->data['pricestatus_data']=$this->bookings_model->getpricestatus();
             $this->data['companydata']=$this->bookings_model->getcompanystatus();
            $this->data["car_name"]=$this->bookings_model->getcarname($booking["carfield"]);
            $this->data['page_name'] = 'booking_payment';
            $this->data['page_title'] = "Payment Details";
            $this->data['content'] = 'site/booking/booking_payment';
            $map= $this->session->userdata['mapdatainfo'];
            $this->data['pickupaddress']=$map['pickupaddress'];
            $this->data['dropoffaddress']=$map['dropoffaddress'];
            $this->data['passengers']=$this->bookings_model->booking_passengers('vbs_passengers',$client->id);
            $this->data["paymentmethods"]=$this->bookings_model->paymentmethods();
           
            $this->_render_page('templates/site_template', $this->data);
    }

    public function booking_save(){
       //declare variable 
       //pickup category variables
       $pickupcategory="";$pickaddressfield="";$pickotheraddressfield="";
       $pickzipcodefield="";$pickcityfield="";
       $pickpackagefield="";$pickpackagename="";
       $pickairportfield="";$pickairportname="";$pickairportflightnumberfield="";$pickairportarrivaltimefield="";
       $picktrainfield="";$picktrainname="";$picktrainnumberfield="";$picktrainarrivaltimefield="";
       $pickhotelfield="";$pickhotelname="";
       $pickparkfield="";$pickparkname="";
       $pickmondaytype=0;$pickthuesdaytype=0;$pickwednesdaytype=0;$pickthursdaytype=0;$pickfridaytype=0;$picksaturdaytype=0;
       $picksundaytype=0;
       $pickmonday="";$pickthuesday="";$pickwednesday="";$pickthursday="";$pickfriday="";$picksaturday="";$picksunday="";
       $pickdatefield="";$picktimefield="";$waitingtimefield="";$returndatefield="";$returntimefield="";
     
       //dropoff category variables
       $dropoffcategory="";$dropaddressfield="";$dropotheraddressfield="";$dropzipcodefield="";$dropcityfield="";
       $droppackagefield="";$droppackagename="";
       $dropairportfield="";$dropairportname="";$dropairportflightnumberfield="";$dropairportarrivaltimefield="";
       $droptrainfield="";$droptrainname="";$droptrainnumberfield="";$droptrainarrivaltimefield="";
       $drophotelfield="";$drophotelname="";
       $dropparkfield="";$dropparkname="";
       $returnmondaytype=0;$returnthuesdaytype=0;$returnwednesdaytype=0;$returnthursdaytype=0;$returnfridaytype=0;$returnsaturdaytype=0;
       $returnsundaytype=0;
       $returnmonday="";$returnthuesday="";$returnwednesday="";$returnthursday="";$returnfriday="";$returnsaturday="";$returnsunday="";
       $service="";$servicecategory="";
      

       //other vairable  
       $regularcheckfield=0;$returncheckfield=0;$wheelcarcheckfield=0;
       $starttimefield="";$startdatefield="";$endtimefield="";$enddatefield="";
       $carfield="";      
       $mapaddress="";$dropmapaddress="";
      //end declare variable
        $date=date("Y-m-d");
        //stopper section
        $bookingstore['stops']=array();
        if(isset($_POST['stopaddress'])){
          
               $stopaddressfield=$this->input->post('stopaddress');
               $stopwaittimefield=$this->input->post('stopwaittime');
               $stopcount=count($stopaddressfield);

               if($stopcount > 0){
                $stopno=0;
                 for($i=0;$i<count($stopaddressfield);$i++){
                  if(!empty($stopaddressfield[$i])){
                     $bookingstore['stops'][$stopno]= array(
                   'stopaddress'  => $stopaddressfield[$i],
                   'stopwaittime' => $stopwaittimefield[$i]
                  
                 );
                      $stopno++;
                  }
                
                }            
              }
           
        }

  //stopper section
             $this->load->library('form_validation');
                
                 $pick_status="1";
                 $drop_status="1";

                 $pickupcategory=$this->input->post("pickupcategory");
                 $dropoffcategory=$this->input->post("dropoffcategory");

                 if($pickupcategory=="address"){  $pick_status="1";}
                 elseif($pickupcategory=="package"){  $pick_status="2";}
                 elseif($pickupcategory=="airport"){  $pick_status="3";}
                 elseif($pickupcategory=="train"){  $pick_status="4";}
                 elseif($pickupcategory=="hotel"){  $pick_status="5";}
                 elseif($pickupcategory=="park"){  $pick_status="6";}

                 if($dropoffcategory=="address"){  $drop_status="1";}
                 elseif($dropoffcategory=="package"){  $drop_status="2";}
                 elseif($dropoffcategory=="airport"){  $drop_status="3";}
                 elseif($dropoffcategory=="train"){  $drop_status="4";}
                 elseif($dropoffcategory=="hotel"){  $drop_status="5";}
                 elseif($dropoffcategory=="park"){  $drop_status="6";}
     
                //set validation rules
                $this->form_validation->set_rules('pickservicecategory', 'Service Category', 'required');
                $this->form_validation->set_rules('pickservice', 'Service', 'required');
                $this->form_validation->set_rules('booking_comment', "Comment", 'trim|max_length[500]');

                //set validation rules for pick up fields
               if($pick_status=="1"){
                 $this->form_validation->set_rules('pickaddressfield', 'addresse', 'trim|required|max_length[200]');
                 $this->form_validation->set_rules('pickotheraddressfield', "Complément d'adresse", 'trim|max_length[200]');
                 $this->form_validation->set_rules('pickzipcodefield', 'zip code', 'trim|required|max_length[50]');
                 $this->form_validation->set_rules('pickcityfield', 'city', 'trim|required|max_length[50]');

               }
               elseif($pick_status=="2"){
                 $this->form_validation->set_rules('pickpackagefield', 'package', 'required');
               }
               elseif($pick_status=="3"){
                
                $this->form_validation->set_rules('pickairportfield', 'airport', 'required');
                $this->form_validation->set_rules('pickairportflightnumberfield', 'flight number', 'required|max_length[50]');
                $this->form_validation->set_rules('pickairportarrivaltimefield', 'arrival time', 'required');
               }
               elseif($pick_status=="4"){
                $this->form_validation->set_rules('picktrainfield', 'train', 'required');
                $this->form_validation->set_rules('picktrainnumberfield', 'train number', 'required|max_length[50]');
                $this->form_validation->set_rules('picktrainarrivaltimefield', 'arrival time', 'required');
               }
               elseif($pick_status=="5"){
                $this->form_validation->set_rules('pickhotelfield', 'hotel', 'required');
               }
               elseif($pick_status=="6"){
                $this->form_validation->set_rules('pickparkfield', 'park', 'required');
               }
                //end validation rules for pick up fields

               //set validation rules for drop off fields
              if($drop_status=="1"){
               $this->form_validation->set_rules('dropaddressfield', 'addresse', 'trim|required|max_length[200]');
               $this->form_validation->set_rules('dropotheraddressfield', "Complément d'adresse", 'trim|max_length[200]');
               $this->form_validation->set_rules('dropzipcodefield', 'zip code', 'trim|required|max_length[50]');
               $this->form_validation->set_rules('dropcityfield', 'city', 'trim|required|max_length[50]');
             }
             elseif($drop_status=="2"){
               $this->form_validation->set_rules('droppackagefield', 'package', 'required');
             }
             elseif($drop_status=="3"){
              
              $this->form_validation->set_rules('dropairportfield', 'airport', 'required');
              $this->form_validation->set_rules('dropairportflightnumberfield', 'flight number', 'required|max_length[50]');
              $this->form_validation->set_rules('dropairportarrivaltimefield', 'arrival time', 'required');
             }
             elseif($drop_status=="4"){
              $this->form_validation->set_rules('droptrainfield', 'train', 'required');
              $this->form_validation->set_rules('droptrainnumberfield', 'train number', 'required|max_length[50]');
              $this->form_validation->set_rules('droptrainarrivaltimefield', 'arrival time', 'required');
             }
             elseif($drop_status=="5"){
              $this->form_validation->set_rules('drophotelfield', 'hotel', 'required');
             }
             elseif($drop_status=="6"){
              $this->form_validation->set_rules('dropparkfield', 'park', 'required');
             }
              //end validation rules for drop off fields
             $this->form_validation->set_message('required', 'REQUIRED');
             $this->form_validation->set_message('max_length','CHARACTERS LENGTH EXCEEDED');
            //check validation is true or false
            if ($this->form_validation->run() == FALSE)
               {                  
                  $this->session->set_flashdata('alert', [
                  'message' => " :something is wrong. please try again",
                  'class' => "alert-danger",
                  'type' => "Error"
                  ]);
                  return redirect("booking");   
               }       
         // end check validation is true or false

           //get last booking id from booking table 
            $bookingid=$this->bookings_model->getlastbookingid();
            //if booking id is not exist then give id to 1
              if(!$bookingid){
                $bookingid=1;
               }

        //regular,return,wheelchair field is checked or not then set value 0 or 1
       if(!empty($this->input->post("regularcheckfield"))){
            $regularcheckfield=1;
          
         }
         if(!empty($this->input->post("returncheckfield"))){
             $returncheckfield=1;
            
         }
         if(!empty($this->input->post("wheelcarcheckfield"))){
             $wheelcarcheckfield=1;
            
         }
       // end regular , return and wheelchair fields



        $service=$this->input->post('pickservice');
        $servicecategory=$this->input->post('pickservicecategory');
        $booking_comment=$this->input->post('booking_comment');


        if(empty($this->input->post("regularcheckfield"))){     
            $picktimefield=$this->input->post("picktimefield");
            $pickdatefield=$this->input->post("pickdatefield");
         }
        if(!empty($this->input->post("returncheckfield")) && empty($this->input->post("regularcheckfield"))){
             $waitingtimefield=$this->input->post("waitingtimefield");
             $returntimefield=$this->input->post("returntimefield");
             $returndatefield=$this->input->post("returndatefield");
        }
        if(!empty($this->input->post("regularcheckfield")) ){
           $startdatefield=$this->input->post("startdatefield");
           $starttimefield=$this->input->post("starttimefield");
           $enddatefield=$this->input->post("enddatefield");
           $endtimefield=$this->input->post("endtimefield");
        }



        //get pickup fields by category
         if($pickupcategory=="address"){
                 
            $pickaddressfield=$this->input->post("pickaddressfield");
            $pickotheraddressfield=$this->input->post("pickotheraddressfield");
            $pickzipcodefield=$this->input->post("pickzipcodefield");
            $pickcityfield=$this->input->post("pickcityfield");
            $mapaddress= $pickaddressfield;
        }
        elseif($pickupcategory=="package"){
            
            $pickpackagefield=$this->input->post("pickpackagefield");
            $packagerecord=$this->bookings_model->poidatarecord('vbs_u_package',$pickpackagefield);
            $poirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$packagerecord['departure']);
            $pickpackagename=$packagerecord['name'];
            $pickzipcodefield=$poirecord['postal'];
            $pickcityfield=$poirecord['ville'];
            $mapaddress= $poirecord['address'].', '.$poirecord['ville'];
        }
        elseif($pickupcategory=="airport"){
           
            $pickairportfield=$this->input->post("pickairportfield");
            $pickairportflightnumberfield=$this->input->post("pickairportflightnumberfield");
            $pickairportarrivaltimefield=$this->input->post("pickairportarrivaltimefield");
            $airportrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$pickairportfield);
            $pickairportname=$airportrecord['name'];
            $pickzipcodefield=$airportrecord['postal'];
            $pickcityfield=$airportrecord['ville'];
             $mapaddress=  $airportrecord['address'].', '.$airportrecord['ville'];
        }
        elseif($pickupcategory=="train"){
          
            $picktrainfield=$this->input->post("picktrainfield");
            $picktrainnumberfield=$this->input->post("picktrainnumberfield");
            $picktrainarrivaltimefield=$this->input->post("picktrainarrivaltimefield");
            $trainrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$picktrainfield);
            $picktrainname=$trainrecord['name'];
            $pickzipcodefield=$trainrecord['postal'];
            $pickcityfield=$trainrecord['ville'];
            $mapaddress=  $trainrecord['address'].', '.$trainrecord['ville'];
        }
        elseif($pickupcategory=="hotel"){
            
            $pickhotelfield=$this->input->post("pickhotelfield");
            $hotelrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$pickhotelfield);
            $pickhotelname=$hotelrecord['name'];
            $pickzipcodefield=$hotelrecord['postal'];
            $pickcityfield=$hotelrecord['ville'];
            $mapaddress=  $hotelrecord['address'].', '.$hotelrecord['ville'];
        }
        elseif($pickupcategory=="park"){
            
            $pickparkfield=$this->input->post("pickparkfield");
            $parkrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$pickparkfield);
            $pickparkname=$parkrecord['name'];
            $pickzipcodefield=$parkrecord['postal'];
            $pickcityfield=$parkrecord['ville'];  
            $mapaddress=  $parkrecord['address'].', '.$parkrecord['ville'];       
        }

        //end pickup fields by category

        //get dropoff fields by category
        if($dropoffcategory=="address"){
          
            $dropaddressfield=$this->input->post("dropaddressfield");
            $dropotheraddressfield=$this->input->post("dropotheraddressfield");
            $dropzipcodefield=$this->input->post("dropzipcodefield");
            $dropcityfield=$this->input->post("dropcityfield");
            $dropmapaddress=  $dropaddressfield;
        }
        elseif($dropoffcategory=="package"){
           
            $droppackagefield=$this->input->post("droppackagefield");
            $packagerecord=$this->bookings_model->poidatarecord('vbs_u_package',$droppackagefield);
            $droppackagename=$packagerecord['name'];
            $poirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$packagerecord['destination']);
            $dropzipcodefield=$poirecord['postal'];
            $dropcityfield=$poirecord['ville'];   
            $dropmapaddress= $poirecord['address'].', '.$poirecord['ville'];       
        }
        elseif($dropoffcategory=="airport"){
            
            $dropairportfield=$this->input->post("dropairportfield");
            $dropairportflightnumberfield=$this->input->post("dropairportflightnumberfield");
            $dropairportarrivaltimefield=$this->input->post("dropairportarrivaltimefield");
            $airportrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$dropairportfield);
            $dropairportname=$airportrecord['name'];
            $dropzipcodefield=$airportrecord['postal'];
            $dropcityfield=$airportrecord['ville'];
            $dropmapaddress=  $airportrecord['address'].', '.$airportrecord['ville'];
        }
        elseif($dropoffcategory=="train"){
            
            $droptrainfield=$this->input->post("droptrainfield");
            $droptrainnumberfield=$this->input->post("droptrainnumberfield");
            $droptrainarrivaltimefield=$this->input->post("droptrainarrivaltimefield");
            $trainrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$droptrainfield);
            $droptrainname=$trainrecord['name'];
            $dropzipcodefield=$trainrecord['postal'];
            $dropcityfield=$trainrecord['ville'];
            $dropmapaddress=  $trainrecord['address'].', '.$trainrecord['ville'];
        }
        elseif($dropoffcategory=="hotel"){
            
            $drophotelfield=$this->input->post("drophotelfield");
            $hotelrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$drophotelfield);
            $drophotelname=$hotelrecord['name'];
            $dropzipcodefield=$hotelrecord['postal'];
            $dropcityfield=$hotelrecord['ville'];
            $dropmapaddress=  $hotelrecord['address'].', '.$hotelrecord['ville'];
        }
        elseif($dropoffcategory=="park"){
             
            $dropparkfield=$this->input->post("dropparkfield");
            $parkrecord=$this->bookings_model->poidatarecord('vbs_u_poi',$dropparkfield);
            $dropparkname=$parkrecord['name'];
            $dropzipcodefield=$parkrecord['postal'];
            $dropcityfield=$parkrecord['ville'];
            $dropmapaddress=  $parkrecord['address'].', '.$parkrecord['ville'];

        }
       //end dropoff fields by category


        //get cars by car type if car is wheelchair or not
        $cartype=$this->input->post('carstypes');
            if($cartype=="1"){
              $carfield=$this->input->post('wheelcarfield');
            }else{
               $carfield=$this->input->post('carfield');
            }

    //convert these values into array and after that we store in session
     $bookingstore["bookingdata"]=array("categorytype"=>$pickupcategory,'address'=>$pickaddressfield,'otheraddress'=>$pickotheraddressfield,
      'zipcode'=>$pickzipcodefield,'city'=>$pickcityfield,'package'=>$pickpackagefield,'pickpackagename'=>$pickpackagename,
      'airport'=>$pickairportfield,'pickairportname'=>$pickairportname,'flightnumber'=>$pickairportflightnumberfield,
      'flightarrivaltime'=>$pickairportarrivaltimefield,'train'=>$picktrainfield,
            'picktrainname'=>$picktrainname,'trainnumber'=>$picktrainnumberfield,'trainarrivalnumber'=>$picktrainarrivaltimefield,
            'hotel'=>$pickhotelfield,'pickhotelname'=>$pickhotelname,'pickparkname'=>$pickparkname,'park'=>$pickparkfield,
            'pickupdate'=>$pickdatefield,'pickuptime'=>$picktimefield,'waitingtime'=>$waitingtimefield,'returndate'=>$returndatefield,
            'returntime'=>$returntimefield,'regularcheck'=>$regularcheckfield,'returncheck'=>$returncheckfield,'wheelchaircheck'=>$wheelcarcheckfield,
            'starttime'=>$starttimefield,'startdate'=>$startdatefield,'endtime'=>$endtimefield,'enddate'=>$enddatefield,
            'carfield'=>$carfield,"dropcategorytype"=>$dropoffcategory,'dropaddress'=>$dropaddressfield,'dropotheraddress'=>$dropotheraddressfield,
            'dropzipcode'=>$dropzipcodefield,'dropcity'=>$dropcityfield,'droppackage'=>$droppackagefield,'droppackagename'=>$droppackagename,
            'dropairport'=>$dropairportfield,'dropairportname'=>$dropairportname,'dropflightnumber'=>$dropairportflightnumberfield,
            'dropflightarrivaltime'=>$dropairportarrivaltimefield,'droptrain'=>$droptrainfield,'droptrainname'=>$droptrainname,
            'droptrainnumber'=>$droptrainnumberfield,'droptrainarrivalnumber'=>$droptrainarrivaltimefield,'drophotel'=>$drophotelfield,'drophotelname'=>$drophotelname,'droppark'=>$dropparkfield,'dropparkname'=>$dropparkname,'bookingid'=>$bookingid,'servicecategory'=>$servicecategory,'service'=>$service,'booking_comment'=>$booking_comment,'mapaddress'=>$mapaddress,'dropmapaddress'=>$dropmapaddress);

    // regular day and time fields
    if(!empty($this->input->post("pickmondaycheckfield"))){
        $pickmonday=$this->input->post("pickmondayfield");
        $pickmondaytype=1;
        }
        if(!empty($this->input->post("picktuesdaycheckfield"))){
        $pickthuesday=$this->input->post("picktuesdayfield");
        $pickthuesdaytype=1;
       }
        if(!empty($this->input->post("pickwednesdaycheckfield"))){
        $pickwednesday=$this->input->post("pickwednesdayfield");
        $pickwednesdaytype=1;
       }
        if(!empty($this->input->post("pickthursdaycheckfield"))){
        $pickthursday=$this->input->post("pickthursdayfield");
        $pickthursdaytype=1;
       }
        if(!empty($this->input->post("pickfridaycheckfield"))){
        $pickfriday=$this->input->post("pickfridayfield");
        $pickfridaytype=1;
       }
        if(!empty($this->input->post("picksaturdaycheckfield"))){
        $picksaturday=$this->input->post("picksaturdayfield");
        $picksaturdaytype=1;
       }
        if(!empty($this->input->post("picksundaycheckfield"))){
        $picksunday=$this->input->post("picksundayfield");
        $picksundaytype=1;
       }

        $bookingstore['bookingpickregular']=array(
           array("day"=>"Monday","type"=>$pickmondaytype,"time"=>$pickmonday),
           array("day"=>"Tuesday","type"=>$pickthuesdaytype,"time"=>$pickthuesday),
           array("day"=>"Wednesday","type"=>$pickwednesdaytype,"time"=>$pickwednesday),
           array("day"=>"Thursday","type"=>$pickthursdaytype,"time"=>$pickthursday),
           array("day"=>"Friday","type"=>$pickfridaytype,"time"=>$pickfriday),
           array("day"=>"Saturday","type"=>$picksaturdaytype,"time"=>$picksaturday),
           array("day"=>"Sunday","type"=>$picksundaytype,"time"=>$picksunday),
        );

       //return day and time check fields
        if(!empty($this->input->post("returnmondaycheckfield"))){
        $returnmonday=$this->input->post("returnmondayfield");
        $returnmondaytype=1;
       }
        if(!empty($this->input->post("returntuesdaycheckfield"))){
        $returnthuesday=$this->input->post("returntuesdayfield");
        $returnthuesdaytype=1;
       }
        if(!empty($this->input->post("returnwednesdaycheckfield"))){
        $returnwednesday=$this->input->post("returnwednesdayfield");
        $returnwednesdaytype=1;
       }
        if(!empty($this->input->post("returnthursdaycheckfield"))){
        $returnthursday=$this->input->post("returnthursdayfield");
        $returnthursdaytype=1;
       }
        if(!empty($this->input->post("returnfridaycheckfield"))){
        $returnfriday=$this->input->post("returnfridayfield");
        $returnfridaytype=1;
       }
        if(!empty($this->input->post("returnsaturdaycheckfield"))){
        $returnsaturday=$this->input->post("returnsaturdayfield");
        $returnsaturdaytype=1;
       }
        if(!empty($this->input->post("returnsundaycheckfield"))){
        $returnsunday=$this->input->post("returnsundayfield");
        $returnsundaytype=1;
       }
       $bookingstore['bookingreturnregular']=array(
           array("day"=>"Monday","type"=>$returnmondaytype,"time"=>$returnmonday),
           array("day"=>"Tuesday","type"=>$returnthuesdaytype,"time"=>$returnthuesday),
           array("day"=>"Wednesday","type"=>$returnwednesdaytype,"time"=>$returnwednesday),
           array("day"=>"Thursday","type"=>$returnthursdaytype,"time"=>$returnthursday),
           array("day"=>"Friday","type"=>$returnfridaytype,"time"=>$returnfriday),
           array("day"=>"Saturday","type"=>$returnsaturdaytype,"time"=>$returnsaturday),
           array("day"=>"Sunday","type"=>$returnsundaytype,"time"=>$returnsunday),
        );


       $this->session->set_userdata($bookingstore);

            redirect("booking_calprice_save");
    }
    public function booking_calprice_save(){

         //declare variables
       $perioddiscount=0;$welcomediscount=0;$rewarddiscount=0;$promocodediscount=0;
       $perioddiscountfee=0;$welcomediscountfee=0;$rewarddiscountfee=0;$promocodediscountfee=0;
       $isnighttime=false;$ismydecember=false;$isnotworkingday=false;
       $nighttimefee=0;$maydecdayfee=0;$notworkingdayfee=0;
       $nighttimediscount=0;$maydecdaydiscount=0;$notworkingdaydiscount=0;
       $isdrivermeal=false;$drivermealfee=0;
       $isdriverrest=false;$driverrestfee=0;
       $vatname='';$vatdiscount=0;$vatfee=0;
       $totalprice=0;
       $isreturnnighttime=false;$isreturnmydecember=false;$isreturnnotworkingday=false;
       $isreturndrivermeal=false;$isreturndriverrest=false;
       $distance=0;
       $realrideprice=0;$realridetotalprice=0;
       //end variables
 
            if(!(bool) $this->basic_auth->is_login() || !$this->basic_auth->is_client()) {
               $this->session->set_userdata("current_booking_url",'booking_calprice_save');
               redirect('client/identification');
             }

            if(!$this->session->userdata['bookingdata']){
               redirect("booking");
             }
          $bookingstops=$this->session->userdata['stops']; 
          $bookingdata=$this->session->userdata['bookingdata'];    
          $pickdatefield=$bookingdata['pickupdate'];
          $picktimefield=$bookingdata['pickuptime'];
          $startdatefield=$bookingdata['startdate'];
          $starttimefield=$bookingdata['starttime']; 
          $returndatefield=$bookingdata['returndate'];
          $returntimefield=$bookingdata['returntime'];
          $enddatefield=$bookingdata['enddate'];
          $endtimefield=$bookingdata['endtime'];  
          $waitingtimefield=$bookingdata['waitingtime'];
          $mapaddress=$bookingdata['mapaddress'];
          $dropmapaddress=$bookingdata['dropmapaddress'];
          $carfield=$bookingdata['carfield'];
          $regularcheck=$bookingdata['regularcheck'];
          $returncheck=$bookingdata['returncheck'];
          $servicedata= $this->bookings_model->getservicesbyid('vbs_u_service', $bookingdata['service']);
          $vatdata= $this->bookings_model->getservicesbyid('vbs_u_vat', $servicedata->tva);
          $vatname=$vatdata->name_of_var;
          $vatdiscount=$vatdata->vat;
          $result=$this->get_coordinates($mapaddress,$dropmapaddress,$bookingstops);
          
           
        if($result){
          //print_r($result);
          if($carfield){
           $pricedata=$this->bookings_model->getpricebycarid($carfield);
       
           if($pricedata){
            
            if($result['distance'] > 0){
              

               //$distance=round($result['distance']*1.609344, 2);
               $dis=str_replace(',', '', $result['distance']);

               $distance=round(floatval($dis) * 1.609344 , 2);

            }
            
            $time=$result['time'];
            $minutes=$result['minutes'];
            $hours=$minutes/60;
            $waitingminutes=0;
            $discount=0;
            

            
            if($regularcheck==0){
                    $pickdate=$pickdatefield;
                      $picktime=$picktimefield;
                      $perioddiscounts=$this->bookings_model->getdiscountbydate($pickdate,$picktime);              
                  if($perioddiscounts){
                    $perioddiscount=$perioddiscounts->discount;
                  }
                   //check booking date is a 1may or 25 december
                   $is1mayordecember=$this->bookings_model->getdateofmaydec($pickdate);
                   if($is1mayordecember){
                      $ismydecember=true;
                    
                   }
        
                   //check picktime is night time
                  $isnighttime=$this->checknighttime($pricedata,$picktime);
                 
                   //check driver meal
                   $isdrivermeal=$this->checkdrivermealtime($pricedata,$picktime);
                   //end driver meal
                 
                  //booking date in  not working days and driver rest in not working day and more than 3 hours ride
                    $notworkingdayscheck=$this->bookings_model->getnotworkingdate($pricedata->id,$pickdate);
                    $todayissunday=$this->isWeekend($pickdate) ;
                 
                    if($notworkingdayscheck || $todayissunday){
                         $isnotworkingday=true;
                         if($hours > 3){
                           $isdriverrest=true;
                         }
                        
                    }


               //give the values if user select return ride
               if($returncheck!=0){
                $returndate=$returndatefield;
                        $returntime=$returntimefield;
                       
                       //period discount
                         if(!$perioddiscounts){
                       $returnperioddiscounts=$this->bookings_model->getdiscountbydate($returndate,$returntime);
                        if($returnperioddiscounts){
                        $perioddiscount=$returnperioddiscounts->discount;
                      }
                     }
        
                   //check returntime is night time
                  $isreturnnighttime=$this->checknighttime($pricedata,$returntime);
                 
                   //check driver meal
                   $isreturndrivermeal=$this->checkdrivermealtime($pricedata,$returntime);
                   //end driver meal
                 
                 

                    if($pickdate != $returndate){
                       //check booking date is a 1may or 25 december
                     $isreturn1mayordecember=$this->bookings_model->getdateofmaydec($returndate);
                     if($isreturn1mayordecember){
                        $isreturnmydecember=true;
                      
                     }
                              //booking date in  not working days and driver rest in not working day and more than 3 hours ride
                      $notreturnworkingdayscheck=$this->bookings_model->getnotworkingdate($pricedata->id,$returndate);
                      $todayissunday=$this->isWeekend($returndate) ;
                   
                      if($notreturnworkingdayscheck || $todayissunday){
                           $isreturnnotworkingday=true;
                           if($hours > 3){
                             $isreturndriverrest=true;
                           }
                          
                      }
                    }
               }

            
            }



                    
   
                $client=$this->session->userdata['user'];

                //check client is already exist in booking table for the bonus welcome
                $clientexistcount=$this->bookings_model->clientexist($client->id);
               
                if(!$clientexistcount){
                     $welcomediscounts=$this->bookings_model->getwelcomediscount();
                      $welcomediscount=$welcomediscounts->discount;
                    
                }else{
                  
                     $rewarddiscounts=$this->bookings_model->getrewarddiscount();
                  
                     if( $clientexistcount >= $rewarddiscounts->no_of_booking){
                      $rewarddiscount=$rewarddiscounts->discount;
                   
                     }
                }

             
                if(!empty($waitingtimefield)){
                        $waitarray=explode(":", $waitingtimefield);
                        $waitingminutes= (($waitarray[0]*60) + $waitarray[1]);
                }
                foreach ($bookingstops as $stop) {
                  $waitarray = explode(":", $stop['stopwaittime']);
                  $waitingminutes += (($waitarray[0]*60) + $waitarray[1]);
                }
        
            $discount=$welcomediscount+$rewarddiscount+$perioddiscount;
           
           
           
            $price=($pricedata->pickup_fee + ($pricedata->price_km_approche * $distance)+($pricedata->price_trajet * $minutes) + ($pricedata->attente * $waitingminutes));
            if($price <  $pricedata->minimum_fee){
              $price=$pricedata->minimum_fee;
            }
            if($regularcheck == 0){
              if($returncheck !=0){
                   $price=($price * 2);
                  $totalprice=$price;

              }else{
                 $totalprice=$price;
              }
            }else{
               $totalprice=$price;
            }
           
             if($isnighttime){
              if($pricedata->nightfeetype==1){
               $totalprice=$totalprice + $pricedata->majoration_de_nuit;
               $nighttimefee=$pricedata->majoration_de_nuit;
               
              }else{
                  $totalprice=$totalprice + ($price * ($pricedata->majoration_de_nuit/100));
                   $nighttimefee=($price * ($pricedata->majoration_de_nuit/100));
                   $nighttimediscount=$pricedata->majoration_de_nuit;
              }
         
            }
            if($isnotworkingday){
               if($pricedata->notworkfeetype==1){
               $totalprice=$totalprice + $pricedata->supplement_jour_ferie;
               $notworkingdayfee=$pricedata->supplement_jour_ferie;

              }else{
                  $totalprice=$totalprice + ($price * ($pricedata->supplement_jour_ferie/100));
                   $notworkingdayfee=($price * ($pricedata->supplement_jour_ferie/100));
                   $notworkingdaydiscount=$pricedata->supplement_jour_ferie;
              }
            
            }
            if($ismydecember){
               if($pricedata->decfeetype==1){
               $totalprice=$totalprice + $pricedata->supplement_1er;
               $maydecdayfee=$pricedata->supplement_1er;
               
              }else{
                  $totalprice=$totalprice + ($price * ($pricedata->supplement_1er/100));
                  $maydecdayfee=($price * ($pricedata->supplement_1er/100));
                  $maydecdaydiscount=$pricedata->supplement_1er;
              }
             
            }
            if($isdrivermeal){
                  $totalprice=$totalprice + $pricedata->panier_repas;
                  $drivermealfee=$pricedata->panier_repas;
            }
             if($isdriverrest){
                  $totalprice=$totalprice + $pricedata->repos_compensateur;
                  $driverrestfee=$pricedata->repos_compensateur;

            }
            //return ride add values
               
                if($isreturnnighttime){
              if($pricedata->nightfeetype==1){
               $totalprice=$totalprice + $pricedata->majoration_de_nuit;
               $nighttimefee +=$pricedata->majoration_de_nuit;
               
              }else{
                  $totalprice=$totalprice + ($price * ($pricedata->majoration_de_nuit/100));
                   $nighttimefee +=($price * ($pricedata->majoration_de_nuit/100));
                   $nighttimediscount +=$pricedata->majoration_de_nuit;
              }
         
            }
            if($isreturnnotworkingday){
               if($pricedata->notworkfeetype==1){
               $totalprice=$totalprice + $pricedata->supplement_jour_ferie;
               $notworkingdayfee +=$pricedata->supplement_jour_ferie;

              }else{
                  $totalprice=$totalprice + ($price * ($pricedata->supplement_jour_ferie/100));
                   $notworkingdayfee +=($price * ($pricedata->supplement_jour_ferie/100));
                   $notworkingdaydiscount +=$pricedata->supplement_jour_ferie;
              }
            
            }
            if($isreturnmydecember){
               if($pricedata->decfeetype==1){
               $totalprice=$totalprice + $pricedata->supplement_1er;
               $maydecdayfee +=$pricedata->supplement_1er;
               
              }else{
                  $totalprice=$totalprice + ($price * ($pricedata->supplement_1er/100));
                  $maydecdayfee +=($price * ($pricedata->supplement_1er/100));
                  $maydecdaydiscount +=$pricedata->supplement_1er;
              }
             
            }
            if($isreturndrivermeal){
                  $totalprice=$totalprice + $pricedata->panier_repas;
                  $drivermealfee +=$pricedata->panier_repas;
            }
             if($isreturndriverrest){
                  $totalprice=$totalprice + $pricedata->repos_compensateur;
                  $driverrestfee +=$pricedata->repos_compensateur;

            }
            //end return ride add values
          
            if($welcomediscount!=0){
               $totalprice=$totalprice - ($price * ($welcomediscount/100));
               $welcomediscountfee=($price * ($welcomediscount/100));
            }
            if($rewarddiscount!=0){
               $totalprice=$totalprice - ($price * ($rewarddiscount/100));
               $rewarddiscountfee= ($price * ($rewarddiscount/100));
            }
            if($perioddiscount!=0){
             
               $totalprice=$totalprice - ($price * ($perioddiscount/100));
               $perioddiscountfee=($price * ($perioddiscount/100));
            }
            if($vatdiscount!=0){
             
               $totalprice=$totalprice + ($price * ($vatdiscount/100));
               $vatfee=($price * ($vatdiscount/100));
            }
          
            
            
             $nighttimefee=round($nighttimefee, 2);
             $notworkingdayfee=round($notworkingdayfee, 2);
             $maydecdayfee=round($maydecdayfee, 2);
            $drivermealfee=round($drivermealfee, 2);
             $driverrestfee=round($driverrestfee, 2);
             $welcomediscountfee=round($welcomediscountfee, 2);
             $rewarddiscountfee=round($rewarddiscountfee, 2);
             $perioddiscountfee=round($perioddiscountfee, 2);
             $totalprice=round($totalprice, 2);
             $price=round($price, 2);

             //price and distance double in return ride
               if($regularcheck==0){
                if($returncheck!=0){
                  
                  $minutes = $minutes * 2;
                  $distance=round($distance * 2, 2);
                 }
               }

            $caltimehour=intdiv($minutes, 60);
            $caltimeminutes=$minutes % 60;

            if($caltimehour > 1){
              $caltimehour= $caltimehour." "."hours";
            }elseif($caltimehour == 1){
              $caltimehour= $caltimehour." "."hour";
            }else{
               $caltimehour= "";
            }
            if($caltimeminutes > 1){
              $caltimeminutes= $caltimeminutes." "."mins";
            }elseif($caltimeminutes == 1){
               $caltimeminutes= $caltimeminutes." "."min";
            }else{
               $caltimeminutes= "";
            }

            $time=$caltimehour." ".$caltimeminutes;
             //end calculate
                 //calculate Real Ride Price
            if($pricedata->car_place==0){
                 $companydata=$this->bookings_model->getcompanystatus();
               $realridepickdistancevalue=$this->get_other_coordinates($companydata->address,$mapaddress);
              $dis1=str_replace(',', '', $realridepickdistancevalue['distance']);
              $realridepickdistance=round(floatval($dis1) * 1.609344 , 2);
              
               $realridedropdistancevalue=$this->get_other_coordinates($companydata->address,$dropmapaddress);
              $dis2=str_replace(',', '', $realridedropdistancevalue['distance']);
              $realridedropdistance=round(floatval($dis2) * 1.609344 , 2);
               
               $realridepickprice=($pricedata->price_trajet_retour * $realridepickdistance);
               $realridedropprice=($pricedata->return_rate * $realridedropdistance);
               $realrideprice=$realridepickprice + $realridedropprice;
               $realridetotalprice=$price + $realrideprice;
               $realrideprice=round($realrideprice, 2);
               $realridetotalprice=round($realridetotalprice, 2);
              
            }
            //calculate Real Ride Price
             //is ride fee pick and return
             $isnighttimetype = 0;
             $isnotworkingdaytype = 0;
             $ismydecembertype = 0;
             $isdrivermealtype = 0;
             $isdriverresttype = 0;

             $isreturnnighttimetype = 0;
             $isreturnnotworkingdaytype = 0;
             $isreturnmydecembertype = 0;
             $isreturndrivermealtype = 0;
             $isreturndriverresttype = 0;

             if($isnighttime){
               $isnighttimetype = 1;
             }
             if($isnotworkingday){
               $isnotworkingdaytype = 1;
             }
             if($ismydecember){
               $ismydecembertype = 1;
             }
             if($isdrivermeal){
               $isdrivermealtype = 1;
             }
             if($isdriverrest){
               $isdriverresttype = 1;
             }
             if($isreturnnighttime){
               $isreturnnighttimetype = 1;
             }
             if($isreturnnotworkingday){
               $isreturnnotworkingdaytype = 1;
             }
             if($isreturnmydecember){
               $isreturnmydecembertype = 1;
             }
             if($isreturndrivermeal){
               $isreturndrivermealtype = 1;
             }
             if($isreturndriverrest){
               $isreturndriverresttype = 1;
             }
         //is ride fee pick and return
           
            $bookingstore['mapdatainfo']=array('distance'=> $distance,'time'=>$time,'price'=>$price,'totalprice'=>$totalprice,'nighttimefee'=>$nighttimefee,'notworkingdayfee'=>$notworkingdayfee,'maydecdayfee'=>$maydecdayfee,'nighttimediscount'=>$nighttimediscount,'notworkingdaydiscount'=>$notworkingdaydiscount,'maydecdaydiscount'=>$maydecdaydiscount,'pickupaddress'=>$mapaddress,'dropoffaddress'=>$dropmapaddress,'welcomediscount'=>$welcomediscount,'rewarddiscount'=>$rewarddiscount,'perioddiscount'=>$perioddiscount,'promocodediscount'=>$promocodediscount,'totaldiscount'=>$discount,'welcomediscountfee'=>$welcomediscountfee,'rewarddiscountfee'=>$rewarddiscountfee,'perioddiscountfee'=>$perioddiscountfee,'promocodediscountfee'=>$promocodediscountfee,'vatname'=>$vatname,'vatdiscount'=>$vatdiscount,'vatfee'=>round($vatfee, 2),'drivermealfee'=>$drivermealfee,'driverrestfee'=>$driverrestfee,'realrideprice'=>$realrideprice,'realridetotalprice'=>$realridetotalprice,'isnighttimetype' => $isnighttimetype,'isnotworkingdaytype' => $isnotworkingdaytype,'ismydecembertype' => $ismydecembertype,'isdrivermealtype' => $isdrivermealtype,'isdriverresttype' => $isdriverresttype,'isreturnnighttimetype' => $isreturnnighttimetype,'isreturnnotworkingdaytype' => $isreturnnotworkingdaytype,'isreturnmydecembertype' => $isreturnmydecembertype,'isreturndrivermealtype' => $isreturndrivermealtype,'isreturndriverresttype' => $isreturndriverresttype);
                $this->session->set_userdata($bookingstore);
                 return redirect("booking_quote");
           }else{
             $this->session->set_flashdata('alert', [
                        'message' => " :something is wrong. please try again",
                        'class' => "alert-danger",
                        'type' => "Error"
                    ]);
            return redirect("booking");
               }

          }else{
                $this->session->set_flashdata('alert', [
                        'message' => " :something is wrong. please try again",
                        'class' => "alert-danger",
                        'type' => "Error"
                    ]);
            return redirect("booking");
          }

        }else{
            $this->session->set_flashdata('alert', [
                        'message' => " :something is wrong. please try again",
                        'class' => "alert-danger",
                        'type' => "Error"
                    ]);
            return redirect("booking");
        }
    }
function checknighttime($pricedata,$picktime){
  $isnighttime=false;
   if($pricedata->night_time_from >= "12 : 00" && $pricedata->night_time_to >= "12 : 00"  && $picktime >= "12 : 00"){
                  if($picktime >= $pricedata->night_time_from &&  $picktime <= $pricedata->night_time_to){
                      $isnighttime=true;
                     
                  }
            
             }

             elseif($pricedata->night_time_from <= "12 : 00" && $pricedata->night_time_to <= "12 : 00"  && $picktime <= "12 : 00"){
                  if($picktime >= $pricedata->night_time_from &&  $picktime <= $pricedata->night_time_to){
                      $isnighttime=true;
                     
                  }
            
             }
              elseif($pricedata->night_time_from >= "12 : 00" && $pricedata->night_time_to <= "12 : 00"  && $picktime >= "12 : 00"){
                  if($picktime >= $pricedata->night_time_from){
                      $isnighttime=true;
                    
                  }
            
             }
              elseif($pricedata->night_time_from <= "12 : 00" && $pricedata->night_time_to >= "12 : 00"  && $picktime >= "12 : 00"){
                  if($picktime <= $pricedata->night_time_to){
                      $isnighttime=true;
                      
                  }
            
             }
              elseif($pricedata->night_time_from >= "12 : 00" && $pricedata->night_time_to <= "12 : 00"  && $picktime <= "12 : 00"){
                  if($picktime <= $pricedata->night_time_to){
                      $isnighttime=true;
                    
                  }
            
             }
              elseif($pricedata->night_time_from <= "12 : 00" && $pricedata->night_time_to >= "12 : 00"  && $picktime <= "12 : 00"){
                  if($picktime >= $pricedata->night_time_from){
                      $isnighttime=true;
                    
                  }
            
             }
             return $isnighttime;
}
//driver meal time function return true or false
 function checkdrivermealtime($pricedata,$picktime){
        //check picktime is driver meal
           //driver first meal check
              $isdrivermeal=false;
             if($pricedata->driver_meal_from >= "12 : 00" && $pricedata->driver_meal_to >= "12 : 00"  && $picktime >= "12 : 00"){
                  if($picktime >= $pricedata->driver_meal_from &&  $picktime <= $pricedata->driver_meal_to){
                      $isdrivermeal=true;
                     
                  }
            
             }

             elseif($pricedata->driver_meal_from <= "12 : 00" && $pricedata->driver_meal_to <= "12 : 00"  && $picktime <= "12 : 00"){
                  if($picktime >= $pricedata->driver_meal_from &&  $picktime <= $pricedata->driver_meal_to){
                      $isdrivermeal=true;
                     
                  }
            
             }
              elseif($pricedata->driver_meal_from >= "12 : 00" && $pricedata->driver_meal_to <= "12 : 00"  && $picktime >= "12 : 00"){
                  if($picktime >= $pricedata->driver_meal_from){
                      $isdrivermeal=true;
                    
                  }
            
             }
              elseif($pricedata->driver_meal_from <= "12 : 00" && $pricedata->driver_meal_to >= "12 : 00"  && $picktime >= "12 : 00"){
                  if($picktime <= $pricedata->driver_meal_to){
                      $isdrivermeal=true;
                      
                  }
            
             }
              elseif($pricedata->driver_meal_from >= "12 : 00" && $pricedata->driver_meal_to <= "12 : 00"  && $picktime <= "12 : 00"){
                  if($picktime <= $pricedata->driver_meal_to){
                      $isdrivermeal=true;
                    
                  }
            
             }
              elseif($pricedata->driver_meal_from <= "12 : 00" && $pricedata->driver_meal_to >= "12 : 00"  && $picktime <= "12 : 00"){
                  if($picktime >= $pricedata->driver_meal_from){
                      $isdrivermeal=true;
                    
                  }
            
             }
             //end first meal
             //driver second meal
                 if($pricedata->driver_othermeal_from >= "12 : 00" && $pricedata->driver_othermeal_to >= "12 : 00"  && $picktime >= "12 : 00"){
                  if($picktime >= $pricedata->driver_othermeal_from &&  $picktime <= $pricedata->driver_othermeal_to){
                      $isdrivermeal=true;
                     
                  }
            
             }

             elseif($pricedata->driver_othermeal_from <= "12 : 00" && $pricedata->driver_othermeal_to <= "12 : 00"  && $picktime <= "12 : 00"){
                  if($picktime >= $pricedata->driver_othermeal_from &&  $picktime <= $pricedata->driver_othermeal_to){
                      $isdrivermeal=true;
                     
                  }
            
             }
              elseif($pricedata->driver_othermeal_from >= "12 : 00" && $pricedata->driver_othermeal_to <= "12 : 00"  && $picktime >= "12 : 00"){
                  if($picktime >= $pricedata->driver_othermeal_from){
                      $isdrivermeal=true;
                    
                  }
            
             }
              elseif($pricedata->driver_othermeal_from <= "12 : 00" && $pricedata->driver_othermeal_to >= "12 : 00"  && $picktime >= "12 : 00"){
                  if($picktime <= $pricedata->driver_othermeal_to){
                      $isdrivermeal=true;
                      
                  }
            
             }
              elseif($pricedata->driver_othermeal_from >= "12 : 00" && $pricedata->driver_othermeal_to <= "12 : 00"  && $picktime <= "12 : 00"){
                  if($picktime <= $pricedata->driver_othermeal_to){
                      $isdrivermeal=true;
                    
                  }
            
             }
              elseif($pricedata->driver_othermeal_from <= "12 : 00" && $pricedata->driver_othermeal_to >= "12 : 00"  && $picktime <= "12 : 00"){
                  if($picktime >= $pricedata->driver_othermeal_from){
                      $isdrivermeal=true;
                    
                  }
            
             }
             //end second meal
            return  $isdrivermeal;
  //end driver meal           
    }

    public function confirmbooking(){
       if (!(bool) $this->basic_auth->is_login() || !$this->basic_auth->is_client()) {

              $this->session->set_userdata("current_booking_url",'confirmbooking');
              redirect('client/identification');
        }
    $booking=$this->session->userdata['bookingdata'];  
    $payment_method=$this->input->post("payment_method");
    $paymentstatus=$this->input->post('paymentstatus');
    $methods=$this->bookings_model->getpaymentmethod('vbs_u_payment_method',$payment_method);
    //form validation for credit card and direct debit
    if($paymentstatus=="1"){
        $methodstr = strtolower($methods->name);
        $methodstr = str_replace(" ", "",  $methodstr);
        if($methodstr == "creditcard" || $methodstr == "bankdirectdebit"){
      if($methodstr == "creditcard"){
                 $this->form_validation->set_rules('name_credit', 'name of card', 'trim|required|max_length[50]');
                 $this->form_validation->set_rules('card_num_credit', 'number of card', 'trim|required|max_length[20]');
                 $this->form_validation->set_rules('exp_month_credit', 'expired month', 'trim|required|max_length[2]');
                 $this->form_validation->set_rules('exp_year_credit', 'expired year', 'trim|required|max_length[4]');
                 $this->form_validation->set_rules('cvc_credit', 'cvc code', 'trim|required|max_length[4]');
               }
     elseif($methodstr == "bankdirectdebit"){
       $this->form_validation->set_rules('civility_debit', 'civility', 'trim|required');
       $this->form_validation->set_rules('fname_debit', 'first name', 'trim|required|max_length[50]');
       $this->form_validation->set_rules('lname_debit', 'last name', 'trim|required|max_length[50]');
       $this->form_validation->set_rules('bankname_debit', 'bank name', 'trim|required|max_length[50]');
       $this->form_validation->set_rules('agencyname_debit', 'agency name', 'trim|required|max_length[50]');
       $this->form_validation->set_rules('agencyaddr_debit', 'agency address', 'trim|required|max_length[200]');
       $this->form_validation->set_rules('otheraddr_debit', 'other address', 'trim|required|max_length[200]');
       $this->form_validation->set_rules('agencyzipcode_debit', 'zip code', 'trim|required|max_length[10]');
       $this->form_validation->set_rules('agencycity_debit', 'city', 'trim|required|max_length[50]');
       $this->form_validation->set_rules('ibnnum_debit', 'ibn number', 'trim|required|max_length[50]');
       $this->form_validation->set_rules('swiftcode_debit', 'swift code', 'trim|required|max_length[50]');
       $this->form_validation->set_rules('agreement_debit', 'agreement', 'trim|required');
     
     }
      $this->form_validation->set_message('required', 'REQUIRED');
      $this->form_validation->set_message('max_length','LENGTH EXCEED');
      if ($this->form_validation->run() == FALSE)
        {

          if($methodstr == "creditcard"){
           $this->session->set_flashdata('payment_method_form', [
                                      'credit_card_name' =>array("error"=>form_error('name_credit'),"value"=>$this->form_validation->set_value('name_credit')), 
                                       'credit_card_number' =>array("error"=>form_error('card_num_credit'),"value"=>$this->form_validation->set_value('card_num_credit')), 
                                       'credit_card_month' =>array("error"=>form_error('exp_month_credit'),"value"=>$this->form_validation->set_value('exp_month_credit')), 
                                       'credit_card_year' =>array("error"=>form_error('exp_year_credit'),"value"=>$this->form_validation->set_value('exp_year_credit')), 
                                       'credit_card_cvc' =>array("error"=>form_error('cvc_credit'),"value"=>$this->form_validation->set_value('cvc_credit')), 
                                       'level'=>array('step'=>'1')
                                      
                                                
           ]);
         }elseif($methodstr == "bankdirectdebit"){
           $this->session->set_flashdata('payment_method_form', [
                                      'civility_debit' =>array("error"=>form_error('civility_debit'),"value"=>$this->form_validation->set_value('civility_debit')),
                                      'fname_debit' =>array("error"=>form_error('fname_debit'),"value"=>$this->form_validation->set_value('fname_debit')),
                                      'lname_debit' =>array("error"=>form_error('lname_debit'),"value"=>$this->form_validation->set_value('lname_debit')),
                                      'bankname_debit' =>array("error"=>form_error('bankname_debit'),"value"=>$this->form_validation->set_value('bankname_debit')),
                                      'agencyname_debit' =>array("error"=>form_error('agencyname_debit'),"value"=>$this->form_validation->set_value('agencyname_debit')),
                                      'agencyaddr_debit' =>array("error"=>form_error('agencyaddr_debit'),"value"=>$this->form_validation->set_value('agencyaddr_debit')),
                                      'otheraddr_debit' =>array("error"=>form_error('otheraddr_debit'),"value"=>$this->form_validation->set_value('otheraddr_debit')),
                                      'agencyzipcode_debit' =>array("error"=>form_error('agencyzipcode_debit'),"value"=>$this->form_validation->set_value('agencyzipcode_debit')),
                                      'agencycity_debit' =>array("error"=>form_error('agencycity_debit'),"value"=>$this->form_validation->set_value('agencycity_debit')),
                                      'ibnnum_debit' =>array("error"=>form_error('ibnnum_debit'),"value"=>$this->form_validation->set_value('ibnnum_debit')),
                                      'swiftcode_debit' =>array("error"=>form_error('swiftcode_debit'),"value"=>$this->form_validation->set_value('swiftcode_debit')),
                                      'agreement_debit' =>array("error"=>form_error('agreement_debit'),"value"=>$this->form_validation->set_value('agreement_debit')),
                                       'level'=>array('step'=>'5')
                                      
                                                
           ]);
         }
                   redirect("booking_payment");  
      }
    }
   }
    // exit();
    //end validation           
     
      if($payment_method){
         if($this->session->userdata['bookingdata']){
            $client=$this->session->userdata['user'];
            $clientdata=$this->bookings_model->getclientdatabyid($client->id);
            $car_name=$this->bookings_model->getcarname($booking["carfield"]);
            $booking=$this->session->userdata['bookingdata'];
            $mapinfodata=$this->session->userdata['mapdatainfo'];
            $stopsdata=$this->session->userdata['stops'];
            $pickupdate;$returndate;$startdate;$enddate;
          if(!empty($booking['pickupdate'])){
             $pickupdate=str_replace('/', '-', $booking['pickupdate']);
             $pickupdate=strtotime($pickupdate);
             $pickupdate = date('Y-m-d',$pickupdate);
          }
          if(!empty($booking['returndate'])){
             $returndate=str_replace('/', '-', $booking['returndate']);
             $returndate=strtotime($returndate);
             $returndate = date('Y-m-d',$returndate);
          }
          if(!empty($booking['startdate'])){
             $startdate=str_replace('/', '-', $booking['startdate']);
             $startdate=strtotime($startdate);
             $startdate = date('Y-m-d',$startdate);
          }
          if(!empty($booking['enddate'])){
             $enddate=str_replace('/', '-', $booking['enddate']);
             $enddate=strtotime($enddate);
             $enddate = date('Y-m-d',$enddate);
          }


           if(!empty($booking['pickupdate'])){
               $bookingdate=$pickupdate;
              }
              else{
                $bookingdate=$startdate;
              }
          $id = $this->bookings_model->addbookingrecord('vbs_bookings',[
          'user_id'     => @$client->id,
          'booking_ref'        => create_timestampdmy_uid($bookingdate,($booking['bookingid']+1)),
          'pick_date'          => $pickupdate,
          'pick_time'          => $booking['pickuptime'],
          'pick_point'         => $this->cleanstr($mapinfodata['pickupaddress']),
          'drop_point'         => $this->cleanstr($mapinfodata['dropoffaddress']),
          'totaldiscount'      => $mapinfodata['totaldiscount'],
          'distance'           => $mapinfodata['distance'],
          'vehicle_selected'   => $booking['carfield'],
          'cost_of_journey'    => $mapinfodata['price'],
          'price'              => $mapinfodata['price'],
          'totalprice'         => $mapinfodata['totalprice'],
          'bookdate'           => @date('Y-m-d'),
          'booktime'           => @date('h : i'),
          'registered_name'    => $clientdata->username,
          'phone'              => $clientdata->phone,
          'email'              => $clientdata->email,
          'transaction_id'     => ($booking['bookingid']+1),
          'payer_id'           => 0,
          'package_type'       => "Null",
          'info_to_drivers'    => "Null",
          'payer_name'         => "Null",
          'is_new'             => 0,
          'unread'             => 0,
          'time_of_journey'    => $mapinfodata['time'],
          'pickupcategory'     => $booking['categorytype'],
          'dropoffcategory'    => $booking['dropcategorytype'],
          'waiting_time'       => $booking['waitingtime'],
          'return_time'        => $booking['returntime'],
          'return_date'        => $returndate,
          'start_time'         => $booking['starttime'],
          'start_date'         => $startdate,
          'end_time'           => $booking['endtime'],
          'end_date'           => $enddate,
          'car_id'             => $booking['carfield'],
          'pickuppackage_id'   => $booking['package'],
          'droppackage_id'     => $booking['droppackage'],
          'pickuphotel_id'     => $booking['hotel'],
          'drophotel_id'       => $booking['drophotel'],
          'pickuppark_id'      => $booking['park'],
          'droppark_id'        => $booking['droppark'],
          'regular'            => $booking['regularcheck'],
          'returncheck'        => $booking['returncheck'],
          'wheelchair'         => $booking['wheelchaircheck'],
          'payment_method_id'  => $payment_method,
          'service_id'         => $booking['service'],
          'service_category_id'=> $booking['servicecategory'],
          'booking_comment'    => $booking['booking_comment'],
          'ridedate'           => $bookingdate,
          'realrideprice'      => $mapinfodata['realrideprice'],
          'realridetotalprice' => $mapinfodata['realridetotalprice'],
          'pickzipcode'        => $booking['zipcode'],
          'pickcity'           => $booking['city'],
          'dropzipcode'        => $booking['dropzipcode'],
          'dropcity'           => $booking['dropcity'],

        ]);
          if($id){
            //add notification in booking
        
              $module='6';
              $notificationstatuspending='1';

              $notificationdata     = $this->bookings_model->getnotification('vbs_notifications',['department' => $module,'status'=>$notificationstatuspending]);

              if($notificationdata){
                  $notificationid = $this->bookings_model->addbookingrecord('vbs_bookings_notification',[
                 'date'            => date('Y-m-d'),
                 'time'            => date('h : i'),
                 'booking_id'      => $id,
                 'notification_id' => $notificationdata->id
                ]);
              }

               //add notification in booking

               //add stops
            foreach($stopsdata as $stop){
                $stopid=$this->bookings_model->addbookingrecord('vbs_booking_stops',[
                  'stopaddress'          => $stop['stopaddress'],
                  'stopwaittime'         => $stop['stopwaittime'],
                  'booking_id'           =>  @$id,
                ]);
            }
               //add stops
               //add discount and fee and tax in vbs_booking_pricevalues
              $pricevalueid = $this->bookings_model->addbookingrecord('vbs_bookings_pricevalues',[
                  'nighttimefee'         => $mapinfodata['nighttimefee'],
                  'notworkingdayfee'     => $mapinfodata['notworkingdayfee'],
                  'maydecdayfee'         => $mapinfodata['maydecdayfee'],
                  'drivermealfee'        => $mapinfodata['drivermealfee'],
                  'driverrestfee'        => $mapinfodata['driverrestfee'],
                  'nighttimediscount'    => $mapinfodata['nighttimediscount'],
                  'notworkingdaydiscount'=> $mapinfodata['notworkingdaydiscount'],
                  'maydecdaydiscount'    => $mapinfodata['maydecdaydiscount'],
                  'welcomediscount'      => $mapinfodata['welcomediscount'],
                  'perioddiscount'       => $mapinfodata['perioddiscount'],
                  'rewarddiscount'       => $mapinfodata['rewarddiscount'],
                  'promocodediscount'    => $mapinfodata['promocodediscount'],
                  'welcomediscountfee'   => $mapinfodata['welcomediscountfee'],
                  'perioddiscountfee'    => $mapinfodata['perioddiscountfee'],
                  'rewarddiscountfee'    => $mapinfodata['rewarddiscountfee'],
                  'promocodediscountfee' => $mapinfodata['promocodediscountfee'],
                  'vatdiscount'          => $mapinfodata['vatdiscount'],
                  'vatfee'               => $mapinfodata['vatfee'],
                  'isnighttimetype'                => $mapinfodata['isnighttimetype'],
                  'isnotworkingdaytype'            => $mapinfodata['isnotworkingdaytype'],
                  'ismydecembertype'               => $mapinfodata['ismydecembertype'],
                  'isdrivermealtype'               => $mapinfodata['isdrivermealtype'],
                  'isdriverresttype'               => $mapinfodata['isdriverresttype'],
                  'isreturnnighttimetype'          => $mapinfodata['isreturnnighttimetype'],
                  'isreturnnotworkingdaytype'      => $mapinfodata['isreturnnotworkingdaytype'],
                  'isreturnmydecembertype'         => $mapinfodata['isreturnmydecembertype'],
                  'isreturndrivermealtype'         => $mapinfodata['isreturndrivermealtype'],
                  'isreturndriverresttype'         => $mapinfodata['isreturndriverresttype'],
                  'booking_id'           =>  @$id,
                  
              ]);
               //end discount and fee and tax  section

             if(!empty($booking["airport"])){
              $airportid = $this->bookings_model->addbookingrecord('vbs_booking_airport',[
                'flightnumber'     =>  $this->cleanstr($booking["flightnumber"]),
                'arrivaltime'      =>  $this->cleanstr($booking["flightarrivaltime"]),
                'poi_id'           =>  @$booking["airport"],
                'booking_id'       =>  @$id,
                'type'             =>  "pickup"
            ]);
             }
              if(!empty($booking["dropairport"])){
              $dropairportid = $this->bookings_model->addbookingrecord('vbs_booking_airport',[
                'flightnumber'     =>  $this->cleanstr($booking["dropflightnumber"]),
                'arrivaltime'      =>  $this->cleanstr($booking["dropflightarrivaltime"]),
                'poi_id'           =>  @$booking["dropairport"],
                'booking_id'       =>  @$id,
                'type'             =>  "dropoff"
            ]);
             }
              if(!empty($booking["train"])){
              $trainid = $this->bookings_model->addbookingrecord('vbs_booking_train',[
                'trainnumber'     =>  $this->cleanstr($booking["trainnumber"]),
                'arrivaltime'     =>  $this->cleanstr($booking["trainarrivalnumber"]),
                'poi_id'          =>  @$booking["train"],
                'booking_id'      =>  @$id,
                'type'            =>  "pickup",

            ]);
             }
              if(!empty($booking["droptrain"])){
              $droptrainid = $this->bookings_model->addbookingrecord('vbs_booking_train',[
                'trainnumber'     => $this->cleanstr($booking["droptrainnumber"]),
                'arrivaltime'     => $this->cleanstr($booking["droptrainarrivalnumber"]),
                'poi_id'          => @$booking["droptrain"],
                'booking_id'      => @$id,
                'type'           => "dropoff"
            ]);
             }
              if(!empty($booking["address"])){
              $addressid = $this->bookings_model->addbookingrecord('vbs_booking_address',[
                'address'          =>  $this->cleanstr($booking["address"]),
                'otheraddress'     =>  $this->cleanstr($booking["otheraddress"]),
                'city'             =>  $this->cleanstr($booking["city"]),
                'zipcode'         =>   $this->cleanstr($booking["zipcode"]),
                'booking_id'       =>  @$id,
                'type'             =>  "pickup"
            ]);
             }
              if(!empty($booking["dropaddress"])){
              $dropaddressid = $this->bookings_model->addbookingrecord('vbs_booking_address',[
                'address'          =>  $this->cleanstr($booking["dropaddress"]),
                'otheraddress'     =>  $this->cleanstr($booking["dropotheraddress"]),
                'city'             =>  $this->cleanstr($booking["dropcity"]),
                'zipcode'          =>  $this->cleanstr($booking["dropzipcode"]),
                'booking_id'       =>  @$id,
                'type'             =>  "dropoff"
            ]);
             }
             if($this->session->userdata['passengers']){
              $passengers=$this->session->userdata['passengers'];
              foreach($passengers as $passenger){
                   $passengerid = $this->bookings_model->addbookingrecord('vbs_booking_passengers',[
                   'passenger_id'     => @$passenger["id"],
                   'booking_id '      => @$id
            ]);
              }
             }
              if($this->session->userdata['bookingpickregular']){
              $pickregular=$this->session->userdata['bookingpickregular'];
              foreach($pickregular as $regular){
                if($regular["type"]==1){
                  $regularid = $this->bookings_model->addbookingrecord('vbs_booking_regschedule',[
                   'day'             => @$regular["day"],
                   'time'            => @$regular["time"],
                   'type'            => "regular",
                   'booking_id'     => @$id
            ]);
                }
                   
              }
             }
              if($this->session->userdata['bookingreturnregular']){
              $pickreturn=$this->session->userdata['bookingreturnregular'];
              foreach($pickreturn as $return){
                if($return["type"]==1){
                  $returnid = $this->bookings_model->addbookingrecord('vbs_booking_regschedule',[
                   'day'         => @$return["day"],
                   'time'        => @$return["time"],
                   'type'        => "return",
                   'booking_id'  => @$id
            ]);
                }
                   
              }
             }
               if($paymentstatus=="1"){ 
                  if($methodstr == "creditcard"){
                    $creditcardid = $this->bookings_model->addbookingrecord('vbs_booking_creditcard',[
                   'name_credit'         =>  $this->cleanstr($_POST["name_credit"]),
                   'card_num_credit'     =>  $this->cleanstr($_POST["card_num_credit"]),
                   'exp_month_credit'    =>  $this->cleanstr($_POST["exp_month_credit"]),
                   'exp_year_credit'     =>  $this->cleanstr($_POST["exp_year_credit"]),
                   'cvc_credit'          =>  $this->cleanstr($_POST["cvc_credit"]),
                   'booking_id'         =>   @$id
                    ]);
                    }
                  elseif($methodstr == "bankdirectdebit"){
                    $directdebitid = $this->bookings_model->addbookingrecord('vbs_booking_directdebit',[
                   'civility_debit '     => $this->cleanstr($_POST["civility_debit"]),
                   'fname_debit'         => $this->cleanstr($_POST["fname_debit"]),
                   'lname_debit'         => $this->cleanstr($_POST["lname_debit"]),
                   'bankname_debit'      => $this->cleanstr($_POST["bankname_debit"]),
                   'agencyname_debit'    => $this->cleanstr($_POST["agencyname_debit"]),
                   'agencyaddr_debit'    => $this->cleanstr($_POST["agencyaddr_debit"]),
                   'otheraddr_debit'     => $this->cleanstr($_POST["otheraddr_debit"]),
                   'agencyzipcode_debit' => $this->cleanstr($_POST["agencyzipcode_debit"]),
                   'agencycity_debit'    => $this->cleanstr($_POST["agencycity_debit"]),
                   'ibnnum_debit'        => $this->cleanstr($_POST["ibnnum_debit"]),
                   'swiftcode_debit'     => $this->cleanstr($_POST["swiftcode_debit"]),
                   'booking_id'          => @$id
                    ]);
                   }
                 }
               
                 $this->session->set_flashdata('alert', [
                        'message' => "! Booking was completed and we will review it soon.",
                        'class' => "alert-success",
                        'type' => "Congratulation"
                    ]);
                 //removed session
                 if($this->session->userdata['passengers']){
                     $this->session->unset_userdata('passengers');
                  }
                   if($this->session->userdata['bookingdata']){
                     $this->session->unset_userdata('bookingdata');
                  }
                   if($this->session->userdata['bookingpickregular']){
                     $this->session->unset_userdata('bookingpickregular');
                  }
                   if($this->session->userdata['bookingreturnregular']){
                     $this->session->unset_userdata('bookingreturnregular');
                  }
                   if($this->session->userdata['mapdatainfo']){
                     $this->session->unset_userdata('mapdatainfo');
                  }
                  if($this->session->userdata['bookingextravalues']){
                     $this->session->unset_userdata('bookingextravalues');
                  }
                   if($this->session->userdata['stops']){
                     $this->session->unset_userdata('stops');
                  }
                 //removed session
                 return redirect("client/bookings");
         
          }
      } else{
          $this->session->set_flashdata('alert', [
                        'message' => " :something went wrong. please try again",
                        'class' => "alert-danger",
                        'type' => "Error"
                    ]);
            return redirect("client/bookings");
      }
    
      }
      else{
               $this->session->set_flashdata('alert', [
                        'message' => " :something is wrong. please try again",
                        'class' => "alert-danger",
                        'type' => "Error"
                    ]);
            return redirect("client/bookings");
      }
     
    }

function get_other_coordinates($pickupaddress,$dropoffaddress){
 //replace string
 $pickupaddress=str_replace(", ,", "+", $pickupaddress);
 $pickupaddress=str_replace(" , ", "+", $pickupaddress);
 $pickupaddress=str_replace(", ", "+", $pickupaddress);
 $pickupaddress=str_replace(" ,", "+", $pickupaddress);
 $pickupaddress=str_replace(",", "+", $pickupaddress);
 $pickupaddress=str_replace(" - ", "+", $pickupaddress);
 $pickupaddress=str_replace("- ", "+", $pickupaddress);
 $pickupaddress=str_replace(" -", "+", $pickupaddress);
 $pickupaddress=str_replace("-", "+", $pickupaddress);
 $pickupaddress=str_replace(" ", "+", $pickupaddress);

 
 $dropoffaddress=str_replace(", ,", "+", $dropoffaddress);
 $dropoffaddress=str_replace(" , ", "+", $dropoffaddress);
 $dropoffaddress=str_replace(", ", "+", $dropoffaddress);
 $dropoffaddress=str_replace(" ,", "+", $dropoffaddress);
 $dropoffaddress=str_replace(",", "+", $dropoffaddress);
 $dropoffaddress=str_replace(" - ", "+", $dropoffaddress);
 $dropoffaddress=str_replace("- ", "+", $dropoffaddress);
 $dropoffaddress=str_replace(" -", "+", $dropoffaddress);
 $dropoffaddress=str_replace("-", "+", $dropoffaddress);
 $dropoffaddress=str_replace(" ", "+", $dropoffaddress);

//replace string

$googlekey=$this->bookings_model->getgoogleapikey("vbs_googleapi");

  $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".$pickupaddress."&destinations=".$dropoffaddress."&key=".$googlekey->api_key;
  //echo $url;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch,CURLOPT_POST,false);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
      
    if($response_a != null){

      $status= $response_a['rows'][0]['elements'][0]['status'];
         if ( $status == 'ZERO_RESULTS' ){
          return false;
         }else{
           $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
            $time = $response_a['rows'][0]['elements'][0]['duration']['text'];
             $seconds = $response_a['rows'][0]['elements'][0]['duration']['value'];
             $minutes=round($seconds/60);
            
            return array('distance'=>$dist,'time'=>$time,'minutes'=>$minutes);
         }

     
    }else{
      return false;
    }
    
}

 function get_coordinates($pickupaddress,$dropoffaddress,$bookingstops)
{
  $googlekey=$this->bookings_model->getgoogleapikey("vbs_googleapi");  
  $stopcounts=count($bookingstops);

  if($stopcounts > 0){
   
    $dist;
    $time;
    $seconds;
    $minutes;
  //pickupaddress to first stoper
 
    $pickupaddress=str_replace(", ,", "+", $pickupaddress);
    $pickupaddress=str_replace(" , ", "+", $pickupaddress);
    $pickupaddress=str_replace(", ", "+", $pickupaddress);
    $pickupaddress=str_replace(" ,", "+", $pickupaddress);
    $pickupaddress=str_replace(",", "+", $pickupaddress);
    $pickupaddress=str_replace(" - ", "+", $pickupaddress);
    $pickupaddress=str_replace("- ", "+", $pickupaddress);
    $pickupaddress=str_replace(" -", "+", $pickupaddress);
    $pickupaddress=str_replace("-", "+", $pickupaddress);
    $pickupaddress=str_replace(" ", "+", $pickupaddress);

 //replace string
   $stopaddress=str_replace(", ,", "+", $bookingstops[0]['stopaddress']);
   $stopaddress=str_replace(" , ", "+", $bookingstops[0]['stopaddress']);
   $stopaddress=str_replace(", ", "+",  $bookingstops[0]['stopaddress']);
   $stopaddress=str_replace(" ,", "+",  $bookingstops[0]['stopaddress']);
   $stopaddress=str_replace(",", "+",   $bookingstops[0]['stopaddress']);
   $stopaddress=str_replace(" - ", "+", $bookingstops[0]['stopaddress']);
   $stopaddress=str_replace("- ", "+",  $bookingstops[0]['stopaddress']);
   $stopaddress=str_replace(" -", "+",  $bookingstops[0]['stopaddress']);
   $stopaddress=str_replace("-", "+",   $bookingstops[0]['stopaddress']);
   $stopaddress=str_replace(" ", "+",   $bookingstops[0]['stopaddress']);
    
   $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".$pickupaddress."&destinations=".$stopaddress."&key=".$googlekey->api_key;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch,CURLOPT_POST,false);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
      
    if($response_a != null){

      $status= $response_a['rows'][0]['elements'][0]['status'];
         if ( $status == 'ZERO_RESULTS' ){
          return false;
         }
         else{
            $dist     = $response_a['rows'][0]['elements'][0]['distance']['text'];
            $time     = $response_a['rows'][0]['elements'][0]['duration']['text'];
            $seconds  = $response_a['rows'][0]['elements'][0]['duration']['value'];
            $minutes  = round($seconds/60);
         }

     
    }else{
      return false;
    }
 
  //pickupaaddress to first stoper

  //first stoper to next stoper
    if($stopcounts > 1){
    for($i=0;$i < ($stopcounts-1);$i++){
     
    $stopaddress1=str_replace(", ,", "+", $bookingstops[$i]['stopaddress']);
    $stopaddress1=str_replace(" , ", "+", $bookingstops[$i]['stopaddress']);
    $stopaddress1=str_replace(", ", "+", $bookingstops[$i]['stopaddress']);
    $stopaddress1=str_replace(" ,", "+", $bookingstops[$i]['stopaddress']);
    $stopaddress1=str_replace(",", "+", $bookingstops[$i]['stopaddress']);
    $stopaddress1=str_replace(" - ", "+", $bookingstops[$i]['stopaddress']);
    $stopaddress1=str_replace("- ", "+", $bookingstops[$i]['stopaddress']);
    $stopaddress1=str_replace(" -", "+", $bookingstops[$i]['stopaddress']);
    $stopaddress1=str_replace("-", "+", $bookingstops[$i]['stopaddress']);
    $stopaddress1=str_replace(" ", "+", $bookingstops[$i]['stopaddress']);

 //replace string
   $stopaddress2=str_replace(", ,", "+", $bookingstops[$i+1]['stopaddress']);
   $stopaddress2=str_replace(" , ", "+", $bookingstops[$i+1]['stopaddress']);
   $stopaddress2=str_replace(", ", "+",  $bookingstops[$i+1]['stopaddress']);
   $stopaddress2=str_replace(" ,", "+",  $bookingstops[$i+1]['stopaddress']);
   $stopaddress2=str_replace(",", "+",   $bookingstops[$i+1]['stopaddress']);
   $stopaddress2=str_replace(" - ", "+", $bookingstops[$i+1]['stopaddress']);
   $stopaddress2=str_replace("- ", "+",  $bookingstops[$i+1]['stopaddress']);
   $stopaddress2=str_replace(" -", "+",  $bookingstops[$i+1]['stopaddress']);
   $stopaddress2=str_replace("-", "+",   $bookingstops[$i+1]['stopaddress']);
   $stopaddress2=str_replace(" ", "+",   $bookingstops[$i+1]['stopaddress']);
    
   $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".$stopaddress1."&destinations=".$stopaddress2."&key=".$googlekey->api_key;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch,CURLOPT_POST,false);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
      
    if($response_a != null){

      $status= $response_a['rows'][0]['elements'][0]['status'];
         if ( $status == 'ZERO_RESULTS' ){
          return false;
         }
         else{
            $dist     += $response_a['rows'][0]['elements'][0]['distance']['text'];
            $time     = $response_a['rows'][0]['elements'][0]['duration']['text'];
            $seconds  = $response_a['rows'][0]['elements'][0]['duration']['value'];
            $minutes  += round($seconds/60);
         }

     
    }else{
      return false;
    }
   
  } 
}
  //first stoper to next stoper

 //last stoper to dropoff
    $dropoffaddress=str_replace(", ,", "+", $dropoffaddress);
    $dropoffaddress=str_replace(" , ", "+", $dropoffaddress);
    $dropoffaddress=str_replace(", ", "+", $dropoffaddress);
    $dropoffaddress=str_replace(" ,", "+", $dropoffaddress);
    $dropoffaddress=str_replace(",", "+", $dropoffaddress);
    $dropoffaddress=str_replace(" - ", "+", $dropoffaddress);
    $dropoffaddress=str_replace("- ", "+", $dropoffaddress);
    $dropoffaddress=str_replace(" -", "+", $dropoffaddress);
    $dropoffaddress=str_replace("-", "+", $dropoffaddress);
    $dropoffaddress=str_replace(" ", "+", $dropoffaddress);

 //replace string
   $stopaddress=str_replace(", ,", "+", $bookingstops[$stopcounts-1]['stopaddress']);
   $stopaddress=str_replace(" , ", "+", $bookingstops[$stopcounts-1]['stopaddress']);
   $stopaddress=str_replace(", ", "+",  $bookingstops[$stopcounts-1]['stopaddress']);
   $stopaddress=str_replace(" ,", "+",  $bookingstops[$stopcounts-1]['stopaddress']);
   $stopaddress=str_replace(",", "+",   $bookingstops[$stopcounts-1]['stopaddress']);
   $stopaddress=str_replace(" - ", "+", $bookingstops[$stopcounts-1]['stopaddress']);
   $stopaddress=str_replace("- ", "+",  $bookingstops[$stopcounts-1]['stopaddress']);
   $stopaddress=str_replace(" -", "+",  $bookingstops[$stopcounts-1]['stopaddress']);
   $stopaddress=str_replace("-", "+",   $bookingstops[$stopcounts-1]['stopaddress']);
   $stopaddress=str_replace(" ", "+",   $bookingstops[$stopcounts-1]['stopaddress']);
    
   $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".$stopaddress."&destinations=".$dropoffaddress."&key=".$googlekey->api_key;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch,CURLOPT_POST,false);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
      
    if($response_a != null){

      $status= $response_a['rows'][0]['elements'][0]['status'];
         if ( $status == 'ZERO_RESULTS' ){
          return false;
         }
         else{
            $dist     += $response_a['rows'][0]['elements'][0]['distance']['text'];
            $time     = $response_a['rows'][0]['elements'][0]['duration']['text'];
            $seconds  = $response_a['rows'][0]['elements'][0]['duration']['value'];
            $minutes  += round($seconds/60);
         }

     
    }else{
      return false;
    }
 //last stoper to dropoff
  return array('distance'=>$dist,'time'=>$time,'minutes'=>$minutes);
 }
 else{

  //if stoper is null
  //replace string
 $pickupaddress=str_replace(", ,", "+", $pickupaddress);
 $pickupaddress=str_replace(" , ", "+", $pickupaddress);
 $pickupaddress=str_replace(", ", "+", $pickupaddress);
 $pickupaddress=str_replace(" ,", "+", $pickupaddress);
 $pickupaddress=str_replace(",", "+", $pickupaddress);
 $pickupaddress=str_replace(" - ", "+", $pickupaddress);
 $pickupaddress=str_replace("- ", "+", $pickupaddress);
 $pickupaddress=str_replace(" -", "+", $pickupaddress);
 $pickupaddress=str_replace("-", "+", $pickupaddress);
 $pickupaddress=str_replace(" ", "+", $pickupaddress);

 
 $dropoffaddress=str_replace(", ,", "+", $dropoffaddress);
 $dropoffaddress=str_replace(" , ", "+", $dropoffaddress);
 $dropoffaddress=str_replace(", ", "+", $dropoffaddress);
 $dropoffaddress=str_replace(" ,", "+", $dropoffaddress);
 $dropoffaddress=str_replace(",", "+", $dropoffaddress);
 $dropoffaddress=str_replace(" - ", "+", $dropoffaddress);
 $dropoffaddress=str_replace("- ", "+", $dropoffaddress);
 $dropoffaddress=str_replace(" -", "+", $dropoffaddress);
 $dropoffaddress=str_replace("-", "+", $dropoffaddress);
 $dropoffaddress=str_replace(" ", "+", $dropoffaddress);

//replace string

$googlekey=$this->bookings_model->getgoogleapikey("vbs_googleapi");

  $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".$pickupaddress."&destinations=".$dropoffaddress."&key=".$googlekey->api_key;
  //echo $url;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch,CURLOPT_POST,false);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
      
    if($response_a != null){

      $status= $response_a['rows'][0]['elements'][0]['status'];
         if ( $status == 'ZERO_RESULTS' ){
          return false;
         }else{
          
           $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
            $time = $response_a['rows'][0]['elements'][0]['duration']['text'];
            $seconds = $response_a['rows'][0]['elements'][0]['duration']['value'];
            $minutes=round($seconds/60);
               
        
            return array('distance'=>$dist,'time'=>$time,'minutes'=>$minutes);

         }

     
    }else{
      return false;
    }
   
  //if stoper is null
 }

    
}
public function getbookingservices(){
    $id=(int)$_GET['category_id'];

    if($id>0){
       $servicedata = $this->bookings_model->get_services('vbs_u_service',['service_category' => $id]);
       
       $result='<option value="">Service</option>';
               foreach($servicedata as $key => $item){
                $result.='<option  value="'.$item->id.'">'.$item->service_name.'</option>';
             }
      }else{
       $result='<option value="">Service</option>';
    }
    echo $result;
    exit;      
}
public function getpoidatabypackage(){
    $id=(int)$_GET['package_id'];

    if($id>0){
      
       $packagedata=$this->bookings_model->poidatarecord('vbs_u_package',$id);
       $poirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$packagedata['departure']);
       $poicategoryrecord=$this->bookings_model->poidatarecord('vbs_u_ride_category',$poirecord['category_id']);

        $destinationpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$packagedata['destination']);
       $destinationpoicategoryrecord=$this->bookings_model->poidatarecord('vbs_u_ride_category',$destinationpoirecord['category_id']);


       if($poirecord && $poicategoryrecord){
         $data = ['poiname'=>$poirecord['name'],'poiaddress'=>$poirecord['address'],'zipcode'=>$poirecord['postal'],'city'=>$poirecord['ville'],'poicategoryname'=>$poicategoryrecord['ride_cat_name'],'destinationpoiname'=>$destinationpoirecord['name'],'destinationpoiaddress'=>$destinationpoirecord['address'],'destinationzipcode'=>$destinationpoirecord['postal'],'destinationcity'=>$destinationpoirecord['ville'],'destinationpoicategoryname'=>$destinationpoicategoryrecord['ride_cat_name'],'result'=>"200"];
            header('Content-Type: application/json'); 
            echo json_encode($data);  
             
       }else{
            $data = ['result'=>"201"];
            header('Content-Type: application/json'); 
            echo json_encode($data);    
       }
       
      
    }else{
            $data = ['result'=>"201"];
            header('Content-Type: application/json'); 
            echo json_encode($data);    
    }
     
}
public function getpoidatabycategory(){
    $id=(int)$_GET['poi_id'];

    if($id>0){
      
      
       $poirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$id);
     
       if($poirecord){
         $data = ['address'=>$poirecord['address'],'zipcode'=>$poirecord['postal'],'city'=>$poirecord['ville'],'result'=>"200"];
            header('Content-Type: application/json'); 
            echo json_encode($data);  
             
       }else{
            $data = ['result'=>"201"];
            header('Content-Type: application/json'); 
            echo json_encode($data);    
       }
       
      
    }else{
            $data = ['result'=>"201"];
            header('Content-Type: application/json'); 
            echo json_encode($data);    
    }
     
}
public function getdroppoidatabypackage(){
    $id=(int)$_GET['package_id'];

    if($id>0){
      
       $packagedata=$this->bookings_model->poidatarecord('vbs_u_package',$id);
       $poirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$packagedata['destination']);
       $poicategoryrecord=$this->bookings_model->poidatarecord('vbs_u_ride_category',$poirecord['category_id']);
       if($poirecord && $poicategoryrecord){
         $data = ['poiname'=>$poirecord['name'],'poiaddress'=>$poirecord['address'],'zipcode'=>$poirecord['postal'],'city'=>$poirecord['ville'],'poicategoryname'=>$poicategoryrecord['ride_cat_name'],'result'=>"200"];
            header('Content-Type: application/json'); 
            echo json_encode($data);  
             
       }else{
            $data = ['result'=>"201"];
            header('Content-Type: application/json'); 
            echo json_encode($data);    
       }
       
      
    }else{
            $data = ['result'=>"201"];
            header('Content-Type: application/json'); 
            echo json_encode($data);    
    }
     
}
    public function getpromodiscount(){

      $promo_code=$_GET['promo_code'];
     // $id=$this->input->post("passengerfield");
      if(!empty($promo_code) ||  $promo_code!=""){
          $promocodediscount=$this->bookings_model->getpromocodediscount($promo_code);
           
          if($promocodediscount){

       
         if($this->session->userdata['mapdatainfo']){
           $getmapdata['mapdatainfo']=$this->session->userdata['mapdatainfo'];
           $totalprice=$getmapdata['mapdatainfo']['totalprice']-($getmapdata['mapdatainfo']['price'] * $promocodediscount/100);
           $totaldiscount=$getmapdata['mapdatainfo']['totaldiscount'] +   $promocodediscount;
           $promocodediscountfee=($getmapdata['mapdatainfo']['price'] * $promocodediscount/100);
              $totalprice=round($totalprice, 2);
              $promocodediscountfee=round($promocodediscountfee, 2);

            $bookingstore['mapdatainfo']=array('distance'=> $getmapdata['mapdatainfo']['distance'],'time'=>$getmapdata['mapdatainfo']['time'],'price'=>$getmapdata['mapdatainfo']['price'],'totalprice'=>$totalprice, 'nighttimefee'=>$getmapdata['mapdatainfo']['nighttimefee'],'notworkingdayfee'=>$getmapdata['mapdatainfo']['notworkingdayfee'],'maydecdayfee'=>$getmapdata['mapdatainfo']['maydecdayfee'],
              'nighttimediscount'=>$getmapdata['mapdatainfo']['nighttimediscount'],'notworkingdaydiscount'=>$getmapdata['mapdatainfo']['notworkingdaydiscount'],'maydecdaydiscount'=>$getmapdata['mapdatainfo']['maydecdaydiscount'],'pickupaddress'=>$getmapdata['mapdatainfo']['pickupaddress'],'dropoffaddress'=>$getmapdata['mapdatainfo']['dropoffaddress'],'welcomediscount'=>$getmapdata['mapdatainfo']['welcomediscount'],'rewarddiscount'=>$getmapdata['mapdatainfo']['rewarddiscount'],'perioddiscount'=>$getmapdata['mapdatainfo']['perioddiscount'],'promocodediscount'=>$promocodediscount,'totaldiscount'=> $totaldiscount,
              'welcomediscountfee'=>$getmapdata['mapdatainfo']['welcomediscountfee'], 'rewarddiscountfee'=>$getmapdata['mapdatainfo']['rewarddiscountfee'],'perioddiscountfee'=>$getmapdata['mapdatainfo']['perioddiscountfee'],'promocodediscountfee'=>$promocodediscountfee,'vatname'=>$getmapdata['mapdatainfo']['vatname'],'vatdiscount'=>$getmapdata['mapdatainfo']['vatdiscount'],'vatfee'=>$getmapdata['mapdatainfo']['vatfee'],'drivermealfee'=>$getmapdata['mapdatainfo']['drivermealfee'],'driverrestfee'=>$getmapdata['mapdatainfo']['driverrestfee']);
        



                 $this->session->set_userdata($bookingstore);
                   $record=$this->session->userdata['mapdatainfo'];
                    $data = ["discount" => $record,'result'=>"200"];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
         
        }
      else{
           
            $data = ['result'=>"201"];
            header('Content-Type: application/json'); 
            echo json_encode($data);     
      }

     
      }
      else{

           
            $data = ['result'=>"202"];
            header('Content-Type: application/json'); 
            echo json_encode($data);
      }
    


   
    } else{
           
            $data = ['result'=>"203"];
            header('Content-Type: application/json'); 
            echo json_encode($data);
      }

      }
     
function isWeekend($date) {
  
   $date=str_replace('/', '-', $date);
    $weekDay = date('w', strtotime($date));
   
     if ($weekDay == 0){
      return true;
    }
    else{
      return false;
    }
}
  public function add_client_passenger(){
      $client=$this->session->userdata['user'];
    if($client){
           $table='vbs_passengers';
           $id = $this->passenger_model->passengers_Add($table,[
            'civility'    => @$_GET['civility'],
            'fname'     => @$_GET['fname'],
            'lname'     => @$_GET['lname'],
            'mobilephone'     => @$_GET['mobile'],
            'homephone'     => @$_GET['home'],
            'disablecatid' => @$_GET['disable'],
            'created_at'    => @date('Y-m-d'),
            'time'    => @date("H : i"),
            'clientid'    => $client->id,
          ]);
           if ($id) {
            $passengers=$this->bookings_model->booking_passengers('vbs_passengers',$client->id);
             $data = ['result'=>"200",'passengers'=>$passengers];
             header('Content-Type: application/json'); 
             echo json_encode($data);

          }else{
             $data = ['result'=>"201"];
             header('Content-Type: application/json'); 
             echo json_encode($data);
          }
    }else{

            $data = ['result'=>"201"];
            header('Content-Type: application/json'); 
            echo json_encode($data);
           
       }
 }
 public function add_client_aspassenger(){
    $client=$this->session->userdata['user'];
    if($client){
            $clientdata=array(
              'civility'=>$client->civility,
              'f_name'=>$client->first_name,
              'l_name'=>$client->last_name,
              'mobile'=>$client->mobile,
              'phone'=>$client->phone
            );
             $data = ['result'=>"200",'client'=>$clientdata];
             header('Content-Type: application/json'); 
             echo json_encode($data);

  }else{

            $data = ['result'=>"201"];
            header('Content-Type: application/json'); 
            echo json_encode($data);
           
       }
 }
 public function cleanstr($str){
  $str = preg_replace("/[^A-Za-z0-9 ]/", '', $str);
  return $str;
 }
}
