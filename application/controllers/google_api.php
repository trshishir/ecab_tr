<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Google_api extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('cms_model');

        if (!$this->basic_auth->is_login())
            redirect("admin", 'refresh');
        else
            $this->data['user'] = $this->basic_auth->user();
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('general');
        $this->load->helper('language');
        $this->load->library('session');
        $this->load->model('google_api_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');
        $this->load->model('support_model');
        $this->load->model('calls_model');
        $this->load->model('notes_model');

        $this->data['gmaps'] = false;
        $this->data['title'] = "Google API";
        $this->data['css_type'] = array("form", "datatable");
        $this->data['user'] = $this->session->userdata('user');
    }

    public function index() {

        $googleAPI = $this->data['google_api'] = $this->google_api_model->getFirst();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $permissions = json_encode(googlePermission(@$_POST['permission']));
            $insert = [
                'api_key' 		=> @$_POST['api_key'],
                'status' 		=> @$_POST['status'],
                'country_id' 	=> @$_POST['country_id'],
                'permission' 	=> $permissions
            ];

            if(!empty($googleAPI)){
                $update = $this->db->update('vbs_googleapi',$insert, ['id' => $googleAPI->id]);
            } else {
                $update = $this->db->insert('vbs_googleapi', $insert);
            }
            //var_dump($this->db->last_query());exit;
            $this->session->set_flashdata('alert', [
                'message' => ": Successfully updated Google Api.",
                'class' => "alert-success",
                'status_id' => "10",
                'type' => "Success"
            ]);
            redirect('admin/google_api');
        }
        $this->data['countries'] = $this->cms_model->get_all_countries();
        $googleAPI->permission = googlePermission(@$googleAPI->permission);
        $this->data['google_api'] = $googleAPI;
        //var_dump($googleAPI);exit;
        $this->data['content'] = 'admin/google_api';
        //var_dump($this->data);
        $this->_render_page('templates/admin_template', $this->data);
    }

}