<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Affiliates extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('general');
        // $this->load->helper('language');
        if (!$this->basic_auth->is_login())
            redirect("admin", 'refresh');
        else
            $this->data['user'] = $this->basic_auth->user();
        $this->load->model('invoice_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');
        $this->load->model('support_model');
        $this->load->model('calls_model');
        $this->load->model('notes_model');
        $this->load->library('session');
        $this->load->model('user_model');
        $this->load->model('notifications_model');
        $this->load->model('cars_model');

        $this->data['configuration'] = get_configuration();
    }

    public function index() {
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "affiliates";
        $this->data['gmaps'] = false;
        $this->data['title'] = 'Affiliates';
        $this->data['show_subtitle'] = false;
        $this->data['subtitle'] = 'Add Affiliate';
        $this->data['title_link'] = base_url('admin/affiliates');
        $this->data['content'] = 'admin/affiliates/index';

        $this->load->model('calls_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');

        $data = [];
        $data['request'] = $this->request_model->getAll();
        $data['jobs'] = $this->jobs_model->getAll();
        $data['calls'] = $this->calls_model->getAll();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $searchFilter['search_name'] = $_POST['search_name'];
            $searchFilter['search_email'] = $_POST['search_email'];
            $searchFilter['search_phone'] = $_POST['search_phone'];
            $searchFilter['date_from'] = $_POST['date_from'];
            $searchFilter['date_to'] = $_POST['date_to'];
            $searchFilter['status'] = $_POST['status'];
            $data = $this->user_model->searchAffiliate($searchFilter);
        }
        else {
            $data = $this->user_model->getAllAffiliates();
        }
        
        $results = array();
        foreach ($data as $key => $value) {
            $status = ($value['active'] == 1) ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Inactive</span>';
            $checkbox = "";
            $registered_date = date('d/m/Y', strtotime($value['created_at']));
            $registered_time = date('H:i:s', strtotime($value['created_at']));
            $since = timeDiff($value['created_at']);
            $edit_link = "<a href='" . base_url() . 'admin/affiliates/edit/' . $value['id'] . "'" . ">" . create_timestamp_uid($value['created_at'],$value['id']) . "</a>";
            if ($value['company_name'] != "") {
                $name = $value['company_name'];
            }
            else {
                $name = $value['civility'] . ' ' . $value['first_name'] . ' ' . $value['last_name'];
            }
            $results[$key] = array(
                "checkbox" => $checkbox,
                "id" => $value['id'],
                "link" => $edit_link,
                "date" => $registered_date,
                "time" => $registered_time,
                "name" => $name,
                "email" => $value['email'],
                "phone" => $value['phone'],
                "website" => '-',
                "commissions" => '-',
                "payout" => '-',
                "due" => '-',
                "status" => $status,
                "since" => $since
            );
        } // /foreach
        $this->data['data'] = $results;
        $this->_render_page('templates/admin_template', $this->data);
    }

    public function fetchAffiliatesData() {
        $result = array('data' => array());

        $data = $this->user_model->getAllAffiliates();
        foreach ($data as $key => $value) {

            $status = ($value['active'] == 1) ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Inactive</span>';
            $checkbox = "<input type='checkbox' name='affiliates[]' class='ccheckbox' value='" . $value['id'] . "' />";
            $registered_date = date('d/m/Y', strtotime($value['created_at']));
            $registered_time = date('H:i:s', strtotime($value['created_at']));
            $since = timeDiff($value['created_at']);
            $edit_link = "<a href='" . base_url() . 'admin/affiliates/edit/' . $value['id'] . "'" . ">" . create_timestamp_uid($value['created_at'],$value['id']) . "</a>";
            $result['data'][$key] = array(
                $checkbox,
                $edit_link,
                $registered_date,
                $registered_time,
                $value['civility'] . ' ' . $value['first_name'] . ' ' . $value['last_name'],
                $value['email'],
                $value['phone'],
                $status,
                $since
            );
        } // /foreach

        echo json_encode($result);
    }

    public function affiliateSearchData() {
        //if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['search'])) {
        $result = array('data' => array());
        //echo "<PRE>";print_r($_POST);echo "</PRE>";
        $searchFilter['search_name'] = $_POST['search_name'];
        $searchFilter['search_email'] = $_POST['search_email'];
        $searchFilter['search_phone'] = $_POST['search_phone'];
        $searchFilter['date_from'] = $_POST['date_from'];
        $searchFilter['date_to'] = $_POST['date_to'];
        $searchFilter['status'] = $_POST['status'];
        $data = $this->user_model->searchAffiliate($searchFilter);
        if (!empty($data)) {
            foreach ($data as $key => $value) {

                $status = ($value['active'] == 1) ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Inactive</span>';
                $checkbox = "<input type='checkbox' name='affiliates[]' class='ccheckbox' value='" . $value['id'] . "' />";
                $registered_date = date('d/m/Y', strtotime($value['created_at']));
                $registered_time = date('H:i:s', strtotime($value['created_at']));
                $since = timeDiff($value['created_at']);
                $edit_link = "<a href='" . base_url() . 'admin/affiliates/edit/' . $value['id'] . "'" . ">" . create_timestamp_uid($value['created_at'],$value['id']) . "</a>";
                $result['data'][$key] = array(
                    $checkbox,
                    $edit_link,
                    $registered_date,
                    $registered_time,
                    $value['civility'] . ' ' . $value['first_name'] . ' ' . $value['last_name'],
                    $value['email'],
                    $value['phone'],
                    '-',
                    '-',
                    '-',
                    '-',
                    $status,
                    $since
                );
            } // /foreach
        }
        echo json_encode($result);
        //}
    }
    
    public function add() {
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "affiliates";
        $this->data['gmaps'] = false;
        $this->data['title'] = 'Affiliates';
        $this->data['show_subtitle'] = false;
        $this->data['subtitle'] = 'Add Affiliate';
        $this->data['title_link'] = base_url('admin/affiliates');
        
        $this->data['company'] = array('name' => 'company', 'class' => 'user form-control', 'tabindex' => '2', 'placeholder' => 'Company', 'id' => 'company', 'type' => 'text', 'maxlength' => '150', 'value' => $this->form_validation->set_value('company'));
        $this->data['civility'] = array('name' => 'civility', 'class' => 'user form-control', 'tabindex' => '3', 'placeholder' => 'Civility', 'id' => 'civility', 'type' => 'text', 'value' => $this->form_validation->set_value('civility'));
        $this->data['first_name'] = array('name' => 'first_name', 'class' => 'user form-control', 'tabindex' => '4', 'placeholder' => 'First name', 'id' => 'first_name', 'type' => 'text', 'value' => $this->form_validation->set_value('first_name'));
        $this->data['last_name'] = array('name' => 'last_name', 'class' => 'user form-control', 'tabindex' => '5', 'placeholder' => 'Last name', 'id' => 'last_name', 'type' => 'text', 'value' => $this->form_validation->set_value('last_name'));
        $this->data['email'] = array('name' => 'email', 'class' => 'user-name form-control', 'tabindex' => '6', 'placeholder' => 'User Email', 'id' => 'email', 'type' => 'text', 'value' => $this->form_validation->set_value('email'));
        $this->data['phone'] = array('name' => 'phone', 'class' => 'phone1 form-control', 'tabindex' => '7', 'placeholder' => 'phone', 'id' => 'phone', 'type' => 'text', 'maxlength' => '11', 'value' => $this->form_validation->set_value('phone'));
        $this->data['address'] = array('name' => 'address', 'class' => 'user form-control', 'tabindex' => '8', 'placeholder' => 'adresse', 'rows' => '1', 'cols' => '40', 'id' => 'address', 'type' => 'text', 'value' => $this->form_validation->set_value('address'));
        $this->data['address1'] = array('name' => 'address1', 'class' => 'user form-control', 'tabindex' => '9', 'placeholder' => '', 'rows' => '1', 'cols' => '40', 'id' => 'address1', 'type' => 'text', 'value' => $this->form_validation->set_value('address'));
        $this->data['city'] = array('name' => 'city', 'class' => 'user form-control', 'tabindex' => '10', 'placeholder' => 'City', 'id' => 'city', 'type' => 'text', 'value' => $this->form_validation->set_value('city'));
        $this->data['zipcode'] = array('name' => 'zipcode', 'class' => 'user form-control', 'tabindex' => '11', 'placeholder' => 'Zipcode', 'id' => 'zipcode', 'type' => 'text', 'value' => $this->form_validation->set_value('zipcode'));
        $this->data['mobile'] = array('name' => 'mobile', 'class' => 'phone1 form-control', 'tabindex' => '12', 'placeholder' => 'Mobile', 'id' => 'mobile', 'type' => 'text', 'maxlength' => '15', 'value' => $this->form_validation->set_value('mobile'));
        $this->data['fax'] = array('name' => 'fax', 'class' => 'phone1 form-control', 'tabindex' => '13', 'placeholder' => 'Fax', 'id' => 'fax', 'type' => 'text', 'maxlength' => '15', 'value' => $this->form_validation->set_value('fax'));
        $this->data['password'] = array('name' => 'password', 'class' => 'password form-control', 'tabindex' => '14', 'placeholder' => 'Password', 'id' => 'password', 'type' => 'password', 'value' => $this->form_validation->set_value('password'));
        $this->data['confirm_password'] = array('name' => 'confirm_password', 'class' => 'password form-control', 'tabindex' => '15', 'placeholder' => 'Confirm Password', 'id' => 'confirm_password', 'type' => 'password', 'value' => $this->form_validation->set_value('confirm_password'));
        $this->data['content'] = 'admin/affiliates/add';
        
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // validate form input
            $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required|xss_clean');
            $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required|xss_clean');
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[vbs_users.email]');
            $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone1_label'), 'xss_clean|integer|min_length[8]|max_length[10]');
            $this->form_validation->set_rules('mobile', $this->lang->line('create_user_validation_phone1_label'), 'required|xss_clean|integer|min_length[10]|max_length[10]');
            $this->form_validation->set_rules('address', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
            $this->form_validation->set_rules('address1', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
            $this->form_validation->set_rules('city', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
            $this->form_validation->set_rules('zipcode', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean|min_length[5]');
            $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']'); // |matches[confirm_password] 
            $this->load->library('form_validation');

            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            
            if ($this->form_validation->run() == true) {
                
                $username = $this->input->post('first_name') . '' . $this->input->post('last_name');
                $email = strtolower($this->input->post('email'));
                $password = $this->input->post('password');
                $user_role = 8;
                // $user_group = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('groups') . " WHERE name = 'clients'");
                 
                $group_id = array($this->user_model->getGroupID('affiliates')); // array( $user_group[0]->id);// 
                $additional_data = array(
                    'civility' => $this->input->post('civility'),
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'phone' => $this->input->post('phone'),
                    'city' => $this->input->post('city'),
                    'zipcode' => $this->input->post('zipcode'),
                    'address' => $this->input->post('address'),
                    'date_of_registration' => date('Y-m-d')
                );
                if ($this->form_validation->run() == true && $this->ion_auth->register($user_role, $username, $password, $email, $additional_data,$group_id)) {
                    $user_rec = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('users') . " WHERE email = '" . $email . "'");
                    $inputdata['user_id'] = $user_rec[0]->id;
                    $inputdata['address1'] = $this->input->post('address');
                    $inputdata['address2'] = $this->input->post('city');
                    $inputdata['fax_no'] = $this->input->post('fax');
                    $inputdata['company_name'] = $this->input->post('company');
                    $table_name = "users_details";

                    $status = $this->base_model->insert_operation($inputdata, $table_name);

                    // $user = $this->basic_auth->login($username, $password);

                    if ($status) {
                        // $this->session->set_flashdata('message', $this->ion_auth->messages());
                        // check to see if we are creating the user
                        // redirect them back to the admin page
                        $this->session->set_flashdata('messages', $this->ion_auth->messages());
                        redirect('admin/affiliates', 'refresh');
                    } else {
                        $this->session->set_flashdata('error', $this->ion_auth->messages());
                        $this->_render_page('templates/admin_template', $this->data);
                    }
                } else {
                    $this->session->set_flashdata('error', $this->ion_auth->messages());
                    $this->_render_page('templates/admin_template', $this->data);
                }
            }
        }
        else {
            $this->_render_page('templates/admin_template', $this->data);
        }
    }

}
