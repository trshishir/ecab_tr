<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discount extends MY_Controller
{
	function __construct()
	{
			parent::__construct();
		$this->load->library('form_validation');
	
		$this->load->helper('url');
		$this->lang->load('auth');
		$this->lang->load('general');
		$this->load->helper('language');
		$this->load->helper('validate');
		if (!$this->basic_auth->is_login())
			redirect("admin", 'refresh');
		else
		$this->data['user'] = $this->basic_auth->user();
		// $this->load->model('bookings_model');
		$this->load->model('bookings_config_model');
		$this->load->model('request_model');
		$this->load->model('jobs_model');
		$this->load->model('support_model');
		$this->load->model('calls_model');
		$this->load->model('notes_model');
		$this->load->model('notifications_model');
		$this->data['configuration'] = get_configuration();
        $this->load->model('discount_model');
     
	}
	public function index(){
        $this->data['user'] = $this->basic_auth->user();
		$this->data['css_type'] 	= array("form","booking_datatable","datatable");
        $this->data['active_class'] = "discount";
        $this->data['gmaps'] = false;
        $this->data['title'] = "Discounts";
        $this->data['title_link'] = base_url('admin/discounts.php');
        $this->data['content'] = 'admin/discount/index';

        $data = [];
        $data['jobs'] 	 = $this->jobs_model->getAll();
		$data['calls']   = $this->calls_model->getAll();
		$this->data['discount_data'] = $this->discount_model->getAllDiscount();
		$this->data['service_cat'] = $this->bookings_config_model->getAllData('vbs_u_category_service');
        $this->data['service'] = $this->bookings_config_model->getAllData('vbs_u_service');
		 $this->_render_page('templates/admin_template', $this->data);
	}
	public function discountAdd(){
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		$this->discountstore();
	}else show_404();
   }

   public function discountstore(){

		// $error = request_validate();
   	   $promocode=$_POST['promo_code'];
   	   if(!empty($promocode)){
   	    $checkpromocode=$this->discount_model->check_promo_code('vbs_u_discount',$promocode);
   	    if($checkpromocode){
            	$this->session->set_flashdata('alert', [
				'message' => ': Promo code is already exist! Please try again.',
				'class' => "alert-danger",
				'type' => "Error"
		    ]);
			redirect('admin/discounts');
   	     }
   	   }
   
		if (empty($error)) {
			         $discountdatefrom=$_POST['discountdatefrom'];
				   	 $discountdatefrom=str_replace('/', '-', $discountdatefrom);
					 $discountdatefrom=strtotime($discountdatefrom);
					 $datefrom = date('Y-m-d',$discountdatefrom);
                        
                     $discountdateto=$_POST['discountdateto'];
				   	 $discountdateto=str_replace('/', '-', $discountdateto);
					 $discountdateto=strtotime($discountdateto);
					 $dateto = date('Y-m-d',$discountdateto);
                    
					 $promocode=$_POST['promo_code'];
                    
					 

					$user = $this->basic_auth->user();
					$user_id=$user->id;
					$table='vbs_u_discount';
					$id = $this->discount_model->booking_Config_Add($table,[
						'name_of_discount' 		=> @$_POST['name_of_discount'],
						'discount' 		=> @$_POST['discount'],
						'discount_category' => @$_POST['discount_category'],
						'datefrom'=> $datefrom,
						'dateto'=> $dateto,
						'timefrom'=> @$_POST['discounttimefrom'],
						'timeto'=> @$_POST['discounttimeto'],
						'promo_code' => $promocode,
						'no_of_booking'=>@$_POST['no_of_booking'],
						'servicecategory_id'=>@$_POST['servicecategory_id'],
						'service_id'=>@$_POST['service_id'],
						'statut' 		=> @$_POST['discount_statut'],
						'created_at' 		=> @date('Y-m-d'),
						'time' 		=> @date("H : i"),
						'user_id' 		=> $user_id,
					]);

	
					$this->session->set_flashdata('alert', [
						'message' => ": Successfully added a discount .",
						'class' => "alert-success",
						'type' => "Success"
					]);
					redirect('admin/discounts');

		} else {
			$this->session->set_flashdata('alert', [
				'message' => ': Please try again',
				'class' => "alert-danger",
				'type' => "Error"
		    ]);
			redirect('admin/discounts');
		}
	}
	public function discountEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['discount_id'];
			if (!empty($id)) {
				  $promocode=$_POST['promo_code'];
				  if(!empty($promocode)){
				  	    $prevrecord=$this->discount_model->get_discount_record("vbs_u_discount",$id);
				  	    if($prevrecord->promo_code!=$promocode){
				  	     $checkpromocode=$this->discount_model->check_promo_code('vbs_u_discount',$promocode);
				   	     if($checkpromocode){
				            	$this->session->set_flashdata('alert', [
								'message' => ': Promo code is already exist! Please try again.',
								'class' => "alert-danger",
								'type' => "Error"
						    ]);
							redirect('admin/discounts');
				   	     }
				  	    }
				   	   
				   	   }
			     	 $discountdatefrom=$_POST['discountdatefrom'];
				   	 $discountdatefrom=str_replace('/', '-', $discountdatefrom);
					 $discountdatefrom=strtotime($discountdatefrom);
					 $datefrom = date('Y-m-d',$discountdatefrom);
                        
                     $discountdateto=$_POST['discountdateto'];
				   	 $discountdateto=str_replace('/', '-', $discountdateto);
					 $discountdateto=strtotime($discountdateto);
					 $dateto = date('Y-m-d',$discountdateto);

					 $promocode=$_POST['promo_code'];
					 $no_of_booking=$_POST['no_of_booking'];
					 if($_POST['discount_category']==0){
                        $promocode="";
                        $no_of_booking=0;   
					 }
					 elseif($_POST['discount_category']==1){
					 	  $promocode="";
                           $no_of_booking=$no_of_booking;
					 }
					 elseif($_POST['discount_category']==2){
					 	  $promocode="";
                          $no_of_booking=0; 
					 }
					 elseif($_POST['discount_category']==3){
					 	  $promocode=$promocode;
                          $no_of_booking=0; 
					 }	
			$table='vbs_u_discount';
			$update=$this->discount_model->booking_Config_Update($table,[
				'name_of_discount' 		=> @$_POST['name_of_discount'],
				'discount' 		=> @$_POST['discount'],
				'discount_category' => @$_POST['discount_category'],
				'datefrom'=> $datefrom,
				'dateto'=> $dateto,
				'timefrom'=> @$_POST['discounttimefrom'],
				'timeto'=> @$_POST['discounttimeto'],
				'promo_code' => $promocode,
				'no_of_booking'=>$no_of_booking,
				'servicecategory_id'=>@$_POST['servicecategory_id'],
				'service_id'=>@$_POST['service_id'],
				'statut' 		=> @$_POST['discount_statut'],
			], $id);
            
           




			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Discount.",
					'class' => "alert-success",
					'status_id' => "2",
					'type' => "Success"
				]);
				redirect('admin/discounts');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/discounts');
			}
		}
	}else show_404();

	}
	public function discountDelete(){
	$this->load->model('bookings_config_model');
	$id=$_POST['delet_discount_id'];
	$table = "vbs_u_discount";
	$del=$this->discount_model->booking_Config_Delete($table,$id);
	
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "2",
			'type' => "Success"
	]);
	redirect('admin/discounts');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "2",
				'type' => "Error"
		]);
		redirect('admin/discounts');
	}
	show_404();
	}

public function get_ajax_discount(){
		$id=(int)$_GET['discount_id'];

		if($id>0){
			$this->load->model('bookings_config_model');
			$discount_data = $this->discount_model->booking_Config_getAll('vbs_u_discount',['id' => $id]);
			$service_cat = $this->bookings_config_model->getAllData('vbs_u_category_service');
           
		
			foreach($discount_data as $key => $discount){
				  $service = $this->discount_model->booking_Config_getAll('vbs_u_service',['service_category' => $discount->servicecategory_id]);
                if($discount->discount_category=="2"){
                	 $discountdateto=$discount->dateto;
				   	 $discountdateto=str_replace('-', '/', $discountdateto);
					 $discountdateto=strtotime($discountdateto);
					 $dateto = date('d/m/Y',$discountdateto);


					  $discountdatefrom=$discount->datefrom;
				   	 $discountdatefrom=str_replace('-', '/', $discountdatefrom);
					 $discountdatefrom=strtotime($discountdatefrom);
					 $datefrom = date('d/m/Y',$discountdatefrom);
                }
				$result='<input type="hidden" name="discount_id" value="'.$discount->id.'">

				<div class="col-md-12">
					<div class="col-md-2" style="margin-top: 5px;">
					  <div class="form-group">
						<span>Statut</span>
							<select class="form-control" name="discount_statut" required >
								
								<option '; if ($discount->statut == "1") {$result.='selected="selected" '; }
								$result.=' value="1">Show</option>
								<option ';
								if ($discount->statut == "2") { $result.='selected="selected" '; }
								$result.=' value="2">Hide</option>
							</select>
					  </div>
					</div>
					<div class="col-md-2" style="margin-top: 5px;">
					    <div class="form-group">
					      <span>Discount Category</span>
					      <select class="form-control" id="editdiscount_category" name="discount_category" required onchange="editcheckCategory()">
					     <option '; if ($discount->discount_category == "0") {$result.='selected="selected" '; }
								$result.=' value="0">Welcome</option>
					     <option '; if ($discount->discount_category == "1") {$result.='selected="selected" '; }
								$result.=' value="1">Reward</option>
					    <option '; if ($discount->discount_category == "2") {$result.='selected="selected" '; }
								$result.=' value="2">Periode</option>
					    <option '; if ($discount->discount_category == "3") {$result.='selected="selected" '; } $result.=' value="3">Promo Code</option>
					      </select>
					    </div>
					  </div>
					<div class="col-md-2" style="margin-top: 5px;">
					  <div class="form-group">
							<span>Name Of Discount</span>
							<input type="text" class="form-control" name="name_of_discount" placeholder="" value="'.$discount->name_of_discount.'">
						</div>
					</div>
					
			</div>
			<div class="col-md-12">
			   <div class="col-md-2" style="margin-top: 5px;">
			    <div class="form-group">
			      <span>Service Category</span>
			      <select class="form-control" id="editservice_category" name="servicecategory_id" onchange="editaddservice()">
			        <option value="">All</option>';

			       foreach($service_cat as $key => $cat){
			        $result.='<option '; if ($discount->servicecategory_id ==  $cat->id) {$result.='selected="selected" '; } $result.=' value="'.$cat->id.'">'.$cat->category_name.'</option>';
			      }
			      $result.='</select>
			    </div>
			  </div>
			  <div class="col-md-2" style="margin-top: 5px;">
			    <div class="form-group">
			      <span>Services</span>
			      <select class="form-control" id="editservice" name="service_id">
			        <option value="">All</option>';
			       foreach($service as $key => $ser){
			        $result.='<option '; if ($discount->service_id  ==  $ser->id) {$result.='selected="selected" '; } $result.=' value="'.$ser->id.'">'.$ser->service_name.'</option>';
			     }
			        $result.='</select>
			    </div>
			  </div>

			 <div class="col-md-2" style="margin-top: 5px;">
			<div class="form-group">
							<span>Discount %</span>
							<input type="text" class="form-control" name="discount" placeholder="" value="'.$discount->discount.'">
						</div>
					</div>
			</div>
			<div class="col-md-12" id="editdiscountrewardnodiv">
			    <div class="col-md-2" style="margin-top: 5px;">
			    <div class="form-group">
			      <span>After Number of Bookings</span>
			        <input type="number" class="form-control" name="no_of_booking" placeholder="" value="'.$discount->no_of_booking.'" min="0" max="1000">
			    </div>
			  </div>
			</div>
			<div class="col-md-12" id="editdiscountpromocodediv">
			   <div class="col-md-2" style="margin-top: 5px;">
			    <div class="form-group">
			      <span>Promo Code</span>
			        <input type="text" class="form-control" name="promo_code" placeholder="" value="'.$discount->promo_code.'">
			    </div>
			  </div>
			</div>';
if($discount->discount_category=="2"){
			$result.='<div class="col-md-12"  id="editdiscountperiodsdiv">
				  <div class="col-md-2">
				    
				      <div class="col-md-6" style="padding:2px;">  
				       <div class="form-group">
				       <span>From</span>  
				         <input   id="editdiscountdatefrom" class="bdatepicker" name="discountdatefrom" type="text" value="'. date($datefrom) .'"  style="width:100%;" />
				      </div>
				     </div>   
				      
				      <div class="col-md-6" style="padding:2px;">
				         <div class="form-group">
				           <span>To</span>
				        <input   id="editdiscountdateto" class="bdatepicker" name="discountdateto" type="text" value="'. date($dateto) .'" style="width:100%;" />
				        </div>
				     </div>
				</div>

				  <div class="col-md-2">
				     
				      <div class="col-md-6" style="padding:2px;">
				        <div class="form-group">
				           <span>From</span>
				           <input   id="editdiscounttimefrom" name="discounttimefrom" type="text" value="'. date($discount->timefrom).'" style="width:100%;" />
				      </div>
				    </div>

				      
				      <div class="col-md-6" style="padding:2px;">
				        <div class="form-group">
				           <span>To</span>
				           <input   id="editdiscounttimeto" name="discounttimeto" type="text" value="'. date($discount->timeto).'" style="width:100%;" />
				      </div>
				    </div>
				       
				</div>
			
				</div>';
			}else{
				 $result.='<div class="col-md-12"  id="editdiscountperiodsdiv">
				  <div class="col-md-2">
				    
				      <div class="col-md-6" style="padding:2px;">  
				       <div class="form-group">
				       <span>From</span>  
				         <input   id="editdiscountdatefrom" class="bdatepicker" name="discountdatefrom" type="text" value="'. date('d/m/Y') .'"  style="width:100%;" />
				      </div>
				     </div>   
				      
				      <div class="col-md-6" style="padding:2px;">
				         <div class="form-group">
				           <span>To</span>
				        <input   id="editdiscountdateto" class="bdatepicker" name="discountdateto" type="text" value="'. date('d/m/Y') .'" style="width:100%;" />
				        </div>
				     </div>
				</div>

				  <div class="col-md-2">
				     
				      <div class="col-md-6" style="padding:2px;">
				        <div class="form-group">
				           <span>From</span>
				           <input   id="editdiscounttimefrom" name="discounttimefrom" type="text" value="'. date("h : i").'" style="width:100%;" />
				      </div>
				    </div>

				      
				      <div class="col-md-6" style="padding:2px;">
				        <div class="form-group">
				           <span>To</span>
				           <input   id="editdiscounttimeto" name="discounttimeto" type="text" value="'. date("h : i").'" style="width:100%;" />
				      </div>
				    </div>
				       
				</div>
			
				</div>';
		    	}
	    }
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
		public function get_service(){
		$id=(int)$_GET['servicecategory_id'];

		if($id>0){
			 $service = $this->discount_model->booking_Config_getAll('vbs_u_service',['service_category' => $id]);
			 
			 $result='<option value="">All</option>';
				       foreach($service as $key => $ser){
				        $result.='<option  value="'.$ser->id.'">'.$ser->service_name.'</option>';
				     }
			}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;	     
}
}
