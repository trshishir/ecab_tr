<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class library extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('general');
        $this->load->helper('language');
        if (!$this->basic_auth->is_login())
            redirect("admin", 'refresh');
        else
            $this->data['user'] = $this->basic_auth->user();
        $this->load->model('invoice_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');
        $this->load->model('support_model');
        $this->load->model('calls_model');
        $this->load->model('notes_model');
        $this->load->library('session');
        $this->load->model('user_model');
        $this->load->model('notifications_model');
        $this->load->model('cars_model');
        $this->load->model('documents_model');
        $this->data['configuration'] = get_configuration();
    }

    public function index() {
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "library";
        $this->data['gmaps'] = false;
        $this->data['title'] = 'LIBRARY';
        $this->data['show_subtitle'] = false;
        $this->data['subtitle'] = 'Add Library';
        $this->data['title_link'] = base_url('admin/library');
        $this->data['content'] = 'admin/library/indexv2';
        $this->load->model('calls_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');
        $this->data = [];
        $this->['request'] = $this->request_model->getAll();
        $this->['jobs'] = $this->jobs_model->getAll();
        $this->['calls'] = $this->calls_model->getAll();
        $this->data['dcsa'] = array('name' => 'dcsa', 'class' => 'user form-control', 'tabindex' => '1', 'placeholder' => 'Driver/Client/Supplier/Administration', 'id' => 'dcsa', 'type' => 'text', 'maxlength' => '150', 'value' => $this->form_validation->set_value('dcsa'));
        $this->data['doc_title'] = array('name' => 'title', 'class' => 'user form-control', 'tabindex' => '4', 'placeholder' => 'Document Name', 'id' => 'title', 'type' => 'text', 'value' => $this->form_validation->set_value('doc_title'));
        $this->data['un1'] = array('name' => 'un1', 'class' => 'user form-control', 'tabindex' => '5', 'placeholder' => 'Unknown Field', 'id' => 'un1', 'type' => 'text', 'value' => $this->form_validation->set_value('un1'));
        // $this->data['data'] = $this->request_model->getAll();
        // $this->data['data'] = $this->user_model->getAllClients();
        // 12/27/2020
        // $this->data['docs'] = $this->sitemodel->getConfig('document_docs');
        // $this->data['cities'] = $this->sitemodel->getConfig('document_cities');
        // $this->data['statuses'] = $this->sitemodel->getConfig('document_status');
        // $this->data['destinations'] = $this->sitemodel->getConfig('document_destination');
        // $this->data['categories'] = $this->sitemodel->getConfig('document_category');
        // $this->data['groups'] = $this->sitemodel->getConfig('groups');
        // $this->data['all_records'] = $this->documents_model->getAllLibraryTemplates();
        $this->_render_page('templates/admin_template_v2', $this->data);
    }

    public function fetchLibraryData() {
        $result = array('data' => array());

        $data = $this->documents_model->getAllLibraryTemplates();
        foreach ($data as $key => $value) {

            $status = ($value['active'] == 1) ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Inactive</span>';
            $checkbox = "<input type='checkbox' name='library[]' class='ccheckbox' value='" . $value['lid'] . "' />";
            $registered_date = date('d/m/Y', strtotime($value['created_at']));
            $registered_time = date('H:i:s', strtotime($value['created_at']));
            $since = timeDiff($value['created_at']);
            $edit_link = "<a href='" . base_url() . "admin/library/" . $value['lid'] . "/edit.php" . "'>" . create_timestamp_uid($value['created_at'], $value['lid']) . "</a>";
            // echo $edit_link."<br /> dd".$key;
            $view_link = "<a href='" . base_url() . "admin/library/" . $value['lid'] . "/view.php" . "' class='btn btn-default'>View</a>";
            $result['data'][$key] = array(
                $checkbox,
                $edit_link,
                $registered_date,
                $registered_time,
                $value['civility'] . ' ' . $value['first_name'] . ' ' . $value['last_name'],
                $value['doc_destination'],
                $value['doc_category'],
                $value['lib_docname'],
                $view_link,
                $status,
                $since
            );
        } // /foreach

        echo json_encode($result);
    }

    public function clientSearchData() {
        //if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['search'])) {
        $result = array('data' => array());
        //echo "<PRE>";print_r($_POST);echo "</PRE>";
        $searchFilter['search_name'] = $_POST['search_name'];
        $searchFilter['search_email'] = $_POST['search_email'];
        $searchFilter['search_phone'] = $_POST['search_phone'];
        $searchFilter['date_from'] = $_POST['date_from'];
        $searchFilter['date_to'] = $_POST['date_to'];
        $searchFilter['status'] = $_POST['status'];
        $data = $this->user_model->searchClient($searchFilter);
        if (!empty($data)) {
            foreach ($data as $key => $value) {

                $status = ($value['active'] == 1) ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Inactive</span>';
                $checkbox = "<input type='checkbox' name='clients[]' class='ccheckbox' value='" . $value['id'] . "' />";
                $registered_date = date('d/m/Y', strtotime($value['created_at']));
                $registered_time = date('H:i:s', strtotime($value['created_at']));
                $since = timeDiff($value['created_at']);
                $edit_link = "<a href='" . base_url() . "admin/clients/" . $value['id'] . "/edit.php" . "'>" . create_timestamp_uid($value['created_at'], $value['id']) . "</a>";
                $result['data'][$key] = array(
                    $checkbox,
                    $edit_link,
                    $registered_date,
                    $registered_time,
                    $value['civility'] . ' ' . $value['first_name'] . ' ' . $value['last_name'],
                    $value['email'],
                    $value['phone'],
                    $status,
                    $since
                );
            } // /foreach
        }
        echo json_encode($result);
        //}
    }
    
    public function clientExcelDownload() {
        //if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['search'])) {
        $result = array('data' => array());
        //echo "<PRE>";print_r($_POST);echo "</PRE>";
        $searchFilter['search_name'] = $_POST['search_name'];
        $searchFilter['search_email'] = $_POST['search_email'];
        $searchFilter['search_phone'] = $_POST['search_phone'];
        $searchFilter['date_from'] = $_POST['date_from'];
        $searchFilter['date_to'] = $_POST['date_to'];
        $searchFilter['status'] = $_POST['status'];
        $data = $this->user_model->searchClient($searchFilter);
        
        $filename = 'clients_'.date('Ymd').'.xls'; 

        // file creation 
        $file = fopen('php://output','w');
        $header = array("#","Id","Date","Time","Name","Email","Phone","Status","Since"); 
        fputcsv($file, $header);

        if (!empty($data)) {
            foreach ($data as $key => $value) {

                $status = ($value['active'] == 1) ? 'Active' : 'Inactive';
                $checkbox = "";
                $registered_date = date('d/m/Y', strtotime($value['created_at']));
                $registered_time = date('H:i:s', strtotime($value['created_at']));
                $since = timeDiff($value['created_at']);
                $edit_link = create_timestamp_uid($value['created_at'], $value['id']);
                $name = $value['civility'] . ' ' . $value['first_name'] . ' ' . $value['last_name'];
                /*$result['data'][$key] = array(
                    $checkbox,
                    $edit_link,
                    $registered_date,
                    $registered_time,
                    $value['civility'] . ' ' . $value['first_name'] . ' ' . $value['last_name'],
                    $value['email'],
                    $value['phone'],
                    $status,
                    $since
                );
                */
                $line = array($checkbox , $edit_link , $registered_date , $registered_time , $name , $value['email'] , $value['phone'] , $status , $since);
                fputcsv($file,$line); 
                
            } // /foreach
            
        } 
        header("Content-Description: File Transfer"); 
        header('Content-Type: application/octet-stream');
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Type: application/force-download");
        header("Content-Transfer-Encoding: binary");
        // header("Content-Type: application/xls; ");
        header("Content-Disposition: attachment; filename=$filename");
        header('Cache-Control: max-age=0');
        //output all remaining data on a file pointer
        fpassthru($file);
        fclose($file); 
        die();
        //echo json_encode($result);
    }

    public function add() {

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $this->load->library('form_validation');

            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            if ($this->input->post('custom_text')) {
                // $user_group = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('groups') . " WHERE name = 'clients'");

                // $group_id = array($user_group[0]->id);
                $documents_data = array(
                    'added_by' => $this->session->userdata['user_id'],
                    'lib_status' => $this->input->post('status'),
                    'lib_category' => $this->input->post('category'),
                    'lib_destination' => $this->input->post('destination'),
                    'lib_docname' => $this->input->post('title'),
                    'lib_customtext' => $this->input->post('custom_text')
                );
                // print_r($documents_data); echo "our final recorder here wahid "; exit;

                if($this->documents_model->savelibrarytemplate($documents_data)) {

                    $this->session->set_flashdata('messages', $this->ion_auth->messages());
                    redirect('admin/library', 'refresh');
                } else {
                    $this->session->set_flashdata('error', $this->ion_auth->messages());
                    $this->_render_page('templates/admin_template_v2', $this->data);
                }
            }
        }
    }

    public function edit_template() {
        // echo "we are here eidditt"; exit;

        $this->load->model('calls_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');

        $data = [];
        $data['request'] = $this->request_model->getAll();
        $data['jobs'] = $this->jobs_model->getAll();
        $data['calls'] = $this->calls_model->getAll();

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            if ($this->input->post('custom_text')) {
                // $user_group = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('groups') . " WHERE name = 'clients'");

                // $group_id = array($user_group[0]->id);
                $custom = $this->input->post('custom_text');
                $i = 0;
                foreach ($custom as $cus) {

                    $documents_data = array(
                        'added_by' => $this->session->userdata['user_id'],
                        'lib_status' => $_POST['status'][$i],
                        'lib_category' => $_POST['category'][$i],
                        'lib_destination' => $_POST['destination'][$i],
                        'lib_docname' => $_POST['title'][$i],
                        'lib_customtext' => $cus
                    );
                    print_r($documents_data); echo "our final recorder here wahid "; //exit;
                    // $chk = $this->documents_model->updatelibrarytemplate($documents_data);
                    $i++;
                }
                // exit;
                if($chk) {

                    $this->session->set_flashdata('messages', $this->ion_auth->messages());
                    redirect('admin/library', 'refresh');
                } else {
                    $this->session->set_flashdata('error', $this->ion_auth->messages());
                    $this->_render_page('templates/admin_template_v2', $this->data);
                }
            } else {
                $this->session->set_flashdata('error', $this->ion_auth->messages());
                $this->_render_page('templates/admin_template_v2', $this->data);
            }
        } else {
            $this->session->set_flashdata('error', $this->ion_auth->messages());
            $this->_render_page('templates/admin_template_v2', $this->data);
        }
        $this->session->set_flashdata('error', $this->ion_auth->messages());
        $this->_render_page('templates/admin_template_v2', $this->data);
    }

    public function configurations() {
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "Config Clients";
        $this->data['gmaps'] = false;
        $this->data['title'] = $this->lang->line("configurations");
        $this->data['subtitle'] = 'Config Clients';
        $this->data['title_link'] = '#';
        $this->data['content'] = 'admin/clients/configurations';
        $data = [];
        $this->data['data'] = $this->notifications_model->getAll();
        $this->_render_page('templates/admin_template', $this->data);
    }

    public function addclienttypeproccess() {
        extract($_GET);
        //print_r($_GET);
        //die();
        $values = [
            'type' => $type,
            'is_delete' => 1,
            'create_date' => date("Y-m-d H:i:s")
        ];


        // print_r($values);

        $insert = $this->cars_model->insertData('vbs_clientType', $values, 'type', $type);

        if ($insert == 1) {
            echo "success";
        }
    }

    public function addclientpaymentproccess() {
        extract($_GET);
        //print_r($_GET);
        //die();
        $values = [
            'payment' => $payment,
            'is_delete' => 1,
            'create_date' => date("Y-m-d H:i:s")
        ];


        // print_r($values);

        $insert = $this->cars_model->insertData('vbs_clientPayment', $values, 'payment', $payment);

        if ($insert == 1) {
            echo "success";
        }
    }

    public function addclientdelayproccess() {
        extract($_GET);
        //print_r($_GET);
        //die();
        $values = [
            'delay' => $delay,
            'is_delete' => 1,
            'create_date' => date("Y-m-d H:i:s")
        ];


        // print_r($values);

        $insert = $this->cars_model->insertData('vbs_clientDelay', $values, 'delay', $delay);

        if ($insert == 1) {
            echo "success";
        }
    }

    public function getclienttypeproccess() {
        extract($_GET);
        $statut = $this->cars_model->getStaut('vbs_clientType', 'is_delete', '1', $name, $from, $to, 'type');
        $tbody = '';
        if (!empty($statut)) {
            $i = 1;
            foreach ($statut as $stat) {
                $tbody .= '
                <tr>
                <td align="center">
	             <input type="checkbox" name="check" class="TypeClass"  id="TypeCheck" value="' . $stat->id . '" >
	             <input type="hidden" id="update_type" value="' . $stat->type . '">
	             </td>
                 <td align="center">' . $i . '</td>
                 <td align="center">' . $stat->type . '</td>
                 <td align="center">' . $stat->create_date . '</td>
                </tr>
                ';
                $i++;
            }
        } else {
            $tbody .= '<tr align="center"><td colspan="4"><span class="text-danger text-center">No Record Found</span></td></tr>';
        }

        echo $tbody;
    }

    public function getclientpaymentproccess() {
        extract($_GET);
        $statut = $this->cars_model->getStaut('vbs_clientPayment', 'is_delete', '1', $name, $from, $to, 'payment');
        $tbody = '';
        if (!empty($statut)) {
            $i = 1;
            foreach ($statut as $stat) {
                $tbody .= '
                <tr>
                <td align="center">
                 <input type="checkbox" name="check" class="PaymentClass"  id="PaymentCheck" value="' . $stat->id . '" >
                 <input type="hidden" id="update_payment" value="' . $stat->payment . '">
                 </td>
                 <td align="center">' . $i . '</td>
                 <td align="center">' . $stat->payment . '</td>
                 <td align="center">' . $stat->create_date . '</td>
                </tr>
                ';
                $i++;
            }
        } else {
            $tbody .= '<tr align="center"><td colspan="4"><span class="text-danger text-center">No Record Found</span></td></tr>';
        }

        echo $tbody;
    }

    public function getclientdelayproccess() {
        extract($_GET);
        $statut = $this->cars_model->getStaut('vbs_clientDelay', 'is_delete', '1', $name, $from, $to, 'delay');
        $tbody = '';
        if (!empty($statut)) {
            $i = 1;
            foreach ($statut as $stat) {
                $tbody .= '
                <tr>
                <td align="center">
                 <input type="checkbox" name="check" class="DelayClass"  id="DelayCheck" value="' . $stat->id . '" >
                 <input type="hidden" id="update_delay" value="' . $stat->delay . '">
                 </td>
                 <td align="center">' . $i . '</td>
                 <td align="center">' . $stat->delay . '</td>
                 <td align="center">' . $stat->create_date . '</td>
                </tr>
                ';
                $i++;
            }
        } else {
            $tbody .= '<tr align="center"><td colspan="4"><span class="text-danger text-center">No Record Found</span></td></tr>';
        }

        echo $tbody;
    }

    public function updateclienttypeproccess() {
        extract($_GET);

        /* print_r($_GET);
          die(); */
        $values = [
            'type' => $type
        ];
        $update = $this->cars_model->updateStatut($id, $type, $values, 'vbs_clientType', 'type');

        if ($update == 1) {
            echo "success";
        } else {
            echo "failed";
        }
    }

    public function updateclientpaymentproccess() {
        extract($_GET);

        /* print_r($_GET);
          die(); */
        $values = [
            'payment' => $payment
        ];
        $update = $this->cars_model->updateStatut($id, $payment, $values, 'vbs_clientPayment', 'payment');

        if ($update == 1) {
            echo "success";
        } else {
            echo "failed";
        }
    }

    public function updateclientdelayproccess() {
        extract($_GET);

        /* print_r($_GET);
          die(); */
        $values = [
            'delay' => $delay
        ];
        $update = $this->cars_model->updateStatut($id, $delay, $values, 'vbs_clientDelay', 'delay');

        if ($update == 1) {
            echo "success";
        } else {
            echo "failed";
        }
    }

    public function deleteclienttypeproccess() {
        extract($_GET);

        //$delete =  $this->cars_model->deleteDelete('vbs_carStatut',$id);
        $values = [
            'is_delete' => '0'
        ];
        $update = $this->cars_model->deactive($values, $id, 'vbs_clientType');
        if ($update == 1) {
            echo "success";
        }
    }

    public function deleteclientpaymentproccess() {
        extract($_GET);

        //$delete =  $this->cars_model->deleteDelete('vbs_carStatut',$id);
        $values = [
            'is_delete' => '0'
        ];
        $update = $this->cars_model->deactive($values, $id, 'vbs_clientPayment');
        if ($update == 1) {
            echo "success";
        }
    }

    public function deleteclientdelayproccess() {
        extract($_GET);

        //$delete =  $this->cars_model->deleteDelete('vbs_carStatut',$id);
        $values = [
            'is_delete' => '0'
        ];
        $update = $this->cars_model->deactive($values, $id, 'vbs_clientDelay');
        if ($update == 1) {
            echo "success";
        }
    }

}
