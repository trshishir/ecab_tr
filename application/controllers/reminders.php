<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reminders extends MY_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->lang->load('auth');
		$this->load->helper('language');
		if (!$this->basic_auth->is_login())
			redirect("admin", 'refresh');
		else
			$this->data['user'] = $this->basic_auth->user();
		$this->load->model('reminders_model');
		$this->load->model('request_model');
		$this->load->model('jobs_model');
		$this->load->model('calls_model');
		$this->load->model('support_model');
		$this->load->model('user_model');
		$this->data['configuration'] = get_configuration();
	}

	public function index(){
		$this->data['css_type'] 	= array("form","datatable");
		$this->data['active_class'] = "reminders";
		$this->data['gmaps'] 		= false;
		$this->data['title'] 		= $this->lang->line("reminders");
		//$this->data['subtitle'] 		= $this->lang->line("reminders");
		$this->data['title_link'] 	= base_url('admin/reminders');
		$this->data['content'] 		= 'admin/reminders/index';
        $data = [];
		$this->data['data'] = $this->reminders_model->getAll();
		$this->data['obj'] = $this;
		$this->_render_page('templates/admin_template', $this->data);
	}
	
	public function getUserByID($userid){
		return $this->reminders_model->getUserByID($userid);
	}

	public function add(){
		$this->data['css_type'] 	= array("form");
		$this->data['active_class'] = "reminders";
		$this->data['gmaps'] 	= false;
		$this->data['title'] 	= $this->lang->line("reminders");
		$this->data['subtitle'] 	= '> Add Reminder';
		$this->data['show_subtitle'] 	= true;
		$this->data['title_link'] 	= base_url('admin/reminders.php');
		$this->data['content']  = 'admin/reminders/add';
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->addreminders();
		}
		$this->_render_page('templates/admin_template', $this->data);
	}

	public function addreminders(){
		$error = $this->reminders_model->reminder_validate(true);
		if (empty($error)) {
			$message = isset($_POST['message']) ? $_POST['message'] : '';
			
			if(!empty($_POST['reminder_days'])){
				foreach($_POST['reminder_days'] as $k=>$v){
					$reminders[] = array(
									'before_after' => $_POST['before_after'][$k],
									'reminder_days' => $v
								);
				}
				$reminders = json_encode($reminders);
			}else{
				$reminders = '';
			}
			
			$id = $this->reminders_model->create([
				'reminder_status' 	=> @$_POST['reminder_status'],
				'user_id' 	=> $this->session->userdata('user_id'),
				'name' 	=> @$_POST['name'],
				'status' 	=> @$_POST['status'],
				'module' => @$_POST['module'],
				'subject' => @$_POST['subject'],
				'send_copy_to_department_operator' => @$_POST['send_copy_to_department_operator'],
				'message' => $message,
				'reminders' => $reminders,
				'created_at' 	=> date('Y-m-d H:i:s'),
			]);
			if($id){
				$this->session->set_flashdata('alert', [
					'message' => "Successfully Created.",
					'class' => "alert-success",
					'type' => "Success"
				]);
				redirect('admin/reminders');
				
			}else{
				$this->session->set_flashdata('alert', [
					'message' => "Something went wrong.",
					'class' => "alert-danger",
					'type' => "Error"
				]);
				redirect('admin/reminders');
			}
			
			
		} else {
	     	$this->session->set_flashdata('alert', [
				'message' => @$error[0],
				'class' => "alert-danger",
				'type' => "Error"
			]);
			redirect('admin/reminders');
		}
	}

	public function edit($id){
		  // code by s_a 
		$timestampid=$id;
		$date = substr($timestampid, 0, 8);
		$id =str_replace($date,"",$timestampid);
		$id=ltrim($id, "0");
      // code by s_a 
		$this->data['data']	= $this->reminders_model->get(['id' => $id]);
		if($this->data['data'] != false) {
			$this->data['css_type'] = array("form");
			$this->data['active_class'] = "reminders";
			$this->data['gmaps'] 	= false;
			$this->data['title'] 	= $this->lang->line("reminders");
			$this->data['subtitle'] = ' > '.create_timestamp_uid($this->data['data']->created_at,$this->data['data']->id);
			$this->data['show_subtitle'] 	= true;
			$this->data['title_link'] 	= base_url('admin/reminders.php');
			$this->data['content']  = 'admin/reminders/edit';
			$this->_render_page('templates/admin_template', $this->data);
		} else show_404();
	}

	public function update($id){
		$error = $this->reminders_model->reminder_validate(true);
		$con = $this->reminders_model->get(['id' => $id]);
		if($con != false) {
			if (empty($error)) {
				$message = isset($_POST['message']) ? $_POST['message'] : '';
				
				if(!empty($_POST['reminder_days'])){
					foreach($_POST['reminder_days'] as $k=>$v){
						$reminders[] = array(
										'before_after' => $_POST['before_after'][$k],
										'reminder_days' => $v
									);
					}
					$reminders = json_encode($reminders);
				}else{
					$reminders = '';
				}
				
				$this->reminders_model->update([
					'reminder_status' 	=> @$_POST['reminder_status'],
					'user_id' 	=> $this->session->userdata('user_id'),
					'name' 	=> @$_POST['name'],
					'status' 	=> @$_POST['status'],
					'module' => @$_POST['module'],
					'subject' => @$_POST['subject'],
					'send_copy_to_department_operator' => @$_POST['send_copy_to_department_operator'],
					'message' => $message,
					'reminders' => $reminders,
					'updated_at' 	=> date('Y-m-d'),
				], $id);

				$this->session->set_flashdata('alert', [
						'message' => "Successfully Updated.",
						'class' => "alert-success",
						'type' => "Success"
				]);
				redirect('admin/reminders');
			} else {
				$this->session->set_flashdata('alert', [
						'message' => @$error[0],
						'class' => "alert-danger",
						'type' => "Error"
				]);
				redirect('admin/reminders');
			}

			redirect('admin/reminders');
		} else show_404();
	}

	public function delete(){
		$id=$_POST['reminder_id'];
		$this->reminders_model->delete($id);
		$this->session->set_flashdata('alert', [
				'message' => "Successfully deleted.",
				'class' => "alert-success",
				'type' => "Success"
		]);
		redirect('admin/reminders');
	}
}