<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client_config extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->lang->load('auth');
		$this->lang->load('general');
		$this->load->helper('language');
		$this->load->helper('validate');
		if (!$this->basic_auth->is_login())
			redirect("admin", 'refresh');
		else
		$this->data['user'] = $this->basic_auth->user();
		$this->load->model('client_config_model');
	
		$this->load->model('request_model');
		$this->load->model('jobs_model');
		$this->load->model('support_model');
		$this->load->model('calls_model');
		$this->load->model('notes_model');
		$this->load->model('notifications_model');
		$this->data['configuration'] = get_configuration();

	}

	
	public function index(){

		$this->data['user'] = $this->basic_auth->user();
		$this->data['css_type'] 	= array("form","booking_datatable","datatable");
		$this->data['active_class'] = "client_config";
		$this->data['gmaps'] 		= false;
		$this->data['title'] 		= 'Client Configurations';
		$this->data['title_link'] 	= base_url('admin/client_config');
		$this->data['content'] 		= 'admin/client_config/index';

   		$data = [];
		$this->data['client_status_data'] = $this->client_config_model->getAllData('vbs_clientstatus');
		$this->data['client_civilite_data'] = $this->client_config_model->getAllData('vbs_clientcivilite');
		$this->data['client_delay_data'] = $this->client_config_model->getAllData('vbs_clientdelay');
		$this->data['client_payment_data'] = $this->client_config_model->getAllData('vbs_clientpayment');
		$this->data['client_type_data'] = $this->client_config_model->getAllData('vbs_clienttype');


		$this->_render_page('templates/admin_template', $this->data);
	}
	public function statusAdd(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->statusStore();
		}else{
			return redirect("admin/client_config");
		}
		


	}
	public function statusStore(){
			// $error = request_validate();
			if (empty($error)) {
				//$dob = to_unix_date(@$_POST['dob']);
				$user = $this->basic_auth->user();
			 	$user_id=$user->id;

				$id = $this->client_config_model->createRecord('vbs_clientstatus',[
					'statut' 		=> $_POST['statut'],
					'name' 		    => trim($_POST['name']),
					'user_id'       => $user_id
				]);
			
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a client statut .",
					'class' => "alert-success",
					'status_id' => "1",
					'type' => "Success"
				]);
				redirect('admin/client_config');
			} else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/client_config');
			}
	}
	public function statusEdit(){
		
		
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['client_status_id'];
			if (!empty($id)) {
			$update=$this->client_config_model->createUpdate('vbs_clientstatus',[
				'statut' 		=> @$_POST['statut'],
			    'name' 		    => trim($_POST['name']),
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully updated a client statut.",
					'class' => "alert-success",
					'status_id' => "1",
					'type' => "Success"
				]);
				redirect('admin/client_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/client_config');
			}
		}
	}else{
		redirect('admin/client_config');
	}
		
	}
	public function statusDelete(){
		
		$id=$_POST['delet_client_status_id'];
		$del=$this->client_config_model->createDelete('vbs_clientstatus',['id'=>$id]);
		if ($del==true) {
		$this->session->set_flashdata('alert', [
				'message' => "Successfully deleted.",
				'class' => "alert-success",
				'status_id' => "1",
				'type' => "Success"
		]);
		redirect('admin/client_config');
		}
		else {
			$this->session->set_flashdata('alert', [
					'message' => "Please try again.",
					'class' => "alert-danger",
					'status_id' => "1",
					'type' => "Error"
			]);
			redirect('admin/client_config');
		}
	}
	public function get_ajax_status_data(){
		$id=(int)$_GET['client_status_id'];

		if($id>0){
			$this->load->model('client_config_model');
			$clientStatus = $this->client_config_model->getSingleRecord('vbs_clientstatus',['id' => $id]);
			
			
				$result='<input type="hidden" name="client_status_id" value="'.$clientStatus->id.'">
			 <div class="col-md-12"> 
			 	<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($clientStatus->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($clientStatus->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div> 
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Name</span>
					<input type="text" class="form-control" name="name" placeholder="" value="'.$clientStatus->name.'" required>
				</div>
				</div>
				</div>';
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}

	public function civiliteAdd(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->civiliteStore();
		}else show_404();
	}
	public function civiliteStore(){

		if (empty($error)) {
			
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_clientcivilite';
			$id = $this->client_config_model->createRecord($table,[
				'civilite' 		=> trim($_POST['civilite']),
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a civilite .",
				'class' => "alert-success",
				'status_id' => "2",
				'type' => "Success"
			]);
			redirect('admin/client_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'type' => "Error"
			];
			redirect('admin/client_config');
		}
	}
	public function civiliteEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['civilite_id'];
			if (!empty($id)) {
			$table='vbs_clientcivilite';
			$update=$this->client_config_model->createUpdate($table,[
				'civilite' 		=> trim($_POST['civilite']),
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Civilite.",
					'class' => "alert-success",
					'status_id' => "2",
					'type' => "Success"
				]);
				redirect('admin/client_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/client_config');
			}
		}
	}else show_404();

	}
	public function civiliteDelete(){
	$this->load->model('client_config_model');
	$id=$_POST['delet_civilite_id'];
	$table = "vbs_clientcivilite";
	$del=$this->client_config_model->createDelete($table,['id'=>$id]);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "2",
			'type' => "Success"
	]);
	redirect('admin/client_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "2",
				'type' => "Error"
		]);
		redirect('admin/client_config');
	}
	 show_404();
	}
	public function get_ajax_civilite_data(){
		$id=(int)$_GET['civilite_id'];

		if($id>0){
			$this->load->model('client_config_model');
			$civilite = $this->client_config_model->getSingleRecord('vbs_clientcivilite',['id' => $id]);
			
			
				$result='<input type="hidden" name="civilite_id" value="'.$civilite->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($civilite->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($civilite->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Civilite</span>
					<input type="text" class="form-control" name="civilite" placeholder="" value="'.$civilite->civilite.'">
				</div>
				</div>
				</div>';
		
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
//delay
	public function delayAdd(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->delayStore();
		}else show_404();
	}
	public function delayStore(){

		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_clientdelay';
			$id = $this->client_config_model->createRecord($table,[
				'delay' 		=> trim($_POST['delay']),
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a delay of payment.",
				'class' => "alert-success",
				'status_id' => "5",
				'type' => "Success"
			]);
			redirect('admin/client_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'status_id' => "5",
				'type' => "Error"
			];
			redirect('admin/client_config');
		}
	}
	public function delayEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['delay_id'];
			if (!empty($id)) {
			$table='vbs_clientdelay';
			$update=$this->client_config_model->createUpdate($table,[
				'delay' 		=> trim($_POST['delay']),
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully updated a delay of payment.",
					'class' => "alert-success",
					'status_id' => "5",
					'type' => "Success"
				]);
				redirect('admin/client_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "5",
					'type' => "Error"
				];
				redirect('admin/client_config');
			}
		}
	}else show_404();

	}
	public function delayDelete(){
	$this->load->model('client_config_model');
	$id=$_POST['delet_delay_id'];
	$table = "vbs_clientdelay";
	$del=$this->client_config_model->createDelete($table,['id'=>$id]);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "5",
			'type' => "Success"
	]);
	redirect('admin/client_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "5",
				'type' => "Error"
		]);
		redirect('admin/client_config');
	}
	 show_404();
	}
	public function get_ajax_delay_data(){
		$id=(int)$_GET['delay_id'];

		if($id>0){
			$this->load->model('client_config_model');
			$delay_data = $this->client_config_model->getAllData('vbs_clientdelay',['id' => $id]);
			
			foreach($delay_data as $key => $delay){
				$result='<input type="hidden" name="delay_id" value="'.$delay->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($delay->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($delay->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Delay</span>
					<input type="text" class="form-control" name="delay" placeholder="" value="'.$delay->delay.'">
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
//payment
	public function paymentAdd(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->paymentStore();
		}else show_404();
	}
	public function paymentStore(){

		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_clientpayment';
			$id = $this->client_config_model->createRecord($table,[
				'payment' 		=> trim($_POST['payment']),
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a payment.",
				'class' => "alert-success",
				'status_id' => "4",
				'type' => "Success"
			]);
			redirect('admin/client_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'status_id' => "4",
				'type' => "Error"
			];
			redirect('admin/client_config');
		}
	}
	public function paymentEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['payment_id'];
			if (!empty($id)) {
			$table='vbs_clientpayment';
			$update=$this->client_config_model->createUpdate($table,[
				'payment' 		=> trim($_POST['payment']),
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully updated a payment.",
					'class' => "alert-success",
					'status_id' => "4",
					'type' => "Success"
				]);
				redirect('admin/client_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "4",
					'type' => "Error"
				];
				redirect('admin/client_config');
			}
		}
	}else show_404();

	}
	public function paymentDelete(){
	$this->load->model('client_config_model');
	$id=$_POST['delet_payment_id'];
	$table = "vbs_clientpayment";
	$del=$this->client_config_model->createDelete($table,['id'=>$id]);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "4",
			'type' => "Success"
	]);
	redirect('admin/client_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "4",
				'type' => "Error"
		]);
		redirect('admin/client_config');
	}
	 show_404();
	}
	public function get_ajax_payment_data(){
		$id=(int)$_GET['payment_id'];

		if($id>0){
			$this->load->model('client_config_model');
			$payment_data = $this->client_config_model->getAllData('vbs_clientpayment',['id' => $id]);
			
			foreach($payment_data as $key => $payment){
				$result='<input type="hidden" name="payment_id" value="'.$payment->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($payment->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($payment->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Payment</span>
					<input type="text" class="form-control" name="payment" placeholder="" value="'.$payment->payment.'">
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	//type
	public function typeAdd(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->typeStore();
		}else show_404();
	}
	public function typeStore(){

		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_clienttype';
			$id = $this->client_config_model->createRecord($table,[
				'type' 		=> trim($_POST['type']),
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a type.",
				'class' => "alert-success",
				'status_id' => "3",
				'type' => "Success"
			]);
			redirect('admin/client_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'status_id' => "3",
				'type' => "Error"
			];
			redirect('admin/client_config');
		}
	}
	public function typeEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['type_id'];
			if (!empty($id)) {
			$table='vbs_clienttype';
			$update=$this->client_config_model->createUpdate($table,[
				'type' 		=> trim($_POST['type']),
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully updated a type.",
					'class' => "alert-success",
					'status_id' => "3",
					'type' => "Success"
				]);
				redirect('admin/client_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "3",
					'type' => "Error"
				];
				redirect('admin/client_config');
			}
		}
	}else show_404();

	}
	public function typeDelete(){
	$this->load->model('client_config_model');
	$id=$_POST['delet_type_id'];
	$table = "vbs_clienttype";
	$del=$this->client_config_model->createDelete($table,['id'=>$id]);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "3",
			'type' => "Success"
	]);
	redirect('admin/client_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "3",
				'type' => "Error"
		]);
		redirect('admin/client_config');
	}
	 show_404();
	}
	public function get_ajax_type_data(){
		$id=(int)$_GET['type_id'];

		if($id>0){
			$this->load->model('client_config_model');
			$type_data = $this->client_config_model->getAllData('vbs_clienttype',['id' => $id]);
			
			foreach($type_data as $key => $type){
				$result='<input type="hidden" name="type_id" value="'.$type->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($type->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($type->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Type</span>
					<input type="text" class="form-control" name="type" placeholder="" value="'.$type->type.'">
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	function do_user_upload_image($filename,$name) {
        
        $config['upload_path'] = './uploads/vehicle_images/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['max_size'] = '10240';
        $config['file_name'] = str_replace(' ', '_', $filename['name']);
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        
        if (!$this->upload->do_upload($name)) {
            return NULL;
        } else {
            $avatar = $this->upload->data();
            $avatar = $avatar['file_name'];
        
            return $avatar;
        }
        
    }
}
