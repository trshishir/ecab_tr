<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partner_config extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->lang->load('auth');
		$this->lang->load('general');
		$this->load->helper('language');
		$this->load->helper('validate');
		if (!$this->basic_auth->is_login())
			redirect("admin", 'refresh');
		else
		$this->data['user'] = $this->basic_auth->user();
		$this->load->model('partner_config_model');
		$this->load->model('partners_model');
		$this->load->model('request_model');
		$this->load->model('jobs_model');
		$this->load->model('support_model');
		$this->load->model('calls_model');
		$this->load->model('notes_model');
		$this->load->model('notifications_model');
		$this->data['configuration'] = get_configuration();

	}


	public function index(){

		$this->data['user'] = $this->basic_auth->user();
		$this->data['css_type'] 	= array("form","booking_datatable","datatable");
		$this->data['active_class'] = "partner";
		$this->data['gmaps'] 		= false;
		$this->data['title'] 		= 'Partner Configurations';
		$this->data['title_link'] 	= base_url('admin/partner_config');
		$this->data['content'] 		= 'admin/partner_config/index';
   		$data = [];
		
		$this->data['civilite_data'] = $this->partner_config_model->getAllData('vbs_partnercivilite');
		$this->data['partner_status_data'] = $this->partner_config_model->getAllData('vbs_partnerstatus');
		$this->data['type_data'] = $this->partner_config_model->getAllData('vbs_partnertype');
		
		$data['partners'] = $this->partner_config_model->getAll();
		$data['jobs'] 	  = $this->jobs_model->getAll();
		$data['calls']    = $this->calls_model->getAll();
		$this->data['data'] = $this->partner_config_model->getAll();
		$this->_render_page('templates/admin_template', $this->data);
	}
	public function partnerStatusadd(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->partnerStatusstore();
		}else{
			redirect('admin/partner_config');
		}
		

	}
	public function partnerStatusstore(){
		
			if (empty($error)) {
				
				$user = $this->basic_auth->user();
			 	$user_id=$user->id;

				$id = $this->partner_config_model->addPartnerStatus([
					'status' 		=> @$_POST['status'],
					'statut' 		=> @$_POST['statut'],
					'user_id'       => $user_id
				]);
			
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a partner statut .",
					'class' => "alert-success",
					'status_id' => "1",
					'type' => "Success"
				]);
				redirect('admin/partner_config');
			} else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "1",
					'type' => "Error"
				];
				redirect('admin/partner_config');
			}
	}
	public function partnerStatusEdit(){
		$this->load->model('partner_config_model');
		$this->data['css_type'] 	= array("form","datatable");
		$this->data['active_class'] = "partner_config";
		$this->data['title'] 		= $this->lang->line("partners");
		$this->data['title_link'] 	= base_url('admin/partner_config');
		$this->data['content'] 		= 'admin/partner_config/index';
		$this->data['active_status'] 		= '1';
		$this->load->model('calls_model');
		$this->load->model('request_model');
		$this->load->model('jobs_model');
		$data = [];
		$data['partners'] = $this->partner_config_model->getAll();
		$data['jobs'] 	 = $this->jobs_model->getAll();
		$data['calls']   = $this->calls_model->getAll();
		$this->data['data'] = $this->partner_config_model->getAll();
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['partner_status_id'];
			if (!empty($id)) {
			$update=$this->partner_config_model->partnerStatusUpdate([
				'status' 		=> @$_POST['status'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a partner statut.",
					'class' => "alert-success",
					'status_id' => "1",
					'type' => "Success"
				]);
				redirect('admin/partner_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "1",
					'type' => "Error"
				];
					redirect('admin/partner_config');
			}
		}
	}
		redirect('admin/partner_config');
	}
	public function deletePartnerStatus(){
		$this->load->model('partner_config_model');
		$id=$_POST['delet_partner_status_id'];
		$del=$this->partner_config_model->deletePartnerStatus($id);
		if ($del==true) {
		$this->session->set_flashdata('alert', [
				'message' => "Successfully deleted.",
				'class' => "alert-success",
				'status_id' => "1",
				'type' => "Success"
		]);
		redirect('admin/partner_config');
		}
		else {
			$this->session->set_flashdata('alert', [
					'message' => "Please try again.",
					'class' => "alert-danger",
					'status_id' => "1",
					'type' => "Error"
			]);
			redirect('admin/partner_config');
		}
	}
	public function get_ajax_partnerStatus(){
		$id=(int)$_GET['partner_status_id'];

		if($id>0){
			$this->load->model('partner_config_model');
			$partner_status_data = $this->partner_config_model->partner_Config_getAll('vbs_partnerstatus',['id' => $id]);
			
			foreach($partner_status_data as $key => $partnerStatus){
				$result='<input type="hidden" name="partner_status_id" value="'.$partnerStatus->id.'">
			 <div class="col-md-12">  
			 	<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($partnerStatus->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($partnerStatus->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div> 
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Status</span>
					<input type="text" class="form-control" name="status" placeholder="" value="'.$partnerStatus->status.'" required>
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}

	public function civiliteAdd(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->civilitestore();
		}else show_404();
	}
	public function civilitestore(){
		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_partnercivilite';
			$id = $this->partner_config_model->partner_Config_Add($table,[
				'civilite' 		=> @$_POST['civilite'],
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a civilite .",
				'class' => "alert-success",
				'status_id' => "2",
				'type' => "Success"
			]);
			redirect('admin/partner_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'status_id' => "2",
				'type' => "Error"
			];
			redirect('admin/partner_config');
		}
	}
	public function civiliteEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['civilite_id'];
			if (!empty($id)) {
			$table='vbs_partnercivilite';
			$update=$this->partner_config_model->partner_Config_Update($table,[
				'civilite' 		=> @$_POST['civilite'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Civilite.",
					'class' => "alert-success",
					'status_id' => "2",
					'type' => "Success"
				]);
				redirect('admin/partner_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "2",
					'type' => "Error"
				];
				redirect('admin/partner_config');
			}
		}
	}else show_404();

	}
	public function civiliteDelete(){
	$this->load->model('partner_config_model');
	$id=$_POST['delet_civilite_id'];
	$table = "vbs_partnercivilite";
	$del=$this->partner_config_model->partner_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "2",
			'type' => "Success"
	]);
	redirect('admin/partner_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "2",
				'type' => "Error"
		]);
		redirect('admin/partner_config');
	}
	 show_404();
	}
	public function get_ajax_civilite(){
		$id=(int)$_GET['civilite_id'];

		if($id>0){
			$this->load->model('partner_config_model');
			$civilite_data = $this->partner_config_model->partner_Config_getAll('vbs_partnercivilite',['id' => $id]);
			
			foreach($civilite_data as $key => $civilite){
				$result='<input type="hidden" name="civilite_id" value="'.$civilite->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($civilite->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($civilite->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Civilite</span>
					<input type="text" class="form-control" name="civilite" placeholder="" value="'.$civilite->civilite.'">
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	// type 
	public function typeAdd(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->typestore();
		}else show_404();
	}
	public function typestore(){
		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_partnertype';
			$id = $this->partner_config_model->partner_Config_Add($table,[
				'type' 		=> @$_POST['type'],
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a type .",
				'class' => "alert-success",
				'status_id' => "3",
				'type' => "Success"
			]);
			redirect('admin/partner_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'status_id' => "3",
				'type' => "Error"
			];
			redirect('admin/partner_config');
		}
	}
	public function typeEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['type_id'];
			if (!empty($id)) {
			$table='vbs_partnertype';
			$update=$this->partner_config_model->partner_Config_Update($table,[
				'type' 		=> @$_POST['type'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Type.",
					'class' => "alert-success",
					'status_id' => "3",
					'type' => "Success"
				]);
				redirect('admin/partner_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "3",
					'type' => "Error"
				];
				redirect('admin/partner_config');
			}
		}
	}else show_404();

	}
	public function typeDelete(){
	$this->load->model('partner_config_model');
	$id=$_POST['delet_type_id'];
	$table = "vbs_partnertype";
	$del=$this->partner_config_model->partner_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "3",
			'type' => "Success"
	]);
	redirect('admin/partner_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "3",
				'type' => "Error"
		]);
		redirect('admin/partner_config');
	}
	 show_404();
	}
	public function get_ajax_type(){
		$id=(int)$_GET['type_id'];

		if($id>0){
			$this->load->model('partner_config_model');
			$type_data = $this->partner_config_model->partner_Config_getAll('vbs_partnertype',['id' => $id]);
			
			foreach($type_data as $key => $type){
				$result='<input type="hidden" name="type_id" value="'.$type->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($type->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($type->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Type de Piece didentite</span>
					<input type="text" class="form-control" name="type" placeholder="" value="'.$type->type.'">
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	
}
