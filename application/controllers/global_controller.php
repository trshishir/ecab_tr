<?php
/**
 * Created by PhpStorm.
 * User: minhaj
 * Date: 6/10/2020
 * Time: 1:49 AM
 */

class Global_controller extends MY_Controller
{
    public function __construct() {

        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('language');
    }

    public function set_language($lang)
    {
        $this->session->set_userdata('lang', $lang);
        if (empty($_SERVER['HTTP_REFERER'])) {
            redirect('admin/dashboard');
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
}