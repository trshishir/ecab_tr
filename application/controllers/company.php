<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Company extends MY_Controller
{
    function __construct(){
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('general');
        $this->load->helper('language');
        if (!$this->basic_auth->is_login()) {
            redirect("admin", 'refresh');
        } else {
            $this->data['user'] = $this->basic_auth->user();
        }
        $this->load->model('company_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');
        $this->load->model('support_model');
        $this->load->model('calls_model');

        $this->data['configuration'] = get_configuration();
    }

    public function index(){
        $this->data['css_type'] = array("form");
        $this->data['active_class'] = "Company Profile";
        $this->data['gmaps'] 	= false;
        $this->data['title'] 	= 'Company Profile';
        $this->data['title_link'] 	= base_url('admin/company');
        $this->data['company']  = $this->company_model->getFirst();
        $this->data['countries'] = $this->cms_model->get_all_countries();

        $this->data['content']  = 'admin/company/index';
        $this->_render_page('templates/admin_template', $this->data);
    }

    public function save(){


// echo '<pre>';
// var_dump($this->input->post());
// die();

        extract($this->input->post());

        $this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('phone', 'Phone', 'required');


        /*		$this->form_validation->set_rules('fax', 'Fax', 'required');
                $this->form_validation->set_rules('website', 'Website', 'required');
                $this->form_validation->set_rules('street', 'Street', 'required');
                $this->form_validation->set_rules('zip_code', 'Zip Code', 'required');
                $this->form_validation->set_rules('city', 'City', 'required');
                $this->form_validation->set_rules('country', 'Country', 'required');
                $this->form_validation->set_rules('sirft', 'SIRFT', 'required');
                $this->form_validation->set_rules('rcs', 'RCS', 'required');
                $this->form_validation->set_rules('licence', 'Licence', 'required');
                $this->form_validation->set_rules('numero_tva', 'NUMERO TVA', 'required');
                $this->form_validation->set_rules('capital', 'Capital', 'required');
                $this->form_validation->set_rules('facebook_link', 'Facebook Link', 'required');
                $this->form_validation->set_rules('youtube_link', 'Youtube Link', 'required');
                $this->form_validation->set_rules('instagram_link', 'Instagram Link', 'required');
        */

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('admin/company');
        } else {
            $path_to_folder = 'uploads/company/';
            $config['upload_path'] = realpath($path_to_folder);
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['max_size']     = '3000000'; // 3mb

            $old_logo = $this->input->post('old_logo');
            $path_to_folder_old = $path_to_folder.$old_logo;
            $new_name = uniqid('COM_', true);
            $new_name = explode('.', $new_name)[0];
            $config['file_name'] = $new_name;

            $this->load->library('upload', $config);
            $logo = $old_logo;
            if($this->upload->do_upload('logo')) {
                $logo = $this->upload->data();
                $logo = $logo['file_name'];
                if(is_file($path_to_folder_old)) {
                    unlink($path_to_folder_old);
                }
            }


            if (! empty($_FILES['footer_logo']['name'])) {
                // File upload configuration
                $config['upload_path'] =  'uploads/company/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '1024';
                $config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                // Create file upload info
                $_FILES['photo']['name'] = $_FILES['footer_logo']['name'];
                $_FILES['photo']['type'] = $_FILES['footer_logo']['type'];
                $_FILES['photo']['tmp_name'] = $_FILES['footer_logo']['tmp_name'];
                $_FILES['photo']['error'] = $_FILES['footer_logo']['error'];
                $_FILES['photo']['size'] = $_FILES['footer_logo']['size'];

                // Upload file to server
                if ($this->upload->do_upload('photo')) {

                    $footer_logo=   $this->upload->data();
                    $company_footer_logo = $footer_logo['file_name'];
                }
                else{
                    // var_dump($this->upload->display_errors());
                }
            }
            else{
                $company_footer_logo = $footer_logo;
            }


            if (! empty($_FILES['website_bg']['name'])) {
                // File upload configuration
                $config['upload_path'] =  'uploads/company/';
                $config['allowed_types'] = '*';
                $config['max_size'] = '1024';
                $config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                // Create file upload info
                $_FILES['photo']['name'] = $_FILES['website_bg']['name'];
                $_FILES['photo']['type'] = $_FILES['website_bg']['type'];
                $_FILES['photo']['tmp_name'] = $_FILES['website_bg']['tmp_name'];
                $_FILES['photo']['error'] = $_FILES['website_bg']['error'];
                $_FILES['photo']['size'] = $_FILES['website_bg']['size'];

                // Upload file to server
                if ($this->upload->do_upload('photo')) {

                    $website_bg=   $this->upload->data();
                    $company_website_bg = $website_bg['file_name'];
                }
                else{
                    // var_dump($this->upload->display_errors());
                }
            }
            else{
                $company_website_bg = $website_bg;
            }


// var_dump($_FILES['audio_file']);
            if (! empty($_FILES['audio_file']['name'])) {
                // File upload configuration
                $config['upload_path'] =  'assets/system_design/audio/';
                $config['allowed_types'] = '*';
                $config['max_size'] = '1024';
                $config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                // Create file upload info
                $_FILES['photo']['name'] = $_FILES['audio_file']['name'];
                $_FILES['photo']['type'] = $_FILES['audio_file']['type'];
                $_FILES['photo']['tmp_name'] = $_FILES['audio_file']['tmp_name'];
                $_FILES['photo']['error'] = $_FILES['audio_file']['error'];
                $_FILES['photo']['size'] = $_FILES['audio_file']['size'];

                // Upload file to server
                if ($this->upload->do_upload('photo')) {

                    $audio_file=   $this->upload->data();
                    $company_audio_file = $audio_file['file_name'];
                }
                else{
                    // var_dump($this->upload->display_errors());
                }
            }
            else{
                $company_audio_file = $audio_file;
            }



// var_dump($this->input->post());

// die();


            $db_array = array(
                'type' 		=> $this->input->post('type'),
                'name' 		=> $this->input->post('name'),
                'email' 	=> $this->input->post('email'),
                'phone' 	=> $this->input->post('phone'),
                'fax' 		=> $this->input->post('fax'),
                'website' 	=> $this->input->post('website'),
                'logo' 		=> $logo,
                'footer_logo'   => $company_footer_logo,
                'address' 	=> $this->input->post('address'),
                'address2'    => $this->input->post('address2'),
                'zip_code' 	=> $this->input->post('zip_code'),
                'city' 		=> $this->input->post('city'),
                'region' 	=> $this->input->post('region'),
                'country'   => $this->input->post('country'),
                'sirft' 	=> $this->input->post('sirft'),
                'rcs' 		=> $this->input->post('rcs'),
                'licence' 	=> $this->input->post('licence'),
                'numero_tva' => $this->input->post('numero_tva'),
                'capital' 	 => $this->input->post('capital'),
                'facebook_link' 	=> $this->input->post('facebook_link'),
                'twitter_link' 		=> $this->input->post('twitter_link'),
                'linkedin_link'         => $this->input->post('linkedin_link'),
                'youtube_link'         => $this->input->post('youtube_link'),
                'instagram_link'         => $this->input->post('instagram_link'),
                'radio_link'    => $this->input->post('radio_link'),
                'radio_autoplay'      => (isset($radio_autoplay))?(int)$radio_autoplay:0,
                'front_autoplay'      => (isset($front_autoplay))?(int)$front_autoplay:0,
                'front_autoplay_repeat'      => ($front_autoplay_repeat)?(int)$front_autoplay_repeat:0,
                // 'weather_widget_location'       => $this->input->post('weather_widget_location'),
                'default_front_language'      => $this->input->post('default_front_language'),
                'default_front_language_status'=> $this->input->post('default_front_language_status'),
                'default_front_currency'      => $this->input->post('default_front_currency'),
                'default_front_currency_status'      => $this->input->post('default_front_currency_status'),
                'default_admin_language'      => $this->input->post('default_admin_language'),
                'default_admin_language_status'      => $this->input->post('default_admin_language_status'),
                'default_admin_currency'      => $this->input->post('default_admin_currency'),
                'default_admin_currency_status'      => $this->input->post('default_admin_currency_status'),
                'default_area_language'      => $this->input->post('default_area_language'),
                'default_area_language_status'      => $this->input->post('default_area_language_status'),
                'default_area_currency'      => $this->input->post('default_area_currency'),
                'default_area_currency_status'      => $this->input->post('default_area_currency_status'),
                'default_unity'         => $this->input->post('default_unity'),
                'price_calculation_method'      => $this->input->post('price_calculation_method'),
                'payment_gatways' 	=> $this->input->post('payment_gatways'),



                'audio_file'       => $company_audio_file,
                'website_bg'       => $company_website_bg,
                'from_day'         => json_encode($from_day),
                'to_day'         => json_encode($to_day),
                'start_time_1'       => json_encode($start_time_1),
                'start_time_2'       => json_encode($start_time_2),
                'end_time_1'       => json_encode($end_time_1),
                'end_time_2'       => json_encode($end_time_2),
                'ios_client'       => $ios_client,
                'ios_driver'       => $ios_driver,
                'android_client'       => $android_client,
                'android_driver'       => $android_driver,



            );

            $company = $this->company_model->getFirst();

            if($company != false)
                $this->company_model->update($db_array, $company['id']);
            else
                $this->company_model->insert($db_array);

            $this->session->set_flashdata('alert', [
                'message' 	=> "Successfully Updated.",
                'class' 	=> "alert-success",
                'type' 		=> "Success"
            ]);

            redirect('admin/company');
        }
    }
}

