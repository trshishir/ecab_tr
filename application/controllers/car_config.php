<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Car_config extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->lang->load('auth');
		$this->lang->load('general');
		$this->load->helper('language');
		$this->load->helper('validate');
		if (!$this->basic_auth->is_login())
			redirect("admin", 'refresh');
		else
		$this->data['user'] = $this->basic_auth->user();
		// $this->load->model('cars_model');
		$this->load->model('car_config_model');
		$this->load->model('cars_model');
		$this->load->model('request_model');
		$this->load->model('jobs_model');
		$this->load->model('support_model');
		$this->load->model('calls_model');
		$this->load->model('notes_model');
		$this->load->model('notifications_model');
		$this->data['configuration'] = get_configuration();

	}

	
	public function index(){

		$this->data['user'] = $this->basic_auth->user();
		$this->data['css_type'] 	= array("form","booking_datatable","datatable");
		$this->data['active_class'] = "car";
		$this->data['gmaps'] 		= false;
		$this->data['title'] 		= 'Car Configurations';
		$this->data['title_link'] 	= base_url('admin/car_config');
		$this->data['content'] 		= 'admin/car_config/index';
   		$data = [];
		$this->data['car_status_data'] = $this->car_config_model->getAllData('vbs_carstatut');
		$this->data['car_marque_data'] = $this->car_config_model->getAllData('vbs_carmarque');
		$this->data['car_civilite_data'] = $this->car_config_model->getAllData('vbs_carcivilite');
		$this->data['car_modele_data'] = $this->car_config_model->getAllData('vbs_carmodele');
		$this->data['car_type_data'] = $this->car_config_model->getAllData('vbs_cartype');
		$this->data['car_nature_data'] = $this->car_config_model->getAllData('vbs_carnature');
		$this->data['car_couleur_data'] = $this->car_config_model->getAllData('vbs_carcouleur');
		$this->data['car_courroie_data'] = $this->car_config_model->getAllData('vbs_carcourroie');
		$this->data['car_burant_data'] = $this->car_config_model->getAllData('vbs_carcarburant');
		$this->data['car_serie_data'] = $this->car_config_model->getAllData('vbs_carserie');
		$this->data['car_boite_data'] = $this->car_config_model->getAllData('vbs_carboite');
		
		$this->data['car_age_data'] = $this->car_config_model->getAllData('vbs_carage');
		
		
		$data['jobs'] 	 = $this->jobs_model->getAll();
		$data['calls']   = $this->calls_model->getAll();
		$this->data['data'] = $this->car_config_model->getAll();
		$this->_render_page('templates/admin_template', $this->data);
	}
	public function carStatusadd(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->carStatusstore();
		}else{
			return redirect("admin/car_config");
		}
		


	}
	public function carStatusstore(){
			// $error = request_validate();
			if (empty($error)) {
				//$dob = to_unix_date(@$_POST['dob']);
				$user = $this->basic_auth->user();
			 	$user_id=$user->id;

				$id = $this->car_config_model->car_Config_Add('vbs_carstatut',[
					'statut' 		=> @$_POST['statut'],
					'name' 		=> @$_POST['name'],
					'user_id' =>$user_id
				]);
			
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully added a car statut .",
					'class' => "alert-success",
					'status_id' => "1",
					'type' => "Success"
				]);
				redirect('admin/car_config');
			} else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/car_config');
			}
	}
	public function carStatusEdit(){
		
		
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['car_status_id'];
			if (!empty($id)) {
			$update=$this->car_config_model->car_Config_Update('vbs_carstatut',[
				'statut' 		=> @$_POST['statut'],
			    'name' 		    => @$_POST['name'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a car statut.",
					'class' => "alert-success",
					'status_id' => "1",
					'type' => "Success"
				]);
				redirect('admin/car_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/car_config');
			}
		}
	}else{
		redirect('admin/car_config');
	}
		
	}
	public function deleteCarStatus(){
		
		$id=$_POST['delet_car_status_id'];
		$del=$this->car_config_model->car_Config_Delete('vbs_carstatut',$id);
		if ($del==true) {
		$this->session->set_flashdata('alert', [
				'message' => "Successfully deleted.",
				'class' => "alert-success",
				'status_id' => "1",
				'type' => "Success"
		]);
		redirect('admin/car_config');
		}
		else {
			$this->session->set_flashdata('alert', [
					'message' => "Please try again.",
					'class' => "alert-danger",
					'status_id' => "1",
					'type' => "Error"
			]);
			redirect('admin/car_config');
		}
	}
	public function get_ajax_carStatus(){
		$id=(int)$_GET['car_status_id'];

		if($id>0){
			$this->load->model('car_config_model');
			$car_status_data = $this->car_config_model->car_Config_getAll('vbs_carstatut',['id' => $id]);
			
			foreach($car_status_data as $key => $carStatus){
				$result='<input type="hidden" name="car_status_id" value="'.$carStatus->id.'">
			 <div class="col-md-12"> 
			 	<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($carStatus->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($carStatus->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div> 
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Name</span>
					<input type="text" class="form-control" name="name" placeholder="" value="'.$carStatus->name.'" required>
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}

	public function civiliteAdd(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->civilitestore();
		}else show_404();
	}
	public function civilitestore(){

		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_carcivilite';
			$id = $this->car_config_model->car_Config_Add($table,[
				'civilite' 		=> @$_POST['civilite'],
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a civilite .",
				'class' => "alert-success",
				'status_id' => "2",
				'type' => "Success"
			]);
			redirect('admin/car_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'type' => "Error"
			];
			redirect('admin/car_config');
		}
	}
	public function civiliteEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['civilite_id'];
			if (!empty($id)) {
			$table='vbs_carcivilite';
			$update=$this->car_config_model->car_Config_Update($table,[
				'civilite' 		=> @$_POST['civilite'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Civilite.",
					'class' => "alert-success",
					'status_id' => "2",
					'type' => "Success"
				]);
				redirect('admin/car_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'type' => "Error"
				];
				redirect('admin/car_config');
			}
		}
	}else show_404();

	}
	public function civiliteDelete(){
	$this->load->model('car_config_model');
	$id=$_POST['delet_civilite_id'];
	$table = "vbs_carcivilite";
	$del=$this->car_config_model->car_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "2",
			'type' => "Success"
	]);
	redirect('admin/car_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "2",
				'type' => "Error"
		]);
		redirect('admin/car_config');
	}
	 show_404();
	}
	public function get_ajax_civilite(){
		$id=(int)$_GET['civilite_id'];

		if($id>0){
			$this->load->model('car_config_model');
			$civilite_data = $this->car_config_model->car_Config_getAll('vbs_carcivilite',['id' => $id]);
			
			foreach($civilite_data as $key => $civilite){
				$result='<input type="hidden" name="civilite_id" value="'.$civilite->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($civilite->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($civilite->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Civilite</span>
					<input type="text" class="form-control" name="civilite" placeholder="" value="'.$civilite->civilite.'">
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
//marque
	public function marqueAdd(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->marquestore();
		}else show_404();
	}
	public function marquestore(){

		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_carmarque';
			$id = $this->car_config_model->car_Config_Add($table,[
				'marque' 		=> @$_POST['marque'],
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a marque .",
				'class' => "alert-success",
				'status_id' => "3",
				'type' => "Success"
			]);
			redirect('admin/car_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'status_id' => "3",
				'type' => "Error"
			];
			redirect('admin/car_config');
		}
	}
	public function marqueEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['marque_id'];
			if (!empty($id)) {
			$table='vbs_carmarque';
			$update=$this->car_config_model->car_Config_Update($table,[
				'marque' 		=> @$_POST['marque'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Marque.",
					'class' => "alert-success",
					'status_id' => "3",
					'type' => "Success"
				]);
				redirect('admin/car_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "3",
					'type' => "Error"
				];
				redirect('admin/car_config');
			}
		}
	}else show_404();

	}
	public function marqueDelete(){
	$this->load->model('car_config_model');
	$id=$_POST['delet_marque_id'];
	$table = "vbs_carmarque";
	$del=$this->car_config_model->car_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "3",
			'type' => "Success"
	]);
	redirect('admin/car_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "3",
				'type' => "Error"
		]);
		redirect('admin/car_config');
	}
	 show_404();
	}
	public function get_ajax_marque(){
		$id=(int)$_GET['marque_id'];

		if($id>0){
			$this->load->model('car_config_model');
			$marque_data = $this->car_config_model->car_Config_getAll('vbs_carmarque',['id' => $id]);
			
			foreach($marque_data as $key => $marque){
				$result='<input type="hidden" name="marque_id" value="'.$marque->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($marque->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($marque->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Marque</span>
					<input type="text" class="form-control" name="marque" placeholder="" value="'.$marque->marque.'">
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	//modele
	public function modeleAdd(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->modelestore();
		}else show_404();
	}
	public function modelestore(){

		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_carmodele';
			$id = $this->car_config_model->car_Config_Add($table,[
				'modele' 		=> @$_POST['modele'],
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a modele .",
				'class' => "alert-success",
				'status_id' => "4",
				'type' => "Success"
			]);
			redirect('admin/car_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'status_id' => "4",
				'type' => "Error"
			];
			redirect('admin/car_config');
		}
	}
	public function modeleEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['modele_id'];
			if (!empty($id)) {
			$table='vbs_carmodele';
			$update=$this->car_config_model->car_Config_Update($table,[
				'modele' 		=> @$_POST['modele'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Modele.",
					'class' => "alert-success",
					'status_id' => "4",
					'type' => "Success"
				]);
				redirect('admin/car_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "4",
					'type' => "Error"
				];
				redirect('admin/car_config');
			}
		}
	}else show_404();

	}
	public function modeleDelete(){
	$this->load->model('car_config_model');
	$id=$_POST['delet_modele_id'];
	$table = "vbs_carmodele";
	$del=$this->car_config_model->car_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "4",
			'type' => "Success"
	]);
	redirect('admin/car_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "4",
				'type' => "Error"
		]);
		redirect('admin/car_config');
	}
	 show_404();
	}
	public function get_ajax_modele(){
		$id=(int)$_GET['modele_id'];

		if($id>0){
			$this->load->model('car_config_model');
			$modele_data = $this->car_config_model->car_Config_getAll('vbs_carmodele',['id' => $id]);
			
			foreach($modele_data as $key => $modele){
				$result='<input type="hidden" name="modele_id" value="'.$modele->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($modele->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($modele->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Modele</span>
					<input type="text" class="form-control" name="modele" placeholder="" value="'.$modele->modele.'">
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	//age
	public function ageAdd(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->agestore();
		}else show_404();
	}
	public function agestore(){

		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_carage';
			$id = $this->car_config_model->car_Config_Add($table,[
				'age' 		=> @$_POST['age'],
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a age .",
				'class' => "alert-success",
				'status_id' => "5",
				'type' => "Success"
			]);
			redirect('admin/car_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'status_id' => "5",
				'type' => "Error"
			];
			redirect('admin/car_config');
		}
	}
	public function ageEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['age_id'];
			if (!empty($id)) {
			$table='vbs_carage';
			$update=$this->car_config_model->car_Config_Update($table,[
				'age' 		=> @$_POST['age'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Age du Vehicule.",
					'class' => "alert-success",
					'status_id' => "5",
					'type' => "Success"
				]);
				redirect('admin/car_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "5",
					'type' => "Error"
				];
				redirect('admin/car_config');
			}
		}
	}else show_404();

	}
	public function ageDelete(){
	$this->load->model('car_config_model');
	$id=$_POST['delet_age_id'];
	$table = "vbs_carage";
	$del=$this->car_config_model->car_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "5",
			'type' => "Success"
	]);
	redirect('admin/car_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "5",
				'type' => "Error"
		]);
		redirect('admin/car_config');
	}
	 show_404();
	}
	public function get_ajax_age(){
		$id=(int)$_GET['age_id'];

		if($id>0){
			$this->load->model('car_config_model');
			$age_data = $this->car_config_model->car_Config_getAll('vbs_carage',['id' => $id]);
			
			foreach($age_data as $key => $age){
				$result='<input type="hidden" name="age_id" value="'.$age->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($age->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($age->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Age du Vehicule</span>
					<input type="text" class="form-control" name="age" placeholder="" value="'.$age->age.'">
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	//serie
	public function serieAdd(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->seriestore();
		}else show_404();
	}
	public function seriestore(){

		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_carserie';
			$id = $this->car_config_model->car_Config_Add($table,[
				'serie' 		=> @$_POST['serie'],
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a serie .",
				'class' => "alert-success",
				'status_id' => "6",
				'type' => "Success"
			]);
			redirect('admin/car_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'status_id' => "6",
				'type' => "Error"
			];
			redirect('admin/car_config');
		}
	}
	public function serieEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['serie_id'];
			if (!empty($id)) {
			$table='vbs_carserie';
			$update=$this->car_config_model->car_Config_Update($table,[
				'serie' 		=> @$_POST['serie'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Serie.",
					'class' => "alert-success",
					'status_id' => "6",
					'type' => "Success"
				]);
				redirect('admin/car_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "6",
					'type' => "Error"
				];
				redirect('admin/car_config');
			}
		}
	}else show_404();

	}
	public function serieDelete(){
	$this->load->model('car_config_model');
	$id=$_POST['delet_serie_id'];
	$table = "vbs_carserie";
	$del=$this->car_config_model->car_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "6",
			'type' => "Success"
	]);
	redirect('admin/car_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "6",
				'type' => "Error"
		]);
		redirect('admin/car_config');
	}
	 show_404();
	}
	public function get_ajax_serie(){
		$id=(int)$_GET['serie_id'];

		if($id>0){
			$this->load->model('car_config_model');
			$serie_data = $this->car_config_model->car_Config_getAll('vbs_carserie',['id' => $id]);
			
			foreach($serie_data as $key => $serie){
				$result='<input type="hidden" name="serie_id" value="'.$serie->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($serie->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($serie->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Serie</span>
					<input type="text" class="form-control" name="serie" placeholder="" value="'.$serie->serie.'">
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	//boite
	public function boiteAdd(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->boitestore();
		}else show_404();
	}
	public function boitestore(){

		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_carboite';
			$id = $this->car_config_model->car_Config_Add($table,[
				'boite' 		=> @$_POST['boite'],
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a boite .",
				'class' => "alert-success",
				'status_id' => "7",
				'type' => "Success"
			]);
			redirect('admin/car_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'status_id' => "7",
				'type' => "Error"
			];
			redirect('admin/car_config');
		}
	}
	public function boiteEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['boite_id'];
			if (!empty($id)) {
			$table='vbs_carboite';
			$update=$this->car_config_model->car_Config_Update($table,[
				'boite' 		=> @$_POST['boite'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Boite.",
					'class' => "alert-success",
					'status_id' => "7",
					'type' => "Success"
				]);
				redirect('admin/car_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "7",
					'type' => "Error"
				];
				redirect('admin/car_config');
			}
		}
	}else show_404();

	}
	public function boiteDelete(){
	$this->load->model('car_config_model');
	$id=$_POST['delet_boite_id'];
	$table = "vbs_carboite";
	$del=$this->car_config_model->car_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "7",
			'type' => "Success"
	]);
	redirect('admin/car_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "7",
				'type' => "Error"
		]);
		redirect('admin/car_config');
	}
	 show_404();
	}
	public function get_ajax_boite(){
		$id=(int)$_GET['boite_id'];

		if($id>0){
			$this->load->model('car_config_model');
			$boite_data = $this->car_config_model->car_Config_getAll('vbs_carboite',['id' => $id]);
			
			foreach($boite_data as $key => $boite){
				$result='<input type="hidden" name="boite_id" value="'.$boite->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($boite->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($boite->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Boite</span>
					<input type="text" class="form-control" name="boite" placeholder="" value="'.$boite->boite.'">
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	//burant
	public function burantAdd(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->burantstore();
		}else show_404();
	}
	public function burantstore(){

		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_carcarburant';
			$id = $this->car_config_model->car_Config_Add($table,[
				'carburant' 		=> @$_POST['burant'],
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a burant .",
				'class' => "alert-success",
				'status_id' => "8",
				'type' => "Success"
			]);
			redirect('admin/car_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'status_id' => "8",
				'type' => "Error"
			];
			redirect('admin/car_config');
		}
	}
	public function burantEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['burant_id'];
			if (!empty($id)) {
			$table='vbs_carcarburant';
			$update=$this->car_config_model->car_Config_Update($table,[
				'carburant' 		=> @$_POST['burant'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Burant.",
					'class' => "alert-success",
					'status_id' => "8",
					'type' => "Success"
				]);
				redirect('admin/car_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "8",
					'type' => "Error"
				];
				redirect('admin/car_config');
			}
		}
	}else show_404();

	}
	public function burantDelete(){
	$this->load->model('car_config_model');
	$id=$_POST['delet_burant_id'];
	$table = "vbs_carcarburant";
	$del=$this->car_config_model->car_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "8",
			'type' => "Success"
	]);
	redirect('admin/car_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "8",
				'type' => "Error"
		]);
		redirect('admin/car_config');
	}
	 show_404();
	}
	public function get_ajax_burant(){
		$id=(int)$_GET['burant_id'];

		if($id>0){
			$this->load->model('car_config_model');
			$burant_data = $this->car_config_model->car_Config_getAll('vbs_carcarburant',['id' => $id]);
			
			foreach($burant_data as $key => $burant){
				$result='<input type="hidden" name="burant_id" value="'.$burant->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($burant->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($burant->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Burant</span>
					<input type="text" class="form-control" name="burant" placeholder="" value="'.$burant->carburant.'">
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	//courroie
	public function courroieAdd(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->courroiestore();
		}else show_404();
	}
	public function courroiestore(){

		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_carcourroie';
			$id = $this->car_config_model->car_Config_Add($table,[
				'courroie' 		=> @$_POST['courroie'],
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a courroie .",
				'class' => "alert-success",
				'status_id' => "9",
				'type' => "Success"
			]);
			redirect('admin/car_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'status_id' => "9",
				'type' => "Error"
			];
			redirect('admin/car_config');
		}
	}
	public function courroieEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['courroie_id'];
			if (!empty($id)) {
			$table='vbs_carcourroie';
			$update=$this->car_config_model->car_Config_Update($table,[
				'courroie' 		=> @$_POST['courroie'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Courroie.",
					'class' => "alert-success",
					'status_id' => "9",
					'type' => "Success"
				]);
				redirect('admin/car_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "9",
					'type' => "Error"
				];
				redirect('admin/car_config');
			}
		}
	}else show_404();

	}
	public function courroieDelete(){
	$this->load->model('car_config_model');
	$id=$_POST['delet_courroie_id'];
	$table = "vbs_carcourroie";
	$del=$this->car_config_model->car_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "9",
			'type' => "Success"
	]);
	redirect('admin/car_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "9",
				'type' => "Error"
		]);
		redirect('admin/car_config');
	}
	 show_404();
	}
	public function get_ajax_courroie(){
		$id=(int)$_GET['courroie_id'];

		if($id>0){
			$this->load->model('car_config_model');
			$courroie_data = $this->car_config_model->car_Config_getAll('vbs_carcourroie',['id' => $id]);
			
			foreach($courroie_data as $key => $courroie){
				$result='<input type="hidden" name="courroie_id" value="'.$courroie->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($courroie->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($courroie->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Courroie</span>
					<input type="text" class="form-control" name="courroie" placeholder="" value="'.$courroie->courroie.'">
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	//couleur
	public function couleurAdd(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->couleurstore();
		}else show_404();
	}
	public function couleurstore(){

		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_carcouleur';
			$id = $this->car_config_model->car_Config_Add($table,[
				'couleur' 		=> @$_POST['couleur'],
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a couleur .",
				'class' => "alert-success",
				'status_id' => "10",
				'type' => "Success"
			]);
			redirect('admin/car_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'status_id' => "10",
				'type' => "Error"
			];
			redirect('admin/car_config');
		}
	}
	public function couleurEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['couleur_id'];
			if (!empty($id)) {
			$table='vbs_carcouleur';
			$update=$this->car_config_model->car_Config_Update($table,[
				'couleur' 		=> @$_POST['couleur'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Couleur.",
					'class' => "alert-success",
					'status_id' => "10",
					'type' => "Success"
				]);
				redirect('admin/car_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "10",
					'type' => "Error"
				];
				redirect('admin/car_config');
			}
		}
	}else show_404();

	}
	public function couleurDelete(){
	$this->load->model('car_config_model');
	$id=$_POST['delet_couleur_id'];
	$table = "vbs_carcouleur";
	$del=$this->car_config_model->car_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "10",
			'type' => "Success"
	]);
	redirect('admin/car_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "10",
				'type' => "Error"
		]);
		redirect('admin/car_config');
	}
	 show_404();
	}
	public function get_ajax_couleur(){
		$id=(int)$_GET['couleur_id'];

		if($id>0){
			$this->load->model('car_config_model');
			$couleur_data = $this->car_config_model->car_Config_getAll('vbs_carcouleur',['id' => $id]);
			
			foreach($couleur_data as $key => $couleur){
				$result='<input type="hidden" name="couleur_id" value="'.$couleur->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($couleur->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($couleur->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Couleur</span>
					<input type="text" class="form-control" name="couleur" placeholder="" value="'.$couleur->couleur.'">
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	//nature
	public function natureAdd(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->naturestore();
		}else show_404();
	}
	public function naturestore(){

		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_carnature';
			$id = $this->car_config_model->car_Config_Add($table,[
				'nature' 		=> @$_POST['nature'],
				'statut' 		=> @$_POST['statut'],
				'user_id' =>$user_id
			]);
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a nature .",
				'class' => "alert-success",
				'status_id' => "11",
				'type' => "Success"
			]);
			redirect('admin/car_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'status_id' => "11",
				'type' => "Error"
			];
			redirect('admin/car_config');
		}
	}
	public function natureEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['nature_id'];
			if (!empty($id)) {
			$table='vbs_carnature';
			$update=$this->car_config_model->car_Config_Update($table,[
				'nature' 		=> @$_POST['nature'],
				'statut' 		=> @$_POST['statut'],
			], $id);
			if ($update==true) {
				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Nature.",
					'class' => "alert-success",
					'status_id' => "11",
					'type' => "Success"
				]);
				redirect('admin/car_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "11",
					'type' => "Error"
				];
				redirect('admin/car_config');
			}
		}
	}else show_404();

	}
	public function natureDelete(){
	$this->load->model('car_config_model');
	$id=$_POST['delet_nature_id'];
	$table = "vbs_carnature";
	$del=$this->car_config_model->car_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "11",
			'type' => "Success"
	]);
	redirect('admin/car_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "11",
				'type' => "Error"
		]);
		redirect('admin/car_config');
	}
	 show_404();
	}
	public function get_ajax_nature(){
		$id=(int)$_GET['nature_id'];

		if($id>0){
			$this->load->model('car_config_model');
			$nature_data = $this->car_config_model->car_Config_getAll('vbs_carnature',['id' => $id]);
			
			foreach($nature_data as $key => $nature){
				$result='<input type="hidden" name="nature_id" value="'.$nature->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($nature->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($nature->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Nature</span>
					<input type="text" class="form-control" name="nature" placeholder="" value="'.$nature->nature.'">
				</div>
				</div>
				</div>';
			}
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	//type
	public function typeAdd(){

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->typestore();
		}else show_404();
	}
	public function typestore(){

		// $error = request_validate();
		if (empty($error)) {
			//$dob = to_unix_date(@$_POST['dob']);
			$user = $this->basic_auth->user();
			$user_id=$user->id;
			$table='vbs_cartype';
			$id = $this->car_config_model->car_Config_Add($table,[
				'type_name' 		=> @$_POST['type'],
				'statut' 	    	=> @$_POST['statut'],
				'car_icon'          => $this->do_user_upload_image($_FILES['car_icon'],'car_icon'),
				'user_id' =>$user_id
			]);
			$passengercap=$this->input->post('passengercap');
            $lugagescap=$this->input->post("lugagescap");
            $wheelchairscap=$this->input->post("wheelchairscap");
            $babycap=$this->input->post("babycap");
            $animalscap=$this->input->post("animalscap");
             for($i=0;$i<count($passengercap);$i++){
                  $cid = $this->car_config_model->car_Config_Add("vbs_type_of_car_configuration",[
              'passengers' =>$passengercap[$i],
              'baby'       =>$babycap[$i],
              'wheelchairs'=>$wheelchairscap[$i],
              'luggage'    =>$lugagescap[$i],
              'animals'    =>$animalscap[$i],
              'type_id'   =>$id
              ]);
            }
			$this->session->set_flashdata('alert', [
				'message' => ": Successfully added a type .",
				'class' => "alert-success",
				'status_id' => "12",
				'type' => "Success"
			]);
			redirect('admin/car_config');
		} else {
			$this->data['alert'] = [
				'message' => 'Please try again',
				'class' => "alert-danger",
				'status_id' => "12",
				'type' => "Error"
			];
			redirect('admin/car_config');
		}
	}
	public function typeEdit(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=@$_POST['type_id'];
			if (!empty($id)) {

			$table='vbs_cartype';
			$caricon='';
			$record=$this->car_config_model->getSingleRecord($table,['id'=>$id]);
			
			    if(empty($_FILES['car_icon']['name']))
                 {
                  $caricon=$record->car_icon;
                 }else{
                  $caricon = $this->do_user_upload_image($_FILES['car_icon'],'car_icon');
                 }

			$update=$this->car_config_model->car_Config_Update($table,[
				'type_name' 		=> @$_POST['type'],
				'statut'            => @$_POST['statut'],
				'car_icon'          =>  $caricon
				

			], $id);
			if ($update==true) {
              $configdel=$this->car_config_model->car_Config_Delete_Where('vbs_type_of_car_configuration',['type_id'=>$id]);
           
           if($this->input->post("passengercap")){
            $passengercap=$this->input->post('passengercap');
            $lugagescap=$this->input->post("lugagescap");
            $wheelchairscap=$this->input->post("wheelchairscap");
            $babycap=$this->input->post("babycap");
            $animalscap=$this->input->post("animalscap");

              for($i=0;$i<count($passengercap);$i++){
            			$cid = $this->car_config_model->car_Config_Add("vbs_type_of_car_configuration",[
							'passengers' =>$passengercap[$i],
							'baby' 		 =>$babycap[$i],
							'wheelchairs'=>$wheelchairscap[$i],
							'luggage' 	 =>$lugagescap[$i],
							'animals' 	 =>$animalscap[$i],
							'type_id' 	 =>$id
		  				]);
                     }
              } 

				$this->session->set_flashdata('alert', [
					'message' => ": Successfully Updated a Type.",
					'class' => "alert-success",
					'status_id' => "12",
					'type' => "Success"
				]);
				redirect('admin/car_config');
			}else {
				$this->data['alert'] = [
					'message' => 'Please try again',
					'class' => "alert-danger",
					'status_id' => "12",
					'type' => "Error"
				];
				redirect('admin/car_config');
			}
		}
	}else show_404();

	}
	public function typeDelete(){
	$this->load->model('car_config_model');
	$id=$_POST['delet_type_id'];
	$table = "vbs_cartype";
	$del=$this->car_config_model->car_Config_Delete($table,$id);
	if ($del==true) {
	$this->session->set_flashdata('alert', [
			'message' => "Successfully deleted.",
			'class' => "alert-success",
			'status_id' => "12",
			'type' => "Success"
	]);
	redirect('admin/car_config');
	}
	else {
		$this->session->set_flashdata('alert', [
				'message' => "Please try again.",
				'class' => "alert-danger",
				'status_id' => "12",
				'type' => "Error"
		]);
		redirect('admin/car_config');
	}
	 show_404();
	}
	public function get_ajax_type(){
		$id=(int)$_GET['type_id'];

		if($id>0){
			$this->load->model('car_config_model');
			$type_data = $this->car_config_model->car_Config_getAll('vbs_cartype',['id' => $id]);
			$car_configuration_data = $this->car_config_model->car_Config_getAll('vbs_type_of_car_configuration ',['type_id' => $id]);
			
			foreach($type_data as $key => $type){
				$result='<input type="hidden" name="type_id" value="'.$type->id.'">
				<div class="col-md-12">
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
				<span style="font-weight: bold;">Statut</span>
				<select class="form-control" name="statut" required style="background: #fff !important;">
					<option ';
					if ($type->statut == "0") {$result.='selected="selected" '; }
					$result.='value="0">Show</option>
					<option ';
					if ($type->statut == "1") {$result.='selected="selected" '; }
					$result.='value="1">Hide</option>
				</select>
				</div>
			</div>
				<div class="col-md-2" style="margin-top: 5px;">
				<div class="form-group">
					<span style="font-weight: bold;">Type</span>
					<input type="text" class="form-control" name="type" placeholder="" value="'.$type->type_name.'">
				</div>
				</div>
				       <div class="col-md-2" style="margin-top: 5px;">
			';
                if(!empty($type->car_icon)){
                    $result.='<div class="col-md-12"> 
		                         <img  id="editcar_logo_preview" src="'.base_url().'/uploads/vehicle_images/'.$type->car_icon.'" style="width: 200px; height: 150px;" >
		                        </div>';
                       }else{
                       	 $result.='<div class="col-md-12"> 
		                         <img  id="editcar_logo_preview" src="'.base_url('uploads/vehicle_images/default_car.png').'" style="width: 200px; height: 150px;" >
		                        </div>';
                       }
			 $result.='<div class="col-md-12">
                         <div class="form-group">
            <input type="file" name="car_icon" class="form-control-file"  onChange="editcarpreview(this)"   style="width:100%;">
            <span>Car Icone</span>
          </div>
       </div>
         
    </div>
				</div>';

			}
				if($car_configuration_data){
        $result.=' <div class="maineditcapacitybox">
     <div class="col-md-6" style="position:relative;width: 54%;">
       <button type="button"   onclick="editboxes();" class="plusgreeniconconfig"><i class="fa fa-plus"></i></button>
     </div>';}
     else{
     	   $result.=' <div class="maineditcapacitybox">
     <div class="col-md-6" style="position:relative;width: 54%;">
       <button type="button"  onclick="editboxes();" class="plusgreeniconconfig"><i class="fa fa-plus"></i></button>
     </div>';
     $result.=' <div class="capacitybox" >
          <div class="col-md-6" style="margin-top:10px;margin-left: 1.5%;background-image: linear-gradient(to bottom, #fff 0, #e0e0e0 100%);border-color: #ccc;padding: 6px 12px;"> 

             <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6 text-right" style="padding-top: 7px;">
                    <span>Passengers</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="passengercap[]" style="width:100%;" class="form-control" required>
                     
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
                  </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Luggage</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
 
                  <select name="lugagescap[]" style="width:100%;" class="form-control" required>
                     
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
                </div>
             </div>
             <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Wheelchairs</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="wheelchairscap[]" style="width:100%;" class="form-control" required>
                    
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
                </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Babys</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="babycap[]" style="width:100%;" class="form-control" required>
                     
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
              </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Animals</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="animalscap[]" style="width:100%;" class="form-control" required>
                    
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
               </div>
             </div>
          
           </div>  
         </div></div>';
     }
		 	foreach ($car_configuration_data as $key => $car_Config) {
		 			$result.='<div class="capacitybox">
		 			<input type="hidden" name="carconfigid[]" value="'.$car_Config->id.'">
         <div class="col-md-6" style="margin-top:10px;margin-left: 1.5%;background-image: linear-gradient(to bottom, #fff 0, #e0e0e0 100%);border-color: #ccc;padding: 6px 12px;"> 

             <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6 text-right" style="padding-top: 7px;">
                    <span>Passengers</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="passengercap[]" style="width:100%;" class="form-control" required>
                     
                      <option'; if ($car_Config->passengers  == "0") {$result.=' selected="selected" '; }
							$result.=' value="0">0</option>
                      <option'; if ($car_Config->passengers  == "1") {$result.=' selected="selected" '; }
							$result.=' value="1">1</option>
                      <option'; if ($car_Config->passengers  == "2") {$result.=' selected="selected" '; }
							$result.=' value="2">2</option>
                      <option'; if ($car_Config->passengers  == "3") {$result.=' selected="selected" '; }
							$result.=' value="3">3</option>
                      <option'; if ($car_Config->passengers  == "4") {$result.=' selected="selected" '; }
							$result.=' value="4">4</option>
                      <option'; if ($car_Config->passengers  == "5") {$result.=' selected="selected" '; }
							$result.=' value="5">5</option>
                      <option'; if ($car_Config->passengers  == "6") {$result.=' selected="selected" '; }
							$result.=' value="6">6</option>
                      <option'; if ($car_Config->passengers  == "7") {$result.=' selected="selected" '; }
							$result.=' value="7">7</option>
                      <option'; if ($car_Config->passengers  == "8") {$result.=' selected="selected" '; }
							$result.=' value="8">8</option>
                      <option'; if ($car_Config->passengers  == "9") {$result.=' selected="selected" '; }
							$result.=' value="9">9</option>
                      <option'; if ($car_Config->passengers  == "10") {$result.=' selected="selected" '; }
							$result.=' value="10">10</option>
                  </select> 

                </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Luggage</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">

                  <select name="lugagescap[]" style="width:100%;" class="form-control" required>
                    
                      <option'; if ($car_Config->luggage   == "0") {$result.=' selected="selected" '; }
							$result.=' value="0">0</option>
                      <option'; if ($car_Config->luggage   == "1") {$result.=' selected="selected" '; }
							$result.=' value="1">1</option>
                      <option'; if ($car_Config->luggage   == "2") {$result.=' selected="selected" '; }
							$result.=' value="2">2</option>
                      <option'; if ($car_Config->luggage   == "3") {$result.=' selected="selected" '; }
							$result.=' value="3">3</option>
                      <option'; if ($car_Config->luggage   == "4") {$result.=' selected="selected" '; }
							$result.=' value="4">4</option>
                      <option'; if ($car_Config->luggage   == "5") {$result.=' selected="selected" '; }
							$result.=' value="5">5</option>
                      <option'; if ($car_Config->luggage   == "6") {$result.=' selected="selected" '; }
							$result.=' value="6">6</option>
                      <option'; if ($car_Config->luggage   == "7") {$result.=' selected="selected" '; }
							$result.=' value="7">7</option>
                      <option'; if ($car_Config->luggage   == "8") {$result.=' selected="selected" '; }
							$result.=' value="8">8</option>
                      <option'; if ($car_Config->luggage   == "9") {$result.=' selected="selected" '; }
							$result.=' value="9">9</option>
                      <option'; if ($car_Config->luggage   == "10") {$result.=' selected="selected" '; }
							$result.=' value="10">10</option>
                  </select> 

                </div>
             </div>
             <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Wheelchairs</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="wheelchairscap[]" style="width:100%;" class="form-control" required>
                
                     <option'; if ($car_Config->wheelchairs   == "0") {$result.=' selected="selected" '; }
							$result.=' value="0">0</option>
                      <option'; if ($car_Config->wheelchairs   == "1") {$result.=' selected="selected" '; }
							$result.=' value="1">1</option>
                      <option'; if ($car_Config->wheelchairs   == "2") {$result.=' selected="selected" '; }
							$result.=' value="2">2</option>
                      <option'; if ($car_Config->wheelchairs   == "3") {$result.=' selected="selected" '; }
							$result.=' value="3">3</option>
                      <option'; if ($car_Config->wheelchairs   == "4") {$result.=' selected="selected" '; }
							$result.=' value="4">4</option>
                      <option'; if ($car_Config->wheelchairs   == "5") {$result.=' selected="selected" '; }
							$result.=' value="5">5</option>
                      <option'; if ($car_Config->wheelchairs   == "6") {$result.=' selected="selected" '; }
							$result.=' value="6">6</option>
                      <option'; if ($car_Config->wheelchairs   == "7") {$result.=' selected="selected" '; }
							$result.=' value="7">7</option>
                      <option'; if ($car_Config->wheelchairs   == "8") {$result.=' selected="selected" '; }
							$result.=' value="8">8</option>
                      <option'; if ($car_Config->wheelchairs   == "9") {$result.=' selected="selected" '; }
							$result.=' value="9">9</option>
                      <option'; if ($car_Config->wheelchairs   == "10") {$result.=' selected="selected" '; }
							$result.=' value="10">10</option>
                  </select> 

             </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Babys</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="babycap[]" style="width:100%;" class="form-control" required>
                     
                    <option'; if ($car_Config->baby   == "0") {$result.=' selected="selected" '; }
							$result.=' value="0">0</option>
                      <option'; if ($car_Config->baby   == "1") {$result.=' selected="selected" '; }
							$result.=' value="1">1</option>
                      <option'; if ($car_Config->baby   == "2") {$result.=' selected="selected" '; }
							$result.=' value="2">2</option>
                      <option'; if ($car_Config->baby   == "3") {$result.=' selected="selected" '; }
							$result.=' value="3">3</option>
                      <option'; if ($car_Config->baby   == "4") {$result.=' selected="selected" '; }
							$result.=' value="4">4</option>
                      <option'; if ($car_Config->baby   == "5") {$result.=' selected="selected" '; }
							$result.=' value="5">5</option>
                      <option'; if ($car_Config->baby   == "6") {$result.=' selected="selected" '; }
							$result.=' value="6">6</option>
                      <option'; if ($car_Config->baby   == "7") {$result.=' selected="selected" '; }
							$result.=' value="7">7</option>
                      <option'; if ($car_Config->baby   == "8") {$result.=' selected="selected" '; }
							$result.=' value="8">8</option>
                      <option'; if ($car_Config->baby   == "9") {$result.=' selected="selected" '; }
							$result.=' value="9">9</option>
                      <option'; if ($car_Config->baby   == "10") {$result.=' selected="selected" '; }
							$result.=' value="10">10</option>
                  </select> 

              </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Animals</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="animalscap[]" style="width:100%;" class="form-control" required>
                    
                      <option'; if ($car_Config->animals   == "0") {$result.=' selected="selected" '; }
							$result.=' value="0">0</option>
                      <option'; if ($car_Config->animals   == "1") {$result.=' selected="selected" '; }
							$result.=' value="1">1</option>
                      <option'; if ($car_Config->animals   == "2") {$result.=' selected="selected" '; }
							$result.=' value="2">2</option>
                      <option'; if ($car_Config->animals   == "3") {$result.=' selected="selected" '; }
							$result.=' value="3">3</option>
                      <option'; if ($car_Config->animals   == "4") {$result.=' selected="selected" '; }
							$result.=' value="4">4</option>
                      <option'; if ($car_Config->animals   == "5") {$result.=' selected="selected" '; }
							$result.=' value="5">5</option>
                      <option'; if ($car_Config->animals   == "6") {$result.=' selected="selected" '; }
							$result.=' value="6">6</option>
                      <option'; if ($car_Config->animals   == "7") {$result.=' selected="selected" '; }
							$result.=' value="7">7</option>
                      <option'; if ($car_Config->animals   == "8") {$result.=' selected="selected" '; }
							$result.=' value="8">8</option>
                      <option'; if ($car_Config->animals   == "9") {$result.=' selected="selected" '; }
							$result.=' value="9">9</option>
                      <option'; if ($car_Config->animals   == "10") {$result.=' selected="selected" '; }
							$result.=' value="10">10</option>
                  </select> 

              </div>
             </div>
                    
           </div>  ';
           if($counts!=0){
           	$result .='<div class="col-md-6" style="position:relative;width: 54%;">
					       <button type="button"   onclick="deleteconfigbox(this);" class="minusrediconconfig"><i class="fa fa-minus"></i></button>
					     </div>';
					    

           }

            $counts++; 
         $result .='</div>';
		 	}
		 	if($car_configuration_data){
		 	$result.='</div>';
		 }
		}else{
			$result="Please select Record!";
		}
		echo $result;
		exit;
	}
	function do_user_upload_image($filename,$name) {
        
        $config['upload_path'] = './uploads/vehicle_images/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['max_size'] = '10240';
        $config['file_name'] = str_replace(' ', '_', $filename['name']);
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        
        if (!$this->upload->do_upload($name)) {
            return NULL;
        } else {
            $avatar = $this->upload->data();
            $avatar = $avatar['file_name'];
        
            return $avatar;
        }
        
    }
}
