<?php
class Currency extends MY_Controller{
public function __construct()
    {
        parent::__construct();
        // load form and url helpers
        $data = [];
        $this->load->model('my_model');
        $this->load->model('general_model');
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('general');
        $this->load->helper('language');
        $this->load->helper('validate');
        if (!$this->basic_auth->is_login())
            redirect("admin", 'refresh');
        else
        $data['user'] = $this->basic_auth->user();
        $this->load->model('bookings_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');
        $this->load->model('support_model');
        $this->load->model('calls_model');
        $this->load->model('notes_model');
        $this->load->model('notifications_model');
        $this->load->model('cms_model');
        $this->load->model('userx_model');
        $this->load->model('filemanagement_model');
        $this->load->model('currency_model');
        $this->data['configuration'] = get_configuration();
    }
    public function index()
    {
        $this->data['user'] = $this->basic_auth->user();
        $this->data['css_type']   = array("form","datatable");
        $this->data['active_class'] = "currency";
        $this->data['gmaps']      = false;
        $this->data['title']      = $this->lang->line("currencies");
        $this->data['title_link']     = base_url('admin/currency');
        $this->data['company_data'] = $this->general_model->get('vbs_company');
        $this->data['company_data'] = $this->data['company_data'][0];
        $this->data['currency'] = $this->my_model->get_table_row_query("SELECT vbs_currencies.*,vbs_users.civility,vbs_users.first_name,vbs_users.last_name FROM vbs_currencies INNER JOIN vbs_users ON vbs_users.id = vbs_currencies.added_by");
        $this->data['content']  = 'admin/currency/index';
        $this->_render_page('templates/admin_template',$this->data);
    }
    public function add_currency()
    {
        $this->data['css_type']   = array("form");
        $this->data['active_class'] = "dashboard";
        $this->data['gmaps']      = false;
        $this->data['title']        = $this->lang->line("currencies");
        $this->data['title_link']     = base_url('admin/currency');
        $this->data['subtitle']     = '>' .$this->lang->line("add currency");
        $this->data['user'] = $this->basic_auth->user();
        $this->data['content']  = 'admin/currency/add_currency';
        $this->data['company_data'] = $this->general_model->get('vbs_company');
        $this->data['company_data'] = $this->data['company_data'][0];
        $this->_render_page('templates/admin_template',$this->data);
    }
    public function store_currency()
    {
        extract($_POST);
        $data=array(
            'statut'=>$statut,
            'currency_name'=>$currency_name,
            'currency_code'=>$currency_code,
            'conversion'=>$conversion,
            'symbole'=>$symbole,
            'added_by'=>$this->session->userdata('user_id'),
            'default_status'=>$default_status,
            'allow_front_change'=>$allow_front_change,
            'allow_admin_change'=>$allow_admin_change,
            'allow_areas_change'=>$allow_areas_change,
            'created_at'=>date('Y-m-d H:i:s'),
        );
      
        $lastid=$this->general_model->add('vbs_currencies',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                    'message' => "Currency Added Successfully.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
            redirect('admin/currency');
        }
    }
    public function edit_currency($id)
    {
        $this->data['css_type']   = array("form","datatable");
        $this->data['active_class'] = "dashboard";
        $this->data['gmaps']      = false;
        $this->data['title']        = $this->lang->line("currencies");
        $this->data['title_link']     = base_url('admin/currency');
        $this->data['user'] = $this->basic_auth->user();
        $this->data['currency'] = $this->my_model->get_table_row_query("SELECT * FROM vbs_currencies where id = '".$id."'");
        $this->data['currency'] = $this->data['currency'][0];
        $this->data['subtitle']     = '>'.' '.create_timestamp_uid($data['currency']['created_at'],$id);
        $this->data['company_data'] = $this->general_model->get('vbs_company');
        $this->data['company_data'] = $this->data['company_data'][0];
        $this->data['content']  = 'admin/currency/edit_currency';
        $this->_render_page('templates/admin_template',$this->data);
    }
    public function update_currency()
    {
        extract($_POST);
        $data=array(
            'statut'=>$statut,
            'currency_name'=>$currency_name,
            'currency_code'=>$currency_code,
            'conversion'=>$conversion,
            'symbole'=>$symbole,
            'added_by'=>$this->session->userdata('user_id'),
            'default_status'=>$default_status,
            'allow_front_change'=>$allow_front_change,
            'allow_admin_change'=>$allow_admin_change,
            'allow_areas_change'=>$allow_areas_change,
            'updated_at'=>date('Y-m-d H:i:s'),
        );
        $lastid=$this->my_model->update_table(array('id'=>$id),'vbs_currencies',$data);
        if($lastid)
        {
            $this->session->set_flashdata('alert', [
                    'message' => "Currency Updated Successfully.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
            redirect('admin/currency');
        }
    }

    public function view_currency($id){
        $this->data['css_type']   = array("form","datatable");
        $this->data['active_class'] = "dashboard";
        $this->data['gmaps']      = false;
        $this->data['title']        = $this->lang->line("currency");
        $this->data['title_link']     = base_url('admin/currency');
        $this->data['currency'] = $this->my_model->get_table_row_query("SELECT * FROM vbs_currencies where id = '".$id."'");
        $this->data['currency'] = $this->data['currency'][0];
        $this->data['subtitle']     = '>'.' '.create_timestamp_uid($this->data['currency']['created_at'],$id);
        $this->data['company_data'] = $this->general_model->get('vbs_company');
        $this->data['company_data'] = $this->data['company_data'][0];
        $this->data['content']  = 'admin/currency/view_currency';
        $this->_render_page('templates/admin_template',$this->data);
    }
    
}
?>