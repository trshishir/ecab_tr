<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('cms_model');

        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('general');
        $this->load->helper('language');
        $this->load->library('session');
        $this->load->model('user_model');
        $this->load->model('invoice_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');
        $this->load->model('support_model');
        $this->load->model('calls_model');
        $this->load->model('notes_model');
        $this->load->model('notifications_model');
        $this->load->model('cars_model');
        $this->load->model('company_model');

        $this->data['configuration'] = get_configuration();
        $this->data['title'] = "Driver Dashboard";
        $this->data['css_type'] = array("form", "datatable");
        $this->data['user'] = $this->session->userdata('user');
    }
        /* Added by Saravanan.R
      STARTS
     */
    
    /*
    update by rahul689
     */

    public function index($param=null) {

// var_dump($param); die();
        // $this->data['news'] =$this->cms_model->getCmsList_by_status('news',1);
if($param==null)
{
        $this->data['news'] =$this->cms_model->getCmsList_by_status('news',1,"home");
       // var_dump($rec);exit;
        $this->data['title'] = 'Blog';
        $this->data['subtitle'] = 'Blog';  
        $this->data['content'] = 'site/news';

}
else{
        $slug = explode(".",$param)[0];
        $this->data['details'] =$this->cms_model->getCmsListing_id(array('post_type' =>'news', 'link_url'=>$slug),$this->lang->lang());
        $this->data['sub_heading']          = $rec->name;
        $this->data['title']                = 'Blog';        
        $this->data['content']              = 'site/news_details';
        $this->data['author'] = $this->user_model->get(array('user.id'=>$rec->created_by));
}
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "news";
        $this->data['gmaps'] = false;

        $this->data['title_link'] = '#';
        $this->_render_page('templates/site_template', $this->data);
    }
}