<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cms extends MY_Controller
{
function __construct()
{
parent::__construct();
define('PUBPATH',str_replace(SELF,'',FCPATH));
define('BASE_URL',base_url());
$this->load->library('form_validation');
$this->load->helper('url');
$this->lang->load('auth');
$this->lang->load('general');
$this->load->helper('language');
$this->load->helper('validate');
if (!$this->basic_auth->is_login())
redirect("admin", 'refresh');
else
$this->data['user'] = $this->basic_auth->user();
$this->load->model('request_model');
$this->load->model('cms_model');
$this->load->model('jobs_model');
$this->load->model('support_model');
$this->load->model('calls_model');
$this->load->model('notes_model');
$this->load->model('notifications_model');
$this->data['configuration'] = get_configuration();
}
public function index(){
    $this->data['css_type']     = array("form","datatable");
$this->data['active_class'] = "cms";
        $this->data['gmaps']        = false;
        $this->data['title']        = $this->lang->line("cms");
    $this->data['title_link']   = base_url('cms/index');
        $this->data['content']      = 'admin/cms/index';
$this->load->model('calls_model');
$this->load->model('request_model');
$this->load->model('jobs_model');
$data = [];
$data['cms'] = $this->cms_model->getAll();
//$data['bookings'] = $this->bookings_model->getAll();
    $data['jobs']    = $this->jobs_model->getAll();
$data['calls']   = $this->calls_model->getAll();
$this->data['data'] = $this->cms_model->getAll();
$this->_render_page('templates/admin_template', $this->data);
}
public function addcms(){
    $this->data['css_type']     = array("form");
$this->data['active_class'] = "cms";
    $this->data['gmaps']    = false;
    $this->data['title']    = $this->lang->line("cms");
$this->data['title_link'] = base_url('cms/addnew');
$this->data['subtitle'] = "Add New";
$this->data['content']  = 'admin/cms/addcms';
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
$this->store();
}
$this->_render_page('templates/admin_template', $this->data);
}
public function storenew(){
//$error = request_validate();
if (empty($error)) {
//$dob = to_unix_date(@$_POST['dob']);
$id = $this->cms_model->create([
        'status'        => @$_POST['status'],
    'name'          => @$_POST['name'],
    'subject'       => @$_POST['subject'],
        'message'       => @$_POST['message']
]);
// echo $id; exit;
$this->session->set_flashdata('alert', [
'message' => "Successfully Created.",
'class' => "alert-success",
'type' => "Success"
]);
redirect('cms/edit/'.$id);
} else {
$this->data['alert'] = [
'message' => @$error[0],
'class' => "alert-danger",
'type' => "Error"
];
}
}
public function add(){
    $this->data['css_type']     = array("form");
$this->data['active_class'] = "cms";
    $this->data['gmaps']    = false;
    $this->data['title']    = $this->lang->line("cms");
$this->data['title_link'] = base_url('cms/add');
$this->data['subtitle'] = "Add";
$this->data['content']  = 'admin/cms/add';
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
$this->storenew();
}
$this->_render_page('templates/admin_template', $this->data);
}
public function store(){
//$error = request_validate();
if (empty($error)) {
//$dob = to_unix_date(@$_POST['dob']);
$id = $this->cms_model->create([
        'status'        => @$_POST['status'],
    'name'          => @$_POST['name'],
    'subject'       => @$_POST['subject'],
        'message'       => @$_POST['message']
]);
// echo $id; exit;
$this->session->set_flashdata('alert', [
'message' => "Successfully Created.",
'class' => "alert-success",
'type' => "Success"
]);
redirect('cms/edit/'.$id);
} else {
$this->data['alert'] = [
'message' => @$error[0],
'class' => "alert-danger",
'type' => "Error"
];
}
}
public function edit($id){
        $this->data['data']         = $this->cms_model->get(['id' => $id]);
if($this->data['data'] != false) {
$this->data['css_type'] = array("form");
$this->data['active_class'] = "cms";
$this->data['gmaps'] = false;
    $this->data['title']    = $this->lang->line("cms");
$this->data['title_link'] = base_url('cms/edit');
//$this->data['subtitle'] = create_timestamp_uid($this->data['data']->bookdate,$id);
$this->data['content']  = 'admin/cms/edit';
//if($this->data['data']->unread != 0)
//$this->cms_model->update(['unread' => 0], $id);
// if($this->data['data']->is_conformed == "pending")
    //  $this->bookings_model->update(['is_conformed' => "Pending"], $id);
$this->_render_page('templates/admin_template', $this->data);
} else show_404();
}
public function update($id){
$request = $this->cms_model->get(['id' => $id]);
if($request != false) {
//$error = request_validate();
if (empty($error)) {
//$dob = to_unix_date(@$_POST['dob']);
$check = 1;
if(@$_POST['status'] == "New"){
$check = 1;
}else if(@$_POST['status'] == "Pending"){
$check = 2;
}else if(@$_POST['status'] == "Replied"){
$check = 3;
}else{
$check = 4;
}
$notifications = $this->notifications_model->get(array('status'=>$check, 'department'=>4, 'notification_status'=>1));
if($notifications != null && !empty($notifications)){
$MAIL = $this->smtp_model->get(array('id' => 1));
$check = sendReply($request,$notifications->subject,$notifications->message,"",$MAIL,array(@$_POST['status']));
}
$this->cms_model->update([
        'name'      => @$_POST['name'],
    'subject'   => @$_POST['subject'],
    'message'   => @$_POST['message'],
    'status'    => @$_POST['status'],
], $id);
$this->session->set_flashdata('alert', [
'message' => "Successfully Updated.",
'class' => "alert-success",
'type' => "Success"
]);
} else {
$this->session->set_flashdata('alert', [
'message' => @$error[0],
'class' => "alert-danger",
'type' => "Error"
]);
}
redirect('cms/edit/'.$id);
} else show_404();
}
public function reply($id){
$call = $this->cms_model->get(['id' => $id]);
if($call != false) {
$this->form_validation->set_rules('reply_subject', 'Subject', 'trim|xss_clean|min_length[0]|max_length[200]');
$this->form_validation->set_rules('reply_message', 'Message', 'trim|xss_clean|min_length[0]|max_length[5000]');
if ($this->form_validation->run() !== false) {
$subject = isset($_POST['reply_subject']) ? $_POST['reply_subject'] : '';
$message = isset($_POST['reply_message']) ? $_POST['reply_message'] : '';
$check = sendReply($call,$subject,$message);
if($check['status'] != false) {
$this->cms_model->update(['last_action' => date('Y-m-d H:i:s')], $id);
$this->session->set_flashdata('alert', [
'message' => "Successfully Reply Sent.",
'class' => "alert-success",
'type' => "Success"
]);
} else
$this->session->set_flashdata('alert', [
'message' => $check['message'],
'class' => "alert-danger",
'type' => "Danger"
]);
} else {
$validator['messages'] = "";
foreach ($_POST as $key => $inp) {
if(form_error($key) != false){
$this->session->set_flashdata('alert', [
'message' => form_error($key,"<span>","</span>"),
'class' => "alert-danger",
'type' => "Danger"
]);
break;
}
}
}
redirect('admin/request/'.$id.'/edit');
} else show_404();
}
public function delete($id){
$this->cms_model->delete($id);
$this->session->set_flashdata('alert', [
'message' => "Successfully deleted.",
'class' => "alert-success",
'type' => "Success"
]);
redirect('cms/index');
}
/*
// Added by RAHUL689
*/
public function services()
{
    $this->data['css_type']     = array("form","datatable");
$this->data['active_class'] = "cms";
        $this->data['gmaps']        = false;
        $this->data['title']        = $this->lang->line("services");
    $this->data['title_link']   = base_url('cms/services');
        $this->data['content']      = 'admin/cms/services/index';
$this->load->model('calls_model');
$this->load->model('request_model');
$this->load->model('jobs_model');
$data = [];
$this->data['category'] = $this->cms_model->getActiveCategory('services_cat');
$this->_render_page('templates/admin_template', $this->data);
}
public function service_category()
{
    $this->data['css_type']     = array("form","datatable");
$this->data['active_class'] = "cms";
        $this->data['gmaps']        = false;
        $this->data['title']        = $this->lang->line("service_category");
    $this->data['title_link']   = base_url('cms/services/category');
        $this->data['content']      = 'admin/cms/services/category';
$this->load->model('calls_model');
$this->load->model('request_model');
$this->load->model('jobs_model');
$data = [];
// $this->data['data'] = $this->cms_model->getActiveCategory();
$this->_render_page('templates/admin_template_v2', $this->data);
}
public function ajax_service_category()
{
extract($this->input->post());
$slug ='services';
//
if($action=='add')
{
echo json_encode($this->cms_model->add_new_category($name,$slug,$status));
}
if($action=='update')
{
echo json_encode($this->cms_model->update_category($name,$slug,$status,$id));
}
die();
}
public function ajax_get_category()
{
extract($this->input->post());
//
if(isset($slug))
{
echo json_encode($this->cms_model->getActiveCategory($slug));
}
if(isset($id))
{
echo json_encode($this->cms_model->getActiveCategory_id($id));
}
die();
}
public function add_new_services()
{
// echo '<pre>';
    // var_dump($this->input->post());
    // die();
            $path_to_folder = 'uploads/cms/';
            $config['upload_path'] = realpath($path_to_folder);
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['max_size']= '3000000'; // 3mb
            $new_name = uniqid('cms_', true);
            $new_name = explode('.', $new_name)[0];
            $config['file_name'] = $new_name;
            $this->load->library('upload', $config);
            if($this->upload->do_upload('img'))
            {
                $avatar = $this->upload->data();
                $avatar = $avatar['file_name'];
            }
            else
            {
                $avatar = NULL;
            }
            if(!empty($this->input->post('title'))){
                $title = $this->input->post('title');
            } elseif(!empty($this->input->post('name'))){
                $title = $this->input->post('name');
            } else {
                $title = random_string();
            }
            $pageType = $this->input->post("page_type");
            $where = !empty($pageType) ? " AND page_type='$pageType'" : "";
            $slug = create_unique_slug("vbs_cms_page_post", "link_url", $title,0,$where);
            if($this->cms_model->save_cms_page_listing($this->input->post(),$this->basic_auth->user()->id,$avatar,'addNew',$slug))
            {
                $this->session->set_flashdata('alert', [
                    'message' => "Successfully Created.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
                echo json_encode('true');
            }
            else{
                $this->session->set_flashdata('alert', [
                    'message' => "Failed! creating new services.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
                echo json_encode('false');
            }
            die();
        }
        public function ajax_update_cms_listing()
        {
    //         var_dump($this->input->post());
    // die();
            extract($this->input->post());
            $path_to_folder = 'uploads/cms/';
            $config['upload_path'] = realpath($path_to_folder);
            // $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['allowed_types'] = '*';
            $config['max_size']= '3000000'; // 3mb
            $new_name = uniqid('cms_', true);
            $new_name = explode('.', $new_name)[0];
            $config['file_name'] = $new_name;
            $this->load->library('upload', $config);
            if($this->upload->do_upload('img'))
            {
                $avatar = $this->upload->data();
                $avatar = $avatar['file_name'];
            }
            else
            {
                // var_dump($this->upload->display_errors());
                $avatar = NULL;
            }
    // var_dump($avatar);
    // die('here');
            $id = $this->basic_auth->user()->id;
            if(!empty($this->input->post('title'))){
                $title = $this->input->post('title');
            } elseif(!empty($this->input->post('name'))){
                $title = $this->input->post('name');
            } else {
                $title = random_string();
            }
            $pageType = $this->input->post("page_type");
            $where = !empty($pageType) ? " AND page_type='$pageType'" : "";
            $slug = create_unique_slug("vbs_cms_page_post", "link_url", $title, $id, $where);
            //var_dump($id);exit;
            if($this->cms_model->save_cms_page_listing($this->input->post(),$id,$avatar,'update',$slug))
            {
                $this->session->set_flashdata('alert', [
                    'message' => "Successfully Updated.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
                echo json_encode('true');
            }
            else{
                $this->session->set_flashdata('alert', [
                    'message' => "Not Updated! ",
                    'class' => "alert-warning",
                    'type' => "Success"
                ]);
                echo json_encode('false');
            }
            die();
        }
        public function ajax_get_all_cms_listing()
        {
            extract($this->input->post());
            if(isset($post_type))
            {
                echo json_encode($this->cms_model->getCmsList($post_type));
            }
            die();
        }
        public function ajax_delete_cms_listing()
        {
            extract($this->input->post());
            if($this->cms_model->delete_cms_page_listing($id))
            {
                $this->session->set_flashdata('alert', [
                    'message' => "Successfully Deleted.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
                echo json_encode('true');
            }
            else{
                $this->session->set_flashdata('alert', [
                    'message' => "Faild! to Delted the listing.",
                    'class' => "alert-success",
                    'type' => "Success"
                ]);
                echo json_encode('false');
            }
            die();
        }
        public function legals()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = $this->lang->line("legals");
            $this->data['title_link'] = base_url('cms/legals');
            $this->data['content'] = 'admin/cms/legals/index';
            $this->load->model('calls_model');
            $this->load->model('request_model');
            $this->load->model('jobs_model');
            $data = [];
            $this->data['category'] = $this->cms_model->getActiveCategory('services');
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function news()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = $this->lang->line("news");
            $this->data['title_link'] = base_url('cms/news');
            $this->data['content'] = 'admin/cms/news/index';
            $this->data['category']     = $this->cms_model->getActiveCategory('news_cat',1);
            $this->load->model('calls_model');
            $this->load->model('request_model');
            $this->load->model('jobs_model');
            $data = [];
            // $this->data['category'] = $this->cms_model->getActiveCategory('news');
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function prices()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = $this->lang->line("prices");
            $this->data['title_link'] = base_url('cms/prices');
            $this->data['content'] = 'admin/cms/prices/index';
            $this->load->model('calls_model');
            $this->load->model('request_model');
            $this->load->model('jobs_model');
            $data = [];
            $this->data['category'] = $this->cms_model->getActiveCategory('services_cat');
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function banners()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = $this->lang->line("banners");
            $this->data['title_link'] = base_url('cms/banners');
            $this->data['content'] = 'admin/cms/banners/index';
            $this->load->model('calls_model');
            $this->load->model('request_model');
            $this->load->model('jobs_model');
            $data = [];
            $this->data['banners'] = $this->cms_model->getCMSBanner();
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function flash_info()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = $this->lang->line("flashnews");
            $this->data['title_link'] = base_url('cms/flashnews');
            $this->data['content'] = 'admin/cms/flashnews/index';
            $this->load->model('calls_model');
            $this->load->model('request_model');
            $this->load->model('jobs_model');
            $data = [];
            $this->data['flashnews'] = $this->cms_model->getCmsFlashNews();
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function seo()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = $this->lang->line("seo");
            $this->data['title_link'] = base_url('cms/seo');
            $this->data['content'] = 'admin/cms/seo/index';
            $this->load->model('calls_model');
            $this->load->model('request_model');
            $this->load->model('jobs_model');
            $data = [];
            $this->data['seo'] = $this->cms_model->getCmsSeo();
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function cms_config()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = "CMS Config";
            $this->data['title_link'] = base_url('cms/cms config');
            $this->data['content'] = 'admin/cms/config/index';
            $this->load->model('calls_model');
            $this->load->model('request_model');
            $this->load->model('jobs_model');
            $data = [];
            $this->data['category'] = $this->cms_model->getActiveCategory('services');
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function fleet()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = $this->lang->line("fleet");
            $this->data['title_link'] = base_url('cms/fleet');
            $this->data['content'] = 'admin/cms/fleet/index';
            $this->load->model('calls_model');
            $this->load->model('request_model');
            $this->load->model('jobs_model');
            $data = [];
            $this->data['car_category'] = $this->cms_model->getActiveCategory('cars_cat');
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function zones()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = $this->lang->line("zones");
            $this->data['title_link'] = base_url('cms/zones');
            $this->data['content'] = 'admin/cms/zones/index';
            $this->load->model('calls_model');
            $this->load->model('request_model');
            $this->load->model('jobs_model');
            $data = [];
            $this->data['countries'] = $this->cms_model->get_all_countries();
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function faq()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = $this->lang->line("faq");
            $this->data['title_link'] = base_url('cms/faq');
            $this->data['content'] = 'admin/cms/faq/index';
            $this->load->model('calls_model');
            $this->load->model('request_model');
            $this->load->model('jobs_model');
            $data = [];
            $this->data['category'] = $this->cms_model->getActiveCategory('faq_cat');
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function services_cat()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = 'CMS Config';
            $this->data['title_link'] = base_url('cms/services_cat');
            $this->data['content'] = 'admin/cms/config/services';
            $this->load->model('calls_model');
            $this->load->model('request_model');
            $this->load->model('jobs_model');
            $data = [];
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function faq_cat()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = 'CMS Config';
            $this->data['title_link'] = base_url('cms/faq_cat');
            $this->data['content'] = 'admin/cms/config/faq';
            $this->load->model('calls_model');
            $this->load->model('request_model');
            $this->load->model('jobs_model');
            $data = [];
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function car_cat()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = 'CMS Config';
            $this->data['title_link'] = base_url('cms/car_cat');
            $this->data['content'] = 'admin/cms/config/fleet';
            $this->load->model('calls_model');
            $this->load->model('request_model');
            $this->load->model('jobs_model');
            $data = [];
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function downloads_cat()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = 'CMS Config';
            $this->data['title_link'] = base_url('cms/downloads_cat');
            $this->data['content'] = 'admin/cms/config/downloads';
            $data = [];
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function news_cat()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = 'CMS Config';
            $this->data['title_link'] = base_url('cms/news_cat');
            $this->data['content'] = 'admin/cms/config/news';
            $data = [];
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function udapts_cat()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = 'CMS Config';
            $this->data['title_link'] = base_url('cms/udapts_cat');
            $this->data['content'] = 'admin/cms/config/udapts';
            $data = [];
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function documentations_cat()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = 'CMS Config';
            $this->data['title_link'] = base_url('cms/documentations_cat');
            $this->data['content'] = 'admin/cms/config/documentations';
            $data = [];
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function tutorials_cat()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = 'CMS Config';
            $this->data['title_link'] = base_url('cms/tutorials_cat');
            $this->data['content'] = 'admin/cms/config/tutorials';
            $this->load->model('calls_model');
            $this->load->model('request_model');
            $this->load->model('jobs_model');
            $data = [];
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function testimonials()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = $this->lang->line("testimonials");
            $this->data['title_link'] = base_url('cms/testimonials');
            $this->data['content'] = 'admin/cms/testimonials/index';
            $this->load->model('calls_model');
            $this->load->model('request_model');
            $this->load->model('jobs_model');
            $data = [];
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function downloads()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = $this->lang->line("downloads");
            $this->data['title_link'] = base_url('cms/downloads');
            $this->data['content'] = 'admin/cms/downloads/index';
            $this->load->model('calls_model');
            $this->load->model('request_model');
            $this->load->model('jobs_model');
            $data = [];
            $this->data['category'] = $this->cms_model->getActiveCategory('downloads_cat');
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function single_tutorials($id)
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = $this->lang->line("tutorials");
            $this->data['title_link'] = base_url('admin/tutorials');
            $this->data['content'] = 'admin/cms/tutorials/single';
            $this->data['id']      = $id;
            $inputData =[
                'id'=>$id,
                'post_type'=>'tutorials'
            ];
            $details= $this->cms_model->getCmsListing_id($inputData);
            $this->data['details']      =  $details;
            $this->data['subtitle'] =  $details->category_name.' > '.$details->title;
            $data = [];
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function tutorials()
        {
            $this->data['css_type']     = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps']        = false;
            $this->data['title']        = $this->lang->line("tutorials");
            $this->data['title_link']   = base_url('cms/tutorials');
            $this->data['content']      = 'admin/cms/tutorials/index';
            $data = [];
            $this->data['category'] = $this->cms_model->getActiveCategory('tutorials_cat');
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function single_documentations($id)
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = $this->lang->line("documentations");
            $this->data['title_link'] = base_url('admin/documentations');
            $this->data['content'] = 'admin/cms/documentations/single';
            $this->data['id']      = $id;
            $inputData =[
                'id'=>$id,
                'post_type'=>'documentations'
            ];
            $details= $this->cms_model->getCmsListing_id($inputData);
            $this->data['details']      =  $details;
            $this->data['subtitle'] =  $details->category_name.' > '.$details->title;
            $data = [];
            $this->data['category'] = $this->cms_model->getActiveCategory('documentations_cat');
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function documentations()
        {
            $this->data['css_type']     = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps']        = false;
            $this->data['title']        = $this->lang->line("documentations");
            $this->data['title_link']   = base_url('cms/documentations');
            $this->data['content']      = 'admin/cms/documentations/index';
            $data = [];
            $this->data['category'] = $this->cms_model->getActiveCategory('documentations_cat');
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function updates()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = $this->lang->line("udapts");
            $this->data['title_link'] = base_url('cms/updates');
            $this->data['content'] = 'admin/cms/udapts/index';
            $data = [];
            $this->data['category'] = $this->cms_model->getActiveCategory('udapts_cat');
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function partners()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = $this->lang->line("partners_logos");
            $this->data['title_link'] = base_url('cms/partners');
            $this->data['content'] = 'admin/cms/partners/index';
            $this->load->model('calls_model');
            $this->load->model('request_model');
            $this->load->model('jobs_model');
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function slide_show(){
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = $this->lang->line("slider");
            $this->data['title_link'] = base_url('cms/slide_show');
            $this->data['content'] = 'admin/cms/slide_show/index';
            $this->load->model('calls_model');
            $this->load->model('request_model');
            $this->load->model('jobs_model');
            $this->data['category'] = $this->cms_model->getActiveCategory('services_cat');
            $this->data['services'] = $this->cms_model->getCmsListing_id(['post_type' => 'services', 'status' => 1]);
            $this->data['news_category'] = $this->cms_model->getActiveCategory('news_cat');
            $this->data['news'] = $this->cms_model->getCmsListing_id(['post_type' => 'news']);
            $this->_render_page('templates/admin_template', $this->data);
        }
        public function info_bulls()
        {
            $this->data['css_type'] = array("form","datatable");
            $this->data['active_class'] = "cms";
            $this->data['gmaps'] = false;
            $this->data['title'] = $this->lang->line("info_bulls");
            $this->data['title_link'] = base_url('cms/info_bulls');
            $this->data['content'] = 'admin/cms/info_bulls/index';
            $this->load->model('calls_model');
            $this->load->model('request_model');
            $this->load->model('jobs_model');
            $this->data['data'] = $this->cms_model->getFirstInfobul();
            $this->_render_page('templates/admin_template', $this->data);
        }
        /* datatable ajax pagination function base on p[ost type */
            public function ajax_get_cms_listing()
            {
            //
                echo json_encode($this->cms_model->getCmsListing_id($this->input->post()));
                die();
            }
            public function ajax_get_region_listing()
            {
            //
                echo json_encode($this->cms_model->ajax_get_region_listing($this->input->post()));
                die();
            }
            public function ajax_get_cities_listing()
            {
            //
                echo json_encode($this->cms_model->ajax_get_cities_listing($this->input->post()));
                die();
            }
            public function services_list_ajax_data_set()
            {
                $this->load->helper('url');
                extract($this->input->post());
                $con = &get_instance();
                $host = $con->db->hostname;
                $user = $con->db->username;
                $password = $con->db->password;
                $dbname =  $con->db->database;
                $con = mysqli_connect($host, $user, $password,$dbname);
            // Check connection
                if (!$con) {
                    die("Connection failed: " . mysqli_connect_error());
                }
            ## Read value
                $draw = $_POST['draw'];
                $row = $_POST['start'];
            $rowperpage = $_POST['length']; // Rows display per page
            $columnIndex = $_POST['order'][0]['column']; // Column index
            $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
            $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
            $searchValue = $_POST['search']['value']; // Search value
            ## Search 
            $searchQuery = " ";
            if($searchValue != ''){
                $searchQuery .= " AND ( (cpp.title LIKE '%".$searchValue."%')  
                OR (cpp.name LIKE '%".$searchValue."%')  
                OR (cpp.title LIKE '%".$searchValue."%')  
                OR (u.username LIKE '%".$searchValue."%') 
                OR (cc.name LIKE '%".$searchValue."%') )";
            }
            # Custom Filter
            $status = null;
            if (!empty($this->input->post('status')))
            {
                $userStatus = $this->input->post('status')=='*'?"0":$this->input->post('status');
                $status = " AND cpp.status ='$userStatus'";
            }
            ## Total number of records without filtering
            $sel = mysqli_query($con,"SELECT COUNT(*) as allcount
            FROM vbs_cms_page_post  as cpp
            LEFT JOIN  vbs_cms_page_post as cc ON cpp.category_id = cc.id
            LEFT JOIN  vbs_users as u ON cpp.created_by = u.id
            WHERE cpp.page_type = '".$post_type."'");
            $records = mysqli_fetch_assoc($sel);
            $totalRecords = $records['allcount'];
            ## Total number of record with filtering
            $sel = mysqli_query($con,"SELECT COUNT(*) as allcount 
            FROM vbs_cms_page_post  as cpp
            LEFT JOIN  vbs_cms_page_post as cc ON cpp.category_id = cc.id
            LEFT JOIN  vbs_users as u ON cpp.created_by = u.id
            WHERE cpp.page_type = ' ".$post_type."' ".$searchQuery);
            $records = mysqli_fetch_assoc($sel);
            $totalRecordwithFilter = $records['allcount'];
            ## Fetch records
            $totalQuery = "SELECT cpp.*, cc.name as category , u.username, country.name as country, regions.name as region, cities.name as city 
            FROM vbs_cms_page_post  as cpp
            LEFT JOIN  vbs_cms_page_post as cc ON cpp.category_id = cc.id
            LEFT JOIN  vbs_countries as country ON cpp.country_id = country.id
            LEFT JOIN  vbs_regions as regions ON cpp.region_id = regions.id
            LEFT JOIN  vbs_cities as cities ON cpp.cities_id = cities.id
            LEFT JOIN  vbs_users as u ON cpp.created_by = u.id
            WHERE cpp.page_type ='".$post_type."' ".$searchQuery." ORDER BY cpp.".$columnName." ".$columnSortOrder." LIMIT ".$row.",".$rowperpage;
            $lastQuery = mysqli_query($con, $totalQuery);
    // var_dump($totalQuery);
    // var_dump($lastQuery);
            $data = array();
            $sl = 1;
            while ($record = mysqli_fetch_assoc($lastQuery)) {
                $dt = new DateTime($record['created_date']);
                $modification_date = new DateTime($record['modification_date']);
                $date2 = new DateTime("now");
                $interval = $dt->diff($date2);
                $since = (($interval->y!=0)?$interval->y.' Years ':'').(($interval->m!=0)?$interval->m.' Months ':'').(($interval->h!=0)?$interval->h.' Hours ':'').(($interval->i!=0)?$interval->i.' Minutes ':'').(($interval->s!=0)?$interval->s.' Seconds':'');
                if($post_type =='services')
                {
                    $data[] = array(
                        "created_date" => '<input id="listCheck" onclick="select_check(this)" data-id="'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'"  name="listCheck"  type="checkbox" class="ccheckbox" value="'.$record['id'].'">',
                        "id"=>'<a onclick="edit_item('.$record['id'].','.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).')">'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'</a>',
                        "date"=>$dt->format('d/m/Y'),
                        "time"=>$dt->format('H:i'),
                        "name"=>$record['name'],
                        "category_name"=>$record['category'],
                        "image"=>'<img width="50px" height="50px" alt="No image" src="'.BASE_URL.'/uploads/cms/'.$record['image'].'">',
                        "status"=>(($record['status']==1)?("<span class='label label-success'>Enable</span>"):(($record['status']==2)?("<span class='label label-danger'>Disable</span>"):("<span class='label label-warning'>Pending</span>"))),
                        "username"=>$record['username'],
                        "since"=>$since,
                        // "settings"=>'<button class="btn btn-primary" onclick="edit_item('.$record['id'].')" >Edit</button>  <button class="btn btn-warning" onclick="delete_item('.$record['id'].')" >Delete</button>'
                    );
                }
                if($post_type =='legals')
                {
                    $prev_url =BASE_URL.'legales.php/?id='.$record['id'];
                    $data[] = array(
                        "created_date" => '<input id="listCheck" onclick="select_check(this)" data-id="'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'"  name="listCheck"  type="checkbox" class="ccheckbox" value="'.$record['id'].'">',
                        // "id"=>'<a onclick="edit_item('.$record['id'].','.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).')">'.$record['title'].'</a>',
                        "id"=>'<a onclick="edit_item('.$record['id'].','.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).')">'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'</a>',
                        "date"=>$dt->format('d/m/Y'),
                        "time"=>$dt->format('H:i'),
                        "title"=>$record['title'],
                        "language"=>$record['language'],
                        "username"=>$record['username'],
                        "status"=>(($record['status']==1)?("<span class='label label-success'>Enable</span>"):(($record['status']==2)?("<span class='label label-danger'>Disable</span>"):("<span class='label label-warning'>Pending</span>"))),
                        "preview"=>'<a class="btn btn-default" href="'.$prev_url.'" target="_blank">Preview</a>',
                        // "created_at"=>$record['created_at'],
                        // "updated_at"=>$record['updated_at'],
                        "since"=>$since,
                        //
                    );
                }
                if($post_type =='news')
                {
                    $data[] = array(
                        "created_date" => '<input id="listCheck" onclick="select_check(this)"  name="listCheck" data-id="'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'"  type="checkbox" class="ccheckbox" value="'.$record['id'].'">',
                        "id"=>'<a onclick="edit_item('.$record['id'].','.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).')">'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'</a>',
                        "date"=>$dt->format('d/m/Y'),
                        "time"=>$dt->format('H:i'),
                        "username"=>$record['username'],
                        "title"=>$record['title'],
                        "language"=>$record['language'],
                        "image"=>'<img width="50px" height="50px" alt="No image" src="'.BASE_URL.'/uploads/cms/'.$record['image'].'">',
                        "status"=>(($record['status']==1)?("<span class='label label-success'>Enable</span>"):(($record['status']==2)?("<span class='label label-danger'>Disable</span>"):("<span class='label label-warning'>Pending</span>"))),
                        // "created_at"=>$record['created_at'],
                        // "updated_at"=>$record['updated_at'],
                        "since"=>$since,
                        //
                        // "settings"=>'<button class="btn btn-primary" onclick="edit_item('.$record['id'].')" >Edit</button>  <button class="btn btn-warning" onclick="delete_item('.$record['id'].')" >Delete</button>'
                    );
                }
                if($post_type =='prices')
                {
                    $data[] = array(
                        "created_date" => '<input id="listCheck" onclick="select_check(this)" data-id="'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'"  name="listCheck"  type="checkbox" class="ccheckbox" value="'.$record['id'].'">',
                        "id"=>'<a onclick="edit_item('.$record['id'].','.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).')">'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'</a>',
                        "publish_date"=>$dt->format('d/m/Y H:i:s'),
                        "modification_date"=>$modification_date->format('d/m/Y H:i:s'),
                        "username"=>$record['username'],
                        "category_name"=>$record['category'],
                        "price_type"=>$record['price_type'],
                        "p_price"=>$record['p_price'],
                        "m_price"=>$record['m_price'],
                        "n_price"=>$record['n_price'],
                        "nw_price"=>$record['nw_price'],
                        "status"=>(($record['status']==1)?("<span class='label label-success'>Enable</span>"):(($record['status']==2)?("<span class='label label-danger'>Disable</span>"):("<span class='label label-warning'>Pending</span>"))),
                        "since"=>$since,
                        // "settings"=>'<button class="btn btn-primary" onclick="edit_item('.$record['id'].')" >Edit</button>  <button class="btn btn-warning" onclick="delete_item('.$record['id'].')" >Delete</button>'
                    );
                }
                if($post_type =='fleet')
                {
                    $data[] = array(
                        "created_date" => '<input id="listCheck" onclick="select_check(this)"  name="listCheck" data-id="'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'"  type="checkbox" class="ccheckbox" value="'.$record['id'].'">',
                        "id"=>'<a onclick="edit_item('.$record['id'].','.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).')">'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'</a>',
                        "date"=>$dt->format('d/m/Y'),
                        "time"=>$dt->format('H:i'),
                        "username"=>$record['username'],
                        "name"=>$record['name'],
                        "model"=>$record['model'],
                        "wheelchairaccess"=>($record['wheelchairaccess']==1)?'Yes':'No',
                        "climatisation"=>($record['climatisation']==1)?'Yes':'No',
                        "image"=>'<img width="50px" height="50px" alt="No image" src="'.BASE_URL.'/uploads/cms/'.$record['image'].'">',
                        "status"=>(($record['status']==1)?("<span class='label label-success'>Enable</span>"):(($record['status']==2)?("<span class='label label-danger'>Disable</span>"):("<span class='label label-warning'>Pending</span>"))),
                        // "created_at"=>$record['created_at'],
                        // "updated_at"=>$record['updated_at'],
                        "since"=>$since,
                        //
                        // "settings"=>'<button class="btn btn-primary" onclick="edit_item('.$record['id'].')" >Edit</button>  <button class="btn btn-warning" onclick="delete_item('.$record['id'].')" >Delete</button>'
                    );
                }
                if($post_type =='partners')
                {
                    $data[] = array(
                        "created_date" => '<input id="listCheck" onclick="select_check(this)"  name="listCheck" data-id="'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'"  type="checkbox" class="ccheckbox" value="'.$record['id'].'">',
                        "id"=>'<a onclick="edit_item('.$record['id'].','.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).')">'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'</a>',
                        "date"=>$dt->format('d/m/Y'),
                        "time"=>$dt->format('H:i'),
                        "username"=>$record['username'],
                        "name"=>$record['name'],
                        "image"=>'<img width="50px" height="50px" alt="No image" src="'.BASE_URL.'/uploads/cms/'.$record['image'].'">',
                        "status"=>(($record['status']==1)?("<span class='label label-success'>Enable</span>"):(($record['status']==2)?("<span class='label label-danger'>Disable</span>"):("<span class='label label-warning'>Pending</span>"))),
                        // "created_at"=>$record['created_at'],
                        // "updated_at"=>$record['updated_at'],
                        "since"=>$since,
                        //
                        // "settings"=>'<button class="btn btn-primary" onclick="edit_item('.$record['id'].')" >Edit</button>  <button class="btn btn-warning" onclick="delete_item('.$record['id'].')" >Delete</button>'
                    );
                }
                if($post_type =='faq')
                {
                    $data[] = array(
                        "created_date" => '<input id="listCheck" onclick="select_check(this)"  name="listCheck" data-id="'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'"  type="checkbox" class="ccheckbox" value="'.$record['id'].'">',
                        "id"=>'<a onclick="edit_item('.$record['id'].','.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).')">'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'</a>',
                        "date"=>$dt->format('d/m/Y'),
                        "time"=>$dt->format('H:i'),
                        "username"=>$record['username'],
                        "category"=>$record['category'],
                        "title"=>$record['title'],
                        "language"=>$record['language'],
                        "status"=>(($record['status']==1)?("<span class='label label-success'>Enable</span>"):(($record['status']==2)?("<span class='label label-danger'>Disable</span>"):("<span class='label label-warning'>Pending</span>"))),
                        // "created_at"=>$record['created_at'],
                        // "updated_at"=>$record['updated_at'],
                        "since"=>$since,
                        //
                        // "settings"=>'<button class="btn btn-primary" onclick="edit_item('.$record['id'].')" >Edit</button>  <button class="btn btn-warning" onclick="delete_item('.$record['id'].')" >Delete</button>'
                    );
                }
                if($post_type =='zones')
                {
                    $data[] = array(
                        "created_date" => '<input id="listCheck" onclick="select_check(this)"  name="listCheck" data-id="'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'"  type="checkbox" class="ccheckbox" value="'.$record['id'].'">',
                        "id"=>'<a onclick="edit_item('.$record['id'].','.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).')">'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'</a>',
                        "date"=>$dt->format('d/m/Y'),
                        "time"=>$dt->format('H:i'),
                        "username"=>$record['username'],
                        "title"=>$record['name'],
                        "country"=>$record['country'],
                        "region"=>$record['region'],
                        "city"=>$record['city'],
                        "picture"=>'<img width="50px" height="50px" alt="No image" src="'.BASE_URL.'/uploads/cms/'.$record['image'].'">',
                        "language"=>$record['language'],
                        "status"=>(($record['status']==1)?("<span class='label label-success'>Enable</span>"):(($record['status']==2)?("<span class='label label-danger'>Disable</span>"):("<span class='label label-warning'>Pending</span>"))),
                        // "created_at"=>$record['created_at'],
                        // "updated_at"=>$record['updated_at'],
                        "since"=>$since,
                        //
                        // "settings"=>'<button class="btn btn-primary" onclick="edit_item('.$record['id'].')" >Edit</button>  <button class="btn btn-warning" onclick="delete_item('.$record['id'].')" >Delete</button>'
                    );
                }
                if($post_type =='downloads')
                {
                    $data[] = array(
                        "created_date" => '<input id="listCheck" onclick="select_check(this)"  name="listCheck" data-id="'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'"  type="checkbox" class="ccheckbox" value="'.$record['id'].'">',
                        "id"=>'<a onclick="edit_item('.$record['id'].','.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).')">'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'</a>',
                        "date"=>$dt->format('d/m/Y'),
                        "time"=>$dt->format('H:i'),
                        "username"=>$record['username'],
                        "category"=>$record['category'],
                        "name"=>$record['name'],
                        "status"=>(($record['status']==1)?("<span class='label label-success'>Enable</span>"):(($record['status']==2)?("<span class='label label-danger'>Disable</span>"):("<span class='label label-warning'>Pending</span>"))),
                        // "created_at"=>$record['created_at'],
                        // "updated_at"=>$record['updated_at'],
                        "since"=>$since,
                        //
                        // "settings"=>'<button class="btn btn-primary" onclick="edit_item('.$record['id'].')" >Edit</button>  <button class="btn btn-warning" onclick="delete_item('.$record['id'].')" >Delete</button>'
                    );
                }
                if($post_type =='tutorials')
                {
                    $data[] = array(
                        "created_date" => '<input id="listCheck" onclick="select_check(this)"  name="listCheck" data-id="'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'"  type="checkbox" class="ccheckbox" value="'.$record['id'].'">',
                        "id"=>'<a onclick="edit_item('.$record['id'].','.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).')">'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'</a>',
                        "date"=>$dt->format('d/m/Y'),
                        "time"=>$dt->format('H:i'),
                        "username"=>$record['username'],
                        "category"=>$record['category'],
                        "title"=>$record['title'],
                        "status"=>(($record['status']==1)?("<span class='label label-success'>Enable</span>"):(($record['status']==2)?("<span class='label label-danger'>Disable</span>"):("<span class='label label-warning'>Pending</span>"))),
                        // "created_at"=>$record['created_at'],
                        // "updated_at"=>$record['updated_at'],
                        "since"=>$since,
                        //
                        // "settings"=>'<button class="btn btn-primary" onclick="edit_item('.$record['id'].')" >Edit</button>  <button class="btn btn-warning" onclick="delete_item('.$record['id'].')" >Delete</button>'
                    );
                }
                if($post_type =='documentations')
                {
                    $data[] = array(
                        "created_date" => '<input id="listCheck" onclick="select_check(this)"  name="listCheck" data-id="'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'"  type="checkbox" class="ccheckbox" value="'.$record['id'].'">',
                        "id"=>'<a onclick="edit_item('.$record['id'].','.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).')">'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'</a>',
                        "date"=>$dt->format('d/m/Y'),
                        "time"=>$dt->format('H:i'),
                        "username"=>$record['username'],
                        "category"=>$record['category'],
                        "title"=>$record['title'],
                        "status"=>(($record['status']==1)?("<span class='label label-success'>Enable</span>"):(($record['status']==2)?("<span class='label label-danger'>Disable</span>"):("<span class='label label-warning'>Pending</span>"))),
                        // "created_at"=>$record['created_at'],
                        // "updated_at"=>$record['updated_at'],
                        "since"=>$since,
                        //
                        // "settings"=>'<button class="btn btn-primary" onclick="edit_item('.$record['id'].')" >Edit</button>  <button class="btn btn-warning" onclick="delete_item('.$record['id'].')" >Delete</button>'
                    );
                }
                if($post_type =='updates')
                {
                    $data[] = array(
                        "created_date" => '<input id="listCheck" onclick="select_check(this)"  name="listCheck" data-id="'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'"  type="checkbox" class="ccheckbox" value="'.$record['id'].'">',
                        "id"=>'<a onclick="edit_item('.$record['id'].','.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).')">'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'</a>',
                        "date"=>$dt->format('d/m/Y'),
                        "time"=>$dt->format('H:i'),
                        "username"=>$record['username'],
                        "status"=>(($record['status']==1)?("<span class='label label-success'>Enable</span>"):(($record['status']==2)?("<span class='label label-danger'>Disable</span>"):("<span class='label label-warning'>Pending</span>"))),
                        // "created_at"=>$record['created_at'],
                        // "updated_at"=>$record['updated_at'],
                        "since"=>$since,
                        //
                        // "settings"=>'<button class="btn btn-primary" onclick="edit_item('.$record['id'].')" >Edit</button>  <button class="btn btn-warning" onclick="delete_item('.$record['id'].')" >Delete</button>'
                    );
                }
                if($post_type =='testimonials')
                {
                    $data[] = array(
                        "created_date" => '<input id="listCheck" onclick="select_check(this)"  name="listCheck" data-id="'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'"  type="checkbox" class="ccheckbox" value="'.$record['id'].'">',
                        "id"=>'<a onclick="edit_item('.$record['id'].','.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).')">'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'</a>',
                        "date"=>$dt->format('d/m/Y'),
                        "time"=>$dt->format('H:i'),
                        "username"=>$record['username'],
                        "name"=>$record['name'],
                        "gender"=>$record['gender'],
                        "language"=>$record['language'],
                        "status"=>(($record['status']==1)?("<span class='label label-success'>Enable</span>"):(($record['status']==2)?("<span class='label label-danger'>Disable</span>"):("<span class='label label-warning'>Pending</span>"))),
                        // "created_at"=>$record['created_at'],
                        // "updated_at"=>$record['updated_at'],
                        "since"=>$since,
                        //
                        // "settings"=>'<button class="btn btn-primary" onclick="edit_item('.$record['id'].')" >Edit</button>  <button class="btn btn-warning" onclick="delete_item('.$record['id'].')" >Delete</button>'
                    );
                }
                if($post_type =='cars_cat')
                {
                    $data[] = array(
                        "created_date" => '<input id="listCheck" onclick="select_check(this)"  name="listCheck" data-id="'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'"  type="checkbox" class="ccheckbox" value="'.$record['id'].'">',
                        "id"=>'<a onclick="edit_item('.$record['id'].','.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).')">'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'</a>',
                        "date"=>$dt->format('d/m/Y'),
                        "time"=>$dt->format('H:i'),
                        "username"=>$record['username'],
                        "name"=>$record['name'],
                        "status"=>(($record['status']==1)?("<span class='label label-success'>Enable</span>"):(($record['status']==2)?("<span class='label label-danger'>Disable</span>"):("<span class='label label-warning'>Pending</span>"))),
                        // "created_at"=>$record['created_at'],
                        // "updated_at"=>$record['updated_at'],
                        "since"=>$since,
                        //
                        // "settings"=>'<button class="btn btn-primary" onclick="edit_item('.$record['id'].')" >Edit</button>  <button class="btn btn-warning" onclick="delete_item('.$record['id'].')" >Delete</button>'
                    );
                }
                if($post_type =='faq_cat')
                {
                    $data[] = array(
                        "created_date" => '<input id="listCheck" onclick="select_check(this)"  name="listCheck" data-id="'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'"  type="checkbox" class="ccheckbox" value="'.$record['id'].'">',
                        "id"=>'<a onclick="edit_item('.$record['id'].','.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).')">'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'</a>',
                        "date"=>$dt->format('d/m/Y'),
                        "time"=>$dt->format('H:i'),
                        "username"=>$record['username'],
                        "name"=>$record['name'],
                        "status"=>(($record['status']==1)?("<span class='label label-success'>Enable</span>"):(($record['status']==2)?("<span class='label label-danger'>Disable</span>"):("<span class='label label-warning'>Pending</span>"))),
                        // "created_at"=>$record['created_at'],
                        // "updated_at"=>$record['updated_at'],
                        "since"=>$since,
                        //
                        // "settings"=>'<button class="btn btn-primary" onclick="edit_item('.$record['id'].')" >Edit</button>  <button class="btn btn-warning" onclick="delete_item('.$record['id'].')" >Delete</button>'
                    );
                }
                if($post_type =='downloads_cat')
                {
                    $data[] = array(
                        "created_date" => '<input id="listCheck" onclick="select_check(this)"  name="listCheck" data-id="'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'"  type="checkbox" class="ccheckbox" value="'.$record['id'].'">',
                        "id"=>'<a onclick="edit_item('.$record['id'].','.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).')">'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'</a>',
                        "date"=>$dt->format('d/m/Y'),
                        "time"=>$dt->format('H:i'),
                        "username"=>$record['username'],
                        "name"=>$record['name'],
                        "status"=>(($record['status']==1)?("<span class='label label-success'>Enable</span>"):(($record['status']==2)?("<span class='label label-danger'>Disable</span>"):("<span class='label label-warning'>Pending</span>"))),
                        // "created_at"=>$record['created_at'],
                        // "updated_at"=>$record['updated_at'],
                        "since"=>$since,
                        //
                        // "settings"=>'<button class="btn btn-primary" onclick="edit_item('.$record['id'].')" >Edit</button>  <button class="btn btn-warning" onclick="delete_item('.$record['id'].')" >Delete</button>'
                    );
                }
                if($post_type =='services_cat')
                {
                    $data[] = array(
                        "created_date" => '<input id="listCheck" onclick="select_check(this)"  name="listCheck" data-id="'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'"  type="checkbox" class="ccheckbox" value="'.$record['id'].'">',
                        "id"=>'<a onclick="edit_item_services_cat('.$record['id'].','.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).')">'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'</a>',
                        "date"=>$dt->format('d/m/Y'),
                        "time"=>$dt->format('H:i'),
                        "username"=>$record['username'],
                        "name"=>$record['name'],
                        "status"=>(($record['status']==1)?("<span class='label label-success'>Enable</span>"):(($record['status']==2)?("<span class='label label-danger'>Disable</span>"):("<span class='label label-warning'>Pending</span>"))),
                        // "created_at"=>$record['created_at'],
                        // "updated_at"=>$record['updated_at'],
                        "since"=>$since,
                        //
                        // "settings"=>'<button class="btn btn-primary" onclick="edit_item('.$record['id'].')" >Edit</button>  <button class="btn btn-warning" onclick="delete_item('.$record['id'].')" >Delete</button>'
                    );
                }
                $category_names =['tutorials_cat','documentations_cat','udapts_cat','news_cat'];
                // if($post_type =='tutorials_cat')
                if (in_array($post_type, $category_names))
                {
                    $data[] = array(
                        "created_date" => '<input id="listCheck" onclick="select_check(this)"  name="listCheck" data-id="'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'"  type="checkbox" class="ccheckbox" value="'.$record['id'].'">',
                        "id"=>'<a onclick="edit_item('.$record['id'].','.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).')">'.$dt->format('dmY').''.str_pad($record['id'], 5, '0', STR_PAD_LEFT).'</a>',
                        "date"=>$dt->format('d/m/Y'),
                        "time"=>$dt->format('H:i'),
                        "username"=>$record['username'],
                        "name"=>$record['name'],
                        "status"=>(($record['status']==1)?("<span class='label label-success'>Enable</span>"):(($record['status']==2)?("<span class='label label-danger'>Disable</span>"):("<span class='label label-warning'>Pending</span>"))),
                        // "created_at"=>$record['created_at'],
                        // "updated_at"=>$record['updated_at'],
                        "since"=>$since,
                        //
                        // "settings"=>'<button class="btn btn-primary" onclick="edit_item('.$record['id'].')" >Edit</button>  <button class="btn btn-warning" onclick="delete_item('.$record['id'].')" >Delete</button>'
                    );
                }
                $sl++;
            }
            ## Response
            $response = array(
                "draw" => intval($draw),
                "iTotalRecords" => $totalRecordwithFilter,
                "iTotalDisplayRecords" => $totalRecords,
                "aaData" => $data
            );
            echo json_encode($response);
            die();
        }
        public function add_cms_flash_news()
        {
            $this->cms_model->updateCmsFlashNews($this->input->post());
            $this->session->set_flashdata('alert', [
                'message' => "Successfully Saved.",
                'class' => "alert-success",
                'type' => "Success"
            ]);
            redirect($_SERVER['HTTP_REFERER']);
        }
        public function add_cms_seo()
        {
            $this->cms_model->updateCmsSeo($this->input->post(),$this->basic_auth->user()->id);
            redirect($_SERVER['HTTP_REFERER']);
        }
        public function add_infobull()
        {
            // var_dump($this->input->post());
            extract($this->input->post());
            $db_array = array(
                'client_status' => ($client_status=='on')?1:0,
                'client_desc' => $client_desc,
                'driver_status' =>($driver_status=='on')?1:0,
                'driver_desc' => $driver_desc,
                'job_status' =>($job_status=='on')?1:0,
                'job_desc' => $job_desc,
                'aff_status' =>($aff_status=='on')?1:0,
                'aff_desc' => $aff_desc,
            );
            $this->cms_model->update_info_bull($db_array);
            redirect($_SERVER['HTTP_REFERER']);
        }
        public function add_cms_banner()
        {
            extract($this->input->post());
    // var_export($_FILES);
    // die();
            $path_to_folder = 'uploads/cms/';
            $config['upload_path'] = realpath($path_to_folder);
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['max_size']= '3000000'; // 3mb
            $new_name = uniqid('cms_', true);
            $new_name = explode('.', $new_name)[0];
            $config['file_name'] = $new_name;
            $this->load->library('upload', $config);
            if($this->upload->do_upload('img'))
            {
                $avatar = $this->upload->data();
                $avatar = $avatar['file_name'];
            }
            else
            {
                // var_export($this->upload->display_errors());
                $avatar = NULL;
            }
    // var_export($avatar);
    // die();
            $old_photos_name=$this->cms_model->save_banner_listing($avatar,$section_name);
            switch (trim($section_name)) {
                case "client_section":
                unlink(PUBPATH."uploads/cms/" . $old_photos_name->client_section);
                break;
                case "driver_section":
                unlink(PUBPATH."uploads/cms/" . $old_photos_name->driver_section);
                break;
                case "partner_section ":
                unlink(PUBPATH."uploads/cms/" . $old_photos_name->partner_section );
                break;
                case "jobs_section ":
                unlink(PUBPATH."uploads/cms/" . $old_photos_name->jobs_section );
                break;
            }
            redirect($_SERVER['HTTP_REFERER']);
        }
        public function ajax_get_cms_sliders()
        {
            extract($this->input->post());
            $resutl =$this->cms_model->getCmsSlider($section);
            echo json_encode($resutl);
            die();
        }
        public function ajax_delete_cms_sliders()
        {
            extract($this->input->post());
            $resutl =$this->cms_model->deleteCmsSlider($id);
            if($resutl)
            {
                unlink(PUBPATH."uploads/cms/slider/" . $resutl->img);
                echo json_encode($resutl);
                die();
            }
            echo json_encode($resutl);
            die();
        }
        public function add_cms_slider()
        {
            // File upload configuration
            $config['upload_path'] = './uploads/cms/slider/';
            $config['allowed_types'] = '*';
            $config['max_size'] = '1024';
            $config['encrypt_name'] = TRUE;
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $errors = array();
            $data = $this->input->post();
            $mapArr = [];
            foreach($data as $key => $val){
                $exp = explode("__",$key);
                if(count($exp) == 2)
                    $mapArr[$exp[1]][$exp[0]] = $val;
            }
            $uploadable = ["photo","leftIcon","rightIcon"];
            if(isset($mapArr) && !empty($mapArr))
                foreach ($mapArr as $key => $item) {
                    foreach ($uploadable as $val) {
                        $imgFile = $val . "File__$key";
                        if (isset($item[$val]) && $item[$val] == "1" && !empty($_FILES[$imgFile])) {
                            $_FILES['photo']['name'] = $_FILES[$imgFile]['name'];
                            $_FILES['photo']['type'] = $_FILES[$imgFile]['type'];
                            $_FILES['photo']['tmp_name'] = $_FILES[$imgFile]['tmp_name'];
                            $_FILES['photo']['error'] = $_FILES[$imgFile]['error'];
                            $_FILES['photo']['size'] = $_FILES[$imgFile]['size'];
                            // Upload file to server
                            if ($this->upload->do_upload('photo')) {
                                $mapArr[$key][$val] = $this->upload->data();
                            } else {
                                array_push($errors, array(
                                    'error' => $this->upload->display_errors()
                                ));
                            }
                        }
                    }
                }
                if (count($errors) != 0) {
                    $resp['errors']  = $errors;
                    $resp['success'] = 0;
                } else {
                //var_dump($mapArr);exit;
                    if(!empty($mapArr))
                        $this->cms_model->save_cms_sliders($mapArr);
                    $resp['success'] = 1;
                }
                echo json_encode($resp);
            }
        }