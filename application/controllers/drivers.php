<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Drivers extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('general');
        $this->load->helper('language');
        $this->load->helper('validate');
        $this->load->helper('mypdf');
        if (!$this->basic_auth->is_login())
            redirect("admin", 'refresh');
        else
        $this->data['user'] = $this->basic_auth->user(); 
        $this->load->model('drivers_model');  
        $this->load->model('request_model');
        $this->load->model('cms_model');
        $this->load->model('base_model');
        $this->load->model('jobs_model');
        $this->load->model('support_model');
        $this->load->model('calls_model');
        $this->load->model('notes_model');
        $this->load->model('notifications_model');
          $this->load->model('userx_model');
        $this->data['configuration'] = get_configuration();

    }

    
    public function index(){

        $this->data['user'] = $this->basic_auth->user();
        $this->data['css_type']     = array("form","booking_datatable","datatable");
        $this->data['active_class'] = "drivers";
        $this->data['gmaps']        = false;
        $this->data['title']        = 'Drivers';
        $this->data['title_link']   = base_url('admin/drivers');
        $this->data['content']      = 'admin/drivers/profile';

        $data = [];
        $this->data['countries'] = $this->cms_model->get_all_countries();
        $this->data['post_data'] = $this->drivers_model->getAllData('vbs_driverpost');
        $this->data['contract_data'] = $this->drivers_model->getAllData('vbs_drivercontract');
        $this->data['driver_status_data'] = $this->drivers_model->getAllData('vbs_driverstatus');
        $this->data['civilite_data'] = $this->drivers_model->getAllData('vbs_drivercivilite');
        $this->data['pattern_data'] = $this->drivers_model->getAllData('vbs_driverpattern');
        $this->data['nature_data'] = $this->drivers_model->getAllData('vbs_drivernature');
        $this->data['hours_data'] = $this->drivers_model->getAllData('vbs_driverhours');
        $this->data['type_data'] = $this->drivers_model->getAllData('vbs_drivertype');
        $this->data['cars'] = $this->drivers_model->getAllData('vbs_carsadded');
        $flashbookdata =  $this->session->flashdata('searchdata');
        if(isset($flashbookdata['record']) ){
          $this->data['driver_data'] = $flashbookdata['record'];
          $this->data['search_from_period']=$flashbookdata['fields']['search_from_period'];
          $this->data['search_to_period']=$flashbookdata['fields']['search_to_period'];
          $this->data['search_status']=$flashbookdata['fields']['search_status'];
          $this->data['search_contract']=$flashbookdata['fields']['search_contract'];
          $this->data['search_hours']=$flashbookdata['fields']['search_hours'];
         }else{
           $this->data['driver_data'] = $this->drivers_model->getAllData('vbs_driverprofile');
         }
         $this->data['googleapikey']=$this->drivers_model->getuniquerecord("vbs_googleapi");
    
        $this->_render_page('templates/admin_template', $this->data);
    }
 public function profileAdd(){

      if ($_SERVER['REQUEST_METHOD'] === 'POST') {
          $this->profileStore();
      }else{
          return redirect("admin/drivers");
      }
  }
  public function profileStore(){

          if (empty($error)) {   
                //date converation
             
            $dateenaissance=str_replace('/', '-', $_POST['dateenaissance']);
            $dateenaissance=strtotime($dateenaissance);
            $dateenaissance = date('Y-m-d',$dateenaissance);


            $datedexpiration=str_replace('/', '-', $_POST['datedexpiration']);
            $datedexpiration=strtotime($datedexpiration);
            $datedexpiration = date('Y-m-d',$datedexpiration);


            $datedexpiration2=str_replace('/', '-', $_POST['datedexpiration2']);
            $datedexpiration2=strtotime($datedexpiration2);
            $datedexpiration2 = date('Y-m-d',$datedexpiration2);


            $datedexpiration3=str_replace('/', '-', $_POST['datedexpiration3']);
            $datedexpiration3=strtotime($datedexpiration3);
            $datedexpiration3 = date('Y-m-d',$datedexpiration3);


            $datedexpiration4=str_replace('/', '-', $_POST['datedexpiration4']);
            $datedexpiration4=strtotime($datedexpiration4);
            $datedexpiration4 = date('Y-m-d',$datedexpiration4);

            $datedexpiration5=str_replace('/', '-', $_POST['datedexpiration5']);
            $datedexpiration5=strtotime($datedexpiration5);
            $datedexpiration5 = date('Y-m-d',$datedexpiration5);


            $datedelivrance1=str_replace('/', '-', $_POST['datedelivrance1']);
            $datedelivrance1=strtotime($datedelivrance1);
            $datedelivrance1 = date('Y-m-d',$datedelivrance1);

            $datedelivrance2=str_replace('/', '-', $_POST['datedelivrance2']);
            $datedelivrance2=strtotime($datedelivrance2);
            $datedelivrance2 = date('Y-m-d',$datedelivrance2);


            $datedelivrance3=str_replace('/', '-', $_POST['datedelivrance3']);
            $datedelivrance3=strtotime($datedelivrance3);
            $datedelivrance3 = date('Y-m-d',$datedelivrance3);

            $datedelivrance4=str_replace('/', '-', $_POST['datedelivrance4']);
            $datedelivrance4=strtotime($datedelivrance4);
            $datedelivrance4 = date('Y-m-d',$datedelivrance4);

            $datedentree=str_replace('/', '-', $_POST['datedentree']);
            $datedentree=strtotime($datedentree);
            $datedentree = date('Y-m-d',$datedentree);


            $datedesortie=str_replace('/', '-', $_POST['datedesortie']);
            $datedesortie=strtotime($datedesortie);
            $datedesortie = date('Y-m-d',$datedesortie);

            $car_affect_date=str_replace('/', '-', $_POST['car_affect_date']);
            $car_affect_date=strtotime($car_affect_date);
            $car_affect_date = date('Y-m-d',$car_affect_date);

            
              //date converation
               $username     = $this->input->post('prenom') . ' ' . $this->input->post('nom');
               $email        = strtolower($this->input->post('email'));
               $password     = date("dmY", strtotime($_POST['dateenaissance']));
               $user_role    = 7;
               $isemailexist = $this->drivers_model->isemailalreayexist(['email'=>$email],'vbs_users');

               if($isemailexist){
                       $this->session->set_flashdata('alert', [
                           'message' => ": Email is already exist in users.",
                           'class' => "alert-danger",
                           'type' => "Error"
                       ]);
                       redirect('admin/drivers', 'refresh');
               }


               $user_group = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('groups') . " WHERE name = 'drivers'");

               $group_id        = array($user_group[0]->id);
               $additional_data = array(
                   'civility'     => $this->drivers_model->getSingleRecord('vbs_drivercivilite',['id'=>$this->input->post('civilite')])->civilite,
                   'first_name'   => $this->input->post('prenom'),
                   'last_name'    => $this->input->post('nom'),
                   'phone'        => $this->input->post('phone'),
                   'country'      => $this->input->post('paysdenaissance'),
                   'region'       => $this->input->post('region'),
                   'city'         => $this->input->post('villedenaissance'),
                   'zipcode'      => $this->input->post('postalcode'),
                   'address'      => $this->input->post('address'),
                   'address1'     => $this->input->post('address2'),
                   'image'        => $this->do_user_upload_image($_FILES['driverImg'],'driverImg'),
                   //'ville'        => trim($this->input->post('ville')),
                   'mobile'       => trim($this->input->post('mobile')),
                   'dob'          => $dateenaissance
               );

               if ($this->ion_auth->register($user_role, $username, $password, $email, $additional_data, $group_id)) {
                   $user_rec = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('users') . " WHERE email = '" . $email . "'");

              $user = $this->basic_auth->user();
              $user_id=$user->id;
             
            
              $id = $this->drivers_model->createRecord('vbs_driverprofile',[
                  'status'                      =>  $_POST['status'],
                  'civilite'                    =>  $_POST['civilite'],
                  'nom'                         =>  trim($_POST['nom']),
                  'prenom'                      =>  trim($_POST['prenom']),
                  'address'                     =>  trim($_POST['address']),
                  'address2'                    =>  trim($_POST['address2']),
                  'postalcode'                  =>  trim($_POST['postalcode']),
                  'email'                       =>  trim($_POST['email']),
                  'phone'                       =>  trim($_POST['phone']),
                  'mobile'                      =>  trim($_POST['mobile']),
                  //'ville'                       =>  trim($_POST['ville']),
                  'car_affect_date'             =>  $car_affect_date,
                  'car_affect_time'             =>  $_POST['car_affect_time'],
                  'car_id'                      =>  $_POST['car_id'],
                  'driverImg'                   =>  $this->do_upload_image($_FILES['driverImg'],'driverImg'),
                  'dateenaissance'              =>  $dateenaissance,
                  'villedenaissance'            =>  $_POST['villedenaissance'],
                  'region'                      =>  $_POST['region'],
                  'paysdenaissance'             =>  $_POST['paysdenaissance'],
                  'poste'                       =>  $this->input->post('poste'),
                  'datedentree'                 =>  $datedentree,
                  'datedesortie'                =>  $datedesortie,
                  'motif'                       =>  $this->input->post('motif'),
                  'typecontrat'                 =>  $this->input->post('typecontrat'),
                  'natureducontrat'             =>  $this->input->post('natureducontrat'),
                  'nombredheuremensuel'         =>  $this->input->post('nombredheuremensuel'),
                  'typedepiecedidentite'        =>  $this->input->post('typedepiecedidentite'),
                  'typedepiecedidentite'        =>  $this->input->post('typedepiecedidentite'),
                  'numerropiecedidentite'       =>  trim($this->input->post('numerropiecedidentite')),
                  'datedexpiration'             =>  $datedexpiration,
                  'numerodesecuritesociale'     =>  trim($this->input->post('numerodesecuritesociale')),
                  'numerodesecuritesocialefile' =>  $this->do_upload_image($_FILES['numerodesecuritesocialefile'],'numerodesecuritesocialefile'),
                  'pumeropermis'                =>  trim($this->input->post('pumeropermis')),
                  'upload1'                     =>  $this->do_upload_image($_FILES['upload1'],'upload1'),
                  'upload2'                     =>  $this->do_upload_image($_FILES['upload2'],'upload2'),
                  'datedelivrance1'             =>  $datedelivrance1,
                  'datedexpiration2'            =>  $datedexpiration2,
                  'certificatmedicate'          =>  $this->do_upload_image($_FILES['certificatmedicate'],'certificatmedicate'),
                  'datedelivrance2'             =>  $datedelivrance2,
                  'datedexpiration3'            =>  $datedexpiration3,
                  'psc1'                        =>  $this->do_upload_image($_FILES['psc1'],'psc1'),
                  'datedexpiration4'            =>  $datedexpiration4,
                  'datedelivrance3'             =>  $datedelivrance3,
                  'medecinedetravai'            =>  $this->do_upload_image($_FILES['medecinedetravai'],'medecinedetravai'),
                  'datedelivrance4'             =>  $datedelivrance4,
                  'datedexpiration5'            =>  $datedexpiration5,
                  'autrdeiplome1'               =>  $this->do_upload_image($_FILES['autrdeiplome1'],'autrdeiplome1'),
                  'autrediplome2'               =>  $this->do_upload_image($_FILES['autrediplome2'],'autrediplome2'),
                  'autrediplome3'               =>  $this->do_upload_image($_FILES['autrediplome3'],'autrediplome3'),
                  'userid'                      =>  $user_rec[0]->id,
                  'added_by_user'               =>  $user_id,
                  'fleetoperator'               =>  $this->input->post('fleetoperator'),
                  'driverworkingdays'           =>  $_POST['driverworkingdays']
              ]);
              
              if($id){
                   $exp_array=array(
                    'datedexpiration' =>   $datedexpiration,
                    'datedexpiration2' =>  $datedexpiration2,
                    'datedexpiration3' =>  $datedexpiration3,
                    'datedexpiration4' =>  $datedexpiration4,
                    'datedexpiration5' =>  $datedexpiration5,
                   );

                   $isremindercreate=$this->reminder_store($id,$exp_array);
                   $isnotificationcreate=$this->notification_store($id,$exp_array);
                   if($isremindercreate == true && $isnotificationcreate == true){
                            $this->session->set_flashdata('alert', [
                             'message'   => ": Successfully added a driver profile .",
                             'class'     => "alert-success",
                             'status_id' => "1",
                             'type'      => "Success"
                              ]);
                           redirect('admin/drivers');
                   }else{
                        $this->data['alert'] = [
                          'message' => "Driver's notification or reminder missing in notification or reminder page. Please add these and try again.",
                          'class'   => "alert-danger",
                          'status_id' => "1",
                          'type'    => "Error"
                            ];
                            redirect('admin/drivers');
                        }
                   }
              else{
                   $this->data['alert'] = [
                  'message' => 'Please try again',
                  'class'   => "alert-danger",
                  'status_id' => "1",
                  'type'    => "Error"
                    ];
                    redirect('admin/drivers');
                  }
            }
          } else {
              $this->data['alert'] = [
                  'message' => 'Please try again',
                  'class'   => "alert-danger",
                  'status_id' => "1",
                  'type'    => "Error"
              ];
              redirect('admin/drivers');
          }
  }


    public function profileEdit(){
      

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $id   = $this->input->post('profile_id');
            if (!empty($id)) {

                   //date converation
              $dateenaissance=str_replace('/', '-', $_POST['dateenaissance']);
              $dateenaissance=strtotime($dateenaissance);
              $dateenaissance = date('Y-m-d',$dateenaissance);


              $datedexpiration=str_replace('/', '-', $_POST['datedexpiration']);
              $datedexpiration=strtotime($datedexpiration);
              $datedexpiration = date('Y-m-d',$datedexpiration);


              $datedexpiration2=str_replace('/', '-', $_POST['datedexpiration2']);
              $datedexpiration2=strtotime($datedexpiration2);
              $datedexpiration2 = date('Y-m-d',$datedexpiration2);


              $datedexpiration3=str_replace('/', '-', $_POST['datedexpiration3']);
              $datedexpiration3=strtotime($datedexpiration3);
              $datedexpiration3 = date('Y-m-d',$datedexpiration3);


              $datedexpiration4=str_replace('/', '-', $_POST['datedexpiration4']);
              $datedexpiration4=strtotime($datedexpiration4);
              $datedexpiration4 = date('Y-m-d',$datedexpiration4);

              $datedexpiration5=str_replace('/', '-', $_POST['datedexpiration5']);
              $datedexpiration5=strtotime($datedexpiration5);
              $datedexpiration5 = date('Y-m-d',$datedexpiration5);


              $datedelivrance1=str_replace('/', '-', $_POST['datedelivrance1']);
              $datedelivrance1=strtotime($datedelivrance1);
              $datedelivrance1 = date('Y-m-d',$datedelivrance1);

              $datedelivrance2=str_replace('/', '-', $_POST['datedelivrance2']);
              $datedelivrance2=strtotime($datedelivrance2);
              $datedelivrance2 = date('Y-m-d',$datedelivrance2);


              $datedelivrance3=str_replace('/', '-', $_POST['datedelivrance3']);
              $datedelivrance3=strtotime($datedelivrance3);
              $datedelivrance3 = date('Y-m-d',$datedelivrance3);

              $datedelivrance4=str_replace('/', '-', $_POST['datedelivrance4']);
              $datedelivrance4=strtotime($datedelivrance4);
              $datedelivrance4 = date('Y-m-d',$datedelivrance4);


              $datedentree=str_replace('/', '-', $_POST['datedentree']);
              $datedentree=strtotime($datedentree);
              $datedentree = date('Y-m-d',$datedentree);


              $datedesortie=str_replace('/', '-', $_POST['datedesortie']);
              $datedesortie=strtotime($datedesortie);
              $datedesortie = date('Y-m-d',$datedesortie);
             
              $car_affect_date=str_replace('/', '-', $_POST['car_affect_date']);
              $car_affect_date=strtotime($car_affect_date);
              $car_affect_date = date('Y-m-d',$car_affect_date);
              
                //date converation

               
                $driver        = $this->drivers_model->getsinglerecord('vbs_driverprofile',['id'=>$id]);
                $userprofile   = $this->drivers_model->getSingleRecord('vbs_users',['id'=>$driver->userid]);

                $driveruserimg='';
                $driverImg='';
                $upload1='';
                $upload2=''; 
                $certificatmedicate='';
                $psc1='';
                $medecinedetravai='';
                $autrdeiplome1='';
                $autrediplome2='';
                $autrediplome3='';
                $numerodesecuritesocialefile='';
                

                if(!empty($_FILES['driverImg']['name']))
                {
                  $driveruserimg=$this->do_user_upload_image($_FILES['driverImg'],'driverImg');
                }else{
                    $driveruserimg=$userprofile->image;
                }
                if(!empty($_FILES['driverImg']['name']))
                {
                  $driverImg=$this->do_upload_image($_FILES['driverImg'],'driverImg');
                }else{
                    $driverImg=$driver->driverImg;
                }
                if(!empty($_FILES['upload1']['name']))
                {
                  $upload1=$this->do_upload_image($_FILES['upload1'],'upload1');
                }else{
                    $upload1=$driver->upload1;
                }
                if(!empty($_FILES['upload2']['name']))
                {
                  $upload2=$this->do_upload_image($_FILES['upload2'],'upload2');
                }else{
                    $upload2=$driver->upload2;
                }
                if(!empty($_FILES['certificatmedicate']['name']))
                {
                  $certificatmedicate=$this->do_upload_image($_FILES['certificatmedicate'],'certificatmedicate');
                }else{
                    $certificatmedicate=$driver->certificatmedicate;
                }
                if(!empty($_FILES['psc1']['name']))
                {
                  $psc1=$this->do_upload_image($_FILES['psc1'],'psc1');
                }else{
                    $psc1=$driver->psc1;
                }
                if(!empty($_FILES['medecinedetravai']['name']))
                {
                  $medecinedetravai=$this->do_upload_image($_FILES['medecinedetravai'],'medecinedetravai');
                }else{
                    $medecinedetravai=$driver->medecinedetravai;
                }
                if(!empty($_FILES['autrdeiplome1']['name']))
                {
                  $autrdeiplome1=$this->do_upload_image($_FILES['autrdeiplome1'],'autrdeiplome1');
                }else{
                    $autrdeiplome1=$driver->autrdeiplome1;
                }
                if(!empty($_FILES['autrediplome2']['name']))
                {
                  $autrediplome2=$this->do_upload_image($_FILES['autrediplome2'],'autrediplome2');
                }else{
                    $autrediplome2=$driver->autrediplome2;
                }
                if(!empty($_FILES['autrediplome3']['name']))
                {
                  $autrediplome3=$this->do_upload_image($_FILES['autrediplome3'],'autrediplome3');
                }else{
                    $autrediplome3=$driver->autrediplome3;
                }
                 if(!empty($_FILES['numerodesecuritesocialefile']['name']))
                {
                  $numerodesecuritesocialefile=$this->do_upload_image($_FILES['numerodesecuritesocialefile'],'numerodesecuritesocialefile');
                }else{
                    $numerodesecuritesocialefile=$driver->numerodesecuritesocialefile;
                }

               

   /* $email=$this->input->post('email');
    $isemailexist=$this->drivers_model->isemailalreayexist(['id !='=> $userprofile->id,'email'=>$email],'vbs_users');
                if($isemailexist){
                    $this->session->set_flashdata('alert', [
                            'message' => ": Email is already exist.",
                            'class' => "alert-danger",
                            'type' => "Error"
                        ]);
                        redirect('admin/drivers', 'refresh');
                }

            $password='';
            if(empty($this->input->post('password'))){
                $password=$userprofile->password;
            }else{
                $password= md5($this->input->post('password'));
            }*/
                 //$password=md5(date("dmY", strtotime($_POST['dateenaissance'])));
             $db_array = array(
                    'civility'     => $this->drivers_model->getSingleRecord('vbs_drivercivilite',['id'=>$this->input->post('civilite')])->civilite,
                    'first_name'=> $this->input->post('prenom'),
                    'last_name' => $this->input->post('nom'),
                    'phone'     => $this->input->post('phone'),
                    'country'   => $this->input->post('paysdenaissance'),
                    'region'    => $this->input->post('region'),
                    'city'      => $this->input->post('villedenaissance'),
                    'zipcode'   => $this->input->post('postalcode'),
                    'address'   => $this->input->post('address'),
                    'address1'  => $this->input->post('address2'),
                    //'password'  => $password,
                    'image'     => $driveruserimg,
                    'ville'     => trim($this->input->post('ville')),
                    'mobile'    => trim($this->input->post('mobile')),
                    'dob'       => $dateenaissance
                );

                $updateuserprofile=$this->userx_model->update($db_array,$userprofile->id);
               
               
                if($updateuserprofile){
                      $update=$this->drivers_model->createUpdate('vbs_driverprofile',[
                        'status' =>  $this->input->post('status'),
                        'civilite' =>  $this->input->post('civilite'),
                        'nom' =>  $this->input->post('nom'),
                        'prenom' =>  $this->input->post('prenom'),
                        'address' =>  $this->input->post('address'),
                        'address2' =>  $this->input->post('address2'),
                        'postalcode' =>  $this->input->post('postalcode'),
                        'ville' =>  $this->input->post('ville'),
                        'phone' =>  $this->input->post('phone'),
                        'mobile'           =>  trim($_POST['mobile']),
                        'ville'            =>  trim($_POST['ville']),
                        'car_affect_date'  =>  $car_affect_date,
                        'car_affect_time'  =>  $_POST['car_affect_time'],
                        'car_id'           =>  $_POST['car_id'],
                        //'email' =>  $this->input->post('email'),
                        'driverImg' => $driverImg,
                        'dateenaissance'         => $dateenaissance,
                        'villedenaissance' =>  $this->input->post('villedenaissance'),
                        'paysdenaissance' =>  $this->input->post('paysdenaissance'),
                        'poste' =>  $this->input->post('poste'),
                        'datedentree' =>  $datedentree,
                        'datedesortie' =>  $datedesortie,
                        'motif' =>  $this->input->post('motif'),
                        'typecontrat' =>  $this->input->post('typecontrat'),
                        'natureducontrat' =>  $this->input->post('natureducontrat'),
                        'nombredheuremensuel' =>  $this->input->post('nombredheuremensuel'),
                        'typedepiecedidentite' =>  $this->input->post('typedepiecedidentite'),
                        'typedepiecedidentite' =>  $this->input->post('typedepiecedidentite'),
                        'numerropiecedidentite' =>  $this->input->post('numerropiecedidentite'),
                        'datedexpiration' =>  $datedexpiration,
                        'pumeropermis' =>  $this->input->post('pumeropermis'),
                        'upload1' => $upload1,
                        'upload2' => $upload2,
                        'datedelivrance1' =>  $datedelivrance1,
                        'datedexpiration2' =>  $datedexpiration2,
                        'certificatmedicate' => $certificatmedicate,
                        'datedelivrance2' => $datedelivrance2,
                        'datedexpiration3' =>  $datedexpiration3,
                        'psc1' => $psc1,
                        'datedexpiration4' =>  $datedexpiration4,
                        'datedelivrance3' =>  $datedelivrance3,
                        'medecinedetravai' => $medecinedetravai,
                        'datedelivrance4' =>  $datedelivrance4,
                        'datedexpiration5' =>  $datedexpiration5,
                        'autrdeiplome1' => $autrdeiplome1,
                        'autrediplome2' => $autrediplome2,
                        'autrediplome3' => $autrediplome3,
                        'numerodesecuritesocialefile' => $numerodesecuritesocialefile,
                        'fleetoperator' =>  $this->input->post('fleetoperator'),
                        'numerodesecuritesociale' => $this->input->post('numerodesecuritesociale'),
                        'driverworkingdays'           =>  $_POST['driverworkingdays']
                      ], $id);
                      if ($update){
                        $exp_array=array(
                          'datedexpiration' =>   $datedexpiration,
                          'datedexpiration2' =>  $datedexpiration2,
                          'datedexpiration3' =>  $datedexpiration3,
                          'datedexpiration4' =>  $datedexpiration4,
                          'datedexpiration5' =>  $datedexpiration5
                         );

                         $isremindercreate=$this->reminder_update($id,$exp_array);
                         $isnotificationcreate=$this->notification_update($id,$exp_array);
                         if($isremindercreate == true && $isnotificationcreate == true){
                                 $this->session->set_flashdata('alert', [
                                    'message' => ": Successfully updated a driver profile.",
                                    'class' => "alert-success",
                                    'status_id' => "1",
                                    'type' => "Success"
                                ]);
                                redirect('admin/drivers');
                         }else{
                              $this->data['alert'] = [
                                'message' => "Driver's notification or reminder missing in notification or reminder page. Please add these and try again.",
                                'class'   => "alert-danger",
                                'status_id' => "1",
                                'type'    => "Error"
                                  ];
                                  redirect('admin/drivers');
                              }
                         
                      }else {
                        $this->data['alert'] = [
                       'message' => 'Please try again',
                       'class' => "alert-danger",
                       'status_id' => "1",
                       'type' => "Error"
                       ];
                       redirect('admin/drivers');
                    }
                }else {
                    $this->data['alert'] = [
                    'message' => 'Please try again',
                    'class' => "alert-danger",
                    'status_id' => "1",
                    'type' => "Error"
                     ];
                     redirect('admin/drivers');
                }
       
        }else{
           redirect('admin/drivers');
          }
     }else{
           redirect('admin/drivers');
          }
        
    }
    public function profileDelete(){
        
        $id=$_POST['delete_profile_id'];
        $driver=$this->drivers_model->getsinglerecord('vbs_driverprofile',['id'=>$id]);
        $del=$this->drivers_model->deleteDriverProfile($driver);
        if ($del==true) {
        $this->session->set_flashdata('alert', [
                'message' => "Successfully deleted.",
                'class' => "alert-success",
                'status_id' => "1",
                'type' => "Success"
        ]);
        redirect('admin/drivers');
        }
        else {
            $this->session->set_flashdata('alert', [
                    'message' => "Please try again.",
                    'class' => "alert-danger",
                    'status_id' => "1",
                    'type' => "Error"
            ]);
            redirect('admin/drivers');
        }
    }
    public function get_ajax_profile_data(){
        $id=(int)$_GET['profile_id'];
       

        if($id>0){

                $data = [];

        //config data
        $this->data['post_data'] = $this->drivers_model->getAllData('vbs_driverpost');
        $this->data['contract_data'] = $this->drivers_model->getAllData('vbs_drivercontract');
        $this->data['driver_status_data'] = $this->drivers_model->getAllData('vbs_driverstatus');
        $this->data['civilite_data'] = $this->drivers_model->getAllData('vbs_drivercivilite');
        $this->data['pattern_data'] = $this->drivers_model->getAllData('vbs_driverpattern');
        $this->data['nature_data'] = $this->drivers_model->getAllData('vbs_drivernature');
        $this->data['hours_data'] = $this->drivers_model->getAllData('vbs_driverhours');
        $this->data['type_data'] = $this->drivers_model->getAllData('vbs_drivertype');
         $this->data['cars'] = $this->drivers_model->getAllData('vbs_carsadded');
        //config data

        $this->data['driver'] = $this->drivers_model->getsinglerecord('vbs_driverprofile',['id'=>$id]);
        //$this->data['driverprofile'] = $this->drivers_model->getsinglerecord('vbs_users',['id'=>$this->data['driver']->userid]);
        $this->data['countries'] = $this->cms_model->get_all_countries();
        $this->data['regions'] = $this->drivers_model->ajax_get_region_listing($this->data['driver']->paysdenaissance);
        $this->data['cities'] = $this->drivers_model->ajax_get_cities_listings($this->data['driver']->region);
        $this->data['drivercustomreminders']=$this->drivers_model->getMultipleRecord('vbs_drivers_customreminders',['driver_id'=> $this->data['driver']->id]);
        $this->data['drivernotifications']=$this->drivers_model->getMultipleRecord("vbs_drivers_notification",['driver_id'=>$this->data['driver']->id]);
        $this->data['driverreminders']=$this->drivers_model->getMultipleRecord("vbs_drivers_reminders",['driver_id'=>$this->data['driver']->id]);

        $result=$this->load->view('admin/drivers/index', $this->data, TRUE);
        echo $result;
        exit();
        }else{
            $result="Please select Record!";
        }
        echo $result;
        exit;
    }
/* --------------------------------- Driver Reminders Add Section start -------------------------------------------- */
function reminder_store($driver_id,$data){

 // $isreminderexist=$this->drivers_model->isdataalreayexist('vbs_drivers_reminders',['driver_id'=>$driver_id]);
  //if($isreminderexist){
    //$del=$this->drivers_model->createDelete('vbs_drivers_reminders',['driver_id'=>$driver_id]);
  //}
  $currenttime = date('h : i');
  $module='6';
  $statusexpired='1';
  $remindersdata   = $this->drivers_model->getMultipleRecord('vbs_reminders',['module' => $module,'status'=> $statusexpired]);
                       
  if($remindersdata){
    $remindertype=1;

    foreach ($remindersdata as $reminder) {
          $reminders = json_decode($reminder->reminders);
          $days         = $reminders[0]->reminder_days;

      //day add into date
      $new_datedexpiration  = date('Y-m-d', strtotime($data['datedexpiration']. ' - '.$days.' days'));
      $new_datedexpiration2 = date('Y-m-d', strtotime($data['datedexpiration2']. ' - '.$days.' days'));
      $new_datedexpiration3 = date('Y-m-d', strtotime($data['datedexpiration3']. ' - '.$days.' days'));
      $new_datedexpiration4 = date('Y-m-d', strtotime($data['datedexpiration4']. ' - '.$days.' days'));
      $new_datedexpiration5 = date('Y-m-d', strtotime($data['datedexpiration5']. ' - '.$days.' days'));
      //day add into date

           $reminderid = $this->drivers_model->createRecord('vbs_drivers_reminders',[
                           'typeiddate'                  => $new_datedexpiration,
                           'typeidtime'                  => $currenttime,
                           'permitnumberdate'            => $new_datedexpiration2,
                           'permitnumbertime'            => $currenttime,
                           'medicalcertificatedate'      => $new_datedexpiration3,
                           'medicalcertificatetime'      => $currenttime,
                           'psc1date'                    => $new_datedexpiration4,
                           'psc1time'                    => $currenttime,
                           'medicineoftravaidate'        => $new_datedexpiration5,
                           'medicineoftravaitime'        => $currenttime,
                           'driver_id'                   => $driver_id,
                           'reminder_id'                 => $reminder->id,
                           'type'                        => $remindertype
                          ]); 
           $remindertype++;
    }
    return true;
  }else{
    return false;
  }
}

/* --------------------------------- Driver Reminders Add Section close -------------------------------------------- */

/* --------------------------------- Driver notification Add Section start -------------------------------------------- */
function notification_store($driver_id,$data){
 
  //$isnotificationexist=$this->drivers_model->isdataalreayexist('vbs_drivers_notification',['driver_id'=>$driver_id]);
  //if($isnotificationexist){
    //$del=$this->drivers_model->createDelete('vbs_drivers_notification',['driver_id'=>$driver_id]);
  //}
  $currenttime = date('h : i');
  $department='7';
  $statusexpired='1';
  $notificationdata  = $this->drivers_model->getSingleRecord('vbs_notifications',['department' => $department,'status'=>$statusexpired]);
                       
  if($notificationdata){
 
         $notificationid = $this->drivers_model->createRecord('vbs_drivers_notification',[
                         'typeiddate'                  => $data['datedexpiration'],
                         'typeidtime'                  => $currenttime,
                         'permitnumberdate'            => $data['datedexpiration2'],
                         'permitnumbertime'            => $currenttime,
                         'medicalcertificatedate'      => $data['datedexpiration3'],
                         'medicalcertificatetime'      => $currenttime,
                         'psc1date'                    => $data['datedexpiration4'],
                         'psc1time'                    => $currenttime,
                         'medicineoftravaidate'        => $data['datedexpiration5'],
                         'medicineoftravaitime'        => $currenttime,
                         'driver_id'                   => $driver_id,
                         'notification_id'             => $notificationdata->id,
                        ]); 
         
    return true;     
  }else{
    return false;
  }
} 
/* --------------------------------- Driver notification Add Section close -------------------------------------------- */
/* --------------------------------- Driver Reminders Update Section start -------------------------------------------- */
function reminder_update($driver_id,$data){
  
  $driver_reminder_prev_data=$this->drivers_model->getOneRecord('vbs_drivers_reminders',['driver_id' => $driver_id]);
  $reminderdata   = $this->drivers_model->getOneRecord('vbs_reminders',['id' => $driver_reminder_prev_data->reminder_id]);

  if($reminderdata){
    
          $reminders = json_decode($reminderdata->reminders);
          $days      = $reminders[0]->reminder_days;

      //day add into date
      $new_datedexpiration  = date('Y-m-d', strtotime($data['datedexpiration']. ' - '.$days.' days'));
      $new_datedexpiration2 = date('Y-m-d', strtotime($data['datedexpiration2']. ' - '.$days.' days'));
      $new_datedexpiration3 = date('Y-m-d', strtotime($data['datedexpiration3']. ' - '.$days.' days'));
      $new_datedexpiration4 = date('Y-m-d', strtotime($data['datedexpiration4']. ' - '.$days.' days'));
      $new_datedexpiration5 = date('Y-m-d', strtotime($data['datedexpiration5']. ' - '.$days.' days'));
      //day add into date

           $reminderid = $this->drivers_model->createUpdate('vbs_drivers_reminders',[
                           'typeiddate'                  => $new_datedexpiration,
                           'permitnumberdate'            => $new_datedexpiration2,
                           'medicalcertificatedate'      => $new_datedexpiration3,
                           'psc1date'                    => $new_datedexpiration4,
                           'medicineoftravaidate'        => $new_datedexpiration5
                          ],$driver_reminder_prev_data->id); 
          if($reminderid){return true;}
          else{return false;} 
  }else{
    return false;
  }
}

/* --------------------------------- Driver Reminders Update Section close -------------------------------------------- */

/* --------------------------------- Driver notification Update Section start -------------------------------------------- */
function notification_update($driver_id,$data){
 $driver_notification_prev_data=$this->drivers_model->getOneRecord('vbs_drivers_notification',['driver_id' => $driver_id]);
              
  if($driver_notification_prev_data){
         $notificationid = $this->drivers_model->createUpdate('vbs_drivers_notification',[
                         'typeiddate'                  => $data['datedexpiration'],
                         'permitnumberdate'            => $data['datedexpiration2'],
                         'medicalcertificatedate'      => $data['datedexpiration3'],
                         'psc1date'                    => $data['datedexpiration4'],
                         'medicineoftravaidate'        => $data['datedexpiration5']
                        ],$driver_notification_prev_data->id);     
     if($notificationid){return true;}
     else{return false;} 
  }else{
    return false;
  }
} 
/* --------------------------------- Driver notification Update Section close -------------------------------------------- */
  /* --------------------------------- Driver notification pdf  Section start -------------------------------------------- */

    public function notification_pdf($drivernotificationid,$driver_id,$columnnumber){

     $pdfArrays=createdrivernotificationpdf($drivernotificationid,$driver_id,$columnnumber);  
      $this->data['pdf']=$pdfArrays['pdf']; 
      $this->data['fileName']=$pdfArrays['fileName'];  

      $this->load->view('admin/drivers/profile/notificationpdf',$this->data);
    }
   /* --------------------------------- Driver notification pdf  Section close -------------------------------------------- */
 /* --------------------------------- Driver reminder pdf  Section start -------------------------------------------- */

     public function reminder_pdf($driverreminderid,$driver_id,$columnnumber,$leve){

     $pdfArrays=createdriverreminderpdf($driverreminderid,$driver_id,$columnnumber);  
      $this->data['pdf']=$pdfArrays['pdf']; 
      $this->data['fileName']=$pdfArrays['fileName'];  

      $this->load->view('admin/drivers/profile/reminderpdf',$this->data);
    }
     /* --------------------------------- Driver reminder pdf  Section close -------------------------------------------- */

   /* --------------------------------- Driver custom reminder  Section start -------------------------------------------- */

  public function addcustomreminders(){
  
  $driver_id=$_GET['driver_id'];
  $title=$_GET['title'];
  $date=$_GET['date'];
  $time=$_GET['time'];
  $comment=$_GET['comment'];

  //add custom comment
          
                     $date=$date;
                     $date=str_replace('/', '-', $date);
                     $date=strtotime($date);
                     $date = date('Y-m-d',$date);
                      $crid = $this->drivers_model->createRecord("vbs_drivers_customreminders",[
                        'date'     => $date,
                        'time'     => $time,
                        'title'    => $title,
                        'comment'  => $comment,
                        'driver_id' => $driver_id
                        ]);
                  
             if($crid){
                    $record=array(
                        'date'     => $_GET['date'],
                        'time'     => $time,
                        'title'    => $title,
                        'comment'  => $comment,
                        'driver_id' => $driver_id,
                        'customreminderid'=>$crid
                    );
                    $data = ['result'=>"200",'record'=>$record];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
             }
             else{
                    $data = ['result'=>"201",'error'=>'something went wrong.'];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
             }
}


public function removecustomreminders(){
  $customreminderid=$_GET['customreminderid'];
  $del=$this->drivers_model->createDelete('vbs_drivers_customreminders',['id'=>$customreminderid]);
  if($del){
                    $data = ['result'=>"200"];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
  }else{
                    $data = ['result'=>"201",'error'=>'something went wrong.'];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
  }
}
/* --------------------------------- Driver custom reminder  Section close -------------------------------------------- */

//search
public function get_drivers_bysearch(){
  
  $from_period=$this->input->post('from_period');
  $to_period=$this->input->post('to_period');
  $statut=$this->input->post('statut');
  $typecontrat=$this->input->post('typecontrat');
  $nombredheuremensuel=$this->input->post('nombredheuremensuel');
//$from_period='26/12/2020';
//$to_period='28/12/2020';

  $search=array();
    
     if(!empty($from_period)){

       $from_period=str_replace('/', '-', $from_period);
       $from_period=strtotime($from_period);
       $from_period = date('Y-m-d',$from_period);

       $search["DATE_FORMAT(created_at,'%Y-%m-%d') >="]=$from_period;
       $from_period = strtotime($from_period);
       $from_period = date('d/m/Y',$from_period);

     }
     if(!empty($to_period)){
         $to_period=str_replace('/', '-', $to_period);
         $to_period=strtotime($to_period);
         $to_period = date('Y-m-d',$to_period);

         $search["DATE_FORMAT(created_at,'%Y-%m-%d') <="]=$to_period;
         $to_period = strtotime($to_period);
         $to_period = date('d/m/Y',$to_period);
     }
     
     if($statut != ''){
      $search['status']=$statut;
     }
     if($typecontrat != ''){
      $search['typecontrat']=$typecontrat;
     }
     if($nombredheuremensuel != ''){
      $search['nombredheuremensuel']=$nombredheuremensuel;
     }
    
  
     $data=$this->drivers_model->getAllData('vbs_driverprofile',$search);
     $this->session->set_flashdata('searchdata', [
              'record' =>  $data,  
              'fields' =>  array("search_from_period"=>$from_period,"search_to_period"=>$to_period,"search_status"=>$statut,'search_contract'=>$typecontrat,'search_hours'=>$nombredheuremensuel)
                                                              
            ]);
  
          return redirect("admin/drivers");
        
}
//search
//send access detail
public function sendaccessdetail(){
  $email    = $this->input->post('email');
  $password = $this->input->post('password');
  $id       = $this->input->post('id');

  if(!empty($email) && !empty($password) && !empty($id)){
     $driver=$this->drivers_model->getsinglerecord('vbs_driverprofile',['id'=>$id]);
    //sent email
    //Email Sent
     
            $content="Hi ".$this->drivers_model->getSingleRecord('vbs_drivercivilite',['id'=>$driver->civilite])->civilite." ".$driver->prenom." ".$driver->nom.",<br>";
            $content.="You 've received access of your account from ECAB LLC."."<br>";
            $content.="You account email address is ".$email." and your password is ".$password;
                     $this->load->library('mail_bookingsender');
                    $mailData['email']   = $email;
                    $mailData['subject'] = "You've received messages from ECAB LLC";
                    $mailData['message']    = $content;
                    $sent = $this->mail_bookingsender->sendMail($mailData);
                    //return $sent;
    //sent email
              if($sent){
                    $data = ['result'=>"200"];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
              }else{
                   $data = ['result'=>"201"];
                   header('Content-Type: application/json'); 
                   echo json_encode($data);
              }
           
        }else{
            $data = ['result'=>"201"];
            header('Content-Type: application/json'); 
            echo json_encode($data);
        }
}
//send access detail
/* add driver notification */
public function adddrivernotification(){

  $column1=$_POST['column1'];
  $column2=$_POST['column2'];
  $date=$_POST['date'];
  $time=$_POST['time'];
  $driver_id=$_POST['driver_id'];
  $department='7';
  $statusexpired='1';
  $notificationdata  = $this->drivers_model->getSingleRecord('vbs_notifications',['department' => $department,'status'=>$statusexpired]);

  //convert into date format
  $date=str_replace('/', '-', $date);
  $date=strtotime($date);
  $date = date('Y-m-d',$date);
  //convert into date format
  $notificationid = $this->drivers_model->createRecord('vbs_drivers_notification',[
                           $column1                     => $date,
                           $column2                     => $time,
                          'driver_id'                   => $driver_id,
                          'notification_id'             => $notificationdata->id,
                        ]); 
      $record=array('notification_id'=>$notificationdata->id,'status'=>'Pending');
      $data = ['result'=>"200",'record'=>$record];
      header('Content-Type: application/json'); 
      echo json_encode($data);

}
/* add driver notification */
/* add driver reminder */
public function adddriverreminder(){

  $column1=$_POST['column1'];
  $column2=$_POST['column2'];
  $date=$_POST['date'];
  $time=$_POST['time'];
  $driver_id=$_POST['driver_id'];
  $module='6';
  $statusexpired='1';
  $remindersdata = $this->drivers_model->getSingleRecord('vbs_reminders',['module' => $module,'status'=> $statusexpired]);

  //convert into date format
    $date=str_replace('/', '-', $date);
    $date=strtotime($date);
    $date = date('Y-m-d',$date);
  //convert into date format
  
  $reminderid = $this->drivers_model->createRecord('vbs_drivers_reminders',[
                           $column1                     => $date,
                           $column2                     => $time,
                          'driver_id'                   => $driver_id,
                          'reminder_id'                 => $remindersdata->id,
                        ]); 
      $record=array('reminder_id'=>$reminderid,'status'=>'Pending');
      $data = ['result'=>"200",'record'=>$record];
      header('Content-Type: application/json'); 
      echo json_encode($data);

}
/* add driver reminder */
  /*resources*/
    public function resourceAdd(){

      if ($_SERVER['REQUEST_METHOD'] === 'POST') {
          $this->resourceStore();
      }else{
          return redirect("admin/drivers");
      }
  }
  public function resourceStore(){
          

              $user = $this->basic_auth->user();
              $user_id=$user->id;

              $id = $this->drivers_model->createRecord('vbs_driver_resource',[
                  'status'           => $_POST['status'],
                  'civilite'         => $_POST['civilite'],
                  'nom'              => trim($_POST['nom']),
                  'prenom'          => trim($_POST['prenom']),
                  'department'       => trim($_POST['department']),
                  'job'              => trim($_POST['job']),
                  'address1'         => trim($_POST['address1']),
                  'address2'         => trim($_POST['address2']),
                  'email'            => trim($_POST['email']),
                  'phone'            => trim($_POST['phone']),
                  'mobile'           => trim($_POST['mobile']),
                  'fax'              => trim($_POST['fax']),
                  'code_postal'      => trim($_POST['code_postal']),
                  'ville'            => $_POST['ville'],
                  'region'           => $_POST['region'],
                  'country'          => $_POST['country'],
                  'user_id'          => $user_id,
                  'profile_id'       => $_POST['profile_id']
              ]);
          if($id){
            $this->data['driver_resource_data']=$this->drivers_model->getAllData('vbs_driver_resource',['profile_id'=>$_POST['profile_id']]);
            $record=$this->load->view('admin/drivers/resource/list', $this->data, TRUE);
            $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully added a driver resource.",'messageType'=>'Success','className'=>'alert-success'];
            header('Content-Type: application/json'); 
            echo json_encode($data);
            }
           else {
              $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
              header('Content-Type: application/json'); 
              echo json_encode($data);
          }
         
  }


    public function resourceEdit(){
      

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $id   = $this->input->post('resource_id');
            if(!empty($id)) {
                $resource = $this->drivers_model->getSingleRecord('vbs_driver_resource',['id'=>$id]);              
                $update=$this->drivers_model->createUpdate('vbs_driver_resource',[
                          'status'           => $_POST['status'],
                          'civilite'         => $_POST['civilite'],
                          'nom'              => trim($_POST['nom']),
                          'prenom'          => trim($_POST['prenom']),
                          'department'       => trim($_POST['department']),
                          'job'              => trim($_POST['job']),
                          'address1'         => trim($_POST['address1']),
                          'address2'         => trim($_POST['address2']),
                          'email'            => trim($_POST['email']),
                          'phone'            => trim($_POST['phone']),
                          'mobile'           => trim($_POST['mobile']),
                          'fax'              => trim($_POST['fax']),
                          'code_postal'      => trim($_POST['code_postal']),
                          'ville'            => $_POST['ville'],
                          'region'           => $_POST['region'],
                          'country'          => $_POST['country']
                      ], $id);
                      if ($update){
                        $this->data['driver_resource_data']=$this->drivers_model->getAllData('vbs_driver_resource',['profile_id'=>$resource->profile_id]);
                          $record=$this->load->view('admin/drivers/resource/list', $this->data, TRUE);
                          $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully updated a driver resource.",'messageType'=>'Success','className'=>'alert-success'];
                          header('Content-Type: application/json'); 
                          echo json_encode($data);
                      }else {
                        $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                        header('Content-Type: application/json'); 
                        echo json_encode($data);
                    }
             
                  }else {
                    $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
                    }
                }else{
                    $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
                    header('Content-Type: application/json'); 
                    echo json_encode($data);
                }
    }
    public function resourceDelete(){
        
        $id=$_POST['delete_resource_id'];
        $resource = $this->drivers_model->getSingleRecord('vbs_driver_resource',['id'=>$id]);  
        $del=$this->drivers_model->createDelete('vbs_driver_resource',['id'=>$id]);
        if ($del==true) {
        $this->data['driver_resource_data']=$this->drivers_model->getAllData('vbs_driver_resource',['profile_id'=>$resource->profile_id]);
          $record=$this->load->view('admin/drivers/resource/list', $this->data, TRUE);
          $data = ['result'=>"200",'record'=>$record,'message'=>" Successfully deleted a driver resource.",'messageType'=>'Success','className'=>'alert-success'];
          header('Content-Type: application/json'); 
          echo json_encode($data);
        }
        else {
         $data = ['result'=>"201",'message'=>" Something went wrong.",'messageType'=>'Error','className'=>'alert-danger'];
         header('Content-Type: application/json'); 
         echo json_encode($data);
        }
    }
    public function get_ajax_resource_data(){
        $id=(int)$_GET['resource_id'];

        if($id>0){

                $data = [];
                $this->data['countries'] = $this->cms_model->get_all_countries();
                $this->data['driver_status_data'] = $this->drivers_model->getAllData('vbs_driverstatus');
                $this->data['driver_civilite_data'] = $this->drivers_model->getAllData('vbs_drivercivilite');
                $this->data['driver_delay_data'] = $this->drivers_model->getAllData('vbs_driverdelay');
                $this->data['driver_payment_data'] = $this->drivers_model->getAllData('vbs_driverpayment');
                $this->data['driver_type_data'] = $this->drivers_model->getAllData('vbs_drivertype');
                $this->data['resource'] = $this->drivers_model->getSingleRecord('vbs_driver_resource',['id'=>$id]);
                $this->data['regions'] = $this->drivers_model->ajax_get_region_listing($this->data['resource']->country);
                $this->data['cities'] = $this->drivers_model->ajax_get_cities_listings($this->data['resource']->region);
                $result=$this->load->view('admin/drivers/resource/edit', $this->data, TRUE);
                echo $result;
                exit();
        }else{
            $result="Please select Record!";
        }
        echo $result;
        exit;
    }
    /*resources*/


    function ajax_get_cities_listing(){
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            
            $result = $this->drivers_model->ajax_get_cities_listings($_POST['region_id']);
            echo json_encode($result);
        }
    }
    function ajax_get_region_listing(){
       if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            
            $result = $this->drivers_model->ajax_get_region_listing($_POST['country_id']);
            echo json_encode($result);
        } 
    }

    function do_upload_image($filename,$name) {
        
        $config['upload_path'] = './uploads/drivers/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['max_size'] = '10240';
        $config['file_name'] = str_replace(' ', '_', $filename['name']);
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        
        if (!$this->upload->do_upload($name)) {
            return NULL;
        } else {
            $avatar = $this->upload->data();
            $avatar = $avatar['file_name'];
        
            return $avatar;
        }
        
    }
  


        function do_user_upload_image($filename,$name) {
        
        $config['upload_path'] = './uploads/user/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['max_size'] = '10240';
        $config['file_name'] = str_replace(' ', '_', $filename['name']);
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        
        if (!$this->upload->do_upload($name)) {
            return NULL;
        } else {
            $avatar = $this->upload->data();
            $avatar = $avatar['file_name'];
        
            return $avatar;
        }
        
    }
}
