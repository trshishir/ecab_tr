<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EmailCronJob extends MY_Controller
{
  function __construct()
  {
        parent::__construct();
        $this->load->model('quotes_model');
        $this->load->model('invoice_model');
        $this->load->model('bookings_model');
        $this->load->model('cron_model');
        $this->load->helper('mypdf');
  }

  public function sendcronemails(){
     $issent=0;
     $currentdatetime=date("Y-m-d H : i");

    //Booking Module Start

     //get booking notification
     $bookingnotifications=$this->cron_model->getnotification('vbs_bookings_notification',[
       "CONCAT(date, ' ', time) <=" => $currentdatetime,
       "issent"                     => $issent   
     ]);
     //get booking notification
     
     if($bookingnotifications){

      foreach ($bookingnotifications as $item) {
         $isemailsent=$this->sendemailbookingnotification($item->id,$item->booking_id);
         if($isemailsent){
           $update = $this->bookings_model->updatebookingrecord('vbs_bookings_notification',[
                'issent'          => 1
                ],['id'=>$item->id]);
         }
         
      }   
     }
  
     //Booking Module End

     //Quote Module Start
     //get quote notification
     $quotenotifications=$this->cron_model->getnotification('vbs_quotes_notification',[
       "CONCAT(date, ' ', time) <=" => $currentdatetime,
       "issent"                     => $issent   
     ]);
     //get quote notification

     if($quotenotifications){

      foreach ($quotenotifications as $item) {
        $booking_id=$this->quotes_model->getsinglerecord("vbs_quotes",['id'=>$item->quote_id])->booking_id;
         $isemailsent=$this->sendemailquotenotification($item->id,$booking_id);

         if($isemailsent){
           $update = $this->quotes_model->updaterecord('vbs_quotes_notification',[
                'issent'          => 1
                ],['id'=>$item->id]);
         }
         
      }   
     }
     //Quote Module End

     //Invoice Module Start
     //get invoice notification
     $invoicenotifications=$this->cron_model->getnotification('vbs_invoices_notification',[
         "CONCAT(date, ' ', time) <=" => $currentdatetime,
         "issent"                     => $issent   
     ]);
     //get invoice notification

     if($invoicenotifications){

        foreach ($invoicenotifications as $item) {
             $booking_id=$this->invoice_model->getsinglerecord("vbs_invoices",['id'=>$item->invoice_id])->booking_id;
             $isemailsent=$this->sendemailinvoicenotification($item->id,$booking_id);

             if($isemailsent){
               $update = $this->invoice_model->updaterecord('vbs_invoices_notification',[
                  'issent'          => 1
                ],['id'=>$item->id]);
             }
             
        }   
     }
     //Invoice Module End


     //QUOTE MODULE SEND REMINDER PDF
      //get quote reminder
     $quotereminders=$this->cron_model->getreminder('vbs_quotes_reminder',[
         "CONCAT(date, ' ', time) <=" => $currentdatetime,
         "issent"                     => $issent   
     ]);
     //get quote reminder

     if($quotereminders){

        foreach ($quotereminders as $item) {
             $booking_id=$this->quotes_model->getsinglerecord("vbs_quotes",['id'=>$item->quote_id])->booking_id;
             $isemailsent=$this->sendemailquotereminder($item->id,$item->type,$booking_id);

             if($isemailsent){
               $update = $this->quotes_model->updaterecord('vbs_quotes_reminder',[
                  'issent'          => 1
                ],['id'=>$item->id]);
             }
             
        }   
     }
     //QUOTE MODULE SEND REMINDER PDF

    //INVOICE MODULE SEND REMINDER PDF
      //get invoice reminder
     $invoicereminders=$this->cron_model->getreminder('vbs_invoices_reminder',[
         "CONCAT(date, ' ', time) <=" => $currentdatetime,
         "issent"                     => $issent   
     ]);
     //get invoice reminder

     if($invoicereminders){

        foreach ($invoicereminders as $item) {
             $booking_id=$this->invoice_model->getsinglerecord("vbs_invoices",['id'=>$item->invoice_id])->booking_id;
             $isemailsent=$this->sendemailinvoicereminder($item->id,$item->type,$booking_id);

             if($isemailsent){
               $update = $this->invoice_model->updaterecord('vbs_invoices_reminder',[
                  'issent'          => 1
                ],['id'=>$item->id]);
             }
             
        }   
     }
     //INVOICE MODULE SEND REMINDER PDF

  }

  
  //create notification pdf and sent to mail
public function sendemailbookingnotification($bookingnotificationid,$booking_id){

$pdfNotificationArrays=createbookingnotificationpdf($bookingnotificationid,$booking_id); 
$pdffile=$pdfNotificationArrays['pdf']->Output(strtolower($pdfNotificationArrays['fileName']).".pdf", 'S');
$filename=$pdfNotificationArrays['fileName'];

$dbbookingdata=$this->bookings_model->getdbbookingrecord('vbs_bookings',array('id'=>$booking_id));
$client=$this->bookings_model->getclientdatabyid($dbbookingdata->user_id);
 
  //Email Sent
$content="Hi ".$client->civility." ".$client->first_name." ".$client->last_name.",<br>";
$content.="You 've received notification letter from ECAB LLC. Please check these files.";
                     $this->load->library('mail_bookingsender');
                    $mailData['email']   = $client->email;
                    $mailData['subject'] = "You've received messages from ECAB LLC";
                    $mailData['message']    = $content;
                    $mailData['firstpdffile']    = $pdffile;
                    $mailData['firstpdffilename']    = strtolower($filename).".pdf";

                    $sent = $this->mail_bookingsender->sendMail($mailData);
                    return $sent;
                  
}
//create notification pdf and sent to mail




//Quote Module Create Notification Pdf Start
public function sendemailquotenotification($quotenotificationid,$booking_id){
$pdfNotificationArrays=createnotificationpdf($quotenotificationid,$booking_id); 
$pdffile=$pdfNotificationArrays['pdf']->Output(strtolower($pdfNotificationArrays['fileName']).".pdf", 'S');
$filename=$pdfNotificationArrays['fileName'];


$pdfArrays=createstringquotepdf($booking_id);   
$quotepdffile=$pdfArrays['pdf']->Output(strtolower($pdfArrays['fileName']).".pdf", 'S');
$quotepdffilename=$pdfArrays['fileName'];

$dbbookingdata=$this->bookings_model->getdbbookingrecord('vbs_bookings',array('id'=>$booking_id));
$client=$this->bookings_model->getclientdatabyid($dbbookingdata->user_id);


  //create pdf 
  //Email Sent
$content="Hi ".$client->civility." ".$client->first_name." ".$client->last_name.",<br>";
$content.="You 've received notification letter from ECAB LLC. Please check these files.";
                     $this->load->library('mail_bookingsender');
                    $mailData['email']   = $client->email;
                    $mailData['subject'] = "You've received messages from ECAB LLC";
                    $mailData['message']    = $content;
                    $mailData['firstpdffile']    = $pdffile;
                    $mailData['secondpdffile']    = $quotepdffile;
                    $mailData['firstpdffilename']    = strtolower($filename).".pdf";
                    $mailData['secondpdffilename']    = strtolower($quotepdffilename).".pdf";
                


                    $sent = $this->mail_bookingsender->sendMail($mailData);
                    return $sent;
               
}

//Quote Moduel Create Notification Pdf End


//INVOICE MODULE CREATE NOTIFICAITON PDF START
public function sendemailinvoicenotification($invoicenotificationid,$booking_id){
 
  /* CREATE NOTIFICATION PDF */
$pdfNotificationArrays=createinvoicenotificationpdf($invoicenotificationid,$booking_id); 
$pdffile=$pdfNotificationArrays['pdf']->Output(strtolower($pdfNotificationArrays['fileName']).".pdf", 'S');
$filename=$pdfNotificationArrays['fileName'];
 /* CREATE NOTIFICATION PDF */

/*CREATE INVOICE PDF */
$pdfArrays=createinvoicepdf($booking_id);   
$invoicepdffile=$pdfArrays['pdf']->Output(strtolower($pdfArrays['fileName']).".pdf", 'S');
$invoicepdffilename=$pdfArrays['fileName'];
/*CREATE INVOICE PDF */

$dbbookingdata=$this->bookings_model->getdbbookingrecord('vbs_bookings',array('id'=>$booking_id));
$client=$this->bookings_model->getclientdatabyid($dbbookingdata->user_id);
  //Email Sent
$content="Hi ".$client->civility." ".$client->first_name." ".$client->last_name.",<br>";
$content.="You 've received notification letter from ECAB LLC. Please check these files.";
                     $this->load->library('mail_bookingsender');
                    $mailData['email']   = $client->email;
                    $mailData['subject'] = "You've received messages from ECAB LLC";
                    $mailData['message']    = $content;
                    $mailData['firstpdffile']    = $pdffile;
                    $mailData['secondpdffile']    = $invoicepdffile;
                    $mailData['firstpdffilename']    = strtolower($filename).".pdf";
                    $mailData['secondpdffilename']    = strtolower($invoicepdffilename).".pdf";
                


                    $sent = $this->mail_bookingsender->sendMail($mailData);
                    return $sent;
                    /*if($sent === true) {
                       echo "message sent";
                    }else{
                       echo "message not sent";
                    }*/
       
  //Email sent   
 
        //exit();
}

//INVOICE MODULE CREATE NOTIFICATION PDF END


//QUOTE MODULE CREATE REMINDER PDF START
public function sendemailquotereminder($quotereminderid,$level,$booking_id){
 
  $pdfReminderArrays=createreminderpdf($quotereminderid,$level,$booking_id); 
  $reminderpdffile=$pdfReminderArrays['pdf']->Output(strtolower($pdfReminderArrays['fileName']).".pdf", 'S');
  $filename=$pdfReminderArrays['fileName'];


  $pdfArrays=createstringquotepdf($booking_id);   
  $quotepdffile=$pdfArrays['pdf']->Output(strtolower($pdfArrays['fileName']).".pdf", 'S');
  $quotepdffilename=$pdfArrays['fileName'];

  $dbbookingdata=$this->bookings_model->getdbbookingrecord('vbs_bookings',array('id'=>$booking_id));
  $client=$this->bookings_model->getclientdatabyid($dbbookingdata->user_id);
  //Email Sent

$content="Hi ".$client->civility." ".$client->first_name." ".$client->last_name.",<br>";
$content.="You 've received reminder letter from ECAB LLC. Please check these files.";
                    $this->load->library('mail_bookingsender');
                    $mailData['email']   =  $client->email;
                    $mailData['subject'] = "You've received messages from ECAB LLC";
                    $mailData['message']    = $content;
                    $mailData['firstpdffile']    = $reminderpdffile;
                    $mailData['secondpdffile']    = $quotepdffile;
                    $mailData['firstpdffilename']    = strtolower($filename).".pdf";
                    $mailData['secondpdffilename']    = strtolower($quotepdffilename).".pdf";
                


                    $sent = $this->mail_bookingsender->sendMail($mailData);
                    return $sent;

    //create pdf
}
//QUOTE MODULE CREATE REMINDER PDF END

//INVOICE MODULE CREATE REMINDER PDF START
public function sendemailinvoicereminder($invoicereminderid,$level,$booking_id){
  $pdfReminderArrays=createinvoicereminderpdf($invoicereminderid,$level,$booking_id); 
  $reminderpdffile=$pdfReminderArrays['pdf']->Output(strtolower($pdfReminderArrays['fileName']).".pdf", 'S');
  $filename=$pdfReminderArrays['fileName'];

   /*CREATE INVOICE PDF */
  $pdfArrays=createinvoicepdf($booking_id);   
  $invoicepdffile=$pdfArrays['pdf']->Output(strtolower($pdfArrays['fileName']).".pdf", 'S');
  $invoicepdffilename=$pdfArrays['fileName'];
  /*CREATE INVOICE PDF */

  $dbbookingdata=$this->bookings_model->getdbbookingrecord('vbs_bookings',array('id'=>$booking_id));
  $client=$this->bookings_model->getclientdatabyid($dbbookingdata->user_id);
  //Email Sent

$content="Hi ".$client->civility." ".$client->first_name." ".$client->last_name.",<br>";
$content.="You 've received reminder letter from ECAB LLC. Please check these files.";
                    $this->load->library('mail_bookingsender');
                    $mailData['email']   =  $client->email;
                    $mailData['subject'] = "You've received messages from ECAB LLC";
                    $mailData['message']    = $content;
                    $mailData['firstpdffile']    = $reminderpdffile;
                    $mailData['secondpdffile']    = $invoicepdffile;
                    $mailData['firstpdffilename']    = strtolower($filename).".pdf";
                    $mailData['secondpdffilename']    = strtolower($invoicepdffilename).".pdf";
                


                    $sent = $this->mail_bookingsender->sendMail($mailData);
                    return $sent;

    //create pdf
}
//INVOICE MODULE CREATE REMINDER PDF END
}