<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class documents extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('general');
        $this->load->helper('language');
        if (!$this->basic_auth->is_login())
            redirect("admin", 'refresh');
        else
            $this->data['user'] = $this->basic_auth->user();
        $this->load->model('invoice_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');
        $this->load->model('support_model');
        $this->load->model('calls_model');
        $this->load->model('notes_model');
        $this->load->library('session');
        $this->load->model('user_model');
        $this->load->model('notifications_model');
        $this->load->model('cars_model');
        $this->load->model('documents_model');

        $this->data['configuration'] = get_configuration();
    }

    public function index() {
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "documents";
        $this->data['gmaps'] = false;
        $this->data['title'] = 'Documents';
        $this->data['show_subtitle'] = false;
        $this->data['subtitle'] = 'Add Document';
        $this->data['title_link'] = base_url('admin/documents');
        $this->data['content'] = 'admin/documents/indexv2';

        $this->load->model('calls_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');

        $data = [];
        $data['request'] = $this->request_model->getAll();
        $data['jobs'] = $this->jobs_model->getAll();
        $data['calls'] = $this->calls_model->getAll();

        $this->data['dcsa'] = array('name' => 'dcsa', 'class' => 'user form-control', 'tabindex' => '2', 'placeholder' => 'Driver/Client/Supplier/Administration', 'id' => 'dcsa', 'type' => 'text', 'maxlength' => '150', 'value' => $this->form_validation->set_value('dcsa'));
        $this->data['doc_title'] = array('name' => 'doc_title', 'class' => 'user form-control', 'tabindex' => '4', 'placeholder' => 'Document Name', 'id' => 'doc_title', 'type' => 'text', 'value' => $this->form_validation->set_value('doc_title'));

        $this->data['statuses'] = $this->sitemodel->getConfig('document_status');
        $this->data['categories'] = $this->sitemodel->getConfig('document_category');
        $this->data['cities'] = $this->sitemodel->getConfig('document_cities');
        $this->data['signatures'] = $this->sitemodel->getConfig('document_signature');
        $this->data['docs'] = $this->sitemodel->getConfig('document_docs');
        $this->data['departments'] = $this->sitemodel->getConfig('document_departments');
        $this->data['managers'] = $this->sitemodel->getConfig('document_managers');
        $this->data['destinations'] = $this->sitemodel->getConfig('document_destination');
        $this->data['groups'] = $this->sitemodel->getConfig('groups');
        $this->data['all_records'] = $this->documents_model->getAllDocuments();

        // $this->data['data'] = $this->request_model->getAll();
        // $this->data['data'] = $this->user_model->getAllClients();
        $this->_render_page('templates/admin_template_v2', $this->data);
    }

    public function view($id) {
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "documents";
        $this->data['gmaps'] = false;
        $this->data['title'] = 'View Document';
        $this->data['show_subtitle'] = false;
        $this->data['subtitle'] = 'View Document';
        $this->data['title_link'] = base_url('admin/documents');
        $this->data['content'] = 'admin/documents/view';

        $this->load->model('calls_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');
        $this->load->model('documents_model');

        $data = [];
        $data['request'] = $this->request_model->getAll();
        $data['jobs'] = $this->jobs_model->getAll();
        $data['calls'] = $this->calls_model->getAll();
        $this->data['document_data'] = $this->documents_model->getSpecificDocument($id);
        // print_r($data['document_data']);

        // $this->data['data'] = $this->request_model->getAll();
        // $this->data['data'] = $this->user_model->getAllClients();
        $this->_render_page('templates/admin_template_v2', $this->data);
    }

    public function fetchDocumentsData() {
        $result = array('data' => array());

        $data = $this->documents_model->getAllDocuments();
        foreach ($data as $key => $value) {

            $added_by = $this->sitemodel->getSpecificUser($value['added_by']);
            foreach($added_by as $added) {
                $final_added_by = $added['civility'] . ' ' . $added['first_name'] . ' ' . $added['last_name'];
            }

            $status = ($value['active'] == 1) ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Inactive</span>';
            $checkbox = "<input type='checkbox' name='documents[]' class='ccheckbox' value='" . $value['did'] . "' />";
            $registered_date = date('d/m/Y', strtotime($value['created_at']));
            $registered_time = date('H:i:s', strtotime($value['created_at']));
            $since = timeDiff($value['created_at']);
            //$edit_link = "<a href='" . base_url() . "admin/documents/" . $value['did'] . "/edit.php" . "'>" . create_timestamp_uid($value['created_at'], $value['did']) . "</a>";
            $edit_link = "<a href='#'>" . create_timestamp_uid($value['created_at'], $value['did']) . "</a>";
            // echo $edit_link."<br /> dd".$key;
            $view_link = "<a href='" . base_url() . "admin/documents/" . $value['did'] . "/view.php" . "' class='btn btn-default'>View</a>";
            $result['data'][$key] = array(
                $checkbox,
                $edit_link,
                $registered_date,
                $registered_time,
                $final_added_by,
                $value['description'],
                $value['civility'] . ' ' . $value['first_name'] . ' ' . $value['last_name'],
                $value['doc_category'],
                $value['doc_name'],
                $value['doc_date'],
                $view_link,
                $status,
                $since
            );
        } // /foreach

        echo json_encode($result);
    }

    public function clientSearchData() {
        //if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['search'])) {
        $result = array('data' => array());
        //echo "<PRE>";print_r($_POST);echo "</PRE>";
        $searchFilter['search_name'] = $_POST['search_name'];
        $searchFilter['search_email'] = $_POST['search_email'];
        $searchFilter['search_phone'] = $_POST['search_phone'];
        $searchFilter['date_from'] = $_POST['date_from'];
        $searchFilter['date_to'] = $_POST['date_to'];
        $searchFilter['status'] = $_POST['status'];
        $data = $this->user_model->searchClient($searchFilter);
        if (!empty($data)) {
            foreach ($data as $key => $value) {

                $status = ($value['active'] == 1) ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Inactive</span>';
                $checkbox = "<input type='checkbox' name='clients[]' class='ccheckbox' value='" . $value['id'] . "' />";
                $registered_date = date('d/m/Y', strtotime($value['created_at']));
                $registered_time = date('H:i:s', strtotime($value['created_at']));
                $since = timeDiff($value['created_at']);
                $edit_link = "<a href='" . base_url() . "admin/clients/" . $value['id'] . "/edit.php" . "'>" . create_timestamp_uid($value['created_at'], $value['id']) . "</a>";
                $result['data'][$key] = array(
                    $checkbox,
                    $edit_link,
                    $registered_date,
                    $registered_time,
                    $value['civility'] . ' ' . $value['first_name'] . ' ' . $value['last_name'],
                    $value['email'],
                    $value['phone'],
                    $status,
                    $since
                );
            } // /foreach
        }
        echo json_encode($result);
        //}
    }
    
    public function clientExcelDownload() {
        //if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['search'])) {
        $result = array('data' => array());
        //echo "<PRE>";print_r($_POST);echo "</PRE>";
        $searchFilter['search_name'] = $_POST['search_name'];
        $searchFilter['search_email'] = $_POST['search_email'];
        $searchFilter['search_phone'] = $_POST['search_phone'];
        $searchFilter['date_from'] = $_POST['date_from'];
        $searchFilter['date_to'] = $_POST['date_to'];
        $searchFilter['status'] = $_POST['status'];
        $data = $this->user_model->searchClient($searchFilter);
        
        $filename = 'clients_'.date('Ymd').'.xls'; 

        // file creation 
        $file = fopen('php://output','w');
        $header = array("#","Id","Date","Time","Name","Email","Phone","Status","Since"); 
        fputcsv($file, $header);

        if (!empty($data)) {
            foreach ($data as $key => $value) {

                $status = ($value['active'] == 1) ? 'Active' : 'Inactive';
                $checkbox = "";
                $registered_date = date('d/m/Y', strtotime($value['created_at']));
                $registered_time = date('H:i:s', strtotime($value['created_at']));
                $since = timeDiff($value['created_at']);
                $edit_link = create_timestamp_uid($value['created_at'], $value['id']);
                $name = $value['civility'] . ' ' . $value['first_name'] . ' ' . $value['last_name'];
                /*$result['data'][$key] = array(
                    $checkbox,
                    $edit_link,
                    $registered_date,
                    $registered_time,
                    $value['civility'] . ' ' . $value['first_name'] . ' ' . $value['last_name'],
                    $value['email'],
                    $value['phone'],
                    $status,
                    $since
                );
                */
                $line = array($checkbox , $edit_link , $registered_date , $registered_time , $name , $value['email'] , $value['phone'] , $status , $since);
                fputcsv($file,$line); 
                
            } // /foreach
            
        } 
        header("Content-Description: File Transfer"); 
        header('Content-Type: application/octet-stream');
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Type: application/force-download");
        header("Content-Transfer-Encoding: binary");
        // header("Content-Type: application/xls; ");
        header("Content-Disposition: attachment; filename=$filename");
        header('Cache-Control: max-age=0');
        //output all remaining data on a file pointer
        fpassthru($file);
        fclose($file); 
        die();
        //echo json_encode($result);
    }

    public function add() {

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // validate form input
            // $this->form_validation->set_rules('dcsa', $this->lang->line('create_user_validation_fname_label'), 'required|xss_clean');
            // $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required|xss_clean');
            // $this->form_validation->set_rules('zipcode', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean|min_length[5]');
            // $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']'); // |matches[confirm_password] 
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            if ($this->input->post('dcsaadd')) {
                // $user_group = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('groups') . " WHERE name = 'clients'");

                // $group_id = array($user_group[0]->id);
                $documents_data = array(
                    'added_by' => $this->session->userdata['user_id'],
                    'usergroup' => $this->input->post('dcsaadd'),
                    'username' => $this->input->post('dcsa'),
                    'doc_category' => $this->input->post('doc_category'),
                    'doc_date' => $this->input->post('doc_date'),
                    'doc_title' => $this->input->post('doc_title'),
                    'doc_status' => $this->input->post('status')
                );
                // print_r($documents_data); echo "our final recorder here wahid "; exit;

                if($this->documents_model->savedocument($documents_data)) {

                    $this->session->set_flashdata('messages', $this->ion_auth->messages());
                    redirect('admin/documents', 'refresh');
                } else {
                    $this->session->set_flashdata('error', $this->ion_auth->messages());
                    $this->_render_page('templates/admin_template_v2', $this->data);
                }

                // if ($this->form_validation->run() == true && $this->ion_auth->register($user_role, $username, $password, $email, $additional_data, $group_id)) {
                //     $user_rec = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('users') . " WHERE email = '" . $email . "'");
                //     $inputdata['user_id'] = $user_rec[0]->id;
                //     $inputdata['address1'] = $this->input->post('address');
                //     $inputdata['address2'] = $this->input->post('city');
                //     $inputdata['fax_no'] = $this->input->post('fax');
                //     $inputdata['company_name'] = $this->input->post('company');
                //     $table_name = "users_details";

                //     $status = $this->base_model->insert_operation($inputdata, $table_name);

                //     // $user = $this->basic_auth->login($username, $password);

                //     if ($status) {

                //         // $this->session->set_flashdata('message', $this->ion_auth->messages());
                //         // check to see if we are creating the user
                //         // redirect them back to the admin page
                //         $this->session->set_flashdata('messages', $this->ion_auth->messages());
                //         redirect('admin/clients', 'refresh');
                //     } else {
                //         $this->session->set_flashdata('error', $this->ion_auth->messages());
                //         $this->_render_page('templates/admin_template_v2', $this->data);
                //     }
                // } else {
                //     $this->session->set_flashdata('error', $this->ion_auth->messages());
                //     $this->_render_page('templates/admin_template_v2', $this->data);
                // }
            }
        }
    }
    public function edit() {

        $chk = false;
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            if ($this->input->post('dcsa1')) {
                // $user_group = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('groups') . " WHERE name = 'clients'");

                $dcsa1 = $this->input->post('dcsa1');
                $i = 0;
                foreach ($dcsa1 as $d) {
                // $group_id = array($user_group[0]->id);
                    $did = $_POST['dids'][$i];
                    if($did != '' && $_POST['dcsa'][$i] != '' && $_POST['doc_category'][$i] != '' && $_POST['doc_date'][$i] != '' && $_POST['city'][$i] != '' && $_POST['doc_title'][$i] != '' && $_POST['status'][$i] != '') {
                        $documents_data = array(
                            'added_by' => $this->session->userdata['user_id'],
                            'usergroup' => $d,
                            'username' => $_POST['dcsa'][$i],
                            'doc_category' => $_POST['doc_category'][$i],
                            'doc_date' => $_POST['doc_date'][$i],
                            'doc_title' => $_POST['doc_title'][$i],
                            'doc_status' => $_POST['status'][$i]
                        );

                        $i++;
                        $chk = $this->documents_model->updatedocument($documents_data,$did);
                        // print_r($documents_data); echo "our final recorder here wahid ";
                    }
                }
                // exit;
            }

            if($chk) {

                $this->session->set_flashdata('messages', $this->ion_auth->messages());
                redirect('admin/documents', 'refresh');
            } else {
                $this->session->set_flashdata('error', $this->ion_auth->messages());
                $this->_render_page('templates/admin_template_v2', $this->data);
            }
        }
    }

    public function getUsers(){
        $id = $this->input->get("user_cat");
        echo "wahid value is: ".$id;
        $users = $this->documents_model->getSpecificUsers($id);
        $response = '<option value="">Select Username</option>';
        if($users) {
            foreach($users as $user) {
                $response.='<option value="'.$user->user_id.'">'.$user->first_name.' '.$user->last_name.'</option>';
            }
        } else {
            $response = "";
            $response = '<option value="">No Username Found</option>';
        }
        // print_r($users);
        echo $response;

    }

    // public function edit($id) {
    //     // $id = $this->uri->segment(4);
    //     $this->data['alert'] = "";
    //     $this->data['css_type'] = array("form", "datatable");
    //     $this->data['active_class'] = "Client";
    //     $this->data['gmaps'] = false;
    //     $this->data['title'] = 'Client Edit';
    //     $this->load->library('form_validation');
    //     $userDetails = $this->user_model->getUserDetail($id);

    //     $this->data['civility'] = array('name' => 'civility', 'class' => 'user form-control', 'placeholder' => 'Civility', 'id' => 'civility', 'type' => 'text', 'value' => $this->form_validation->set_value('civility'));
    //     $this->data['first_name'] = array('name' => 'first_name', 'class' => 'user form-control', 'placeholder' => 'First name', 'id' => 'first_name', 'type' => 'text', 'value' => $userDetails->first_name);
    //     $this->data['last_name'] = array('name' => 'last_name', 'class' => 'user form-control', 'placeholder' => 'Last name', 'id' => 'last_name', 'type' => 'text', 'value' => $userDetails->last_name);
    //     $this->data['email'] = array('name' => 'email', 'class' => 'user-name form-control', 'placeholder' => 'User Email', 'id' => 'email', 'type' => 'text', 'value' => $userDetails->email);
    //     $this->data['phone'] = array('name' => 'phone', 'class' => 'phone1 form-control', 'placeholder' => 'phone', 'id' => 'phone', 'type' => 'text', 'maxlength' => '11', 'value' => $userDetails->phone);
    //     $this->data['mobile'] = array('name' => 'mobile', 'class' => 'phone1 form-control', 'placeholder' => 'Mobile', 'id' => 'mobile', 'type' => 'text', 'maxlength' => '15', 'value' => $userDetails->mobile_no);
    //     $this->data['fax'] = array('name' => 'fax', 'class' => 'phone1 form-control', 'placeholder' => 'Fax', 'id' => 'fax', 'type' => 'text', 'maxlength' => '15', 'value' => $userDetails->fax_no);
    //     $this->data['password'] = array('name' => 'password', 'class' => 'password form-control', 'placeholder' => 'Password', 'id' => 'password', 'type' => 'password', 'value' => $this->form_validation->set_value('password'));
    //     $this->data['confirm_password'] = array('name' => 'confirm_password', 'class' => 'password form-control', 'placeholder' => 'Confirm Password', 'id' => 'confirm_password', 'type' => 'password', 'value' => $this->form_validation->set_value('confirm_password'));
    //     $this->data['company'] = array('name' => 'company', 'class' => 'user form-control', 'placeholder' => 'Company', 'id' => 'company', 'type' => 'text', 'maxlength' => '150', 'value' => $userDetails->_company_name);
    //     $this->data['address'] = array('name' => 'address', 'class' => 'user form-control', 'placeholder' => 'adresse', 'rows' => '1', 'cols' => '40', 'id' => 'address', 'type' => 'text', 'value' => $userDetails->address1);
    //     $this->data['address1'] = array('name' => 'address1', 'class' => 'user form-control', 'placeholder' => '', 'rows' => '1', 'cols' => '40', 'id' => 'address1', 'type' => 'text', 'value' => $userDetails->address2);
    //     $this->data['city'] = array('name' => 'city', 'class' => 'user form-control', 'placeholder' => 'City', 'id' => 'city', 'type' => 'text', 'value' => $userDetails->city);
    //     $this->data['zipcode'] = array('name' => 'zipcode', 'class' => 'user form-control', 'placeholder' => 'Zipcode', 'id' => 'zipcode', 'type' => 'text', 'value' => $userDetails->zipcode);

    //     if (isset($_POST['submit'])) {
    //         $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required|xss_clean');
    //         $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required|xss_clean');
    //         $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
    //         $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone1_label'), 'xss_clean|integer|min_length[8]|max_length[10]');
    //         $this->form_validation->set_rules('mobile', $this->lang->line('create_user_validation_phone1_label'), 'xss_clean|integer|min_length[10]|max_length[10]');
    //         $this->form_validation->set_rules('address', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
    //         $this->form_validation->set_rules('address1', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
    //         $this->form_validation->set_rules('city', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean');
    //         $this->form_validation->set_rules('zipcode', $this->lang->line('create_user_validation_address_label'), 'required|xss_clean|min_length[5]');
    //         $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

    //         if ($this->form_validation->run() == true) {
    //             $username = $this->input->post('first_name') . '' . $this->input->post('last_name');
    //             $userData = array(
    //                 'username' => strtolower($username),
    //                 'civility' => $this->input->post('civility'),
    //                 'first_name' => $this->input->post('first_name'),
    //                 'last_name' => $this->input->post('last_name'),
    //                 'company_name' => $this->input->post('company'),
    //                 'email' => $this->input->post('email'),
    //                 'phone' => $this->input->post('phone'),
    //                 'city' => $this->input->post('city'),
    //                 'zipcode' => $this->input->post('zipcode')
    //             );
    //             $additionalData = array('address1' => $this->input->post('address'),
    //                 'address2' => $this->input->post('address1'),
    //                 'company_name' => $this->input->post('company'),
    //                 'fax_no' => $this->input->post('fax')
    //             );
    //             if (!empty($this->input->post('mobile'))) {
    //                 $additionalData['mobile_no'] = $this->input->post('mobile');
    //             }
    //             //  $status = $this->base_model->update_operation($userData, 'users');
    //             $status = $this->user_model->updateUser($userData, $additionalData, $id);
    //             if ($status) {
    //                 $this->session->set_flashdata('alert', [
				// 		'message' => "Successfully Updated.",
				// 		'class' => "alert-success",
				// 		'type' => "Success"
				// ]);
    //             }
    //             else {
    //                 $this->session->set_flashdata('alert', [
				// 		'message' => @$error[0],
				// 		'class' => "alert-danger",
				// 		'type' => "Error"
				// ]);
    //             }
    //         }
    //     }
        
    //     $userDetails = $this->user_model->getUserDetail($id);
    //     // echo "<PRE>";print_r($userDetails);echo "</PRE>";
    //     $this->data['data'] = $userDetails;
    //     $this->data['content'] = 'admin/clients/edit';
    //     $this->_render_page('templates/admin_template_v2', $this->data);
    // }


    public function edit_library_template() {
        // echo "we are here eidditt"; exit;
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $this->load->library('form_validation');

            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            if ($this->input->post('custom_text')) {
                // $user_group = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('groups') . " WHERE name = 'clients'");

                // $group_id = array($user_group[0]->id);
                $custom = $this->input->post('custom_text');
                $i = 0;
                foreach ($custom as $cus) {

                    $documents_data = array(
                        'added_by' => $this->session->userdata['user_id'],
                        'lib_status' => $_POST['status'][$i],
                        'lib_category' => $_POST['category'][$i],
                        'lib_destination' => $_POST['destination'][$i],
                        'lib_docname' => $_POST['title'][$i],
                        'lib_customtext' => $cus
                    );
                    print_r($documents_data); echo "our final recorder here wahid "; //exit;
                    // $chk = $this->documents_model->updatelibrarytemplate($documents_data);
                    $i++;
                }
                exit;
                if($chk) {

                    $this->session->set_flashdata('messages', $this->ion_auth->messages());
                    redirect('admin/library', 'refresh');
                } else {
                    $this->session->set_flashdata('error', $this->ion_auth->messages());
                    $this->_render_page('templates/admin_template_v2', $this->data);
                }
            }
        }
    }

    public function configurations() {
        $this->data['css_type'] = array("form", "datatable");
        $this->data['active_class'] = "Config Clients";
        $this->data['gmaps'] = false;
        $this->data['title'] = $this->lang->line("configurations");
        $this->data['subtitle'] = 'Config Clients';
        $this->data['title_link'] = '#';
        $this->data['content'] = 'admin/clients/configurations';
        $data = [];
        $this->data['data'] = $this->notifications_model->getAll();
        $this->_render_page('templates/admin_template', $this->data);
    }

    public function addclienttypeproccess() {
        extract($_GET);
        //print_r($_GET);
        //die();
        $values = [
            'type' => $type,
            'is_delete' => 1,
            'create_date' => date("Y-m-d H:i:s")
        ];


        // print_r($values);

        $insert = $this->cars_model->insertData('vbs_clientType', $values, 'type', $type);

        if ($insert == 1) {
            echo "success";
        }
    }

    public function addclientpaymentproccess() {
        extract($_GET);
        //print_r($_GET);
        //die();
        $values = [
            'payment' => $payment,
            'is_delete' => 1,
            'create_date' => date("Y-m-d H:i:s")
        ];


        // print_r($values);

        $insert = $this->cars_model->insertData('vbs_clientPayment', $values, 'payment', $payment);

        if ($insert == 1) {
            echo "success";
        }
    }

    public function addclientdelayproccess() {
        extract($_GET);
        //print_r($_GET);
        //die();
        $values = [
            'delay' => $delay,
            'is_delete' => 1,
            'create_date' => date("Y-m-d H:i:s")
        ];


        // print_r($values);

        $insert = $this->cars_model->insertData('vbs_clientDelay', $values, 'delay', $delay);

        if ($insert == 1) {
            echo "success";
        }
    }

    public function getclienttypeproccess() {
        extract($_GET);
        $statut = $this->cars_model->getStaut('vbs_clientType', 'is_delete', '1', $name, $from, $to, 'type');
        $tbody = '';
        if (!empty($statut)) {
            $i = 1;
            foreach ($statut as $stat) {
                $tbody .= '
                <tr>
                <td align="center">
	             <input type="checkbox" name="check" class="TypeClass"  id="TypeCheck" value="' . $stat->id . '" >
	             <input type="hidden" id="update_type" value="' . $stat->type . '">
	             </td>
                 <td align="center">' . $i . '</td>
                 <td align="center">' . $stat->type . '</td>
                 <td align="center">' . $stat->create_date . '</td>
                </tr>
                ';
                $i++;
            }
        } else {
            $tbody .= '<tr align="center"><td colspan="4"><span class="text-danger text-center">No Record Found</span></td></tr>';
        }

        echo $tbody;
    }

    public function getclientpaymentproccess() {
        extract($_GET);
        $statut = $this->cars_model->getStaut('vbs_clientPayment', 'is_delete', '1', $name, $from, $to, 'payment');
        $tbody = '';
        if (!empty($statut)) {
            $i = 1;
            foreach ($statut as $stat) {
                $tbody .= '
                <tr>
                <td align="center">
                 <input type="checkbox" name="check" class="PaymentClass"  id="PaymentCheck" value="' . $stat->id . '" >
                 <input type="hidden" id="update_payment" value="' . $stat->payment . '">
                 </td>
                 <td align="center">' . $i . '</td>
                 <td align="center">' . $stat->payment . '</td>
                 <td align="center">' . $stat->create_date . '</td>
                </tr>
                ';
                $i++;
            }
        } else {
            $tbody .= '<tr align="center"><td colspan="4"><span class="text-danger text-center">No Record Found</span></td></tr>';
        }

        echo $tbody;
    }

    public function getclientdelayproccess() {
        extract($_GET);
        $statut = $this->cars_model->getStaut('vbs_clientDelay', 'is_delete', '1', $name, $from, $to, 'delay');
        $tbody = '';
        if (!empty($statut)) {
            $i = 1;
            foreach ($statut as $stat) {
                $tbody .= '
                <tr>
                <td align="center">
                 <input type="checkbox" name="check" class="DelayClass"  id="DelayCheck" value="' . $stat->id . '" >
                 <input type="hidden" id="update_delay" value="' . $stat->delay . '">
                 </td>
                 <td align="center">' . $i . '</td>
                 <td align="center">' . $stat->delay . '</td>
                 <td align="center">' . $stat->create_date . '</td>
                </tr>
                ';
                $i++;
            }
        } else {
            $tbody .= '<tr align="center"><td colspan="4"><span class="text-danger text-center">No Record Found</span></td></tr>';
        }

        echo $tbody;
    }

    public function updateclienttypeproccess() {
        extract($_GET);

        /* print_r($_GET);
          die(); */
        $values = [
            'type' => $type
        ];
        $update = $this->cars_model->updateStatut($id, $type, $values, 'vbs_clientType', 'type');

        if ($update == 1) {
            echo "success";
        } else {
            echo "failed";
        }
    }

    public function updateclientpaymentproccess() {
        extract($_GET);

        /* print_r($_GET);
          die(); */
        $values = [
            'payment' => $payment
        ];
        $update = $this->cars_model->updateStatut($id, $payment, $values, 'vbs_clientPayment', 'payment');

        if ($update == 1) {
            echo "success";
        } else {
            echo "failed";
        }
    }

    public function updateclientdelayproccess() {
        extract($_GET);

        /* print_r($_GET);
          die(); */
        $values = [
            'delay' => $delay
        ];
        $update = $this->cars_model->updateStatut($id, $delay, $values, 'vbs_clientDelay', 'delay');

        if ($update == 1) {
            echo "success";
        } else {
            echo "failed";
        }
    }

    public function deleteclienttypeproccess() {
        extract($_GET);

        //$delete =  $this->cars_model->deleteDelete('vbs_carStatut',$id);
        $values = [
            'is_delete' => '0'
        ];
        $update = $this->cars_model->deactive($values, $id, 'vbs_clientType');
        if ($update == 1) {
            echo "success";
        }
    }

    public function deleteclientpaymentproccess() {
        extract($_GET);

        //$delete =  $this->cars_model->deleteDelete('vbs_carStatut',$id);
        $values = [
            'is_delete' => '0'
        ];
        $update = $this->cars_model->deactive($values, $id, 'vbs_clientPayment');
        if ($update == 1) {
            echo "success";
        }
    }

    public function deleteclientdelayproccess() {
        extract($_GET);

        //$delete =  $this->cars_model->deleteDelete('vbs_carStatut',$id);
        $values = [
            'is_delete' => '0'
        ];
        $update = $this->cars_model->deactive($values, $id, 'vbs_clientDelay');
        if ($update == 1) {
            echo "success";
        }
    }

}
