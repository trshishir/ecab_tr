<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends MY_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->lang->load('auth');
		$this->lang->load('general');
		$this->load->helper('language');
		if (!$this->basic_auth->is_login())
		{
			redirect("admin", 'refresh');
		}
		else
		{
			$this->data['user'] = $this->basic_auth->user();
		}
		$this->load->model('userx_model');
		$this->load->model('request_model');
		$this->load->model('jobs_model');
		$this->load->model('support_model');
		$this->load->model('calls_model');
		$this->load->model('cms_model');

		$this->data['configuration'] = get_configuration();
	}

	public function index(){
		$this->data['css_type'] 	= array("form","datatable");
		$this->data['active_class'] = "users";
		$this->data['gmaps'] 	= false;
		$this->data['title'] 	= 'Users'; // $this->lang->line("calls");
		$this->data['title_link'] 	= base_url('admin/users');
		if(null != $this->session->userdata('search_text')){
			$this->data['users_data'] = $this->userx_model->getAll(array('first_name'=>$this->session->userdata('search_text'),'grp.name'=>'admin'));
			$this->data['search_text'] = $this->session->userdata('search_text');
		}else{
			$this->data['users_data'] = $this->userx_model->getAll(['grp.name'=>'admin']);
			$this->data['search_text'] = "";
		}
		$this->session->unset_userdata('search_text');
		$this->data['content']  = 'admin/users/index';
		$this->_render_page('templates/admin_template', $this->data);
	}

	public function set_search_session(){
		$this->ci =& get_instance();
		$array=array(
			'search_text'=>$this->input->get("text")
		);
		$this->ci->session->set_userdata($array);
		echo json_encode(array('text' => $this->session->userdata('search_text')));
	}

	public function add()
	{
		$this->data['css_type'] 	= array("form");
		$this->data['active_class'] = "users";
		$this->data['gmaps'] 	= false;
		$this->data['title'] 	= 'Users'; // $this->lang->line("calls");
		$this->data['title_link'] 	= base_url('admin/users');
		$this->data['subtitle'] = " > Add";
		$this->data['show_subtitle'] = true;
		$this->data['departments'] = $this->userx_model->departments();
		$this->data['roles'] = $this->userx_model->roles();
		$this->data['company_data'] = $this->userx_model->get_company();
		$this->data['content']  = 'admin/users/add';
		$this->data['countries'] = $this->cms_model->get_all_countries();
		$this->_render_page('templates/admin_template', $this->data);
	}

	public function save()
	{
		$this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
		// $this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]');
		$this->form_validation->set_rules('password', 'Password', 'required');
/*		$this->form_validation->set_rules('phone', 'Phone', 'required');
		$this->form_validation->set_rules('link', 'Link', 'required');*/
		$this->form_validation->set_rules('department[]', 'Department', 'required');
		$this->form_validation->set_rules('role', 'Role', 'required');

		if ($this->form_validation->run() == FALSE)
        {
        	$this->session->set_flashdata('validation_errors', validation_errors());
        	redirect('admin/users/add');
        }
        else
        {
        	$path_to_folder = 'uploads/user/';
        	$config['upload_path'] = realpath($path_to_folder);
        	$config['allowed_types'] = 'gif|jpg|jpeg|png';
        	$config['max_size']	= '3000000'; // 3mb
        	$new_name = uniqid('USR_', true);
        	$new_name = explode('.', $new_name)[0];
        	$config['file_name'] = $new_name;

        	$this->load->library('upload', $config);
        	if($this->upload->do_upload('avatar'))
        	{
        		$avatar = $this->upload->data();
        		$avatar = $avatar['file_name'];
        	}
        	else
        	{
        		$avatar = NULL;
			}
        	$config1['upload_path'] = realpath($path_to_folder);
        	$config1['allowed_types'] = 'gif|jpg|jpeg|png';
        	$config1['max_size']	= '3000000'; // 3mb
        	$new_name = uniqid('USR_', true);
        	$new_name = explode('.', $new_name)[0];
        	$config1['file_name'] = "sig_".$new_name;
			$this->load->library('upload', $config1);
			if($this->upload->do_upload('document_signature'))
        	{
        		$document_signature = $this->upload->data();
        		$document_signature = $document_signature['file_name'];
        	}
        	else
        	{
        		$document_signature = NULL;
        	}

        	$db_array = array(
				'ip_address' => $this->input->ip_address(),
        		// 'username' => $this->input->post('username'),
        		'password' => md5($this->input->post('password')),
        		'email' => $this->input->post('email'),
        		'active' => $this->input->post('status'),
        		'first_name' => $this->input->post('first_name'),
        		'last_name' => $this->input->post('last_name'),
        		'phone' => $this->input->post('phone'),
				'image' => $avatar,
				'document_signature' => $document_signature,
        		'department_id' => implode(",", $this->input->post('department')),
				'role_id' => $this->input->post('role'),
				'civility' => $this->input->post('gender'),
        		'gender' => $this->input->post('gender'),
				'link' => $this->input->post('link'),
				'fax' => $this->input->post('fax'),
				'address' => $this->input->post('address'),
				'address1' => $this->input->post('address1'),
				'zipcode' => $this->input->post('zipcode'),
				'city' => $this->input->post('city'),
				'country' => $this->input->post('country'),
				'region' => $this->input->post('region'),
        	);

//			var_dump($db_array);exit;
        	$id = $this->userx_model->create($db_array);
        	$this->session->set_flashdata('alert', [
        			'message' => "Role successfully added.",
        			'class' => "alert-success",
        			'type' => "Success"
        	]);
        	 // code by s_a 
        	$row_data = $this->userx_model->get(['user.id' => $id]);
        	$timestampid=create_timestamp_uid($row_data['created_at'],$row_data['id']);
        	  // code by s_a 
        	redirect('admin/users/'.$timestampid.'/edit');
        }
	}

	public function edit($id){
     // code by s_a 
		$timestampid=$id;
		$date = substr($timestampid, 0, 8);
		$id =str_replace($date,"",$timestampid);
		$id=ltrim($id, "0");
      // code by s_a 

		$this->data['css_type'] 	= array("form");
		$this->data['active_class'] = "users";
		$this->data['gmaps'] 	= false;
		$this->data['title'] 	= 'Users'; // $this->lang->line("calls");
		$this->data['title_link'] 	= base_url('admin/users');
		$this->data['row_data'] = $this->userx_model->get(['user.id' => $id]);
		$this->data['departments'] = $this->userx_model->departments();
		$this->data['roles'] = $this->userx_model->roles();
		$this->data['company_data'] = $this->userx_model->get_company();
		$this->data['show_subtitle'] = true;
		$this->data['subtitle'] = ' > '.create_timestamp_uid($this->data['row_data']['created_at'],$this->data['row_data']['id']);
		$this->data['content']  = 'admin/users/edit';
		$this->data['countries'] = $this->cms_model->get_all_countries();
		$this->_render_page('templates/admin_template', $this->data);
	}

	public function update($id)
	{
		 // code by s_a 
		$timestampid=$id;
		$date = substr($timestampid, 0, 8);
		$id =str_replace($date,"",$timestampid);
		$id=ltrim($id, "0");
      // code by s_a 
		$id_data = $this->userx_model->get(['user.id' => $id]);
		if($id_data != false)
		{
			$this->form_validation->set_rules('first_name', 'First Name', 'required');
			$this->form_validation->set_rules('last_name', 'Last Name', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check['.$id.']');
			// $this->form_validation->set_rules('username', 'Username', 'required|callback_username_check['.$id.']');
/*			$this->form_validation->set_rules('phone', 'Phone', 'required');
			$this->form_validation->set_rules('link', 'Link', 'required');*/
			$this->form_validation->set_rules('department[]', 'Department', 'required');
			$this->form_validation->set_rules('role', 'Role', 'required');

			if ($this->form_validation->run() == FALSE)
	        {
	        	$this->session->set_flashdata('validation_errors', validation_errors());
	        	 // code by s_a 
	        	redirect('admin/users/'.$timestampid.'/edit');
	        	 // code by s_a 
	        }
	        else
	        {
	        	$path_to_folder = 'uploads/user/';
	        	$old_avatar = $this->input->post('old_avatar');
				$path_to_folder_old = $path_to_folder.$old_avatar;
				$old_document_signature = $this->input->post('old_document_signature');
	        	$path_to_folder_old1 = $path_to_folder.$old_document_signature;
	        	$config['upload_path'] = realpath($path_to_folder);
	        	$config['allowed_types'] = 'gif|jpg|jpeg|png';
	        	$config['max_size']	= '3000000'; // 3mb
	        	$new_name = uniqid('USR_', true);
	        	$new_name = explode('.', $new_name)[0];
	        	$config['file_name'] = $new_name;

	        	$this->load->library('upload', $config);
	        	if($this->upload->do_upload('avatar'))
	        	{
	        		$avatar = $this->upload->data();
	        		$avatar = $avatar['file_name'];
	        		if(is_file($path_to_folder_old))
	        		{
	        			unlink($path_to_folder_old);
	        		}
	        	}
	        	else
	        	{
	        		$avatar = $old_avatar;
				}
				$config1['upload_path'] = realpath($path_to_folder);
				$config1['allowed_types'] = 'gif|jpg|jpeg|png';
				$config1['max_size']	= '3000000'; // 3mb
				$new_name = uniqid('USR_', true);
				$new_name = explode('.', $new_name)[0];
				$config1['file_name'] = "sig_".$new_name;
				$this->load->library('upload', $config1);
				if($this->upload->do_upload('document_signature'))
	        	{
	        		$document_signature = $this->upload->data();
	        		$document_signature = $document_signature['file_name'];
	        		if(is_file($path_to_folder_old1))
	        		{
	        			unlink($path_to_folder_old1);
	        		}
	        	}
	        	else
	        	{
	        		$document_signature = $old_document_signature;
	        	}

	        	if(empty($this->input->post('password')))
	        	{
	        		$password = $this->input->post('old_password');
	        	}
	        	else
	        	{
	        		$password = md5($this->input->post('password'));
	        	}
	        	$db_array = array(
	        		// 'username' => $this->input->post('username'),
	        		'password' => md5($this->input->post('password')),
	        		'email' => $this->input->post('email'),
	        		'active' => $this->input->post('status'),
	        		'first_name' => $this->input->post('first_name'),
	        		'last_name' => $this->input->post('last_name'),
	        		'phone' => $this->input->post('phone'),
					'image' => $avatar,
					'document_signature' => $document_signature,
					//date_of_registration' => date('Y-m-d'),
	        		'department_id' => implode(",", $this->input->post('department')),
					'role_id' => $this->input->post('role'),
					'civility' => $this->input->post('gender'),
	        		'gender' => $this->input->post('gender'),
					'link' => $this->input->post('link'),
					'fax' => $this->input->post('fax'),
					'address' => $this->input->post('address'),
					'address1' => $this->input->post('address1'),
					'zipcode' => $this->input->post('zipcode'),
					'city' => $this->input->post('city'),
					'country' => $this->input->post('country'),
					'region' => $this->input->post('region'),
	        	);

	        	$this->userx_model->update($db_array,$id);
	        	   //code by s_a
	        	$user=$this->basic_auth->user();
	        	if($user->id == $id){
                $user->image=$avatar;
                $this->session->set_userdata("user", $user);
                  }
               //code by s_a
	        	$this->session->set_flashdata('alert', [
	        			'message' => "User successfully updated.",
	        			'class' => "alert-success",
	        			'type' => "Success"
	        	]);
	        	 // code by s_a 
	        	redirect('admin/users/'.$timestampid.'/edit');
	        	 // code by s_a 
	        }
		}
		else
		{
			show_404();
		}
	}

	public function email_check($str,$id)
    {
        if ($this->userx_model->validate_string('vbs_users','email',$str,$id) === true)
        {
            $this->form_validation->set_message('email_check', $str.' is already in use');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    public function username_check($str,$id)
    {
        if ($this->userx_model->validate_string('vbs_users','username',$str,$id) === true)
        {
            $this->form_validation->set_message('username_check', $str.' is already in use');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

	public function delete($id){
		$this->userx_model->delete($id);

		$this->session->set_flashdata('alert', [
				'message' => "User successfully deleted.",
				'class' => "alert-success",
				'type' => "Success"
		]);
		redirect('admin/users');
	}

	public function delete_data(){
		$array= $_POST["arr"];
		$array=trim($array, ' ');
		$array=rtrim($array, ',');
		$array=ltrim($array, ',');
		$array = explode(',', $array);


		$this->userx_model->delete_data($array);
		$this->session->set_flashdata('alert', [
				'message' => "User successfully deleted.",
				'class' => "alert-success",
				'type' => "Success"
		]);
		redirect('admin/users');
		
	}
}