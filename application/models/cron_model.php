<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Cron_Model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	public function getnotification($table,$where){
		 $query = $this->db->where($where)->get($table);
	     return $query->num_rows() > 0 ? $query->result() : false;
	}
	public function getreminder($table,$where){
		 $query = $this->db->where($where)->get($table);
	     return $query->num_rows() > 0 ? $query->result() : false;
	}
}