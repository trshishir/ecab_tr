<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Bookings_Config_Model extends CI_Model {

	function __construct(){
		parent::__construct();
	}


	public static $table = "bookings";

	public function get($where = []){
		$query = $this->db->get_where(self::$table, $where);
		return $query->num_rows() > 0 ? $query->row() : false;
	}
	public function categoryget($where = []){
		$query = $this->db->get_where('vbs_booking_category', $where);
		return $query->num_rows() > 0 ? $query->row() : false;
	}


	public function getAll($where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get(self::$table);
		return $query->num_rows() > 0 ? $query->result() : false;
	}


	public function getAllCategory($where = [], $limit = false){
			if(!empty($where)) $this->db->where($where);
			if($limit != false) $this->db->limit($limit);
			$this->db->order_by('id','desc');
			$query = $this->db->get('vbs_booking_category');
			return $query->num_rows() > 0 ? $query->result() : false;
		}

  public function QouteChartCount(){
      $data = $this->db->query("SELECT COUNT(id) as count,MONTHNAME(created_at) as month_name FROM vbs_request WHERE YEAR(created_at) >= '2019'
    GROUP BY YEAR(created_at),MONTH(created_at)");
      return  $data->result();;
  }
  public function QouteLineChart() {
      $dayQuery =  $this->db->query("SELECT  MONTHNAME(created_at) as y, COUNT(id) as a FROM vbs_request WHERE  YEAR(created_at) >= '2019'  GROUP BY YEAR(created_at),MONTH(created_at)");
      return $dayQuery->result();
  }
	public function create($data){
		$this->db->trans_begin();
		$this->db->insert(self::$table, $data);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}


	public function categorycreate($data){
		$this->db->trans_begin();
		$this->db->insert('vbs_booking_category', $data);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}

	public function categoryupdate($data, $id){
		if($this->db->update('vbs_booking_category', $data, ['id' => $id]))
			return true;
		else
			return false;
	}

	public function update($data, $id){
		if($this->db->update(self::$table, $data, ['id' => $id]))
			return true;
		else
			return false;
	}

	public function delete($id){
		return $this->db->delete(self::$table, ['id' => $id]);
	}


	public static function validate(){
		$reqInputs = [
				'civility'  => 'Civility',
				'name'      => 'Nom',
				'prename'   => 'Prenom',
				'email'     => 'Votre email',
				'tel'       => 'Telephone',
				'message'   => 'Votre message'
		];

		$error = [];
		foreach($reqInputs as $key => $input){
			$check = !isset($_POST[$key]) || $_POST[$key] == "" || empty($_POST[$key]);
			if($check){
				$error[] = "Merci de compléter toutes les cases correctement.";
			}
			if($key == "email" AND !filter_var($_POST[$key], FILTER_VALIDATE_EMAIL)){
				$error[] = "S'il vous plaît entrer un email valide.";
			}
			if($key == "tel" AND !preg_match('/^0[0123456790]\d{8}$/', $_POST[$key])){
				$error[] = "Veuillez entrer un numéro de téléphone valide avec 10 chiffres commençant par 0.";
			}
		}
		return $error;
	}

	public function getAllCarcat($where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get('vbs_vehicle_categories');
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	public function getAllClientList($where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get('vbs_client');
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	public function getAllDriverList($where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get('vbs_driverProfile');
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	public function getAllCarList($where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get('vbs_vehicle');
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	public function getAllService($where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get('vbs_services');
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	//Start Booking Config Code
	public function getuser($id){
		$where =array('id'=> $id);
		$table='vbs_users';
		$query = $this->db->get_where($table, $where);
		 if ($query->num_rows() > 0) {
			$query=  $query->row();
		return	$query = 'Mr. '. $query->first_name .' '.$query->last_name;
		}
		else {
			return false;
		}
	}
	public function getservicecategorybyid($table,$id){

	    	$where =array('id'=> $id);
          $query = $this->db->get_where($table, $where);

		 if ($query->num_rows() > 0) {
			$query=  $query->row()->category_name;
			return $query;
		}else{
			return false;
		}
	}
	public function getvatbyid($table,$id){

	    	$where =array('id'=> $id);
          $query = $this->db->get_where($table, $where);

		 if ($query->num_rows() > 0) {
			$query=  $query->row()->vat;
			return $query;
		}else{
			return false;
		}
	}
		public function getpoicatgorybyid($table,$id){

	    	$where =array('id'=> $id);
          $query = $this->db->get_where($table, $where);

		 if ($query->num_rows() > 0) {
			$query=  $query->row()->ride_cat_name;
			return $query;
		}else{
			return false;
		}
	}
	public function getpaymentmethodsbycategory($categoryid){
		$status=1;
		 $q=$this->db
	  ->select(['payment.name'])
	  ->from('vbs_servicepayment_category as servicepayment')
	  ->where(['servicepayment.servicecategory_id'=>$categoryid])
	  ->join('vbs_u_payment_method as payment', 'servicepayment.paymentmethod_id=payment.id','inner')
	  ->get();
	  return $q->result();
	}

	public function getAllVat($where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get('vbs_u_vat');
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	
	public function addVat($data){
		$table = "vbs_u_vat";
		$this->db->trans_begin();
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}
	public function vatUpdate($data, $id){
		$table = "vbs_u_vat";
		if($this->db->update($table, $data, ['id' => $id]))
			return true;
		else
			return false;
	}
	public function deletevat($id){
			$table = "vbs_u_vat";
		return $this->db->delete($table, ['id' => $id]);
	}
public function getconfigurationrecord($table,$id){
 $q=$this->db->where('id',$id)->get($table);
		return $result = $q->row_array();
}
	// Discount in Booking congfig
	public function booking_Config_getAll($table,$where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get($table);
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	// Discount in Booking congfig
public function getAllData($table,$where = [], $limit = false){
	if(!empty($where)) $this->db->where($where);
	if($limit != false) $this->db->limit($limit);
	$this->db->order_by('id','desc');
	$query = $this->db->get($table);
	return $query->num_rows() > 0 ? $query->result() : false;
}
public function getAllDiscount($where = [], $limit = false){
	if(!empty($where)) $this->db->where($where);
	if($limit != false) $this->db->limit($limit);
	$this->db->order_by('id','desc');
	$query = $this->db->get('vbs_u_discount');
	return $query->num_rows() > 0 ? $query->result() : false;
}

	public function booking_Config_Add($table,$data){
		// $table = "vbs_u_discount";
		$this->db->trans_begin();
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}
	public function booking_Config_Update($table,$data, $id){
		// $table = "vbs_u_discount";
		if($this->db->update($table, $data, ['id' => $id]))
			return true;
		else
			return false;
	}
	public function booking_Config_Delete($table,$id){
		return $this->db->delete($table, ['id' => $id]);
	}
	public function booking_catpaymentConfig_Delete($table,$id){
		return $this->db->delete($table, ['servicecategory_id' => $id]);
	}

	public function	booking_Carconfig_Delete($table,$id){

		return $this->db->delete($table, ['car_id' => $id]);
	}

	public function  d_type($id){
		$q=$this->db->where("id",$id)->get("vbs_u_ride_category");
		return $q->row()->ride_cat_name;
	}
	public function  departure($id){
		$q=$this->db->where("id",$id)->get("vbs_u_poi");
		return $q->row()->name;
	}
	public function  destination_type($id){
		$q=$this->db->where("id",$id)->get("vbs_u_ride_category");
		return $q->row()->ride_cat_name;
	}
	public function  destination($id){
		$q=$this->db->where("id",$id)->get("vbs_u_poi");
		return $q->row()->name;
	}

//added by sultan
	public function booking_Configworkingdata_Delete($table,$id){
		return $this->db->delete($table, ['pirce_car_id' => $id]);
	}
	public function booking_discountperiod_Delete($table,$id){
		return $this->db->delete($table, ['discount_id' => $id]);
	}
	public function getAllStatut($where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get('vbs_u_bookingstatut');
		return $query->num_rows() > 0 ? $query->result() : false;
	}
		public function addStatut($data){
		$table = "vbs_u_bookingstatut";
		$this->db->trans_begin();
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}
		public function statutUpdate($data, $id){
		$table = "vbs_u_bookingstatut";
		if($this->db->update($table, $data, ['id' => $id]))
			return true;
		else
			return false;
	}
	public function deletestatut($id){
			$table = "vbs_u_bookingstatut";
		return $this->db->delete($table, ['id' => $id]);
	}
	public function deleteapi($id){
			$table = "vbs_u_googleapi";
		return $this->db->delete($table, ['id' => $id]);
	}
	public function addApi($data){
		$table = "vbs_u_googleapi";
		$this->db->trans_begin();
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}
		public function apiUpdate($data, $id){
		$table = "vbs_u_googleapi";
		if($this->db->update($table, $data, ['id' => $id]))
			return true;
		else
			return false;
	}
		//add price status
	public function deletepricestatus($id){
			$table = "vbs_price_status";
		return $this->db->delete($table, ['id' => $id]);
	}
	public function addpricestatus($data){
		$table = "vbs_price_status";
		$this->db->trans_begin();
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}
		public function pricestatusUpdate($data, $id){
		$table = "vbs_price_status";
		if($this->db->update($table, $data, ['id' => $id]))
			return true;
		else
			return false;
	}
	public function getpricestatusdata($table){
	$q= $this->db->order_by('id',"desc")
		->limit(1)
		->get($table);
		return ($q->num_rows > 0)? $q->row() :false;
		
	}
	//add disable category
	public function addDisablecat($data){
		$table = "vbs_u_disablecategory";
		$this->db->trans_begin();
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}
		public function disablecatUpdate($data, $id){
		$table = "vbs_u_disablecategory";
		if($this->db->update($table, $data, ['id' => $id]))
			return true;
		else
			return false;
	}
	public function deletedisablecat($id){
			$table = "vbs_u_disablecategory";
		return $this->db->delete($table, ['id' => $id]);
	}
	//end disable category
	//invoice section
	public function getinvoicestatusdata($table){
	$q= $this->db->order_by('id',"desc")
		->limit(1)
		->get($table);
		return ($q->num_rows > 0)? $q->row() :false;
		
	}
	public function invoicestatusUpdate($data, $id){
		$table = "vbs_invoices_config";
		if($this->db->update($table, $data, ['id' => $id]))
			return true;
		else
			return false;
	}

	public function addinvoicestatus($data){
		$table = "vbs_invoices_config";
		$this->db->trans_begin();
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}
	//invoice section

}
