<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Filemanagement_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public static $table = "filemanagement";

    public function get($where = []) {
        $query = $this->db->get_where(self::$table, $where);
        return $query->num_rows() > 0 ? $query->row() : false;
    }

    public function getFile($where = []) {
        $query = $this->db->get_where(self::$table, $where);
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function getUserFiles($username) {
        $username = strtolower($username);
        $query = $this->db->query("SELECT * FROM vbs_filemanagement WHERE LOWER(name) = '" . $username . "' OR LOWER(added_by_lastname) = '" . $username . "'");
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }
    
    public function getAll($where = [], $limit = false) {
        if (!empty($where))
            $this->db->where($where);
        if ($limit != false)
            $this->db->limit($limit);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get(self::$table);
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function getAllSenderNames(){
        $query = $this->db->query("SELECT selection FROM vbs_selections");
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function add_sender($data) {
        return $this->db->insert("vbs_selections", $data);
    }

    public function delete_sender($data) {
        return $this->db->delete("vbs_selections", ['selection' => $data]);
    }

    public function create($data) {
        $this->db->trans_begin();
        $this->db->insert(self::$table, $data);
        $insert_id = $this->db->insert_id();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return $insert_id;
        }
    }

    public function update($data, $id) {
        if ($this->db->update(self::$table, $data, ['id' => $id]))
            return true;
        else
            return false;
    }

    public function delete($id) {
        return $this->db->delete(self::$table, ['id' => $id]);
    }
    public function delete_multiple(){
        echo "here";
    }




    public static function validate() {

        $reqInputs = [
            'status' => 'status',
            'type' => 'type',
            'date' => 'date',
            'name' => 'name',
            'nature' => 'nature',
            'priority' => 'priority',
            'alert' => 'alert',
            'delay_date' => 'delay_date',
           
//            'note' => 'note',
            'send_date' => 'send_date',
            'send_date_hour' => 'send_date_hour',
            'added_by_firstname' => 'added_by_firstname',
            'added_by_lastname' => 'added_by_lastname',
            'description' => 'description',
//            'selection' => 'selection',
            'name_selection' => 'name_selection',
            'files' => 'file'
        ];

        $error = [];

        foreach ($reqInputs as $key => $input) {
            $check = !isset($_POST[$key]) || $_POST[$key] == "" || empty($_POST[$key]);
            if ($key == 'files') {
                $check = (!isset($_FILES['files']) || empty($_FILES['files']['name']));
            }
            if ($check) {
                echo $key;
                die();
                $error[] = "Merci de compléter toutes les cases correctement.";
            }
            if ($key == "email" AND ! filter_var($_POST[$key], FILTER_VALIDATE_EMAIL)) {
                $error[] = "S'il vous plaît entrer un email valide.";
            }
            if ($key == "tel" AND ! preg_match('/^0[0123456790]\d{8}$/', $_POST[$key])) {
                $error[] = "Veuillez entrer un numéro de téléphone valide avec 10 chiffres commençant par 0.";
            }
        }

        return $error;
    }

    public function search_files($searchFilter) {

        $whereClause = "";

        $whereClause .= " LOWER(file) != '' ";

        if($searchFilter['name'] !='') {
            $whereClause .= " AND LOWER(name) LIKE '%". strtolower($searchFilter['name']) . "%' ";
        }

        if($searchFilter['type'] !='') {
            $whereClause .= " AND LOWER(type) LIKE '%". strtolower($searchFilter['type']) . "%' ";
        }

        if($searchFilter['alert'] !='') {
            $whereClause .= " AND LOWER(alert) LIKE '%". strtolower($searchFilter['alert']) . "%' ";
        }

        if($searchFilter['department'] !='') {
            $whereClause .= " AND LOWER(destination) LIKE '%". strtolower($searchFilter['department']) . "%' ";
        }

        if($searchFilter['priority'] !='') {
            $whereClause .= " AND LOWER(priority) LIKE '%". strtolower($searchFilter['priority']) . "%' ";
        }

        if($searchFilter['date_from'] !='' && $searchFilter['date_to'] != '') {
            $whereClause .= " AND ( date >= '" . date('Y-m-d',strtotime($searchFilter['date_from'])) . " 00:00:00' AND date <= '" . date('Y-m-d',strtotime($searchFilter['date_to'])) ." 23:59:59' ) ";
        }
        if($searchFilter['date_from'] !='' && $searchFilter['date_to'] =='') {
            $whereClause .= " AND date >= '" . date('Y-m-d H:i:s',strtotime($searchFilter['date_from'])) . "' ";
        }
        if($searchFilter['date_to'] !='' && $searchFilter['date_from'] =='') {
            $whereClause .= " AND date <= '" . date('Y-m-d H:i:s',strtotime($searchFilter['date_to'])) . "' ";
        }
        if($searchFilter['status'] !='') {
            $whereClause .= " AND status ='".$searchFilter['status']."'";
        }
        if($searchFilter['nature'] !='') {
            $whereClause .= " AND nature ='".$searchFilter['nature']."'";
        }
         
        $searchQuery = "SELECT * From vbs_filemanagement Where ".$whereClause;

        $query = $this->db->query($searchQuery);
    
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    function run_query($query) {
        return($this->db->query($query)->result());
    }

}
