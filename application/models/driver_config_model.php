<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Driver_Config_Model extends CI_Model {

	function __construct(){
		parent::__construct();
	}


	public static $table = "driverprofile";

	public function get($where = []){
		$query = $this->db->get_where(self::$table, $where);
		return $query->num_rows() > 0 ? $query->row() : false;
	}
	public function categoryget($where = []){
		$query = $this->db->get_where('vbs_driver_category', $where);
		return $query->num_rows() > 0 ? $query->row() : false;
	}


	public function getAll($where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get(self::$table);
		return $query->num_rows() > 0 ? $query->result() : false;
	}


	public function getAllCategory($where = [], $limit = false){
			if(!empty($where)) $this->db->where($where);
			if($limit != false) $this->db->limit($limit);
			$this->db->order_by('id','desc');
			$query = $this->db->get('vbs_driver_category');
			return $query->num_rows() > 0 ? $query->result() : false;
		}

  public function QouteChartCount(){
      $data = $this->db->query("SELECT COUNT(id) as count,MONTHNAME(created_at) as month_name FROM vbs_request WHERE YEAR(created_at) >= '2019'
    GROUP BY YEAR(created_at),MONTH(created_at)");
      return  $data->result();;
  }
  public function QouteLineChart() {
      $dayQuery =  $this->db->query("SELECT  MONTHNAME(created_at) as y, COUNT(id) as a FROM vbs_request WHERE  YEAR(created_at) >= '2019'  GROUP BY YEAR(created_at),MONTH(created_at)");
      return $dayQuery->result();
  }
	public function create($data){
		$this->db->trans_begin();
		$this->db->insert(self::$table, $data);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}


	public function categorycreate($data){
		$this->db->trans_begin();
		$this->db->insert('vbs_driver_category', $data);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}

	public function categoryupdate($data, $id){
		if($this->db->update('vbs_driver_category', $data, ['id' => $id]))
			return true;
		else
			return false;
	}

	public function update($data, $id){
		if($this->db->update(self::$table, $data, ['id' => $id]))
			return true;
		else
			return false;
	}

	public function delete($id){
		return $this->db->delete(self::$table, ['id' => $id]);
	}


	public static function validate(){
		$reqInputs = [
				'civility'  => 'Civility',
				'name'      => 'Nom',
				'prename'   => 'Prenom',
				'email'     => 'Votre email',
				'tel'       => 'Telephone',
				'message'   => 'Votre message'
		];

		$error = [];
		foreach($reqInputs as $key => $input){
			$check = !isset($_POST[$key]) || $_POST[$key] == "" || empty($_POST[$key]);
			if($check){
				$error[] = "Merci de compléter toutes les cases correctement.";
			}
			if($key == "email" AND !filter_var($_POST[$key], FILTER_VALIDATE_EMAIL)){
				$error[] = "S'il vous plaît entrer un email valide.";
			}
			if($key == "tel" AND !preg_match('/^0[0123456790]\d{8}$/', $_POST[$key])){
				$error[] = "Veuillez entrer un numéro de téléphone valide avec 10 chiffres commençant par 0.";
			}
		}
		return $error;
	}

	public function getAllCarcat($where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get('vbs_vehicle_categories');
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	public function getAllClientList($where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get('vbs_client');
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	public function getAllDriverList($where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get('vbs_driverProfile');
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	public function getAllCarList($where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get('vbs_vehicle');
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	public function getAllService($where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get('vbs_services');
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	//Start Booking Config Code
	public function getuser($id){
		$where =array('id'=> $id);
		$table='vbs_users';
		$query = $this->db->get_where($table, $where);
		 if ($query->num_rows() > 0) {
			$query=  $query->row();
		return	$query = 'Mr. '. $query->first_name .' '.$query->last_name;
		}
		else {
			return false;
		}
	}

	public function getDriverStatus($where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get('vbs_driverstatus');
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	public function addDriverStatus($data){
		$table = "vbs_driverstatus";
		$this->db->trans_begin();
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}
	public function driverStatusUpdate($data, $id){
		$table = "vbs_driverstatus";
		if($this->db->update($table, $data, ['id' => $id]))
			return true;
		else
			return false;
	}
	public function deleteDriverStatus($id){
			$table = "vbs_driverstatus";
		return $this->db->delete($table, ['id' => $id]);
	}

	// Civilite in Booking congfig
	public function driver_Config_getAll($table,$where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get($table);
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	// Civilite in Booking congfig
	public function getAllData($table,$where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get($table);
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	public function getAllCivilite($where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get('vbs_drivercivilite');
		return $query->num_rows() > 0 ? $query->result() : false;
	}

	public function driver_Config_Add($table,$data){
		// $table = "vbs_drivercivilite";
		$this->db->trans_begin();
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}
	public function driver_Config_Update($table,$data, $id){
		// $table = "vbs_drivercivilite";
		if($this->db->update($table, $data, ['id' => $id]))
			return true;
		else
			return false;
	}
	public function driver_Config_Delete($table,$id){
		return $this->db->delete($table, ['id' => $id]);
	}

	public function  d_type($id=0){
		$fieldArr=array('0'=>"POI",'1'=>"City",'2'=>"Region");
		return $fieldArr[$id];
	}
	public function  departure($id=0){
		$fieldArr=array('0'=>"CDG",'1'=>"Park",'2'=>"Hotel");
		return $fieldArr[$id];
	}
	public function  destination_type($id=0){
		$fieldArr=array('0'=>"POI",'1'=>"City",'2'=>"Region");
		return $fieldArr[$id];
	}
	public function  destination($id=0){
		$fieldArr=array('0'=>"CDG",'1'=>"Park",'2'=>"Hotel");
		return $fieldArr[$id];
	}

}
