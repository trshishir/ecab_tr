<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Cms_Model extends CI_Model {

    function __construct(){
        parent::__construct();
    }


    public static $table = "cms";

    public function get($where = []){
        $query = $this->db->get_where(self::$table, $where);
        return $query->num_rows() > 0 ? $query->row() : false;
    }

    public function getActiveAll(){
        $query = $this->db->get("vbs_cms");
        return $query->result();
    }



    public function get_all_countries(){
        $query = $this->db->get("countries");
        return $query->result();
    }



    public function getAll($where = [], $limit = false){
        if(!empty($where)) $this->db->where($where);
        if($limit != false) $this->db->limit($limit);
        $this->db->order_by('id','desc');
        $query = $this->db->get(self::$table);
        return $query->num_rows() > 0 ? $query->result() : false;
    }
    // public function QouteChartCount(){
    //     $data = $this->db->query("SELECT COUNT(id) as count,MONTHNAME(created_at) as month_name FROM vbs_request WHERE YEAR(created_at) >= '2019'
    //   GROUP BY YEAR(created_at),MONTH(created_at)");
    //     return  $data->result();;
    // }
    // public function QouteLineChart() {
    //     $dayQuery =  $this->db->query("SELECT  MONTHNAME(created_at) as y, COUNT(id) as a FROM vbs_request WHERE  YEAR(created_at) >= '2019'  GROUP BY YEAR(created_at),MONTH(created_at)");

    //     return $dayQuery->result();

    // }
    public function create($data){
        $this->db->trans_begin();
        $this->db->insert(self::$table, $data);
        $insert_id = $this->db->insert_id();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return $insert_id;
        }
    }

    public function update($data, $id){
        if($this->db->update(self::$table, $data, ['id' => $id]))
            return true;
        else
            return false;
    }

    public function update_info_bull($data, $id=1){

        if($this->db->update('cms_info_bull', $data, ['id' => $id]))
            return true;
        else
            return false;
    }


    public function getFirstInfobul(){
        $query = $this->db->get('cms_info_bull');
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }


    public function delete($id){
        return $this->db->delete(self::$table, ['id' => $id]);
    }



    /*

    Added By rahul689

     */


    public function add_new_category($name,$slug,$status)
    {
        $data = array(
            'name' => $name,
            'slug' => $slug,
            'status' => ($status)?1:0
        );

        if($this->db->insert('cms_category', $data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function update_category($name,$slug,$status,$id)
    {
        $status= ($status=='true')?1:0;

        $data = array(
            'name' => $name,
            'slug' => $slug,
            'status' =>$status
        );

        $this->db->set($data );
        $this->db->where('id', $id);

        if($this->db->update('cms_category'))
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public function getActiveCategory($slug,$status=null){
        if($status!=null)
        {

            $status= ($status)?1:0;
            $this->db->where('status',$status);
        }
        $this->db->where('page_type',$slug);
        $this->db->where('is_category',1);

        $query = $this->db->get("cms_page_post");

        return $query->result();
    }



    public function getActiveCategory_id($id){
        $this->db->where('id',$id);

        $this->db->where('page_type',$slug);
        $this->db->where('is_category',1);

        $query = $this->db->get("cms_page_post");

        return $query->result();
    }


    public function save_banner_listing($img_name, $section)
    {
        $old_result = $this->db->get('cms_banner')->row();

        $this->db->set($section,  $img_name);
        $this->db->where('id', 1);
        $this->db->update('cms_banner');

        return $old_result;

    }

    public function getCMSBanner()
    {
        return 	$this->db->get('cms_banner')->row();
    }

    public function getCmsSlider($section)
    {
        $select = 'cms_slider.*,cms_slider.category_name as category_id,
        cms_slider.service_name as service_id';
        if($section != "jobs") {
            $select .= ',cpp.name AS category_name,cpp1.title AS service_name,
                        cpp1.short_description AS service_short_description,
                        cpp.link_url as category_link,
                        cpp1.link_url as service_link,
                        cpp1.link,
                        cpp1.description AS service_description';
        } else {
            $select .= ',cpp.jobcategory AS category_name, cpp2.job AS service_name,
                        cpp1.description AS service_short_description,
                        cpp1.job_slug as service_link,
                        cpp1.description AS service_description';
        }
        $this->db->select($select);
        $this->db->from('cms_slider');

        if($section != "jobs") {
            $this->db->join('cms_page_post AS cpp', 'cms_slider.category_name=cpp.id', 'left');
            $this->db->join('cms_page_post AS cpp1', 'cms_slider.service_name=cpp1.id', 'left');
        } else {
            $this->db->join('job_jobcategory as cpp', 'cpp.id = cms_slider.category_name', 'left');
            $this->db->join('job AS cpp1', 'cpp1.id = cms_slider.service_name', 'left');
            $this->db->join('job_job AS cpp2', 'cpp2.id = cpp1.job_id', 'left');
        }


        $this->db->where('cms_slider.section',$section);
        // $this->db->where('cms_slider.status',$status);

        $result = $this->db->get();

        $result = $result != false ? $result->result() : [];

// echo $this->db->last_query();
// die();



        // $this->db->where('section', $section);
        // $result=$this->db->get('cms_slider')->result();

        return $result;

    }

    public function deleteCmsSlider($id)
    {

        $this->db->where('id', $id);
        $old_result=$this->db->get('cms_slider')->row();

        $this->db->where('id', $id);
        if($this->db->delete('cms_slider'))
        {
            return $old_result;
        }
        else{
            return false;

        }

    }

    public function save_cms_sliders($data){
        $this->db->trans_begin();
        foreach($data as $key => $item){

            $arr = [
                'section' => @$item['section'],
                'category_name' => (int)@$item['category'],
                'service_name' => (int)@$item['sub_category'],
            ];
            if(@$item['photo'] != "0" && is_array($item['photo'])){
                $arr['img'] = $item['photo']['file_name'];
            }

            if(@$item['leftIcon'] != "0" && is_array($item['leftIcon'])){
                $arr['left_icon'] = $item['leftIcon']['file_name'];
            }

            if(@$item['rightIcon'] != "0" && is_array($item['rightIcon'])){
                $arr['right_icon'] = $item['rightIcon']['file_name'];
            }

            //var_dump($arr);
            if(isset($item['sid']) && $item['sid'] != "0"){
                //var_dump($arr);
                //var_dump($item['sid']);
                $this->db->update("cms_slider", $arr, ['id' => (int)$item['sid']]);
            } else {
                $this->db->insert("cms_slider", $arr);
            }
            //var_dump($this->db->last_query());
        }
        //var_dump($this->db->trans_status() === FALSE);exit;
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            //exit;
            return true;
        }
    }

    public function add_cms_slider($input,$file_data,$left_icons,$right_icons)
    {
        extract($input);
// echo '<pre>';
// var_export($input);
// var_export($left_icons['file_name']);
// var_export($right_icons['file_name']);
// var_export($file_data);
        // die();

        $data['section'] =$section;
        if(isset($category_name)){
            $data['category_name'] =$category_name;

        }
        if(!isset($service_name) && isset($old_service_name))
        {
            $service_name =$old_service_name;
        }

        if(isset($service_name))
        {
            $data['service_name'] =$service_name;

        }
        if(isset($title))
        {
            $data['title'] =$title;

        }
        if(isset($description))
        {
            $data['description'] =$description;

        }
        if(isset($know_more))
        {
            $data['know_more'] =$know_more;

        }
        if(isset($subtitle))
        {
            $data['subtitle'] =$subtitle;

        }
        if(isset($book_now))
        {
            $data['book_now'] =$book_now;

        }
        if(isset($know_more_link))
        {
            $data['know_more_link'] =$know_more_link;

        }

        if(isset($book_now_link))
        {
            $data['book_now_link'] =$book_now_link;

        }


        if(!isset($left_icons['file_name']) && isset($old_left_icon))
        {
            $left_icons =$old_left_icon;
        }
        if(isset($left_icons))
        {

            if(is_array($left_icons))
            {
                $data['left_icon'] =$left_icons['file_name'];

            }
            else{
                $data['left_icon'] =$left_icons;

            }

        }

        if(!isset($right_icons['file_name']) && isset($old_right_icon))
        {
            $right_icons =$old_right_icon;
        }


        if(isset($right_icons))
        {

            if(is_array($right_icons))
            {
                $data['right_icon'] =$right_icons['file_name'];

            }
            else{
                $data['right_icon'] =$right_icons;

            }

        }

// echo '<pre>';
// var_dump($data);
// die();

        if(!empty($file_data))
        {
            $i=0;
            foreach ($file_data as $value) {
                $data['img'] =$value['fileInfo']['file_name'];
                $data['link'] =$link[$i];

                $this->db->insert('cms_slider', $data);
                $i++;
            }

        }
        // else{
        // $this->db->insert('cms_slider', $data);
        $section_slider =$this->getCmsSlider($section);

        foreach ($section_slider as $key => $value) {

            if(isset($category_name))
            {
                $this->db->set('category_name', $category_name);
            }
            if(isset($category_name))
            {
                $this->db->set('category_name', $category_name);
            }
            if(isset($service_name))
            {
                $this->db->set('service_name', $service_name);
            }
            if(isset($title))
            {
                $this->db->set('title', $title);
            }
            if(isset($subtitle))
            {
                $this->db->set('subtitle', $subtitle);
            }
            if(isset($description))
            {
                $this->db->set('description', $description);
            }
            if(isset($know_more))
            {
                $this->db->set('know_more', $know_more);
            }
            if(isset($book_now))
            {
                $this->db->set('book_now', $book_now);
            }
            if(isset($know_more_link))
            {
                $this->db->set('know_more_link', $know_more_link);
            }
            if(isset($book_now_link))
            {
                $this->db->set('book_now_link', $book_now_link);
            }


            if(isset($right_icons))
            {

                if(is_array($right_icons))
                {
                    $this->db->set('right_icon', $right_icons['file_name']);

                }
                else{
                    $this->db->set('right_icon', $right_icons);

                }
            }

            if(isset($left_icons))
            {

                if(is_array($left_icons))
                {
                    $this->db->set('left_icon', $left_icons['file_name']);

                }
                else{
                    $this->db->set('left_icon', $left_icons);

                }
            }


            $this->db->where('id', $value->id);
            $this->db->update('cms_slider');


            // echo $this->last_query();

            // die();

// }

        }

// die();
        // else{
        // 	die('found');
        // }
        return true;

    }





    public function getCmsFlashNews()
    {
        return 	$this->db->get('cms_flash_news')->row();
    }

    public function updateCmsFlashNews( $data)
    {


        extract($data);

        $this->db->set('home',  $home);
        $this->db->set('driver',  $driver);
        $this->db->set('partner',  $partner);
        $this->db->set('jobs',  $jobs);
        $this->db->where('id', 1);
        $this->db->update('cms_flash_news');


    }

    public function getCmsSeo()
    {
        $this->db->where('page_type', 'seo');

        return $this->db->get('cms_page_post')->row();
    }


    public function updateCmsSeo( $data, $user_id)
    {

        $result = $this->getCmsSeo();
        extract($data);


// var_dump($title);

// die();



        if(empty($result ))
        {


            if(isset($user_id))
            {
                $data['created_by'] =$user_id;
            }

            if(isset($title))
            {
                $data['title'] =$title;
            }
            $data['status'] =1;
            $data['page_type'] ='seo';


            if(isset($google_analytics))
            {
                $data['google_analytics'] =$google_analytics;
            }



            if(isset($facebook_ads))
            {
                $data['facebook_ads'] =$facebook_ads;
            }



            if(isset($microsoft_ads))
            {
                $data['microsoft_ads'] =$microsoft_ads;
            }



            if(isset($google_ads))
            {
                $data['google_ads'] =$google_ads;
            }


            $this->db->insert('cms_page_post', $data);

        }

        else{



            $this->db->set('title ',  $title );
            $this->db->set('meta_tag ',  $meta_tag );
            $this->db->set('meta_description ',  $meta_description );
            $this->db->set('google_analytics ',  $google_analytics );
            $this->db->set('microsoft_ads',  $microsoft_ads);
            $this->db->set('facebook_ads ',  $facebook_ads );
            $this->db->set('google_ads ',  $google_ads );
            $this->db->where('page_type', 'seo');
            $this->db->update('cms_page_post');


        }
        return true;
    }





    public function save_cms_page_listing($ref_data,$user_id,$image_name,$action, $linkUrl = "")
    {
        extract($ref_data);

// echo '<pre>';
// var_dump($ref_data );
// die();
// 
// 
        if((isset($price_type)) && $price_type=='description')
        {
            $p_price =0;
            $m_price =0;
            $n_price =0;
            $nw_price =0;
        }
// 

        if($action=='addNew')
        {
            if(isset($user_id))
            {
                $data['created_by'] =$user_id;
            }

            if(isset($name))
            {
                $data['name'] =$name;
            }
            if(isset($status))
            {
                $data['status'] =$status;
            }
            if(isset($category_id))
            {
                $data['category_id'] =$category_id;
            }

            if(isset($page_type))
            {
                $data['page_type'] =$page_type;
            }
            if(isset($title))
            {
                $data['title'] =$title;
            }
            if(isset($language))
            {
                $data['language'] =$language;
            }
            if(isset($news_location))
            {
                $data['news_location'] =$news_location;
            }

            if(isset($link_url))
            {
                $data['link_url'] =$link_url;
            }

            if(!empty($linkUrl))
            {
                $data['link_url'] = $linkUrl;
            }

            if(isset($link))
            {
                $data['link'] =$link;
            }
            if(isset($description))
            {
                $data['description'] =$description;
            }
            if(isset($meta_tag))
            {
                $data['meta_tag'] =$meta_tag;
            }
            if($image_name !=null)
            {
                $data['image'] =$image_name;
            }

            if(isset($short_description))
            {
                $data['short_description'] =$short_description;
            }

            if(isset($meta_description))
            {
                $data['meta_description'] =$meta_description;
            }

            if(isset($price_type))
            {
                $data['price_type'] =$price_type;
            }
            if(isset($p_price))
            {
                $data['p_price'] =$p_price;
            }


            if(isset($m_price))
            {
                $data['m_price'] =$m_price;
            }


            if(isset($n_price))
            {
                $data['n_price'] =$n_price;
            }


            if(isset($nw_price))
            {
                $data['nw_price'] =$nw_price;
            }
            if(isset($services_id) && $services_id!='')
            {
                $data['services_id'] =$services_id;
            }


            if(isset($model))
            {
                $data['model'] =$model;
            }


            if(isset($capacite1))
            {
                $data['capacite1'] =$capacite1;
            }


            if(isset($capacite2))
            {
                $data['capacite2'] =$capacite2;
            }


            if(isset($capacite1))
            {
                $data['capacite1'] =$capacite1;
            }


            if(isset($fuel_type))
            {
                $data['fuel_type'] =$fuel_type;
            }


            if(isset($climatisation))
            {
                $data['climatisation'] =$climatisation;
            }


            if(isset($wheelchairaccess))
            {
                $data['wheelchairaccess'] =$wheelchairaccess;
            }


            if(isset($gender))
            {
                $data['gender'] =$gender;
            }


            if(isset($zone_type))
            {
                $data['zone_type'] =$zone_type;
            }

            if(isset($is_category))
            {
                $data['is_category'] =$is_category;
            }

            if(isset($car_category))
            {
                $data['car_category'] =$car_category;
            }

            if(isset($passengers))
            {
                $data['passengers'] =json_encode($passengers);
            }



            if(isset($wheelchairs))
            {
                $data['wheelchairs'] =json_encode($wheelchairs);
            }



            if(isset($babyseats))
            {
                $data['babyseats'] =json_encode($babyseats);
            }



            if(isset($animals))
            {
                $data['animals'] =json_encode($animals);
            }



            if(isset($lugages))
            {
                $data['lugages'] =json_encode($lugages);
            }



            if(isset($country_id))
            {
                $data['country_id'] =($country_id);
            }

            if(isset($region_id))
            {
                $data['region_id'] =($region_id);
            }

            if(isset($cities_id))
            {
                $data['cities_id'] =($cities_id);
            }

            if(isset($start_from_price))
            {
                $data['start_from_price'] =($start_from_price);
            }
            else{
                $data['start_from_price'] =null;
            }

            if(isset($is_price_start_from))
            {
                $data['is_price_start_from'] =($is_price_start_from);
            }

            if(isset($rubens_promo))
            {
                $data['rubens_promo'] =($rubens_promo=='on')?1:0;
            }
            else{
                $data['rubens_promo']=0;
            }

            if(isset($rubens_new))
            {
                $data['rubens_new'] =($rubens_new=='on')?1:0;
            }
            else{
                $data['rubens_new']=0;
            }



            if($this->db->insert('cms_page_post', $data))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        if($action=='update')
        {


            if(isset($name))
            {
                $this->db->set('name', $name);
            }
            if(isset($status))
            {
                $this->db->set('status', $status);
            }
            if(isset($category_id))
            {
                $this->db->set('category_id', $category_id);
            }

            if(isset($page_type))
            {
                $this->db->set('page_type', $page_type);
            }
            if(isset($title))
            {
                $this->db->set('title', $title);
            }
            if(isset($language))
            {
                $this->db->set('language', $language);
            }
            if(isset($news_location))
            {
                $this->db->set('news_location', $news_location);
            }

            if(isset($link))
            {
                $this->db->set('link', $link);
            }

            if(isset($link_url))
            {
                $this->db->set('link_url', $link_url);
            }

            if(!empty($linkUrl))
            {
                $this->db->set('link_url', $linkUrl);
            }

            if(isset($description))
            {
                $this->db->set('description', $description);
            }
            if(isset($meta_tag))
            {
                $this->db->set('meta_tag', $meta_tag);
            }
            if($image_name !=null)
            {
                $this->db->set('image', $image_name);
            }

            if(isset($short_description))
            {
                $this->db->set('short_description', $short_description);
            }


            if(isset($meta_description))
            {
                $this->db->set('meta_description', $meta_description);
            }



            if(isset($p_price))
            {
                $this->db->set('p_price', $p_price);

            }


            if(isset($m_price))
            {
                $this->db->set('m_price', $m_price);

            }


            if(isset($n_price))
            {
                $this->db->set('n_price', $n_price);

            }


            if(isset($nw_price))
            {
                $this->db->set('nw_price', $nw_price);

            }

            if(isset($services_id) && $services_id!='')
            {
                $this->db->set('services_id', $services_id);

            }
            if(isset($model))
            {
                $this->db->set('model', $model);

            }
            if(isset($capacite1))
            {
                $this->db->set('capacite1', $capacite1);

            }
            if(isset($capacite2))
            {
                $this->db->set('capacite2', $capacite2);

            }
            if(isset($capacite3))
            {
                $this->db->set('capacite3', $capacite3);

            }
            if(isset($capacite1))
            {
                $this->db->set('capacite1', $capacite1);

            }
            if(isset($fuel_type))
            {
                $this->db->set('fuel_type', $fuel_type);

            }
            if(isset($climatisation))
            {
                $this->db->set('climatisation', $climatisation);

            }

            if(isset($wheelchairaccess))
            {
                $this->db->set('wheelchairaccess', $wheelchairaccess);

            }

            if(isset($gender))
            {
                $this->db->set('gender', $gender);

            }

            if(isset($is_category))
            {
                $this->db->set('is_category', $is_category);

            }


            if(isset($zone_type))
            {
                $this->db->set('zone_type', $zone_type);

            }


            if(isset($car_category))
            {
                $this->db->set('car_category', $car_category);

            }

            if(isset($passengers))
            {
                $this->db->set('passengers', json_encode($passengers));

            }


            if(isset($wheelchairs))
            {
                $this->db->set('wheelchairs', json_encode($wheelchairs));

            }


            if(isset($babyseats))
            {
                $this->db->set('babyseats', json_encode($babyseats));

            }


            if(isset($animals))
            {
                $this->db->set('animals', json_encode($animals));

            }


            if(isset($lugages))
            {
                $this->db->set('lugages', json_encode($lugages));

            }


            if(isset($country_id))
            {
                $this->db->set('country_id', $country_id);
            }

            if(isset($region_id))
            {
                $this->db->set('region_id', $region_id);
            }

            if(isset($cities_id))
            {
                $this->db->set('cities_id', $cities_id);
            }

            if(isset($rubens_new))
            {
                $this->db->set('rubens_new', ($rubens_new=='on')?1:0);
            }
            else{
                $this->db->set('rubens_new', 0);

            }

            if(isset($rubens_promo))
            {
                $this->db->set('rubens_promo',($rubens_promo=='on')?1:0);
            }
            else{
                $this->db->set('rubens_promo', 0);

            }

            if(isset($is_price_start_from))
            {
                $this->db->set('is_price_start_from', $is_price_start_from);
            }

            if(isset($start_from_price))
            {
                $this->db->set('start_from_price', $start_from_price);
            }
            else{
                $this->db->set('start_from_price', null);

            }

            if(isset($price_type))
            {
                $this->db->set('price_type', $price_type);
            }



            $date = date('Y-m-d H:i:s');
            $this->db->set('modification_date', $date );





            $this->db->where('id', $id);

            if($this->db->update('cms_page_post'))
            {
                return true;
            }
            else
            {
                //echo $this->db->last_query() . "<br/>";
                return false;
            }

        }
    }




    public function getCmsList_by_status($post_type,$status,$news_location = ""){
        // Duplicate column user.gender alias given as useR_gener on 17-July-2020 by Saravanan.R
        $this->db->select('cms_page_post.*,cpp.title AS package_title,cpp.link_url AS category_link,cpp.name AS category_name, users.username,users.first_name,users.last_name,users.gender as user_gender,users.civility');
        $this->db->from('cms_page_post');
        if($post_type=='prices')
        {
            $this->db->join('cms_page_post AS cpp', 'cms_page_post.services_id=cpp.id', 'left');
        }
        else if($post_type=='fleet')
        {
            $this->db->join('cms_page_post AS cpp', 'cms_page_post.car_category=cpp.id', 'left');
        }
        else{
            $this->db->join('cms_page_post AS cpp', 'cms_page_post.category_id=cpp.id', 'left');
        }


        $this->db->join('users', 'cms_page_post.created_by=users.id', 'left');
        $this->db->where('cms_page_post.page_type',$post_type);
        $this->db->where('cms_page_post.status',$status);

        if(!empty($news_location)){
            if($news_location != "blog")
                $this->db->where('cms_page_post.news_location',$news_location);
            else{
                $this->db->where("(`vbs_cms_page_post`.`news_location`='blog' OR `vbs_cms_page_post`.`news_location`='home' OR `vbs_cms_page_post`.`news_location` IS NULL)", null, false);
            }
        }

        $query = $this->db->get();

        //echo $this->db->last_query() . "<br/>";

        return $query->result();
    }


    public function getZonesList_by_status($post_type,$status){
        $this->db->select('cms_page_post.*,cpp.title AS package_title,cpp.name AS category_name, users.username,country.name as country, regions.name as region, cities.name as city ');
        $this->db->from('cms_page_post');
        $this->db->join('cms_page_post AS cpp', 'cms_page_post.category_id=cpp.id', 'left');
        $this->db->join('countries AS country', 'cms_page_post.country_id=country.id', 'left');
        $this->db->join('vbs_regions AS regions', 'cms_page_post.region_id=regions.id', 'left');
        $this->db->join('vbs_cities AS cities', 'cms_page_post.cities_id=cities.id', 'left');


        $this->db->join('users', 'cms_page_post.created_by=users.id', 'left');
        $this->db->where('cms_page_post.page_type',$post_type);
        $this->db->where('cms_page_post.status',$status);

        $query = $this->db->get();

// echo $this->db->last_query();

// die();
        return $query->result();
    }



    public function getCmsList_by_id($post_type,$id){
        $this->db->select('cms_page_post.*,cpp.name AS category_name, users.username');
        $this->db->from('cms_page_post');
        $this->db->join('cms_page_post AS cpp', 'cms_page_post.category_id=cpp.id', 'left');
        $this->db->join('users', 'cms_page_post.created_by=users.id', 'left');
        $this->db->where('cms_page_post.page_type',$post_type);
        $this->db->where('cms_page_post.status',1);

        $this->db->where('cms_page_post.id',$id);

        $query = $this->db->get();

        return $query->result();
    }



    public function ajax_get_region_listing($input){
        extract($input);

        $this->db->where('country_id',$country_id);

        $query = $this->db->get("regions");

        return $query->result();
    }


    public function ajax_get_cities_listing($input){
        extract($input);

        $this->db->where('country_id',$country_id);
        $this->db->where('region_id',$region_id);

        $query = $this->db->get("cities");

        return $query->result();
    }

    public function getCmsList($post_type){
        $this->db->select('cms_page_post.*,cpp.name AS category_name, users.username,users.gender,users.civility');
        $this->db->from('cms_page_post');
        $this->db->join('cms_page_post AS cpp', 'cms_page_post.category_id=cpp.id', 'left');
        $this->db->join('users', 'cms_page_post.created_by=users.id', 'left');
        $this->db->where('cms_page_post.page_type',$post_type);

        $query = $this->db->get();

        return $query->result();
    }


    public function getCmsListing_id($data,$lang=null){
        extract($data);

        $this->db->select('cms_page_post.*,cpp.link_url AS category_link,cpp.name AS category_name, users.username, users.gender, users.civility');
        $this->db->from('cms_page_post');
        $this->db->join('cms_page_post as cpp', 'cms_page_post.category_id=cpp.id', 'left');
        $this->db->join('users', 'cms_page_post.created_by=users.id', 'left');

        if($lang!=null){
            $this->db->where('cms_page_post.language',$lang);
        }

        if(isset($news_location)){
            $this->db->where('cms_page_post.news_location',$news_location);
        }

        if(isset($post_type)){
            $this->db->where('cms_page_post.page_type', $post_type);
        }

        if(isset($status)){
            $this->db->where('cms_page_post.status', $status);
        }

        if(isset($cat_id)){
            $this->db->where('cms_page_post.category_id',$cat_id);
        }

        if(isset($id) || isset($link_url)){
            if(isset($link_url))
                $this->db->where('cms_page_post.link_url',$link_url);
            else
                $this->db->where('cms_page_post.id',$id);
            $query = $this->db->get();
// echo $this->db->last_query();
            return $query->row();
        } else {
            $query = $this->db->get();
            return $query->result();
        }
    }




    public function delete_cms_page_listing($id)
    {
        $this->db->where('id', $id);


        if($this->db->delete('cms_page_post'))
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public function getCMSPage($post_type,$page_name,$lang='en') {
        $result = new stdClass();
        $this->db->select('cms_page_post.*,cpp.title AS package_title,cpp.name AS category_name, users.username,users.first_name,users.last_name,users.gender,users.civility');
        $this->db->from('cms_page_post');
        $this->db->join('cms_page_post AS cpp', 'cms_page_post.category_id=cpp.id', 'left');
        $this->db->join('users', 'cms_page_post.created_by=users.id', 'left');
        $this->db->where('cms_page_post.page_type',$post_type);
        $this->db->where('cms_page_post.title',$page_name);
        $this->db->where('cms_page_post.language',$lang);
        $this->db->where('cms_page_post.status',1);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $page_result = $query->result();
            $result = $page_result[0];
        }
        else {
            $result->title = $page_name;
            $result->description = "Yet to update content";
        }

        return $result;
    }




}