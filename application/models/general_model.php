<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class General_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}
	function add($table_name,$data)
	{
		$this->db->insert($table_name, $data);
 		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}
	function update_where($table_name,$data,$where)
	{
		$this->db->where($where);
     	$result = $this->db->update($table_name, $data);
     	echo $this->db->last_query();exit;
   		return  $this->db->affected_rows();
	}
	function update($table_name,$data,$db_field,$value)
	{
		$this->db->where($db_field,$value);
     	$result = $this->db->update($table_name, $data);
   		return  $result;
	}
	function update_all_records($table_name,$data){
		$result = $this->db->update($table_name, $data);
   		return  $result;
	}
	function get_column($table,$where,$column){
		$this->db->select($column);
		$this->db->where($where);
		$this->db->from($table);
		$result = $this->db->get();
		$result = $result->result_array();
		return $result[0][$column];
	}
	function get_record($table,$db_field,$value){
		$this->db->where($db_field,$value);
		$this->db->from($table);
		$result = $this->db->get();
		return $result->result_array();
	}
	function get_bywhere($table,$where){
		$this->db->where($where);
		$this->db->from($table);
		$result = $this->db->get();
		return $result->num_rows();
	}
	function get_bywhere_array($table,$where){
		$this->db->where($where);
		$this->db->from($table);
		$result = $this->db->get();
		return $result->result_array();
	}
	function getGraders(){
		$query = $this->db->query('select name from admin_user where project='.$this->project.' and role = 2');
		return $query->result_array();
	}
	function delete_by_where($table_name,$where){
		$this->db->where($where);
		$this->db->delete($table_name);
	}
	function delete($table_name,$db_field,$value)
	{
		$this->db->where($db_field,$value);
		$this->db->delete($table_name);
	}
	function get($table_name)
	{
		$query = $this->db->select('*')->from($table_name)->get();
		return $query->result_array();
	}
    public function all_files()
    {
        $language = array(
            "auth_lang.php" => "./application/",
            "dvbs_lang.php" => "./application/",
            "general_lang.php" => "./application/",
            "ion_auth_lang.php" => "./application/",
            "modules_lang.php" => "./application/",
            "valid_lang.php" => "./application/",
        );
        return $language;
    }
}