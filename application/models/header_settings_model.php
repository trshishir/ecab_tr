<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class header_settings_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    public static $table = "header_settings";
    
    public function getHeaderSettings() {
        $query = $this->db->select("chat_status,active_by")
                ->from(self::$table . " hs")->get();

        return $query->num_rows() > 0 ? $query->row_array() : false;
    }
}
    