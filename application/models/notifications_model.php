<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Notifications_Model extends CI_Model {

    function __construct(){
        parent::__construct();
    }


    public static $table = "notifications";


    public function get($where = []){
        $query = $this->db->get_where(self::$table, $where);
        return $query->num_rows() > 0 ? $query->row() : false;
    }

    public function getAll($where = [], $limit = false){
        if(!empty($where)) $this->db->where($where);
        if($limit != false) $this->db->limit($limit);
        $this->db->order_by('id','desc');
        $query = $this->db->get(self::$table);
        return $query->num_rows() > 0 ? $query->result() : false;
    }

    public function create($data){
        $this->db->trans_begin();
        $this->db->where('status',$data['status']);
        $this->db->where('department',$data['department']);
        $q = $this->db->get(self::$table);
        if ( $q->num_rows() == 0 )
        {
            $this->db->insert(self::$table, $data);
            $insert_id = $this->db->insert_id();
        }else{
            return 0;
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return $insert_id;
        }
    }

    public function update($data, $id){
        //code by s_a
        $this->db->trans_begin();
        $this->db->where('status',$data['status']);
        $this->db->where('department',$data['department']);
        $this->db->where('id !=',$id);
        $q = $this->db->get(self::$table);
        if( $q->num_rows() == 0 )
        {
           $q=$this->db->update(self::$table, $data, ['id' => $id]);
        }else{
           return 0;
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
        //code by s_a
       /* if($this->db->update(self::$table, $data, ['id' => $id]))
            return true;
        else
            return false;*/
    }

    public function delete($id){
        return $this->db->delete(self::$table, ['id' => $id]);
    }


    public static function replaceNotificationData($data, $message = "", $prefix = "", $fullReplace = []){

        $find = $replace = [];

        foreach ($fullReplace as $key => $val){
            $find[] = $key;
            $replace[] = $val;
        }

        foreach ($data as $key => $val){
            $find[] = "{" . $prefix . "_" . $key . "}";
            $replace[] = $val;
        }

        return str_replace($find, $replace, $message);
    }


    public static function addCompanyDetails($company_data, $message = ""){
        $message .= '<div class="row section-company-info" ' .
            'style=" background: -webkit-linear-gradient(#efefef, #ECECEC, #CECECE);' .
            'margin: 10px 0px;max-height: 600px;border: 2px solid #a4a8ab;padding:10px;">
                        <div class="col-md-5">
                            <div class="text_company">';

        if(isset($company_data['name']) && !empty($company_data['name'])){
            $message .= '<p><span>'.$company_data["name"].'</span></p>';
        }
        if(isset($company_data['email']) && !empty($company_data['email'])){
            $message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Email:</span><span>'.$company_data['email'].'</span></p>';
        }
        if(isset($company_data['phone']) && !empty($company_data['phone'])){
            $message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Phone:</span><span>'.$company_data['phone'].'</span></p>';
        }
        if(isset($company_data['fax']) && !empty($company_data['fax'])){
            $message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Fax:</span><span>'.$company_data['fax'].'</span></p>';
        }
        if(isset($company_data['website']) && !empty($company_data['website'])){
            $message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Website:</span><span>'.$company_data['website'].'</span></p>';
        }
        if(isset($company_data['city']) && !empty($company_data['city'])){
            $message .= '<p><span style="font-weight: 600;font-size: 16px;width: 60px;display: inline-block;">Address:</span><span>'.$company_data['city'].' '.$company_data['country'].'</span></p>';
        }
        $message .= '<p class="social_icons">';
        if(isset($company_data['facebook_link']) && !empty($company_data['facebook_link'])){
            $message .= '<a style="font-size: 12px;display: inline-block;height: 25px;width: 25px;background: #fff;border-radius: 50%;text-align: center;line-height: 25px;margin-right: 15px;" href="'.$company_data['facebook_link'].'" target="_blank"><i class="fa fa-facebook"></i></a>';
        }
        if(isset($company_data['youtube_link']) && !empty($company_data['youtube_link'])){
            $message .= '<a style="font-size: 12px;display: inline-block;height: 25px;width: 25px;background: #fff;border-radius: 50%;text-align: center;line-height: 25px;margin-right: 15px;" href="'.$company_data['youtube_link'].'" target="_blank"><i class="fa fa-youtube"></i></a>';
        }
        if(isset($company_data['instagram_link']) && !empty($company_data['instagram_link'])){
            $message .= '<a style="font-size: 12px;display: inline-block;height: 25px;width: 25px;background: #fff;border-radius: 50%;text-align: center;line-height: 25px;margin-right: 15px;" href="'.$company_data['instagram_link'].'" target="_blank"><i class="fa fa-instagram"></i></a>';
        }
        $message .= '</p>
                        </div>
                            </div>
                            <div class="col-md-7" style="margin-top: 15px;">
                                <div class="profile_image">';
        $message .= '<a href="" class="company_image"><img style="width: 100%;max-width: 300px;max-height: 370px;"';
        if(isset($company_data['logo']) && !empty($company_data['logo'])){
            $message .= 'src="'.base_url('uploads/company').'/'.$company_data['logo'].'"';
        }
        $message .= 'alt=""></a>';
        $message .= '</div>
                            </div>
                        </div>';
        return $message;
    }

    function notification_validate($cvReq = true){
        $reqInputs = [
            'notification_status'      => 'Notification Status',
            'name'      => 'Name',
            'status'      => 'Status',
            'department'      => 'Department',
            'subject'      => 'Subject',
        ];

        $error = [];
        foreach($reqInputs as $key => $input){
            $check = !isset($_POST[$key]) || $_POST[$key] == "" || empty($_POST[$key]);
            if($check && !in_array($key, ['message','letter','cv']) ){
                $error[] = "Merci de compléter toutes les cases correctement.$key";
            }
            if($key == "email" AND !filter_var($_POST[$key], FILTER_VALIDATE_EMAIL)){
                $error[] = "S'il vous plaît entrer un email valide.";
            }
            if($key == "tel" AND !preg_match('/^0[0123456790]\d{8}$/', $_POST[$key])){
                $error[] = "Veuillez entrer un numéro de téléphone valide avec 10 chiffres commençant par 0.";
            }
            if($key == "dob" AND !valid_date($_POST[$key])){
                $error[] = "Veuillez entrer une date valide";
            } elseif($key == "dob" AND calculateAge(to_unix_date($_POST[$key])) < 18){
                $error[] = "l'âge doit être supérieur à 18 ans";
            }
            if($key == "cv" AND (!isset($_FILES['cv']) || empty($_FILES['cv']['name'])))
                $error[] = "veuillez joindre cv.";
        }
        return $error;
    }
public function getuser($id){
        $where =array('id'=> $id);
        $table='vbs_users';
        $query = $this->db->get_where($table, $where);
         if ($query->num_rows() > 0) {
            $query=  $query->row();
        return  $query = 'Mr. '. $query->first_name .' '.$query->last_name;
        }
        else {
            return false;
        }
    }
}