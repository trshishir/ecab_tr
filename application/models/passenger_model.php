<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Passenger_Model extends CI_Model {

	function __construct(){
		parent::__construct();
	}
	public function getpassengers($clientid){
	
	$query=$this->db->where(['clientid'=>$clientid])->get('vbs_passengers');
	return $query->num_rows() > 0 ? $query->result() : false;
}
public function passengers_Add($table,$data){
		// $table = "vbs_u_passenger";
		$this->db->trans_begin();
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}
	public function passengers_Update($table,$data, $id){
		// $table = "vbs_u_passenger";
		if($this->db->update($table, $data, ['id' => $id]))
			return true;
		else
			return false;
	}
	public function passengers_Delete($table,$id){
		return $this->db->delete($table, ['id' => $id]);
	}
 public function getpassengerrecord($id){
 	  return $this->db
        ->where(["id"=>$id])
		->get('vbs_passengers')
		->row();
	
	
}
public function getuser($id){
		$where =array('id'=> $id);
		$table='vbs_users';
		$query = $this->db->get_where($table, $where);
		 if ($query->num_rows() > 0) {
			$query=  $query->row();
		return	$query = $query->civility.' '. $query->first_name .' '.$query->last_name;
		}
		else {
			return false;
		}
	}
public function getdisablecategories($table){
  return $this->db->get($table)->result();
}
public function getdiscategory($table,$id){
 return $this->db
        ->where(["id"=>$id])
		->get($table)
		->row()->name_of_disablecategory;
}
}
