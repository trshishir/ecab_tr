<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Pipline_Model extends CI_Model {

	function __construct(){
		parent::__construct();
	}


	public static $table = "piplines";


	public function get($where = []){
		$query = $this->db->get_where(self::$table, $where);
		return $query->num_rows() > 0 ? $query->row() : false;
	}

	public function getNotes($where = []){
		$query = $this->db->get_where("vbs_piplines_notes", $where);
		return $query->num_rows() > 0 ? $query->result() : false;
	}

	public function getList($where = []){
		$query = $this->db->get_where("vbs_piplines_list", $where);
		return $query->num_rows() > 0 ? $query->result() : false;
	}

	public function getAllOperators($where = [], $limit = false){
		$this->db->select("users.id, users.first_name, users.last_name")
			->where("role_id", 10)
			->from("vbs_users users");
		$query = $this->db->get();
		return $query->num_rows() > 0 ? $query->result_array() : false;
	}

	public function getAllQuotes($where = [], $limit = false){
		$this->db->select("request.id, request.status, request.created_at, request.last_action")
			->from("vbs_request request");
		$query = $this->db->get();
		return $query->num_rows() > 0 ? $query->result_array() : false;
	}

	public function getAll($where = [], $limit = false){
		$query = $this->db->select("pipline.*, user.first_name as user_first_name, user.last_name as user_last_name, (select note.quote_id from vbs_piplines_notes note where note.pipline_id = pipline.id order by note.id desc limit 1) as qt_id, (select note.quote_status from vbs_piplines_notes note where note.pipline_id = pipline.id order by note.id desc limit 1) as st, (select note.quote_date from vbs_piplines_notes note where note.pipline_id = pipline.id order by note.id desc limit 1) as dt")
			->from(self::$table . " pipline")
			->join("users user","pipline.sales_operator = user.id","left")
			->where($where)->get();
		return $query->num_rows() > 0 ? $query->result() : false;
	}
    
	public function create($data){
		$this->db->trans_begin();
		$this->db->insert(self::$table, $data);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}

	public function createQuoteDetails($data, $table){
		$this->db->trans_begin();
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}

	public function update($data, $id){
		if($this->db->update(self::$table, $data, ['id' => $id]))
			return true;
		else
			return false;
	}

	public function delete($id){
		return $this->db->delete(self::$table, ['id' => $id]);
	}

	public function delete1($id, $table){
		return $this->db->delete($table, ['pipline_id' => $id]);
	}

	public function delete_data($array){
		$this->db->where_in('id', $array);
		return $this->db->delete(self::$table);
	}

	function pipline_validate($cvReq = true){
		$reqInputs = [
			'status'      => 'Status',
			'civility'    => 'Civility',
			'first_name'      => 'First Name',
			'last_name'       => 'Last Name',
			'company'      => 'Company',
			'phone'      => 'Phone',
			'fax'      => 'Fax',
			'email'      => 'Email',
			'sales_operator'      => 'Sales Operator',
			'address'      => 'Address',
			'zip_code'      => 'Zip Code',
			'city'      => 'City',
		];
	
		$error = [];
		foreach($reqInputs as $key => $input){
			$check = !isset($_POST[$key]) || $_POST[$key] == "" || empty($_POST[$key]);
			if($check && !in_array($key, ['message','letter','cv']) ){
				$error[] = "Merci de compléter toutes les cases correctement.$key";
			}
			if($key == "email" AND !filter_var($_POST[$key], FILTER_VALIDATE_EMAIL)){
				$error[] = "S'il vous plaît entrer un email valide.";
			}
			if($key == "tel" AND !preg_match('/^0[0123456790]\d{8}$/', $_POST[$key])){
				$error[] = "Veuillez entrer un numéro de téléphone valide avec 10 chiffres commençant par 0.";
			}
			if($key == "dob" AND !valid_date($_POST[$key])){
				$error[] = "Veuillez entrer une date valide";
			} elseif($key == "dob" AND calculateAge(to_unix_date($_POST[$key])) < 18){
				$error[] = "l'âge doit être supérieur à 18 ans";
			}
			if($key == "cv" AND (!isset($_FILES['cv']) || empty($_FILES['cv']['name'])))
				$error[] = "veuillez joindre cv.";
		}
		return $error;
	}

}