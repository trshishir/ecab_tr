<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Language_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	public static $table = "languages";
	public static $trans_table = "lang_token";
	public static $lang_trans = "lang_trans";

    public function language_create($data){
		$this->db->trans_begin();
		
		$this->db->insert(self::$table, $data);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}
	public function translation_create($data){
		$this->db->trans_begin();
		$dataid = $this->db->query("SELECT id FROM vbs_lang_token where description = '".$data['token_id']."'");
		$lang_id = $dataid->result_array();
		if(isset($lang_id[0]['id']) && $lang_id[0]['id'] != ''){
		    $newdata=array('lang_id'=>$data['lang'],
                'token_id'=> $lang_id[0]['id'],
                'translation'=>$data['translation'],
               );
		}
		else{
		    $transdata=array('lang'=>$data['lang'],
                'description'=> $data['token_id'],
               ); 
		    $this->db->insert(self::$trans_table, $transdata);
		    $insert_id = $this->db->insert_id();
		    $newdata=array('lang_id'=>$data['lang'],
                'token_id'=> $insert_id,
                'translation'=>$data['translation'],
               );
		}
// 		$newdata=array('lang_id'=>$data['lang'],
//                 'description'=> $lang_id[0]['id'],
//                 'token'=>$data['translation'],
//               );
		$this->db->insert(self::$lang_trans, $newdata);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}
	public function taran_inner_create($data){
		$this->db->trans_begin();
		
		$this->db->insert(self::$lang_trans, $data);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}

    public function get_lang()
    {        if ($this->session->userdata('lang')) {
        return $this->session->userdata('lang');
    } else {
        $en='english';
        return $en;
    }
    }


    public function all_files()
    {
        $language = array(
            "auth_lang.php" => "./application/",
            "dvbs_lang.php" => "./application/",
            "general_lang.php" => "./application/",
            "ion_auth_lang.php" => "./application/",
            "modules_lang.php" => "./application/",
            "valid_lang.php" => "./application/",
        );
        return $language;
    }
    public function update_default_language()
    {
        extract($_POST);
        $all_default_status_language = $this->db->query('update vbs_languages set defult = "no"');
        $set_default_language = $this->db->query('update vbs_languages set defult = "yes" where id = "'.$id.'"');
        return  $this->db->affected_rows();
    }

}