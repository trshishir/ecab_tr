<?php if (! defined('BASEPATH')) exit('No direct script access allowed');



class documents_model extends CI_Model



{

    function __construct(){

        parent::__construct();

    }


    function savedocument($documents_data)
	{
        // echo "We're in here model of documents<br />";
        // print_r($documents_data);
        // exit;
        if ($this->db->insert('documents', $documents_data)) {
            return true;
        } else {
            return false;
        }

    	// $query="insert into vbs_client values('$status','$civilite','$nom','$pre_nom','$company','$address1','$address2','$email','$phone','$mobile','$fax','$code_postal','$ville'
    	//                                         ,'$client_type','$birthday','$disabled','$payment_method','$payment_delay','$note')";
    	// $this->db->query($query);
	}
    function updatedocument($documents_data,$did)
    {
        // echo "We're in here model of documents<br />";
        // print_r($documents_data);
        // exit;
        // $this->db->update('documents', $documents_data, array('id' => $did));
        if ($this->db->update('documents', $documents_data, array('id' => $did))) {
            return true;
        } else {
            return false;
        }
    }
    function savelibrarytemplate($documents_data)
    {
        // echo "We're in here model of documents<br />";
        // print_r($documents_data);
        // exit;
        if ($this->db->insert('library_templates', $documents_data)) {
            return true;
        } else {
            return false;
        }
    }
    function updatelibrarytemplate($documents_data)
    {
        // echo "We're in here model of documents<br />";
        // print_r($documents_data);
        // exit;
        if ($this->db->update('library_templates', $documents_data)) {
            return true;
        } else {
            return false;
        }
    }

    function getAllDocuments()
    {
        // echo "We're in here model of documents<br />";
        // print_r($documents_data);
        // exit;
        $query = $this->db->select("*,documents.id AS did,documents.doc_category as doccategory,documents.city as doccity")
                ->join("users ud", "ud.id = documents.username", "INNER")
                ->join("groups g", "g.id = documents.usergroup", "INNER")
                ->join("document_category c", "c.id = documents.doc_category", "INNER")
                // ->join("document_cities ci", "ci.id = documents.city", "INNER")
                // ->join("document_signature s", "s.id = documents.signature", "INNER")
                // ->join("document_status st", "st.id = documents.doc_status", "INNER")
                ->join("document_docs d", "d.id = documents.doc_title", "INNER")
                // ->join("document_departments d1", "d1.id = documents.department", "INNER")
                // ->join("document_managers m", "m.id = documents.manager", "INNER")
                ->order_by('documents.id', 'asc')->get('documents');

        // print_r($this->db->error());
        // exit;

        return $query->num_rows() > 0 ? $query->result_array() : false;

        // $q = $this->db->get('products');
        // if ($q->num_rows() > 0) {
        //     foreach (($q->result()) as $row) {
        //         $data[] = $row;
        //     }
        //     return $data;
        // }
        // return FALSE;

        // $query="insert into vbs_client values('$status','$civilite','$nom','$pre_nom','$company','$address1','$address2','$email','$phone','$mobile','$fax','$code_postal','$ville'
        //                                         ,'$client_type','$birthday','$disabled','$payment_method','$payment_delay','$note')";
        // $this->db->query($query);
    }

    function getAllLibraryTemplates()
    {
        // echo "We're in here model of documents<br />";
        // print_r($documents_data);
        // exit;
        $query = $this->db->select("*,library_templates.id AS lid")
                ->join("users ud", "ud.id = library_templates.added_by", "INNER")
                ->join("document_category c", "c.id = library_templates.lib_category", "INNER")
                ->join("document_status st", "st.id = library_templates.lib_status", "INNER")
                ->join("document_destination d1", "d1.id = library_templates.lib_destination", "INNER")
                ->order_by('library_templates.id', 'asc')->get('library_templates');

        // print_r($this->db->error());
        // exit;

        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    function getSpecificDocument($id)
    {
        // echo "We're in here model of documents<br />";
        // print_r($documents_data);
        // exit;
        $query = $this->db->select("*,documents.id AS did")
                ->join("users ud", "ud.id = documents.added_by", "INNER")
                ->where('documents.id',$id)
                ->order_by('documents.id', 'desc')->get('documents');

        // if ($query->num_rows() > 0) {
        //     foreach (($query->result()) as $row) {
        //         $data[] = $row;
        //     }
        //     return $data;
        // }
        // return FALSE;
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    function getSpecificUsers($id)
    {
        // echo "We're in here model of documents<br />";
        // print_r($documents_data);
        // exit;
        $query = $this->db->select("*")
                ->join("users ud", "ud.id = users_groups.user_id", "INNER")
                ->where('users_groups.group_id',$id)
                ->order_by('users_groups.id', 'asc')->get('users_groups');

        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
        // return $query->num_rows() > 0 ? $query->result_array() : false;
    }
	
	
    // public function getdata($TableName){
    //     $query = $this->db->get('vbs_client');
    //     return $query->result();
    // }


    // public function insertData($TableName,$values,$column,$parameter){

    //     $this->db->select('*');

    //     $this->db->from($TableName);

    //     $this->db->where($column,$parameter);

    //     $query = $this->db->get();



    //     if($query->num_rows() == 0){

    //         return  $this->db->insert($TableName,$values);

    //     }

    // }



    // public function getStaut($TableName,$Column,$Parameter,$statut,$from,$to,$column){

    //     $this->db->select('*');

    //     $this->db->from($TableName);

    //     if($statut != ""){

    //         $this->db->like($column,$statut);

    //     }elseif($from != "" && $to != ""){

    //         $this->db->where('create_date >=', $from .'  '. date('h:i:s'));

    //         $this->db->where('create_date <=', $to .'  '. date('h:i:s'));

    //     }

    //     $this->db->where($Column,$Parameter);

    //     $query = $this->db->get();



    //     return $query->result();

    // }





    //  public function updateStatut($id,$statut,$values,$TableName,$column){

    //     $this->db->select('*');

    //     $this->db->from($TableName);

    //     $this->db->where($column,$statut);

    //      $this->db->where('is_delete','1');

    //     $query = $this->db->get();



    //     if($query->num_rows() > 0){

    //         return false;

    //     }else{

    //         $this->db->where('id',$id);

    //         return $this->db->update($TableName,$values);

    //     }

    //  }







    //  public function deleteDelete($TableName,$id){

    //         $this->db->where('id',$id);

    //         return $this->db->delete($TableName);

    //  }



    //  public function deactive($values,$id,$TableName){

    //     $this->db->set($values);

    //     $this->db->where('id',$id);

    //     return $this->db->update($TableName);

    //  }



}

?>