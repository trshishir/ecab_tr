<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Comment_Model extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getAll($id) {
		$searchQuery = "select 
							comment.id,
							comment.comment,
							comment.first_name,
							comment.last_name,
							comment.date,
							comment.time,
							department.name as 'Dept Name',
							user.username as 'Username',
							file.file as 'Filename' 
						from vbs_comment comment 
						left join vbs_departments department on comment.department = department.id 
						left join vbs_users user on comment.user_id = user.id 
						left join vbs_filemanagement file on comment.file_id = file.id where file_id = " . $id;

		$query = $this->db->query($searchQuery);

        return $query->num_rows() > 0 ? $query->result() : false;
    }

	public function create($data) {
        print_r($this->db->insert("vbs_comment", $data));
    }

    public function delete($id) {
    	return $this->db->delete("vbs_comment", ['id' => $id]);
    }
}
