<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Cms_pages_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}


	public static $table = "cms_pages";

	public function get($where = []){
		$query = $this->db->get_where(self::$table, $where);
		return $query->num_rows() > 0 ? $query->row() : false;
	}
	
	public function getActiveAll(){
		$query = $this->db->get("cms_pages");
		return $query->result();
	}
	
	public function getjoinAll(){
		
		$this->db->select('a.page_type,a.name,a.message,a.id,a.status,a.meta_tags,b.title as page_type_name');     // select all columns
		$this->db->from('vbs_cms_pages as a'); // FROM table
		$this->db->join('vbs_pagetype as b', 'b.id = a.page_type','left'); // INNER JOIN
		$query = $this->db->get(); 
		return $query->result();
		
		//$query = $this->db->get("cms_pages");
		//return $query->result();
	}



	public function getAll($where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get(self::$table);
		return $query->num_rows() > 0 ? $query->result() : false;
	}

	public function create($data){
		$this->db->trans_begin();
		$this->db->insert(self::$table, $data);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}

	public function update($data, $id){
		if($this->db->update(self::$table, $data, ['id' => $id]))
			return true;
		else
			return false;
	}

	public function delete($id){
		return $this->db->delete(self::$table, ['id' => $id]);
	}


}