<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class google_api_model extends CI_Model {

    const table = "vbs_googleapi";
    function __construct() {

        parent::__construct();
    }

    public function getFirst(){
        $query = $this->db->select("g.*, c.name country, c.code as country_code")
            ->from(self::table . ' as g')
            ->join("vbs_countries as c", 'c.id = g.country_id','left')
            ->order_by('id','desc')
            ->get();
        return $query->num_rows() > 0 ? $query->row() : [];
    }
}
