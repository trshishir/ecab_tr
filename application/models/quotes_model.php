<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Quotes_Model extends CI_Model {

	function __construct(){
		parent::__construct();
	}
public function getquotesallrecords(){
	$confirm=1;
       $q=$this->db
	  ->select(['qt.id','qt.status as quotestatus','qt.created_at','qt.date as quotedate','qt.time as quotetime','qt.booking_id','booking.booktime','booking.pickzipcode','booking.pickcity','booking.dropzipcode','booking.dropcity','booking.booking_comment','booking.user_id','booking.booking_ref','booking.pick_date','booking.pick_time','booking.pick_point','booking.drop_point','booking.distance','booking.cost_of_journey','booking.price','booking.totalprice','booking.payment_received','booking.is_conformed','booking.bookdate','booking.waiting_time','booking.return_time','booking.return_date','booking.start_time','booking.start_date','booking.end_time','booking.end_date','booking.regular','booking.returncheck','booking.wheelchair','booking.pickupcategory','booking.dropoffcategory','booking.last_action','car.id as carid','car.car_cat_name as carname','method.id as payment_id','method.name as payment_name','service.id as servicecat_id',
	  	'service.category_name as servicecat_name','ser.id service_id','ser.service_name as service_name'])
	  ->from('vbs_quotes as qt')
	  ->join('vbs_bookings as booking','qt.booking_id=booking.id','left')
	  ->join('vbs_u_car_category as car', 'booking.car_id=car.id','left')
	  ->join('vbs_u_service as ser', 'booking.service_id=ser.id','left')
	  ->join('vbs_u_payment_method as method', 'booking.payment_method_id=method.id','left')
	  ->join('vbs_u_category_service as service', 'booking.service_category_id=service.id','left')
	  
	  ->order_by('qt.id', 'DESC')
	  ->get();
       return  $q->result();
}
public function getquotesallrecordsbysearch($where,$query,$otherquery){
        $query=urldecode($query);
         $otherquery=urldecode($otherquery);

       $this->db
	 ->select(['qt.id','qt.status as quotestatus','qt.created_at','qt.date as quotedate','qt.time as quotetime','qt.booking_id','booking.booktime','booking.pickzipcode','booking.pickcity','booking.dropzipcode','booking.dropcity','booking.booking_comment','booking.user_id','booking.booking_ref','booking.pick_date','booking.pick_time','booking.pick_point','booking.drop_point','booking.distance','booking.cost_of_journey','booking.price','booking.totalprice','booking.payment_received','booking.is_conformed','booking.bookdate','booking.waiting_time','booking.return_time','booking.return_date','booking.start_time','booking.start_date','booking.end_time','booking.end_date','booking.regular','booking.returncheck','booking.wheelchair','booking.pickupcategory','booking.dropoffcategory','booking.last_action','car.id as carid','car.car_cat_name as carname','method.id as payment_id','method.name as payment_name','service.id as servicecat_id',
	  	'service.category_name as servicecat_name','ser.id service_id','ser.service_name as service_name'])
	  ->from('vbs_quotes as qt')
	  ->join('vbs_bookings as booking','qt.booking_id=booking.id','left')
	  ->join('vbs_users as user', 'booking.user_id=user.id','left')
	  ->join('vbs_u_car_category as car', 'booking.car_id=car.id','left')
	  ->join('vbs_u_service as ser', 'booking.service_id=ser.id','left')
	  ->join('vbs_u_payment_method as method', 'booking.payment_method_id=method.id','left')
	  ->join('vbs_u_category_service as service', 'booking.service_category_id=service.id','left');
	  if(!empty($where)) $this->db->where($where); 
      if(!empty($query)){
        $this->db->like('user.username',$query);
      }  
      if(empty($query) &&  !empty($otherquery)){
      	 $this->db->like('qt.id',$otherquery);
      }
	  $q=$this->db->order_by('qt.id', 'DESC')->get();   
      return  $q->result();
}
public function deletequotedata($table,$where){
return $this->db->delete($table,$where);
}
public function getremindersnotification($table,$where){

	$query = $this->db->where($where)->limit(1)->get($table);
	return $query->num_rows() > 0 ? $query->row() : false;
 }
public function getsingleremindernotification($table,$where){
  return $this->db->where($where)->get($table)->row();
}
public function updaterecord($table,$data,$where){
$this->db->where($where)->update($table, $data);
return true;
}
public function addrecord($table,$data){
$this->db->insert($table, $data);
$insert_id = $this->db->insert_id();
return $insert_id;
}
public function deleterecord($table,$where){
return $this->db->delete($table,$where);
}
public function getsinglerecord($table,$where){
   $query = $this->db->where($where)->limit(1)->get($table);
	return $query->num_rows() > 0 ? $query->row() : false;
}
public function getmultiplerecord($table,$where){

    $query = $this->db->where($where)->get($table);
	return $query->num_rows() > 0 ? $query->result() : false;
}
public function getrecord($table){
	    $query = $this->db->get($table);
	   return $query->num_rows() > 0 ? $query->result() : false;
}
public function getquotenotification($where){

       $query=$this->db
	  ->select(['qtn.id','qtn.date','qtn.time','qtn.issent','qtn.quote_id','qtn.notification_id'])
	  ->from('vbs_quotes_notification as qtn')
	  ->join('vbs_notifications as nt','qtn.notification_id=nt.id','left')
	  ->where($where)
	  ->limit(1)
	  ->get();
      return $query->num_rows() > 0 ? $query->row() : false;
}
public function getquotereminders($where){

       $query=$this->db
	  ->select(['qtr.id','qtr.date','qtr.time','qtr.issent','qtr.quote_id','qtr.reminder_id','qtr.type'])
	  ->from('vbs_quotes_reminder as qtr')
	  ->join('vbs_reminders as rm','qtr.reminder_id=rm.id','left')
	  ->where($where)
	  ->get();
     	return $query->num_rows() > 0 ? $query->result() : false;
}
public function getquotesbyclientid($where){
  $query=$this->db
	  ->select(['bk.*','qr.id as quoteid','qr.date as quotedate','qr.time as quotetime'])
	  ->from('vbs_bookings as bk')
	  ->join('vbs_quotes as qr','bk.id = qr.booking_id')
	  ->where($where)
	  ->get();
     	return $query->num_rows() > 0 ? $query->result() : false;
}
public function totalquotesnumber(){
		$query = $this->db->get("vbs_quotes");
		return $query->num_rows();
	}
}
