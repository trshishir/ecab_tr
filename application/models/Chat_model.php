<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Chat_model extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->database();
	}


	function changechatingboxsetting($status)
	{
		// echo "Model";exit;
		$data = array("chat_status"=>$status,"active_by"=>0);
		$this->db->where("id",1);
		$this->db->update("vbs_header_settings",$data);
	}

	function getchatstatus()
	{
		return $this->db->get("vbs_header_settings")->row_array();
	}

	function insertbasicchatdetails($name,$email,$telephone,$ipaddress,$pagetimblad,$countryName)
	{
		$data = array("name"=>$name,"email"=>$email,"telephone"=>$telephone,"status"=>"active","user_type"=>"visitor","ip_address"=>$ipaddress,"dateandtime"=>date('y-m-d H:i:s'),"location"=>$countryName,"page_timblad"=>$pagetimblad);
		$this->db->select('*');
		$this->db->from('vbs_basic_chat_details');
		$this->db->where(array("name"=>$name,"email"=>$email,"telephone"=>$telephone));
		$q = $this->db->get()->row_array();
		if(empty($q))
		{
			$this->db->insert("vbs_basic_chat_details",$data);
			$userid = $this->db->insert_id();
			return array('status'=>0,'userid'=>$userid);
		}
		else
		{
			return array('status'=>1,'userid'=>$q['id']);
		}		
	}

	function insertchatmessagedatadetails($userid_from,$messagetext,$attachfile)
	{
		$data_update_active_status_arr = array("is_user_active"=>1);
		$this->db->where("id",$userid_from);
		$this->db->update("vbs_basic_chat_details",$data_update_active_status_arr);

		$data = array("userid_from"=>$userid_from,"userid_to"=>1,"message_text"=>$messagetext,"status"=>"pending","dateandtime"=>date('y-m-d H:i:s'),"attachfile"=>$attachfile);
		$this->db->insert('vbs_messages_history',$data);
		$last_msg_id = $this->db->insert_id();

		$this->db->where(array("id"=>$last_msg_id,"userid_from"=>$userid_from));
		return $this->db->get('vbs_messages_history')->row_array();

	}

	function getusermessages($userid)
	{
		$this->db->where("userid_from",$userid);
		return $this->db->get("vbs_messages_history")->result();
	}

	function getallnewmessageshistory()
	{
		$this->db->select('vbs_a.*,vbs_b.*');
		$this->db->group_by("vbs_a.userid_from");
		$this->db->join("vbs_basic_chat_details vbs_b","vbs_b.id=vbs_a.userid_from","INNER");
		$this->db->order_by("vbs_a.id","DESC");
		return $this->db->get("vbs_messages_history vbs_a")->result();
	}

	function getlast_message($userid)
	{
		$this->db->where(array("userid_from"=>$userid));
		$this->db->order_by("id","DESC");
		return $this->db->get("vbs_messages_history")->row_array();
	}

	function getcount_message($userid)
	{
		$this->db->where(array("userid_from"=>$userid,"status"=>"pending","type"=>0));
		return $this->db->get("vbs_messages_history")->num_rows();
	}

	function getallspecificusermessageshistory($userid)
	{
		$where = "(userid_from=$userid AND userid_to=1)";
		// echo $where;exit;
		// $this->db->where(array("userid_from"=>intval($userid),"userid_to"=>1));
		$this->db->where($where);
		return $this->db->get("vbs_messages_history")->result();
	}

	function updateallspecificusermessageshistory($userid)
	{
		$data = array("status"=>"read");
		$this->db->where(array("userid_from"=>$userid));
		$this->db->update("vbs_messages_history",$data);
	}

	function insertadminreplydata($userid,$messagetext,$attachfile)
	{
		$this->db->query('SET NAMES utf8mb4');

		$data_chat_sound_update = array("chat_sound_status"=>1);
		$this->db->where("id",$userid);
		$this->db->update("vbs_basic_chat_details",$data_chat_sound_update);

		$data = array("userid_from"=>$userid,"userid_to"=>1,"message_text"=>$messagetext,"status"=>"pending","dateandtime"=>date('y-m-d H:i:s'),"type"=>1,"attachfile"=>$attachfile);
		$this->db->insert('vbs_messages_history',$data);
		$last_msg_id = $this->db->insert_id();

		$this->db->where(array("id"=>$last_msg_id,"userid_from"=>$userid));
		return $this->db->get('vbs_messages_history')->row_array();
	}

	function updatesoundstatus()
	{
		$sound_data = array("is_chat_sound_status"=>1);
		$this->db->where("id",1);
		$this->db->update("vbs_chat_sound_status",$sound_data);
	}

	function getvisitorchathistory($userid)
	{
		$this->db->where(array("userid_from"=>$userid,"userid_to"=>1));
		return $this->db->get("vbs_messages_history")->result();
	}

	function getchat_sound_status()
	{
		return $this->db->get("vbs_chat_sound_status")->row_array();
	}

	function updatechatsound_val()
	{
		$data = array("is_chat_sound_status"=>0);
		$this->db->where("id",1);
		$this->db->update("vbs_chat_sound_status",$data);
	}

	function updatechat_open_status()
	{
		$data = array("is_chat_open"=>1);
        $this->db->where("id",1);
        $this->db->update("vbs_chat_sound_status",$data);
	}

	function updatechat_open_status_off()
	{
		$data = array("is_chat_open"=>0);
        $this->db->where("id",1);
        $this->db->update("vbs_chat_sound_status",$data);
	}

	function getallpendingmessagescount()
	{
		$this->db->where("status","pending");
		$this->db->where("type",0);
		return $this->db->get("vbs_messages_history")->num_rows();
	}

	function updatechat_open_status_on()
	{
		$data = array("is_chat_open"=>1);
        $this->db->where("id",1);
        $this->db->update("vbs_chat_sound_status",$data);
	}

	function getuserchatsoundstatus($userid)
	{
		$this->db->where("id",$userid);
		return $this->db->get("vbs_basic_chat_details")->row_array();
	}

	function updatesoundstatusdivdata($userid)
	{
		$data = array("chat_sound_status"=>0);
		$this->db->where("id",$userid);
		$this->db->update("vbs_basic_chat_details",$data);
	}

	function getusersnameandotherdata($userid)
	{
		$this->db->where("id",$userid);
		return $this->db->get("vbs_basic_chat_details")->row_array();
	}

	function getquickreplies()
	{
		return $this->db->get("vbs_quick_replies")->result();
	}

	function changetypinguserdtatus($userid)
	{
		$data = array("is_user_typing"=>1);
		$this->db->where("id",$userid);
		$this->db->update("vbs_basic_chat_details",$data);
	}

	function changetypinguserdtatus_off($userid)
	{
		$data = array("is_user_typing"=>0);
		$this->db->where("id",$userid);
		$this->db->update("vbs_basic_chat_details",$data);
	}

	function changeadmintypinguserdtatus($userid)
	{
		$data = array("is_admin_typing"=>1);
		$this->db->where("id",$userid);
		$this->db->update("vbs_basic_chat_details",$data);
	}

	function changeadmintypinguserdtatus_off($userid)
	{
		$data = array("is_admin_typing"=>0);
		$this->db->where("id",$userid);
		$this->db->update("vbs_basic_chat_details",$data);
	}

	function changeuserstatusactive($userid)
	{
		$data = array("is_user_active"=>2);
		$this->db->where("id",$userid);
		$this->db->update("vbs_basic_chat_details",$data);
	}

	function chageactivestatusdataofflinedata($userid)
	{
		$data = array("is_user_active"=>0);
		$this->db->where("id",$userid);
		$this->db->update("vbs_basic_chat_details",$data);
	}

	function likeamessage($msgid)
	{
		$data = array("is_like"=>1);
		$this->db->where("id",$msgid);
		$this->db->update("vbs_messages_history",$data);
	}

	function dislikeamessage($msgid)
	{
		$data = array("is_like"=>0);
		$this->db->where("id",$msgid);
		$this->db->update("vbs_messages_history",$data);
	}



}
?>