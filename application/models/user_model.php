<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public static $table = "users";
    public static $userDetails = "users_details";
    public static $resetPassword = "reset_password";

    public function get($where = []) {
        $query = $this->db->select("user.*,role.name as role,department.name as department")
                        ->from(self::$table . " user")
                        ->join("roles role", "role.id = user.role_id", "left")
                        ->join("departments department", "department.id = user.department_id", "left")
                        ->where($where)->get();
        return $query->num_rows() > 0 ? $query->row() : false;
    }
    
    public function getUserDetail($userID) {
        $query = $this->db->select("user.*,gp.name as group_name,ud.address1 as address1, ud.address2 as address2,ud.company_name as _company_name,ud.mobile_no as mobile_no, ud.fax_no as fax_no")
                ->from(self::$table . " user")
                ->join("users_details ud", "ud.user_id = user.id", "INNER")
                ->join("users_groups ug", "ug.user_id = user.id", "INNER")
                ->join("groups gp", "gp.id = ug.group_id", "INNER") 
                ->where(['user.id' => $userID])
                ->get();
        
        return $query->num_rows() > 0 ? $query->row() : false;
    }

    public function login($username, $password, $usernameCol = "email") {
        $query = $this->db->select("user.*,role.name as role,department.name as department")
                ->from(self::$table . " user")
                ->join("roles role", "role.id = user.role_id", "left")
                ->join("departments department", "department.id = user.department_id", "left")
                ->where([$usernameCol => $username, 'password' => md5($password)])
                ->get();

        return $query->num_rows() > 0 ? $query->row() : false;
    }
    
    public function admin_login($username, $password, $usernameCol = "email") {
        $group_id = 1;
        $query = $this->db->select("user.*,gp.name as group_name")
                ->from(self::$table . " user")
                ->join("users_groups ug", "ug.user_id = user.id", "left")
                ->join("groups gp", "gp.id = ug.group_id", "left") 
                ->where([$usernameCol => $username, 'password' => md5($password),'group_id' => $group_id])
                ->get();

        return $query->num_rows() > 0 ? $query->row() : false;
    }

    public function client_login($username, $password, $usernameCol = "email") {
        // $role_id = 6; // , 7, 8, 9
        $group_id = 2; // Memers/Client group id
        // $query = $this->db->where([$usernameCol => $username, 'password' => md5($password)])->WHERE_IN('role_id', $where_in)->get('vbs_users')->row_array();

        $query = $this->db->select("user.*,gp.name as group_name")
                ->from(self::$table . " user")
                ->join("users_groups ug", "ug.user_id = user.id", "left")
                ->join("groups gp", "gp.id = ug.group_id", "left") 
                ->where([$usernameCol => $username, 'password' => md5($password),'group_id' => $group_id])
                ->get();

        return $query->num_rows() > 0 ? $query->row() : false;
    }

    public function partner_login($username, $password, $usernameCol = "email") {
        // $role_id = 9;
        $group_id = 4; // Memers/Client group id
        $query = $this->db->select("user.*,gp.name as group_name")
                ->from(self::$table . " user")
                ->join("users_groups ug", "ug.user_id = user.id", "left")
                ->join("groups gp", "gp.id = ug.group_id", "left") 
                ->where([$usernameCol => $username, 'password' => md5($password),'group_id' => $group_id])
                ->get();

        return $query->num_rows() > 0 ? $query->row() : false;
    }

    public function driver_login($username, $password, $usernameCol = "email") {
        // $role_id = 7;
        $group_id = 5; // Memers/Client group id
        $query = $this->db->select("user.*,gp.name as group_name")
                ->from(self::$table . " user")
                ->join("users_groups ug", "ug.user_id = user.id", "left")
                ->join("groups gp", "gp.id = ug.group_id", "left") 
                ->where([$usernameCol => $username, 'password' => md5($password),'group_id' => $group_id])
                ->get();

        return $query->num_rows() > 0 ? $query->row() : false;
    }

    public function jobseeker_login($username, $password, $usernameCol = "email") {
        // $role_id = 8;
        $group_id = 6; // Memers/Client group id
        $query = $this->db->select("user.*,gp.name as group_name")
                ->from(self::$table . " user")
                ->join("users_groups ug", "ug.user_id = user.id", "left")
                ->join("groups gp", "gp.id = ug.group_id", "left") 
                ->where([$usernameCol => $username, 'password' => md5($password),'group_id' => $group_id])
                ->get();

        return $query->num_rows() > 0 ? $query->row() : false;
    }
    
    public function affiliate_login($username, $password, $usernameCol = "email") {
        // $role_id = 9;
        // $group_id = 4; // Memers/Client group id
        $user_group = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('groups') . " WHERE name = 'affiliates'");
        $group_id = $user_group[0]->id;
        
        $query = $this->db->select("user.*,gp.name as group_name")
                ->from(self::$table . " user")
                ->join("users_groups ug", "ug.user_id = user.id", "left")
                ->join("groups gp", "gp.id = ug.group_id", "left") 
                ->where([$usernameCol => $username, 'password' => md5($password),'group_id' => $group_id])
                ->get();

        return $query->num_rows() > 0 ? $query->row() : false;
    }

    public function getAll($where = [], $limit = false) {

        $this->db->select("user.*,role.name as role,department.name as department")
                ->from(self::$table . " user")
                ->join("roles role", "role.id = user.role_id", "left")
                ->join("departments department", "department.id = user.department_id", "left");

        if (!empty($where))
            $this->db->where($where);
        if ($limit != false)
            $this->db->limit($limit);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }
	public function getAllData($table,$where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get($table);
		return $query->num_rows() > 0 ? $query->result() : false;
	}
    public function create($data) {
        $this->db->trans_begin();
        $this->db->insert(self::$table, $data);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function update($data, $id) {
        if ($this->db->update(self::$table, $data, ['id' => $id]))
            return true;
        else
            return false;
    }
    
    public function updateUser($userData,$additionalData,$id) {
        $this->db->trans_begin();
        $usr_status = $this->db->update($this->db->dbprefix(self::$table), $userData, ['id' => $id]);
        // echo "table:".self::$table.' updated ' . $this->db->trans_status(); 
        if ($this->db->trans_status()) {
            $ud_status = $this->db->update($this->db->dbprefix(self::$userDetails), $additionalData, ['user_id' => $id]);
            //$updateQuery = "UPDATE " . $this->db->dbprefix(self::$userDetails) . " SET address1='".$additionalData['address1']."', address2='".$additionalData['address2']
            //            ."', company_name='".$additionalData['company_name']."', mobile_no='".$additionalData['mobile_no']."', fax_no='".$additionalData['fax_no']."' WHERE user_id=".$id;
             
            //$this->db->query($updateQuery); 
            if ($usr_status && $ud_status) {
                // echo "table:".self::$userDetails.' updated'. $this->db->trans_status();;exit;
                $this->db->trans_commit();
                return true;
            }
        } else {
            return false;
        }
    }

    public function delete($id) {
        return $this->db->delete(self::$table, ['id' => $id]);
    }

    public static function sendReply($obj, $subject, $message, $type = "contact") {

        require_once(APPPATH . "third_party/phpmailer/Exception.php");
        require_once(APPPATH . "third_party/phpmailer/PHPMailer.php");
        require_once(APPPATH . "third_party/phpmailer/SMTP.php");
        $mail = new \PHPMailer\PHPMailer\PHPMailer(true);
        $emailMessage = nl2br($message) . "<br>";

        $emailMessage .= "<h4 style='color: #000'>".APP_NAME."SAS<br>";
        $emailMessage .= "Service secrétariat</h4>";
        $emailMessage .= "<p><b>Siteweb : </b>www.ecab.app</p>";
        $emailMessage .= "<p><b>Email : </b>direction@ecab.app</p>";
        $emailMessage .= "<p><b>Tél : </b>01 48 13 09 34</p>";
        $emailMessage .= "<p><b>Fax : </b>01 83 62 84 04</p>";
        $emailMessage .= "<p><b>Adresse : </b>du siège social : 48-50 Avenue d'Enghien 93800 Epinay sur seine</p>";


        try {
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'tls://smtp.office365.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;
            $mail->CharSet = 'UTF-8';                           // Enable SMTP authentication
            $replyTo = $type == "job" ? 'direction@ecab.app' : 'contact@ecab.app';
            $mail->addReplyTo($replyTo, APP_NAME);
            $mail->Username = 'direction@ecab.app';                 // SMTP username
            $mail->Password = 'Hanouf77';                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to
            //Recipients
            $mail->setFrom('direction@ecab.app', APP_NAME);
            $mail->addAddress($obj->email, $obj->civility . " " . $obj->first_name . " " . $obj->last_name);
            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body = $emailMessage;
            $check = $mail->send();
            return [
                'status' => true,
                'message' => 'Message sent'
            ];
        } catch (\PHPMailer\PHPMailer\Exception $e) {
            return [
                'status' => false,
                'message' => 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo
            ];
        }
    }

    public function getResetPassword($where) {
        $query = $this->db->get_where(self::$resetPassword, $where);
        return $query->num_rows() > 0 ? $query->row() : false;
    }

    public function addResetPassword($data) {
        $this->db->trans_begin();
        $this->db->insert(self::$resetPassword, $data);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function updateResetPassword($data, $id) {
        if ($this->db->update(self::$resetPassword, $data, ['id' => $id]))
            return true;
        else
            return false;
    }

    public function deleteResetPassword($id) {
        return $this->db->delete(self::$resetPassword, ['id' => $id]);
    }
    
    public function getGroupID($groupName) { 
        $user_group = '';
        $query = $this->db->select("groups.*")
                    ->from("vbs_groups groups")
                    ->where(['name' => $groupName])
                    ->get();
        if ($query->num_rows() > 0) {
            $user_group = $query->result_array();
        } 
        $group_id = $user_group[0]['id']; 
        return $group_id;
    }

    public function isClient($id) {
        $group_id = $this->getGroupID('clients');
        
        $searchQuery = "SELECT COUNT(user.id) as is_found 
                            FROM vbs_users as user
                                INNER JOIN vbs_users_groups ug ON ug.user_id = user.id
                            WHERE group_id=".$group_id . " AND user.id=" . $id;
        $query = $this->db->query($searchQuery);
        $result = $query->result_array()[0];
        
        return $result['is_found'] == 0 ? false : true;
    }
    
    public function getAllClients() {
        $group_id = $this->getGroupID('clients');
        $query = $this->db->select("user.*,ud.address1 as address1,ud.address2 as address2,ud.company_name as company_name,ud.fax_no as fax_no,gp.name as group_name")
                ->from(self::$table . " user")
                ->join("users_details ud", "ud.user_id = user.id", "INNER")
                ->join("users_groups ug", "ug.user_id = user.id", "INNER")
                ->join("groups gp", "gp.id = ug.group_id", "INNER")
                ->where(['group_id' => $group_id])
                ->order_by('id', 'desc')->get();

        return $query->num_rows() > 0 ? $query->result_array() : false;
    }
    
    public function getClientsTotal() {
        $group_id = $this->getGroupID('clients');
        $total_clients = 0;
        
        $query = $this->db->select("COUNT(user.id) as total_clients")
                ->from(self::$table . " user")
                ->join("users_details ud", "ud.user_id = user.id", "INNER")
                ->join("users_groups ug", "ug.user_id = user.id", "INNER")
                ->join("groups gp", "gp.id = ug.group_id", "INNER")
                ->where(['group_id' => $group_id,'user.active'=> '1'])->get();
        if($query->num_rows() > 0) {
            $clientRS = $query->result_array()[0];
            $total_clients = $clientRS['total_clients'];
        }
        return $total_clients;
    }
    public function searchClient($searchFilter) {
        $group_id = $this->getGroupID('clients');
        $whereClause = "";
        if($searchFilter['search_name'] !='') {
            $whereClause .= " AND LOWER(username) LIKE '%". strtolower($searchFilter['search_name']) . "%' ";
        }
        if($searchFilter['search_email'] !='') {
            $whereClause .= " AND LOWER(user.email) LIKE '%" . strtolower($searchFilter['search_email']) . "%' ";
        }
        if($searchFilter['search_phone'] !='') {
            $whereClause .= " AND phone LIKE '%" . $searchFilter['search_phone'] . "%' ";
        }
        if($searchFilter['date_from'] !='' && $searchFilter['date_to'] != '') {
            $whereClause .= " AND ( user.created_at >= '" . date('Y-m-d',strtotime($searchFilter['date_from'])) . " 00:00:00' AND user.created_at <= '" . date('Y-m-d',strtotime($searchFilter['date_to'])) ." 23:59:59' ) ";
        }
        if($searchFilter['date_from'] !='' && $searchFilter['date_to'] =='') {
            $whereClause .= " AND created_at >= '" . date('Y-m-d H:i:s',strtotime($searchFilter['date_from'])) . "' ";
        }
        if($searchFilter['date_to'] !='' && $searchFilter['date_from'] =='') {
            $whereClause .= " AND created_at <= '" . date('Y-m-d H:i:s',strtotime($searchFilter['date_to'])) . "' ";
        }
        if($searchFilter['status'] !='') {
            $whereClause .= " AND user.active =".$searchFilter['status'];
        }
         
        $searchQuery = "SELECT user.*
                                ,ud.address1 as address1,ud.address2 as address2,ud.company_name as company_name,ud.fax_no as fax_no
                                ,gp.name as group_name 
                            FROM vbs_users as user
                                INNER JOIN vbs_users_details ud ON ud.user_id = user.id
                                INNER JOIN vbs_users_groups ug ON ug.user_id = user.id
                                INNER JOIN vbs_groups gp ON gp.id = ug.group_id
                            WHERE group_id=".$group_id . $whereClause . " ORDER BY user.id DESC";
        $query = $this->db->query($searchQuery);
        // echo "<PRE>";print_r($this->db->last_query());echo "</PRE>";
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }
    
    public function isDriver($id) {
        $group_id = $this->getGroupID('drivers');
        
        $searchQuery = "SELECT COUNT(user.id) as is_found 
                            FROM vbs_users as user
                                INNER JOIN vbs_users_groups ug ON ug.user_id = user.id
                            WHERE group_id=".$group_id . " AND user.id=" . $id;
        $query = $this->db->query($searchQuery);
        $result = $query->result_array()[0];
        
        return $result['is_found'] == 0 ? false : true;
    }
    
    public function getAllDrivers() {
        $group_id = $this->getGroupID('drivers');
        $query = $this->db->select("user.*,ud.address1 as address1,ud.address2 as address2,ud.company_name as company_name,ud.fax_no as fax_no,gp.name as group_name")
                ->from(self::$table . " user")
                ->join("users_details ud", "ud.user_id = user.id", "INNER")
                ->join("users_groups ug", "ug.user_id = user.id", "INNER")
                ->join("groups gp", "gp.id = ug.group_id", "INNER")
                ->where(['group_id' => $group_id])
                ->order_by('id', 'desc')->get();

        return $query->num_rows() > 0 ? $query->result_array() : false;
    }
    
    public function getDriversTotal() {
        $group_id = $this->getGroupID('drivers');
        $total_drivers = 0;
        
        $query = $this->db->select("COUNT(user.id) as total_drivers")
                ->from(self::$table . " user")
                ->join("users_details ud", "ud.user_id = user.id", "INNER")
                ->join("users_groups ug", "ug.user_id = user.id", "INNER")
                ->join("groups gp", "gp.id = ug.group_id", "INNER")
                ->where(['group_id' => $group_id,'user.active'=> '1'])->get();
        if($query->num_rows() > 0) {
            $driverRS = $query->result_array()[0];
            $total_drivers = $driverRS['total_drivers'];
        }
         
        return $total_drivers;
    }
    
    public function searchDriver($searchFilter) {
        $group_id = $this->getGroupID('drivers');
        $whereClause = "";
        if($searchFilter['search_name'] !='') {
            $whereClause .= " AND LOWER(username) LIKE '%". strtolower($searchFilter['search_name']) . "%' ";
        }
        if($searchFilter['search_email'] !='') {
            $whereClause .= " AND LOWER(user.email) LIKE '%" . strtolower($searchFilter['search_email']) . "%' ";
        }
        if($searchFilter['search_phone'] !='') {
            $whereClause .= " AND phone LIKE '%" . $searchFilter['search_phone'] . "%' ";
        }
        if($searchFilter['date_from'] !='' && $searchFilter['date_to'] != '') {
            $whereClause .= " AND ( user.created_at >= '" . date('Y-m-d',strtotime($searchFilter['date_from'])) . " 00:00:00' AND user.created_at <= '" . date('Y-m-d',strtotime($searchFilter['date_to'])) ." 23:59:59' ) ";
        }
        if($searchFilter['date_from'] !='' && $searchFilter['date_to'] =='') {
            $whereClause .= " AND created_at >= '" . date('Y-m-d H:i:s',strtotime($searchFilter['date_from'])) . "' ";
        }
        if($searchFilter['date_to'] !='' && $searchFilter['date_from'] =='') {
            $whereClause .= " AND created_at <= '" . date('Y-m-d H:i:s',strtotime($searchFilter['date_to'])) . "' ";
        }
        if($searchFilter['status'] !='') {
            $whereClause .= " AND user.active =".$searchFilter['status'];
        }
         
        $searchQuery = "SELECT user.*
                                ,ud.address1 as address1,ud.address2 as address2,ud.company_name as company_name,ud.fax_no as fax_no
                                ,gp.name as group_name 
                            FROM vbs_users as user
                                INNER JOIN vbs_users_details ud ON ud.user_id = user.id
                                INNER JOIN vbs_users_groups ug ON ug.user_id = user.id
                                INNER JOIN vbs_groups gp ON gp.id = ug.group_id
                            WHERE group_id=".$group_id . $whereClause . " ORDER BY user.id DESC";
        $query = $this->db->query($searchQuery);
        
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }
    
    public function isPartner($id) {
        $group_id = $this->getGroupID('partners');
        
        $searchQuery = "SELECT COUNT(user.id) as is_found 
                            FROM vbs_users as user
                                INNER JOIN vbs_users_groups ug ON ug.user_id = user.id
                            WHERE group_id=".$group_id . " AND user.id=" . $id;
        $query = $this->db->query($searchQuery);
        $result = $query->result_array()[0];
        
        return $result['is_found'] == 0 ? false : true;
    }
    
    public function getAllPartners() {
        $group_id = $this->getGroupID('partners');
        $query = $this->db->select("user.*,ud.address1 as address1,ud.address2 as address2,ud.company_name as company_name,ud.fax_no as fax_no,gp.name as group_name")
                ->from(self::$table . " user")
                ->join("users_details ud", "ud.user_id = user.id", "INNER")
                ->join("users_groups ug", "ug.user_id = user.id", "INNER")
                ->join("groups gp", "gp.id = ug.group_id", "INNER")
                ->where(['group_id' => $group_id])
                ->order_by('id', 'desc')->get();
        
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }
    
    public function getPartnersTotal() {
        $group_id = $this->getGroupID('partners');
        $total_partners = 0;
        
        $query = $this->db->select("COUNT(user.id) as total_partners")
                ->from(self::$table . " user")
                ->join("users_details ud", "ud.user_id = user.id", "INNER")
                ->join("users_groups ug", "ug.user_id = user.id", "INNER")
                ->join("groups gp", "gp.id = ug.group_id", "INNER")
                ->where(['group_id' => $group_id,'user.active'=> '1'])->get();
        if($query->num_rows() > 0) {
            $partnerRS = $query->result_array()[0];
            $total_partners = $partnerRS['total_partners'];
        }
        return $total_partners;
    }
    
    public function searchPartner($searchFilter) {
        $group_id = $this->getGroupID('partners');
        $whereClause = "";
        if($searchFilter['search_name'] !='') {
            $whereClause .= " AND LOWER(username) LIKE '%". strtolower($searchFilter['search_name']) . "%' ";
        }
        if($searchFilter['search_email'] !='') {
            $whereClause .= " AND LOWER(user.email) LIKE '%" . strtolower($searchFilter['search_email']) . "%' ";
        }
        if($searchFilter['search_phone'] !='') {
            $whereClause .= " AND phone LIKE '%" . $searchFilter['search_phone'] . "%' ";
        }
        if($searchFilter['date_from'] !='' && $searchFilter['date_to'] != '') {
            $whereClause .= " AND ( user.created_at >= '" . date('Y-m-d',strtotime($searchFilter['date_from'])) . " 00:00:00' AND user.created_at <= '" . date('Y-m-d',strtotime($searchFilter['date_to'])) ." 23:59:59' ) ";
        }
        if($searchFilter['date_from'] !='' && $searchFilter['date_to'] =='') {
            $whereClause .= " AND created_at >= '" . date('Y-m-d H:i:s',strtotime($searchFilter['date_from'])) . "' ";
        }
        if($searchFilter['date_to'] !='' && $searchFilter['date_from'] =='') {
            $whereClause .= " AND created_at <= '" . date('Y-m-d H:i:s',strtotime($searchFilter['date_to'])) . "' ";
        }
        if($searchFilter['status'] !='') {
            $whereClause .= " AND user.active =".$searchFilter['status'];
        }
         
        $searchQuery = "SELECT user.*
                                ,ud.address1 as address1,ud.address2 as address2,ud.company_name as company_name,ud.fax_no as fax_no
                                ,gp.name as group_name 
                            FROM vbs_users as user
                                INNER JOIN vbs_users_details ud ON ud.user_id = user.id
                                INNER JOIN vbs_users_groups ug ON ug.user_id = user.id
                                INNER JOIN vbs_groups gp ON gp.id = ug.group_id
                            WHERE group_id=".$group_id . $whereClause . " ORDER BY user.id DESC";
        $query = $this->db->query($searchQuery);
        
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }
    
    public function isJobseeker($id) {
        $group_id = $this->getGroupID('jobseekers');
        
        $searchQuery = "SELECT COUNT(user.id) as is_found 
                            FROM vbs_users as user
                                INNER JOIN vbs_users_groups ug ON ug.user_id = user.id
                            WHERE group_id=".$group_id . " AND user.id=" . $id;
        $query = $this->db->query($searchQuery);
        $result = $query->result_array()[0];
        
        return $result['is_found'] == 0 ? false : true;
    }
    
    public function getAllJobseekers() {
        $group_id = $this->getGroupID('jobseekers');
        $query = $this->db->select("user.*,ud.address1 as address1,ud.address2 as address2,ud.company_name as company_name,ud.fax_no as fax_no,gp.name as group_name")
                ->from(self::$table . " user")
                ->join("users_details ud", "ud.user_id = user.id", "INNER")
                ->join("users_groups ug", "ug.user_id = user.id", "INNER")
                ->join("groups gp", "gp.id = ug.group_id", "INNER")
                ->where(['group_id' => $group_id])
                ->order_by('id', 'desc')->get();
        
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }
    
    public function getJobseekersTotal() {
        $group_id = $this->getGroupID('jobseekers');
        $total_jobseekers = 0;
        
        $query = $this->db->select("COUNT(user.id) as total_jobseekers")
                ->from(self::$table . " user")
                ->join("users_details ud", "ud.user_id = user.id", "INNER")
                ->join("users_groups ug", "ug.user_id = user.id", "INNER")
                ->join("groups gp", "gp.id = ug.group_id", "INNER")
                ->where(['group_id' => $group_id,'user.active'=> '1'])->get();
        if($query->num_rows() > 0) {
            $jobseekerRS = $query->result_array()[0];
            $total_jobseekers = $jobseekerRS['total_jobseekers'];
        }
        return $total_jobseekers;
    }

    public function getPiplinesTotal() {
        $query = $this->db->select("COUNT(p.id) as total_piplines")->from("vbs_piplines p")->get();
        if($query->num_rows() > 0) {
            $piplines = $query->result_array()[0];
            $total_piplines = $piplines['total_piplines'];
        }
        return $total_piplines;
    }
    
    public function searchJobseeker($searchFilter) {
        $group_id = $this->getGroupID('jobseekers');
        $whereClause = "";
        if($searchFilter['search_name'] !='') {
            $whereClause .= " AND LOWER(username) LIKE '%". strtolower($searchFilter['search_name']) . "%' ";
        }
        if($searchFilter['search_email'] !='') {
            $whereClause .= " AND LOWER(user.email) LIKE '%" . strtolower($searchFilter['search_email']) . "%' ";
        }
        if($searchFilter['search_phone'] !='') {
            $whereClause .= " AND phone LIKE '%" . $searchFilter['search_phone'] . "%' ";
        }
        if($searchFilter['date_from'] !='' && $searchFilter['date_to'] != '') {
            $whereClause .= " AND ( user.created_at >= '" . date('Y-m-d',strtotime($searchFilter['date_from'])) . " 00:00:00' AND user.created_at <= '" . date('Y-m-d',strtotime($searchFilter['date_to'])) ." 23:59:59' ) ";
        }
        if($searchFilter['date_from'] !='' && $searchFilter['date_to'] =='') {
            $whereClause .= " AND created_at >= '" . date('Y-m-d H:i:s',strtotime($searchFilter['date_from'])) . "' ";
        }
        if($searchFilter['date_to'] !='' && $searchFilter['date_from'] =='') {
            $whereClause .= " AND created_at <= '" . date('Y-m-d H:i:s',strtotime($searchFilter['date_to'])) . "' ";
        }
        if($searchFilter['status'] !='') {
            $whereClause .= " AND user.active =".$searchFilter['status'];
        }
         
        $searchQuery = "SELECT user.*
                                ,ud.address1 as address1,ud.address2 as address2,ud.company_name as company_name,ud.fax_no as fax_no
                                ,gp.name as group_name 
                            FROM vbs_users as user
                                INNER JOIN vbs_users_details ud ON ud.user_id = user.id
                                INNER JOIN vbs_users_groups ug ON ug.user_id = user.id
                                INNER JOIN vbs_groups gp ON gp.id = ug.group_id
                            WHERE group_id=".$group_id . $whereClause . " ORDER BY user.id DESC";
        $query = $this->db->query($searchQuery);
        // echo "<PRE>";print_r($this->db->last_query());echo "</PRE>";
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }
//added by sultan
    public function client_login_check($username, $password, $usernameCol = "email") {
        // $role_id = 6; // , 7, 8, 9
        $group_id = 2; // Memers/Client group id
        // $query = $this->db->where([$usernameCol => $username, 'password' => md5($password)])->WHERE_IN('role_id', $where_in)->get('vbs_users')->row_array();

        $query = $this->db->select("user.*,gp.name as group_name")
                ->from(self::$table . " user")
                ->join("users_groups ug", "ug.user_id = user.id", "left")
                ->join("groups gp", "gp.id = ug.group_id", "left") 
                ->where([$usernameCol => $username, 'password' => md5($password),'group_id' => $group_id])
                ->get();

//return $query->row();
        return ($query->num_rows() > 0) ? $query->row() : false;
    }
    
    public function isAffiliate($id) {
        $group_id = $this->getGroupID('affiliates');
        
        $searchQuery = "SELECT COUNT(user.id) as is_found 
                            FROM vbs_users as user
                                INNER JOIN vbs_users_groups ug ON ug.user_id = user.id
                            WHERE group_id=".$group_id . " AND user.id=" . $id;
        $query = $this->db->query($searchQuery);
        $result = $query->result_array()[0];
        
        return $result['is_found'] == 0 ? false : true;
    }
    
    public function getAllAffiliates() {
        $group_id = $this->getGroupID('affiliates');
        $query = $this->db->select("user.*,ud.address1 as address1,ud.address2 as address2,ud.company_name as company_name,ud.fax_no as fax_no,gp.name as group_name")
                ->from(self::$table . " user")
                ->join("users_details ud", "ud.user_id = user.id", "INNER")
                ->join("users_groups ug", "ug.user_id = user.id", "INNER")
                ->join("groups gp", "gp.id = ug.group_id", "INNER")
                ->where(['group_id' => $group_id])
                ->order_by('id', 'desc')->get();
        
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }
    
    public function getAffiliatesTotal() {
        $group_id = $this->getGroupID('affiliates');
        $total_affiliates = 0;
        
        $query = $this->db->select("COUNT(user.id) as total_affiliates")
                ->from(self::$table . " user")
                ->join("users_details ud", "ud.user_id = user.id", "INNER")
                ->join("users_groups ug", "ug.user_id = user.id", "INNER")
                ->join("groups gp", "gp.id = ug.group_id", "INNER")
                ->where(['group_id' => $group_id,'user.active'=> '1'])->get();
        if($query->num_rows() > 0) {
            $affiliateRS = $query->result_array()[0];
            $total_affiliates = $affiliateRS['total_affiliates'];
        }
        return $total_affiliates;
    }
    
    public function searchAffiliate($searchFilter) {
        $group_id = $this->getGroupID('affiliates');
        $whereClause = "";
        if($searchFilter['search_name'] !='') {
            $whereClause .= " AND LOWER(username) LIKE '%". strtolower($searchFilter['search_name']) . "%' ";
        }
        if($searchFilter['search_email'] !='') {
            $whereClause .= " AND LOWER(user.email) LIKE '%" . strtolower($searchFilter['search_email']) . "%' ";
        }
        if($searchFilter['search_phone'] !='') {
            $whereClause .= " AND phone LIKE '%" . $searchFilter['search_phone'] . "%' ";
        }
        if($searchFilter['date_from'] !='' && $searchFilter['date_to'] != '') {
            $whereClause .= " AND ( user.created_at >= '" . date('Y-m-d',strtotime($searchFilter['date_from'])) . " 00:00:00' AND user.created_at <= '" . date('Y-m-d',strtotime($searchFilter['date_to'])) ." 23:59:59' ) ";
        }
        if($searchFilter['date_from'] !='' && $searchFilter['date_to'] =='') {
            $whereClause .= " AND created_at >= '" . date('Y-m-d H:i:s',strtotime($searchFilter['date_from'])) . "' ";
        }
        if($searchFilter['date_to'] !='' && $searchFilter['date_from'] =='') {
            $whereClause .= " AND created_at <= '" . date('Y-m-d H:i:s',strtotime($searchFilter['date_to'])) . "' ";
        }
        if($searchFilter['status'] !='') {
            $whereClause .= " AND user.active =".$searchFilter['status'];
        }
         
        $searchQuery = "SELECT user.*
                                ,ud.address1 as address1,ud.address2 as address2,ud.company_name as company_name,ud.fax_no as fax_no
                                ,gp.name as group_name 
                            FROM vbs_users as user
                                INNER JOIN vbs_users_details ud ON ud.user_id = user.id
                                INNER JOIN vbs_users_groups ug ON ug.user_id = user.id
                                INNER JOIN vbs_groups gp ON gp.id = ug.group_id
                            WHERE group_id=".$group_id . $whereClause . " ORDER BY user.id DESC";
        $query = $this->db->query($searchQuery);
        
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }
    
    public function insert_item($items) {
        return $this->db->insert(self::$table,$items);
    }
}
