<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Bookings_Model extends CI_Model {

	function __construct(){
		parent::__construct();
	}


	public static $table = "bookings";

	public function get($where = []){
		$query = $this->db->get_where(self::$table, $where);
		return $query->num_rows() > 0 ? $query->row() : false;
	}
	public function categoryget($where = []){
		$query = $this->db->get_where('vbs_booking_category', $where);
		return $query->num_rows() > 0 ? $query->row() : false;
	}
	

	public function getAll($where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get(self::$table);
		return $query->num_rows() > 0 ? $query->result() : false;
	}

public function getAllCategory($where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get('vbs_booking_category');
		return $query->num_rows() > 0 ? $query->result() : false;
	}

    public function QouteChartCount(){
        $data = $this->db->query("SELECT COUNT(id) as count,MONTHNAME(created_at) as month_name FROM vbs_request WHERE YEAR(created_at) >= '2019'
      GROUP BY YEAR(created_at),MONTH(created_at)");
        return  $data->result();;
    }
    public function QouteLineChart() {
        $dayQuery =  $this->db->query("SELECT  MONTHNAME(created_at) as y, COUNT(id) as a FROM vbs_request WHERE  YEAR(created_at) >= '2019'  GROUP BY YEAR(created_at),MONTH(created_at)");

        return $dayQuery->result();

    }
	public function create($data){
		$this->db->trans_begin();
		$this->db->insert(self::$table, $data);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}

	public function categorycreate($data){
		$this->db->trans_begin();
		$this->db->insert('vbs_booking_category', $data);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}

	public function categoryupdate($data, $id){
		if($this->db->update('vbs_booking_category', $data, ['id' => $id]))
			return true;
		else
			return false;
	}

	public function update($data, $id){
		if($this->db->update(self::$table, $data, ['id' => $id]))
			return true;
		else
			return false;
	}

	public function delete($id){
		return $this->db->delete(self::$table, ['id' => $id]);
	}


	public static function validate(){
		$reqInputs = [
				'civility'  => 'Civility',
				'name'      => 'Nom',
				'prename'   => 'Prenom',
				'email'     => 'Votre email',
				'tel'       => 'Telephone',
				'message'   => 'Votre message'
		];

		$error = [];
		foreach($reqInputs as $key => $input){
			$check = !isset($_POST[$key]) || $_POST[$key] == "" || empty($_POST[$key]);
			if($check){
				$error[] = "Merci de compléter toutes les cases correctement.";
			}
			if($key == "email" AND !filter_var($_POST[$key], FILTER_VALIDATE_EMAIL)){
				$error[] = "S'il vous plaît entrer un email valide.";
			}
			if($key == "tel" AND !preg_match('/^0[0123456790]\d{8}$/', $_POST[$key])){
				$error[] = "Veuillez entrer un numéro de téléphone valide avec 10 chiffres commençant par 0.";
			}
		}

		return $error;
	}
	//created by sultan nothing change in above code
	public function booking_Config_getAll($table){
		$this->db->order_by('id','desc');
		$query = $this->db->get($table);
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	public function poidatarecord($table,$id){

		 $q=$this->db->where('id',$id)->get($table);
		 return $result = $q->row_array();
		
	}
		public function passengerrecord($table,$id){

		 $q=$this->db->where('id',$id)->get($table);
		 return $result = $q->row_array();
		
	}
	public function booking_passengers($table,$id){
		$this->db->order_by('id','desc');
		$query = $this->db->where("clientid",$id)->get($table);
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	public function getcarsdata(){
       $statut=1;
       $type=0;
       $q=$this->db
	  ->from('vbs_u_car_category')
	  ->where(['statut'=>$statut,'cartype'=>$type])
	  ->order_by('id', 'DESC')
	  ->get();
       return  $q->result();
	}
	public function getcarsconfigdata($car_id){
	  
       $q=$this->db
	  ->from('vbs_car_configuration')
	  ->where(['car_id'=>$car_id])
	  ->order_by('id', 'DESC')
	  ->get();
       return  $q->result();
}
public function getwheelcarsdata(){
       $statut=1;
       $type=1;
       $q=$this->db
	  ->from('vbs_u_car_category')
	  ->where(['statut'=>$statut,'cartype'=>$type])
	  ->order_by('id', 'DESC')
	  ->get();
       return  $q->result();

}
public function getlastbookingid(){
	return $this->db->order_by('id',"desc")
		->limit(1)
		->get('vbs_bookings')
		->row()->id;
	
}
public function getpricebycarid($carid){
	$statut=1;
	return $this->db->order_by('id',"desc")
		->limit(1)
		->where(["car_category"=>$carid,"statut"=>$statut])
		->get('vbs_u_price_car')
		->row();
}
public function getdiscountbydate($pickdate,$time){

   //$time="14:00";
   //$pickdate="17/07/2020";

$status=1;
return $this->db->order_by('id',"desc")
		->limit(1)
		->where(['statut'=>$status])
		->where("str_to_date('$pickdate','%d/%m/%Y') BETWEEN datefrom AND dateto")
	    ->where(['timefrom <'=>$time,'timeto >'=>$time])
	    ->get('vbs_u_discount')
		->row();

}
public function getwelcomediscount(){

   //$time="14:00";
   //$pickdate="17/07/2020";
$status=1;
$type=0;
return $this->db->order_by('id',"desc")
		->limit(1)
		->where(['statut'=>$status,'discount_category'=>$type])
	    ->get('vbs_u_discount')
		->row();

}

public function getrewarddiscount(){

   //$time="14:00";
   //$pickdate="17/07/2020";
$status=1;
$type=1;
return $this->db->order_by('id',"desc")
		->limit(1)
		->where(['statut'=>$status,'discount_category'=>$type])
	    ->get('vbs_u_discount')
		->row();

}



public function getdiscountdata($discountid){
	$statut=1;
	return $this->db->order_by('id',"desc")
		->limit(1)
		->where(["id"=>$discountid,"statut"=>$statut])
		->get('vbs_u_discount')
		->row()->discount;
}
public function getcarname($id){
	    return $this->db
        ->where(["id"=>$id])
		->get('vbs_u_car_category')
		->row()->car_cat_name;
} 
public function paymentmethods(){
	$status=1;
	 
	$q=$this->db->order_by('paymentorder',"asc")
		->limit(6)
		->where(["status"=>$status])
		->get('vbs_u_payment_method');
		 return  $q->result();
}
public function getclientdatabyid($id){
	  return $this->db
        ->where(["id"=>$id])
		->get('vbs_users')
		->row();
}
public function addbookingrecord($table,$data){
$this->db->insert($table, $data);
$insert_id = $this->db->insert_id();
return $insert_id;
}


public function getbookingallrecords(){

       $q=$this->db
	  ->select(['booking.id','booking.booktime','booking.pickzipcode','booking.pickcity','booking.dropzipcode','booking.dropcity','booking.user_id','booking.booking_ref','booking.pick_date','booking.pick_time','booking.pick_point','booking.drop_point','booking.distance','booking.cost_of_journey','booking.price','booking.totalprice','booking.payment_received','booking.is_conformed','booking.bookdate','booking.waiting_time','booking.return_time','booking.return_date','booking.start_time','booking.start_date','booking.end_time','booking.end_date','booking.regular','booking.returncheck','booking.wheelchair','booking.pickupcategory','booking.dropoffcategory','booking.last_action','car.id as carid','car.car_cat_name as carname','method.id as payment_id','method.name as payment_name','service.id as servicecat_id',
	  	'service.category_name as servicecat_name','ser.id service_id','ser.service_name as service_name'])
	  ->from('vbs_bookings as booking')
	  ->join('vbs_u_car_category as car', 'booking.car_id=car.id','left')
	  ->join('vbs_u_service as ser', 'booking.service_id=ser.id','left')
	  ->join('vbs_u_payment_method as method', 'booking.payment_method_id=method.id','left')
	  ->join('vbs_u_category_service as service', 'booking.service_category_id=service.id','left')
	  ->order_by('booking.id', 'DESC')
	  ->get();
       return  $q->result();
}
public function deletebookingdata($table,$where){
return $this->db->delete($table,$where);
}
public function deletebookingchilddata($table,$id){
return $this->db->delete($table, ['booking_id' => $id]);
}
public function getbookingsbydate($datefrom,$dateto){

   //$time="14:00";
   //$pickdate="17/07/2020";

   $q=$this->db
	  ->select(['booking.id','booking.registered_name','booking.pick_date','booking.pick_time','booking.pick_point','booking.drop_point','booking.distance','booking.cost_of_journey','booking.payment_received','booking.is_conformed','booking.bookdate','booking.return_time','booking.return_date','booking.return_time','booking.start_time','booking.start_date','booking.end_time','booking.end_date','booking.regular','booking.returncheck','booking.wheelchair','booking.pickupcategory','booking.dropoffcategory','booking.user_id','booking.user_id','booking.last_action','car.id as carid','car.car_cat_name as carname','method.id as payment_id','method.name as payment_name','service.id as servicecat_id',
	  	'service.category_name as servicecat_name','ser.id service_id','ser.service_name as service_name'])
	  ->from('vbs_bookings as booking')
	  ->join('vbs_u_car_category as car', 'booking.car_id=car.id','left')
	   ->join('vbs_u_service as ser', 'booking.service_id=ser.id','left')
	  ->join('vbs_u_payment_method as method', 'booking.payment_method_id=method.id','left')
	   ->join('vbs_u_category_service as service', 'booking.service_category_id=service.id','left')
	   ->where("pick_date BETWEEN str_to_date('$datefrom','%m/%d/%Y') AND str_to_date('$dateto','%m/%d/%Y')")
	  ->order_by('car.id', 'DESC')
	    ->get();
	    return $q->result();

}
public function getbookingsbyid($query){
	 $query=urldecode($query);
      $q=$this->db
	  ->from('brand')
	  ->order_by('id', 'DESC')
	  ->like('name',$query)
	  ->limit($limit,$offset)
	  ->get();
      
          return $q->result();
       
}
public function getbookingsbybookingref($query){
	 $query=urldecode($query);
     $q=$this->db
	  ->select(['booking.id','booking.registered_name','booking.pick_date','booking.pick_time','booking.pick_point','booking.drop_point','booking.distance','booking.cost_of_journey','booking.payment_received','booking.is_conformed','booking.bookdate','booking.return_time','booking.return_date','booking.return_time','booking.start_time','booking.start_date','booking.end_time','booking.end_date','booking.regular','booking.returncheck','booking.wheelchair','booking.pickupcategory','booking.dropoffcategory','booking.user_id','booking.user_id','booking.last_action','car.id as carid','car.car_cat_name as carname','method.id as payment_id','method.name as payment_name','service.id as servicecat_id',
	  	'service.category_name as servicecat_name','ser.id service_id','ser.service_name as service_name'])
	  ->from('vbs_bookings as booking')
	  ->join('vbs_u_car_category as car', 'booking.car_id=car.id','left')
	   ->join('vbs_u_service as ser', 'booking.service_id=ser.id','left')
	  ->join('vbs_u_payment_method as method', 'booking.payment_method_id=method.id','left')
	   ->join('vbs_u_category_service as service', 'booking.service_category_id=service.id','left')
	  ->like('booking_ref',$query)
	 ->order_by('car.id', 'DESC')
	    ->get();
	    return $q->result();
      
         
       
}
public function getbookingsbybookingstatus($query){
	 $query=urldecode($query);
     $q=$this->db
	  ->select(['booking.id','booking.registered_name','booking.pick_date','booking.pick_time','booking.pick_point','booking.drop_point','booking.distance','booking.cost_of_journey','booking.payment_received','booking.is_conformed','booking.bookdate','booking.return_time','booking.return_date','booking.return_time','booking.start_time','booking.start_date','booking.end_time','booking.end_date','booking.regular','booking.returncheck','booking.wheelchair','booking.pickupcategory','booking.dropoffcategory','booking.user_id','booking.user_id','booking.last_action','car.id as carid','car.car_cat_name as carname','method.id as payment_id','method.name as payment_name','service.id as servicecat_id',
	  	'service.category_name as servicecat_name','ser.id service_id','ser.service_name as service_name'])
	  ->from('vbs_bookings as booking')
	  ->join('vbs_u_car_category as car', 'booking.car_id=car.id','left')
	   ->join('vbs_u_service as ser', 'booking.service_id=ser.id','left')
	  ->join('vbs_u_payment_method as method', 'booking.payment_method_id=method.id','left')
	   ->join('vbs_u_category_service as service', 'booking.service_category_id=service.id','left')
	  ->like('is_conformed',$query)
	 ->order_by('car.id', 'DESC')
	    ->get();
	    return $q->result();
       
}
public function getbookingsbybookingservice($serviceid){
	
     $q=$this->db
	  ->select(['booking.id','booking.registered_name','booking.pick_date','booking.pick_time','booking.pick_point','booking.drop_point','booking.distance','booking.cost_of_journey','booking.payment_received','booking.is_conformed','booking.bookdate','booking.return_time','booking.return_date','booking.return_time','booking.start_time','booking.start_date','booking.end_time','booking.end_date','booking.regular','booking.returncheck','booking.wheelchair','booking.pickupcategory','booking.dropoffcategory','booking.user_id','booking.user_id','booking.last_action','car.id as carid','car.car_cat_name as carname','method.id as payment_id','method.name as payment_name','service.id as servicecat_id',
	  	'service.category_name as servicecat_name','ser.id service_id','ser.service_name as service_name'])
	  ->from('vbs_bookings as booking')
	  ->join('vbs_u_car_category as car', 'booking.car_id=car.id','left')
	   ->join('vbs_u_service as ser', 'booking.service_id=ser.id','left')
	  ->join('vbs_u_payment_method as method', 'booking.payment_method_id=method.id','left')
	   ->join('vbs_u_category_service as service', 'booking.service_category_id=service.id','left')
	  ->where('service_id',$serviceid)
	  ->order_by('car.id', 'DESC')
	    ->get();
	    return $q->result();
      
        
       
}
public function getbookingsbybookingregular($query){
	
     $q=$this->db
	  ->select(['booking.id','booking.registered_name','booking.pick_date','booking.pick_time','booking.pick_point','booking.drop_point','booking.distance','booking.cost_of_journey','booking.payment_received','booking.is_conformed','booking.bookdate','booking.return_time','booking.return_date','booking.return_time','booking.start_time','booking.start_date','booking.end_time','booking.end_date','booking.regular','booking.returncheck','booking.wheelchair','booking.pickupcategory','booking.dropoffcategory','booking.user_id','booking.user_id','booking.last_action','car.id as carid','car.car_cat_name as carname','method.id as payment_id','method.name as payment_name','service.id as servicecat_id',
	  	'service.category_name as servicecat_name','ser.id service_id','ser.service_name as service_name'])
	  ->from('vbs_bookings as booking')
	  ->join('vbs_u_car_category as car', 'booking.car_id=car.id','left')
	   ->join('vbs_u_service as ser', 'booking.service_id=ser.id','left')
	  ->join('vbs_u_payment_method as method', 'booking.payment_method_id=method.id','left')
	   ->join('vbs_u_category_service as service', 'booking.service_category_id=service.id','left')
	  ->where('regular',$query)
	 ->order_by('car.id', 'DESC')
	    ->get();
	    return $q->result();
       
}
public function getcategory($table,$id){

	    	$where =array('id'=> $id);
          $query = $this->db->get_where($table, $where);

		 if ($query->num_rows() > 0) {
			$query=  $query->row()->ride_cat_name;
			return $query;
		}else{
			return false;
		}
	}
public function get_services($table,$where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get($table);
		return $query->num_rows() > 0 ? $query->result() : false;
	}	

public function clientexist($client_id){
$q=$this->db->where(["user_id"=>$client_id])->get('vbs_bookings');
return $q->num_rows()>0?$q->num_rows():false;
}
public function getpromocodediscount($promo_code){
	$status=1;
	$q=$this->db->where(["promo_code"=>$promo_code,'statut'=>$status])->order_by('id',"desc")
		->limit(1)->get('vbs_u_discount');
    return $q->num_rows()>0?$q->row()->discount:false;
}
public function getdateofmaydec($pickdate){

   //$time="14:00";
   //$pickdate="17/07/2020";
	$getdate=explode("/",$pickdate);
		
        if( ($getdate[0]==1 || $getdate[0]==25) && ($getdate[1]==5 || $getdate[2]==12) ){
        return true;
        }else{
        return false;
        }
}
public function getnotworkingdate($carid,$pickdate){
 
  

   //$time="14:00";
   //$pickdate="17/07/2020";
      $getdate=explode("/",$pickdate);
      $day=$getdate[0];
      $month=$getdate[1];
       $day=ltrim($day,"0");
       $month=ltrim( $month,"0");

        
        $q=$this->db
		->where(["pirce_car_id"=>$carid,"day"=>$day,'month'=>$month])
		->limit(1)
		->get('vbs_notworking_days');
		
		return  $q->num_rows()>0 ? true : false;
   
}
public function getservicesbyid($table,$id){
$q=$this->db->where(['id'=>$id])->get($table);
return $q->row();
}
	public function booking_Configuration($table,$where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get($table);
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	public function getgoogleapikey($table){
	return $this->db->order_by('id',"desc")
		->limit(1)
		->get($table)
		->row();
	
}
public function getpricestatus(){
		return $this->db->order_by('id',"desc")
		->limit(1)
		->get("vbs_price_status")
		->row();
}
public function getcompanystatus(){
	return $this->db
        ->select(['com.id','com.name','com.address','com.address2','com.type','com.zip_code','com.city','com.street','com.phone','com.fax','com.email','com.logo','com.website','com.capital','com.sirft','com.numero_tva','com.licence','rg.name as regionname','rc.name as countryname'])
       ->from('vbs_company as com')
       ->join('vbs_regions as rg', 'com.region=rg.id','left')
        ->join('vbs_countries as rc', 'com.country=rc.id','left')
        ->order_by('com.id', 'DESC')
        ->limit(1)
        ->get()->row();
}
public function totalbookingnumber(){
		$query = $this->db->get(self::$table);
		return $query->num_rows();
	}
public function getpaymentmethod($table,$id){
$q=$this->db->where(['id'=>$id])->get($table);
return $q->row();
}
public function getbookingallrecordsbyclientid($clientid){
	  $q=$this->db
	  ->select(['booking.id','booking.user_id','booking.booking_ref','booking.pick_date','booking.pick_time','booking.pick_point','booking.drop_point','booking.distance','booking.cost_of_journey','booking.price','booking.totalprice','booking.payment_received','booking.is_conformed','booking.bookdate','booking.waiting_time','booking.return_time','booking.return_date','booking.start_time','booking.start_date','booking.end_time','booking.end_date','booking.regular','booking.returncheck','booking.wheelchair','booking.pickupcategory','booking.dropoffcategory','booking.last_action','car.id as carid','car.car_cat_name as carname','method.id as payment_id','method.name as payment_name','service.id as servicecat_id',
	  	'service.category_name as servicecat_name','ser.id service_id','ser.service_name as service_name'])
	  ->from('vbs_bookings as booking')
	  ->join('vbs_u_car_category as car', 'booking.car_id=car.id','left')
	  ->join('vbs_u_service as ser', 'booking.service_id=ser.id','left')
	  ->join('vbs_u_payment_method as method', 'booking.payment_method_id=method.id','left')
	  ->join('vbs_u_category_service as service', 'booking.service_category_id=service.id','left')
	  ->where('booking.user_id',$clientid)
	  ->order_by('booking.id', 'DESC')
	  ->get();
       return  $q->result();
}
public function getbookingallrecordsbysearch($where,$query,$otherquery){
        $query=urldecode($query);
         $otherquery=urldecode($otherquery);

       $this->db
	  ->select(['booking.id','booking.booktime','booking.pickzipcode','booking.pickcity','booking.dropzipcode','booking.dropcity','booking.user_id','booking.booking_ref','booking.pick_date','booking.pick_time','booking.pick_point','booking.drop_point','booking.distance','booking.cost_of_journey','booking.price','booking.totalprice','booking.payment_received','booking.is_conformed','booking.bookdate','booking.waiting_time','booking.return_time','booking.return_date','booking.start_time','booking.start_date','booking.end_time','booking.end_date','booking.regular','booking.returncheck','booking.wheelchair','booking.pickupcategory','booking.dropoffcategory','booking.last_action','car.id as carid','car.car_cat_name as carname','method.id as payment_id','method.name as payment_name','service.id as servicecat_id',
	  	'service.category_name as servicecat_name','ser.id service_id','ser.service_name as service_name','user.username as username'])
	  ->from('vbs_bookings as booking')
	  ->join('vbs_users as user', 'booking.user_id=user.id','left')
	  ->join('vbs_u_car_category as car', 'booking.car_id=car.id','left')
	  ->join('vbs_u_service as ser', 'booking.service_id=ser.id','left')
	  ->join('vbs_u_payment_method as method', 'booking.payment_method_id=method.id','left')
	  ->join('vbs_u_category_service as service', 'booking.service_category_id=service.id','left');
	  if(!empty($where)) $this->db->where($where); 
      if(!empty($query)){
        $this->db->like('user.username',$query);
      }  
      if(empty($query) &&  !empty($otherquery)){
      	 $this->db->like('booking.id',$otherquery);
      }
	  $q=$this->db->order_by('booking.id', 'DESC')->get();
	 // print_r($this->db->last_query());    
	 // exit();
       return  $q->result();
}
public function getusersbyrole($role){
$groupid=$this->db->where(['name'=>$role])->get("vbs_groups")->row()->id;
	if($groupid){
          $q=$this->db
           ->select(['u.id','u.civility','u.first_name','u.last_name'])
          ->from("vbs_users as u")
          ->join('vbs_users_groups as ug','u.id=ug.user_id')
          ->where(['ug.group_id'=>$groupid])
          ->get();
          return $q->result();
	}else{
        return false;
	}
}
public function getdbbookingrecord($table,$where){
	$q=$this->db->where($where)->get($table);
	return $q->row();
}
public function getdbmultiplebookingrecord($table,$where){
	$q=$this->db->where($where)->get($table);
	return $q->result();
}
public function getdbpassengersbookingrecord($where){

          $q=$this->db
           ->select(['p.id','p.civility','p.fname','p.lname','p.mobilephone','p.homephone','p.disablecatid','ds.name_of_disablecategory'])
          ->from("vbs_bookings as b")
          ->join('vbs_booking_passengers as bp','b.id=bp.booking_id')
          ->join('vbs_passengers as p','p.id=bp.passenger_id')
          ->join('vbs_u_disablecategory as ds','ds.id=p.disablecatid')
          ->where($where)
          ->get();
          return $q->result();
	
}
public function updatebookingrecord($table,$data,$where){
$this->db->where($where)->update($table, $data);
return true;
}
public function checkdataexist($table,$where){
$query=$this->db->where($where)->get($table);
return $query->num_rows() > 0 ? true : false;
}
//17/11/2020
public function getnotification($table,$where){

	$query = $this->db->where($where)->limit(1)->get($table);
	return $query->num_rows() > 0 ? $query->row() : false;
 }
 public function getbookingnotification($where){

       $query=$this->db
	  ->select(['bnt.id','bnt.date','bnt.time','bnt.issent','bnt.booking_id','bnt.notification_id'])
	  ->from('vbs_bookings_notification as bnt')
	  ->join('vbs_notifications as nt','bnt.notification_id=nt.id','left')
	  ->where($where)
	  ->limit(1)
	  ->get();
	 
      return $query->num_rows() > 0 ? $query->row() : false;
}
public function getsinglerecord($table,$where){
   $query = $this->db->where($where)->limit(1)->get($table);
	return $query->num_rows() > 0 ? $query->row() : false;
}
public function getmultiplerecord($table,$where){

    $query = $this->db->where($where)->get($table);
	return $query->num_rows() > 0 ? $query->result() : false;
}
public function getpaidpayment(){
$query = $this->db->query("SELECT SUM(totalprice) as paid
FROM vbs_bookings 
LEFT JOIN vbs_invoices ON vbs_invoices.booking_id=vbs_bookings.id
where vbs_invoices.status = 1");
return $query->num_rows() > 0 ? $query->row() : false;
}
public function getpendingpayment(){
$query = $this->db->query("SELECT SUM(totalprice) as pending
FROM vbs_bookings 
LEFT JOIN vbs_invoices ON vbs_invoices.booking_id=vbs_bookings.id
where vbs_invoices.status = 0");
return $query->num_rows() > 0 ? $query->row() : false;	
}
//17/11/2020
public function getsalespayment(){
$query = $this->db->query("SELECT SUM(totalprice) as sales
FROM vbs_bookings 
INNER JOIN vbs_invoices ON vbs_invoices.booking_id=vbs_bookings.id");
return $query->num_rows() > 0 ? $query->row() : false;
}
//end by sultan 
}
