<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Discount_Model extends CI_Model {

	function __construct(){
		parent::__construct();
	}
	public function getAllDiscount($where = [], $limit = false){
	if(!empty($where)) $this->db->where($where);
	if($limit != false) $this->db->limit($limit);
	$this->db->order_by('id','desc');
	$query = $this->db->get('vbs_u_discount');
	return $query->num_rows() > 0 ? $query->result() : false;
}
public function booking_Config_Add($table,$data){
		// $table = "vbs_u_discount";
		$this->db->trans_begin();
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $insert_id;
		}
	}
	public function booking_Config_Update($table,$data, $id){
		// $table = "vbs_u_discount";
		if($this->db->update($table, $data, ['id' => $id]))
			return true;
		else
			return false;
	}
	public function booking_Config_Delete($table,$id){
		return $this->db->delete($table, ['id' => $id]);
	}
		public function booking_Config_getAll($table,$where = [], $limit = false){
		if(!empty($where)) $this->db->where($where);
		if($limit != false) $this->db->limit($limit);
		$this->db->order_by('id','desc');
		$query = $this->db->get($table);
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	public function check_promo_code($table,$promocode){
       $q=$this->db->where("promo_code",$promocode)->get($table);
       return ($q->num_rows()>0)?true:false;
	}
	public function get_discount_record($table,$id){
		$q=$this->db->where("id",$id)->get($table);
		return $q->row();
	}
	public function get_service($id){
		$q=$this->db->where("id",$id)->get("vbs_u_service");
		return $q->row()->service_name;
	}
	public function get_service_category($id){
		$q=$this->db->where("id",$id)->get("vbs_u_category_service");
		return $q->row()->category_name;
	}
}
