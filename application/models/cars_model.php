<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Cars_Model extends CI_Model {

    function __construct(){
        parent::__construct();
    }


    public function getAllData($table,$where = [], $limit = false){
        if(!empty($where)) $this->db->where($where);
        if($limit != false) $this->db->limit($limit);
        $this->db->order_by('id','desc');
        $query = $this->db->get($table);
        return $query->num_rows() > 0 ? $query->result() : false;
    }
    public function getSingleRecord($table,$where = []){
        if(!empty($where)) $this->db->where($where);
        $this->db->order_by('id','desc');
        $query = $this->db->get($table);
        return $query->num_rows() > 0 ? $query->row() : false;
    }
     public function getOneRecord($table,$where = []){
        if(!empty($where)) $this->db->where($where);
        $this->db->order_by('id','asc');
        $this->db->limit('1');
        $query = $this->db->get($table);
        return $query->num_rows() > 0 ? $query->row() : false;
    }
    public function getMultipleRecord($table,$where){
    $query = $this->db->where($where)->get($table);
    return $query->num_rows() > 0 ? $query->result() : false;
    }

    public function createRecord($table,$data){

        $this->db->trans_begin();
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return $insert_id;
        }
        //print_r($this->db->last_query());    
        //exit();
    }
    public function createUpdate($table,$data, $id){

        if($this->db->update($table, $data, ['id' => $id]))
            return true;
        else
            return false;
    }

    public function createDelete($table,$where){
        return $this->db->delete($table, $where);
    }

    public function deleteCarProfile($car){
      $this->db->trans_begin();
       
       $carcustomreminderDel=$this->db->delete('vbs_cars_customreminders', ['car_id'=>$car->id]);
       $carreminderDel=$this->db->delete('vbs_cars_reminders', ['car_id'=>$car->id]);
       $carnotificationDel=$this->db->delete('vbs_cars_notification', ['car_id'=>$car->id]);

       $carprofileDel=$this->db->delete('vbs_carsadded', ['id'=>$car->id]);

      if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function getuser($id){
        $where =array('id'=> $id);
        $table='users';
        $query = $this->db->get_where($table, $where);
         if ($query->num_rows() > 0) {
            $query=  $query->row();
        return  $query = $query->civility.' '.$query->first_name .' '.$query->last_name;
        }
        else {
            return false;
        }
    }
   public function isdataalreayexist($table,$where){
     $query=$this->db->where($where)->get($table);
     return $query->num_rows() > 0 ? true : false;
    }
   public function isemailalreayexist($where,$table){
   $query=$this->db->where($where)->get($table);
   return $query->num_rows() > 0 ? true : false;
    }

   public function ajax_get_cities_listings($input){
        $this->db->where('region_id',$input);
        $query = $this->db->get("cities");
        return $query->result();
    }
    public function ajax_get_region_listing($input){
        $this->db->where('country_id',$input);
        $query = $this->db->get("vbs_regions");
        return $query->result();
    }
    public function getuniquerecord($table){
    return $this->db->order_by('id',"desc")
        ->limit(1)
        ->get($table)
        ->row();    
    }
}
