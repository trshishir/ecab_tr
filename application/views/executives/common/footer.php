   </div>

    <!--/.span9--> 

  </div>
<section class="footer-top">
    <div class="container">
        <div class="row">
            <div class="col-md-3 padding-lr">&nbsp;</div>
            <div class="col-md-2">
                <a href="<?php echo site_url();?>/welcome/contactUs"><span class="send-us-email"><i class="fa fa-envelope"></i> contact@navetteo.fr</span></a>
            </div>
            <div class="col-md-3">
                <span class="call-us" style="margin-right:20px"><i class="fa fa-phone"></i> Fax 01 85 09 02 33</span>
            </div>
            <div class="col-md-2">
                <span class="live-support"><i class="fa fa-wechat"></i> Live Support</span>
            </div>
            <div class="col-md-2">
                <a href="<?php echo site_url();?>/welcome/contactUs"><span class="ticket-support"><i class="fa fa-headphones"></i> Ticket Support</span></a>
            </div>
        </div>
    </div>
</section>
<section class="footer">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 padding-lr">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3" style="width:33%">
                    <div id="logo"><img src="<?php echo base_url(); ?>assets/system_design/images/navetteo-logo.png" /></div>
                    <div class="copyright-left">
                        <p><?php echo $this->lang->line('copyright');?></p>
                    </div>
                    <p class="credit-cards"><!--<label><?php echo $this->lang->line('cards_we_accept');?></label>--><img alt="Cards we accept" src="<?php echo base_url();?>/assets/system_design/images/credit-cards-logos.png"></p>
                    <p style="float:left"><a href="http://www.copyrightfrance.com/certificat-depot-copyright-france-28RF1G2.htm" target="_BLANK"><img src="<?php echo base_url();?>/assets/system_design/images/copyright-logo.gif" style="margin-top: 5px;margin-left: 0px"></a></p>
                </div>
              <div class="col-lg-9 col-md-9 col-sm-9"  style="width:67%">
                  <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        <div class="footer_div">
                          <div class="footer_heading">
                            <h5>Transfert Aeroport<?php echo $this->lang->line('transport_shuttle');?></h5>
                          </div>
                          <!--./footer_heading-->
                          <div class="footer_links">
                            <ul>
                              <li> <a href="<?php echo site_url();?>/services/3">Aeroport D'Orly</a></li>
                              <li><a href="<?php echo site_url();?>/services/4">Aeroport CDG</a></li>
                             <li><a href="<?php echo site_url();?>/services/5">Aeroport Beauvais</a></li>
                             <li><a href="<?php echo site_url();?>/services/28">Aeroport Le Bourget</a></li>
                             <li><a href="<?php echo site_url();?>/services/26">Aeroport Paris Vatry</a></li>
                            </ul>
                          </div>
                        </div>
                        <!--./footer_div--> 
                    </div>	
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        <div class="footer_div">
                          <div class="footer_heading">
                            <h5>Transfert Gare<?php echo $this->lang->line('transport_gare');?></h5>
                          </div>
                          <!--./footer_heading-->
                          <div class="footer_links">
                            <ul>
                              <li> <a href="<?php echo site_url();?>/services/6">Gare Du Nord</a></li>
                              <li><a href="<?php echo site_url();?>/services/7">Gare Montparnasse</a></li>
                             <li><a href="<?php echo site_url();?>/services/8">Gare Saint Lazard</a></li>
                             <li><a href="<?php echo site_url();?>/services/9">Gare De Lyon</a></li>
                             <li><a href="<?php echo site_url();?>/services/21">Gare DE L'est</a></li>
                            </ul>
                          </div>
                        </div>
                        <!--./footer_div--> 
                    </div>	  

                    <div class="col-lg-3 col-md-3 col-sm-4">
                        <div class="footer_div">
                          <div class="footer_heading">
                            <h5><?php echo $this->lang->line('transfer_parkgarden'); ?></h5>
                          </div>
                          <!--./footer_heading-->
                          <div class="footer_links">
                            <ul>
                                <li><a href="<?php echo site_url();?>/services/23"><?php echo $this->lang->line('transfer_disneyland'); ?></a></li>
                                <li><a href="<?php echo site_url();?>/services/25"><?php echo $this->lang->line('transfer_asterix'); ?></a></li>
                                <li><a href="<?php echo site_url();?>/services/10"><?php echo $this->lang->line('transfer_paristour'); ?></a></li>
                                <li><a href="<?php echo site_url();?>/services/11"><?php echo $this->lang->line('transfer_versailles'); ?></a></li>
                            </ul>
                          </div>
                        </div>
                        <!--./footer_div--> 
                    </div>    
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        <div class="footer_div">
                          <div class="footer_heading">
                            <h5><?php echo $this->lang->line('usefullinks_page');?></h5>
                          </div>
                          <div class="footer_links">
                            <ul>
                                <li><a href="<?php echo site_url(); ?>/welcome/prices"><?php echo $this->lang->line('ourprice_page');?></a></li>
                                <li><a href="<?php echo site_url(); ?>/welcome/zones"><?php echo $this->lang->line('zones_page');?></a></li>
                                <li><a href="<?php echo site_url(); ?>/welcome/fleet"><?php echo $this->lang->line('page_fleet');?></a></li>            
                             <li><a href="<?php echo site_url(); ?>/welcome/faqs"><?php echo $this->lang->line('FAQs');?></a></li>
                             <li><a href="<?php echo site_url(); ?>/welcome/contactUs"><?php echo $this->lang->line('contact_page');?></a></li>
                            </ul>
                          </div>
                        </div>
                    </div>
                    <!--<div class="col-lg-3 col-md-3 col-sm-4">
                        <div class="footer_div">
                          <div class="footer_heading">
                            <h5>Visites Guidees<?php echo $this->lang->line('visit_guides');?></h5>
                          </div>
                          <div class="footer_links">
                            <ul>
                              <li> <a href="<?php echo site_url();?>/services/10">Paris Tourr</a></li>
                              <li><a href="<?php echo site_url();?>/services/11">Chateaux De Versailles</a></li>
                             <li><a href="<?php echo site_url();?>/services/12">Fontainebleau</a></li>
                            <li><a href="<?php echo site_url();?>/services/22">Chantilly</a></li> 
                            </ul>
                          </div>
                        </div>
                    </div>
                      
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        <div class="footer_div">
                          <div class="footer_heading">
                            <h5>Circuits Touristiques<?php echo $this->lang->line('circuits_tourist');?></h5>
                          </div>
                          <div class="footer_links">
                            <ul>
                              <li><a href="<?php echo site_url();?>/services/13">Honfleur & Deauville</a></li>
                              <li><a href="<?php echo site_url();?>/services/14">Plages Des Debarquements</a></li>
                             <li><a href="<?php echo site_url();?>/services/15">Saint Malo & Monto Saint-Michel</a></li>
                            </ul>
                          </div>
                        </div>
                    </div>  -->
                    
                    </div>
              </div>
          </div>
      </div>
    </div>
    <!--./container--> 
  </section>
  <section class="bottom_footer">
    <div class="container">
        <div class="col-md-9 padding-lr">
            <div class="footer-quick-links">
            <ul>
                <li><a href="<?php echo site_url();?>/welcome/termsServices"><?php echo $this->lang->line('terms_conditions');?></a></li>
                <li><a href="<?php echo site_url();?>/welcome/privacyPolicy"><?php echo $this->lang->line('privacy_policy');?></a></li>
                <li><a href="<?php echo site_url();?>/welcome/contactUs"><?php echo $this->lang->line('report_abuse');?></a></li>
                <li><a href="<?php echo site_url();?>/welcome/legalNotice"><?php echo $this->lang->line('legal_notice');?></a></li>
            </ul>
            </div> 
        </div>
        <div class="col-md-3 padding-lr">
            <div>
                Page rendered in {elapsed_time} seconds. 
            </div>
        </div>
    </div>
  </section>   
<!--
<div class="footer">
    <div class="container"> <b class="copyright"> &copy; 2014  <?php echo $this->lang->line('our_car_booking');?> </b><?php echo $this->lang->line('all_rights_reserved');?>.
        <p style="float:right">Page rendered in {elapsed_time} seconds. </p>
    </div>
</div>
-->
<script src="<?php echo base_url();?>/assets/system_design/scripts/jquery.min.js"></script> 



 <script src="<?php echo base_url();?>assets/system_design/scripts/timepicki.js"></script>
    <script>
    	$('#timepicker1').timepicki({increase_direction:'up'});
		$('#timepicker2').timepicki({increase_direction:'up'});
 	 
    </script>

<!--Date Table--> 

<?php if(in_array("datatable",$css_type)) { ?>

<script type="text/javascript" language="javascript" src="<?php echo base_url();?>/assets/system_design/scripts/jquery.dataTables.js"></script> 

<script type="text/javascript" language="javascript" class="init">



$(document).ready(function() {

	$('#example').dataTable();

	$('.example').dataTable();

} );



	</script> 

	<?php } ?>

	<!--Date Table--> 

		

	<script src="<?php echo base_url();?>/assets/system_design/scripts/BeatPicker.min.js"></script> 

    <script src="<?php echo base_url();?>/assets/system_design/scripts/sidemenu-script.js"></script>

    

	<script src="<?php echo base_url();?>/assets/system_design/scripts/bootstrap.min.js" type="text/javascript"></script> 

	

	<!--Calendar--> 

	<?php if(in_array("calendar",$css_type)) { ?>

	<script src="<?php echo base_url();?>/assets/system_design/scripts/responsive-calendar.js"></script> 

	<script type="text/javascript">

      $(document).ready(function () {

        $(".responsive-calendar").responsiveCalendar({

          time: '2013-05',

          events: {

            "2013-04-30": {"number": 5, "url": "http://w3widgets.com/responsive-slider"},

            "2013-04-26": {"number": 1, "url": "http://w3widgets.com"}, 

            "2013-05-03":{"number": 1}, 

            "2013-06-12": {}}

        });

      });

    </script>

	<?php } ?>

	<!--Form elements-->

	<?php if(in_array("form",$css_type)) { ?>

	

	 <link href="<?php echo base_url();?>/assets/system_design/css/uniform.default.css" rel="stylesheet" media="screen">

        <link href="<?php echo base_url();?>/assets/system_design/css/chosen.min.css" rel="stylesheet" media="screen">

 

        <script src="<?php echo base_url();?>/assets/system_design/scripts/jquery.uniform.min.js"></script>

        <script src="<?php echo base_url();?>/assets/system_design/scripts/chosen.jquery.min.js"></script>

 

		<script type="text/javascript" src="<?php echo base_url();?>/assets/system_design/scripts/jquery.validate.min.js"></script>

		<script src="<?php echo base_url();?>/assets/system_design/scripts/form-validation.js"></script>
 
 
<?php if($gmaps == "true") { ?>
 <!--<script src="http://maps.googleapis.com/maps/api/js?libraries=places,geometry"></script>-->
 <!--script src="<?php echo base_url();?>assets/system_design/scripts/jquery.min.js"></script--> 
 <script src="<?php echo base_url();?>assets/system_design/scripts/jquery.geocomplete.js"></script>
 <script src="<?php echo base_url();?>assets/system_design/scripts/logger.js"></script>
 <script src="<?php echo base_url();?>assets/system_design/scripts/gmap3.js"></script>
<?php } ?>
<?php if ($this->router->method == 'map') { echo "map page;"?>
 <script src="http://maps.googleapis.com/maps/api/js?libraries=places,geometry"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/system_design/scripts/trackflight.js"></script>
<?php } else { ?>
 <script src="http://maps.googleapis.com/maps/api/js?callback=initMap&libraries=places,geometry"></script>
<?php } ?>
         <script>



 jQuery(document).ready(function() {  

 FormValidation.init();
	 <?php if($gmaps == "true") { ?> 
	 
	 Pickpoint = '';
	Droppoint = '';
	Pickup_time = '';
	$(".location").geocomplete({
          country: '<?php echo $site_settings->site_country;?>' 
	}).bind("geocode:result", function(event, result){
   		// $(this).val(JSON.stringify(result.formatted_address));		
                //$form_address = JSON.stringify(result.formatted_address);
                //$address = $form_address.replace('\"','');
   		//$(this).val($address);		
                $fieldName = $(this).attr('id');
                console.log("Field Name: "+$fieldName);
                $formatted_address = result.formatted_address;
                console.log("form address : "+$formatted_address);
   		$("#"+$fieldName).val($formatted_address);
                console.log($fieldName +" : "+$("#"+$fieldName).val());
                
   			
   			if(($('#local_pick').val()!='' && $('#local_drop').val()!='')) {
				Pickpoint = $('#local_pick').val();	
				Droppoint = $('#local_drop').val();
				Pickup_time = $('#timepicker1').val();
				page = $('#local_drop').attr("alt");
				
				
				
			}	
			
			
			if(Pickpoint!='' && Droppoint!='') {
				//alert(Pickpoint+" -->  "+Droppoint); 
				get_map(Pickpoint,Droppoint,Pickup_time,page);		
				$('#journey_details').fadeIn(1500);		
			}
     });
	<?php } ?>
			
		
	});
			
	function get_map(PickLocation,DropLocation,Pickup_time,page) {
		//alert('getmap.... boy....');
$("#dummy").gmap3({ 
 clear: {},

  getroute:{
    options:{
        origin:PickLocation,
        destination:DropLocation,
        travelMode: google.maps.DirectionsTravelMode.DRIVING,
		/*ENABLE IT IF MILES*/
		<?php if(isset($distance_unit) && $distance_unit=='mile') {?>
		unitSystem: google.maps.UnitSystem.IMPERIAL,
		<?php } ?>		
    },
    callback: function(results){
	//alert('in results');  
	var dist0 = results.routes[0].legs[0].distance.text;
//	 var dist=Math.round(dist0.split(" ")[0])+dist0.split(" ")[1];
	 var dist=dist0.split(" ")[0];
	var time = results.routes[0].legs[0].duration.text+" (Approx)"; 
	//alert('dist:'+dist);
	//$('#time_id').text(time);
	//$("#total_time").val(time);
	//$('#distance_id').text(dist0);
	$('#distance').val(dist0+"  "+time);		
					
			$.ajax({			 

			 type: 'POST',			  

			 url: "<?php echo site_url();?>/bookingsystem/getVechicles",
			 data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&distance='+dist+','+'&Pickup_time='+Pickup_time+'&page='+page,
			 cache: false,			 
			 success: function(data) {			
			 //alert(data);
			 $('#cars_data_list').show();
			 $('#cars_data_list').html(data);
			 }		  		
			
			});
	}
  }
});
} 
			
			
			
			
	 		$(function() {

			 

				$(".uniform_on").uniform();

				$(".chzn-select").chosen();

			 

	 

			});

			
		
        
  $("#return_journey").click(function(){
          
		   
	
	if($('#return_journey').is(":checked")) {
		
		$("#waiting_time_div").fadeIn(500);
		total = parseInt($("#car_cost").val())*2;
		$("#total_cost").val(total);
	
	} else {
      
		$("#waiting_time_div").fadeOut(500);
		value = parseInt($("#car_cost").val());
		$("#total_cost").val(value);
		
	}
        
   });
 

 $("#waiting_time").change(function(){
	
	ins = $('#waiting_time').val().split(' ');
	
	total = parseInt($("#car_cost").val()*2)+parseInt(ins[1]);
	
	$("#total_cost").val(total);
	
  });
 
 function setActive(id) {
		
		numid = id.split('_')[1];
		$('#waitnreturn').fadeIn(500);
		carCost = $('#cab_'+numid).val();	
		cost = (carCost.split("_")[1]);
		$('#car_cost').val(cost);
		$("#total_cost").val(cost); 
		
}
        $url = "<?php echo site_url(); ?>";          
        $lang = $url.split(".fr/");
        if($lang[1] == 'en') {
            $(".copyright-left").css("font-size","9px");
        }
        if($lang[1] == 'fr') {
            $(".copyright-left").css("font-size","9px");
        }
        
        $('svt > rect').attr("fill", "#f5f5f5");
        $(".top-calendar").click(function() {
           $(".calendar-widget").toggle();
        });

        </script>

		<?php } ?>

</body>

