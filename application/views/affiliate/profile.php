<?php 

$profile = json_decode(json_encode($admin_details), true); 
extract($profile);

?>
<section id="content">
    <?php $this->load->view('partner/common/breadcrumbs'); ?>
    <div class="row" style="margin-bottom:40px;">
        <div class="col-md-9 col-md-offset-1">
            <?php // echo "<PRE>";print_r($profile);echo "</PRE>"; ?>
                <?php echo form_open('partner/signup', array("id" => "partner_signupform")); ?>
                <div class="row">
                    <div class="col-md-6 form-group">  
                        <label> Statut </label>                                
                        <select class="form-control" name="statut" id="statut">
                            <option value="1">Independent</option>
                            <option value="2">Company</option>
                        </select>

                    </div>
                    <div class="col-md-6 form-group"> 
                        <div class="divCompany" style="display:none;">
                            <label>Company</label>
                            <?php echo form_input($company,$company); ?>
                            <?php echo form_error('company'); ?>
                        </div> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 form-group"> 
                        <label> Civility </label>                                
                        <select class="form-control" name="civility">
                            <option value="Mr">Mr</option>
                            <option value="Mrs">Mrs</option>
                        </select> 
                    </div>
                    <div class="col-md-5 form-group"> 
                        <label>First Name</label>
                        <?php echo form_input($first_name,$first_name); ?>
                        <?php echo form_error('first_name'); ?> 
                    </div>
                    <div class="col-md-5 form-group"> 
                        <label>Last Name</label>
                        <?php echo form_input($last_name,$last_name); ?>
                        <?php echo form_error('last_name'); ?> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group"> 
                        <label>Email</label>
                        <?php echo form_input($email,$email); ?>
                        <?php echo form_error('email'); ?> 
                    </div>
                    <div class="col-md-6 form-group"> 
                        <label>Phone</label>
                        <?php echo form_input($phone,$phone); ?>
                        <?php echo form_error('phone'); ?> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group"> 
                        <label>Address</label>
                        <?php echo form_textarea($address,$address1); ?>
                        <?php echo form_error('address'); ?> 
                    </div>
                    <div class="col-md-6 form-group"> 
                        <label>&nbsp;</label>
                        <?php echo form_textarea($address1,$address2); ?>
                        <?php echo form_error('address1'); ?> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group"> 
                        <label>City</label>
                        <?php echo form_input($city,$city); ?>
                        <?php echo form_error('city'); ?> 
                    </div>
                    <div class="col-md-6 form-group"> 
                        <label>Zipcode</label>
                        <?php echo form_input($zipcode,$zipcode); ?>
                        <?php echo form_error('zipcode'); ?> 
                    </div>
                </div>
                <div class="row">
                    <!-- <div class="col-md-6 form-group"> 
                        <label>Mobile</label>
                        <?php // echo form_input($mobile,$mobile); ?>
                        <?php // echo form_error('mobile'); ?> 
                    </div> -->
                    <div class="col-md-6 form-group"> 
                        <label>Fax</label>
                        <?php echo form_input($fax_no,$fax_no); ?>
                        <?php echo form_error('fax'); ?> 
                    </div>
                </div>
                 
                <div class="row">
                    <div class="col-md-12"> 
                        <input type="submit" class="button green_button" value="Update" style="margin-top: 30px;" />  
                    </div>
                </div>
                <?php echo form_close(); ?>
        </div>
    </div>
</div>
</section>    
<script>
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: {lat: 50.6371834, lng: 3.063017400000035}
        });
        var geocoder = new google.maps.Geocoder();

        geocodeAddress(geocoder, map);
    }

    function geocodeAddress(geocoder, resultsMap) {
        var address = "<?php echo $profile_info->address1 . "" . $profile_info->address2; ?>";
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                resultsMap.setCenter(results[0].geometry.location);
                var contentString = '<h3><strong><?php echo $profile_info->first_name . " " . $profile_info->last_name ?></strong></h3><p><?php echo $profile_info->address1 . "<br/>" . $profile_info->address2; ?></p>'
                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });

                var marker = new google.maps.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location
                });
                marker.addListener('click', function () {
                    infowindow.open(resultsMap, marker);
                });
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }
</script>
<script src="http://maps.googleapis.com/maps/api/js?libraries=places,geometry&callback=initMap"></script>
<script type="text/javascript">
    (function ($, W, D)
    {
        var JQUERY4U = {};

        JQUERY4U.UTIL =
                {
                    setupFormValidation: function ()
                    {
                        //Additional Methods			

                        $.validator.addMethod("lettersonly", function (a, b) {
                            return this.optional(b) || /^[a-z ]+$/i.test(a)
                        }, "<?php echo $this->lang->line('valid_name'); ?>");

                        $.validator.addMethod("phoneNumber", function (uid, element) {
                            return (this.optional(element) || uid.match(/^([0-9]*)$/));
                        }, "<?php echo $this->lang->line('valid_number'); ?>");


                        //form validation rules
                        $("#profile_form").validate({
                            rules: {
                                user_name: {
                                    required: true,
                                    lettersonly: true
                                },

                                email: {
                                    required: true,
                                    email: true
                                },
                                phone: {
                                    required: true,
                                    phoneNumber: true,
                                    rangelength: [10, 11]
                                },
                                first_name: {
                                    required: true,
                                    lettersonly: true

                                }
                            },

                            messages: {
                                user_name: {
                                    required: "<?php echo $this->lang->line('user_name_valid'); ?>"
                                },
                                email: {
                                    required: "<?php echo $this->lang->line('email_valid'); ?>"
                                },
                                phone: {
                                    required: "<?php echo $this->lang->line('phone_valid'); ?>"
                                },
                                first_name: {
                                    required: "<?php echo $this->lang->line('first_name_valid'); ?>"
                                }
                            },

                            submitHandler: function (form) {
                                form.submit();
                            }
                        });
                    }
                }

        //when the dom has loaded setup form validation rules
        $(D).ready(function ($) {
            JQUERY4U.UTIL.setupFormValidation();
        });

    })(jQuery, window, document);

</script>