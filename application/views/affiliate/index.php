<?php $locale_info = localeconv(); ?>
<style>
    @media only screen and (min-width: 1400px){
        .table-filter input, .table-filter select{
            max-width: 9% !important;
        }
        .table-filter select{
            max-width: 95px !important;
        }
        .table-filter .dpo {
            max-width: 90px !important;
        }
    }
</style>
<section id="content">
    <div class="row">
        <div class="col-md-12"><!--col-md-10 padding white right-p-->
            <div class="content">
                <?php $this->load->view('affiliate/common/breadcrumbs');?>
                <div class="row">
                    <div class="col-md-12">sara
                        <?php
                        $flashAlert =  $this->session->flashdata('alert');
                        if(isset($flashAlert['message']) && !empty($flashAlert['message'])){?>
                            <br>
                            <div style="padding: 5px 12px" class="alert <?=$flashAlert['class']?>">
                                <strong><?=$flashAlert['type']?></strong> <?=$flashAlert['message']?>
                                <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>
                        <div class="module">
                            <div class="module-head">
                                <!--<h3> <?php if(isset($title)) echo $title;?></h3>-->
                            </div>
                            <?php $this->load->model('calls_model'); ?>
                            <div class="module-body table-responsive">
                                <table id="example" class="cell-border" cellspacing="0" width="100%" data-selected_id="">
                                    <thead>
                                    <tr>
                                        <th class="no-sort text-center">#</th>
                                        <th class="column-id"><?php echo $this->lang->line('id');?></th>
                                        <th class="column-date"><?php echo $this->lang->line('date');?></th>
                                        <th class="column-time"><?php echo $this->lang->line('time');?></th>
                                        <th class="column-civility"><?php echo $this->lang->line('civility');?></th>
                                        <th class="column-first_name" ><?php echo $this->lang->line('first_name');?></th>
                                        <th class="column-last_name"><?php echo $this->lang->line('last_name');?></th>
                                        <!--<th class="column-subject"><?php echo $this->lang->line('subject');?></th> 
                                        <th class="column-email"><?php echo $this->lang->line('email');?></th>-->
                                        <th class="column-phone"><?php echo $this->lang->line('phone');?></th>
                                        <th class="column-subject"><?php echo $this->lang->line('subject');?></th>
                                        <th class="column-status"><?php echo $this->lang->line('status');?></th>
                                        <th class="column-since"><?php echo $this->lang->line('since');?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(isset($data) && !empty($data)):?>
                                        <?php $days = (int) $popups->request_closing_days_1; ?>
                                        <?php foreach($data as $key => $item):?>
                                            <?php 
                                                $now = time();
                                                $your_date = strtotime($item->last_action);
                                                $datediff = $now - $your_date;
                                                $diff = round($datediff / (60 * 60 * 24));
                                                if($days != 0){
                                                    if($diff >= $days){
                                                        if($item->status != "Closed"){
                                                            $this->calls_model->update(array('status'=>'Closed'), $item->id);
                                                        }
                                                    }
                                                }
                                            ?>
                                            <tr <?php if($item->unread == 1){ ?> class="unread" <?php }?>>
                                                <td>
                                                    <input type="checkbox" data-id="<?=$item->id;?>" class="checkbox singleSelect">
                                                </td>
                                                <td>
                                                    <a href="<?=site_url("affiliate/support/".$item->id."/edit")?>">
                                                        <?=create_timestamp_uid($item->created_at,$item->id);?>
                                                    </a>
                                                </td>
                                                <td><?=from_unix_date($item->created_at)?></td>
                                                <td><?=from_unix_time($item->created_at)?></td>
                                                <td><?=$item->civility;?></td>
                                                <td><?=$item->first_name;?></td>
                                                <td><?=$item->last_name;?></td>
                                                <!--<td><?php // $item->company;?></td> 
                                                <td><?php // $item->email;?></td>-->
                                                <td><?=$item->telephone;?></td>
                                                <td><?=$item->subject;?></td>
                                                <td><?=$item->status?></td>
                                                <td style="white-space: nowrap"><?=timeDiff($item->last_action);?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    </tbody>
                                </table>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/.module-->
            </div>
            <!--/.content-->
        </div>
    </div>
</section>

<div id="table-filter" class="hide">
    <input type="text" placeholder="Name" data-name="name" class="form-control">
    <select class="form-control" data-name="subject">
        <option value="">All Subject</option>
        <?php foreach(config_model::$call_subjects as $key => $subject):?>
            <option <?=set_value('subject',$this->input->post('subject')) == $subject ? "selected" : ""?> value="<?=$subject?>"><?=$subject?></option>
        <?php endforeach;?>
    </select>
    <input type="text" placeholder="Email" data-name="email" class="form-control">
    <input type="text" placeholder="Phone" data-name="phone" class="form-control">
    <select class="form-control" data-name="status">
        <option value="">All Status</option>
        <?php foreach(config_model::$status as $key => $status):?>
            <option <?=set_value('status',$this->input->post('status')) == $status ? "selected" : ""?> value="<?=$status?>"><?=$status?></option>
        <?php endforeach;?>
    </select>
    <input type="text" placeholder="From" data-name="date_from" class="dpo">
    <input type="text" placeholder="To" data-name="date_to" class="dpo">
</div>