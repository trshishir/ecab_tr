<?php $page_lang = 'lang="fr"'; ?>
<!DOCTYPE html>
<html <?php echo $page_lang; ?>>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="<?php if (isset($meta_keywords)) echo $meta_keywords; ?>"/>
        <meta name="description" content="<?php if (isset($meta_description)) echo $meta_description; ?>"/>
        <link rel='shortcut icon' href='<?php echo base_url(); ?>assets/system_design/images/favicon.ico'
              type='image/x-icon'/>
        <title><?php echo $title; ?></title>
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>assets/system_design/css/buttons.dataTables.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/system_design/css/wimpy.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/system_design/css/styles.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/system_design/css/callback.css" rel="stylesheet">
        <?php if (isset($site_settings->site_theme) && $site_settings->site_theme == "Red") { ?>
            <link href="<?php echo base_url(); ?>assets/system_design/css/cab-2ntheame.css" rel="stylesheet">
        <?php } else { ?>
            <link href="<?php echo base_url(); ?>assets/system_design/css/cab.css" rel="stylesheet">
            <!--<link href="<?php echo base_url(); ?>assets/system_design/css/custom-style.css?v=1.11" rel="stylesheet">-->
            <link href="<?php echo base_url(); ?>assets/system_design/css/affiliate-style.css?v=1.11" rel="stylesheet">
        <?php } ?>
        <link href="<?php echo base_url(); ?>assets/system_design/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <link href="//cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css"  rel="stylesheet">
        <script src="//cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/timepicker@1.13.0/jquery.timepicker.min.css" />
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="<?php echo base_url(); ?>assets/system_design/scripts/jquery.js"></script>
        <script src="<?php echo base_url(); ?>/assets/system_design/form_validation/js/jquery.validate.min.js"
        type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/assets/system_design/form_validation/js/additional-methods.min.js"
        type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/assets/system_design/ckeditor.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/assets/system_design/scripts/html2canvas.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/timepicker@1.13.0/jquery.timepicker.min.js"></script>
        <?php
                $this->load->model('calls_model');
                $this->load->model('request_model');
                $this->load->model('jobs_model');
                $this->load->model('support_model');
                $data = [];
                $data['request'] = $this->request_model->getAll();
                $data['jobs'] = $this->jobs_model->getAll();
                $data['calls'] = $this->calls_model->getAll();
                $data['support'] = $this->support_model->getAll();
                $drivers_request_count = $this->support_model->headerCount();

                foreach ($data as $key => $d) {
                    if ($d != false) {
                        foreach ($d as $i) {
                            if (!empty($i->status))
                                $this->data[$key][strtolower($i->status)] = isset($this->data[$key][strtolower($i->status)]) ? $this->data[$key][strtolower($i->status)] + 1 : 1;
                        }
                    }
                }
        ?>
    </head>
    <body>
    <div id="loaderDiv"><img src="<?= base_url()?>assets/theme/default/images/carloader.gif"></div>
    <div id="loaderTable"><img src="<?= base_url()?>assets/theme/default/images/carloader.gif"></div>
        <header style="">
            <div class="header-wrapper" style="padding: 0;">
                <div class="container body-border" > <!--  navbar-fixed-top-->
                    <div class="top-section">
                        <div class="row-fluid">
                            <div class="col-md-12">
                                <ul class="nav topbar-nav menu">
                                    <!--<li>
                                        <a href="#" data-toggle="modal" data-target="#myModal" class="btn btn-default btn-mar" >
                                            <i class="fa fa-comments-o"></i> <span class="badge badge-danger navbar-badge">0</span> Messenger
                                        </a>
                                    </li>
                                    <li>
                                        <script src="<?php echo base_url(); ?>assets/system_design/scripts/wimpy.js"></script>
                                        <! - - Wimpy Player Instance - s- >
                                        <div class="he-player" data-wimpyplayer data-media="http://direct.francebleu.fr/live/fb1071-midfi.mp3" data-skin="<?php // echo base_url(); ?>assets/system_design/scripts/wimpy.skins/001.tsv"></div>
                                    </li>-->
                                    <li>
                                        <!--<a href="Javascript:;" class="btn btn-default btn-mar" >
                                            <i id="topbar_time"><?php // echo date('l d F Y H:i:s'); ?>...</i>
                                        </a> -->
                                        <div class="btn btn-time" >
                                            <!--<span id="topbar_date"><?php // echo date('l d F Y'); ?></span>
                                            <span id="topbar_time">
                                                <?php // echo date('H:i'); ?>
                                            </span> -->
                                            <span id="topbar_date_time"><?php echo date('D d M Y H:i'); ?></span>
                                        </div>
                                        <script>
                                            var phptimezone = "<?php echo date_default_timezone_get(); ?>";
                                            // new Date().toLocaleString("en-US", {timeZone: "Europe/Paris"})
                                            $(document).ready(function () {
                                                var daysNames = [
                                                                "Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"];
                                                var monthsNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                                                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                                                var monthsNames2 = ["January", "February", "March", "April", "May", "June",
                                                                "July", "August", "September", "October", "November", "December"];
                                                setInterval(function () {
                                                                var d = new Date().toLocaleString("en-US", {timeZone: phptimezone});
                                                                d = new Date(d);
                                                                var date = `${daysNames[d.getDay()]} ${d.getDate()} ${monthsNames[d.getMonth()]} ${d.getFullYear()}`;
                                                                // var time = `${d.getHours()}:${d.getMinutes()}:${d.getSeconds()};
                                                                var time = `${d.getHours()}:${d.getMinutes()}`;
                                                                // $('#topbar_date').text(date);
                                                                // $('#topbar_time').text(time);
                                                                $('#topbar_date_time').text(date + ' ' + time);
                                                }, 1000);
                                                // $("#topbar_time,#topbar_date").datepicker({changeMonth: true,changeYear: true,});
                                                // $("#topbar_time").datepicker("setDate", new Date());
                                                $("#topbar_date_time").datepicker({changeMonth: true,changeYear: true,});
                                                $("#topbar_date_time").datepicker("setDate", new Date());
                                            });
                                        </script>
                                    </li>
                                    <!--<li>
                                        <?php // include_once('weather.php'); ?>
                                    </li> -->
                                    <li class="soperator-li">
                                        <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false" style="color: #111;">
                                            <span class='user-status online'></span>
                                            <span class="sales-operator-icon">
                                                <img src="<?php echo base_url(); ?>assets/system_design/images/user-160x160.png" alt="<?php echo $user->first_name . ' ' . $user->last_name; ?>" title="<?php echo $user->first_name . ' ' . $user->last_name; ?>" />
                                            </span>
                                            <span style="display: block;line-height: 10px;left: 6px;position: relative;font-size:12px;">Sales Operator</span>
                                            <span style="display: block;line-height: 10px;left: 6px;position: relative;top:2px;"><?php echo $user->civility . ' ' . $user->first_name . ' ' . $user->last_name; ?> </span>
                                            <span style="display: block;line-height: 10px;left: 6px;position: relative;top:3px;"><i class="fa fa-phone"></i> 01 22 54 22 00</span>
                                        </a>
                                    </li>
				    <li>
                                        <div class="top-support-link">
                                            <a href="<?php echo site_url(); ?>affiliate/support.php"><span class="ticket-support"><i class="fa fa-headphones"></i> Ticket</span></a>
                                        </div>
                                        <div class="top-support-link">
                                            <a href="Javascript:;" class="topbar-chat-btn">
                                                <span class="call-us" ><i class="fa fa-comments-o" ></i> Chat</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="foperator-li">
                                        <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false" style="color: #111;">
                                            <span class='user-status online'></span>
                                            <span class="sales-operator-icon">
                                                <img src="<?php echo base_url(); ?>assets/system_design/images/user-160x160.png" alt="<?php echo $user->first_name . ' ' . $user->last_name; ?>" title="<?php echo $user->first_name . ' ' . $user->last_name; ?>" />
                                            </span>
                                            <span style="display: block;line-height: 10px;left: 6px;position: relative;font-size:12px;">Support Operator</span>
                                            <span style="display: block;line-height: 10px;left: 6px;position: relative;top:2px;"><?php echo $user->civility . ' ' . $user->first_name . ' ' . $user->last_name; ?> </span>
                                            <span style="display: block;line-height: 10px;left: 6px;position: relative;top:3px;"><i class="fa fa-phone"></i> 01 22 54 22 00</span>
                                        </a>
                                    </li>
                                    <li>
                                        <div class="top-support-link">
                                            <a href="<?php echo site_url(); ?>affiliate/support.php"><span class="ticket-support"><i class="fa fa-headphones"></i> Ticket</span></a>
                                        </div>
                                        <div class="top-support-link">
                                            <a href="Javascript:;" class="topbar-chat-btn">
                                                <span class="call-us" ><i class="fa fa-comments-o" ></i> Chat</span>
                                            </a>
                                        </div>
                                    </li>
                                </ul>

                                <?php
                                $languages = $this->db->where('status', 1)->order_by('title', 'ASC')->get('vbs_languages')->result();
                                ?>
                                <ul class="nav topbar-nav menu pull-right">
                                    <li class="lang-switch">
                                        <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"> 
                                            <img class='en-icon' src="<?= base_url('assets/system_design/images/' . substr($this->config->item('language'),0,2) . '-icon.png'); ?>" />
                                            <span><?= ucwords($this->config->item('language')); ?><b class="caret"></b></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <?php
                                            foreach ($languages as $lang) :
                                                ?>
                                                <li>
                                                    <a href="<?= base_url() ?>global_controller/set_language/<?= $lang->title ?>">
                                                        <img class='en-icon' src="<?php echo base_url().$lang->flag ?>" />
                                                        <?= ucwords($lang->title); ?>
                                                    </a>
                                                </li>
                                            <?php
                                            endforeach;
                                            ?>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class="currency">
                                            <select name="department" class="form-control remove-apperance" >
                                                <option value="fa fa-usd" selected>&#x24; USD</option>
                                                <option value="fa fa-eur">&#x20ac; EURO</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li class="userli">
                                        <span class="greetings"><?php echo (CURRENT_DAY == 'PM') ? 'Good Evening' : 'Good Afternoon';?></span>
                                    </li>
                                    <li>
                                        <span class="user-box">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false" style="color: #111;">
                                                <span class='user-status online'></span>
                                                <?php echo $user->civility . ' ' . $user->first_name . ' ' . $user->last_name; ?> 
                                                <span class="user-icon">
                                                    <img src="<?php echo base_url(); ?>assets/system_design/images/user-160x160.png" alt="<?php echo $user->first_name . ' ' . $user->last_name; ?>" title="<?php echo $user->first_name . ' ' . $user->last_name; ?>" />
                                                </span>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="Javascript:;" class="btn-default"><i class="fa fa-fw fa-wrench"></i> <?php echo $this->lang->line('configurations'); ?></a></li>
                                                <li><a href="<?php echo site_url('admin/supplier'); ?>" class="btn-default"><i class="fa fa-user"></i> <?php echo $this->lang->line('supplier'); ?></a></li>
                                                <li><a href="Javascript:;" class="btn-default"><i class="fa fa-fw fa-info"></i> <?php echo $this->lang->line('help'); ?></a></li>
                                                
                                            </ul>
                                        </span>
                                    </li>
				    <li><a href="<?php echo site_url(); ?>affiliate/logout" class="logout"><i class="fa fa-fw fa-power-off"></i><?php echo $this->lang->line('log_out'); ?></a></li>	
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid secondary-header">
                        <div class="col-md-12 mt-pr-none"> <!--padding-p-l-->
                            <a href="<?php echo site_url(); ?>home.php">
                                <div class="logo">
                                    <img src="<?php echo base_url(); ?>assets/system_design/images/ecab-logo-client.png" alt="eCab Logo" title="eCab Logo" />
                                </div>
                            </a>
                         <!-- padding-p-r -->
                            <div class="pull-left outside-box">
                                <div class="service-box">
                                    <div id="service-i"><i  class="fa fa-money"></i></div>
                                    <div class="comm_stats">
                                        <span class="service-b"><?php echo '2 $'; // isset($this->data['calls']) ? array_pop(array_reverse($this->data['calls'])) : 0 ?></span>
                                    </div>
                                    <span class="service-p"><?php echo $this->lang->line('commissions'); ?></span>
                                    
                                </div>
                                <div class="service-box">
                                    <div id="service-i"><i class="fa fa-book"></i></div>
                                    <div class="pay_stats">
                                        <span class="service-b"><?php echo '1'; // isset($this->data['jobs']) ? array_pop(array_reverse($this->data['jobs'])) : 0 ?></span>
                                    </div>
                                    <span class="service-p"><?php echo $this->lang->line('payments'); ?></span>
                                    
                                </div>
                                <div class="service-box">
                                    <div id="service-i"><i class="fa fa-book"></i></div>
                                    <div class="cont_stats">
                                        <!--<span class="service-b"><?php // isset($this->data['request']) ? $this->data['request']['new'] : 0 ?></span>-->
                                        <span class="service-b"><?php echo '5'; // isset($this->data['request']) ? array_pop(array_reverse($this->data['request'])) : 0 ?></span>
                                    </div>
                                    <span class="service-p"><?php echo $this->lang->line('contracts'); ?></span>
                                    
                                </div>
                                <!--<div class="service-box">
                                    <div id="service-i"><i class="fa fa-car"></i></div>
                                    <div>
                                        <span class="service-b"><?php // isset($this->data['support']) ? $this->data['support']['new'] : 0 ?></span>
                                    </div>
                                    <span class="service-p">Booking Approved</span>
                                </div>-->
                                <div class="service-box">
                                    <div id="service-i"><i class="fa fa-book"></i></div>
                                    <div class="book_stats">
                                        <span class="service-b">10</span>
                                    </div>
                                    <span class="service-p"><?php echo $this->lang->line('bookings'); ?></span>
                                    
                                </div>
                                <div class="service-box">
                                    <div id="service-i"><i class="fa fa-pencil"></i></div>
                                    <div class="quote_stats">
                                        <span class="service-b"><?= isset($this->data['request']['new']) ? $this->data['request']['new'] : 0 ?></span>
                                    </div>
                                    <span class="service-p"><?php echo $this->lang->line('quotes'); ?></span>
                                    
                                </div>
                                <div class="service-box">
                                    <div id="service-i"><i class="fa fa-calculator"></i></div>
                                    <div class="inv_stats">
                                        <span class="service-b">12</span>
                                    </div>
                                    <span class="service-p"><?php echo $this->lang->line('invoices'); ?></span>
                                    
                                </div>
                                <div class="service-box passengers">
                                    <div id="service-i"><i class="fa fa-users"></i></div>
                                    <div class="pass_stats">
                                        <span class="service-b">0</span>
                                    </div>
                                    <span class="service-p"><?php echo $this->lang->line('passengers'); ?></span>
                                    
                                </div>
                                <div class="service-box">
                                    <div id="service-i"><i class="fa fa-envelope"></i></div>
                                    <div class="msg_stats">
                                        <span class="service-b">0</span>
                                    </div>
                                    <span class="service-p"><?php echo $this->lang->line('messages'); ?></span>
                                    
                                </div>
                                <div class="service-box">
                                    <div id="service-i"><i class="fa fa-support"></i></div>
                                    <div class="supp_stats">
                                        <span class="service-b">0</span>
                                    </div>
                                    <span class="service-p"><?php echo $this->lang->line('support'); ?></span>
                                    
                                </div>
                            </div>
                            &nbsp;&nbsp;
                            <?php if ($this->lang->lang() == 'fr') { ?>
                                <div class="col-md-4 right french-contact-number"><!--padding-p-r-->
                                <?php
                                //$phone = $site_settings->land_line;
                                echo "01 48 13 09 34";
                                ?>
                                    <p class="working-hours" style=" left:35px;">Du Lundi au Vendredi <br/>de 9h à 12h et de 14h
                                        à 18h</p>
                                </div>
                            <?php } else { ?>
                                <!-- <div class="col-md-4 right english-contact-number">
                            <?php
                                    //    $phone = $site_settings->phone;
                                    echo "01 48 13 09 34";
                            ?>
                                    <p class="working-hours" style="left:20px;">From Monday to Friday <br/>9h to 12h and from
                                        14h to 18h</p>
                                </div> -->
                            <?php } ?>
                            <div class="col-md-1 right">
                                <img src="<?php echo base_url(); ?>assets/system_design/images/call-us-girl.png"
                                     class="call-us-girl" alt="Call Us" title="Call Us"/></div>
                            <?php if ($this->lang->lang() == 'fr') { ?>
                                <div class="header-icons-fr">
                                    <img src="<?php echo base_url(); ?>assets/system_design/images/new_footer_header_images/logo1ssside.png"
                                         alt="Aeroport Service" width="250px" height="80px" title="Aeroport Service"/>
                                    <img src="<?php echo base_url(); ?>assets/system_design/images/new_footer_header_images/logoagain.png"
                                         alt="Aeroport Service" width="250px" height="80px" title="Aeroport Service"/>
                                    <img src="<?php echo base_url(); ?>assets/system_design/images/new_footer_header_images/disable.png"
                                         alt="Aeroport Service" width="250px" height="80px" title="Aeroport Service"/>
                                </div>
                            <?php } else if ($this->lang->lang() == 'ens') { ?>
                                <div class="header-icons-en">
                                    <img src="<?php echo base_url(); ?>assets/system_design/images/header/aeroport.png"
                                         alt="Airport Service" title="Airport Service"/>
                                    <img src="<?php echo base_url(); ?>assets/system_design/images/header/passengers.png"
                                         alt="Passengers" title="Passengers"/>
                                    <img src="<?php echo base_url(); ?>assets/system_design/images/header/circuit-touristiques.png"
                                         alt="Tours Circuit" title="Tours Circuit"/>
                                    <img src="<?php echo base_url(); ?>assets/system_design/images/header/transfert-gare.png"
                                         alt="Railway Transfer" title="Railway Transfer"/>
                                    <img src="<?php echo base_url(); ?>assets/system_design/images/header/airport-transfers.png"
                                         style="width:95px" alt="Airport Transfer" title="Airport Transfer"/>
                                    <img src="<?php echo base_url(); ?>assets/system_design/images/header/free-quotes-en.png"
                                         alt="Free Quotes" title="Free Quotes"/>
                                    <img src="<?php echo base_url(); ?>assets/system_design/images/header/24hr-fr.png"
                                         alt="24/7 Service" title="24/7 Service"/>
                                    <img src="<?php echo base_url(); ?>assets/system_design/images/header/disabled.png"
                                         alt="Disabled" title="Disabled"/>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row-fluid main-menu">
                        <div class="body-border" style=""> 
                            <div class="row-fluid">
                                <nav class="navbar navbar-navetteo menu-total">
                                    <div class="navbar-header">
                                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed nav-bar-btn" type="button">
                                            <span class="sr-only"><?php echo $this->lang->line('toggle_navigation'); ?></span> 
                                            <span class="icon-bar"></span> <span class="icon-bar"></span> 
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>
                                    <div class="collapse navbar-collapse res-menu" id="navbar">
                                        <ul class="nav navbar-nav menu">
                                            <li <?php if (isset($active_class)) { if ($active_class == "dashboard") echo 'class="active"'; } ?>>
                                                <a href="<?php echo site_url(); ?>affiliate/home.php"><i class="fa fa-tachometer" style="font-size: 15px;margin-right: 5px;"></i><span><?php echo $this->lang->line('home'); ?></span></a>
                                            </li>
                                            <li <?php if (isset($active_class)) { if ($active_class == "profile") echo 'class="active"'; } ?>>
                                                <a href="<?php echo site_url('affiliate/profile'); ?>"><i class="fa fa-user" style="font-size: 15px;margin-right: 5px;"></i><span><?php echo 'Profile'; // $this->lang->line('contact');  ?></span></a>
                                            </li>
                                            <li <?php if (isset($active_class)) { if ($active_class == "passengers") echo 'class="active"'; } ?>>
                                                <a href="<?php echo site_url('affiliate/passengers'); ?>"><i class="fa fa-users" style="font-size: 15px;margin-right: 5px;"></i><span><?php echo 'Passengers'; // $this->lang->line('contact');  ?></span></a>
                                            </li>
                                            <li <?php if (isset($active_class)) { if ($active_class == "contracts") echo 'class="active"'; } ?>>
                                                <a href="<?php echo site_url('affiliate/contracts'); ?>"><i class="fa fa-edit" style="font-size: 15px;margin-right: 5px;"></i><span><?php echo 'Contracts'; // $this->lang->line('contact');  ?></span></a>
                                            </li>
                                            <li <?php if (isset($active_class)) { if ($active_class == "quotes") echo 'class="active"'; } ?>>
                                                <a href="<?php echo site_url('affiliate/quotes'); ?>"><i class="fa fa-pencil" style="font-size: 15px;margin-right: 5px;"></i><span><?php echo 'Quotes'; // $this->lang->line('contact');  ?></span></a>
                                            </li>
                                            <li <?php if (isset($active_class)) { if ($active_class == "bookings") echo 'class="active"'; } ?>>
                                                <a href="<?php echo site_url('affiliate/bookings'); ?>"><i class="fa fa-book" style="font-size: 15px;margin-right: 5px;"></i><span><?php echo 'Bookings'; // $this->lang->line('contact');  ?></span></a>
                                            </li>
                                            <li <?php if (isset($active_class)) { if ($active_class == "sales") echo 'class="active"'; } ?>>
                                                <a href="<?php echo site_url('affiliate/sales'); ?>"><i class="fa fa-dollar" style="font-size: 15px;margin-right: 5px;"></i><span><?php echo 'Sales'; // $this->lang->line('jobs');  ?></span></a>
                                            </li>
                                            <li <?php if (isset($active_class)) { if ($active_class == "commissions") echo 'class="active"'; } ?>>
                                                <a href="<?php echo site_url('affiliate/commissions'); ?>"><i class="fa fa-registered" style="font-size: 15px;margin-right: 5px;"></i><span><?php echo 'Commissions'; // $this->lang->line('jobs');  ?></span></a>
                                            </li>
                                            <li <?php if (isset($active_class)) { if ($active_class == "payments") echo 'class="active"'; } ?>>
                                                <a href="<?php echo site_url('affiliate/payments'); ?>"><i class="fa fa-money" style="font-size: 15px;margin-right: 5px;"></i>Payments</a>
                                            </li>
                                            <li <?php if (isset($active_class)) { if ($active_class == "messages") echo 'class="active"'; } ?>>
                                                <a href="<?php echo base_url() ?>affiliate/messages.php"><i class="fa fa-envelope" style="font-size: 15px;margin-right: 5px;"></i>Messages</a> 
                                            </li>
                                             <li <?php if (isset($active_class)) { if ($active_class == "support") echo 'class="active"'; } ?>>
                                                <a href="<?php echo base_url() ?>affiliate/support.php"><i class="fa fa-support" style="font-size: 15px;margin-right: 5px;"></i>Support</a> 
                                            </li>
                                        </ul>
                                    </div>
                                    <!--/.nav-collapse -->
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        
    </header>
    <!-- Modal -->
    <div class="modal left fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="overflow: inherit;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?php
                            $CI2 = & get_instance();
                            $CI2->load->model('my_model');
                            $language2 = $CI->my_model->get_table_row_query2("SELECT * FROM vbs_header_settings");
                            // print_r($language2['chat_status']);
                    ?>
                    <div class="filter-div card-body text-center">
                        <label class="switch" style="margin-bottom: -6px;">
                            <input type="checkbox" id="chatboxswitch" value="active" <?php if ($language2['chat_status'] == "active") { echo "checked"; } else { } ?>>
                            <span class="slider round"></span>
                        </label>
                        <select class="form-control" style="display: inline-block; width: inherit !important;">
                            <option value=""> Category </option>
                            <option value="">a</option>
                            <option value="">b</option>
                            <option value="">c</option>
                            <option value="">d</option>
                        </select>
                        <select class="form-control" style="display: inline-block; width: inherit !important;">
                            <option value=""> Status </option>
                            <option value="">a</option>
                            <option value="">b</option>
                            <option value="">c</option>
                            <option value="">d</option>
                        </select> 
                    </div>
                </div>
                <div class="modal-body height-div3" style="padding-left: 0px; padding-right: 0px; padding-bottom: 0px;">
                    <!-- <p class="text-center">Loading...</p> -->
                    <div class=""> 
                        <audio id="pop" style="visibility: hidden;height: 0px;width: 0px;">
                            <source src="<?php echo base_url(); ?>assets/chat_audio_sound.mp3" type="audio/mpeg">
                        </audio>                      
                        <div class="height-div33">

                        </div>
                    </div>
                </div>
            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->
    <div class="row pt-3">
        <div class="chat-main" style="opacity: 0; z-index: -1; background-color: #fff;">
            <div class="col-md-12 chat-header rounded-top bg-primary text-white">
                <div class="row">
                    <div class="col-md-9 hidechatbx hide-chat-box2 username pl-2">
                        <i class="fa fa-circle text-success" aria-hidden="false" style="position: relative; top: 0px; left: 0px;"></i>
                        <h6 class="m-0" id="userchatname"></h6>
                    </div>
                    <div class="col-md-3 options text-right pr-2">
                        <b class="hide-chat-box2" style="font-size: 32px;position: relative;top: 2px;cursor: context-menu;"> - </b>
                        <i class="fa fa-times closechatbox" aria-hidden="false"></i>
                    </div>
                </div>
            </div>
            <div class="chat-content">
                <div class="col-md-12 chats border" id="chatsbox">

                </div>
                <div class="col-md-12 message-box border pl-2 pr-2 border-top-0" onclick="removeextrafunctionality()">
                    <?php echo form_open_multipart('insertadminchathistory', ['id' => 'adminreplymsgform', 'class' => 'form']); ?>
                    <!-- <form action="" id="adminreplymsgform" enctype="multipart/form-data" method="post"> -->
                    <div id="attachfile_div" class="text-center" style="float: right;width: 70px; display: none;">
                        <div id="otherfieattacmentdiv"></div>
                        <img src="" id="changeattachfileimage" style="width: 100%;border: 1px solid #ddd; border-radius: 4px; padding: 3px;">
                        <div class="text-center" id="attachfilename" style="line-height: 1;"></div>
                    </div>
                    <input type="text" name="messagetext" id="messagetext" class="pl-0 pr-0 w-100" placeholder="Type a message..." autocomplete="off"  required="required"/>
                    <input type="hidden" name="userid" id="input_userid">
                    <div class="tools" style="clear: both;">
                        <!-- <i class="fa fa-meh-o" aria-hidden="true"></i> -->
                        <input type="file" name="chatfile" id="chatfile" style="visibility: hidden;height: 0px;width: 0px;" onchange="addattachfileto_div()">
                        <i class="fa fa-paperclip" aria-hidden="true" onclick="addchatfile()"></i>
                        <a href="javascript:void(0)" title="" data-toggle="popover" data-html="true" data-placement="top" data-content="<ul class='quickchatreply'><li onclick='quickchatreplyfnc(1)'>Sure, let`s get started</li><li onclick='quickchatreplyfnc(2)'>Interesting, but I need info</li><li onclick='quickchatreplyfnc(3)'>I`m not able to do this</li></ul>"> Quick reply </a>
                        <input type="submit" name="submit" class="btn btn-primary" value="Send" style="float: right; padding: 5px; height: inherit !important; box-shadow: none !important; margin-bottom: 5px;">
                    </div>
                    <!-- </form> -->
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            getnewmessagesdata();
            var soundstatus;
            function getnewmessagesdata() {
                var status = 'active';
                $.ajax({
                    url: '<?php echo base_url(); ?>Messages/getallnewmessages',
                    method: 'get',
                    data: 'status=' + status,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (result)
                    {
                        var obj = JSON.parse(result);
                        if (obj.sound_status == 1 && obj.ischat_open == 0)
                        {
                            $('audio#pop')[0].play();
                            clearTimeout(soundstatus);
                            updatechatsound_status();
                        }
                        // alert(obj.sound_status+" HTML="+obj.chat_html_data);return;
                        console.log(1);
                        $('.height-div33').html(obj.chat_html_data);
                        setTimeout(getnewmessagesdata, 5000);
                    }
                });
            }
            function updatechatsound_status()
            {
                var status = "active";
                soundstatus = setTimeout(function () {
                    $.ajax({
                        url: '<?php echo base_url(); ?>Messages/changechatsound_status',
                        method: 'get',
                        data: 'status=' + status,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (result)
                        {
                            // $('audio#pop')[0].pause();
                            console.log(result);
                        }
                    });
                }, 800);
            }
            $('[data-toggle="popover"]').popover();
        });
        $('.hide-chat-box').click(function () {
            $('.chat-content').slideDown();
            $('.chat-main').css({'opacity': 1, 'z-index': 9999});
            $('.hidechatbx').addClass('hide-chat-box');
        });
        $('.hide-chat-box2').click(function () {
            $('.chat-content').slideToggle();
            $('.chat-main').css('opacity', 1);
            $('.hidechatbx').addClass('hide-chat-box2');
        });
        $('.height-div33').click(function () {
            $('.chat-content').slideDown();
            $('.chat-main').css('opacity', 1);
            $('.hidechatbx').addClass('hide-chat-box2');
        });

        $('#chatboxswitch').click(function () {
            if ($(this).prop("checked")) {
                // alert();return;
                var status = 'active';
                $.ajax({
                    url: '<?php echo base_url(); ?>Messages/changechatboxstatus',
                    method: 'get',
                    data: 'status=' + status,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (result)
                    {
                        console.log("Chat active");
                    }
                });
            } else
            {
                var status = 'inactive';
                $.ajax({
                    url: '<?php echo base_url(); ?>Messages/changechatboxstatus',
                    method: 'get',
                    data: 'status=' + status,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (result)
                    {
                        console.log("Chat Inactive");
                    }
                });
            }
        });
        function removeextrafunctionality()
        {
            $('.close').click();
        }
        function quickchatreplyfnc(val)
        {
            if (val == 1)
            {
                $('#messagetext').val('Sure, let`s get started');
            }
            if (val == 2)
            {
                $('#messagetext').val('Interesting, but I need info');
            }
            if (val == 3)
            {
                $('#messagetext').val('I`m not able to do this');
            }
            $('#adminreplymsgform').submit();
        }
        function addattachfileto_div()
        {
            $('#attachfile_div').css({'display': 'block'});
            var attachfilename_val = $('#chatfile').val();
            var extension = attachfilename_val.split('.').pop();
            // alert(extension);return;
            if (extension == "png" || extension == "PNG" || extension == "jpg" || extension == "jpeg" || extension == "gif")
            {
                var oFReader = new FileReader();
                oFReader.readAsDataURL(document.getElementById("chatfile").files[0]);
                oFReader.onload = function (oFREvent) {
                    document.getElementById("changeattachfileimage").src = oFREvent.target.result;
                }
            } else
            {
                $('#changeattachfileimage').attr('src', '<?php echo base_url(); ?>assets/chat_files/file_upload_image.png');
            }
            var attachfilename_vall = attachfilename_val.substring(attachfilename_val.lastIndexOf("\\") + 1, attachfilename_val.length);
            $('#attachfilename').text(attachfilename_vall);
            $('#messagetext').attr("required", false);
        }
        var specificuserSettimeout;
        function getspecificuserchathistory(userid, user_name)
        {
            clearTimeout(specificuserSettimeout);
            var status = 'active';
            $.ajax({
                url: '<?php echo base_url(); ?>Messages/getallspecificusermessages',
                method: 'get',
                data: 'userid=' + userid,
                cache: false,
                contentType: false,
                processData: false,
                success: function (result)
                {
                    $('#userchatname').text(user_name);
                    $('#input_userid').val(userid);
                    $('.chat-main').css({'z-index': 9999, 'opacity': 1});
                    $('#chatsbox').html(result);
                    var elem = document.getElementById('chatsbox');
                    elem.scrollTop = elem.scrollHeight;
                    specificuserSettimeout = setTimeout(getspecificuserchathistory.bind(null, userid, user_name), 3000);
                }
            });
        }

        $('#adminreplymsgform').on('submit', function (e) {
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                url: '<?php echo base_url(); ?>insertadminchathistory',
                method: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (result)
                {
                    // alert(result);
                    $('#attachfile_div').css({'display': 'none'});
                    $('#messagetext').val('');
                    $('#chatfile').val('');
                    $('#messagetext').attr("required", true);
                    $('#chat_ul').append(result);
                    $('.popover').hide();
                    var elem = document.getElementById('chatsbox');
                    elem.scrollTop = elem.scrollHeight;
                }
            });
        });
        $('.closechatbox').on('click', function () {
            $('.chat-main').css({'opacity': 0, 'z-index': -1});
            clearTimeout(specificuserSettimeout);
            $.ajax({
                url: '<?php echo base_url(); ?>Messages/change_is_chat_open_status_to_off',
            method: 'get',
            data: 'status=1',
            cache: false,
            success: function (result)
            {
                console.log('Sound off');
            }
        });
    });
    function addchatfile()
    {
        $('#chatfile').click();
    }
    // window.setInterval(function() {
    var elem = document.getElementById('chatsbox');
    elem.scrollTop = elem.scrollHeight;
    // }, 2000);
</script>