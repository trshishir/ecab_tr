</header>
<div class="container body-border"> 
    <div class="breadcrumb">
        <div class="row">
            <aside class="nav-links">
                <ul>
                    <li> <a href="<?php echo site_url(); ?>"> <?php echo $this->lang->line('home_page'); ?>  </a> </li>
                    <li class="active"><a href="javascript:void(0)">&nbsp;<?php if (isset($sub_heading)) echo $sub_heading; ?> </a></li>
                </ul>
            </aside>
        </div>
    </div>
    <div class="error-box">
        <div class="row">
            <div class="col-md-12" style="text-align: center;">
                <h1>404</h1>
                <h3>Page Not Found</h3>
                <p>Sorry! We could't find the page you were clicked for.</p>
                <p><a href='Javascript:history.go(-1);'>Click here</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="text-align: left;">
                <?php 
                        $ci =& get_instance();
                        $sql = $ci->db->last_query();
                        $message = "Query:".$sql . "<br/>";
                        $message .= "No:".$this->db->_error_number() . "<br/>";
                        $message .= "Message:".$this->db->_error_message() . "<br/>";
                        $trace = debug_backtrace();
                        $output = 'UserLog: ' . $trace[1]["class"] . '::' . $trace[1]['function'] . ' - ';
                        if (is_array($message)) {
                            $message = print_r($message, true);
                        }
                        $output .= $message;
                        echo $output; 
                ?>
            </div>
        </div>
    </div>
</div>