<section id="content">
    <?php $this->load->view('users/common/breadcrumbs'); ?>
    <?php echo form_open('client/profile', array("id" => "client_profileform")); ?>
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6 form-group">  
                    <label> Statut </label>                                
                    <select class="form-control" name="statut" id="statut" tabindex="1">
                        <option value="1" <?php echo ($statut == 1) ? 'selected=selected' : '';?>>Independent</option>
                        <option value="2" <?php echo ($statut == 2) ? 'selected=selected' : '';?>>Company</option>
                    </select>
                </div>
                <div class="col-md-6 form-group"> 
                    <div class="divCompany" style="display:none;">
                        <label>Company</label>
                        <?php echo form_input($company); ?>
                        <?php echo form_error('company'); ?>
                    </div> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 form-group"> 
                    <label>Email</label>
                    <?php echo form_input($email); ?>
                    <?php echo form_error('email'); ?> 
                </div>
                <div class="col-md-6 form-group"> 
                    <label>Phone</label>
                    <?php echo form_input($phone); ?>
                    <?php echo form_error('phone'); ?> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 form-group"> 
                    <label>City</label>
                    <?php echo form_input($city); ?>
                    <?php echo form_error('city'); ?> 
                </div>
                <div class="col-md-6 form-group"> 
                    <label>Zipcode</label>
                    <?php echo form_input($zipcode); ?>
                    <?php echo form_error('zipcode'); ?> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 form-group"> 
                    <label>Password</label>
                    <?php echo form_input($password); ?>
                    <?php echo form_error('password'); ?> 
                </div> 
                <div class="col-md-6 form-group"> 
                    <label>Confirm Password</label>
                    <?php echo form_input($confirm_password); ?>
                    <?php echo form_error('confirm_password'); ?> 
                </div>  
            </div> 
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-2 form-group"> 
                    <label> Civility </label>                                
                    <select class="form-control" name="civility" tabindex="3">
                        <option value="Mr">Mr</option>
                        <option value="Miss">Miss</option>
                        <option value="Mrs">Mrs</option>
                        <option value="Mme">Mme</option>
                    </select>
                </div>
                <div class="col-md-4 form-group"> 
                    <label>First Name</label>
                    <?php echo form_input($first_name); ?>
                    <?php echo form_error('first_name'); ?> 
                </div>
                <div class="col-md-6 form-group"> 
                    <label>Last Name</label>
                    <?php echo form_input($last_name); ?>
                    <?php echo form_error('last_name'); ?> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group"> 
                    <label>Address</label>
                    <?php echo form_textarea($address); ?>
                    <?php echo form_error('address'); ?> 
                </div>
                <div class="col-md-6 form-group"> 
                    <label>&nbsp;</label>
                    <?php echo form_textarea($address1); ?>
                    <?php echo form_error('address1'); ?> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group"> 
                    <label>Mobile</label>
                    <?php echo form_input($mobile); ?>
                    <?php echo form_error('mobile'); ?> 
                </div> 
                <div class="col-md-6 form-group"> 
                    <label>Fax</label>
                    <?php echo form_input($fax); ?>
                    <?php echo form_error('fax'); ?> 
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12"> 
            <div class="pull-right">
                <input type="submit" class="btn btn-default" value="Save" tabindex="16" />  
                <input type="button" class="btn btn-default" value="Cancel" onclick="return hideAdd();" tabindex="17" />  
            </div>
        </div>
    </div> 
<?php echo form_close(); ?>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        //Additional Methods            
        $.validator.addMethod("lettersonly", function (a, b) {
            return this.optional(b) || /^[a-z ]+$/i.test(a)
        }, "<?php echo $this->lang->line('valid_name'); ?>");

        $.validator.addMethod("phoneNumber", function (uid, element) {
            return (this.optional(element) || uid.match(/^([0-9]*)$/));
        }, "<?php echo $this->lang->line('valid_phone_number'); ?>");


        $.validator.addMethod("pwdmatch", function (repwd, element) {
            var pwd = $('#password').val();
            return (this.optional(element) || repwd == pwd);
        }, "<?php echo $this->lang->line('valid_passwords'); ?>");

        //form validation rules
        $("#client_profileform").validate({
            rules: {
                statut: {
                    required: true
                },
                company: {
                    required: function () {
                        return $("#statut").val() == "2";
                    }
                },
                first_name: {
                    required: true,
                    lettersonly: true
                },
                email: {
                    required: true,
                    email: true
                },
                phone: {
                    required: true,
                    phoneNumber: true,
                    rangelength: [10, 11]
                } 
            },
            messages: {
                statut: {
                    required: "Please select the statut"
                },
                company: {
                    required: "Please enter the company"
                },
                first_name: {
                    required: "<?php echo $this->lang->line('first_name_valid'); ?>"
                },
                email: {
                    required: "<?php echo $this->lang->line('email_valid'); ?>"
                },
                phone: {
                    required: "<?php echo $this->lang->line('phone_valid'); ?>"
                } 
            },
            submitHandler: function (form) {
                form.submit();
            }
        });

        $('#statut').on('change', function () {
            status = $(this).val();
            $('.divCompany').hide();
            if (status == 1) {
                $('.divCompany').hide();
            } else if (status == 2) {
                $('.divCompany').show();
            }
        });
        
        <?php if ($user_data->company_name != "" ) : ?>
                $('.divCompany').show();
        <?php endif; ?>
    });

</script>