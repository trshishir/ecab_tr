
<style>
   .truncate {

  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
    .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
  background:none;
}
  .nav-tabs > li.active {
    background: linear-gradient(#ffffff, #ffffff 25%, #d0d0d0) !important;
    border-bottom: none;
    }
  .nav-tabs > li {
    background-image: linear-gradient(to bottom, #fff 0, #e0e0e0 100%);
    border-left: 1px solid #ccc !important;
    height: 55px;
    margin: 0px !important;
    }

    .nav-tabs > li > a {

    color: #616161 !important;
    font-size: 12px;
    padding-left: 10px;
    display: block !important;
    width: 100% !important;
    height: 100% !important;
    text-transform: uppercase;
    font-weight: bold;
    transition: 0.2s;
    outline: none;
    border: none;
    border-radius: 0px;
}
.dataTables_wrapper .dataTables_filter {
    float: left;
    text-align: left;
    margin-left: 5px;
}
input[type="search"]{
    border:1px solid #cccccc;
    padding-left: 5px;
    border-radius: 0px;
}
input[type="search"]:focus{
    border:1px solid #cccccc !important;
}
     
    .tab-pane{
        padding: 30px 30px 30px 14px;
    }
    .configTable{
        padding: 0px;
    }
    .dataTables_wrapper .dataTables_filter input {
    margin-right: 2px;
    margin-left: 0 !important;
    max-width: 100% !important;
    width: 100% !important;
    float: right;
    }
    .dataTables_wrapper{
        margin-top: 20px;
    }
    .nav-tabs li a {
    color: #616161 !important;
    font-size: 11px;
    margin: 0px !important;
    padding: 15px;
    display: block !important;
    width: 100% !important;
    height: 100% !important;
    text-transform: uppercase;
    font-weight: bold;
    transition: 0.2s;
    outline: none;
}
.nav-tabs {
    border-bottom: none;
}
    
  .nav-tabs li a:hover,.nav-tabs li a:focus {
    /*background: linear-gradient(to bottom, #ececec 0%, #ececec 39%, #f0efef 39%, #b7b3b3 100%) !important;
    background:linear-gradient(to bottom,  #c1c1c1 0%, #ececec 39%, #ececec 39%, #fbfbfb 100%) !important;*/
    background: linear-gradient(to bottom, #c1c1c1 4%,#ececec 30%,#ececec 50%,#b7b3b3 100%) !important;
    color: #616161 !important;
    cursor: pointer;
}
.nav-tabs li.active a {
    /*background:linear-gradient(to bottom,  #c1c1c1 0%, #ececec 39%, #ececec 39%, #fbfbfb 100%) !important;*/
    background: linear-gradient(to bottom, #c1c1c1 4%,#ececec 30%,#ececec 50%,#b7b3b3 100%) !important;
}
/*
button.btn:hover, input.btn:hover{
    background-color: #e0e0e0;
    background-position: 0 -15px;
} */
</style>

<style>
    /* label > input {
        visibility: hidden;
    }
    label {
        display: block;
        margin: 0 0 0 -10px;
        padding: 0 0 20px 0;
        height: 20px;
        width: 150px;
    }

    label > img {
        display: inline-block;
        padding: 0px;
        height:10px;
        width:10px;
        background: none;
    }
    label > input:checked +img {
        background: url(http://cdn1.iconfinder.com/data/icons/onebit/PNG/onebit_34.png);
        background-repeat: no-repeat;
        background-position:center center;
        background-size:10px 10px;
    }
    .form-control {
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    } */
    /* button:active, .btn:active, .button:active, .dataTables_paginate span.paginate_active {
        background-color: #f1f1f1;
        border-color: #b2b2b2 #c7c7c7 #c7c7c7 #b2b2b2;
        -webkit-box-shadow: inset 0 2px 1px rgba(0, 0, 0, 0.1);
        -moz-box-shadow: inset 0 2px 1px rgba(0, 0, 0, 0.1);
        box-shadow: inset 0 2px 1px rgba(0, 0, 0, 0.1);
    }

    input.date{
        width: 100%;
        float: left;
        height: 34px;
    } */
    .tab-content .btn, button.btn {
    min-height: 28px;
    border: 1px solid #bbb;
    min-width: 80px;
    background-image: linear-gradient(to bottom, #fff 0, #e0e0e0 100%);
    background-repeat: repeat-x;
    border-color: #dbdbdb;
    text-shadow: 0 1px 0 #fff;
    border-color: #ccc;
}
.delete-icon {
    background: url(http://uniqueweb.co.in/project/ecabapp//assets/delete-icon.png) no-repeat left center !important;
    padding: 0px 0px 0px 22px;
}
.save-icon {
    background: url(http://uniqueweb.co.in/project/ecabapp//assets/save-icon.png) no-repeat left center !important;
    padding: 0px 0px 0px 22px;
}
</style>
<section id="content">
    <div class="listDiv"><!--col-md-10 padding white right-p-->
    <?php $this->load->view('users/common/breadcrumbs');?>
        <?php $this->load->view('users/common/alert');?>
        <?php echo $this->session->flashdata('message'); ?>
 <div>
<div class="row">
  <div class="ListBookings" >
  
 
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
                    <tr>
                                
                                 <th class="text-center column-id">id</th>
                                 <th class="text-center column-first_name" >Service</th>
                                 <th class="text-center column-first_name" >Service Category</th>
                                 <th class="text-center column-civility">Date</th>
                                 <th class="text-center column-civility">Time</th>                                
                                 <th class="text-center column-civility">Pickup</th>                               
                                 <th class="text-center column-civility">Dropoff</th>
                                 <th class="text-center column-civility">Car Category</th>
                                 <th class="text-center column-civility">Return</th>
                                 <th class="text-center column-civility">Regular</th>
                                 <th class="text-center column-civility">Wheelchair</th>
                                 <th class="text-center column-civility">Price</th>
                                 <th class="text-center column-civility">Payment</th>
                                 <th class="text-center column-civility">Statut</th>
                                 <th class="text-center column-since"><?php echo $this->lang->line('since');?></th>
                    </tr>
          </thead>
           <tbody>
                        <?php if(isset($bookings) && !empty($bookings)):?>

                                <?php foreach($bookings as $key => $item):?>

                                    <tr>

                                       

                                        <td class="text-center">

                                           
                                              <?php
                                                 $pickdate;
                                                 if(empty($item->pick_date) || $item->pick_date==null){
                                                    $pickdate=$item->start_date;
                                                 }else{
                                                  $pickdate=$item->pick_date;
                                                 }
                                               ?>
                                          
                                                <?=create_timestampdmy_uid($pickdate,$item->id);?>

                                           

                                        </td>
                                       
                                         <td class="text-center"><?= $item->service_name;?></td>
                                          <td class="text-center"><?= $item->servicecat_name;?></td>
                                         <?php if(empty($item->pick_date)): ?>
                                             <td class="text-center"><?= from_unix_date($item->start_date); ?></td>
                                         <?php else: ?>
                                            <td class="text-center"><?= from_unix_date($item->pick_date); ?></td>
                                         <?php endif; ?>
                                         <?php if(empty($item->pick_time)): ?>
                                            <td class="text-center"><?= $item->start_time; ?></td>
                                        <?php else: ?>
                                        <td class="text-center"><?= $item->pick_time; ?></td>
                                          <?php endif; ?>

                                        <td class="text-center"><?=$item->pick_point?></td>
                                        <td class="text-center"><?=$item->drop_point?></td>
                                        <td class="text-center"><?= $item->carname;?></td>
                                         <?php if($item->returncheck==1): ?>
                                           <td class="text-center"> Yes </td>
                                          <?php else: ?>
                                             <td class="text-center">No</td>
                                         <?php endif;?>
                                         <?php if($item->regular==1): ?>
                                           <td class="text-center"> Yes </td>
                                          <?php else: ?>
                                             <td class="text-center">No</td>
                                         <?php endif;?>  
                                          <?php if($item->wheelchair==1): ?>
                                           <td class="text-center">Yes</td>
                                          <?php else: ?>
                                             <td class="text-center">No</td>
                                         <?php endif;?>  
                                        <td class="text-center"><?=$item->totalprice;?> €</td>
                                        <td class="text-center"><?=$item->payment_name;?></td>                                    
                             
                                       <?php
                                   
                                           if($item->is_conformed=="pending"){
                                            echo "<td class='text-center'><span class='label label-warning'>Pending</span></td>";
                                           }
                                           elseif($item->is_conformed=="confirm"){
                                              echo "<td class='text-center'><span class='label label-success'>Confirmed</span></td>";
                                           }
                                           elseif($item->is_conformed=="cancelled"){
                                          echo "<td class='text-center'><span class='label label-danger'>Cancelled</span></td>";
                                           }
                                           else{
                                             echo "<td class='text-center'></td>";
                                           }
                                       ?>
                                    

                                        <td class="text-center" style="white-space: nowrap"><?=timeDiff($item->last_action);?></td>

                                    </tr>

                                <?php endforeach; ?>

                            <?php endif; ?>
                    </tbody>
      </table>
      <br>
    </div>
  </div>
</div>

</div>
</div>
</div>
</section>

