
<style>
   .truncate {

  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
    .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
  background:none;
}
  .nav-tabs > li.active {
    background: linear-gradient(#ffffff, #ffffff 25%, #d0d0d0) !important;
    border-bottom: none;
    }
  .nav-tabs > li {
    background-image: linear-gradient(to bottom, #fff 0, #e0e0e0 100%);
    border-left: 1px solid #ccc !important;
    height: 55px;
    margin: 0px !important;
    }

    .nav-tabs > li > a {

    color: #616161 !important;
    font-size: 12px;
    padding-left: 10px;
    display: block !important;
    width: 100% !important;
    height: 100% !important;
    text-transform: uppercase;
    font-weight: bold;
    transition: 0.2s;
    outline: none;
    border: none;
    border-radius: 0px;
}
.dataTables_wrapper .dataTables_filter {
    float: left;
    text-align: left;
    margin-left: 5px;
}
input[type="search"]{
    border:1px solid #cccccc;
    padding-left: 5px;
    border-radius: 0px;
}
input[type="search"]:focus{
    border:1px solid #cccccc !important;
}
     
    .tab-pane{
        padding: 30px 30px 30px 14px;
    }
    .configTable{
        padding: 0px;
    }
    .dataTables_wrapper .dataTables_filter input {
    margin-right: 2px;
    margin-left: 0 !important;
    max-width: 100% !important;
    width: 100% !important;
    float: right;
    }
    .dataTables_wrapper{
        margin-top: 20px;
    }
    .nav-tabs li a {
    color: #616161 !important;
    font-size: 11px;
    margin: 0px !important;
    padding: 15px;
    display: block !important;
    width: 100% !important;
    height: 100% !important;
    text-transform: uppercase;
    font-weight: bold;
    transition: 0.2s;
    outline: none;
}
.nav-tabs {
    border-bottom: none;
}
    
  .nav-tabs li a:hover,.nav-tabs li a:focus {
    /*background: linear-gradient(to bottom, #ececec 0%, #ececec 39%, #f0efef 39%, #b7b3b3 100%) !important;
    background:linear-gradient(to bottom,  #c1c1c1 0%, #ececec 39%, #ececec 39%, #fbfbfb 100%) !important;*/
    background: linear-gradient(to bottom, #c1c1c1 4%,#ececec 30%,#ececec 50%,#b7b3b3 100%) !important;
    color: #616161 !important;
    cursor: pointer;
}
.nav-tabs li.active a {
    /*background:linear-gradient(to bottom,  #c1c1c1 0%, #ececec 39%, #ececec 39%, #fbfbfb 100%) !important;*/
    background: linear-gradient(to bottom, #c1c1c1 4%,#ececec 30%,#ececec 50%,#b7b3b3 100%) !important;
}
/*
button.btn:hover, input.btn:hover{
    background-color: #e0e0e0;
    background-position: 0 -15px;
} */
</style>
<style media="screen">
/* select { */

/* styling */
/* padding: 5px !important;
background: url(<?php echo base_url();?>/assets/arrow.png) no-repeat right #ffffff !important;
    -webkit-appearance: none; */
    /* background-size:
        25px 25px,
        25px 25px,
        13px 13.5em !important; */
/* }
.dataTables_paginate{
  display: inline !important;
}
.dataTables_filter{
  float: left !important;
}
.dataTables_filter label{
  float: left !important;
  margin-left:2px;
}
.dataTables_filter label input[type="search"]{
  float: left !important;
  max-width: 150px !important;
} */

</style>
<style>
    /* label > input {
        visibility: hidden;
    }
    label {
        display: block;
        margin: 0 0 0 -10px;
        padding: 0 0 20px 0;
        height: 20px;
        width: 150px;
    }

    label > img {
        display: inline-block;
        padding: 0px;
        height:10px;
        width:10px;
        background: none;
    }
    label > input:checked +img {
        background: url(http://cdn1.iconfinder.com/data/icons/onebit/PNG/onebit_34.png);
        background-repeat: no-repeat;
        background-position:center center;
        background-size:10px 10px;
    }
    .form-control {
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    } */
    /* button:active, .btn:active, .button:active, .dataTables_paginate span.paginate_active {
        background-color: #f1f1f1;
        border-color: #b2b2b2 #c7c7c7 #c7c7c7 #b2b2b2;
        -webkit-box-shadow: inset 0 2px 1px rgba(0, 0, 0, 0.1);
        -moz-box-shadow: inset 0 2px 1px rgba(0, 0, 0, 0.1);
        box-shadow: inset 0 2px 1px rgba(0, 0, 0, 0.1);
    }

    input.date{
        width: 100%;
        float: left;
        height: 34px;
    } */
    .tab-content .btn, button.btn {
    min-height: 28px;
    border: 1px solid #bbb;
    min-width: 80px;
    background-image: linear-gradient(to bottom, #fff 0, #e0e0e0 100%);
    background-repeat: repeat-x;
    border-color: #dbdbdb;
    text-shadow: 0 1px 0 #fff;
    border-color: #ccc;
}
.delete-icon {
    background: url(http://uniqueweb.co.in/project/ecabapp//assets/delete-icon.png) no-repeat left center !important;
    padding: 0px 0px 0px 22px;
}
.save-icon {
    background: url(http://uniqueweb.co.in/project/ecabapp//assets/save-icon.png) no-repeat left center !important;
    padding: 0px 0px 0px 22px;
}
</style>
<section id="content">
    <div class="listDiv"><!--col-md-10 padding white right-p-->
    <?php $this->load->view('users/common/breadcrumbs');?>
        <?php $this->load->view('users/common/alert');?>
        <?php echo $this->session->flashdata('message'); ?>
 <div>
<div class="row">
  <div class="ListPassenger" >
  <input type="hidden" class="chk-Addpassenger-btn" value="">
   <input type="hidden" class="Addpassengerfullid" value="">
  <!-- <div class="col-md-12">
      <div class="toolbar"style="float: right; margin-bottom:5px;">
        <button class="btn btn-sm btn-default" onclick="Passengeradd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>&nbsp;
        <button class="btn btn-sm btn-default" onclick="PassengerEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>&nbsp;
        <button class="btn btn-sm btn-default" onclick="PassengerDelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button>&nbsp;
      </div>
    </div> -->
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" >#</th>
            <th class="text-center"><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center"><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center"><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" style="width: 10% !important;"><?php echo $this->lang->line('added_by'); ?></th>    
            <th class="text-center">Name</th>           
            <th class="text-center">Mobile Phone</th>
            <th class="text-center">Disable Category</th>           
            <th class="text-center" >Since</th>

          </tr>
          </thead>
          <tbody>
              <?php 
             
              if (!empty($passenger_data)): ?>
              <?php $cnt=1; ?>
              <?php foreach($passenger_data as $key => $item):?>
                  <tr>
                    <td class="no-sort text-center"style="width:2%;">
                        <input type="checkbox" class="chk-mainpassenger-template" data-input="<?=$item->id?>">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="PassengeridEdit('<?=$item->id?>','<?=date("dmY", strtotime($item->created_at)) .'0000'. $item->id;?>')"><?=date("dmY", strtotime($item->created_at)) .'0000'. $item->id;?></a></td>
                      <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                      <td class="text-center"><?=date("H:i:s", strtotime($item->since))?></td>
                       <td class="text-center truncate"><?php
                        $user=$this->passenger_model->getuser($item->clientid);
                        $user=str_replace(".", " ", $user);
                        echo $user;
                        ?></td>
                    
                    
                      <td class="text-center"><?= $item->civility.' '.$item->fname.' '.$item->lname  ;?></td>
                      <td class="text-center"><?=$item->mobilephone ;?></td>
                     
                      <td class="text-center"><?php echo  $this->passenger_model->getdiscategory('vbs_u_disablecategory',$item->disablecatid);  ?></td>
                      <td class="text-center"><?= timeDiff($item->since) ?></td>

                  </tr>
                  <?php $cnt++; ?>
              <?php endforeach; ?>
              <?php endif; ?>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>
<?=form_open("client/passengers/passengeradd")?>
<!-- <form action="<?=base_url();?>client/passengers/vatadd" class="" method="post"  enctype="multipart/form-data"> -->
<div class="Passengeradd" style="display: none;" >
  <div class="col-md-12">
  <div class="col-md-3" style="margin-top: 5px;">
    <div class="form-group"> 
        <span> Civility </span>                                
        <select class="form-control" name="civility" required>
            <option value="Mr">Mr</option>
            <option value="Mrs">Mrs</option>
        </select> 
    </div>
  </div>
   <div class="col-md-3" style="margin-top: 5px;">
    <div class="form-group">
      <span>First Name</span>
        <input type="text" class="form-control" name="fname" placeholder="" value="" required>
    </div>
  </div>
  <div class="col-md-3" style="margin-top: 5px;">
    <div class="form-group">
      <span>Last Name</span>
        <input type="text" class="form-control" name="lname" placeholder="" value="" required>
    </div>
  </div>
</div>
<div class="col-md-12">
   <div class="col-md-3" style="margin-top: 5px;">
    <div class="form-group">
      <span>Mobile Phone</span>
        <input type="text" class="form-control" name="mobilephone" placeholder="" value="" required>
    </div>
  </div>
   <div class="col-md-3" style="margin-top: 5px;">
    <div class="form-group">
      <span>Home Phone</span>
        <input type="text" class="form-control" name="homephone" placeholder="" value="" required>
    </div>
  </div>

  <div class="col-md-3" style="margin-top: 5px;">
    <div class="form-group">
      <span>Category of Disable</span>
      <select class="form-control"  name="disablecatid" required >
        <option value="">Select</option>
        <?php foreach ($disablecategories as $disable): ?>
          <option value="<?= $disable->id;  ?>"><?= $disable->name_of_disablecategory;  ?></option>
        <?php endforeach; ?>

      </select>
    </div>
  </div>
</div>

  <div class="col-md-12 clearfix" style="padding-right: 70px;">
  <button  class="btn"style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
    <button type="button" class="btn" style="float:right; margin-left:7px;height:34px;" onclick="cancelPassenger()"><span class="fa fa-close"> Cancel </span></button>

  <?php echo form_close(); ?>
</div>
</div>
<div class="passengerEdit" style="display:none">
  <?=form_open("client/passengers/passengeredit")?>
  <div class="passengerEditajax">
  </div>
  <div class="col-md-12 clearfix" style="padding-right: 70px;">
  <button  class="btn"style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
    <button type="button" class="btn" style="float:right; margin-left:7px;height:34px;" onclick="cancelPassenger()"><span class="fa fa-close"> Cancel </span></button>

  </div>
  <?php echo form_close(); ?>
</div>
<div class="col-md-12 passengerDelete" style="margin-top: 15px; display: none;">
  <?=form_open("client/passengers/deletepassenger")?>
    <input  name="tablename" value="vbs_job_statut" type="hidden" >
    <input type="hidden" id="passengerdeletid" name="delet_passenger_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn"style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

    <button type="button" class="btn" style="cursor: pointer;" onclick="cancelPassenger()">No</button>
  <?php echo form_close(); ?>
</div>
</div>
</div>
</div>
</section>
<script>

 
  function Passengeradd()
{
   $('#adddisbreadcrumb').remove();
   $(".breadcrumb").append("<span id='adddisbreadcrumb'> > Add passenger</span>");
 setupDivPassenger();
 $(".Passengeradd").show();
}

function cancelPassenger()
{
  $('#adddisbreadcrumb').remove();
   $('#editdisbreadcrumb').remove();
  setupDivPassenger();
  $(".ListPassenger").show();
}


function PassengerEdit()
{
   
    var val = $('.chk-Addpassenger-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }

    $('#editdisbreadcrumb').remove();
  var passengerid=$('.Addpassengerfullid').val();

   $(".breadcrumb").append("<span id='editdisbreadcrumb'> > "+passengerid+"</span>");
        setupDivPassenger();
        $(".passengerEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'client/passengers/get_ajax_passenger'; ?>',
                data: {'passenger_id': val},
                success: function (result) {
                   

                  document.getElementsByClassName("passengerEditajax")[0].innerHTML = result;
                 
                }
            });
}
function PassengeridEdit(id,fullid){

  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      $('#editdisbreadcrumb').remove();
   

   $(".breadcrumb").append("<span id='editdisbreadcrumb'> > "+fullid+"</span>");
        setupDivPassenger();
        $(".passengerEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'client/passengers/get_ajax_passenger'; ?>',
                data: {'passenger_id': val},
                success: function (result) {
                   

                  document.getElementsByClassName("passengerEditajax")[0].innerHTML = result;
                  
                }
            });
}
function PassengerDelete()
{
  var val = $('.chk-Addpassenger-btn').val();
  if(val != "")
  {
  setupDivPassenger();
  $(".passengerDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivPassenger()
{
  // $(".passengerEditEdit").hide();
  $(".Passengeradd").hide();
  $(".passengerEdit").hide();
  $(".ListPassenger").hide();
  $(".passengerDelete").hide();

}
$('input.chk-mainpassenger-template').on('change', function() {
  $('input.chk-mainpassenger-template').not(this).prop('checked', false);
   var parent= $(this).parent();
  var sibling=$(parent).next().html();
  var id = $(this).attr('data-input');
  $('.chk-Addpassenger-btn').val(id);
  $('#passengerdeletid').val(id);
 
   $('.Addpassengerfullid').val(sibling);
});


</script>
<script>
  $(document).ready(function () {
    $(window).on('load', function() {
       passengerEvent();
      
  
      });
     function passengerEvent(){

     
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button type="button" id="passengeraddfunc" class="dt-button buttons-copy buttons-html5"  onclick="Passengeradd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="PassengerEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="PassengerDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
   
    
  
   });

</script>
