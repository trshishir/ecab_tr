<script type="text/javascript">
    function showAdd() {
        hideForms();
        $('.addDiv').show();
        //showAddHead();
    }
    function hideForms() {
        $('.addDiv').hide();
        $('.listDiv').hide();
    }
    function hideAdd() {
        hideForms();
        $('.listDiv').show();
    }
    (function ($, W, D) {
        var JQUERY4U = {};

        JQUERY4U.UTIL =
                {
                    setupFormValidation: function ()
                    {
                        //Additional Methods            
                        $.validator.addMethod("lettersonly", function (a, b) {
                            return this.optional(b) || /^[a-z ]+$/i.test(a)
                        }, "<?php echo $this->lang->line('valid_name'); ?>");

                        $.validator.addMethod("phoneNumber", function (uid, element) {
                            return (this.optional(element) || uid.match(/^([0-9]*)$/));
                        }, "<?php echo $this->lang->line('valid_phone_number'); ?>");


                        $.validator.addMethod("pwdmatch", function (repwd, element) {
                            var pwd = $('#password').val();
                            return (this.optional(element) || repwd == pwd);
                        }, "<?php echo $this->lang->line('valid_passwords'); ?>");

                        //form validation rules
                        $("#jobseeker_addform").validate({
                            rules: {
                                statut: {
                                    required:true
                                },
                                company: {
                                    required:  function(){
                                                    return $("#statut").val() == "2";
                                              }
                                },
                                first_name: {
                                    required: true,
                                    lettersonly: true
                                },
                                email: {
                                    required: true,
                                    email: true
                                },
                                phone:{
                                        required: true,
                                        phoneNumber: true,
                                        rangelength: [10, 11]
                                },
                                password:{
                                        required: true,
                                        rangelength: [8, 30]
                                },
                                confirm_password: {
                                    required:true,
                                    pwdmatch:true
                                }
                            },
                            messages: {
                                statut: {
                                    required:"Please select the statut"
                                },
                                company: {
                                    required:"Please enter the company"
                                },
                                first_name: {
                                    required: "<?php echo $this->lang->line('first_name_valid');?>"
                                },
                                email:{
                                    required: "<?php echo $this->lang->line('email_valid');?>"
                                },
                                phone:{
                                        required: "<?php echo $this->lang->line('phone_valid');?>"
                                },
                                password: {
                                    required: "<?php echo $this->lang->line('password_valid');?>"
                                },
                                password_confirm:{
                                        required: "<?php echo $this->lang->line('confirm_password_valid');?>"
                                }
                            }, 
                            submitHandler: function(form) {
                                form.submit();
                            }
                        });
                    }
                }

        //when the dom has loaded setup form validation rules
        $(D).ready(function ($) {
            JQUERY4U.UTIL.setupFormValidation();
        });

    })(jQuery, window, document);
    
</script>
<section id="content">
    <div class="listDiv">
        <?php $this->load->view('admin/common/breadcrumbs'); ?>
        <div class="row-fluid">
            <?php   $flashAlert = $this->session->flashdata('alert');
                    if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
            ?>
                <br>
                <div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
                    <strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
                    <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php   } ?>

            <div class="module">
                <?php echo $this->session->flashdata('message'); ?>
                <div class="module-head">
                       <div class="module-filter">
                           <div class="col-md-7">
                                <?php echo form_open('jobseekers', array("id" => "search_filter")); ?>
                                    <div class="row  row-no-gutters padding-b">
                                        <div class="col-md-2">
                                             <input type="text" name="search_name" id="search_name" placeholder="Name" class="input-large form-control" data-name="name">
                                        </div>
                                        <div class="col-md-2">
                                             <input type="text" name="search_email" id="search_email" placeholder="Email" data-name="email" class="form-control">
                                        </div>
                                        <div class="col-md-2">
                                             <input type="text" name="search_phone" id="search_phone" placeholder="Phone" data-name="phone" class="form-control">
                                        </div>
                                        <div class="col-md-1">
                                            <input type="text" name="date_from" id="date_from" placeholder="From" data-name="date_from" class="dpo">
                                        </div>
                                        <div class="col-md-1">
                                            <input type="text" name="date_to" id="date_to" placeholder="To" data-name="date_to" class="dpo">
                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control" name="status" id="status" data-name="status">
                                                 <option value="">All Status</option>
                                                 <option value="New">New</option>
                                                 <option value="Pending">Pending</option>
                                                 <option value="Replied">Replied</option>
                                                 <option value="Closed">Closed</option>
                                             </select>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="submit" name="search" id="search" value="Search" class="btn btn-sm btn-default" />
                                        </div>
                                     </div>
                                <?php echo form_close(); ?>
                                </div>
                            <div class="col-md-5 no-right-pad">
                               <div class="dt-buttons">
                                   <a href="<?php echo site_url() ?>jobseeker/add" class="btn btn-sm btn-default"><i class="fa fa-plus"></i> Add</a> 
                                   <a href="Javascript:;" class="btn btn-sm btn-default editBtn"><i class="fa fa-pencil"></i> Edit</a> 
                                   <a href="Javascript:;" class="btn btn-sm btn-default delBtn"><i class="fa fa-trash"></i> Delete</a>
                                   <!--<a href="#" class="btn btn-sm btn-default delBtn"><i class="fa fa-download"></i> Import</a>-->
                                   <a href="Javascript:;" class="btn btn-sm btn-default excelDownload"><i class="fa fa-download"></i> Excel</a>
                                   <a href="Javascript:;" class="btn btn-sm btn-default csvDownload"><i class="fa fa-download"></i> CSV</a>
                                   <a href="Javascript:;" class="btn btn-sm btn-default pdfDownload"><i class="fa fa-download"></i> PDF</a>
                               </div>
                           </div>
                        </div>
                </div>
                <div class="clearfix"></div>
                <div class="module-body table-responsive">
                    <table id="manageJobseekerTable" class="table table-bordered table-striped  table-hover dataTable" cellspacing="0" width="100%" data-selected_id="">
                        <thead>
                            <tr>
                                <th class="no-sort text-center"><input type='checkbox' name='allJobseekers' id="allJobseekers" value='' onclick="Javascript:selectAllJobseekers();" /></th>
                                <th class="column-id"><?php echo $this->lang->line('id'); ?></th>
                                <th class="column-date"><?php echo "Date"; ?></th>
                                <th class="column-date"><?php echo "Time"; ?></th>
                                <th class="column-name" ><?php echo $this->lang->line('name'); ?></th>
                                <th class="column-email"><?php echo $this->lang->line('email'); ?></th>
                                <th class="column-phone"><?php echo $this->lang->line('phone'); ?></th>
                                <th class="column-status"><?php echo $this->lang->line('status'); ?></th>
                                <th class="column-since"><?php echo "Since"; ?></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
</section>
</div>
 <script>
    $(document).ready(function() {
        var manageTable = $('#manageJobseekerTable').DataTable({
            'ajax': '<?php echo site_url() ?>jobs/fetchJobseekersData',
            'order': [],
            'paging': true,
            'pageLength': 15,
            'columnDefs': [ { "targets": 0, "className": "text-center", "width": "4%" },
                            { "targets": 1, "className": "text-center", "width": "4%" },
                            { "targets": 2, "className": "text-center", "width": "4%" },
                            { "targets": 3, "className": "text-center", "width": "4%" },
                            { "targets": 4, "className": "text-center", "width": "10%" },
                            { "targets": 5, "className": "text-center", "width": "8%" },
                            { "targets": 6, "className": "text-center", "width": "4%" },
                            { "targets": 7, "className": "text-center", "width": "5%" },
                            { "targets": 8, "className": "text-center", "width": "8%" }
                          ] 
        });
        
        $(".pdfDownload").on('click',function() {
           alert('PDF Download inprogress...'); 
        });
        $(".csvDownload").on('click',function() {
           alert('CSV Download inprogress...'); 
        });
        $(".excelDownload").on('click',function() {
           alert('Excel Download inprogress...'); 
        });
        
        // Search form validation rules
        $("#search_filter").validate({
            rules: {
                search_name: { required:false }
            },
            messages: {
                search_name: {
                    required:"Please enter the name"
                }
            },
            submitHandler: function(form) {
                $.ajax({
                        url: '<?php echo site_url() ?>jobs/jobseekerSearchData',
                        type: 'post',
                        data: $("#search_filter").serialize(), // /converting the form data into array and sending it to server
                        dataType: 'json',
                        success: function (response) {
                            console.log(response.data);
                            //  $("#manageTable tbody").empty();
                            //if (response.success === true) {
                            console.log("Table" + $.fn.dataTable.isDataTable('#manageJobseekerTable'));

                            if ($.fn.dataTable.isDataTable('#manageJobseekerTable') == false) {
                                $('#manageJobseekerTable').DataTable({
                                    'data': response.data,
                                    'order': [],
                                    'paging': true,
                                    'pageLength': 15,
                                    'columnDefs': [ { "targets": 0, "className": "text-center", "width": "4%" },
                                                    { "targets": 1, "className": "text-center", "width": "4%" },
                                                    { "targets": 2, "className": "text-center", "width": "4%" },
                                                    { "targets": 3, "className": "text-center", "width": "4%" },
                                                    { "targets": 4, "className": "text-center", "width": "10%" },
                                                    { "targets": 5, "className": "text-center", "width": "8%" },
                                                    { "targets": 6, "className": "text-center", "width": "4%" },
                                                    { "targets": 7, "className": "text-center", "width": "5%" },
                                                    { "targets": 8, "className": "text-center", "width": "8%" }
                                                  ]
                                });
                            } else {
                                var table = $('#manageJobseekerTable').DataTable();
                                table.destroy();
                                $('#manageJobseekerTable').DataTable({
                                    'data': response.data,
                                    'order': [],
                                    'paging': true,
                                    'pageLength': 15,
                                    'columnDefs': [ { "targets": 0, "className": "text-center", "width": "4%" },
                                                    { "targets": 1, "className": "text-center", "width": "4%" },
                                                    { "targets": 2, "className": "text-center", "width": "4%" },
                                                    { "targets": 3, "className": "text-center", "width": "4%" },
                                                    { "targets": 4, "className": "text-center", "width": "10%" },
                                                    { "targets": 5, "className": "text-center", "width": "8%" },
                                                    { "targets": 6, "className": "text-center", "width": "4%" },
                                                    { "targets": 7, "className": "text-center", "width": "5%" },
                                                    { "targets": 8, "className": "text-center", "width": "8%" }
                                                  ]
                                });
                            }
                        }
                    })
            }
        });
    });
</script>