
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
  <!--client passenger driver cars -->
 
      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 booking_serivce_inner_tab quotestatushiddendiv" style="padding-left:0px;width: 18%;">
      <div class="form-group">
       <input type="hidden" name="invoice_id" id="invoiceidfieldvalue" value="<?= $invoices->id ?>">
       <span style="font-weight: 900;">Invoice Statut</span>
             <select name="invoice_status"  class="servicecategoryinput" required>
                   <option value="">Statut</option>
                  
                   <option <?= ($invoices->status =='0' ? 'selected' :''); ?> value="0">Pending</option>
                   <option <?= ($invoices->status =='1' ? 'selected' :''); ?> value="1">Paid</option>
                   <option <?= ($invoices->status =='2' ? 'selected' :''); ?> value="2">Cancelled</option>
                 
             </select>
                  
              <?php if(isset($service_form['driver']['error']) && !empty($service_form['driver']['error'])): ?>  
                  <div class='invalid-error'><?= $service_form['driver']['error']?></div>
              <?php endif; ?>
                  
        </div>
    </div>
    
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 booking_serivce_inner_tab" style="padding-left:0px;width: 18%;">
      <div class="form-group">
       <span style="font-weight: 900;">Client</span>
             <select name="client_field"  class="servicecategoryinput" id="edituserisclient"  onchange="editgetpassengers()" required>
                   <option value="">&#xf007; Clients</option>
                  <?php foreach($clients_data as $key => $item): ?>
                   <option <?= ($client_id==$item->id)?"selected":""; ?> value="<?= $item->id ?>">&#xf007; <?= $item->civility.' '.$item->first_name.' '.$item->last_name; ?></option>
                   <?php endforeach; ?>
             </select>
                  
              <?php if(isset($service_form['client']['error']) && !empty($service_form['client']['error'])): ?>  
                  <div class='invalid-error'><?= $service_form['client']['error']?></div>
              <?php endif; ?>
                  
        </div>
    </div> 
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-10 booking_serivce_inner_tab" style="padding-left:0px;width: 18%;">
      <div class="form-group">
       <span style="font-weight: 900;">Passengers</span>
             <select name="passenger"  class="servicecategoryinput" id="editpassengerselectdiv">
                   <option value="">&#xf007; Passengers</option>
                   <?php
                       $passengers_data=$this->bookings_model->booking_passengers('vbs_passengers',$client_id);
                      foreach($passengers_data as $key => $item): 
                     ?>
                   <option  value="<?= $item->id ?>">&#xf007; <?= $item->civility.' '.$item->fname.' '.$item->lname; ?></option>
                   <?php endforeach; ?>
             </select>
                  
              <?php if(isset($service_form['passengers']['error']) && !empty($service_form['passengers']['error'])): ?>  
                  <div class='invalid-error'><?= $service_form['passengers']['error']?></div>
              <?php endif; ?>
                  
        </div>
    </div> 
    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-2" style="margin-top: 10px;width: 4%;padding-left:0px;padding-top: 20px;">
      
     <button type="button" class="plusgreeniconconfig"  onclick="editaddpassengers()"><i class="fa fa-plus"></i></button>
    </div>
    <div class="col-md-5" style="padding-left:0px;width: 42%;">
   <div class="col-md-12 pdz">
    <div class="col-md-12 pdz">
      
      <div class="col-md-2 pdz" style="font-weight: 900;">Date</div>
      <div class="col-md-2 pdz" style="font-weight: 900;">Time</div>
      <div class="col-md-6 pdz" style="font-weight: 900;">Since</div>
    </div>
     <div class="col-md-12 pdz" style="margin-top: 8px;">
     
      <div class="col-md-2 pdz"><?= from_unix_date($invoices->date); ?></div>
      <div class="col-md-2 pdz"><?= $invoices->time; ?></div>
      <div class="col-md-6 pdz"><?=timeDiff($invoices->created_at);?></div>
    </div>
 </div>
</div>


 

  <!--client passenger driver cars -->
</div>
