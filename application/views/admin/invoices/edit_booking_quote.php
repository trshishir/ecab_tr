                             <div class="col-md-4" style="margin-top:15px;">
                 
                              <div class="col-md-12" style="padding:0px;">
                               <div class="map-wrapper" >
                                  <div id="editmapheader" style="border-top-left-radius: 15px;border-top-right-radius: 15px;"><h4>Ride Map View</h4></div>
                                   <?php
                                   $k=1;
                                   foreach ($dbstopsdata as $stop):
                                    ?>
                                      <input type="hidden" id="otherstopaddr_<?= $k ?>" value="<?= $stop->stopaddress; ?>">
                                   <?php
                                    $k++;
                                    endforeach;
                                     ?>
                                   <input type="hidden" id="stoppercount" value="<?= count($dbstopsdata); ?>">
                                    <input type="hidden" id="editpickupaddress" value="<?=  $dbbookingdata->pick_point; ?>">
                                    <input type="hidden" id="editdropoffaddress" value="<?= $dbbookingdata->drop_point ?>">
                                    <input type="hidden" id="editpickotheraddress" value="<?= $dbpickaddressdata->otheraddress; ?>">
                                     <input type="hidden" id="editdropotheraddress" value="<?=  $dbdropaddressdata->otheraddress; ?>">
                                  

                                     <div id="editmapsearchloader" style="position: absolute;top: 50%;left: 45%;z-index: 1200;"> 
                                            <img src="<?= base_url()?>assets/theme/default/images/loader_2.gif" >
                                      </div>
                                      
                                         <div id="editmap" tyle="border: 1px solid #cccccc;">
                                     
                                   
                                    </div>                                     
                                  </div>
                                  </div>
                                  <div class="col-md-12" style="margin-top: 46px;padding:0px;">
                                   <button id="distancebtn" class="btn  grbtn brz" style="border-bottom-left-radius: 15px;text-align:left;font-size: 12px;padding-right: 0px;">Estimated Distance : <?= $dbbookingdata->distance ?> km</button>
                                   <button id="timebtn" class="btn  grbtn brz blz pdz" style="font-size: 12px;">Estimated Time : <?= str_replace('min', 'minute', $dbbookingdata->time_of_journey) ?> </button>
                                    <button class="btn  grbtn blz" id="bookingpricedivbtn" style="border-bottom-right-radius: 15px;text-align:right;font-size: 12px;padding-left: 0px;">Estimated Price : <?= $dbbookingdata->price ?> € HT</button>
                                  </div>
                                  <!--Discount and fee -->
                                   <div class="col-md-12"  style="padding: 0px;" id="bookingpricevaluesdetailsdiv">
                        
                                                                      <!-- Fees -->
                                  <div class="col-md-12  clearfix amoutndetaildiv" >

                                     <div class="col-md-5" style="padding: 0px;">
                                       <div class="dd1_amount">
                                        <span style="font-weight:900;">Fees</span>
                                       </div>
                                    </div>
                                      <div class="col-md-3" style="padding: 0px;text-align: center;">
                                         <span style="font-weight:900;">%</span>
                                      </div>
                                    <div class="col-md-4" style="padding: 0px;">
                                     <div class="dd2_amount">
                                      <span style="font-weight:900;">Amount</span>
                                    </div>
                                  </div>
                                     
                                   </div>
                                    <?php if($dbdiscountandfeedata->drivermealfee!=0): ?>
                                  <div class="col-md-12 clearfix amoutndetaildiv" >
                                    <div class="col-md-5" style="padding: 0px;">
                                     <div class="dd1_amount">
                                      <span>Driver Meal Fee :</span>
                                     </div>
                                   </div>
                                    <div class="col-md-3" style="padding: 0px;text-align:center;">
                                    
                                       <span></span>
                                    
                                     </div>
                                    <div class="col-md-4" style="padding: 0px;">
                                     <div class="dd2_amount">
                                      <span>+   <?php echo $dbdiscountandfeedata->drivermealfee;?> €</span>
                                    </div>  
                                    </div>       
                                   </div>
                                 <?php endif; ?>
                                   <?php if($dbdiscountandfeedata->driverrestfee!=0): ?>
                                  <div class="col-md-12 clearfix amoutndetaildiv" >
                                    <div class="col-md-5" style="padding: 0px;">
                                     <div class="dd1_amount">
                                      <span>Driver Rest Fee :</span>
                                     </div>
                                   </div>
                                    <div class="col-md-3" style="padding: 0px;text-align:center;">
                                    
                                       <span></span>
                                    
                                     </div>
                                    <div class="col-md-4" style="padding: 0px;">
                                     <div class="dd2_amount">
                                      <span>+   <?php echo $dbdiscountandfeedata->driverrestfee;?> €</span>
                                    </div>  
                                    </div>       
                                   </div>
                                 <?php endif; ?>
                                  <?php if($dbdiscountandfeedata->nighttimefee!=0): ?>
                                  <div class="col-md-12 clearfix amoutndetaildiv" >
                                    <div class="col-md-5" style="padding: 0px;">
                                     <div class="dd1_amount">
                                      <span>Night Fee :</span>
                                     </div>
                                   </div>
                                    <div class="col-md-3" style="padding: 0px;text-align:center;">
                                     <?php if($dbdiscountandfeedata->nighttimediscount!=0):  ?>
                                       <span><?php echo $dbdiscountandfeedata->nighttimediscount;?>%</span>
                                     <?php endif; ?>
                                     </div>
                                    <div class="col-md-4" style="padding: 0px;">
                                     <div class="dd2_amount">
                                      <span>+   <?php echo $dbdiscountandfeedata->nighttimefee;?> €</span>
                                    </div>  
                                    </div>       
                                   </div>
                                 <?php endif; ?>

                                   <?php if($dbdiscountandfeedata->notworkingdayfee!=0): ?>
                                  <div class="col-md-12 clearfix amoutndetaildiv" >
                                    <div class="col-md-5" style="padding: 0px;">
                                     <div class="dd1_amount">
                                      <span>Sunday And Not Working Day Fee :</span>
                                     </div>
                                   </div>
                                    <div class="col-md-3" style="padding: 0px;text-align: center;">
                                     <?php if($dbdiscountandfeedata->notworkingdaydiscount!=0):  ?>
                                       <span><?php echo $dbdiscountandfeedata->notworkingdaydiscount;?>%</span>
                                     <?php endif; ?>
                                    </div>
                                    <div class="col-md-4" style="padding: 0px;">
                                     <div class="dd2_amount">
                                      <span>+   <?php echo $dbdiscountandfeedata->notworkingdayfee;?> €</span>
                                    </div>  
                                    </div>       
                                   </div>
                                 <?php endif; ?>


                                  <?php if($dbdiscountandfeedata->maydecdayfee!=0): ?>
                                  <div class="col-md-12 clearfix amoutndetaildiv" >
                                    <div class="col-md-5" style="padding: 0px;">
                                     <div class="dd1_amount">
                                      <span>1st Mai And 25 December Fee :</span>
                                     </div>
                                   </div>
                                    <div class="col-md-3" style="padding: 0px;text-align: center;">
                                      <?php if($dbdiscountandfeedata->maydecdaydiscount!=0):  ?>
                                       <span><?php echo $dbdiscountandfeedata->maydecdaydiscount;?>%</span>
                                     <?php endif; ?>
                                    </div>
                                    <div class="col-md-4" style="padding: 0px;">
                                     <div class="dd2_amount">
                                      <span>+   <?php echo $dbdiscountandfeedata->maydecdayfee;?> €</span>
                                    </div>   
                                    </div>      
                                   </div>
                                 <?php endif; ?>
                                  <!--Fees -->
                                    <?php if($dbdiscountandfeedata->vatdiscount!=0): ?>
                                     <div class="col-md-12  clearfix amoutndetaildiv" >

                                     <div class="col-md-5" style="padding: 0px;">
                                       <div class="dd1_amount">
                                        <span style="font-weight:900;">Vat</span>
                                       </div>
                                    </div>
                                      <div class="col-md-3" style="padding: 0px;text-align: center;">
                                         <span style="font-weight:900;">%</span>
                                      </div>
                                    <div class="col-md-4" style="padding: 0px;">
                                     <div class="dd2_amount">
                                      <span style="font-weight:900;">Amount</span>
                                    </div>
                                  </div>
                                     
                                   </div>

                                   <div class="col-md-12 clearfix amoutndetaildiv" >
                                      <div class="col-md-5" style="padding: 0px;">
                                     <div class="dd1_amount">
                                      <span><?= $vatname ?> : </span>
                                     </div>
                                   </div>
                                    <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $dbdiscountandfeedata->vatdiscount;?>%</span></div>
                                    <div class="col-md-4" style="padding: 0px;">
                                     <div class="dd2_amount">
                                      <span>+   <?php echo $dbdiscountandfeedata->vatfee;?> €</span>
                                    </div>
                                  </div>  
                                   </div>
                                    <?php endif; ?>
                                    <div class="col-md-12  clearfix amoutndetaildiv" >

                                     <div class="col-md-5" style="padding: 0px;">
                                       <div class="dd1_amount">
                                        <span style="font-weight:900;">Discounts</span>
                                       </div>
                                    </div>
                                      <div class="col-md-3" style="padding: 0px;text-align: center;">
                                         <span style="font-weight:900;">%</span>
                                      </div>
                                    <div class="col-md-4" style="padding: 0px;">
                                     <div class="dd2_amount">
                                      <span style="font-weight:900;">Amount</span>
                                    </div>
                                  </div>
                                     
                                   </div>
                                 
                                 <?php if($dbdiscountandfeedata->welcomediscount!=0): ?>
                                  <div class="col-md-12 clearfix amoutndetaildiv" >
                                   <div class="col-md-5" style="padding: 0px;">
                                     <div class="dd1_amount">
                                      <span>Welcome Discount :</span>
                                     </div>
                                   </div>
                                   <div class="col-md-3" style="padding: 0px; text-align: center;">
                                    
                                      <span><?php echo $dbdiscountandfeedata->welcomediscount;?>%</span>
                                    
                                   </div>
                                    <div class="col-md-4" style="padding: 0px;">
                                     <div class="dd2_amount">
                                      <span>-  <?php echo $dbdiscountandfeedata->welcomediscountfee;?> €</span>
                                    </div>
                                  </div>
                                     
                                   </div>
                                 <?php endif; ?>
                                 
                                  <?php if($dbdiscountandfeedata->rewarddiscount!=0): ?>
                                   <div class="col-md-12 clearfix amoutndetaildiv" >
                                   <div class="col-md-5" style="padding: 0px;">
                                     <div class="dd1_amount">
                                      <span>Reward Discount :</span>
                                     </div>
                                   </div>
                                    <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $dbdiscountandfeedata->rewarddiscount;?>%</span></div>
                                    <div class="col-md-4" style="padding: 0px;">
                                     <div class="dd2_amount">
                                      <span>-  <?php echo $dbdiscountandfeedata->rewarddiscountfee;?> €</span>
                                    </div>
                                  </div>
                                     
                                   </div>
                                    <?php endif; ?>
                                  <?php if($dbdiscountandfeedata->perioddiscount!=0): ?>
                                   <div class="col-md-12 clearfix amoutndetaildiv" >
                                      <div class="col-md-5" style="padding: 0px;">
                                     <div class="dd1_amount">
                                      <span>Period Discount :</span>
                                     </div>
                                   </div>
                                    <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $dbdiscountandfeedata->perioddiscount;?>%</span></div>
                                    <div class="col-md-4" style="padding: 0px;">
                                     <div class="dd2_amount">
                                      <span>-   <?php echo $dbdiscountandfeedata->perioddiscountfee;?> €</span>
                                    </div>
                                  </div>
                                     
                                   </div>
                                    <?php endif; ?>
                                    <div class="col-md-12 clearfix amoutndetaildiv" id="promo_code_div" >
                                      <?php if($dbdiscountandfeedata->promocodediscount!=0): ?>
                                     
                                   <div class="col-md-5" style="padding: 0px;">
                                     <div class="dd1_amount">
                                      <span>Discount Code :</span>
                                     </div>
                                   </div>
                                    <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $dbdiscountandfeedata->promocodediscount;?>%</span></div>
                                     <div class="col-md-4" style="padding: 0px;">
                                      <div class="dd2_amount">
                                        <span>-   <?php echo $dbdiscountandfeedata->promocodediscountfee;?> €</span>
                                      </div>
                                    </div>
                                   <?php endif; ?>
                                    
                                     
                                   </div>
                       
                           

                                    </div>
                                  <!--Discount and fee -->
                                      <div class="col-md-12 clearfix grdiv amountdetaillastdiv">
                             <div class="col-md-3" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Price To Pay :</span>
                              </div>
                            </div>
                             <div class="col-md-7" style="padding: 0px;text-align: center;">
                             
                              <div class="col-md-4" style="padding: 0px;">                         
                                 <span style="font-weight: 900;">Custom Price :</span>
                              </div>
                              <div class="col-md-8" style="padding: 0px;">
                                <div class="col-md-8" style="padding: 0px;">
                                  <input type="text" class="form-control" id="custompricefield" name="customprice" value="" >
                                </div>
                                  <div class="col-md-4" style="padding: 0px;">
                                  <button type="button" class="btn btn-default" style="font-weight: 900;margin-right:10px;height: 30px;line-height: 1px;" onclick="addautocustomeprice(this,<?= $dbbookingdata->id; ?>);">Apply</button>
                                </div>
                              
                              </div>
                            
                              </div>
                              <div class="col-md-2" style="padding: 0px;">
                               <div class="dd2_amount" >
                                <span style="font-weight: 900;" id="totalpricettcdiv"><?php echo ($dbbookingdata->totalprice);?> € TTC</span>
                               </div>
                             </div>
                            
                           </div>
                             <div class="col-md-12 clearfix grdiv amountdetaillastdiv" style="margin-top: 10px;">
                             <div class="dd1_amount">
                               <span style="font-weight: 900;">Rest Due :</span>
                              </div>
                              <div class="dd2_amount">
                                
                               <span style="font-weight: 900;"><?php echo ($dbbookingdata->totalprice - $dbbookingdata->payment_received); ?> € </span>
                             </div>
                           </div>
                                                     
                            <div class="col-md-12 clearfix grdiv amountdetaillastdiv" style="margin-top: 10px;">
                             <div class="dd1_amount">
                               <span style="font-weight: 900;">Suggested Price :</span>
                              </div>
                              <div class="dd2_amount">
                                <button type="button" class="btn btn-default" style="font-weight: 900;margin-right: 35px; height: 30px; line-height: 1px;" onclick="addcustomeprice(this,<?= $dbbookingdata->realridetotalprice; ?>,<?= $dbbookingdata->id; ?>);">Apply</button>
                               <span style="font-weight: 900;"><?php echo $dbbookingdata->realridetotalprice ;?> € HT</span>
                             </div>
                           </div>


                 <div class="col-md-12 paymentmethodboxdiv" style="margin-top: 10px;">
                      
                      <div class="col-md-12" style="padding:0px;" >

                          <button class="btn payment-method-header-title" style="pointer-events:none;">
                          Payment Methode         
                          </button>        
                      </div>
                               
                              
                  <div class="col-md-12" style="padding:0px;">             
                            <ul role="tablist" id="paymentmethodsullist" class="nav nav-tabs on-bo-he  payement" style="width: 100%;">
                              <?php
                               $methodcount=1;

                               foreach ($paymentmethods as $key => $method):
                                

                                 if($methodcount<7):
                              ?>

                            <li class="payment-method-buttons-list" style="padding: 0px !important;width: 16.65%;margin:0px !important;border:0px;">         
                            <input type="radio" name="payment_method"  value="<?= $method->id ?>" id="methodbtn<?= $methodcount ?>" <?= ($dbbookingdata->payment_method_id==$method->id)?'checked':''; ?> style="top: 8px !important;">
                            <button type="button" class="btn grepbtn payment-method-buttons-btn" style="padding: 6px 0px 0px 22px !important;"><span style="font-size: 11.5px;"><?= $method->name ?></span>
                            </button>
                            </li>

                              <?php
                              $methodcount++;
                               endif;
                               endforeach;
                              ?>
                            </ul>
                   </div>
                            
                    <div class="col-md-12" id="paymentmethoddiv" >
                              <?php
                                   $paymentmethodcount=1;
                                   $cardtype="credit";
                                   foreach ($paymentmethods as $key => $method):
                                    if($paymentmethodcount<7):
                                  ?>
                                <div class="payments" id="paymentmethod<?= $paymentmethodcount ?>" style="text-align: unset;">
                                <h4 style="font-size:18px;font-weight:900;"> Pay With <?= $method->name ?> </h4>
                                <!-- Credit Card Start -->
                                <?php
                                         if($method->category_client == 1): 
                                         $cardtype="credit";
                                        
                                ?>         

                                         <?php  include 'editcreditcard.php';?> 
                                <!-- Credit Card End -->
                                    
                                 <!-- Bank Debit Card Start -->
                                <?php
                                        elseif($method->category_client == 2): 
                                         $cardtype="debit";
                                ?>
                                         <?php  include 'editdebitcard.php';?>
                                 <!-- Bank Debit Card End -->

                                 <!-- Other Payment ways Start -->
                                <?php else: ?>
                                  <p><?= $method->description ?></p>
                                <?php endif; ?>
                                <!-- Other Payment ways End -->
                                                               
                                </div>
                                 <?php
                                   $paymentmethodcount++;
                                   endif;
                                   endforeach;
                                  ?> 
                     </div>
                                    <!--<div class="down-btn">-->
                      
                    </div>
         <!-- Custom Payment -->
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pdz" style="margin-top: 20px;">
               <div class="col-md-12 pdz" style="font-weight: 900;">Payments Raws : </div>
               <div class="col-md-12 pdz" id="custompaymentrecorded">
                 <div class="col-md-12 pdz">
                  <div class="col-md-2 pdz" style="text-align: center;font-weight: 900;">Date</div>
                  <div class="col-md-2 pdz" style="text-align: center;font-weight: 900;">Time</div>
                  <div class="col-md-2 pdz" style="text-align: center;font-weight: 900;">Methode</div>
                  <div class="col-md-2 pdz" style="text-align: center;font-weight: 900;">Amount</div>
                   <div class="col-md-2 pdz" style="text-align: center;font-weight: 900;">Reference</div>
                </div>
                <?php  foreach ($custompayments as $custom): 
                       $date = strtotime($custom->date);
                       $date = date('d/m/Y',$date);
                       $date= $date;
                    ?>

                    <div class="custompaymentbox col-md-12 pdz" style="margin-top: 5px;">
                      <div class="col-md-2 text-center pdz"><?= from_unix_date($custom->date)  ?></div>
                      <div class="col-md-2 text-center pdz"><?= $custom->time  ?></div>
                      <div class="col-md-2 text-center pdz"><?= $custom->method  ?></div>
                      <div class="col-md-2 text-center pdz"><?= $custom->amount  ?></div>
                      <div class="col-md-2 text-center pdz"><?= $custom->reference  ?></div>
                      <div class="col-md-2" style="padding-left: 52px;">
                        <button type="button" onclick="removecustompaymentbox(this,<?= $custom->id ?>);" class="minusrediconcustomreminder"><i class="fa fa-minus" style="margin:0px !important;"></i></button></div>
                    </div>
              <?php endforeach; ?>
               </div>
              
               <div class="maincustompaymentbox col-md-12 pdz" style="margin-top: 10px;">
                <div class="custompaymentbox col-md-12 pdz">
                  <div class="col-md-12 pdz">
                         <div class="col-md-2" style="padding: 0px;">                             
                            <input  class="pickup-dt-bkg hasDatepicker" id="custompaymentdatefield"  type="text" value="<?= date('d/m/Y'); ?>" name="custompaymentdatefield" placeholder="<?php echo 'Date'; ?>" autocomplete="off" />     
                          </div>
                           <div class="col-md-2" style="padding-right: 0px;" >
                            <input   id="custompaymenttimepicker" type="text" value="<?= date('h : i'); ?>" name="custompaymenttimefield" placeholder="<?php echo 'Time'; ?>"  style="width:100%" />       
                          </div> 
                           <div class="col-md-2" style="padding-right: 0px;">
                            <input type="text" class="form-control"  name="custompaymentmethodfield" id="custompaymentmethodfield" placeholder="Methode" >
                          </div>
                          <div class="col-md-2" style="padding-right: 0px;">
                            <input type="text" class="form-control"  name="custompaymentamountfield" id="custompaymentamountfield" placeholder="Amount" >
                          </div>
                          <div class="col-md-2" style="padding-right: 0px;">
                            <input type="text" class="form-control"  name="custompaymentreferencefield" id="custompaymentreferencefield" placeholder="Reference" >
                          </div>
                   <div class="col-md-2" style="padding-left: 52px;">
                      <button type="button" onclick="addcustompaymentbox();" class="plusgreeniconconfig"><i class="fa fa-plus" style="margin:0px !important;"></i></button>
                   </div>
                 </div>
               </div>
               </div>
          </div>
          <!-- Custom Payment -->
              </div> 
                           
                            
         
     
        <div class="clearfix"></div>
       
       

      
  
