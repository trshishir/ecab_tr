<?php
$booking=$this->session->userdata['bookingdata'];
$bookingregular=$this->session->userdata['bookingpickregular'];
$bookingreturn=$this->session->userdata['bookingreturnregular'];
$displaypassengers=$this->session->userdata['passengers'];
$stopsdata=$this->session->userdata['stops'];

$map= $this->session->userdata['mapdatainfo'];
if(!empty($booking['pickupdate'])){
 $bookingdate=str_replace('/', '-', $booking['pickupdate']);
}
else{
  $bookingdate=str_replace('/', '-', $booking['startdate']);
}
 $pickupaddress1=str_replace((', '.$booking['dropcity']), "", $pickupaddress);
 $dropoffaddress1=str_replace((', '.$booking['dropcity']), "", $dropoffaddress);
?>
<?php
 $attributes = array("name" => 'booking_form', "id" => 'confirm_booking_form');
  echo form_open('admin/bookings/save_booking_confirm',$attributes);
?>
            <div class="col-md-12 online" style="padding: 0px;">
                <div id="bookinginformationlistdiv">
                    <div class="col-md-7" >
                        <div class="row">
                          <div class="col-md-12" style="padding: 0px;">
                           <div class="col-md-6" style="padding: 0px">
                          
                            <p style="font-size:13px !important;"><span style="margin-right:5px;font-weight: 900;">Client : </span><br>
                              <?php echo "<span style='font-weight:900;'>".$client->civility.' '.$client->first_name.' '.$client->last_name.'</span><br>'.$client->address.'<br>';?>
                             <?php if(!empty($client->address2) || $client->address2 != null):  ?>
                              <?php echo $client->address2.'<br>'; ?>
                             <?php endif; ?>
                             <?php echo $client->zipcode.'  '.$client->city;?></p>
                           </div>
                            <div class="col-md-6 " style="padding: 0px;">
                                  
                                  <div class="col-md-12" style="padding:0px;">
                                          
                                       <ul class="ulbdnone" id="passengerdiv" >
                                        <li class="text-left" style="font-weight: 900;">Passengers :</li>
                                          <?php foreach($displaypassengers as $passenger): ?>
                                           <li class="text-left" style="position: relative;"><span class="fa fa-user" style="margin-right:5px;"></span><span><?php echo $passenger['name'];?></span>, <span class="fa fa-mobile" style="margin-right:5px;"></span><span><?php echo $passenger['phone'];?></span>, <span class="fa fa-phone" style="margin-right:5px;"></span><span><?php echo $passenger['home'];?></span>, <span class="fa fa-wheelchair" style="margin-right:5px;"></span><span><?= $passenger['disable'] ?></span>
                                           
                                                 
                                          </li>

                                          <?php endforeach; ?>    
                                           
                                       </ul>

                                    </div>
                         
                                
                                </div>
                            </div>
                           
                         


<div class="col-md-12" style="padding:0px;">

  <div class="col-md-6" style="padding:0px;">
    <ul class="ulbdnone" >
    <li class="text-left" ><strong>Service Category : </strong>
      <?php  
     $servicecategory= $this->bookings_model->getservicesbyid('vbs_u_category_service', $booking['servicecategory']);
       echo $servicecategory->category_name;
       ?></li>
      </ul>
    <ul class="ulbdnone bts-1">
    <li style="border-bottom:0px;">
        <strong>Pick Up Category : </strong><?php echo ucwords($booking['categorytype']);?>
        <ul class="ulbdnone  clearfix" >             
        
          <?php if($booking['categorytype']=="address"): ?>
            <?php
               $pickshortaddress='';
               $pickshortaddress=$booking['address'];
               $pickshortaddress= str_replace(($booking['city'].','), "", $pickshortaddress);
               $pickshortaddress= str_replace(($booking['zipcode']), "",  $pickshortaddress);
               $pickshortaddress=str_ireplace("france","",$pickshortaddress);
               $pickshortaddress=rtrim($pickshortaddress,' ');
               $pickshortaddress=rtrim($pickshortaddress,',');
               $pickshortaddress = preg_replace('!\s+!', ' ', $pickshortaddress);
             ?>
         
        <li class="text-left"><strong>Complément d'adresse : </strong><?php echo $booking['otheraddress'];?></li>
        
         <li class="text-left"><strong>Addresse : </strong><?php echo $pickshortaddress;?></li>
         
        
         <?php elseif($booking['categorytype']=="package"): ?>
         <li class="text-left"><strong>Name : </strong><?php echo $booking['pickpackagename'];?></li>
         <li class="text-left"><strong>Addresse : </strong><?php echo $pickupaddress1;?></li>
         <?php elseif($booking['categorytype']=="airport"): ?>
         <li class="text-left"><strong>Name : </strong><?php echo $booking['pickairportname'];?></li>
          <li class="text-left"><strong>Flight Number : </strong><?php echo $booking['flightnumber'];?>
           <strong style="margin-left: 5px;">Arrival Time : </strong><?php echo $booking['flightarrivaltime'];?></li>
         <li class="text-left"><strong>Addresse : </strong><?php echo $pickupaddress1;?></li>
         <?php elseif($booking['categorytype']=="train"): ?>
         <li class="text-left"><strong>Name : </strong><?php echo $booking['picktrainname'];?></li>
         <li class="text-left"><strong>Train Number : </strong><?php echo $booking['trainnumber'];?>
         <strong style="margin-left: 5px;">Arrival Time : </strong><?php echo $booking['trainarrivalnumber'];?></li>
         <li class="text-left"><strong>Addresse : </strong><?php echo $pickupaddress1;?></li>
          <?php elseif($booking['categorytype']=="hotel"): ?>
         <li class="text-left"><strong>Name : </strong><?php echo $booking['pickhotelname'];?></li>
         <li class="text-left"><strong>Addresse : </strong><?php echo $pickupaddress1;?></li>
          <?php elseif($booking['categorytype']=="park"): ?>
         <li class="text-left"><strong>Name : </strong><?php echo $booking['pickparkname'];?></li>
         <li class="text-left"><strong>Addresse : </strong><?php echo $pickupaddress1;?></li>
        <?php endif; ?>
          <li>
          <ul class="ulbdnone clearfix" >
         <li class="text-left" style="float:left;border:0px;padding:0px;"><strong>Zip Code : </strong><?php echo $booking['zipcode'];?> </li>
         <li class="text-left" style="float:left;border:0px;padding:0px 10px;"><strong>City : </strong><?php echo $booking['city'];?></li>
         </ul>
       </li>
        
       </ul>
    </li>
    </ul>
      
    <?php
     $car_configuration_data= $this->bookings_model->booking_Configuration('vbs_car_configuration',['car_id' => $booking['carfield']]);
     ?>
   
    
        
   </div>
   <div class="col-md-6" style="padding:0px;">
    <ul class="ulbdnone" >
       <li class="text-left"><strong>Service : </strong>
      <?php
     $service= $this->bookings_model->getservicesbyid('vbs_u_service', $booking['service']);
     echo $service->service_name;
       ?>
      </li>
    </ul>
  <ul  class="ulbdnone" >
    <li style="border-bottom:0px;">
        <strong >Drop Off Category : </strong><?php echo ucwords($booking['dropcategorytype']);?>
        <ul  class="ulbdnone clearfix">             
       
          <?php if($booking['dropcategorytype']=="address"): ?>
          <?php 
             $dropshortaddress='';
             $dropshortaddress=$booking['dropaddress'];
             $dropshortaddress= str_replace(($booking['dropcity'].','), "", $dropshortaddress);
             $dropshortaddress= str_replace(($booking['dropzipcode']), "",  $dropshortaddress);
              $dropshortaddress=str_ireplace("france","",$dropshortaddress);
               $dropshortaddress=rtrim($dropshortaddress,' ');
               $dropshortaddress=rtrim($dropshortaddress,',');
             $dropshortaddress = preg_replace('!\s+!', ' ', $dropshortaddress);


          ?>
                <li class="text-left"><strong>Complément d'adresse : </strong><?php echo $booking['dropotheraddress'];?></li>
          
         <li class="text-left"><strong>Addresse : </strong><?php echo $dropshortaddress;?></li>
       
         <?php elseif($booking['dropcategorytype']=="package"): ?>
         <li class="text-left"><strong>Name : </strong><?php echo $booking['droppackagename'];?></li>
         <li class="text-left"><strong>Addresse : </strong><?php echo $dropoffaddress1;?></li>
         <?php elseif($booking['dropcategorytype']=="airport"): ?>
         <li class="text-left"><strong>Name : </strong><?php echo $booking['dropairportname'];?></li>
         <li class="text-left"><strong>Flight Number : </strong><?php echo $booking['dropflightnumber'];?>
         <strong style="margin-left: 5px;">Arrival Time : </strong><?php echo $booking['dropflightarrivaltime'];?></li>
         <li class="text-left"><strong>Addresse : </strong><?php echo $dropoffaddress1;?></li>
         <?php elseif($booking['dropcategorytype']=="train"): ?>
         <li class="text-left"><strong>Name : </strong><?php echo $booking['droptrainname'];?></li>
           <li class="text-left"><strong>Train Number : </strong><?php echo $booking['droptrainnumber'];?>
           <strong style="margin-left: 5px;">Arrival Time : </strong><?php echo $booking['droptrainarrivalnumber'];?></li>
         <li class="text-left"><strong>Addresse : </strong><?php echo $dropoffaddress1;?></li>
          <?php elseif($booking['dropcategorytype']=="hotel"): ?>
         <li class="text-left"><strong>Name : </strong><?php echo $booking['drophotelname'];?></li>
         <li class="text-left"><strong>Addresse : </strong><?php echo $dropoffaddress1;?></li>
          <?php elseif($booking['dropcategorytype']=="park"): ?>
         <li class="text-left"><strong>Name : </strong><?php echo $booking['dropparkname'];?></li>
         <li class="text-left"><strong>Addresse : </strong><?php echo $dropoffaddress1;?></li>
        <?php endif; ?>
        <li>
          <ul  class="ulbdnone clearfix" >
        <li class="text-left" style="float:left;border:0px;padding:0px;"><strong>Zip Code : </strong><?php echo $booking['dropzipcode'];?></li>
        <li class="text-left" style="float:left;border:0px;padding:0px 10px;"><strong>City : </strong><?php echo $booking['dropcity'];?></li>
         </ul>
       </li>
       </ul>
    </li>
    </ul>
           
   </div>
 </div>
   <div class="col-md-12" style="padding: 0px;border-bottom:1px solid #ececec;">
    <?php if($booking['regularcheck']!=1):?>
     <div class="col-md-6" style="padding:0px;">
        <ul class="ulbdnone clearfix" >             
        <li class="text-left" style="float: left;padding:0px !important;border:0px;"><strong >Pick Up Date : </strong><?php echo  $booking['pickupdate'];?></li>
         <li class="text-left"  style="float: left;padding: 0px !important;margin-left: 13px;border:0px;"><strong>Pick Up Time : </strong><?php echo  $booking['pickuptime'];?></li>
       </ul> 
     </div>
     <?php endif;?>
      <div class="col-md-6" style="padding:0px;">
                 <div  style="float:left; margin-right:5px;">
                    
                        <span><strong>Regular : </strong></span><?php echo ($booking['regularcheck']==1)?"Yes":'No';  ?>,
                    
                    </div>

                    <div style="float:left; margin-right:5px;">
                       
                        <span><strong>Return : </strong></span> <?php echo ($booking['returncheck']==1)?"Yes":'No';  ?>,
                    
                    </div>
                     <div style="float:left; margin-right:5px;">
                     
                        <span><strong>Wheelchair : </strong> </span> <?php echo ($booking['wheelchaircheck']==1)?"Yes":'No';  ?>
                     
                    </div>
             </div>
        
   </div>
 
          <?php if($booking['returncheck']==1 && $booking['regularcheck']!=1 ):?>
     <div class="col-md-12" style="padding:0px;">     
        <ul class="ulbdnone clearfix datetimequotediv">             
            <li class="text-left"  ><strong>Waiting Time : </strong>
              <?php
                $waitarray=explode(":", $booking['waitingtime']);
                if($waitarray[0] > 1 && $waitarray[1] > 1){
                   echo $waitarray[0]." hours and ".$waitarray[1]." minutes ";
                 }
                 elseif($waitarray[0] > 1 && $waitarray[1] <= 1){
                   echo $waitarray[0]." hours and ".$waitarray[1]." minute ";
                 }
                  elseif($waitarray[0] <= 1 && $waitarray[1] > 1){
                   echo $waitarray[0]." hour and ".$waitarray[1]." minutes ";
                 }
                 else{
                  echo $waitarray[0]." hour and ".$waitarray[1]." minute ";
                 }
               
                        ?></li>
            <li class="text-left"><strong>Return Date : </strong><?php echo  $booking['returndate'];?></li>
            <li class="text-left"><strong>Return Time : </strong><?php echo  $booking['returntime'];?></li>
   
       </ul>
    </div>
       <?php endif; ?>
   <?php if($booking['regularcheck']==1):?>
             <div class="col-md-12 bbts-1">
              <div class="col-md-6" style="padding:0px;">
                  <span><strong>Start Date : </strong><?php echo  $booking['startdate'];?>, </span>
                    <span><strong>Start Time : </strong><?php echo  $booking['starttime'];?> </span>
               </div>     
                <div class="col-md-6" style="padding:0px;">
                    <span><strong>End Date : </strong><?php echo  $booking['enddate'];?>, </span>
                  <span><strong>End Time : </strong><?php echo  $booking['endtime'];?></span>
                </div>
                </div>
    <div class="col-md-12" style="padding: 0px;">
      <table cellpadding="0" cellspacing="0" 
      style="width:100%;" align="center" id="timesspantable">
        <tr  class="quotepicktimecol fw-900">
          <td>&nbsp;</td>
        
         
                          
           
              <td>Monday</td>
           
              <td>Tuesday</td>
          
               <td>Wednesday</td>
           
       
              <td>Thursday</td>
        
         
               <td>Friday</td>
       
        
              <td>Saturday</td>
          
               <td>Sunday</td>
            

          
          
         
          
         
          
         
        </tr>
        <tr class="quotepicktimecol">
          <td style="font-size: 11px;font-weight: 900;text-align: left;">PickUp Time</td>
           <?php foreach($bookingregular as $regular): ?>
            <?php if(!empty($regular['time']) || $regular['time']!=''): ?>
              <td>
               <?php echo $regular['time'];?>
               </td>
             <?php else: ?>  
                 
                    <td>No Ride</td>
                 
            <?php endif; ?>
           <?php endforeach; ?>
        </tr>
          <?php if($booking['returncheck']==1):?>
        <tr class="quotepicktimecol">
          <td style="font-size: 11px;font-weight: 900;text-align: left;">Return Time</td>
           <?php foreach($bookingreturn as $return): ?>
             <?php if( !empty($return['time']) || $return['time']!=''): ?>
               <td>
              <?php echo $return['time'];?>
             </td>
                <?php else: ?>  
                  <td>No Ride</td>
             <?php endif; ?>
          <?php endforeach; ?>
        </tr>
       <?php endif; ?>
      </table>
    </div>
     <?php endif; ?>
       
    <div class="col-md-12" style="padding: 0px;">
   
      <ul class="ulbdnone bts-1">             
         <li class="text-left" style="border-bottom:0px;"><strong>Stops : </strong>
          <?php foreach ($stopsdata as $stop): ?>
          <ul class="ulbdnone clearfix">
             <li class="text-left" style="width:100%;"><span style="display: inline-block;width:50%;"><strong>Addresse : </strong><?php echo  $stop['stopaddress'];?></span><span style="display: inline-block;width:50%;"><strong>Waiting Time : </strong><?php 
             $waitarray = explode(":", $stop['stopwaittime']);
             if($waitarray[0] > 1 && $waitarray[1] > 1){
               echo $waitarray[0]." hours ".$waitarray[1]." minutes";
             }
             elseif($waitarray[0] < 2 && $waitarray[1] < 2){
               echo $waitarray[0]." hour ".$waitarray[1]." minute";
             }
             elseif($waitarray[0] > 1 && $waitarray[1] < 2){
               echo $waitarray[0]." hours ".$waitarray[1]." minute";
             }
             elseif($waitarray[0] < 2 && $waitarray[1] > 1){
               echo $waitarray[0]." hour ".$waitarray[1]." minutes";
             }
            
              ?></span></li>
          </ul>
        <?php endforeach; ?>
      
         </li>
       </ul> 
  
 </div>       
  <div class="col-md-12" style="padding:0px;">
   <div class="col-md-6" style="padding:0px;">
     <ul class="ulbdnone"  id="carquotestab">
    <li style="border-bottom:0px;">
        <strong>Car Category : </strong><?php echo $car_name  ?>
        <ul class="ulbdnone"  >   
          
          <?php
          $configno=0;
           foreach ($car_configuration_data as $config): 
            $configno++;
            ?>     
         <li class="text-left">
          <span><strong>Configuration <?= $configno ?> :  </strong></span>
          <span><?php echo $config->passengers;?><strong> Passengers,</strong></span>
          <span><?php echo $config->luggage;?><strong> Luggage,</strong></span>
          <span><?php echo $config->wheelchairs;?><strong> Wheelchairs,</strong></span>
          <span><?php echo $config->baby;?><strong> Babys,</strong></span>
          <span><?php echo $config->animals;?><strong> Animals</strong></span>
        </li>
        
           <?php endforeach; ?> 
       
       </ul>
    </li>
    </ul>
   </div> 
     <?php if(!empty($booking['booking_comment'])): ?>
       <div class="col-md-6" style="padding:0px;">
       <div class="form-group">
            <label style="font-weight: 900;margin-bottom: 10px;">Comment :</label>
       <textarea class="form-control" rows="4" name="booking_comment" id="commentbox" disabled style="background-color: #fff;resize: none;cursor: default;" ><?php echo $booking['booking_comment'];  ?></textarea>
    
         </div>
       </div>

       <?php endif; ?>  
           </div>                      
 </div>
</div>
                
                  
                  
                    <div class="col-md-5" style="padding-right:0px;">
                        <div>
                            <div class="col-md-12" id="addquotesection_opt" style="padding: 0px;">
                                <div class="map-wrapper" >
                                  <div id="editmapheader" style="border-top-left-radius: 15px;border-top-right-radius: 15px;"><h4>Ride Map View</h4></div>
                                   <?php for($i=0;$i<=count($stopsdata);$i++): ?>
                                      <input type="hidden" id="otherstopaddr_<?= $i+1; ?>" value="<?= $stopsdata[$i]['stopaddress']; ?>">
                                   <?php endfor; ?>
                                   <input type="hidden" id="stoppercount" value="<?= count($stopsdata); ?>">
                                    <input type="hidden" id="editpickupaddress" value="<?= $pickupaddress ?>">
                                    <input type="hidden" id="editdropoffaddress" value="<?= $dropoffaddress ?>">
                                    <input type="hidden" id="editpickotheraddress" value="<?= $booking['otheraddress'] ?>">
                                     <input type="hidden" id="editdropotheraddress" value="<?= $booking['dropotheraddress'] ?>">
                                  

                                     <div id="editmapsearchloader" style="position: absolute;top: 50%;left: 45%;z-index: 1200;"> 
                                            <img src="<?= base_url()?>assets/theme/default/images/loader_2.gif" >
                                      </div>
                                      
                                 <div id="editmap" >
                                     
                                     
                                </div>
                             </div>  
                          </div>
  <div class="col-md-12 paymentmethodboxdiv" id="addpaymentsection_opt" style="display: none;border-bottom-left-radius: 0px !important;border-bottom-right-radius: 0px !important;">
                              <?php  include 'add_booking_payment.php';?> 
                            </div>
                          
                            <div class="col-md-12" id="pricediscountarea" style="margin-top: 46px;padding: 0px;">
                             <button id="distancebtn" class="btn  grbtn brz" style="border-bottom-left-radius: 15px;text-align:left;">Est. Distance : <?= $map['distance'] ?> km</button>
                             <button id="timebtn" class="btn  grbtn blz brz">Est. Time : <?= str_replace('min', 'minute', $map['time']) ?> </button>
                              <button class="btn  grbtn blz" id="bookingpricedivbtn" style="border-bottom-right-radius: 15px;text-align:right;">Est. Price : <?= $map['price'] ?> € HT</button>
                           </div>
                      
                         
                      
                          <div class="col-md-12" style="padding: 0px;">
                             <?php if($map['vatdiscount']!=0): ?>
                              <div class="col-md-12  clearfix amoutndetaildiv" >

                              <div class="col-md-5" style="padding: 0px;">
                                <div class="dd1_amount">
                                 <span style="font-weight:900;">Vat</span>
                                </div>
                             </div>
                               <div class="col-md-3" style="padding: 0px;text-align: center;">
                                  <span style="font-weight:900;">%</span>
                               </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span style="font-weight:900;">Amount</span>
                             </div>
                           </div>
                              
                            </div>

                            <div class="col-md-12 clearfix amoutndetaildiv" >
                               <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span><?= $map['vatname'] ?> : </span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $map['vatdiscount'];?>%</span></div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $map['vatfee'];?> €</span>
                             </div>
                           </div>  
                            </div>
                             <?php endif; ?>
                             <div class="col-md-12  clearfix amoutndetaildiv" >

                              <div class="col-md-5" style="padding: 0px;">
                                <div class="dd1_amount">
                                 <span style="font-weight:900;">Discount and Fee name</span>
                                </div>
                             </div>
                               <div class="col-md-3" style="padding: 0px;text-align: center;">
                                  <span style="font-weight:900;">%</span>
                               </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span style="font-weight:900;">Amount</span>
                             </div>
                           </div>
                              
                            </div>
                           <?php if($map['drivermealfee']!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                             <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Driver Meal Fee :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align:center;">
                             
                                <span></span>
                             
                              </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $map['drivermealfee'];?> €</span>
                             </div>  
                             </div>       
                            </div>
                          <?php endif; ?>
                            <?php if($map['driverrestfee']!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                             <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Driver Rest Fee :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align:center;">
                             
                                <span></span>
                             
                              </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $map['driverrestfee'];?> €</span>
                             </div>  
                             </div>       
                            </div>
                          <?php endif; ?>
                           <?php if($map['nighttimefee']!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                             <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Night Fee :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align:center;">
                              <?php if($map['nighttimediscount']!=0):  ?>
                                <span><?php echo $map['nighttimediscount'];?>%</span>
                              <?php endif; ?>
                              </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $map['nighttimefee'];?> €</span>
                             </div>  
                             </div>       
                            </div>
                          <?php endif; ?>

                            <?php if($map['notworkingdayfee']!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                             <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Sunday And Not Working Day Fee :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;">
                              <?php if($map['notworkingdaydiscount']!=0):  ?>
                                <span><?php echo $map['notworkingdaydiscount'];?>%</span>
                              <?php endif; ?>
                             </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $map['notworkingdayfee'];?> €</span>
                             </div>  
                             </div>       
                            </div>
                          <?php endif; ?>


                           <?php if($map['maydecdayfee']!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                             <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>1st Mai And 25 December Fee :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;">
                               <?php if($map['maydecdaydiscount']!=0):  ?>
                                <span><?php echo $map['maydecdaydiscount'];?>%</span>
                              <?php endif; ?>
                             </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $map['maydecdayfee'];?> €</span>
                             </div>   
                             </div>      
                            </div>
                          <?php endif; ?>


                      

                         
                          <?php if($map['welcomediscount']!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                            <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Welcome Discount :</span>
                              </div>
                            </div>
                            <div class="col-md-3" style="padding: 0px; text-align: center;">
                             
                               <span><?php echo $map['welcomediscount'];?>%</span>
                             
                            </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>-  <?php echo $map['welcomediscountfee'];?> €</span>
                             </div>
                           </div>
                              
                            </div>
                          <?php endif; ?>
                        
                           <?php if($map['rewarddiscount']!=0): ?>
                            <div class="col-md-12 clearfix amoutndetaildiv" >
                            <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Reward Discount :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $map['rewarddiscount'];?>%</span></div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>-  <?php echo $map['rewarddiscountfee'];?> €</span>
                             </div>
                           </div>
                              
                            </div>
                             <?php endif; ?>
                           <?php if($map['perioddiscount']!=0): ?>
                            <div class="col-md-12 clearfix amoutndetaildiv" >
                               <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Period Discount :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $map['perioddiscount'];?>%</span></div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>-   <?php echo $map['perioddiscountfee'];?> €</span>
                             </div>
                           </div>
                              
                            </div>
                             <?php endif; ?>
                             
                          <?php if($map['promocodediscount']!=0): ?>
                          <div class="col-md-12 clearfix amoutndetaildiv" id="promo_code_div" >
                            <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Code Discount :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $map['promocodediscount'];?>%</span></div>
                              <div class="col-md-4" style="padding: 0px;">
                               <div class="dd2_amount">
                                 <span>-   <?php echo $map['promocodediscountfee'];?> €</span>
                               </div>
                             </div>
                            </div>
                            <?php endif; ?>
                             
                              
                             <div class="col-md-12 clearfix grdiv amountdetaillastdiv">
                             <div class="col-md-3" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Price To Pay :</span>
                              </div>
                            </div>
                             <div class="col-md-5" style="padding: 0px;text-align: center;">
                              
                              </div>
                              <div class="col-md-4" style="padding: 0px;">
                               <div class="dd2_amount">
                                <span style="font-weight: 900;"><?php echo $map['totalprice']; ?> € TTC</span>
                               </div>
                             </div>
                            
                           </div>
                            
                        
                           

                          </div>
                                  
                         
                          
                        </div>
                         
                    </div>
                </div>
            </div>
   
        
     
        <div class="clearfix"></div>
        <div class="online" id="addquotesection_opt2" >
            <div class="col-md-12" style="margin-bottom: 10px;padding:0px;">
                <div class="col-md-6">
                      
                </div>
                <div class="col-md-6" style="padding:0px;">
                    <button  type="button" class="btn btn-default right" id="nextpaymentdetailbtns" style="font-weight: 900;">Next Payment Details <i class="fa fa-arrow-circle-o-right"></i> </button> 
                </div>
           </div>
       </div>
        <div class="online" id="addpaymentsection_opt2" style="display: none;">
            <div class="col-md-12" style="margin-bottom: 10px;padding:0px;">
                <div class="col-md-6">
                        <button  type="button" class="btn btn-default" id="backquotedetailsbtns" style="font-weight:900;margin-left: -15px;"> <i class="fa fa-arrow-circle-o-left"></i> Back To Quote  </button>
                </div>
                <div class="col-md-6" style="padding:0px;">
                    <button type="submit" class="btn btn-default right" id="confirmpaymentdetailsbtns" style="font-weight: 900;">Confirm Payment Methode<i class="fa fa-arrow-circle-o-right"></i> </button> 
                </div>
           </div>
       </div>
        <div class="clearfix"></div>
    </div>
  </div>
</div>
<?php form_close(); ?>

