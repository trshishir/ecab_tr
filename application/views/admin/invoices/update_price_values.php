 <!-- Fees -->
                           <div class="col-md-12  clearfix amoutndetaildiv" >

                              <div class="col-md-5" style="padding: 0px;">
                                <div class="dd1_amount">
                                 <span style="font-weight:900;">Fees</span>
                                </div>
                             </div>
                               <div class="col-md-3" style="padding: 0px;text-align: center;">
                                  <span style="font-weight:900;">%</span>
                               </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span style="font-weight:900;">Amount</span>
                             </div>
                           </div>
                              
                            </div>
                             <?php if($dbdiscountandfeedata->drivermealfee!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                             <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Driver Meal Fee :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align:center;">
                             
                                <span></span>
                             
                              </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $dbdiscountandfeedata->drivermealfee;?> €</span>
                             </div>  
                             </div>       
                            </div>
                          <?php endif; ?>
                            <?php if($dbdiscountandfeedata->driverrestfee!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                             <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Driver Rest Fee :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align:center;">
                             
                                <span></span>
                             
                              </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $dbdiscountandfeedata->driverrestfee;?> €</span>
                             </div>  
                             </div>       
                            </div>
                          <?php endif; ?>
                           <?php if($dbdiscountandfeedata->nighttimefee!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                             <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Night Fee :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align:center;">
                              <?php if($dbdiscountandfeedata->nighttimediscount!=0):  ?>
                                <span><?php echo $dbdiscountandfeedata->nighttimediscount;?>%</span>
                              <?php endif; ?>
                              </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $dbdiscountandfeedata->nighttimefee;?> €</span>
                             </div>  
                             </div>       
                            </div>
                          <?php endif; ?>

                            <?php if($dbdiscountandfeedata->notworkingdayfee!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                             <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Sunday And Not Working Day Fee :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;">
                              <?php if($dbdiscountandfeedata->notworkingdaydiscount!=0):  ?>
                                <span><?php echo $dbdiscountandfeedata->notworkingdaydiscount;?>%</span>
                              <?php endif; ?>
                             </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $dbdiscountandfeedata->notworkingdayfee;?> €</span>
                             </div>  
                             </div>       
                            </div>
                          <?php endif; ?>


                           <?php if($dbdiscountandfeedata->maydecdayfee!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                             <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>1st Mai And 25 December Fee :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;">
                               <?php if($dbdiscountandfeedata->maydecdaydiscount!=0):  ?>
                                <span><?php echo $dbdiscountandfeedata->maydecdaydiscount;?>%</span>
                              <?php endif; ?>
                             </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $dbdiscountandfeedata->maydecdayfee;?> €</span>
                             </div>   
                             </div>      
                            </div>
                          <?php endif; ?>
                           <!--Fees -->
                             <?php if($dbdiscountandfeedata->vatdiscount!=0): ?>
                              <div class="col-md-12  clearfix amoutndetaildiv" >

                              <div class="col-md-5" style="padding: 0px;">
                                <div class="dd1_amount">
                                 <span style="font-weight:900;">Vat</span>
                                </div>
                             </div>
                               <div class="col-md-3" style="padding: 0px;text-align: center;">
                                  <span style="font-weight:900;">%</span>
                               </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span style="font-weight:900;">Amount</span>
                             </div>
                           </div>
                              
                            </div>

                            <div class="col-md-12 clearfix amoutndetaildiv" >
                               <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span><?= $vatname ?> : </span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $dbdiscountandfeedata->vatdiscount;?>%</span></div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $dbdiscountandfeedata->vatfee;?> €</span>
                             </div>
                           </div>  
                            </div>
                             <?php endif; ?>
                             <div class="col-md-12  clearfix amoutndetaildiv" >

                              <div class="col-md-5" style="padding: 0px;">
                                <div class="dd1_amount">
                                 <span style="font-weight:900;">Discounts</span>
                                </div>
                             </div>
                               <div class="col-md-3" style="padding: 0px;text-align: center;">
                                  <span style="font-weight:900;">%</span>
                               </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span style="font-weight:900;">Amount</span>
                             </div>
                           </div>
                              
                            </div>
                
                          <?php if($dbdiscountandfeedata->welcomediscount!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                            <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Welcome Discount :</span>
                              </div>
                            </div>
                            <div class="col-md-3" style="padding: 0px; text-align: center;">
                             
                               <span><?php echo $dbdiscountandfeedata->welcomediscount;?>%</span>
                             
                            </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>-  <?php echo $dbdiscountandfeedata->welcomediscountfee;?> €</span>
                             </div>
                           </div>
                              
                            </div>
                          <?php endif; ?>
                        
                           <?php if($dbdiscountandfeedata->rewarddiscount!=0): ?>
                            <div class="col-md-12 clearfix amoutndetaildiv" >
                            <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Reward Discount :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $dbdiscountandfeedata->rewarddiscount;?>%</span></div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>-  <?php echo $dbdiscountandfeedata->rewarddiscountfee;?> €</span>
                             </div>
                           </div>
                              
                            </div>
                             <?php endif; ?>
                           <?php if($dbdiscountandfeedata->perioddiscount!=0): ?>
                            <div class="col-md-12 clearfix amoutndetaildiv" >
                               <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Period Discount :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $dbdiscountandfeedata->perioddiscount;?>%</span></div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>-   <?php echo $dbdiscountandfeedata->perioddiscountfee;?> €</span>
                             </div>
                           </div>
                              
                            </div>
                             <?php endif; ?>
                             <div class="col-md-12 clearfix amoutndetaildiv" id="promo_code_div" >
                               <?php if($dbdiscountandfeedata->promocodediscount!=0): ?>
                              
                            <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Discount Code :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $dbdiscountandfeedata->promocodediscount;?>%</span></div>
                              <div class="col-md-4" style="padding: 0px;">
                               <div class="dd2_amount">
                                 <span>-   <?php echo $dbdiscountandfeedata->promocodediscountfee;?> €</span>
                               </div>
                             </div>
                            <?php endif; ?>
                             
                              
                            </div>