<style>
    .minusrediconcustomreminder{
    border: 1px solid red;
    cursor: pointer;
    overflow: hidden;
    outline: none;
    font-size: 15px;
    color: #ffffff;
    height: 22px;
    width: 20px;
    border-radius: 50%;
    background-color: red;
    text-align: center;
    margin: 0px;
    line-height: 20px;
}
.minusrediconcustomreminder > i{
    margin:0px;
}
    .minusrediconcustompayment{
    border: 1px solid red;cursor: pointer;overflow: hidden;outline: none;font-size: 16px;
    color: #ffffff;
    height: 25px;
    width: 24px;
    border-radius: 50%;
    background-color: red;
    text-align: center;
    margin: 0px;
}
.minusrediconcustompayment > i{
    margin:0px;
}
.custompaymentset{
  padding-left: 0px !important;
  width: 20% !important;
}
  .truncate {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
    .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
  background:none;
}
  .nav-tabs > li.active {
    background: linear-gradient(#ffffff, #ffffff 25%, #d0d0d0) !important;
    border-bottom: none;
    }
  .nav-tabs > li {
    background-image: linear-gradient(to bottom, #fff 0, #e0e0e0 100%);
    border-left: 1px solid #ccc !important;
    height: 55px;
    margin: 0px !important;
    }

    .nav-tabs > li > a {

    color: #616161 !important;
    font-size: 12px;
    padding-left: 10px;
    display: block !important;
    width: 100% !important;
    height: 100% !important;
    text-transform: uppercase;
    font-weight: bold;
    transition: 0.2s;
    outline: none;
    border: none;
    border-radius: 0px;
}
.dataTables_wrapper .dataTables_filter {
    float: left;
    text-align: left;
    margin-left: 5px;
}
input[type="search"]{
    border:1px solid #cccccc;
    padding-left: 5px;
    border-radius: 0px;
}
input[type="search"]:focus{
    border:1px solid #cccccc !important;
}
     
    .tab-pane{
        padding: 30px 30px 30px 14px;
    }
    .configTable{
        padding: 0px;
    }
    .dataTables_wrapper .dataTables_filter input {
    margin-right: 2px;
    margin-left: 0 !important;
    max-width: 100% !important;
    width: 100% !important;
    float: right;
    }
    .dataTables_wrapper{
        margin-top: 20px;
    }
    .nav-tabs li a {
    color: #616161 !important;
    font-size: 11px;
    margin: 0px !important;
    padding: 15px;
    display: block !important;
    width: 100% !important;
    height: 100% !important;
    text-transform: uppercase;
    font-weight: bold;
    transition: 0.2s;
    outline: none;
}
.nav-tabs {
    border-bottom: none;
}
    
  .nav-tabs li a:hover,.nav-tabs li a:focus {
    /*background: linear-gradient(to bottom, #ececec 0%, #ececec 39%, #f0efef 39%, #b7b3b3 100%) !important;
    background:linear-gradient(to bottom,  #c1c1c1 0%, #ececec 39%, #ececec 39%, #fbfbfb 100%) !important;*/
    background: linear-gradient(to bottom, #c1c1c1 4%,#ececec 30%,#ececec 50%,#b7b3b3 100%) !important;
    color: #616161 !important;
    cursor: pointer;
}
.nav-tabs li.active a {
    /*background:linear-gradient(to bottom,  #c1c1c1 0%, #ececec 39%, #ececec 39%, #fbfbfb 100%) !important;*/
    background: linear-gradient(to bottom, #c1c1c1 4%,#ececec 30%,#ececec 50%,#b7b3b3 100%) !important;
}
/*
button.btn:hover, input.btn:hover{
    background-color: #e0e0e0;
    background-position: 0 -15px;
} */
.dataTables_wrapper .dataTables_paginate .paginate_button{
   color:#fff !important;
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current{
   color:#fff !important;
}

.dataTables_wrapper .dataTables_paginate .paginate_button:hover {
     background: linear-gradient(to bottom, #0d85a8 0%, #35add1 39%,#44c0e5 100%) !important;
   
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
     background: linear-gradient(to bottom, #0d85a8 0%, #35add1 39%,#44c0e5 100%) !important;
    color:#fff !important;
}
#DataTables_Table_0_filter label{
    display: none !important;
}
.form-control,input[type="text"]{
  height: 30px !important;
}
.searchbookbtn{
  font-size: 13px !important;
}
.bdr{
  border-radius: 4px;
}
.btn-default:focus {
  
    border: solid 1px #fff !important;
  }
 textarea.form-control {
    height: auto !important;
}
.tab-content .pick_up_txtbox_bkg {
    height: 40px !important;
}
.tab-content .form-control, .tab-content input[type="text"] {
    height: 40px !important;
}
.booking-tab .nav-tabs > li{
  height: auto !important;
}
.card {
    background-color: transparent;
}
.payment-method-buttons-list {
    background-image: none !important;  
    height: auto !important;
  }
.payment-method-buttons-list button{
  border: 1px solid #d0d0d0 !important;
}
#paymentmethoddiv input{
  height: 36px !important;
}
#paymentmethoddiv select{
  height: 36px !important;
}
.cardform-control {
    margin-bottom: 0px;
    padding-bottom: 0px;
    position: relative;
}
</style>


<section id="content">
    <div class="listDiv"><!--col-md-10 padding white right-p-->
    <?php $this->load->view('admin/common/breadcrumbs');?>
        <?php $this->load->view('admin/common/alert');?>
        <?php echo $this->session->flashdata('message'); ?>
 <div>
<div class="row">
  <div class="ListBooking">
  <input type="hidden" class="chk-Addbooking-btn" value="">
   <input type="hidden" class="Addbookingfullid" value="">
 
  <div class="col-md-12">
  <div class="module-body table-responsive">
    
       <div id="searchform">
        <?php
        $attributes = array("name" => 'booking_search_form',"class"=>'form-inline', "id" => 'booking_search_form');
        echo form_open('admin/invoices/get_bookings_bysearch',$attributes);
        ?>
            
               <div class="form-group">
                  <input type="text" name="otherkeyword"  class="form-control" placeholder="Search keyword"  value="<?= $search_otherkeyword; ?>"  />
              </div>
              <div class="form-group">
                 <input type="text" name="from_period" class="bsearchdatepicker bdr" placeholder="From" value="<?= $search_from_period; ?>" autocomplete="off"/> 
              </div>
              <div class="form-group">
                 <input type="text" name="to_period" class="bsearchdatepicker bdr" placeholder="To"  value="<?= $search_to_period; ?>" autocomplete="off"/> 
              </div>
              <div class="form-group">
                  <input type="text" name="keyword"  class="form-control" placeholder="Search by client"  value="<?= $search_keyword; ?>"  />
              </div>

               <div class="form-group">
                  <select name="regular" id="searchservicecategoryfield" style="float: left;width: 140px !important;max-width: 140px !important;" class="form-control" onchange="addsearchservice()">
                      <option value="">Service Category</option>
                                <?php
                                  foreach($service_cat as $key => $item){   
                                 ?>
                                   <option <?= ($search_regular==$item->id)?"selected":""; ?> value="<?= $item->id ?>"><?= $item->category_name ?></option>
                                   <?php 
                                   
                                      }    
                                     ?>
                    
                  </select> 
              </div>

              <div class="form-group">
                 <select name="service" id="searchservicefield" style="float: left;width:100px !important;" class="form-control">
                      <option value="">Service</option>
                      <?php  
                        $service=$this->bookings_model->get_services('vbs_u_service',['service_category' => $search_regular]);
                       foreach($service as $key => $item) { ?>
                                  <option <?= ($search_service==$item->id)?"selected":''; ?> value="<?= $item->id ?>"><?= $item->service_name ?></option>
                      <?php   }    ?>
                  </select> 
              </div>
               <div class="form-group">
                 <select name="paymentmethod" style="float: left;width: 140px !important;max-width: 140px !important;" class="form-control">
                      <option value="">Payment Methode</option>
                      <?php   foreach($paymentmethods as $key => $item) { ?>
                                  <option <?= ($search_methods==$item->id)?"selected":''; ?> value="<?= $item->id ?>"><?= $item->name ?></option>
                      <?php   }    ?>
                  </select> 
              </div>
               <div class="form-group">
                 <select name="statut" style="float: left;width:100px !important;" class="form-control">
                  <option value="">Statut</option>
                  <option <?= ($search_status== '0' )?"selected":''; ?> value="0">Pending</option>
                  <option <?= ($search_status== '1')?"selected":''; ?> value="1">Paid</option>
                  <option <?= ($search_status== '2')?"selected":''; ?> value="2">Cancelled</option>
                </select> 
              </div>
              
              <button type="submit" class="btn btn-default searchbookbtn">Search</button>
           <?php echo form_close(); ?>
       </div>
      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
                    <tr>
                                 <th class="no-sort text-center">#</th>
                                 <th class="text-center column-id">id</th>
                                 <th class="text-center column-civility">Date</th>
                                 <th class="text-center column-civility">Time</th>
                                 <th class="text-center column-first_name">Name</th>
                                 <th class="text-center column-first_name">Service Category</th>
                                 <th class="text-center column-first_name">Service</th>
                                <th class="text-center column-first_name">Notification</th>
                                 <th class="text-center column-first_name">File</th>
                                 <?php for($i=0;$i<3;$i++){ ?>
                                  <th class="text-center column-first_name">Reminder <?= $i+1 ?></th>
                                   <th class="text-center column-first_name">File</th>
                                 <?php } ?>

                                <th class="text-center column-civility">Price</th>
                                 <th class="text-center column-civility">Payment</th>
                                <th class="text-center column-first_name">File</th>
                                 <th class="text-center column-civility">Statut</th>
                                 <th class="text-center column-since">Since</th>
                    </tr>
          </thead>
        <tbody>
                        <?php if(isset($invoices) && !empty($invoices)):?>

                                <?php
                                  $notificationpendingstatus="1";
                                  $reminderpendingstatus="2";
                                 foreach($invoices as $key => $item):
                                   $invoicenotificationdata=$this->invoice_model->getinvoicenotification(['invoice_id'=>$item->invoice_id,'nt.status'=>$notificationpendingstatus]);
                                 $invoicereminderdata=$this->invoice_model->getinvoicereminders(['invoice_id'=> $item->invoice_id,'rm.status'=> $reminderpendingstatus]);
                                
                              
                                  ?>
 
                                    <tr>

                                        <td class="text-center">
                                          <input type="checkbox" class="chk-booking-ref" data-input="<?=$item->booking_id; ?>">
                                          

                                        </td>
                                          
                                        <td class="text-center">

                                            <a href="javascript:void()" onclick="BookingidEdit('<?= $item->booking_id ?>','<?= create_timestampdmy_uid($item->invoicedate,$item->invoice_id); ?>');" >                                          
                                                <?=create_timestampdmy_uid($item->invoicedate,$item->invoice_id);?>
                                            </a>

                                        </td>
                                      <td class="text-center"><?= from_unix_date($item->invoicedate); ?></td>
                                       <td class="text-center"><?= $item->invoicetime; ?></td>
                                         <td class="text-center">
                                            <?php 
                                          $user=$this->bookings_config_model->getuser($item->user_id);
                                          $user=str_replace(".", " ", $user);
                                          echo $user;
                                          ?></td>
                                         <td class="text-center"><?= $item->servicecat_name;?></td> 
                                         <td class="text-center"><?= $item->service_name;?></td>
                                     <?php if($invoicenotificationdata): ?>
                                        <td class="text-center"><?= from_unix_date($invoicenotificationdata->date).' '.$invoicenotificationdata->time; ?></td>
                                        <td class="text-center">
                                          <a href="<?= base_url(); ?>admin/invoices/notification_pdf/<?= $invoicenotificationdata->id  ?>/<?= $item->booking_id ?>" target="_blank">
                                            <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                                          </a>
                                        </td>
                                      <?php else: ?>
                                         <td class="text-center">-</td>
                                         <td class="text-center">-</td>
                                      <?php endif; ?>
                           <?php if($invoicereminderdata): ?>
                                         <!-- Show Reminder Icon -->
                                         <?php 
                                          $count=0;
                                          foreach ($invoicereminderdata as $reminder):
                                            if($count < 3):
                                          ?>
                                             <td class="text-center"><?= from_unix_date($reminder->date).' '.$reminder->time; ?></td>
                                             <td class="text-center">
                                                    <a href="<?= base_url(); ?>admin/invoices/reminder_pdf/<?= $reminder->id ?>/<?= ($count+1) ?>/<?= $item->booking_id ?>" target="_blank">
                                                      <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20" alt="Submit Form">
                                                    </a>
                                                  </td>
                                           <?php
                                            $count++;
                                             endif;
                                            endforeach;
                                          ?> 
                                         <?php
                                          if($count != 3):
                                            $remaincount=3 - $count;
                                            for($i =0 ; $i < $remaincount; $i++){
                                          ?>
                                           <td class="text-center">-</td>
                                            <td class="text-center">-</td>
                                         <?php
                                           }
                                          endif; 
                                          ?>   
                                         <!-- Show Reminder Icon -->

                                    <?php else: ?>
                                       <td class="text-center">-</td>
                                       <td class="text-center">-</td>
                                       <td class="text-center">-</td>
                                       <td class="text-center">-</td>
                                       <td class="text-center">-</td>
                                       <td class="text-center">-</td>
                                    <?php endif; ?>                               
                             <td class="text-center"><?=$item->totalprice;?> €</td>
                             <td class="text-center"><?=$item->payment_name;?></td>
                              <td class="text-center">
                              <a href="<?= base_url(); ?>admin/invoices/invoices_pdf/<?= $item->booking_id ?>" target="_blank">
                                <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20" alt="Submit Form">
                              </a>
               
                                </td>
                                       <?php

                                           if($item->invoicestatus== 0){
                                            echo "<td class='text-center'><span class='label label-warning'>Pending</span></td>";
                                           }
                                           elseif($item->invoicestatus== 1){
                                              echo "<td class='text-center'><span class='label label-success'>Paid</span></td>";
                                           }
                                           elseif($item->invoicestatus== 2){
                                          echo "<td class='text-center'><span class='label label-danger'>Cancelled</span></td>";
                                           }
                                           else{
                                             echo "<td class='text-center'></td>";
                                           }
                                       ?>
                                    
                                  <?php
                                  $currentdate=date("Y-m-d");
                                  $datestatus="0";
                                  $status=1;


                  $dbinvoicesdata=$this->bookings_config_model->getinvoicestatusdata('vbs_invoices_config');
                                  
                                       $date=$item->invoicedate;
                                  $availabledate=date('Y-m-d', strtotime($date. ' + '.$dbinvoicesdata->invoicedelay.' days'));
                                
                                  if($currentdate > $availabledate){
                                   $datestatus="1";
                                  }else{
                                   $datestatus="0";
                                  }
                                    
                                   ?>
                                        <?php
                                          if($datestatus == "1"){
                                          echo "<td class='text-center' style='white-space: nowrap'><span class='label label-danger'>".timeDiff($item->invoicecreateat)."</span></td>";
                                           }
                                           else{
                                             echo '<td class="text-center" style="white-space: nowrap">'.timeDiff($item->invoicecreateat).'</td>';
                                           }
                                           ?>
                                       

                                    </tr>

                                <?php endforeach; ?>

                            <?php endif; ?>
                    </tbody>
      </table>
      <br>
    </div>
  </div>
</div>


<div class="Bookingadd" style="display: none;">
  <div style="padding:0px 30px;">
   
 
<!-- add booking  -->
<div class="bookingaddsection">
 <?php  include 'booking_box_new.php' ?> 
</div>
<div class="bookingaddinvoicesection">
  
</div>

</div>
<!--  end booking -->
</div>
<!-- Invoice Add Section -->
<div class="Quoteadd" style="display: none;">
    <?php
      echo form_open('admin/quotes/addquoteinvoicerecord');
    ?>
    <div class="col-md-8" style="margin-bottom: 10px;padding:0px 15px;"> 
      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 booking_serivce_inner_tab" style="padding-left:0px;width: 18%;">
        <div class="form-group">
             <select name="bookingclientname"  class="servicecategoryinput" id="booking_client_id" onchange="editgetquotes()">
                   <option value="">&#xf007; Clients</option>
                  <?php foreach($clients_data as $key => $item): ?>
                         <option  value="<?= $item->id ?>">&#xf007; <?= $item->civility.' '.$item->first_name.' '.$item->last_name; ?></option>
                   <?php endforeach; ?>
             </select>
        </div>
    </div>      
     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 booking_serivce_inner_tab" style="padding-left:0px;width: 18%;">
        <div class="form-group">   
            <select name="bookingid"  class="servicecategoryinput" id="quote_field_id" required  onchange="QuoteViewFunc()" >
                   <option value="">&#xf02d; Quotes</option>
                 
             </select>
                  
        </div>
      </div> 
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 booking_serivce_inner_tab" id="quoteviewstatus" style="padding-left:0px;width: 18%;display: none;">
          <div class="form-group">
               <select name="quotestatus"  class="servicecategoryinput" required>
                     <option value="">Statut</option>
                     <option value="1">Accepted</option>
               </select>
          </div>
      </div> 
    </div>
  <div style="padding:0px 30px;">
   <div class="row">
    
    <div class="quoteviewsection">
   <!-- Quote Detail Section -->

   <!-- Quote Detail Section -->
      
    </div>
   
  </div>
 </div>

     <div class="row submit-row" style="display: none;padding:0px 30px;" id="invoiceaddbtns">
      <div class="col-md-12" style="padding:0px;margin-top:10px;">                               
        
             <button type="submit" class="btn btn-default" style=" float:right; margin-left:7px;font-size: 18px !important;"><span class="fa fa-save"></span> Add </button>
            <a href="<?= base_url(); ?>admin/invoices"  class="btn btn-default" style="float:right; margin-left:7px;font-size: 18px !important;" ><span class="fa fa-close"> Cancel </span></a>

      </div>         
    </div>
    <?php echo form_close(); ?>
</div>

<!-- Invoice Add Section -->
<div class="Bookingdetail" style="display:none">
  <div style="padding:0px 30px;">
   <div class="row">
    
    <div class="bookingdetailajaxsection">
   
   </div>
   
</div>
</div>
 
</div>
<div class="Bookingedit" style="display:none">
  <div style="padding:0px 30px;">
   <div class="row">
    <?php
 $attributes = array("name" => 'booking_form', "id" => 'edit_booking_form');
  echo form_open('admin/invoices/update_booking_data',$attributes);
?>
    <div class="bookingeditajaxsection">
   
   </div>
    <div class="row submit-row">
      <div class="col-md-12">                               
        <button type="submit" class="btn btn-default" style=" float:right; margin-left:7px;font-size: 18px !important;"><span class="fa fa-save"></span> Save </button>
            <a href="<?= base_url(); ?>admin/invoices"  class="btn btn-default" style="float:right; margin-left:7px;font-size: 18px !important;" ><span class="fa fa-close"> Cancel </span></a>
                   
      </div>
         
        </div>
     <?php echo form_close(); ?>
</div>
</div>
 
</div>
<!-- Export Excel -->
 <?php
    $attributes = array("name" => 'excel_export_form', "id" => 'excel_export_form');   
    echo form_open('admin/invoices/export_excel_file',$attributes);
  ?>
<?php echo form_close(); ?>
<!-- Export Excel -->
<div class="col-md-12 Bookingdelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
  <?=form_open("admin/invoices/delete_invoices_record")?>
    <input  name="tablename" value="vbs_job_statut" type="hidden" >
    <input type="hidden" id="bookingdeletid" name="delet_booking_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn btn-default"style=" float:left;margin-right: 10px;"><span class="fa fa-save"></span> Yes </button>

     <button type="button" class="btn btn-default" style="cursor: pointer;" onclick="cancelBooking()"><span class="fa fa-close"></span>No</button>
  <?php echo form_close(); ?>
</div>
</div>
</div>
</div>
</section>
<?php include 'editbookingscript.php' ?>
<script type="text/javascript">
  //bookings index 

    $(window).load(function() {
    $("#searchform").appendTo("#DataTables_Table_0_filter");
  });

    
function cancelBooking()
{
  $('#adddisbreadcrumb').remove();
   $('#editdisbreadcrumb').remove();
      setupDivBooking();
  $(".ListBooking").show();
}
     
 function ExcelExport(){
$('#excel_export_form').submit();
}
 
function Bookingadd()
{
  
   $('#adddisbreadcrumb').remove();
   $(".breadcrumb").append("<span id='adddisbreadcrumb'> > Add Invoice</span>");
   setupDivBooking();
   $(".Bookingadd").show();
}
function Quoteadd(){
   $('#adddisbreadcrumb').remove();
   $(".breadcrumb").append("<span id='adddisbreadcrumb'> > Add Invoice</span>");
   setupDivBooking();
   $(".Quoteadd").show();
  
}




function BookingDelete()
{
  var val = $('.chk-Addbooking-btn').val();
  if(val != "")
  {
  setupDivBooking();
  $(".Bookingdelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivBooking()
{
  // $(".discountEditEdit").hide();
  $(".Bookingadd").hide();
  $(".Bookingedit").hide();
  $(".ListBooking").hide();
  $(".Bookingdelete").hide();
   $(".Bookingdetail").hide();

}
$('input.chk-booking-ref').on('change', function() {
  $('input.chk-booking-ref').not(this).prop('checked', false);
  var parent= $(this).parent();
  var sibling=$(parent).next().html();
  var id = $(this).attr('data-input');
  $('.chk-Addbooking-btn').val(id);
  $('#bookingdeletid').val(id);
  $('.Addbookingfullid').val(sibling);
});


//edit discount period and promo code

  $(document).ready(function () {
    $(window).on('load', function() {
       bookingEvent();
      
  
      });
     function bookingEvent(){
     $('.dt-buttons>.buttons-excel').remove();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button type="button" id="bookingaddfunc" class="dt-button buttons-copy buttons-html5"  onclick="Quoteadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="BookingEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="BookingDelete()"><i class="fa fa-trash"></i> Delete</span></button>\n\<button type="button"  class="dt-button buttons-copy buttons-html5"  onclick="ExcelExport()">Excel</button></div>';
        $('.addevent').append(html);
    }
   

  });
  //booking index
</script>

<?php  include 'bookingscript.php' ?> 