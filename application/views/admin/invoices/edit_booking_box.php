<?php   
$displaypassengers=$this->session->userdata['passengers'];
$stopcount=count($dbstopsdata);
$ispickmonday='0';
$ispicktuesday='0';
$ispickwednesday='0'; 
$ispickthursday='0';
$ispickfriday='0';
$ispicksaturday='0';
$ispicksunday='0';

$pickmondaytime=date('h : i');
$picktuesdaytime=date('h : i');
$pickwednesdaytime=date('h : i');
$pickthursdaytime=date('h : i');
$pickfridaytime=date('h : i');
$picksaturdaytime=date('h : i');
$picksundaytime=date('h : i');


$isreturnmonday='0';
$isreturntuesday='0';
$isreturnwednesday='0'; 
$isreturnthursday='0';
$isreturnfriday='0';
$isreturnsaturday='0';
$isreturnsunday='0';

$returnmondaytime=date('h : i');
$returntuesdaytime=date('h : i');
$returnwednesdaytime=date('h : i');
$returnthursdaytime=date('h : i');
$returnfridaytime=date('h : i');
$returnsaturdaytime=date('h : i');
$returnsundaytime=date('h : i');

$pickairportarrivaltime=(empty($dbpickairportdata->arrivaltime))?date('h : i'):$dbpickairportdata->arrivaltime;
$picktrainarrivaltime=(empty($dbpicktraindata->arrivaltime))?date('h : i'):$dbpicktraindata->arrivaltime;
$dropairportarrivaltime=(empty($dbdropairportdata->arrivaltime))?date('h : i'):$dbdropairportdata->arrivaltime;
$droptrainarrivaltime=(empty($dbdroptraindata->arrivaltime))?date('h : i'):$dbdroptraindata->arrivaltime;


$start_date=date('d/m/Y');
$end_date=date('d/m/Y');
$pick_date=date('d/m/Y');
$return_date=date('d/m/Y');

$start_time=date('h : i');
$end_time=date('h : i');
$pick_time=date('h : i');
$return_time=date('h : i');
$waiting_time=date('h : i');



$client_id=$dbbookingdata->user_id;
$driver_id=$dbbookingdata->driver_id;
$pick_status="1";
$drop_status="1";
$servicecat_id=$dbbookingdata->service_category_id;
$packagedepart_id=$dbbookingdata->pickuppackage_id;
$packagedestincation_id=$dbbookingdata->droppackage_id;
$pickairportpoiid=$dbpickairportdata->poi_id;
$picktrainpoiid=$dbpicktraindata->poi_id;
$pickparkpoiid=$dbbookingdata->pickuppark_id;
$pickhotelpoiid=$dbbookingdata->pickuphotel_id;
$dropairportpoiid=$dbdropairportdata->poi_id;
$droptrainpoiid=$dbdroptraindata->poi_id;
$dropparkpoiid=$dbbookingdata->droppark_id;
$drophotelpoiid=$dbbookingdata->drophotel_id;
$pick_form =  $this->session->flashdata('pick_form');
$drop_form =  $this->session->flashdata('drop_form');
$service_form =  $this->session->flashdata('service_form');
$car_name=$this->bookings_model->getcarname($dbbookingdata->car_id);
$client=$this->bookings_model->getclientdatabyid($dbbookingdata->user_id);
$servicedata= $this->bookings_model->getservicesbyid('vbs_u_service', $dbbookingdata->service_id);
$vatdata= $this->bookings_model->getservicesbyid('vbs_u_vat', $servicedata->tva);
$vatname=$vatdata->name_of_var;

     
      $pickairportpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$pickairportpoiid);
      $picktrainpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$picktrainpoiid);
      $pickhotelpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$pickhotelpoiid);
      $pickparkpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$pickparkpoiid);


        
      $dropairportpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$dropairportpoiid);
      $droptrainpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$droptrainpoiid);
      $drophotelpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$drophotelpoiid);
      $dropparkpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$dropparkpoiid);

      
       $packagedata=$this->bookings_model->poidatarecord('vbs_u_package',$packagedepart_id);
       $packagepoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$packagedata['departure']);
       $poicategoryrecord=$this->bookings_model->poidatarecord('vbs_u_ride_category',$packagepoirecord['category_id']);


       $droppackagedata=$this->bookings_model->poidatarecord('vbs_u_package',$packagedestincation_id);
       $droppoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$droppackagedata['destination']);
       $poidropcategoryrecord=$this->bookings_model->poidatarecord('vbs_u_ride_category',$droppoirecord['category_id']);
             
               

       if(!empty($dbbookingdata->start_date) || $dbbookingdata->start_date != null){ 
         $time = strtotime($dbbookingdata->start_date);
         $newformat = date('d/m/Y',$time);
         $start_date= $newformat;
       }
       if(!empty($dbbookingdata->end_date) || $dbbookingdata->end_date != null){ 
         $time = strtotime($dbbookingdata->end_date);
         $newformat = date('d/m/Y',$time);
         $end_date= $newformat;
       }
       if(!empty($dbbookingdata->pick_date) || $dbbookingdata->pick_date != null){ 
         $time = strtotime($dbbookingdata->pick_date);
         $newformat = date('d/m/Y',$time);
         $pick_date= $newformat;
       }
       if(!empty($dbbookingdata->return_date) || $dbbookingdata->return_date != null){ 
         $time = strtotime($dbbookingdata->return_date);
         $newformat = date('d/m/Y',$time);
         $return_date= $newformat;
       }

       foreach($dbpickregulardata as $regular){
        if(strtolower($regular->day)=='monday'){
             $ispickmonday='1';
             $pickmondaytime=$regular->time;
           }
          elseif(strtolower($regular->day)=='tuesday'){
             $ispicktuesday='1';
             $picktuesdaytime=$regular->time;
           }
          elseif(strtolower($regular->day)=='wednesday'){
             $ispickwednesday='1';
             $pickwednesdaytime=$regular->time;
           }
          elseif(strtolower($regular->day)=='thursday'){
             $ispickthursday='1';
             $pickthursdaytime=$regular->time;
           }
          elseif(strtolower($regular->day)=='friday'){
             $ispickfriday='1';
             $pickfridaytime=$regular->time;
           }
          elseif(strtolower($regular->day)=='saturday'){
             $ispicksaturday='1';
             $picksaturdaytime=$regular->time;
           }
          elseif(strtolower($regular->day)=='sunday'){
             $ispicksunday='1';
             $picksundaytime=$regular->time;
           }         
      }
    foreach($dbreturndata as $return){
      if(strtolower($return->day)=='monday'){
           $isreturnmonday='1';
           $returnmondaytime=$return->time;
         }
        elseif(strtolower($return->day)=='tuesday'){
           $isreturntuesday='1';
           $returntuesdaytime=$return->time;
         }
        elseif(strtolower($return->day)=='wednesday'){
           $isreturnwednesday='1';
           $returnwednesdaytime=$return->time;
         }
        elseif(strtolower($return->day)=='thursday'){
           $isreturnthursday='1';
           $returnthursdaytime=$return->time;
         }
        elseif(strtolower($return->day)=='friday'){
           $isreturnfriday='1';
           $returnfridaytime=$return->time;
         }
        elseif(strtolower($return->day)=='saturday'){
           $isreturnsaturday='1';
           $returnsaturdaytime=$return->time;
         }
        elseif(strtolower($return->day)=='sunday'){
           $isreturnsunday='1';
           $returnsundaytime=$return->time;
         }
    }
    if(!empty($dbbookingdata->start_time) || $dbbookingdata->start_time != null){   
       $start_time= $dbbookingdata->start_time;
     }
     if(!empty($dbbookingdata->end_time) || $dbbookingdata->end_time != null){  
       $end_time= $dbbookingdata->end_time;
     }
     if(!empty($dbbookingdata->pick_time) || $dbbookingdata->pick_time != null){    
       $pick_time= $dbbookingdata->pick_time;
     }
     if(!empty($dbbookingdata->return_time) || $dbbookingdata->return_time != null){  
       $return_time= $dbbookingdata->return_time;
     }
     if(!empty($dbbookingdata->waiting_time) || $dbbookingdata->waiting_time != null){   
       $waiting_time= $dbbookingdata->waiting_time;
     }
     
   
?>



<div class="col-lg-8 col-md-8 col-sm-8" style="padding: 0px;">
  <?php include 'edit_booking_categoryarea.php' ?>
<div id="editbookingaddsection" style="margin-top: 10px;">
        <div class="col-md-12" style="margin-top: 10px;">
                <div class="col-md-2 quotestatushiddendiv" style="padding-left: 0px;width: 18%;"></div>
               <div class="col-md-2" style="padding-left: 0px;width: 18%;">
              
                <p style="font-size:13px !important;">
                  <?php echo "<span style='font-weight:900;'>".$client->civility.' '.$client->first_name.' '.$client->last_name.'</span><br>'.$client->address.'<br>';?>
                 <?php if(!empty($client->address1) || $client->address1 != null):  ?>
                  <?php echo $client->address1.'<br>'; ?>
                 <?php endif; ?>
                 <?php echo $client->zipcode.'  '.$client->city;?></p>
               </div> 
            <div class="col-md-6" style="width: 42%;padding-left:0px;">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xs-12" style="padding:0px;">
                
                   <ul class="ulbdnone" id="editpassengerdiv" style="list-style-type:none;padding-left:0px;">
                     <?php foreach($displaypassengers as $passenger): ?>
                       <li class="text-left clearfix" >
                        <div style="float:left;"><span class="fa fa-user" style="margin-right:5px;"></span><span><?php echo $passenger['name'];?></span>, <span class="fa fa-mobile" style="margin-right:5px;"></span><span><?php echo $passenger['phone'];?></span>, <span class="fa fa-phone" style="margin-right:5px;"></span><span><?php echo $passenger['home'];?></span>, <span class="fa fa-wheelchair" style="margin-right:5px;"></span><span><?= $passenger['disable'] ?></span></div>
                       
                             <div style="float:left;"><button type="button" class="minusredicon" onclick="editremovepassengers(<?= $passenger['id'] ?>)" ><i class="fa fa-minus"></i></button></div>
                      </li>

                      <?php endforeach; ?>   
                       
                   </ul>
             
              </div>
            </div>                       
            </div>
  <div class="col-lg-12 col-md-12 col-sm-12" style="margin-bottom:10px;">
     <div class="col-lg-5 col-md-5 col-sm-12" style="padding:0px;width: 46%;">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 booking_serivce_inner_tab" style="padding-left:0px;">
      <div class="form-group">
         <div class="input-group">
            <span class="input-group-addon">
            <i class="fa fa-car"></i>
            </span>
        <select name="pickservicecategory"  class="servicecategoryinput" id="editpickservicecategoryinput" onchange="editaddservice()" required>
                      <option value="">Service Category</option>
                                <?php
                                  foreach($service_cat as $key => $item){   
                                 ?>
                                   <option <?= ($servicecat_id==$item->id)?"selected":""; ?> value="<?= $item->id ?>"><?= $item->category_name ?></option>
                                   <?php 
                                   
                                      }    
                                     ?>
                  </select>
                   </div>
                   <?php
                     if(isset($service_form['service_category']['error']) && !empty($service_form['service_category']['error'])){     
                          echo "<div class='invalid-error'>".$service_form['service_category']['error']."</div>";
                            }
                      ?>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 booking_serivce_inner_tab" style="padding-right:0px;">
      <div class="form-group">
           <div class="input-group">
                <span class="input-group-addon">
                <i class="fa fa-car"></i>
                </span>
          <select name="pickservice"   class="pickserviceinput" id="editpickserviceinput" required>
                      <option value="">Service</option>
                               <?php
                                     $service=$this->bookings_model->get_services('vbs_u_service',['service_category' => $servicecat_id]);
                                   foreach($service as $key => $item){
                                 
                                 ?>
                                   <option <?= ($dbbookingdata->service_id==$item->id)?"selected":""; ?> value="<?= $item->id ?>"><?= $item->service_name ?></option>
                                   <?php 
                                   
                                      }    
                                     ?>
                  </select>
                </div>
                   <?php
                     if(isset($service_form['service']['error']) && !empty($service_form['service']['error'])){     
                          echo "<div class='invalid-error'>".$service_form['service']['error']."</div>";
                            }
                      ?>
                    </div>
    </div>
    </div>
  </div>
 <input type="hidden" name="booking_id" value="<?= $dbbookingdata->id; ?>">
   <?php
$flashAlert =  $this->session->flashdata('alert');
if(isset($flashAlert['message']) && !empty($flashAlert['message'])){?>
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div style="padding: 5px 12px" class="alert <?=$flashAlert['class']?>">
        <strong><?=$flashAlert['type']?></strong> <?=$flashAlert['message']?>
        <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
  </div>
<?php } ?>

  <div class="col-lg-12 col-md-12 col-sm-12" style="width: 100%;">
    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12  booking_tab_size" style="padding: 0px;width: 46%;">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
        <div class="booking-tab">
           <input type="hidden"  id="editpickupcategory" name="pickupcategory" value="<?= $dbbookingdata->pickupcategory ?>">
         
        
          <ul class="nav nav-tabs">
           
             <li><button class="btn btn-default extra-pad booking" style="font-weight:900;pointer-events: none;padding-bottom: 11px;border-radius:0px;width:100%; background-image: linear-gradient(rgb(255, 255, 255) 0px, rgb(224, 224, 224) 100%) !important;color: #616161 !important;"> <?php echo 'Pickup'; ?></button></li>
              <li <?= $dbbookingdata->pickupcategory=="address" ? 'class="active"':''?> style="width:14.5%;"><a id="editpickaddresscategorybtn" data-toggle="tab" href="#editpick_address"><i class="fa fa-map-marker"></i> <?php echo 'Address'; ?></a></li>
             <li <?=$dbbookingdata->pickupcategory=="package"? 'class="active"':''?> style="width:16%;"><a id="editpickpackagecategorybtn" data-toggle="tab" href="#editpick_package"><i class="fa fa-suitcase"></i> <?php echo 'Package'; ?></a></li>
             <li <?=$dbbookingdata->pickupcategory=="airport"? 'class="active"':''?> style="width:15.5%;"><a id="editpickairportcategorybtn" data-toggle="tab" href="#editpick_airport"><i class="fa fa-plane"></i> <?php echo 'Airport'; ?></a></li>
             <li <?=$dbbookingdata->pickupcategory=="train"? 'class="active"':''?>><a id="editpicktraincategorybtn" data-toggle="tab" href="#editpick_train"><i class="fa fa-train"></i> <?php echo 'Train'; ?></a></li>
             <li <?=$dbbookingdata->pickupcategory=="hotel"? 'class="active"':''?>><a id="editpickhotelcategorybtn" data-toggle="tab" href="#editpick_hotel"><i class="fa fa-hotel"></i> <?php echo 'Hotel'; ?></a></li>
             <li <?=$dbbookingdata->pickupcategory=="park"? 'class="active"':''?>><a id="editpickparkcategorybtn" data-toggle="tab" href="#editpick_park"><i class="fa fa-tree"></i> <?php echo 'Park'; ?></a></li>
             
         </ul>
        </div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
        <div class="tab-content">

            <div id="editpick_address" class="tab-pane fade  <?=$dbbookingdata->pickupcategory== 'address'? 'in active':''?>">
               <input type="hidden" id="editpickloc_lat" />
               <input type="hidden" id="editpickloc_long" />
                <div class="row">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                   <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;margin:8px 0px 0px -5px;"></i>
                 </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                   <div class="form-group edit-cardform-control">
                      <div class="input-group" style="position: relative;">
                        <span class="input-group-addon">
                        <i class="fa fa-map-marker"></i>
                        </span>
                        
                         <input type="hidden" name="pickaddressfield" placeholder="Addresse line"  class="pick_up_txtbox_bkg" id="editpickupaddressfield"
                          value="<?= $dbpickaddressdata->address; ?>" >
                          <?php
                          $pickshortaddress='';
                            
                              $pickshortaddress=$dbpickaddressdata->address;                           
                              $pickshortaddress= str_replace(($dbpickaddressdata->city), "", $pickshortaddress);
                                                                                                
                              $pickshortaddress= str_replace(($dbpickaddressdata->zipcode), "",  $pickshortaddress);
                                
                            
                                  $pickshortaddress=str_ireplace("france","",$pickshortaddress);
                                  $pickshortaddress=rtrim($pickshortaddress,' ');
                                  $pickshortaddress=rtrim($pickshortaddress,',');
                                  $pickshortaddress = preg_replace('!\s+!', ' ', $pickshortaddress);
                                 
                              

                           ?>

                         <input type="text" placeholder="Addresse line"  class="pick_up_txtbox_bkg" id="editpickupautocomplete"   onFocus="geolocate()"
                          value="<?php echo $pickshortaddress  ?>"  style="padding-right: 30px;">
                        <div id="editcurrentuserpoibtn" onclick="editgetCurrentLocation()">
                          <img src="<?= base_url()?>assets/theme/default/images/target-a.svg">
                        </div>

                       
                      <div id="editcurrentuserpoidiv">
                      <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker" style="top: 4px;"></i>
                         <input type="text" readonly id="editcurrentuserpoiinput" onclick="editsetPickCurrentLocation()">
                          
                         
                      </div> 
                   </div>
                      </div>
                      <div id="editlocationloaderdiv">
                        <img src="<?= base_url()?>assets/theme/default/images/loader_2.gif" >
                      </div>
                         
                      </div>
                        <?php
                         if(isset($pick_form['pick_address']['error']) && !empty($pick_form['pick_address']['error'])){     
                              echo "<div class='invalid-error'>".$pick_form['pick_address']['error']."</div>";
                                }
                          ?>

                          <small>Required</small>
                   </div>
                 </div>
               </div>

              <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text" name="pickotheraddressfield"  placeholder="Complément d'adresse"  class="pick_up_txtbox_bkg" value="<?= $dbpickaddressdata->otheraddress ?>" style="padding-left:43px !important;">
                          <?php
                         if(isset($pick_form['pick_additional_address']['error']) && !empty($pick_form['pick_additional_address']['error'])){     
                              echo "<div class='invalid-error'>".$pick_form['pick_additional_address']['error']."</div>";
                                }
                          ?>
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px 5px 0px 0px;">
                          <div class="form-group edit-cardform-control">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                               <input type="text" name="pickzipcodefield" placeholder="Zip code"  class="pick_up_txtbox_bkg" id="editpickup_code" value="<?= $dbpickaddressdata->zipcode; ?>">
                                
                            </div>
                             <?php
                                 if(isset($pick_form['pick_zip_code']['error']) && !empty($pick_form['pick_zip_code']['error'])){     
                                      echo "<div class='invalid-error'>".$pick_form['pick_zip_code']['error']."</div>";
                                        }
                                  ?>
                                 <small>Required</small> 
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px 0px 0px 5px;">
                              <div class="form-group edit-cardform-control">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text" name="pickcityfield" placeholder="City"  class="pick_up_txtbox_bkg" id="editpickup_city" value="<?=  $dbpickaddressdata->city ?>">
                                         
                                    </div>
                                    <?php
                                         if(isset($pick_form['pick_city']['error']) && !empty($pick_form['pick_city']['error'])){     
                                              echo "<div class='invalid-error'>".$pick_form['pick_city']['error']."</div>";
                                                }
                                          ?>
                                      <small>Required</small>    
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
            </div>
          <div id="editpick_package" class="tab-pane fade <?=$dbbookingdata->pickupcategory== 'package'? 'in active':''?>">
              <div class="row">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                     <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                  </div>
                  <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-suitcase"></i>
                        </span>
                        
                     <select name="pickpackagefield" id="editpickpackagefield"  class="packagebox_pick_bkg " onchange="editaddpoidata()">
                       <option value="">Select</option>
                 
                       <?php
                        foreach($poi_package as $package){
                         ?>
                       <option <?=  ($dbbookingdata->pickuppackage_id==$package->id)?"selected":""; ?> value="<?= $package->id ?>"><?= $package->name ?></option>
                       <?php  }  ?>
                      </select>
                    </div>
                               <?php
                                 if(isset($pick_form['pick_package']['error']) && !empty($pick_form['pick_package']['error'])){     
                                      echo "<div class='invalid-error'>".$pick_form['pick_package']['error']."</div>";
                                        }
                                  ?>
                                  <small>Required</small>
                      </div>
                  </div>
              </div> 
              
                <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-bullseye"></i>
                              </span>
                             <input type="text"   id="editpackagedepartcategoryname" value="<?= $poicategoryrecord['ride_cat_name']  ?>" disabled style="background-color: #f6f6f6;" >
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-bullseye"></i>
                                      </span>
                                       <input type="text"   id="editpackagedepartname" value="<?= $packagepoirecord['name']  ?>" disabled style="background-color: #f6f6f6;" >
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="editpackagedepartaddress" placeholder="Addresse" value="<?= $packagepoirecord['address']  ?>" disabled style="background-color: #f6f6f6;" >
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
               <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="editpackagedepartzipcode" placeholder="Zip code" value="<?= $packagepoirecord['postal']  ?>" disabled style="background-color: #f6f6f6;" >
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="editpackagedepartcity" placeholder="City" value="<?= $packagepoirecord['ville']  ?>" disabled style="background-color: #f6f6f6;" >
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
             
          </div>


          <div id="editpick_airport" class="tab-pane fade <?=$dbbookingdata->pickupcategory== 'airport'? 'in active':''?>">
               <div class="row">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                      <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                  </div>
                  <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                     <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-plane"></i>
                        </span>

                      <select name="pickairportfield" id="editairport_pick"  class="airlocation_pick_bkg " onchange="editgetpickcategorydata(this)">
                       <option value="">Select</option>
                 
                       <?php
                        foreach($poi_data as $airport){ 
                       $category=$this->bookings_model->getcategory('vbs_u_ride_category',$airport->category_id);
                                 
                          if(strtolower($category)=="airport"){
                         ?>
                       <option <?php echo ($pickairportpoiid==$airport->id)?"selected":"";?> value="<?= $airport->id ?>"><?= $airport->name ?></option>
                       <?php 
                             }
                              }    
                             ?>
                      </select>
                    </div>
                               <?php
                                 if(isset($pick_form['pick_airport']['error']) && !empty($pick_form['pick_airport']['error'])){     
                                      echo "<div class='invalid-error'>".$pick_form['pick_airport']['error']."</div>";
                                        }
                                  ?>
                                  <small>Required</small>
                     </div>             
                  </div>
               </div>
               <div class="row" style="margin-top:5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2"></div>
                  <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small edit-cardform-control" style="padding: 0px;padding-right:5px;">
                      <input type="text"  name="pickairportflightnumberfield" placeholder="Flight No" class="pickup_flight_no_bkg " id="editpickup_flight_no" 
                           value="<?= $dbpickairportdata->flightnumber; ?>">
                            <?php
                                 if(isset($pick_form['pick_airport_number']['error']) && !empty($pick_form['pick_airport_number']['error'])){     
                                      echo "<div class='invalid-error'>".$pick_form['pick_airport_number']['error']."</div>";
                                        }
                                  ?>
                         <small style="margin-left:0px !important;">Required</small>         
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small col-arrival" style="padding: 0px;padding-left:5px;">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding:0px;padding-top: 9px;">
                      <label for="timepicker_pick_fly">Arrival Time</label>
                    </div>
                     <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                         <input name="pickairportarrivaltimefield" type="text" id="edittimepicker_pick_fly"  value="<?= $pickairportarrivaltime ?>"  class="pickup_flight_time_bkg" />
                           <?php
                                 if(isset($pick_form['pick_airport_arrival']['error']) && !empty($pick_form['pick_airport_arrival']['error'])){     
                                      echo "<div class='invalid-error'>".$pick_form['pick_airport_arrival']['error']."</div>";
                                        }
                                  ?>
                      </div>
                  </div>
                </div>
                </div>
               </div>
                <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="editpickairportaddress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $pickairportpoirecord['address'] ?>" >
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="editpickairportzipcode" placeholder="Zip code" disabled style="background-color: #f6f6f6;"  value="<?= $pickairportpoirecord['postal'] ?>">
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="editpickairportcity" placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $pickairportpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>

          </div>

          <div id="editpick_train" class="tab-pane fade <?=$dbbookingdata->pickupcategory== 'train'? 'in active':''?>">
                  <div class="row">
                      <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                          <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                      </div>
                      <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                      <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-train"></i>
                        </span>
                          <select name="picktrainfield" class="trainlocation_pick_bkg " id="edittrain_pick"  onchange="editgetpickcategorydata(this)">
                            <option value="">Select</option>
                              <?php
                                foreach($poi_data as $train){
                                   $category=$this->bookings_model->getcategory('vbs_u_ride_category',$train->category_id);
                                 
                                  if(strtolower($category)=="train"){
                                 ?>
                            <option <?= ($picktrainpoiid==$train->id)?"selected":""; ?> value="<?= $train->id ?>"><?= $train->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                          </select>
                        </div>
                           <?php
                                 if(isset($pick_form['pick_train']['error']) && !empty($pick_form['pick_train']['error'])){     
                                      echo "<div class='invalid-error'>".$pick_form['pick_train']['error']."</div>";
                                        }
                                  ?>
                                  <small>Required</small>
                          </div>        
                      </div>
                  </div>
                  <div class="row" style="margin-top:5px;">
                    <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2"></div>
                     <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small edit-cardform-control" style="padding: 0px;padding-right:5px;">
                            <input id="editpickup_train_no" class="pickup_train_no_bkg" type="text"  name="picktrainnumberfield" placeholder="Train No" value="<?= $dbpicktraindata->trainnumber ?>">
                             <?php
                                   if(isset($pick_form['pick_train_number']['error']) && !empty($pick_form['pick_train_number']['error'])){     
                                        echo "<div class='invalid-error'>".$pick_form['pick_train_number']['error']."</div>";
                                          }
                                    ?>
                              <small style="margin-left: 0px !important;"></small>      
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small col-arrival" style="padding: 0px;padding-left:5px;">
                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding:0px;padding-top: 9px;">
                            <label for="timepicker_pick_stn">Arrival Time</label>
                          </div>
                           <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                             <input  class="pickup_flight_time_bkg" id="edittimepicker_pick_stn" name="picktrainarrivaltimefield" type="text" value="<?= $picktrainarrivaltime ?>" />
                               <?php
                                   if(isset($pick_form['pick_train_arrival']['error']) && !empty($pick_form['pick_train_arrival']['error'])){     
                                        echo "<div class='invalid-error'>".$pick_form['pick_train_arrival']['error']."</div>";
                                          }
                                    ?>
                            </div> 
                        </div>
                      </div>
                    </div>
                  </div>

               <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="editpicktrainaddress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $picktrainpoirecord['address'] ?>">
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="editpicktrainzipcode" placeholder="Zip code"  disabled style="background-color: #f6f6f6;" value="<?= $picktrainpoirecord['postal'] ?>" >
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="editpicktraincity" placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $picktrainpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
          </div>


          <div id="editpick_hotel" class="tab-pane fade <?=$dbbookingdata->pickupcategory== 'hotel'? 'in active':''?>">
           <div class="row">
              <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                  <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
              </div>
              <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                 <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-hotel"></i>
                        </span>
                  <select name="pickhotelfield" class="hotellocation_pick_bkg" id="edithotel_pick"  onchange="editgetpickcategorydata(this)">
                      <option value="">Select</option>
                      <?php
                            foreach($poi_data as $hotel){
                               $category=$this->bookings_model->getcategory('vbs_u_ride_category',$hotel->category_id);

                              if(strtolower($category)=="hotel"){
                             ?>
                           <option <?=  ($pickhotelpoiid==$hotel->id)?"selected":""; ?> value="<?= $hotel->id ?>"><?= $hotel->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                  </select>
                </div>
                    <?php
                                 if(isset($pick_form['pick_hotel']['error']) && !empty($pick_form['pick_hotel']['error'])){     
                                      echo "<div class='invalid-error'>".$pick_form['pick_hotel']['error']."</div>";
                                        }
                                  ?>
                       <small style="margin-left: 0px !important;">Required</small>           
              </div>
            </div>
            </div>
             <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="editpickhoteladdress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $pickhotelpoirecord['address'] ?>">
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="editpickhotelzipcode" placeholder="Zip code"  disabled style="background-color: #f6f6f6;" value="<?= $pickhotelpoirecord['postal'] ?>">
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="editpickhotelcity"  placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $pickhotelpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
          </div>


          <div id="editpick_park" class="tab-pane fade <?=$dbbookingdata->pickupcategory== 'park'? 'in active':''?>">
           <div class="row">
              <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                  <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i> 
              </div>
              <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                  <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-tree"></i>
                        </span>
                  <select name="pickparkfield" class="parklocation_pick_bkg " id="editpark_pick"  onchange="editgetpickcategorydata(this)">
                   <option value="">Select</option>
                   <?php
                                foreach($poi_data as $park){
                                  $category=$this->bookings_model->getcategory('vbs_u_ride_category',$park->category_id);
                                 
                          if(strtolower($category)=="park"){
                                 ?>
                                   <option <?=  ($pickparkpoiid==$park->id)?"selected":""; ?> value="<?= $park->id ?>"><?= $park->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                 </select>
               </div>
                   <?php
                                 if(isset($pick_form['pick_park']['error']) && !empty($pick_form['pick_park']['error'])){     
                                      echo "<div class='invalid-error'>".$pick_form['pick_park']['error']."</div>";
                                        }
                                  ?>
                           <small style="margin-left: 0px !important;">Required</small>       
                </div>
              </div>
              </div>
               <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="editpickparkaddress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $pickparkpoirecord['address'] ?>">
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="editpickparkzipcode"  placeholder="Zip code" disabled style="background-color: #f6f6f6;" value="<?= $pickparkpoirecord['postal'] ?>">
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="editpickparkcity" placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $pickparkpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
          </div>
       </div>
      </div>
       
    </div>

    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 booking_tab_size booking_icon_tab"> <img src="<?= base_url()?>assets/theme/default/images/carloader2.gif" width="130" height="130"> </div>
    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 booking_tab_size " style="padding:0px;width: 46%;">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
          <div class="booking-tab">
             <input type="hidden" value="<?= $dbbookingdata->dropoffcategory ?>" id="editdropoffcategory" name="dropoffcategory">
            <ul class="nav nav-tabs">

             <li><button class="btn btn-default extra-pad" style="font-weight:900;pointer-events: none;padding-bottom: 11px;border-radius:0px;width:100%;background-image: linear-gradient(rgb(255, 255, 255) 0px, rgb(224, 224, 224) 100%) !important;color: #616161 !important;"><?php echo 'Dropoff'; ?></button></li>
               <li  <?= $dbbookingdata->dropoffcategory=="address" ? 'class="active"':''?> style="width:14.5%;"><a id="editdropaddresscategorybtn" data-toggle="tab" href="#editdrop_address"><i class="fa fa-map-marker"></i> <?php echo 'Address'; ?></a></li>
             <li  <?= $dbbookingdata->dropoffcategory=="package" ? 'class="active"':''?>  style="width:16%;"><a id="editdroppackagecategorybtn" data-toggle="tab" href="#editdrop_package"><i class="fa fa-suitcase"></i> <?php echo 'Package'; ?></a></li>

             <li  <?= $dbbookingdata->dropoffcategory=="airport" ? 'class="active"':''?> style="width:15.5%;"><a id="editdropairportcategorybtn" data-toggle="tab" href="#editdrop_airport"><i class="fa fa-plane"></i> <?php echo 'Airport'; ?></a></li>
             <li  <?= $dbbookingdata->dropoffcategory=="train" ? 'class="active"':''?>><a id="editdroptraincategorybtn" data-toggle="tab" href="#editdrop_train"><i class="fa fa-train"></i> <?php echo 'Train'; ?></a></li>
             <li  <?= $dbbookingdata->dropoffcategory=="hotel" ? 'class="active"':''?>><a id="editdrophotelcategorybtn" data-toggle="tab" href="#editdrop_hotel"><i class="fa fa-hotel"></i> <?php echo 'Hotel'; ?></a></li>
             <li  <?= $dbbookingdata->dropoffcategory=="park" ? 'class="active"':''?>><a id="editdropparkcategorybtn" data-toggle="tab" href="#editdrop_park"><i class="fa fa-tree"></i> <?php echo 'Park'; ?></a></li>
           </ul>
          </div>
      </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
     <div class="tab-content">
        <div id="editdrop_address" class="tab-pane fade  <?= $dbbookingdata->dropoffcategory=="address" ? 'in active':''?>">
              <input type="hidden" id="editdroploc_lat" />
               <input type="hidden" id="editdroploc_long" />
               <div class="row">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                   <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;margin:8px 0px 0px -5px;"></i>
                 </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                   <div class="form-group edit-cardform-control">
                      <div class="input-group" style="position: relative;">
                        <span class="input-group-addon">
                        <i class="fa fa-map-marker"></i>
                        </span>
                         <input type="hidden" name="dropaddressfield" placeholder="Addresse line"  class="pick_up_txtbox_bkg" id="editdropoffaddressfield" onFocus="geolocate()"  value="<?= $dbdropaddressdata->address; ?>">
                          <?php
                          $dropshortaddress='';
                           
                                 $dropshortaddress=$dbdropaddressdata->address;
                                
                                  $dropshortaddress= str_replace(($dbdropaddressdata->city), "", $dropshortaddress);
                                  
                             
                                
                                  $dropshortaddress= str_replace(($dbdropaddressdata->zipcode), "",  $dropshortaddress);
                                
                             
                                 $dropshortaddress=str_ireplace("france","",$dropshortaddress);
                                  $dropshortaddress=rtrim($dropshortaddress,' ');
                                  $dropshortaddress=rtrim($dropshortaddress,',');
                                 $dropshortaddress = preg_replace('!\s+!', ' ', $dropshortaddress);
                              

                           ?>
                              <input type="text"  placeholder="Addresse line"  class="pick_up_txtbox_bkg" id="editdropoffautocomplete" onFocus="geolocate()"  value="<?php echo $dropshortaddress ?>" style="padding-right: 30px;">

                        <div id="editdropcurrentuserpoibtn" onclick="editgetDropCurrentLocation()">
                           <img src="<?= base_url()?>assets/theme/default/images/target-a.svg">
                        </div>
                       <div id="editdropcurrentuserpoidiv">
                        <div class="form-group">
                        <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker" style="top: 4px;"></i>
                         <input type="text" readonly id="editdropcurrentuserpoiinput" onclick="editsetDropCurrentLocation()">
                          
                         
                          </div> 
                        </div>
                      </div>
                       <div id="editdroplocationloaderdiv">
                        <img src="<?= base_url()?>assets/theme/default/images/loader_2.gif" >
                      </div>
                      </div>
                       <?php
                         if(isset($drop_form['drop_address']['error']) && !empty($drop_form['drop_address']['error'])){     
                              echo "<div class='invalid-error'>".$drop_form['drop_address']['error']."</div>";
                                }
                          ?>
                          <small>Required</small>
                   </div>
                 </div>
               </div>

              <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                     <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                        <input type="text" name="dropotheraddressfield" placeholder="Complément d'adresse"  class="pick_up_txtbox_bkg" style="padding-left:43px !important;" value="<?= $dbdropaddressdata->otheraddress ?>">
                         <?php
                         if(isset($drop_form['drop_additional_address']['error']) && !empty($drop_form['drop_additional_address']['error'])){     
                              echo "<div class='invalid-error'>".$drop_form['drop_additional_address']['error']."</div>";
                                }
                          ?>
                      </div>
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small col-zip-code" style="padding: 0px 5px 0px 0px;">
                          <div class="form-group edit-cardform-control">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                               <input type="text" name="dropzipcodefield" placeholder="Zip code"  class="pick_up_txtbox_bkg" id="editdropoff_code" 
                               value="<?= $dbdropaddressdata->zipcode; ?>">

                            </div>
                             <?php
                         if(isset($drop_form['drop_zip_code']['error']) && !empty($drop_form['drop_zip_code']['error'])){     
                              echo "<div class='invalid-error'>".$drop_form['drop_zip_code']['error']."</div>";
                                }
                          ?>
                           <small>Required</small>
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small col-city" style="padding: 0px 0px 0px 5px;">
                              <div class="form-group edit-cardform-control">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text" name="dropcityfield" placeholder="City"  class="pick_up_txtbox_bkg" id="editdropoff_city"   value="<?=  $dbdropaddressdata->city ?>">
                                    </div>
                                        <?php
                                       if(isset($drop_form['drop_city']['error']) && !empty($drop_form['drop_city']['error'])){     
                                            echo "<div class='invalid-error'>".$drop_form['drop_city']['error']."</div>";
                                              }
                                        ?>
                                        <small>Required</small>
                               </div>
                          </div>
                       </div>
                   </div>
              </div>
       </div>
        <div id="editdrop_package" class="tab-pane fade  <?= $dbbookingdata->dropoffcategory=="package" ? 'in active':''?>">
            <div class="row">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                   <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-suitcase"></i>
                        </span>
                  <select name="droppackagefield" id="editdroppackagefield" class="packagebox_pick_bkg " onchange="editadddroppoidata()">
                       <option value="">Select</option>
                 
                       <?php
                        foreach($poi_package as $package){
                         ?>
                       <option <?=  ($dbbookingdata->droppackage_id==$package->id)?"selected":""; ?> value="<?= $package->id ?>"><?= $package->name ?></option>
                       <?php  }  ?>
                      </select>
                    </div>
                        <?php
                         if(isset($drop_form['drop_package']['error']) && !empty($drop_form['drop_package']['error'])){     
                              echo "<div class='invalid-error'>".$drop_form['drop_package']['error']."</div>";
                                }
                          ?>
                          <small>Required</small>
                </div>
              </div>
            </div>  
            
                  <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-bullseye"></i>
                              </span>
                             <input type="text"   id="editpackagedestinationcategoryname" value="<?= $poidropcategoryrecord['ride_cat_name']  ?>" disabled style="background-color: #f6f6f6;" >
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-bullseye"></i>
                                      </span>
                                       <input type="text"   id="editpackagedestinationname" value="<?= $droppoirecord['name']  ?>" disabled style="background-color: #f6f6f6;" >
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text" placeholder="Addresse"   id="editpackagedestinationaddress" value="<?= $droppoirecord['address']  ?>" disabled style="background-color: #f6f6f6;">
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
           
                   <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text" placeholder="Zip code"   id="editpackagedestinationzipcode" value="<?= $droppoirecord['postal']  ?>" disabled style="background-color: #f6f6f6;" >
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text" placeholder="City"  id="editpackagedestinationcity" value="<?= $droppoirecord['ville']  ?>" disabled style="background-color: #f6f6f6;" >
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
        </div>
        <div id="editdrop_airport" class="tab-pane fade  <?= $dbbookingdata->dropoffcategory=="airport" ? 'in active':''?>">
                   <div class="row">
                      <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                          <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                      </div>
                      <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                         <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-plane"></i>
                        </span>
                          <select name="dropairportfield" id="editairport_drop"  class="airlocation_pick_bkg " onchange="editgetdropcategorydata(this)">
                           <option value="">Select</option>
                            <?php
                                foreach($poi_data as $airport){
                                   $category=$this->bookings_model->getcategory('vbs_u_ride_category',$airport->category_id);
                                  if(strtolower($category)=="airport"){
                                 ?>
                                   <option <?= ($dropairportpoiid==$airport->id)?"selected":""; ?> value="<?= $airport->id ?>"><?= $airport->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                          </select>
                        </div>
                           <?php
                         if(isset($drop_form['drop_airport']['error']) && !empty($drop_form['drop_airport']['error'])){     
                              echo "<div class='invalid-error'>".$drop_form['drop_airport']['error']."</div>";
                                }
                          ?>
                          <small>Required</small>
                    </div>
                  </div>
                  </div>
                  <div class="row" style="margin-top:5px;">
                      <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2"></div>
                        <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small edit-cardform-control" style="padding: 0px;padding-right:5px;">
                          <input type="text" name="dropairportflightnumberfield" placeholder="Flight No" class="pickup_flight_no_bkg " id="editdropof_flight_no" value="<?= $dbdropairportdata->flightnumber; ?>">
                           <?php
                         if(isset($drop_form['drop_airport_number']['error']) && !empty($drop_form['drop_airport_number']['error'])){     
                              echo "<div class='invalid-error'>".$drop_form['drop_airport_number']['error']."</div>";
                                }
                          ?>
                          <small style="margin-left: 0px !important;">Required</small>
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small col-arrival" style="padding: 0px;padding-left:5px;">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding:0px;padding-top: 9px;">
                          <label for="timepicker_drop_fly_1">Arrival Time</label>
                        </div>
                         <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                         <input  class="pickup_flight_time_bkg" id="edittimepicker_drop_fly_1" name="dropairportarrivaltimefield" type="text" value="<?= $dropairportarrivaltime ?>" />
                          <?php
                         if(isset($drop_form['drop_airport_arrival']['error']) && !empty($drop_form['drop_airport_arrival']['error'])){     
                              echo "<div class='invalid-error'>".$drop_form['drop_airport_arrival']['error']."</div>";
                                }
                          ?>
                       </div>
                          </div>
                     </div>
                    </div>
                  </div>

                   <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="editdropairportaddress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $dropairportpoirecord['address'] ?>" >
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="editdropairportzipcode" placeholder="Zip code"  disabled style="background-color: #f6f6f6;"  value="<?= $dropairportpoirecord['postal'] ?>">
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="editdropairportcity" placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $dropairportpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>




              
        </div>
        <div id="editdrop_train" class="tab-pane fade  <?=$dbbookingdata->dropoffcategory=="train" ? 'in active':''?>">
                     <div class="row">
                        <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                            <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                        </div>
                        <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                           <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-train"></i>
                        </span>
                            <select name="droptrainfield" id="edittrain_drop" class="trainlocation_pick_bkg" onchange="editgetdropcategorydata(this)">
                              <option value="">Select</option>
                                <?php
                                foreach($poi_data as $train){
                                   $category=$this->bookings_model->getcategory('vbs_u_ride_category',$train->category_id);
                                  if(strtolower($category)=="train"){
                                 ?>
                          <option  <?= ($droptrainpoiid==$train->id)?"selected":""; ?> value="<?= $train->id ?>"><?= $train->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                          </select>
                        </div>
                           <?php
                         if(isset($drop_form['drop_train']['error']) && !empty($drop_form['drop_train']['error'])){     
                              echo "<div class='invalid-error'>".$drop_form['drop_train']['error']."</div>";
                                }
                          ?>
                          <small>Required</small>
                      </div>
                    </div>
                    </div>
                    <div class="row" style="margin-top:5px;">
                       <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2"></div>
                      <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">                      
                        <div class="ccol-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small edit-cardform-control" style="padding: 0px;padding-right:5px;">
                            <input  class="pickup_train_no_bkg" type="text"  name="droptrainnumberfield" placeholder="Train No" id="editdropoff_train_no"  value="<?= $dbdroptraindata->trainnumber ?>">
                              <?php
                         if(isset($drop_form['drop_train_number']['error']) && !empty($drop_form['drop_train_number']['error'])){     
                              echo "<div class='invalid-error'>".$drop_form['drop_train_number']['error']."</div>";
                                }
                          ?>
                          <small style="margin-left: 0px !important;">Required</small>
                        </div>
                        <div class="ccol-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small col-arrival" style="padding: 0px;padding-left:5px;">
                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding:0px;padding-top: 9px;">
                              <label for="pickup_train_time_bkg">Arrival Time</label>
                            </div>
                          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                            <input style="width:100% !important;" class="pickup_train_time_bkg" id="edittimepicker_drop_stn" name="droptrainarrivaltimefield" type="text" value="<?= $droptrainarrivaltime ?>" />
                              <?php
                         if(isset($drop_form['drop_train_arrival']['error']) && !empty($drop_form['drop_train_arrival']['error'])){     
                              echo "<div class='invalid-error'>".$drop_form['drop_train_arrival']['error']."</div>";
                                }
                          ?>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>

                     <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="editdroptrainaddress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $droptrainpoirecord['address'] ?>">
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="editdroptrainzipcode" placeholder="Zip code"  disabled style="background-color: #f6f6f6;" value="<?= $droptrainpoirecord['postal'] ?>" >
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="editdroptraincity" placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $droptrainpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
        </div>
        <div id="editdrop_hotel" class="tab-pane fade  <?= $dbbookingdata->dropoffcategory=="hotel" ? 'in active':''?>">
         <div class="row">
            <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
            </div>
            <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
               <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-hotel"></i>
                        </span>
                <select name="drophotelfield" class="hotellocation_pick_bkg" id="edithotel_drop" onchange="editgetdropcategorydata(this)">
                    <option value="">Select</option>
                    <?php
                                foreach($poi_data as $hotel){
                                  $category=$this->bookings_model->getcategory('vbs_u_ride_category',$hotel->category_id);
                                  if(strtolower($category)=="hotel"){
                                 ?>
                                   <option <?=  ($drophotelpoiid==$hotel->id)?"selected":""; ?> value="<?= $hotel->id ?>"><?= $hotel->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                </select>
              </div>
                   <?php
                         if(isset($drop_form['drop_hotel']['error']) && !empty($drop_form['drop_hotel']['error'])){     
                              echo "<div class='invalid-error'>".$drop_form['drop_hotel']['error']."</div>";
                                }
                          ?>
                          <small>Required</small>
            </div>
          </div>
         </div>
           <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="editdrophoteladdress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $drophotelpoirecord['address'] ?>">
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="editdrophotelzipcode" placeholder="Zip code"  disabled style="background-color: #f6f6f6;" value="<?= $drophotelpoirecord['postal'] ?>">
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="editdrophotelcity" placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $drophotelpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
        </div>
        <div id="editdrop_park" class="tab-pane fade  <?= $dbbookingdata->dropoffcategory=="park" ? 'in active':''?>">
             <div class="row">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                    <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                   <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-tree"></i>
                        </span>
                    <select name="dropparkfield" class="parklocation_pick_bkg" id="editpark_drop"  onchange="editgetdropcategorydata(this)">
                 <option value="">Select</option>
                   <?php
                                foreach($poi_data as $park){
                                   $category=$this->bookings_model->getcategory('vbs_u_ride_category',$park->category_id);
                                  if(strtolower($category)=="park"){
                                 ?>
                                   <option <?=  ($dropparkpoiid==$park->id)?"selected":""; ?> value="<?= $park->id ?>"><?= $park->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                 </select>
               </div>
                  <?php
                         if(isset($drop_form['drop_park']['error']) && !empty($drop_form['drop_park']['error'])){     
                              echo "<div class='invalid-error'>".$drop_form['drop_park']['error']."</div>";
                                }
                          ?>
                          <small>Required</small>
                </div>
              </div>
            </div>
             <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="editdropparkaddress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $dropparkpoirecord['address'] ?>">
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="editdropparkzipcode" placeholder="Zip code"  disabled style="background-color: #f6f6f6;" value="<?= $dropparkpoirecord['postal'] ?>">
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="editdropparkcity" placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $dropparkpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>

        </div>



    </div>
  </div>
 
</div>

   
  

  
    </div>
  
 

    <div class="date-time" style="margin-top: 10px;margin-bottom:10px;">
     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="width: 46%;padding-right:0px;">
     <div class="col-lg-12 col-md-12 col-sm-12" id="editstopaddressdiv" style="padding:0px;margin-top: 10px;">
      <input type="hidden" id="stopcountfield" value="<?= $stopcount; ?>">
      <?php

      $n=1;
       foreach ($dbstopsdata as $stop):
        ?>
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 stopinnerdiv" style="padding:0px;">
        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12" style="padding:0px;">
          <div class="form-group"><div class="input-group" style="position: relative;">
            <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
            <input type="text" name="stopaddress[]" placeholder="Addresse line"  class="pick_up_txtbox_bkg" id="editstopaddr_<?= $n ?>"   onFocus="geolocate()" value="<?= $stop->stopaddress; ?>"  style="padding-right: 30px;height: 30px !important;border-radius: 0px">
          </div> 
          <div class="currentstoplocationbtn" onclick="editgetStopCurrentLocation(<?= $n ?>)"><img src="<?= base_url()?>assets/theme/default/images/target-a.svg"></div><div class="currentstoplocationdiv" id="editstopercurrentlocationdiv_<?= $n ?>"><div class="form-group"><div class="inner-addon left-addon"><i class="glyphicon glyphicon-map-marker" style="top: -2px;"></i><input type="text" readonly id="editstopercurrentlocationfield_<?= $n ?>" class="currentstoppoiinput" onclick="editsetStopCurrentLocation(<?= $n ?>)" style="height:30px !important;"></div></div></div><div class="currentstoplocationloader" id="editstoperlocationloader_<?= $n ?>"><img src="<?= base_url()?>assets/theme/default/images/loader_2.gif" ></div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding-right:0px;">
        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12" style="padding-right:0px;text-align:right;">
          <label class="pickup_time pickup_time_label" style="margin:0px !important;padding-top: 9px !important;font-size:13px !important;"><?php echo 'Waiting Time'; ?></label>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12" style="padding-right:0px;text-align:right;padding-left: 5px;">
          <input class="stopwaitingtimepicker" id="editstopwaitingtimepicker_<?= $n ?>"  name="stopwaittime[]" type="text" value="<?= $stop->stopwaittime; ?>" style="width:100%;height: 30px !important;border-radius: 0px" />
        </div>
      </div>
      <div class="col-lg-1 col-md-1 col-sm-1" style="position:relative;"><button type="button"  onclick="editremovestopboxes(this,<?= $n ?>);" class="minusstopicon"><i class="fa fa-minus" style="margin:0px;"></i></button>
      </div>
    </div>
  <?php
   $n++;
   endforeach;
    ?>
     </div>
  
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;" >
         <div id="editreturn_chairwheel" >
                    <div  style="float:left; margin-right:20px;">
                      <div class="squaredFour">
                        <span>Reccurent </span><input class="edit_regular_check" id="editregularcheckfieldid" type="checkbox" name="regularcheckfield" <?= ($dbbookingdata->regular=='1')?"checked":""; ?> value="1" onchange="edit_regular_check();" style="margin:0px 10px;float:none;">
                          <label for="editregularcheckfieldid"></label>
                      </div>
                    </div>

                    <div style="float:left; margin-right:20px;">
                         <div class="squaredFour">
                        <span>Return </span><input class="edit_return_car"  type="checkbox" id="editreturncheckfieldid" name="returncheckfield" value="1" 
                        <?= ($dbbookingdata->returncheck=='1')?"checked":""; ?> onchange="edit_return_car();" style="margin:0px 10px;float:none;">
                           <label for="editreturncheckfieldid"></label>
                      </div>
                    </div>
                     <div style="float:left; margin-right:20px;">
                         <div class="squaredFour">
                        <span>Wheelchair </span><input class="edit_wheelchair_carssh" type="checkbox" id="editwheelcarcheckfieldid" name="wheelcarcheckfield" value="1" <?= ($dbbookingdata->wheelchair=='1')?"checked":""; ?> onchange="edit_wheelchair_carssh();" style="margin:0px 10px;float:none;">
                           <label for="editwheelcarcheckfieldid"></label>
                      </div>
                    </div>
                      <div style="float:right;">
                        
                        <span>Add Extra Stop </span> <button type="button" onclick="editaddstopboxes();" class="plusgreeniconconfig" style="margin: 0px 0px 0px 10px!important;"><i class="fa fa-plus" style="margin:0px !important;"></i></button>
                           
                     
                    </div>
                    
                    <div class="clearfix"></div>
             </div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px;padding-top:20px;">
        <div class="pickupreturn_datetime_div">
              <div id="editpickup_datetime_div" >
                <div class="pickdt-bkg" style="text-align:center;"> 
                    <label class="pickup_date pickup_dt_label" style="margin:0px !important;font-size:13px !important;"> <?php echo 'Pickup Date'; ?> </label>
                    
                    <input id="editdtpicker" class="pickup-dt-bkg bdatepicker"  type="text" value="<?= $pick_date; ?>" name="pickdatefield" placeholder="<?php echo 'Pickup Date'; ?>"  />
                </div>
                <div class="picktime-bkg" style="text-align:center;"> 
                   <label class="pickup_time pickup_time_label" style="margin:0px !important;font-size:13px !important;"><?php echo 'Pickup Time'; ?></label>
                    
                    <input   id="edittimepicker1" name="picktimefield" type="text" value="<?= $pick_time; ?>" style="width:100%" />
                </div>
              </div>
          
              <div class="return_datetime_div" style="display: none;">

                <div class="picktime-bkg" style="text-align:center;"> 
                   <label class="pickup_time pickup_time_label" style="margin:0px !important;"><?php echo 'Waiting Time'; ?></label>
                  
                    
                    <input id="editwaitingtimepicker" name="waitingtimefield" type="text" value="<?= $waiting_time; ?>" style="width:100%" />
                </div>

                <div class="dropdt-bkg" style="text-align:center;">
                     <label style="margin:0px !important;" class="dropof_date dropof_dt_label"> <?php echo 'Return Date'; ?> </label>
                    

                   
                    <input id="editdtpicker1" class="bdatepicker"  type="text" value="<?= $return_date; ?>" name="returndatefield" placeholder="<?php echo 'Drop Date'; ?>"  />
                </div>

                <div class="droptime-bkg" style="text-align:center;">
                    <label class="dropof_time dropof_time_label" style="margin:0px !important;"><?php echo 'Return Time'; ?></label>
                   
            
                    <input   id="edittimepicker2" name="returntimefield" type="text" value="<?= $return_time; ?>" style="width:100%;margin-top: 0px !important;" />
                </div>
              </div> 
        </div>
        
          <div class="startend_datetime_div" style="display: none;">

             <div class="pickdt-bkg" style="text-align:center;"> 
                    <label class="pickup_date start_dt_label" style="margin:0px !important;"> <?php echo 'Start Date'; ?> </label>
                 
                  <input  class="pickup-dt-bkg bdatepicker"  type="text" value="<?= $start_date; ?>" name="startdatefield" placeholder="<?php echo 'Start Date'; ?>"  />
              </div>
              <div class="picktime-bkg" style="text-align:center;"> 
               
                  <label class="pickup_time start_time_label" style="margin:0px !important;"><?php echo 'Start Time'; ?></label>
                  
                  <input   id="editstarttimepicker" name="starttimefield" type="text" value="<?= $start_time; ?>" style="width:100%" />
              </div>

              <div class="dropdt-bkg" style="text-align:center;">       
                  <label style="margin:0px !important;" class="dropof_date endof_dt_label"> <?php echo 'End Date'; ?> </label>
           
                  <input class="bdatepicker"  type="text" value="<?= $end_date; ?>" name="enddatefield" placeholder="<?php echo 'End Date'; ?>"  />
              </div>

              <div class="droptime-bkg" style="text-align:center;">
      
                  <label class="dropof_time endof_time_label" style="margin:0px !important;"><?php echo 'End Time'; ?></label>
          
                  <input   id="editendtimepicker" name="endtimefield" type="text" value="<?= $end_time; ?>" style="width:100%;margin-top: 0px !important;" />
              </div>
          </div>
      </div>
    
     </div> 
    
    </div>
  <div  id="edittimeslotsrtn" style="display: none;" >
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  
      <table cellpadding="0" cellspacing="0" 
      style="width:100%;" align="center" id="edittimesspantable">
        <tr style="text-align:left;" id="editreturnpicktimetable">
          <td>&nbsp;</td>
          <td><?php echo 'Monday'; ?></td>
          <td><?php echo 'Tuesday'; ?></td>
          <td><?php echo 'Wednesday'; ?></td>
          <td><?php echo 'Thursday'; ?></td>
          <td><?php echo 'Friday'; ?></td>
          <td><?php echo 'Saturday'; ?></td>
          <td><?php echo 'Sunday'; ?></td>
        </tr>
        <tr class="go-table" style="text-align:left;">
          <td><?php echo 'Pickup Time'; ?></td>
          <td class="weekdays_none_floating">
            <input name="pickmondaycheckfield" class="edit_weekdays_go" id='pickdaycheck_1' type="checkbox" <?= ($ispickmonday=='1')?"checked":""; ?> value="1" onclick="edit_weekdays_go(this);">
            <input  class="wkdays_time_pick " id="editgo_time_1" name="pickmondayfield" type="text"  value="<?= $pickmondaytime ?>" style="float:none !important;" />
          </td>
          <td class="weekdays_none_floating"><input name="picktuesdaycheckfield" class="edit_weekdays_go" id='pickdaycheck_2' type="checkbox" <?= ($ispicktuesday=='1')?"checked":""; ?> value="2"  onclick="edit_weekdays_go(this);">
            <input  class="wkdays_time_pick " id="editgo_time_2" name="picktuesdayfield" type="text" value="<?= $picktuesdaytime ?>" style="float:none !important;"/>
          </td>
          <td class="weekdays_none_floating"><input name="pickwednesdaycheckfield" class="edit_weekdays_go" id='pickdaycheck_3' type="checkbox" <?= ($ispickwednesday=='1')?"checked":""; ?> value="3" onclick="edit_weekdays_go(this);">
            <input  class="wkdays_time_pick " id="editgo_time_3" name="pickwednesdayfield" type="text" value="<?= $pickwednesdaytime ?>" style="float:none !important;" />
          </td class="weekdays_none_floating">
          <td class="weekdays_none_floating"><input name="pickthursdaycheckfield" class="edit_weekdays_go" id='pickdaycheck_4' type="checkbox" <?= ($ispickthursday=='1')?"checked":""; ?> value="4" onclick="edit_weekdays_go(this);">
            <input  class="wkdays_time_pick " id="editgo_time_4" name="pickthursdayfield" type="text" value="<?= $pickthursdaytime ?>"style="float:none !important;" />
          </td>
          <td class="weekdays_none_floating"><input name="pickfridaycheckfield" class="edit_weekdays_go" id='pickdaycheck_5' type="checkbox" <?= ($ispickfriday=='1')?"checked":""; ?> value="5" onclick="edit_weekdays_go(this);">
            <input  class="wkdays_time_pick " id="editgo_time_5" name="pickfridayfield" type="text" value="<?= $pickfridaytime ?>" style="float:none !important;" />
          </td>
          <td class="weekdays_none_floating"><input name="picksaturdaycheckfield" class="edit_weekdays_go" id='pickdaycheck_6' type="checkbox" <?= ($ispicksaturday=='1')?"checked":""; ?> value="6" onclick="edit_weekdays_go(this);">
            <input  class="wkdays_time_pick " id="editgo_time_6" name="picksaturdayfield" type="text" value="<?= $picksaturdaytime ?>"style="float:none !important;" />
          </td>
          <td class="weekdays_none_floating"><input name="picksundaycheckfield" class="edit_weekdays_go" id='pickdaycheck_7' type="checkbox" <?= ($ispicksunday=='1')?"checked":""; ?> value="7" onclick="edit_weekdays_go(this);">
            <input  class="wkdays_time_pick " id="editgo_time_7" name="picksundayfield" type="text" value="<?= $picksundaytime ?>"style="float:none !important;" />
          </td>
        </tr>
        <tr class="back-table" style="text-align:left;">
          <td style="width:10%;text-align: left;"><?php echo 'Return Time'; ?></td>
          <td class="weekdays_none_floating">
            <input name="returnmondaycheckfield" class="edit_weekdays_back" id='returndaycheck_1' type="checkbox" <?= ($isreturnmonday=='1')?"checked":""; ?> value="1" onclick="edit_weekdays_back(this);">
            <input  class="wkdays_time_pick " id="editback_time_1" name="returnmondayfield" type="text" value="<?= $returnmondaytime ?>" style="float: none !important; "/>
          </td>
          <td class="weekdays_none_floating">
            <input name="returntuesdaycheckfield" class="edit_weekdays_back" id='returndaycheck_2' type="checkbox" <?= ($isreturntuesday=='1')?"checked":""; ?> value="2" onclick="edit_weekdays_back(this);">
            <input  class="wkdays_time_pick " id="editback_time_2" name="returntuesdayfield" type="text" value="<?= $returntuesdaytime ?>" style="float:none !important;" />
          </td>
          <td class="weekdays_none_floating">
            <input name="returnwednesdaycheckfield" class="edit_weekdays_back" id='returndaycheck_3' type="checkbox" <?= ($isreturnwednesday=='1')?"checked":""; ?> value="3" onclick="edit_weekdays_back(this);">
            <input  class="wkdays_time_pick " id="editback_time_3" name="returnwednesdayfield" type="text" value="<?= $returnwednesdaytime ?>" style="float:none !important;"/>
          </td>
          <td class="weekdays_none_floating">
            <input name="returnthursdaycheckfield" class="edit_weekdays_back" id='returndaycheck_4' type="checkbox" <?= ($isreturnthursday=='1')?"checked":""; ?> value="4" onclick="edit_weekdays_back(this);">
            <input  class="wkdays_time_pick " id="editback_time_4" name="returnthursdayfield" type="text" value="<?= $returnthursdaytime ?>" style="float:none !important;"/>
          </td>
          <td class="weekdays_none_floating">
            <input name="returnfridaycheckfield" class="edit_weekdays_back" id='returndaycheck_5' type="checkbox" <?= ($isreturnfriday=='1')?"checked":""; ?> value="5" onclick="edit_weekdays_back(this);">
            <input  class="wkdays_time_pick " id="editback_time_5" name="returnfridayfield" type="text" value="<?= $returnfridaytime ?>" style="float:none !important;"/>
          </td>
          <td class="weekdays_none_floating">
            <input name="returnsaturdaycheckfield" class="edit_weekdays_back" id='returndaycheck_6' type="checkbox" <?= ($isreturnsaturday=='1')?"checked":""; ?> value="6" onclick="edit_weekdays_back(this);">
            <input  class="wkdays_time_pick " id="editback_time_6" name="returnsaturdayfield" type="text" value="<?= $returnsaturdaytime ?>" style="float:none !important;"/>
          </td>
          <td class="weekdays_none_floating">
            <input name="returnsundaycheckfield" class="edit_weekdays_back" id='returndaycheck_7' type="checkbox" <?= ($isreturnsunday=='1')?"checked":""; ?>  value="7" onclick="edit_weekdays_back(this);">
            <input  class="wkdays_time_pick " id="editback_time_7" name="returnsundayfield" type="text" value="<?= $returnsundaytime ?>" style="float:none !important;"/>
          </td>
        </tr>
      </table>
    </div>
  </div>
  
     <?php  include 'editwheelchairdiv.php';?> 
              
<div class="col-md-12">
<!-- Reminder Notification Custom Reminder  Section -->
     <div class="col-md-6 pdz" >

           <!--REminder and Notification -->
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pdz">
          <div class="col-md-12 pdz" style="font-weight: 900;">Notification and Reminders : </div>
            <div class="col-md-12 pdz"> 
               <div class="col-md-2 pdz text-center" style="font-weight: 900;"></div>
               <div class="col-md-2 pdz text-center" style="font-weight: 900;">Date</div>
               <div class="col-md-2 pdz text-center" style="font-weight: 900;">Time</div>
               <div class="col-md-2 pdz text-center" style="font-weight: 900;">File</div>
               <div class="col-md-2 pdz text-center" style="font-weight: 900;">Statut</div>
               <div class="col-md-2 pdz text-center" style="font-weight: 900;">Resend</div>
            </div>
        <!-- Notification -->
        <div class="col-md-12 pdz" id="notificationmaindiv">
          <div class="col-md-12 pdz">
            <div class="col-md-2 pdz" style="font-weight: 900;">Notification : </div>
            <div class="col-md-2 pdz">                             
                       <input <?php if($invoices->status !='0'){echo "style='pointer-events: none;background-color: #def4faab;'";} ?>  class="pickup-dt-bkg hasDatepicker" id="notificationdatefield"  type="text" value="<?= date('d/m/Y'); ?>" name="notificationdatefield" placeholder="<?php echo 'Notification Date'; ?>" autocomplete="off" />     
            </div>
            <div class="col-md-2" >

                       <input <?php if($invoices->status !='0'){echo "style='pointer-events: none;background-color: #def4faab;'";} ?>   id="notificationtimepicker"  type="text" value="<?= date('H : i'); ?>" name="notificationtimefield" placeholder="<?php echo 'Notification Time'; ?>"  style="width:100%" />
                   
            </div>
            <div class="col-md-2 pdz text-center" >
             
            </div>
             <div class="col-md-2 pdz text-center" id="notificationsentstatus"></div>
            <div class="col-md-2 pdz text-center" >
            <div id="notificationsentloaderbtn" style="position: relative;display: none;">
              <img src="<?= base_url()?>assets/theme/default/images/bookloader.gif" style="width: 28px;height: 28px;position: absolute;left: -16px;top: 0px;">
            </div>
              <button type="button" class="btn btn-default" id="notificationsentbtn" <?php if($invoices->status !='0'){echo "style='pointer-events: none;'";} ?> style="height: 30px;line-height: 1px;" onclick="addinvoicenotification(<?= $dbbookingdata->id; ?>)">Send</button>
            </div>      
          </div>
           <?php foreach ($invoicenotifications as $notification): ?>
            <div class="col-md-12 pdz">
                  <div class="col-md-2 pdz text-center"></div>            
                 <div class="col-md-2 pdz text-center" ><?= from_unix_date($notification->date); ?></div>
                 <div class="col-md-2 pdz text-center" ><?= $notification->time ?></div>
                  <div class="col-md-2 pdz text-center" >
                             <a href="<?= base_url(); ?>admin/invoices/notification_pdf/<?= $notification->id  ?>/<?= $dbbookingdata->id ?>" target="_blank">
                               <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                             </a>
                 </div>
                 <div class="col-md-2 pdz text-center" ><?= ($notification->issent == '1')?"Sent":"Pending"; ?></div>
                 <div class="col-md-2 pdz text-center" ></div>
             
            </div>    
             <?php endforeach; ?>
      </div>
       <!-- Notification -->
       <!-- Reminders -->
          <?php if($reminders): ?>
            <?php 
          
            $remindercount=0;
            $remindertype=1;
          foreach ($reminders as $reminder):
            if($remindercount < 3):
           

            ?>
          <div class="col-md-12 pdz" id="remindermaindiv_<?= $remindertype ?>">  
           <div class="col-md-12 pdz" style="margin-top: 5px;">
            <div class="col-md-2 pdz" style="font-weight: 900;">Reminder <?= $remindertype ?> : </div>
            <div class="col-md-2 pdz">
                    
                       <input  class="pickup-dt-bkg hasDatepicker" <?php if($invoices->status !='0'){echo "style='pointer-events: none;background-color: #def4faab;'";} ?> id="reminderdatefield_<?= $remindertype ?>"  type="text" value="<?= date('d/m/Y'); ?>" name="reminderdatefield" placeholder="<?php echo 'Reminder Date'; ?>"  />
                  
            </div>
            <div class="col-md-2">
                     
                       <input   id="remindertimepicker_<?= $remindertype ?>" <?php if($invoices->status !='0'){echo "style='pointer-events: none;background-color: #def4faab;'";} ?>  type="text" value="<?= date('H : i'); ?>" name="remindertimefield" placeholder="<?php echo 'Reminder Time'; ?>"  style="width:100%" />
            </div>
             <div class="col-md-2 pdz text-center">
              
             </div>
             <div class="col-md-2 pdz text-center" id="remindersentstatus_<?= $remindertype ?>"></div>
            <div class="col-md-2 pdz text-center" >
              <div id="remindersentloaderbtn_<?= $remindertype ?>" style="position: relative;display: none;">
              <img src="<?= base_url()?>assets/theme/default/images/bookloader.gif" style="width: 28px;height: 28px;position: absolute;left: -16px;top: 0px;">
            </div>
              <button type="button" id="remindersentbtn_<?= $remindertype ?>" <?php if($invoices->status !='0'){echo "style='pointer-events: none;'";} ?> class="btn btn-default" style="height: 30px;line-height: 1px;" onclick="addinvoicereminder(<?= $dbbookingdata->id; ?>,<?= $reminder->id ?>,<?= $remindertype ?>)">Send</button>
            </div>      
          </div>
          <?php if($remindertype == 1): ?>
          <?php foreach ($invoicefirstreminders as $freminder): ?>
            <div class="col-md-12 pdz">
                  <div class="col-md-2 pdz text-center"></div>            
                 <div class="col-md-2 pdz text-center" ><?= from_unix_date($freminder->date); ?></div>
                 <div class="col-md-2 pdz text-center" ><?= $freminder->time ?></div>
                  <div class="col-md-2 pdz text-center" >
                             <a href="<?= base_url(); ?>admin/invoices/reminder_pdf/<?= $freminder->id ?>/<?= $remindertype ?>/<?= $dbbookingdata->id  ?>" target="_blank">
                               <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                             </a>
                 </div>
                 <div class="col-md-2 pdz text-center" ><?= ($freminder->issent == '1')?"Sent":"Pending"; ?></div>
                 <div class="col-md-2 pdz text-center" ></div>
             
            </div>  
              <?php endforeach; ?>
           <?php elseif($remindertype == 2): ?>
          <?php foreach ($invoicesecondreminders as $sceminder): ?>
            <div class="col-md-12 pdz">
                  <div class="col-md-2 pdz text-center"></div>            
                 <div class="col-md-2 pdz text-center" ><?= from_unix_date($sceminder->date); ?></div>
                 <div class="col-md-2 pdz text-center" ><?= $sceminder->time ?></div>
                  <div class="col-md-2 pdz text-center" >
                             <a href="<?= base_url(); ?>admin/invoices/reminder_pdf/<?= $sceminder->id ?>/<?= $remindertype ?>/<?= $dbbookingdata->id  ?>" target="_blank">
                               <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                             </a>
                 </div>
                 <div class="col-md-2 pdz text-center" ><?= ($sceminder->issent == '1')?"Sent":"Pending"; ?></div>
                 <div class="col-md-2 pdz text-center" ></div>
             
            </div>  
           <?php endforeach; ?>
             <?php elseif($remindertype == 3): ?>
          <?php foreach ($invoicethirdreminders as $theminder): ?>
            <div class="col-md-12 pdz">
                  <div class="col-md-2 pdz text-center"></div>            
                 <div class="col-md-2 pdz text-center" ><?= from_unix_date($theminder->date); ?></div>
                 <div class="col-md-2 pdz text-center" ><?= $theminder->time ?></div>
                  <div class="col-md-2 pdz text-center" >
                             <a href="<?= base_url(); ?>admin/invoices/reminder_pdf/<?= $theminder->id ?>/<?= $remindertype ?>/<?= $dbbookingdata->id  ?>" target="_blank">
                               <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                             </a>
                 </div>
                 <div class="col-md-2 pdz text-center" ><?= ($theminder->issent == '1')?"Sent":"Pending"; ?></div>
                 <div class="col-md-2 pdz text-center" ></div>
             
            </div>    
             <?php endforeach; ?>
           <?php endif; ?>
        </div>
      <?php 
        $remindertype++;
        $remindercount++;
         endif;
         endforeach; 
        ?>
        

         <?php 
          endif; 
          ?> 
        <!-- Reminder -->
        </div>
          <!--Reminder and Notification -->
          <!-- Custom Reminders -->
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pdz">
               <div class="col-md-12 pdz" style="font-weight: 900;">Custom Reminders : </div>
               <div class="col-md-12 pdz" id="customreminderrecorded">
                 <div class="col-md-12 pdz">
                  <div class="col-md-3 pdz" style="text-align: center;font-weight: 900;">Cananl</div>
                  <div class="col-md-2 pdz" style="text-align: center;font-weight: 900;">Date</div>
                  <div class="col-md-2 pdz" style="text-align: center;font-weight: 900;">Time</div>
                  <div class="col-md-3 pdz" style="text-align: center;font-weight: 900;">Comment</div>
                </div>
                <?php  foreach ($invoicecustomreminders as $custom): 
                       $date = strtotime($custom->date);
                       $date = date('d/m/Y',$date);
                       $date= $date;
                    ?>

                    <div class="customreminderbox col-md-12 pdz">
                      <div class="col-md-3 text-center pdz"><?= $custom->title  ?></div>
                      <div class="col-md-2 text-center pdz"><?= $custom->date  ?></div>
                      <div class="col-md-2 text-center pdz"><?= $custom->time  ?></div>
                      <div class="col-md-3 text-center pdz" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;"><?= $custom->comment  ?></div>
                      <div class="col-md-2" style="padding-left: 52px;padding-top: 7px;">
                        <button type="button" onclick="removecustomreminderbox(this,<?= $custom->id ?>);" class="minusrediconcustomreminder"><i class="fa fa-minus" style="margin:0px !important;"></i></button></div>
                    </div>
              <?php endforeach; ?>
               </div>
               <input type="hidden" id="customremindertotalcount" value="1">
               <div class="maincustomreminderbox col-md-12 pdz">
                <div class="customreminderbox col-md-12 pdz">
                  <div class="col-md-12 pdz">
                   <div class="col-md-10 pdz" style="margin-top: 5px;">
                          <div class="col-md-3" style="padding-left: 0px;">
                            
                            <div class="form-group">
                              
                                     <select name="customremindertitlefield" id="customremindertitlefield" style="height: 30px !important;">
                                           <option value="">Select Canal</option>
                                           <option value="Email">Email</option>
                                           <option value="Phone">Phone</option>
                                           <option value="Letter">Letter</option>
                                           
                                         
                                     </select>
                                          
                                                                             
                                </div>
                          </div>


                          <div class="col-md-3 pdz">                             
                            <input  class="pickup-dt-bkg hasDatepicker" id="customreminderdatefield"  type="text" value="<?= date('d/m/Y'); ?>" name="customreminderdatefield[]" placeholder="<?php echo 'Custom Date'; ?>" autocomplete="off" />     
                          </div>
                           <div class="col-md-3" >
                            <input   id="customremindertimepicker_1"  type="text" value="<?= date('h : i'); ?>" name="customremindertimefield[]" placeholder="<?php echo 'Custom Time'; ?>"  style="width:100%" />       
                          </div> 
                   </div>
                   <div class="col-md-2" style="padding-left: 52px;padding-top: 7px;">
                      <button type="button" onclick="addcustomreminderbox();" class="plusgreeniconconfig"><i class="fa fa-plus" style="margin:0px !important;"></i></button>
                   </div>
                 </div>
                 <div class="col-md-12 pdz" style="margin-top:5px;">
                   <div class="col-md-10" style="padding-left:0px;">
                     <textarea rows="3" placeholder="write a comment" id="customremindercommentfield" name="customremindercommentfield"></textarea>
                   </div>
                 </div>
               </div>
               </div>
          </div>
          <!-- Custom Reminders -->
     </div>
     <!-- Reminder Notification Custom Reminder  Section -->
<div class="col-md-6 pdz">
   <!-- Customer Comment -->
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pdz">
        <div class="form-group">
            <label style="font-weight: 900;margin-bottom: 10px;">Comment : (by customer)</label>
            <textarea class="form-control" rows="4" name="booking_comment" id="editcommentbox" placeholder="Write a comment here..."><?= $dbbookingdata->booking_comment; ?></textarea>
    
        </div>
       </div>
    <!-- Customer Comment -->
     <!-- Reference -->
    <div class="col-md-12  booking_serivce_inner_tab pdz" style="margin-bottom: 10px;">
      <div class="form-group">
       <span>Reference </span>
             <input type="text" name="reference" placeholder="Reference" value="<?= $invoices->reference ?>" style="height: 36px !important;" >
                  
        </div>
    </div> 
    <!-- Reference -->
    <!-- Admin Comment -->
    <div class="col-lg-12 col-md-12 col-sm-12" style="padding: 0px;margin-bottom: 5px;">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
          <div class="form-group">
            <label style="font-weight: 900;margin-bottom: 10px;">Admin Note : </label>
             <textarea class="form-control" rows="4" name="booking_adminnote"  placeholder="Write a note here..."><?= $dbbookingdata->booking_admininvoicenote; ?></textarea>
         </div>
       </div>
      </div>
    <!-- Admin Comment -->
  
 
  
   
 </div> 
   
</div> 

         
      
    
 </div>
</div>
<?php  include 'edit_booking_quote.php';?> 