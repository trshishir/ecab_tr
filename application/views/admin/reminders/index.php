<?php $locale_info = localeconv(); ?>
<style>
    .table-filter .dpo {
        max-width: 62px;
    }
    .table-filter span {
        margin: 4px 2px 0 3px;
    }
    .table-filter input[type="number"]{
        max-width: 48px;
    }
    @media only screen and (min-width: 1400px){
        .table-filter input, .table-filter select{
            max-width: 6% !important;
        }
        .table-filter select{
            max-width: 85px !important;
        }
        .table-filter .dpo {
            max-width: 90px !important;
        }
    }
    .paginate_button.current.btn.btn-default {
    background: linear-gradient(to bottom, #0d85a8 0%, #35add1 39%,#44c0e5 100%) !important;
    color: #fff !important;
}
    .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current {
         background: linear-gradient(to bottom, #0d85a8 0%, #35add1 39%,#44c0e5 100%) !important;
    color: #fff !important;
    }
     .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button {
         background: linear-gradient(to bottom, #0d85a8 0%, #35add1 39%,#44c0e5 100%) !important;
    color: #fff !important;
    }
     .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
         background: linear-gradient(to bottom, #0d85a8 0%, #35add1 39%,#44c0e5 100%) !important;
    color: #fff !important;
    }
    .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
         background: linear-gradient(to bottom, #0d85a8 0%, #35add1 39%,#44c0e5 100%) !important;
    color: #fff !important;
    }
       
</style>
<div class="col-md-12"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12" id="reminderlist">
                <input type="hidden" class="chk-Addreminder-btn" value="">
                <?php
                $flashAlert =  $this->session->flashdata('alert');
                if(isset($flashAlert['message']) && !empty($flashAlert['message'])){?>
                    <br>
                    <div style="padding: 5px 12px" class="alert <?=$flashAlert['class']?>">
                        <strong><?=$flashAlert['type']?></strong> <?=$flashAlert['message']?>
                        <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>
                <div class="module">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="module-head">
                        <!--<h3> <?php if(isset($title)) echo $title;?></h3>-->
                    </div>
                    <div class="module-body table-responsive">
                        
                        <table id="example" class="cell-border" cellspacing="0" width="100%" data-selected_id="">
                            <thead>
                            <tr>
                                <th class="no-sort text-center">#</th>
                                <th class="text-center column-id"><?php echo $this->lang->line('id');?></th>
                                <th class="text-center column-date"><?php echo $this->lang->line('date');?></th>
                                <th class="text-center column-date"><?php echo $this->lang->line('time');?></th>
                                <th class="text-center column-name" ><?php echo $this->lang->line('added_by');?></th>
                                <th class="text-center column-name" ><?php echo $this->lang->line('name');?></th>
                                <th class="text-center column-name" ><?php echo $this->lang->line('module');?></th>
                                <th class="text-center column-name" >Statut</th>
                                <th class="text-center column-name" ><?php echo $this->lang->line('subject');?></th>
                                <th class="text-center column-name" ><?php echo $this->lang->line('message');?></th>
                                 <th class="text-center column-name" >Days</th>
                                <th class="text-center column-name" >Statut</th>
                                <th class="text-center">Since</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(isset($data) && !empty($data)):?>
                                <?php foreach($data as $key => $item):?>
                                    <tr>
                                        <td class="text-center">
                                            <input type="checkbox" data-input="<?=$item->id;?>" data-time="<?= create_timestamp_uid($item->created_at,$item->id); ?>" class="checkbox ccheckbox singleSelect chk-mainnotifiCat-template" onchange="changeeditfunc(<?=$item->id;?>,this)">
                                        </td>
                                        <td class="text-center">
                                           
                                            <a href="<?=site_url("admin/reminders/".create_timestamp_uid($item->created_at,$item->id)."/edit")?>">
                                                <?=create_timestamp_uid($item->created_at,$item->id);?>
                                            </a>
                                        </td>
                                        <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                                        <td class="text-center"><?=from_unix_time($item->created_at)?></td>
                                       <td class="text-center">
                                          <?php 
                                        $user=$this->reminders_model->getuser($item->user_id);
                                        $user=str_replace(".", " ", $user);
                                        echo $user;
                                        ?></td>
                                        <td class="text-center"><?=$item->name;?></td>
                                        <td class="text-center"><?php if($item->module == 1){echo 'Job Applications';}else if($item->module == 2){echo 'Quotes';}else if($item->module == 3){echo 'Calls';}else if($item->module == 4){echo 'Invoices';}else if($item->module == 5){echo 'Support';}else if($item->module == 6){echo 'Driver';}else if($item->module == 7){echo 'Client';}else if($item->module == 8){echo 'Car';}else if($item->module == 9){echo 'Partner';}else{echo '-';} ?></td>
                                        <td class="text-center"><?php if($item->status == 2){echo 'Pending';}elseif($item->status == 1){echo 'Expired';}else{echo '-';} ?></td>
                                         <td class="text-center"><?=$item->subject;?></td>
                                         <td class="text-center"><?php $out = strlen($item->message) > 12 ? substr($item->message,0,12)."....." : $item->message; ?><?=$out;?></td>
                                        
                                        <td class="text-center">
                                            <?php $reminder = json_decode($item->reminders)[0];
                                                if(!empty($reminder)){
                                                    echo ucfirst($reminder->before_after).' '.$reminder->reminder_days.' days';
                                                }
                                            ?>
                                        </td>
                                      <td class="text-center"><?php if($item->reminder_status == 1){echo '<span class="label label-success">Enabled</span>';}else{echo '<span class="label label-danger">Disabled</span>';} ?></td>
                                        <td style="white-space: nowrap"><?=timeDiff($item->updated_at);?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <br>
                    </div>
                </div>
            </div>
        <div class="col-md-12" id="reminderdeldiv" style="display:none;border:1px solid silver;padding: 20px 10px;margin-top: 15px;">
         <?=form_open("admin/reminders/delete")?>
     
      <input type="hidden" id="deletereminderid" name="reminder_id" value="">
      <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
      <button  class="btn btn-default"style=" float:left;margin-right: 10px;padding-left: 20px;padding-right: 20px;"> Yes </button>
      <button type="button" class="btn btn-default" style="cursor: pointer;padding-left: 20px;padding-right: 20px;" onclick="cancelReminderDel()">No</button>
    <?php echo form_close(); ?>
    </div>
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<!-- <div id="table-filter" class="hide">
    <select class="form-control" data-name="offer">
        <option value="">All Jobs</option>
        <?php foreach(config_model::$job_offers as $key => $offer):?>
            <option value="<?=$offer?>"><?=$offer?></option>
        <?php endforeach;?>
    </select>
    <span class="pull-left">Age:</span>
    <input type="number" placeholder="From" data-name="age_from" class="form-control">
    <input type="number" placeholder="To" data-name="age_to" class="form-control">
    <select class="form-control" data-name="civility">
        <option value="">All Civility</option>
            <?php foreach(config_model::$civility as $key => $civil):?>
                <option <?=set_value('civility',$this->input->post('civility')) == $civil ? "selected" : ""?> value="<?=$civil?>"><?=$civil?></option>
            <?php endforeach;?>
    </select>
    <select class="form-control dep-filter" data-name="department">
        <option value="">All Departments</option>
    </select>
    <input type="text" placeholder="From" class="dpo" data-name="date_from">
    <input type="text" placeholder="To" class="dpo" data-name="date_to">
    <select class="form-control" data-name="status">
        <option value="">All Status</option>
        <?php foreach(config_model::$job_status as $key => $status):?>
            <option <?=set_value('status',$this->input->post('status')) == $status ? "selected" : ""?> value="<?=$status?>"><?=$status?></option>
        <?php endforeach;?>
    </select>
</div> -->
<script type="text/javascript">
     $(document).ready(function() {
        $('.delBtn').addClass('reminderdelbtn');
        $('.reminderdelbtn').removeClass('delBtn');

         $('.editBtn').addClass('remindereditBtn');
        $('.remindereditBtn').removeClass('editBtn');
       
        $('.reminderdelbtn').click(function(event){
            event.preventDefault();
            var val=$('.chk-Addreminder-btn').val();
            if(val == ''){
                alert("Please select a row to delete.");
            }
            else{
                 $('#reminderdeldiv').show();
                 $('#reminderlist').hide();
            }
           
        });

        $('.remindereditBtn').click(function(event){
           
           if ($('.singleSelect:checked').length == 1){
            var val= $('.singleSelect:checked').attr('data-time');
            if(val == ''){
                  event.preventDefault();
                alert("Please select a row.");

            }
            else{
                var str="admin/reminders/"+val+"/edit"
               var url='<?php echo site_url("'+str+'")?>'; 
                 $(this).attr("href", url);
                 
            }
        }else{
              event.preventDefault();
                alert("Please select a row.");
        }
           
        });
       
     });
     function cancelReminderDel(){
         $('#reminderdeldiv').hide();
         $('#reminderlist').show();
     }
     function changeeditfunc(val,e){
       
        $('input.chk-mainnotifiCat-template').not(e).prop('checked', false);
        if($(e).is(":checked")) {
             $('.chk-Addreminder-btn').val(val);
             $('#deletereminderid').val(val);
           }else{
             $('.chk-Addreminder-btn').val('');
             $('#deletereminderid').val('');
           }
     }
</script>