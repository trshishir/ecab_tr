<?php $locale_info = localeconv(); ?>
<link href="<?php echo base_url(); ?>assets/system_design/css/simple-rating.css" rel="stylesheet">
<div class="col-md-12"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12">
                <?php $this->load->view('admin/common/alert');?>
                <div class="module">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="module-head">
                    </div>
                    <div class="module-body">
                        <?=form_open("admin/reminders/".$data->id."/update")?>
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label>Reminder Statut*</label>
                                    <select name="reminder_status" id="reminder_status" class="form-control" required >
                                        <option value="">---Select---</option>
                                        <option <?php if($data->reminder_status == 1){echo 'selected';} ?> value="1">Enabled</option>
                                        <option <?php if($data->reminder_status == 2){echo 'selected';} ?> value="2">Disabled</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label>Module*</label>
                                    <select name="module" id="module" class="form-control" onchange="modulechange()" required >
                                        <option value="">---Select---</option>
                                        <option <?php if($data->module == 1){echo 'selected';} ?> value="1">Job Applications</option>
                                        <option <?php if($data->module == 2){echo 'selected';} ?> value="2">Quotes</option>
                                        <option <?php if($data->module == 3){echo 'selected';} ?> value="3">Calls</option>
                                        <option <?php if($data->module == 4){echo 'selected';} ?> value="4">Invoices</option>
                                        <option <?php if($data->module == 5){echo 'selected';} ?> value="5">Support</option>
                                        <option <?php if($data->module == 6){echo 'selected';} ?> value="6">Driver</option>
                                        <option <?php if($data->module == 7){echo 'selected';} ?> value="7">Client</option>
                                        <option <?php if($data->module == 8){echo 'selected';} ?> value="8">Car</option>
                                        <option <?php if($data->module == 9){echo 'selected';} ?> value="9">Partner</option>
                                    </select>
                                </div>
                            </div>
                        
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label>Statut*</label>
                                    <select name="status" id="status" class="form-control" required >
                                        <option value="">---Select---</option>  
                                         <option <?php if($data->status == 1){echo 'selected';} ?> value="1">Expired</option>
                                        <option <?php if($data->status == 2){echo 'selected';} ?> value="2">Pending</option>
                                      
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label>Name*</label>
                                    <input type="text" maxlength="100" class="form-control" required name="name" placeholder="Name*" value="<?=set_value('name',$data->name)?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                             <div class="col-xs-12">
                                 <label>Reminder Time :</label>
                            </div>
                            <div class="col-xs-12" style="padding:0px;">
                               
                                  
                                  
                                        
                                            <?php $c = 0; $reminders = json_decode($data->reminders);
                                                foreach ($reminders as  $k=>$v) {
                                            ?>
                                                <div class="col-xs-3">
                                                    <div class="form-group">
                                                        <select class="form-control" name="before_after[]"  required>
                                                            
                                                            <option value="after" <?php echo $v->before_after=='after'?'selected':''; ?>>After</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                 <div class="col-xs-3">
                                                    <div class="form-group">
                                                        <select class="form-control" name="reminder_days[]"  required>
                                                            <option value="">Number of Days</option>
                                                            <?php for($i=1;$i<=10;$i++){ ?>
                                                            <option value="<?php echo $i; ?>" <?php echo $v->reminder_days==$i?'selected':''; ?>><?php echo $i; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div> 
                                                 </div>   
                                                
                                               
                                            <?php $c++; } ?>
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label></label>
                                    <input type="checkbox" <?php if($data->send_copy_to_department_operator==1){echo 'checked';} ?>  id="send_copy_to_department_operator" style="margin: 17px 10px 11px 3px !important;"> <span style="display: inline-block;padding-top: 10px;" >Send copy to department operator</span>
                                    <input type="hidden" name="send_copy_to_department_operator" value="<?= $data->send_copy_to_department_operator ?>">
                                </div>
                            </div> 
                                   
                               
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                               <div class="form-group">
                                    <label>Subject*</label>
                                    <input type="text" class="form-control" id="subject" required name="subject" placeholder="Subject*" value="<?=set_value('subject',$data->subject)?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea rows="4" class="form-control message" name="message" placeholder="Message"><?=set_value('name',$data->message)?></textarea>
                                    <script>
                                        CKEDITOR.replace("message", {
                                            customConfig: "<?=base_url("assets/system_design/config.js")?>"
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group" id="userfullshortcode">
                              
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                 <div class="text-right">
                                    <a href="<?=base_url("admin/reminders")?>" class="btn btn-default"><span class="fa fa-close" style="margin-right: 3px;"></span>Cancel</a>
                                    <button class="btn btn-default"><span class="fa fa-save"></span>Save</button>
                                  
                                </div>
                                
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/simple-rating.js"></script>

<script type="text/javascript">
   //code by a_s
  var job="<p>Reply :&nbsp;{last_job_reminder_user_reply}</p><hr /><p>Sent from :&nbsp;{job_reminder_sender_email}</p><p>Date :&nbsp;{job_reminder_date}, Time :&nbsp;{job_reminder_time}</p><p>Sender Name :&nbsp;{job_reminder_civility}&nbsp;{job_reminder_first_name}&nbsp;{job_reminder_last_name} Company :&nbsp;{job_reminder_company_name}</p><p>Subject :&nbsp;{job_reminder_subject}</p><p>Message :&nbsp;{job_reminder_message}</p><hr />";
var quote="<p>Dear &nbsp;{client_civility}&nbsp;{client_firstname}&nbsp;{client_lastname} </p><p>we contact you about your quote {quote_id}done on {quote_date}</p>";
var call="<p>Reply :&nbsp;{last_call_reminder_user_reply}</p><hr /><p>Sent from :&nbsp;{call_reminder_sender_email}</p><p>Date :&nbsp;{call_reminder_date}, Time :&nbsp;{call_reminder_time}</p><p>Sender Name :&nbsp;{call_reminder_civility}&nbsp;{call_reminder_first_name}&nbsp;{call_reminder_last_name} Company :&nbsp;{call_reminder_company_name}</p><p>Subject :&nbsp;{call_reminder_subject}</p><p>Message :&nbsp;{call_reminder_message}</p><hr />";
var invoice="<p>Dear &nbsp;{client_civility}&nbsp;{client_firstname}&nbsp;{client_lastname} </p><p>we contact you about your invoice {invoice_id}done on {invoice_date}</p>";
var support="<p>Reply :&nbsp;{last_support_reminder_user_reply}</p><hr /><p>Sent from :&nbsp;{support_reminder_sender_email}</p><p>Date :&nbsp;{support_reminder_date}, Time :&nbsp;{support_reminder_time}</p><p>Sender Name :&nbsp;{support_reminder_civility}&nbsp;{support_reminder_first_name}&nbsp;{support_reminder_last_name} Company :&nbsp;{support_reminder_company_name}</p><p>Subject :&nbsp;{support_reminder_subject}</p><p>Message :&nbsp;{support_reminder_message}</p><hr />";
var driver="<p>Dear &nbsp;{user_civility}&nbsp;{user_firstname}&nbsp;{user_lastname} </p><p>we contact you about your  {driver_column_name} expired on {driver_column_expire_date}</p>";
var client="<p>Dear &nbsp;{user_civility}&nbsp;{user_firstname}&nbsp;{user_lastname} </p><p>we contact you about your informations. </p>";

var car="<p>Dear &nbsp;{user_civility}&nbsp;{user_firstname}&nbsp;{user_lastname} </p><p>we contact you about your car</p>";
var partner="<p>Dear &nbsp;{user_civility}&nbsp;{user_firstname}&nbsp;{user_lastname} </p><p>we contact you about your  {partner_column_name} expired on {partner_column_expire_date}</p>";


//usefull short code start section
var usecodejob='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{sender_name}\')" >{sender_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_name}\')" >{user_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_reminder_status}\')" >{job_reminder_status}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_reminder_subject}\')" >{job_reminder_subject}</a></span><span><a href="javascript:void();" onclick="appendText(\'{last_job_reminder_user_reply}\')" >{last_job_reminder_user_reply}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_reminder_sender_email}\')" >{job_reminder_sender_email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_reminder_date}\')" >{job_reminder_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_reminder_time}\')" >{job_reminder_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_reminder_civility}\')" >{job_reminder_civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_reminder_first_name}\')" >{job_reminder_first_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_reminder_last_name}\')" >{job_reminder_last_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_reminder_company_name}\')" >{job_reminder_company_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_reminder_message}\')" >{job_reminder_message}</a></span>';
var usecodequote='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{quote_id}\')" >{quote_id}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_date}\')" >{quote_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_reminder_status}\')" >{quote_reminder_status}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_reminder_subject}\')" >{quote_reminder_subject}</a></span><span><a href="javascript:void();" onclick="appendText(\'{last_quote_reminder_user_reply}\')" >{last_quote_reminder_user_reply}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_reminder_sender_email}\')" >{quote_reminder_sender_email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_reminder_date}\')" >{quote_reminder_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_reminder_time}\')" >{quote_reminder_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_civility}\')" >{client_civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_firstname}\')" >{client_firstname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_lastname}\')" >{client_lastname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_reminder_company_name}\')" >{quote_reminder_company_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_reminder_message}\')" >{quote_reminder_message}</a></span>';
var usecodecall='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{sender_name}\')" >{sender_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_name}\')" >{user_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_reminder_status}\')" >{call_reminder_status}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_reminder_subject}\')" >{call_reminder_subject}</a></span><span><a href="javascript:void();" onclick="appendText(\'{last_call_reminder_user_reply}\')" >{last_call_reminder_user_reply}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_reminder_sender_email}\')" >{call_reminder_sender_email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_reminder_date}\')" >{call_reminder_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_reminder_time}\')" >{call_reminder_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_reminder_civility}\')" >{call_reminder_civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_reminder_first_name}\')" >{call_reminder_first_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_reminder_last_name}\')" >{call_reminder_last_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_reminder_company_name}\')" >{call_reminder_company_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_reminder_message}\')" >{call_reminder_message}</a></span>';
var usecodeinvoice='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{invoice_id}\')" >{invoice_id}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_date}\')" >{invoice_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_reminder_status}\')" >{invoice_reminder_status}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_reminder_subject}\')" >{invoice_reminder_subject}</a></span><span><a href="javascript:void();" onclick="appendText(\'{last_invoice_reminder_user_reply}\')" >{last_invoice_reminder_user_reply}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_reminder_sender_email}\')" >{invoice_reminder_sender_email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_reminder_date}\')" >{invoice_reminder_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_reminder_time}\')" >{invoice_reminder_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_civility}\')" >{client_civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_firstname}\')" >{client_firstname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_lastname}\')" >{client_lastname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_reminder_company_name}\')" >{invoice_reminder_company_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_reminder_message}\')" >{invoice_reminder_message}</a></span>';
var usecodesupport='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{sender_name}\')" >{sender_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_name}\')" >{user_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_reminder_status}\')" >{support_reminder_status}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_reminder_subject}\')" >{support_reminder_subject}</a></span><span><a href="javascript:void();" onclick="appendText(\'{last_support_reminder_user_reply}\')" >{last_support_reminder_user_reply}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_reminder_sender_email}\')" >{support_reminder_sender_email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_reminder_date}\')" >{support_reminder_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_reminder_time}\')" >{support_reminder_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_reminder_civility}\')" >{support_reminder_civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_reminder_first_name}\')" >{support_reminder_first_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_reminder_last_name}\')" >{support_reminder_last_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_reminder_company_name}\')" >{support_reminder_company_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_reminder_message}\')" >{support_reminder_message}</a></span>';
var usecodedriver='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{driver_id}\')" >{driver_id}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_date}\')" >{driver_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_reminder_status}\')" >{driver_reminder_status}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_reminder_subject}\')" >{driver_reminder_subject}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_column_name}\')" >{driver_column_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_reminder_sender_email}\')" >{driver_reminder_sender_email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_reminder_date}\')" >{driver_reminder_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_reminder_time}\')" >{driver_reminder_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_civility}\')" >{user_civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_firstname}\')" >{user_firstname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_lastname}\')" >{user_lastname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{company_name}\')" >{company_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_reminder_message}\')" >{driver_reminder_message}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_column_expire_date}\')" >{driver_column_expire_date}</a></span>';
var usecodeclient='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{client_id}\')" >{client_id}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_date}\')" >{client_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_time}\')" >{client_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{company}\')" >{company}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{email}\')" >{email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{phone}\')" >{phone}</a></span><span><a href="javascript:void();" onclick="appendText(\'{mobile}\')" >{mobile}</a></span><span><a href="javascript:void();" onclick="appendText(\'{fax}\')" >{fax}</a></span><span><a href="javascript:void();" onclick="appendText(\'{code_postal}\')" >{code_postal}</a></span><span><a href="javascript:void();" onclick="appendText(\'{civility}\')" >{civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_firstname}\')" >{user_firstname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{lastname}\')" >{lastname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date_of_birth}\')" >{date_of_birth}</a></span>';
var usecodecar='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{car_id}\')" >{car_id}</a></span><span><a href="javascript:void();" onclick="appendText(\'{marque}\')" >{marque}</a></span><span><a href="javascript:void();" onclick="appendText(\'{modele}\')" >{modele}</a></span><span><a href="javascript:void();" onclick="appendText(\'{car_statut}\')" >{car_statut}</a></span><span><a href="javascript:void();" onclick="appendText(\'{immatriculation}\')" >{immatriculation}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{nombre_de_place}\')" >{nombre_de_place}</a></span><span><a href="javascript:void();" onclick="appendText(\'{phone}\')" >{phone}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date_dimmatriculation}\')" >{date_dimmatriculation}</a></span><span><a href="javascript:void();" onclick="appendText(\'{age_du_vehicule}\')" >{age_du_vehicule}</a></span><span><a href="javascript:void();" onclick="appendText(\'{serie}\')" >{serie}</a></span><span><a href="javascript:void();" onclick="appendText(\'{boite_a_vitesse}\')" >{boite_a_vitesse}</a></span><span><a href="javascript:void();" onclick="appendText(\'{carburant}\')" >{carburant}</a></span><span><a href="javascript:void();" onclick="appendText(\'{car_type}\')" >{car_type}</a></span><span><a href="javascript:void();" onclick="appendText(\'{courroie}\')" >{courroie}</a></span><span><a href="javascript:void();" onclick="appendText(\'{couleur}\')" >{couleur}</a></span><span><a href="javascript:void();" onclick="appendText(\'{nature}\')" >{nature}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_date_dentree}\')" >{vendeur_date_dentree}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_kilometrage_dentree}\')" >{vendeur_kilometrage_dentree}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_prix_dachat}\')" >{vendeur_prix_dachat}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_civilite}\')" >{vendeur_civilite}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_first_name}\')" >{vendeur_first_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_last_name}\')" >{vendeur_last_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_image}\')" >{vendeur_image}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_societe}\')" >{vendeur_societe}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_societe_image}\')" >{vendeur_societe_image}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_address}\')" >{vendeur_address}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_address2}\')" >{vendeur_address2}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_code_postal}\')" >{vendeur_code_postal}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_ville}\')" >{vendeur_ville }</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_date_dentree}\')" >{acheteur_date_dentree}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_kilometrage_dentree}\')" >{acheteur_kilometrage_dentree}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_prix_dachat}\')" >{acheteur_prix_dachat}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_civilite}\')" >{acheteur_civilite}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_first_name}\')" >{acheteur_first_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_last_name}\')" >{acheteur_last_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_image}\')" >{acheteur_image}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_societe}\')" >{acheteur_societe}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_societe_image}\')" >{acheteur_societe_image}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_address}\')" >{acheteur_address}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_address2}\')" >{acheteur_address2}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_code_postal}\')" >{acheteur_code_postal}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_ville}\')" >{acheteur_ville }</a></span>';
var usecodepartner='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{partner_id}\')" >{partner_id}</a></span><span><a href="javascript:void();" onclick="appendText(\'{partner_date}\')" >{partner_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{partner_reminder_status}\')" >{partner_reminder_status}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{partner_reminder_subject}\')" >{partner_reminder_subject}</a></span><span><a href="javascript:void();" onclick="appendText(\'{partner_column_name}\')" >{partner_column_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{partner_reminder_sender_email}\')" >{partner_reminder_sender_email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{partner_reminder_date}\')" >{partner_reminder_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{partner_reminder_time}\')" >{partner_reminder_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_civility}\')" >{user_civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_firstname}\')" >{user_firstname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_lastname}\')" >{user_lastname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{company_name}\')" >{company_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{partner_reminder_message}\')" >{partner_reminder_message}</a></span><span><a href="javascript:void();" onclick="appendText(\'{partner_column_expire_date}\')" >{partner_column_expire_date}</a></span>';

//usefull short code end section 
  //code by a_s
    function appendText(text){
        CKEDITOR.instances['message'].insertHtml(text);
    }
  //code by a_s
    function modulechange(){
        var module = $('#module').val();
        var html = '';
        var subject = '';
        
         $("#subject").empty();
         $("#userfullshortcode").empty();

           
       
  
          if(module == "1"){
            subject = "{job_reminder_subject}";
             CKEDITOR.instances['message'].setData(job, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
             document.getElementById("userfullshortcode").innerHTML =  usecodejob;   
          }
          else if(module == "2"){
           
            subject = "{quote_reminder_subject}";
              CKEDITOR.instances['message'].setData(quote, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
          
            document.getElementById("userfullshortcode").innerHTML =  usecodequote;
          }
          else if(module == "3"){
            subject = "{call_reminder_subject}";
             CKEDITOR.instances['message'].setData(call, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
             document.getElementById("userfullshortcode").innerHTML =  usecodecall;

          }
          else if(module == "4"){
           
            subject = "{invoice_reminder_subject}";
             CKEDITOR.instances['message'].setData(invoice, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
             document.getElementById("userfullshortcode").innerHTML =  usecodeinvoice;
          }
           else if(module == "5"){
            subject = "{support_reminder_subject}";
             CKEDITOR.instances['message'].setData(support, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
            document.getElementById("userfullshortcode").innerHTML =  usecodesupport;
          }
        else if(module == "6"){
           
            subject = "{driver_reminder_subject}";
             CKEDITOR.instances['message'].setData(driver, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
             document.getElementById("userfullshortcode").innerHTML =  usecodedriver;
          }
        else if(module == "7"){
            subject = "{client_reminder_subject}";
             CKEDITOR.instances['message'].setData(client, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
            document.getElementById("userfullshortcode").innerHTML =  usecodeclient;
          }
          else if(module == "8"){
            subject = "{car_reminder_subject}";
             CKEDITOR.instances['message'].setData(car, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
            document.getElementById("userfullshortcode").innerHTML =  usecodecar;
          }

        else if(module == "9"){
          subject = "{partner_reminder_subject}";
           CKEDITOR.instances['message'].setData(partner, function()
              {
                    CKEDITOR.instances['message'].resetDirty();
              });
          document.getElementById("userfullshortcode").innerHTML =  usecodepartner;
        }         
               
                 document.getElementById("subject").value = subject;
      
    }
//code by a_s
 $(document).ready(function() {
        //code by s_a
         var module = $('#module').val();
            if(module == "1"){
             document.getElementById("userfullshortcode").innerHTML =  usecodejob; 
            }
            else if(module == "2"){
             document.getElementById("userfullshortcode").innerHTML =  usecodequote; 
            }
             else if(module == "3"){
             document.getElementById("userfullshortcode").innerHTML =  usecodecall; 
            }
             else if(module == "4"){
             document.getElementById("userfullshortcode").innerHTML =  usecodeinvoice; 
            }
             else if(module == "5"){
             document.getElementById("userfullshortcode").innerHTML =  usecodesupport; 
            }
              else if(module == "6"){
             document.getElementById("userfullshortcode").innerHTML =  usecodedriver; 
            }
             else if(module == "7"){
             document.getElementById("userfullshortcode").innerHTML =  usecodeclient; 
            }
             else if(module == "8"){
             document.getElementById("userfullshortcode").innerHTML =  usecodecar; 
            }
            else if(module == "9"){
             document.getElementById("userfullshortcode").innerHTML =  usecodepartner; 
            }

    });

         $('#send_copy_to_department_operator').change(function() {
            if(this.checked) {
                $("input[name='send_copy_to_department_operator']").val("1");
            }else{
                $("input[name='send_copy_to_department_operator']").val("0");
            }
        });
        //code by s_a
</script>
<style>
    .two-fields{
        width: 100%;
        border: 1px solid #ccc;
        padding: 10px;
        background: linear-gradient(to bottom, #fbfbfb 0%, #ececec 39%, #ececec 39%, #c1c1c1 100%);
    }
    .attach-main {
        display: flex;
    }
    .attach-buttons {
        padding: 10px 0px 0px 20px;
        text-align: left;
    }
</style>