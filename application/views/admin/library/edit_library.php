<?php $locale_info = localeconv(); ?>
<link href="<?php echo base_url(); ?>assets/system_design/css/simple-rating.css" rel="stylesheet">
<div class="col-md-12" style="height: calc(100vh - 140px)"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12">
                <?php $this->load->view('admin/common/alert');?>
                <div class="module">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="module-head">
                    </div>
                    <div class="module-body">
                        <?=form_open_multipart("admin/library/update_library")?>
                        <input type="hidden" name="id" value="<?=$library['id'];?>">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label>Statut *</label>
                                    <select name="statut" id="notification_status" class="form-control" required>
                                        <option <?php if($library['statut'] == 1){echo 'selected';} ?> value="1">Enabled</option>
                                        <option <?php if($library['statut'] == 2){echo 'selected';} ?> value="2">Disabled</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label>Destination *</label>
                                    <input type="text" maxlength="100" class="form-control" required name="destination" placeholder="Destination" value="<?= $library['destination'];?>">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label>Category *</label>
                                    <select name="user_category" id="department" class="form-control" required onchange="modulechange()">
                                        <option value="">---Select---</option>
                                        <option <?php if($library['user_category'] == 1){echo 'selected';} ?> value="1">Job Applications</option>
                                        <option <?php if($library['user_category'] == 2){echo 'selected';} ?> value="2">Quotes</option>
                                        <option <?php if($library['user_category'] == 3){echo 'selected';} ?> value="3">Calls</option>
                                        <option <?php if($library['user_category'] == 4){echo 'selected';} ?> value="4">Invoices</option>
                                        <option <?php if($library['user_category'] == 5){echo 'selected';} ?> value="5">Support</option>
                                        <option <?php if($library['user_category'] == 6){echo 'selected';} ?> value="6">Booking</option>
                                         <option <?php if($library['user_category'] == 7){echo 'selected';} ?> value="7">Driver</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label>Document Name *</label>
                                     <input type="text" maxlength="100" class="form-control" required name="documents_name" placeholder="Document Name" value="<?= $library['documents_name'];?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>Short Codes</label>
                                    <textarea rows="4" class="form-control message"  name="message" placeholder="Message"><?=set_value('message',$this->input->post('message'))?>  <?= $library['short_code'];?></textarea>
                                    <script>
                                        CKEDITOR.replace("message", {
                                            customConfig: "<?=base_url("assets/system_design/config.js")?>"
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group" id="userfullshortcode">
                                   
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-right">
                                     <a href="<?=base_url("admin/library")?>" class="btn btn-default"><span class="fa fa-close" style="margin-right: 3px;"></span>Cancel</a>
                                    <button class="btn btn-default"><span class="fa fa-save"></span>Save</button>
                                   
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/simple-rating.js"></script>

<script type="text/javascript">
var job="<p>Reply :&nbsp;{last_job_notification_user_reply}</p><hr /><p>Sent from :&nbsp;{job_notification_sender_email}</p><p>Date :&nbsp;{job_notification_date}, Time :&nbsp;{job_notification_time}</p><p>Sender Name :&nbsp;{job_notification_civility}&nbsp;{job_notification_first_name}&nbsp;{job_notification_last_name} Company :&nbsp;{job_notification_company_name}</p><p>Subject :&nbsp;{job_notification_subject}</p><p>Message :&nbsp;{job_notification_message}</p><hr />";
var quote="<p>Dear &nbsp;{client_civility}&nbsp;{client_firstname}&nbsp;{client_lastname} </p><p>we contact you about your quote {quote_id}done on {quote_date}</p>";
var call="<p>Reply :&nbsp;{last_call_notification_user_reply}</p><hr /><p>Sent from :&nbsp;{call_notification_sender_email}</p><p>Date :&nbsp;{call_notification_date}, Time :&nbsp;{call_notification_time}</p><p>Sender Name :&nbsp;{call_notification_civility}&nbsp;{call_notification_first_name}&nbsp;{call_notification_last_name} Company :&nbsp;{call_notification_company_name}</p><p>Subject :&nbsp;{call_notification_subject}</p><p>Message :&nbsp;{call_notification_message}</p><hr />";
var invoice="<p>Dear &nbsp;{client_civility}&nbsp;{client_firstname}&nbsp;{client_lastname} </p><p>we contact you about your invoice {invoice_id}done on {invoice_date}</p>";
var support="<p>Reply :&nbsp;{last_support_notification_user_reply}</p><hr /><p>Sent from :&nbsp;{support_notification_sender_email}</p><p>Date :&nbsp;{support_notification_date}, Time :&nbsp;{support_notification_time}</p><p>Sender Name :&nbsp;{support_notification_civility}&nbsp;{support_notification_first_name}&nbsp;{support_notification_last_name} Company :&nbsp;{support_notification_company_name}</p><p>Subject :&nbsp;{support_notification_subject}</p><p>Message :&nbsp;{support_notification_message}</p><hr />";
var booking="<p>Dear &nbsp;{client_civility}&nbsp;{client_firstname}&nbsp;{client_lastname} </p><p>we contact you about your booking {booking_id}done on {booking_date}</p>";
var driver="<p>Dear &nbsp;{user_civility}&nbsp;{user_firstname}&nbsp;{user_lastname} </p><p>we contact you about your  {driver_column_name} expired on {driver_column_expire_date}</p>";

//usefull short code start section
var usecodejob='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{sender_name}\')" >{sender_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_name}\')" >{user_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_notification_status}\')" >{job_notification_status}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_notification_subject}\')" >{job_notification_subject}</a></span><span><a href="javascript:void();" onclick="appendText(\'{last_job_notification_user_reply}\')" >{last_job_notification_user_reply}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_notification_sender_email}\')" >{job_notification_sender_email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_notification_date}\')" >{job_notification_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_notification_time}\')" >{job_notification_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_notification_civility}\')" >{job_notification_civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_notification_first_name}\')" >{job_notification_first_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_notification_last_name}\')" >{job_notification_last_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_notification_company_name}\')" >{job_notification_company_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_notification_message}\')" >{job_notification_message}</a></span>';
var usecodequote='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{quote_id}\')" >{quote_id}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_date}\')" >{quote_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_notification_status}\')" >{quote_notification_status}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_notification_subject}\')" >{quote_notification_subject}</a></span><span><a href="javascript:void();" onclick="appendText(\'{last_quote_notification_user_reply}\')" >{last_quote_notification_user_reply}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_notification_sender_email}\')" >{quote_notification_sender_email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_notification_date}\')" >{quote_notification_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_notification_time}\')" >{quote_notification_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_civility}\')" >{client_civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_firstname}\')" >{client_firstname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_lastname}\')" >{client_lastname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_notification_company_name}\')" >{quote_notification_company_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_notification_message}\')" >{quote_notification_message}</a></span>';
var usecodecall='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{sender_name}\')" >{sender_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_name}\')" >{user_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_notification_status}\')" >{call_notification_status}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_notification_subject}\')" >{call_notification_subject}</a></span><span><a href="javascript:void();" onclick="appendText(\'{last_call_notification_user_reply}\')" >{last_call_notification_user_reply}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_notification_sender_email}\')" >{call_notification_sender_email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_notification_date}\')" >{call_notification_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_notification_time}\')" >{call_notification_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_notification_civility}\')" >{call_notification_civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_notification_first_name}\')" >{call_notification_first_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_notification_last_name}\')" >{call_notification_last_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_notification_company_name}\')" >{call_notification_company_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_notification_message}\')" >{call_notification_message}</a></span>';
var usecodeinvoice='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{invoice_id}\')" >{invoice_id}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_date}\')" >{invoice_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_notification_status}\')" >{invoice_notification_status}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_notification_subject}\')" >{invoice_notification_subject}</a></span><span><a href="javascript:void();" onclick="appendText(\'{last_invoice_notification_user_reply}\')" >{last_invoice_notification_user_reply}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_notification_sender_email}\')" >{invoice_notification_sender_email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_notification_date}\')" >{invoice_notification_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_notification_time}\')" >{invoice_notification_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_civility}\')" >{client_civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_firstname}\')" >{client_firstname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_lastname}\')" >{client_lastname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_notification_company_name}\')" >{invoice_notification_company_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_notification_message}\')" >{invoice_notification_message}</a></span>';
var usecodesupport='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{sender_name}\')" >{sender_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_name}\')" >{user_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_notification_status}\')" >{support_notification_status}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_notification_subject}\')" >{support_notification_subject}</a></span><span><a href="javascript:void();" onclick="appendText(\'{last_support_notification_user_reply}\')" >{last_support_notification_user_reply}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_notification_sender_email}\')" >{support_notification_sender_email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_notification_date}\')" >{support_notification_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_notification_time}\')" >{support_notification_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_notification_civility}\')" >{support_notification_civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_notification_first_name}\')" >{support_notification_first_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_notification_last_name}\')" >{support_notification_last_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_notification_company_name}\')" >{support_notification_company_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_notification_message}\')" >{support_notification_message}</a></span>';
var usecodebooking='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{booking_id}\')" >{booking_id}</a></span><span><a href="javascript:void();" onclick="appendText(\'{booking_date}\')" >{booking_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{booking_notification_status}\')" >{booking_notification_status}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{booking_notification_subject}\')" >{booking_notification_subject}</a></span><span><a href="javascript:void();" onclick="appendText(\'{last_booking_notification_user_reply}\')" >{last_booking_notification_user_reply}</a></span><span><a href="javascript:void();" onclick="appendText(\'{booking_notification_sender_email}\')" >{booking_notification_sender_email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{booking_notification_date}\')" >{booking_notification_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{booking_notification_time}\')" >{booking_notification_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_civility}\')" >{client_civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_firstname}\')" >{client_firstname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_lastname}\')" >{client_lastname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{booking_notification_company_name}\')" >{booking_notification_company_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{booking_notification_message}\')" >{booking_notification_message}</a></span>';
var usecodedriver='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{driver_id}\')" >{driver_id}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_date}\')" >{driver_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_notification_status}\')" >{driver_notification_status}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_notification_subject}\')" >{driver_notification_subject}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_column_name}\')" >{driver_column_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_notification_sender_email}\')" >{driver_notification_sender_email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_notification_date}\')" >{driver_notification_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_notification_time}\')" >{driver_notification_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_civility}\')" >{user_civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_firstname}\')" >{user_firstname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_lastname}\')" >{user_lastname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{company_name}\')" >{company_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_notification_message}\')" >{driver_notification_message}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_column_expire_date}\')" >{driver_column_expire_date}</a></span>';

//usefull short code end section 

    $(document).ready(function() {
        $('#send_copy_to_department_operator').change(function() {
            if(this.checked) {
                $("input[name='send_copy_to_department_operator']").val("1");
            }else{
                $("input[name='send_copy_to_department_operator']").val("0");
            }
        });
        //CKEDITOR.instances['message'].insertHtml('<p>Reply :&nbsp;{last_quote_notification_user_reply}</p><hr /><p>Sent from :&nbsp;{quote_notification_sender_email}</p><p>Date :&nbsp;{quote_notification_date}, Time :&nbsp;{quote_notification_time}</p><p>Sender Name :&nbsp;{client_civility}&nbsp;{client_firstname}&nbsp;{client_lastname} Company :&nbsp;{quote_notification_company_name}</p><p>Subject :&nbsp;{quote_notification_subject}</p><p>Message :&nbsp;{quote_notification_message}</p><hr />');
    });
   /* $(window).load(function(){
        CKEDITOR.instances['message'].insertHtml('<p>Reply :&nbsp;{last_quote_notification_user_reply}</p><hr /><p>Sent from :&nbsp;{quote_notification_sender_email}</p><p>Date :&nbsp;{quote_notification_date}, Time :&nbsp;{quote_notification_time}</p><p>Sender Name :&nbsp;{client_civility}&nbsp;{client_firstname}&nbsp;{client_lastname} Company :&nbsp;{quote_notification_company_name}</p><p>Subject :&nbsp;{quote_notification_subject}</p><p>Message :&nbsp;{quote_notification_message}</p><hr />');
    });*/
    function appendText(text){
        CKEDITOR.instances['message'].insertHtml(text);
    }
    function modulechange(){
        var module = $('#department').val();
        var html = '';
        var subject = '';
         $("#status").empty();
         $("#subject").empty();
         $("#userfullshortcode").empty();

            html = '<option value="">---Select---</option>';
            html +='<option value="1">New</option>';
            html +='<option value="2">Pending</option>';
            html +='<option value="3">Replied</option>';
            html +='<option value="4">Closed</option>';
       
  
          if(module == "1"){
            subject = "{job_notification_subject}";
             CKEDITOR.instances['message'].setData(job, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
             document.getElementById("userfullshortcode").innerHTML =  usecodejob;   
          }
          else if(module == "2"){
            html = '<option value="">---Select---</option>';
            html +='<option value="1">Pending</option>';
            html +='<option value="2">Accepted</option>';
            html +='<option value="3">Denied</option>';
            html +='<option value="4">Cancelled</option>';
            subject = "{quote_notification_subject}";
              CKEDITOR.instances['message'].setData(quote, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
          
            document.getElementById("userfullshortcode").innerHTML =  usecodequote;
          }
          else if(module == "3"){
            subject = "{call_notification_subject}";
             CKEDITOR.instances['message'].setData(call, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
             document.getElementById("userfullshortcode").innerHTML =  usecodecall;

          }
          else if(module == "4"){
            html = '<option value="">---Select---</option>';
            html +='<option value="1">Pending</option>';
            html +='<option value="2">Paid</option>';
            html +='<option value="3">Cancelled</option>';
            subject = "{invoice_notification_subject}";
             CKEDITOR.instances['message'].setData(invoice, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
             document.getElementById("userfullshortcode").innerHTML =  usecodeinvoice;
          }
           else if(module == "5"){
            subject = "{support_notification_subject}";
             CKEDITOR.instances['message'].setData(support, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
            document.getElementById("userfullshortcode").innerHTML =  usecodesupport;
          }
            else if(module == "6"){
            html = '<option value="">---Select---</option>';
            html +='<option value="1">Pending</option>';
            html +='<option value="2">Confirmed</option>';
            html +='<option value="3">Cancelled</option>';
            subject = "{booking_notification_subject}";
             CKEDITOR.instances['message'].setData(booking, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
            document.getElementById("userfullshortcode").innerHTML =  usecodebooking;
          }
           else if(module == "7"){
            html = '<option value="">---Select---</option>';
            html +='<option value="1">Expired</option>';
            subject = "{driver_notification_subject}";
             CKEDITOR.instances['message'].setData(driver, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
             document.getElementById("userfullshortcode").innerHTML =  usecodedriver;
          }
          else{
                html = '<option value="">---Select---</option>';

            }
         
                document.getElementById("status").innerHTML =  html;
                 document.getElementById("subject").value = subject;
      
    }
   
</script>