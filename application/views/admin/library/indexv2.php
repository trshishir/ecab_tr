<script type="text/javascript">

    var inc_var = 0;

    function showAdd() {
        hideForms();
        $('.addDiv').show();
        //showAddHead();
    }
    function hideForms() {
        $('.addDiv').hide();
        $('.editDiv').hide();
        $('.listDiv').hide();

        for(var i = 1; i <= inc_var; i++) {
            $('#editform'+i).hide();
        }
    }
    function hideAdd() {
        hideForms();
        $('.listDiv').show();
    }
    (function ($, W, D) {
        var JQUERY4U = {};

        JQUERY4U.UTIL =
                {
                    setupFormValidation: function ()
                    {
                        //Additional Methods            
                        $.validator.addMethod("lettersonly", function (a, b) {
                            return this.optional(b) || /^[a-z ]+$/i.test(a)
                        }, "<?php echo $this->lang->line('valid_name'); ?>");

                        //form validation rules
                        $("#document_addform").validate({
                            rules: {
                                status: {
                                    required:true
                                },
                                category: {
                                    required:true
                                },
                                destination: {
                                    required:true
                                },
                                title: {
                                    required:true
                                },
                                custom_text: {
                                    required:true
                                }
                            },
                            messages: {
                                status: {
                                    required:"Please select the status"
                                },
                                category: {
                                    required:"Please select the category"
                                },
                                destination: {
                                    required:"Please select the destination"
                                },
                                title: {
                                    required:"Please select the document name"
                                },
                                custom_text: {
                                    required:"Please provide custom text"
                                }
                            }, 
                            submitHandler: function(form) {
                                form.submit();
                            }
                        });
                    }
                }

        //when the dom has loaded setup form validation rules
        $(D).ready(function ($) {
            JQUERY4U.UTIL.setupFormValidation();
        });

    })(jQuery, window, document);
    
</script>
<section id="content">
    <div class="listDiv">
        <?php $this->load->view('admin/common/breadcrumbs'); ?>
        <div class="row-fluid">
            <?php   $flashAlert = $this->session->flashdata('alert');
                    if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
            ?>
                <br>
                <div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
                    <strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
                    <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php   } ?>

            <div class="module">
                <?php echo $this->session->flashdata('message'); ?>
                <div class="module-head">
                       <div class="module-filter">
                           <div class="col-md-8">
                                <?php echo form_open('clients', array("id" => "search_filter")); ?>
                                    <div class="row  row-no-gutters padding-b">
                                        <!-- <div class="col-md-2">
                                             <input type="text" name="search_name" id="search_name" placeholder="Name" class="input-large form-control" data-name="name">
                                        </div> -->
                                        <!--<div class="col-md-2">
                                             <select class="form-control" data-name="subject">
                                                 <option>All Subject</option>
                                                 <option value="Demande de Rappel ecab.app">Demande de Rappel ecab.app</option>
                                                 <option selected="" value="Demande de Devis ecab.app">Demande de Devis ecab.app</option>
                                                 <option value="Candidature Chauffeur ecab.app">Candidature Chauffeur ecab.app</option>
                                             </select>
                                        </div>-->
                                        <!-- <div class="col-md-2">
                                             <input type="text" name="search_email" id="search_email" placeholder="Email" data-name="email" class="form-control">
                                        </div>
                                        <div class="col-md-2">
                                             <input type="text" name="search_phone" id="search_phone" placeholder="Phone" data-name="phone" class="form-control">
                                        </div> -->
                                        <div class="col-md-1">
                                            <input type="text" name="date_from" id="date_from" placeholder="From" data-name="date_from" class="dpo">
                                        </div>
                                        <div class="col-md-1">
                                            <input type="text" name="date_to" id="date_to" placeholder="To" data-name="date_to" class="dpo">
                                        </div>
                                        <div class="col-md-1">
                                            <select class="form-control" name="status" id="status" data-name="status">
                                                 <option value="">All Status</option>
                                                 <option value="1">Active</option>
                                                 <option value="0">Inactive</option>
                                             </select>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="submit" name="search" id="search" value="Search" class="btn btn-sm btn-default" />
                                        </div>
                                     </div>
                                <?php echo form_close(); ?>
                            </div>
                           <div class="col-md-4 no-right-pad">
                               <div class="dt-buttons">
                                   <a href="javascript:;" class="btn btn-sm btn-default" onclick="showAdd();"><i class="fa fa-plus"></i> Add</a> 
                                   <a href="Javascript:;" class="btn btn-sm btn-default editBtn" onclick="return editClick();"><i class="fa fa-pencil"></i> Edit</a> 
                                   <a href="Javascript:;" class="btn btn-sm btn-default delBtn"><i class="fa fa-trash"></i> Delete</a>
                                   <!--<a href="#" class="btn btn-sm btn-default delBtn"><i class="fa fa-download"></i> Import</a>-->
                                   <a href="Javascript:;" class="btn btn-sm btn-default excelDownload"><i class="fa fa-download"></i> Excel</a>
                                   <a href="Javascript:;" class="btn btn-sm btn-default csvDownload"><i class="fa fa-download"></i> CSV</a>
                                   <a href="Javascript:;" class="btn btn-sm btn-default pdfDownload"><i class="fa fa-download"></i> PDF</a>
                               </div>
                           </div>
                        </div>
                </div>
                <div class="clearfix"></div>
                <div class="module-body table-responsive">
                    <table id="manageDocumentsTable" class="table table-bordered table-striped  table-hover dataTable" cellspacing="0" width="100%" data-selected_id="">
                        <thead>
                            <tr>
                                <th class="no-sort text-center"><input type='checkbox' name='allClients' id="allClients" value='' onclick="Javascript:selectAllClients();" /></th>
                                <th class="column-id"><?php echo $this->lang->line('id'); ?></th>
                                <th class="column-date"><?php echo "Date"; ?></th>
                                <th class="column-time"><?php echo "Time"; ?></th>
                                <th class="column-col1" ><?php echo "Added By"; ?></th>
                                <th class="column-col2" ><?php echo "Destination"; ?></t1h>
                                <th class="column-dcs" ><?php echo "Category"; ?></th>
                                <th class="column-dcs1" ><?php echo "Doc Name"; ?></th>
                                <th class="column-view"><?php echo $this->lang->line('view'); ?></th>
                                <th class="column-status"><?php echo $this->lang->line('status'); ?></th>
                                <th class="column-since"><?php echo "Since"; ?></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="addDiv" style="display:none;">
        <?php $this->data['show_subtitle'] = true ; $this->load->view('admin/common/breadcrumbs'); ?>
        <div class="row-fluid">
            <?php   $flashAlert = $this->session->flashdata('alert');
                    if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
            ?>
                <br>
                <div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
                    <strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
                    <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php   } ?>
        </div>
        <?php echo form_open('library/add', array("id" => "document_addform")); ?>
        <div class="row">
            <div class="col-md-12">
                <!-- <div class="row">
                    <div class="col-md-2 form-group"> 
                        <label> Civility </label>                                
                        <select class="form-control" name="civility" tabindex="3">
                            <option value="Mr">Mr</option>
                            <option value="Miss">Miss</option>
                            <option value="Mrs">Mrs</option>
                            <option value="Mme">Mme</option>
                        </select>
                    </div>
                </div> -->
                <div class="row" style="margin-top: 100px;">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label>Statut</label>
                            <select class="form-control"  name="status" id="status" tabindex="1" data-name="status">
                                <option value="Enabled">Enabled</option>
                                <option value="Disabled">Disabled</option>
                             </select>
                        </div>
                        <div class="col-md-3 form-group"> 
                            <label> User Category <img class="displayimageloader" style="float: left; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" /> </label>                                
                            <select class="form-control" name="dcsaadd" id="dcsaadd" tabindex="1" onchange="return onUserGroupChange(this.value,0);">
                                <option value="">Select User Category</option>
                        <?php foreach($groups as $g) { ?>
                                <option value="<?=$g->id;?>"><?=$g->description;?></option>
                        <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-3 form-group"> 
                            <!-- <label>Name</label> -->
                            <label> Username </label>
                            <select class="form-control" name="dcsa" id="dcsa" tabindex="2">
                                <option value="">-- Select User Category First --</option>
                            </select>
                        </div>
                        <!-- <div class="col-md-3">
                            <label>Destination</label>
                            <select class="form-control"  name="destination" id="destination" tabindex="3" data-name="status">
                                <option value="">Select Destination</option>
                        <?php foreach($destinations as $des) { ?>
                                <option value="<?=$des->id;?>"><?=$des->doc_destination;?></option>
                        <?php } ?>
                             </select>
                        </div> -->
                        <div class="col-md-3 form-group"> 
                            <label>Document Name</label>
                            <?php echo form_input($doc_title); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- <div class="col-md-12">
                        <div class="col-md-4">
                            <label>Logo</label>
                            <input class="form-control" type="file" name="logo" id="logo">
                            <label>Address</label>
                            <input class="form-control" type="text" placeholder="Address" name="address" id="address"><br />
                            <label>Field Label</label>
                            <input class="form-control" type="text" placeholder="Label" name="field1" id="field1">
                            <br />
                            <label>Field Label</label>
                            <input class="form-control" type="text" placeholder="Label" name="field2" id="field2">
                        </div>
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                            <label>Destination Short Codes</label>
                            <textarea class="form-control" rows="11" cols="10" placeholder="Destination Short Codes"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <label>Subject</label>
                            <input class="form-control" type="text" placeholder="Subject" name="subject" id="subject">
                        </div>
                    </div> -->
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <label>Custom Text</label>
                            <!-- <textarea class="form-control" rows="20" name="custom_text" id="custom_text" tabindex="5" placeholder="Custom Text"></textarea> -->
                            <textarea id="editor" name="custom_text" rows="20"></textarea>
                            <!-- <div class="editor"></div> -->

                        </div>
                    </div>
                    <!-- <div class="col-md-12">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4 form-group"> 
                            <label> Select City </label>                                
                            <select class="form-control" name="city" id="city" tabindex="1">
                                <option value="">Select City</option>
                        <?php foreach($cities as $c) { ?>
                                <option value="<?=$c->id;?>"><?=$c->city;?></option>
                        <?php } ?>
                            </select>
                            <label>Date</label>
                            <input class="form-control" type="date" placeholder="Date" name="date" id="date">
                            <br />
                            <label>Signature</label>
                            <textarea class="form-control" rows="4" cols="4" placeholder="Signature Short Codes"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <label>Company Name | Company Address | Short Codes from Company Page</label>
                            <textarea class="form-control" rows="4" cols="4" placeholder="Company Name | Company Address"></textarea>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
        <div class="row">
            <br />
            <div class="col-md-12"> 
                <div class="pull-right">
                    <!-- <input type="submit" class="btn btn-default" value="Save" tabindex="8" />   -->
                    <!-- <input type="button" class="btn btn-default" value="Cancel" onclick="return hideAdd();" tabindex="9" /> -->
                    <button type="button" class="btn btn-default" tabindex="7" onclick="return hideAdd();"><i class="fa fa-ban"></i>&nbsp;&nbsp;Cancel</button>
                    <button type="submit" class="btn btn-default" tabindex="6"><i class="fa fa-save"></i>&nbsp;&nbsp;Save</button>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>

    </div>
    <div class="editDiv" style="display:none;">
        <?php $this->data['show_subtitle'] = true ; $this->load->view('admin/common/breadcrumbs'); ?>
        <div class="row-fluid">
            <?php   $flashAlert = $this->session->flashdata('alert');
                    if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
            ?>
                <br>
                <div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
                    <strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
                    <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php   } ?>
        </div>
    <?php 
        echo form_open('library/edit_template', array("id" => "library_editform")); 

        $n = 1;
        foreach ($all_records as $key => $value) {
    ?>
        <div class="row" id="editform<?=$n?>">
            <div class="col-md-12">
                <div class="row" style="margin-top: 100px;">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label>Status</label>
                            <select class="form-control"  name="status[]" id="status" tabindex="1" data-name="status">
                                <option value="">All Status</option>
                        <?php foreach($statuses as $sta) { ?>
                                <option value="<?=$sta->id;?>" <?php if($value['lib_status'] == $sta->id) echo 'selected'; ?>><?=$sta->doc_status;?></option>
                        <?php } ?>
                             </select>
                        </div>
                        <div class="col-md-3 form-group"> 
                            <label> Document Category </label> 
                            <select class="form-control" name="category[]" id="doc_category" tabindex="2">
                                <option value="">Select Category</option>
                        <?php foreach($categories as $cat) { ?>
                                <option value="<?=$cat->id;?>" <?php if($value['lib_category'] == $cat->id) echo 'selected'; ?>><?=$cat->doc_category;?></option>
                        <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Destination</label>
                            <select class="form-control"  name="destination[]" id="destination" tabindex="3" data-name="status">
                                <option value="">Select Destination</option>
                        <?php foreach($destinations as $des) { ?>
                                <option value="<?=$des->id;?>" <?php if($value['lib_destination'] == $des->id) echo 'selected'; ?>><?=$des->doc_destination;?></option>
                        <?php } ?>
                             </select>
                        </div>
                        <div class="col-md-3 form-group"> 
                            <label>Select Document Name</label>
                            <input type="text" name="title[]" value="" class="user form-control" tabindex="4" placeholder="Document Name" id="title">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <label>Custom Text</label>
                            <!-- <textarea class="form-control" rows="20" name="custom_text[]" id="custom_text" tabindex="5" placeholder="Custom Text"><?=$value['lib_customtext'];?></textarea> -->
                            <textarea id="editor" name="custom_text[]" rows="20"><?=$value['lib_customtext'];?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript"> inc_var = <?php echo $n; ?></script>
    <?php 
            $n++;
        } 
    ?>
        <div class="row">
            <br />
            <div class="col-md-12"> 
                <div class="pull-right">
                    <!-- <input type="submit" class="btn btn-default" value="Save" tabindex="8" />   -->
                    <!-- <input type="button" class="btn btn-default" value="Cancel" onclick="return hideAdd();" tabindex="9" /> -->
                    <button type="button" class="btn btn-default" tabindex="7" onclick="return hideAdd();"><i class="fa fa-ban"></i>&nbsp;&nbsp;Cancel</button>
                    <button type="submit" class="btn btn-default" tabindex="6"><i class="fa fa-save"></i>&nbsp;&nbsp;Update</button>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
</section>
</div>
<!--
<div class="filter-search" style="display: none;">
    <?php // echo form_open('clients', array("id" => "search_filter")); ?>
    <form action="<?php echo base_url();?>clients" method="post" accept-charset="utf-8" name="search_filter" id="search_filter">
        <div class="row  row-no-gutters padding-b">
            <div class="col-md-2">
                 <input type="text" name="search_name" id="search_name" placeholder="Name" class="input-large form-control" data-name="name">
            </div>
            <div class="col-md-2">
                 <input type="text" name="search_email" id="search_email" placeholder="Email" data-name="email" class="form-control">
            </div>
            <div class="col-md-2">
                 <input type="text" name="search_phone" id="search_phone" placeholder="Phone" data-name="phone" class="form-control">
            </div>
            <div class="col-md-1">
                <input type="text" name="date_from" id="date_from" placeholder="From" data-name="date_from" class="dpo">
            </div>
            <div class="col-md-1">
                <input type="text" name="date_to" id="date_to" placeholder="To" data-name="date_to" class="dpo">
            </div>
            <div class="col-md-1">
                <select class="form-control" name="status" id="status" data-name="status" style="width:100%!important;">
                     <option value="">All Status</option>
                     <option value="1">Active</option>
                     <option value="0">Inactive</option>
                 </select>
            </div>
            <div class="col-md-2">
                <input type="submit" name="search" id="search" value="Search" class="btn btn-sm btn-default" />
            </div>
         </div>
    <?php // echo form_close(); ?>
    </div>
</div>
-->
 <script>

    function editClick(){
        // alert("edit clicked");
        var values = $("input:checkbox:checked")
              .map(function(){return $(this).val();}).get();
        if(values != "") {
            hideForms();
            $('.editDiv').show();
            // alert(values);
            var str = values+'';
            var result = str.split(',');
            // alert("legnth:  " + result.length);
            for(var i = 0; i < result.length; i++) {
                // alert( "value " + result[i] );
                $('#editform'+result[i]).show();
            }
        } else {
            alert("Please select at least one record.")
        }
        // window.location.href="<?php echo base_url();?>admin/library/edit.php";
    }

    // initSample();

    $(document).ready(function() {
        CKEDITOR.replace('editor',
            {
               height: '500px'
            });
        // $("#document_addform").on("submit", function () {
        //     var hvalue = $('#editor').text();
        //     var data = CKEDITOR.instances.editor.getData();
        //     // $(this).append("<textarea id='document_body' name='document_body' style='display:none'>"+hvalue+"</textarea>");
        //     var removeElements = function(data) {
        //         var wrapped = $("<div>" + data + "</div>");
        //         wrapped.find('script').remove();
        //         return wrapped.html();
        //     }
        //     $("#document_body").val(removeElements);
        //     alert("submittedd " + data);
        // });
        var manageTable = $('#manageDocumentsTable').DataTable({
            'ajax': 'library/fetchLibraryData',
            'order': [],
            'paging': true,
            'pageLength': 15,
            'columnDefs': [ { "targets": 0, "className": "text-center", "width": "4%" },
                            { "targets": 1, "className": "text-center", "width": "4%" },
                            { "targets": 2, "className": "text-center", "width": "4%" },
                            { "targets": 3, "className": "text-center", "width": "4%" },
                            { "targets": 4, "className": "text-center", "width": "10%" },
                            { "targets": 5, "className": "text-center", "width": "8%" },
                            { "targets": 6, "className": "text-center", "width": "4%" },
                            { "targets": 7, "className": "text-center", "width": "5%" },
                            { "targets": 8, "className": "text-center", "width": "5%" },
                            { "targets": 9, "className": "text-center", "width": "5%" },
                            { "targets": 10, "className": "text-center", "width": "8%" }
                          ] 
        });
        
        $(".pdfDownload").on('click',function() {
           alert('PDF Download inprogress...'); 
        });
        $(".csvDownload").on('click',function() {
           alert('CSV Download inprogress...'); 
        });
        $(".excelDownload").on('click',function() {
           // console.log('Excel Download inprogress...'); 
           $.ajax({
                        url: 'clients/clientExcelDownload',
                        type: 'post',
                        data: $("#search_filter").serialize(), // /converting the form data into array and sending it to server
                        dataType: 'json',
                        success: function (response) {
                            //console.log(response.data);
                        }
                 });
        });
        
        // var client_html = "<div class='filter-search col-md-8'>  <form action='' method='post' accept-charset='utf-8' name='search_filter' id='search_filter'> <div class='row  row-no-gutters padding-b'> <div class='col-md-2'> <input type='text' name='search_name' id='search_name' placeholder='Name' class='input-large form-control' data-name='name'> </div> <div class='col-md-2'> <input type='text' name='search_email' id='search_email' placeholder='Email' data-name='email' class='form-control'> </div> <div class='col-md-2'> <input type='text' name='search_phone' id='search_phone' placeholder='Phone' data-name='phone' class='form-control'> </div> <div class='col-md-1'> <input type='text' name='date_from' id='date_from' placeholder='From' data-name='date_from' class='dpo'> </div> <div class='col-md-1'> <input type='text' name='date_to' id='date_to' placeholder='To' data-name='date_to' class='dpo'> </div> <div class='col-md-1'> <select class='form-control' name='status' id='status' data-name='status' style='width:100%!important;'> <option value=''>All Status</option> <option value='1'>Active</option> <option value='0'>Inactive</option> </select> </div> <div class='col-md-2'> <input type='submit' name='search' id='search' value='Search' class='btn btn-sm btn-default' /> </div> </div> </form> </div>";
        // $('.dt-buttons').before(client_html);
        // $('.buttons-excel').before('<a href="javascript:;" class="btn btn-sm btn-default" onclick="showAdd();"><i class="fa fa-plus"></i> Add</a> ');
        
        // Search form validation rules
        $("#search_filter").validate({
            rules: {
                search_name: { required:false }
            },
            messages: {
                search_name: {
                    required:"Please enter the name"
                }
            },
            submitHandler: function(form) {
                $.ajax({
                        url: 'clients/clientSearchData',
                        type: 'post',
                        data: $("#search_filter").serialize(), // /converting the form data into array and sending it to server
                        dataType: 'json',
                        success: function (response) {
                            console.log(response.data);
                            //  $("#manageTable tbody").empty();
                            //if (response.success === true) {
                            console.log("Table" + $.fn.dataTable.isDataTable('#manageDocumentsTable'));

                            if ($.fn.dataTable.isDataTable('#manageDocumentsTable') == false) {
                                $('#manageDocumentsTable').DataTable({
                                    'data': response.data,
                                    'order': [],
                                    'paging': true,
                                    'pageLength': 15,
                                    'columnDefs': [ { "targets": 0, "className": "text-center", "width": "4%" },
                                                    { "targets": 1, "className": "text-center", "width": "4%" },
                                                    { "targets": 2, "className": "text-center", "width": "4%" },
                                                    { "targets": 3, "className": "text-center", "width": "4%" },
                                                    { "targets": 4, "className": "text-center", "width": "10%" },
                                                    { "targets": 5, "className": "text-center", "width": "8%" },
                                                    { "targets": 6, "className": "text-center", "width": "4%" },
                                                    { "targets": 7, "className": "text-center", "width": "5%" },
                                                    { "targets": 8, "className": "text-center", "width": "5%" },
                                                    { "targets": 9, "className": "text-center", "width": "5%" },
                                                    { "targets": 10, "className": "text-center", "width": "8%" }
                                                  ]
                                });
                            } else {
                                var table = $('#manageDocumentsTable').DataTable();
                                table.destroy();
                                $('#manageDocumentsTable').DataTable({
                                    'data': response.data,
                                    'order': [],
                                    'paging': true,
                                    'pageLength': 15,
                                    'columnDefs': [ { "targets": 0, "className": "text-center", "width": "4%" },
                                                    { "targets": 1, "className": "text-center", "width": "4%" },
                                                    { "targets": 2, "className": "text-center", "width": "4%" },
                                                    { "targets": 3, "className": "text-center", "width": "4%" },
                                                    { "targets": 4, "className": "text-center", "width": "10%" },
                                                    { "targets": 5, "className": "text-center", "width": "8%" },
                                                    { "targets": 6, "className": "text-center", "width": "4%" },
                                                    { "targets": 7, "className": "text-center", "width": "5%" },
                                                    { "targets": 8, "className": "text-center", "width": "5%" },
                                                    { "targets": 9, "className": "text-center", "width": "5%" },
                                                    { "targets": 9, "className": "text-center", "width": "5%" },
                                                    { "targets": 10, "className": "text-center", "width": "8%" }
                                                  ]
                                });
                            }
                        }
                    })
            }
        });
    });

    $(document).ready(function() {

        $("#dcsa1").on('change', function(){

            var v = $("#dcsa1").val();
            // alert("working " + v);

            $.ajax({
                url: "documents/getUsers",
                type: 'get',
                data: {user_cat : v}, // /converting the form data into array and sending it to server
                success: function (response) {
                    if(response) {
                        $('#dcsa').find('option').remove()
                        $('#dcsa').append(response);
                    }
                    // alert("yes working " + response);
                }
            });
        });
    });
</script>