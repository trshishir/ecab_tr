                   
                      <div class="col-md-12" style="padding:0px;" >

                          <button class="btn payment-method-header-title" style="pointer-events:none;">
                          Payment Methode         
                          </button>        
                      </div>
                               
                              
                  <div class="col-md-12" style="padding:0px;">             
                            <ul role="tablist" id="paymentmethodsullist" class="nav nav-tabs on-bo-he  payement" style="width: 100%;">
                              <?php
                               $methodcount=1;
                               foreach ($paymentmethods as $key => $method):
                                 if($methodcount<6):
                              ?>

                            <li class="payment-method-buttons-list" style="padding: 0px !important;width: 20%;margin:0px !important;border:0px;">         
                            <input type="radio" name="payment_method"  value="<?= $method->id ?>" id="methodbtn<?= $methodcount ?>"  >
                            <button type="button" class="btn btn-default extra-pad payment-method-buttons-btn"><span style="font-size: 11.5px;"><?= $method->name ?></span>
                            </button>
                            </li>

                              <?php
                              $methodcount++;
                               endif;
                               endforeach;
                              ?>
                            </ul>
                   </div>
                            
                    <div class="col-md-12" id="paymentmethoddiv" >
                              <?php
                                   $paymentmethodcount=1;
                                   $cardcount=0;
                                   $cardtype="credit";
                                   foreach ($paymentmethods as $key => $method):
                                    if($paymentmethodcount<6):
                                  ?>
                                  <div class="payments" id="paymentmethod<?= $paymentmethodcount ?>" style="text-align: unset;">
                                <h4 style="font-size:18px;font-weight:900;"> Pay With <?= $method->name ?> </h4>
                                <?php if($method->category_client==1): ?>
                                  <?php  if($cardcount==0){  $cardtype="credit";}else{$cardtype="debit";} ?>
          
           <?php if($cardtype=='credit'): ?>
            <div class="card">              
                <div class="card-body bg-light">
                    <?php if (validation_errors()): ?>
                        <div class="alert alert-danger" role="alert">
                            <strong>Oops!</strong>
                            <?php echo validation_errors() ;?> 
                        </div>  
                    <?php endif ?>
                    <div id="payment-errors"></div>  
                       <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                        <div class="form-group cardform-control" >
                          <input type="text" list="cardnames" placeholder="Name on card"  name="name_<?= $cardtype ?>" id="name_<?= $cardtype ?>" autocomplete="off" value="">
                            <datalist id="cardnames">
                              <option value="Visa">
                              <option value="Mastercard">
                              <option value="Amex">
                              <option value="Unionpay">
                              <option value="Jcb">
                              <option value="Discover">
                              <option value="Diners">
                            </datalist>
                           
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                              
                             </small>
                        </div>  
                      </div>
                    </div>

                      
                      <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                         <div class="form-group cardform-control">
                         
                            <input type="text" name="card_num_<?= $cardtype ?>" id="card_num_<?= $cardtype ?>" class="form-control" placeholder="Card Number" autocomplete="off" value="" >
                             <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                   
                              </small>
                        </div>
                      </div>
                    </div>
                       
                        
                        <div class="row" style="padding-bottom: 20px;">

                            <div class="col-sm-8">
                                 <div class="row">
                                    <div class="col-sm-6">
                                  <div class="form-group cardform-control" >
                                        <select name="exp_month_<?= $cardtype ?>" id="exp_month_<?= $cardtype ?>">
                                          <option value="">Month</option>
                                          <?php for($i=1;$i<13;$i++){ ?>
                                             <option  value="<?= $i ?>"><?= ($i<10)?'0'.$i:$i; ?></option>
                                          <?php } ?>
                                        </select>
                                          
                                             <i class="fa fa-check-circle"></i>
                                              <i class="fa fa-exclamation-circle"></i>
                                              <small>
                                             
                                              </small>
                                          
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                    <div class="form-group cardform-control">
                                          <select name="exp_year_<?= $cardtype ?>" id="exp_year_<?= $cardtype ?>">
                                          <option value="">Year</option>
                                          <?php for($j=20;$j<36;$j++){ ?>
                                            <option  value="<?= '20'.$j ?>"><?= '20'.$j ?></option>
                                          <?php } ?>
                                          
                                        
                                        </select>
                                          
                                             <i class="fa fa-check-circle"></i>
                                              <i class="fa fa-exclamation-circle"></i>
                                              <small>
                                             
                                              </small>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group cardform-control">
                                    <input type="text" name="cvc_<?= $cardtype ?>" id="cvc_<?= $cardtype ?>" maxlength="4" class="form-control" autocomplete="off" placeholder="CVC" value="" >
                                     <i class="fa fa-check-circle"></i>
                                      <i class="fa fa-exclamation-circle"></i>
                                      <small>
                                      
                                      </small>
                                </div>
                            </div>
                        </div>                   
                   </div>
               </div>
               <?php else: ?>
             <div class="card">              
                <div class="card-body bg-light">
                    
                    <div id="payment-errors"></div>  
                                                           
                      <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-4">
                        <div class="form-group cardform-control">
                        <select class="form-control" name="civility_<?= $cardtype ?>" id="civility_<?= $cardtype ?>" style="height:40px;">
                                <option  value="Mr">Mr</option>
                                <option  value="Mrs">Mrs</option>
                                
                        </select>
                           
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                               
                              </small>
                        </div>  
                      </div>
                       <div class="col-md-4">
                        <div class="form-group cardform-control">

                            <input type="text" name="fname_<?= $cardtype ?>" id="fname_<?= $cardtype ?>" class="form-control" placeholder="First Name" value="" >
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                               
                              </small>
                        </div>  
                      </div>
                       <div class="col-md-4">
                        <div class="form-group cardform-control">

                            <input type="text" name="lname_<?= $cardtype ?>" id="lname_<?= $cardtype ?>" class="form-control" placeholder="Last Name" value="" >
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                               
                              </small>
                        </div>  
                      </div>
                    </div>
                      
                      <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                        <div class="form-group cardform-control">

                            <input type="text" name="bankname_<?= $cardtype ?>" id="bankname_<?= $cardtype ?>" class="form-control" placeholder="Bank Name" value="" >
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                               
                              </small>
                        </div>  
                      </div>
                    </div>
                    <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                        <div class="form-group cardform-control">

                            <input type="text" name="agencyname_<?= $cardtype ?>" id="agencyname_<?= $cardtype ?>" class="form-control" placeholder="Agency Name" value="" >
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                               
                              </small>
                        </div>  
                      </div>
                    </div>
                    <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                        <div class="form-group cardform-control">

                            <input type="text" name="agencyaddr_<?= $cardtype ?>" id="agencyaddr_<?= $cardtype ?>" class="form-control" placeholder="Agency Addresse" value="" >
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                               
                              </small>
                        </div>  
                      </div>
                    </div>
                     <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                        <div class="form-group cardform-control">

                            <input type="text" name="otheraddr_<?= $cardtype ?>" id="otheraddr_<?= $cardtype ?>" class="form-control" placeholder="Addresse" value="" >
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                              
                              </small>
                        </div>  
                      </div>
                    </div>
                    <div class="row" style="padding-bottom: 20px;">
                        
                       <div class="col-md-6">
                        <div class="form-group cardform-control">

                            <input type="text" name="agencyzipcode_<?= $cardtype ?>" id="agencyzipcode_<?= $cardtype ?>" class="form-control" placeholder="Zip Code" value="" >
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                                
                              </small>
                        </div>  
                      </div>
                       <div class="col-md-6">
                        <div class="form-group cardform-control">

                            <input type="text" name="agencycity_<?= $cardtype ?>" id="agencycity_<?= $cardtype ?>" class="form-control" placeholder="City" value="" >
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                                
                              </small>
                        </div>  
                      </div>
                    </div>
                    <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                        <div class="form-group cardform-control">

                            <input type="text" name="ibnnum_<?= $cardtype ?>" id="ibnnum_<?= $cardtype ?>" class="form-control" placeholder="IBAN Number" value="" >
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                               
                              </small>
                        </div>  
                      </div>
                    </div>
                    <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                        <div class="form-group cardform-control">

                            <input type="text" name="swiftcode_<?= $cardtype ?>" id="swiftcode_<?= $cardtype ?>" class="form-control" placeholder="Swift Code" value="" >
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                              
                              </small>
                        </div>  
                      </div>
                    </div>
                     <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                       <div class="col-md-1" style="padding:0px;width: 4% !important;">
                        <div class="form-group cardform-control">                       
                       <input class="agreement_check" id="agreement_<?= $cardtype ?>" type="checkbox" name="agreement_<?= $cardtype ?>" value="1" checked style="margin:-6px 0px;" >   

                             <small>
                            
                             </small>  
                      </div>
                       </div>  
                        <div class="col-md-11" style="padding:0px;width: 96% !important;">
                          <p style="font-size: 13px;">I allow company under to debit my account every time with amount of my bills.</p>
                        </div>
                           
                       
                      </div>
                    </div>
                     <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                        <p style="font-size:13px !important;"><?php echo strtoupper($companydata->name).' '.strtoupper($companydata->type).'<br>'.$companydata->street.'<br>'.$companydata->zip_code.' '.$companydata->city;?></p>
                           
                       
                      </div>
                    </div>
                   
                  
                   </div>
               </div>
               <?php endif; ?>
            <?php  $cardcount++; ?>
                                <?php else: ?>
                                  <p><?= $method->description ?></p>
                                <?php endif; ?>
                                                               
                            </div>
                                 <?php
                                  $paymentmethodcount++;
                                   endif;
                                   endforeach;
                                  ?>
                        </div>
                                    <!--<div class="down-btn">-->
                      
