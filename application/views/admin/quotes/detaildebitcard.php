             <div class="card">              
                <div class="card-body bg-light">
                    
                    <div id="payment-errors"></div>  
                                                           
                      <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-4">
                        <div class="form-group cardform-control">
                        <select class="form-control" name="civility_<?= $cardtype ?>" id="civility_<?= $cardtype ?>" style="height:40px;" style="pointer-events: none;">
                                <option <?php  if($dbdebitbankdata){ echo ($dbdebitbankdata->civility_debit=='Mr')?"selected":"";} ?> value="Mr">Mr</option>
                                <option <?php  if($dbdebitbankdata){ echo ($dbdebitbankdata->civility_debit=='Mrs')?"selected":"";} ?> value="Mrs">Mrs</option>
                                
                        </select>
                           
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                               
                              </small>
                        </div>  
                      </div>
                       <div class="col-md-4">
                        <div class="form-group cardform-control">

                            <input type="text" name="fname_<?= $cardtype ?>" id="fname_<?= $cardtype ?>" class="form-control" placeholder="First Name" style="pointer-events: none;" value="<?php if($dbdebitbankdata){echo $dbdebitbankdata->fname_debit;
                            } ?>" >
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                               
                              </small>
                        </div>  
                      </div>
                       <div class="col-md-4">
                        <div class="form-group cardform-control">

                            <input type="text" name="lname_<?= $cardtype ?>" id="lname_<?= $cardtype ?>" class="form-control" placeholder="Last Name" style="pointer-events: none;" value="<?php if($dbdebitbankdata){echo $dbdebitbankdata->lname_debit;
                            } ?>" >
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                               
                              </small>
                        </div>  
                      </div>
                    </div>
                      
                      <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                        <div class="form-group cardform-control">

                            <input type="text" name="bankname_<?= $cardtype ?>" id="bankname_<?= $cardtype ?>" class="form-control" placeholder="Bank Name" style="pointer-events: none;" value="<?php if($dbdebitbankdata){ echo $dbdebitbankdata->bankname_debit;
                            } ?>" >
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                               
                              </small>
                        </div>  
                      </div>
                    </div>
                    <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                        <div class="form-group cardform-control">

                            <input type="text" name="agencyname_<?= $cardtype ?>" id="agencyname_<?= $cardtype ?>" class="form-control" placeholder="Agency Name" style="pointer-events: none;" value="<?php
                           if($dbdebitbankdata){ echo $dbdebitbankdata->agencyname_debit;
                            } ?>" >
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                               
                              </small>
                        </div>  
                      </div>
                    </div>
                    <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                        <div class="form-group cardform-control">

                            <input type="text" name="agencyaddr_<?= $cardtype ?>" id="agencyaddr_<?= $cardtype ?>" class="form-control" placeholder="Agency Addresse" style="pointer-events: none;" value="<?php if($dbdebitbankdata){echo $dbdebitbankdata->agencyaddr_debit;
                            } ?>" >
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                               
                              </small>
                        </div>  
                      </div>
                    </div>
                     <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                        <div class="form-group cardform-control">

                            <input type="text" name="otheraddr_<?= $cardtype ?>" id="otheraddr_<?= $cardtype ?>" class="form-control" placeholder="Addresse" style="pointer-events: none;" value="<?php if($dbdebitbankdata){echo $dbdebitbankdata->otheraddr_debit;
                            } ?>" >
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                              
                              </small>
                        </div>  
                      </div>
                    </div>
                    <div class="row" style="padding-bottom: 20px;">
                        
                       <div class="col-md-6">
                        <div class="form-group cardform-control">

                            <input type="text" name="agencyzipcode_<?= $cardtype ?>" id="agencyzipcode_<?= $cardtype ?>" class="form-control" placeholder="Zip Code" style="pointer-events: none;" value="<?php if($dbdebitbankdata){echo $dbdebitbankdata->agencyzipcode_debit;
                            } ?>" >
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                                
                              </small>
                        </div>  
                      </div>
                       <div class="col-md-6">
                        <div class="form-group cardform-control">

                            <input type="text" name="agencycity_<?= $cardtype ?>" id="agencycity_<?= $cardtype ?>" class="form-control" placeholder="City" style="pointer-events: none;" value="<?php if($dbdebitbankdata){echo $dbdebitbankdata->agencycity_debit;
                            } ?>" >
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                                
                              </small>
                        </div>  
                      </div>
                    </div>
                    <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                        <div class="form-group cardform-control">

                            <input type="text" name="ibnnum_<?= $cardtype ?>" id="ibnnum_<?= $cardtype ?>" class="form-control" placeholder="IBAN Number" style="pointer-events: none;" value="<?php if($dbdebitbankdata){echo $dbdebitbankdata->ibnnum_debit;
                            } ?>" >
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                               
                              </small>
                        </div>  
                      </div>
                    </div>
                    <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                        <div class="form-group cardform-control">

                            <input type="text" name="swiftcode_<?= $cardtype ?>" id="swiftcode_<?= $cardtype ?>" class="form-control" placeholder="Swift Code" style="pointer-events: none;" value="<?php if($dbdebitbankdata){echo $dbdebitbankdata->swiftcode_debit;
                            } ?>" >
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                              
                              </small>
                        </div>  
                      </div>
                    </div>
                     <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                       <div class="col-md-1" style="padding:0px;width: 4% !important;">
                        <div class="form-group cardform-control">                       
                       <input class="agreement_check" id="agreement_<?= $cardtype ?>" type="checkbox" name="agreement_<?= $cardtype ?>" style="pointer-events: none;" value="1" checked style="margin:-6px 0px;" >   

                             <small>
                            
                             </small>  
                      </div>
                       </div>  
                        <div class="col-md-11" style="padding:0px;width: 96% !important;">
                          <p style="font-size: 13px;">I allow company under to debit my account every time with amount of my bills.</p>
                        </div>
                           
                       
                      </div>
                    </div>
                     <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                        <p style="font-size:13px !important;"><?php echo strtoupper($companydata->name).' '.strtoupper($companydata->type).'<br>'.$companydata->street.'<br>'.$companydata->zip_code.' '.$companydata->city;?></p>
                           
                       
                      </div>
                    </div>
                   
                  
                   </div>
               </div>