<?php    
$displaypassengers=$this->session->userdata['passengers'];
$client_id=0;
$pick_status="1";
$drop_status="1";
$servicecat_id=0;
$packagedepart_id=0;
$packagedestincation_id=0;
$pickairportpoiid=0;
$picktrainpoiid=0;
$pickparkpoiid=0;
$pickhotelpoiid=0;
$dropairportpoiid=0;
$droptrainpoiid=0;
$dropparkpoiid=0;
$drophotelpoiid=0;
     
   $pickairportpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$pickairportpoiid);
   $picktrainpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$picktrainpoiid);
   $pickhotelpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$pickhotelpoiid);
   $pickparkpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$pickparkpoiid);


   $dropairportpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$dropairportpoiid);
   $droptrainpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$droptrainpoiid);
   $drophotelpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$drophotelpoiid);
   $dropparkpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$dropparkpoiid);

 $attributes = array("name" => 'booking_form', "id" => 'booking_form');
  echo form_open('admin/quotes/booking_save',$attributes);
?>
 <div class="row booknow-content new-box-type" style="margin-top: 10px;">

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="width:100%;">
      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 booking_serivce_inner_tab" style="padding-left:0px;width: 19%;">
      <div class="form-group">
       
             <select name="booking_status"  class="servicecategoryinput" required>
                   <option value="">Statut</option>
                  
                   <option <?= ($dbbookingdata->is_conformed=='0' ? 'selected' :''); ?> value="0">Pending</option>
                   <option <?= ($dbbookingdata->is_conformed=='1' ? 'selected' :''); ?> value="1">Confirmed</option>
                   <option <?= ($dbbookingdata->is_conformed=='2' ? 'selected' :''); ?> value="2">Cancelled</option>
                 
             </select>
                  
              <?php if(isset($service_form['driver']['error']) && !empty($service_form['driver']['error'])): ?>  
                  <div class='invalid-error'><?= $service_form['driver']['error']?></div>
              <?php endif; ?>
                  
        </div>
    </div>
  
  
  <!--client passenger driver cars -->
   
      

    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 booking_serivce_inner_tab" style="padding-left:0px;width: 19%;">
      <div class="form-group">
       
             <select name="client_field"  class="servicecategoryinput" id="userisclient"  onchange="getpassengers()" required>
                   <option value="">&#xf007; Clients</option>
                  <?php foreach($clients_data as $key => $item): ?>
                   <option <?php  if(isset($service_form['client']['value']) && !empty($service_form['client']['value'])){ echo ($service_form['client']['value']==$item->id)?"selected":"";} ?> value="<?= $item->id ?>">&#xf007; <?= $item->civility.' '.$item->first_name.' '.$item->last_name; ?></option>
                   <?php endforeach; ?>
             </select>
                  
              <?php if(isset($service_form['client']['error']) && !empty($service_form['client']['error'])): ?>  
                  <div class='invalid-error'><?= $service_form['client']['error']?></div>
              <?php endif; ?>
                  
        </div>
    </div> 
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-10 booking_serivce_inner_tab" style="padding-left:0px;width: 19%;">
      <div class="form-group">
       
             <select name="passenger"  class="servicecategoryinput" id="passengerselectdiv">
                   <option value="">&#xf007; Passengers</option>
                   <?php
                       $passengers_data=$this->bookings_model->booking_passengers('vbs_passengers',$client_id);
                      foreach($passengers_data as $key => $item): 
                     ?>
                   <option  value="<?= $item->id ?>">&#xf007; <?= $item->civility.' '.$item->fname.' '.$item->lname; ?></option>
                   <?php endforeach; ?>
             </select>
                  
              <?php if(isset($service_form['passengers']['error']) && !empty($service_form['passengers']['error'])): ?>  
                  <div class='invalid-error'><?= $service_form['passengers']['error']?></div>
              <?php endif; ?>
                  
        </div>
    </div> 
    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-2" style="margin-top: 10px;width: 5%;padding-left:0px;">
     <button type="button" class="plusgreeniconconfig"  onclick="addpassengers()"><i class="fa fa-plus"></i></button>
    </div> 
  </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xs-12" style="padding:0px;">
         <div class="col-md-12" style="margin-left: 37.5%;width: 50.5%;">
           <ul class="ulbdnone" id="passengerdiv" style="list-style-type:none;padding-left:0px;">
            
              <?php foreach($displaypassengers as $passenger): ?>
               <li class="text-left" style="position: relative;"><span class="fa fa-user" style="margin-right:5px;"></span><span><?php echo $passenger['name'];?></span>, <span class="fa fa-mobile" style="margin-right:5px;"></span><span><?php echo $passenger['phone'];?></span>, <span class="fa fa-phone" style="margin-right:5px;"></span><span><?php echo $passenger['home'];?></span>, <span class="fa fa-wheelchair" style="margin-right:5px;"></span><span><?= $passenger['disable'] ?></span>
               
                     <button type="button" class="minusredicon" onclick="removepassengers(<?= $passenger['id'] ?>)"><i class="fa fa-minus"></i></button>
              </li>

              <?php endforeach; ?>    
               
           </ul>

        </div>
      </div>

  <!--client passenger driver cars -->
  <div class="col-md-12" style="margin-bottom: 10px;">
    <div class="col-lg-5 col-md-12 col-sm-12" style="padding:0px;width: 46%;">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 booking_serivce_inner_tab" style="padding-left:0px;">
      <div class="form-group">
         <div class="input-group">
            <span class="input-group-addon">
            <i class="fa fa-car"></i>
            </span>
        <select name="pickservicecategory"  class="servicecategoryinput" id="pickservicecategoryinput" onchange="addservice()" required>
                      <option value="">Service Category</option>
                                <?php
                                  foreach($service_cat as $key => $item){   
                                 ?>
                                   <option <?php  if(isset($service_form['service_category']['value']) && !empty($service_form['service_category']['value'])){ echo ($service_form['service_category']['value']==$item->id)?"selected":"";} ?> value="<?= $item->id ?>"><?= $item->category_name ?></option>
                                   <?php 
                                   
                                      }    
                                     ?>
                  </select>
                   </div>
                   <?php
                     if(isset($service_form['service_category']['error']) && !empty($service_form['service_category']['error'])){     
                          echo "<div class='invalid-error'>".$service_form['service_category']['error']."</div>";
                            }
                      ?>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 booking_serivce_inner_tab" style="padding-right:0px;">
      <div class="form-group">
           <div class="input-group">
                <span class="input-group-addon">
                <i class="fa fa-car"></i>
                </span>
          <select name="pickservice" class="pickserviceinput" id="pickserviceinput" required>
                      <option value="">Service</option>
                               <?php
                                     $service=$this->bookings_model->get_services('vbs_u_service',['service_category' => $servicecat_id]);
                                   foreach($service as $key => $item){
                                 
                                 ?>
                                   <option  value="<?= $item->id ?>"><?= $item->service_name ?></option>
                                   <?php 
                                   
                                      }    
                                     ?>
                  </select>
                </div>
                   <?php
                     if(isset($service_form['service']['error']) && !empty($service_form['service']['error'])){     
                          echo "<div class='invalid-error'>".$service_form['service']['error']."</div>";
                            }
                      ?>
                    </div>
    </div>
    </div>
  </div>
  <div class="col-lg-12 col-md-12 col-sm-12" style="width: 100%;">
    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12  booking_tab_size" style="padding: 0px;width: 46%;">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
        <div class="booking-tab">
           <input type="hidden"  id="pickupcategory" name="pickupcategory" value="address">
         
        
          <ul class="nav nav-tabs">
           
             <li><button class="btn btn-default extra-pad booking" style="font-weight:900;pointer-events: none;padding-bottom: 11px;border-radius:0px;width:100%; background-image: linear-gradient(rgb(255, 255, 255) 0px, rgb(224, 224, 224) 100%) !important;color: #616161 !important;"> <?php echo 'Pickup'; ?></button></li>
              <li <?=$pick_status=="1"? 'class="active"':''?> style="width:14.5%;"><a id="pickaddresscategorybtn" data-toggle="tab" href="#pick_address"><i class="fa fa-map-marker"></i> <?php echo 'Address'; ?></a></li>
             <li <?=$pick_status=="2"? 'class="active"':''?> style="width:16%;"><a id="pickpackagecategorybtn" data-toggle="tab" href="#pick_package"><i class="fa fa-suitcase"></i> <?php echo 'Package'; ?></a></li>
             <li <?=$pick_status=="3"? 'class="active"':''?> style="width:15.5%;"><a id="pickairportcategorybtn" data-toggle="tab" href="#pick_airport"><i class="fa fa-plane"></i> <?php echo 'Airport'; ?></a></li>
             <li <?=$pick_status=="4"? 'class="active"':''?>><a id="picktraincategorybtn" data-toggle="tab" href="#pick_train"><i class="fa fa-train"></i> <?php echo 'Train'; ?></a></li>
             <li <?=$pick_status=="5"? 'class="active"':''?>><a id="pickhotelcategorybtn" data-toggle="tab" href="#pick_hotel"><i class="fa fa-hotel"></i> <?php echo 'Hotel'; ?></a></li>
             <li <?=$pick_status=="6"? 'class="active"':''?>><a id="pickparkcategorybtn" data-toggle="tab" href="#pick_park"><i class="fa fa-tree"></i> <?php echo 'Park'; ?></a></li>
             
         </ul>
        </div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
        <div class="tab-content">

            <div id="pick_address" class="tab-pane fade  <?=$pick_status== '1'? 'in active':''?>">
               <input type="hidden" id="pickloc_lat" />
               <input type="hidden" id="pickloc_long" />
                <div class="row">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                   <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;margin:8px 0px 0px -5px;"></i>
                 </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                   <div class="form-group edit-cardform-control">
                      <div class="input-group" style="position: relative;">
                        <span class="input-group-addon">
                        <i class="fa fa-map-marker"></i>
                        </span>
                        
      <input type="hidden" name="pickaddressfield" placeholder="Addresse line"  class="pick_up_txtbox_bkg" id="pickupaddress" value="" >
                        

                         <input type="text" placeholder="Addresse line"  class="pick_up_txtbox_bkg" id="pickupautocomplete"   onFocus="geolocate()"
                          value="<?php echo $pickshortaddress  ?>"  style="padding-right: 30px;">
                        <div id="currentuserpoibtn" onclick="getCurrentLocation()">
                          <img src="<?= base_url()?>assets/theme/default/images/target-a.svg">
                        </div>

                       
                      <div id="currentuserpoidiv">
                      <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker" style="top: 4px;"></i>
                         <input type="text" readonly id="currentuserpoiinput" onclick="setPickCurrentLocation()">
                          
                         
                      </div> 
                   </div>
                      </div>
                      <div id="locationloaderdiv">
                        <img src="<?= base_url()?>assets/theme/default/images/loader_2.gif" >
                      </div>
                         
                      </div>
                       
                           <small>Required</small>
                   </div>
                 </div>
               </div>

              <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text" name="pickotheraddressfield"  placeholder="Complément d'adresse"  class="pick_up_txtbox_bkg" value="" style="padding-left:43px !important;">                         
                      </div>                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px 5px 0px 0px;">
                          <div class="form-group edit-cardform-control">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                               <input type="text" name="pickzipcodefield" placeholder="Zip code"  class="pick_up_txtbox_bkg" id="pickup_code" value="">
                                
                            </div>
                            
                                  <small>Required</small>
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px 0px 0px 5px;">
                              <div class="form-group edit-cardform-control">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                   <input type="text" name="pickcityfield" placeholder="City"  class="pick_up_txtbox_bkg" id="pickup_city" value="">
                                         
                                    </div>
                                          <small>Required</small>
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
            </div>
          <div id="pick_package" class="tab-pane fade <?=$pick_status== '2'? 'in active':''?>">
              <div class="row">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                     <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                  </div>
                  <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-suitcase"></i>
                        </span>
                        
                     <select name="pickpackagefield" id="pickpackagefield"  class="packagebox_pick_bkg " onchange="addpoidata()">
                       <option value="">Select</option>
                 
                       <?php
                        foreach($poi_package as $package){
                         ?>
                       <option value="<?= $package->id ?>"><?= $package->name ?></option>
                       <?php  }  ?>
                      </select>
                    </div>
                               
                                  <small>Required</small>
                                </div>
                  </div>
              </div> 
              <?php
               $packagedata=$this->bookings_model->poidatarecord('vbs_u_package',$packagedepart_id);
               $packagepoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$packagedata['departure']);
               $poicategoryrecord=$this->bookings_model->poidatarecord('vbs_u_ride_category',$packagepoirecord['category_id']);
               ?>
                <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-bullseye"></i>
                              </span>
                             <input type="text"   id="packagedepartcategoryname" value="<?= $poicategoryrecord['ride_cat_name']  ?>" disabled style="background-color: #f6f6f6;" >
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-bullseye"></i>
                                      </span>
                                       <input type="text"   id="packagedepartname" value="<?= $packagepoirecord['name']  ?>" disabled style="background-color: #f6f6f6;" >
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="packagedepartaddress" placeholder="Addresse" value="<?= $packagepoirecord['address']  ?>" disabled style="background-color: #f6f6f6;" >
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
               <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="packagedepartzipcode" placeholder="Zip code" value="<?= $packagepoirecord['postal']  ?>" disabled style="background-color: #f6f6f6;" >
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="packagedepartcity" placeholder="City" value="<?= $packagepoirecord['ville']  ?>" disabled style="background-color: #f6f6f6;" >
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
             
          </div>


          <div id="pick_airport" class="tab-pane fade <?=$pick_status== '3'? 'in active':''?>">
               <div class="row">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                      <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                  </div>
                  <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                     <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-plane"></i>
                        </span>
                      <select name="pickairportfield" id="airport_pick"  class="airlocation_pick_bkg " onchange="getpickcategorydata(this)">
                       <option value="">Select</option>
                 
                       <?php
                        foreach($poi_data as $airport){ 
                       $category=$this->bookings_model->getcategory('vbs_u_ride_category',$airport->category_id);
                                 
                          if(strtolower($category)=="airport"){
                         ?>
                       <option value="<?= $airport->id ?>"><?= $airport->name ?></option>
                       <?php 
                             }
                              }    
                             ?>
                      </select>
                    </div>
                               
                                  <small>Required</small>
                     </div>             
                  </div>
               </div>
               <div class="row" style="margin-top:5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2"></div>
                  <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small  edit-cardform-control" style="padding: 0px;padding-right:5px;">
                      <input type="text"  name="pickairportflightnumberfield" placeholder="Flight No" class="pickup_flight_no_bkg" 
                      id="pickup_flight_no" value="">
                            
                                  <small style="margin-left:0px !important;">Required</small> 
                                 
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small col-arrival" style="padding: 0px;padding-left:5px;">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding:0px;padding-top: 9px;">
                      <label for="timepicker_pick_fly">Arrival Time</label>
                    </div>
                     <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                         <input name="pickairportarrivaltimefield" type="text" id="timepicker_pick_fly"  value="<?php echo date('h : i'); ?>"  class="pickup_flight_time_bkg" />
                          
                      </div>
                  </div>
                </div>
                </div>
               </div>
                <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="pickairportaddress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $pickairportpoirecord['address'] ?>" >
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="pickairportzipcode" placeholder="Zip code" disabled style="background-color: #f6f6f6;"  value="<?= $pickairportpoirecord['postal'] ?>">
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="pickairportcity" placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $pickairportpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>

          </div>

          <div id="pick_train" class="tab-pane fade <?=$pick_status== '4'? 'in active':''?>">
                  <div class="row">
                      <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                          <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                      </div>
                      <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                      <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-train"></i>
                        </span>
                          <select name="picktrainfield" class="trainlocation_pick_bkg " id="train_pick"  onchange="getpickcategorydata(this)">
                            <option value="">Select</option>
                              <?php
                                foreach($poi_data as $train){
                                   $category=$this->bookings_model->getcategory('vbs_u_ride_category',$train->category_id);
                                 
                                  if(strtolower($category)=="train"){
                                 ?>
                                   <option  value="<?= $train->id ?>"><?= $train->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                          </select>
                        </div>
                          
                                   <small>Required</small>
                          </div>        
                      </div>
                  </div>
                  <div class="row" style="margin-top:5px;">
                    <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2"></div>
                     <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small  edit-cardform-control" style="padding: 0px;padding-right:5px;">
                            <input id="pickup_train_no" class="pickup_train_no_bkg" type="text"  name="picktrainnumberfield" placeholder="Train No" value="">
                             
                                      <small style="margin-left:0px !important;">Required</small> 
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small col-arrival " style="padding: 0px;padding-left:5px;">
                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding:0px;padding-top: 9px;">
                            <label for="timepicker_pick_stn">Arrival Time</label>
                          </div>
                           <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                             <input  class="pickup_flight_time_bkg" id="timepicker_pick_stn" name="picktrainarrivaltimefield" type="text" value="<?php echo date('h : i'); ?>" />
                          
                            </div> 
                          
                             
                        </div>
                      </div>
                    </div>
                  </div>

               <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="picktrainaddress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $picktrainpoirecord['address'] ?>">
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="picktrainzipcode" placeholder="Zip code"  disabled style="background-color: #f6f6f6;" value="<?= $picktrainpoirecord['postal'] ?>" >
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="picktraincity" placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $picktrainpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
          </div>


          <div id="pick_hotel" class="tab-pane fade <?=$pick_status== '5'? 'in active':''?>">
           <div class="row">
              <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                  <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
              </div>
              <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                 <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-hotel"></i>
                        </span>
                  <select name="pickhotelfield" class="hotellocation_pick_bkg" id="hotel_pick"  onchange="getpickcategorydata(this)">
                      <option value="">Select</option>
                      <?php
                                foreach($poi_data as $hotel){
                                   $category=$this->bookings_model->getcategory('vbs_u_ride_category',$hotel->category_id);

                                  if(strtolower($category)=="hotel"){
                                 ?>
                                   <option  value="<?= $hotel->id ?>"><?= $hotel->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                  </select>
                </div>
                  
                                  <small>Required</small> 
              </div>
            </div>
            </div>
             <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="pickhoteladdress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $pickhotelpoirecord['address'] ?>">
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="pickhotelzipcode" placeholder="Zip code"  disabled style="background-color: #f6f6f6;" value="<?= $pickhotelpoirecord['postal'] ?>">
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="pickhotelcity"  placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $pickhotelpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
          </div>


          <div id="pick_park" class="tab-pane fade <?=$pick_status== '6'? 'in active':''?>">
           <div class="row">
              <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                  <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i> 
              </div>
              <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                  <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-tree"></i>
                        </span>
                  <select name="pickparkfield" class="parklocation_pick_bkg " id="park_pick"  onchange="getpickcategorydata(this)">
                   <option value="">Select</option>
                   <?php
                                foreach($poi_data as $park){
                                  $category=$this->bookings_model->getcategory('vbs_u_ride_category',$park->category_id);
                                 
                          if(strtolower($category)=="park"){
                                 ?>
                                   <option  value="<?= $park->id ?>"><?= $park->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                 </select>
               </div>
                   <small>Required</small> 
                </div>
              </div>
              </div>
               <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="pickparkaddress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $pickparkpoirecord['address'] ?>">
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="pickparkzipcode"  placeholder="Zip code" disabled style="background-color: #f6f6f6;" value="<?= $pickparkpoirecord['postal'] ?>">
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="pickparkcity" placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $pickparkpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
          </div>
       </div>
      </div>
       
    </div>

    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 booking_tab_size booking_icon_tab"> <img src="<?= base_url()?>assets/theme/default/images/carloader2.gif" width="160" height="160" style="right:-2px !important;"> </div>
    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 booking_tab_size " style="padding:0px;width: 46%;">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
          <div class="booking-tab">
             <input type="hidden" value="address" id="dropoffcategory" name="dropoffcategory">
            <ul class="nav nav-tabs">

             <li><button class="btn btn-default extra-pad" style="font-weight:900;pointer-events: none;padding-bottom: 11px;border-radius:0px;width:100%;background-image: linear-gradient(rgb(255, 255, 255) 0px, rgb(224, 224, 224) 100%) !important;color: #616161 !important;"><?php echo 'Dropoff'; ?></button></li>
              <li class="active" style="width:14.5%;"><a id="dropaddresscategorybtn" data-toggle="tab" href="#drop_address"><i class="fa fa-map-marker"></i> <?php echo 'Address'; ?></a></li>
             <li style="width:16%;"><a id="droppackagecategorybtn" data-toggle="tab" href="#drop_package"><i class="fa fa-suitcase"></i> <?php echo 'Package'; ?></a></li>

             <li style="width:15.5%;"><a id="dropairportcategorybtn" data-toggle="tab" href="#drop_airport"><i class="fa fa-plane"></i> <?php echo 'Airport'; ?></a></li>
             <li><a id="droptraincategorybtn" data-toggle="tab" href="#drop_train"><i class="fa fa-train"></i> <?php echo 'Train'; ?></a></li>
             <li><a id="drophotelcategorybtn" data-toggle="tab" href="#drop_hotel"><i class="fa fa-hotel"></i> <?php echo 'Hotel'; ?></a></li>
             <li><a id="dropparkcategorybtn" data-toggle="tab" href="#drop_park"><i class="fa fa-tree"></i> <?php echo 'Park'; ?></a></li>
           </ul>
          </div>
      </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
     <div class="tab-content">
        <div id="drop_address" class="tab-pane fade  <?=$drop_status== '1'? 'in active':''?>">
              <input type="hidden" id="droploc_lat" />
               <input type="hidden" id="droploc_long" />
               <div class="row">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                   <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;margin:8px 0px 0px -5px;"></i>
                 </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                   <div class="form-group edit-cardform-control">
                      <div class="input-group" style="position: relative;">
                        <span class="input-group-addon">
                        <i class="fa fa-map-marker"></i>
                        </span>
                         <input type="hidden" name="dropaddressfield" placeholder="Addresse line"  class="pick_up_txtbox_bkg" id="dropoffaddress" value="">

                              <input type="text"  placeholder="Addresse line"  class="pick_up_txtbox_bkg" id="dropoffautocomplete" onFocus="geolocate()"  value="" style="padding-right: 30px;">

                        <div id="dropcurrentuserpoibtn" onclick="getDropCurrentLocation()">
                           <img src="<?= base_url()?>assets/theme/default/images/target-a.svg">
                        </div>
                       <div id="dropcurrentuserpoidiv">
                        <div class="form-group">
                        <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker" style="top: 4px;"></i>
                         <input type="text" readonly id="dropcurrentuserpoiinput" onclick="setDropCurrentLocation()">
                          
                         
                          </div> 
                        </div>
                      </div>
                       <div id="droplocationloaderdiv">
                        <img src="<?= base_url()?>assets/theme/default/images/loader_2.gif" >
                      </div>
                      </div>

                          <small>Required</small> 
                   </div>
                 </div>
               </div>

              <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                     <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                        <input type="text" name="dropotheraddressfield" placeholder="Complément d'adresse"  class="pick_up_txtbox_bkg" style="padding-left:43px !important;" value="">

                      </div>
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small col-zip-code" style="padding: 0px 5px 0px 0px;">
                          <div class="form-group edit-cardform-control">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                               <input type="text" name="dropzipcodefield" placeholder="Zip code"  class="pick_up_txtbox_bkg" id="dropoff_code" value="">
                            </div>
                          
                          <small>Required</small> 
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small col-city" style="padding: 0px 0px 0px 5px;">
                              <div class="form-group edit-cardform-control">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text" name="dropcityfield" placeholder="City"  class="pick_up_txtbox_bkg" id="dropoff_city"   value="">
                                    </div>
                                        <small>Required</small> 
                               </div>
                             </div>
                           </div>
                         </div>
                       </div>
                      </div>
        <div id="drop_package" class="tab-pane fade  <?=$drop_status== '2'? 'in active':''?>">
            <div class="row">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                   <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-suitcase"></i>
                        </span>
                  <select name="droppackagefield" id="droppackagefield" class="packagebox_pick_bkg " onchange="adddroppoidata()">
                       <option value="">Select</option>
                 
                       <?php
                        foreach($poi_package as $package){
                         ?>
                       <option  value="<?= $package->id ?>"><?= $package->name ?></option>
                       <?php  }  ?>
                      </select>
                    </div>
                  <small>Required</small> 
                </div>
              </div>
            </div>  
             <?php
               $packagedata=$this->bookings_model->poidatarecord('vbs_u_package',$packagedestincation_id);
               $droppoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$packagedata['destination']);
               $poicategoryrecord=$this->bookings_model->poidatarecord('vbs_u_ride_category',$droppoirecord['category_id']);
               ?>
                  <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-bullseye"></i>
                              </span>
                             <input type="text"   id="packagedestinationcategoryname" value="<?= $poicategoryrecord['ride_cat_name']  ?>" disabled style="background-color: #f6f6f6;" >
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-bullseye"></i>
                                      </span>
                                       <input type="text"   id="packagedestinationname" value="<?= $droppoirecord['name']  ?>" disabled style="background-color: #f6f6f6;" >
                                         
                                    </div>                                   
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
              <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text" placeholder="Addresse"   id="packagedestinationaddress" value="<?= $droppoirecord['address']  ?>" disabled style="background-color: #f6f6f6;">
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
           
                   <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text" placeholder="Zip code"   id="packagedestinationzipcode" value="<?= $droppoirecord['postal']  ?>" disabled style="background-color: #f6f6f6;" >
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text" placeholder="City"  id="packagedestinationcity" value="<?= $droppoirecord['ville']  ?>" disabled style="background-color: #f6f6f6;" >
                                         
                                    </div>                                   
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
        <div id="drop_airport" class="tab-pane fade  <?=$drop_status== '3'? 'in active':''?>">
                   <div class="row">
                      <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                          <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                      </div>
                      <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                         <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-plane"></i>
                        </span>
                          <select name="dropairportfield" id="airport_drop"  class="airlocation_pick_bkg " onchange="getdropcategorydata(this)">
                           <option value="">Select</option>
                            <?php
                                foreach($poi_data as $airport){
                                   $category=$this->bookings_model->getcategory('vbs_u_ride_category',$airport->category_id);
                                  if(strtolower($category)=="airport"){
                                 ?>
                                   <option  value="<?= $airport->id ?>"><?= $airport->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                          </select>
                        </div>                          
                      <small>Required</small> 
                    </div>
                  </div>
                  </div>
                  <div class="row" style="margin-top:5px;">
                      <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2"></div>
                        <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small  edit-cardform-control" style="padding: 0px;padding-right:5px;">
                          <input type="text" name="dropairportflightnumberfield" placeholder="Flight No" class="pickup_flight_no_bkg " id="dropof_flight_no" value="">
                         
                          <small style="margin-left:0px !important;">Required</small> 
                          
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small col-arrival" style="padding: 0px;padding-left:5px;">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding:0px;padding-top: 9px;">
                          <label for="timepicker_drop_fly_1">Arrival Time</label>
                        </div>
                         <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                         <input  class="pickup_flight_time_bkg" id="timepicker_drop_fly_1" name="dropairportarrivaltimefield" type="text" value="<?php echo date('h : i'); ?>" />
                      
                       </div>
                      </div>
                     </div>
                    </div>
                  </div>

                   <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="dropairportaddress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $dropairportpoirecord['address'] ?>" >
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="dropairportzipcode" placeholder="Zip code"  disabled style="background-color: #f6f6f6;"  value="<?= $dropairportpoirecord['postal'] ?>">
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="dropairportcity" placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $dropairportpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>




              
        </div>
        <div id="drop_train" class="tab-pane fade  <?=$drop_status== '4'? 'in active':''?>">
                     <div class="row">
                        <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                            <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                        </div>
                        <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                           <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-train"></i>
                        </span>
                            <select name="droptrainfield" id="train_drop" class="trainlocation_pick_bkg" onchange="getdropcategorydata(this)">
                              <option value="">Select</option>
                                <?php
                                foreach($poi_data as $train){
                                   $category=$this->bookings_model->getcategory('vbs_u_ride_category',$train->category_id);
                                  if(strtolower($category)=="train"){
                                 ?>
                                   <option value="<?= $train->id ?>"><?= $train->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                          </select>
                        </div>
                           
                          <small>Required</small> 
                      </div>
                    </div>
                    </div>
                    <div class="row" style="margin-top:5px;">
                       <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2"></div>
                      <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">                      
                        <div class="ccol-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small  edit-cardform-control" style="padding: 0px;padding-right:5px;">
                            <input  class="pickup_train_no_bkg" type="text"  name="droptrainnumberfield" placeholder="Train No"  value="">
                            
                          <small style="margin-left:0px !important;">Required</small> 
                         
                        </div>
                        <div class="ccol-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small col-arrival" style="padding: 0px;padding-left:5px;">
                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding:0px;padding-top: 9px;">
                              <label for="pickup_train_time_bkg">Arrival Time</label>
                            </div>
                          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                            <input style="width:100% !important;" class="pickup_train_time_bkg" id="timepicker_drop_stn" name="droptrainarrivaltimefield" type="text" value="<?php echo date('h : i'); ?>" />
                             
                          </div>
                        </div>
                    </div>
                  </div>
                </div>

                     <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="droptrainaddress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $droptrainpoirecord['address'] ?>">
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="droptrainzipcode" placeholder="Zip code"  disabled style="background-color: #f6f6f6;" value="<?= $droptrainpoirecord['postal'] ?>" >
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="droptraincity" placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $droptrainpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
        </div>
        <div id="drop_hotel" class="tab-pane fade  <?=$drop_status== '5'? 'in active':''?>">
         <div class="row">
            <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
            </div>
            <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
               <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-hotel"></i>
                        </span>
                <select name="drophotelfield" class="hotellocation_pick_bkg" id="hotel_drop" onchange="getdropcategorydata(this)">
                    <option value="">Select</option>
                    <?php
                foreach($poi_data as $hotel){
                  $category=$this->bookings_model->getcategory('vbs_u_ride_category',$hotel->category_id);
                  if(strtolower($category)=="hotel"){
                 ?>
                   <option value="<?= $hotel->id ?>"><?= $hotel->name ?></option>
                   <?php 
                     }
                      }    
                     ?>
                </select>
              </div>
                  
                           <small>Required</small> 
            </div>
          </div>
         </div>
           <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="drophoteladdress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $drophotelpoirecord['address'] ?>">
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="drophotelzipcode" placeholder="Zip code"  disabled style="background-color: #f6f6f6;" value="<?= $drophotelpoirecord['postal'] ?>">
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="drophotelcity" placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $drophotelpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
        </div>
        <div id="drop_park" class="tab-pane fade  <?=$drop_status== '6'? 'in active':''?>">
             <div class="row">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                    <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                   <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-tree"></i>
                        </span>
                    <select name="dropparkfield" class="parklocation_pick_bkg" id="park_drop"  onchange="getdropcategorydata(this)">
                 <option value="">Select</option>
                   <?php
                                foreach($poi_data as $park){
                                   $category=$this->bookings_model->getcategory('vbs_u_ride_category',$park->category_id);
                                  if(strtolower($category)=="park"){
                                 ?>
                              <option value="<?= $park->id ?>"><?= $park->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                 </select>
               </div>
                
                           <small>Required</small> 
                </div>
              </div>
            </div>
             <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="dropparkaddress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $dropparkpoirecord['address'] ?>">
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="dropparkzipcode" placeholder="Zip code"  disabled style="background-color: #f6f6f6;" value="<?= $dropparkpoirecord['postal'] ?>">
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="dropparkcity" placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $dropparkpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>

        </div>



    </div>
  </div>
 
</div>

   
  

  
    </div>
  
</div>
 <div class="row" style="margin-top: 10px;margin-bottom:10px;">
     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="stopaddressdiv" style="width:46%;padding-right:0px;">
       
     </div>
   </div>
    <div class="date-time row" style="margin-top: 10px;margin-bottom:10px;">
     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="width: 46%;padding-right:0px;">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;" >
         <div id="return_chairwheel" >
                    <div  style="float:left; margin-right:20px;">
                      <div class="squaredFour">
                        <span>Reccurent </span><input class="regular_check" id="regularcheckfieldid" type="checkbox" name="regularcheckfield" value="1" style="margin:0px 10px;float:none;">
                          <label for="regularcheckfieldid"></label>
                      </div>
                    </div>

                    <div style="float:left; margin-right:20px;">
                         <div class="squaredFour">
                        <span>Return </span><input class="return_car" type="checkbox" id="returncheckfieldid" name="returncheckfield" value="1" style="margin:0px 10px;float:none;">
                           <label for="returncheckfieldid"></label>
                      </div>
                    </div>
                     <div style="float:left; margin-right:20px;">
                         <div class="squaredFour">
                        <span>Wheelchair </span><input class="wheelchair_carssh" type="checkbox" id="wheelcarcheckfieldid" name="wheelcarcheckfield" value="1" style="margin:0px 10px;float:none;">
                           <label for="wheelcarcheckfieldid"></label>
                      </div>
                    </div>
                    <div style="float:right;">
                        
                        <span>Add Extra Stop </span> <button type="button" onclick="addstopboxes();" class="plusgreeniconconfig" style="margin: 0px 0px 0px 10px!important;"><i class="fa fa-plus" style="margin:0px !important;"></i></button>
                           
                     
                    </div>
                    <div class="clearfix"></div>
             </div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px;padding-top:20px;">
        <div class="pickupreturn_datetime_div">
              <div id="pickup_datetime_div" >
                <div class="pickdt-bkg" style="text-align:center;"> 
                    <label class="pickup_date pickup_dt_label" style="margin:0px !important;"> <?php echo 'Pickup Date'; ?> </label>
                    
                    <input id="dtpicker" class="pickup-dt-bkg bdatepicker"  type="text" value="<?php echo date('d/m/Y'); echo set_value('pick_date'); ?>" name="pickdatefield" placeholder="<?php echo 'Pickup Date'; ?>"  />
                </div>
                <div class="picktime-bkg" style="text-align:center;"> 
                   <label class="pickup_time pickup_time_label" style="margin:0px !important;"><?php echo 'Pickup Time'; ?></label>
                    
                    <input   id="timepicker1" name="picktimefield" type="text" value="<?php echo date("h : i"); ?>" style="width:100%" />
                </div>
              </div>
          
              <div class="return_datetime_div" style="display: none;">

                <div class="picktime-bkg" style="text-align:center;"> 
                   <label class="pickup_time pickup_time_label" style="margin:0px !important;"><?php echo 'Waiting Time'; ?></label>
                  
                    
                    <input   id="waitingtimepicker" name="waitingtimefield" type="text" value="<?php echo date('00 : 00') ?>" style="width:100%" />
                </div>

                <div class="dropdt-bkg" style="text-align:center;">
                     <label style="margin:0px !important;" class="dropof_date dropof_dt_label"> <?php echo 'Return Date'; ?> </label>
                    

                   
                    <input id="dtpicker1 " class="bdatepicker"  type="text" value="<?php echo date('d/m/Y'); echo set_value('drop_date'); ?>" name="returndatefield" placeholder="<?php echo 'Drop Date'; ?>"  />
                </div>

                <div class="droptime-bkg" style="text-align:center;">
                    <label class="dropof_time dropof_time_label" style="margin:0px !important;"><?php echo 'Return Time'; ?></label>
                   
            
                    <input   id="timepicker2" name="returntimefield" type="text" value="<?php echo date('h : i') ?>" style="width:100%;margin-top: 0px !important;" />
                </div>
              </div> 
        </div>
        
          <div class="startend_datetime_div" style="display: none;">

             <div class="pickdt-bkg" style="text-align:center;"> 
                    <label class="pickup_date start_dt_label" style="margin:0px !important;"> <?php echo 'Start Date'; ?> </label>
                 
                  <input  class="pickup-dt-bkg bdatepicker"  type="text" value="<?php echo date('d/m/Y'); echo set_value('pick_date'); ?>" name="startdatefield" placeholder="<?php echo 'Start Date'; ?>"  />
              </div>
              <div class="picktime-bkg" style="text-align:center;"> 
               
                  <label class="pickup_time start_time_label" style="margin:0px !important;"><?php echo 'Start Time'; ?></label>
                  
                  <input   id="starttimepicker" name="starttimefield" type="text" value="<?php echo date("h : i"); ?>" style="width:100%" />
              </div>

              <div class="dropdt-bkg" style="text-align:center;">       
                  <label style="margin:0px !important;" class="dropof_date endof_dt_label"> <?php echo 'End Date'; ?> </label>
           
                  <input class="bdatepicker"  type="text" value="<?php echo date('d/m/Y'); echo set_value('drop_date'); ?>" name="enddatefield" placeholder="<?php echo 'End Date'; ?>"  />
              </div>

              <div class="droptime-bkg" style="text-align:center;">
      
                  <label class="dropof_time endof_time_label" style="margin:0px !important;"><?php echo 'End Time'; ?></label>
          
                  <input   id="endtimepicker" name="endtimefield" type="text" value="<?php echo date('h : i') ?>" style="width:100%;margin-top: 0px !important;" />
              </div>
          </div>
      </div>
    
     </div> 
     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 booking_comment_tab_size">
          <div class="form-group">
            <label style="font-weight: 900;margin-bottom: 10px;">Comment :</label>
       <textarea class="form-control" rows="3" name="booking_comment" id="commentbox" placeholder="Write a comment here..."><?php  if(isset($service_form['comment']['value']) && !empty($service_form['comment']['value'])){ echo $service_form['comment']['value']; } ?></textarea>
    <?php
       if(isset($service_form['comment']['error']) && !empty($service_form['comment']['error'])){     
            echo "<div class='invalid-error'>".$service_form['comment']['error']."</div>";
              }
           ?>
         </div>
     </div>    
    </div>
  <div class="row" id="timeslotsrtn" style="display: none;" >
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  
      <table cellpadding="0" cellspacing="0" 
      style="width:100%;" align="center" id="timesspantable">
        <tr style="text-align:left;" id="returnpicktimetable">
          <td>&nbsp;</td>
          <td><?php echo 'Monday'; ?></td>
          <td><?php echo 'Tuesday'; ?></td>
          <td><?php echo 'Wednesday'; ?></td>
          <td><?php echo 'Thursday'; ?></td>
          <td><?php echo 'Friday'; ?></td>
          <td><?php echo 'Saturday'; ?></td>
          <td><?php echo 'Sunday'; ?></td>
        </tr>
        <tr class="go-table" style="text-align:left;">
          <td><?php echo 'Pickup Time'; ?></td>
          <td class="weekdays_none_floating">
            <input name="pickmondaycheckfield" class="weekdays_go" type="checkbox" value="1">
            <input  class="wkdays_time_pick " id="go_time_1" name="pickmondayfield" type="text" value="<?php echo date('h : i'); ?>" style="float:none !important;" />
          </td>
          <td class="weekdays_none_floating"><input name="picktuesdaycheckfield" class="weekdays_go" type="checkbox" value="2">
            <input  class="wkdays_time_pick " id="go_time_2" name="picktuesdayfield" type="text" value="<?php echo date('h : i'); ?>" style="float:none !important;"/>
          </td>
          <td class="weekdays_none_floating"><input name="pickwednesdaycheckfield" class="weekdays_go" type="checkbox" value="3">
            <input  class="wkdays_time_pick " id="go_time_3" name="pickwednesdayfield" type="text" value="<?php echo date('h : i'); ?>" style="float:none !important;" />
          </td class="weekdays_none_floating">
          <td class="weekdays_none_floating"><input name="pickthursdaycheckfield" class="weekdays_go" type="checkbox" value="4">
            <input  class="wkdays_time_pick " id="go_time_4" name="pickthursdayfield" type="text" value="<?php echo date('h : i'); ?>"style="float:none !important;" />
          </td>
          <td class="weekdays_none_floating"><input name="pickfridaycheckfield" class="weekdays_go" type="checkbox" value="5">
            <input  class="wkdays_time_pick " id="go_time_5" name="pickfridayfield" type="text" value="<?php echo date('h : i'); ?>" style="float:none !important;" />
          </td>
          <td class="weekdays_none_floating"><input name="picksaturdaycheckfield" class="weekdays_go" type="checkbox" value="6">
            <input  class="wkdays_time_pick " id="go_time_6" name="picksaturdayfield" type="text" value="<?php echo date('h : i'); ?>"style="float:none !important;" />
          </td>
          <td class="weekdays_none_floating"><input name="picksundaycheckfield" class="weekdays_go" type="checkbox" value="7">
            <input  class="wkdays_time_pick " id="go_time_7" name="picksundayfield" type="text" value="<?php echo date('h : i'); ?>"style="float:none !important;" />
          </td>
        </tr>
        <tr class="back-table" style="text-align:left;">
          <td style="width:10%;text-align: left;"><?php echo 'Return Time'; ?></td>
          <td class="weekdays_none_floating">
            <input name="returnmondaycheckfield" class="weekdays_back" type="checkbox" value="1">
            <input  class="wkdays_time_pick " id="back_time_1" name="returnmondayfield" type="text" value="<?php echo date('h : i'); ?>" style="float: none !important; "/>
          </td>
          <td class="weekdays_none_floating">
            <input name="returntuesdaycheckfield" class="weekdays_back" type="checkbox" value="2">
            <input  class="wkdays_time_pick " id="back_time_2" name="returntuesdayfield" type="text" value="<?php echo date('h : i'); ?>" style="float:none !important;" />
          </td>
          <td class="weekdays_none_floating">
            <input name="returnwednesdaycheckfield" class="weekdays_back" type="checkbox" value="3">
            <input  class="wkdays_time_pick " id="back_time_3" name="returnwednesdayfield" type="text" value="<?php echo date('h : i'); ?>" style="float:none !important;"/>
          </td>
          <td class="weekdays_none_floating">
            <input name="returnthursdaycheckfield" class="weekdays_back" type="checkbox" value="4">
            <input  class="wkdays_time_pick " id="back_time_4" name="returnthursdayfield" type="text" value="<?php echo date('h : i'); ?>" style="float:none !important;"/>
          </td>
          <td class="weekdays_none_floating">
            <input name="returnfridaycheckfield" class="weekdays_back" type="checkbox" value="5">
            <input  class="wkdays_time_pick " id="back_time_5" name="returnfridayfield" type="text" value="<?php echo date('h : i'); ?>" style="float:none !important;"/>
          </td>
          <td class="weekdays_none_floating">
            <input name="returnsaturdaycheckfield" class="weekdays_back" type="checkbox" value="6">
            <input  class="wkdays_time_pick " id="back_time_6" name="returnsaturdayfield" type="text" value="<?php echo date('h : i'); ?>" style="float:none !important;"/>
          </td>
          <td class="weekdays_none_floating">
            <input name="returnsundaycheckfield" class="weekdays_back" type="checkbox" value="7">
            <input  class="wkdays_time_pick " id="back_time_7" name="returnsundayfield" type="text" value="<?php echo date('h : i'); ?>" style="float:none !important;"/>
          </td>
        </tr>
      </table>
    </div>
  </div>
  



     <?php  include 'wheelchairdiv.php';?> 
              

          <div class="row submit-row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="position: relative;">
              <div style="float:right;">
                <button type="submit"  class="btn btn-default  btn-xs right btn-booking-next" style="margin-right: -13px !important;font-weight:normal;text-transform:none !important;float: left !important;">NEXT <i class="fa fa-arrow-circle-o-right"></i> 
               </button>
               <div id="loaderdivaddbooking" style="float: left;margin-right: -13px;margin-left: 15px;display: none;"><img src="<?= base_url()?>assets/theme/default/images/bookloader.gif" style="width: 40px;height: 40px;"></div>
             </div>
            </div>
        </div>
     <?php echo form_close(); ?>    