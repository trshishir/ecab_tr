<?php $locale_info = localeconv(); ?>
<style>
/* .table-filter .dpo {
        max-width: 62px;
    }
  input[type="file"]{
    border: 1px solid #ccc;
    width: 100%;
    width: 200px;
    padding: 3px 0px 3px 5px; */
/* background: #fff; */
/* background: #fffdfd;
    } */
/* input[type="file"]:hover{
            box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2), 0 0 3px rgba(0, 0, 0, 0.2);
    }
    .table-filter span {
        margin: 4px 2px 0 3px;
    }
    .table-filter input[type="number"]{
        max-width: 48px;
    }
    @media only screen and (min-width: 1400px){
        .table-filter input, .table-filter select{
            max-width: 6% !important;
        }
        .table-filter select{
            max-width: 85px !important;
        }
        .table-filter .dpo {
            max-width: 90px !important;
        }
    } */
.nav-tabs>li.active {
    background: linear-gradient(#ffffff, #ffffff 25%, #d0d0d0) !important;
    /* border-bottom: none; */
}

.nav-tabs>li {
    background-image: linear-gradient(to bottom, #fff 0, #e0e0e0 100%);
    border-left: 1px solid #ccc !important;
    height: 55px;
    margin: 0px !important;
}

.nav-tabs>li>a {

    color: #616161 !important;
    font-size: 12px;
    padding-left: 10px;
    display: block !important;
    width: 100% !important;
    height: 100% !important;
    text-transform: uppercase;
    font-weight: bold;
    transition: 0.2s;
    outline: none;
    border: none;
    border-radius: 0px;
}

.dataTables_wrapper .dataTables_filter {
    float: left;
    text-align: left;
}

}

/*
       #seracTab {
           background: linear-gradient(to bottom, #ffffff 0%, #f6f6f6 47%, #ddd 100%);
           height: 45px;
           margin: 0px 0px 20px 0px;
           border-radius: 4px;
           border: solid 1px #efefef;
           padding: 4px;
           text-align: center;
       }
       thead{
           background: transparent;
           border-bottom: 1px solid #111111;
           font-family: inherit;
           font-size: 100%;
           font-style: inherit;
           font-weight: inherit;
           line-height: 100%;
           vertical-align: baseline;
       }
       th{
           text-shadow: 0 -1px 0 #ffffff;
       } */
.tab-pane {
    padding: 30px 30px 30px 14px;
}

.configTable {
    padding: 0px;
}

.dataTables_wrapper .dataTables_filter input {
    margin-right: 2px;
    margin-left: 0 !important;
    max-width: 100% !important;
    width: 100% !important;
    float: right;
}

.dataTables_wrapper {
    margin-top: 20px;
}

.nav-tabs li a {
    color: #616161 !important;
    font-size: 11px;
    margin: 0px !important;
    padding: 15px;
    display: block !important;
    width: 100% !important;
    height: 100% !important;
    text-transform: uppercase;
    font-weight: bold;
    transition: 0.2s;
    outline: none;
}

.nav-tabs {
    border-bottom: none;
}
.nav-tabs li a:hover,
.nav-tabs li a:focus {
    /*background: linear-gradient(to bottom, #ececec 0%, #ececec 39%, #f0efef 39%, #b7b3b3 100%) !important;
        background:linear-gradient(to bottom,  #c1c1c1 0%, #ececec 39%, #ececec 39%, #fbfbfb 100%) !important;*/
    background: linear-gradient(to bottom, #c1c1c1 4%, #ececec 30%, #ececec 50%, #b7b3b3 100%) !important;
    color: #616161 !important;
    cursor: pointer;
}

.nav-tabs li.active a {
    /*background:linear-gradient(to bottom,  #c1c1c1 0%, #ececec 39%, #ececec 39%, #fbfbfb 100%) !important;*/
    background: linear-gradient(to bottom, #c1c1c1 4%, #ececec 30%, #ececec 50%, #b7b3b3 100%) !important;
}

</style>

<style>
.tab-content .btn,
button.btn {
    min-height: 28px;
    border: 1px solid #bbb;
    min-width: 80px;
    background-image: linear-gradient(to bottom, #fff 0, #e0e0e0 100%);
    background-repeat: repeat-x;
    border-color: #dbdbdb;
    text-shadow: 0 1px 0 #fff;
    border-color: #ccc;
}

.delete-icon {
    background: url(<?=base_url();?>/assets/delete-icon.png) no-repeat left center !important;
    padding: 0px 0px 0px 22px;
}

.save-icon {
    background: url(<?=base_url();?>/assets/save-icon.png) no-repeat left center !important;
    padding: 0px 0px 0px 22px;
}

input.btn {
    min-height: 28px;
    border: 1px solid #bbb;
    min-width: 80px;
}

form>fieldset>section {
    padding-top: 5px;
    padding-bottom: 5px;
    width: 48%;
    float: left;
}

form>fieldset>section input,
form>fieldset>section select {
    width: 70%;
    margin: 0 auto;
    display: inline-block;
    height: 40px;
}

#form-ttl {
    font-size: 18px;
    text-align: center;
    padding-top: 10px;
}

.form-btm {
    float: right;
    width: 100%;
    margin-top: 10px;
}

.listDiv {
    min-height: 700px;
}

.dataTable td {
    min-width: 90px;
}

.ms-options-wrap button {
    width: 100% !important;
    border-color: #fff #fff #fff #fff;
    border-color: #bfbfbf;
    text-shadow: 0 0px 0 rgba(255, 255, 255, 0.7);
    color: #555555;
    background-color: #fff;
    -webkit-box-shadow: inset 0 0px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
    -moz-box-shadow: inset 0 0px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
    box-shadow: inset 0 0px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
    background: transparent linear-gradient(#ffffff, #ffffff, #ffffff) repeat scroll 0% 0%;
    background-color: transparent;
    background-image: #ffffff;
    height: 34px;
    font-size: 17px;
}

.ms-options ul {
    margin-left: -16px;
    background: #fff !important;
}

input:hover,
input:focus,
textarea:hover,
textarea:focus {
    -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2), 0 0 3px rgba(0, 0, 0, 0.2);
    -moz-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2), 0 0 3px rgba(0, 0, 0, 0.2);
    box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2), 0 0 3px rgba(0, 0, 0, 0.2);
}

input,
textarea,
input:invalid,
input:required,
textarea:required {
    -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2);
    -moz-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2);
    box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2);
}

html,
body,
textarea,
input {
    color: #6f6f6f;
}

input,
textarea {
    background-color: #ffffff;
    border-color: #bbbbbb;
}

input,
textarea {
    width: 99%;
    border: 1px solid #cccccc;
    padding: 4px 2px;
    margin: 0 1px;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
}

#DataTables_Table_0_filter {
    margin-left: 3px;
}
</style>


<?php $active_status = $this->session->flashdata('alert'); ?>
<?php if (!isset($active_status['price_stu_id'])) {
    $active_status['price_stu_id'] = '';
} ?>
<?php if (!isset($active_status['status_id'])) {
    $active_status['status_id'] = '';
} ?>
<?php if ($active_status['status_id'] != '') {
    $status = $active_status['status_id'];
    $tabstatus = $active_status['status_id'];
} else {
    $status = '1';
    $tabstatus = '1';
} ?>
<?php if ($active_status['price_stu_id'] != '') {
    $price_stu = $active_status['price_stu_id'];
} else {
    $price_stu = '1';
} ?>
<section id="content">
    <div class="listDiv">
        <!--col-md-10 padding white right-p-->
        <?php $this->load->view('admin/common/breadcrumbs'); ?>
        <?php $this->load->view('admin/common/alert'); ?>
        <?php echo $this->session->flashdata('message'); ?>
        <div class="row-fluid" id="ShowTabs" style="margin-bottom: 10px; margin-left:14px;    padding-right: 20px;">

            <div class="tab-content responsive" style="width:100%;display: table;">
                <!--status tab starts-->

                <div class="row">
                    <div class="ListdriverProfile">
                        <input type="hidden" class="chk-AddDriverProfile-btn" value="">

                        <div class="col-md-12" style="padding:0px">
                            <div class="module-body table-responsive">
                                <table
                                    class="configTable table table-bordered table-striped  table-hover dataTable no-footer"
                                    cellspacing="0" width="100%" data-selected_id="">
                                    <thead>
                                        <tr>
                                            <th class="no-sort text-center" style="width:2% !important;">#</th>
                                            <th class="text-center" style="width:3% !important;">
                                                <?php echo $this->lang->line('id'); ?></th>
                                            <th class="text-center" style="width:6% !important;">
                                                <?php echo $this->lang->line('date'); ?></th>
                                           
                                            <th class="text-center" style="width:6% !important;">
                                                <?php echo $this->lang->line('added_by'); ?></th>
                                            <th class="column-first_name text-center" style="width:6% !important;">Driver Name
                                                </th>
                                            <th class="column-first_name text-center" style="width:6% !important;">
                                                Address</th>
                                            <th class="column-first_name text-center" style="width:6% !important;">
                                                Postal Code</th>
                                                <th class="column-first_name text-center" style="width:6% !important;">
                                                City</th>
                                                <th class="column-first_name text-center" style="width:6% !important;">
                                                Contract Nature </th>
                                                <th class="column-first_name text-center" style="width:6% !important;">
                                                Contract Type </th>
                                                <th class="column-first_name text-center" style="width:6% !important;">
                                                Monthly Hours </th>
                                            <th class="column-first_name text-center" style="width:6% !important;">
                                                <?php echo $this->lang->line('status'); ?></th>
                                            <th class="text-center" style="width:10%;">Since</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $cnt = 1; ?>
                                        <?php if (!empty($driver_status_data)): ?>
                                        <?php foreach ($driver_status_data as $key => $item): ?>
                                        <tr>
                                            <td class="text-center">
                                                <input type="checkbox" class="chk-mainvat-template"
                                                    data-input="<?= $item->id ?>">
                                            </td>
                                            <td class="text-center"><a href="javascript:void()"
                                                    onclick="driverProfileidEdit('<?= $item->id ?>')"><?= date("dmY", strtotime($item->created_at)) . '0000' . $item->id; ?></a>
                                            </td>
                                            <td class="text-center"><?= from_unix_date($item->created_at) ?></td>
                                            </td>
                                            <td class="text-center"><?= $this->drivers_model->getuser($item->userid);?>
                                            </td>
                                            <td class="text-center"><?= $item->civilite .' '. $item->nom .' '. $item->prenom;  ?></td>
                                           
                                            <td class="text-center"><?= $item->address; ?></td>
                                            <td class="text-center"><?= $item->postalcode; ?></td>
                                            <td class="text-center"><?= $item->ville; ?></td>
                                            <td class="text-center"><?= $item->natureducontrat; ?></td>
                                            <td class="text-center"><?= $item->typecontrat; ?></td>
                                            <td class="text-center"><?= $item->nombredheuremensuel; ?></td>
                                            <td class="text-center">
                                                <?=$item->status=='EN CONGE'?'<span class="label label-success">'.$item->status.'</span>':'<span class="label label-info">'.$item->status.'</span>';?>
                                            </td>
                                            <td class="text-center"><?= timeDiff($item->created_at) ?></td>

                                        </tr>
                                        <?php $cnt++; ?>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                                <br>
                            </div>
                        </div>
                    </div>

                    <div class="driverProfileEdit" style="display:none">
                        <ul class="nav nav-tabs responsive">
                            <li <?= $status == 1 ? 'class="active"' : '' ?>><a href="#PROFILE" class="profile_E"
                                    role="tab" data-toggle="tab">PROFILE</a>
                            </li>
                            <li <?= $status == 2 ? 'class="active"' : '' ?>><a href="#DRIVERS" class="drivers_E"
                                    role="tab" data-toggle="tab">DRIVERS</a>
                            </li>
                            <li <?= $status == 3 ? 'class="active"' : '' ?>><a href="#CARS" class="cars_E" role="tab"
                                    data-toggle="tab">CARS</a></li>
                            <li <?= $status == 4 ? 'class="active"' : '' ?>><a href="#DOCUMENTS" role="tab"
                                    class="documents_E" data-toggle="tab">DOCUMENTS</a>
                            </li>
                            <li <?= $status == 5 ? 'class="active"' : '' ?>><a href="#RESOURCES" class="resources_E"
                                    role="tab" data-toggle="tab">RESOURCES</a></li>
                            <li <?= $status == 6 ? 'class="active"' : '' ?>><a href="#BOOKINGS" class="bookings_E"
                                    role="tab" data-toggle="tab">BOOKINGS</a>
                            </li>
                            <li <?= $status == 7 ? 'class="active"' : '' ?>><a href="#TIMESHEET" class="timesheet_E"
                                    role="tab" data-toggle="tab">TIMESHEET</a></li>
                            <li <?= $status == 8 ? 'class="active"' : '' ?>><a href="#AVAILABILITY"
                                    class="availability_E" role="tab" data-toggle="tab">AVAILABILITY</a></li>

                            <li <?= $status == 9 ? 'class="active"' : '' ?>><a href="#REQUESTS" class="requests_E"
                                    role="tab" data-toggle="tab">REQUESTS</a>
                            </li>

                            <li <?= $status == 10 ? 'class="active"' : '' ?>><a href="#INFRACTIONS" class="infactions_E"
                                    role="tab" data-toggle="tab">INFRACTIONS</a></li>

                            <li <?= $status == 11 ? 'class="active"' : '' ?>><a href="#PAYMENTS" class="payments_E"
                                    role="tab" data-toggle="tab">PAYMENTS</a></li>

                            <li <?= $status == 12 ? 'class="active"' : '' ?>><a href="#SUPPORT" class="support_E"
                                    role="tab" data-toggle="tab">SUPPORT</a>
                            </li>

                            <li <?= $status == 13 ? 'class="active"' : '' ?>><a href="#MESSAGES" class="messages_E"
                                    role="tab" data-toggle="tab">MESSAGES</a></li>
                        </ul>
                        <div class="tab-pane fade <?= $status == 1 ? 'in active' : '' ?>"
                            style="padding: 10px; padding-top:0px;" id="PROFILE">
                            <span class="driver_profile">
                                <?= form_open("admin/driver_info/driverProfileEdit") ?>

                                <div class="driverProfileEditajax">
                                </div>
                                <div class="row" style="margin-top:5px;">
                                    <button class="btn" style=" float:right; margin-left:7px;"><span
                                            class="save-icon"></span> Update
                                    </button>
                                    <a class="btn" style="float:right; margin-left:7px;" href="javascript:void()"
                                        onclick="cancelDriverProfile()"><span class="delete-icon"> Cancel </span></a>

                                </div>
                                <?php echo form_close(); ?>
                            </span>
                        </div>
                        <div class="tab-pane fade <?= $status == '2' ? 'in active' : '' ?>" id="DRIVERS">
                            <?php echo "<span class='DRIVERS'>DRIVERS</span>" ?>
                        </div>
                        <div class="tab-pane fade <?= $status == '3' ? 'in active' : '' ?>" id="CARS">
                            <?php echo "<span class='CARS'>CARS</span>" ?>
                        </div>

                        <div class="tab-pane fade <?= $status == '4' ? 'in active' : '' ?>" id="DOCUMENTS">
                            <?php include('document.php'); ?>
                        </div>
                        <div class="tab-pane fade <?= $status == '5' ? 'in active' : '' ?>" id="RESOURCES">
                            <?php include('resources.php'); ?>
                        </div>
                        <div class="tab-pane fade <?= $status == '6' ? 'in active' : '' ?>" id="BOOKINGS">
                            <?php echo "<span class='BOOKINGS'>BOOKINGS</span>" ?>
                        </div>
                        <div class="tab-pane fade <?= $status == '7' ? 'in active' : '' ?>" id="TIMESHEET">
                            <?php echo "<span class='TIMESHEET'>TIMESHEET</span>" ?>
                        </div>
                        <div class="tab-pane fade <?= $status == '8' ? 'in active' : '' ?>" id="AVAILABILITY">
                            <?php echo "<span class='AVAILABILITY'>AVAILABILITY</span>" ?>
                        </div>
                        <div class="tab-pane fade <?= $status == '9' ? 'in active' : '' ?>" id="REQUESTS">
                            <?php echo "<span class='REQUESTS'>REQUESTS</span>" ?>
                        </div>
                        <div class="tab-pane fade <?= $status == '10' ? 'in active' : '' ?>" id="INFRACTIONS">
                            <?php echo "<span class='INFRACTIONS'>INFRACTIONS</span>" ?>
                        </div>
                        <div class="tab-pane fade <?= $status == '11' ? 'in active' : '' ?>" id="PAYMENTS">
                            <?php echo "<span class='PAYMENTS'>PAYMENTS</span>" ?>
                        </div>
                        <div class="tab-pane fade <?= $status == '12' ? 'in active' : '' ?>" id="SUPPORT">
                            <?php echo "<span class='SUPPORT'>SUPPORT</span>" ?>
                        </div>
                        <div class="tab-pane fade <?= $status == '13' ? 'in active' : '' ?>" id="MESSAGES">
                            <?php echo "<span class='MESSAGES'>MESSAGES</span>" ?>
                        </div>
                    </div>
                    <!-- <form action="<?= base_url(); ?>admin/driver_info/vatadd" class="" method="post"  enctype="multipart/form-data"> -->
                    <div class="driverProfileAdd" style="display: none;">




                        <?= form_open_multipart("admin/driver_info/driverProfileadd", array("id" => "driver_addform")) ?>
                        <div class="row">
                        <div class="col-md-6">

                            <div class="row" style="margin-top: 10px;">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 10px;">
                                            <span style="font-weight: bold;">Statut </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <select class="form-control" name="status">
                                                <option value="DISPONIBLE">DISPONIBLE</option>
                                                <option value="EN CONGE">EN CONGE</option>
                                                <option value="EN ARRET MALADIE">EN ARRET MALADIE</option>
                                                <option value="ABSENT">ABSENT</option>
                                                <option value="INACTIF">INACTIF</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 10px;">
                                            <span style="font-weight: bold;">Civilite</span>
                                        </div>
                                        <div class="col-md-3" style="padding: 0px;">
                                            <select class="form-control" name="civilite">
                                                <option value="Mr">Mr</option>
                                                <option value="Miss">Miss</option>
                                                <option value="Mme">Mme</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px;">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 10px;">
                                            <span style="font-weight: bold;">Nom</span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="nom" class="form-control" type="text">
                                        </div>

                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 10px;">
                                            <span style="font-weight: bold;">Prenom</span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="prenom" class="form-control" type="text">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 10px;">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 10px;">
                                            <span style="font-weight: bold;">Address</span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="address" class="form-control" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 10px;">
                                            <span style="font-weight: bold;">Address</span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="address" class="form-control" type="text">
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="row" style="margin-top: 10px;">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-4" style="padding: 0px;margin-top: 10px;">
                                            <span style="font-weight: bold;">Code Postal</span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="postalcode" class="form-control" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 10px;">
                                            <span style="font-weight: bold;">Ville</span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="ville" class="form-control" type="text">
                                        </div>
                                    </div>
                                </div>

                               
                            </div>

                        </div>

                        <div class="col-md-6" style="padding: 0px;">
                            <div class="row" style="margin-top: 10px;">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 10px;">
                                            <span style="font-weight: bold;">Driver Image</span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input class="driver_image" name="driverImg" type="file">
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6" style="text-align: center;">
                                    <img id="preview_driver" alt="Preview Logo"
                                        src="<?= base_url() ?>/assets/images/no-preview.jpg"
                                        style="border: 1px solid;cursor: pointer;height: 210px;width: 210px;position: relative;z-index: 10;">
                                </div>



                            </div>
                        </div>
                        </div>

                        <div style="clear: both"></div>

                        <div class="row" style="margin-top: 10px;">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: bold;">Date de Naissance</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input name="dateenaissance" required="" class="form-control datepicker"
                                            type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: bold;">Ville de Naissance</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input name="villedenaissance" required="" class="form-control" type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: bold;">Pays de Naissance</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input name="paysdenaissance" class="form-control" type="text">
                                    </div>

                                </div>
                            </div>

                        </div>


                        <div class="row" style="margin-top: 10px;">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 10px;">
                                        <span style="font-weight: bold;">Poste </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <select class="form-control" name="poste">
                                            <option value="Chauffeur Accompagnateur">Chauffeur Accompagnateur</option>
                                            <option value="Chauffeur">Chauffeur</option>
                                            <option value="Secretaire">Secretaire</option>
                                            <option value="Comptable">Comptable</option>
                                            <option value="Regulateur">Regulateur</option>
                                            <option value="Commercial">Commercial</option>
                                            <option value="Responsable Parc">Responsable Parc</option>
                                            <option value="Auxiliaire de vie">Auxiliaire de vie</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 10px;">
                                        <span style="font-weight: bold;">Date Dentree</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="datedentree" class="form-control datepicker"
                                            type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: bold;">Date de Sortie</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="datedesortie" class="form-control datepicker"
                                            type="text">
                                    </div>

                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 10px;">
                                        <span style="font-weight: bold;">Motif</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <select class="form-control" name="motif">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row" style="margin-top: 10px;">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 10px;">
                                        <span style="font-weight: bold;">Availability Contrat</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <select class="form-control" name="typecontrat">
                                            <option value="CDI">CDI</option>
                                            <option value="CDD">CDD</option>
                                            <option value="SAISONNIERE">SAISONNIERE</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: bold;">Nature du Contrat</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <select class="form-control" name="natureducontrat">
                                            <option value="Temps Plein">Temps Plein</option>
                                            <option value="Temps Partiel">Temps Partiel</option>
                                            <option value="Scolaire">Scolaire</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;padding-right: 0px;">
                                        <span style="font-weight: bold;">Nombre Dheure Mensuel</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <select class="form-control" name="nombredheuremensuel">
                                            <option value="104">104</option>
                                            <option value="65">65</option>
                                            <option value="151,67">151,67</option>
                                        </select>
                                    </div>

                                </div>
                            </div>

                        </div>


                        <div class="row" style="margin-top: 10px;">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: bold;">Availability de Piece didentite</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <select class="form-control" name="typedepiecedidentite">
                                            <option value="CIN">CIN</option>
                                            <option value="Titre de sejour">Titre de sejour</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 10px;">
                                        <span style="font-weight: bold;">Upload</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="upload1" type="file">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;padding-right: 0px;">
                                        <span style="font-weight: bold;">Numerro Piece Didentite</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="numerropiecedidentite" class="form-control"
                                            type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: bold;">Date dexpiration</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="datedexpiration" class="form-control datepicker"
                                            type="text">
                                    </div>

                                </div>
                            </div>

                        </div>


                        <div class="row" style="margin-top: 10px;">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: bold;">Numero Permis</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="pumeropermis" class="form-control" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 10px;">
                                        <span style="font-weight: bold;">Upload</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="Upload2" type="file">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: bold;">Date Delivrance</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="datedelivrance1" class="form-control datepicker"
                                            type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: bold;">Date Dexpiration</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="datedexpiration2" class="form-control datepicker"
                                            type="text">
                                    </div>

                                </div>
                            </div>

                        </div>


                        <div class="row" style="margin-top: 10px;">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: bold;">Certificat Medicate</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="certificatmedicate" type="file">
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: bold;">Date Delivrance</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="datedelivrance2" class="form-control datepicker"
                                            type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: bold;">Date Dexpiration</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="datedexpiration3" class="form-control datepicker"
                                            type="text">
                                    </div>

                                </div>
                            </div>

                        </div>


                        <div class="row" style="margin-top: 10px;">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 10px;">
                                        <span style="font-weight: bold;">PSC1</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="PSC1" type="file">
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: bold;">Date Delivrance</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="datedelivrance3" class="form-control datepicker"
                                            type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: bold;">Date Dexpiration</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="datedexpiration4" class="form-control datepicker"
                                            type="text">
                                    </div>

                                </div>
                            </div>

                        </div>


                        <div class="row" style="margin-top: 10px;">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: bold;">Medecine de Travai</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="medecinedetravai" type="file">
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: bold;">Date Delivrance</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="datedelivrance4" class="form-control datepicker"
                                            type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: bold;">Date Dexpiration</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="datedexpiration5" class="form-control  datepicker"
                                            type="text">
                                    </div>

                                </div>
                            </div>

                        </div>

                        <div class="row" style="margin-top: 10px;">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: bold;">Autre Diplome1</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="autrdeiplome1" type="file">
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: bold;">Autre Diplome2</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="autrediplome2" type="file">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: bold;">Autre Diplome3</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="autrediplome3" type="file">
                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="row"></div>
                        <div class="row" style="margin-top: 10px; margin-right: 3px;">

                            <button class="btn" style="float:right; margin-left:7px;"><span class="save-icon"></span>
                                Save </button>
                            <a class="btn" style="float:right; margin-left:7px;" href="javascript:void()"
                                onclick="cancelDriverProfile()"><span class="delete-icon"> Cancel </span></a>
                        </div>
                        <?php echo form_close(); ?>




                        <div class="col-md-12 driverProfileDelete"
                            style="border:1px solid #ccc;padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
                            <?= form_open("admin/driver_info/deleteDriverProfile") ?>
                            <input name="tablename" value="vbs_job_statut" type="hidden">
                            <input type="hidden" id="driverProfileid" name="delet_driver_status_id" value="">
                            <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10px;"> Are you
                                sure you want to delete selected?
                            </div>
                            <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
                            <button class="btn" style=" float:left;margin-right: 10px;"><span class="save-icon"></span>
                                Yes
                            </button>

                            <a class="btn" style="cursor: pointer;" onclick="cancelDriverProfile()"><span
                                    class="delete-icon">No</span> </a>
                            <?php echo form_close(); ?>
                        </div>
                    </div>

                </div>


            </div>
            <!--/.module-->
        </div>

    </div>
</section>
<script>
(function($, W, D) {
    var JQUERY4U = {};

    JQUERY4U.UTIL = {
        setupFormValidation: function() {
            //Additional Methods
            $.validator.addMethod("lettersonly", function(a, b) {
                return this.optional(b) || /^[a-z ]+$/i.test(a)
            }, "<?php echo $this->lang->line('valid_name'); ?>");

            $.validator.addMethod("phoneNumber", function(uid, element) {
                return (this.optional(element) || uid.match(/^([0-9]*)$/));
            }, "<?php echo $this->lang->line('valid_phone_number'); ?>");


            $.validator.addMethod("pwdmatch", function(repwd, element) {
                var pwd = $('#password').val();
                return (this.optional(element) || repwd == pwd);
            }, "<?php echo $this->lang->line('valid_passwords'); ?>");

            //form validation rules
            $("#driver_addform").validate({
                rules: {
                    statut: {
                        required: true
                    },
                    company: {
                        required: function() {
                            return $("#statut").val() == "2";
                        }
                    },
                    first_name: {
                        required: true,
                        lettersonly: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    phone: {
                        required: true,
                        phoneNumber: true,
                        rangelength: [10, 11]
                    },
                    password: {
                        required: true,
                        rangelength: [8, 30]
                    },
                    confirm_password: {
                        required: true,
                        pwdmatch: true
                    }
                },
                messages: {
                    statut: {
                        required: "Please select the statut"
                    },
                    company: {
                        required: "Please enter the company"
                    },
                    first_name: {
                        required: "<?php echo $this->lang->line('first_name_valid');?>"
                    },
                    email: {
                        required: "<?php echo $this->lang->line('email_valid');?>"
                    },
                    phone: {
                        required: "<?php echo $this->lang->line('phone_valid');?>"
                    },
                    password: {
                        required: "<?php echo $this->lang->line('password_valid');?>"
                    },
                    password_confirm: {
                        required: "<?php echo $this->lang->line('confirm_password_valid');?>"
                    }
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        }
    }

    //when the dom has loaded setup form validation rules
    $(D).ready(function($) {
        JQUERY4U.UTIL.setupFormValidation();
    });

})(jQuery, window, document);

jQuery(function($) {
    $.datepicker.regional['fr'] = {
        closeText: 'Fermer',
        prevText: '&#x3c;Préc',
        nextText: 'Suiv&#x3e;',
        currentText: 'Aujourd\'hui',
        monthNames: ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin',
            'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'
        ],
        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Jun',
            'Jul', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec'
        ],
        dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
        dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
        weekHeader: 'Sm',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: '',
        maxDate: '+12M +0D',
        showButtonPanel: true
    };
    $.datepicker.setDefaults($.datepicker.regional['fr']);

});

$(document).ready(function() {
    $(".datepicker").datepicker({
        dateFormat: "dd-mm-yy",
        regional: "fr"
    });
    $('#MainAdd').click(function() {

        $('#MainTab').hide();
        $('#ShowTabs').attr('class', 'row');

    });
  
});

// Passengers part

function AddPassengers() {
    setupDivPassengers();
    $(".AddPassengers").show();
}

function cancelPassengers() {
    setupDivPassengers();
    $(".ListPassengers").show();
}

function setupDivPassengers() {
    $(".AddPassengers").hide();
    $(".ListPassengers").hide();
    $(".EditPassengers").hide();
    $(".DeletePassengers").hide();
}


// Vat part

function driverProfileAdd() {
    setupDriverProfile();
    $(".driverProfileAdd").show();
}

function cancelDriverProfile() {
    setupDriverProfile();
    $(".ListdriverProfile").show();
}

function VatDelete() {
    var val = $('.chk-AddDriverProfile-btn').val();
    if (val != "") {
        setupDriverProfile();
        $(".driverProfileDelete").show();
    } else {
        alert('Please select a row to delete.');
    }
}

function driverProfileEdit() {
    var val = $('.chk-AddDriverProfile-btn').val();
    if (val == '') {
        alert("Please select record!");
        return false;
    }
    setupDriverProfile();
    $(".driverProfileEdit").show();
    $.ajax({
        type: "GET",
        url: '<?php echo base_url().'admin/driver_info/get_ajax_driverProfile'; ?>',
        data: {
            'driver_status_id': val
        },
        success: function(result) {
            // alert(result);
            document.getElementsByClassName("driverProfileEditajax")[0].innerHTML = result;
        }
    });
}

function driverProfileidEdit(id) {
    var val = id;
    if (val == '') {
        alert("Please select record!");
        return false;
    }
    setupDriverProfile();
    $(".driverProfileEdit").show();
    $.ajax({
        type: "GET",
        url: '<?php echo base_url().'admin/driver_info/get_ajax_driverProfile'; ?>',
        data: {
            'driver_status_id': val
        },
        success: function(result) {
            // alert(result);
            document.getElementsByClassName("driverProfileEditajax")[0].innerHTML = result;
        }
    });
}

function setupDriverProfile() {
    $(".driverProfileAdd").hide();
    $(".driverProfileEdit").hide();
    $(".ListdriverProfile").hide();
    $(".driverProfileDelete").hide();

}

// $('input.chk-mainvat-template').on('change', function() {
//     $('input.chk-mainvat-template').not(this).prop('checked', false);
//     var id = $(this).attr('data-input');
//     console.log(id)
//     $('.chk-AddDriverProfile-btn').val(id);
//     $('#driverProfileid').val(id);
// });
$(document).on('change', 'input.chk-mainvat-template', function() {
    $('input.chk-mainvat-template').not(this).prop('checked', false);
    var id = $(this).attr('data-input');
    console.log(id)
    $('.chk-AddDriverProfile-btn').val(id);
    $('#driverProfileid').val(id);
});

function getCaraddes() {
    $.ajax({
        url: "<?php echo base_url('admin/getCaradded') ?>",
        method: "GET",
        success: function(success) {
            $('#tableBody').html(success);
        },
        error: function(xhr) {
            console.log(xhr.responseText);
        }
    });
}


$('#save').on('click', function() {
    driverProfileEvent();
})
</script>
<script>
$(document).ready(function() {
    $(window).on('load', function() {
        <?php
        if ($tabstatus == '1') {
            ?>
            driverProfileEvent(); <?php
        } else if ($tabstatus == '2') {
            ?>
            AvailabilityEvent(); <?php
        } else if ($tabstatus == '3') {
            ?>
            postEvent(); <?php
        } else if ($tabstatus == '4') {
            ?>
            patternEvent(); <?php
        } else if ($tabstatus == '5') {
            ?>
            AvailabilityEvent(); <?php
        } else if ($tabstatus == '6') {
            ?>
            AvailabilityEvent(); <?php
        } else if ($tabstatus == '7') {
            ?>
            AvailabilityEvent(); <?php
        } else if ($tabstatus == '8') {
            ?>
            AvailabilityEvent(); <?php
        } else if ($tabstatus == '9') {
            ?>
            AvailabilityEvent(); <?php
        } else if ($tabstatus == '10') {
            ?>
            AvailabilityEvent(); <?php
        } else if ($tabstatus == '11') {
            ?>
            AvailabilityEvent(); <?php
        } else if ($tabstatus == '12') {
            ?>
            AvailabilityEvent(); <?php
        } else if ($tabstatus == '13') {
            ?>
            AvailabilityEvent(); <?php
        } ?>

    });


    $('.profile_E').click(function() {
        $('.driver_profile').show();
        $('.DRIVERS').hide();
        $('.CARS').hide();
        $('.DOCUMENTS').hide();
        $('.RESOURCES').hide();
        $('.BOOKINGS').hide();
        $('.TIMESHEET').hide();
        $('.AVAILABILITY').hide();
        $('.REQUESTS').hide();
        $('.INFRACTIONS').hide();
        $('.PAYMENTS').hide();
        $('.SUPPORT').hide();
        $('.MESSAGES').hide();
        driverProfileEvent();
    });
    $('.drivers_E').click(function() {
        $('.driver_profile').hide();
        $('.DRIVERS').show();
        $('.CARS').hide();
        $('.DOCUMENTS').hide();
        $('.RESOURCES').hide();
        $('.BOOKINGS').hide();
        $('.TIMESHEET').hide();
        $('.AVAILABILITY').hide();
        $('.REQUESTS').hide();
        $('.INFRACTIONS').hide();
        $('.PAYMENTS').hide();
        $('.SUPPORT').hide();
        $('.MESSAGES').hide();
        AvailabilityEvent();
    });
    $('.cars_E').click(function() {
        $('.driver_profile').hide();
        $('.DRIVERS').hide();
        $('.CARS').show();
        $('.DOCUMENTS').hide();
        $('.RESOURCES').hide();
        $('.BOOKINGS').hide();
        $('.TIMESHEET').hide();
        $('.AVAILABILITY').hide();
        $('.REQUESTS').hide();
        $('.INFRACTIONS').hide();
        $('.PAYMENTS').hide();
        $('.SUPPORT').hide();
        $('.MESSAGES').hide();
        AvailabilityEvent();
    });
    $('.documents_E').click(function() {
        $('.driver_profile').hide();
        $('.DRIVERS').hide();
        $('.CARS').hide();
        $('.DOCUMENTS').show();
        $('.RESOURCES').hide();
        $('.BOOKINGS').hide();
        $('.TIMESHEET').hide();
        $('.AVAILABILITY').hide();
        $('.REQUESTS').hide();
        $('.INFRACTIONS').hide();
        $('.PAYMENTS').hide();
        $('.SUPPORT').hide();
        $('.MESSAGES').hide();
        documentsEvent();
    });
    $('.resources_E').click(function() {
        $('.driver_profile').hide();
        $('.DRIVERS').hide();
        $('.CARS').hide();
        $('.DOCUMENTS').hide();
        $('.RESOURCES').show();
        $('.BOOKINGS').hide();
        $('.TIMESHEET').hide();
        $('.AVAILABILITY').hide();
        $('.REQUESTS').hide();
        $('.INFRACTIONS').hide();
        $('.PAYMENTS').hide();
        $('.SUPPORT').hide();
        $('.MESSAGES').hide();
        resourcesEvent();
    });
    $('.bookings_E').click(function() {
        $('.driver_profile').hide();
        $('.DRIVERS').hide();
        $('.CARS').hide();
        $('.DOCUMENTS').hide();
        $('.RESOURCES').hide();
        $('.BOOKINGS').show();
        $('.TIMESHEET').hide();
        $('.AVAILABILITY').hide();
        $('.REQUESTS').hide();
        $('.INFRACTIONS').hide();
        $('.PAYMENTS').hide();
        $('.SUPPORT').hide();
        $('.MESSAGES').hide();
        AvailabilityEvent();
    });
    $('.timesheet_E').click(function() {
        $('.driver_profile').hide();
        $('.DRIVERS').hide();
        $('.CARS').hide();
        $('.DOCUMENTS').hide();
        $('.RESOURCES').hide();
        $('.BOOKINGS').hide();
        $('.TIMESHEET').show();
        $('.AVAILABILITY').hide();
        $('.REQUESTS').hide();
        $('.INFRACTIONS').hide();
        $('.PAYMENTS').hide();
        $('.SUPPORT').hide();
        $('.MESSAGES').hide();
        AvailabilityEvent();
    });
    $('.availability_E').click(function() {
        $('.driver_profile').hide();
        $('.DRIVERS').hide();
        $('.CARS').hide();
        $('.DOCUMENTS').hide();
        $('.RESOURCES').hide();
        $('.BOOKINGS').hide();
        $('.TIMESHEET').hide();
        $('.AVAILABILITY').show();
        $('.REQUESTS').hide();
        $('.INFRACTIONS').hide();
        $('.PAYMENTS').hide();
        $('.SUPPORT').hide();
        $('.MESSAGES').hide();
        AvailabilityEvent();

    });
    $('.requests_E').click(function() {
        $('.driver_profile').hide();
        $('.DRIVERS').hide();
        $('.CARS').hide();
        $('.DOCUMENTS').hide();
        $('.RESOURCES').hide();
        $('.BOOKINGS').hide();
        $('.TIMESHEET').hide();
        $('.AVAILABILITY').hide();
        $('.REQUESTS').show();
        $('.INFRACTIONS').hide();
        $('.PAYMENTS').hide();
        $('.SUPPORT').hide();
        $('.MESSAGES').hide();
        AvailabilityEvent();

    });
    $('.infractions_E').click(function() {
        $('.driver_profile').hide();
        $('.DRIVERS').hide();
        $('.CARS').hide();
        $('.DOCUMENTS').hide();
        $('.RESOURCES').hide();
        $('.BOOKINGS').hide();
        $('.TIMESHEET').hide();
        $('.AVAILABILITY').hide();
        $('.REQUESTS').hide();
        $('.INFRACTIONS').show();
        $('.PAYMENTS').hide();
        $('.SUPPORT').hide();
        $('.MESSAGES').hide();
        AvailabilityEvent();

    });
    $('.payments_E').click(function() {
        $('.driver_profile').hide();
        $('.DRIVERS').hide();
        $('.CARS').hide();
        $('.DOCUMENTS').hide();
        $('.RESOURCES').hide();
        $('.BOOKINGS').hide();
        $('.TIMESHEET').hide();
        $('.AVAILABILITY').hide();
        $('.REQUESTS').hide();
        $('.INFRACTIONS').hide();
        $('.PAYMENTS').show();
        $('.SUPPORT').hide();
        $('.MESSAGES').hide();
        AvailabilityEvent();

    });
    $('.support_E').click(function() {
        $('.driver_profile').hide();
        $('.DRIVERS').hide();
        $('.CARS').hide();
        $('.DOCUMENTS').hide();
        $('.RESOURCES').hide();
        $('.BOOKINGS').hide();
        $('.TIMESHEET').hide();
        $('.AVAILABILITY').hide();
        $('.REQUESTS').hide();
        $('.INFRACTIONS').hide();
        $('.PAYMENTS').hide();
        $('.SUPPORT').show();
        $('.MESSAGES').hide();
        AvailabilityEvent();

    });
    $('.messages_E').click(function() {
        $('.driver_profile').hide();
        $('.DRIVERS').hide();
        $('.CARS').hide();
        $('.DOCUMENTS').hide();
        $('.RESOURCES').hide();
        $('.BOOKINGS').hide();
        $('.TIMESHEET').hide();
        $('.AVAILABILITY').hide();
        $('.REQUESTS').hide();
        $('.INFRACTIONS').hide();
        $('.PAYMENTS').hide();
        $('.SUPPORT').hide();
        $('.MESSAGES').show();
        AvailabilityEvent();

    });


    function driverProfileEvent() {
        $('.removeevent').remove();
        var html =
            '<div class="removeevent"><div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="driverProfileAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="driverProfileEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="VatDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
        $('.table-filter').append("<form class='filter-form'><input type='text' value='' placeholder='Name' class='filter' data-column-index='4'><input type='text' placeholder='City' value='' class='filter' data-column-index='7'><input type='text' value='' placeholder='Address' class='filter' data-column-index='5'></form>");
    }

    function documentsEvent() {
        $('.removeevent').remove();
        var html =
            '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Documentsadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="DocumentsEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="DocumentsDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }

    function resourcesEvent() {
        $('.removeevent').remove();
        var html =
            '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Resourcesadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="ResourcesEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="ResourcesDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }


    function AvailabilityEvent() {
        $('.removeevent').remove();
        var html = '';
        $('.addevent').append(html);

    }


})
</script>