<style>
#DataTables_Table_0_filter{
        margin-left: 3px;
    }
</style>
<div class="row RESOURCES">
  <div class="ListResources" >
  <input type="hidden" class="chk-Addresources-btn" value="">
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" style="width:2%;">#</th>
            <th class="text-center" ><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('time'); ?></th>
            <th class=" text-center">Resources</th>
            <th class="text-center" >Since</th>

          </tr>
          </thead>
          <tbody>
              <?php if (!empty($resources_data)): ?>
              <?php $cnt=1; ?>
              <?php foreach($resources_data as $key => $item):?>
                  <tr>
                    <td class="no-sort text-center"style="width:2%;">
                        <input type="checkbox" class="chk-mainresources-template" data-input="<?=$item->id?>">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="ResourcesidEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->create_date)) .'0000'. $item->id;?></a></td>
                      <td class="text-center"><?=from_unix_date($item->create_date)?></td>
                      <td class="text-center"><?=date("H:i:s", strtotime($item->create_date))?></td>
                      <td class="text-center"><?=$item->resources; ?></td>
                     
                      <td class="text-center"><?= timeDiff($item->create_date) ?></td>

                  </tr>
                  <?php $cnt++; ?>
              <?php endforeach; ?>
              <?php endif; ?>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>
<?=form_open("admin/driver_config/resourcesadd")?>
<!-- <form action="<?=base_url();?>admin/driver_config/vatadd" class="" method="post"  enctype="multipart/form-data"> -->
<div class="Resourcesadd" style="display: none;" >
    <div class="row" style="margin-top: 10px;">
        <div class="col-md-2">
            <div class="form-group">
                <div class="col-md-12" style="margin-top: 10px;">
                    <span style="font-weight: bold;">Remise de Materiel</span>
                </div>
            </div>
        </div>


        <div class="col-md-2">
            <div class="form-group">
                <div class="col-md-5" style="margin-top: 10px;padding-left: 0px;">
                    <span style="font-weight: bold;">Ressources</span>
                </div>
                <div class="col-md-7" style="padding: 0px;">
                    <select class="form-control multiselect jqmsLoaded" multiple="" style="display: none;">
                    </select><div class="ms-options-wrap" style="position: relative;"><button>Select</button><div class="ms-options" style="min-height: 200px; max-height: 949px; overflow: auto; display: none;"><div class="ms-search"><input type="text" value="" placeholder="Search" class="placeholder"></div><a href="#" class="ms-selectall global">Select all</a><ul style="column-count: 1; column-gap: 0px;"></ul></div></div>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group">
                <div class="col-md-4" style="margin-top: 10px;">
                    <span style="font-weight: bold;">Date</span>
                </div>
                <div class="col-md-6" style="padding: 0px;">
                    <input name="" class="form-control datepicker hasDatepicker" type="text" id="dp1591458431610">
                </div>
            </div>
        </div>


        <div class="col-md-1">
            <div class="form-group">
                <div class="col-md-4" style="margin-top: 10px;padding: 0px;">
                    <span style="font-weight: bold;">Time</span>
                </div>
                <div class="col-md-8" style="padding: 0px;">
                    <input name="" class="form-control timepicker1" type="text">
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group">
                <div class="col-md-4" style="margin-top: 10px;padding: 0px;">
                    <span style="font-weight: bold;">Employee</span>
                </div>
                <div class="col-md-8" style="padding: 0px;">
                    <select class="form-control">
                        <option value="">Select</option>
                    </select>
                </div>
            </div>
        </div>
        
        <div class="col-md-3">
            <div class="form-group">
                <div class="col-md-4" style="margin-top: 10px;">
                    <span style="font-weight: bold;">Telecharger</span>
                </div>
                <div class="col-md-8" style="padding: 0px;">
                    <input type="file">
                </div>
            </div>
        </div>


    </div>

    <div class="row" style="margin-top: 10px;">
        <div class="col-md-2">
            <div class="form-group">
                <div class="col-md-12" style="margin-top: 10px;">
                    <span style="font-weight: bold;">Etat</span>
                </div>
            </div>
        </div>

        <div class="col-md-9">
            <div class="form-group">
                <div class="col-md-12" style="padding: 0px;">
                    <textarea class="form-control"></textarea>
                </div>
            </div>
        </div>
        <div class="col-md-1">
            <button type="button" class="btn AddRemisedeMateriel">Add</button>
        </div>


    </div>

    <span class="RemisedeMateriel"></span>


    <div class="row" style="margin-top: 10px;">
        <div class="col-md-2">
            <div class="form-group">
                <div class="col-md-12" style="margin-top: 10px;">
                    <span style="font-weight: bold;">Restitution de Materiel</span>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group">
                <div class="col-md-5" style="margin-top: 10px;padding-left: 0px;">
                    <span style="font-weight: bold;">Ressources</span>
                </div>
                <div class="col-md-7" style="padding: 0px;">
                        <select class="form-control multiselect jqmsLoaded" multiple="" style="display: none;"></select><div class="ms-options-wrap" style="position: relative;"><button>Select</button><div class="ms-options" style="min-height: 200px; max-height: 949px; overflow: auto; display: none;"><div class="ms-search"><input type="text" value="" placeholder="Search" class="placeholder"></div><a href="#" class="ms-selectall global">Select all</a><ul style="column-count: 1; column-gap: 0px;"></ul></div></div>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group">
                <div class="col-md-4" style="margin-top: 10px;">
                    <span style="font-weight: bold;">Date</span>
                </div>
                <div class="col-md-6" style="padding: 0px;">
                    <input name="" class="form-control datepicker hasDatepicker" type="text" id="dp1591458431611">
                </div>
            </div>
        </div>


        <div class="col-md-1">
            <div class="form-group">
                <div class="col-md-4" style="margin-top: 10px;padding: 0px;">
                    <span style="font-weight: bold;">Time</span>
                </div>
                <div class="col-md-8" style="padding: 0px;">
                    <input name="" class="form-control timepicker1" type="text">
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group">
                <div class="col-md-4" style="margin-top: 10px;padding: 0px;">
                    <span style="font-weight: bold;">Employee</span>
                </div>
                <div class="col-md-8" style="padding: 0px;">
                    <select class="form-control">
                        <option value="">Select</option>
                    </select>
                </div>
            </div>
        </div>
        
        <div class="col-md-3">
            <div class="form-group">
                <div class="col-md-4" style="margin-top: 10px;">
                    <span style="font-weight: bold;">Telecharger</span>
                </div>
                <div class="col-md-8" style="padding: 0px;">
                    <input type="file">
                </div>
            </div>
        </div>

    </div>

    <div class="row" style="margin-top: 10px;">
        <div class="col-md-2">
            <div class="form-group">
                <div class="col-md-12" style="margin-top: 10px;">
                    <span style="font-weight: bold;">Etat</span>
                </div>
            </div>
        </div>

        <div class="col-md-9">
            <div class="form-group">
                <div class="col-md-12" style="padding: 0px;">
                    <textarea class="form-control"></textarea>
                </div>
            </div>
        </div>
        <div class="col-md-1">
            <button type="button" class="btn AddRestitutiondeMateriel">Add</button>
        </div>
    </div>

    <span class="RestitutiondeMateriel"></span>
                                                
                                                

                                            



                                                <div class="row" style="margin-top: 20px;">
                                                    <div class="col-md-12">
                                                        <a class="btn" style="float: right;cursor: pointer;" onclick="hideAdd()"><span class="delete-icon">Cancel</span></a>
                                                        <button style="padding: 3px 13px;margin-right: 10px;float:right;" type="submit" class="btn"><span class="save-icon"></span> Save</button>
                                                        <img id="displayimageloader" style="float: right; margin-right: 10px;display: none;" src="https://cab-booking-script.com/demo/v1/css/loading.gif">
                                                    </div>
                                                </div>
    <div class="row"></div>
    <div class="col-md-9" style="margin-top:10px">
    <button  class="btn"style="float:right; margin-left:7px;"><span class="save-icon"></span> Save </button>
        <button class="btn" style="float:right; margin-left:7px;" href="javascript:void()" onclick="cancelResources()"><span class="delete-icon"> Cancel </span></button>

    <?php echo form_close(); ?>
    </div>
</div>
<div class="resourcesEdit" style="display:none">
  <?=form_open("admin/driver_config/resourcesedit")?>
  <div class="resourcesEditajax">
  </div>
  <div class="col-md-2">
  <button  class="btn"style="float:right; margin-left:7px;"><span class="save-icon"></span> Update </button>
    <a class="btn" style="float:right; margin-left:7px;" href="javascript:void()" onclick="cancelResources()"><span class="delete-icon"> Cancel </span></a>

  </div>
  <?php echo form_close(); ?>
</div>
<div class="col-md-12 resourcesDelete" style="border:1px solid #ccc;padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
  <?=form_open("admin/driver_config/deleteresources")?>
    <input  name="tablename" value="vbs_driverresources" type="hidden" >
    <input type="hidden" id="resourcesdeletid" name="delet_resources_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn"style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

    <a class="btn" style="cursor: pointer;" onclick="cancelResources()"><span class="delete-icon">No</span></a>
  <?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
function Resourcesadd()
{
  setupDivResources();
  $(".Resourcesadd").show();
}

function cancelResources()
{
  setupDivResources();
  $(".ListResources").show();
}
// function ResourcesEdit()
// {
//   var val = $('.chk-Addresources-btn').val();
//   if(val != "")
//   {
//     setupDivResources();
//     $(".resourcesEdit").show();
//     $("#resourcesEditview"+val).show();
//   }
// }

function ResourcesEdit()
{
    var val = $('.chk-Addresources-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivResources();
        $(".resourcesEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/driver_config/get_ajax_resources'; ?>',
                data: {'resources_id': val},
                success: function (result) {
                  // alert(result);
                  document.getElementsByClassName("resourcesEditajax")[0].innerHTML = result;
                }
            });
}
function ResourcesidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivResources();
      $(".resourcesEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/driver_config/get_ajax_resources'; ?>',
              data: {'resources_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("resourcesEditajax")[0].innerHTML = result;
              }
          });
}
function ResourcesDelete()
{
  var val = $('.chk-Addresources-btn').val();
  if(val != "")
  {
  setupDivResources();
  $(".resourcesDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivResources()
{
  // $(".resourcesEditEdit").hide();
  $(".Resourcesadd").hide();
  $(".resourcesEdit").hide();
  $(".ListResources").hide();
  $(".resourcesDelete").hide();

}
$('input.chk-mainresources-template').on('change', function() {
  $('input.chk-mainresources-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Addresources-btn').val(id);
  $('#resourcesdeletid').val(id);
});

</script>
