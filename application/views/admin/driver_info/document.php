<style>
#DataTables_Table_0_filter{
        margin-left: 3px;
    }
</style>
<div class="row DOCUMENTS">
  <div class="ListDocuments" >
  <input type="hidden" class="chk-Adddocuments-btn" value="">
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" style="width:2%;">#</th>
            <th class="text-center" ><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('time'); ?></th>
            <th class=" text-center">Documents</th>
            <th class="text-center" >Since</th>

          </tr>
          </thead>
          <tbody>
              <?php if (!empty($documents_data)): ?>
              <?php $cnt=1; ?>
              <?php foreach($documents_data as $key => $item):?>
                  <tr>
                    <td class="no-sort text-center"style="width:2%;">
                        <input type="checkbox" class="chk-maindocuments-template" data-input="<?=$item->id?>">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="DocumentsidEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->create_date)) .'0000'. $item->id;?></a></td>
                      <td class="text-center"><?=from_unix_date($item->create_date)?></td>
                      <td class="text-center"><?=date("H:i:s", strtotime($item->create_date))?></td>
                      <td class="text-center"><?=$item->documents; ?></td>
                     
                      <td class="text-center"><?= timeDiff($item->create_date) ?></td>

                  </tr>
                  <?php $cnt++; ?>
              <?php endforeach; ?>
              <?php endif; ?>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>
<?=form_open("admin/driver_config/documentsadd")?>
<!-- <form action="<?=base_url();?>admin/driver_config/vatadd" class="" method="post"  enctype="multipart/form-data"> -->
<div class="Documentsadd" style="display: none;" >
<div class="row" style="margin-top: 10px;">

<div class="col-md-3">
    <div class="form-group">
        <div class="col-md-4" style="margin-top: 5px;">
            <span style="font-weight: bold;">Certificat Medicate</span>
        </div>
        <div class="col-md-8" style="padding: 0px;">
            <input required="" name="certificatmedicate" type="file">
        </div>
    </div>
</div>


<div class="col-md-3">
    <div class="form-group">
        <div class="col-md-4" style="margin-top: 5px;">
            <span style="font-weight: bold;">Date Delivrance</span>
        </div>
        <div class="col-md-8" style="padding: 0px;">
            <input required="" name="datedelivrance2"
                   class="form-control datepicker" type="text">
        </div>
    </div>
</div>

<div class="col-md-3">
    <div class="form-group">
        <div class="col-md-4" style="margin-top: 5px;">
            <span style="font-weight: bold;">Date Dexpiration</span>
        </div>
        <div class="col-md-8" style="padding: 0px;">
            <input required="" name="datedexpiration3"
                   class="form-control datepicker" type="text">
        </div>

    </div>
</div>

</div>


<div class="row" style="margin-top: 10px;">

<div class="col-md-3">
    <div class="form-group">
        <div class="col-md-4" style="margin-top: 10px;">
            <span style="font-weight: bold;">PSC1</span>
        </div>
        <div class="col-md-8" style="padding: 0px;">
            <input required="" name="PSC1" type="file">
        </div>
    </div>
</div>


<div class="col-md-3">
    <div class="form-group">
        <div class="col-md-4" style="margin-top: 5px;">
            <span style="font-weight: bold;">Date Delivrance</span>
        </div>
        <div class="col-md-8" style="padding: 0px;">
            <input required="" name="datedelivrance3"
                   class="form-control datepicker" type="text">
        </div>
    </div>
</div>

<div class="col-md-3">
    <div class="form-group">
        <div class="col-md-4" style="margin-top: 5px;">
            <span style="font-weight: bold;">Date Dexpiration</span>
        </div>
        <div class="col-md-8" style="padding: 0px;">
            <input required="" name="datedexpiration4"
                   class="form-control datepicker" type="text">
        </div>

    </div>
</div>

</div>


<div class="row" style="margin-top: 10px;">

<div class="col-md-3">
    <div class="form-group">
        <div class="col-md-4" style="margin-top: 5px;">
            <span style="font-weight: bold;">Medecine de Travai</span>
        </div>
        <div class="col-md-8" style="padding: 0px;">
            <input required="" name="medecinedetravai" type="file">
        </div>
    </div>
</div>


<div class="col-md-3">
    <div class="form-group">
        <div class="col-md-4" style="margin-top: 5px;">
            <span style="font-weight: bold;">Date Delivrance</span>
        </div>
        <div class="col-md-8" style="padding: 0px;">
            <input required="" name="datedelivrance4"
                   class="form-control datepicker" type="text">
        </div>
    </div>
</div>

<div class="col-md-3">
    <div class="form-group">
        <div class="col-md-4" style="margin-top: 5px;">
            <span style="font-weight: bold;">Date Dexpiration</span>
        </div>
        <div class="col-md-8" style="padding: 0px;">
            <input required="" name="datedexpiration5"
                   class="form-control  datepicker" type="text">
        </div>

    </div>
</div>

</div>

<div class="row" style="margin-top: 10px;">

<div class="col-md-3">
    <div class="form-group">
        <div class="col-md-4" style="margin-top: 5px;">
            <span style="font-weight: bold;">Autre Diplome1</span>
        </div>
        <div class="col-md-8" style="padding: 0px;">
            <input required="" name="autrdeiplome1" type="file">
        </div>
    </div>
</div>


<div class="col-md-3">
    <div class="form-group">
        <div class="col-md-4" style="margin-top: 5px;">
            <span style="font-weight: bold;">Autre Diplome2</span>
        </div>
        <div class="col-md-8" style="padding: 0px;">
            <input required="" name="autrediplome2" type="file">
        </div>
    </div>
</div>

<div class="col-md-3">
    <div class="form-group">
        <div class="col-md-4" style="margin-top: 5px;">
            <span style="font-weight: bold;">Autre Diplome3</span>
        </div>
        <div class="col-md-8" style="padding: 0px;">
            <input required="" name="autrediplome3" type="file">
        </div>

    </div>
</div>

</div>
  <div class="row"></div>
  <div class="col-md-9" style="margin-top:10px">
  <button  class="btn"style="float:right; margin-left:7px;"><span class="save-icon"></span> Save </button>
    <button class="btn" style="float:right; margin-left:7px;" href="javascript:void()" onclick="cancelDocuments()"><span class="delete-icon"> Cancel </span></button>

  <?php echo form_close(); ?>
</div>
</div>
<div class="documentsEdit" style="display:none">
  <?=form_open("admin/driver_config/documentsedit")?>
  <div class="documentsEditajax">
  </div>
  <div class="col-md-2">
  <button  class="btn"style="float:right; margin-left:7px;"><span class="save-icon"></span> Update </button>
    <a class="btn" style="float:right; margin-left:7px;" href="javascript:void()" onclick="cancelDocuments()"><span class="delete-icon"> Cancel </span></a>

  </div>
  <?php echo form_close(); ?>
</div>
<div class="col-md-12 documentsDelete" style="border:1px solid #ccc;padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
  <?=form_open("admin/driver_config/deletedocuments")?>
    <input  name="tablename" value="vbs_driverdocuments" type="hidden" >
    <input type="hidden" id="documentsdeletid" name="delet_documents_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn"style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

    <a class="btn" style="cursor: pointer;" onclick="cancelDocuments()"><span class="delete-icon">No</span></a>
  <?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
function Documentsadd()
{
  setupDivDocuments();
  $(".Documentsadd").show();
}

function cancelDocuments()
{
  setupDivDocuments();
  $(".ListDocuments").show();
}
// function DocumentsEdit()
// {
//   var val = $('.chk-Adddocuments-btn').val();
//   if(val != "")
//   {
//     setupDivDocuments();
//     $(".documentsEdit").show();
//     $("#documentsEditview"+val).show();
//   }
// }

function DocumentsEdit()
{
    var val = $('.chk-Adddocuments-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivDocuments();
        $(".documentsEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/driver_config/get_ajax_documents'; ?>',
                data: {'documents_id': val},
                success: function (result) {
                  // alert(result);
                  document.getElementsByClassName("documentsEditajax")[0].innerHTML = result;
                }
            });
}
function DocumentsidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivDocuments();
      $(".documentsEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/driver_config/get_ajax_documents'; ?>',
              data: {'documents_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("documentsEditajax")[0].innerHTML = result;
              }
          });
}
function DocumentsDelete()
{
  var val = $('.chk-Adddocuments-btn').val();
  if(val != "")
  {
  setupDivDocuments();
  $(".documentsDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivDocuments()
{
  // $(".documentsEditEdit").hide();
  $(".Documentsadd").hide();
  $(".documentsEdit").hide();
  $(".ListDocuments").hide();
  $(".documentsDelete").hide();

}
$('input.chk-maindocuments-template').on('change', function() {
  $('input.chk-maindocuments-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Adddocuments-btn').val(id);
  $('#documentsdeletid').val(id);
});

</script>
