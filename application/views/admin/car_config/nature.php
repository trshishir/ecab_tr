<div class="row">
  <div class="ListNature" >
  <input type="hidden" class="chk-Addnature-btn" value="">
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" style="width:2%;">#</th>
            <th class="text-center" ><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('added_by'); ?></th>
            <th class=" text-center">Nature</th>
            <th class=" text-center">Statut</th>
            <th class="text-center" >Since</th>

          </tr>
          </thead>
          <tbody>
              <?php if (!empty($car_nature_data)): ?>
              <?php $cnt=1; ?>
              <?php foreach($car_nature_data as $key => $item):?>
                  <tr>
                    <td class="no-sort text-center"style="width:2%;">
                        <input type="checkbox" class="chk-mainnature-template" data-input="<?=$item->id?>">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="NatureidEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->create_date)) .'0000'. $item->id;?></a></td>
                      <td class="text-center"><?=from_unix_date($item->create_date)?></td>
                      <td class="text-center"><?=date("H:i:s", strtotime($item->create_date))?></td>
                      <td class="text-center"><?= $this->car_config_model->getuser($item->user_id);?></td>
                      <td class="text-center"><?=$item->nature; ?></td>
                      <td class="text-center"><?=$item->statut=='0'?'<span class="label label-success">Active</span>':'<span class="label label-danger">Hide</span>';?></td>

                      <td class="text-center"><?= timeDiff($item->create_date) ?></td>

                  </tr>
                  <?php $cnt++; ?>
              <?php endforeach; ?>
              <?php endif; ?>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>
<?=form_open("admin/car_config/natureadd")?>
<!-- <form action="<?=base_url();?>admin/car_config/vatadd" class="" method="post"  enctype="multipart/form-data"> -->
<div class="Natureadd" style="display: none;" >
  <div class="col-md-12">
    <div class="col-md-2" style="margin-top: 5px;">
      <div class="form-group">
        <span style="font-weight: bold;">Statut</span>
        <select class="form-control" name="statut" required>
          <option value="">Statut</option>
          <option value="0">Show</option>
          <option value="1">Hide</option>
        </select>
      </div>
    </div>
  <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span style="font-weight: bold;">Nature</span>
        <input type="text" required class="form-control" name="nature" placeholder="" value="">
    </div>
  </div>
</div>
 <div class="col-md-12">  
                            
      <!-- icons -->
       <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
      <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelNature()"><span class="fa fa-close"> Cancel </span></button>
      <!-- icons -->
     
   </div>  
</div>
  <?php echo form_close(); ?>
<div class="natureEdit" style="display:none">
  <?=form_open("admin/car_config/natureedit")?>
  <div class="natureEditajax">
  </div>
<div class="col-md-12">                          
      <!-- icons -->
       <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
      <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelNature()"><span class="fa fa-close"> Cancel </span></button>
      <!-- icons -->   
   </div>  
</div>
 <?php echo form_close(); ?>
<div class="col-md-12 natureDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
  <?=form_open("admin/car_config/deletenature")?>
    <input  name="tablename" value="vbs_carnature" type="hidden" >
    <input type="hidden" id="naturedeletid" name="delet_nature_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

    <a class="btn btn-default" style="cursor: pointer;" onclick="cancelNature()"><span class="delete-icon">No</span></a>
  <?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
function Natureadd()
{
  setupDivNature();
  $(".Natureadd").show();
}

function cancelNature()
{
  setupDivNature();
  $(".ListNature").show();
}
// function NatureEdit()
// {
//   var val = $('.chk-Addnature-btn').val();
//   if(val != "")
//   {
//     setupDivNature();
//     $(".natureEdit").show();
//     $("#natureEditview"+val).show();
//   }
// }

function NatureEdit()
{
    var val = $('.chk-Addnature-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivNature();
        $(".natureEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/car_config/get_ajax_nature'; ?>',
                data: {'nature_id': val},
                success: function (result) {
                  // alert(result);
                  document.getElementsByClassName("natureEditajax")[0].innerHTML = result;
                }
            });
}
function NatureidEdit(val){

  
  if (val=='')
  {
   
      alert("Please select record ddd!");
      return false;
  }
      setupDivNature();
      $(".natureEdit").show();

      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/car_config/get_ajax_nature'; ?>',
              data: {'nature_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("natureEditajax")[0].innerHTML = result;
              }
          });
}
function NatureDelete()
{
  var val = $('.chk-Addnature-btn').val();
  if(val != "")
  {
  setupDivNature();
  $(".natureDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivNature()
{
  // $(".natureEditEdit").hide();
  $(".Natureadd").hide();
  $(".natureEdit").hide();
  $(".ListNature").hide();
  $(".natureDelete").hide();

}
function setupDivNatureConfig()
{
  // $(".natureEditEdit").hide();
  $(".Natureadd").hide();
  $(".natureEdit").hide();
  $(".ListNature").show();
  $(".natureDelete").hide();

}
$('input.chk-mainnature-template').on('change', function() {
  $('input.chk-mainnature-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Addnature-btn').val(id);
  $('#naturedeletid').val(id);
});

</script>
