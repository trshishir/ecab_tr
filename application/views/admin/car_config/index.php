<?php $locale_info = localeconv(); ?>

<style>
   
   
  .nav-tabs > li.active {
   background:linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%);
    border-bottom: none;
    }
    .nav-tabs>li.active>a{
         border: 1px solid #44c0e5!important;
    }
  .nav-tabs > li {
     background:linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%);
  
   border-left: 1px solid #44c0e5!important;
    height: 55px;
    margin: 0px !important;
    }

    .nav-tabs > li > a {

    background:linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%);
    color: #fff !important;
    font-size: 12px;
    padding-left: 10px;
    display: block !important;
    width: 100% !important;
    height: 100% !important;
    text-transform: uppercase;
    font-weight: bold;
    transition: 0.2s;
    outline: none;
    border: none;
    border-radius: 0px;
    line-height: 30px !important;
}
.dataTables_wrapper .dataTables_filter {
    float: left;
    text-align: left;
    margin-left: 10px;
}
input[type="search"]{
    border:1px solid #cccccc;
    padding-left: 5px;
    border-radius: 0px;
}
input[type="search"]:focus{
    border:1px solid #cccccc !important;
}
    
    .tab-pane{
        padding: 30px 30px 30px 14px;
    }
    .configTable{
        padding: 0px;
    }
    .dataTables_wrapper .dataTables_filter input {
    margin-right: 2px;
    margin-left: 0 !important;
    max-width: 100% !important;
    width: 100% !important;
    float: right;
    }
    .dataTables_wrapper{
        margin-top: 20px;
    }
   
    
  

</style>

<style>
 
.delete-icon {
    background: url(http://uniqueweb.co.in/project/ecabapp//assets/delete-icon.png) no-repeat left center !important;
    padding: 0px 0px 0px 22px;
}
.save-icon {
    background: url(http://uniqueweb.co.in/project/ecabapp//assets/save-icon.png) no-repeat left center !important;
    padding: 0px 0px 0px 22px;
}
#editnotworkingbtn,#notworkingbtn{
    background: linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%) !important;
}
.plusgreeniconconfig{
    border: 1px solid green;cursor: pointer;overflow: hidden;outline: none;font-size: 16px;
    color: #ffffff;
    height: 25px;
    width: 24px;
    border-radius: 50%;
    background-color: green;
    position: absolute;
    top: 22px;
    right: -20px;
    z-index: 10;
    text-align: center;
    margin: 0px;
}
.minusrediconconfig{
    border: 1px solid red;cursor: pointer;overflow: hidden;outline: none;font-size: 16px;
    color: #ffffff;
    height: 25px;
    width: 24px;
    border-radius: 50%;
    background-color: red;
    position: absolute;
    bottom: 9px;
    right: -20px;
    z-index: 10;
    text-align: center;
    margin: 0px;
}
.minusrediconconfig > i,.plusgreeniconconfig > i{
    margin:0px;
}
#editcarrestcost,#carrestcost{
    background-color: #add8e63d !important;
}
.dataTables_wrapper .dataTables_paginate .paginate_button{
   color:#fff !important;
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current{
   color:#fff !important;
}

.dataTables_wrapper .dataTables_paginate .paginate_button:hover {
     background: linear-gradient(to bottom, #0d85a8 0%, #35add1 39%,#44c0e5 100%) !important;
   
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
     background: linear-gradient(to bottom, #0d85a8 0%, #35add1 39%,#44c0e5 100%) !important;
    color:#fff !important;
}

</style>



<?php $active_status =  $this->session->flashdata('alert');?>
<?php if (!isset($active_status['price_stu_id']) ) {
  $active_status['price_stu_id']='';
} ?>
<?php if (!isset($active_status['status_id'])) {
  $active_status['status_id']='';
} ?>
<?php if ($active_status['status_id']!='') { $status=$active_status['status_id']; $tabstatus=$active_status['status_id']; }else{$status='1'; $tabstatus='1';} ?>
<?php if ($active_status['price_stu_id']!='') { $price_stu=$active_status['price_stu_id']; }else{$price_stu='1';} ?>
<section id="content">
    <div class="listDiv"><!--col-md-10 padding white right-p-->
    <?php $this->load->view('admin/common/breadcrumbs');?>
        <?php $this->load->view('admin/common/alert');?>
        <?php echo $this->session->flashdata('message'); ?>
            <div class="row-fluid"  id="ShowTabs" style="margin-bottom: 10px; margin-left:0px;">
                <ul class="nav nav-tabs responsive">
                                            <li <?=$status==1? 'class="active"':''?>><a href="#STATUS" class="status_E" role="tab" data-toggle="tab">STATUT</a></li>
                                            <li <?=$status==2? 'class="active"':''?>><a href="#CIVILITY" class="clivility_E" role="tab" data-toggle="tab">CIVILITE</a></li>
                                            <li <?=$status==3? 'class="active"':''?>><a href="#MARQUE" class="marque_E" role="tab" data-toggle="tab">MARQUE</a></li>
                                            <li <?=$status==4? 'class="active"':''?>><a href="#MODELE"role="tab" class="modele_E" data-toggle="tab">MODELE</a></li>
                                            <li <?=$status==5? 'class="active"':''?>><a href="#AGE" class="age_E" role="tab" data-toggle="tab">AGE DU VEHICULE</a></li>
                                            <li <?=$status==6? 'class="active"':''?>><a href="#SERIE" class="serie_E" role="tab" data-toggle="tab">SERIE</a></li>
                                            <li <?=$status==7? 'class="active"':''?>><a href="#BOITE" class="boite_E" role="tab" data-toggle="tab">BOITE A VITESSE</a></li>
                                            <li <?=$status==8? 'class="active"':''?>><a href="#BURANT" class="burant_E" role="tab" data-toggle="tab">CARBURANT</a></li>
                                            <li <?=$status==9? 'class="active"':''?>><a href="#COURROIE" class="courroie_E" role="tab" data-toggle="tab">COURROIE</a></li>
                                            <li <?=$status==10? 'class="active"':''?>><a href="#COULEUR" class="couleur_E" role="tab" data-toggle="tab">COULEUR</a></li>
                                            <li <?=$status==11? 'class="active"':''?>><a href="#NATURE" class="nature_E" role="tab" data-toggle="tab">NATURE</a></li>
                                            <li <?=$status==12? 'class="active"':''?>><a href="#TYPE" class="type_E" role="tab" data-toggle="tab">TYPE</a></li>
                                </ul>
                <div class="tab-content responsive" style="width:100%;display: table;">
                        <!--status tab starts-->
                        <div class="tab-pane fade <?=$status==1? 'in active':''?>" id="STATUS" style="border: 1px solid #ccc;">
                            <div class="row">
                            <div class="ListcarStatus">
                                <input type="hidden" class="chk-AddCarStatus-btn" value="">
                     
                            <div class="col-md-12">
                            <div class="module-body table-responsive">
                                <table class="configTable table table-bordered table-striped  table-hover dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
                                    <thead>
                                    <tr>
                                        <th class="no-sort text-center"style="width:2% !important;">#</th>
                                        <th class="text-center" style="width:3% !important;"><?php echo $this->lang->line('id'); ?></th>
                                        <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('date'); ?></th>
                                        <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('time'); ?></th>
                                        <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('added_by'); ?></th>
                                        <th class="column-first_name text-center" style="width:6% !important;">Name</th>
                                        <th class="column-first_name text-center" style="width:6% !important;">Statut</th>
                                        <th class="text-center" style="width:10%;">Since</th>
                                        
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php $cnt=1; ?>
                                        <?php 
                                        if (!empty($car_status_data)): 
                                           
                                        ?>
                                        <?php foreach($car_status_data as $key => $item):?>
                                            <tr>
                                                <td class="text-center">
                                                  <input type="checkbox" class="chk-mainvat-template" data-input="<?=$item->id?>"></td> 
                                                <td class="text-center"><a href="javascript:void()" onclick="carStatusidEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->create_date)) .'0000'. $item->id;?></a></td>
                                                <td class="text-center"><?=from_unix_date($item->create_date)?></td>
                                                <td class="text-center"><?=date("H:i:s", strtotime($item->create_date))?></td>
                                                <td class="text-center"><?= $this->car_config_model->getuser($item->user_id);?></td>
                                                <td class="text-center"><?= $item->name; ?></td>
                                                 <td class="text-center"><?=$item->statut=='0'?'<span class="label label-success">Active</span>':'<span class="label label-danger">Hide</span>';?></td>

                                                <td class="text-center"><?= timeDiff($item->create_date) ?></td>

                                            </tr>
                                            <?php $cnt++; ?>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                                <br>
                                </div>
                            </div>
                                                    </div>
                            <?=form_open("admin/car_config/carStatusadd")?>
                            <div class="carStatusAdd" style="display: none;" >
                        <div class="col-md-12">     
                             <div class="col-md-2" style="margin-top: 5px;">
                              <div class="form-group">
                                <span style="font-weight: bold;">Statut</span>
                                <select class="form-control" name="statut" required>
                                  <option value="">Statut</option>
                                  <option value="0">Show</option>
                                  <option value="1">Hide</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-2" style="margin-top: 5px;">
                                <div class="form-group">
                                <span style="font-weight: bold;">Name</span>
                                    <input type="text" class="form-control" name="name" placeholder="" value="" required="">
                                </div>
                            </div>
                        </div>
                            <div class="col-md-12">  
                            
                                <!-- icons -->
                                 <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                                <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelCarStatus()"><span class="fa fa-close"> Cancel </span></button>
                                <!-- icons -->
                               
                             </div>      
                            </div>
                            <?php echo form_close(); ?>
                            <div class="carStatusEdit" style="display:none">
                            <?=form_open("admin/car_config/carStatusEdit")?>
                            <div class="carStatusEditajax">
                            </div>
                             <div class="col-md-12">  
                            
                                <!-- icons -->
                                 <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                                <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelCarStatus()"><span class="fa fa-close"> Cancel </span></button>
                                <!-- icons -->
                               
                             </div>  
                            
                            <?php echo form_close(); ?>
                            </div>
                            <div class="col-md-12 carStatusDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
                                        <?=form_open("admin/car_config/deleteCarStatus")?>
                                            <input  name="tablename" value="vbs_job_statut" type="hidden" >
                                            <input type="hidden" id="carStatusid" name="delet_car_status_id" value="">
                                            <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10px;"> Are you sure you want to delete selected?</div>
                                            <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
                                <button  class="btn btn-default"style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

                                            <a class="btn btn-default" style="cursor: pointer;" onclick="cancelCarStatus()"><span class="delete-icon">No</span> </a>
                                        <?php echo form_close(); ?>
                                    </div>
                        </div>
                        </div>
                        <div class="tab-pane fade <?=$status== '2'? 'in active':''?>" id="CIVILITY" style="border: 1px solid #ccc;">
                        <?php include('civilite.php'); ?>
                        </div>
                        <div class="tab-pane fade <?=$status== '3'? 'in active':''?>" id="MARQUE" style="border: 1px solid #ccc;">
                        <?php include('marque.php'); ?>
                        </div>

                        <div class="tab-pane fade <?=$status== '4'? 'in active':''?>"  id="MODELE" style="border: 1px solid #ccc;">
                        <?php include('modele.php'); ?>
                        </div>
                        <div class="tab-pane fade <?=$status== '5'? 'in active':''?>" id="AGE" style="border: 1px solid #ccc;">
                        <?php include('age.php'); ?>
                        </div>
                        <div class="tab-pane fade <?=$status== '6'? 'in active':''?>" id="SERIE" style="border: 1px solid #ccc;">
                        <?php include('serie.php'); ?>
                        </div>
                        <div class="tab-pane fade <?=$status== '7'? 'in active':''?>" id="BOITE" style="border: 1px solid #ccc;">
                        <?php include('boite.php'); ?>
                        </div>
                        <div class="tab-pane fade <?=$status== '8'? 'in active':''?>" id="BURANT" style="border: 1px solid #ccc;">
                        <?php include('burant.php'); ?>
                        </div>
                        <div class="tab-pane fade <?=$status== '9'? 'in active':''?>" id="COURROIE" style="border: 1px solid #ccc;">
                        <?php include('courroie.php'); ?>
                        </div>
                        <div class="tab-pane fade <?=$status== '10'? 'in active':''?>" id="COULEUR" style="border: 1px solid #ccc;">
                        <?php include('couleur.php'); ?>
                        </div>
                        <div class="tab-pane fade <?=$status== '11'? 'in active':''?>" id="NATURE" style="border: 1px solid #ccc;">
                        <?php include('nature.php'); ?>
                        </div>
                        <div class="tab-pane fade <?=$status== '12'? 'in active':''?>" id="TYPE" style="border: 1px solid #ccc;">
                        <?php include('type.php'); ?>
                        </div>
                      
                   
                     
            </div>
            <!--/.module-->
        </div>

    </div>
</section>
<script>
    jQuery(function($){
        $.datepicker.regional['fr'] = {
            closeText: 'Fermer',
            prevText: '&#x3c;Préc',
            nextText: 'Suiv&#x3e;',
            currentText: 'Aujourd\'hui',
            monthNames: ['Janvier','Fevrier','Mars','Avril','Mai','Juin',
                'Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
            monthNamesShort: ['Jan','Fev','Mar','Avr','Mai','Jun',
                'Jul','Aou','Sep','Oct','Nov','Dec'],
            dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
            dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
            dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
            weekHeader: 'Sm',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: '',
            maxDate: '+12M +0D',
            showButtonPanel: true
        };
        $.datepicker.setDefaults($.datepicker.regional['fr']);
       
    });

    $(document).ready(function(){
        $( ".datepicker" ).datepicker({
            dateFormat: "dd-mm-yy",
            regional: "fr"
        });
        $('#MainAdd').click(function(){

            $('#MainTab').hide();
            $('#ShowTabs').attr('class','row');

        });

    });
    // Passengers part

    function AddPassengers()
    {
        setupDivPassengers();
        $(".AddPassengers").show();
    }

    function cancelPassengers()
    {
        setupDivPassengers();
        $(".ListPassengers").show();
    }

    function setupDivPassengers()
    {
        $(".AddPassengers").hide();
        $(".ListPassengers").hide();
        $(".EditPassengers").hide();
        $(".DeletePassengers").hide();
    }


    // Vat part

    function carStatusAdd()
    {
        setupCarStatus();
        $(".carStatusAdd").show();
    }

    function cancelCarStatus()
    {
        setupCarStatus();
        $(".ListcarStatus").show();
    }
    function VatDelete()
    {
        var val = $('.chk-AddCarStatus-btn').val();
        if(val != "")
        {
            setupCarStatus();
            $(".carStatusDelete").show();
        }else{
            alert('Please select a row to delete.');
        }
    }
    function carStatusEdit()
    {
        var val = $('.chk-AddCarStatus-btn').val();
        if (val=='')
        {
            alert("Please select record!");
            return false;
        }
        setupCarStatus();
        $(".carStatusEdit").show();
        $.ajax({
            type: "GET",
            url: '<?php echo base_url().'admin/car_config/get_ajax_carStatus'; ?>',
            data: {'car_status_id': val},
            success: function (result) {
                // alert(result);
                document.getElementsByClassName("carStatusEditajax")[0].innerHTML = result;
            }
        });
    }
    function carStatusidEdit(id){
        var val = id;
        if (val=='')
        {
            alert("Please select record!");
            return false;
        }
        setupCarStatus();
        $(".carStatusEdit").show();
        $.ajax({
            type: "GET",
            url: '<?php echo base_url().'admin/car_config/get_ajax_carStatus'; ?>',
            data: {'car_status_id': val},
            success: function (result) {
                // alert(result);
                document.getElementsByClassName("carStatusEditajax")[0].innerHTML = result;
            }
        });
    }
    function setupCarStatus()
    {
        $(".carStatusAdd").hide();
        $(".carStatusEdit").hide();
        $(".ListcarStatus").hide();
        $(".carStatusDelete").hide();

    }
      function setupCarStatusConfig()
    {
        $(".carStatusAdd").hide();
        $(".carStatusEdit").hide();
        $(".ListcarStatus").show();
        $(".carStatusDelete").hide();

    }
    // $('input.chk-mainvat-template').on('change', function() {
    //     $('input.chk-mainvat-template').not(this).prop('checked', false);
    //     var id = $(this).attr('data-input');
    //     console.log(id)
    //     $('.chk-AddCarStatus-btn').val(id);
    //     $('#carStatusid').val(id);
    // });
    $(document).on('change', 'input.chk-mainvat-template', function() {
        $('input.chk-mainvat-template').not(this).prop('checked', false);
        var id = $(this).attr('data-input');
        console.log(id)
        $('.chk-AddCarStatus-btn').val(id);
        $('#carStatusid').val(id);
    });

    function getCaraddes(){
        $.ajax({
            url:"<?php echo base_url('admin/getCaradded') ?>",
            method:"GET",
            success:function(success){
                $('#tableBody').html(success);
            },error:function(xhr){
                console.log(xhr.responseText);
            }
        });
    }


    $('#save').on('click',function(){
        carStatusEvent();
    })

</script>
<script>
$(document).ready(function () {
    $(window).on('load', function() {
        <?php if ($tabstatus=='1') { ?>
        carStatusEvent();
        <?php }else if ($tabstatus=='2'){ ?>
        civiliteEvent();
        <?php }else if ($tabstatus=='3'){ ?>
        marqueEvent();
        <?php }else if ($tabstatus=='4'){ ?>
        modeleEvent();
        <?php }else if ($tabstatus=='5'){ ?>
         ageEvent();
        <?php }else if ($tabstatus=='6'){ ?>
        serieEvent();
        <?php }else if ($tabstatus=='7'){ ?>
        boiteEvent();
        <?php }else if ($tabstatus=='8'){ ?>
        burantEvent();
        <?php }else if ($tabstatus=='9'){ ?>
        courroieEvent();
        <?php }else if ($tabstatus=='10'){  ?>
        couleurEvent();
        <?php }else if ($tabstatus=='11'){  ?>
        natureEvent();
         <?php }else if ($tabstatus=='12'){  ?>
        typeEvent();
        <?php } ?>

    });
    $('.status_E').click(function(){
        carStatusEvent();
    });
    $('.clivility_E').click(function(){
      civiliteEvent();
    });
    $('.marque_E').click(function(){
        marqueEvent();
    });
    $('.modele_E').click(function(){
        modeleEvent();
    });
      $('.age_E').click(function(){
        ageEvent();
    });
      $('.serie_E').click(function(){
        serieEvent();
    });
      $('.boite_E').click(function(){
        boiteEvent();
    });
      $('.burant_E').click(function(){
        burantEvent();
    });
      $('.courroie_E').click(function(){
        courroieEvent();
    });
      $('.couleur_E').click(function(){
        couleurEvent();
    });
      $('.nature_E').click(function(){
        natureEvent();
    });
     $('.type_E').click(function(){
        typeEvent();
    });

    function carStatusEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="carStatusAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="carStatusEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="VatDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }

    function civiliteEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Civiliteadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="CiviliteEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="CiviliteDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
      function marqueEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Marqueadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="MarqueEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="MarqueDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
     function modeleEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Modeleadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="ModeleEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="ModeleDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
      function ageEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Ageadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="AgeEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="AgeDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
       function serieEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Serieadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="SerieEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="SerieDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }

     function boiteEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Boiteadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="BoiteEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="BoiteDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }

     function burantEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Burantadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="BurantEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="BurantDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }

     function courroieEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Courroieadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="CourroieEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="CourroieDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }


     function couleurEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Couleuradd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="CouleurEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="CouleurDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }

     function natureEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Natureadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="NatureEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="NatureDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }

     function typeEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Typeadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="TypeEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="TypeDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
 
 function removeAllResources(){
    setupDivCiviliteConfig();
    setupDivMarqueConfig();
    setupCarStatusConfig();
    setupDivModeleConfig();
    setupDivAgeConfig();
    setupDivSerieConfig();
    setupDivBoiteConfig();
    setupDivBurantConfig();
    setupDivCourroieConfig();
    setupDivCouleurConfig();
    setupDivNatureConfig();
    setupDivTypeConfig();

 }
    
});

</script>
