<div class="row">
  <div class="ListType" >
  <input type="hidden" class="chk-Addtype-btn" value="">
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" style="width:2%;">#</th>
            <th class="text-center" ><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('added_by'); ?></th>
            <th class=" text-center">Icon</th>
            <th class=" text-center">Type</th>
            <th class=" text-center">Statut</th>
            <th class="text-center" >Since</th>

          </tr>
          </thead>
          <tbody>
              <?php if (!empty($car_type_data)): ?>
              <?php $cnt=1; ?>
              <?php foreach($car_type_data as $key => $item):?>
                  <tr>
                    <td class="no-sort text-center"style="width:2%;">
                        <input type="checkbox" class="chk-maintype-template" data-input="<?=$item->id?>">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="TypeidEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->create_date)) .'0000'. $item->id;?></a></td>
                      <td class="text-center"><?=from_unix_date($item->create_date)?></td>
                      <td class="text-center"><?=date("H:i:s", strtotime($item->create_date))?></td>
                      <td class="text-center"><?= $this->car_config_model->getuser($item->user_id);?></td>
                      <td class="text-center"><img src="<?=base_url()?>/uploads/vehicle_images/<?= $item->car_icon ?>" width="60" height="30" ></td>
                      <td class="text-center"><?=$item->type_name; ?></td>
                      <td class="text-center"><?=$item->statut=='0'?'<span class="label label-success">Active</span>':'<span class="label label-danger">Hide</span>';?></td>

                      <td class="text-center"><?= timeDiff($item->create_date) ?></td>

                  </tr>
                  <?php $cnt++; ?>
              <?php endforeach; ?>
              <?php endif; ?>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>
<?=form_open_multipart("admin/car_config/typeadd")?>
<!-- <form action="<?=base_url();?>admin/car_config/vatadd" class="" method="post"  enctype="multipart/form-data"> -->
<div class="Typeadd" style="display: none;" >
  <div class="col-md-12">
    <div class="col-md-2" style="margin-top: 5px;">
      <div class="form-group">
        <span style="font-weight: bold;">Statut</span>
        <select class="form-control" name="statut" required>
          <option value="">Statut</option>
          <option value="0">Show</option>
          <option value="1">Hide</option>
        </select>
      </div>
    </div>
  <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span style="font-weight: bold;">Type</span>
        <input type="text" required class="form-control" name="type" placeholder="" value="">
    </div>
  </div>
   <div class="col-md-2" style="margin-top: 5px;">
     <div class="col-md-12">
       <img  id="car_logo_preview" src="<?php echo base_url(); ?>uploads/vehicle_images/default_car.png" style="width: 200px; height: 150px;">
     </div>
     <div class="col-md-12">
     <div class="form-group">
       
       <input type="file" name="car_icon" class="form-control-file" id="caricon" onChange="carpreview(this)"  required style="width:100%;">
       <span>Car Icone</span>
     </div>
   </div>
  </div>
</div>
    <div class="maincapacitybox col-md-12">
    <div class="col-md-6" style="position:relative;width: 54%;">
       <button type="button" onclick="addboxes();" class="plusgreeniconconfig"><i class="fa fa-plus" style="margin:0px !important;"></i></button>
     </div>
       <div class="capacitybox" >
          <div class="col-md-6" style="margin-top:10px;margin-left: 1.5%;background-image: linear-gradient(to bottom, #fff 0, #e0e0e0 100%);border-color: #ccc;padding: 6px 12px;"> 

             <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6 text-right" style="padding-top: 7px;">
                    <span>Passengers</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="passengercap[]" style="width:100%;" class="form-control" required>
                     
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
                  </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Luggage</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
 
                  <select name="lugagescap[]" style="width:100%;" class="form-control" required>
                     
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
                </div>
             </div>
             <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Wheelchairs</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="wheelchairscap[]" style="width:100%;" class="form-control" required>
                    
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
                </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Babys</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="babycap[]" style="width:100%;" class="form-control" required>
                     
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
              </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Animals</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="animalscap[]" style="width:100%;" class="form-control" required>
                    
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
               </div>
             </div>
          
           </div>  
         </div>  

    </div>
 <div class="col-md-12">  
                            
      <!-- icons -->
       <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
      <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelType()"><span class="fa fa-close"> Cancel </span></button>
      <!-- icons -->
     
   </div>  
</div>
  <?php echo form_close(); ?>
<div class="typeEdit" style="display:none">
  <?=form_open_multipart("admin/car_config/typeedit")?>
  <div class="typeEditajax">
  </div>
<div class="col-md-12">                          
      <!-- icons -->
       <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
      <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelType()"><span class="fa fa-close"> Cancel </span></button>
      <!-- icons -->   
   </div>  
</div>
 <?php echo form_close(); ?>
<div class="col-md-12 typeDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
  <?=form_open("admin/car_config/deletetype")?>
    <input  name="tablename" value="vbs_cartype" type="hidden" >
    <input type="hidden" id="typedeletid" name="delet_type_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

    <a class="btn btn-default" style="cursor: pointer;" onclick="cancelType()"><span class="delete-icon">No</span></a>
  <?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
function Typeadd()
{
  setupDivType();
  $(".Typeadd").show();
}

function cancelType()
{
  setupDivType();
  $(".ListType").show();
}
// function TypeEdit()
// {
//   var val = $('.chk-Addtype-btn').val();
//   if(val != "")
//   {
//     setupDivType();
//     $(".typeEdit").show();
//     $("#typeEditview"+val).show();
//   }
// }

function TypeEdit()
{
    var val = $('.chk-Addtype-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivType();
        $(".typeEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/car_config/get_ajax_type'; ?>',
                data: {'type_id': val},
                success: function (result) {
                  // alert(result);
                  document.getElementsByClassName("typeEditajax")[0].innerHTML = result;
                }
            });
}
function TypeidEdit(val){

  
  if (val=='')
  {
   
      alert("Please select record ddd!");
      return false;
  }
      setupDivType();
      $(".typeEdit").show();

      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/car_config/get_ajax_type'; ?>',
              data: {'type_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("typeEditajax")[0].innerHTML = result;
              }
          });
}
function TypeDelete()
{
  var val = $('.chk-Addtype-btn').val();
  if(val != "")
  {
  setupDivType();
  $(".typeDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivType()
{
  // $(".typeEditEdit").hide();
  $(".Typeadd").hide();
  $(".typeEdit").hide();
  $(".ListType").hide();
  $(".typeDelete").hide();

}
function setupDivTypeConfig()
{
  // $(".typeEditEdit").hide();
  $(".Typeadd").hide();
  $(".typeEdit").hide();
  $(".ListType").show();
  $(".typeDelete").hide();

}
$('input.chk-maintype-template').on('change', function() {
  $('input.chk-maintype-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Addtype-btn').val(id);
  $('#typedeletid').val(id);
});

</script>
<script type="text/javascript">
 
function carpreview(e) {
  if (e.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e){
      document.querySelector('#car_logo_preview').setAttribute('src', e.target.result);
    }
    reader.readAsDataURL(e.files[0]);
  }
}
function editcarpreview(e) {
  if (e.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e){
      document.querySelector('#editcar_logo_preview').setAttribute('src', e.target.result);
    }
    reader.readAsDataURL(e.files[0]);
  }
}
 
function addboxes(){
   $("div.maincapacitybox").append($("div.capacitybox:first").clone(true).append('<div class="col-md-6" style="position:relative;width: 54%;"><button type="button"  onclick="removebox(this);" class="minusrediconconfig"><i class="fa fa-minus" style="margin:0px;"></i></button></div>'));
}
function removebox(e){
   $(e).closest(".capacitybox").remove();
}
function editremovebox(e){
   $(e).closest(".capacitybox").remove();
}
function editboxes(){
   $("div.maineditcapacitybox").append($("div.capacitybox:first").clone(true).append('<div class="col-md-6" style="position:relative;width: 54%;"><button type="button"  onclick="editremovebox(this);" class="minusrediconconfig"><i class="fa fa-minus"></i></button></div>'));
}
function deleteconfigbox(e){
   $(e).closest(".capacitybox").remove();
}
</script>