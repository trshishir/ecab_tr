<?php $locale_info = localeconv(); ?>
<section id="content" style="min-height:400px;">
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12">
                <?php $this->load->view('admin/common/alert');?>
                <div class="module">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="module-head">
                    </div>
                    <div class="module-body">
                        <?=form_open("admin/google_api")?>
                        <h2 class="panel-title" style="font-weight: bold">General:</h2>
                        <div class="row" style="margin-top: 5px;">
                            <div class="col-xs-2">
                                <div class="form-group">
                                    <label>Statut:</label>
                                    <select class="form-control" name="status" required>

                                        <option <?=@$google_api->status == 1 ? "selected" : ""?> value="1">Show</option>
                                        <option <?=@$google_api->status == 0 ? "selected" : ""?> value="0">Hide</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-5">
                                <div class="form-group">
                                    <label>Api Key</label>
                                    <input type="text" class="form-control" name="api_key" placeholder="" value="<?=@$google_api->api_key?>">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label>Country</label>
                                    <select class="form-control" name="country_id">
                                        <option value="0">Select Country</option>
                                        <?php foreach($countries as $data):?>
                                            <option <?=@$google_api->country_id == $data->id ? "selected" : ""?> value="<?=$data->id;?>"><?=$data->name;?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <h2 class="panel-title" style="font-weight: bold">Front:</h2>
                        <div class="row">
                            <div class="col-xs-4 col-md-2">
                                <div class="form-group">
                                    <label>Statut:</label>
                                    <select class="form-control" name="permission[front][status]" required>
                                        <option <?=@$google_api->permission->front->status == 1 ? "selected" : ""?> value="1">Show</option>
                                        <option <?=@$google_api->permission->front->status == 0 ? "selected" : ""?> value="0">Hide</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2">
                                <div class="form-group">
                                    <label>Position:</label>
                                    <select class="form-control" name="permission[front][position]" required>
                                        <option <?=@$google_api->permission->front->position == 1 ? "selected" : ""?> value="1">Enable</option>
                                        <option <?=@$google_api->permission->front->position == 0 ? "selected" : ""?> value="0">Disable</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2">
                                <div class="form-group">
                                    <label>Live Traffic:</label>
                                    <select class="form-control" name="permission[front][live]" required>
                                        <option <?=@$google_api->permission->front->live == 1 ? "selected" : ""?> value="1">Enable</option>
                                        <option <?=@$google_api->permission->front->live == 0 ? "selected" : ""?> value="0">Disable</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <h2 class="panel-title" style="font-weight: bold">Areas:</h2>
                        <div class="row">
                            <div class="col-xs-4 col-md-2">
                                <div class="form-group">
                                    <label>Statut:</label>
                                    <select class="form-control" name="permission[area][status]" required>
                                        <option <?=@$google_api->permission->area->status == 1 ? "selected" : ""?> value="1">Show</option>
                                        <option <?=@$google_api->permission->area->status == 0 ? "selected" : ""?> value="0">Hide</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2">
                                <div class="form-group">
                                    <label>Position:</label>
                                    <select class="form-control" name="permission[area][position]" required>
                                        <option <?=@$google_api->permission->area->position == 1 ? "selected" : ""?> value="1">Enable</option>
                                        <option <?=@$google_api->permission->area->position == 0 ? "selected" : ""?> value="0">Disable</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2">
                                <div class="form-group">
                                    <label>Live Traffic:</label>
                                    <select class="form-control" name="permission[area][live]" required>
                                        <option <?=@$google_api->permission->area->live == 1 ? "selected" : ""?> value="1">Enable</option>
                                        <option <?=@$google_api->permission->area->live == 0 ? "selected" : ""?> value="0">Disable</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <h2 class="panel-title" style="font-weight: bold">Admin Map View:</h2>
                        <div class="row">
                            <div class="col-xs-4 col-md-2">
                                <div class="form-group">
                                    <label>Statut:</label>
                                    <select class="form-control" name="permission[map_view][status]" required>
                                        <option <?=@$google_api->permission->map_view->status == 1 ? "selected" : ""?> value="1">Show</option>
                                        <option <?=@$google_api->permission->map_view->status == 0 ? "selected" : ""?> value="0">Hide</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2">
                                <div class="form-group">
                                    <label>Position:</label>
                                    <select class="form-control" name="permission[map_view][position]" required>
                                        <option <?=@$google_api->permission->map_view->position == 1 ? "selected" : ""?> value="1">Enable</option>
                                        <option <?=@$google_api->permission->map_view->position == 0 ? "selected" : ""?> value="0">Disable</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2">
                                <div class="form-group">
                                    <label>Live Traffic:</label>
                                    <select class="form-control" name="permission[map_view][live]" required>
                                        <option <?=@$google_api->permission->map_view->live == 1 ? "selected" : ""?> value="1">Enable</option>
                                        <option <?=@$google_api->permission->map_view->live == 0 ? "selected" : ""?> value="0">Disable</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <h2 class="panel-title" style="font-weight: bold">Admin User Profile:</h2>
                        <div class="row">
                            <div class="col-xs-4 col-md-2">
                                <div class="form-group">
                                    <label>Statut:</label>
                                    <select class="form-control" name="permission[profile][status]" required>
                                        <option <?=@$google_api->permission->profile->status == 1 ? "selected" : ""?> value="1">Show</option>
                                        <option <?=@$google_api->permission->profile->status == 0 ? "selected" : ""?> value="0">Hide</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2">
                                <div class="form-group">
                                    <label>Position:</label>
                                    <select class="form-control" name="permission[profile][position]" required>
                                        <option <?=@$google_api->permission->profile->position == 1 ? "selected" : ""?> value="1">Enable</option>
                                        <option <?=@$google_api->permission->profile->position == 0 ? "selected" : ""?> value="0">Disable</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2">
                                <div class="form-group">
                                    <label>Live Traffic:</label>
                                    <select class="form-control" name="permission[profile][live]" required>
                                        <option <?=@$google_api->permission->profile->live == 1 ? "selected" : ""?> value="1">Enable</option>
                                        <option <?=@$google_api->permission->profile->live == 0 ? "selected" : ""?> value="0">Disable</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <h2 class="panel-title" style="font-weight: bold">Admin Booking:</h2>
                        <div class="row">
                            <div class="col-xs-4 col-md-2">
                                <div class="form-group">
                                    <label>Statut:</label>
                                    <select class="form-control" name="permission[booking][status]" required>
                                        <option <?=@$google_api->permission->booking->status == 1 ? "selected" : ""?> value="1">Show</option>
                                        <option <?=@$google_api->permission->booking->status == 0 ? "selected" : ""?> value="0">Hide</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2">
                                <div class="form-group">
                                    <label>Position:</label>
                                    <select class="form-control" name="permission[booking][position]" required>
                                        <option <?=@$google_api->permission->booking->position == 1 ? "selected" : ""?> value="1">Enable</option>
                                        <option <?=@$google_api->permission->booking->position == 0 ? "selected" : ""?> value="0">Disable</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2">
                                <div class="form-group">
                                    <label>Live Traffic:</label>
                                    <select class="form-control" name="permission[booking][live]" required>
                                        <option <?=@$google_api->permission->booking->live == 1 ? "selected" : ""?> value="1">Enable</option>
                                        <option <?=@$google_api->permission->booking->live == 0 ? "selected" : ""?> value="0">Disable</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-right">
                                    <a href="<?=base_url("admin/dashboard")?>" class="btn btn-default"><i class="fa fa-times"></i> Cancel</a>
                                    <button class="btn btn-default"><i class="fa fa-floppy-o"></i> Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
    <!--/.module-->
    <!--/.content-->
</section>