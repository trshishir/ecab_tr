
<section id="content">
    <div class="listDiv">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <?php $this->load->view('admin/common/alert');?>
        <div class="module">
            <?php echo $this->session->flashdata('message'); ?>
            <div class="module-body">
                <?=form_open("admin/weather/".$data->id."/update")?>

                <div class="row">

                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Source of Data*</label>
                            <select class="form-control" name="source_of_data" >

                                <option <?= $data->source_of_data == 'fio' ? 'selected' : ''; ?> value="fio">Forecast.io</option>
                                <option <?= $data->source_of_data == 'yhw' ? 'selected' : ''; ?> value="yhw">Yahoo!</option>
                                <option <?= $data->source_of_data == 'wwo' ? 'selected' : ''; ?> value="wwo">World Weather Online</option>
                                <option <?= $data->source_of_data == 'owm' ? 'selected' : ''; ?> value="owm">Open Weather Map</option>
                                <option <?= $data->source_of_data == 'wug' ? 'selected' : ''; ?> value="wug">Wunderground</option>
                                <option <?= $data->source_of_data == 'ham' ? 'selected' : ''; ?> value="ham">HAMweather</option>
                            </select>



                            <!-- ham|yhw|wwo|owm|wug -->
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Show Description*</label>
                            <select class="form-control" name="showDiscription">
                                <option <?= $data->showDiscription == 'true' ? 'selected' : ''; ?> value="true">Yes</option>
                                <option <?= $data->showDiscription == 'false' ? 'selected' : ''; ?> value="false">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Show Region*</label>
                            <select class="form-control" name="showRegion" >
                                <option <?= $data->showRegion == 'true' ? 'selected' : ''; ?> value="true">Yes</option>
                                <option <?= $data->showRegion == 'false' ? 'selected' : ''; ?> value="false">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Show Forcasts*</label>
                            <select class="form-control" name="forecasts" >
                                <option <?= $data->forecasts == 'true' ? 'selected' : ''; ?> value="true">Yes</option>
                                <option <?= $data->forecasts == 'false' ? 'selected' : ''; ?> value="false">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Enable Search*</label>
                            <select class="form-control" name="enableSearch" >
                                <option <?= $data->enableSearch == 'true' ? 'selected' : ''; ?> value="true">Yes</option>
                                <option <?= $data->enableSearch == 'false' ? 'selected' : ''; ?> value="false">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Location*</label>
                            <input type="text" value="<?= $data->location; ?>" class="form-control" name="location">

                        </div>
                    </div>

                    <!--                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Show Country*</label>
                                    <select class="form-control" name="showCountry" >
                                            <option <?= $data->showCountry == 'true' ? 'selected' : ''; ?> value="true">Yes</option>
                                            <option <?= $data->showCountry == 'false' ? 'selected' : ''; ?> value="false">No</option>
                                    </select>
                                </div>
                            </div> -->

                </div>
                <div class="row">
                    <!--   <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Show Details*</label>
                                    <select class="form-control" name="showDetails" >
                                            <option <?= $data->showDetails == 'true' ? 'selected' : ''; ?> value="true">Yes</option>
                                            <option <?= $data->showDetails == 'false' ? 'selected' : ''; ?> value="false">No</option>
                                    </select>
                                </div>
                            </div> -->

                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Days *</label>
                            <select  class="form-control" name="nbForecastDays">

                                <?php for($i=1; $i<=7; $i++ ):?>
                                    <option <?php echo ($data->nbForecastDays==$i)?'selected':'';?> value="<?=$i;?>"><?=$i;?></option>
                                <?php endfor;?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Background *</label>
                            <select  class="form-control" name="background">
                                <option <?= $data->background == 'gray' ? 'selected' : ''; ?> value="gray">Gray</option>
                                <option <?= $data->background == 'navy' ? 'selected' : ''; ?> value="navy">Navy Blue</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="pull-right">
                            <input type="button" class="btn btn-default fa fa-input" value="&#xf05e Cancel" onclick="return hideAdd();" tabindex="17" />
                            <button class="btn btn-default fa fa-save" name="submit"> Save</button>
                        </div>
                    </div>
                </div>
                <br>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</section>