    <?php $this->load->view('admin/dashboard_css'); ?>
    <section id="content"  style="height: calc(100vh - 140px);">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.0/morris.min.js"></script>

        <form onsubmit="chartFformSubmit(event)" id="chartFilterForm" class="row" method="GET">
            <div class="col-md-12" >
                <div  class="filter-group">
                    <div class="col-md-1">
                        <div class="filter-label">
                           <div>Filter By</div>
                        </div>
                    </div>
                    <div class="col-md-2">
                            <select name="modules" style="float: left;margin-right: 10px;background: transparent linear-gradient(#FBFBFB,#ECECEC,#CECECE) repeat scroll 0% 0% !important;" class="form-control" disabled>
                                <option>CEO View</option>
                                <option>Manager View</option>
                                <option>Fleet Manager View</option>
                                <option>Sales Man View</option>
                                <option>Support Operator View</option>
                                <option>Accounter View</option>
                            </select>
                    </div> 
                    <div class="col-md-2">       
                        <select name="year_lists" style="float: left; margin-right: 10px;background: transparent linear-gradient(#FBFBFB,#ECECEC,#CECECE) repeat scroll 0% 0% !important;" class="form-control">
                            <option>2021</option>
                            <option>2020</option>
                            <option>2019</option>
                        </select>
                    </div>
                    <div class="col-md-2">           
                        <select name="month_lists" style="float: left; margin-right: 10px;background: transparent linear-gradient(#FBFBFB,#ECECEC,#CECECE) repeat scroll 0% 0% !important;" class="form-control">
                            <option value="01" <?php if(date('F') =='January'){ echo 'selected'; }?>>January</option>
                            <option value="02" <?php if(date('F') =='February'){ echo 'selected'; }?>>February</option>
                            <option value="03" <?php if(date('F') =='March'){ echo 'selected'; }?>>March</option>
                            <option value="04" <?php if(date('F') =='April'){ echo 'selected'; }?>>April</option>
                            <option value="05" <?php if(date('F') =='May'){ echo 'selected'; }?>>May</option>
                            <option value="06" <?php if(date('F') =='June'){ echo 'selected'; }?>>June</option>
                            <option value="07" <?php if(date('F') =='July'){ echo 'selected'; }?>>July</option>
                            <option value="08" <?php if(date('F') =='August'){ echo 'selected'; }?>>August</option>
                            <option value="09" <?php if(date('F') =='September'){ echo 'selected'; }?>>September</option>
                            <option value="10" <?php if(date('F') =='October'){ echo 'selected'; }?>>October</option>
                            <option value="11" <?php if(date('F') =='November'){ echo 'selected'; }?>>November</option>
                            <option value="12" <?php if(date('F') =='December'){ echo 'selected'; }?>>December</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <div style="margin-right: 7px;margin-top: 7px;float: left;">From</div>
                            <input type="text" name="from_period" id="from_period" class="form-control placeholder hasDatepicker_from_ds" style="float: left;background: transparent linear-gradient(#FBFBFB,#ECECEC,#CECECE) repeat scroll 0% 0% !important; width: 86%;" placeholder="From Date">
                    </div>
                    <div class="col-md-2">
                        <div style="margin-right: 7px;margin-top: 7px;float: left;">To</div>
                            <input type="text" name="to_period" id="to_period" class="form-control placeholder hasDatepicker" style="float: left;background: transparent linear-gradient(#FBFBFB,#ECECEC,#CECECE) repeat scroll 0% 0% !important;width: 90%;" placeholder="To Date">
                    </div>
                    <div class="col-md-1" style="text-align: left;">
                            <input class="btn btn-default" type="submit" name="search_by" value="Search" >
                    </div>


                    </div>
                </div>
            <?php $this->load->view('admin/dashboard_graph_row1');?>
            <?php $this->load->view('admin/dashboard_graph_row2');?>
            <?php $this->load->view('admin/dashboard_graph_row3');?>
            <?php $this->load->view('admin/dashboard_graph_row4');?>
            <?php $this->load->view('admin/dashboard_graph_row7');?>
            <?php $this->load->view('admin/dashboard_graph_row5');?>
            <?php $this->load->view('admin/dashboard_graph_row6');?>
        </form>

    </section>
    <!--/.content-->
</div>
<!--/.span9-->
</div>
<!--/.container-->
</section>
<?php //$this->load->view('admin/dashboard_graph_js'); ?>
<?php $this->load->view('admin/dashboard_js'); ?>
