<style type="text/css">
    input.btn {
        min-height: 28px;
        border: 1px solid #bbb;
        min-width: 80px;
    }
    form > fieldset > section {
        padding-top: 5px;
        padding-bottom: 5px;
        width: 48%;
        float: left;
    }
    form > fieldset > section input,
    form> fieldset >section select {
        width: 70%;
        margin: 0 auto;
        display: inline-block;
        height: 40px;
    }
    #form-ttl {
        font-size: 18px;
        text-align: center;
        padding-top: 10px;
    }
    .form-btm {
        float: right;
        width: 100%;
        margin-top: 10px;
    }
    .listDiv {
        min-height: 700px;
    }
    .dataTable td {
        min-width: 50px;
    }
    .filter-group {
        background: linear-gradient(to bottom, #44c0e5 0%, #35add1 39%, #0d85a8 100%) !important;
        height: 45px;
        margin: 0px 0px 20px 10px;
        border-radius: 4px;
        border: solid 1px #efefef;
        padding: 5px;
        text-align: center;
    }
</style>

<section id="content">
    <?php $this->load->view('admin/common/breadcrumbs'); ?>
    <div class="row">
        <div class="col-md-12">

            <!--<h1>First Board</h1>-->
            <form id="formone" method="post" action="<?php echo base_url('admin/dispatch_reports'); ?>" style="display: inline;">
                <table class="table table-borderless">
                    <tbody>
                    <tr>
                        <td>
                            <div class="filter-label">
                                <div>Filter By</div>
                            </div>
                        </td>
                        <td>
                            <input type="hidden" name="type" value="search">
                            <select class="form-control drivers-list" name="select_driver">
                                <option value="All drivers">All drivers</option>
                                <?php if(isset($drivers)){ foreach ($drivers as $value){ ?>
                                    <option value="<?php echo $value['driver']; ?>" <?php if($searched_driver== $value[ 'driver']){ echo "selected" ;} ?> >
                                        <?php echo $value[ 'driver']; ?> </option>
                                <?php } } ?>
                            </select>
                        </td>
                        <td>

                        </td>
                        <td>
                            <select class="form-control cars-list" name="select_car">
                                <option value="All cars">All cars</option>
                                <?php if(isset($cars)): foreach ($cars as $value):?>
                                    <option value="<?php echo $value['car']; ?>" <?php if($searched_car==$value[ 'car']){ echo "selected" ;} ?> >
                                        <?php echo $value[ 'car']; ?> </option>
                                <?php endforeach; endif;?>

                            </select>
                        </td>
                        <td>
                            <select class="form-control clients-list" name="select_client">
                                <option value="All clients">All clients</option>
                                <?php if(isset($clients)): foreach ($clients as $value):?>
                                    <option value="<?php echo $value['client']; ?>" <?php if($searched_client== $value[ 'client']){ echo "selected" ;} ?> >
                                        <?php echo $value[ 'client']; ?> </option>
                                <?php endforeach; endif;?>

                            </select>
                        </td>
                        <td>
                            <input type="text" name="from_period" class="datetimepicker datetimepicker2" placeholder="From date" value="<?php if(isset($searched_from)){ echo $searched_from; }else{echo "2016/12/1 05:03";} ?>"/> <span style="float: left;padding: 3px;padding-top: 10px">to</span>
                        </td>
                        <td>
                            <input type="text" name="to_period" class="datetimepicker" placeholder="To date" value="<?php if(isset($searched_to)){ echo $searched_to; }else{echo "2028/1/1 00:00";} ?>"/>
                        </td>
                        <td>
                            <select name="status" class="form-control">
                                <option value="All status" <?php if($searched_status=="All status" ){ echo "selected" ;} ?>> All status </option>
                                <option value="done" <?php if($searched_status=="done" ){ echo "selected" ;} ?>>done</option>
                                <option value="pending" <?php if($searched_status=="pending" ){ echo "selected" ;} ?>>pending</option>
                                <option value="confirmed" <?php if($searched_status=="confirmed" ){ echo "selected" ;} ?>>confirmed</option>
                                <option value="cancelled" <?php if($searched_status=="cancelled" ){ echo "selected" ;} ?>>cancelled</option>
                            </select>
                        </td>
                        <td>
                            <input class="btn" type="submit" id="search_by" value="Search"/>
                        </td>
                    </tr>
                    </tbody>
                </table>
        </div>

        <div class="col-md-4">

            <div class="page-action">

                <a class="btn"><span class="add-icon"> Add</span></a>&nbsp;
                <a class="btn" ><span class="edit-icon">  Edit </span> </a>&nbsp;
                <a class="btn" ><span class="delete-icon">Delete    </a>&nbsp;
                <a class="btn"><span class="csv-icon">   Export CSV</a>


            </div>

        </div>

    </div>
    <table class="table table-bordered data-table dataTable">
        <input type="hidden" class="clicked-btn" value="0">
        <thead>

        <tr>

            <th>
                <!--input type="checkbox" name="all"/-->
            </th>

            <th>ID#</th>
            <th>Date</th>
            <th>Client name</th>
            <th>Driver name</th>
            <th>Status</th>

            <th>Planned Start time   </th>
            <th>Real Start time      </th>
            <th>Difference </th>

            <th>Planned Arrival time </th>
            <th>Real Arrival time    </th>
            <th>Difference</th>

            <th>Planned Pickup time  </th>
            <th>Real Pickup time     </th>
            <th>Difference</th>

            <th>Planned Dropoff time </th>
            <th>Real Dropoff time    </th>
            <th>Difference </th>
        </tr>

        </thead>
        <tbody>
        <tr>
            <?php if(isset($rides)): ; foreach ($rides as $key=> $value):  ?>
            <td style="min-width: 0px;width: 20px">
                <input class="chk-bx" type="checkbox" data-input="<?php echo $value['id']; ?>" />
            </td>
            <td style="min-width: 0px;width: 20px"> <?php echo $key + 1;?> </td>
            <td> <?php $p = explode(" ", $value['start_time']); echo $p[0] ?>  </td>
            <td> <?php echo $value[ 'client'];?>  </td>
            <td> <?php echo $value[ 'driver'];?>  </td>
            <td> <?php echo $value[ 'status'];?>   </td>

            <td><?php $p = new DateTime($value['start_time']); echo $p->format('H:i'); ?></td>
            <td><?php $q = new DateTime($value['actual_start_time']);echo $q->format('H:i'); ?></td>
            <td style="color:white;background-color:<?php if($q > $p){ echo "red";}else{ echo "green"; } ?>">
                <?php $i = $p->diff($q); echo $i->h . ":" .$i->i; ?>
            </td>

            <td><?php   $r = new DateTime($value[ 'arrival_time']); echo $r->format('H:i');; ?> </td>
            <td > <?php $s = new DateTime($value[ 'actual_arrival_time']); echo $s->format('H:i');; ?></td>
            <td style="color:white;background-color:<?php if($s > $r){ echo "red";}else{ echo "green"; } ?>">
                <?php $i = $r->diff($s); echo $i->h . ":" .$i->i; ?>
            </td>

            <td><?php $t = new DateTime($value['pickup_time']);         echo $t->format('H:i');; ?></td>
            <td><?php $u = new DateTime($value[ 'actual_pickup_time']); echo $u->format('H:i');; ?></td>
            <td style="color:white;background-color:<?php if($u > $t){ echo "red";}else{ echo "green"; } ?>">
                <?php $i = $t->diff($u); echo $i->h . ":" .$i->i; ?>
            </td>

            <td><?php $v = new DateTime($value[ 'dropoff_time']); echo $v->format('H:i'); ?></td>
            <td><?php $w = new DateTime($value[ 'actual_dropoff_time']); echo $w->format('H:i'); ?></td>
            <td style="color:white;background-color:<?php if($w > $v){ echo "red";}else{ echo "green"; } ?>">
                <?php $i = $v->diff($w); echo $i->h . ":" .$i->i; ?>
            </td>

        </tr>

        <?php $sl= $key + 1; endforeach; endif;?>

        </tbody>

        <tfoot>

        <tr>
            <th>Total</th>

            <th>
                <?php printf( "%02d", $sl);?>
            </th>

            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>


        </tr>

        </tfoot>

    </table>

</section>
<!-- end div #content -->

<script>
    $(document).ready(function() {

        //   $(".dataTables_length").hide();

        // $(".dataTables_filter").hide();

        $("#from_period").wl_Date({
            dateFormat: 'dd/mm/yy'
        });

        $("#to_period").wl_Date({
            dateFormat: 'dd/mm/yy'
        });

    });
</script>




<style>
    #locationField,
    #controls {
        position: relative;
        width: 480px;
    }
    .label {
        text-align: right;
        font-weight: bold;
        width: 100px;
        color: #303030;
    }
    #address {
        border: 1px solid #000090;
        background-color: #f0f0ff;
        width: 480px;
        padding-right: 2px;
    }
    #address td {
        font-size: 10pt;
    }
    .field {
        width: 99%;
    }
    .slimField {
        width: 80px;
    }
    .wideField {
        width: 200px;
    }
    #locationField {
        height: 20px;
        margin-bottom: 2px;
    }
    .save-icon {}
</style>

<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/stylesheets/jquery.datetimepicker.css" />

<script src="<?php echo base_url() ?>assets/jsnew/jquery.datetimepicker.full.js"></script>
<script>
    $.datetimepicker.setLocale('en');

    $('.datetimepicker').datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        disabledDates: ['1986/01/08', '1986/01/09', '1986/01/10'],
        startDate: '2016-12-1'
    });
    var date = '2016/12/15 05:03';
    $('.datetimepicker').datetimepicker({
        step: 10
    });
    //$('.datetimepicker1').datetimepicker({value: date});
    //$('.datetimepicker2').datetimepicker({value:'2016/12/1 05:03'});
</script>