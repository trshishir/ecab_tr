
<script src='<?php echo base_url(); ?>assets/jsnew/dhtmlxscheduler.js' type="text/javascript" charset="utf-8">
</script>
<script src='<?php echo base_url(); ?>assets/jsnew/dhtmlxscheduler_timeline.js' type="text/javascript" charset="utf-8"></script>
<script src='<?php echo base_url(); ?>assets/jsnew/dhtmlxscheduler_tooltip.js' type="text/javascript" charset="utf-8"></script>
<script src='<?php echo base_url(); ?>assets/jsnew/api.js' type="text/javascript" charset="utf-8"></script>

<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>assets/stylesheets/dhtmlxscheduler.css'>


<style type="text/css" media="screen">
    html,
    body {
        margin: 0;
        padding: 0;
        height: 100%;
    }
    table td,
    table th {
        border: 0;
        padding: 0;
        text-align: inherit;
    }
    #scheduler_here table {
        background: inherit;
    }
    .xdsoft_datetimepicker{
        display: none !important;
    }
    /* ADDED */
    
    .dhx_cal_event div.dhx_footer,
    .dhx_cal_event.past_event div.dhx_footer,
    .dhx_cal_event.event_2 div.dhx_footer,
    .dhx_cal_event.event_0 div.dhx_footer,
    .dhx_cal_event.event_1 div.dhx_footer,
    .dhx_cal_event.event_3 div.dhx_footer {
        background-color: transparent !important;
    }
    .dhx_cal_event .dhx_body {
        -webkit-transition: opacity 0.1s;
        transition: opacity 0.1s;
        opacity: 0.7;
    }
    .dhx_cal_event .dhx_title {
        line-height: 12px;
    }
    .dhx_cal_event_line:hover,
    .dhx_cal_event:hover .dhx_body,
    .dhx_cal_event.selected .dhx_body,
    .dhx_cal_event.dhx_cal_select_menu .dhx_body {
        opacity: 1;
    }
    /* DONE*/
    
    .dhx_cal_event.event_done div,
    .dhx_cal_event_line.event_done {
        background-color: blue;
        !important;
        border-color: #aad !important;
    }
    .dhx_cal_event_clear.event_done {
        color: blue !important;
    }

/* CONFIRMED*/

.dhx_cal_event.event_confirmed div,
.dhx_cal_event_line.event_confirmed {
    background-color: green !important;
    border-color: #ada !important;
}
.dhx_cal_event_clear.event_confirmed{
    color: green !important;
}



/* CANCELLED*/

.dhx_cal_event.event_cancelled div,
.dhx_cal_event_line.event_cancelled {
    background-color: red !important;
    border-color: #daa !important;
}
.dhx_cal_event_clear.event_cancelled {
    color: red !important;
}
/*PENDING */

.dhx_cal_event.event_pending div,
.dhx_cal_event_line.event_pending {
    background-color: orange !important;
    border-color: #dda !important;

    background-image: -webkit-gradient(linear, left top, left bottom, from(#ef9500), to(#ffa500));
}
.dhx_cal_event_clear.event_pending {
    color: orange !important;
}

/*PENDING 

.dhx_cal_event.event_confirmed div,
.dhx_cal_event_line.event_confirmed {
    background-color: green !important;
    border-color: #add !important;
}
.dhx_cal_event_clear.event_confirmed{
    color: green !important;
}*/

form {
    display: inline;
}
.driver-icn,
.car-icn {
    width: 30px;
    height: 30px;
}
.driver-name,
.car-name {
    font-size: 13px;
}
.dhx_matrix_cell,
.dhx_matrix_scell {
    overlow: hidden;
    padding-left: 5px;
    text-align: left;
    vertical-align: middle;
}
.dhx_matrix_scell{

    width: 169px !important;
}
.type-btn {
    display: inline;
    float: left;
}
.type-btn .bitn.active {
    background-color: green;
    color: white;
}
.bitn {
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.42857143;
    text-align: center;
    vertical-align: middle;
    cursor: pointer;
    height: 35px;
    user-select: none;
    background: white;
    border: 1px solid grey;
    border-radius: 4px;
}
.bitn:hover {
    color: #333;
}
.dhx_month_body {
    height: 30px;
}

    td.dhx_matrix_cell, td.dhx_matrix_cell>div{
        width: 45px !important;
    }

    div.dhx_cal_navline{
        padding-right: 20px;
    padding-top: 2px;
    }
    .dhx_cal_container .dhx_cal_data>table>tbody>tr>td:first-child{
            background-color: #e1e1e1;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#e2e2e2), to(#ffffff));
    }
    .dhx_matrix_cell{
        background-color: #f4f4f4;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#ffffff));
    }

</style>


<script type="text/javascript" charset="utf-8">
    function init() {


        scheduler.templates.tooltip_date_format = scheduler.date.date_to_str("%Y-%m-%d %H:%i");
        scheduler.locale.labels.timeline_tab = "Timeline";
        scheduler.locale.labels.section_custom = "Section";
        scheduler.config.details_on_create = true;
        scheduler.config.details_on_dblclick = true;
        scheduler.config.xml_date = "%Y-%m-%d %H:%i";
        scheduler.xy.scale_height = 30; //height of top nav
        scheduler.xy.bar_height = 45; //height of event bar
        scheduler.config.active_link_view = "week"; //where we'll jump from the Month view
        //scheduler.xy.min_event_height = 10;
        scheduler.config.max_month_events = 2;
        scheduler.config.readonly = true; /* ADDITION */
        scheduler.locale.labels.section_status = "status";

        var format = scheduler.date.date_to_str("%Y-%m-%d %H:%i");
        scheduler.templates.tooltip_text = function(start, end, event) {
            return "<b>Event:</b> " + event.text + "<br/><b>Driver:</b> " +
                event.driver + "<br/><b>Client:</b> " + event.client + "<br/><b>Car:</b> " + event.car + "<br/><b>Start date:</b> " +
                format(start) + "<br/><b>End date:</b> " + format(end);
        };

        scheduler.templates.event_class = function(start, end, event) {
            var css = "";

            if (event.status) // if event has status property then special class should be assigned
                css += "event_" + event.status;

            if (event.id == scheduler.getState().select_id) {
                css += " selected";
            }
            return css; // default return
        };

        //===============
        //Configuration
        //===============
        var drivers = <?php echo json_encode($mod_drivers); ?> ;
        var cars = <?php echo json_encode($mod_cars); ?> ;

        scheduler.createTimelineView({
            name: "timeline",
            x_unit: "hour",
            x_date: "%H:%i",
            x_step: 1,
            x_size: 24,
            x_start: 0,
            //x_length: 48,
            <?php
            if ($display_type == "drivers") { ?>
                y_unit: drivers,
                    y_property: "driver", 
          <?php
            } else { ?>
                y_unit: cars,
                    y_property: "car", <?php
            } ?>
            render: "bar",
            section_autoheight: false,
            fit_events: true,
            dy: 50,
            dx: 160,
            second_scale: {
                x_unit: "day",
                x_date: "%M"
            }

        });


        //===============
        //Data loading
        //===============
        //    scheduler.config.readonly = true;    
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth()).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();  
        scheduler.config.lightbox.sections = [{
            name: "description",
            height: 130,
            map_to: "client",
            type: "textarea",
            focus: true
        }, {
            name: "custom",
            height: 23,
            type: "select",
            options: status,
            map_to: "status"
        }, {
            name: "time",
            height: 72,
            type: "time",
            map_to: "auto"
        }];
        scheduler.init('scheduler_here', new Date(yyyy, mm, dd), "timeline");
        var rides = <?php echo json_encode($rides); ?> ;
        scheduler.parse(rides, "json");
    }
</script>
<script type="text/javascript">
    $(document).ready(function() { <?php
        if ($display_type == "drivers") { ?>
            showDrivers('drivers'); <?php
        } else { ?>
            showCars('cars'); <?php
        } ?>

        $('.type-btn button').click(function() {
            var id = $(this).prop('id');
            if (id == "cars") {
                showCars(id);
            } else {
                showDrivers(id);
            }
        });
    });

    function showCars(id) {
        $('.type-btn button').removeClass('active');
        $('#formone input.cate').prop('name', '');
        $('#formtwo input.cate').prop('name', '');
        $('.type-btn #' + id).addClass('active');
        $('#formone input.' + id + '-input').prop('name', 'cat');
        $('.drivers-list').hide();
        $('.cars-list').show();
    }

    function showDrivers(id) {
        $('.type-btn button').removeClass('active');
        $('#formone input.cate').prop('name', '');
        $('#formtwo input.cate').prop('name', '');
        $('.type-btn #' + id).addClass('active');
        $('#formone input.' + id + '-input').prop('name', 'cat');
        $('.cars-list').hide();
        $('.drivers-list').show();
    }
    function alertDriver(){
    
   <?php $a = ''; foreach($rides as $v){ $a = $a.'|'.$v['id']; }  $p = explode("|", $a);  ?>
   <?php for($i = 0;$i < count($p);$i++){?>
        //alert(<?php echo $p[$i] ?>);
    <?php }  ?>
        var val = '<?php echo $a; ?>';
        $.ajax({  
            type: 'POST',  
            url: 'http://ecab.app/admin/alertDriver', 
            data: { id: val },
dataType: 'json',
            success: function(response) {
                alert("Email successfully sent");

                //var response = JSON.parse(response);
                console.log(response.status);
            }
        });        
    }
    function printPdf(){
    <?php 
     foreach ($rides as $key => $value) { $values = array_keys($value);}
     ?> 
    //  var val   = '<?php echo serialize($values); ?>' ;
      var arr   = '<?php echo serialize($rides);  ?>' ;
      var head  = '<?php echo serialize($values); ?>' ;
      $.ajax({  
            type: 'POST',  
            url: 'http://ecab.app/admin/printPdf', 
            data: { head: head, body: arr },
            dataType: 'json',
            success: function(response) {
                //alert("Email successfully sent");
                console.log(response.status);
            }
        });   

     } 

    function printExcel(){
    <?php 
     foreach ($rides as $key => $value) { $values = array_keys($value);}
     ?> 
    //  var val   = '<?php echo serialize($values); ?>' ;
      var arr   = '<?php echo serialize($rides);  ?>' ;
      var head  = '<?php echo serialize($values); ?>' ;
      $.ajax({  
            type: 'POST',  
            url: 'http://ecab.app/admin/printExcel', 
            data: { head: head, body: arr },
            dataType: 'json',
            success: function(response) {
                //alert("Email successfully sent");
                console.log(response.status);
            }
        });   

     } 

     function typeOf (obj) {
  return {}.toString.call(obj).split(' ')[1].slice(0, -1).toLowerCase();
}
</script>
</head>
<section id="content" style="height:100%;width: 100%;display: table;">

    <div class="container" style="height: 100%;width: 100%;padding-left: 15px">

       <?php $this->load->view('admin/common/breadcrumbs');?>
        <?php $this->load->view('admin/common/alert');?>
        <?php echo $this->session->flashdata('message'); ?>

        <div class="col-md-12 g12" style="height: 90%;width:99%; padding-right: 27px">


            <div class="filter-group" style="margin:0;">

                <div class="col-md-1">
                    <div class="filter-label">
                        <div>Filter By</div>
                    </div>
                </div>
                <script type="text/javascript">
                </script>
                <div class="col-md-11" >
                    <div class="type-btn">
                        <button id="drivers" class="bitn active">
                            <img style="height:25px;width:25px;" src='http://ecab.app/images/driver.png'>
                        </button>
                        <button id="cars" class="bitn">
                            <img style="height:25px;width:25px;" src='http://ecab.app/images/car.png'>
                        </button>&nbsp;&nbsp;
                    </div>

                    <form id="formone" method="post" action="<?php echo base_url('admin/dispatch_planning'); ?>" style="float:left;display:table;width: 47%">
                        <input type="hidden" name="cat" value="drivers" class="drivers-input cate">
                        <input type="hidden" name="" value="cars" class="cars-input cate">
                        <input type="hidden" name="type" value="sort">
                        
                            <input type="text" name="from_period" class="bsearchdatepicker text-center" style="width: 16%;float:left; " placeholder="From date" value="<?php if(isset($searched_from)){ echo $searched_from; }else{echo date('d/m/Y');} ?>"/> <span style="float: left;padding: 3px;padding-top: 10px">to</span>

                            <input type="text" name="to_period" class="bsearchdatepicker text-center" style="width: 16%;float:left; " placeholder="To date" value="<?php if(isset($searched_to)){ echo $searched_to; }else{echo date('d/m/Y');;} ?>"/>


                        <select style="float: left;width:16%;" class="form-control drivers-list" name="select_driver">
                            <option value="All drivers">All drivers</option>
                            <?php if(isset($drivers)){ foreach ($drivers as $value){ ?>
                            <option value="<?php echo $value['driver']; ?>" <?php if($searched_driver== $value[ 'driver']){ echo "selected" ;} ?> >
                                <?php echo $value[ 'driver']; ?> </option>
                            <?php } } ?>
                        </select>&nbsp;&nbsp;

                        <select style="float:left;width:16%;" class="form-control cars-list" name="select_car">
                            <option value="All cars">All cars</option>
                            <?php if(isset($cars)): foreach ($cars as $value):?>
                            <option value="<?php echo $value['car']; ?>" <?php if($searched_car==$value[ 'car']){ echo "selected" ;} ?> >
                                <?php echo $value[ 'car']; ?> </option>
                            <?php endforeach; endif;?>

                        </select>&nbsp;&nbsp;

                        <select style="float:left;width:17%;" class="form-control clients-list" name="select_client">
                            <option value="All clients">All clients</option>
                            <?php if(isset($clients)): foreach ($clients as $value):?>
                            <option value="<?php echo $value['client']; ?>" <?php if($searched_client== $value[ 'client']){ echo "selected" ;} ?> >
                                <?php echo $value[ 'client']; ?> </option>
                            <?php endforeach; endif;?>

                        </select>&nbsp;&nbsp;

                        <select style="float:left;width:16%;" class="form-control status-list" name="select_status">
                            <option value="All status">All status</option>
                            <option value="done" <?php if($searched_status=='done' ){ echo "selected" ;} ?> >done </option>
                            <option value="confirmed" <?php if($searched_status=='confirmed' ){ echo "selected" ;} ?> >confirmed </option>
                            <option value="cancelled" <?php if($searched_status=='cancelled' ){ echo "selected" ;} ?> >cancelled </option>
                            <option value="pending" <?php if($searched_status=='pending' ){ echo "selected" ;} ?> >pending </option>
                        </select>&nbsp;&nbsp;

                        <input class="btn" type="submit" value="View" style="float:left" />
                        <!--a class="btn" style="float: left" href="http://ecab.app/admin/dispatch/planning.php"> Reset </a-->
                    </form>
                    <form id="formtwo" action="<?php echo base_url('admin/dispatch_planning'); ?>" method="post" style="float:left;display:table;width: 16%">
                        <input type="hidden" name="cat" value="drivers" class="drivers-input cate">
                        <input type="hidden" name="type" value="search">
                        <input type="text" name="search_word" id="search-word" class="form-contrl" value="" style="width: 60%;float:left; " placeholder="Search keyword" />&nbsp;&nbsp;
                        <input type="submit" class="btn" value="Search" style="float:left" />
                    </form>
                    <a class="btn" onclick="printPdf()"> <span class="mail-icon klicked-btn"> <i class="fa fa-file-pdf-o"></i> Export pdf </span> </a>
                    <a class="btn" onclick="printExcel()"> <span class="mail-icon klicked-btn"> <i class="fa fa-file-excel-o"></i> Export excel </span> </a>
                    <a class="btn" onclick="alertDriver()" ><span class="mail-icon klicked-btn"> <i class="fa fa-clipboard"></i>  Send Email </span> </a>

                </div>

            </div>

<style type="text/css">
</style>



            <body onload="init();">
                <div id="scheduler_here" class="dhx_cal_container" style='width:100%; height:100%;'>
                    <div class="dhx_cal_navline">
                        <div class="dhx_cal_next_button" style="float: right;">&nbsp;</div>
                        <div class="dhx_cal_prev_button" style="float: right;">&nbsp;</div>
                        <div class="dhx_cal_today_button" style="float: right;"></div>&nbsp;&nbsp;&nbsp;&nbsp;
                        <div class="dhx_cal_date"  style="left:50%;position: absolute;"></div>
                        <div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div>
                        <div class="dhx_cal_tab" name="week_tab" style="right:140px;"></div>
                        <div class="dhx_cal_tab" name="month_tab" style="right:76px;"></div>
                        <div class="dhx_cal_tab" name="timeline_tab" style="right:280px;"></div>
                    </div>
                    <div class="dhx_cal_header">
                    </div>
                    <div class="dhx_cal_data">
                    </div>
                </div>
            </body>


        </div>

    </div>

</section>

<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }
    .switch input {
        display: none;
    }
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }
    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }
    input:checked + .slider {
        background-color: #2196F3;
    }
    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }
    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }
    /* Rounded sliders */
    
    .slider.round {
        border-radius: 34px;
    }
    .slider.round:before {
        border-radius: 50%;
    }
</style>

<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>stylesheets/jquery.datetimepicker.css" />

<script src="<?php echo base_url() ?>assets/jsnew/jquery.datetimepicker.full.js"></script>
<script>
    $.datetimepicker.setLocale('en');

    $('.datetimepicker').datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        disabledDates: ['1986/01/08', '1986/01/09', '1986/01/10'],
        startDate: '2016-12-1'
    });
    var date = '2016/12/15 05:03';
    $('.datetimepicker').datetimepicker({
        step: 10
    });
    //$('.datetimepicker1').datetimepicker({value: date});
    //$('.datetimepicker2').datetimepicker({value:'2016/12/1 05:03'});
</script>
