<style type="text/css">
    input.btn {
        min-height: 28px;
        border: 1px solid #bbb;
        min-width: 80px;
    }
    form > fieldset > section {
        padding-top: 5px;
        padding-bottom: 5px;
        width: 48%;
        float: left;
    }
    form > fieldset > section input,
    form> fieldset >section select {
        width: 70%;
        margin: 0 auto;
        display: inline-block;
        height: 40px;
    }
    #form-ttl {
        font-size: 18px;
        text-align: center;
        padding-top: 10px;
    }
    .form-btm {
        float: right;
        width: 100%;
        margin-top: 10px;
    }
    .listDiv {
        min-height: 700px;
    }
    .dataTable td {
        min-width: 90px;
    }
    .filter-group {
        background: linear-gradient(to bottom, #44c0e5 0%, #35add1 39%, #0d85a8 100%) !important;
        height: 45px;
        margin: 0px 0px 20px 10px;
        border-radius: 4px;
        border: solid 1px #efefef;
        padding: 5px;
        text-align: center;
    }
</style>

<section id="content" style="display: table;">

    <div class="container-fluid">

        <div id="breadcrumb">

            <a href="<?php echo base_url();?>index.php?admin/dashboard" class="tip-bottom" data-original-title="Go to Home"><i class="fa fa-home"></i> Home</a>

            <a href="<?php echo base_url();?>index.php?admin/manage_board1" class="current">
                <?php echo $page_title?>
            </a>

        </div>

<!--        <div class="col-md-12">-->

            <!--<h1>First Board</h1>-->


            <div class="listDiv">
                <div class="filter-group">

                    <div class="col-md-1" style="padding-left: 0px">

                        <div class="filter-label">

                            <div>Filter By</div>


                        </div>

                    </div>
                    <div class="col-md-7" style="padding-left: 0px;">

<!--                    <div class="col-md-7" style="padding-left: 0px;">-->

                        <form id="formone" method="post" action="<?php echo base_url('admin/dispatch_reports'); ?>" style="display: inline;">
                            <input type="hidden" name="type" value="search">

                            <!--input type="text" name="search-word" id="search-word" class="form-control" style="width: 20%;float:left; " placeholder="Search keyword" value="<?php //if(isset($searched_word)){ echo $searched_word; } ?>" /-->
                            <input type="text" name="from_period" class="datetimepicker datetimepicker2" style="width: 13%;float:left; " placeholder="From date" value="<?php if(isset($searched_from)){ echo $searched_from; }else{echo "2016/12/1 05:03";} ?>"/> <span style="float: left;padding: 3px;padding-top: 10px">to</span>

                            <input type="text" name="to_period" class="datetimepicker" style="width: 13%;float:left; " placeholder="To date" value="<?php if(isset($searched_to)){ echo $searched_to; }else{echo "2028/1/1 00:00";} ?>"/>


                        <select style="float: left;width:13%;" class="form-control drivers-list" name="select_driver">
                            <option value="All drivers">All drivers</option>
                            <?php if(isset($drivers)){ foreach ($drivers as $value){ ?>
                            <option value="<?php echo $value['driver']; ?>" <?php if($searched_driver== $value[ 'driver']){ echo "selected" ;} ?> >
                                <?php echo $value[ 'driver']; ?> </option>
                            <?php } } ?>
                        </select>&nbsp;&nbsp;

                        <select style="float:left;width:13%;" class="form-control cars-list" name="select_car">
                            <option value="All cars">All cars</option>
                            <?php if(isset($cars)): foreach ($cars as $value):?>
                            <option value="<?php echo $value['car']; ?>" <?php if($searched_car==$value[ 'car']){ echo "selected" ;} ?> >
                                <?php echo $value[ 'car']; ?> </option>
                            <?php endforeach; endif;?>

                        </select>&nbsp;&nbsp;

                        <select style="float:left;width:13%;" class="form-control clients-list" name="select_client">
                            <option value="All clients">All clients</option>
                            <?php if(isset($clients)): foreach ($clients as $value):?>
                            <option value="<?php echo $value['client']; ?>" <?php if($searched_client== $value[ 'client']){ echo "selected" ;} ?> >
                                <?php echo $value[ 'client']; ?> </option>
                            <?php endforeach; endif;?>

                        </select>&nbsp;&nbsp;

                            <select name="status" style="float: left;width:11%;" class="form-control">
                                <option value="All status" <?php if($searched_status=="All status" ){ echo "selected" ;} ?>> All status </option>
                                <option value="done" <?php if($searched_status=="done" ){ echo "selected" ;} ?>>done</option>
                                <option value="pending" <?php if($searched_status=="pending" ){ echo "selected" ;} ?>>pending</option>
                                <option value="confirmed" <?php if($searched_status=="confirmed" ){ echo "selected" ;} ?>>confirmed</option>
                                <option value="cancelled" <?php if($searched_status=="cancelled" ){ echo "selected" ;} ?>>cancelled</option>
                            </select>

                            <input class="btn" type="submit" id="search_by" value="Search" style="float:left;width: 8%;" />
                        </form>
                    </div>
<!--                    </div>-->

                    <div class="col-md-4">

                        <div class="page-action">

                            <a class="btn"><span class="add-icon"> Add</span></a>&nbsp;
                            <a class="btn" ><span class="edit-icon">  Edit </span> </a>&nbsp;
                            <a class="btn" ><span class="delete-icon">Delete    </a>&nbsp;
                            <a class="btn"><span class="csv-icon">   Export CSV</a>
                            

                        </div>

                    </div>

                </div>
                <table class="table table-bordered data-table dataTable">
                    <input type="hidden" class="clicked-btn" value="0">
                    <thead>

                        <tr>

                            <th>
                                <!--input type="checkbox" name="all"/-->
                            </th>

                            <th>ID#</th>
                            <th>Date</th>
                            <th>Driver name</th>
                            <th>Client name</th>
                            <th>Car </th>
                            <th>Status</th>
                            <th>Real Start time      </th>
                            <th>Real Arrival time    </th>
                            <th>Real Pickup time     </th>
                            <th>Waiting time     </th>
                            <th>Assistance time    </th>
                            <th>Real Dropoff time    </th>
                            <th>Waiting time     </th>
                            <th>Assistance time    </th>
                            <th>Total working time</th>

                       </tr>

                    </thead>
                    <tbody>
                        <tr>
                        <?php if(isset($rides)): ; foreach ($rides as $key=> $value):  ?>

                        <td>
                            <input class="chk-bx" type="checkbox" data-input="<?php echo $value['id']; ?>" />
                        </td>
                        <td style="width:20px"> <?php echo $key + 1;?> </td>
                        <td> <?php $p = explode(" ", $value[ 'start_time']); echo $p[0] ?>  </td>
                        <td> <?php echo $value[ 'driver'];?>  </td>
                        <td> <?php echo $value[ 'client'];?>  </td>
                        <td> <?php echo $value[ 'car'];?>  </td>
                        <td> <?php echo $value[ 'status'];?>   </td>
                        <td ><?php $p = new DateTime($value['actual_start_time']); echo $p->format('H:i'); ?></td>
                        <td > <?php $p = new DateTime($value['actual_arrival_time']); echo $p->format('H:i'); ?></td>
                        <td ><?php $p = new DateTime($value['actual_pickup_time']); echo $p->format('H:i'); ?></td>
                        <td> <?php echo $value['waiting_time_pickup']; ?>  </td>
                        <td> <?php echo $value['assistance_time_pickup'];?> </td>
                        <td ><?php $p = new DateTime($value['actual_dropoff_time']); echo $p->format('H:i');
                        $w1 = $value['waiting_time_pickup'];
                        $a1 = $value['assistance_time_pickup'];
                        $w2 = $value['waiting_time_end'];
                        $a2 = $value['assistance_time_dropoff'];
                        $s = strtotime($value['actual_start_time']);
                        $a = strtotime($value['actual_arrival_time']);
 
                        $p = strtotime($value['actual_pickup_time']);
                        $d = strtotime($value['actual_dropoff_time']);
                        
                        $i  = abs(($a - $s) + ($d - $p));
                        $dt = round($i / 60);
                      ?></td>


                        <td> <?php echo $value['waiting_time_end'] ?></td>
                        <td><?php echo $value['assistance_time_dropoff']; ?></td>

                        <td> <?php echo $dt + ($w1 / 2) +($a1 + $a2) + ($w2 / 4);  ?></td>
                                                </tr>

                        <?php $sl= $key + 1; endforeach; endif;?>

                    </tbody>

                    <tfoot>

                        <tr>
                            <th>Total</th>

                            <th>
                                <?php printf( "%02d", $sl);?>
                            </th>

                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>

                    </tfoot>

                </table>


            </div>

        </div>

<!--    </div>-->

</section>
<!-- end div #content -->

<script>
    $(document).ready(function() {

     //   $(".dataTables_length").hide();

       // $(".dataTables_filter").hide();

        $("#from_period").wl_Date({
            dateFormat: 'dd/mm/yy'
        });

        $("#to_period").wl_Date({
            dateFormat: 'dd/mm/yy'
        });

    });
</script>




<style>
    #locationField,
    #controls {
        position: relative;
        width: 480px;
    }
    .label {
        text-align: right;
        font-weight: bold;
        width: 100px;
        color: #303030;
    }
    #address {
        border: 1px solid #000090;
        background-color: #f0f0ff;
        width: 480px;
        padding-right: 2px;
    }
    #address td {
        font-size: 10pt;
    }
    .field {
        width: 99%;
    }
    .slimField {
        width: 80px;
    }
    .wideField {
        width: 200px;
    }
    #locationField {
        height: 20px;
        margin-bottom: 2px;
    }
    .save-icon {}
</style>

<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>stylesheets/jquery.datetimepicker.css" />

<script src="<?php echo base_url() ?>javascripts/jquery.datetimepicker.full.js"></script>
<script>
    $.datetimepicker.setLocale('en');

    $('.datetimepicker').datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        disabledDates: ['1986/01/08', '1986/01/09', '1986/01/10'],
        startDate: '2016-12-1'
    });
    var date = '2016/12/15 05:03';
    $('.datetimepicker').datetimepicker({
        step: 10
    });
    //$('.datetimepicker1').datetimepicker({value: date});
    //$('.datetimepicker2').datetimepicker({value:'2016/12/1 05:03'});
</script>