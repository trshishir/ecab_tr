<!-- STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/reset.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/typography.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/styles.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/github.css">
<style type="text/css">
    #map_canvas{
        height: 650px;
    }
    .sav{
        float: left;display: table;width: 14%;
    }
    .sav select{
        width: calc(100% - 50px);
    }
</style>

<section id="content">
    <div class="container">
        <div id="breadcrumb">
            <a href="<?php echo base_url();?>index.php?admin/dashboard" class="tip-bottom" data-original-title="Go to Home"><i class="fa fa-home"></i> Home</a>
            <a href="<?php echo base_url();?>indfex.php?admin/manage_board1" class="current"><?php echo $page_title?></a>
        </div>
        <div class="col-md-12 g12">
            <!--<h1>First Board</h1>-->
            <div class="filter-group" style="margin:0;">
                <div class="col-md-1">
                    <div class="filter-label">
                        <div>Filter By</div>
                    </div>
                </div>
                <script type="text/javascript">
                    $(document).ready(function(){
                        hideFilters();
                        $('.select_ca').show();

                        $('.filter_submit').click(function(){
                            $('#map_canvas').trigger('reset');
                            resetSelects('lsti');
                            var x = $(this).prop('id');
                            if(x == 0){
                                var y = $(this).prop('data-input');
                                $('#map_canvas').trigger('pickup', +y);
                            }else{
                                $('#map_canvas').trigger('show', +x);
                            }

                        });
                    });
                    function lsti(){
                        var x = document.getElementById("select_l").value;
                        var a = x.split('|');
                        resetSelects('not');
                        if(x == "all"){
                            showAll();
                        }else if(a[0] == "d"){
                            showD(a[1]);
                        }else if(a[0] == "c"){
                            showC(a[1]);
                        }
                    }
                    function showAll(){
                        $('#map_canvas').trigger('showAll');
                    }
                    function showD(x){
                        $('#map_canvas').trigger('showOne', x);
                    }
                    function showC(x){
                        $('#map_canvas').trigger('showOne', x);
                    }
                    function resetSelects(a){
                        if(a == "lsti"){
                            $(".fltrs select.lsti").prop('selectedIndex', 0);
                        }else{
                            $(".fltrs select:not('.lsti')").prop('selectedIndex', 0);
                        }
                    }

                    function hideFilters(){
                        $('.sd').hide();
                    }
                    function handleSelect(){
                        hideFilters();
                        var v = document.getElementById("select_type").value;
                        $('.'+v).show();
                    }
                    function drivers(){
                        var x = document.getElementById("select_d").value;
                        $('.filter_submit').attr('id', x);

                    }
                    function clients(){
                        var x = document.getElementById("select_c").value;
                        $('.filter_submit').attr('id', x);

                    }
                    function cars(){
                        var x = document.getElementById("select_ca").value;
                        $('.filter_submit').attr('id', x);

                    }
                    function rides(){
                        var x = document.getElementById("select_r").value;
                        $('.filter_submit').attr('id', 0);
                        $('.filter_submit').prop('data-input', x);

                    }
                    function poi(){
                        var x = document.getElementById("select_poi").value;
                        if(x!= "all"){$('#map_canvas').trigger('pOi', x);};

                    }
                    function extras(){
                        $('#map_canvas').trigger('reset');
                    }
                </script>
                <div class="col-md-11 fltrs">
                    <div class="yearDivs select_type">
                        <span style="float: left;padding-top: 10px">Category:</span>
                        <select name="select_type" style="float: left;width:14%;" class="form-control" id="select_type" onchange="handleSelect()">
                            <option value="select_ca">Cars    </option>
                            <option value="select_d"> Drivers </option>
                            <option value="select_c"> Clients </option>
                            <option value="select_r"> Rides   </option>
                        </select>
                    </div>
                    <div class="yearDivs select_d sd">
                        <span  style="float: left;padding-top: 10px">&nbsp;&nbsp;Status :</span>
                        <select name="select_d" style="float: left;width:14%;" class="form-control" id="select_d" onchange="drivers()">
                            <option>Sort drivers</option>
                            <option value="31">All drivers</option>
                            <option value="1">Available drivers</option>
                            <option value="2">Unavailable drivers</option>
                            <option value="3">Pending drivers</option>
                            <option value="4">Active drivers</option>
                            <option value="5">Inactive drivers</option>
                            <option value="6">Suspended drivers</option>
                        </select>
                    </div>
                    <div class="yearDivs select_c sd">
                        <span  style="float: left;padding-top: 10px">&nbsp;&nbsp;Status :</span>
                        <select name="select_c" style="float: left;width:14%;" class="form-control" id="select_c" onchange="clients()">
                            <option>Sort clients</option>
                            <option value="32">All clients</option>
                            <option value="7">Prospect clients</option>
                            <option value="8">Active clients</option>
                            <option value="9">Inactive clients</option>
                            <option value="10">Suspended clients</option>
                        </select>
                    </div>
                    <div class="yearDivs select_ca sd">
                        <span  style="float: left;padding-top: 10px">&nbsp;&nbsp;Status :</span>
                        <select name="select_ca" style="float: left;width:14%;" class="form-control" id="select_ca" onchange="cars()">
                            <option>Sort cars</option>
                            <option value="33">All cars</option>
                            <option value="11">Available cars</option>
                            <option value="12">Unavailable cars</option>
                            <option value="13">Inactive cars</option>
                        </select>
                    </div>
                    <div class="yearDivs select_r sd">
                        <span  style="float: left;padding-top: 10px">&nbsp;&nbsp;Status :</span>
                        <select name="year_lists" style="float: left;width:16%;" class="form-control" id="select_r" onchange="rides()">
                            <option>Sort rides</option>
                            <?php foreach ($rides as $key => $value) { ?>
                                <option value="<?php echo $key; ?>"> <?php echo $value['driver'].' - '.$value['client'] ;?> </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="yearDivs">
                        <input type="button" style="float: left;width:13%;" class="btn filter_submit" id="" value="View" />
                    </div>&nbsp;&nbsp;&nbsp;&nbsp;

                    <div class="yearDivs select_l   sav" >
                        <span  style="float: left;padding-top: 10px">&nbsp;&nbsp;List :</span>
                        <select name="year_lists" style="" class="form-control lsti" id="select_l" onchange="lsti()">
                            <option> Select list </option>
                            <option value="all"> All         </option>
                            <?php foreach ($rides as $key => $value) { ?>
                                <option value="d|<?php echo $value['driver']; ?>"> <?php echo $value['driver'];?> </option>
                            <?php } ?>
                            <?php foreach ($rides as $key => $value) { ?>
                                <option value="c|<?php echo $value['client']; ?>"> <?php echo $value['client'];?> </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="yearDivs sav">
                        <span  style="float: left;padding-top: 10px">&nbsp;&nbsp;POI :</span>
                        <select name="year_lists" style="" class="form-control" id="select_poi" onchange="poi()">
                            <option value="all"> Select Poi </option>
                            <option value="airport"> Airports </option>
                            <option value="park"> Parks </option>
                            <option value="train_station"> Train stations  </option>
                        </select>
                    </div>
                </div>
            </div>

            <div id="wrapper">
                <div id="map_canvas"></div>
            </div>



        </div>
    </div>
</section><!-- end div #content -->

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCy7MCFqdOTqf8PZlcAbXvuVnRhRg5v_TM&libraries=places">

</script>

<script type="text/javascript">


    (function($, window, undefined) {

        "use strict";

        $.fn.mapit = function(options) {

            var defaults = {
                latitude:    48.918245,
                longitude:    2.623316,
                zoom:            10,
                type:            'ROADMAP',
                scrollwheel: false,
                marker: {
                    latitude:    48.918245,
                    longitude:    2.623316,
                    icon:           'http://cab-booking-script.com/images/home.png',
                    title:          'The Headquarters',
                    open:           false,
                    center:         true
                },
                address: '<h2>The Headquarters</h2><p>Address 1, Ile de France<br />Paris, France</p><p>Tel.: +30 210 123 4567<br />Fax: +30 210 123 4567</p>',

            <?php


            echo "locations:[";  echo "\n";
            function hc($cord){
                $myArray = explode('|', $cord);
                return $myArray;
//long,lat
            }
            //Name, Adresse, Phone, Car immat
            function hS($a, $b){
                if($b == 1){switch ($a) {
                    case 'available':   return 1; break;
                    case 'unavailable': return 2; break;
                    case 'pending':     return 3; break;
                    case 'active':      return 4; break;
                    case 'inactive':    return 5; break;
                    case 'suspended':   return 6; break;
                    default:            return 0; break;
                }}
                if($b == 2){switch ($a) {
                    case 'prospect':    return 7; break;
                    case 'active':      return 8; break;
                    case 'inactive':    return 9; break;
                    case 'suspended':   return 10; break;
                    default:            return 0; break;
                }}if($b == 3){switch ($a) {
                    case 'available':   return 11; break;
                    case 'unavailable': return 12; break;
                    case 'inactive':    return 13; break;
                    default:            return 0; break;
                }}}
            function hI($a, $b){
                if($b == 1){ switch ($a) {
                    case 'suspended': case 'unavailable':
                        return 'http://cab-booking-script.com/images/closed-driver.png'; break;
                    case 'inactive': case 'pending': case 'available':
                        return 'http://cab-booking-script.com/images/inactive-driver.png'; break;
                    case 'active':
                        return 'http://cab-booking-script.com/images/active-driver.png'; break;
                    default:
                        return 0; break;
                }}
                elseif($b == 2){ switch ($a) {
                    case 'suspended':
                        return 'http://cab-booking-script.com/images/closed-client.png'; break;
                    case 'inactive':  case 'prospect':
                        return 'http://cab-booking-script.com/images/inactive-client.png'; break;
                    case 'active':
                        return 'http://cab-booking-script.com/images/active-client.png'; break;
                    default:
                        return 0; break;
                }}
                elseif($b == 3){ switch ($a) {
                    case 'inactive': case 'unavailable':
                        return 'http://cab-booking-script.com/images/inactive-car.png'; break;
                    case 'available':
                        return 'http://cab-booking-script.com/images/active-car.png'; break;
                    default:
                        return 0; break;
                }}
            }

            foreach ($rides as $key => $value) {
                $cord = hc($value['origin_cord']);
                $card = hc($value['destination_cord']);
//Name, Adresse, Phone, Car imm
                foreach ($drivers as $key => $v) {
                    if($value['driver'] == $v['civility'] . ' ' . $v['firstname'] . ' ' . $v['lastname'] ){ $driver = $v;}
                }
                $icn =  hI($driver['status'], 1);
                $stat = hs($driver['status'], 1);

                echo "[". $cord[1] .",".$cord[0] .", '".$icn."','".$value["driver"] ."','".$value["destination"]."',"."false".", ".$stat.",'".$value["car"]."','".$driver['phone']."','".$driver["address"]."','".$driver["email"]."']";

                echo ',';
                echo "\n";
            }

            foreach ($rides as $key => $value) {
                $cord = hc($value['origin_cord']);
                $card = hc($value['destination_cord']);
                foreach ($clients as $key => $v) {
                    if($value['client'] == $v['civility'] . ' ' . $v['firstname'] . ' ' . $v['lastname'] ){ $client = $v;}
                }
                $icn =  hI($client['status'], 2);
                $stat = hs($client['status'], 2);
//Name, adresse, phone, email
                echo "[". $cord[1] .",".$cord[0] .", '".$icn."','".$value["client"] ."','".$value["destination"]."',"."false".", ".$stat.",'".$value["driver"]."','".$value["car"]."','".$client["address"] ."','".$client['phone'] ."','".$client["email"] ."']";

                echo ',';
                echo "\n";
            }

            foreach ($rides as $key => $value) {
                $cord = hc($value['origin_cord']);
                $card = hc($value['destination_cord']);
                foreach ($cars as $key => $v) {
                    if($value['car'] == $v['imnum'] ){ $car = $v;}
                }
                $icn =  hI($car['status'], 3);
                $stat = hs($car['status'], 3);
//Name, marque, adresse, immat
                echo "[". $cord[1] .",".$cord[0] .", '".$icn."','".$value["car"] ."','".$value["destination"]."',"."false".", ".$stat.",'".$value["car"]."','".$car["name"] ."','".$car['marque'] ."','".$car["address"] ."']";

                if($key != count($rides) -1){ echo ',';}
                elseif($key == count($rides) - 1){
                    echo "\n";  }
                echo "\n";
            }
            ?>
        ],

//pickup adresse, dropoff adresse, Estimate Distance,Estimate Time     
            rides: [
                <?php
                foreach ($rides as $key => $value) {
                    $cord = hc($value['origin_cord']);
                    $card = hc($value['destination_cord']);
                    $jsonString = file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$cord[1].",".$cord[0]."&destinations=".$card[1].','.$cord[0]."&mode=driving");
                    $arr = json_decode($jsonString, true);
                    $time = $arr['rows'][0]['elements'][0]['duration']['text'];
                    $dist = $arr['rows'][0]['elements'][0]['distance']['text'];

                    echo " [' ". $cord[1] ." ',' ".$cord[0] ." ',' ". $card[1] ." ',' ".$card[0] ." ',' pickup1','".$value['origin']."','".$value['destination']."','".$dist."','".$time."']";
                    echo ',';
                    echo "\n";
                }

                ?>  ]

        };

            var options = $.extend(defaults, options);

            $(this).each(function() {

                var $this = $(this);
                var infowindow;
                // Init Map
                var directionsDisplay = new google.maps.DirectionsRenderer({
                    suppressMarkers: true
                });

                var mapOptions = {
                    scrollwheel: options.scrollwheel,
                    scaleControl: false,
                    center: options.marker.center ? new google.maps.LatLng(options.marker.latitude, options.marker.longitude) : new google.maps.LatLng(options.latitude, options.longitude),
                    zoom: options.zoom,
                    mapTypeControl: true,
                    mapTypeControlOptions: {
                        style: google.maps.MapTypeControlStyle.VERTICAL_BAR,

                        //position: google.maps.ControlPosition.LEFT_TOP
                    },


                    zoomControl: true,
                    zoomControlOptions: {
                        position: google.maps.ControlPosition.LEFT_TOP,
                        style: google.maps.ZoomControlStyle.LARGE
                    },
                    scaleControl: true,
                    streetViewControl: true,
                    streetViewControlOptions: {
                        position: google.maps.ControlPosition.LEFT_TOP
                    },
                    mapTypeId: eval('google.maps.MapTypeId.' + options.type)
                };

                var map = new google.maps.Map(document.getElementById($this.attr('id')), mapOptions);
                directionsDisplay.setMap(map);




                // Styles
                if (options.styles) {
                    var GRAYSCALE_style = [{
                        featureType: "all",
                        elementType: "all",
                        stylers: [{
                            saturation: -100
                        }]
                    }];
                    var MIDNIGHT_style = [{
                        featureType: 'water',
                        stylers: [{
                            color: '#021019'
                        }]
                    }, {
                        featureType: 'landscape',
                        stylers: [{
                            color: '#08304b'
                        }]
                    }, {
                        featureType: 'poi',
                        elementType: 'geometry',
                        stylers: [{
                            color: '#0c4152'
                        }, {
                            lightness: 5
                        }]
                    }, {
                        featureType: 'road.highway',
                        elementType: 'geometry.fill',
                        stylers: [{
                            color: '#000000'
                        }]
                    }, {
                        featureType: 'road.highway',
                        elementType: 'geometry.stroke',
                        stylers: [{
                            color: '#0b434f'
                        }, {
                            lightness: 25
                        }]
                    }, {
                        featureType: 'road.arterial',
                        elementType: 'geometry.fill',
                        stylers: [{
                            color: '#000000'
                        }]
                    }, {
                        featureType: 'road.arterial',
                        elementType: 'geometry.stroke',
                        stylers: [{
                            color: '#0b3d51'
                        }, {
                            lightness: 16
                        }]
                    }, {
                        featureType: 'road.local',
                        elementType: 'geometry',
                        stylers: [{
                            color: '#000000'
                        }]
                    }, {
                        elementType: 'labels.text.fill',
                        stylers: [{
                            color: '#ffffff'
                        }]
                    }, {
                        elementType: 'labels.text.stroke',
                        stylers: [{
                            color: '#000000'
                        }, {
                            lightness: 13
                        }]
                    }, {
                        featureType: 'transit',
                        stylers: [{
                            color: '#146474'
                        }]
                    }, {
                        featureType: 'administrative',
                        elementType: 'geometry.fill',
                        stylers: [{
                            color: '#000000'
                        }]
                    }, {
                        featureType: 'administrative',
                        elementType: 'geometry.stroke',
                        stylers: [{
                            color: '#144b53'
                        }, {
                            lightness: 14
                        }, {
                            weight: 1.4
                        }]
                    }]
                    var BLUE_style = [{
                        featureType: 'water',
                        stylers: [{
                            color: '#46bcec'
                        }, {
                            visibility: 'on'
                        }]
                    }, {
                        featureType: 'landscape',
                        stylers: [{
                            color: '#f2f2f2'
                        }]
                    }, {
                        featureType: 'road',
                        stylers: [{
                            saturation: -100
                        }, {
                            lightness: 45
                        }]
                    }, {
                        featureType: 'road.highway',
                        stylers: [{
                            visibility: 'simplified'
                        }]
                    }, {
                        featureType: 'road.arterial',
                        elementType: 'labels.icon',
                        stylers: [{
                            visibility: 'off'
                        }]
                    }, {
                        featureType: 'administrative',
                        elementType: 'labels.text.fill',
                        stylers: [{
                            color: '#444444'
                        }]
                    }, {
                        featureType: 'transit',
                        stylers: [{
                            visibility: 'off'
                        }]
                    }, {
                        featureType: 'poi',
                        stylers: [{
                            visibility: 'off'
                        }]
                    }]
                    var mapType = new google.maps.StyledMapType(eval(options.styles + '_style'), {
                        name: options.styles
                    });
                    map.mapTypes.set(options.styles, mapType);
                    map.setMapTypeId(options.styles);
                }

                // Home Marker
                var home = new google.maps.Marker({
                    map: map,
                    position: new google.maps.LatLng(options.marker.latitude, options.marker.longitude),
                    icon: new google.maps.MarkerImage(options.marker.icon),
                    title: options.marker.title
                });

                // Add info on the home marker
                var info = new google.maps.InfoWindow({
                    content: options.address
                });

                // Open the info window immediately
                if (options.marker.open) {
                    info.open(map, home);
                } else {
                    google.maps.event.addListener(home, 'click', function() {
                        info.open(map, home);
                    });
                };

                // Create Markers (locations)
                var infowindow = new google.maps.InfoWindow();
                var marker, i;
                var markers = [];


                for (i = 0; i < options.locations.length; i++) {

                    // Add Markers
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(options.locations[i][0], options.locations[i][1]),
                        map: map,
                        icon: new google.maps.MarkerImage(options.locations[i][2] || options.marker.icon),
                        title: options.locations[i][3] + '-' + options.locations[i][7]
                    });

                    // Create an array of the markers
                    markers.push(marker);
                    // Init info for each marker
                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        // var cnt = '<p>'+options.locations[i][3]+'</p> Hello' ;
                        //driver_name, driver_phone, car_imm, driver addres           client_name, client phone, address     ;
                        if (options.locations[i][6] < 7) {
                            return function() {
                                infowindow.setContent('<p><b>Name:</b>' + options.locations[i][3] + '<br>Address:' + options.locations[i][9] + '<br> Phone:' + options.locations[i][8] + '<br> Email:' + options.locations[i][10] + '<br>Car Imm:' + options.locations[i][7] +'</p>');
                                infowindow.open(map, marker);
                            }
                        } else if (options.locations[i][6] > 6 && options.locations[i][6] < 11) {
                            return function() {
                                infowindow.setContent('<p><b>Name:</b>' + options.locations[i][3] + '<br>Address:' + options.locations[i][9] + '<br> Phone:' + options.locations[i][10] + '<br> Email :' + options.locations[i][11]  + '</p>');
                                infowindow.open(map, marker);
                            }
                        } else {
                            return function() {
                                infowindow.setContent('<p><b>Name:</b>' + options.locations[i][8] + '<br>Marque:' + options.locations[i][9] + '<br> Address:' + options.locations[i][10] + '<br>Immatriculation:' + options.locations[i][3] + '</p>');
                                infowindow.open(map, marker);
                            }
                        }

                    })(marker, i));

                };
                var sv = new google.maps.places.PlacesService(map);
                var iW = new google.maps.InfoWindow();

                $this.on('pOi', function(event, t) {
                    $this.trigger('reset');
                    $this.trigger('hide_all');
                    var request = {
                        bounds: map.getBounds(),
                        types: [ t ]
                    };
                    sv.radarSearch(request, callback);
                });

                function callback(results, status) {
                    if (status !== google.maps.places.PlacesServiceStatus.OK) {
                        console.error(status);
                        return;
                    }
                    for (var i = 0, result; result = results[i]; i++) {
                        addMarker(result);
                    }
                }

                var markrs = [];
                function addMarker(place) {
                    var x = document.getElementById("select_poi").value;
                    if(x == 'airport') {
                        var icn = 'http://cab-booking-script.com/images/airport.png';
                    }else if(x == 'park'){
                        var icn = 'http://cab-booking-script.com/images/park.png'
                    }else if(x == 'train_station'){
                        var icn = 'http://cab-booking-script.com/images/train.png'
                    }

                    var markr = new google.maps.Marker({
                        map: map,
                        position: place.geometry.location,
                        icon: {
                            url: icn
                        }
                    });

                    markrs.push(markr);
                    google.maps.event.addListener(markr, 'click', function() {
                        sv.getDetails(place, function(result, status) {
                            if (status !== google.maps.places.PlacesServiceStatus.OK) {
                                console.error(status);
                                return;
                            }
                            iW.setContent(result.name);
                            iW.open(map, markr);
                        });
                    });
                }



                var mroutes = [];

                function createMarker(latlng, label, html, url, i) {
                    var contentString = html;
                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        icon: url,
                        title: label,
                        zIndex: Math.round(latlng.lat() * -100000) << 5
                    });
                    mroutes.push(marker);
                    google.maps.event.addListener(marker, 'click', function() {
                        infowindow.setContent(contentString);
                        infowindow.open(map, marker);
                    });
                }

                // Directions
                var directionsService = new google.maps.DirectionsService();

                $this.on('pickup', function(event, origin) {
                    $this.trigger('reset');
                    $this.trigger('hide_all');
                    var i = origin;
                    var request = {
                        origin: new google.maps.LatLng(options.rides[i][0], options.rides[i][1]),
                        destination: new google.maps.LatLng(options.rides[i][2], options.rides[i][3]),
                        travelMode: google.maps.TravelMode.DRIVING
                    };
                    directionsService.route(request, function(result, status) {
                        if (status == google.maps.DirectionsStatus.OK) {
                            directionsDisplay.setDirections(result);
                            var startLocation = new Object();
                            var endLocation = new Object();
                            var sHtml = '<p><b>Pickup address: ' + options.rides[i][5] + '<br><br></b>Dropoff address: ' + options.rides[i][6] + '<br> Estimate distance: ' + options.rides[i][7] + '<br>Estimate time: ' + options.rides[i][8] + '</p>';
                            var eHtml = '<p><b>Dropoff address: ' + options.rides[i][6] + '<br><br></b>Dropoff address: ' + options.rides[i][5] + '<br> Estimate distance: ' + options.rides[i][7] + '<br>Estimate time: ' + options.rides[i][8] + '</p>';
                            var legs = result.routes[0].legs;
                            for (i = 0; i < legs.length; i++) {
                                if (i == 0) {
                                    startLocation.latlng = legs[i].start_location;
                                    startLocation.address = legs[i].start_address;
                                }
                                if (i == legs.length - 1) {
                                    endLocation.latlng = legs[i].end_location;
                                    endLocation.address = legs[i].end_address;
                                }
                                var steps = legs[i].steps;
                            }
                            //pickup adresse, dropoff adresse, Estimate Distance,Estimate Time
                            createMarker(endLocation.latlng, "Dropoff", eHtml, "http://www.google.com/mapfiles/markerB.png", i)
                            createMarker(startLocation.latlng, "Pickup", sHtml, "http://maps.gstatic.com/mapfiles/markers2/marker_greenA.png", i);



                        };
                    });
                });

                // Hide Markers Once (helper)
                $this.on('hide_all', function() {
                    directionsDisplay.set('directions', null);
                    for (var i = 0; i < options.locations.length; i++) {
                        markers[i].setVisible(false);
                    };
                    for (var i = 0; i < mroutes.length; i++) {
                        mroutes[i].setVisible(false);
                    }
                    for (var i = 0; i < markrs.length; i++) {
                        markrs[i].setVisible(false);
                    }
                    // markr.setVisible(false);
                });
                $this.on('showAll', function(event) {
                    $this.trigger('hide_all');
                    $this.trigger('reset');
                    // Set bounds
                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0; i < options.locations.length; i++) {
                        if (options.locations[i][6] < 9) {
                            markers[i].setVisible(true);
                        };

                        // Add markers to bounds
                        bounds.extend(markers[i].position);
                    };
                    map.fitBounds(bounds);
                });

                // locationsMarkers Per Category (helper)
                $this.on('showOne', function(event, category) {
                    $this.trigger('hide_all');
                    $this.trigger('reset');

                    // Set bounds
                    var bounds = new google.maps.LatLngBounds();
                    for (var i = 0; i < options.locations.length; i++) {
                        if (options.locations[i][3] == category) {
                            markers[i].setVisible(true);
                        };

                        // Add markers to bounds
                        bounds.extend(markers[i].position);
                    };
                    // Auto focus and center
                    map.fitBounds(bounds);
                });

                // locationsMarkers Per Category (helper)
                $this.on('show', function(event, category) {
                    $this.trigger('hide_all');
                    $this.trigger('reset');

                    // Set bounds
                    var bounds = new google.maps.LatLngBounds();

                    if (category == 31) {
                        for (var i = 0; i < options.locations.length; i++) {
                            if (options.locations[i][6] < 7) {
                                markers[i].setVisible(true);
                            };

                            // Add markers to bounds
                            bounds.extend(markers[i].position);
                        };
                    } else if (category == 32) {
                        for (var i = 0; i < options.locations.length; i++) {
                            if (options.locations[i][6] > 6 && options.locations[i][6] < 11) {
                                markers[i].setVisible(true);
                            };

                            //Add markers to bounds
                            bounds.extend(markers[i].position);
                        };

                    } else if (category == 33) {
                        for (var i = 0; i < options.locations.length; i++) {
                            if (options.locations[i][6] >10) {
                                markers[i].setVisible(true);
                            };

                            //Add markers to bounds
                            bounds.extend(markers[i].position);
                        };
                    } else {
                        for (var i = 0; i < options.locations.length; i++) {
                            if (options.locations[i][6] == category) {
                                markers[i].setVisible(true);
                            };

                            // Add markers to bounds
                            bounds.extend(markers[i].position);
                        };
                    }
                    // Auto focus and center
                    map.fitBounds(bounds);
                });

                // Hide Markers Per Category (helper)
                $this.on('hide', function(event, category) {
                    for (var i = 0; i < options.locations.length; i++) {
                        if (options.locations[i][6] == category) {
                            markers[i].setVisible(false);
                        };
                    };

                });

                // Clear Markers (helper)
                $this.on('clear', function() {
                    if (markers) {
                        for (var i = 0; i < markers.length; i++) {
                            markers[i].setMap(null);
                        };
                    };
                });

                $this.on('reset', function() {
                    directionsDisplay.set('directions', null);
                    $this.trigger('hide_all');
                    map.setCenter(new google.maps.LatLng(options.latitude, options.longitude), options.zoom);
                });
                // Hide Markers Once (helper)
                $this.on('show_all', function() {
                    for (var i = 0; i < options.locations.length; i++) {
                        markers[i].setVisible(true);
                    };
                });

                // Hide all locations once
                //$this.trigger('hide_all');
                $this.trigger('show_all');

            });

        };

        $(document).ready(function() {
            $('[data-toggle="mapit"]').mapit();
        });

    })(jQuery);
</script>

<script src="<?php echo base_url(); ?>assets/jsnew/initializers.js"></script>

</div>
