<script type="text/javascript">


    function hideAdd() {
        hideForms();
        $('.listDiv').show();
        $('.AddnewQuotesShow').hide();
        $('.AddnewQuotesHide').show();
    }

    function showAdd() {
        hideForms();
        $('.addDiv').show();
        $('.ChangeEventQuotes').html('Add New factoring');
        $('.AddnewQuotesHide').hide();
        $('.AddnewQuotesShow').show();
    }

    function showEdit() {
        var val = $('.chk-produit-template-btn').val();
        if(val != "" && val != 0)
        {
            window.location.href='<?=base_url();?>admin/editaccounting.php?id='+val;
        }
    }

    function showPDF() {
        var val = $('.chk-produit-template-btn').val();
        if(val != "" )
        {
            $('#myModalPDF'+val).modal('show');
        }
    }


    function showDelete()
    {
        var val = $('.chk-produit-template-btn').val();
        if(val != "" && val != 0)
        {
            hideForms();
            $('.ChangeEventQuotes').html('Delete Transaction');
            $('.AddnewQuotesHide').hide();
            $('.AddnewQuotesShow').show();
            $('.deleteDiv').show();
            $('#delete-id').val(val);
        }
    }


    function hideForms() {

        $('.addDiv').hide();
        $('.editDiv').hide();
        $('.deleteDiv').hide();
        $('.listDiv').hide();
    }






    $(document).ready(function() {
        hideAdd();

        $('input.chk-produit-template').on('change', function() {
            $('input.chk-produit-template').not(this).prop('checked', false);
            var id = $(this).attr('data-input');
            $('.chk-produit-template-btn').val(id);
        });
    });



</script>
<style type="text/css">
    input.btn {
        min-height: 28px;
        border: 1px solid #bbb;
        min-width: 80px;
    }
    form > fieldset > section {
        padding-top: 5px;
        padding-bottom: 5px;
        width: 48%;
        float: left;
    }
    form > fieldset > section input,
    form> fieldset >section select {
        width: 70%;
        margin: 0 auto;
        display: inline-block;
        height: 40px;
    }
    #form-ttl {
        font-size: 18px;
        text-align: center;
        padding-top: 10px;
    }
    .timepicker_wrap
    {
        width: 124px !important;
        /*left:-40px !important;*/
        top:35px !important;

    }
    .form-btm {
        float: right;
        width: 100%;
        margin-top: 10px;
    }
    .listDiv {
        min-height: 700px;
    }
    .dataTable td {
        min-width: 90px;
    }
</style>
<link href="<?=base_url();?>css/timepicki.css" rel="stylesheet">
<script src="<?=base_url();?>css/timepicki.js"></script>
<section id="content">

    <div class="container">

        <div id="breadcrumb" class="AddnewQuotesHide">
            <a href="<?php echo base_url();?>index.php?admin/dashboard" class="tip-bottom" data-original-title="Go to Home"><i class="fa fa-home"></i> Accueil</a>
            <a href="<?php echo base_url();?>admin/accounting.php" class="current">
                <?php echo $page_title?>
            </a>
        </div>

        <div id="breadcrumb" class="AddnewQuotesShow" style="display: none;">
            <a href="<?php echo base_url();?>index.php?admin/dashboard" class="tip-bottom" data-original-title="Go to Home"><i class="fa fa-home"></i> Accueil</a>
            <a href="<?php echo base_url();?>admin/factoring.php" class="current">
                <?php echo $page_title?>
            </a>
            <a href="<?php echo base_url();?>admin/factoring.php" class="current ChangeEventQuotes">
                Add New factoring
            </a>
        </div>

        <div class="col-md-12 g12">
            <div class="listDiv" >
                <div class="filter-group">
                    <div class="col-md-10" style="padding-left: 0px;">
                        <form  method="get" action="" style="padding-top: 0px;background: none;" >

                            <select class="form-control" style="width: 10%;float:left; ">
                                <option value="">Client</option>
                            </select>

                            <select class="form-control" style="width: 10%;float:left; ">
                                <option value="">Category</option>
                            </select>
                            <select class="form-control" style="width: 7%;float:left; ">
                                <option value="">Tous</option>
                                <option value="Bill">Bill</option>
                                <option value="Sale">Sale</option>
                            </select>

                            <select class="form-control" style="width: 7%;float:left; ">
                                <option value="">Tous</option>
                                <option value="Amount">Amount</option>
                                <option value="Payment">Payment</option>
                            </select>

                            <select class="form-control" style="width: 7%;float:left; ">
                                <option value="">Tous</option>
                                <option value="Cash">Cash</option>
                                <option value="CB">CB</option>
                                <option value="Prelevement">Prelevement</option>
                                <option value="Cheque">Cheque</option>
                                <option value="Factor">Factor</option>
                            </select>




                            <span style="float: left;padding: 3px;padding-top: 10px">De</span>
                            <input type="text" name="from_period" class="datepicker" style="width: 8%;float:left;height: 34px; "  value="<?=!empty($this->input->get('from_period'))?$this->input->get('from_period'):''?>" />
                            <span style="float: left;padding: 3px;padding-top: 10px">A</span>
                            <input type="text" name="to_period" class="datepicker" style="width: 8%;float:left;height: 34px; " value="<?=!empty($this->input->get('to_period'))?$this->input->get('to_period'):''?>"/>
                            <button type="submit" style="float: left;"  class="btn"><img src="<?=base_url()?>images/search.png" style="height: 18px;width: 18px;"></button>


                        </form>

                    </div>

                    <div class="col-md-2" style="padding: 0px;">

                        <div class="page-action">
                            <a class="btn" onclick="showAdd()"><span class="add-icon"></span></a>
                            <a class="btn" onclick="showEdit()"><span class="edit-icon"></span></a>
                            <a class="btn" onclick="showDelete()"><span class="delete-icon"></span></a>
                            <a class="btn" onclick="showPDF()" style="padding: 3px 7px 4px 7px;">PDF</a>
                            <a class="btn" onclick="" style="padding: 3px 7px 4px 7px;">CSV</a>
                        </div>

                    </div>

                </div>
                <table class="table table-bordered data-table dataTable">
                    <input type="hidden" class="chk-produit-template-btn" value="">
                    <thead>
                    <tr>
                        <th></th>
                        <th>ID</th>
                        <th>Statut</th>
                        <th> Numbero de Facture </th>
                        <th> Date de Facture </th>
                        <th> Date D' Echeance </th>
                        <th>Client</th>
                        <th>Montant</th>
                        <th> Date de remise </th>
                        <th> Montant Finance	</th>
                        <th> Date de Financement </th>
                        <th> Montant Regle Par le client </th>
                        <th> Date du Reglment par le Client </th>
                        <th> Mode Reglement </th>
                        <th> Solde Restant </th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>

                </table>
            </div>


            <div class="col-md-12 deleteDiv" style="border:1px solid #ccc;padding: 0px;">
                <form method="post" action="<?php echo base_url('admin/DeleteAccountingProduit');?>" style="padding: 30px;">
                    <input type="hidden" id="delete-id" name="id" value="">
                    <div style="display: inline-block;float:left;margin-right: 10px;"> Are you sure you want to delete selected booking?</div>
                    <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;width: 50px;margin-top: -11px;margin-right: 10px;" />
                    <a class="btn" href="#" style="margin-top: -11px;" onclick="hideAdd()">No</a>
                </form>
            </div>


            <div class="col-md-12 addDiv" style="border:1px solid #ccc;padding-left:3px;margin-bottom: 10px">
                <form action="" class="SelltoFactorFormSave allformhide" id="SelltoFactor" method="post" style="width: 100%;background-image: none;">
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-2" style="width: 11%;">
                            <div class="form-group">
                                <div class="col-md-12" style="padding: 0px;">
                                    <select class="form-control changeForm" id="changeSelltoFactor" name="type" >
                                        <option value="Sell to Factor">Sell to Factor </option>
                                        <option value="Factor Payment">Factor Payment</option>
                                        <option value="Client Payment">Client Payment</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-2" style="width: 15%;">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 10px;" >
                                    <span style="font-weight: bold;">Statut</span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                    <select style="width:100%" class="form-control" name="status_id" required="">
                                        <option value="">Selectionner</option>
                                    </select>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-3" style="width: 15%;">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 10px;" >
                                    <span style="font-weight: bold;">Client</span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                    <select style="width:100%" class="form-control" name="status_id" required="">
                                        <option value="">Selectionner</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3" style="width: 15%;">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 5px;padding-left: 0px;" >
                                    <span style="font-weight: bold;">Invoice Number</span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                    <select style="width:100%" class="form-control" name="status_id" required="">
                                        <option value="">Selectionner</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2" style="width: 20%;">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 5px;" >
                                    <span style="font-weight: bold;">Date de Facture</span>
                                </div>
                                <div class="col-md-6" style="padding: 0px;">
                                    <input required="" style="background: yellow;" name="date_de_facture" class="form-control datepicker" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2" style="width: 20%;">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 5px;" >
                                    <span style="font-weight: bold;">Date D' Echeance</span>
                                </div>
                                <div class="col-md-6" style="padding: 0px;">
                                    <input required="" style="background: yellow;" name="date_d_echeance" class="form-control datepicker" type="text">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 10px;">


                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 10px;" >
                                    <span style="font-weight: bold;">Montant</span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                    <input required="" style="background: yellow;" name="montant" class="form-control" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 5px;" >
                                    <span style="font-weight: bold;">Date de remise</span>
                                </div>
                                <div class="col-md-4" style="padding: 0px;">
                                    <input  class="form-control datepicker" name="date_de_remise" type="text">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 10px;margin-top: 20px;">
                        <div class="col-md-12">
                            <a class="btn" style="float: right;" href="#" onclick="hideAdd()"><span class="delete-icon" >Annuler</span></a>
                            <button style="padding: 3px 13px;margin-right: 10px;float:right;" type="submit" class="btn"><span class="save-icon"></span> Enregistrer</button>
                            <img class="displayimageloader" style="float: right; margin-right: 10px;display: none;" src="<?php echo base_url() . 'css/loading.gif' ?>" />
                        </div>
                    </div>

                </form>


                <form action="" class="FactorPaymentFormSave allformhide" id="FactorPayment" method="post" style="width: 100%;background-image: none; display: none;">
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-2" style="width: 11%;">
                            <div class="form-group">
                                <div class="col-md-12" style="padding: 0px;">
                                    <select class="form-control changeForm" id="changeFactorPayment" name="type" >
                                        <option value="Sell to Factor">Sell to Factor </option>
                                        <option value="Factor Payment">Factor Payment</option>
                                        <option value="Client Payment">Client Payment</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-2" style="width: 15%;">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 10px;" >
                                    <span style="font-weight: bold;">Statut</span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                    <select style="width:100%" class="form-control" name="status_id" required="">
                                        <option value="">Selectionner</option>
                                    </select>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-3" style="width: 15%;">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 10px;" >
                                    <span style="font-weight: bold;">Client</span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                    <select style="width:100%" class="form-control" name="status_id" required="">
                                        <option value="">Selectionner</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-3" style="width: 15%;">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 5px;padding-left: 0px;" >
                                    <span style="font-weight: bold;">Invoice Number</span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                    <select style="width:100%" class="form-control" name="status_id" required="">
                                        <option value="">Selectionner</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2" style="width: 20%;">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 5px;" >
                                    <span style="font-weight: bold;">Date de Facture</span>
                                </div>
                                <div class="col-md-6" style="padding: 0px;">
                                    <input required="" style="background: yellow;" name="date_de_facture" class="form-control datepicker" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2" style="width: 20%;">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 5px;" >
                                    <span style="font-weight: bold;">Date D' Echeance</span>
                                </div>
                                <div class="col-md-6" style="padding: 0px;">
                                    <input required="" style="background: yellow;" name="date_d_echeance" class="form-control datepicker" type="text">
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row" style="margin-top: 10px;">


                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 10px;" >
                                    <span style="font-weight: bold;">Amount</span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                    <input required="" style="" name="amount" class="form-control" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 5px;padding-left: 0px;" >
                                    <span style="font-weight: bold;">Financed Amount</span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                    <input required="" style="" name="amount" class="form-control" type="text">
                                </div>
                            </div>
                        </div>


                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-5" style="margin-top: 5px;padding-left: 0px;" >
                                    <span style="font-weight: bold;">Date de Financement</span>
                                </div>
                                <div class="col-md-7" style="padding: 0px;">
                                    <input  class="form-control datepicker" name="date_de_remise" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 5px;padding-left: 0px;" >
                                    <span style="font-weight: bold;">Factoring Comission</span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                    <input required="" style="background: yellow;" name="amount" class="form-control" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2" style="width: 18%;">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 5px;padding-left: 0px;" >
                                    <span style="font-weight: bold;">%. Amount Financed</span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                    <input required="" style="background: yellow;" name="amount" class="form-control"  type="text">
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row" style="margin-bottom: 10px;margin-top: 20px;">
                        <div class="col-md-12">
                            <a class="btn" style="float: right;" href="#" onclick="hideAdd()"><span class="delete-icon" >Annuler</span></a>
                            <button style="padding: 3px 13px;margin-right: 10px;float:right;" type="submit" class="btn"><span class="save-icon"></span> Enregistrer</button>
                            <img class="displayimageloader" style="float: right; margin-right: 10px;display: none;" src="<?php echo base_url() . 'css/loading.gif' ?>" />
                        </div>
                    </div>

                </form>

                <form action="" class="ClientPaymentFormSave allformhide" id="ClientPayment" method="post" style="width: 100%;background-image: none; display: none;">
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-2" style="width: 11%;">
                            <div class="form-group">
                                <div class="col-md-12" style="padding: 0px;">
                                    <select class="form-control changeForm" id="changeClientPayment" name="type" >
                                        <option value="Sell to Factor">Sell to Factor </option>
                                        <option value="Factor Payment">Factor Payment</option>
                                        <option value="Client Payment">Client Payment</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-2" style="width: 15%;">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 10px;" >
                                    <span style="font-weight: bold;">Statut</span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                    <select style="width:100%" class="form-control" name="status_id" required="">
                                        <option value="">Selectionner</option>
                                    </select>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-3" style="width: 15%;">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 10px; " >
                                    <span style="font-weight: bold;">Client</span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                    <select style="width:100%" class="form-control" name="status_id" required="">
                                        <option value="">Selectionner</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3" style="width: 15%;">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 5px;padding-left: 0px;" >
                                    <span style="font-weight: bold;">Invoice Number</span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                    <select style="width:100%" class="form-control" name="status_id" required="">
                                        <option value="">Selectionner</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2" style="width: 15%;">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 10px;padding-left: 0px;" >
                                    <span style="font-weight: bold;">Amount</span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                    <input required=""  name="" class="form-control" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2" style="width: 15%;">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 5px;padding-left: 0px;" >
                                    <span style="font-weight: bold;">Date de Facture</span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                    <input required="" style="background: yellow;" name="date_de_facture" class="form-control datepicker" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2" style="width: 14%;">
                            <div class="form-group">
                                <div class="col-md-5" style="margin-top: 5px;padding-left: 0px;" >
                                    <span style="font-weight: bold;">Date D' Echeance</span>
                                </div>
                                <div class="col-md-7" style="padding: 0px;">
                                    <input required="" style="background: yellow;" name="date_d_echeance" class="form-control datepicker" type="text">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 15px;">

                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 5px;padding-right: 0;">
                                    <span style="font-weight: bold;">Montant Regle Par le client</span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                    <input class="form-control " name="montant_regle_par_le_client" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3" style="width: 17%;">
                            <div class="form-group">
                                <div class="col-md-6" style="margin-top: 5px; padding: 0px;">
                                    <span style="font-weight: bold;">Date du Reglment par le Client</span>
                                </div>
                                <div class="col-md-6" style="padding: 0px;">
                                    <input class="form-control datepicker" name="date_du_reglment_par_le_client" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 5px;padding-left: 0px;" >
                                    <span style="font-weight: bold;">Mode Reglement</span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                    <select class="form-control ModeReglementChange" name="mode_reglement_id">
                                        <option value="">Selectionner</option>
                                        <?php
                                        foreach ($account_modereglement as $modereglement)
                                        { ?>
                                            <option value="<?=$modereglement->id?>"><?=$modereglement->modereglement?></option>
                                            <?php
                                        }
                                        ?>

                                    </select>
                                </div>

                            </div>
                        </div>


                        <div class="col-md-3" id="showChequeNumber" style="display: none;width: 18%">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 5px;padding-left: 0px;" >
                                    <span style="font-weight: bold;">Numero de Cheque</span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                    <input  class="form-control " name="number_of_cheque" type="text">
                                </div>

                            </div>
                        </div>



                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 5px;" >
                                    <span style="font-weight: bold;">Solde Restant</span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                    <input type="text" style="background: yellow;" class="form-control">
                                </div>
                            </div>
                        </div>


                    </div>



                    <div class="row" style="margin-bottom: 10px;margin-top: 20px;">
                        <div class="col-md-12">
                            <a class="btn" style="float: right;" href="#" onclick="hideAdd()"><span class="delete-icon" >Annuler</span></a>
                            <button style="padding: 3px 13px;margin-right: 10px;float:right;" type="submit" class="btn"><span class="save-icon"></span> Enregistrer</button>
                            <img class="displayimageloader" style="float: right; margin-right: 10px;display: none;" src="<?php echo base_url() . 'css/loading.gif' ?>" />
                        </div>
                    </div>

                </form>


            </div>
        </div>

    </div>

</section>
<!-- end div #content -->


<script type="text/javascript">


    $('.ModeReglementChange').change(function(){
        var value =	$(".ModeReglementChange option:selected").text();
        if(value == "Cheque")
        {
            $('#showChequeNumber').show();
        }
        else
        {
            $('#showChequeNumber').hide();
        }
    });



    $('.changeForm').change(function(){
        $('.allformhide').hide();
        var value = $(this).val();
        if(value == "Sell to Factor")
        {
            $('#changeSelltoFactor').val('Sell to Factor');
            $('#SelltoFactor').show();

        }
        else if(value == "Factor Payment")
        {
            $('#changeFactorPayment').val('Factor Payment');
            $('#FactorPayment').show();
        }
        else if(value == "Client Payment")
        {
            $('#changeClientPayment').val('Client Payment');
            $('#ClientPayment').show();
        }


    });




    jQuery(function($){
        $.datepicker.regional['fr'] = {
            closeText: 'Fermer',
            prevText: '&#x3c;Pr�c',
            nextText: 'Suiv&#x3e;',
            currentText: 'Aujourd\'hui',
            monthNames: ['Janvier','Fevrier','Mars','Avril','Mai','Juin',
                'Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
            monthNamesShort: ['Jan','Fev','Mar','Avr','Mai','Jun',
                'Jul','Aou','Sep','Oct','Nov','Dec'],
            dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
            dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
            dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
            weekHeader: 'Sm',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: '',
            maxDate: '+12M +0D',
            showButtonPanel: true
        };
        $.datepicker.setDefaults($.datepicker.regional['fr']);
    });

    $('document').ready(function(){

        $('.timepicker1').timepicki({
            show_meridian:false,
            min_hour_value:0,
            max_hour_value:23,
            overflow_minutes:true,
            increase_direction:'up',

        });



        $( ".datepicker" ).datepicker({
            dateFormat: "dd-mm-yy",
            regional: "fr"
        });

    });
</script>







<script>
    $(document).ready(function() {



        $("#from_period").wl_Date({
            dateFormat: 'dd/mm/yy'
        });

        $("#to_period").wl_Date({
            dateFormat: 'dd/mm/yy'
        });

    });
</script>




<style>
    #locationField,
    #controls {
        position: relative;
        width: 480px;
    }
    .label {
        text-align: right;
        font-weight: bold;
        width: 100px;
        color: #303030;
    }
    #address {
        border: 1px solid #000090;
        background-color: #f0f0ff;
        width: 480px;
        padding-right: 2px;
    }
    #address td {
        font-size: 10pt;
    }
    .field {
        width: 99%;
    }
    .slimField {
        width: 80px;
    }
    .wideField {
        width: 200px;
    }
    #locationField {
        height: 20px;
        margin-bottom: 2px;
    }
    .save-icon {}
</style>

<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>stylesheets/jquery.datetimepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>stylesheets/jquery.timepicker.css" />

<script src="<?php echo base_url() ?>javascripts/jquery.datetimepicker.full.js"></script>
<script src="<?php echo base_url() ?>javascripts/jquery.timepicker.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script>
    $( function() {
        $( ".datepicker" ).datepicker({'dateFormat': 'yy-mm-dd'});
        $('.timepicker').timepicker({ 'timeFormat': 'H:i' });
    } );
</script>

<script>
    $(document).ready(function(){
        $.datetimepicker.setLocale('en');

        $('.datetimepicker1').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            disabledDates: ['1986/01/08', '1986/01/09', '1986/01/10'],
            startDate: '2016-12-1'
        });

        var date = '2016/12/15 05:03';//
        $('.datetimepicker1').datetimepicker({
            step: 10, format: 'yyyy-MM-dd',
            dateFormat: '',
            timeFormat: 'hh:mm tt'
        });

    })
    //$('.datetimepicker1').datetimepicker({value: date});
    //$('.datetimepicker2').datetimepicker({value:'2016/12/1 05:03'});
</script>