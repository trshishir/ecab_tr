<?php
$locale_info = localeconv(); ?>
<style type="text/css">
    label{font-weight: 600; font-size: 13px;}
    .custom_style_row{margin-top: 30px; margin-bottom: 30px;}
    input[type=submit], button[type=submit]{
        color: #333;
        cursor: pointer;
        height: 34px;
        font-size: 16px;
        padding: 1px 28px 3px;
        border-radius: 5px;
        text-transform: none;
        font-family: inherit;
        font-weight: 500;
    }
    .body-border{
        min-height: 500px;
    }
    .custom_disabled{
        background-color: #F4F4F5 !important; 
    }
</style>
<div class="col-md-12" style="height: calc(100vh - 140px);"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12">
                <?php $this->load->view('admin/common/alert');?>
                <div class="module">
                    <?php
                    echo $this->session->flashdata('message');
                    $validation_erros = $this->session->flashdata('validation_errors');
                    if(!empty($validation_erros)){
                        ?>
                        <div class="form-validation-errors alert alert-danger">
                            <h3 style="font-size: 20px; text-align: center;">Validation Errors</h3>
                            <?php echo $validation_erros; ?>
                        </div>
                    <?php } ?>
                    <div class="module-head">
                    </div>
                    <div class="module-body">
                        <?=form_open_multipart("admin/currency/update_currency")?>
                        <input type="hidden" name="id" value="<?= ($currency['id'])?>">
                        <div class="col-xs-12">
                            <div class="form-group col-xs-3" style="padding-left: 0px;">
                                <label for="exampleInputEmail1">Statut</label>
                                <select class="form-control custom_disabled" name="statut" disabled>
                                    <option value="show" <?= ($currency['statut'] == 'show' ? 'selected':'') ?>>Show</option>
                                    <option value="hide" <?= ($currency['statut'] == 'hide' ? 'selected':'') ?>>Hide</option>
                                    <option value="published" <?= ($currency['statut'] == 'published' ? 'selected':'')?>>Published</option>
                                </select>
                            </div>
                            <div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Currency Name</label>
                                <input type="text" class="form-control custom_disabled" name="currency_name" value='<?= ($currency['currency_name'])?>' disabled>
                            </div>
                        </div>
                        </div>
                        
                         <div class="col-xs-1">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Short Code</label>
                                <input type="text" class="form-control custom_disabled" name="currency_code" value='<?= ($currency['currency_code'])?>' disabled>
                            </div>
                        </div>
                         <div class="col-xs-1" style="width: 170px;">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Conversion</label>
                                <input type="text" class="form-control custom_disabled" name="conversion" value='<?= ($currency['conversion'])?>' disabled>
                            </div>
                        </div>
                        <div class="col-xs-1" style="margin-top: 36px;text-align: right;width: 39px;">
                            <div class="form-group">
                                <label for="exampleInputEmail1">USD</label>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Symbole</label>
                                <input type="text" class="form-control custom_disabled" name="symbole" value='<?= ($currency['symbole'])?>' disabled>
                            </div>
                        </div>

                        <div class="col-xs-1">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Default</label>
                                <select class="form-control custom_disabled" name="default_status" disabled>
                                    <option value="no"<?= ($currency['default_status'] == 'no' ? 'selected':'') ?>>No</option>
                                    <option value="yes"<?= ($currency['default_status'] == 'yes' ? 'selected':'') ?>>Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Allow Front Change</label>
                                <select class="form-control custom_disabled" name="allow_front_change" disabled>
                                    <option value="no" <?= ($currency['allow_front_change'] == 'no' ? 'selected':'') ?>>No</option>
                                    <option value="yes" <?= ($currency['allow_front_change'] == 'yes' ? 'selected':'') ?>>Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Allow Admin Change</label>
                                <select class="form-control custom_disabled" name="allow_admin_change" disabled>
                                 <option value="yes" <?= ($currency['allow_admin_change'] == 'yes' ? 'selected':'') ?>>Yes</option>
                                 <option value="no" <?= ($currency['allow_admin_change'] == 'no' ? 'selected':'') ?>>No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Allow Areas Change</label>
                                <select class="form-control custom_disabled" name="allow_areas_change" disabled>
                                    <option value="no" <?= ($currency['allow_areas_change'] == 'front' ? 'selected':'') ?>>No</option>
                                    <option value="yes" <?= ($currency['allow_areas_change'] == 'front' ? 'selected':'') ?>>Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 text-right">
                            <div class="form-group">
                                <a href="<?= base_url('admin/currency'); ?>" class="btn btn-default"><i class="fa fa-times"></i>Cancel</a>
                            </div>
                        </div>
                        <!-- </form> -->
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div><?php $locale_info = localeconv(); ?>
<style>
    @media only screen and (min-width: 1400px){
        .table-filter input, .table-filter select{
            max-width: 9% !important;
        }
        .table-filter select{
            max-width: 95px !important;
        }
        .table-filter .dpo {
            max-width: 90px !important;
        }
    }
</style>
