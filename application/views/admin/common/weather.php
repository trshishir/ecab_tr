<link href="<?= base_url(); ?>assets/css/easy-weather.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?= base_url(); ?>assets/js/easy-weather-keys.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/easy-weather.js"></script>
<div class="ew" ></div>
<?php
//  var_dump($weather);

// die();
?>

<?php if($weather->background == "navy"){?>
<style>
    .ew-light-blue {
        background: #007FBD;
        color: #fff;
        text-shadow: none;
        border: none;
    }
</style>
<?php } ?>
<script>
    $('.ew').EasyWeather(
    	{
            showDescription: <?php echo $weather->showDiscription;?>,
			forecasts: <?php echo $weather->forecasts;?>,
			location: '<?php echo $weather->location;?>',
			tempUnit: 'C',
             weekDays:["Sat","Sun","Mon","Tue","Wed","Thu","Fri"],
			// showDetails: <?php echo $weather->showDetails;?>,
	        providerId: 'fio',
            // providerId:'<?php echo $weather->source_of_data;?>',
            providerSequenceIds: 'ham|yhw|wwo|owm|wug|fio',
            showMinMax: false,
            details:'humidity|pressure|wind',
            enableSearch:<?php echo $weather->enableSearch;?>,
    	 	orientation: 'horizontal',
    	 	width: '115%',
    	 	height: '44px',
    	 	nbForecastDays:<?php echo $weather->nbForecastDays;?>,
            showRegion:<?php echo $weather->showRegion;?>,
            // showCountry:<?php echo $weather->showCountry;?>,
            template: {
                forecast: '<div class="ew-left" style="margin: -6px 5px 0px 0px; font-size:9px;">' +
            '<div style="max-height:22px; font-size:12px;"><span><img width="25px" src="{icn}" ></span><strong>{date}</strong></div>' +
            '<div><small>{minmax}</small></div>' +
            // '<div>{desc}</div>'+
            '</div>',
                },
            load: function() {
                // removes the icon + current temp elements
                $('.ew-content').find('.ew-wrap-cond').remove();
                $('.ew-content').find('.ew-provider').remove();
                $('.ew-header').remove();
                // $('.ew-content').addClass('custom_gray_bg');
                <?php if($weather->background == "navy"){?>
                $('.ew-content').css('background','#007FBD');
                <?php } else {?>
                $('.ew-content').css('background','#007FBD');
                <?php }?>
              }



    	});



</script>
