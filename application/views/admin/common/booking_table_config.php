<!-- <script src="<?php echo base_url(); ?>assets/system_design/scripts/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/datatables/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/datatables/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/datatables/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/datatables/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/datatables/buttons.html5.min.js"></script> -->

<script class="init">
    function toDate(dateStr) {
        if (dateStr) {
            console.log(dateStr);
            var parts = dateStr.split("/");
            return new Date(parts[2], parts[1] - 1, parts[0])
        }

    }
    // Date range filter
    var minDateFilter = "";
    var maxDateFilter = "";

    var minAgeFilter = "";
    var maxAgeFilter = "";

    var dateIndex = $(".configTable thead").find(".column-date").index();
    $.fn.dataTableExt.afnFiltering.push(
            function (oSettings, aData, iDataIndex) {
                if ($("#table-filter .dpo").length > 0) {
                    if (typeof aData._date == 'undefined') {
                        if (aData[dateIndex]) {
                            aData._date = toDate(aData[dateIndex]).getTime();
                        }
                    }

                    if (minDateFilter && !isNaN(minDateFilter)) {
                        //console.log(aData._date);
                        //console.log(minDateFilter);
                        //console.log("min");
                        //console.log(aData._date < minDateFilter);
                        if (aData._date < minDateFilter) {
                            return false;
                        }
                    }

                    if (maxDateFilter && !isNaN(maxDateFilter)) {

                        //console.log(maxDateFilter);
                        //console.log(aData._date);
                        //console.log("max")
                        //console.log(aData._date > maxDateFilter);
                        if (aData._date > maxDateFilter) {
                            return false;
                        }
                    }
                }

                return true;
            }
    );

    var ageIndex = $(".configTable thead").find(".column-age").index();
    $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                var min = parseInt($('.table-filter input[data-name="age_from"]').val(), 10);
                var max = parseInt($('.table-filter input[data-name="age_to"]').val(), 10);
                var age = parseFloat(data[ageIndex]) || 0; // use data for the age column

                if ((isNaN(min) && isNaN(max)) ||
                        (isNaN(min) && age <= max) ||
                        (min <= age && isNaN(max)) ||
                        (min <= age && age <= max))
                {
                    return true;
                }
                return false;
            }
    );

    if ($(".configTable thead").find(".column-first_name").length > 0) {
        $.fn.dataTable.ext.search.push(
                function (settings, data, dataIndex) {
                    var firstNameIndex = $(".configTable thead").find(".column-first_name").index();
                    var lastNameIndex = $(".configTable thead").find(".column-last_name").index();
                    var sName = $(document.body).find('.table-filter input[data-name="name"]').val();
                    var fnRegex = new RegExp(sName, 'i');
                    var fName = data[firstNameIndex].replace(/[^\w\s]|_/g, function ($1) {
                        return ' ' + $1 + ' ';
                    }).replace(/[ ]+/g, ' ').split(' ');
                    if (lastNameIndex != -1) {
                        var lName = data[lastNameIndex].replace(/[^\w\s]|_/g, function ($1) {
                            return ' ' + $1 + ' ';
                        }).replace(/[ ]+/g, ' ').split(' ');
                    }


                    return fnRegex.test(fName[0]) || fnRegex.test(fName[1]) || fnRegex.test(fName[2]) || fnRegex.test(lName[0]) || fnRegex.test(lName[1]) || fnRegex.test(lName[1]);
                }
        );
    }

    $('.bdatepicker').datepicker({
        format: "dd/mm/yyyy"
    });

    var datatables = $('.configTable').DataTable({
        columnDefs: [
            {targets: 'no-sort', orderable: false}
        ],
        dom: '<"table-filter">B<"toolbar">frtip',
        language: {search: "", class: "form-control", searchPlaceholder: "Search"},
        buttons: [
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        initComplete: function () {
            $("div.toolbar")
                    .html('<div class="addevent"><div class="removeevent"></div></div>');
            var filterInp = $("#table-filter");
            if (filterInp.length > 0) {
                var tableFilter = $("div.table-filter");
                tableFilter.html(filterInp.html());

                if ($(".table-filter .dpo").length > 0) {
                    $('.table-filter .dpo[data-name="date_from"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            minDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                    $('.table-filter .dpo[data-name="date_to"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            maxDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                }
            }

            this.api().columns().every(function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                    );

                            column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                        });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });

        },
        "drawCallback": function () {
            $('.dataTables_paginate .paginate_button').addClass('btn btn-default');
        }
    });
    var datatables = $('.configTable_language').DataTable({
        columnDefs: [
            {targets: 'no-sort', orderable: false}
        ],
        dom: '<"table-filter">B<"toolbar">frtip',
        language: {search: "", searchPlaceholder: "Search"},
        buttons: [
            'copyHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        initComplete: function () {
            $("div.toolbar").html('<a href="<?= base_url("admin/$active_class/add.php") ?>" class="btn btn-sm btn-default"><i class="fa fa-plus"></i> Add</a> <a href="javascript:void(0);" class="btn btn-sm btn-default lang_edit" data-edit="0" onClick="setUpdateAction();"><i class="fa fa-pencil"></i> Edit</a> <a href="javascript:void(0);" class="btn btn-sm btn-default lang_edit" data-edit="0" onClick="setgoogletranslateAction();"><i class="fa fa-language"></i> Translate</a>  <a href="<?= base_url("admin/$active_class/importcsv.php") ?>" class="btn btn-sm btn-default delBtn"><i class="fa fa-upload"></i> Import Csv</a>');
            var filterInp = $("#table-filter");
            if (filterInp.length > 0) {
                var tableFilter = $("div.table-filter");
                tableFilter.html(filterInp.html());

                if ($(".table-filter .dpo").length > 0) {
                    $('.table-filter .dpo[data-name="date_from"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            minDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                    $('.table-filter .dpo[data-name="date_to"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            maxDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                }
            }
        },
        "drawCallback": function () {
            $('.dataTables_paginate .paginate_button').addClass('btn btn-default');
        }
    });
    $(document).ready(function () {

        if ($(".table-filter .dpo").length > 0) {
            $(".table-filter .dpo[data-name='date_from']").on("change", function () {
                minDateFilter = new Date(toDate(this.value)).getTime();
                datatables.draw();
            });
            $(".table-filter .dpo[data-name='date_to']").on("change", function () {
                maxDateFilter = new Date(toDate(this.value)).getTime();
                datatables.draw();
            });
        }
        $('.table-filter input[data-name="age_from"], .table-filter input[data-name="age_to"]').on("keyup change", function () {
            datatables.draw();
        });
        $(".table-filter input[data-name='name']").on("keyup", function () {
            datatables.draw();
        });
        $('.example').dataTable();
        $('.table-filter input').addClass('form-control'); // <-- add this line
        $(".singleSelect:checked").prop("checked", false);
        $(".singleSelect").click(function () {
            var that = $(this);
            var checked = that.is(":checked");
            $("table .singleSelect:checked").prop("checked", false);
            if (checked) {
                that.prop("checked", true);
                //console.log(that.data('id'));
                $(".configTable").attr("data-selected_id", that.data('id'))
            } else
                $(".configTable").attr("data-selected_id", "")
        });

        var filterInputs = $(".table-filter input, .table-filter select");
        filterInputs.each(function (x, y) {
            var name = $(y).data('name');
            //console.log(name)
            if (name != undefined) {
                $(y).on("change keyup", function () {
                    if ($.inArray(name, ['name', 'date_from', 'date_to', 'age_from', 'age_to']) === -1) {
                        var colIndex = $(".configTable thead").find(".column-" + name).index();
                        if (datatables.column(colIndex).search() !== this.value) {
                            datatables.column(colIndex).search(this.value).draw();
                        }
                    }
                });
            }
        });

    });
    var dtable = $('.configTable').DataTable();
    $('.filter').on('keyup change', function() {
  //clear global search values
        dtable.search('');
        dtable.column($(this).data('columnIndex')).search(this.value).draw();
    });

    $(".dataTables_filter input").on('keyup change', function() {
    //clear column search values
         dtable.columns().search('');
    //clear input values
        $('.filter').val('');
    });

</script>
<style>
.dataTables_length, .dataTables_filter, .dataTables_info, .dataTables_paginate {
    display: block;
}
.dataTables_filter label > input {
    visibility: visible;
}
</style>
