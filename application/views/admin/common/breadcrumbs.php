<div class="breadcrumb">
    <a href="<?php echo site_url();?>/admin/dashboard">
        <i class="fa fa-home" style="font-size: 15px;margin-right: 5px;"></i>Home</a>
    <?php if(isset($title)) {
    		if($title == 'View Document'){
    			$color = "#000000";
    		} else { $color = ""; }
        echo " > ";
        echo isset($title_link) ? "<a href='" . $title_link . "' style='color: ".$color."'>$title</a>" : $title;
    } ?>
    <?php if(isset($subtitle))  echo " > ";
        echo isset($subtitle_link) ? "<a href='" . $subtitle_link . "'>$subtitle</a>" : $subtitle;?>
</div>
