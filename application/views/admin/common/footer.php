    </div>
</div>
<section class="footer-new ">
    <div class="container-fluid">
        <div class="row-fluid ">
            <div class="col-md-3">
                <p class="develop-p pull-left">Powered by <a href="https://cabsofts.com/ecab" target="_BLANK">eCab V 1.0</a></p>
            </div>
            <div class="col-md-3">
                <p class="develop-p">Copyright © 2020 <a href="https://cabsofts.com/" target="_BLANK">CAB SOFTS</a></p>
            </div>
            <div class="col-md-6">
                <div class="row-fluid ">
                    <div class="contact">
                        <div class="col-md-4">
                            <span class="call-us"><i class="fa fa-phone" ></i> (+1) 302 261 9397</span>
                        </div> 
                        <div class="col-md-4">
                            <a href="<?php echo site_url(); ?>supporttickets.php">
                                <span class="call-us"><i class="fa fa-comments-o" ></i> Chat Support</span>
                            </a>
                        </div> 
                        <div class="col-md-4">
                            <a href="<?php echo site_url(); ?>supporttickets.php"><span class="ticket-support"><i class="fa fa-headphones" ></i> Ticket Support</span></a>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->

<script>
    $("#loaderDiv").show();
    $(function () {
        //$(".chzn-select").chosen();
        setTimeout(function () {
            $("#loaderDiv").hide();
        }, 1200)
    });
</script>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/Chart.js"></script>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/Chart.min.js"></script>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/canvasjs.min.js"></script>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/jquery.canvasjs.min.js"></script>

<!--<script src="<?php echo base_url(); ?>assets/system_design/scripts/moment.min.js"></script>-->
<script src="<?php echo base_url(); ?>assets/system_design/scripts/bootstrap.min.js"></script>
<!--<script src="<?php echo base_url(); ?>assets/system_design/scripts/BeatPicker.min.js"></script>-->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/timepicki.js"></script>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/bx-slider.js"></script>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/html2canvas.js"></script>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/jquery.creditCardValidator.js"></script>


<!--<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>-->
<script>

    function addwaitingtime() {
      var time1= document.getElementById("timepicker1").value;
      var time2= document.getElementById("waitingtimepicker").value
      var a=time1.split(":");
      var b=time2.split(":");
      var h=parseInt(a[0])+parseInt(b[0]);
      var m=parseInt(a[1])+parseInt(b[1]);
      if(m>=60){
        m=m-60;
        if(m<10){
          m="0"+m; 
      }
      h=h+1;
  }else{
     if(m<10){
      m="0"+m; 
  }
}

if(h >= 24){

    h=h-24;
    if(h<10){
      h="0"+h;
  }
}else{
 if(h<10){
  h="0"+h;
}
}
document.getElementById("timepicker2").value = h.toString()+" : "+m.toString();
}
$('#waitingtimepicker').timepicki({
    type_of_timezone:"zero",
    step_size_minutes:'5',
    show_meridian:false,
    min_hour_value:0,
    max_hour_value:23,
    overflow_minutes:true,
    increase_direction:'up',
    disable_keyboard_mobile: true});

$('#starttimepicker').timepicki({
    step_size_minutes:'5',
    show_meridian:false,
    min_hour_value:0,
    max_hour_value:23,
    overflow_minutes:true,
    increase_direction:'up',
    disable_keyboard_mobile: true});
$('#endtimepicker').timepicki({
    step_size_minutes:'5',
    show_meridian:false,
    min_hour_value:0,
    max_hour_value:23,
    overflow_minutes:true,
    increase_direction:'up',
    disable_keyboard_mobile: true});

 $('#timepicker1').timepicki({
        step_size_minutes:'5',
        on_change: addwaitingtime,
        show_meridian:false,
        min_hour_value:0,
        max_hour_value:23,
        overflow_minutes:true,
        increase_direction:'up',
        disable_keyboard_mobile: true});
$('#timepicker2').timepicki({
  step_size_minutes:'5',
  show_meridian: false,
  min_hour_value: 0,
  max_hour_value: 23,
  overflow_minutes: true,
  increase_direction: 'up',
  disable_keyboard_mobile: true});
$('#go_time_1, #back_time_1').timepicki({  step_size_minutes:'5',show_meridian: false, min_hour_value: 0, max_hour_value: 23, overflow_minutes: true, increase_direction: 'up', disable_keyboard_mobile: true});
$('#go_time_2, #back_time_2').timepicki({  step_size_minutes:'5',show_meridian: false, min_hour_value: 0, max_hour_value: 23, overflow_minutes: true, increase_direction: 'up', disable_keyboard_mobile: true});
$('#go_time_3, #back_time_3').timepicki({  step_size_minutes:'5',show_meridian: false, min_hour_value: 0, max_hour_value: 23, overflow_minutes: true, increase_direction: 'up', disable_keyboard_mobile: true});
$('#go_time_4, #back_time_4').timepicki({  step_size_minutes:'5',show_meridian: false, min_hour_value: 0, max_hour_value: 23, overflow_minutes: true, increase_direction: 'up', disable_keyboard_mobile: true});
$('#go_time_5, #back_time_5').timepicki({  step_size_minutes:'5',show_meridian: false, min_hour_value: 0, max_hour_value: 23, overflow_minutes: true, increase_direction: 'up', disable_keyboard_mobile: true});
$('#go_time_6, #back_time_6').timepicki({step_size_minutes:'5',show_meridian: false, min_hour_value: 0, max_hour_value: 23, overflow_minutes: true, increase_direction: 'up', disable_keyboard_mobile: true});
$('#go_time_7, #back_time_7').timepicki({step_size_minutes:'5',show_meridian: false, min_hour_value: 0, max_hour_value: 23, overflow_minutes: true, increase_direction: 'up', disable_keyboard_mobile: true});
$('#timepicker_pick_fly').timepicki({
  step_size_minutes:'5',
  show_meridian:false,
  min_hour_value:0,
  max_hour_value:23,
  overflow_minutes:true,
  increase_direction:'up',
  disable_keyboard_mobile: true});
$('#timepicker_pick_fly1').timepicki({
    step_size_minutes:'5',
    show_meridian:false,
    min_hour_value:0,
    max_hour_value:23,
    overflow_minutes:true,
    increase_direction:'up',
    disable_keyboard_mobile: true});
$('#timepicker_drop_fly_1').timepicki({
    step_size_minutes:'5',
    show_meridian:false,
    min_hour_value:0,
    max_hour_value:23,
    overflow_minutes:true,
    increase_direction:'up',
    disable_keyboard_mobile: true});
$('#timepicker_pick_stn').timepicki({
    step_size_minutes:'5',
    show_meridian:false,
    min_hour_value:0,
    max_hour_value:23,
    overflow_minutes:true,
    increase_direction:'up',
    disable_keyboard_mobile: true});
$('#timepicker_pick_stn1').timepicki({
  step_size_minutes:'5',
  show_meridian:false,
  min_hour_value:0,
  max_hour_value:23,
  overflow_minutes:true,
  increase_direction:'up',
  disable_keyboard_mobile: true});
$('#timepicker_drop_stn').timepicki({
  step_size_minutes:'5',
  show_meridian:false,
  min_hour_value:0,
  max_hour_value:23,
  overflow_minutes:true,
  increase_direction:'up',
  disable_keyboard_mobile: true});
    $('#discounttimefrom').timepicki({
          step_size_minutes:'5',
        show_meridian: false,
        min_hour_value: 0,
        max_hour_value: 23,
        overflow_minutes: true,
        increase_direction: 'up',
        disable_keyboard_mobile: true});
    $('#discounttimeto').timepicki({
          step_size_minutes:'5',
        show_meridian: false,
        min_hour_value: 0,
        max_hour_value: 23,
        overflow_minutes: true,
        increase_direction: 'up',
        disable_keyboard_mobile: true});
     
    $('#drivermealfrom').timepicki({
          step_size_minutes:'5',
        show_meridian: false,
        min_hour_value: 0,
        max_hour_value: 23,
        overflow_minutes: true,
        increase_direction: 'up',
        disable_keyboard_mobile: true});
    $('#drivermealto').timepicki({
          step_size_minutes:'5',
        show_meridian: false,
        min_hour_value: 0,
        max_hour_value: 23,
        overflow_minutes: true,
        increase_direction: 'up',
        disable_keyboard_mobile: true});
    $('#driverothermealfrom').timepicki({
          step_size_minutes:'5',
        show_meridian: false,
        min_hour_value: 0,
        max_hour_value: 23,
        overflow_minutes: true,
        increase_direction: 'up',
        disable_keyboard_mobile: true});
    $('#driverothermealto').timepicki({
          step_size_minutes:'5',
        show_meridian: false,
        min_hour_value: 0,
        max_hour_value: 23,
        overflow_minutes: true,
        increase_direction: 'up',
        disable_keyboard_mobile: true});
    $('#nighttimefrom').timepicki({
          step_size_minutes:'5',
        show_meridian: false,
        min_hour_value: 0,
        max_hour_value: 23,
        overflow_minutes: true,
        increase_direction: 'up',
        disable_keyboard_mobile: true});
    $('#nighttimeto').timepicki({
          step_size_minutes:'5',
        show_meridian: false,
        min_hour_value: 0,
        max_hour_value: 23,
        overflow_minutes: true,
        increase_direction: 'up',
        disable_keyboard_mobile: true});
</script>
<?php if (in_array("homebooking", $css_type)) { ?>

    <?php echo $this->load->view('site/common/script'); ?>

<?php } ?>

<?php if (in_array("onlinebooking", $css_type) || in_array("passengerdetails", $css_type)) { ?>

    <?php echo $this->load->view('site/common/online_script'); ?>

<?php } ?>
<!-- <script src="<?php //echo base_url(); ?>assets/system_design/plugins/summernote/summernote.min.js"></script> -->

<!--Date Table-->
<?php if (in_array("datatable", $css_type)) { ?>
    <script src="<?php echo base_url(); ?>assets/system_design/scripts/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/system_design/scripts/datatables/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/system_design/scripts/datatables/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/system_design/scripts/datatables/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/system_design/scripts/datatables/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/system_design/scripts/datatables/buttons.html5.min.js"></script>
    <!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script> -->
    <script class="init">
        function toDate(dateStr) {
            if (dateStr) {
                // console.log(dateStr);
                var parts = dateStr.split("/");
                return new Date(parts[2], parts[1] - 1, parts[0])
            }

        }
        // Date range filter
        var minDateFilter = "";
        var maxDateFilter = "";

        var minAgeFilter = "";
        var maxAgeFilter = "";

        var dateIndex = $("#manageClientTable thead").find(".column-date").index();
        $.fn.dataTableExt.afnFiltering.push(
            function (oSettings, aData, iDataIndex) {
                if ($("#table-filter .dpo").length > 0) {
                    if (typeof aData._date == 'undefined') {
                        if (aData[dateIndex]) {
                            aData._date = toDate(aData[dateIndex]).getTime();
                        }
                    }

                    if (minDateFilter && !isNaN(minDateFilter)) {
                            //console.log(aData._date);
                            //console.log(minDateFilter);
                            //console.log("min");
                            //console.log(aData._date < minDateFilter);
                            if (aData._date < minDateFilter) {
                                return false;
                            }
                        }

                        if (maxDateFilter && !isNaN(maxDateFilter)) {

                            //console.log(maxDateFilter);
                            //console.log(aData._date);
                            //console.log("max")
                            //console.log(aData._date > maxDateFilter);
                            if (aData._date > maxDateFilter) {
                                return false;
                            }
                        }
                    }

                    return true;
                }
                );

        var ageIndex = $("#example thead").find(".column-age").index();
        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                var min = parseInt($('.table-filter input[data-name="age_from"]').val(), 10);
                var max = parseInt($('.table-filter input[data-name="age_to"]').val(), 10);
                    var age = parseFloat(data[ageIndex]) || 0; // use data for the age column

                    if ((isNaN(min) && isNaN(max)) ||
                        (isNaN(min) && age <= max) ||
                        (min <= age && isNaN(max)) ||
                        (min <= age && age <= max))
                    {
                        return true;
                    }
                    return false;
                }
                );

        if ($("#example thead").find(".column-first_name").length > 0) {
            $.fn.dataTable.ext.search.push(
                function (settings, data, dataIndex) {
                    var firstNameIndex = $("#example thead").find(".column-first_name").index();
                    var lastNameIndex = $("#example thead").find(".column-last_name").index();
                    var sName = $(document.body).find('.table-filter input[data-name="name"]').val();
                    var fnRegex = new RegExp(sName, 'i');
                    var fName = data[firstNameIndex].replace(/[^\w\s]|_/g, function ($1) {
                        return ' ' + $1 + ' ';
                    }).replace(/[ ]+/g, ' ').split(' ');
                    if (lastNameIndex != -1) {
                        var lName = data[lastNameIndex].replace(/[^\w\s]|_/g, function ($1) {
                            return ' ' + $1 + ' ';
                        }).replace(/[ ]+/g, ' ').split(' ');
                    }


                    return fnRegex.test(fName[0]) || fnRegex.test(fName[1]) || fnRegex.test(fName[2]) || fnRegex.test(lName[0]) || fnRegex.test(lName[1]) || fnRegex.test(lName[1]);
                }
                );
        }

        if ($("#example thead").find(".column-email").length > 0) {
            $.fn.dataTable.ext.search.push(
                function (settings, data, dataIndex) {
                    var emailIndex = $("#example thead").find(".column-email").index();
                    var email = $(document.body).find('.table-filter input[data-name="email"]').val();
                    var emailRegex = new RegExp(email, 'i');

                    var fName = data[emailIndex].replace(/[^\w\s]|_/g, function ($1) {
                        return ' ' + $1 + ' ';
                    }).replace(/[ ]+/g, ' ').split(' ');

                    return emailRegex.test(fName[0]) || emailRegex.test(fName[1]) || emailRegex.test(fName[2]);
                }
            );
        }

        if ($("#example thead").find(".column-phone").length > 0) {
            $.fn.dataTable.ext.search.push(
                function (settings, data, dataIndex) {
                    var phoneIndex = $("#example thead").find(".column-phone").index();
                    var phone = $(document.body).find('.table-filter input[data-name="phone"]').val();
                    var phoneRegex = new RegExp(phone, 'i');

                    var fName = data[phoneIndex].replace(/[^\w\s]|_/g, function ($1) {
                        return ' ' + $1 + ' ';
                    }).replace(/[ ]+/g, ' ').split(' ');

                    return phoneRegex.test(fName[0]) || phoneRegex.test(fName[1]) || phoneRegex.test(fName[2]);
                }
            );
        }

        if ($("#example thead").find(".column-status").length > 0) {
            $.fn.dataTable.ext.search.push(
                function (settings, data, dataIndex) {
                    var statusIndex = $("#example thead").find(".column-status").index();
                    var status = $(document.body).find('.table-filter select[data-name="status"]').val();
                    var statusRegex = new RegExp(status, 'i');

                    var fName = data[statusIndex].replace(/[^\w\s]|_/g, function ($1) {
                        return ' ' + $1 + ' ';
                    }).replace(/[ ]+/g, ' ').split(' ');

                    return statusRegex.test(fName[0]) || statusRegex.test(fName[1]) || statusRegex.test(fName[2]);
                }
            );
        }

        $('.bdatepicker,.hasDatepicker').datepicker({
            format: "dd/mm/yyyy"
        }).datepicker('setDate', 'today');
        
        $('.bsearchdatepicker,.hasDatepicker').datepicker({
            format: "dd/mm/yyyy"
        });
         //added by sultan

         var table = $('#bookingtable').DataTable( {
            lengthChange: false,
            buttons: [ 'csv', 'excel', 'pdf']
        } );

         table.buttons().container()
         .prependTo( '.bookingdatabtns' );
    //added by sultan
    var datatables1 = $('#example').DataTable({
        columnDefs: [
        {className: "dt-body-center ", targets: "_all"},
        {targets: 'no-sort', orderable: false}
        ],
        dom: '<"table-filter">B<"toolbar">frtip',
        language: {search: "", searchPlaceholder: "Search"},
        buttons: [
        'excelHtml5',
        'csvHtml5',
        'pdfHtml5'
        ],
        initComplete: function () {
            $("div.toolbar")
            .html('<a href="<?= base_url("admin/$active_class/add.php") ?>" class="btn btn-sm btn-default"><i class="fa fa-plus"></i> Add</a> <a href="#" class="btn btn-sm btn-default editBtn"><i class="fa fa-pencil"></i> Edit</a> <a href="#" class="btn btn-sm btn-default delBtn"><i class="fa fa-trash"></i> Delete</a>');
            var filterInp = $("#table-filter");
            if (filterInp.length > 0) {
                var tableFilter = $("div.table-filter");
                tableFilter.html(filterInp.html());

                if ($(".table-filter .dpo").length > 0) {
                    $('.table-filter .dpo[data-name="date_from"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            minDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                    $('.table-filter .dpo[data-name="date_to"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            maxDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                }
            }

            this.api().columns().every(function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                .appendTo($(column.footer()).empty())
                .on('change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                        );

                    column
                    .search(val ? '^' + val + '$' : '', true, false)
                    .draw();
                });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });

        },
        "drawCallback": function () {
            $('.dataTables_paginate .paginate_button').addClass('btn btn-default');
        }
    });
    var datatables = $('#example_language').DataTable({
        columnDefs: [
        {targets: 'no-sort', orderable: false}
        ],
        dom: '<"table-filter">B<"toolbar">frtip',
        language: {search: "", searchPlaceholder: "Search"},
        buttons: [
        'copyHtml5',
        'csvHtml5',
        'pdfHtml5'
        ],
        initComplete: function () {
            $("div.toolbar").html('<a href="javascript:void(0);" class="btn btn-sm btn-default" data-edit="0" onClick="setgoogletranslateAction();"><i class="fa fa-language"></i> Translate</a><a href="<?= base_url("admin/$active_class/add.php") ?>" class="btn btn-sm btn-default"><i class="fa fa-plus"></i> Add</a> <a href="javascript:void(0);" class="btn btn-sm btn-default " data-edit="0" onClick="setUpdateAction_on_same_page();"><i class="fa fa-pencil"></i> Edit</a> <a href="<?= base_url("admin/$active_class/importcsv.php") ?>" class="btn btn-sm btn-default delBtn"><i class="fa fa-upload"></i> Import Csv</a>');
            var filterInp = $("#table-filter");
            if (filterInp.length > 0) {
                var tableFilter = $("div.table-filter");
                tableFilter.html(filterInp.html());

                if ($(".table-filter .dpo").length > 0) {
                    $('.table-filter .dpo[data-name="date_from"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            minDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                    $('.table-filter .dpo[data-name="date_to"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            maxDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                }
            }
        },
        "drawCallback": function () {
            $('.dataTables_paginate .paginate_button').addClass('btn btn-default');
        }
    });
    $('#selectall').click(function (event) {
        if (this.checked) {
            $('.checkboxx').each(function () {
                        var that = $(this); //loop through each checkbox
                        $(this).prop('checked', true); //check
                        that.attr("data-tags", that.data('id')); 
                    });
        } else {
                    $('.checkboxx').each(function () { //loop through each checkbox
                        $(this).prop('checked', false); //uncheck              
                    });
                }
            });
    $('.checkboxx').click(function (event) {
        if (this.checked) {
            var that = $(this);
            that.attr("data-tags", that.data('id'));
            var check;
            $("input.checkboxx").each(function(index, element){
                if(element.checked){
                    check=true
                }
                else{
                    check=false;
                    return check
                }  
            });
            if(check==true){
                $('#selectall').prop('checked', true);
            }
        } else {
            $('#selectall').prop('checked', false); 
        }
    });
    $(document).ready(function () {

        if ($(".table-filter .dpo").length > 0) {
            $(".table-filter .dpo[data-name='date_from']").on("change", function () {
                minDateFilter = new Date(toDate(this.value)).getTime();
                datatables.draw();
                datatables1.draw();
            });
            $(".table-filter .dpo[data-name='date_to']").on("change", function () {
                maxDateFilter = new Date(toDate(this.value)).getTime();
                datatables.draw();
                datatables1.draw();
            });
        }
        $('.table-filter input[data-name="age_from"], .table-filter input[data-name="age_to"]').on("keyup change", function () {
            datatables.draw();
            datatables1.draw();
        });
        $(".table-filter input[data-name='name']").on("keyup", function () {
            datatables.draw();
            datatables1.draw();
        });
        $(".table-filter input[data-name='email']").on("keyup", function () {
            datatables.draw();
            datatables1.draw();
        });
        $(".table-filter select[data-name='status']").on("change", function () {
            datatables.draw();
            datatables1.draw();
        });
        $(".table-filter input[data-name='phone']").on("keyup", function () {
            datatables.draw();
            datatables1.draw();
        });
        $('.example').dataTable();
        $(".singleSelect:checked").prop("checked", false);
        <?php $segment = $this->uri->segment(2);
        if(trim($segment, " ") != "users"){
            ?>
        $(".singleSelect").click(function () {
            var that = $(this);
            var checked = that.is(":checked");
            $("table .singleSelect:checked").prop("checked", false);
            if (checked) {
                that.prop("checked", true);
                    //console.log(that.data('id'));
                    $("#example").attr("data-selected_id", that.data('id'))
                } else
                $("#example").attr("data-selected_id", "")
            });
        <?php } ?>

        var filterInputs = $(".table-filter input, .table-filter select");
        filterInputs.each(function (x, y) {
            var name = $(y).data('name');
                //console.log(name)
                if (name != undefined) {
                    $(y).on("change keyup", function () {
                        if ($.inArray(name, ['name', 'date_from', 'date_to', 'age_from', 'age_to']) === -1) {
                            var colIndex = $("#example thead").find(".column-" + name).index();
                            if (datatables.column(colIndex).search() !== this.value) {
                                datatables.column(colIndex).search(this.value).draw();
                            }
                        }
                    });
                }
            });

    });

</script>
<?php } ?>
<!--Date Table-->
<!--Date Table-->
<?php if (in_array("booking_datatable", $css_type)) { ?>
    <?php include('booking_table_config.php'); ?>
<?php } ?>
<!--Date Table-->



<script>
    function setUpdateAction_on_same_page(){
        $.each($(".checkbox:checked"), function () {
            var sentence = $(this).attr('data-value');
            var data_id = $(this).attr('data-id');
            var data_token = $(this).attr('data-token');
            var td_id = $(this).attr('id');
            $('#' + td_id + '_tdpre').css("background", "#FFF");
            $('#' + td_id + '_tdpre').css("color", "#000");
        });
    }
    function setgoogletranslateAction() {

        $.each($(".checkbox:checked"), function () {
            var sentence = $(this).attr('data-value');
            var data_id = $(this).attr('data-id');
            var data_token = $(this).attr('data-token');
            var td_id = $(this).attr('id');
            $('#' + td_id + '_tdpre')
            .css("background", "#FFF url(<?= base_url('assets/system_design/images/ajax-loader.gif'); ?>) no-repeat center right 5px / 25px");
            <?php if (isset($short_code)) : ?>
                if ('<?= $short_code; ?>' != 'en') {
                    var lang_id = '<?= $lang_id;?>';
                    $.ajax({
                        url: "https://www.googleapis.com/language/translate/v2?key=AIzaSyB-ktxnSZFDZnKjjPz6tbk_iDMN2FxkXmo&q=" + sentence + "&source=en&target=<?= @$short_code; ?>",
                        dataType: 'json',
                        success: function (res) {
                            var translated = "";
                            $.each(res.data.translations, function (index, val) {
                                translated = val.translatedText;
                                translate = translated.toLowerCase().replace(/\b[a-z]/g, function (letter) {
                                    return letter.toUpperCase();
                                });
                            });
                            $.ajax({

                                url: '<?= base_url("language/update_translation"); ?>',
                                type: "GET",
                                // data: 'column=translation&data='+translate+'&id='+ data_id + '&lang_id=' + <?= $lang_id; ?> + '&token_id=' + data_token,
                                data:{
                                  'column': 'translation', 
                                  'data': translate, 
                                  'id': data_id, 
                                  'lang_id': lang_id, 
                                  'token_id': data_token, 
                                },
                                success: function (data) {
                                    $('#' + td_id + '_tdpre').html(translate);
                                    $('#' + td_id + '_tdpre').css("background", "#FDFDFD");
                                }
                            });
                        }
                    });
                } else {
                    $.ajax({
                        url: '<?= base_url("language/update_translation"); ?>',
                        type: "GET",
                        data: 'column=translation&data=' + sentence
                        + '&id=' + data_id + '&lang_id=' + <?= $lang_id; ?> + '&token_id=' + data_token,
                        success: function (data) {
                            $('#' + td_id + '_tdpre').html(sentence);
                            $('#' + td_id + '_tdpre').css("background", "#FDFDFD");
                        }
                    });
                }
            <?php endif; ?>
        });
    }
    function setUpdateAction() {
        if ($('.lang_edit').attr('data-edit') == 1) {
            document.trasUser.action = "<?= base_url('language/edit_translation/'.$short_code); ?>";
            document.trasUser.submit();
        } else {
            alert('Select atlest one checkbox to edit');
        }
    }
    function setDeleteAction() {
        document.trasUser.action = "<?= base_url('language/delete_translation/'.$short_code); ?>";
        document.trasUser.submit();
    }

    $(function () {

     $(document.body).on("click", ".editBtn", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            window.location.href = "<?= site_url(); ?>admin/<?=$active_class ?>/" + checkBoxVal + "/edit.php"
        } else {
            alert("Select atleast one record");
        }
    });
    <?php $segment = $this->uri->segment(2);
        if(trim($segment, " ") != "users" && trim($segment, " ") != "pipline"){ ?>
     $(document.body).on("click", ".delBtn", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            $.ajax({
                url: "<?php echo base_url(); ?>admin/filemanagement/delete",
                method: "GET",
                data: {ids: checkBoxVal},
                error: function(xhr, status, error) {
                        // alert(xhr.responseText);
                        alert("Error: " + error);
                    },
                    success: function (msg) {
                        location.reload();
                    }
                });
        } else {
            alert("Select atleast one record");
        }
    });
<?php } ?>

     function getFormattedDate(date) {
        return date.getDate() + 
        "/" +  (date.getMonth() + 1) +
        "/" +  date.getFullYear();
    }
    $(document.body).on("click", "#searchbtn", function() {
        var type = $('#filter_type').val();
        var user = $('#filter_user').val();
        var _alert = $('#filter_alert').val();
        var nature = $('#filter_nature').val();
        var destination = $('#filter_destination').val();
        var priority=$('#filter_priority').val();
        var status=$('#filter_status').val();
        var filterToDate = $('#filter_To_date').val();
        var filterFromDate = $('#filter_From_date').val();
        var data = [type, user, _alert, nature, destination, priority, status, filterFromDate, filterToDate];
        $.ajax({
            url: "<?php echo base_url(); ?>admin/filemanagement/fileSearchData",
            method: "GET",
            data: {data: data},
            error: function(xhr, status, error) {
                alert("Error: " + error);
            },
            success: function (response) {
                response = JSON.parse(response);
                console.log(response.data);
                if ($.fn.dataTable.isDataTable('#example') != false) {
                    var table = $('#example').DataTable();
                    table.destroy();
                }
                $('#example').DataTable({
                    'data': response.data,
                    'order': [],
                    'paging': true,
                    'pageLength': 15,
                    'columnDefs': [ { "targets": 0, "className": "text-center", "width": "6%" },
                    { "targets": 1, "className": "text-center", "width": "6%" },
                    { "targets": 2, "className": "text-center", "width": "6%" },
                    { "targets": 3, "className": "text-center", "width": "6%" },
                    { "targets": 4, "className": "text-center", "width": "6%" },
                    { "targets": 5, "className": "text-center", "width": "6%" },
                    { "targets": 6, "className": "text-center", "width": "6%" },
                    { "targets": 7, "className": "text-center", "width": "6%" },
                    { "targets": 8, "className": "text-center", "width": "6%" },
                    { "targets": 9, "className": "text-center", "width": "6%" },
                    { "targets": 10, "className": "text-center", "width": "6%" },
                    { "targets": 11, "className": "text-center", "width": "6%" },
                    { "targets": 12, "className": "text-center", "width": "6%" },
                    { "targets": 13, "className": "text-center", "width": "6%" },
                    { "targets": 14, "className": "text-center", "width": "6%" },
                    { "targets": 15, "className": "text-center", "width": "6%" },
                    { "targets": 16, "className": "text-center", "width": "6%" },
                    { "targets": 17, "className": "text-center", "width": "6%" },
                    ],
                    dom: '<"table-filter">B<"toolbar">frtip',
                    language: {search: "", searchPlaceholder: "Search"},
                    buttons: [
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                    ],
                    initComplete: function () {
                        $("div.toolbar")
                        .html('<a href="<?= base_url("admin/$active_class/add.php") ?>" class="btn btn-sm btn-default"><i class="fa fa-plus"></i> Add</a> <a href="#" class="btn btn-sm btn-default editBtn"><i class="fa fa-pencil"></i> Edit</a> <a href="#" class="btn btn-sm btn-default delBtn"><i class="fa fa-trash"></i> Delete</a>');
                        var filterInp = $("#table-filter");
                        if (filterInp.length > 0) {
                            var tableFilter = $("div.table-filter");
                            tableFilter.html(filterInp.html());
                            if ($(".table-filter .dpo").length > 0) {
                                $('.table-filter .dpo[data-name="date_from"]').datepicker({
                                    format: "dd/mm/yyyy",
                                    "onSelect": function (date) {
                                        minDateFilter = new Date(date).getTime();
                                        datatables.fnDraw();
                                    }
                                });
                                $('.table-filter .dpo[data-name="date_to"]').datepicker({
                                    format: "dd/mm/yyyy",
                                    "onSelect": function (date) {
                                        maxDateFilter = new Date(date).getTime();
                                        datatables.fnDraw();
                                    }
                                });
                            }
                        }
                        this.api().columns().every(function () {
                            var column = this;
                            var select = $('<select><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                    );
                                column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                            });
                            column.data().unique().sort().each(function (d, j) {
                                select.append('<option value="' + d + '">' + d + '</option>')
                            });
                        });
                    },
                    "drawCallback": function () {
                        $('.dataTables_paginate .paginate_button').addClass('btn btn-default');
                    }
                });
    htmlString = 
    '<a class="btn btn-sm btn-default editBtn" style="margin-right:5px;">File Manager</a>';
    $( ".toolbar" ).prepend( htmlString );
}
});
});    

    $(document.body).on("click", ".driverselcet", function () {

        var that = $(this);
        var checked = that.is(":checked");
        $("table .driverselcet:checked").prop("checked", false);
        if (checked) {
            that.prop("checked", true);
                    //console.log(that.data('id'));
                    $("#manageDriverTable").attr("data-selected_id", that.data('id'))
                } else
                $("#manageDriverTable").attr("data-selected_id", "")
            });
    $(document.body).on("click", ".editBtn", function () {
        <?php $segment = $this->uri->segment(2);
        if(trim($segment, " ") == "drivers"){
            ?>
            var selected = $("#manageDriverTable").attr("data-selected_id");
            if (selected != "") {
                location.href = "<?= site_url(); ?>admin/<?= $active_class ?>/" + selected + "/edit.php"
            } else{
                alert("Please select a row to edit.");
            }
        <?php } ?>
    });

    $(document.body).on("click", ".delBtn", function () {
        <?php $segment = $this->uri->segment(2);
        if(trim($segment, " ") == "drivers"){
            ?>
            var selected = $("#manageDriverTable").attr("data-selected_id");
            if (selected != "") {
                location.href = "<?= site_url(); ?>admin/<?= $active_class ?>/" + selected + "/delete.php"
            } else{
                alert("Please select a row to delete.");
            }
        <?php }else if(trim($segment, " ") == "users"){ ?>
            var checkbox = $('.singleSelect:checked');
            if (checkbox.length > 0) {
                var checkBoxVal = [];
                $(checkbox).each(function() {
                    var val = $(this).attr('data-id');
                    checkBoxVal.push(val);
                });
                $.ajax({
                    url: "<?php echo base_url(); ?>admin/users/deleteusers",
                    method: "GET",
                    data: {arr: checkBoxVal},
                    error: function(xhr, status, error) {
                            // alert(xhr.responseText);
                            alert("Error: " + error);
                        },
                        success: function (msg) {
                            location.reload();
                        }
                    });
            } else {
                alert("Select atleast one record");
            }
        <?php //}else if(trim($segment, " ") == "pipline"){ ?>
            // var checkbox = $('.singleSelect:checked');
            // if (checkbox.length > 0) {
            //     var checkBoxVal = [];
            //     $(checkbox).each(function() {
            //         var val = $(this).attr('data-id');
            //         checkBoxVal.push(val);
            //     });
            //     $.ajax({
            //         url: "<?php echo base_url(); ?>admin/users/deletepiplines",
            //         method: "GET",
            //         data: {arr: checkBoxVal},
            //         error: function(xhr, status, error) {
            //                 // alert(xhr.responseText);
            //                 alert("Error: " + error);
            //             },
            //             success: function (msg) {
            //                 // location.reload();
            //             }
            //         });
            // } else {
            //     alert("Select atleast one record");
            // }
        <?php }else{ ?>
            var selected = $("#example").attr("data-selected_id");
            if (selected != "") {
                location.href = "<?= site_url(); ?>admin/<?= $active_class ?>/" + selected + "/delete.php"
            } else{
                alert("Please select a row to delete.");
            }
        <?php } ?>
    });
    $(document.body).on("click", ".clientselect", function () {

        var that = $(this);
        console.log(that.is(":checked"))
        var checked = that.is(":checked");
        $("table .clientselect:checked").prop("checked", false);
        if (checked) {
            that.prop("checked", true);
                    console.log(that.data('id'));
                    $(".clientTable").attr("data-selected_id", that.data('id'))
                } else
                $(".clientTable").attr("data-selected_id", "")
            });
        $(document.body).on("click", ".editBtn", function () {
        <?php $segment = $this->uri->segment(2);
        if(trim($segment, " ") == "clients"){
            ?>
            var selected = $(".clientTable").attr("data-selected_id");
            if (selected != "") {
                location.href = "<?= site_url(); ?>admin/<?= $active_class ?>/" + selected + "/edit.php"
            } else{
                alert("Please select a row to edit.");
            }
        <?php } ?>
        });

        $(document.body).on("click", ".delBtn", function () {
        <?php $segment = $this->uri->segment(2);
        if(trim($segment, " ") == "clients"){
            ?>
            var selected = $(".clientTable").attr("data-selected_id");
            if (selected != "") {
                location.href = "<?= site_url(); ?>admin/<?= @$active_class ?>/" + selected + "/delete.php"
            } else{
                alert("Please select a row to delete.");
            }
        <?php } ?>
        });
    $(document.body).on("click", ".replyBtn", function () {
        $(this).text(function (i, text) {
            return text == "Reply" ? "Close" : "Reply";
        });
        $(".replyDiv").toggleClass('hide');
    });

    $(document.body).on("click", ".addNote", function () {
        $("#noteDIv").append('<div class="row"> <div class="col-xs-10"> <input type="text" class="form-control" placeholder="Write note here" name="note[]"> </div> <div class="col-xs-2"> <button type="button" class="btn btn-circle btn-success btn-sm addNote"><i class="fa fa-plus"></i></button> <button type="button" class="btn btn-circle btn-danger btn-sm delNote"><i class="fa fa-minus"></i></button> </div> </div>');
    });

    $(document.body).on("click", ".addFile", function () {
        id=Math.ceil(Math.random(0,1)*1000);
        input =  '<label for="'+id+'"><i class="btn">Choose file</i><i class="name"></i></label><input id="'+id+'" type="file" name="attachment[]" style="display:none">';
        $("#attachDiv").append($('<div class="attach-main row"> <div class="col-xs-8"> ' + input + '</div> <div class="col-xs-4"> <button type="button" class="btn btn-circle btn-success btn-sm addFile"><i class="fa fa-plus"></i></button> <button type="button" class="btn btn-circle btn-danger btn-sm delFile"><i class="fa fa-minus"></i></button></div></div>'));
    });

    $(document.body).on("click", ".delFile", function () {
        $(this).closest('.attach-main').remove();
    });
    $(document.body).on("click", ".delNote", function () {
        $(this).closest('.row').remove();
    });
});
</script>

<script type="text/javascript" class="init">

    function setActiveOnlinePackage(id) {
        numid = id.split('_')[1];
        $('#cars_data_list div').removeClass('active');
        $('li').removeClass('active');
        $('#' + id).parent().closest('ul').addClass('active');
        $('#' + id).parent().parent().addClass('car-sel-bx active');

    }

</script>
<!--Slider-->
<?php if (in_array("slider", $css_type)) { ?>
<?php } ?>
<!--Slider-->

<script type="text/javascript">
    $('.partners').bxSlider({
        minSlides: 5,
        maxSlides: 8,
        slideWidth: 240,
        slideMargin: 10,
        infiniteLoop: true,
        auto: true
    });
    $url = "<?php echo site_url(); ?>";
    $lang = $url.split(".fr/");
    if ($lang[1] == 'en') {
        $(".liContactUs").css("width", "108px");
        $(".liContactUs").css("text-align", "center");
        $(".top-links").css("margin-left", "0px");
        //  $(".copyright-left").css("font-size","9px");
    } else if ($lang[1] == 'fr') {
        $(".liContactUs").css("width", "147px");
        $(".liContactUs").css("text-align", "center");
        $(".liContactUs a").css("font-size", "12px");
        $(".top-links").css("margin-left", "-50px");
        //  $(".copyright-left").css("font-size","9px");
    }
    $(".scroll-up > .bx-wrapper").css("max-width", "570px");
    $(document).ready(function () {
        $("select[id='num2']").change(function () {
            var val = $(this).val();
            var type = $("#replyType").val();
            if (val != "") {
                $.ajax({
                    type: 'POST',
                    url: "<?php echo site_url(); ?>welcome/getReply",
                    data: '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&val=' + val + '&type=' + type,
                    cache: false,
                    success: function (data) {
                        CKEDITOR.instances['reply_message'].insertHtml(data);
                    }
                });
            }
        });
    });

    $('#allReminders').click(function (event) {
      if (this.checked) {
            $('.ccheckbox').each(function () { //loop through each checkbox
                $(this).prop('checked', true); //check 
            });
        } else {
            $('.ccheckbox').each(function () { //loop through each checkbox
                $(this).prop('checked', false); //uncheck              
            });
        }
    });
    $('#allRoles').click(function (event) {
      if (this.checked) {
            $('.checkbox').each(function () { //loop through each checkbox
                $(this).prop('checked', true); //check 
            });
        } else {
            $('.checkbox').each(function () { //loop through each checkbox
                $(this).prop('checked', false); //uncheck              
            });
        }
    });
    $("#statut").on('change', function () {
        var statut_id = $(this).val();
        if (statut_id == 2) {
            $(".divCompany").show();
        } else {
            $(".divCompany").hide();
        }
    });
</script>
<script class="init">

    <?php if (in_array("datatable", $css_type)) { ?>
    var dateIndex = $(".driverInnerTable thead").find(".column-date").index();
    var quoteDateIndex = $("#example").find(".column-date").index();
    $.fn.dataTableExt.afnFiltering.push(
        function (oSettings, aData, iDataIndex) {
            var newIndex = dateIndex;
            if (newIndex < 0) {
                newIndex = quoteDateIndex;
            }
            if ($("#table-filter .dpo").length > 0) {
                if (typeof aData._date == 'undefined') {
                    if (aData[newIndex]) {
                        aData._date = toDate(aData[newIndex]).getTime();
                    }
                }

                if (minDateFilter && !isNaN(minDateFilter)) {
                        //console.log(aData._date);
                        //console.log(minDateFilter);
                        //console.log("min");
                        //console.log(aData._date < minDateFilter);
                        if (aData._date < minDateFilter) {
                            return false;
                        }
                    }

                    if (maxDateFilter && !isNaN(maxDateFilter)) {

                        //console.log(maxDateFilter);
                        //console.log(aData._date);
                        //console.log("max")
                        //console.log(aData._date > maxDateFilter);
                        if (aData._date > maxDateFilter) {
                            return false;
                        }
                    }
                }

                return true;
            }
            );

    var ageIndex = $(".driverInnerTable thead").find(".column-age").index();
    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var min = parseInt($('.table-filter input[data-name="age_from"]').val(), 10);
            var max = parseInt($('.table-filter input[data-name="age_to"]').val(), 10);
                var age = parseFloat(data[ageIndex]) || 0; // use data for the age column

                if ((isNaN(min) && isNaN(max)) ||
                    (isNaN(min) && age <= max) ||
                    (min <= age && isNaN(max)) ||
                    (min <= age && age <= max))
                {
                    return true;
                }
                return false;
            }
            );

    if ($(".driverInnerTable thead").find(".column-first_name").length > 0) {
        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                var firstNameIndex = $(".driverInnerTable thead").find(".column-first_name").index();
                var lastNameIndex = $(".driverInnerTable thead").find(".column-last_name").index();
                var sName = $(document.body).find('.table-filter input[data-name="name"]').val();
                var fnRegex = new RegExp(sName, 'i');
                var fName = data[firstNameIndex].replace(/[^\w\s]|_/g, function ($1) {
                    return ' ' + $1 + ' ';
                }).replace(/[ ]+/g, ' ').split(' ');
                if (lastNameIndex != -1) {
                    var lName = data[lastNameIndex].replace(/[^\w\s]|_/g, function ($1) {
                        return ' ' + $1 + ' ';
                    }).replace(/[ ]+/g, ' ').split(' ');
                }


                return fnRegex.test(fName[0]) || fnRegex.test(fName[1]) || fnRegex.test(fName[2]) || fnRegex.test(lName[0]) || fnRegex.test(lName[1]) || fnRegex.test(lName[1]);
            }
            );
    }

    $('.bdatepicker').datepicker({
        format: "dd/mm/yyyy"
    });

    var datatables = $('.driverInnerTable').DataTable({
        columnDefs: [
        {targets: 'no-sort', orderable: false}
        ],
        dom: '<"table-filter">B<"toolbar">frtip',
        language: {search: "", class: "form-control", searchPlaceholder: "Search"},
        buttons: [
        'excelHtml5',
        'csvHtml5',
        'pdfHtml5'
        ],
        initComplete: function () {
            $("div.toolbar")
            .html('<div class="addevent"><div class="removeevent"></div></div>');
            var filterInp = $("#table-filter");
            if (filterInp.length > 0) {
                var tableFilter = $("div.table-filter");
                tableFilter.html(filterInp.html());

                if ($(".table-filter .dpo").length > 0) {
                    $('.table-filter .dpo[data-name="date_from"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            minDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                    $('.table-filter .dpo[data-name="date_to"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            maxDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                }
            }

            this.api().columns().every(function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                .appendTo($(column.footer()).empty())
                .on('change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                        );

                    column
                    .search(val ? '^' + val + '$' : '', true, false)
                    .draw();
                });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });

        },
        "drawCallback": function () {
            $('.dataTables_paginate .paginate_button').addClass('btn btn-default');
        }
    });
    var datatables = $('.driverInnerTable_language').DataTable({
        columnDefs: [
        {targets: 'no-sort', orderable: false}
        ],
        dom: '<"table-filter">B<"toolbar">frtip',
        language: {search: "", searchPlaceholder: "Search"},
        buttons: [
        'copyHtml5',
        'csvHtml5',
        'pdfHtml5'
        ],
        initComplete: function () {
            $("div.toolbar").html('<a href="<?= base_url("admin/$active_class/add.php") ?>" class="btn btn-sm btn-default"><i class="fa fa-plus"></i> Add</a> <a href="javascript:void(0);" class="btn btn-sm btn-default lang_edit" data-edit="0" onClick="setUpdateAction();"><i class="fa fa-pencil"></i> Edit</a> <a href="javascript:void(0);" class="btn btn-sm btn-default lang_edit" data-edit="0" onClick="setgoogletranslateAction();"><i class="fa fa-language"></i> Translate</a>  <a href="<?= base_url("admin/$active_class/importcsv.php") ?>" class="btn btn-sm btn-default delBtn"><i class="fa fa-upload"></i> Import Csv</a>');
            var filterInp = $("#table-filter");
            if (filterInp.length > 0) {
                var tableFilter = $("div.table-filter");
                tableFilter.html(filterInp.html());

                if ($(".table-filter .dpo").length > 0) {
                    $('.table-filter .dpo[data-name="date_from"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            minDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                    $('.table-filter .dpo[data-name="date_to"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            maxDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                }
            }
        },
        "drawCallback": function () {
            $('.dataTables_paginate .paginate_button').addClass('btn btn-default');
        }
    });
    $(document).ready(function () {

        if ($(".table-filter .dpo").length > 0) {
            $(".table-filter .dpo[data-name='date_from']").on("change", function () {
                minDateFilter = new Date(toDate(this.value)).getTime();
                datatables.draw();
            });
            $(".table-filter .dpo[data-name='date_to']").on("change", function () {
                maxDateFilter = new Date(toDate(this.value)).getTime();
                datatables.draw();
            });
        }
        $('.table-filter input[data-name="age_from"], .table-filter input[data-name="age_to"]').on("keyup change", function () {
            datatables.draw();
        });
        $(".table-filter input[data-name='name']").on("keyup", function () {
            datatables.draw();
        });
        $(".table-filter input[data-name='email']").on("keyup", function () {
            datatables.draw();
        });
        $('.example').dataTable();
        $('.table-filter input').addClass('form-control'); // <-- add this line
        $(".singleSelect:checked").prop("checked", false);
        <?php $segment = $this->uri->segment(2);
        if(trim($segment, " ") != "users"){
            ?>
        $(".singleSelect").click(function () {
            var that = $(this);
            var checked = that.is(":checked");
            $("table .singleSelect:checked").prop("checked", false);
            if (checked) {
                that.prop("checked", true);
                //console.log(that.data('id'));
                $(".driverInnerTable").attr("data-selected_id", that.data('id'))
            } else
            $(".driverInnerTable").attr("data-selected_id", "")
        });
    <?php } ?>

        var filterInputs = $(".table-filter input, .table-filter select");
        filterInputs.each(function (x, y) {
            var name = $(y).data('name');
            //console.log(name)
            if (name != undefined) {
                $(y).on("change keyup", function () {
                    if ($.inArray(name, ['name', 'date_from', 'date_to', 'age_from', 'age_to']) === -1) {
                        var colIndex = $(".driverInnerTable thead").find(".column-" + name).index();
                        if (datatables.column(colIndex).search() !== this.value) {
                            datatables.column(colIndex).search(this.value).draw();
                        }
                    }
                });
            }
        });

    });
    var dtable = $('.driverInnerTable').DataTable();
    $('.filter').on('keyup change', function() {
  //clear global search values
  dtable.search('');
  dtable.column($(this).data('columnIndex')).search(this.value).draw();

  var ageIndex = $("#manageClientTable thead").find(".column-age").index();
        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                var min = parseInt($('.table-filter input[data-name="age_from"]').val(), 10);
                var max = parseInt($('.table-filter input[data-name="age_to"]').val(), 10);
                    var age = parseFloat(data[ageIndex]) || 0; // use data for the age column

                    if ((isNaN(min) && isNaN(max)) ||
                        (isNaN(min) && age <= max) ||
                        (min <= age && isNaN(max)) ||
                        (min <= age && age <= max))
                    {
                        return true;
                    }
                    return false;
                }
                );

        if ($("#manageClientTable thead").find(".column-first_name").length > 0) {
            $.fn.dataTable.ext.search.push(
                function (settings, data, dataIndex) {
                    var firstNameIndex = $("#manageClientTable thead").find(".column-first_name").index();
                    var lastNameIndex = $("#manageClientTable thead").find(".column-last_name").index();
                    var sName = $(document.body).find('.table-filter input[data-name="name"]').val();
                    var fnRegex = new RegExp(sName, 'i');
                    var fName = data[firstNameIndex].replace(/[^\w\s]|_/g, function ($1) {
                        return ' ' + $1 + ' ';
                    }).replace(/[ ]+/g, ' ').split(' ');
                    if (lastNameIndex != -1) {
                        var lName = data[lastNameIndex].replace(/[^\w\s]|_/g, function ($1) {
                            return ' ' + $1 + ' ';
                        }).replace(/[ ]+/g, ' ').split(' ');
                    }


                    return fnRegex.test(fName[0]) || fnRegex.test(fName[1]) || fnRegex.test(fName[2]) || fnRegex.test(lName[0]) || fnRegex.test(lName[1]) || fnRegex.test(lName[1]);
                }
                );
        }
        var datatables = $('#manageClientTable').DataTable({
        columnDefs: [
        {targets: 'no-sort', orderable: false}
        ],
        dom: '<"table-filter">B<"toolbar">frtip',
        language: {search: "", searchPlaceholder: "Search"},
        buttons: [
        'excelHtml5',
        'csvHtml5',
        'pdfHtml5'
        ],
        initComplete: function () {
            $("div.toolbar")
            .html('<a href="<?= base_url("admin/$active_class/add.php") ?>" class="btn btn-sm btn-default"><i class="fa fa-plus"></i> Add</a> <a href="#" class="btn btn-sm btn-default editBtn"><i class="fa fa-pencil"></i> Edit</a> <a href="#" class="btn btn-sm btn-default delBtn"><i class="fa fa-trash"></i> Delete</a>');
            var filterInp = $("#table-filter");
            if (filterInp.length > 0) {
                var tableFilter = $("div.table-filter");
                tableFilter.html(filterInp.html());

                if ($(".table-filter .dpo").length > 0) {
                    $('.table-filter .dpo[data-name="date_from"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            minDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                    $('.table-filter .dpo[data-name="date_to"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            maxDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                }
            }

            this.api().columns().every(function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                .appendTo($(column.footer()).empty())
                .on('change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                        );

                    column
                    .search(val ? '^' + val + '$' : '', true, false)
                    .draw();
                });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });

        },
        "drawCallback": function () {
            $('.dataTables_paginate .paginate_button').addClass('btn btn-default');
        }
    });
    var datatables = $('#manageClientTable_language').DataTable({
        columnDefs: [
        {targets: 'no-sort', orderable: false}
        ],
        dom: '<"table-filter">B<"toolbar">frtip',
        language: {search: "", searchPlaceholder: "Search"},
        buttons: [
        'copyHtml5',
        'csvHtml5',
        'pdfHtml5'
        ],
        initComplete: function () {
            $("div.toolbar").html('<a href="<?= base_url("admin/$active_class/add.php") ?>" class="btn btn-sm btn-default"><i class="fa fa-plus"></i> Add</a> <a href="javascript:void(0);" class="btn btn-sm btn-default lang_edit" data-edit="0" onClick="setUpdateAction();"><i class="fa fa-pencil"></i> Edit</a> <a href="javascript:void(0);" class="btn btn-sm btn-default lang_edit" data-edit="0" onClick="setgoogletranslateAction();"><i class="fa fa-language"></i> Translate</a>  <a href="<?= base_url("admin/$active_class/importcsv.php") ?>" class="btn btn-sm btn-default delBtn"><i class="fa fa-upload"></i> Import Csv</a>');
            var filterInp = $("#table-filter");
            if (filterInp.length > 0) {
                var tableFilter = $("div.table-filter");
                tableFilter.html(filterInp.html());

                if ($(".table-filter .dpo").length > 0) {
                    $('.table-filter .dpo[data-name="date_from"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            minDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                    $('.table-filter .dpo[data-name="date_to"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            maxDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                }
            }
        },
        "drawCallback": function () {
            $('.dataTables_paginate .paginate_button').addClass('btn btn-default');
        }
    });
});

    $(".dataTables_filter input").on('keyup change', function() {
    //clear column search values
    dtable.columns().search('');
    //clear input values
    $('.filter').val('');
});

</script>
    <?php } ?>
<style>
    .dataTables_length, .dataTables_filter, .dataTables_info, .dataTables_paginate {
        /* display: block; */
    }
    .dataTables_filter label > input {
        /* visibility: visible; */
    }
</style>

</body>
</html>
