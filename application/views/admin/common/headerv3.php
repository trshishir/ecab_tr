<?php $page_lang = 'lang="fr"'; ?>
<!DOCTYPE html>
<html <?php echo $page_lang; ?>>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="<?php if (isset($meta_keywords)) echo $meta_keywords; ?>"/>
    <meta name="description" content="<?php if (isset($meta_description)) echo $meta_description; ?>"/>
    <link rel='shortcut icon' href='<?php echo base_url(); ?>assets/system_design/images/favicon.ico' type='image/x-icon'/>
    <title><?php echo $title; ?></title>
    <!-- Bootstrap -->
     <link href="<?php echo base_url(); ?>assets/system_design/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/system_design/css/buttons.dataTables.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/system_design/css/wimpy.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/system_design/css/styles.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/system_design/css/callback.css" rel="stylesheet">
    <?php if (isset($site_settings->site_theme) && $site_settings->site_theme == "Red") { ?>
        <link href="<?php echo base_url(); ?>assets/system_design/css/cab-2ntheame.css" rel="stylesheet">
    <?php } else { ?>
        <link href="<?php echo base_url(); ?>assets/system_design/css/cab.css" rel="stylesheet">
        <!-- <link href="<?php // echo base_url(); ?>assets/system_design/css/custom-style.css" rel="stylesheet"> -->
        <link href="<?php echo base_url(); ?>assets/system_design/css/admin-style.css" rel="stylesheet">
    <?php } ?>
    
      <!--added by sultan-->
         <?php
            if($active_class=="bookings"){?>

                 <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/default/css/style.css">

          <?php  } ?>
         <!-- added by sultan-->
    <link href="<?php echo base_url(); ?>assets/system_design/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css"  rel="stylesheet">
    <script src="<?php echo base_url(); ?>assets/system_design/scripts/jquery.js"></script>
    <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
    <script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/timepicker@1.13.0/jquery.timepicker.min.css" />
    <script src="<?php echo base_url(); ?>assets/system_design/scripts/chosen.jquery.min.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
   
    <script src="<?php echo base_url(); ?>/assets/system_design/form_validation/js/jquery.validate.min.js"
        type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/assets/system_design/form_validation/js/additional-methods.min.js"
            type="text/javascript"></script>
             <script src="<?php echo base_url(); ?>/assets/system_design/plugins/tinymce/tinymce.min.js" type="text/javascript"></script>
             <script src="<?php echo base_url(); ?>/assets/system_design/plugins/tinymce/jquery.tinymce.min.js" type="text/javascript"></script>
             <script src="<?php echo base_url(); ?>/assets/system_design/plugins/tinymce/plugins/image/plugin.min.js" type="text/javascript"></script>
              <script src="<?php echo base_url(); ?>/assets/system_design/plugins/tinymce/plugins/code/plugin.min.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>/assets/system_design/ckeditor.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>/assets/system_design/ckeditor.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>/assets/js/ckeditor.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>/assets/js/sample.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>/assets/system_design/scripts/html2canvas.js" type="text/javascript"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/timepicker@1.13.0/jquery.timepicker.min.js"></script>
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/emojis_assets/css/jquery.emojipicker.css">
            <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.2.min.js"></script> -->
            <script type="text/javascript" src="<?php echo base_url(); ?>/emojis_assets/js/jquery.emojipicker.js"></script>
            <!-- Emoji Data -->
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/emojis_assets/css/jquery.emojipicker.tw.css">
            <script type="text/javascript" src="<?php echo base_url(); ?>/emojis_assets/js/jquery.emojis.js"></script>
            <script type="text/javascript">
                $(document).ready(function (e) {
                    $('#input-default').emojiPicker();
                    $('#input-custom-size').emojiPicker({
                        width: '300px',
                        height: '200px'
                    });
                    $('#input-left-position').emojiPicker({
                        position: 'left'
                    });
                    $('#create').click(function (e) {
                        e.preventDefault();
                        $('#text-custom-trigger').emojiPicker({
                            width: '300px',
                            height: '200px',
                            button: false
                        });
                    });
                    $('#toggle').click(function (e) {
                        e.preventDefault();
                        $('#text-custom-trigger').emojiPicker('toggle');
                    });
                    $('#destroy').click(function (e) {
                        e.preventDefault();
                        $('#text-custom-trigger').emojiPicker('destroy');
                    })
                // keyup event is fired
                $(".emojiable-question, .emojiable-option").on("keyup", function () {
                    //console.log("emoji added, input val() is: " + $(this).val());
                });
            });
        </script>
        <?php
        $this->load->model('calls_model');
        $this->load->model('request_model');
        $this->load->model('jobs_model');
        $this->load->model('support_model');
        $data = [];
        $data['request'] = $this->request_model->getAll();
        $data['jobs'] = $this->jobs_model->getAll();
        $data['calls'] = $this->calls_model->getAll();
        $data['support'] = $this->support_model->getAll();
        $drivers_request_count = $this->support_model->headerCount();
        foreach ($data as $key => $d) {
            if ($d != false) {
                foreach ($d as $i) {
                    if (!empty($i->status))
                        $this->data[$key][strtolower($i->status)] = isset($this->data[$key][strtolower($i->status)]) ? $this->data[$key][strtolower($i->status)] + 1 : 1;
                }
            }
        }
        ?>
    </head>
    <body>
    <div id="loaderDiv"><img src="<?= base_url()?>assets/theme/default/images/carloader.gif"></div>
    <div id="loaderTable"><img src="<?= base_url()?>assets/theme/default/images/carloader.gif"></div>
        <?php //echo $googleApiUrl; ?>
        <header style="">
            <div class="header-wrapper" style="padding: 0;">
                <div class="container-fluid body-border" > <!--  navbar-fixed-top-->
                    <div class="top-section">
                        <div class="row-fluid">
                            <div class="col-md-12">
                                <ul class="nav topbar-nav menu pull-left">
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#myModal" class="btn btn-default btn-mar messenger-btn" >
                                            <i class="fa fa-comments-o"></i> <span class="badge badge-danger navbar-badge">0</span> Messenger <span class="messenger-dot">...</span>
                                        </a>
                                    </li>
                                    <li>
                                        <script src="<?php echo base_url(); ?>assets/system_design/scripts/wimpy.js"></script>
                                        <!-- Wimpy Player Instance -->
                                        <div class="he-player" data-wimpyplayer data-media="http://direct.francebleu.fr/live/fb1071-midfi.mp3" data-skin="<?php echo base_url(); ?>assets/system_design/scripts/wimpy.skins/001.tsv"></div>
                                    </li>
                                    <li>
                                        <span class="btn btn-time" id="datepicker">
                                            <i id="topbar_time"><?php echo date('l d F Y H:i:s'); ?>...</i>
                                        </span>
                                        <script>
                                            var phptimezone = "<?php echo date_default_timezone_get(); ?>";
                                            // new Date().toLocaleString("en-US", {timeZone: "Europe/Paris"})
                                            $(document).ready(function () {
                                                var daysNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                                                var monthsNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                                                var monthsNames2 = ["January", "February", "March", "April", "May", "June",
                                                "July", "August", "September", "October", "November", "December"];
                                                setInterval(function () {
                                                    var d = new Date().toLocaleString("en-US", {timeZone: phptimezone});
                                                    d = new Date(d);
                                                    var date = `${daysNames[d.getDay()]} ${d.getDate()} ${monthsNames[d.getMonth()]} ${d.getFullYear()}`;
                                                    var time = `${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`;
                                                    $('#topbar_time').html(date + " <br> " + time);
                                                }, 1000);





                                            $("#datepicker").datepicker({
                                                changeMonth: true,
                                                changeYear: true,
                                            });
                                            $("#datepicker").datepicker("setDate", new Date());



                                            });

                                      </script>
                                  </li>
                                  <li>
                                    <?php include_once('weather.php'); ?>
                                </li>
                            </ul>
                                <?php
                                $languages = $this->db->where('status', 1)->order_by('title', 'ASC')->get('vbs_languages')->result();
                                ?>
                            <ul class="nav topbar-nav menu pull-right">
                                <li>
                                    <a href="<?php echo site_url(); ?>admin/clients.php" class="btn btn-default btn-mar" >
                                        <i class="fa fa-user"></i>
                                        <span class="badge badge-danger navbar-badge"><?php echo (isset($total_clients) ? $total_clients : 0); ?>
                                    </span>
                                    Clients
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo site_url(); ?>admin/driver_info.php" class="btn btn-default btn-mar" >
                                    <i class="fa fa-user"></i>
                                    <span class="badge badge-danger navbar-badge"><?php echo (isset($total_drivers) ? $total_drivers : 0); ?>
                                </span>
                                Drivers
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo site_url(); ?>admin/cars.php" class="btn btn-default btn-mar" >
                                <i class="fa fa-user"></i>
                                <span class="badge badge-danger navbar-badge"><?php echo "10"; //  isset($this->data['jobs']) ? array_pop(array_reverse($this->data['jobs'])) : 0 ?>
                            </span>
                            Cars
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url(); ?>admin/partners.php" class="btn btn-default btn-mar" >
                            <i class="fa fa-user"></i>
                            <span class="badge badge-danger navbar-badge"><?php echo (isset($total_partners) ? $total_partners : 0); ?>
                        </span>
                        Partners
                    </a>
                </li>
                <li>
                        <a href="<?php echo site_url(); ?>admin/affilation_applications.php" class="btn btn-default btn-mar" >
                            <i class="fa fa-user"></i>
                            <span class="badge badge-danger navbar-badge"><?php echo (isset($total_partners) ? $total_partners : 0); ?>
                        </span>
                        Affiliation
                    </a>
                </li>
                <li>
                    <a href="<?php echo site_url(); ?>admin/applications.php" class="btn btn-default btn-mar" >
                        <i class="fa fa-registered"></i>
                        <span class="badge badge-danger navbar-badge"><?php echo (isset($total_jobseekers) ? $total_jobseekers : 0); ?>
                    </span>
                    Applications
                </a>
            </li>
            <li>
                <a href="<?php echo site_url(); ?>admin/calls.php" class="btn btn-default btn-mar" >
                    <i class="fa fa-phone"></i>
                    <span class="badge badge-danger navbar-badge"><?php echo "10"; // isset($this->data['calls']) ? array_pop(array_reverse($this->data['calls'])) : 0 ?>
                </span>
                <?php echo $this->lang->line('calls'); ?>
            </a>
        </li>
        <li>
            <a href="<?php echo site_url(); ?>admin/supports.php" class="btn btn-default btn-mar" >
                <i class="fa fa-support"></i> 
                <span class="badge badge-danger navbar-badge"><?php echo "23"; //  isset($this->data['support']) ? $this->data['support']['new'] : 0 ?>
            </span>
            <?php echo $this->lang->line('supports'); ?>
        </a>
    </li>
    <li>
        <a href="<?php echo site_url(); ?>admin/quote_requests.php" class="btn btn-default btn-mar" >
            <i class="fa fa-paper-plane"></i>
            <span class="badge badge-danger navbar-badge"><?php echo "23"; ?>
            <?php // if (!isset($this->data['request']['new'])) $this->data['request']['new'] = ''; ?>
            <?php // isset($this->data['request']) ? $this->data['request']['new'] : 0 ?>
            <?php // isset($this->data['request']) ? array_pop(array_reverse($this->data['request'])) : 0 ?>
        </span>Quotes
    </a>
</li>
<li>
    <a href="<?php echo site_url(); ?>admin/drivers_requests.php" class="btn btn-default btn-mar">
        <i class="fa fa-paper-plane"></i> 
        <span class="badge badge-danger navbar-badge"><?php echo count($drivers_request_count); ?></span>Requests
    </a>
</li>
<li>
    <a href="<?= base_url(); ?>admin/files.php" class="btn btn-default btn-mar">
        <i class="fa fa-file-o"></i> <span class="badge badge-danger navbar-badge">0</span>
        <?php echo $this->lang->line('files'); ?>
    </a>
</li>
<li>
    <a href="<?= base_url(); ?>admin/tasks.php" class="btn btn-default btn-mar">
        <i class="fa fa-tasks"></i> <span class="badge badge-danger navbar-badge">0</span>
        <?php echo $this->lang->line('task'); ?>
    </a>
</li>

<li class="lang-switch">
    <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"> 
        <img class='en-icon' src="<?= base_url('assets/system_design/images/' . substr($this->config->item('language'),0,2) . '-icon.png'); ?>" />
        <?= ucwords($this->config->item('language')); ?><b class="caret"></b>
    </a>
    <ul class="dropdown-menu">
        <?php
        foreach ($languages as $lang) :
            ?>
            <li>
                <a href="<?= base_url() ?>global_controller/set_language/<?= $lang->title ?>">
                    <img class='en-icon' src="<?php echo base_url().$lang->flag ?>" />
                    <?= ucwords($lang->title); ?>
                </a>
            </li>
        <?php
        endforeach;
        ?>
</li>
<li class="lang-switch">

    <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"> 
        <?php
        if($company_data['default_currency'] =='usd')
            echo '&#36; USD';
        if($company_data['default_currency'] =='euro')
            echo '&euro; EURO';


        ?>
        <b class="caret"></b>
    </a>
    <ul class="dropdown-menu">                

        <li>
            <a>
                &euro; EURO
            </a>
        </li>

        <li>
            <a>
             &#36; USD
         </a>
     </li>
 </ul>
</li>
                                    <!--<li class="userli">
                                        <span class="greetings"><?php //echo (CURRENT_DAY == 'PM') ? 'Good Evening' : 'Good Afternoon';?></span>
                                    </li>-->
                                    <li class="users-submenu">
                                        <span class="user-box">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false" style="color: #111;">
                                                <span class='user-status online'></span>
                                                <?php echo $user->civility . ' ' . $user->first_name . ' ' . $user->last_name; ?> 
                                                <span class="user-icon">
                                                      <!-- code by s_a -->
                                                    <?php if(empty($user->image)): ?>
                                                    <img src="<?php echo base_url(); ?>assets/system_design/images/user-160x160.png" alt="<?php echo $user->first_name . ' ' . $user->last_name; ?>" title="<?php echo $user->first_name . ' ' . $user->last_name; ?>" />
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url(); ?>uploads/user/<?= $user->image ?>" alt="<?php echo $user->first_name . ' ' . $user->last_name; ?>" title="<?php echo $user->first_name . ' ' . $user->last_name; ?>" />
                                                    <?php endif; ?>
                                                     <!-- code by s_a -->
                                                </span>
                                                <b class="caret"></b>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li class="dropdown-submenu">
                                                    <div class="dropdown" style = "width:100%;">
                                                        <button class="dropbtn btn-default" style = "width:100%;text-align:left;"><i style = "font-weight:bold;" class="fa fa-angle-left"></i> <i class="fa fa-fw fa-cogs"></i><?php echo $this->lang->line('settings'); ?></button>
                                                        <div class="dropdown-content">
                                                            <a class="btn-default" href="<?php echo base_url('admin/company.php'); ?>"><i class="fa fa-fw fa-file"></i> <?php echo $this->lang->line('company_profile'); ?></a>
                                                            <a class="btn-default" href="<?php echo base_url('admin/users'); ?>"><i class="fa fa-fw fa-users"></i> <?php echo $this->lang->line('users'); ?></a>
                                                            <a class="btn-default" href="<?php echo base_url('admin/roles'); ?>"><i class="fa fa-fw fa-tag"></i> Roles</a>
                                                            <a class="btn-default" href="<?php echo base_url('admin/departments'); ?>"><i class="fa fa-fw fa-table"></i><?php echo $this->lang->line('departments'); ?></a>
                                                            <a class="btn-default" href="<?php echo base_url('admin/quick_replies.php'); ?>"><i class="fa fa-fw fa-commenting"></i> <?php echo $this->lang->line('quick_replies'); ?></a>
                                                            <a class="btn-default" href="<?php echo base_url('admin/smtp.php'); ?>"><i class="fa fa-fw fa-envelope"></i> <?php echo $this->lang->line('smtp'); ?></a>
                                                            <a class="btn-default" href="<?php echo base_url('admin/callback/1/edit'); ?>"><i class="fa fa-fw fa-phone"></i> <?php echo $this->lang->line('callback'); ?></a>
                                                            <a class="btn-default" href="<?php echo base_url('admin/notifications.php'); ?>"><i class="fa fa-fw fa-bell"></i> <?php echo $this->lang->line('notifications'); ?></a>
                                                            <a class="btn-default" href="<?php echo base_url('admin/reminders.php'); ?>"><i class="fa fa-fw fa-clock-o"></i> <?php echo $this->lang->line('reminders'); ?></a>
                                                            <a class="btn-default" href="<?php echo base_url('admin/popups/1/edit.php'); ?>"><i class="fa fa-fw fa-wrench"></i> <?php echo $this->lang->line('popups'); ?></a>
                                                            <a class="btn-default" href="<?= base_url(); ?>admin/language.php" class="flag-arabic"><i class="fa fa-language"></i> <?php echo $this->lang->line('languages'); ?> </a>

                                                            <a class="btn-default" href="<?= base_url(); ?>admin/themes.php" class="flag-arabic"><i class="fa fa-codepen"></i> <?php echo 'Themes'; ?> </a>

                                                            <a class="btn-default" href="<?= base_url(); ?>admin/info_bulls.php" class="flag-arabic"><i class="fa fa-info"></i> <?php echo 'INFO BULLS'; ?> </a>

                                                            <a class="btn-default" href="<?= base_url(); ?>admin/flash_info.php" class="flag-arabic"><i class="fa fa-bolt"></i> <?php echo 'FLASH INFO'; ?> </a>

                                                            <a class="btn-default" href="<?= base_url(); ?>admin/seo.php" class="flag-arabic"><i class="fa fa-bullhorn"></i> <?php echo 'SEO'; ?> </a>




                                                
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="dropdown" style = "width:100%;">
                                                            <button class="dropbtn btn-default" style = "width:100%;text-align:left;"><i style = "font-weight:bold;" class="fa fa-angle-left"></i> <i class="fa fa-fw fa-server"></i><?php echo $this->lang->line('configurations'); ?></button>
                                                            <div class="dropdown-content">
                                                                <a class="btn-default" href="<?php echo site_url('admin/booking_config'); ?>"><i class="fa fa-ticket"></i>Booking</a>
                                                                <a class="btn-default" href="<?php echo site_url('admin/supplier'); ?>" class="btn-default"><i class="fa fa-user"></i> <?php echo $this->lang->line('supplier'); ?></a>
                                                                <a class="btn-default" href="<?= base_url(); ?>admin/recruitment_config.php" class="btn-default"><i class="fa fa-fw fa-registered"></i> <?php echo 'Recruitment' ?></a>
                                                                <a class="btn-default" href="<?= base_url(); ?>admin/weather.php" class="btn-default"><i class="fa fa-fw fa-cloud"></i> <?php echo 'Weather' ?></a>
                                                                <a class="btn-default" href="<?= base_url(); ?>admin/cms/cms_config.php" class="btn-default"><i class="fa fa-fw fa-cloud"></i> <?php echo 'CMS'; ?> </a>
                                                                <a class="btn-default" href="<?php echo site_url('admin/driver_config'); ?>"> <i class="fa fa-user"></i> Driver</a>
                                                                <a class="btn-default" href="<?php echo site_url('admin/configclients'); ?>"> <i class="fa fa-user"></i> Clients</a>
                                                                <a class="btn-default" href="<?php echo site_url('admin/configcars'); ?>"> <i class="fa fa-car"></i> Cars</a>
                                                                <a class="btn-default" href="<?= base_url(); ?>admin/documents_config.php" class="btn-default"><i class="fa fa-file"></i> <?php echo 'DOCUMENTS'; ?> </a>
                                                                <a class="btn-default" href="<?= base_url(); ?>admin/library.php" class="btn-default"><i class="fa fa-file"></i> <?php echo 'LIBRARY'; ?> </a>
                                                                <a class="btn-default" href="<?= base_url(); ?>admin/google_api.php" class="btn-default"><i class="fa fa-map-marker"></i> <?php echo $this->lang->line('google_api'); ?> </a>
                                                                <a href="<?php echo base_url() ?>admin/newsletters/config_board.php" class="flag-arabic"><i class="fa fa-suitcase"></i><?php echo $this->lang->line('newsletter'); ?> </a>
                                                            </div>
                                                        </div>
                                                    </li>
<?php ?> 
                                                    <li><a href="<?= base_url(); ?>admin/tutorials.php" class="btn-default"><i class="fa fa-fw fa-info"></i> <?php echo $this->lang->line('tutorials'); ?></a></li>
                                                    <li><a href="<?= base_url(); ?>admin/documents.php" class="btn-default"><i class="fa fa-fw fa-file"></i> <?php echo 'Documents' ?></a></li>
                                                    <li><a href="<?= base_url(); ?>admin/updates.php" class="btn-default"><i class="fa fa-fw fa-edit"></i> <?php echo 'Updates' ?></a></li>
                                                    <!-- <li><a href="<?= base_url(); ?>admin/assistance.php" class="btn-default"><i class="fa fa-fw fa-info"></i> <?php echo 'Assistance' ?></a></li> -->
<?php  ?>


                                                    <li><a href="<?php echo site_url(); ?>admin/logout" class="btn-default"><i class="fa fa-fw fa-power-off"></i><?php echo $this->lang->line('log_out'); ?></a></li>
                                                
                                                </ul>
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    <!--<div class="audio right">
                        <object class="audioObj" width="140" height="14"
                                style=" float: right;margin-left: 20px;vertical-align: top;"
                                data="http://flash-mp3-player.net/medias/player_mp3_mini.swf"
                                type="application/x-shockwave-flash" title="Adobe Flash Player">
                            <param value="http://flash-mp3-player.net/medias/player_mp3_mini.swf" name="movie">
                            <param value="#5590C1" name="bgcolor">
                            <?php if ($this->lang->lang() == 'fr') { ?>
                                <param value="mp3=<?php echo base_url(); ?>assets/system_design/audio/navetteo_french.mp3&amp;bgcolor=0D528A&amp;slidercolor=fb833e&amp;autoplay=1&amp;autoload=0"
                                       name="FlashVars">
                                   <?php } else {
                                       ?>
                                <param value="mp3=<?php echo base_url(); ?>assets/system_design/audio/navetteo_english.mp3&amp;bgcolor=0D528A&amp;slidercolor=fb833e&amp;autoplay=1&amp;autoload=0"
                                       name="FlashVars">
                                   <?php } ?>
                        </object>
                    </div>-->
                    <?php if (@$site_settings->app_settings == "Enable") { ?>
                        <div class="top-links">
                            <a href="<?php echo site_url(); ?>/welcome/download_app/android"><img
                                src="<?php echo base_url(); ?>assets/system_design/images/header/google-play-icon.png"
                                style="width:48%" alt="Navetteo Google Apps" title="Navetteo Google Apps "/></a>
                                <a href="<?php echo site_url(); ?>/welcome/download_app/ios"><img
                                    src="<?php echo base_url(); ?>assets/system_design/images/header/apple-icon.png"
                                    style="width:48%" alt="Navetteo iPhone Apps" title="Navetteo iPhone Apps"/></a>
                                </div>
                            <?php } ?> 
                            <div class="row-fluid secondary-header"> 
                                <div class="col-md-12 mt-pr-none"> <!--padding-p-l-->
                                    <a href="<?php echo site_url(); ?>dashboard.php">
                                        <div class="logo">
                                            <img src="<?php echo base_url(); ?>assets/system_design/images/logo-admin.png" alt="eCab Logo" title="eCab Logo" />
                                        </div>
                                    </a>
                                    <div class="outside-box">
                                        <div class="service-box">
                                            <div id="service-currency">$</div>
                                            <div id="service-i"><i  class="fa fa-money"></i></div>
                                            <div>
                                                <span class="service-b currency"><?php echo '14650'; // isset($this->data['calls']) ? array_pop(array_reverse($this->data['calls'])) : 0 ?></span>
                                            </div>
                                            <span class="service-p">Sales</span>
                                        </div>
                                        <div class="service-box">
                                            <div id="service-currency">$</div>
                                            <div id="service-i"><i  class="fa fa-money"></i></div>
                                            <div>
                                                <span class="service-b currency"><?php echo '11300'; // isset($this->data['calls']) ? array_pop(array_reverse($this->data['calls'])) : 0 ?></span>
                                            </div>
                                            <span class="service-p">Bills</span>
                                        </div>
                                        <div class="service-box">
                                            <div id="service-currency">$</div>
                                            <div id="service-i" class="fa fa-money"></div>
                                            <div>
                                                <span class="service-b currency"><?php echo "3150"; // isset($this->data['request']) ? array_pop(array_reverse($this->data['request'])) : 0 ?></span>
                                            </div>
                                            <span class="service-p">Profit</span>
                                        </div>
                                        <div class="service-box">
                                            <div id="service-currency">$</div>
                                            <div id="service-i"><i  class="fa fa-money"></i></div>
                                            <div>
                                                <span class="service-b currency"><?php echo '9520'; // isset($this->data['calls']) ? array_pop(array_reverse($this->data['calls'])) : 0 ?></span>
                                            </div>
                                            <span class="service-p">Incomes</span>
                                        </div>
                                        <div class="service-box">
                                            <div id="service-currency">$</div>
                                            <div id="service-i" class="fa fa-money"></div>
                                            <div>
                                                <span class="service-b currency"><?php echo '8410'; // isset($this->data['jobs']) ? array_pop(array_reverse($this->data['jobs'])) : 0 ?></span>
                                            </div>
                                            <span class="service-p">Expenses</span>
                                        </div>
                                        <div class="service-box">
                                            <div id="service-currency">$</div>
                                            <div id="service-i" class="fa fa-money"></div>
                                            <div>
                                                <span class="service-b currency"><?php echo "1110"; // isset($this->data['request']) ? array_pop(array_reverse($this->data['request'])) : 0 ?></span>
                                            </div>
                                            <span class="service-p">Cashflow</span>
                                        </div>
                                        <div class="service-box">
                                            <div id="service-i" class="fa fa-support"></div>
                                            <div>
                                                <span class="service-b"><?= isset($this->data['support']) ? $this->data['support']['new'] : 0 ?></span>
                                            </div>
                                            <span class="service-p">Rides</span>
                                            </div>
                                            <div class="service-box">
                                                <div id="service-i" class="fa fa-ticket"></div>
                                                <div>
                                                    <span class="service-b"><?php echo $totalbooking ? ""; ?></span>
                                                </div>
                                                <span class="service-p"><?php echo $this->lang->line('bookings'); ?></span>
                                            </div>
                                            <div class="service-box">
                                                <div id="service-i" class="fa fa-paper-plane"></div>
                                                <div>
                                                    <span class="service-b"><?= isset($this->data['request']['new']) ? $this->data['request']['new'] : 0 ?></span>
                                                </div>
                                                <span class="service-p"><?php echo $this->lang->line('quotes'); ?></span>
                                            </div>
                                            <div class="service-box">
                                                <div id="service-i" class="fa fa-calculator"></div>
                                                <div>
                                                    <span class="service-b">12</span>
                                                </div>
                                                <span class="service-p"><?php echo $this->lang->line('invoices'); ?></span>
                                            </div>
                                            <div class="service-box">
                                                <div id="service-i" class="fa fa-users"></div>
                                                <div>
                                                    <span class="service-b"><?php echo (isset($total_clients) ? $total_clients : 0); ?></span>
                                                </div>
                                                <span class="service-p"><?php echo $this->lang->line('clients'); ?></span>
                                            </div>
                                            <div class="service-box">
                                                <div id="service-i" class="fa fa-car"></div>
                                                <div>
                                                    <span class="service-b">0</span>
                                                </div>
                                                <span class="service-p"><?php echo $this->lang->line('cars'); ?></span>
                                            </div>
                                            <div class="service-box">
                                                <div id="service-i" class="fa fa-user"></div>
                                                <div>
                                                    <span class="service-b"><?php echo (isset($total_drivers) ? $total_drivers : 0); ?></span>
                                                </div>
                                                <span class="service-p"><?php echo $this->lang->line('drivers'); ?></span>
                                            </div>
                                            <div class="service-box">
                                                <div id="service-i" class="fa fa-user"></div>
                                                <div>
                                                    <span class="service-b"><?php echo (isset($total_partners) ? $total_partners : 0); ?></span>
                                                </div>
                                                <span class="service-p"><?php echo "Partners"; ?></span>
                                            </div>
                                            <div class="service-box">
                                                <div id="service-i" class="fa fa-user"></div>
                                                <div>
                                                    <span class="service-b"><?php echo (isset($total_jobseekers) ? $total_jobseekers : 0); ?></span>
                                                </div>
                                                <span class="service-p"><?php echo "Affiliates"; ?></span>
                                            </div>
                                            <div class="service-box">
                                                <div id="service-i" class="fa fa-user"></div>
                                                <div>
                                                    <span class="service-b"><?php echo (isset($total_jobseekers) ? $total_jobseekers : 0); ?></span>
                                                </div>
                                                <span class="service-p"><?php echo "Jobseekers"; ?></span>
                                            </div>
                                        </div>
                                        <?php if ($this->lang->lang() == 'fr') { ?>
                                            <div class="col-md-4 right french-contact-number"><!--padding-p-r-->
                                                <?php
                                //$phone = $site_settings->land_line;
                                                echo "01 48 13 09 34";
                                                ?>
                                                <p class="working-hours" style=" left:35px;">Du Lundi au Vendredi <br/>de 9h à 12h et de 14h
                                                à 18h</p>
                                            </div>
                                        <?php } else { ?>
                            <!-- <div class="col-md-4 right english-contact-number">
                            <?php
                            //    $phone = $site_settings->phone;
                            //echo "01 48 13 09 34";
                            ?>
                                <p class="working-hours" style="left:20px;">From Monday to Friday <br/>9h to 12h and from
                                    14h to 18h</p>
                                </div> -->
                            <?php } ?>
                            <div class="col-md-1 right">
                                <img src="<?php echo base_url(); ?>assets/system_design/images/call-us-girl.png"
                                class="call-us-girl" alt="Call Us" title="Call Us"/></div>
                                <?php if ($this->lang->lang() == 'fr') { ?>
                                    <div class="header-icons-fr">
                                        <img src="<?php echo base_url(); ?>assets/system_design/images/new_footer_header_images/logo1ssside.png"
                                        alt="Aeroport Service" width="250px" height="80px" title="Aeroport Service"/>
                                        <img src="<?php echo base_url(); ?>assets/system_design/images/new_footer_header_images/logoagain.png"
                                        alt="Aeroport Service" width="250px" height="80px" title="Aeroport Service"/>
                                        <img src="<?php echo base_url(); ?>assets/system_design/images/new_footer_header_images/disable.png"
                                        alt="Aeroport Service" width="250px" height="80px" title="Aeroport Service"/>
                                    </div>
                                <?php } else if ($this->lang->lang() == 'ens') { ?>
                                    <div class="header-icons-en">
                                        <img src="<?php echo base_url(); ?>assets/system_design/images/header/aeroport.png"
                                        alt="Airport Service" title="Airport Service"/>
                                        <img src="<?php echo base_url(); ?>assets/system_design/images/header/passengers.png"
                                        alt="Passengers" title="Passengers"/>
                                        <img src="<?php echo base_url(); ?>assets/system_design/images/header/circuit-touristiques.png"
                                        alt="Tours Circuit" title="Tours Circuit"/>
                                        <img src="<?php echo base_url(); ?>assets/system_design/images/header/transfert-gare.png"
                                        alt="Railway Transfer" title="Railway Transfer"/>
                                        <img src="<?php echo base_url(); ?>assets/system_design/images/header/airport-transfers.png"
                                        style="width:95px" alt="Airport Transfer" title="Airport Transfer"/>
                                        <img src="<?php echo base_url(); ?>assets/system_design/images/header/free-quotes-en.png"
                                        alt="Free Quotes" title="Free Quotes"/>
                                        <img src="<?php echo base_url(); ?>assets/system_design/images/header/24hr-fr.png"
                                        alt="24/7 Service" title="24/7 Service"/>
                                        <img src="<?php echo base_url(); ?>assets/system_design/images/header/disabled.png"
                                        alt="Disabled" title="Disabled"/>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="row-fluid main-menu">
                            <div class="body-border" style=""> 
                                <div class="row-fluid">
                                    <nav class="navbar navbar-navetteo menu-total">
                                        <div class="navbar-header">
                                            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed nav-bar-btn" type="button">
                                                <span class="sr-only"><?php echo $this->lang->line('toggle_navigation'); ?></span> 
                                                <span class="icon-bar"></span> <span class="icon-bar"></span> 
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>
                                        <div class="collapse navbar-collapse res-menu" id="navbar">
                                            <ul class="nav navbar-nav menu">
                                                <li <?php if (isset($active_class)) { if ($active_class == "dashboard") echo 'class="active"'; } ?>>
                                                    <a href="<?php echo site_url(); ?>admin/dashboard.php"><i class="fa fa-tachometer" style="font-size: 15px;margin-right: 5px;"></i><span><?php echo $this->lang->line('dashboard'); ?></span></a>
                                                </li>
                                                <li <?php if (isset($active_class)) { if ($active_class == "clients") echo 'class="active"'; } ?>>
                                                    <a href="<?php echo site_url(); ?>admin/clients.php"><i class="fa fa-users" style="font-size: 15px;margin-right: 5px;"></i>Clients</a>
                                                </li>
                                                <li <?php if (isset($active_class)) { if ($active_class == "cars") echo 'class="active"'; } ?>>
                                                    <a href="<?php echo site_url(); ?>admin/cars.php"><i class="fa fa-car" style="font-size: 15px;margin-right: 5px;"></i>Cars</a>
                                                </li>
                                                <li <?php if (isset($active_class)) { if ($active_class == "drivers") echo 'class="active"'; } ?>>
                                                    <a href="<?php echo site_url(); ?>admin/driver_info.php"><i class="fa fa-user" style="font-size: 15px;margin-right: 5px;"></i>Drivers</a>
                                                </li>
                                                
                                        <!--<li <?php //if (isset($active_class)) { if ($active_class == "booking") echo 'class="active"'; } ?>>
                                            <a href="<?php //echo site_url('admin/booking_config'); ?>"><i class="fa fa-ticket" style="font-size: 15px;margin-right: 5px;"></i>Booking</a>
                                        </li>-->
                                        <li <?php if (isset($active_class)) { if ($active_class == "bookings") echo 'class="active"'; } ?>>
                                            <a href="<?php echo site_url(); ?>admin/bookings.php"><i class="fa fa-ticket" style="font-size: 15px;margin-right: 5px;"></i>Bookings</a>
                                        </li>
                                        <li <?php if (isset($active_class)) { if ($active_class == "quotes") echo 'class="active"'; } ?>>
                                            <a href="<?php echo site_url(); ?>admin/quotes.php"><i class="fa fa-paper-plane" style="font-size: 15px;margin-right: 5px;"></i>Quotes</a>
                                        </li>
                                        <li <?php if (isset($active_class)) { if ($active_class == "contracts") echo 'class="active"'; } ?>>
                                            <a href="<?php echo site_url(); ?>admin/contracts.php"><i class="fa fa-file-archive-o" style="font-size: 15px;margin-right: 5px;"></i>CONTRACTS</a>
                                        </li>
                                        <li <?php if (isset($active_class)) { if ($active_class == "invoices") echo 'class="active"'; } ?>>
                                            <a href="<?php echo site_url(); ?>admin/invoices.php"><i class="fa fa-calculator" style="font-size: 15px;margin-right: 5px;"></i>Invoices</a>
                                        </li>
                                        <li <?php if (isset($active_class)) { if ($active_class == "dispatch") echo 'class="active"'; } ?>>
                                            <a href="<?php echo base_url() ?>admin/support.php"><i class="fa fa-arrow-circle-up" style="font-size: 15px;margin-right: 5px;"></i><span>Dispatch</span></a>
                                        </li>
                                        <li <?php if (isset($active_class)) { if ($active_class == "support") echo 'class="active"'; } ?>>
                                            <a href="<?php echo base_url() ?>admin/timesheet.php"><i class="fa fa-calendar" style="font-size: 15px;margin-right: 5px;"></i><span>Timesheet</span></a>
                                        </li>
                                        <li <?php if (isset($active_class)) { if ($active_class == "support") echo 'class="active"'; } ?>>
                                            <a href="<?php echo base_url() ?>admin/reports.php"><i class="fa fa-bug" style="font-size: 15px;margin-right: 5px;"></i><span>REPORTS</span></a>
                                        </li>
                                        <li <?php if (isset($active_class)) { if ($active_class == "planning") echo 'class="active"'; } ?>>
                                            <a href="#"><i class="fa fa-calculator" style="font-size: 15px;margin-right: 5px;"></i>Planning</a>
                                        </li>
                                        <li <?php if (isset($active_class)) { if ($active_class == "planning") echo 'class="active"'; } ?>>
                                            <a href="<?php echo base_url() ?>admin/dispatch/calender_views.php"><i class="fa fa-calendar" style="font-size: 15px;margin-right: 5px;"></i>Calender view</a>
                                        </li>
                                        <li <?php if (isset($active_class)) { if ($active_class == "planning") echo 'class="active"'; } ?>>
                                            <a href="<?php echo base_url() ?>admin/dispatch/map_views.php"><i class="fa fa-map" style="font-size: 15px;margin-right: 5px;"></i>Map view</a>
                                        </li>
                                        <li>
                                            <a type="button" class=" dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-calculator"></i> Accounting<span class="caret"></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="<?php echo base_url() ?>admin/accounting/sales.php" class="flag-arabic"><i class="fa fa-calculator"></i><?php echo $this->lang->line('sales'); ?> </a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url() ?>admin/accounting/bills.php" class="flag-arabic"><i class="fa fa-calculator"></i><?php echo $this->lang->line('bills'); ?> </a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url() ?>admin/accounting/bank.php" class="flag-arabic"><i class="fa fa-calculator"></i> <?php echo $this->lang->line('bank'); ?> </a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url() ?>admin/accounting/factoring.php" class="flag-arabic"><i class="fa fa-calculator"></i><?php echo $this->lang->line('factoring'); ?> </a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url() ?>admin/accounting/reportss.php" class="flag-arabic"><i class="fa fa-calculator"></i><?php echo $this->lang->line('reports'); ?> </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a type="button" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-suitcase"></i> <?php echo $this->lang->line('CRM'); ?><span class="caret"></span> </a>
                                            <ul class="dropdown-menu">
                                                 <li>
                                                    <a href="<?php echo base_url() ?>admin/discounts.php" class="flag-arabic"><i class="fa fa-book"></i>Discount</a>
                                                </li>
                                                 <li>
                                                    <a href="<?php echo base_url() ?>admin/pipeline.php" class="flag-arabic"><i class="fa fa-suitcase"></i>PIPELINE</a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url() ?>admin/newsletters.php" class="flag-arabic"><i class="fa fa-suitcase"></i>NEWSLETTER</a>
                                                </li>
                                               
                                            </ul>
                                        </li>
                                        <!--
                                        <li <?php // if (isset($active_class)) { if ($active_class == "pagetype") echo 'class="active"'; } ?>>
                                            <a href="<?php // echo site_url('admin/pagetype'); ?>"><i class="fa fa-ticket" style="font-size: 15px;margin-right: 5px;"></i>Page Type</a>
                                        </li>
                                        <li <?php // if (isset($active_class)) { if ($active_class == "services") echo 'class="active"'; } ?>>
                                            <a href="<?php // echo site_url('admin/cmspages'); ?>"><i class="fa fa-ticket" style="font-size: 15px;margin-right: 5px;"></i>Services</a>
                                        </li>
                                    -->
                            <!--             <li>
                                            <a type="button" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-bolt"></i> <?php echo $this->lang->line('CMS'); ?><span class="caret"></span>
                                            </a>
                                            <ul class="dropdown-menu">			
                                                <li><a href="<?php echo base_url() ?>admin/cms/addcms.php">ADD NEW</a></li>
                                                <?php 	$CI =& get_instance();
                                                                $CI->load->model('cms_model');
                                                                $result = $CI->cms_model->getActiveAll();
                                                ?>
                                                <?php foreach ($result as $cms_val) { ?>
                                                    <li><a href="<?php base_url('admin/cms/') ?><?php echo $cms_val->id; ?>"><?php echo $cms_val->name; ?></a></li>
                                                <?php } ?>
                                            </ul>
                                        </li> -->


                                        <li>
                                            <a type="button" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-cogs"></i> <?php echo $this->lang->line('CMS'); ?><span class="caret"></span>
                                            </a>
                                            <ul class="dropdown-menu">      

                                                <li><a href="<?=base_url() ?>admin/cms/services.php"> <i class="fa fa-car"></i> <?php echo $this->lang->line('services'); ?> </a></li>
                                                <li><a href="<?=base_url() ?>admin/cms/legals.php"> <i class="fa fa-gavel"></i> <?php echo $this->lang->line('legals'); ?> </a></li>
                                                <li><a href="<?=base_url() ?>admin/cms/news.php"> <i class="fa fa-newspaper-o"></i> <?php echo $this->lang->line('news'); ?> </a></li>
                                                <li><a href="<?=base_url() ?>admin/cms/prices.php"><i class="fa fa-money"></i> <?php echo $this->lang->line('prices'); ?> </a></li>
                                                <li><a href="<?=base_url() ?>admin/cms/fleet.php"><i class="fa fa-car"></i><?php echo $this->lang->line('fleet'); ?> </a></li>
                                                <li><a href="<?=base_url() ?>admin/cms/faq.php"><i class="fa fa-question-circle"></i><?php echo $this->lang->line('faq'); ?> </a></li>
                                                <li><a href="<?=base_url() ?>admin/cms/downloads.php"><i class="fa fa-download"></i><?php echo $this->lang->line('downloads'); ?> </a></li>
                                                <li><a href="<?=base_url() ?>admin/cms/testimonials.php"><i class="fa fa-comments-o"></i><?php echo $this->lang->line('testimonials'); ?> </a></li>
                                                <li><a href="<?=base_url() ?>admin/cms/slide_show.php"><i class="fa fa-slideshare"></i><?php echo $this->lang->line('slideshows'); ?> </a></li>
                                                <li><a href="<?=base_url() ?>admin/cms/banners.php"><i class="fa fa-picture-o"></i><?php echo $this->lang->line('banners'); ?> </a></li>
                                               <!--  <li><a href="<?=base_url() ?>admin/cms/info_bulls.php"><i class="fa fa-info"></i><?php echo 'INFO BULLS'; ?> </a></li>
                                                 <li><a href="<?=base_url() ?>admin/cms/flash_info.php"><i class="fa fa-bolt"></i><?php echo 'FLASH INFO'; ?> </a></li>
                                                <li><a href="<?=base_url() ?>admin/cms/seo.php"><i class="fa fa-bullhorn"></i><?php echo $this->lang->line('seo'); ?> </a></li>
                                                 -->
                                                <li><a href="<?=base_url() ?>admin/cms/zones.php"><i class="fa fa-globe"></i><?php echo 'ZONES'; ?> </a></li>
                                                <li><a href="<?=base_url() ?>admin/cms/partners.php"><i class="fa fa-users"></i><?php echo 'PARTNERS'; ?> </a></li>
                                               


                                                <li><a href="<?=base_url() ?>admin/cms/tutorials.php"><i class="fa fa-book"></i><?php echo $this->lang->line('tutorials'); ?> </a></li>
                                                <li><a href="<?=base_url() ?>admin/cms/documentations.php"><i class="fa fa-archive"></i><?php echo $this->lang->line('documentations'); ?> </a></li>
                                                <li><a href="<?=base_url() ?>admin/cms/udapts.php"><i class="fa fa-archive"></i><?php echo $this->lang->line('udapts'); ?> </a></li>



                                                <!--   <li><a href="<?=base_url() ?>admin/company.php"><i class="fa fa-bars"></i><?php echo 'TOP BAR'; ?> </a></li> -->


                                            </ul>
                                        </li>

                                        <li>
                                            <a href="<?php echo base_url() ?>admin/fleet">
                                                <i class="fa fa-car" ></i>
                                                <span>Fleet</span>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="<?php echo base_url() ?>admin/ressources">
                                                <i class="fa fa fa-group" ></i>
                                                <span>RESSOURCES</span>
                                            </a>
                                        </li>
                                        <!--<li>
                                            <a href="<?php echo base_url() ?>admin/hr"> <i class="fa fa-user" ></i> <span>Human Resources</span> </a>
                                        </li>-->
                                        <li <?php if (isset($active_class)) { if ($active_class == "documents") echo 'class="active"'; } ?>>
                                            <a href="<?php echo site_url(); ?>admin/documents.php"><i class="fa fa-file" style="font-size: 15px;margin-right: 5px;"></i>Documents</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url() ?>admin/hr"> <i class="fa fa-money" ></i> <span>Wages</span> </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url() ?>admin/infraction.php">
                                                <i class="fa fa-connectdevelop" ></i>
                                                <span>Infractions</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('admin/jobseekers'); ?>">
                                                <i class="fa fa-wrench" ></i>
                                                <span>Jobseekers</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('admin/recruitment'); ?>">
                                                <i class="fa fa-registered" ></i>
                                                <span>Recruitment</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('admin/partners'); ?>">
                                                <i class="fa fa-user" style="font-size: 15px;margin-right: 5px;"></i>
                                                <span>PARTNERS</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url(); ?>admin/affiliates.php">
                                                <i class="fa fa-user" style="font-size: 15px;margin-right: 5px;"></i>
                                                <span>AFFILIATES</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <?php echo form_open_multipart('', ['id' => 'hidden_form', 'class' => 'form']); ?>
    <?= form_close(); ?>
    <!-- Modal -->

    <div class="modal left fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="overflow: inherit;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <!-- <h4 class="modal-title" id="myModalLabel"> Chating </h4> -->
                    <?php
                    $CI2 =& get_instance();
                    // $CI2->load->model('my_model');
                     $CI2->load->model('header_settings_model');
                    $language2 = $CI->header_settings_model->getHeaderSettings();
                    ?>
                    <div class="filter-div card-body text-center">
                        <label class="switch" style="margin-bottom: -6px;">
                            <input type="checkbox" id="chatboxswitch" value="active" <?php
                            if ($language2['chat_status'] == "active") {
                                echo "checked";
                            } else {

                            }
                            ?>>
                            <span class="slider round"></span>
                        </label>
                        <select class="form-control" style="display: inline-block; width: inherit !important;">
                            <option value="">Category</option>
                            <option value="client">Client</option>
                            <option value="driver">Driver</option>
                            <option value="partner">Partner</option>
                            <option value="jobseeker">Jobseeker</option>
                            <option value="affiliate">Affiliate</option>
                        </select>
                        <select class="form-control" style="display: inline-block; width: inherit !important;">
                            <option value="">Status</option>
                            <option value="online">Online</option>
                            <option value="away">Away</option>
                            <option value="offline">Offline</option>
                        </select> 
                    </div>
                </div>
                <div class="modal-body height-div3" style="padding-left: 0px; padding-right: 0px; padding-bottom: 0px;">
                    <!-- <p class="text-center">Loading...</p> -->
                    <div class=""> 
                        <audio id="pop" style="visibility: hidden;height: 0px;width: 0px;">
                            <source src="<?php echo base_url(); ?>assets/chat_audio_sound.mp3" type="audio/mpeg">
                            </audio>                      
                            <div class="height-div33">
                            </div>
                        </div>
                    </div>
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->
        <div class="row pt-3 chat_popups_row">
            <div class="col-md-3"></div>
        </div>
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
        <script type="text/javascript">
            $(document).ready(function () {


                getnewmessagesdata();
                var soundstatus;
                function getnewmessagesdata() {
                    var status = 'active';
                    $.ajax({
                        url: '<?php echo base_url(); ?>Messages/getallnewmessages',
                        method: 'get',
                        data: 'status=' + status,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (result)
                        {
                    // alert(result);return;
                    // var obj = JSON.parse(result);
                    // if(obj.sound_status==1 && obj.ischat_open==0)
                    // if(obj.sound_status==1)
                    // {
                    //     $('audio#pop')[0].play();
                    // clearTimeout(soundstatus);
                    // updatechatsound_status();
                    getchatsound_status();
                    // }
                    // alert(obj.sound_status+" HTML="+obj.chat_html_data);return;
                    // console.log(1);
                    $('.height-div33').html(result);
                    setTimeout(getnewmessagesdata, 5000);
                }
            });
                }
                function getchatsound_status() {
                    var status = 'active';
                    $.ajax({
                        url: '<?php echo base_url(); ?>Messages/checksoundstatusdata',
                        method: 'get',
                        data: 'status=' + status,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (result)
                        {
                            var obj = JSON.parse(result);
                            if (obj.sound_status == 1)
                            {
                        // $('audio#pop')[0].play();
                        var audio = new Audio('<?php echo base_url(); ?>assets/chat_audio_sound.mp3');
                        audio.play();
                        clearTimeout(soundstatus);
                        updatechatsound_status();
                    }
                }
            });
                }
                function updatechatsound_status() {
                    var status = "active";
                    soundstatus = setTimeout(function () {
                        $.ajax({
                            url: '<?php echo base_url(); ?>Messages/changechatsound_status',
                            method: 'get',
                            data: 'status=' + status,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (result)
                            {
                        // $('audio#pop')[0].pause();
                        console.log(result);
                    }
                });
                    }, 800);
                }
                $('[data-toggle="popover"]').popover();
                $('.emojiPicker').find('.recent').remove();
                $('[data-tab="people"]').addClass('active');
            });
            $('.hide-chat-box').click(function () {
                $('.chat-content').slideDown();
                $('.chat-main').css({'opacity': 1, 'z-index': 9999});
                $('.hidechatbx').addClass('hide-chat-box');
            });
            $('.hide-chat-box2').click(function () {
                $('.chat-content').slideToggle();
                $('.chat-main').css('opacity', 1);
                $('.hidechatbx').addClass('hide-chat-box2');
            });
            $('.height-div33').click(function () {
                $('.chat-content').slideDown();
                $('.chat-main').css('opacity', 1);
                $('.hidechatbx').addClass('hide-chat-box2');
            });
            $('.people').click(function () {
                alert(123);
            });
            $('#chatboxswitch').click(function () {
                if ($(this).prop("checked")) {
            // alert();return;
            var status = 'active';
            $.ajax({
                url: '<?php echo base_url(); ?>Messages/changechatboxstatus',
                method: 'get',
                data: 'status=' + status,
                cache: false,
                contentType: false,
                processData: false,
                success: function (result)
                {
                    console.log("Chat active");
                }
            });
        } else
        {
            var status = 'inactive';
            $.ajax({
                url: '<?php echo base_url(); ?>Messages/changechatboxstatus',
                method: 'get',
                data: 'status=' + status,
                cache: false,
                contentType: false,
                processData: false,
                success: function (result)
                {
                    console.log("Chat Inactive");
                }
            });
        }
    });
            function removeextrafunctionality() {
                $('.close').click();
            }
            function quickchatreplyfnc(val) {
        // if(val==1)
        // {
        //     $('#input-left-position').val('Sure, let`s get started');
        // }
        // if(val==2)
        // {
        //     $('#input-left-position').val('Interesting, but I need info');
        // }
        // if(val==3)
        // {
        //     $('#input-left-position').val('I`m not able to do this');
        // }
        var quick_reply_val = $('#quickchatreply_val' + val).val();
        $('#input-left-position' + val).val(quick_reply_val);
        $('.popover').hide();
        // $('#adminreplymsgform').submit();
    }
    function addattachfileto_div(val) {
        // alert(val);
        $('#attachfile_div' + val).css({'display': 'block'});
        var attachfilename_val = $('#chatfile' + val).val();
        var extension = attachfilename_val.split('.').pop();
        // alert(extension);return;
        if (extension == "png" || extension == "PNG" || extension == "jpg" || extension == "jpeg" || extension == "gif")
        {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("chatfile" + val).files[0]);
            oFReader.onload = function (oFREvent) {
                document.getElementById("changeattachfileimage" + val).src = oFREvent.target.result;
            }
        } else
        {
            $('#changeattachfileimage' + val).attr('src', '<?php echo base_url(); ?>assets/chat_files/file_upload_image.png');
        }
        var attachfilename_vall = attachfilename_val.substring(attachfilename_val.lastIndexOf("\\") + 1, attachfilename_val.length);
        // $('#attachfilename'+val).text(attachfilename_vall);
        $('#input-left-position' + val).attr("required", false);
    }
    // var specificuserSettimeout;
    function getspecificuserchathistory(userid, user_name) {
        $('.chat_box_col' + userid).css({'display': 'block'});
        var userids = $('.input_userid');
        var userids = $(".input_userid").map(function () {
            if ($(this).val() == userid)
            {
                return true;
            } else
            {
                return false;
            }
            // return 
        }).get();
        for (var i = 0; i <= userids.length; i++)
        {
            if (userids[i] == true)
            {
                return;
                console.log(true);
            } else
            {
                console.log(false);
            }
        }
        var status = 'active';
        $.ajax({
            url: '<?php echo base_url(); ?>Messages/getallspecificusermessages',
            method: 'get',
            data: 'userid=' + userid,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result)
            {
                // $('#userchatname').text(user_name);
                // $('#input_userid').val(userid);
                var formhidden_id = $('#hidden_form').find("[name='csrf_test_name']").val();
                // $('.chat-main').css({'z-index':9999,'opacity':1});
                var all_res = '<div class="col-md-3 chat_box_col' + userid + '"><form action="javascript:void(0)" id="adminreplymsgform' + userid + '" class="form" onsubmit="adminreplymsgform(' + userid + ')" method="post" enctype="multipart/form-data"><input type="hidden" name="csrf_test_name" value="' + formhidden_id + '" />' + result + '</form></div>';
                // var someText = all_res.replace(/(\r\n|\n|\r)/gm,"");
                $('.chat_popups_row').append(all_res);
                setTimeout(getspecificuserchathistorydatatoUL, 3000);
                // getspecificuserchathistorydatatoUL();
            }
        });
    }
    var specificuserSettimeout;
    function getspecificuserchathistorydatatoUL() {
        clearTimeout(specificuserSettimeout);
        // var userids = $('.input_userid');
        var userids = $(".input_userid").map(function () {
            return $(this).val();
        }).get();
        console.log(userids);
        for (var i = 0; i < userids.length; i++)
        {
            // console.log(userids[i]);
            // if(userids[i]==undefined){}
            // else
            // {
                $.ajax({
                    url: '<?php echo base_url(); ?>Messages/getallspecificusermessagesdatatoul',
                    method: 'get',
                    data: 'userid=' + userids[i],
                    cache: false,
                    contentType: false,
                    processData: false,
                    indexValue: userids[i],
                    success: function (result)
                    {
                    // console.log(this.indexValue);
                    $('#chat_ul' + this.indexValue).html(result);
                    var elem = document.getElementById('chatsbox' + this.indexValue);
                    elem.scrollTop = elem.scrollHeight;
                }
            });
            // } 
        }
        specificuserSettimeout = setTimeout(getspecificuserchathistorydatatoUL, 4000);
    }
    // getspecificuserchathistorydatatoUL();
    // function adminreply()
    // {
    //    var message_text = $('#input-left-position').val();
    //    var userid = $('#input_userid').val();
    //    var attachfile = $('#chatfile').val();
    //    $.ajax({
    //         url:'<?php echo base_url(); ?>Messages/insertadminreply',
    //         method:'get',
    //         data:'userid='+userid+'&input-left-position='+message_text+'&attachfile='+attachfile,
    //         cache:false,
    //         contentType:false,
    //         processData:false,
    //         success:function(result)
    //         {
    //             $('#input-left-position').val('');
    //             $('#chat_ul').append(result);
    //             $('.popover').hide();
    //             var elem = document.getElementById('chatsbox');
    //             elem.scrollTop = elem.scrollHeight;
    //         }
    //     });  
    // }
    function adminreplymsgform(userid) {
        // alert(123);return;
        // e.preventDefault(); 
        var form = document.getElementById('adminreplymsgform' + userid);
        var formData = new FormData(form);
        // formData.append('messagetext', 'HellllllloooOO');
        $.ajax({
            url: '<?php echo base_url(); ?>Messages/insertadminreply',
            method: 'post',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            async: false,
            success: function (result)
            {
                // alert(result);
                $('#attachfile_div' + userid).css({'display': 'none'});
                $('#input-left-position' + userid).val('');
                $('#chatfile' + userid).val('');
                $('#input-left-position' + userid).attr("required", true);
                $('#chat_ul' + userid).append(result);
                // $('.popover').hide();
                var elem = document.getElementById('chatsbox' + userid);
                elem.scrollTop = elem.scrollHeight;
            }
        });
    }
    function closechatbox(val) {
        $('.chat_box_col' + val).css({'display': 'none'});
        // clearTimeout(specificuserSettimeout);
        $.ajax({
            url: '<?php echo base_url(); ?>Messages/change_is_chat_open_status_to_off',
            method: 'get',
            data: 'status=1',
            cache: false,
            success: function (result)
            {
                console.log('Sound off');
            }
        });
    }
    getpendingmessagecount();
    function getpendingmessagecount() {
        $.ajax({
            url: '<?php echo base_url(); ?>Messages/getallpendingmessages',
            method: 'get',
            data: 'status=1',
            cache: false,
            success: function (result)
            {
                if (result == 0)
                {
                    $('#header_pending_msg_count').css('display', 'none');
                } else
                {
                    $('#header_pending_msg_count').css('display', 'block');
                    $('#header_pending_msg_count').html(result);
                }
                setTimeout(getpendingmessagecount, 3000);
            }
        });
    }
    updateclosespecificchatbox();
    function updateclosespecificchatbox() {
        $.ajax({
            url: '<?php echo base_url(); ?>Messages/change_is_chat_open_status_to_off',
            method: 'get',
            data: 'status=1',
            cache: false,
            success: function (result)
            {
                console.log('specific chat closed');
            }
        });
    }
    function updateclosespecificchatbox2(val) {
        // alert(1);
        $('.chat-content' + val).slideToggle();
        $('#minimizechatbox' + val).attr("onclick", "updateclosespecificchatbox2_off(" + val + ")");
        $.ajax({
            url: '<?php echo base_url(); ?>Messages/change_is_chat_open_status_to_on',
            method: 'get',
            data: 'status=1',
            cache: false,
            success: function (result)
            {
                console.log('specific chat on');
            }
        });
    }
    function updateclosespecificchatbox2_off(val) {
        $('.chat-content' + val).slideToggle();
        // alert(1);
        $('#minimizechatbox' + val).attr("onclick", "updateclosespecificchatbox2(" + val + ")");
        $.ajax({
            url: '<?php echo base_url(); ?>Messages/change_is_chat_open_status_to_off',
            method: 'get',
            data: 'status=1',
            cache: false,
            success: function (result)
            {
                console.log('specific chat off');
            }
        });
    }
    function addchatfile(val) {
        $('#chatfile' + val).click();
    }
    function openemojis() {
        $('.emojiPickerIcon').click();
    }
    var usertypingstatus_arr;
    function changeadmintypingstatus(val) {
        clearTimeout(usertypingstatus_arr);
        $.ajax({
            url: '<?php echo base_url(); ?>Messages/changeadmintypingstatus',
            method: 'get',
            data: 'userid=' + val,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data)
            {
                setTimeout(changetypingstatus_off.bind(null, val), 20000);
                console.log(data);
            },
            error: function (data) {
                alert('ERROR: ' + data.status + ' :: ' + data.statusText);
            }
        });
    }
    function changetypingstatus_off(val) {
        $.ajax({
            url: '<?php echo base_url(); ?>Messages/changeadmintypingstatus_off',
            method: 'get',
            data: 'userid=' + val,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data)
            {
                console.log(data);
            },
            error: function (data) {
                alert('ERROR: ' + data.status + ' :: ' + data.statusText);
            }
        });
    }
    // window.setInterval(function() {
        var elem = document.getElementById('chatsbox');
        // elem.scrollTop = elem.scrollHeight;
    // }, 2000);
</script>
<style type="text/css">
    .emojiPickerIcon{
        visibility: hidden;
        height: 0px !important;
        width: 0px !important;
    }
    .emojiPicker {
        display: none;
        position: fixed;
        bottom: 44px;
        top: inherit !important;
        outline: none;
        border: none;
        box-shadow: 0 0 7px #555;
        border-top-left-radius: 4px;
        border-top-right-radius: 4px;
        font-family: Helvetica, Arial, sans-serif;
        width: 280px;
        height: 280px !important;
    }
    .emojiPicker .sections{
        height: 200px !important;
        overflow-y: auto !important;
    }
    .emojiPicker section h1{
        font-size: 14px !important;
        font-weight: 600;
        margin: 0px !important;
    }
    /*.emojiPicker .recent{
        display: none !important;
        }*/
        [data-tab="recent"]{
            display: none !important;
        }
    </style>
