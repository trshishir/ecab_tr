</div>
</div>
<section class="footer-new" style="text-align: center;    overflow: hidden;">
    <div class="container-fluid">
        <div class="row-fluid footer-bg content-border">
            <div class="col-md-3">
                <p class="develop-p pull-left">Powered by <a href="https://cabsofts.com/ecab" target="_BLANK">eCab V 1.0</a></p>
            </div>
            <div class="col-md-3">
                <p class="develop-p">Copyright © 2020 <a href="https://cabsofts.com/" target="_BLANK">CAB SOFTS</a></p>
            </div>
            <div class="col-md-6">
                <div class="row-fluid ">
                    <div class="contact">
                        <div class="col-md-4">
                            <span class="call-us"><i class="fa fa-phone" ></i> (+1) 302 261 9397</span>
                        </div> 
                        <div class="col-md-4">
                            <a href="<?php echo site_url(); ?>supporttickets.php">
                                <span class="call-us"><i class="fa fa-comments-o" ></i> Chat Support</span>
                            </a>
                        </div> 
                        <div class="col-md-4">
                            <a href="<?php echo site_url(); ?>supporttickets.php"><span class="ticket-support"><i class="fa fa-headphones" ></i> Ticket Support</span></a>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $("#loaderDiv").show();
    $(function () {
        //$(".chzn-select").chosen();
        setTimeout(function () {
            $("#loaderDiv").hide();
        }, 1200)
    });
</script>
        <!--<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>-->
<script src="<?php echo base_url(); ?>assets/system_design/scripts/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/timepicki.js"></script>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/bootstrap-datepicker.min.js"></script>

<!--Date Table-->
<?php if (in_array("datatable", $css_type)) { ?>
    <script src="<?php echo base_url(); ?>assets/system_design/scripts/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/system_design/scripts/datatables/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/system_design/scripts/datatables/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/system_design/scripts/datatables/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/system_design/scripts/datatables/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/system_design/scripts/datatables/buttons.html5.min.js"></script>
    <script class="init">

        // function selectAllClients() {
            $('#allClients').click(function (event) {
                if (this.checked) {
                    $('.ccheckbox').each(function () { //loop through each checkbox
                        $(this).prop('checked', true); //check 
                    });
                } else {
                    $('.ccheckbox').each(function () { //loop through each checkbox
                        $(this).prop('checked', false); //uncheck              
                    });
                }
            });
            $('#allClients').click(function (event) {
                if (this.checked) {
                    $('.ccheckbox').each(function () { //loop through each checkbox
                        $(this).prop('checked', true); //check 
                    });
                } else {
                    $('.ccheckbox').each(function () { //loop through each checkbox
                        $(this).prop('checked', false); //uncheck              
                    });
                }
            });
        //}
        $(document).ready(function() {
            $("#statut").on('change', function () {
                var statut_id = $(this).val();
                if (statut_id == 2) {
                    $(".divCompany").show();
                } else {
                    $(".divCompany").hide();
                }
            });
            $('.dpo').datepicker({
                format: "dd-mm-yyyy",
                autoclose: true
            });
            
            
        })
        $(document).ready(function() {
    $(".datepicker").datepicker({
        dateFormat: "dd-mm-yy",
        regional: "fr"
    });
    

});
    </script>
<?php } ?> 
<script type="text/javascript">
    
       $('#discounttimefrom').timepicki({
          step_size_minutes:'5',
        show_meridian: false,
        min_hour_value: 0,
        max_hour_value: 23,
        overflow_minutes: true,
        increase_direction: 'up',
        disable_keyboard_mobile: true});
    $('#discounttimeto').timepicki({
          step_size_minutes:'5',
        show_meridian: false,
        min_hour_value: 0,
        max_hour_value: 23,
        overflow_minutes: true,
        increase_direction: 'up',
        disable_keyboard_mobile: true});
     
    $('#drivermealfrom').timepicki({
          step_size_minutes:'5',
        show_meridian: false,
        min_hour_value: 0,
        max_hour_value: 23,
        overflow_minutes: true,
        increase_direction: 'up',
        disable_keyboard_mobile: true});
    $('#drivermealto').timepicki({
          step_size_minutes:'5',
        show_meridian: false,
        min_hour_value: 0,
        max_hour_value: 23,
        overflow_minutes: true,
        increase_direction: 'up',
        disable_keyboard_mobile: true});
    $('#driverothermealfrom').timepicki({
          step_size_minutes:'5',
        show_meridian: false,
        min_hour_value: 0,
        max_hour_value: 23,
        overflow_minutes: true,
        increase_direction: 'up',
        disable_keyboard_mobile: true});
    $('#driverothermealto').timepicki({
          step_size_minutes:'5',
        show_meridian: false,
        min_hour_value: 0,
        max_hour_value: 23,
        overflow_minutes: true,
        increase_direction: 'up',
        disable_keyboard_mobile: true});
    $('#nighttimefrom').timepicki({
          step_size_minutes:'5',
        show_meridian: false,
        min_hour_value: 0,
        max_hour_value: 23,
        overflow_minutes: true,
        increase_direction: 'up',
        disable_keyboard_mobile: true});
    $('#nighttimeto').timepicki({
          step_size_minutes:'5',
        show_meridian: false,
        min_hour_value: 0,
        max_hour_value: 23,
        overflow_minutes: true,
        increase_direction: 'up',
        disable_keyboard_mobile: true});
   
    
</script>

   <!--Date Table-->
<!--Date Table-->
<?php if (in_array("datatable", $css_type)) { ?>
<?php include('booking_table_config.php'); ?>
<?php } ?>
<!--Date Table--> 

</body>
</html>
