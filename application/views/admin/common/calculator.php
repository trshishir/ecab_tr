
<style>
	/* Main style sheet for jQuery Calculator v2.0.1 */
div.is-calculator, span.is-calculator {
	position: relative;
}
button.calculator-trigger {
	width: 25px;
	padding: 0px;
}
img.calculator-trigger {
	width: 30px;
	height: 30px;
	position: absolute;
	top: 7px;
	left: 7px;
	vertical-align: middle;
	cursor: pointer;
}
.calculator-popup {
	display: none;
	z-index: 10;
	margin: 0;
	padding: 10px;
	border: 1px solid #c0bbbb;
	color: #000;
	background-color: #f4f3f1;
	top: 40px !important;
    left: 35px !important;
    width: 300px !important;
}
.calculator-keyentry {
	position: absolute;
	top: 3px;
	right: 3px;
	width: 0px;
}
.calculator-inline {
	position: relative;
	border: 1px solid #888;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	border-radius: 4px;
	background-color: #f4f3f1;
}
.calculator-inline .calculator-close {
	display: none;
}
.calculator-disabled {
	position: absolute;
	z-index: 100;
	background-color: white;
	opacity: 0.5;
	filter: alpha(opacity=50);
}
.calculator-rtl {
	direction: rtl;
}
.calculator-prompt {
	clear: both;
	text-align: center;
}
.calculator-prompt.ui-widget-header {
	margin: 2px;
}
.calculator-result {
	clear: both;
	margin-bottom: 3px;
	margin-left: 3px;
	padding: 0px 11px;
	text-align: right;
	background-color: #fff;
	border: 1px solid #66afe9;
	-moz-border-radius: 1px;
	-webkit-border-radius: 1px;
	border-radius: 1px;
	font-size: 110%;
	height: 36px;
	line-height: 36px;
    font-size: 20px;
    overflow: hidden;
    width: 270px;
}
.calculator-result span {
	display: inline-block;
	width: 100%;
	 overflow: hidden;
}
.calculator-result .calculator-formula {
	font-size: 60%;
}
.calculator-focussed {
	background-color: #ffc;
}
.calculator-row {
	clear: both;
	width: 100%;
}
.calculator-space {
	float: left;
	margin: 2px;
	width: 30px;
}
.calculator-half-space {
	float: left;
	margin: 1px;
	width: 14px;
}
.calculator-row button {
	position: relative;
	float: left;
	margin: 3px;
	padding: 0px;
	height: 40px;
	background-color: #ffffff !important;
    background: linear-gradient(#ffffff, #ffffff 25%, #efecec) !important;
	-moz-border-radius: 1px;
	-webkit-border-radius: 1px;
	border-radius: 1px;
	border: 1px solid #c0bbbb;
	text-align: center;
	cursor: pointer;
	width: 40px;
}
.calculator-row button:focus{
	outline: 0.1px solid #c0bbbb;
}
.calculator-row .calculator-ctrl {
	width: 60px;
	background-color: #f4f3f1;
}
.calculator-row .calculator-undo, .calculator-row .calculator-clear-error, .calculator-row .calculator-clear {
	width: 86px !important;
}
.calculator-row .calculator-base, .calculator-row .calculator-angle {
	width: 30px;
	font-size: 70%;
}
.calculator-row .calculator-base-active, .calculator-row .calculator-angle-active {
	border: 2px inset #fff;
}
.calculator-digit, .calculator-oper {
	width: 30px;
}
.calculator-mem-empty, .calculator-digit[disabled] {
	color: #888;
}
.calculator-row .calculator-trig {
	font-size: 70%;
}
@-moz-document url-prefix() { // Firefox
	.calculator-trig, .calculator-base {
		text-indent: -3px;
	}
}
.calculator-key-down {
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	border-radius: 4px;
}
.calculator-keystroke {
	display: none;
	width: 16px;
	height: 14px;
	position: absolute;
	left: -1px;
	top: -1px;
	color: #000;
	background-color: #fff;
	border: 1px solid #888;
	font-size: 80%;
}
.calculator-angle .calculator-keystroke, .calculator-base .calculator-keystroke, .calculator-trig .calculator-keystroke {
	top: -2px;
	font-size: 95%;
}
.calculator-keyname {
	width: 22px;
	font-size: 70%;
}

</style>
<script src="<?php echo base_url(); ?>assets/system_design/calculator/jquery.plugin.js"></script>
<script src="<?php echo base_url(); ?>assets/system_design/calculator/jquery.calculator.js"></script>
<script>
	$(window).load(function(){
		$('.main_calculator_div').append($('.calculator-popup'));
	});
$(function () {
	$.calculator.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?php echo base_url(); ?>assets/images/calculator.png'});
	$('#sciCalculator').calculator({layout: $.calculator.scientificLayout});
});
</script>
<div class="main_calculator_div" style="position: relative;">
	<input type="hidden" id="sciCalculator">
</div>
