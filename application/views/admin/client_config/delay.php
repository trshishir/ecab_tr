<div class="row">
  <div class="ListDelay" >
  <input type="hidden" class="chk-Adddelay-btn" value="">
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" style="width:2%;">#</th>
            <th class="text-center" ><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('added_by'); ?></th>
            <th class=" text-center">Delay</th>
            <th class=" text-center">Statut</th>
            <th class="text-center" >Since</th>

          </tr>
          </thead>
          <tbody>
              <?php if (!empty($client_delay_data)): ?>
              <?php $cnt=1; ?>
              <?php foreach($client_delay_data as $key => $item):?>
                  <tr>
                    <td class="no-sort text-center"style="width:2%;">
                        <input type="checkbox" class="chk-maindelay-template" data-input="<?=$item->id?>">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="DelayidEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->create_date)) .'0000'. $item->id;?></a></td>
                      <td class="text-center"><?=from_unix_date($item->create_date)?></td>
                      <td class="text-center"><?=date("H:i:s", strtotime($item->create_date))?></td>
                      <td class="text-center"><?= $this->client_config_model->getuser($item->user_id);?></td>
                      <td class="text-center"><?=$item->delay; ?></td>
                      <td class="text-center"><?=$item->statut=='0'?'<span class="label label-success">Active</span>':'<span class="label label-danger">Hide</span>';?></td>

                      <td class="text-center"><?= timeDiff($item->create_date) ?></td>

                  </tr>
                  <?php $cnt++; ?>
              <?php endforeach; ?>
              <?php endif; ?>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>
<?=form_open("admin/client_config/delayAdd")?>
<div class="Delayadd" style="display: none;" >
  <div class="col-md-12">
    <div class="col-md-2" style="margin-top: 5px;">
      <div class="form-group">
        <span style="font-weight: bold;">Statut</span>
        <select class="form-control" name="statut" required>
          <option value="">Statut</option>
          <option value="0">Show</option>
          <option value="1">Hide</option>
        </select>
      </div>
    </div>
  <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span style="font-weight: bold;">Delay</span>
        <input type="text" required class="form-control" name="delay" placeholder="" value="">
    </div>
  </div>
</div>
 <div class="col-md-12">  
                            
      <!-- icons -->
       <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
      <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelDelay()"><span class="fa fa-close"> Cancel </span></button>
      <!-- icons -->
     
   </div>  
</div>
  <?php echo form_close(); ?>
<div class="delayEdit" style="display:none">
  <?=form_open("admin/client_config/delayEdit")?>
  <div class="delayEditajax">
  </div>
<div class="col-md-12">                          
      <!-- icons -->
       <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
      <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelDelay()"><span class="fa fa-close"> Cancel </span></button>
      <!-- icons -->   
   </div>  
</div>
 <?php echo form_close(); ?>
<div class="col-md-12 delayDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
  <?=form_open("admin/client_config/delayDelete")?>
    <input  name="tablename" value="vbs_clientdelay" type="hidden" >
    <input type="hidden" id="delaydeletid" name="delet_delay_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

    <a class="btn btn-default" style="cursor: pointer;" onclick="cancelDelay()"><span class="delete-icon">No</span></a>
  <?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
function Delayadd()
{
  setupDivDelay();
  $(".Delayadd").show();
}

function cancelDelay()
{
  setupDivDelay();
  $(".ListDelay").show();
}


function DelayEdit()
{
    var val = $('.chk-Adddelay-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivDelay();
        $(".delayEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/client_config/get_ajax_delay_data'; ?>',
                data: {'delay_id': val},
                success: function (result) {
                  document.getElementsByClassName("delayEditajax")[0].innerHTML = result;
                }
            });
}
function DelayidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivDelay();
      $(".delayEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/client_config/get_ajax_delay_data'; ?>',
              data: {'delay_id': val},
              success: function (result) {
           
                document.getElementsByClassName("delayEditajax")[0].innerHTML = result;
              }
          });
}
function DelayDelete()
{
  var val = $('.chk-Adddelay-btn').val();
  if(val != "")
  {
  setupDivDelay();
  $(".delayDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivDelay()
{

  $(".Delayadd").hide();
  $(".delayEdit").hide();
  $(".ListDelay").hide();
  $(".delayDelete").hide();

}
function setupDivDelayConfig()
{
 
  $(".Delayadd").hide();
  $(".delayEdit").hide();
  $(".ListDelay").show();
  $(".delayDelete").hide();

}
$('input.chk-maindelay-template').on('change', function() {
  $('input.chk-maindelay-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Adddelay-btn').val(id);
  $('#delaydeletid').val(id);
});

</script>
