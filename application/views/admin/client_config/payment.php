<div class="row">
  <div class="ListPayment" >
  <input type="hidden" class="chk-Addpayment-btn" value="">
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" style="width:2%;">#</th>
            <th class="text-center" ><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('added_by'); ?></th>
            <th class=" text-center">Payment</th>
            <th class=" text-center">Statut</th>
            <th class="text-center" >Since</th>

          </tr>
          </thead>
          <tbody>
              <?php if (!empty($client_payment_data)): ?>
              <?php $cnt=1; ?>
              <?php foreach($client_payment_data as $key => $item):?>
                  <tr>
                    <td class="no-sort text-center"style="width:2%;">
                        <input type="checkbox" class="chk-mainpayment-template" data-input="<?=$item->id?>">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="PaymentidEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->create_date)) .'0000'. $item->id;?></a></td>
                      <td class="text-center"><?=from_unix_date($item->create_date)?></td>
                      <td class="text-center"><?=date("H:i:s", strtotime($item->create_date))?></td>
                      <td class="text-center"><?= $this->client_config_model->getuser($item->user_id);?></td>
                      <td class="text-center"><?=$item->payment; ?></td>
                      <td class="text-center"><?=$item->statut=='0'?'<span class="label label-success">Active</span>':'<span class="label label-danger">Hide</span>';?></td>

                      <td class="text-center"><?= timeDiff($item->create_date) ?></td>

                  </tr>
                  <?php $cnt++; ?>
              <?php endforeach; ?>
              <?php endif; ?>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>
<?=form_open("admin/client_config/paymentAdd")?>
<div class="Paymentadd" style="display: none;" >
  <div class="col-md-12">
    <div class="col-md-2" style="margin-top: 5px;">
      <div class="form-group">
        <span style="font-weight: bold;">Statut</span>
        <select class="form-control" name="statut" required>
          <option value="">Statut</option>
          <option value="0">Show</option>
          <option value="1">Hide</option>
        </select>
      </div>
    </div>
  <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span style="font-weight: bold;">Payment</span>
        <input type="text" required class="form-control" name="payment" placeholder="" value="">
    </div>
  </div>
</div>
 <div class="col-md-12">  
                            
      <!-- icons -->
       <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
      <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelPayment()"><span class="fa fa-close"> Cancel </span></button>
      <!-- icons -->
     
   </div>  
</div>
  <?php echo form_close(); ?>
<div class="paymentEdit" style="display:none">
  <?=form_open("admin/client_config/paymentEdit")?>
  <div class="paymentEditajax">
  </div>
<div class="col-md-12">                          
      <!-- icons -->
       <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
      <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelPayment()"><span class="fa fa-close"> Cancel </span></button>
      <!-- icons -->   
   </div>  
</div>
 <?php echo form_close(); ?>
<div class="col-md-12 paymentDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
  <?=form_open("admin/client_config/paymentDelete")?>
    <input  name="tablename" value="vbs_clientpayment" type="hidden" >
    <input type="hidden" id="paymentdeletid" name="delet_payment_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

    <a class="btn btn-default" style="cursor: pointer;" onclick="cancelPayment()"><span class="delete-icon">No</span></a>
  <?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
function Paymentadd()
{
  setupDivPayment();
  $(".Paymentadd").show();
}

function cancelPayment()
{
  setupDivPayment();
  $(".ListPayment").show();
}


function PaymentEdit()
{
    var val = $('.chk-Addpayment-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivPayment();
        $(".paymentEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/client_config/get_ajax_payment_data'; ?>',
                data: {'payment_id': val},
                success: function (result) {
                  // alert(result);
                  document.getElementsByClassName("paymentEditajax")[0].innerHTML = result;
                }
            });
}
function PaymentidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivPayment();
      $(".paymentEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/client_config/get_ajax_payment_data'; ?>',
              data: {'payment_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("paymentEditajax")[0].innerHTML = result;
              }
          });
}
function PaymentDelete()
{
  var val = $('.chk-Addpayment-btn').val();
  if(val != "")
  {
  setupDivPayment();
  $(".paymentDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivPayment()
{
  // $(".paymentEditEdit").hide();
  $(".Paymentadd").hide();
  $(".paymentEdit").hide();
  $(".ListPayment").hide();
  $(".paymentDelete").hide();

}
function setupDivPaymentConfig()
{
  // $(".paymentEditEdit").hide();
  $(".Paymentadd").hide();
  $(".paymentEdit").hide();
  $(".ListPayment").show();
  $(".paymentDelete").hide();

}
$('input.chk-mainpayment-template').on('change', function() {
  $('input.chk-mainpayment-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Addpayment-btn').val(id);
  $('#paymentdeletid').val(id);
});

</script>
