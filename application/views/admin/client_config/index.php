<?php $locale_info = localeconv(); ?>

<style>
   
   
  .nav-tabs > li.active {
   background:linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%);
    border-bottom: none;
    }
    .nav-tabs>li.active>a{
         border: 1px solid #44c0e5!important;
    }
  .nav-tabs > li {
     background:linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%);
  
   border-left: 1px solid #44c0e5!important;
    height: 55px;
    margin: 0px !important;
    }

    .nav-tabs > li > a {

    background:linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%);
    color: #fff !important;
    font-size: 12px;
    padding-left: 10px;
    display: block !important;
    width: 100% !important;
    height: 100% !important;
    text-transform: uppercase;
    font-weight: bold;
    transition: 0.2s;
    outline: none;
    border: none;
    border-radius: 0px;
    line-height: 30px !important;
}
.dataTables_wrapper .dataTables_filter {
    float: left;
    text-align: left;
    margin-left: 10px;
}
input[type="search"]{
    border:1px solid #cccccc;
    padding-left: 5px;
    border-radius: 0px;
}
input[type="search"]:focus{
    border:1px solid #cccccc !important;
}
    
    .tab-pane{
        padding: 30px 30px 30px 14px;
    }
    .configTable{
        padding: 0px;
    }
    .dataTables_wrapper .dataTables_filter input {
    margin-right: 2px;
    margin-left: 0 !important;
    max-width: 100% !important;
    width: 100% !important;
    float: right;
    }
    .dataTables_wrapper{
        margin-top: 20px;
    }
   
    
  

</style>

<style>
 
.delete-icon {
    background: url(http://uniqueweb.co.in/project/ecabapp//assets/delete-icon.png) no-repeat left center !important;
    padding: 0px 0px 0px 22px;
}
.save-icon {
    background: url(http://uniqueweb.co.in/project/ecabapp//assets/save-icon.png) no-repeat left center !important;
    padding: 0px 0px 0px 22px;
}
#editnotworkingbtn,#notworkingbtn{
    background: linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%) !important;
}
.plusgreeniconconfig{
    border: 1px solid green;cursor: pointer;overflow: hidden;outline: none;font-size: 16px;
    color: #ffffff;
    height: 25px;
    width: 24px;
    border-radius: 50%;
    background-color: green;
    position: absolute;
    top: 22px;
    right: -20px;
    z-index: 10;
    text-align: center;
    margin: 0px;
}
.minusrediconconfig{
    border: 1px solid red;cursor: pointer;overflow: hidden;outline: none;font-size: 16px;
    color: #ffffff;
    height: 25px;
    width: 24px;
    border-radius: 50%;
    background-color: red;
    position: absolute;
    bottom: 9px;
    right: -20px;
    z-index: 10;
    text-align: center;
    margin: 0px;
}
.minusrediconconfig > i,.plusgreeniconconfig > i{
    margin:0px;
}
#editclientrestcost,#clientrestcost{
    background-color: #add8e63d !important;
}
.dataTables_wrapper .dataTables_paginate .paginate_button{
   color:#fff !important;
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current{
   color:#fff !important;
}

.dataTables_wrapper .dataTables_paginate .paginate_button:hover {
     background: linear-gradient(to bottom, #0d85a8 0%, #35add1 39%,#44c0e5 100%) !important;
   
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
     background: linear-gradient(to bottom, #0d85a8 0%, #35add1 39%,#44c0e5 100%) !important;
    color:#fff !important;
}

</style>



<?php $active_status =  $this->session->flashdata('alert');?>
<?php if (!isset($active_status['price_stu_id']) ) {
  $active_status['price_stu_id']='';
} ?>
<?php if (!isset($active_status['status_id'])) {
  $active_status['status_id']='';
} ?>
<?php if ($active_status['status_id']!='') { $status=$active_status['status_id']; $tabstatus=$active_status['status_id']; }else{$status='1'; $tabstatus='1';} ?>
<?php if ($active_status['price_stu_id']!='') { $price_stu=$active_status['price_stu_id']; }else{$price_stu='1';} ?>
<section id="content">
    <div class="listDiv"><!--col-md-10 padding white right-p-->
    <?php $this->load->view('admin/common/breadcrumbs');?>
        <?php $this->load->view('admin/common/alert');?>
        <?php echo $this->session->flashdata('message'); ?>
            <div class="row-fluid"  id="ShowTabs" style="margin-bottom: 10px; margin-left:0px;">
                                <ul class="nav nav-tabs responsive">
                                            <li <?=$status==1? 'class="active"':''?>><a href="#STATUS" class="status_E" role="tab" data-toggle="tab">STATUT</a></li>
                                            <li <?=$status==2? 'class="active"':''?>><a href="#CIVILITY" class="clivility_E" role="tab" data-toggle="tab">CIVILITE</a></li>
                                            <li <?=$status==3? 'class="active"':''?>><a href="#TYPE" class="type_E" role="tab" data-toggle="tab">TYPE OF CLIENT</a></li>
                                            <li <?=$status==4? 'class="active"':''?>><a href="#PAYMENT" role="tab" class="payment_E" data-toggle="tab">PAYMENT METHODE</a></li>
                                            <li <?=$status==5? 'class="active"':''?>><a href="#DELAY" class="delay_E" role="tab" data-toggle="tab">DELAY OF PAYMENT</a></li>          
                                </ul>
                <div class="tab-content responsive" style="width:100%;display: table;">
                        <!--status tab starts-->
                        <div class="tab-pane fade <?=$status==1? 'in active':''?>" id="STATUS" style="border: 1px solid #ccc;">
                            <div class="row">
                            <div class="ListclientStatus">
                                <input type="hidden" class="chk-AddClientStatus-btn" value="">
                     
                            <div class="col-md-12">
                            <div class="module-body table-responsive">
                                <table class="configTable table table-bordered table-striped  table-hover dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
                                    <thead>
                                    <tr>
                                        <th class="no-sort text-center"style="width:2% !important;">#</th>
                                        <th class="text-center" style="width:3% !important;"><?php echo $this->lang->line('id'); ?></th>
                                        <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('date'); ?></th>
                                        <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('time'); ?></th>
                                        <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('added_by'); ?></th>
                                        <th class="column-first_name text-center" style="width:6% !important;">Name</th>
                                        <th class="column-first_name text-center" style="width:6% !important;">Statut</th>
                                        <th class="text-center" style="width:10%;">Since</th>
                                        
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php $cnt=1; ?>
                                        <?php 
                                        if (!empty($client_status_data)): 
                                           
                                        ?>
                                        <?php foreach($client_status_data as $key => $item):?>
                                            <tr>
                                                <td class="text-center">
                                                  <input type="checkbox" class="chk-mainvat-template" data-input="<?=$item->id?>"></td> 
                                                <td class="text-center"><a href="javascript:void()" onclick="clientStatusidEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->create_date)) .'0000'. $item->id;?></a></td>
                                                <td class="text-center"><?=from_unix_date($item->create_date)?></td>
                                                <td class="text-center"><?=date("H:i:s", strtotime($item->create_date))?></td>
                                                <td class="text-center"><?= $this->client_config_model->getuser($item->user_id);?></td>
                                                <td class="text-center"><?= $item->name; ?></td>
                                                 <td class="text-center"><?=$item->statut=='0'?'<span class="label label-success">Active</span>':'<span class="label label-danger">Hide</span>';?></td>

                                                <td class="text-center"><?= timeDiff($item->create_date) ?></td>

                                            </tr>
                                            <?php $cnt++; ?>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                                <br>
                                </div>
                            </div>
                                                    </div>
                            <?=form_open("admin/client_config/statusAdd")?>
                            <div class="clientStatusAdd" style="display: none;" >
                        <div class="col-md-12">     
                             <div class="col-md-2" style="margin-top: 5px;">
                              <div class="form-group">
                                <span style="font-weight: bold;">Statut</span>
                                <select class="form-control" name="statut" required>
                                  <option value="">Statut</option>
                                  <option value="0">Show</option>
                                  <option value="1">Hide</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-2" style="margin-top: 5px;">
                                <div class="form-group">
                                <span style="font-weight: bold;">Name</span>
                                    <input type="text" class="form-control" name="name" placeholder="" value="" required="">
                                </div>
                            </div>
                        </div>
                            <div class="col-md-12">  
                            
                                <!-- icons -->
                                 <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                                <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelClientStatus()"><span class="fa fa-close"> Cancel </span></button>
                                <!-- icons -->
                               
                             </div>      
                            </div>
                            <?php echo form_close(); ?>
                            <div class="clientStatusEdit" style="display:none">
                            <?=form_open("admin/client_config/statusEdit")?>
                            <div class="clientStatusEditajax">
                            </div>
                             <div class="col-md-12">  
                            
                                <!-- icons -->
                                 <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                                <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelClientStatus()"><span class="fa fa-close"> Cancel </span></button>
                                <!-- icons -->
                               
                             </div>  
                            
                            <?php echo form_close(); ?>
                            </div>
                            <div class="col-md-12 clientStatusDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
                                        <?=form_open("admin/client_config/statusDelete")?>
                                            <input type="hidden" id="clientStatusid" name="delet_client_status_id" value="">
                                            <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10px;"> Are you sure you want to delete selected?</div>
                                <button  class="btn btn-default"style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

                                            <a class="btn btn-default" style="cursor: pointer;" onclick="cancelClientStatus()"><span class="delete-icon">No</span> </a>
                                        <?php echo form_close(); ?>
                            </div>
                        </div>
                        </div>
                        <div class="tab-pane fade <?=$status== '2'? 'in active':''?>" id="CIVILITY" style="border: 1px solid #ccc;">
                        <?php include('civilite.php'); ?>
                        </div>
                        <div class="tab-pane fade <?=$status== '3'? 'in active':''?>" id="TYPE" style="border: 1px solid #ccc;">
                        <?php include('type.php'); ?>
                        </div>

                        <div class="tab-pane fade <?=$status== '4'? 'in active':''?>"  id="PAYMENT" style="border: 1px solid #ccc;">
                        <?php include('payment.php'); ?>
                        </div>
                        <div class="tab-pane fade <?=$status== '5'? 'in active':''?>" id="DELAY" style="border: 1px solid #ccc;">
                        <?php include('delay.php'); ?>
                        </div>

                       
                      
                   
                     
            </div>
            <!--/.module-->
        </div>

    </div>
</section>
<script>
    jQuery(function($){
        $.datepicker.regional['fr'] = {
            closeText: 'Fermer',
            prevText: '&#x3c;Préc',
            nextText: 'Suiv&#x3e;',
            currentText: 'Aujourd\'hui',
            monthNames: ['Janvier','Fevrier','Mars','Avril','Mai','Juin',
                'Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
            monthNamesShort: ['Jan','Fev','Mar','Avr','Mai','Jun',
                'Jul','Aou','Sep','Oct','Nov','Dec'],
            dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
            dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
            dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
            weekHeader: 'Sm',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: '',
            maxDate: '+12M +0D',
            showButtonPanel: true
        };
        $.datepicker.setDefaults($.datepicker.regional['fr']);
       
    });

    $(document).ready(function(){
        $( ".datepicker" ).datepicker({
            dateFormat: "dd-mm-yy",
            regional: "fr"
        });
        $('#MainAdd').click(function(){

            $('#MainTab').hide();
            $('#ShowTabs').attr('class','row');

        });

    });

function cancelClientStatus()
{
  setupClientStatus();
  $(".ListclientStatus").show();
}
     function clientStatusAdd()
    {
        setupClientStatus();
        $(".clientStatusAdd").show();
    }
  
    function clientStatusEdit()
    {
        var val = $('.chk-AddClientStatus-btn').val();
        if (val=='')
        {
            alert("Please select record!");
            return false;
        }
        setupClientStatus();
        $(".clientStatusEdit").show();
        $.ajax({
            type: "GET",
            url: '<?php echo base_url().'admin/client_config/get_ajax_status_data'; ?>',
            data: {'client_status_id': val},
            success: function (result) {
                // alert(result);
                document.getElementsByClassName("clientStatusEditajax")[0].innerHTML = result;
            }
        });
    }
    function clientStatusidEdit(id){
        var val = id;
        if (val=='')
        {
            alert("Please select record!");
            return false;
        }
        setupClientStatus();
        $(".clientStatusEdit").show();
        $.ajax({
            type: "GET",
            url: '<?php echo base_url().'admin/client_config/get_ajax_status_data'; ?>',
            data: {'client_status_id': val},
            success: function (result) {
                // alert(result);
                document.getElementsByClassName("clientStatusEditajax")[0].innerHTML = result;
            }
        });
    }
function StatusDelete()
{
  var val = $('.chk-AddClientStatus-btn').val();
  if(val != "")
  {
  setupClientStatus();
  $(".clientStatusDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}
    function setupClientStatus()
    {
        $(".clientStatusAdd").hide();
        $(".clientStatusEdit").hide();
        $(".ListclientStatus").hide();
        $(".clientStatusDelete").hide();

    }
      function setupDivStatusConfig()
    {
        $(".clientStatusAdd").hide();
        $(".clientStatusEdit").hide();
        $(".ListclientStatus").show();
        $(".clientStatusDelete").hide();

    }

    $(document).on('change', 'input.chk-mainvat-template', function() {
        $('input.chk-mainvat-template').not(this).prop('checked', false);
        var id = $(this).attr('data-input');
        console.log(id)
        $('.chk-AddClientStatus-btn').val(id);
        $('#clientStatusid').val(id);
    });

    function getClientaddes(){
        $.ajax({
            url:"<?php echo base_url('admin/getClientadded') ?>",
            method:"GET",
            success:function(success){
                $('#tableBody').html(success);
            },error:function(xhr){
                console.log(xhr.responseText);
            }
        });
    }


    $('#save').on('click',function(){
        clientStatusEvent();
    })

</script>
<script>
$(document).ready(function () {
    $(window).on('load', function() {
        <?php if ($tabstatus=='1') { ?>
        clientStatusEvent();
        <?php }else if ($tabstatus=='2'){ ?>
        civiliteEvent();
        <?php }else if ($tabstatus=='3'){ ?>
        typeEvent();
        <?php }else if ($tabstatus=='4'){ ?>
        paymentEvent();
        <?php }else if ($tabstatus=='5'){ ?>
         delayEvent();
        <?php } ?>

    });

    $('.status_E').click(function(){
        clientStatusEvent();
    });
    $('.clivility_E').click(function(){
      civiliteEvent();
    });
    $('.type_E').click(function(){
        typeEvent();
    });
    $('.payment_E').click(function(){
        paymentEvent();
    });
      $('.delay_E').click(function(){
        delayEvent();
    });

    

    function clientStatusEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="clientStatusAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="clientStatusEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="StatusDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }

    function civiliteEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Civiliteadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="CiviliteEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="CiviliteDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
      function typeEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Typeadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="TypeEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="TypeDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
     function paymentEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Paymentadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="PaymentEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="PaymentDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
      function delayEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Delayadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="DelayEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="DelayDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }



 
 function removeAllResources(){
    setupDivCiviliteConfig();
    setupDivTypeConfig();
    setupDivStatusConfig();
    setupDivPaymentConfig();
    setupDivDelayConfig();
   
 }
    
});

</script>
