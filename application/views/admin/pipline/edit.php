<?php $locale_info = localeconv(); ?>
<link href="<?php echo base_url(); ?>assets/system_design/css/simple-rating.css" rel="stylesheet">
<div class="col-md-12"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12">
                <?php $this->load->view('admin/common/alert');?>
                <div class="module">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="module-head">
                    </div>
                    <div class="module-body">
                        <?=form_open("admin/pipline/".$data->id."/update")?>
                        <div class="row">
                            <div class="col-xs-7">
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="form-group">
                                            <label>Statut*</label>
                                            <select name="status" id="status" class="form-control" required >
                                                <option value="">---Select---</option>
                                                <option <?php if($data->status == 1){echo "selected";} ?> value="1">Enabled</option>
                                                <option <?php if($data->status == 0){echo "selected";} ?> value="0">Disabled</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-2">
                                        <div class="form-group">
                                            <label>Civility*</label>
                                            <select class="form-control" name="civility" required>
                                                <?php foreach(config_model::$civility as $key => $civil):?>
                                                    <option <?php if($data->civility == $civil){echo "selected";} ?> value="<?=$civil?>"><?=$civil?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label>First Name*</label>
                                            <input type="text" maxlength="100" class="form-control" required name="first_name" placeholder="First Name*" value="<?=set_value('first_name', $data->first_name)?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label>Last Name*</label>
                                            <input type="text" maxlength="100" class="form-control" required name="last_name" placeholder="Last Name*" value="<?=set_value('last_name', $data->last_name)?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-2">
                                        <div class="form-group">
                                            <label>Company*</label>
                                            <input type="text" maxlength="100" class="form-control" required name="company" placeholder="Company*" value="<?=set_value('company', $data->company)?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="form-group">
                                            <label>Sales Operator*</label>
                                            <select name="sales_operator" id="sales_operator" class="form-control" required >
                                                <option value="">---Select---</option>
                                                <?php foreach ($operators as $o) { ?>
                                                    <option <?php if($data->sales_operator == $o['id']){echo "selected";} ?> value="<?=$o['id']?>"><?=$o['first_name']?> <?=$o['last_name']?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label>Address*</label>
                                            <input type="text" class="form-control" required name="address" placeholder="Address*" value="<?=set_value('address', $data->address)?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="form-group">
                                            <label>Address1*</label>
                                            <input type="text" class="form-control" required name="address1" placeholder="Address1*" value="<?=set_value('address', $data->address1)?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea rows="6" class="form-control" name="description" placeholder="Description"><?=set_value('description', $data->description)?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="list1" style="border: 1px solid lightblue;max-height: 500px;overflow-x: hidden;overflow-y: auto;">
                                    <?php $i = 0; foreach ($notes_data as $n) { ?>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-xs-2">
                                                    <div class="form-group">
                                                        <label>Date</label>
                                                        <input type="date" class="form-control" name="date1[]" value="<?=$n->date?>">
                                                    </div>
                                                </div>
                                                <div class="col-xs-2">
                                                    <div class="form-group">
                                                        <label>Time</label>
                                                        <input type="time" class="form-control" name="time1[]" value="<?=$n->time?>">
                                                    </div>
                                                </div>
                                                <div class="col-xs-2">
                                                    <div class="form-group">
                                                        <label>Reminder Type</label>
                                                        <select name="reminder_type[]" class="form-control quote_reminder_type">
                                                            <option <?php if($n->reminder_type == "call"){echo "selected";} ?> value="call">Call</option>
                                                            <option <?php if($n->reminder_type == "email"){echo "selected";} ?> value="email">Email</option>
                                                            <option <?php if($n->reminder_type == "mail"){echo "selected";} ?> value="mail">Mail</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-2">
                                                    <div class="form-group">
                                                        <label>Quote ID</label>
                                                        <select id="quote_id1" name="quote_id1[]" class="form-control quote_id1">
                                                            <option data-status="" data-date="" data-since="" value="0">---Select---</option>
                                                            <?php foreach ($quotes as $q) { ?>
                                                                <option <?php if($n->quote_id == $q['id']){echo "selected";} ?> data-status="<?=$q['status']?>" data-date="<?=date("d/m/Y", strtotime($q['created_at']))?>" data-since="<?=timeDiff($q['last_action'])?>" value="<?=$q['id']?>"><?=$q['id']?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1">
                                                    <div class="form-group">
                                                        <label>Statut</label>
                                                        <input type="text" class="form-control quote_status1" name="quote_status1[]" readonly value="<?=$n->quote_status?>">
                                                    </div>
                                                </div>
                                                <div class="col-xs-2">
                                                    <div class="form-group">
                                                        <label>Date</label>
                                                        <input type="text" class="form-control quote_date1" name="quote_date1[]" readonly value="<?=$n->quote_date?>">
                                                    </div>
                                                </div>
                                                <div class="col-xs-1">
                                                    <div class="form-group">
                                                        <label>Since</label>
                                                        <input type="text" class="form-control quote_since1" name="quote_since1[]" readonly value="<?=$n->quote_since?>">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Note</label>
                                                        <textarea rows="6" class="form-control quote_note" name="note[]" placeholder="Note"><?=set_value('note', $n->note)?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php if($i > 0){ ?>
                                                <br />
                                                <button type = "button" onclick="removeList1(this);" class="btn" style="margin-top: 0px;background-color:red;float: right;margin-right: 27px;margin-bottom: 8px;"><i class="fa fa-times"></i></button>
                                            <?php } $i++; ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-xs-5">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label>Phone*</label>
                                            <input type="tel" class="form-control" required name="phone" placeholder="Phone*" value="<?=set_value('phone', $data->phone)?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label>Fax*</label>
                                            <input type="text" class="form-control" required name="fax" placeholder="Fax*" value="<?=set_value('fax', $data->fax)?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label>Email*</label>
                                            <input type="email" class="form-control" required name="email" placeholder="Email*" value="<?=set_value('email', $data->email)?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label>Zip Code*</label>
                                            <input type="ttextel" class="form-control" required name="zip_code" placeholder="Zip Code*" value="<?=set_value('zip_code', $data->zip_code)?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label>City*</label>
                                            <input type="text" class="form-control" required name="city" placeholder="City*" value="<?=set_value('city', $data->city)?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="list2" style="height: 159px;overflow-x: hidden;overflow-y: auto;border: 1px solid lightblue;">
                                    <?php $j = 0; foreach ($list_data as $l) { ?>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-xs-3">
                                                    <div class="form-group">
                                                        <label>Quote ID</label>
                                                        <select id="quote_id2" name="quote_id2[]" class="form-control quote_id2">
                                                            <option data-status="" data-date="" data-since="" value="0">---Select---</option>
                                                            <?php foreach ($quotes as $q) { ?>
                                                                <option <?php if($l->quote_id == $q['id']){echo "selected";} ?> data-status="<?=$q['status']?>" data-date="<?=date("d/m/Y", strtotime($q['created_at']))?>" data-since="<?=timeDiff($q['last_action'])?>" value="<?=$q['id']?>"><?=$q['id']?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-3">
                                                    <div class="form-group">
                                                        <label>Statut</label>
                                                        <input type="text" class="form-control quote_status2" name="quote_status2[]" readonly value="<?=$l->quote_status?>">
                                                    </div>
                                                </div>
                                                <div class="col-xs-3">
                                                    <div class="form-group">
                                                        <label>Date</label>
                                                        <input type="text" class="form-control quote_date2" name="quote_date2[]" readonly value="<?=$l->quote_date?>">
                                                    </div>
                                                </div>
                                                <div class="col-xs-1">
                                                    <div class="form-group">
                                                        <label>Since</label>
                                                        <input type="text" class="form-control quote_since2" name="quote_since2[]" readonly value="<?=$l->quote_since?>">
                                                    </div>
                                                </div>
                                                <?php if($j == 0){ ?>
                                                    <div class="col-xs-1">
                                                        <div class="form-group">
                                                            <button type = "button" onclick="addList2(this);" class="btn" style="margin-top: 28px; background-color:green;"><i class="fa fa-plus"></i></button>
                                                        </div>
                                                    </div>
                                                <?php }else{ ?>
                                                    <div class="col-xs-1">
                                                        <div class="form-group">
                                                        <button type = "button" onclick="removeList2(this);" class="btn" style="margin-top: 0px;background-color:red;"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                <?php } $j++; ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="col-xs-1">
                                            <button type="button" class="btn" onclick="addList1(this);" style="background-color: green;margin-top: 14px;margin-left: -16px;"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-right">
                                    <button class="btn btn-default">Save</button>
                                    <a href="<?=base_url("admin/pipline")?>" class="btn btn-default">Cancel</a>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/simple-rating.js"></script>

<script type="text/javascript">
    function addList1(e){
        var html = '<div class="row"> '+
            '<div class="col-xs-12"> '+
                '<div class="col-xs-2"> '+
                    '<div class="form-group"> '+
                        '<input type="date" class="form-control" name="date1[]"> '+
                    '</div> '+
                '</div> '+
                '<div class="col-xs-2"> '+
                    '<div class="form-group"> '+
                        '<input type="time" class="form-control" name="time1[]"> '+
                    '</div> '+
                '</div> '+
                '<div class="col-xs-2"> '+
                    '<div class="form-group"> '+
                        '<select name="reminder_type[]" class="form-control"> '+
                            '<option value="call">Call</option> '+
                            '<option value="email">Email</option> '+
                            '<option value="mail">Mail</option> '+
                        '</select> '+
                    '</div> '+
                '</div> '+
                '<div class="col-xs-2"> '+
                    '<div class="form-group"> '+
                        '<select name="quote_id1[]" class="form-control quote_id1">';
                            $("#quote_id1 option").each(function()
                            {
                                html += '<option data-status="'+$(this).attr('data-status')+'" data-date="'+$(this).attr('data-date')+'" data-since="'+$(this).attr('data-since')+'" value="'+$(this).text()+'">'+$(this).text()+'</option>';
                            });
                        html += '</select> '+
                    '</div> '+
                '</div> '+
                '<div class="col-xs-1"> '+
                    '<div class="form-group"> '+
                        '<input type="text" class="form-control quote_status1" name="quote_status1[]" readonly> '+
                    '</div> '+
                '</div> '+
                '<div class="col-xs-2"> '+
                    '<div class="form-group"> '+
                        '<input type="text" class="form-control quote_date1" name="quote_date1[]" readonly> '+
                    '</div> '+
                '</div> '+
                '<div class="col-xs-1"> '+
                    '<div class="form-group"> '+
                        '<input type="text" class="form-control quote_since1" name="quote_since1[]" readonly> '+
                    '</div> '+
                '</div> '+
                '<div class="col-xs-12"> '+
                    '<div class="form-group"> '+
                        '<textarea rows="6" class="form-control note" name="note[]" placeholder="Note"><?=set_value('note',$this->input->post('note'))?></textarea> '+
                    '</div> '+
                '</div> '+
            '</div> '+
            '<br /> '+
            '<button type = "button" onclick="removeList1(this);" class="btn" style="margin-top: 0px;background-color:red;float: right;margin-right: 27px;margin-bottom: 8px;"><i class="fa fa-times"></i></button>' +
        '</div>';
        $('.list1').append(html);
    }
    function removeList1(e){
        $(e).parent().remove();
    }
    function addList2(e){
        var html = '<div class="row"> '+
            '<div class="col-xs-12"> '+
                '<div class="col-xs-3"> '+
                    '<div class="form-group"> '+
                        '<select name="quote_id2[]" class="form-control quote_id2">';
                            $("#quote_id2 option").each(function()
                            {
                                html += '<option data-status="'+$(this).attr('data-status')+'" data-date="'+$(this).attr('data-date')+'" data-since="'+$(this).attr('data-since')+'" value="'+$(this).text()+'">'+$(this).text()+'</option>';
                            });
                        html += '</select> '+
                    '</div> '+
                '</div> '+
                '<div class="col-xs-3"> '+
                    '<div class="form-group"> '+
                        '<input type="text" class="form-control quote_status2" name="quote_status2[]" readonly> '+
                    '</div> '+
                '</div> '+
                '<div class="col-xs-3"> '+
                    '<div class="form-group"> '+
                        '<input type="text" class="form-control quote_date2" name="quote_date2[]" readonly> '+
                    '</div> '+
                '</div> '+
                '<div class="col-xs-1"> '+
                    '<div class="form-group"> '+
                        '<input type="text" class="form-control quote_since2" name="quote_since2[]" readonly> '+
                    '</div> '+
                '</div> '+
                '<div class="col-xs-1"> '+
                    '<div class="form-group"> '+
                        '<button type = "button" onclick="removeList2(this);" class="btn" style="margin-top: 0px;background-color:red;"><i class="fa fa-times"></i></button> '+
                   '</div> '+
                '</div> '+
            '</div> '+
        '</div>';
        $('.list2').append(html);
    }
    function removeList2(e){
        $(e).parent().parent().parent().parent().remove();
    }
    $(document).ready(function(){
        $(document).on("change", ".quote_id1", function(){
            var select_val = $(this).parent().find("select option:selected").val();
            var select_status = $(this).parent().find("select option:selected").attr('data-status');
            var select_date = $(this).parent().find("select option:selected").attr('data-date');
            var select_since = $(this).parent().find("select option:selected").attr('data-since');
            $(this).parent().parent().parent().parent().find(".quote_status1").val(select_status);
            $(this).parent().parent().parent().parent().find(".quote_date1").val(select_date);
            $(this).parent().parent().parent().parent().find(".quote_since1").val(select_since);
        });
        $(document).on("change", ".quote_id2", function(){
            var select_val = $(this).parent().find("select option:selected").val();
            var select_status = $(this).parent().find("select option:selected").attr('data-status');
            var select_date = $(this).parent().find("select option:selected").attr('data-date');
            var select_since = $(this).parent().find("select option:selected").attr('data-since');
            $(this).parent().parent().parent().parent().find(".quote_status2").val(select_status);
            $(this).parent().parent().parent().parent().find(".quote_date2").val(select_date);
            $(this).parent().parent().parent().parent().find(".quote_since2").val(select_since);
        });
    });
</script>