<section id="content">
	<div class="listDiv">
		<?php $this->load->view('admin/common/breadcrumbs'); ?>
		<div class="row-fluid">
			<?php   $flashAlert = $this->session->flashdata('alert');
			if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
			?>
			<br>
			<div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
				<strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
				<button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<?php   } ?>
		</div>
		<div class="module">
			<?php echo $this->session->flashdata('message'); ?>
			<div id="services-section" class="body-bg">
				<div class="">
					<?php   foreach($data_list as $item):    ?>
					<!--<div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px;padding:15px 0px 6px 0px;border: 1px solid lightgray;background:transparent linear-gradient(#fbfbfb, #ececec, #cecece) repeat scroll 0 0; margin-left: 19px;width: 97%;">-->
					<div class="col-md-3 col-sm-12 col-xs-12">
						<div class="box">
							<div class="box_head">
								<h3><?=$item->title;?></h3>
							</div>
							<div class="box_img">
								<?php
									if($item->image==''):
								?>
								<img src="<?php echo base_url();?>assets/images/no-preview.jpg" alt="FIAT SCUDO TPMR - Fiat ScudoTpmr" title="<?php echo $item->title?>">
								<?php
								else:
								?>
								<img src="<?php echo base_url();?>uploads/cms/<?php echo $item->image?>" alt="FIAT SCUDO TPMR - Fiat ScudoTpmr" title="<?php echo $item->title?>">
								<?php
								endif;
								?>
							</div>
							<div class="description">
								<?php
								$text= $item->description;
								$text= strip_tags($text);
								// $text=trim($text);
								$text = substr($text, 0, 150);
								// echo implode(' ', array_slice(explode(' ', $text), 0, 23)).' ...';
								 echo $text.' ...';
								?>
							</div>
							<div class="button-row"> <!--button-row-->
							<div class="read-more"><a href="<?php echo base_url("admin/cms/tutorials/".$item->id."/view");?>" class="button light_green_button">Read more</a></div>
						</div>
						<div class="user_details">
							<img style="width: 30px;margin-left: 10px; border-radius: 50%;
							width: 25px;
							border: 1px solid gray;" src="http://media.npr.org/assets/news/2009/10/27/facebook1_sq-17f6f5e06d5742d8c53576f7c13d5cf7158202a9.jpg?s=16" alt="" />
							<h6 style="width: max-content;margin-top: -31px;margin-left: 45px;font-weight: lighter;"> <?php echo $item->civility.' '.$item->first_name.' '.$item->last_name;?><span style="padding-left: 5px;" class="card_right">Publish Date: <?php echo date('d/m/Y', strtotime($item->created_date));?></span></h6>
						</div>
					</div>
					<!--<div class="col-md-8">
						<div class="col-md-12">
							<div class="col-md-12 " style="left:10px;
								top: -17px;">
								<p class="pull-right"><strong>Publish </strong> Date: <?php echo date('d-m-Y', strtotime($item->created_date));?>,
								<strong>Author</strong> <?php echo $item->civility.' '.$item->first_name.' '.$item->last_name;?></p>
							</div>
						</div>
					</div>-->
				</div>
				<?php endforeach;?>
			</div>
		</div>
		<div class="clearfix"></div>
		<?php /* ?>
		<div class="row">
			<div class="col-md-12">
				<div id="mainbox col-md-12">
					<?php foreach ($data_list as $item):
					// var_dump($item);
					?>
					<div class="card col-md-6">
						<div class="col-md-12">
							<small style="text-transform: uppercase;"><?=$item->category_name;?></small>
							<h3><?=$item->title;?></h3>
						</div>
						<img src="http://media.npr.org/assets/news/2009/10/27/facebook1_sq-17f6f5e06d5742d8c53576f7c13d5cf7158202a9.jpg?s=16" alt="" />
						<h6> <?php echo $item->civility.' '.$item->first_name.' '.$item->last_name;?><span class="card_right">Publish Date: <?php echo date('d/m/Y', strtotime($item->created_date));?></span></h6>
						<div class="col-md-6" style="min-height:70px">
							<?php
							$file =base_url().'uploads/cms/'.$item->image;
							if(get_image_mime_type($file))
							{
								echo "<img class='admin_border' width='100%' height='240px' src='$file'>";
							}
							else{
							?>
							<video width="320" height="240" controls>
								<source src="<?=$file;?>" type="video/mp4">
								Your browser does not support the video tag.
							</video>
							<?php
							}
							?>
						</div>
						<div class="col-md-6" style="min-height:70px">
							<?php
							$text= $item->description;
							$text= strip_tags($text);
							echo implode(' ', array_slice(explode(' ', $text), 0, 30)).' ...';
							?>
						</div>
					</div>
					<?php endforeach;?>
				</div>
				<?php */ ?>
			</div>
		</div>
	</div>
</div>
</section>
<style type="text/css">
	.card {
		width: 49%;
		/*border: 1px solid gray;*/
			/*border-top-color: gray;
			border-top-style: solid;
			border-top-width: 1px;*/
			box-shadow: 1px 1px 3px #888;
			/*border-top: 10px solid green;*/
			min-height: 250px;
			padding-: 10px !important;
			margin-right: 1%;
			background: linear-gradient(to bottom, #e8e0e0 0%, #ececec 39%, #fbf4f4 100%, #8a8989 100%);
			/*background: linear-gradient(to bottom, #e8e0e0 0%, #ececec 39%, #fbf4f4 75%, #8a8989 100%);*/
			float: left;
			margin-bottom: 1%;
		}
		.card > img {
			border-radius: 50%;
			width: 25px;
			margin: 3px 1px 20px 5px;
			border: 1px solid gray;
		}
		.card > h6 {
			font-weight: lighter;
			margin-left: 40px;
			margin-top: -54px;
			border-bottom: groove 3px #f2f2f2;
			margin-right: 5px;
		}
		.card >p{
			margin: 30px 10px;
			font-family: segoe ui;
			line-height: 1.4em;
			font-size: 1.2em;
		}
		#mainbox{
			font-family: calibri;
			box-sizing: border-box;
			justify-content: center;
			/*display: flex;*/
			flex-wrap: wrap;
		}
		.card_right {
			color: #151515;
			float: right !important;
			font-size: 10px;
		}
		.card h3 {
			font-size: 17px;
			padding: 0px;
			margin-bottom: 5px;
			margin-top: 0px;
			text-transform: uppercase;
			font-weight: bold;
			color: #7878d9;
		}
		.box {
			border-color: #b6dce8;
			border-style: solid;
			border-width: 1px 1px 2px 1px;
			border-radius: 10px;
			background: linear-gradient(to bottom, #fafdff 0%, #e8f2f9 100%);
			padding: 0px;
			padding-top: 0px;
			overflow: hidden;
			padding-top: 0px;
			position: relative;
			margin: 0px 0px 15px 0px;
			min-height: 330px;
		}
		.box_head {
			padding: 0px 11px;
			margin: 0px;
			border-radius: 10px;
			overflow: hidden;
			padding-top: 0px;
			position: relative;
		}
		.box_img {
			position: relative;
			z-index: 1;
			width: 110%;
			height: 160px;
			margin-top: -8px;
			margin-left: -11px;
			margin-right: -11px;
		}
		.box .description {
			min-height: 100px;
			height: auto;
			display: block;
			line-height: 22px;
			float: left;
			margin: 0px!important;
			font-size: 14px;
			padding: 5px 10px;
			color: var(--default-text-color)!important;
		}
		.button-row {
			margin-left: -4px;
			margin-right: -4px;
		}
		.box_head h3 {
			margin: 0px -11px 8px;
			padding: 0px;
			font-size: 15px;
			color: #fff;
			line-height: 36px;
			/* background: linear-gradient(to bottom, #c2eaf5 0%, #bfe6f1 39%, #add9e5 100%); */
			background: linear-gradient(to bottom, #0d85a8 0%, #35add1 65%, #44c0e5 100%);
			text-align: center;
			text-transform: uppercase;
			font-weight: 600;
			overflow: hidden;
text-overflow: ellipsis;
display: -webkit-box;
-webkit-line-clamp: 1;
-webkit-box-orient: vertical;
		}
		.box_img img {
			width: 100%;
			height: 155px;
			border-bottom: solid 1px #b6dce8;
			border-top: solid 1px #b6dce8;
		}
		.box .read-more {
			margin-bottom: 15px;
			float: right;
			margin-right: 10px;
		}
		a.light_green_button {
			border-radius: 6px;
			color: #fff;
			font-size: 14px;
			font-weight: bolder;
			padding: 10px 10px;
			text-decoration: none;
			background: #8bc42a linear-gradient(to bottom, #8bc42a 0%, #72b222 100%) repeat scroll 0 0;
			border: 1px solid #FEFEFE;
			box-shadow: 0 1px 0 #72b222 inset;
		}
</style>