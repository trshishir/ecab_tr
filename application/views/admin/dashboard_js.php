
<script type="text/javascript">
    // for row 1
    $('body').on('click','.evolution_g1',function(){
        $('.myChart_g1').css('display', 'none');
        $('.pie-chart_g1').css('display', 'none');
        $('.line-chart_g1').css('display', 'block');
    })
    $('body').on('click','.composition_g1',function(){
        $('.myChart_g1').css('display', 'none');
        $('.pie-chart_g1').css('display', 'block');
        $('.line-chart_g1').css('display', 'none');
    })
    $('body').on('click','.default_g1',function(){
        $('.myChart_g1').css('display', 'block');
        $('.pie-chart_g1').css('display', 'none');
        $('.line-chart_g1').css('display', 'none');
    }) 
    $('body').on('click','.evolution_g2',function(){
        $('.myChart_g2').css('display', 'none');
        $('.pie-chart_g2').css('display', 'none');
        $('.line-chart_g2').css('display', 'block');
    })
     $('body').on('click','.composition_g2',function(){
        $('.myChart_g2').css('display', 'none');
        $('.pie-chart_g2').css('display', 'block');
        $('.line-chart_g2').css('display', 'none');
    })
    $('body').on('click','.default_g2',function(){
        $('.myChart_g2').css('display', 'block');
        $('.pie-chart_g2').css('display', 'none');
        $('.line-chart_g2').css('display', 'none');
    })
    $('body').on('click','.evolution_g3',function(){
        $('.myChart_g3').css('display', 'none');
        $('.pie-chart_g3').css('display', 'none');
        $('.line-chart_g3').css('display', 'block');
    })
    $('body').on('click','.composition_g3',function(){
        $('.myChart_g3').css('display', 'none');
        $('.pie-chart_g3').css('display', 'block');
        $('.line-chart_g3').css('display', 'none');
    })
    $('body').on('click','.default_g3',function(){
        $('.myChart_g3').css('display', 'block');
        $('.pie-chart_g3').css('display', 'none');
        $('.line-chart_g3').css('display', 'none');
    })
    $('body').on('click','.evolution_g4',function(){
        $('.myChart_g4').css('display', 'none');
        $('.pie-chart_g4').css('display', 'none');
        $('.line-chart_g4').css('display', 'block');
    })
    $('body').on('click','.composition_g4',function(){
        $('.myChart_g4').css('display', 'none');
        $('.pie-chart_g4').css('display', 'block');
        $('.line-chart_g4').css('display', 'none');
    })
    $('body').on('click','.default_g4',function(){
        $('.myChart_g4').css('display', 'block');
        $('.pie-chart_g4').css('display', 'none');
        $('.line-chart_g4').css('display', 'none');
    })

    // for row 2
    $('body').on('click','.evolution_g1_row2',function(){
        $('.myChart_g1_row2').css('display', 'none');
        $('.pie-chart_g1_row2').css('display', 'none');
        $('.line-chart_g1_row2').css('display', 'block');
    })
    $('body').on('click','.composition_g1_row2',function(){
        $('.myChart_g1_row2').css('display', 'none');
        $('.pie-chart_g1_row2').css('display', 'block');
        $('.line-chart_g1_row2').css('display', 'none');
    })
    $('body').on('click','.default_g1_row2',function(){
        $('.myChart_g1_row2').css('display', 'block');
        $('.pie-chart_g1_row2').css('display', 'none');
        $('.line-chart_g1_row2').css('display', 'none');
    }) 
    $('body').on('click','.evolution_g2_row2',function(){
        $('.myChart_g2_row2').css('display', 'none');
        $('.pie-chart_g2_row2').css('display', 'none');
        $('.line-chart_g2_row2').css('display', 'block');
    })
     $('body').on('click','.composition_g2_row2',function(){
        $('.myChart_g2_row2').css('display', 'none');
        $('.pie-chart_g2_row2').css('display', 'block');
        $('.line-chart_g2_row2').css('display', 'none');
    })
    $('body').on('click','.default_g2_row2',function(){
        $('.myChart_g2_row2').css('display', 'block');
        $('.pie-chart_g2_row2').css('display', 'none');
        $('.line-chart_g2_row2').css('display', 'none');
    })
    $('body').on('click','.evolution_g3_row2',function(){
        $('.myChart_g3_row2').css('display', 'none');
        $('.pie-chart_g3_row2').css('display', 'none');
        $('.line-chart_g3_row2').css('display', 'block');
    })
    $('body').on('click','.composition_g3_row2',function(){
        $('.myChart_g3_row2').css('display', 'none');
        $('.pie-chart_g3_row2').css('display', 'block');
        $('.line-chart_g3_row2').css('display', 'none');
    })
    $('body').on('click','.default_g3_row2',function(){
        $('.myChart_g3_row2').css('display', 'block');
        $('.pie-chart_g3_row2').css('display', 'none');
        $('.line-chart_g3_row2').css('display', 'none');
    })
    $('body').on('click','.evolution_g4_row2',function(){
        $('.myChart_g4_row2').css('display', 'none');
        $('.pie-chart_g4_row2').css('display', 'none');
        $('.line-chart_g4_row2').css('display', 'block');
    })
    $('body').on('click','.composition_g4_row2',function(){
        $('.myChart_g4_row2').css('display', 'none');
        $('.pie-chart_g4_row2').css('display', 'block');
        $('.line-chart_g4_row2').css('display', 'none');
    })
    $('body').on('click','.default_g4_row2',function(){
        $('.myChart_g4_row2').css('display', 'block');
        $('.pie-chart_g4_row2').css('display', 'none');
        $('.line-chart_g4_row2').css('display', 'none');
    })
    // for row 3
    $('body').on('click','.evolution_g1_row3',function(){
        $('.myChart_g1_row3').css('display', 'none');
        $('.pie-chart_g1_row3').css('display', 'none');
        $('.line-chart_g1_row3').css('display', 'block');
    })
    $('body').on('click','.composition_g1_row3',function(){
        $('.myChart_g1_row3').css('display', 'none');
        $('.pie-chart_g1_row3').css('display', 'block');
        $('.line-chart_g1_row3').css('display', 'none');
    })
    $('body').on('click','.default_g1_row3',function(){
        $('.myChart_g1_row3').css('display', 'block');
        $('.pie-chart_g1_row3').css('display', 'none');
        $('.line-chart_g1_row3').css('display', 'none');
    }) 
    $('body').on('click','.evolution_g2_row3',function(){
        $('.myChart_g2_row3').css('display', 'none');
        $('.pie-chart_g2_row3').css('display', 'none');
        $('.line-chart_g2_row3').css('display', 'block');
    })
     $('body').on('click','.composition_g2_row3',function(){
        $('.myChart_g2_row3').css('display', 'none');
        $('.pie-chart_g2_row3').css('display', 'block');
        $('.line-chart_g2_row3').css('display', 'none');
    })
    $('body').on('click','.default_g2_row3',function(){
        $('.myChart_g2_row3').css('display', 'block');
        $('.pie-chart_g2_row3').css('display', 'none');
        $('.line-chart_g2_row3').css('display', 'none');
    })
    $('body').on('click','.evolution_g3_row3',function(){
        $('.myChart_g3_row3').css('display', 'none');
        $('.pie-chart_g3_row3').css('display', 'none');
        $('.line-chart_g3_row3').css('display', 'block');
    })
    $('body').on('click','.composition_g3_row3',function(){
        $('.myChart_g3_row3').css('display', 'none');
        $('.pie-chart_g3_row3').css('display', 'block');
        $('.line-chart_g3_row3').css('display', 'none');
    })
    $('body').on('click','.default_g3_row3',function(){
        $('.myChart_g3_row3').css('display', 'block');
        $('.pie-chart_g3_row3').css('display', 'none');
        $('.line-chart_g3_row3').css('display', 'none');
    })
    $('body').on('click','.evolution_g4_row3',function(){
        $('.myChart_g4_row3').css('display', 'none');
        $('.pie-chart_g4_row3').css('display', 'none');
        $('.line-chart_g4_row3').css('display', 'block');
    })
    $('body').on('click','.composition_g4_row3',function(){
        $('.myChart_g4_row3').css('display', 'none');
        $('.pie-chart_g4_row3').css('display', 'block');
        $('.line-chart_g4_row3').css('display', 'none');
    })
    $('body').on('click','.default_g4_row3',function(){
        $('.myChart_g4_row3').css('display', 'block');
        $('.pie-chart_g4_row3').css('display', 'none');
        $('.line-chart_g4_row3').css('display', 'none');
    })
    // for row 4
    $('body').on('click','.evolution_g1_row4',function(){
        $('.myChart_g1_row4').css('display', 'none');
        $('.pie-chart_g1_row4').css('display', 'none');
        $('.line-chart_g1_row4').css('display', 'block');
    })
    $('body').on('click','.composition_g1_row4',function(){
        $('.myChart_g1_row4').css('display', 'none');
        $('.pie-chart_g1_row4').css('display', 'block');
        $('.line-chart_g1_row4').css('display', 'none');
    })
    $('body').on('click','.default_g1_row4',function(){
        $('.myChart_g1_row4').css('display', 'block');
        $('.pie-chart_g1_row4').css('display', 'none');
        $('.line-chart_g1_row4').css('display', 'none');
    }) 
    $('body').on('click','.evolution_g2_row4',function(){
        $('.myChart_g2_row4').css('display', 'none');
        $('.pie-chart_g2_row4').css('display', 'none');
        $('.line-chart_g2_row4').css('display', 'block');
    })
     $('body').on('click','.composition_g2_row4',function(){
        $('.myChart_g2_row4').css('display', 'none');
        $('.pie-chart_g2_row4').css('display', 'block');
        $('.line-chart_g2_row4').css('display', 'none');
    })
    $('body').on('click','.default_g2_row4',function(){
        $('.myChart_g2_row4').css('display', 'block');
        $('.pie-chart_g2_row4').css('display', 'none');
        $('.line-chart_g2_row4').css('display', 'none');
    })
    $('body').on('click','.evolution_g3_row4',function(){
        $('.myChart_g3_row4').css('display', 'none');
        $('.pie-chart_g3_row4').css('display', 'none');
        $('.line-chart_g3_row4').css('display', 'block');
    })
    $('body').on('click','.composition_g3_row4',function(){
        $('.myChart_g3_row4').css('display', 'none');
        $('.pie-chart_g3_row4').css('display', 'block');
        $('.line-chart_g3_row4').css('display', 'none');
    })
    $('body').on('click','.default_g3_row4',function(){
        $('.myChart_g3_row4').css('display', 'block');
        $('.pie-chart_g3_row4').css('display', 'none');
        $('.line-chart_g3_row4').css('display', 'none');
    })
    $('body').on('click','.evolution_g4_row4',function(){
        $('.myChart_g4_row4').css('display', 'none');
        $('.pie-chart_g4_row4').css('display', 'none');
        $('.line-chart_g4_row4').css('display', 'block');
    })
    $('body').on('click','.composition_g4_row4',function(){
        $('.myChart_g4_row4').css('display', 'none');
        $('.pie-chart_g4_row4').css('display', 'block');
        $('.line-chart_g4_row4').css('display', 'none');
    })
    $('body').on('click','.default_g4_row4',function(){
        $('.myChart_g4_row4').css('display', 'block');
        $('.pie-chart_g4_row4').css('display', 'none');
        $('.line-chart_g4_row4').css('display', 'none');
    })
    /// for row 5
    $('body').on('click','.evolution_g1_row5',function(){
        $('.myChart_g1_row5').css('display', 'none');
        $('.pie-chart_g1_row5').css('display', 'none');
        $('.line-chart_g1_row5').css('display', 'block');
    })
    $('body').on('click','.composition_g1_row5',function(){
        $('.myChart_g1_row5').css('display', 'none');
        $('.pie-chart_g1_row5').css('display', 'block');
        $('.line-chart_g1_row5').css('display', 'none');
    })
    $('body').on('click','.default_g1_row5',function(){
        $('.myChart_g1_row5').css('display', 'block');
        $('.pie-chart_g1_row5').css('display', 'none');
        $('.line-chart_g1_row5').css('display', 'none');
    }) 
    $('body').on('click','.evolution_g2_row5',function(){
        $('.myChart_g2_row5').css('display', 'none');
        $('.pie-chart_g2_row5').css('display', 'none');
        $('.line-chart_g2_row5').css('display', 'block');
    })
     $('body').on('click','.composition_g2_row5',function(){
        $('.myChart_g2_row5').css('display', 'none');
        $('.pie-chart_g2_row5').css('display', 'block');
        $('.line-chart_g2_row5').css('display', 'none');
    })
    $('body').on('click','.default_g2_row5',function(){
        $('.myChart_g2_row5').css('display', 'block');
        $('.pie-chart_g2_row5').css('display', 'none');
        $('.line-chart_g2_row5').css('display', 'none');
    })
    $('body').on('click','.evolution_g3_row5',function(){
        $('.myChart_g3_row5').css('display', 'none');
        $('.pie-chart_g3_row5').css('display', 'none');
        $('.line-chart_g3_row5').css('display', 'block');
    })
    $('body').on('click','.composition_g3_row5',function(){
        $('.myChart_g3_row5').css('display', 'none');
        $('.pie-chart_g3_row5').css('display', 'block');
        $('.line-chart_g3_row5').css('display', 'none');
    })
    $('body').on('click','.default_g3_row5',function(){
        $('.myChart_g3_row5').css('display', 'block');
        $('.pie-chart_g3_row5').css('display', 'none');
        $('.line-chart_g3_row5').css('display', 'none');
    })
    $('body').on('click','.evolution_g4_row5',function(){
        $('.myChart_g4_row5').css('display', 'none');
        $('.pie-chart_g4_row5').css('display', 'none');
        $('.line-chart_g4_row5').css('display', 'block');
    })
    $('body').on('click','.composition_g4_row5',function(){
        $('.myChart_g4_row5').css('display', 'none');
        $('.pie-chart_g4_row5').css('display', 'block');
        $('.line-chart_g4_row5').css('display', 'none');
    })
    $('body').on('click','.default_g4_row5',function(){
        $('.myChart_g4_row5').css('display', 'block');
        $('.pie-chart_g4_row5').css('display', 'none');
        $('.line-chart_g4_row5').css('display', 'none');
    })
    // for row 6
    $('body').on('click','.evolution_g1_row6',function(){
        $('.myChart_g1_row6').css('display', 'none');
        $('.pie-chart_g1_row6').css('display', 'none');
        $('.line-chart_g1_row6').css('display', 'block');
    })
    $('body').on('click','.composition_g1_row6',function(){
        $('.myChart_g1_row6').css('display', 'none');
        $('.pie-chart_g1_row6').css('display', 'block');
        $('.line-chart_g1_row6').css('display', 'none');
    })
    $('body').on('click','.default_g1_row6',function(){
        $('.myChart_g1_row6').css('display', 'block');
        $('.pie-chart_g1_row6').css('display', 'none');
        $('.line-chart_g1_row6').css('display', 'none');
    }) 
    $('body').on('click','.evolution_g2_row6',function(){
        $('.myChart_g2_row6').css('display', 'none');
        $('.pie-chart_g2_row6').css('display', 'none');
        $('.line-chart_g2_row6').css('display', 'block');
    })
     $('body').on('click','.composition_g2_row6',function(){
        $('.myChart_g2_row6').css('display', 'none');
        $('.pie-chart_g2_row6').css('display', 'block');
        $('.line-chart_g2_row6').css('display', 'none');
    })
    $('body').on('click','.default_g2_row6',function(){
        $('.myChart_g2_row6').css('display', 'block');
        $('.pie-chart_g2_row6').css('display', 'none');
        $('.line-chart_g2_row6').css('display', 'none');
    })
    $('body').on('click','.evolution_g3_row6',function(){
        $('.myChart_g3_row6').css('display', 'none');
        $('.pie-chart_g3_row6').css('display', 'none');
        $('.line-chart_g3_row6').css('display', 'block');
    })
    $('body').on('click','.composition_g3_row6',function(){
        $('.myChart_g3_row6').css('display', 'none');
        $('.pie-chart_g3_row6').css('display', 'block');
        $('.line-chart_g3_row6').css('display', 'none');
    })
    $('body').on('click','.default_g3_row6',function(){
        $('.myChart_g3_row6').css('display', 'block');
        $('.pie-chart_g3_row6').css('display', 'none');
        $('.line-chart_g3_row6').css('display', 'none');
    })
    $('body').on('click','.evolution_g4_row6',function(){
        $('.myChart_g4_row6').css('display', 'none');
        $('.pie-chart_g4_row6').css('display', 'none');
        $('.line-chart_g4_row6').css('display', 'block');
    })
    $('body').on('click','.composition_g4_row6',function(){
        $('.myChart_g4_row6').css('display', 'none');
        $('.pie-chart_g4_row6').css('display', 'block');
        $('.line-chart_g4_row6').css('display', 'none');
    })
    $('body').on('click','.default_g4_row6',function(){
        $('.myChart_g4_row6').css('display', 'block');
        $('.pie-chart_g4_row6').css('display', 'none');
        $('.line-chart_g4_row6').css('display', 'none');
    })

    // for row 7
    $('body').on('click','.evolution_g1_row7',function(){
        $('.myChart_g1_row7').css('display', 'none');
        $('.pie-chart_g1_row7').css('display', 'none');
        $('.line-chart_g1_row7').css('display', 'block');
    })
    $('body').on('click','.composition_g1_row7',function(){
        $('.myChart_g1_row7').css('display', 'none');
        $('.pie-chart_g1_row7').css('display', 'block');
        $('.line-chart_g1_row7').css('display', 'none');
    })
    $('body').on('click','.default_g1_row7',function(){
        $('.myChart_g1_row7').css('display', 'block');
        $('.pie-chart_g1_row7').css('display', 'none');
        $('.line-chart_g1_row7').css('display', 'none');
    }) 
    $('body').on('click','.evolution_g2_row7',function(){
        $('.myChart_g2_row7').css('display', 'none');
        $('.pie-chart_g2_row7').css('display', 'none');
        $('.line-chart_g2_row7').css('display', 'block');
    })
     $('body').on('click','.composition_g2_row7',function(){
        $('.myChart_g2_row7').css('display', 'none');
        $('.pie-chart_g2_row7').css('display', 'block');
        $('.line-chart_g2_row7').css('display', 'none');
    })
    $('body').on('click','.default_g2_row7',function(){
        $('.myChart_g2_row7').css('display', 'block');
        $('.pie-chart_g2_row7').css('display', 'none');
        $('.line-chart_g2_row7').css('display', 'none');
    })
    $('body').on('click','.evolution_g3_row7',function(){
        $('.myChart_g3_row7').css('display', 'none');
        $('.pie-chart_g3_row7').css('display', 'none');
        $('.line-chart_g3_row7').css('display', 'block');
    })
    $('body').on('click','.composition_g3_row7',function(){
        $('.myChart_g3_row7').css('display', 'none');
        $('.pie-chart_g3_row7').css('display', 'block');
        $('.line-chart_g3_row7').css('display', 'none');
    })
    $('body').on('click','.default_g3_row7',function(){
        $('.myChart_g3_row7').css('display', 'block');
        $('.pie-chart_g3_row7').css('display', 'none');
        $('.line-chart_g3_row7').css('display', 'none');
    })
    $('body').on('click','.evolution_g4_row7',function(){
        $('.myChart_g4_row7').css('display', 'none');
        $('.pie-chart_g4_row7').css('display', 'none');
        $('.line-chart_g4_row7').css('display', 'block');
    })
    $('body').on('click','.composition_g4_row7',function(){
        $('.myChart_g4_row7').css('display', 'none');
        $('.pie-chart_g4_row7').css('display', 'block');
        $('.line-chart_g4_row7').css('display', 'none');
    })
    $('body').on('click','.default_g4_row7',function(){
        $('.myChart_g4_row7').css('display', 'block');
        $('.pie-chart_g4_row7').css('display', 'none');
        $('.line-chart_g4_row7').css('display', 'none');
    })
    $(function () {
       var d = new Date();
       var currMonth = d.getMonth();
       var currYear = d.getFullYear();
       var startDate = new Date(currYear, currMonth, 1);
       $(".hasDatepicker_from_ds").datepicker(
        );
       $(".hasDatepicker_from_ds").datepicker(
        "setDate", startDate
        );
    });
    $('.hasDatepicker_to_ds').datepicker({
        format: "dd/mm/yyyy"
    }).datepicker('setDate', 'today');
        
</script>