<?php
$locale_info = localeconv(); ?>

<style type="text/css">
    label{font-weight: 600; font-size: 13px;}
    .custom_style_row{margin-top: 30px; margin-bottom: 30px;}
    input[type=submit], button[type=submit]{
        color: #333;
        cursor: pointer;
        height: 34px;
        font-size: 16px;
        padding: 1px 28px 3px;
        border-radius: 5px;
        text-transform: none;
        font-family: inherit;
        font-weight: 500;
    }
    .body-border{
        min-height: 500px;
    }
    
</style>

<div class="col-md-12" style="height: calc(100vh - 140px);"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12">
                <?php $this->load->view('admin/common/alert');?>
                <div class="module">
                    <?php
                    echo $this->session->flashdata('message');
                    $validation_erros = $this->session->flashdata('validation_errors');
                    if(!empty($validation_erros)){
                        ?>
                        <div class="form-validation-errors alert alert-danger">
                            <h3 style="font-size: 20px; text-align: center;">Validation Errors</h3>
                            <?php echo $validation_erros; ?>
                        </div>
                    <?php } ?>
                    <div class="module-head">
                    </div>
                    <div class="module-body">
                        <?=form_open_multipart("admin/themes/store_theme")?>
                        <div class="col-xs-1">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Statut</label>
                                <select class="form-control" id="status" name="status" required>
                                    <option value="show" >Show</option>
                                    <option value="hide">Hide</option>
                                    <option value="published" >Published</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Theme Name</label>
                                <input type="text" class="form-control" id="theme_name" name="theme_name" aria-describedby="languageHelp" placeholder="Enter Theme Name" required>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Area</label>
                                <select class="form-control" id="area" name="area" required>
                                    <option value="front" >Front</option>
                                    <option value="admin" >Admin</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Default</label>
                                <select class="form-control" id="default_status" name="default_status" required>
                                    <option value="no" >No</option>
                                    <option value="yes">Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Allow Change</label>
                                <select class="form-control" id="allow_change" name="allow_change" required>
                                    <option value="no" >No</option>
                                    <option value="yes">Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Upload Theme</label>
                                <input type="file" class="form-control" id="file" name="file" required>
                            </div>
                        </div>
                        <div class="col-xs-12 text-right">
                            <div class="form-group">
                                <a href="<?= base_url('admin/themes'); ?>" class="btn btn-default"><i class="fa fa-times"></i> Cancel</a>
                                <button type="submit" class="btn btn-default"><i class="fa fa-floppy-o"></i>Save</button>
                            </div>
                        </div>
                        <!-- </form> -->
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div><?php $locale_info = localeconv(); ?>
<style>
    @media only screen and (min-width: 1400px){
        .table-filter input, .table-filter select{
            max-width: 9% !important;
        }
        .table-filter select{
            max-width: 95px !important;
        }
        .table-filter .dpo {
            max-width: 90px !important;
        }
    }
</style>
