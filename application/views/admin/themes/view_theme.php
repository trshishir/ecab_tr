<?php
$locale_info = localeconv(); ?>
<style type="text/css">
    label{font-weight: 600; font-size: 13px;}
    .custom_style_row{margin-top: 30px; margin-bottom: 30px;}
    .custom_disabled{
        background-color: #F4F4F5 !important; 
    }
</style>
<div class="col-md-12" style="height: calc(100vh - 140px);"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12">
                <?php $this->load->view('admin/common/alert');?>
                <div class="module">
                    <?php
                    echo $this->session->flashdata('message');
                    $validation_erros = $this->session->flashdata('validation_errors');
                    if(!empty($validation_erros)){
                        ?>
                        <div class="form-validation-errors alert alert-danger">
                            <h3 style="font-size: 20px; text-align: center;">Validation Errors</h3>
                            <?php echo $validation_erros; ?>
                        </div>
                    <?php } ?>
                    <div class="module-head">
                    </div>
                    <div class="module-body">
                        <?=form_open_multipart("")?>
                        <input type="hidden" name="id" value="<?= ($theme[0]['id'])?>">
                        <div class="col-xs-1">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Statut</label>
                                <select class="form-control custom_disabled" id="status" name="status" disabled="disabled">
                                    <option value="show" <?= ($theme[0]['status'] == 'show' ? 'selected':'') ?>>Show</option>
                                    <option value="hide" <?= ($theme[0]['status'] == 'hide' ? 'selected':'') ?>>Hide</option>
                                    <option value="published" <?= ($theme[0]['status'] == 'published' ? 'selected':'')?>>Published</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Theme Name</label>
                                <input type="text" class="form-control custom_disabled" id="theme_name" name="theme_name" aria-describedby="languageHelp" placeholder="Enter Theme Name" value='<?= ($theme[0]['theme_name'])?>' disabled="disabled">
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Area</label>
                                <select class="form-control custom_disabled" id="area" name="area" disabled="disabled">
                                 <option value="front"<?= ($theme[0]['area'] == 'front' ? 'selected':'') ?>>Front</option>
                                 <option value="admin"<?= ($theme[0]['area'] == 'admin' ? 'selected':'') ?>>Admin</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Default</label>
                                <select class="form-control custom_disabled" id="default_status" name="default_status" disabled="disabled">
                                    <option value="no"<?= ($theme[0]['default_status'] == 'no' ? 'selected':'') ?>>No</option>
                                    <option value="yes"<?= ($theme[0]['default_status'] == 'yes' ? 'selected':'') ?>>Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Allow Change</label>
                                <select class="form-control custom_disabled" id="allow_change" name="allow_change" disabled>
                                    <option value="no"<?= ($theme[0]['allow_change'] == 'no' ? 'selected':'') ?>>No</option>
                                    <option value="yes"<?= ($theme[0]['allow_change'] == 'yes' ? 'selected':'') ?>>Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Upload Theme</label>
                                <input type="file" class="form-control custom_disabled" id="file" name="file" disabled>
                            </div>
                        </div>
                        <div class="col-xs-12 text-right">
                            <div class="form-group">
                                <a href="<?= base_url('admin/themes'); ?>" class="btn btn-default"><i class="fa fa-times"></i>Cancel</a>
                            </div>
                        </div>
                        <!-- </form> -->
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div><?php $locale_info = localeconv(); ?>
<style>
    @media only screen and (min-width: 1400px){
        .table-filter input, .table-filter select{
            max-width: 9% !important;
        }
        .table-filter select{
            max-width: 95px !important;
        }
        .table-filter .dpo {
            max-width: 90px !important;
        }
    }
</style>
