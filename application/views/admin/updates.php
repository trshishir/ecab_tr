<section id="content">
    <div class="listDiv">
        <?php $this->load->view('admin/common/breadcrumbs'); ?>

        <div class="row-fluid">
            <?php
            $flashAlert = $this->session->flashdata('alert');
            if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
                ?>
                <br>
                <div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
                    <strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
                    <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php } ?>
        </div>
        <div class="module">
            <?php echo $this->session->flashdata('message'); ?>
            <div class="clearfix"></div>
            <div class="row">
                <div id="mainbox" class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="panel-group" id="accordion">
                        <?php
                                $data_list = array_reverse($data_list);
                                $count = 1;
                                foreach ($data_list as $item):
                                    // var_dump($item); 
                        ?>

                            <div class="panel panel-default">
                                <div class="panel-heading admin_border">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $count; ?>">UDAPTS OF <?php echo date('d/m/Y', strtotime($item->created_date)); ?><span  class="right">By <?php echo $item->civility . ' ' . $item->first_name . ' ' . $item->last_name; ?></span></a> 
                                    </h4>
                                </div>
                                <div id="collapse<?= $count; ?>" class="panel-collapse collapse <?php echo ($count == 1) ? 'in' : ''; ?>">
                                    <div class="panel-body">

                                        <?php
                                        $text = $item->description;
                                        $text = strip_tags($text);

                                        echo implode(' ', array_slice(explode(' ', $text), 0, 30)) . ' ...';
                                        ?>   
                                    </div>
                                </div>
                            </div>
                        <?php   $count++;
                                endforeach;
                        ?>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    .card {
        /*   border: 1px solid gray;
               border-top-color: gray;
               border-top-style: solid;
               border-top-width: 1px;
           border-top-color: gray;
           border-top-style: solid;
           border-top-width: 1px;*/
        box-shadow: 1px 1px 3px #888;
        /*border-top: 10px solid green;*/
        min-height: 120px;
        padding-: 10px !important;
        margin-right: 1%;
        background: linear-gradient(to bottom, #e8e0e0 0%, #ececec 39%, #fbf4f4 100%, #8a8989 100%);

        /*background: linear-gradient(to bottom, #e8e0e0 0%, #ececec 39%, #fbf4f4 75%, #8a8989 100%);*/
        float: left;
        margin-top: 10px;
    }



    .card > img {
        border-radius: 50%;
        width: 25px;
        margin: 3px 1px 20px 5px;
    }

    .card > h6 {
        font-weight: lighter;
        margin-left: 40px;
        margin-top: -54px;
        border-bottom: groove 3px #f2f2f2;
        margin-right: 5px;
    }

    .card >p{
        margin: 30px 10px;
        font-family: segoe ui;
        line-height: 1.4em;
        font-size: 1.2em;
    }

    #mainbox{
        font-family: calibri;
        box-sizing: border-box;
        justify-content: center;
        /*display: flex;*/
        flex-wrap: wrap;
    }

    .card_right {
        color: #151515;
        float: right !important;
        font-size: 10px;
    }

    .card h3 {
        font-size: 17px;
        padding: 0px;
        margin-bottom: 5px;
        margin-top: 0px;
        text-transform: uppercase;
        font-weight: bold;
    }


</style>
