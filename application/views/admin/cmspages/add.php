<?php $locale_info = localeconv(); ?>
<div class="col-md-12"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12">
                <?php $this->load->view('admin/common/alert');?>
                <div class="module">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="module-head">
                    </div>
                    <div class="module-body">
                        <?=form_open("cmspages/add")?>
						<div class="row">
						<div class="col-xs-8 p-2" style="padding:28px;">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Page Type* </label>
									<select class="form-control" name="page_type" required>
									   <option value="">Select Page Type</option>
									    <?php foreach($page_type as $key => $item):?>
                                        <option value="<?php echo $item->id?>"><?php echo $item->title?></option>
										 <?php endforeach; ?>
                                    </select>
                                    
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Status*</label>
									 <select class="form-control" name="status" required>
                                        <option value="1">Enabled</option>
                                        <option value="2">Disabled</option>
                                    </select>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Language*</label>                
                                        <input id="subject" maxlength="200" type="text" class="form-control" required name="language" placeholder="Language" value="<?=set_value('language',$this->input->post('language'))?>">
                                    <!--</div>-->
                                </div>
                            </div>
							 <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Category of File*</label>
                                        <input id="subject" maxlength="200" type="text" class="form-control" required name="category_of_file" placeholder="Category of File" value="<?=set_value('category_of_file',$this->input->post('category_of_file'))?>">
                                    <!--</div>-->
                                </div>
                            </div>
							<div class="col-xs-4">
                                <div class="form-group">
                                    <label>File Name*</label>
                                        <input id="subject" maxlength="200" type="text" class="form-control" required name="file_name" placeholder="File Name" value="<?=set_value('file_name',$this->input->post('file_name'))?>">
                                    <!--</div>-->
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>Message*</label>
                                    <textarea class="ckeditor" id="editor1" name="message" cols="100" rows="10"><?= set_value('message', $this->input->post('message')) ?></textarea>
                                </div>
                            </div>
                        </div>
						 <div class="row">
                            <div class="col-xs-12">
                                <div class="btn-group" style="float:right!important">
                                                <button type="button" class="btn btn-success">
                                                    <i class="fas fa-upload"></i> Upload <i class="fa fa-plus" aria-hidden="true"></i>
                                                </button>
                                                
                                            </div>
                            </div>
                        </div>
                        </div>
						<div class="col-xs-4 p-2"  style="padding:28px;">
						<div class="row">
						<div class="col-xs-offset-2  col-xs-8 col-sm-offset-2">
                                <div class="form-group"> 
                                    <label>Link Extension*</label>
                                        <input id="subject" maxlength="200" type="text" class="form-control" required name="link_extension" placeholder="Link Extension (.php/.html)" value="<?=set_value('link_extension',$this->input->post('link_extension'))?>">
                                    <!--</div>-->
                                </div>
                            </div>
                        </div>
						<div class="row">
						<div class="col-xs-offset-2  col-xs-8 col-sm-offset-2">
                                <div class="form-group"> 
                                    <label>Meta Tags*</label>
                                       <textarea name="meta_tags" cols="100" rows="10" placeholder="Meta Tags"><?= set_value('meta_tags', $this->input->post('meta_tags')) ?></textarea>
                                    <!--</div>-->
                                </div>
                            </div>
                        </div>
						<div class="row">
						<div class="col-xs-offset-2  col-xs-8 col-sm-offset-2">
                                <div class="form-group"> 
                                    <label>Meta Description*</label>
                                        <textarea name="meta_description" cols="100" rows="10" placeholder="Meta Description"><?= set_value('meta_description', $this->input->post('meta_description')) ?></textarea>
                                </div>
                            </div>
                        </div>
						<div class="row">
						<div class="col-xs-offset-2  col-xs-8 col-sm-offset-2">
						<div class="text-right" style="position: relative;bottom: -58px;">
                            <button class="btn btn-default">Save</button>
                            <a href="<?=base_url("cmspages/index")?>" class="btn btn-default">Cancel</a>
                        </div>
						</div>
						</div>
						</div>
						</div>
						
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $(".bdatepicker").datepicker({
            format: "dd/mm/yyyy"
        });
    });
</script>