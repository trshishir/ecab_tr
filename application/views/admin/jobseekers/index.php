<section id="content">
    <div class="listDiv">
        <?php $this->load->view('admin/common/breadcrumbs'); ?>
        <div class="row-fluid">
            <?php   $flashAlert = $this->session->flashdata('alert');
                    if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
            ?>
                <br>
                <div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
                    <strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
                    <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php   } ?>

            <div class="module">
                <?php echo $this->session->flashdata('message'); ?>
                <div class="module-head">                </div>
                <div class="clearfix"></div>
                <div class="module-body table-responsive">
                    <table id="example" class="table table-bordered table-striped  table-hover dataTable" cellspacing="0" width="100%" data-selected_id="">
                        <thead>
                            <tr>
                                <th class="no-sort text-center"><input type='checkbox' name='allJobseekers' id="allJobseekers" value='' onclick="Javascript:selectAllJobseekers();" /></th>
                                <th class="column-id"><?php echo $this->lang->line('id'); ?></th>
                                <th class="column-date"><?php echo "Date"; ?></th>
                                <th class="column-date"><?php echo "Time"; ?></th>
                                <th class="column-name" ><?php echo $this->lang->line('name'); ?></th>
                                <th class="column-email"><?php echo $this->lang->line('email'); ?></th>
                                <th class="column-phone"><?php echo $this->lang->line('phone'); ?></th>
                                <th class="column-status"><?php echo $this->lang->line('status'); ?></th>
                                <th class="column-since"><?php echo "Since"; ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($data) && !empty($data)): ?>
                                <?php foreach ($data as $key => $item): ?>
                                    <?php ?>
                                    <tr>
                                        <td align="center"><input type='checkbox' name='partners[]' class='ccheckbox' value="<?php echo $item['id']; ?>" /></td>
                                        <td><?php echo $item['link']; ?></td>
                                        <td><?php echo $item['date']; ?></td>
                                        <td><?php echo $item['time']; ?></td>
                                        <td><?php echo $item['name']; ?></td>
                                        <td><?php echo $item['email']; ?></td>
                                        <td><?php echo $item['phone']; ?></td>
                                        <td><?php echo $item['status']; ?></td>
                                        <td><?php echo $item['since']; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="table-filter" class="hide">
    <?php echo form_open('admin/jobseekers', array("id" => "search_filter")); ?>
    <input type="text" placeholder="Name" class="form-control" name="search_name" id="search_name">
    <input type="text" placeholder="Email" class="form-control" name="search_email" id="search_email" >
    <input type="text" placeholder="Phone" name="search_phone" id="search_phone" class="form-control">
    <input type="text" placeholder="From" name="date_from" id="date_from" class="dpo">
    <input type="text" placeholder="To" name="date_to" id="date_to" class="dpo">
    <select class="form-control" name="status" id="status">
        <option value="">All Status</option>
        <?php foreach(config_model::$status as $key => $status):?>
            <option <?=set_value('status',$this->input->post('status')) == $status ? "selected" : ""?> value="<?=$status?>"><?=$status?></option>
        <?php endforeach;?>
    </select>
    <input type="submit" name="search" id="search" value="Search" class="btn btn-sm btn-default" />
    <?php echo form_close(); ?>
</div>
 <script>
    $(document).ready(function() {
        // Search form validation rules
        $("#search_filter").validate({
            rules: {
                search_name: { required:false }
            },
            messages: {
                search_name: {
                    required:"Please enter the name"
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>