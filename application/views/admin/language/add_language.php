<?php
$locale_info = localeconv(); ?>
<script type="text/javascript">
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
    }
</script>

<style type="text/css">
    label{font-weight: 600; font-size: 13px;}
    .custom_style_row{margin-top: 30px; margin-bottom: 30px;}
    input[type=submit], button[type=submit]{
        color: #333;
        cursor: pointer;
        height: 34px;
        font-size: 16px;
        padding: 1px 28px 3px;
        border-radius: 5px;
        text-transform: none;
        font-family: inherit;
        font-weight: 500;
    }
    .body-border{
        min-height: 500px;
    }
</style>

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>


<div class="col-md-12" style="height: calc(100vh - 140px);"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12">
                <?php $this->load->view('admin/common/alert');?>
                <div class="module">
                    <?php
                    echo $this->session->flashdata('message');
                    $validation_erros = $this->session->flashdata('validation_errors');
                    if(!empty($validation_erros)){
                        ?>
                        <div class="form-validation-errors alert alert-danger">
                            <h3 style="font-size: 20px; text-align: center;">Validation Errors</h3>
                            <?php echo $validation_erros; ?>
                        </div>
                    <?php } ?>
                    <div class="module-head">
                    </div>
                    <div class="module-body">
                        <!--  <form method="post" action="<?= base_url('language/store_language'); ?>" enctype="multipart/form-data" accept-charset="utf-8" > -->
                        <?=form_open_multipart("language/store_language")?>

                        <div class="col-xs-1">
                            <div class="form-group">
                                <label for="status">Statut</label>
                                <select class="form-control" id="status" name="status">
                                    <option value="1">Show</option>
                                    <option value="0">Hide</option>
                                    <option value="2">Default</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-1">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Short Code</label>
                                <input type="text" class="form-control" id="Short_code" name="Short_code" aria-describedby="languageHelp" placeholder="Enter Short Code">
                                <!--                              <small id="languageHelp" class="form-text text-muted">Put the Short Code e.g.( en,fr,sp ) hare</small>-->
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1" >Language Name</label>
                                <select data-placeholder="Choose a Language..." name="language" class="form-control">
                                  <option value="english">English</option>
                                  <option value="afrikaans">Afrikaans</option>
                                  <option value="albanian">Albanian</option>
                                  <option value="arabic">Arabic</option>
                                  <option value="armenian">Armenian</option>
                                  <option value="basque">Basque</option>
                                  <option value="bengali">Bengali</option>
                                  <option value="bulgarian">Bulgarian</option>
                                  <option value="batalan">Catalan</option>
                                  <option value="cambodian">Cambodian</option>
                                  <option value="chinese">Chinese (Mandarin)</option>
                                  <option value="croatian">Croatian</option>
                                  <option value="czech">Czech</option>
                                  <option value="danish">Danish</option>
                                  <option value="dutch">Dutch</option>
                                  <option value="estonian">Estonian</option>
                                  <option value="fiji">Fiji</option>
                                  <option value="finnish">Finnish</option>
                                  <option value="french">French</option>
                                  <option value="georgian">Georgian</option>
                                  <option value="german">German</option>
                                  <option value="greek">Greek</option>
                                  <option value="gujarati">Gujarati</option>
                                  <option value="hebrew">Hebrew</option>
                                  <option value="hindi">Hindi</option>
                                  <option value="hungarian">Hungarian</option>
                                  <option value="icelandic">Icelandic</option>
                                  <option value="indonesian">Indonesian</option>
                                  <option value="irish">Irish</option>
                                  <option value="italian">Italian</option>
                                  <option value="japanese">Japanese</option>
                                  <option value="javanese">Javanese</option>
                                  <option value="korean">Korean</option>
                                  <option value="latin">Latin</option>
                                  <option value="latvian">Latvian</option>
                                  <option value="lithuanian">Lithuanian</option>
                                  <option value="macedonian">Macedonian</option>
                                  <option value="malay">Malay</option>
                                  <option value="malayalam">Malayalam</option>
                                  <option value="maltese">Maltese</option>
                                  <option value="maori">Maori</option>
                                  <option value="marathi">Marathi</option>
                                  <option value="mongolian">Mongolian</option>
                                  <option value="nepali">Nepali</option>
                                  <option value="norwegian">Norwegian</option>
                                  <option value="persian">Persian</option>
                                  <option value="polish">Polish</option>
                                  <option value="portuguese">Portuguese</option>
                                  <option value="punjabi">Punjabi</option>
                                  <option value="quechua">Quechua</option>
                                  <option value="romanian">Romanian</option>
                                  <option value="russian">Russian</option>
                                  <option value="samoan">Samoan</option>
                                  <option value="serbian">Serbian</option>
                                  <option value="slovak">Slovak</option>
                                  <option value="slovenian">Slovenian</option>
                                  <option value="spanish">Spanish</option>
                                  <option value="swahili">Swahili</option>
                                  <option value="swedish">Swedish </option>
                                  <option value="tamil">Tamil</option>
                                  <option value="tatar">Tatar</option>
                                  <option value="telugu">Telugu</option>
                                  <option value="thai">Thai</option>
                                  <option value="tibetan">Tibetan</option>
                                  <option value="tonga">Tonga</option>
                                  <option value="turkish">Turkish</option>
                                  <option value="ukrainian">Ukrainian</option>
                                  <option value="urdu" >Urdu</option>
                                  <option value="uzbek">Uzbek</option>
                                  <option value="vietnamese">Vietnamese</option>
                                  <option value="welsh">Welsh</option>
                                  <option value="xhosa">Xhosa</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-1">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Default</label>
                                <select class="form-control" id="defult" name="defult">
                                    <option value="no">No</option>
                                    <option value="yes">Yes</option>
                                </select>
                            </div>
                        </div><div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Allow change in front area</label>
                                <select class="form-control" id="fontchange" name="fontchange">
                                    <option value="no" >No</option>
                                    <option value="yes" >Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Allow change in Admin area</label>
                                <select class="form-control" id="adchange" name="adchange">
                                    <option value="no">No</option>
                                    <option value="yes" >Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Allow change in areas</label>
                                <select class="form-control" id="allow_areas_change" name="allow_areas_change">
                                    <option value="no">No</option>
                                    <option value="yes" >Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-1">
                            <div class="form-group">
                                <label for="flag">Flag png</label>
                                <input type="file" class="form-control-file" id="flag" name="flag">
                                <small id="languageHelp" class="form-text text-muted">Only put PNG flag Icon</small>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="text-right">
                                <a href="<?= base_url('admin/language'); ?>" class="btn btn-default"><i class="fa fa-times"></i> Cancel</a>
                                <button type="submit" class="btn btn-default"><i class="fa fa-floppy-o"></i> Save</button>
                            </div>
                        </div>
                        <!-- </form> -->
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div><?php $locale_info = localeconv(); ?>
<style>
    @media only screen and (min-width: 1400px){
        .table-filter input, .table-filter select{
            max-width: 9% !important;
        }
        .table-filter select{
            max-width: 95px !important;
        }
        .table-filter .dpo {
            max-width: 90px !important;
        }
    }
</style>

<script type="text/javascript">
</script>