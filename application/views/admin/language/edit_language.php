<?php 

$locale_info = localeconv(); ?>
<script type="text/javascript">
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
    }
</script>

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>


<style type="text/css">
    label{font-weight: 600; font-size: 13px;}
    .custom_style_row{margin-top: 30px; margin-bottom: 30px;}
    input[type=submit], button[type=submit]{
    color: #333;
    cursor: pointer;
    height: 34px;
    font-size: 16px;
    padding: 1px 28px 3px;
    border-radius: 5px;
    text-transform: none;
    font-family: inherit;
    font-weight: 500;
    }
    .body-border{
      min-height: 500px;
    }
</style>
<div class="col-md-12" style="height: calc(100vh - 140px);"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12">
                <?php $this->load->view('admin/common/alert');?>
                <div class="module">
                    <?php
                        echo $this->session->flashdata('message');
                        $validation_erros = $this->session->flashdata('validation_errors');
                        if(!empty($validation_erros)){

                    ?>
                        <div class="form-validation-errors alert alert-danger">
                            <h3 style="font-size: 20px; text-align: center;">Validation Errors</h3>
                            <?php echo $validation_erros; ?>
                        </div>
                    <?php } ?>
                    <div class="module-head">
                    </div>
                    <div class="module-body">
                       <!--  <form method="post" action="<?= base_url('language/store_language'); ?>" enctype="multipart/form-data" accept-charset="utf-8" > -->
                          <?=form_open_multipart("language/update_language/".$language['id'])?>
                        <div class="col-xs-1">
                            <div class="form-group">
                                <label for="status">Statut</label>
                                <select class="form-control" id="status" name="status">
                                    <option value="1" <?= ($language['status'] == 1 ? 'selected':'') ?>>Show</option>
                                    <option value="0" <?= ($language['status'] == 0 ? 'selected':'') ?>>Hide</option>
                                    <option value="2" <?= ($language['status'] == 2 ? 'selected':'') ?>>Default</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-1">
                            <div class="form-group">
                              <label for="exampleInputEmail1">Short Code</label>
                              <input type="text" class="form-control" id="short_code" name="short_code" aria-describedby="languageHelp" placeholder="Enter Short Code" value="<?= @$language['short_code'];?>">
                              <!-- <small id="languageHelp" class="form-text text-muted">Put the Short Code e.g.( en,fr,sp ) </small> -->
                            </div>
                          </div>
                          <div class="col-xs-2">
                            <div class="form-group">
                              <label for="exampleInputEmail1">Language Name</label>
                                <select data-placeholder="Choose a Language..." name="language" class="form-control">
                                  <option value="afrikaans" <?= ($language['title'] == 'afrikaans' ? 'selected':'') ?>>Afrikaans</option>
                                  <option value="albanian"<?= ($language['title'] == 'albanian' ? 'selected':'') ?>>Albanian</option>
                                  <option value="arabic"<?= ($language['title'] == 'arabic' ? 'selected':'') ?>>Arabic</option>
                                  <option value="armenian" <?= ($language['title'] == 'armenian' ? 'selected':'') ?>>Armenian</option>
                                  <option value="basque" <?= ($language['title'] == 'basque' ? 'selected':'') ?>>Basque</option>
                                  <option value="bengali" <?= ($language['title'] == 'bengali' ? 'selected':'') ?>>Bengali</option>
                                  <option value="bulgarian" <?= ($language['title'] == 'bulgarian' ? 'selected':'') ?>>Bulgarian</option>
                                  <option value="batalan" <?= ($language['title'] == 'batalan' ? 'selected':'') ?>>Catalan</option>
                                  <option value="cambodian" <?= ($language['title'] == 'cambodian' ? 'selected':'') ?>>Cambodian</option>
                                  <option value="chinese" <?= ($language['title'] == 'chinese' ? 'selected':'') ?>>Chinese (Mandarin)</option>
                                  <option value="croatian" <?= ($language['title'] == 'croatian' ? 'selected':'') ?>>Croatian</option>
                                  <option value="czech" <?= ($language['title'] == 'czech' ? 'selected':'') ?>>Czech</option>
                                  <option value="danish" <?= ($language['title'] == 'danish' ? 'selected':'') ?>>Danish</option>
                                  <option value="dutch" <?= ($language['title'] == 'dutch' ? 'selected':'') ?>>Dutch</option>
                                  <option value="english" <?= ($language['title'] == 'english' ? 'selected':'') ?>>English</option>
                                  <option value="estonian" <?= ($language['title'] == 'estonian' ? 'selected':'') ?>>Estonian</option>
                                  <option value="fiji" <?= ($language['title'] == 'fiji' ? 'selected':'') ?>>Fiji</option>
                                  <option value="finnish" <?= ($language['title'] == 'finnish' ? 'selected':'') ?>>Finnish</option>
                                  <option value="french"<?= ($language['title'] == 'french' ? 'selected':'') ?>>French</option>
                                  <option value="georgian"<?= ($language['title'] == 'georgian' ? 'selected':'') ?>>Georgian</option>
                                  <option value="german"<?= ($language['title'] == 'german' ? 'selected':'') ?>>German</option>
                                  <option value="greek"<?= ($language['title'] == 'greek' ? 'selected':'') ?>>Greek</option>
                                  <option value="gujarati"<?= ($language['title'] == 'gujarati' ? 'selected':'') ?>>Gujarati</option>
                                  <option value="hebrew"<?= ($language['title'] == 'hebrew' ? 'selected':'') ?>>Hebrew</option>
                                  <option value="hindi"<?= ($language['title'] == 'hindi' ? 'selected':'') ?>>Hindi</option>
                                  <option value="hungarian"<?= ($language['title'] == 'hungarian' ? 'selected':'') ?>>Hungarian</option>
                                  <option value="icelandic"<?= ($language['title'] == 'icelandic' ? 'selected':'') ?>>Icelandic</option>
                                  <option value="indonesian"<?= ($language['title'] == 'indonesian' ? 'selected':'') ?>>Indonesian</option>
                                  <option value="irish"<?= ($language['title'] == 'irish' ? 'selected':'') ?>>Irish</option>
                                  <option value="italian"<?= ($language['title'] == 'italian' ? 'selected':'') ?>>Italian</option>
                                  <option value="japanese" <?= ($language['title'] == 'japanese' ? 'selected':'') ?>>Japanese</option>
                                  <option value="javanese" <?= ($language['title'] == 'javanese' ? 'selected':'') ?>>Javanese</option>
                                  <option value="korean" <?= ($language['title'] == 'korean' ? 'selected':'') ?>>Korean</option>
                                  <option value="latin" <?= ($language['title'] == 'latin' ? 'selected':'') ?>>Latin</option>
                                  <option value="latvian" <?= ($language['title'] == 'latvian' ? 'selected':'') ?>>Latvian</option>
                                  <option value="lithuanian" <?= ($language['title'] == 'lithuanian' ? 'selected':'') ?>>Lithuanian</option>
                                  <option value="macedonian" <?= ($language['title'] == 'macedonian' ? 'selected':'') ?>>Macedonian</option>
                                  <option value="malay" <?= ($language['title'] == 'malay' ? 'selected':'') ?>>Malay</option>
                                  <option value="malayalam" <?= ($language['title'] == 'malayalam' ? 'selected':'') ?>>Malayalam</option>
                                  <option value="maltese" <?= ($language['title'] == 'maltese' ? 'selected':'') ?>>Maltese</option>
                                  <option value="maori" <?= ($language['title'] == 'maori' ? 'selected':'') ?>>Maori</option>
                                  <option value="marathi" <?= ($language['title'] == 'marathi' ? 'selected':'') ?>>Marathi</option>
                                  <option value="mongolian" <?= ($language['title'] == 'mongolian' ? 'selected':'') ?>>Mongolian</option>
                                  <option value="nepali" <?= ($language['title'] == 'nepali' ? 'selected':'') ?>>Nepali</option>
                                  <option value="norwegian" <?= ($language['title'] == 'norwegian' ? 'selected':'') ?>>Norwegian</option>
                                  <option value="persian" <?= ($language['title'] == 'persian' ? 'selected':'') ?>>Persian</option>
                                  <option value="polish" <?= ($language['title'] == 'polish' ? 'selected':'') ?>>Polish</option>
                                  <option value="portuguese" <?= ($language['title'] == 'portuguese' ? 'selected':'') ?>>Portuguese</option>
                                  <option value="punjabi" <?= ($language['title'] == 'punjabi' ? 'selected':'') ?>>Punjabi</option>
                                  <option value="quechua" <?= ($language['title'] == 'quechua' ? 'selected':'') ?>>Quechua</option>
                                  <option value="romanian" <?= ($language['title'] == 'romanian' ? 'selected':'') ?>>Romanian</option>
                                  <option value="russian" <?= ($language['title'] == 'russian' ? 'selected':'') ?>>Russian</option>
                                  <option value="samoan" <?= ($language['title'] == 'samoan' ? 'selected':'') ?>>Samoan</option>
                                  <option value="serbian" <?= ($language['title'] == 'serbian' ? 'selected':'') ?>>Serbian</option>
                                  <option value="slovak" <?= ($language['title'] == 'slovak' ? 'selected':'') ?>>Slovak</option>
                                  <option value="slovenian" <?= ($language['title'] == 'slovenian' ? 'selected':'') ?>>Slovenian</option>
                                  <option value="spanish" <?= ($language['title'] == 'spanish' ? 'selected':'') ?>>Spanish</option>
                                  <option value="swahili" <?= ($language['title'] == 'swahili' ? 'selected':'') ?>>Swahili</option>
                                  <option value="swedish" <?= ($language['title'] == 'swedish' ? 'selected':'') ?>>Swedish </option>
                                  <option value="tamil" <?= ($language['title'] == 'tamil' ? 'selected':'') ?>>Tamil</option>
                                  <option value="tatar" <?= ($language['title'] == 'tatar' ? 'selected':'') ?>>Tatar</option>
                                  <option value="telugu" <?= ($language['title'] == 'telugu' ? 'selected':'') ?>>Telugu</option>
                                  <option value="thai" <?= ($language['title'] == 'thai' ? 'selected':'') ?>>Thai</option>
                                  <option value="tibetan" <?= ($language['title'] == 'tibetan' ? 'selected':'') ?>>Tibetan</option>
                                  <option value="tonga" <?= ($language['title'] == 'tonga' ? 'selected':'') ?>>Tonga</option>
                                  <option value="turkish" <?= ($language['title'] == 'turkish' ? 'selected':'') ?>>Turkish</option>
                                  <option value="ukrainian" <?= ($language['title'] == 'ukrainian' ? 'selected':'') ?>>Ukrainian</option>
                                  <option value="urdu" <?= ($language['title'] == 'urdu' ? 'selected':'') ?>>Urdu</option>
                                  <option value="uzbek" <?= ($language['title'] == 'uzbek' ? 'selected':'') ?>>Uzbek</option>
                                  <option value="vietnamese" <?= ($language['title'] == 'vietnamese' ? 'selected':'') ?>>Vietnamese</option>
                                  <option value="welsh" <?= ($language['title'] == 'welsh' ? 'selected':'') ?>>Welsh</option>
                                  <option value="xhosa" <?= ($language['title'] == 'xhosa' ? 'selected':'') ?>>Xhosa</option>
                                </select>
                            </div>
                          </div>
                        <div class="col-xs-1">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Default</label>
                                <select class="form-control" id="defult" name="defult">
                                    <option value="no" <?= ($language['defult'] == 'no' ? 'selected':'') ?>>No</option>
                                    <option value="yes" <?= ($language['defult'] == 'yes' ? 'selected':'') ?>>Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Allow change in front area</label>
                                <select class="form-control" id="fontchange" name="fontchange">
                                    <option value="no" <?= ($language['fontchange'] == 'no' ? 'selected':'') ?>>No</option>
                                    <option value="yes" <?= ($language['fontchange'] == 'yes' ? 'selected':'') ?>>Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Allow change in Admin area</label>
                                <select class="form-control" id="adchange" name="adchange">
                                    <option value="no" <?= ($language['adchange'] == 'no' ? 'selected':'') ?>>No</option>
                                    <option value="yes" <?= ($language['adchange'] == 'yes' ? 'selected':'') ?>>Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Allow change in areas</label>
                                <select class="form-control" id="allow_areas_change" name="allow_areas_change">
                                    <option value="no" <?= ($language['allow_areas_change'] == 'no' ? 'selected':'') ?>>No</option>
                                    <option value="yes" <?= ($language['allow_areas_change'] == 'yes' ? 'selected':'') ?>>Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-1">
                            <div class="form-group">
                                <label for="status">Flag : </label>
                                <div class="form-group">
                                    <input type="file" class="form-control-file" id="flag" name="flag">
                                    <small id="languageHelp" class="form-text text-muted"> Chnage Flag Only put PNG file <img src="<?= base_url('assets/system_design/images/'.$language['flag']); ?>" style="width: 50px;height: 30px;"></small>
                                </div>
                            </div>
                        </div>
                       <!--  <div class="col-xs-8">
                            <div class="form-group">
                                <div></div>
                            </div>
                        </div> -->
                          <div class="col-xs-12">
                            <div class="text-right">
                                <a href="<?= base_url('admin/language'); ?>" class="btn btn-default"><i class="fa fa-times"></i> Cancel</a>
                                <button type="submit" class="btn btn-default"><i class="fa fa-floppy-o"></i> Save</button>
                            </div>
                          </div>
                        <!-- </form> -->
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div><?php $locale_info = localeconv(); ?>
<style>
    @media only screen and (min-width: 1400px){
        .table-filter input, .table-filter select{
            max-width: 9% !important;
        }
        .table-filter select{
            max-width: 95px !important;
        }
        .table-filter .dpo {
            max-width: 90px !important;
        }
    }
</style>

<script type="text/javascript">
</script>