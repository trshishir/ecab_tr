<?php $locale_info = localeconv(); ?>
<style>
   
    .table-filter{
        margin-bottom: -30px !important;   
    }
    .table-filter .dpo {
        max-width: 62px;
    }
    .table-filter span {
        margin: 4px 2px 0 3px;
    }
    .table-filter input[type="number"]{
        max-width: 48px;
    }
    .table-filter input[type="text"]{
        max-width: 125px;
    }
    @media only screen and (min-width: 1400px){
        .table-filter input, .table-filter select{
            max-width: 15% !important;
        }
        .table-filter select{
            max-width: 85px !important;
        }
        .table-filter .dpo {
            max-width: 90px !important;
        }
    }
</style>
<!-- background: -webkit-linear-gradient(#efefef, #ECECEC, #CECECE); -->
<div class="col-md-12"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12" id="userlistdiv">
             
                <?php
                $flashAlert =  $this->session->flashdata('alert');
                if(isset($flashAlert['message']) && !empty($flashAlert['message'])){?>
                    <br>
                    <div style="padding: 5px 12px" class="alert <?=$flashAlert['class']?>">
                        <strong><?=$flashAlert['type']?></strong> <?=$flashAlert['message']?>
                        <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>
                <div class="module">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="module-head">
                        <!--<h3> <?php if(isset($title)) echo $title;?></h3>-->
                    </div>
                    <div class="module-body table-responsive">
                        <table id="example" class="cell-border text-center" cellspacing="0" width="100%" data-selected_id="">
                            <thead>
                            <tr>
                                <th class="no-sort text-center"><input type="checkbox" class="checkbox allSelect" onchange="changeallcheckprop(this)"></th>
                                <th class="text-center"><?php echo $this->lang->line('id');?></th>
                                <th class="text-center">Date</th>
                                <th class="text-center">Time</th>
                                <th class="text-center">Added By</th>
                                <th class="text-center">User</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Phone</th>
                                <th class="text-center">Fax</th>
                                <th class="text-center">Address1</th>
                                <th class="text-center">Address2</th>
                                <th class="text-center">Zip Code</th>
                                <th class="text-center">City</th>
                                <th class="text-center">Role</th>
                                <th class="text-center">Departments</th>
                                <!-- <th class="text-center">Username</th> -->
                                <th class="text-center">Statut</th>
                                <th class="text-center">Since</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach($users_data as $item):?>
                                    <tr>
                                        <td>
                                            
                                             <input type="checkbox" data-input="<?=$item['id'];?>" data-time="<?= create_timestamp_uid($item['created_at'],$item['id']); ?>" class="checkbox singleSelect chk-mainuserCat-template" onchange="changeeditfunc(<?=$item['id'];?>,this)">
                                        </td>
                                        <td>
                                            <!--code by s_a -->
                                            <a href="<?=site_url("admin/users/".create_timestamp_uid($item['created_at'],$item['id'])."/edit")?>"><?=create_timestamp_uid($item['created_at'],$item['id']);?></a>
                                             <!--code by s_a -->
                                        </td>
                                        <td><?=date('d/m/Y', strtotime($item['created_at']));?></td>
                                        <td><?=date('H:i:s', strtotime($item['created_at']));?></td>
                                        <td>Mr Super Admin</td>
                                        <td><?=$item['civility']." ".$item['first_name']." ".$item['last_name'];?></td>
                                        <td><?=$item['email'];?></td>
                                        <td><?=$item['phone'];?></td>
                                        <td>2125551234</td>
                                        <td><?=$item['address'];?></td>
                                        <td><?=$item['address1'];?></td>
                                        <td><?=$item['zipcode'];?></td>
                                        <td><?=$item['cityname'];?></td>
                                        <td><?=$item['role']?></td>
                                        <td>
                                            <?php 
                                                if($item['department_id'] != 0 || $item['department_id'] != null || $item['department_id'] != ""){
                                                    $departments = $this->userx_model->getDepartmentByID($item['department_id']);
                                                    $k = 0;
                                                    foreach($departments as $i){
                                                        if($k == 0){
                                                            echo $i['name'];
                                                        }else{
                                                            echo ", ".$i['name'];
                                                        }
                                                        $k++;
                                                    }
                                                }else{
                                                    echo "-";
                                                }
                                            ?>
                                        </td>
                                        <!-- <td><?=$item['username'];?></td> -->
                                        <td>
                                            <?php if($item['active'] == 1){echo '<span class="label label-success">Active</span>';}else{echo '<span class="label label-danger">Disabled</span>';} ?>
                                        </td>
                                        <td style="white-space: nowrap"><?=timeDiff($item['updated_at'])?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <br>
                    </div>
                </div>
            </div>
            <!-- This is Delete Section Start -->
          <div class="col-md-12" id="usersdeldiv" style="display:none;border:1px solid silver;padding: 20px 10px;margin-top: 15px;">
           <?=form_open("admin/users/deleteusers")?>
            <input type="hidden" id="deleteuserid" class="chk-Adduser-btn" name="arr" value="">
            <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
            <button  class="btn btn-default"style=" float:left;margin-right: 10px;padding-left: 20px;padding-right: 20px;"> Yes </button>
            <button type="button" class="btn btn-default" style="cursor: pointer;padding-left: 20px;padding-right: 20px;" onclick="cancelUsersDel()">No</button>
          <?php echo form_close(); ?>
          </div>
     <!-- This is Delete Section End -->

        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<!-- <div id="table-filter" class="hide">
    <select class="form-control">
        <option>All Jobs</option>
    </select>
    <span class="pull-left">Age:</span>
    <input type="number" placeholder="From" class="form-control">
    <input type="number" placeholder="To" class="form-control">
    <select class="form-control">
        <option>Civility</option>
    </select>
    <select class="form-control dep-filter">
        <option>Department</option>
    </select>
    <input type="text" placeholder="From" class="dpo">
    <input type="text" placeholder="To" class="dpo">
    <select class="form-control">
        <option>All Status</option>
    </select>
</div> -->
<div id="table-filter" class="hide">
    <input type="text" id="txtSearch" value="<?=$search_text?>" placeholder="Search" class="form-control" style="border-radius: 0px;margin-top: 2px;">
    <a href="#" id="search" class="btn btn-sm btn-default" style="height: 35px;">Search</a>
</div>
<script>
    $(document).ready(function(){


        $(document).on("click", "#search", function() {
            if($("#txtSearch").val() != ""){
                var base_url = "<?php echo base_url();?>";
                $.ajax({
                    url: base_url+"admin/users/set_search_session",
                    method: 'GET',
                    contentType: "application/json; charset:utf-8",
                    data: {'text': $("#txtSearch").val()},
                    dataType: 'json',
                    success: function (response) {
                        location.reload();
                    }
                });
            }else{
                alert("Please enter search text!");
                location.reload();
            }
        });
        //code by s_a
                $('.delBtn').addClass('userdelbtn');
        $('.userdelbtn').removeClass('delBtn');

         $('.editBtn').addClass('usereditBtn');
        $('.usereditBtn').removeClass('editBtn');
       
        $('.userdelbtn').click(function(event){
            event.preventDefault();
            var val=$('.chk-Adduser-btn').val();
            if(val == ''){
                alert("Please select a row to delete.");
            }
            else{
                 $('#usersdeldiv').show();
                 $('#userlistdiv').hide();
            }
           
        });

        $('.usereditBtn').click(function(event){
              // when click on edit button first we check the length of checked checkbox if length equal to one then we go further
                if ($('.singleSelect:checked').length == 1){
                         var id= $('.singleSelect:checked').attr('data-time');
                         //then we get value of data-time of checked checkbox and check value is not empty
                          if(id == ''){
                               event.preventDefault();
                               alert("Please select a row.");
                             }
                          else{
                              var str="admin/users/"+id+"/edit"
                              var url='<?php echo site_url("'+str+'")?>'; 
                              $(this).attr("href", url);
                               
                          }
                           //then we get value of data-time of checked checkbox and check value is not empty
                 }
                 else if($('.singleSelect:checked').length < 1){
                   event.preventDefault();
                  alert("Please select a row.");
                 }
                  else if($('.singleSelect:checked').length > 1){
                     event.preventDefault();
                     alert("Please select only one row.");
                 }
               // when click on edit button first we check the length of checked checkbox if length equal to one then we go further
           
        
           
        });
        //code by s_a
    });
    //code by s_a
     function cancelUsersDel(){
         $('#usersdeldiv').hide();
         $('#userlistdiv').show();
     }


     function changeeditfunc(val,e){
                 //if allselect then allselect checkbox checked otherwise not
                if ($('.singleSelect:checked').length == $('.singleSelect').length){
                  $('.allSelect').prop('checked',true);
                 }
                 else {
                  $('.allSelect').prop('checked',false);
                 }
                  //if allselect then allselect checkbox checked otherwise not

             if($(e).is(":checked")) {
                 var current= val;
                 var commacurrent=","+current;
                 var arrayofid = $('.chk-Adduser-btn').val();
                 var iswordcomma = arrayofid.indexOf(commacurrent);
                 var isword = arrayofid.indexOf(current);
                 //check if this new checked value not exist in arrayofid then add otherwise not
                if(isword == -1 && iswordcomma == -1){
                    if(arrayofid == ""){
                         arrayofid=current;
                    }else{
                     arrayofid=arrayofid+commacurrent;
                     }
                 }
                //check if this new checked value not exist in arrayofid then add otherwise not
                $('.chk-Adduser-btn').val(arrayofid);
          
           }else{
                var current= val;
                var commacurrent=","+current;
                var arrayofid = $('.chk-Adduser-btn').val();

                //if arrayofid is empty then no need to go in this section
                 if(arrayofid != ""){
                     var isword = arrayofid.indexOf(commacurrent);
                     if(isword != -1){
                        arrayofid = arrayofid.replace(commacurrent, "");
                     }else{
                        arrayofid = arrayofid.replace(current, "");
                     }  
                 }
                 //if arrayofid is empty then no need to go in this section
                $('.chk-Adduser-btn').val(arrayofid);
               
           
           }
     }


     function changeallcheckprop(e){
        if (e.checked) {
            $(".singleSelect").each(function() {
                this.checked=true;
                var current= $(this).attr('data-input');
                var commacurrent=","+current;
                var arrayofid = $('.chk-Adduser-btn').val();
                //check if this new checked value not exist in arrayofid then add otherwise not
                 var iswordcomma = arrayofid.indexOf(commacurrent);
                 var isword = arrayofid.indexOf(current);
                  if(isword == -1 && iswordcomma == -1){
                     if(arrayofid == ""){
                             arrayofid=current;
                     }else{
                         arrayofid=arrayofid+commacurrent;
                     }
                  }
                 //check if this new checked value not exist in arrayofid then add otherwise not
                $('.chk-Adduser-btn').val(arrayofid);
               
            });
        } else {
            $(".singleSelect").each(function() {
                this.checked=false;
                var current= $(this).attr('data-input');
                var commacurrent=","+current;
                var arrayofid = $('.chk-Adduser-btn').val();
                 //if arrayofid is empty then no need to go in this section
                if(arrayofid != ""){
                     var isword = arrayofid.indexOf(commacurrent);
                     if(isword != -1){
                        arrayofid = arrayofid.replace(commacurrent, "");
                     }else{
                        arrayofid = arrayofid.replace(current, "");
                     }  
                 }
                 //if arrayofid is empty then no need to go in this section
                $('.chk-Adduser-btn').val(arrayofid);
                
            });
        }
     }
     
    //code by s_a
</script>
