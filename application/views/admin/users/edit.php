<?php $locale_info = localeconv(); ?>
<link href="<?php echo base_url(); ?>assets/system_design/multi-select/jquery.multiselect.css" rel="stylesheet">
<style type="text/css">
.float-right{
    float:right !important;
}
   h2{
   font-size: 22px;
   font-weight: 600;
   }
   .image-file-wrap {
   width: 40%;
   border: 1px solid #afadad;
   margin-top: 30px;
   min-height: 220px;
   }
   .social_icons a{
   font-size: 12px;
   display: inline-block;
   height: 25px;
   width: 25px;
   background: #fff;
   border-radius: 50%;
   text-align: center;
   line-height: 25px;
   margin-right: 15px;
   }
   .profile_circle{
   width: 170px;
   height: 170px;
   border-radius: 50%;
   display: inline-block;
   overflow: hidden;
   margin-top: 15px;
   border:2px solid #337ab7;
   }
   .profile_circle img{
   width: 100%;
   }
   .text_company{
   margin-top: 15px;
   }
   .text_company > p >span:nth-child(1){
   font-weight: 600;
   font-size: 16px;
   width: 60px;
   display: inline-block;
   /* min-width:140px; */
   }
    .text_company p{
        margin-bottom:5px !important;
    }
    .circle_image{
        width: 170px;
        height: 170px;
        border-radius: 100% !important;
        overflow: hidden;
        margin-top: 15px;
        border:2px solid #337ab7;
    }
   .circle_image img{
   max-width: 100%;
   height: 100%;
   }
   .company_image img{
   /* width: 100%; */
   max-width: 225px;
   max-height: 370px;
   }
   .section-company-info{
    /* background: -webkit-linear-gradient(#ECECEC , #efefef, #CECECE);
    background: -moz-linear-gradient(#ECECEC , #efefef, #CECECE);
    background: -o-linear-gradient(#ECECEC , #efefef, #CECECE);
    background: linear-gradient(#ECECEC , #efefef, #CECECE); */
    background-image: -webkit-linear-gradient(to bottom, #fff 0, #e0e0e0 100%);
    background-image: -moz-linear-gradient(to bottom, #fff 0, #e0e0e0 100%);
    background-image: -o-linear-gradient(to bottom, #fff 0, #e0e0e0 100%);
    background-image: linear-gradient(to bottom, #fff 0, #e0e0e0 100%);
    margin: 10px 0px;
    max-height: 630px;
    border: 1px solid #a4a8ab;
   }
   .breadcrumb a:nth-child(2){
        color: #337ab7;
    }
</style>
<div class="col-md-12">
   <!--col-md-10 padding white right-p-->
   <div class="content">
      <?php $this->load->view('admin/common/breadcrumbs');?>
      <div class="row">
         <div class="col-md-12">
            <?php $this->load->view('admin/common/alert');?>
            <div class="module">
               <?php
                  echo $this->session->flashdata('message');
                  $validation_erros = $this->session->flashdata('validation_errors');
                  if(!empty($validation_erros)){
                  ?>
               <div class="form-validation-errors alert alert-danger">
                  <h3 style="font-size: 20px; text-align: center;">Validation Errors</h3>
                  <?php echo $validation_erros; ?>
               </div>
               <?php } ?>
               <div class="module-head">
               </div>
               <div class="module-body">
                <!-- code by s_a -->
                  <?=form_open_multipart("admin/users/".create_timestamp_uid($row_data['created_at'],$row_data['id'])."/update")?>
                  <!-- code by s_a -->
                  <div class="row">
                     <div class="col-xs-8">
                        <div class="row">
                           <div class="col-xs-4">
                              <div class="form-group">
                                 <label>Statut*</label>
                                 <select class="form-control" name="status">
                                    <option value="1" <?php if($row_data['active'] == '1'){ echo 'selected="selected"'; } ?>>Active</option>
                                    <option value="0" <?php if($row_data['active'] == '0'){ echo 'selected="selected"'; } ?>>Disable</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-xs-4">
                              <div class="form-group">
                                 <label>Civility*</label>
                                 <select class="form-control" name="gender" id="civ_trg" onchange="active_civ_spider()">
                                    <option value="Mr" <?php if($row_data['gender'] == 'Mr'){ echo 'selected="selected"'; } ?>>Mr</option>
                                    <option value="Mme" <?php if($row_data['gender'] == 'Mme'){ echo 'selected="selected"'; } ?>>Mme</option>
                                    <option value="Mlle" <?php if($row_data['gender'] == 'Mlle'){ echo 'selected="selected"'; } ?>>Mlle</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-xs-4">
                              <div class="form-group">
                                 <label>First Name*</label>
                                 <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" id="first_trg" onkeyup="active_first_spider()" class="form-control" required name="first_name" value="<?=$row_data['first_name'];?>" placeholder="First Name">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-4">
                              <div class="form-group">
                                 <label>Last Name*</label>
                                 <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" class="form-control" id="last_trg" onkeyup="active_last_spider()" required name="last_name" value="<?=$row_data['last_name'];?>" placeholder="Last Name">
                                 </div>
                              </div>
                           </div>
                           <div class="col-xs-4">
                              <div class="form-group">
                                 <label>Email*</label>
                                 <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <input maxlength="50" type="email" class="form-control" required name="email" value="<?=$row_data['email'];?>" placeholder="Email">
                                 </div>
                              </div>
                           </div>
                           <!-- <div class="col-xs-4">
                              <div class="form-group">
                                  <label>Username*</label>
                                  <div class="input-group">
                                      <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                      <input maxlength="50" type="text" class="form-control" required name="username" value="<?=$row_data['username'];?>" placeholder="Username">
                                  </div>
                              </div>
                              </div> -->
                           <div class="col-xs-4">
                              <div class="form-group">
                                 <label>Password*</label>
                                 <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input maxlength="50" type="password" class="form-control" name="password" placeholder="Password">
                                    <input type="hidden" name="old_password" value="<?=$row_data['password'];?>">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-4">
                              <div class="form-group">
                                 <label>Telephone</label>
                                 <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                    <input maxlength="50" type="text" class="form-control" name="phone" value="<?=$row_data['phone'];?>" placeholder="Telephone">
                                 </div>
                              </div>
                           </div>
                           <div class="col-xs-4">
                              <div class="form-group">
                                 <label>Fax</label>
                                 <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-print"></i></span>
                                    <input maxlength="50" type="text" class="form-control" name="fax" value="<?=$row_data['fax'];?>" placeholder="Fax">
                                 </div>
                              </div>
                           </div>
                           <div class="col-xs-4">
                              <div class="form-group">
                                 <label>Website</label>
                                 <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-link"></i></span>
                                    <input type="text" class="form-control" name="link" placeholder="URL" value="<?=$row_data['link'];?>">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-xs-4">
                        <div class="image-file-wrap text-center">
                           <img <?php if(!empty($row_data['image'])){?>src="<?php echo base_url('uploads/user').'/'.$row_data['image'];?>" <?php }else{ ?> src="<?php echo base_url('uploads/user/no-image.jpeg');?>" <?php } ?> class="user_avatar_preview" style="max-width: 100%; min-height: 220px;margin-bottom: -8px;">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-xs-8">
                        <div class="row">
                           <div class="col-xs-4">
                              <div class="form-group">
                                  <label>Address</label>
                                  <textarea name="address" id="" class="form-control" rows="4" placeholder="Address"><?=$row_data['address'];?></textarea>
                              </div>
                           </div>
                           <div class="col-xs-4">
                              <div class="form-group">
                                  <label>Address 1</label>
                                  <textarea name="address1" id="" class="form-control" rows="4" placeholder="Address 1"><?=$row_data['address1'];?></textarea>
                              </div>
                           </div>
                           <div class="col-xs-4">
                              <div class="form-group">
                                 <label>Zip Code</label>
                                 <input type="text" class="form-control" name="zipcode" placeholder="Zip Code" value="<?=$row_data['zipcode'];?>">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                          <!-- code by s_a -->
                           <div class="col-xs-4">
                              <div class="form-group">
                                 <label>Country</label>
                                 <select class="form-control" name="country" id="country_id">
                                    <option value="">Select Country</option>
                                    <?php foreach($countries as $data):?>
                                    <option <?php echo ($data->id==$row_data['country'])?'selected':'';?>  value="<?=$data->id;?>"><?=$data->name;?></option>
                                    <?php endforeach;?>
                                 </select>
                              </div>
                           </div>
                           <!-- code by s_a -->
                           <div class="col-xs-4">
                              <div class="form-group">
                                 <label>Region</label>
                                 <select class="form-control" name="region" id="region_id"></select>
                              </div>
                           </div>
                           <div class="col-xs-4">
                              <div class="form-group">
                                 <label>City</label>
                                 <select class="form-control" name="city" id="cities_id"></select>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-4">
                              <div class="form-group">
                                 <label>Role*</label>
                                 <select class="form-control" required name="role" id="role_trg" onchange="active_rol_spider()">
                                    <option disabled="" selected="">--Select Role--</option>
                                    <?php foreach($roles as $row){ ?>
                                    <option <?php if($row_data['role_id'] == $row['id']){ echo 'selected="selected"'; } ?> value="<?=$row['id'];?>"><?=$row['name'];?></option>
                                    <?php } ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-xs-4">
                              <div class="form-group">
                                 <label>Department*</label>
                                 <select class="form-control 1col active" required name="department[]" id="department_trg" onchange="active_dep_spider()" multiple="multiple">
                                    <?php foreach($departments as $row){ ?>
                                    <option <?php if(in_array($row['id'], explode(",",$row_data['department_id']))){ echo 'selected="selected"'; } ?> value="<?=$row['id'];?>"><?=$row['name'];?></option>
                                    <?php } ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                     <div class="col-md-12">
                        <h3 style="font-size:24px;">Email Signature View</h3>
                     </div>
                  </div>
                  <div class="row section-company-info col-md-12">
                     <div class="col-md-4">
                        <div class="profile_image">
                           <a href="" class="profile_circle"><img style="width: 100%; height: 100%" class="user_avatar_preview" <?php if(!empty($row_data['image'])){ ?>src="<?php echo base_url('uploads/user').'/'.$row_data['image'];?>" <?php }else{ ?> src="<?php echo base_url('uploads/user/no-image.jpeg');?>" <?php } ?> alt=""></a>
                        </div>
                        <div class="text_company">
                           <?php if(isset($company_data['name']) && !empty($company_data['name'])){?>
                           <p><span style="width: 100%;"><?php echo $company_data['name']. " " .$company_data['type']; ?></span></p>
                           <?php } ?>
                           <p class="name-spider"><span style="width: 0px;"></span><span style="font-size: 14px; font-weight: 600;" id="civ-spider-area"><?=ucfirst($row_data['gender']);?></span>   <span id="first-spider-area" style="font-size: 14px; font-weight: 600;"><?=$row_data['first_name'];?></span>  <span style="font-size: 14px; font-weight: 600;" id="last-spider-area"><?=$row_data['last_name'];?></span></p>
                           <?php if($row_data['department_id'] != "" || $row_data['department_id'] != 0){ ?>
                           <p class="dep-spider"><span style="width: 0px;"></span><span style="font-size: 14px; font-weight: 600;" id="dep-spider-area"><?php $dep = explode(",",$row_data['department_id']); $i = 0; foreach($dep as $d){ if($i == 0){echo @$departments[$d]['name'];}else{echo ", ".@$departments[$d]['name'];} $i++; }?></span></p>
                           <?php } ?>
                           <?php if(@$roles[$row_data['role_id']]['name'] != ""){ ?>
                           <p class="rol-spider"><span style="width: 0px;"></span><span style="font-size: 14px; font-weight: 600;" id="rol-spider-area"><?php echo @$roles[$row_data['role_id']]['name'];?></span></p>
                           <?php } ?>
                           <?php if(isset($company_data['email']) && !empty($company_data['email'])){?>
                           <p><span style="display: inline;"><i class="fa fa-envelope" style="color:#0072bb;font-size:18px;width:20px"></i>&nbsp;Email:</span><span style="display: inline;padding-left: 30px;"><a href="mailto:<?php echo $company_data['email']; ?>"><?php echo $company_data['email']; ?></a></span></p>
                           <?php } ?>
                           <?php if(isset($company_data['phone']) && !empty($company_data['phone'])){?>
                           <p><span style="display: inline;"><i class="fa fa-phone" style="color:#0072bb;font-size:18px;width:20px"></i>&nbsp;Phone:</span><span style="display: inline;padding-left: 23px;"><a href="tel:<?php echo $company_data['phone']; ?>"><?php echo $company_data['phone']; ?></a></span></p>
                           <?php } ?>
                           <?php if(isset($company_data['fax']) && !empty($company_data['fax'])){?>
                           <p><span style="display: inline;"><i class="fa fa-print" style="color:#0072bb;font-size:18px;width:20px"></i>&nbsp;Fax:</span><span style="display: inline;padding-left: 44px;"><a href="tel:<?php echo $company_data['fax']; ?>"><?php echo $company_data['fax']; ?></a></span></p>
                           <?php } ?>
                           <?php if(isset($company_data['website']) && !empty($company_data['website'])){?>
                           <p><span style="display: inline;"><i class="fa fa-globe" style="color:#0072bb;font-size:18px;width:20px"></i>&nbsp;Website:</span><span style="display: inline;padding-left: 9px;color:#0072bb;"><?php echo $company_data['website']; ?></span></p>
                           <?php } ?>
                           <?php if(isset($company_data['city']) && !empty($company_data['city'])){?>
                           <p><span style="display: inline;"><i class="fa fa-map-marker" style="color:#0072bb;font-size:18px;width:20px"></i>&nbsp;Address:</span><span style="display: inline;padding-left: 8px;color:#0072bb;"><?php echo $company_data['street'].' '.$company_data['zip_code'].' '.$company_data['city']; ?></span></p>
                           <?php } ?>
                           <p class="social_icons" style="margin-left: 0px;"><span style="min-width: 110px;">Follow Us On: </span>
                           <?php //if(isset($company_data['facebook_link']) && !empty($company_data['facebook_link'])){?>
                           <a title="Facebook" style="background: #1877F2;" href="<?php if($company_data['facebook_link'] != ""){echo $company_data['facebook_link'];}else{echo "#";} ?>" target="_blank"><i style="color: white;" class="fa fa-facebook"></i></a>
                           <a title="Twitter" style="background: #1A91DA;" href="#" target="_blank"><i style="color: white;" class="fa fa-twitter"></i></a>
                           <?php //} ?>
                           <?php //if(isset($company_data['youtube_link']) && !empty($company_data['youtube_link'])){?>
                           <a title="Linkedin" style="background: #0077B5;" href="#" target="_blank"><i style="color: white;" class="fa fa-linkedin"></i></a>
                           <a title="Instagram" style="background: #cc00cc;" href="<?php if($company_data['instagram_link'] != ""){echo $company_data['instagram_link'];}else{echo "#";} ?>" target="_blank"><i style="color: white;" class="fa fa-instagram"></i></a>
                           <?php //} ?>
                           <?php //if(isset($company_data['instagram_link']) && !empty($company_data['instagram_link'])){?>
                           <!-- <a style="background: #0072bb;" href="#" target="_blank"><i style="color: white;" class="fa fa-google-plus"></i></a> -->
                           <a title="Youtube" style="background: #FF0000;" href="<?php if($company_data['youtube_link'] != ""){echo $company_data['youtube_link'];}else{echo "#";} ?>" target="_blank"><i style="color: white;" class="fa fa-youtube-play"></i></a>
                           <?php //} ?>
                           </p>
                        </div>
                     </div>
                     <div class="col-md-8" style="margin-top: 180px;">
                        <div class="profile_image">
                           <a href="" class="company_image"><img
                              <?php if(isset($company_data['logo']) && !empty($company_data['logo'])){?>
                              src="<?= base_url('uploads/company').'/'.$company_data['logo'];?>"
                              <?php } ?> alt=""></a>
                        </div>
                     </div>
                  </div>
                     </div>
                     <div class="col-xs-4">
                        <div class="form-group">
                           <label>Upload Profile Picture</label>
                           <input type="file" name="avatar" id="user_avatar">
                           <input type="hidden" name="old_avatar" value="<?=$row_data['image']?>">
                        </div>
                     </div>
                      <!-- code by s_a -->
                     <div class="col-xs-4">
                        <div class="image-file-wrap text-center" style="min-height: 224px;">
                            <img <?php if(!empty($row_data['document_signature'])){?>src="<?php echo base_url('uploads/user').'/'.$row_data['document_signature'];?>" <?php }else{ ?> src="<?php echo base_url('uploads/user/no-image.jpeg');?>" <?php } ?> class="document_signature_preview" style="max-width: 100%; min-height: 220px;margin-bottom: -8px;">
                        </div>
                     </div>
                     <!-- code by s_a -->
                     <div class="col-xs-4 float-right">
                        <div class="form-group">
                           <label>Upload Document Signature</label>
                           <input type="file" name="document_signature" id="document_signature">
                           <input type="hidden" name="old_document_signature" value="<?=$row_data['document_signature']?>">
                        </div>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-md-12">
                        <div class="text-right">
                           <a href="<?=base_url("admin/users")?>" class="btn btn-default"><i class="fa fa-times"></i> Cancel</a>
                           <button class="btn btn-default"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                     </div>
                  </div>
                  <br>
                  <?php echo form_close(); ?>
               </div>
            </div>
         </div>
      </div>
      <!--/.module-->
   </div>
   <!--/.content-->
</div>
<?php $locale_info = localeconv(); ?>
<style>
   @media only screen and (min-width: 1400px){
   .table-filter input, .table-filter select{
   max-width: 9% !important;
   }
   .table-filter select{
   max-width: 95px !important;
   }
   .table-filter .dpo {
   max-width: 90px !important;
   }
   }
</style>
<script type="text/javascript">
   $(document).on('change','#user_avatar',function(e)
   {
       if(this.files)
       {
           var reader = new FileReader();
           reader.onload = function (e){ $('.user_avatar_preview').attr('src', reader.result); }
           reader.readAsDataURL(this.files[0]);
       }
       else
       {
           console.log('No Image');
       }
   });
   
   $(document).on('change','#document_signature',function(e)
   {
       if(this.files)
       {
           var reader = new FileReader();
           reader.onload = function (e){ $('.document_signature_preview').attr('src', reader.result); }
           reader.readAsDataURL(this.files[0]);
       }
       else
       {
           console.log('No Image');
       }
   });
   
   // function active_dep_spider()
   // {
   //     dep_name = $('#department_trg option:selected').text();
   //     $('#dep-spider-area').text(dep_name);
   // }
   function active_dep_spider()
    {
        dep_name = $('#department_trg option:selected').text();
        // $('#dep-spider-area').text(dep_name);
        $(".dep-spider").remove();
        $( '<p class="dep-spider"><span style="width: 0;"></span><span style="font-size: 14px; font-weight: 600;" id="dep-spider-area">'+dep_name+'</span></p>' ).insertAfter( ".name-spider" );
        if(dep_name == "" || dep_name == null){
            $(".dep-spider").remove();
        }
    }
   
   // function active_rol_spider()
   // {
   //     role_name = $('#role_trg option:selected').text();
   //     $('#rol-spider-area').text(role_name);
   // }
   function active_rol_spider()
    {
        role_name = $('#role_trg option:selected').text();
        $(".rol-spider").remove();
        if( $('#dep-spider-area').length )
        {
            $( '<p class="rol-spider"><span style="width: 0;"></span><span style="font-size: 14px; font-weight: 600;" id="rol-spider-area">'+role_name+'</span></p>' ).insertAfter( ".dep-spider" );
        }else{
            $( '<p class="rol-spider"><span style="width: 0;"></span><span style="font-size: 14px; font-weight: 600;" id="rol-spider-area">'+role_name+'</span></p>' ).insertAfter( ".name-spider" );
        }
        // $('#rol-spider-area').text(role_name);
    }
   function active_civ_spider()
   {
       civ_name = $('#civ_trg option:selected').text();
       $('#civ-spider-area').text(civ_name);
   }
   
   function active_first_spider()
   {
       first_name = $('#first_trg').val();
       $('#first-spider-area').text(first_name);
   }
   
   function active_last_spider()
   {
       last_name = $('#last_trg').val();
       $('#last-spider-area').text(last_name);
   }
</script>
<script type="text/javascript">
   function get_region_base_country(region_id=null)
   {
   
     country_id=$('#country_id').val();
   
   
     $.ajax({
       url:'<?php echo base_url();?>cms/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
   // console.log(response);
   html ='';
   html =html + '<option value="">Select Region</option>';
   
   $.each(response, function( index, value ) {
   // console.log( value, region_id );
   
   
   if(region_id==value.id)
   {
     html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
   
   }
   else{
     html =html + '<option value="'+value.id+'">'+value.name+'</option>';
   
   }
   });
   
   $('#region_id').html(html);
   
   
   }
   
   
   });
   
   }
   
   
   
   function get_cities_base_country_region(region_id=null,cities_id=null)
   {
   
     country_id=$('#country_id').val();
     if(region_id==null)
     {
       region_id=$('#region_id').val();
   
     }
   
   
     $.ajax({
       url:'<?php echo base_url();?>cms/ajax_get_cities_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id+'&region_id='+region_id,
       dataType: 'json',
       success: function(response){
   // console.log(response);
   html ='';
   html =html + ' <option value="">Select cities</option>';
   
   $.each(response, function( index, value ) {
   // console.log( value , cities_id);
   
   
   if(cities_id==value.id)
   {
     html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
   
   }
   else{
     html =html + '<option value="'+value.id+'">'+value.name+'</option>';
   
   }
   });
   
   $('#cities_id').html(html);
   
   
   }
   
   
   });
   
   }
   
   
   
   $(document).ready(function() {
   
   // ajax_get_all_cms_listing
   
   $('#country_id').change(function(){
   get_region_base_country();
   
   });
   $('#region_id').change(function(){
   
   get_cities_base_country_region();
   
   });
   
   get_region_base_country(<?php echo $row_data['region'] != "" ? $row_data['region'] : 0;?>);
   get_cities_base_country_region(<?php echo $row_data['region'] != "" ? $row_data['region'] : 0;?>,<?php echo $row_data['city'] != "" ? $row_data['city'] : 0;?>);
   
   });
   
</script>
<script src="<?php echo base_url(); ?>assets/system_design/multi-select/jquery.multiselect.js" type="text/javascript"></script>
<script>
    $(function() {
        $('select[multiple].active.1col').multiselect({
            columns: 1,
            placeholder: 'Search Department',
            search: true,
            searchOptions: {
                'default': 'Search Department'
            },
            selectAll: true
        });

    });
</script>    