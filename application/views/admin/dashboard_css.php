<style>
    .dataTables_length, .dataTables_filter, .dataTables_info, .dataTables_paginate {
        display: none;
    }
    .module-head h2 {
        font-size: 18px; padding: 5px 0; margin: 0;
    }
    .module {
        border: 1px solid #ccc;
        border-radius: 5px;
        margin: 20px 0;
        padding-bottom: 15px;
    }
    .module .btn {
        margin-left: 10px;
    }
    .btn-mar {
        margin-top: 15px;
    }
    .filter-group{
        background: linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%) !important;
        height: 45px;
        margin: 0px 0px 20px 10px;
        border-radius: 4px;
        border: solid 1px #efefef;
        padding: 5px;
        text-align: center;
    }
    .filter-group div {
        color:#fff;
    }
    .filter-label{
        padding: 0px;
        font-size: 15px;
        display: block;
        position: relative;
        margin: 0px;
    }
    /*
    .chart-outside{
        /*padding: 5px 5px 5px 5px;*/
        /*height: 360px;*/
        /*border: 1px solid #ccc;
        border-radius: 5px;
        text-align: center;
        background-color:#f5f5f5 !important;
    }*/
    
   .form-control{
    color:#000 !important;
   }
    .btn-mar {
        margin-top: 0;
        margin-bottom: 10px;
    }
    .charts{
        margin-left: 2%;
        width: 79%;
        padding: 0px;
        position: relative;
        text-align: left;
    }
    .font-13{
        font-size: 13px;
    }
    input:hover, input:focus, textarea:hover, textarea:focus {
        box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2), 0 0 3px rgba(0, 0, 0, 0.2);
    }
   
</style>