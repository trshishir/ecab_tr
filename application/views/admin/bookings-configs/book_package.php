<style>
.table thead > tr > th {
    background: linear-gradient(#ffffff, #ffffff 25%, #d0d0d0) !important;
    padding: 7px;
    color: #2c2c2c;
    font-weight: bolder;
    font-size: 12px;
}
</style>
  <div class="ListPackage">
    <input type="hidden" class="chk-Addpackage-btn" value="">
    <!-- <div class="col-md-12">
        <div class="toolbar"style="float: right; margin-bottom:5px;">
          <button class="btn btn-sm btn-default" onclick="packageadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>&nbsp;
          <button class="btn btn-sm btn-default" onclick="packageedit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>&nbsp;
          <button class="btn btn-sm btn-default" onclick="packagedelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button>&nbsp;
        </div>
    </div> -->
    <div class="col-md-12">
      <div class="module-body table-responsive">
        <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center"style="width:2%;">#</th>
            <th class="text-center" style="width:3% !important;"><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('user'); ?></th>
            <th class="column-first_name" style="text-align:left !important;">Package Name</th>
            <th class="column-date text-center"style="width:3%;">Statut</th>
            <th class="column-time text-center"style="width:3%;">Depart Type</th>
            <th class="column-time text-center"style="width:3%;">Departure</th>
            <th class="column-time text-center"style="width:3%;">Destination Type</th>
            <th class="column-time text-center"style="width:3%;">Destination</th>
            <th class="column-time text-center"style="width:2%;">Price(Car)</th>
            <th class="column-time text-center"style="width:2%;">Price(Minibus)</th>
          </tr>
          </thead>
          <tbody>
              <?php $cnt=1; ?>
              <?php
              $package_data = $this->bookings_config_model->booking_Config_getAll('vbs_u_package');
              if (!empty($package_data)):
              foreach($package_data as $key => $item):
                    $status=$item->status=='0'?"Show":"Hide";
                    $d_type=$this->bookings_config_model->d_type($item->d_type);
                    $departure=$this->bookings_config_model->departure($item->departure);
                    $destination_type=$this->bookings_config_model->destination_type($item->destination_type);
                    $destination=$this->bookings_config_model->destination($item->destination);
              ?>
                  <tr>
                    <td class="text-center">
                        <input type="checkbox" class="chk-mainpoi-template" data-input="<?=$item->id?>">
                    </td>
                    <td class="text-center"><a href="javascript:void()" onclick="packageidEdit('<?=$item->id?>')"><?=create_timestamp_uid($item->created_at,$item->id);?></a></td>
                    <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                    <td class="text-center"><?=$item->time?></td>
                    <td class="text-center"><?= $this->bookings_config_model->getuser($item->user_id);?></td>
                      <td><?=$item->name?></td>
                      <td class="text-center"><?=$status;?></td>
                      <td class="text-center"><?=$d_type;?></td>
                      <td class="text-center"><?=$departure;?></td>
                      <td class="text-center"><?=$destination_type;?></td>
                      <td class="text-center"><?=$destination;?></td>
                      <td class="text-center"><?=$item->p_car;?></td>
                      <td class="text-center"><?=$item->p_minibus;?></td>

                  </tr>
                  <?php $cnt++; ?>
              <?php endforeach; ?>
              <?php endif; ?>
          </tbody>
      </table>
      </div>
    </div>
  </div>
  <div class="packageadd" style="display: none;" >
      <?=form_open("admin/booking_config/packageadd")?>
      <div class="row" style="margin-top: 10px;">
           <div class="col-md-3" style="margin-top: 5px;">
               <div class="form-group">
                 <span style="font-weight: bold;">Package Name</span>
                  <input type="text" class="form-control" name="name" placeholder="" value="" required>
                </div>
            </div>
            <div class="col-md-3" style="margin-top: 5px;">
                <div class="form-group">
                  <span style="font-weight: bold;">Statut</span>
                  <select class="form-control" name="status" required style="background: #fff !important;">
                      <option value="0">Show</option>
                      <option value="1">Hide</option>
                  </select>
                </div>
            </div>
            <div class="col-md-3" style="margin-top: 5px;">
                <div class="form-group">
                  <span style="font-weight: bold;">Depart Type</span>
                  <select class="form-control" name="d_type" required style="background: #fff !important;">
                      <option value="">Select</option>
                      <option value="0">POI</option>
                      <option value="1">City</option>
                      <option value="2">Region</option>
                  </select>
                </div>
            </div>
      </div>
      <div class="row" style="margin-top: 10px;">
            <div class="col-md-3" style="margin-top: 5px;">
                <div class="form-group">
                  <span style="font-weight: bold;">Departure</span>
                  <select class="form-control" name="departure" required style="background: #fff !important;">
                      <option value="">Select</option>
                      <option value="0">CDG</option>
                      <option value="1">Park</option>
                      <option value="2">Hotel</option>
                  </select>
                </div>
            </div>
            <div class="col-md-3" style="margin-top: 5px;">
                <div class="form-group">
                  <span style="font-weight: bold;">Destination Type</span>
                  <select class="form-control" name="destination_type" required style="background: #fff !important;">
                      <option value="">Select</option>
                      <option value="0">POI</option>
                      <option value="1">City</option>
                      <option value="2">Region</option>
                  </select>
                </div>
            </div>
            <div class="col-md-3" style="margin-top: 5px;">
                <div class="form-group">
                  <span style="font-weight: bold;">Destination</span>
                  <select class="form-control" name="destination" required style="background: #fff !important;">
                      <option value="">Select</option>
                      <option value="0">CDG</option>
                      <option value="1">Park</option>
                      <option value="2">Hotel</option>
                  </select>
                </div>
            </div>
      </div>
      <div class="row" style="margin-top: 10px;">
          <div class="col-md-3" style="margin-top: 5px;">
              <div class="form-group">
                  <span style="font-weight: bold;">Price(Car)</span>
                  <input type="number" class="form-control" name="p_car" placeholder="" value="" required>
              </div>
          </div>
          <div class="col-md-3" style="margin-top: 5px;">
              <div class="form-group">
                  <span style="font-weight: bold;">Price(Minibus)</span>
                  <input type="number" class="form-control" name="p_minibus" placeholder="" value="" required>
              </div>
          </div>
          <div class="col-md-3" style="margin-top: 5px;">
              <div class="form-group">
                  <span style="font-weight: bold;">Price(Van)</span>
                  <input type="number" class="form-control" name="p_van" placeholder="" value="" required>
              </div>
          </div>
      </div>
      <div class="row" style="margin-top: 10px;">
          <div class="col-md-3" style="margin-top: 5px;">
              <div class="form-group">
                  <span style="font-weight: bold;">Price TPMR(Car)</span>
                  <input type="number" class="form-control" name="p_tpmr_car" placeholder="" value="" required>
              </div>
          </div>
          <div class="col-md-3" style="margin-top: 5px;">
              <div class="form-group">
                  <span style="font-weight: bold;">Price TPMR(Minibus)</span>
                  <input type="number" class="form-control" name="p_tpmr_mini" placeholder="" value="" required>
              </div>
          </div>
          <div class="col-md-3" style="margin-top: 5px;">
              <div class="form-group">
                  <span style="font-weight: bold;">Price TPMR(Van)</span>
                  <input type="number" class="form-control" name="p_tpmr_van" placeholder="" value="" required>
              </div>
          </div>
      </div>
      <div class="row" style="margin-top: 10px;">
        <div class="col-md-9" style="margin-top: 5px;">
            <a href="javascript:void()" class="btn" style="float:right; margin-left:7px;" onclick="cancelPackage()"><span class="delete-icon"> Cancel </span></a>
            <button  class="btn"style=" float:right;"><span class="save-icon"></span> Save </button>
        </div>
      </div>
    <?php echo form_close(); ?>
  </div>
  <?=form_open("admin/booking_config/packageedit")?>
<div class="packageedit" style="display:none">
  <div class="packageeditajax">
  </div>
  <div class="row" style="margin-top: 10px;">
			<div class="col-md-9" style="margin-top: 5px;">
				<a href="javascript:void()" class="btn" style="float:right; margin-left:7px;" onclick="cancelPackage()"><span class="delete-icon"> Cancel </span></a>
				<button  class="btn"style=" float:right;"><span class="save-icon"></span> Update </button>
			</div>
		</div>
</div>
<?php echo form_close(); ?>
<div class="col-md-12 packagedelete" style="border:1px solid #ccc;padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
    <?=form_open("admin/booking_config/packagedelete")?>
    <input  name="tablename" value="vbs_job_statut" type="hidden" >
    <input type="hidden" id="packagedeleteid" name="packagedeleteid" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?
    </div>
    <button  class="btn"style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>
    <a class="btn" style="cursor: pointer;" onclick="cancelPackage()">No</a>
  <?php echo form_close(); ?>
</div>
<script src="http://unique/ecabapp/assets/system_design/scripts/jquery.js"></script>
<script type="text/javascript">
  function packageadd()
  {
    setupDivPackage();
    $(".packageadd").show();
  }
  function cancelPackage()
  {
    setupDivPackage();
    $(".ListPackage").show();
  }
  function packageedit()
  {
      var val = $('.chk-Addpackage-btn').val();
      if (val=='')
      {
          alert("Please select record!");
          return false;
      }
      setupDivPackage();
      $(".packageedit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/booking_config/get_ajax_package'; ?>',
              data: {'package_id': val},
              success: function (result) {
                document.getElementsByClassName("packageeditajax")[0].innerHTML = result;
              }
          });
  }
  function packageidEdit(id){
    var val = id;
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
    setupDivPackage();
    $(".packageedit").show();
    $.ajax({
            type: "GET",
            url: '<?php echo base_url().'admin/booking_config/get_ajax_package'; ?>',
            data: {'package_id': val},
            success: function (result) {
              document.getElementsByClassName("packageeditajax")[0].innerHTML = result;
            }
        });
  }
  function packagedelete()
  {
    var val = $('.chk-Addpackage-btn').val();
    if (val=='')
      {
          alert("Please select record!");
          return false;
      }
    $('#packagedeleteid').val(val);
    setupDivPackage();
    $(".packagedelete").show();
  }
  function setupDivPackage()
  {
    $(".packageedit").hide();
    $('.packageadd').hide();
    $('.ListPackage').hide();
    $('.packagedelete').hide();
  }
  $('input.chk-mainpoi-template').on('change', function() {
    $('input.chk-mainpoi-template').not(this).prop('checked', false);
    var id = $(this).attr('data-input');
    $('.chk-Addpackage-btn').val(id);
  });
</script>
