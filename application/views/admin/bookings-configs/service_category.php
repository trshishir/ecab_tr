<div class="row">
  <div class="ListService_cat" >
    <!-- <div class="col-md-12">
      <div class="page-action"style="float: right; margin-bottom:5px;" >
        <button class="btn btn-sm btn-default" onclick="ServiceCatAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>&nbsp;
        <button class="btn btn-sm btn-default" onclick="ServiceCatEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>&nbsp;
        <button class="btn btn-sm btn-default" onclick="ServiceCatDelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button>&nbsp;
      </div>
    </div> -->
  <input type="hidden" class="chk-Add-service_cat-btn" value="">
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center"style="width:2%;">#</th>
            <th class="text-center" style="width:3% !important;"><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('user'); ?></th>
            <th class="column-first_name" style="text-align:left !important;">Category</th>
            <th class="column-time text-center"style="width:8%;">Statut</th>
            <th class="column-time text-center"style="width:8%;">TVA%</th>
            <th class="column-time text-center"style="width:13%">Price Calcul Method</th>
            <th class="column-time"style="width:13%">Booking Form</th>
          </tr>
          </thead>
          <tbody>
              <?php if (!empty($service_cat)): ?>
              <?php $cnt=1; ?>
              <?php foreach($service_cat as $key => $item):?>
                  <tr>
                    <td class="text-center">
                        <input type="checkbox" class="chk-mainservice-cat-template" data-input="<?=$item->id?>">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="Service_catidEdit('<?=$item->id?>')"><?=create_timestamp_uid($item->created_at,$item->id);?></a></td>
                      <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                      <td class="text-center"><?=$item->time?></td>
                      <td class="text-center"><?= $this->bookings_config_model->getuser($item->user_id);?></td>
                      <td><?=$item->category_name;?></td>
                      <td class="text-center"><?=$item->statut=='1'?'Show':'Hide';?></td>
                      <td class="text-center"><?=$item->tva=='1'?'1':'2';?></td>
                      <td class="text-center"><?=$item->pcm=='1'?'10':'20';?></td>
                      <td><?=$item->booking_from=='1'?'Surat':'Rajkot';?></td>
                  </tr>
                  <?php $cnt++; ?>
              <?php endforeach; ?>
              <?php endif; ?>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>
<?=form_open("admin/booking_config/servicecatadd")?>
<!-- <form action="<?=base_url();?>admin/booking_config/vatadd" class="" method="post"  enctype="multipart/form-data"> -->
<div class="Service_catadd" style="display: none;" >
  <div class="col-md-6" style="margin-top: 5px;">
    <div class="form-group">
      <span style="font-weight: bold;"> Name of Category </span>
        <input type="text" class="form-control" name="name_of_category" placeholder="" value="">
    </div>
  </div>
  <div class="col-md-12">
  </div>
  <div class="col-md-3" style="margin-top: 5px;">
    <div class="form-group">
      <span style="font-weight: bold;">Statut</span>
      <select class="form-control" name="service_cat_statut" required>
        <option value="">Statut</option>
        <option value="1">Show</option>
        <option value="2">Hide</option>
      </select>
    </div>
  </div>
  <div class="col-md-3" style="margin-top: 5px;">
    <div class="form-group">
      <span style="font-weight: bold;">TVA% </span>
      <select class="form-control" name="tva" required>
        <option value="">Seleceionner </option>
        <option value="1">1</option>
        <option value="2">2</option>
      </select>
    </div>
  </div>
  <div class="col-md-12">
  </div>
  <div class="col-md-3" style="margin-top: 5px;">
    <div class="form-group">
      <span style="font-weight: bold;"> Price Calcul Methode  </span>
      <select class="form-control" name="pcm" required>
        <option value="">Seleceionner</option>
        <option value="1">10</option>
        <option value="2">20</option>
      </select>
    </div>
  </div>

  <div class="col-md-3" style="margin-top: 5px;">
    <div class="form-group">
      <span style="font-weight: bold;">Booking Form</span>
      <select class="form-control" name="booking_form" required>
        <option value="">Seleceionner</option>
        <option value="1">Surat</option>
        <option value="2">Rajkot</option>
      </select>
    </div>
  </div>
  <div class="col-md-12">
  </div>
  <div class="col-md-6" style="margin-top: 5px;">
    <button class="btn" style="float:right" onclick="cancle_Service_cat()"><span class="delete-icon"> Cancel </span></button>
    <button  class="btn"style="margin-right:7px; float:right"><span class="save-icon"></span> Save </button>
    <?php echo form_close(); ?>
  </div>
</div>
<div class="service_catEdit" style="display:none">
  <?=form_open("admin/booking_config/servicecatedit")?>
  <div class="service_catajax">
  </div>
  <div class="col-md-12">
  </div>
  <div class="col-md-6" style="margin-top: 5px;">
    <a class="btn" style="margin-right:7px; float:right; margin-left:7px;" onclick="cancle_Service_cat()"><span class="delete-icon"> Cancel </span></a>
    <button  class="btn"style="float:right"><span class="save-icon"></span> Update </button>
  </div>
  <?php echo form_close(); ?>
</div>
<div class="col-md-12 service_catDelete" style="border:1px solid #ccc;padding:20px  0px; display: none;">
  <?=form_open("admin/booking_config/deleteservicecat")?>
    <input  name="tablename" value="vbs_job_statut" type="hidden" >
    <input type="hidden" id="service_cat-deletid" name="delet_service_cat_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn"style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>
    <a class="btn" style="cursor: pointer;" onclick="cancle_Service_cat()">No</a>
  <?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
function ServiceCatAdd()
{
  setupDivService_cat();
  $(".Service_catadd").show();
}

function cancle_Service_cat()
{
  setupDivService_cat();
  $(".ListService_cat").show();
}
// function ServiceCatEdit()
// {
//   var val = $('.chk-Add-service_cat-btn').val();
//   if(val != "")
//   {
//     setupDivService_cat();
//     // $(".service_catEdit").show();
//     $("#service_catEditview"+val).show();
//   }else{
//     alert('Please select a row to edit.');
//   }
// }
function ServiceCatEdit()
{
    var val = $('.chk-Add-service_cat-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivService_cat();
        $(".service_catEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/booking_config/get_ajax_service_cat'; ?>',
                data: {'service_cat_id': val},
                success: function (result) {
                  // alert(result);
                  document.getElementsByClassName("service_catajax")[0].innerHTML = result;
                }
            });
}
function Service_catidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivService_cat();
      $(".service_catEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/booking_config/get_ajax_service_cat'; ?>',
              data: {'service_cat_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("service_catajax")[0].innerHTML = result;
              }
          });
}
function ServiceCatDelete()
{
  var val = $('.chk-Add-service_cat-btn').val();
  if(val != "")
  {
  setupDivService_cat();
  $(".service_catDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivService_cat()
{
  // $(".service_catEditEdit").hide();
  $(".Service_catadd").hide();
  $(".service_catEdit").hide();
  $(".ListService_cat").hide();
  $(".service_catDelete").hide();

}
$('input.chk-mainservice-cat-template').on('change', function() {
  $('input.chk-mainservice-cat-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Add-service_cat-btn').val(id);
  $('#service_cat-deletid').val(id);
});

</script>
