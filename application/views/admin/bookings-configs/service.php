<div class="row">
  <div class="ListService" >
      <!-- <div class="col-md-12">
        <div class="page-action"style="float: right; margin-bottom:5px;" >
          <button class="btn btn-sm btn-default" onclick="ServiceAdd()"><i class="fa fa-plus"></i>  <span class="add-icon">Add</span></button>&nbsp;
          <button class="btn btn-sm btn-default" onclick="ServiceEdit()"><i class="fa fa-pencil"></i <span class="edit-icon">Edit</span></button>&nbsp;
          <button class="btn btn-sm btn-default" onclick="ServiceDelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button>&nbsp;
        </div>
      </div> -->
    <input type="hidden" class="chk-Addservice-btn" value="">
    <div class="col-md-12">
    <div class="module-body table-responsive">
        <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
            <thead>
            <tr>
              <th class="no-sort text-center"style="width:2%;">#</th>
              <th class="text-center" style="width:3% !important;"><?php echo $this->lang->line('id'); ?></th>
              <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('date'); ?></th>
              <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('time'); ?></th>
              <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('user'); ?></th>
              <th class="column-first_name" style="text-align:left !important;">Name</th>
              <th class="column-date"style="width:8%;">Service Category</th>
              <th class="text-center"style="width:8%;">Statut</th>
            </tr>
            </thead>
            <tbody>
                <?php if (!empty($vat_data)): ?>
                <?php $cnt=1; ?>
                <?php foreach($service as $key => $item):?>
                    <tr>
                      <td class="text-center">
                          <input type="checkbox" class="chk-mainservice-template" data-input="<?=$item->id?>">
                      </td>
                        <td class="text-center"><a href="javascript:void()" onclick="ServiceidEdit('<?=$item->id?>')"><?=create_timestamp_uid($item->created_at,$item->id);?></a></td>
                        <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                        <td class="text-center"><?=$item->time?></td>
                        <td class="text-center"><?= $this->bookings_config_model->getuser($item->user_id);?></td>
                        <td><?=$item->service_name;?></td>
                        <td><?=$item->service_category=='1'?'Category 1':'Category 2';?></td>
                        <td class="text-center"><?=$item->statut=='1'?'Show':'Hide';?></td>
                    </tr>
                    <?php $cnt++; ?>
                <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
        <br>
      </div>
    </div>
  </div>
  <?=form_open("admin/booking_config/serviceadd")?>
  <!-- <form action="<?=base_url();?>admin/booking_config/vatadd" class="" method="post"  enctype="multipart/form-data"> -->
  <div class="Serviceadd" style="display: none;" >

    <div class="col-md-2" style="margin-top: 5px;">
      <div class="form-group">
        <span style="font-weight: bold;">Service Name</span>
          <input type="text" class="form-control" name="service_name" placeholder="" value="">
      </div>
    </div>
    <div class="col-md-2" style="margin-top: 5px;">
      <div class="form-group">
        <span style="font-weight: bold;">Category</span>
        <select class="form-control" name="service_category" required>
          <option value="">Seleceionner</option>
          <option value="1">Category 1</option>
          <option value="2">Category 2</option>
        </select>
      </div>
    </div>
    <div class="col-md-3" style="margin-top: 5px;">
      <div class="form-group">
        <span style="font-weight: bold;">Statut</span>
        <select class="form-control" name="service_statut" required>
          <option value="">Statut</option>
          <option value="1">Show</option>
          <option value="2">Hide</option>
        </select>
      </div>
    </div>
    <div class="col-md-7">
      <button class="btn" style="float:right; margin-left:7px;" onclick="cancelService()"><span class="delete-icon"> Cancel </span></button>
      <button  class="btn"style=" float:right;"><span class="save-icon"></span> Save </button>
      <?php echo form_close(); ?>
    </div>
  </div>
  <div class="serviceEdit"  style="display:none;">
    <?=form_open("admin/booking_config/serviceedit")?>
    <div class="serviceEditajax">
    </div>
    <div class="col-md-7">
      <a class="btn" style="float:right; margin-left:7px;" onclick="cancelService()"><span class="delete-icon"> Cancel </span></a>
      <button  class="btn"style=" float:right;"><span class="save-icon"></span> Update </button>
    </div>
    <?php echo form_close(); ?>
  </div>
  <div class="col-md-12 serviceDelete" style="border:1px solid #ccc;padding:20px  0px; display: none;">
    <?=form_open("admin/booking_config/deleteservice")?>
      <input  name="tablename" value="vbs_job_statut" type="hidden" >
      <input type="hidden" id="servicedeletid" name="delet_service_id" value="">
      <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
      <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
      <button  class="btn"style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

      <a class="btn" style="cursor: pointer;" onclick="cancelService()">No</a>
    <?php echo form_close(); ?>
  </div>
</div>

<script type="text/javascript">
function ServiceAdd()
{
  setupDivService();
  $(".Serviceadd").show();
}

function cancelService()
{
  setupDivService();
  $(".ListService").show();
}
// function ServiceEdit()
// {
//   var val = $('.chk-Addservice-btn').val();
//   if(val != "")
//   {
//     setupDivService();
//     $(".serviceEdit").show();
//     // $("#serviceEditview"+val).show();
//   }else{
//     alert('Please select a row to edit.');
//   }
// }

function ServiceEdit()
{
    var val = $('.chk-Addservice-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivService();
        $(".serviceEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/booking_config/get_ajax_service'; ?>',
                data: {'service_id': val},
                success: function (result) {
                  // alert(result);
                  document.getElementsByClassName("serviceEditajax")[0].innerHTML = result;
                }
            });
}
function ServiceidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivService();
      $(".serviceEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/booking_config/get_ajax_service'; ?>',
              data: {'service_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("serviceEditajax")[0].innerHTML = result;
              }
          });
}
function ServiceDelete()
{
  var val = $('.chk-Addservice-btn').val();
  if(val != "")
  {
  setupDivService();
  $(".serviceDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivService()
{
  // $(".serviceEditEdit").hide();
  $(".Serviceadd").hide();
  $(".serviceEdit").hide();
  $(".ListService").hide();
  $(".serviceDelete").hide();

}
$('input.chk-mainservice-template').on('change', function() {
  $('input.chk-mainservice-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Addservice-btn').val(id);
  $('#servicedeletid').val(id);
});

</script>
