<div class="row">
  <div class="ListCartCat">
    <!-- <div class="col-md-12">
      <div class="page-action"style="float: right; margin-bottom:5px;" >
        <button class="btn btn-sm btn-default" onclick="carCatAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>&nbsp;
        <button class="btn btn-sm btn-default" onclick="carCatEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>&nbsp;
        <button class="btn btn-sm btn-default" onclick="carCatDelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button>&nbsp;
      </div>
    </div> -->
  <input type="hidden" class="chk-AddCarCat-btn" value="">
  <div class="col-md-12">
    <div class="module-body table-responsive">
        <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
            <thead>
            <tr>
              <th class="no-sort text-center"style="width:2%;">#</th>
              <th class="text-center" style="width:3% !important;"><?php echo $this->lang->line('id'); ?></th>
              <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('date'); ?></th>
              <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('time'); ?></th>
              <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('user'); ?></th>
              <th class="column-first_name" style="text-align:left !important;">Name</th>
              <th class="column-time text-center"style="width:8%;">Statut</th>
            </tr>
            </thead>
            <tbody>
                <?php if (!empty($car_category)): ?>
                <?php $cnt=1; ?>
                <?php foreach($car_category as $key => $item):?>
                    <tr>
                        <td class="text-center"><input type="checkbox" class="chk-mainCarCat-template" data-input="<?=$item->id?>"></td>
                        <td class="text-center"><a href="javascript:void()" onclick="carcatidEdit('<?=$item->id?>')"><?=create_timestamp_uid($item->created_at,$item->id);?></a></td>
                        <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                        <td class="text-center"><?=$item->time?></td>
                        <td class="text-center"><?= $this->bookings_config_model->getuser($item->user_id);?></td>
                        <td><?=$item->car_cat_name;?></td>
                        <td class="text-center"><?=$item->statut=='1'?'Show':'Hide';?></td>
                    </tr>
                    <?php $cnt++; ?>
                <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
        <br>
      </div>
  </div>
</div>

  <?=form_open("admin/booking_config/carCatadd")?>
  <!-- <form action="<?=base_url();?>admin/booking_config/vatadd" class="" method="post"  enctype="multipart/form-data"> -->
  <div class="carCatAdd" style="display: none;">
      <div class="col-md-3" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;"> Car Category Name </span>
            <input type="text" class="form-control" name="car_cat_name" placeholder="" value="">
        </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;">Statut</span>
          <select class="form-control" name="car_cat_statut" required>
            <option value="">Statut</option>
            <option value="1">Show</option>
            <option value="2">Hide</option>
          </select>
        </div>
      </div>
      <div class="col-md-3" style="margin-top: 5px;">
        <div class="form-group">
        <br>
        <button  class="btn"style=" float:left;margin-right:7px;"><span class="save-icon"></span> Save </button>
        <button class="btn" style="float:left;" onclick="cancelCarCat()"><span class="delete-icon"> Cancel </span></button>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?>
  <div class="carCatEdit"  style="display:none;">
  <!-- <div id="carCatEditview<?=$value->id?>" class="carCatEditEdit" style="display: none;"> -->
    <?=form_open("admin/booking_config/carCatedit")?>
    <div class="carCatEditajax">
    </div>
    <div class="col-md-3" style="margin-top: 5px;">
      <div class="form-group">
      <br>
      <button  class="btn"style=" float:left; margin-right:7px;"><span class="save-icon"></span> Update </button>
      <a class="btn" style="float:left;" onclick="cancelCarCat()"><span class="delete-icon"> Cancel </span></a>
      </div>
    </div>
    <?php echo form_close(); ?>
  </div>
  <div class="col-md-12 carCatDelete" style="border:1px solid #ccc;padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
    <?=form_open("admin/booking_config/deletecarCat")?>
      <input  name="tablename" value="vbs_job_statut" type="hidden" >
      <input type="hidden" id="carCatdeletid" name="delete_car_id" value="">
      <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
      <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
      <button  class="btn"style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>
      <a class="btn" style="cursor: pointer;" onclick="cancelCarCat()">No</a>
    <?php echo form_close(); ?>
  </div>
</div>


<script type="text/javascript">
function carCatAdd()
{
  setupDivCarCat();
  $(".carCatAdd").show();
}

function cancelCarCat()
{
  setupDivCarCat();
  $(".ListCartCat").show();
}

function carCatEdit()
{
    var val = $('.chk-AddCarCat-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivCarCat();
        $(".carCatEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/booking_config/get_ajax_car_cat'; ?>',
                data: {'car_cat_id': val},
                success: function (result) {
                  // alert(result);
                  document.getElementsByClassName("carCatEditajax")[0].innerHTML = result;
                }
            });
}
function carcatidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivCarCat();
      $(".carCatEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/booking_config/get_ajax_car_cat'; ?>',
              data: {'car_cat_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("carCatEditajax")[0].innerHTML = result;
              }
          });
}
function carCatDelete()
{
  var val = $('.chk-AddCarCat-btn').val();
  if(val != "")
  {
  setupDivCarCat();
  $(".carCatDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivCarCat()
{
  $(".carCatEditEdit").hide();
  $(".carCatAdd").hide();
  $(".carCatEdit").hide();
  $(".ListCartCat").hide();
  $(".carCatDelete").hide();
}
$('input.chk-mainCarCat-template').on('change', function() {
  $('input.chk-mainCarCat-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-AddCarCat-btn').val(id);
  $('#carCatdeletid').val(id);
});

</script>
