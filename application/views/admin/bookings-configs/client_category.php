<div class="row">
  <div class="ListClientCat">
    <!-- <div class="col-md-12">
      <div class="page-action"style="float: right; margin-bottom:5px;" >
        <button class="btn btn-sm btn-default" onclick="ClientCatAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>&nbsp;
        <button class="btn btn-sm btn-default" onclick="ClientCatEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>&nbsp;
        <button class="btn btn-sm btn-default" onclick="ClientCatDelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button>&nbsp;
      </div>
    </div> -->
  <input type="hidden" class="chk-AddClientCat-btn" value="">
  <div class="col-md-12">
    <div class="module-body table-responsive">
        <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
            <thead>
            <tr>
              <th class="no-sort text-center"style="width:2%;">#</th>
              <th class="text-center" style="width:3% !important;"><?php echo $this->lang->line('id'); ?></th>
              <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('date'); ?></th>
              <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('time'); ?></th>
              <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('user'); ?></th>
              <th class="column-first_name" style="text-align:left !important;">Category</th>
              <th class="column-date"style="width:8%;"> Invoice Periode</th>
              <th class="column-time text-center"style="width:8%;">Statut</th>
            </tr>
            </thead>
            <tbody>
                <?php if (!empty($client_category)): ?>
                <?php $cnt=1; ?>
                <?php foreach($client_category as $key => $item):?>
                    <tr>
                        <td class="text-center"><input type="checkbox" class="chk-mainClientCat-template" data-input="<?=$item->id?>"></td>
                        <td class="text-center"><a href="javascript:void()" onclick="clientidEdit('<?=$item->id?>')"><?=create_timestamp_uid($item->created_at,$item->id);?></a></td>
                        <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                        <td class="text-center"><?=$item->time?></td>
                        <td class="text-center"><?= $this->bookings_config_model->getuser($item->user_id);?></td>
                        <td><?=$item->client_cat_name;?></td>
                        <td><?=$item->invoice_periode=='1'?'Week':'Month';?></td>
                        <td class="text-center"><?=$item->statut=='1'?'Show':'Hide';?></td>
                    </tr>
                    <?php $cnt++; ?>
                <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
        <br>
      </div>
  </div>
</div>
  <?=form_open("admin/booking_config/clientCatadd")?>
  <!-- <form action="<?=base_url();?>admin/booking_config/vatadd" class="" method="post"  enctype="multipart/form-data"> -->
  <div class="ClientCatadd" style="display: none;">
      <div class="col-md-3" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;"> Client Category Name </span>
            <input type="text" class="form-control" name="client_cat_name" placeholder="" value="">
        </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;"> Invoice Periode </span>
          <select class="form-control" name="invoice_periode" required>
            <option value="">Comptant</option>
            <option value="1">Week</option>
            <option value="2">Month</option>
          </select>
        </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;">Statut</span>
          <select class="form-control" name="client_cat_statut" required>
            <option value="">Statut</option>
            <option value="1">Show</option>
            <option value="2">Hide</option>
          </select>
        </div>
      </div>
      <div class="col-md-7">
        <button class="btn" style="float:right; margin-left:7px;" onclick="cancelClientCat()"><span class="delete-icon"> Cancel </span></button>
        <button  class="btn"style=" float:right;"><span class="save-icon"></span> Save </button>
      <?php echo form_close(); ?>
    </div>
  </div>
  <div class="clientCatEdit"  style="display:none;">
  <!-- <div id="clientCatEditview<?=$value->id?>" class="clientCatEditEdit" style="display: none;"> -->
    <?=form_open("admin/booking_config/clientCatedit")?>
    <div class="clientCatEditajax">
    </div>
    <div class="col-md-7">
    <a class="btn" style="float:right; margin-left:7px;" onclick="cancelClientCat()"><span class="delete-icon"> Cancel </span></a>
    <button  class="btn"style=" float:right;"><span class="save-icon"></span> Update </button>
    </div>
    <?php echo form_close(); ?>
  </div>
  <div class="col-md-12 clientCatDelete" style="border:1px solid #ccc;padding:20px  0px; margin-left: 15px;margin-top: 15px;display: none;">
    <?=form_open("admin/booking_config/deleteclientCat")?>
      <input  name="tablename" value="vbs_job_statut" type="hidden" >
      <input type="hidden" id="clientCatdeletid" name="delet_client_id" value="">
      <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
      <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
      <button  class="btn"style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>
      <a class="btn" style="cursor: pointer;" onclick="cancelClientCat()">No</a>
    <?php echo form_close(); ?>
  </div>
</div>


<script type="text/javascript">
function ClientCatAdd()
{
  setupDivClientCat();
  $(".ClientCatadd").show();
}

function cancelClientCat()
{
  setupDivClientCat();
  $(".ListClientCat").show();
}
// function ClientCatEdit()
// {
//   var val = $('.chk-AddClientCat-btn').val();
//   if(val != "")
//   {
//     setupDivClientCat();
//     $(".clientCatEdit").show();
//     $("#clientCatEditview"+val).show();
//   }else{
//     alert('Please select a row to edit.');
//   }
// }

function ClientCatEdit()
{
    var val = $('.chk-AddClientCat-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivClientCat();
        $(".clientCatEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/booking_config/get_ajax_client_cat'; ?>',
                data: {'client_cat_id': val},
                success: function (result) {
                  // alert(result);
                  document.getElementsByClassName("clientCatEditajax")[0].innerHTML = result;
                }
            });
}
function clientidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivClientCat();
      $(".clientCatEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/booking_config/get_ajax_client_cat'; ?>',
              data: {'client_cat_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("clientCatEditajax")[0].innerHTML = result;
              }
          });
}
function ClientCatDelete()
{
  var val = $('.chk-AddClientCat-btn').val();
  if(val != "")
  {
  setupDivClientCat();
  $(".clientCatDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivClientCat()
{
  $(".clientCatEditEdit").hide();
  $(".ClientCatadd").hide();
  $(".clientCatEdit").hide();
  $(".ListClientCat").hide();
  $(".clientCatDelete").hide();
}
$('input.chk-mainClientCat-template').on('change', function() {
  $('input.chk-mainClientCat-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-AddClientCat-btn').val(id);
  $('#clientCatdeletid').val(id);
});

</script>
