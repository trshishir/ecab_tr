<div class="col-md-12 Listcartype" style="border:1px solid #ccc;margin-bottom: 10px">
	<div class="filter-group">
		<div class="col-md-12">
			<div class="page-action"style="float: right; margin-top: 5px;margin-bottom:5px;">
				<button class="btn btn-sm btn-default" onclick="cartypeAdd()"><i class="fa fa-plus"></i>  <span class="add-icon"><?php echo $this->lang->line('add'); ?></span></button>&nbsp;
				<button class="btn btn-sm btn-default" onclick="ServiceEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon"><?php echo $this->lang->line('edit'); ?></span></button>&nbsp;
				<button class="btn btn-sm btn-default" onclick="cartypeDelete()"><i class="fa fa-trash"></i> <span class="delete-icon"><?php echo $this->lang->line('delete'); ?></span></button>&nbsp;
			</div>
		</div>
	</div>
	<input type="hidden" class="chk-AddCartype-btn" value="">
	<div class="col-md-12">
		<div class="module-body table-responsive">
			<table class="table table-bordered data-table dataTable" cellspacing="0" data-selected_id="">
					<thead>
						<tr>
							<th class="no-sort text-center"style="width:2%;">#</th>
							<th class="column-id" style="width:5%;"><?php echo $this->lang->line('id'); ?></th>
							<th class="column-id" style="width:6%;"><?php echo $this->lang->line('date'); ?></th>
							<th class="column-first_name" style="text-align:left !important;"><?php echo $this->lang->line('price_KM_of_Approach_Course'); ?> </th>
							<th class="column-date"> <?php echo $this->lang->line('price_KM_ROUTE'); ?></th>
							<th class="column-time"><?php echo $this->lang->line('price_KM_path_Return'); ?></th>
							<th class="column-time"><?php echo $this->lang->line('waiting'); ?></th>
							<th class="column-time"><?php echo $this->lang->line('car_seat_baby_seat'); ?></th>
							<th class="column-time"><?php echo $this->lang->line('baggage'); ?></th>
							<th class="column-time"><?php echo $this->lang->line('pickup_fee'); ?></th>
						</tr>
					</thead>
					<tbody>

							<?php foreach($price_car_type as $key => $item):?>
									<tr>
										<td class="text-center">
												<input type="checkbox" class="chk-mainCartype-template" data-input="<?=$item->id?>">
										</td>
											<td class="text-center"><?=create_timestamp_uid($item->created_at,$item->id);?></td>
											<td class="text-center"><?=from_unix_date($item->created_at)?></td>
											<td><?=$item->price_km_approche?></td>
											<td><?=$item->price_trajet;?></td>
											<td><?=$item->price_trajet_retour;?></td>
											<td><?=$item->attente;?></td>
											<td><?=$item->siege_bebe;?></td>
											<td><?=$item->bagages;?></td>
											<td><?=$item->pickup_fee;?></td>
									</tr>
								
							<?php endforeach; ?>
					</tbody>
			</table>
			<br>
		</div>
	</div>
</div>
<div class="col-md-12 DeleteCartype" style="border:1px solid #ccc;padding:20px  0px; display: none;">
	<?=form_open("admin/booking_config/cartypepricedelete")?>
		<!-- <input  name="tablename" value="vbs_job_statut" type="hidden" > -->
		<input type="hidden" id="cartypedeletid" name="delet_cartype_id" value="">
		<div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
		<button  class="btn"style=" float:left;margin-right: 10px;"><span class="save-icon"></span> <?php echo $this->lang->line('yes'); ?> </button>
		<a class="btn" style="cursor: pointer;" onclick="cancelCartype()"><?php echo $this->lang->line('no'); ?></a>
	<?php echo form_close(); ?>
</div>
<div class="col-md-12 AddCartype" style="border:1px solid #ccc;margin-bottom: 10px; display:none;">
					   <?=form_open("admin/booking_config/cartypepriceadd")?>
						 <div class="row" style="margin-top: 10px; margin-left:5px;">
									<div class="col-md-2">
										<div class="form-group">
												<span style="font-weight: bold;"><?php echo $this->lang->line('type_name'); ?></span>
												<input required="" class="form-control" name="type_name" type="text" value="" >
											</div>
										</div>
									<div class="col-md-2">
										<div class="form-group">
												<span style="font-weight: bold;"><?php echo $this->lang->line('adaptation'); ?></span>
												<select class="form-control" name="udaptation">
													<option value="1">Normal</option>
													<option value="2">TPMR</option>
												</select>
										</div>
									</div>
										<div class="col-md-2">
											<div class="form-group">
													<span style="font-weight: bold;"><?php echo $this->lang->line('passengers'); ?></span>
													<select name="no_of_persons[]" class="form-control">
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
													</select>
											</div>
										</div>
										<div class="col-md-1">
											<div class="form-group">
													<span style="font-weight: bold;"><?php echo $this->lang->line('luggages'); ?></span>
													<select name="no_of_lugages[]" class="form-control">
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
													</select>
											</div>
										</div>
										<div class="col-md-1" >
											<div class="form-group">
													<span style="font-weight: bold;"><?php echo $this->lang->line('weelchairs'); ?></span>
													<select class="form-control" name="weelchairs[]">
														<option <?=$other_data['2']== '0' ? 'selected' : ''?> value="0">0</option>
														<option <?=$other_data['2']== '1' ? 'selected' : ''?> value="1">1</option>
														<option <?=$other_data['2']== '2' ? 'selected' : ''?> value="2">2</option>
														<option <?=$other_data['2']== '3' ? 'selected' : ''?> value="3">3</option>
														<option <?=$other_data['2']== '4' ? 'selected' : ''?> value="4">4</option>
													</select>
											</div>
										</div>
										<div class="col-md-2" >
											<div class="form-group">
													<span style="font-weight: bold;"><?php echo $this->lang->line('baby_sets'); ?></span>
													<select name="no_of_babyseat[]" class="form-control">
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
														<option value="8">8</option>
													</select>
											</div>
										</div>
										<div class="col-md-1" >
											<div class="form-group">
													<span style="font-weight: bold;"><?php echo $this->lang->line('animals'); ?></span>
													<select name="no_of_animal[]" class="form-control">
														<option value="0">0</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
													</select>
											</div>
										</div>
										<div class="col-md-1">
											 <br>
												<button type="button" style="min-width:35px !important;" class="btn AddNewRow"><i class="fa fa-plus"></i></button>
										</div>
								</div>
								<div id="AppendDataAdd">
								</div>
							<div class="row" style="margin-top: 10px;">
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('price_KM_of_Approach_Course'); ?></span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
												<input type="text"  name="price_km_approche"class="form-control" value="" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4	">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('night_surcharge'); ?></span>
										</div>
										<div class="col-md-2" style="padding: 0px;">
											<select style="width:100%" class="form-control" name="" required="">
												<option value="%">%</option>
												<option value="Fix">Fix</option>
											</select>
										</div>
										<div class="col-md-4" style="padding: 0px;">
											<input type="text" name="majoration_de_nuit"value=""  class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('packed_lunch'); ?>	</span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="panier_repas"value=""  class="form-control" required="">
										</div>
									</div>
								</div>
							</div>
							<div class="row" style="margin-top: 10px;">
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('price_KM_ROUTE'); ?></span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
											<input type="text" name="price_trajet"value=""  class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('holiday_day_supplement'); ?> </span>
										</div>
										<div class="col-md-2" style="padding: 0px;">
											<select style="width:100%" class="form-control" name="" required="">
												<option value="%">%</option>
												<option value="Fix">Fix</option>
											</select>
										</div>
										<div class="col-md-4" style="padding: 0px;">
											<input type="text" name="supplement_jour_ferie"value=""  class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('compensatory_rest'); ?></span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="repos_compensateur"value=""  class="form-control" required="">
										</div>
									</div>
								</div>
							</div>
						   <div class="row" style="margin-top: 10px;">
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('price_KM_path_Return'); ?></span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
											<input type="text" name="price_trajet_retour" value="" class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('supplement_may_1_and_december_25'); ?></span>
										</div>
										<div class="col-md-2" style="padding: 0px;">
											<select style="width:100%" class="form-control" name="" required="">
												<option value="%">%</option>
												<option value="Fix">Fix</option>
											</select>
										</div>
										<div class="col-md-4"  style="padding: 0px;">
											<input type="text" name="supplement_1er" value="" class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('rest'); ?> </span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="repos"value="" class="form-control" required="">
										</div>
									</div>
								</div>
							</div>
						   <div class="row" style="margin-top: 10px;">
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('waiting'); ?></span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
											<input type="text" name="attente" value="" class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('pet'); ?></span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="animal_de_cmpanie"value=""  class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('passenger'); ?></span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="passager"value=""  class="form-control" required="">
										</div>
									</div>
								</div>
							</div>
						   <div class="row" style="margin-top: 10px;">
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('car_seat_baby_seat'); ?></span>
										</div>
											<div class="col-md-5" style="padding: 0px;">
											<input type="text" name="siege_bebe"value=""  class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('accompanying_person'); ?></span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="accompagnateur"value="" class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('armchair'); ?></span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="fauteuil" value="" class="form-control" required="">
										</div>
									</div>
								</div>
							</div>
							<div class="row" style="margin-top: 10px;">
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('baggage'); ?></span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
											<input type="text" name="bagages"value="" class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('baby_seat_rental'); ?></span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="location_de_siege_BEBE" value="" class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('armchair_rental'); ?></span>
										</div>
											<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="location_fauteuil" value="" class="form-control" required="">
										</div>
									</div>
								</div>
							</div>
						   <div class="row" style="margin-top: 10px;">
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('pickup_fee'); ?></span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
											<input type="text" name="pickup_fee" value="" class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('minimum_fee'); ?></span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="minimum_fee"value=""  class="form-control" required="">
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="row" style="margin-bottom: 10px;margin-top: 20px;">
								<div class="col-md-12">
									<div class="form-group">
										<a class="btn" style="float:right; margin-right:3.6%;" onclick="cancelCartype()"><span class="delete-icon"> Cancel </span></a>
										<button  class="btn"style="margin-right:10px; float:right;"><span class="save-icon"></span> <?php echo $this->lang->line('save'); ?> </button>
									</div>
								</div>
							</div>
						<?php echo form_close(); ?>
	</div>

<script type="text/javascript">
function cartypeAdd()
{
	setupDivCartype();
	$(".AddCartype").show();
}
function cancelCartype()
{
	setupDivCartype();
	$(".Listcartype").show();
}
function setupDivCartype()
{
	$(".Listcartype").hide();
	$(".AddCartype").hide();
	$(".DeleteCartype").hide();
}
function cartypeDelete()
{
	var val = $('.chk-AddCartype-btn').val();
	if(val != "")
	{
	setupDivCartype();
	$(".DeleteCartype").show();
	}else{
		alert('Please select a row to delete.');
	}
}
$('input.chk-mainCartype-template').on('change', function() {
	$('input.chk-mainCartype-template').not(this).prop('checked', false);
	var id = $(this).attr('data-input');
	$('.chk-AddCartype-btn').val(id);
	$('#cartypedeletid').val(id);
});
var rowadd = 1;
$('body').delegate('.DeleteAddedRow','click',function(){
	 var id = $(this).attr('id');
	 $('#RemoveAddedRow'+id).remove();
});

$('.AddNewRow').click(function(){
	var html= '<div id="RemoveAddedRow'+rowadd+'"><div class="row" style="margin-top: 10px; margin-left:5px;">\n\
				<div class="col-md-2"></div>\n\
				<div class="col-md-2"></div>\n\
				<div class="col-md-2">\n\
					<div class="form-group">\n\
							<span style="font-weight: bold;">Passengers</span>\n\
							<select name="no_of_persons[]" class="form-control">\n\
								<option value="1">1</option>\n\
								<option value="2">2</option>\n\
								<option value="3">3</option>\n\
								<option value="4">4</option>\n\
								<option value="5">5</option>\n\
								<option value="6">6</option>\n\
								<option value="7">7</option>\n\
							</select>\n\
					</div>\n\
				</div>\n\
				<div class="col-md-1">\n\
					<div class="form-group">\n\
							<span style="font-weight: bold;">Lugages</span>\n\
							<select name="no_of_lugages[]" class="form-control">\n\
								<option value="1">1</option>\n\
								<option value="2">2</option>\n\
								<option value="3">3</option>\n\
								<option value="4">4</option>\n\
								<option value="5">5</option>\n\
								<option value="6">6</option>\n\
								<option value="7">7</option>\n\
							</select>\n\
					</div>\n\
				</div>\n\
				<div class="col-md-1">\n\
					<div class="form-group">\n\
							<span style="font-weight: bold;">Weelchairs</span>\n\
							<select class="form-control" name="weelchairs[]">\n\
								<option value="0">0</option>\n\
								<option value="1">1</option>\n\
								<option value="2">2</option>\n\
								<option value="3">3</option>\n\
								<option value="4">4</option>\n\
							</select>\n\
					</div>\n\
				</div>\n\
				<div class="col-md-2">\n\
					<div class="form-group">\n\
							<span style="font-weight: bold;">Baby Sets</span>\n\
							<select name="no_of_babyseat[]" class="form-control">\n\
								<option value="1">1</option>\n\
								<option value="2">2</option>\n\
								<option value="3">3</option>\n\
								<option value="4">4</option>\n\
								<option value="5">5</option>\n\
								<option value="6">6</option>\n\
								<option value="7">7</option>\n\
								<option value="8">8</option>\n\
							</select>\n\
					</div>\n\
				</div>\n\
				<div class="col-md-1">\n\
					<div class="form-group">\n\
							<span style="font-weight: bold;">Animals</span>\n\
							<select name="no_of_animal[]" class="form-control">\n\
								<option value="0">0</option>\n\
								<option value="1">1</option>\n\
								<option value="2">2</option>\n\
								<option value="3">3</option>\n\
								<option value="4">4</option>\n\
							</select>\n\
					</div>\n\
				</div>\n\
				<div class="col-md-1">\n\
						<br>\n\
					<button type="button" style="min-width: 35px !important;" id='+rowadd+' class="btn DeleteAddedRow"><i class="fa fa-minus"></i></button>\n\
				</div>\n\
		</div></div>';

		$('#AppendDataAdd').append(html);
		rowadd++;
});
</script>
<script type="text/javascript">

var row = <?=$count?>;
$('body').delegate('.DeleteRow','click',function(){
	 var id = $(this).attr('id');
	 $('#RemoveRow'+id).remove();
});

$('.AddRow').click(function(){
	var html= '<div id="RemoveRow'+row+'"><div class="row" style="margin-top: 10px; margin-left:5px;">\n\
				<div class="col-md-2"></div>\n\
				<div class="col-md-2"></div>\n\
				<div class="col-md-2">\n\
					<div class="form-group">\n\
							<span style="font-weight: bold;">Passengers</span>\n\
							<select name="no_of_persons[]" class="form-control">\n\
								<option value="1">1</option>\n\
								<option value="2">2</option>\n\
								<option value="3">3</option>\n\
								<option value="4">4</option>\n\
								<option value="5">5</option>\n\
								<option value="6">6</option>\n\
								<option value="7">7</option>\n\
							</select>\n\
					</div>\n\
				</div>\n\
				<div class="col-md-2">\n\
					<div class="form-group">\n\
							<span style="font-weight: bold;">Lugages</span>\n\
							<select name="no_of_lugages[]" class="form-control">\n\
								<option value="1">1</option>\n\
								<option value="2">2</option>\n\
								<option value="3">3</option>\n\
								<option value="4">4</option>\n\
								<option value="5">5</option>\n\
								<option value="6">6</option>\n\
								<option value="7">7</option>\n\
							</select>\n\
					</div>\n\
				</div>\n\
				<div class="col-md-1">\n\
					<div class="form-group">\n\
							<span style="font-weight: bold;">Weelchairs</span>\n\
							<select class="form-control" name="weelchairs[]">\n\
								<option value="0">0</option>\n\
								<option value="1">1</option>\n\
								<option value="2">2</option>\n\
								<option value="3">3</option>\n\
								<option value="4">4</option>\n\
							</select>\n\
					</div>\n\
				</div>\n\
				<div class="col-md-1">\n\
					<div class="form-group">\n\
							<span style="font-weight: bold;">Baby Sets</span>\n\
							<select name="no_of_babyseat[]" class="form-control">\n\
								<option value="1">1</option>\n\
								<option value="2">2</option>\n\
								<option value="3">3</option>\n\
								<option value="4">4</option>\n\
								<option value="5">5</option>\n\
								<option value="6">6</option>\n\
								<option value="7">7</option>\n\
								<option value="8">8</option>\n\
							</select>\n\
					</div>\n\
				</div>\n\
				<div class="col-md-1">\n\
					<div class="form-group">\n\
							<span style="font-weight: bold;">Animals</span>\n\
							<select name="no_of_animal[]" class="form-control">\n\
								<option value="0">0</option>\n\
								<option value="1">1</option>\n\
								<option value="2">2</option>\n\
								<option value="3">3</option>\n\
								<option value="4">4</option>\n\
							</select>\n\
					</div>\n\
				</div>\n\
				<div class="col-md-1">\n\
						<br>\n\
					<button type="button" style="min-width: 35px !important;" id='+row+' class="btn DeleteRow"><i class="fa fa-minus"></i></button>\n\
				</div>\n\
		</div></div>';

		$('#AppendData').append(html);
		row++;
});


</script>
