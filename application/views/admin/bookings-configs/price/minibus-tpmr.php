<div class="col-md-12 Listminibustpmr" style="border:1px solid #ccc;margin-bottom: 10px">
	<div class="filter-group">
		<div class="col-md-12">
			<div class="page-action"style="float: right; margin-top: 5px;margin-bottom:5px;">
				<button class="btn btn-sm btn-default" onclick="minibustpmrAdd()"><i class="fa fa-plus"></i>  <span class="add-icon"><?php echo $this->lang->line('add'); ?></span></button>&nbsp;
				<button class="btn btn-sm btn-default" onclick="minibustpmrEdit()"><i class="fa fa-pencil"></i <span class="edit-icon"><?php echo $this->lang->line('edit'); ?></span></button>&nbsp;
				<button class="btn btn-sm btn-default" onclick="minibustpmrDelete()"><i class="fa fa-trash"></i> <span class="delete-icon"><?php echo $this->lang->line('delete'); ?></span></button>&nbsp;
			</div>
		</div>
	</div>
	<input type="hidden" class="chk-AddMinibustpmr-btn" value="">
	<div class="col-md-12">
		<div class="module-body table-responsive">
			<table class="table table-bordered data-table dataTable" cellspacing="0" data-selected_id="">
					<thead>
						<tr>
							<th class="no-sort text-center"style="width:2%;">#</th>
							<th class="column-id" style="width:5%;"><?php echo $this->lang->line('id'); ?></th>
							<th class="column-id" style="width:6%;"><?php echo $this->lang->line('date'); ?></th>
							<th class="column-first_name" style="text-align:left !important;"><?php echo $this->lang->line('price_KM_of_Approach_Course'); ?> </th>
							<th class="column-date"> <?php echo $this->lang->line('price_KM_ROUTE'); ?></th>
							<th class="column-time"><?php echo $this->lang->line('price_KM_path_Return'); ?></th>
							<th class="column-time"><?php echo $this->lang->line('waiting'); ?></th>
							<th class="column-time"><?php echo $this->lang->line('car_seat_baby_seat'); ?></th>
							<th class="column-time"><?php echo $this->lang->line('baggage'); ?></th>
							<th class="column-time"><?php echo $this->lang->line('pickup_fee'); ?></th>
						</tr>
					</thead>
					<tbody>
							<?php $cnt=1; ?>
							<?php foreach($price_minibus_tpmr as $key => $item):?>
									<tr>
										<td class="text-center">
												<input type="checkbox" class="chk-mainMinibustpmr-template" data-input="<?=$item->id?>">
										</td>
											<td class="text-center"><?=create_timestamp_uid($item->created_at,$item->id);?></td>
											<td class="text-center"><?=from_unix_date($item->created_at)?></td>
											<td><?=$item->price_km_approche?></td>
											<td><?=$item->price_trajet;?></td>
											<td><?=$item->price_trajet_retour;?></td>
											<td><?=$item->attente;?></td>
											<td><?=$item->siege_bebe;?></td>
											<td><?=$item->bagages;?></td>
											<td><?=$item->pickup_fee;?></td>
									</tr>
									<?php $cnt++; ?>
							<?php endforeach; ?>
					</tbody>
			</table>
			<br>
		</div>
	</div>
</div>
<div class="col-md-12 DeleteMinibustpmr" style="border:1px solid #ccc;padding:20px  0px; display: none;">
	<?=form_open("admin/booking_config/minibustpmrpricedelete")?>
		<!-- <input  name="tablename" value="vbs_job_statut" type="hidden" > -->
		<input type="hidden" id="minibustpmrdeletid" name="delet_minibustpmr_id" value="">
		<div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
		<button  class="btn"style=" float:left;margin-right: 10px;"><span class="save-icon"></span> <?php echo $this->lang->line('yes'); ?> </button>
		<a class="btn" style="cursor: pointer;" onclick="cancelMinibustpmr()"><?php echo $this->lang->line('no'); ?></a>
	<?php echo form_close(); ?>
</div>
<div class="col-md-12 AddMinibustpmr" style="border:1px solid #ccc;margin-bottom: 10px; display:none;">
					   <?=form_open("admin/booking_config/minibustpmrpriceadd")?>
                <!-- <input type="hidden" name="price_minibus_id" value="<?=$value->id?>"> -->
							<div class="row" style="margin-top: 10px;">
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('price_KM_of_Approach_Course'); ?></span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
												<input type="text"  name="price_km_approche"class="form-control" value="" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4	">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('night_surcharge'); ?></span>
										</div>
										<div class="col-md-2" style="padding: 0px;">
											<select style="width:100%" class="form-control" name="" required="">
												<option value="%">%</option>
												<option value="Fix">Fix</option>
											</select>
										</div>
										<div class="col-md-4" style="padding: 0px;">
											<input type="text" name="majoration_de_nuit"value=""  class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('packed_lunch'); ?>	</span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="panier_repas"value=""  class="form-control" required="">
										</div>
									</div>
								</div>
							</div>
							<div class="row" style="margin-top: 10px;">
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('price_KM_ROUTE'); ?></span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
											<input type="text" name="price_trajet"value=""  class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('holiday_day_supplement'); ?> </span>
										</div>
										<div class="col-md-2" style="padding: 0px;">
											<select style="width:100%" class="form-control" name="" required="">
												<option value="%">%</option>
												<option value="Fix">Fix</option>
											</select>
										</div>
										<div class="col-md-4" style="padding: 0px;">
											<input type="text" name="supplement_jour_ferie"value=""  class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('compensatory_rest'); ?></span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="repos_compensateur"value=""  class="form-control" required="">
										</div>
									</div>
								</div>
							</div>
						   <div class="row" style="margin-top: 10px;">
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('price_KM_path_Return'); ?></span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
											<input type="text" name="price_trajet_retour" value="" class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('supplement_may_1_and_december_25'); ?></span>
										</div>
										<div class="col-md-2" style="padding: 0px;">
											<select style="width:100%" class="form-control" name="" required="">
												<option value="%">%</option>
												<option value="Fix">Fix</option>
											</select>
										</div>
										<div class="col-md-4"  style="padding: 0px;">
											<input type="text" name="supplement_1er" value="" class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('rest'); ?> </span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="repos"value="" class="form-control" required="">
										</div>
									</div>
								</div>
							</div>
						   <div class="row" style="margin-top: 10px;">
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('waiting'); ?></span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
											<input type="text" name="attente" value="" class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('pet'); ?></span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="animal_de_cmpanie"value=""  class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('passenger'); ?></span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="passager"value=""  class="form-control" required="">
										</div>
									</div>
								</div>
							</div>
						   <div class="row" style="margin-top: 10px;">
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('car_seat_baby_seat'); ?></span>
										</div>
											<div class="col-md-5" style="padding: 0px;">
											<input type="text" name="siege_bebe"value=""  class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('accompanying_person'); ?></span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="accompagnateur"value="" class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('armchair'); ?></span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="fauteuil" value="" class="form-control" required="">
										</div>
									</div>
								</div>
							</div>
							<div class="row" style="margin-top: 10px;">
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('baggage'); ?></span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
											<input type="text" name="bagages"value="" class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('baby_seat_rental'); ?></span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="location_de_siege_BEBE" value="" class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('armchair_rental'); ?></span>
										</div>
											<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="location_fauteuil" value="" class="form-control" required="">
										</div>
									</div>
								</div>
							</div>
						   <div class="row" style="margin-top: 10px;">
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('pickup_fee'); ?></span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
											<input type="text" name="pickup_fee" value="" class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span style="font-weight: bold;"><?php echo $this->lang->line('minimum_fee'); ?></span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="minimum_fee"value=""  class="form-control" required="">
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="row" style="margin-bottom: 10px;margin-top: 20px;">
								<div class="col-md-12">
									<div class="form-group">
										<a class="btn" style="float:right; margin-right:3.6%;" onclick="cancelMinibustpmr()"><span class="delete-icon"> <?php echo $this->lang->line('cancel'); ?> </span></a>
										<button  class="btn"style="margin-right:10px; float:right;"><span class="save-icon"></span> <?php echo $this->lang->line('save'); ?> </button>
									</div>
								</div>
							</div>
						<?php echo form_close(); ?>
	</div>

<div class="col-md-12 minibustpmrEdit" style="border:1px solid #ccc;margin-bottom: 10px; display:none">
	<?=form_open("admin/booking_config/minibustpmrpriceedit")?>
	<div class="minibustpmrEditajax">
	</div>

	<div class="clearfix"></div>
	<div class="row" style="margin-bottom: 10px;margin-top: 20px;">
		<div class="col-md-12">
			<div class="form-group">
				<a class="btn" style="margin-right:3.6%; float:right; margin-left:7px;" onclick="cancelMinibustpmr()"><span class="delete-icon"> <?php echo $this->lang->line('cancel'); ?> </span></a>
				<button  class="btn"style=" float:right;"><span class="save-icon"></span> <?php echo $this->lang->line('update'); ?> </button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function minibustpmrAdd()
	{
		setupDivMinibustpmr();
		$(".AddMinibustpmr").show();
	}
	function cancelMinibustpmr()
	{
		setupDivMinibustpmr();
		$(".Listminibustpmr").show();
	}
	function setupDivMinibustpmr()
	{
		$(".Listminibustpmr").hide();
		$(".AddMinibustpmr").hide();
		$(".DeleteMinibustpmr").hide();
		$(".minibustpmrEdit").hide();
	}
	function minibustpmrEdit()
	{
			var val = $('.chk-AddMinibustpmr-btn').val();
			if (val=='')
			{
					alert("Please select record!");
					return false;
			}
					setupDivMinibustpmr();
					$(".minibustpmrEdit").show();
					$.ajax({
									type: "GET",
									url: '<?php echo base_url().'admin/booking_config/get_ajax_price_minibustpmr'; ?>',
									data: {'minibustpmr_id': val},
									success: function (result) {
										// alert(result);
										document.getElementsByClassName("minibustpmrEditajax")[0].innerHTML = result;
									}
							});
	}
	function minibustpmrDelete()
	{
		var val = $('.chk-AddMinibustpmr-btn').val();
		if(val != "")
		{
		setupDivMinibustpmr();
		$(".DeleteMinibustpmr").show();
		}else{
			alert('Please select a row to delete.');
		}
	}
	$('input.chk-mainMinibustpmr-template').on('change', function() {
		$('input.chk-mainMinibustpmr-template').not(this).prop('checked', false);
		var id = $(this).attr('data-input');
		$('.chk-AddMinibustpmr-btn').val(id);
		$('#minibustpmrdeletid').val(id);
	});
</script>
