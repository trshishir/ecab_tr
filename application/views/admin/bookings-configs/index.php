<?php $locale_info = localeconv(); ?>
<style>
    .table-filter .dpo {
        max-width: 62px;
    }
  input[type="file"]{
    border: 1px solid #ccc;
    width: 100%;
    width: 200px;
    padding: 3px 0px 3px 5px;
    /* background: #fff; */
    background: #fffdfd;
    }
    input[type="file"]:hover{
            box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2), 0 0 3px rgba(0, 0, 0, 0.2);
    }
    .table-filter span {
        margin: 4px 2px 0 3px;
    }
    .table-filter input[type="number"]{
        max-width: 48px;
    }
    @media only screen and (min-width: 1400px){
        .table-filter input, .table-filter select{
            max-width: 6% !important;
        }
        .table-filter select{
            max-width: 85px !important;
        }
        .table-filter .dpo {
            max-width: 90px !important;
        }
    }
  .nav-tabs > li.active {
    background: linear-gradient(#ffffff, #ffffff 25%, #d0d0d0) !important;
    border-bottom: none;
    }
  .nav-tabs > li {
    background-image: linear-gradient(to bottom, #fff 0, #e0e0e0 100%);
    border-left: 1px solid #ccc !important;
    height: 55px;
    margin: 0px !important;
    }

    .nav-tabs > li > a {

    color: #616161 !important;
    font-size: 12px;
    padding-left: 10px;
    display: block !important;
    width: 100% !important;
    height: 100% !important;
    text-transform: uppercase;
    font-weight: bold;
    transition: 0.2s;
    outline: none;
    border: none;
    border-radius: 0px;
}
    }
    #seracTab {
        background: linear-gradient(to bottom, #ffffff 0%, #f6f6f6 47%, #ddd 100%);
        height: 45px;
        margin: 0px 0px 20px 0px;
        border-radius: 4px;
        border: solid 1px #efefef;
        padding: 4px;
        text-align: center;
    }
    thead{
        background: transparent;
        border-bottom: 1px solid #111111;
        font-family: inherit;
        font-size: 100%;
        font-style: inherit;
        font-weight: inherit;
        line-height: 100%;
        vertical-align: baseline;
    }
    th{
        text-shadow: 0 -1px 0 #ffffff;
    }
    .tab-pane{
        padding: 30px 30px 30px 14px;
    }
    table, .col-md-12{
        padding: 0px;
    }
   /*  table.dataTable, table.dataTable th, table.dataTable td {
       -webkit-box-sizing: content-box;
       -moz-box-sizing: content-box;
       box-sizing: content-box;
       padding: 0px;

   } */
    .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
    background: #eee;
    color: #2c2c2c;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
    }
    .table tbody tr td{
        background-color: white;
    }

    .table{
    padding: 0px;
    margin: 0 auto;
    clear: both;
    border-collapse: separate;
    border-spacing: 0;
    font-size: 12px !important
    }
    .table thead > tr > th{
        background: linear-gradient(#ffffff, #ffffff 25%, #d0d0d0) !important;
        padding: 15px;
        color: #2c2c2c;
        font-weight: bolder;
        font-size: 13px;
    }
    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        border-bottom-width: 1px;
    }
    td{
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        background-color: #f1f1f1;
    }
    input[type="text"],input[type="date"]{
    float: left;
    width: 100%;
    height: 32px !important;
    padding: 2px !important;
    margin-right: 2px !important;
    font-size: 12px;
    }
    input[type="date"]{
        width: 100%;
    }
    input.btn, .btn,button.btn {
    min-height: 28px;
    border: 1px solid #bbb;
    min-width: 80px;
    background-image: linear-gradient(to bottom, #fff 0, #e0e0e0 100%);
    background-repeat: repeat-x;
    border-color: #dbdbdb;
    text-shadow: 0 1px 0 #fff;
    border-color: #ccc;
    }

  /*   button.btn, .btn{
      min-height: 20px;
      border: 1px solid #bbb;
      min-width: 50px;
  } */
.nav-tabs>li>a:hover{
    color: #333;
    background-color: #d4d4d4;
    border-color: #8c8c8c;
}
button.btn:hover, input.btn:hover{
    background-color: #e0e0e0;
    background-position: 0 -15px;
}
</style>
<style media="screen">
select {

/* styling */
padding: 5px !important;
background: url(<?php echo base_url();?>/assets/arrow.png) no-repeat right #ffffff !important;
    -webkit-appearance: none;
    /* background-size:
        25px 25px,
        25px 25px,
        13px 13.5em !important; */
}
.dataTables_paginate{
  display: inline !important;
}
.dataTables_filter{
  float: left !important;
}
.dataTables_filter label{
  float: left !important;
  margin-left:2px;
}
.dataTables_filter label input[type="search"]{
  float: left !important;
  max-width: 150px !important;
}

</style>
<?php $active_status =  $this->session->flashdata('alert');?>
<?php if (!isset($active_status['price_stu_id']) ) {
  $active_status['price_stu_id']='';
} ?>
<?php if (!isset($active_status['status_id'])) {
  $active_status['status_id']='';
} ?>
<?php if ($active_status['status_id']!='') { $status=$active_status['status_id']; $tabstatus=$active_status['status_id']; }else{$status='1'; $tabstatus='1';} ?>
<?php if ($active_status['price_stu_id']!='') { $price_stu=$active_status['price_stu_id']; }else{$price_stu='1';} ?>
<div class="col-md-12"><!--col-md-10 padding white right-p-->
  <?php $this->load->view('admin/common/breadcrumbs');?>
    <?php $this->load->view('admin/common/alert');?>
      <?php echo $this->session->flashdata('message'); ?>
        <div class="col-md-12"  id="ShowTabs" style="margin-bottom: 10px; margin-left:20px;">
           	<ul class="nav nav-tabs responsive">
										<li <?=$status==1 ? 'class="active"' : ''?>><a href="#VAT" class="vat_E" role="tab" data-toggle="tab">VAT</a></li>
										<li <?=$status==2 ? 'class="active"' : ''?>><a href="#DISCOUNT" class="discount_E" role="tab" data-toggle="tab">DISCOUNT</a></li>
										<li <?=$status==3 ? 'class="active"' : ''?>><a href="#SERVICE_CATEGORY" class="serviceCar_E" role="tab" data-toggle="tab">SERVICE CATEGORY</a></li>
										<li <?=$status==4 ? 'class="active"' : ''?>><a href="#SERVICES" role="tab" class="service_E" data-toggle="tab">SERVICES</a></li>
										<li <?=$status==5 ? 'class="active"' : ''?>><a href="#CLIENT_CATEGORY" class="clientCat_E" role="tab" data-toggle="tab">CLIENT CATEGORY</a></li>
                                        <li <?=$status==6 ? 'class="active"' : ''?>><a href="#CAR_CATEGORY" class="carCat_E" role="tab" data-toggle="tab">CAR CATEGORY</a></li>
										<li <?=$status==9 ? 'class="active"' : ''?>><a href="#RIDE_CATEGORY" class="rideCat_E" role="tab" data-toggle="tab">RIDE CATEGORY</a></li>
										<li <?=$status==7 ? 'class="active"' : ''?>><a href="#POI" class="price_E" role="tab" data-toggle="tab">POI</a></li>
										<li <?=$status==8 ? 'class="active"' : ''?>><a href="#PACKAGES" class="package_E" role="tab" data-toggle="tab">PACKAGES</a></li>
							</ul>
              <div class="tab-content responsive" style="width:100%;display: table;">
                    <!--status tab starts-->
                    <div class="tab-pane fade <?=$status==1? 'in active':''?>" id="VAT">
                        <div class="row">
                          <div class="Listvat">
													<input type="hidden" class="chk-Addvat-btn" value="">
                          <div class="col-md-12 text-right">
                          <div style="margin-bottom: 5px;"><a href="javascript:void()" class=" Vataddbtn btn btn-sm btn-default"><i class="fa fa-plus"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Add</font></font></a>
                          <a href="javascript:void()" onclick="VatEdit()" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Edit</font></font></a>
                          <a href="javascript:void()" onclick="VatDelete()" class="btn btn-sm btn-default"><i class="fa fa-trash"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Delete</font></font></a></div>
														</div>
                          <div class="col-md-12 ">
                          <div class="module-body table-responsive">
                              <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" aria-describedby="example_info" cellspacing="0" width="100%" data-selected_id>
                                  <thead>
                                  <tr>
                                    <th class="no-sort text-center"style="width:2% !important;">#</th>
                                    <th class="text-center" style="width:3% !important;"><?php echo $this->lang->line('id'); ?></th>
                                    <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('date'); ?></th>
                                    <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('time'); ?></th>
                                    <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('user'); ?></th>
                                    <th class="column-first_name" style="text-align:left !important;"><?php echo $this->lang->line('name'); ?></th>
                                    <th class="text-center"style="width:8%;">Vat(%)</th>
                                    <th class="text-center"style="width:8%;">Statut</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                      <?php $cnt=1; ?>
                                      <?php if (!empty($vat_data)): ?>
                                      <?php foreach($vat_data as $key => $item):?>
                                          <tr>
                                            <td class="text-center">
                                                <input type="checkbox" class="chk-mainvat-template" data-input="<?=$item->id?>">
                                            </td>
                                              <td class="text-center"><a href="javascript:void()" onclick="VatidEdit('<?=$item->id?>')"><?=create_timestamp_uid($item->created_at,$item->id);?></a></td>
                                              <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                                              <td class="text-center"><?=$item->time?></td>
                                              <td class="text-center"><?= $this->bookings_config_model->getuser($item->user_id);?></td>
                                              <td><?=$item->name_of_var;?></td>
                                              <td class="text-center"><?=$item->vat; ?></td>
                                              <td class="text-center"><?=$item->statut=='1'?'Show':'Hide';?></td>
                                          </tr>
                                          <?php $cnt++; ?>
                                      <?php endforeach; ?>
                                      <?php endif; ?>
                                  </tbody>
                              </table>
                              <br>
                            </div>
                          </div>
												</div>
                        <?=form_open("admin/booking_config/vatadd")?>
                        <!-- <form action="<?=base_url();?>admin/booking_config/vatadd" class="" method="post"  enctype="multipart/form-data"> -->
                        <div class="Vatadd" style="display: none;" >
                          <div class="col-md-3" style="margin-top: 5px;">
                            <div class="form-group">
                              <span style="font-weight: bold;">Name Of Vat</span>
                                <input type="text" class="form-control" name="name_of_vat" placeholder="" value="">
                            </div>
                          </div>
                          <div class="col-md-2" style="margin-top: 5px;">
                            <div class="form-group">
                              <span style="font-weight: bold;">Vat%</span>
                                <input type="text" class="form-control" name="vat" placeholder="" value="">
                            </div>
                          </div>
                          <div class="col-md-2" style="margin-top: 5px;">
                            <div class="form-group">
                              <span style="font-weight: bold;">Statut</span>
                              <select class="form-control" name="vat_statut" required>
                                <option value="">Statut</option>
                                <option value="1">Show</option>
                                <option value="2">Hide</option>
                              </select>
                            </div>
                            <button class="btn" style="float:right; margin-left:7px;" onclick="cancelVat()"><span class="delete-icon"> Cancel </span></button>
                            <button  class="btn"style=" float:right;"><span class="save-icon"></span> Save </button>
                          <?php echo form_close(); ?>
                        </div>
                        </div>
                        <div class="vatEdit" style="display:none">
                          <?=form_open("admin/booking_config/vatedit")?>
                          <div class="vatEditajax">
                          </div>
                          <div class="col-md-7">
                            <a class="btn" style="float:right; margin-left:7px;" onclick="cancelVat()"><span class="delete-icon"> Cancel </span></a>
                            <button  class="btn"style=" float:right;"><span class="save-icon"></span> Update </button>
                          </div>
                          <?php echo form_close(); ?>
                        </div>
                        <div class="col-md-12 vatDelete" style="border:1px solid #ccc;padding:20px  0px; display: none;">
              						<?=form_open("admin/booking_config/deleteVat")?>
              							<input  name="tablename" value="vbs_job_statut" type="hidden" >
              							<input type="hidden" id="vatdeletid" name="delet_vat_id" value="">
              							<div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
              							<!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
                            <button  class="btn"style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

              							<a class="btn" style="cursor: pointer;" onclick="cancelVat()">No</a>
              						<?php echo form_close(); ?>
              					</div>
                      </div>
                    </div>
                    <div class="tab-pane fade <?=$status== '2'? 'in active':''?>" id="DISCOUNT">
                    <?php include('discount.php'); ?>
                    </div>
                    <div class="tab-pane fade <?=$status== '3'? 'in active':''?>" id="SERVICE_CATEGORY">
                    <?php include('service_category.php'); ?>
                    </div>

                    <div class="tab-pane fade <?=$status== '4'? 'in active':''?>"  id="SERVICES">
                    <?php include('service.php'); ?>
                    </div>
                    <div class="tab-pane fade <?=$status== '5'? 'in active':''?>" id="CLIENT_CATEGORY">
                    <?php include('client_category.php'); ?>
                    </div>
                    <div class="tab-pane fade <?=$status== '6'? 'in active':''?>" id="CAR_CATEGORY">
                    <?php include('car_category.php'); ?>
                    </div>
                    <div class="tab-pane fade <?=$status== '9'? 'in active':''?>" id="RIDE_CATEGORY">
                    <?php include('ride_category.php'); ?>
                    </div>
                    <div class="tab-pane fade <?=$status== '7'? 'in active':''?>" id="POI">
                      <?php include('book_poi.php')?>
                    </div>
                    <div class="tab-pane fade <?=$status== '8'? 'in active':''?>" id="PACKAGES">
                      <?php include('book_package.php')?>
                    </div>

        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<style>
     label > input {
        visibility: hidden;
    }
     label {
        display: block;
        margin: 0 0 0 -10px;
        padding: 0 0 20px 0;
        height: 20px;
        width: 150px;
    }

   label > img {
        display: inline-block;
        padding: 0px;
        height:10px;
        width:10px;
        background: none;
    }
     label > input:checked +img {
        background: url(http://cdn1.iconfinder.com/data/icons/onebit/PNG/onebit_34.png);
        background-repeat: no-repeat;
        background-position:center center;
        background-size:10px 10px;
    }
     .form-control {
         display: block;
         width: 100%;
         height: 34px;
         padding: 6px 12px;
         font-size: 14px;
         line-height: 1.42857143;
         color: #555;
         background-color: #fff;
         background-image: none;
         border: 1px solid #ccc;
         border-radius: 4px;
         -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
         box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
         -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
         -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
         transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
     }
     button:active, .btn:active, .button:active, .dataTables_paginate span.paginate_active {
         background-color: #f1f1f1;
         border-color: #b2b2b2 #c7c7c7 #c7c7c7 #b2b2b2;
         -webkit-box-shadow: inset 0 2px 1px rgba(0, 0, 0, 0.1);
         -moz-box-shadow: inset 0 2px 1px rgba(0, 0, 0, 0.1);
         box-shadow: inset 0 2px 1px rgba(0, 0, 0, 0.1);
     }

    input.date{
        width: 100%;
        float: left;
        height: 34px;
    }

</style>

<script>
	jQuery(function($){
	$.datepicker.regional['fr'] = {
		closeText: 'Fermer',
		prevText: '&#x3c;Préc',
		nextText: 'Suiv&#x3e;',
		currentText: 'Aujourd\'hui',
		monthNames: ['Janvier','Fevrier','Mars','Avril','Mai','Juin',
		'Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
		monthNamesShort: ['Jan','Fev','Mar','Avr','Mai','Jun',
		'Jul','Aou','Sep','Oct','Nov','Dec'],
		dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
		dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
		dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
		weekHeader: 'Sm',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: '',
		maxDate: '+12M +0D',
		showButtonPanel: true
		};
	$.datepicker.setDefaults($.datepicker.regional['fr']);
	$(document).click('.Vataddbtn',function () {
        Vatadd();
    })
});
    $(document).ready(function(){
        $( ".datepicker" ).datepicker({
		dateFormat: "dd-mm-yy",
		regional: "fr"
	});
        $('#MainAdd').click(function(){

            $('#MainTab').hide();
            $('#ShowTabs').attr('class','row');

        });

    });
    		// Passengers part

	function AddPassengers()
	{
		setupDivPassengers();
		$(".AddPassengers").show();
	}

	function cancelPassengers()
	{
		setupDivPassengers();
		$(".ListPassengers").show();
	}

	function setupDivPassengers()
	{
		$(".AddPassengers").hide();
		$(".ListPassengers").hide();
		$(".EditPassengers").hide();
		$(".DeletePassengers").hide();
	}


	// Vat part

    window.Vatadd = function()
	{
		setupDivVat();
		console.log('ddd')
		$(".Vatadd").show();
	}

	function cancelVat()
	{
		setupDivVat();
		$(".Listvat").show();
	}
  function VatDelete()
	{
    var val = $('.chk-Addvat-btn').val();
    if(val != "")
    {
		setupDivVat();
		$(".vatDelete").show();
    }else{
      alert('Please select a row to delete.');
    }
	}
  function VatEdit()
  {
      var val = $('.chk-Addvat-btn').val();
      if (val=='')
      {
          alert("Please select record!");
          return false;
      }
          setupDivVat();
          $(".vatEdit").show();
          $.ajax({
                  type: "GET",
                  url: '<?php echo base_url().'admin/booking_config/get_ajax_vat'; ?>',
                  data: {'vat_id': val},
                  success: function (result) {
                    // alert(result);
                    document.getElementsByClassName("vatEditajax")[0].innerHTML = result;
                  }
              });
  }
  function VatidEdit(id){
    var val = id;
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivVat();
        $(".vatEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/booking_config/get_ajax_vat'; ?>',
                data: {'vat_id': val},
                success: function (result) {
                  // alert(result);
                  document.getElementsByClassName("vatEditajax")[0].innerHTML = result;
                }
            });
  }
	function setupDivVat()
	{
		$(".Vatadd").hide();
    $(".vatEdit").hide();
    $(".Listvat").hide();
		$(".vatDelete").hide();

	}
  $('input.chk-mainvat-template').on('change', function() {
    $('input.chk-mainvat-template').not(this).prop('checked', false);
    var id = $(this).attr('data-input');
    $('.chk-Addvat-btn').val(id);
    $('#vatdeletid').val(id);
  });


    function getCaraddes(){
        $.ajax({
            url:"<?php echo base_url('admin/getCaradded') ?>",
            method:"GET",
            success:function(success){
                $('#tableBody').html(success);
            },error:function(xhr){
                console.log(xhr.responseText);
            }
        });
    }


 $('#save').on('click',function(){
    vatevent();
 })

</script>
<script>
$(window).on('load', function() {
  <?php if ($tabstatus=='1') { ?>
            vatEvent();
  <?php }else if ($tabstatus=='2'){ ?>
            discountEvent();
  <?php }else if ($tabstatus=='3'){ ?>
            serviceCatEvent();
  <?php }else if ($tabstatus=='4'){ ?>
            serviceEvent();
  <?php }else if ($tabstatus=='5'){ ?>
            clientCatEvent();
  <?php }else if ($tabstatus=='6'){ ?>
            carCatEvent();
  <?php }else if ($tabstatus=='7'){ ?>
            poiCatEvent();
  <?php }else if ($tabstatus=='8'){ ?>
            packageCatEvent();
  <?php }else if ($tabstatus=='9'){ ?>
            rideCatEvent();
  <?php }else if ($tabstatus=='10'){ ?>

  <?php } ?>

});
$('.vat_E').click(function(){
    vatEvent();
});
$('.discount_E').click(function(){
    discountEvent();
});
$('.serviceCar_E').click(function(){
    serviceCatEvent();
});
$('.service_E').click(function(){
    serviceEvent();
});
$('.clientCat_E').click(function(){
    clientCatEvent();
});
$('.carCat_E').click(function(){
    carCatEvent();
});
$('.price_E').click(function(){
    poiCatEvent();
});
$('.package_E').click(function(){
    packageCatEvent();
});
$('.rideCat_E').click(function(){
    rideCatEvent();
});
function vatEvent(){
  $('.removeevent').remove();
  var html= '<div class="removeevent"><div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Vatadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="VatEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="VatDelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button></div>';
  $('.addevent').append(html);
}

function discountEvent(){
  $('.removeevent').remove();
  var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Discountadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="DiscountEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="DiscountDelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button></div>';
  $('.addevent').append(html);
}
function serviceCatEvent(){
  $('.removeevent').remove();
  var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="ServiceCatAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="ServiceCatEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="ServiceCatDelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button></div>';
  $('.addevent').append(html);
}
function serviceEvent(){
  $('.removeevent').remove();
  var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="ServiceAdd()"><i class="fa fa-plus"></i>  <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="ServiceEdit()"><i class="fa fa-pencil"></i <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="ServiceDelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button></div>';
  $('.addevent').append(html);
}
function clientCatEvent(){
  $('.removeevent').remove();
  var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="ClientCatAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="ClientCatEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="ClientCatDelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button></div>';
  $('.addevent').append(html);
}

function carCatEvent(){
  $('.removeevent').remove();
  var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="carCatAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="carCatEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="carCatDelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button></div>';
  $('.addevent').append(html);
}
function rideCatEvent(){
  $('.removeevent').remove();
  var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="rideCatAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="rideCatEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="rideCatDelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button></div>';
  $('.addevent').append(html);
}
function poiCatEvent(){
  $('.removeevent').remove();
  var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Poiadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="poiEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="PoiDelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button></div>';
  $('.addevent').append(html);
}
function packageCatEvent(){
  $('.removeevent').remove();
  var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="packageadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="packageedit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="packagedelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button></div>';
  $('.addevent').append(html);
}

</script>
