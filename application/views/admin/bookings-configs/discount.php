<div class="row">
  <div class="ListDiscount" >
  <input type="hidden" class="chk-Adddiscount-btn" value="">
  <!-- <div class="col-md-12">
      <div class="toolbar"style="float: right; margin-bottom:5px;">
        <button class="btn btn-sm btn-default" onclick="Discountadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>&nbsp;
        <button class="btn btn-sm btn-default" onclick="DiscountEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>&nbsp;
        <button class="btn btn-sm btn-default" onclick="DiscountDelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button>&nbsp;
      </div>
    </div> -->
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" style="width:2%;">#</th>
            <th class="text-center" style="width:10px !important;"><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('user'); ?></th>
            <th class="column-first_name" style="text-align:left !important;"><?php echo $this->lang->line('name'); ?></th>
            <th class="column-date text-center"><?php echo $this->lang->line('discount'); ?>%</th>
            <th class="column-time text-center"><?php echo $this->lang->line('status'); ?></th>
          </tr>
          </thead>
          <tbody>
              <?php if (!empty($discount_data)): ?>
              <?php $cnt=1; ?>
              <?php foreach($discount_data as $key => $item):?>
                  <tr>
                    <td class="no-sort text-center"style="width:2%;">
                        <input type="checkbox" class="chk-maindiscount-template" data-input="<?=$item->id?>">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="DiscountidEdit('<?=$item->id?>')"><?=create_timestamp_uid($item->created_at,$item->id);?></a></td>
                      <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                      <td class="text-center"><?=$item->time?></td>
                      <td class="text-center"><?= $this->bookings_config_model->getuser($item->user_id);?></td>
                      <td><?=$item->name_of_discount;?></td>
                      <td class="text-center"><?=$item->discount; ?></td>
                      <td class="text-center"><?=$item->statut=='1'?'Show':'Hide';?></td>
                  </tr>
                  <?php $cnt++; ?>
              <?php endforeach; ?>
              <?php endif; ?>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>
<?=form_open("admin/booking_config/discountadd")?>
<!-- <form action="<?=base_url();?>admin/booking_config/vatadd" class="" method="post"  enctype="multipart/form-data"> -->
<div class="Discountadd" style="display: none;" >
  <div class="col-md-3" style="margin-top: 5px;">
    <div class="form-group">
      <span style="font-weight: bold;">Name Of Discount</span>
        <input type="text" class="form-control" name="name_of_discount" placeholder="" value="">
    </div>
  </div>
  <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span style="font-weight: bold;">Discount%</span>
        <input type="text" class="form-control" name="discount" placeholder="" value="">
    </div>
  </div>
  <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span style="font-weight: bold;">Statut</span>
      <select class="form-control" name="discount_statut" required>
        <option value="">Statut</option>
        <option value="1">Show</option>
        <option value="2">Hide</option>
      </select>
    </div>
  </div>
  <div class="col-md-7">
    <button class="btn" style="float:right; margin-left:7px;" onclick="cancelDiscount()"><span class="delete-icon"> Cancel </span></button>
    <button  class="btn"style=" float:right;"><span class="save-icon"></span> Save </button>
  <?php echo form_close(); ?>
</div>
</div>
<div class="discountEdit" style="display:none">
  <?=form_open("admin/booking_config/discountedit")?>
  <div class="discountEditajax">
  </div>
  <div class="col-md-7">
    <a class="btn" style="float:right; margin-left:7px;" onclick="cancelDiscount()"><span class="delete-icon"> Cancel </span></a>
    <button  class="btn"style=" float:right;"><span class="save-icon"></span> Update </button>
  </div>
  <?php echo form_close(); ?>
</div>
<div class="col-md-12 discountDelete" style="border:1px solid #ccc;padding:20px  0px; display: none;">
  <?=form_open("admin/booking_config/deletediscount")?>
    <input  name="tablename" value="vbs_job_statut" type="hidden" >
    <input type="hidden" id="discountdeletid" name="delet_discount_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn"style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

    <a class="btn" style="cursor: pointer;" onclick="cancelDiscount()">No</a>
  <?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
function Discountadd()
{
  setupDivDiscount();
  $(".Discountadd").show();
}

function cancelDiscount()
{
  setupDivDiscount();
  $(".ListDiscount").show();
}
// function DiscountEdit()
// {
//   var val = $('.chk-Adddiscount-btn').val();
//   if(val != "")
//   {
//     setupDivDiscount();
//     $(".discountEdit").show();
//     $("#discountEditview"+val).show();
//   }
// }

function DiscountEdit()
{
    var val = $('.chk-Adddiscount-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivDiscount();
        $(".discountEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/booking_config/get_ajax_discount'; ?>',
                data: {'discount_id': val},
                success: function (result) {
                  // alert(result);
                  document.getElementsByClassName("discountEditajax")[0].innerHTML = result;
                }
            });
}
function DiscountidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivDiscount();
      $(".discountEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/booking_config/get_ajax_discount'; ?>',
              data: {'discount_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("discountEditajax")[0].innerHTML = result;
              }
          });
}
function DiscountDelete()
{
  var val = $('.chk-Adddiscount-btn').val();
  if(val != "")
  {
  setupDivDiscount();
  $(".discountDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivDiscount()
{
  // $(".discountEditEdit").hide();
  $(".Discountadd").hide();
  $(".discountEdit").hide();
  $(".ListDiscount").hide();
  $(".discountDelete").hide();

}
$('input.chk-maindiscount-template').on('change', function() {
  $('input.chk-maindiscount-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Adddiscount-btn').val(id);
  $('#discountdeletid').val(id);
});

</script>
