<script>

    jQuery(function($){
        $.datepicker.regional['fr'] = {
            closeText: 'Fermer',
            prevText: '&#x3c;Préc',
            nextText: 'Suiv&#x3e;',
            currentText: 'Aujourd\'hui',
            monthNames: ['Janvier','Fevrier','Mars','Avril','Mai','Juin',
                'Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
            monthNamesShort: ['Jan','Fev','Mar','Avr','Mai','Jun',
                'Jul','Aou','Sep','Oct','Nov','Dec'],
            dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
            dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
            dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
            weekHeader: 'Sm',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: '',
            maxDate: '+12M +0D',
            showButtonPanel: true
        };
        $.datepicker.setDefaults($.datepicker.regional['fr']);
       
    });

</script>

<script>
 $(document).ready(function () {
   

    $(document).on('click','.profile_E', function() {
        profileEditEvent();
    });
    $(document).on('click','.document_E', function() {
        documentEvent();
    });
    $(document).on('click','.resource_E', function() {
        resourceEvent();
    });
    $(document).on('click','.availability_E', function() {
        availabilityEvent();
    });
    $(document).on('click','.infraction_E', function() {
        infractionEvent();
    });
    $(document).on('click','.request_E', function() {
        requestEvent();
    });
    $(document).on('click','.booking_E', function() {
        bookingEvent();
    });
    $(document).on('click','.timesheet_E', function() {
        timesheetEvent();
    });
    $(document).on('click','.payment_E', function() {
        paymentEvent();
    });
    $(document).on('click','.support_E', function() {
        supportEvent();
    });
    $(document).on('click','.message_E', function() {
        messageEvent();
    });

  


    
});
    function profileEditEvent(){
        removeAllResources();
        $('.removeinnerevent').remove();
    
    }

function documentEvent(){
    
        removeAllResources();
        $('.removeinnerevent').remove();
        var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="documentAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="documentEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="documentDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addinnerevent').append(html);
    }
  function resourceEvent(){
    
        removeAllResources();
        $('.removeinnerevent').remove();
        var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="resourceAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="resourceEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="resourceDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addinnerevent').append(html);
    }


    function availabilityEvent(){
        removeAllResources();
        $('.removeinnerevent').remove();
        var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="availabilityAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="availabilityEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="availabilityDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addinnerevent').append(html);
    }


      function infractionEvent(){
        removeAllResources();
        $('.removeinnerevent').remove();
        var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="infractionAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="infractionEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="infractionDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addinnerevent').append(html);
    }


    function requestEvent(){
        removeAllResources();
        $('.removeinnerevent').remove();
        var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="requestAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="requestEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="requestDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addinnerevent').append(html);
    }

  function bookingEvent(){
    removeAllResources();
    $('.removeinnerevent').remove();
    var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="bookingAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="bookingEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="bookingDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addinnerevent').append(html);
    }


        function timesheetEvent(){
        removeAllResources();
        $('.removeinnerevent').remove();
        var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="timesheetAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="timesheetEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="timesheetDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addinnerevent').append(html);
    }

   



     function paymentEvent(){
      removeAllResources();
      $('.removeinnerevent').remove();
      var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="paymentAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
    <button class="dt-button buttons-copy buttons-html5" onclick="paymentEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
    <button class="dt-button buttons-copy buttons-html5" onclick="paymentDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
          $('.addinnerevent').append(html);
      }


     function supportEvent(){
      removeAllResources();
      $('.removeinnerevent').remove();
      var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="supportAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
    <button class="dt-button buttons-copy buttons-html5" onclick="supportEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
    <button class="dt-button buttons-copy buttons-html5" onclick="supportDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
          $('.addinnerevent').append(html);
      }


    function messageEvent(){
     removeAllResources();
     $('.removeinnerevent').remove();
     var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="messageAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
    <button class="dt-button buttons-copy buttons-html5" onclick="messageEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
    <button class="dt-button buttons-copy buttons-html5" onclick="messageDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
          $('.addinnerevent').append(html);
      }





 
 function removeAllResources(){
   setupDivDocumentConfig();
   //setupDivResourceConfig();
   /*setupDivProfileConfig();
   setupDivMethodeConfig();
   setupDivPassengerConfig();
   setupDivRideConfig();
   setupDivBookingConfig();
   setupDivQuoteConfig();
   setupDivContractConfig();
   setupDivInvoiceConfig();
   setupDivPaymentConfig();
   setupDivSupportConfig();
   setupDivMessageConfig();*/
   
 }
function createinnerconfigtable(tablename){
 
  // Date range filter
    var datatables = $(tablename).DataTable({
        columnDefs: [
            {targets: 'no-sort', orderable: false}
        ],
        dom: '<"table-filter">B<"innertoolbar">frtip',
        language: {search: "", class: "form-control", searchPlaceholder: "Search"},
        buttons: [
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        initComplete: function () {
            $("div.innertoolbar")
                    .html('<div class="addinnerevent"><div class="removeinnerevent"></div></div>');
            var filterInp = $("#table-filter");
            if (filterInp.length > 0) {
                var tableFilter = $("div.table-filter");
                tableFilter.html(filterInp.html());

                if ($(".table-filter .dpo").length > 0) {
                    $('.table-filter .dpo[data-name="date_from"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            minDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                    $('.table-filter .dpo[data-name="date_to"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            maxDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                }
            }

            this.api().columns().every(function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                    );

                            column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                        });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });

        },
        "drawCallback": function () {
            $('.dataTables_paginate .paginate_button').addClass('btn btn-default');
        }
    });
 



                /* inner config*/
 }
</script>
