 <script>
      function initMap() {
        const map = new google.maps.Map(document.getElementById("map"), {
          zoom: 15,
          center: { lat: -34.397, lng: 150.644 },
        });
        const geocoder = new google.maps.Geocoder();
        geocodeAddress(geocoder, map);
        //document.getElementById("submit").addEventListener("click", () => {
         // geocodeAddress(geocoder, map);
       //});
      }

      function geocodeAddress(geocoder, resultsMap) {
        const address = document.getElementById("mapaddress").value;
        const address2 = document.getElementById("mapaddress2").value;
        const zipcode = document.getElementById("mapzipcode").value;
        const city = document.getElementById("mapcity").value;
        const mobile = document.getElementById("mapmobile").value;
        const phone = document.getElementById("mapphone").value;
        const name = document.getElementById("mapname").value;
        const partnerimg=document.getElementById("mappartnerimg").value;
        const partnerdefaultimg=document.getElementById("mappartnerdefaultimg").value;
        
        var contents = '<div class="col-md-12 pdhz" id="mapmaincontentdiv">';
        if(partnerimg){
           contents += '<div class="col-md-5 pdhz" id="mapinnercontentdivleft"><img class="zoommap" alt="Driver Icon" src="<?= base_url() ?>/uploads/partners/'+ partnerimg +'" ></div>';
        }
        else{
          contents += '<div class="col-md-5 pdhz" id="mapinnercontentdivleft"><img class="zoommap" alt="Driver Icon" src="<?= base_url() ?>/assets/images/'+ partnerdefaultimg +'" ></div>';
        }
        contents += '<div class="col-md-7 pdhz" id="mapinnercontentdivright"><div style="margin-top:0px !important;"><strong>'+name+'</strong></div>';
        if(address){
           contents += "<div>"+address+'</div>';
        }
        if(address2){
           contents += "<div>"+address2+'</div>';
        }
        if(zipcode){
           contents += "<div>"+zipcode+","+city+'</div>';
        }
       
        if(mobile){
           contents += "<div><strong>Mobile Phone : </strong>"+mobile+'</div>';
        }
        if(phone){
           contents += "<div><strong>Phone : </strong>"+phone+'</div>';
        }
      contents += '</div></div>';
       

        geocoder.geocode({ address: address }, (results, status) => {
          if (status === "OK") {
            resultsMap.setCenter(results[0].geometry.location);
          var startmarker =   new google.maps.Marker({
              map: resultsMap,
              position: results[0].geometry.location,
              draggable: false,
              icon: {url:'<?= base_url(); ?>/assets/images/partner.png',scaledSize: new google.maps.Size(45, 45),labelOrigin: new google.maps.Point(23, 18)},
              clickable:true
            });
           var startinfowindow=new google.maps.InfoWindow({
             content:"<div style='width:300px;'>"+contents+"</div>",
              disableAutoPan: true
        });
        startinfowindow.open(resultsMap,startmarker);
          startmarker.addListener('click',function(){
          startinfowindow.open(resultsMap,startmarker);
        });
          } else {
            alert(
              "Geocode was not successful for the following reason: " + status
            );
          }
        });
      }
    </script>