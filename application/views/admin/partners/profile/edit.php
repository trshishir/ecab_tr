                          <?php
                                 $dob = strtotime($partner->dob);
                                 $dob = date('d/m/Y',$dob);
                                 $dob = $dob;

                                 $civilite=$this->partners_model->getSingleRecord('vbs_partnercivilite',['id'=>$partner->civilite])->civilite;
                                 if(strtolower($civilite) ==  'mr'){
                                    $defaultimg='male-default.jpg';
                                 }elseif(strtolower($civilite) ==  'mrs'){
                                   $defaultimg='female-default.jpg';
                                 }else{
                                   $defaultimg='male-default.jpg';
                                 }
                             ?>         
 





             <?php echo form_open_multipart('admin/partners/profileEdit'); ?>
                 <input type="hidden" value="<?= $partner->id ?>" name="profile_id" id="partner_id">
                 <div class="col-md-12 pdhz">
                                     <div class="col-md-9 pdhz">
                      <!-- Row 1 -->
                        <div class="col-md-12 pdhz" style="margin-top:10px;" >
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <span style="font-weight: normal;">Statut : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <select class="form-control" name="statut" required>
                                               <?php foreach ($partner_status_data as $item): ?>
                                             <option <?php if($item->id == $partner->statut){ echo "selected"; } ?> value="<?= $item->id ?>"><?= $item->status ?></option>
                                             <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                        <div class="form-group">
                                              <div class="col-md-4" >
                                                  <span style="font-weight: normal;">Civility : </span>
                                              </div>
                                              <div class="col-md-8" style="padding: 0px;">
                                                  <select class="form-control" name="civilite" required>
                                                        <option value="">Select</option>
                                                      <?php foreach ($civilite_data as $item): ?>
                                                          <option <?php if($item->id == $partner->civilite){ echo "selected"; } ?> value="<?= $item->id ?>"><?= $item->civilite ?></option>
                                                      <?php endforeach; ?>
                                                  </select>
                                              </div>
                                        </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-4" >
                                            <span style="font-weight: normal;">First Name : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="prenom" class="form-control" type="text" value="<?= $partner->prenom ?>" required>
                                        </div>
                                    </div>
                                </div>                           
                          </div>


                             <!-- Row 2 -->
                          <div class="col-md-12 pdhz" style="margin-top:10px;" >                          
                               <div class="col-md-4">
                                   <div class="form-group">
                                       <div class="col-md-4" >
                                           <span style="font-weight: normal;">Last Name : </span>
                                       </div>
                                       <div class="col-md-8" style="padding: 0px;">
                                           <input name="nom" class="form-control" type="text" value="<?= $partner->nom ?>" required>
                                       </div>

                                   </div>
                               </div>
                               <div class="col-md-4">
                                   <div class="form-group">
                                       <div class="col-md-4" >
                                           <span style="font-weight: normal;">Category : </span>
                                       </div>
                                       <div class="col-md-8" style="padding: 0px;">
                                           <select class="form-control" id="edit_partner_category" name="category" required>     
                                               <option <?php if('0' == $partner->category){ echo "selected"; } ?>  value="0">Independent</option>
                                               <option <?php if('1' == $partner->category){ echo "selected"; } ?>  value="1">Company</option>
                                           </select>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-4">
                                      <div class="form-group" id="edit_partner_category_div" <?= (($partner->category == '1')?'style="display: block;"':'style="display: none;"'); ?> >
                                          <div class="col-md-4" >
                                              <span style="font-weight: normal;">Company : </span>
                                          </div>
                                          <div class="col-md-8" style="padding: 0px;">
                                              <input name="company" class="form-control" type="text" placeholder="Company Name" value="<?= $partner->company ?>">
                                          </div>
                                      </div>
                              </div>
                          </div>


                        <!-- Row 3 -->
                           <div class="col-md-12 pdhz" style="margin-top: 10px;">
                                <div class="col-md-4">
                                     <div class="form-group">
                                         <div class="col-md-4" >
                                             <span style="font-weight: normal;">Date of Birth : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;">
                                            <span id="edit_dob_span"></span>
                                             <input name="dob" id="edit_date_of_birth_field" class="form-control datepicker" value="<?= $dob ?>"
                                                 type="text">
                                         </div>
                                     </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" >
                                         <div class="col-md-4" >
                                             <span style="font-weight: normal;">Email : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;">
                                             <input name="email" class="form-control" style="width:100%;" type="email" value="<?= $partner->email ?>" required>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                          <div class="col-md-4" >
                                              <span style="font-weight: normal;">Phone : </span>
                                          </div>
                                          <div class="col-md-8" style="padding: 0px;">
                                              <input name="phone" class="form-control" style="width:100%;" type="phone" value="<?= $partner->phone ?>">
                                          </div>
                                      </div>
                                 </div>
                           </div>
                              <!-- Row 4 -->
                           <div class="col-md-12 pdhz" style="margin-top:10px;" >
                                   <div class="col-md-4">  
                                        <div class="form-group">
                                            <div class="col-md-4" >
                                                <span style="font-weight: normal;">Address : </span>
                                            </div>
                                            <div class="col-md-8" style="padding: 0px;">
                                                <input name="address" class="form-control" type="text" placeholder="address" value="<?= $partner->address ?>">
                                            </div>
                                        </div>
                                    </div>
                                   <div class="col-md-4">  
                                        <div class="form-group">
                                            <div class="col-md-4" >
                                                <span style="font-weight: normal;">Address 2 : </span>
                                            </div>
                                            <div class="col-md-8" style="padding: 0px;">
                                                <input name="address2" class="form-control" type="text" placeholder="address 2" value="<?= $partner->address2 ?>">
                                            </div>
                                        </div>
                                    </div>
                                   <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-md-4" >
                                                <span style="font-weight: normal;">Zip Code : </span>
                                            </div>
                                            <div class="col-md-8" style="padding: 0px;">
                                                <input name="zipcode" class="form-control" type="text" value="<?= $partner->zipcode ?>">
                                            </div>
                                        </div>
                                    </div>    
                              </div>
                                
                          </div>
                          
                      
                         <div class="col-md-3" style="text-align: center;margin-top:10px;">
                                <div class="form-group">
                                    <div class="col-md-4">
                                       
                                    </div>
                                    <div class="col-md-5" style="padding: 0px;">
                                      <input type="hidden" name="partner_image_pos" id="partner_image_pos" value="<?= (($partner->profile_image)?'1':'0'); ?>">
                                      <button type="button" id="editdefaultimgremovebtn" onclick="editremovedefaultpartnerimage(this);" class="minusrediconcustomreminder" <?= (($partner->profile_image)?'style="display:block;z-index: 20;right: 15px;top: 0px;"':'style="display:none;z-index: 20;right: 15px;top: 0px;"');?> ><i class="fa fa-minus" style="margin:0px !important;"></i></button>



                                      <input type="hidden" id="defaultpartnerimage" value="<?= $defaultimg ?>">
                                        <?php if($partner->profile_image): ?>
                                            <img id="edit_preview_partner" alt="Preview Logo"
                                        src="<?= base_url() ?>/uploads/partners/<?= $partner->profile_image  ?>"
                                        style="border: 1px solid #75b0d7;cursor: pointer;height: 170px;width: 170px;position: relative;z-index: 10;background-color: #fff;">
                                        <?php else: ?>
                                            <img id="edit_preview_partner" alt="Preview Logo"
                                        src="<?= base_url() ?>/assets/images/<?= $defaultimg ?>"
                                        style="border: 1px solid #75b0d7;cursor: pointer;height: 170px;width: 170px;position: relative;z-index: 10;">
                                        <?php endif; ?>
                                    </div>
                                </div>      
                          </div>  
                      </div>      

                      
                          <!-- Section 2 -->
                    <!-- Row 1 -->                                  
                  <div class="col-md-12 pdhz" style="margin-top: 10px;">
                     <div class="col-md-3">
                      <div class="form-group">
                          <div class="col-md-4" >
                              <span style="font-weight: normal;">Country : </span>
                          </div>
                          <div class="col-md-8" style="padding: 0px;">
                          <select required="required"  class="form-control" name="country" id="edit_country_id" tabindex="1">
                              <option value="">Select Country</option>
                              <?php foreach($countries as $data):?>
                                  <option <?php if($partner->country == $data->id){ echo "selected"; } ?> value="<?=$data->id;?>"><?=$data->name;?></option>
                              <?php endforeach;?>
                          </select>
                          </div>
                      </div>
                  </div>  
                 <div class="col-md-3">
                     <div class="form-group">
                         <div class="col-md-4" >
                             <span style="font-weight: normal;">Region : </span>
                         </div>
                         <div class="col-md-8" style="padding: 0px;">
                            <div class="form-group">
                               
                                 <select required class="form-control" name="region" id="edit_region_id">
                                   <?php foreach($regions as $data):?>
                                       <option <?php if($partner->region == $data->id){ echo "selected"; } ?> value="<?=$data->id;?>"><?=$data->name;?></option>
                                   <?php endforeach;?>
                                 </select>
                             </div>
                         </div>

                     </div>
                 </div> 
                 <div class="col-md-3">
                      <div class="form-group">
                          <div class="col-md-4" >
                              <span style="font-weight: normal;">City : </span>
                          </div>
                          <div class="col-md-8" style="padding: 0px;">
                          <select required="required" class="form-control" name="city" id="edit_cities_id" tabindex="1">
                            <?php foreach($cities as $data):?>
                                <option <?php if($partner->city == $data->id){ echo "selected"; } ?> value="<?=$data->id;?>"><?=$data->name;?></option>
                            <?php endforeach;?>
                          </select>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-3">
                       <div class="form-group">
                           <div class="col-md-4" >
                               <span style="font-weight: normal;" id="editdefaultimgspan"> <?= (($partner->profile_image)?'Partner Image : ':'Default Image : '); ?></span>
                           </div>
                           <div class="col-md-8">
                               <input class="edit_partner_image" name="profile_image" type="file" >
                           </div>

                       </div>
                   </div>
                  </div>
                  <!-- Row 2 -->  
                  <div class="col-md-12 pdhz" style="margin-top: 10px;">

                      <div class="col-md-3">
                        <div class="form-group">
                              <div class="col-md-4" >
                                  <span style="font-weight: normal;">Mobile : </span>
                              </div>
                              <div class="col-md-8" style="padding: 0px;">
                                  <input name="mobile" class="form-control" style="width:100%;" type="phone" value="<?= $partner->mobile ?>">
                              </div>
                          </div>
                     </div>
                      <div class="col-md-3">
                        <div class="form-group">
                              <div class="col-md-4" >
                                  <span style="font-weight: normal;">Fax : </span>
                              </div>
                              <div class="col-md-8" style="padding: 0px;">
                                  <input name="fax" class="form-control" style="width:100%;" type="text" value="<?= $partner->fax ?>">
                              </div>
                          </div>
                     </div>
                  </div>
                  
              <!-- Row 2 -->


               <div class="col-md-12 pdhz" style="margin-top: 10px;">
                   <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-md-4" >
                                <span style="font-weight: normal;">Source : </span>
                            </div>
                            <div class="col-md-8" style="padding: 0px;">
                                <select class="form-control" name="source" required>
                                  <option value="">Select</option>
                                  <option <?php if($partner->source == '1'){ echo "selected"; } ?>  value="1">xyz</option>
                                </select>
                            </div>
                        </div>
                    </div>
                   <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-md-4" >
                                <span style="font-weight: normal;">Partner Advisor : </span>
                            </div>
                            <div class="col-md-8" style="padding: 0px;">
                                <select class="form-control" name="advisor" required>
                                  <option value="">Select</option>
                                     <?php foreach($users_data as $item):?>
                                <option <?php if($partner->advisor == $item['id']){ echo "selected"; } ?>  value="<?= $item['id'] ?>"><?= $item['civility']." ".$item['first_name']." ".$item['last_name'];?></option>
                                <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                         <div class="form-group">
                             <div class="col-md-4" >
                                 <span style="font-weight: normal;">Partner Support : </span>
                             </div>
                             <div class="col-md-8" style="padding: 0px;">
                                 <select class="form-control" name="support" required>
                                  <option value="">Select</option>
                                      <?php foreach($users_data as $item):?>
                                 <option <?php if($partner->support == $item['id']){ echo "selected"; } ?>  value="<?= $item['id'] ?>"><?= $item['civility']." ".$item['first_name']." ".$item['last_name'];?></option>
                                 <?php endforeach; ?>
                                 </select>
                             </div>
                         </div>
                     </div>
                   
               </div>
               <div class="col-md-12 pdhz" style="margin-top:10px;" >
                                   
                                           <div class="col-md-6" style="padding-left: 30px;">
                                            <div class="col-md-12 send-access-div">
                                           
                                            <div class="col-md-12 pdhz" style="margin-top: 5px;">
                                                 <div class="col-md-5 pdhz">
                                                       <div class="form-group">
                                                           <div class="col-md-4">
                                                               <span style="font-weight: normal;">Email : </span>
                                                           </div>
                                                           <div class="col-md-8" style="padding: 0px;">
                                                               <input name="email" id="partner_email"  class="form-control" type="email" value="<?=  $partner->email ?>">
                                                           </div>
                                                       </div>
                                                </div>
                                                <div class="col-md-5 pdhz">
                                                  <div class="form-group">
                                                      <div class="col-md-4">
                                                          <span style="font-weight: normal;">Password : </span>
                                                      </div>
                                                      <div class="col-md-8" style="padding: 0px;">
                                                          <input name="password" id="partner_password"  class="form-control" type="password" value="<?= date("dmY", strtotime($partner->dob)) ?>">
                                                      </div>
                                                   </div>
                                                 </div>
                                                  <div class="col-md-2">
                                                    <div id="sendaccessloaderbtn" style="position: relative;display: none;">
                                                      <img src="<?= base_url()?>assets/theme/default/images/bookloader.gif" style="width: 28px;height: 28px;position: absolute;left: 110px;top: 5px;">
                                                    </div>
                                                       <button type="button" class="btn btn-default" onclick="sendaccessdetail();">Send Access</button>
                                                   </div>
                                            </div>
                                                      
                                         </div>         
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top: 10px;" id="mainpartnernotediv">
                                 <div class="col-md-12 pdhz">
                                   <div class="col-md-3">
                                    <textarea rows="3" class="form-control" placeholder="write a note" id="partnernotefield" name="partnernotefield" style="border-radius: 0px;"></textarea>
                                  </div>
                                   <div class="col-md-1 pdhz" style="position: relative;">
                                           <button type="button" onclick="partnercreatenote('#partnernotefield','#mainpartnernotediv');" class="plusgreeniconconfig" style="top:0px !important;left:5px !important;"><i class="fa fa-plus" style="margin:0px !important;"></i></button>
                                     </div>
                                   </div>
                                    <?php foreach ($partnernotes as $item): ?>
                                   <div class="col-md-12 pdhz partnernotebox" style="margin-top:10px;">
                                     <div class="col-md-3">
                                       <textarea rows="3" class="form-control"  style="border-radius: 0px;pointer-events: none;background-color: #def4faab !important;"><?= $item->note ?></textarea>
                                     </div>
                                     <div class="col-md-1" style="position: relative;">
                                       <button type="button" onclick="partnerremovenote(this,<?= $item->id ?>);" class="minusrediconcustomreminder" style="left:5px !important;top: 0px !important;"><i class="fa fa-minus" style="margin:0px !important;"></i></button>
                                     </div>
                                   </div>
                                    <?php endforeach;?>
                                </div>
                               
                        <!-- Row 8 -->
                    

                        <div class="col-md-12 pdlz" style="margin-top: 20px;">
           <!--Reminder and Notification -->
    <div class="col-md-6" >
        <!-- Notification -->
            <?php  include 'notifications.php';?>
        <!-- Notification -->
        <!-- Reminders -->
            <?php  include 'reminders.php';?>
        <!-- Reminder -->
        <!-- Custom Reminders -->
            <?php  include 'customreminders.php';?>
        <!-- Custom Reminders -->
     </div>
     <!--Reminder and Notification -->


                     <div class="col-md-5 col-md-offset-1" style="padding-right: 0px;">
                         <div class="map-wrapper" >
                            <div id="map"></div>
                         </div>
                        <input id="mapaddress" type="hidden" value="<?= $partner->address ?>" />
                        <input id="mapaddress2" type="hidden" value="<?= $partner->address2 ?>" />
                        <input id="mapzipcode" type="hidden" value="<?= $partner->zipcode ?>" />
                        <input id="mapcity" type="hidden" value="<?= $this->partners_model->getSingleRecord('vbs_cities',['id'=>$partner->city])->name ?>" />
                        <input id="mapmobile" type="hidden" value="<?= $partner->mobile ?>" />
                        <input id="mapphone" type="hidden" value="<?= $partner->phone ?>" />
                        <input id="mapname" type="hidden" value="<?= $this->partners_model->getSingleRecord('vbs_partnercivilite',['id'=>$partner->civilite])->civilite.' '.$partner->prenom.' '.$partner->nom ?>" />
                        <input id="mappartnerimg" type="hidden" value="<?= $partner->profile_image ?>" />
                        <input id="mappartnerdefaultimg" type="hidden" value="<?= $defaultimg ?>" />
                    </div>
                </div>

                           <div class="col-md-12">                       
                             <!-- icons -->
                              <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                             <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelProfile()"><span class="fa fa-close"> Cancel </span></button>
                             <!-- icons -->  
                          </div>

                     <?php echo form_close(); ?>
 