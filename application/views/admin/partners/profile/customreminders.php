                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 15px;">
                             <div class="col-md-12 pdhz" style="font-weight: 900;">CUSTOM REMINDERS : </div>
                             <div class="col-md-12 pdhz" id="customreminderrecorded">
                               <div class="col-md-12 pdhz">
                                <div class="col-md-2 pdlz" style="text-align: left;font-weight: 900;">Canal</div>
                                <div class="col-md-2 pdlz" style="text-align: left;font-weight: 900;">Date</div>
                                <div class="col-md-2 pdlz" style="text-align: left;font-weight: 900;">Time</div>
                                <div class="col-md-3 pdlz" style="text-align: left;font-weight: 900;">Subject</div>
                                <div class="col-md-2 pdlz" style="text-align: left;font-weight: 900;">Comment</div>
                                 <div class="col-md-1" style="text-align: left;font-weight: 900;"></div>
                              </div>

                              <?php  foreach ($partnercustomreminders as $custom): 
                                     $date = strtotime($custom->date);
                                     $date = date('d/m/Y',$date);
                                     $date= $date;
                                  ?>

                                  <div class="customreminderbox col-md-12 pdhz" style="margin-top: 5px;">
                                   
                                    <div class="col-md-2 text-left pdlz"><?= $custom->title  ?></div>
                                    <div class="col-md-2 text-left pdlz"><?= from_unix_date($custom->date)  ?></div>
                                    <div class="col-md-2 text-left pdlz"><?= $custom->time  ?></div>
                                    <div class="col-md-3 text-left pdlz"><?= $custom->subject  ?></div>
                                    <div class="col-md-2 text-left pdlz" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;"><?= $custom->comment  ?></div>
                                  
                                    <div class="col-md-1" style="position: relative;">
                                      <button type="button" onclick="removecustomreminderbox(this,<?= $custom->id ?>);" class="minusrediconcustomreminder" style="left:5px !important;"><i class="fa fa-minus" style="margin:0px !important;"></i></button></div>
                                  </div>
                            <?php endforeach; ?>
                             </div>
                             <input type="hidden" id="customremindertotalcount" value="1">
                             <div class="maincustomreminderbox col-md-12 pdhz">
                              <div class="customreminderbox col-md-12 pdhz" style="margin-top: 10px;">
                                <div class="col-md-12 pdhz">
                               
                                        <div class="col-md-2 pdlz">
                                          <div class="form-group">
                                                   <select class="form-control nbrhl" name="customremindertitlefield" id="customremindertitlefield" style="padding: 0px 12px;">
                                                         <option value="">Select Canal</option>
                                                         <option value="Email">Email</option>
                                                         <option value="Phone">Phone</option>
                                                         <option value="Letter">Letter</option>    
                                                   </select>                                          
                                              </div>
                                        </div>


                                        <div class="col-md-2 pdlz">                             
                                          <input  class="pickup-dt-bkg datepicker form-control nbrhl" id="customreminderdatefield"  type="text" value="<?= date('d/m/Y'); ?>" name="customreminderdatefield[]" placeholder="<?php echo 'Custom Date'; ?>" autocomplete="off" />     
                                        </div>
                                         <div class="col-md-2 pdlz">
                                          <input   id="customremindertimepicker" class="form-control nbrhl"  type="text" value="<?= date('h : i'); ?>" name="customremindertimefield[]" placeholder="<?php echo 'Custom Time'; ?>"  style="width:100%" />       
                                        </div>
                                        <div class="col-md-2 pdlz">
                                          <div class="form-group">
                                                   <select class="form-control nbrhl" name="customremindersubjectfield" id="customremindersubjectfield" style="padding: 0px 12px;">
                                                         <option value="">Subject</option>
                                                   <option value="Identity Card Expired">Identity Card Expired</option>
                                                   <option value="Partner License Expired">Partner License Expired</option>
                                                   <option value="Medical Certificate Expired">Medical Certificate Expired</option>
                                                   <option value="PSC1 Expired">PSC1 Expired</option>
                                                   <option value="Work Medecine Expired">Work Medecine Expired</option>
                                                            
                                                   </select>                                          
                                              </div>
                                        </div>
                                        <div class="col-md-3 pdlz"></div>

                                
                                 <div class="col-md-1 pdhz" style="position: relative;">
                                    <button type="button" onclick="addcustomreminderbox();" class="plusgreeniconconfig" style="top:0px !important;left:5px !important;"><i class="fa fa-plus" style="margin:0px !important;"></i></button>
                                 </div>
                               </div>
                               <div class="col-md-12 pdhz" style="margin-top:5px;">
                                 <div class="col-md-10 pdhz" style="width: 65% !important;">
                                   <textarea rows="3" class="form-control" placeholder="write a comment" id="customremindercommentfield" name="customremindercommentfield" style="border-radius: 0px;"></textarea>
                                 </div>
                               </div>
                             </div>
                             </div>
                        </div>