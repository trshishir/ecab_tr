       <div id="searchform" style="display: none;">
        <?php
        $attributes = array("name" => 'partner_search_form',"class"=>'form-inline', "id" => 'partner_search_form');
        echo form_open('admin/partners/get_partners_bysearch',$attributes);
        ?>
            
               <div class="form-group">
                 <select name="category" style="float: left;width:150px !important;margin-right: 2px;" class="form-control" >
                  <option value="">Category</option>
                  <option <?php if($search_category == '0'){ echo "selected"; } ?>  value="0">Independent</option>
                  <option <?php if($search_category == '1'){ echo "selected"; } ?>  value="1">Company</option>
                </select> 
              </div>

               <div class="form-group">
                 <select name="payment_delay" style="float: left;width:150px !important;margin-right: 2px;" class="form-control" >
                  <option value="">Payment Delay</option>
                  <option <?php if($search_delay == '0'){ echo "selected"; } ?>  value="10">10</option>
                  <option <?php if($search_delay == '1'){ echo "selected"; } ?>  value="20">20</option>
                </select> 
              </div>
              <div class="form-group">
                 <select name="payment_method" style="float: left;width:150px !important;margin-right: 2px;" class="form-control" >
                  <option value="">Payment Methode</option>
                  <option <?php if($search_method == 'credit'){ echo "selected"; } ?>  value="credit">Credit Card</option>
                  <option <?php if($search_method == 'debit'){ echo "selected"; } ?>  value="debit">Debit Card</option>
                </select> 
              </div>
           
              <div class="form-group">
                 <input type="text" name="from_period" class="bsearchdatepicker bdr form-control" placeholder="From" value="<?= $search_from_period; ?>" autocomplete="off"/> 
              </div>
              <div class="form-group">
                 <input type="text" name="to_period" class="bsearchdatepicker bdr form-control" placeholder="To"  value="<?= $search_to_period; ?>" autocomplete="off"/> 
              </div>
               
  
               <div class="form-group">
                 <select name="statut" style="float: left;width:150px !important;" class="form-control">
                  <option value="">Statut</option>
                 <?php foreach ($partner_status_data as $item): ?>
                   <option  <?php if($item->id == $search_status){ echo "selected"; } ?> value="<?= $item->id ?>"><?= $item->status ?></option>
                   <?php endforeach; ?>
               
                </select> 
              </div>
            <div class="form-group">
              <button type="submit" class="btn btn-default searchpartnerbtn" style="height: 27px;line-height: 3px;outline: none;margin-left: 5px;">Search</button>
            </div>
           <?php echo form_close(); ?>
       </div>