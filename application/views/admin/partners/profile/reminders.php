 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px;">
          <div class="col-md-12 pdhz" style="font-weight: 900;">REMINDERS : </div>
            <div class="col-md-12 pdhz">  
               <div class="col-md-2 pdhz text-center" style="font-weight: 900;">Date</div>
               <div class="col-md-2 pdhz text-center" style="font-weight: 900;">Time</div>
               <div class="col-md-3 pdhz text-center" style="font-weight: 900;">Subject</div>
               <div class="col-md-1 pdhz text-center" style="font-weight: 900;">File</div>
               <div class="col-md-2 pdhz text-center" style="font-weight: 900;">Statut</div>
               <div class="col-md-2 pdhz text-center" style="font-weight: 900;">Resend</div>
            </div>        


 <!-- Identity Reminder -->  
  <?php $a=1; ?>
  <?php foreach ($partnerreminders as $reminder): ?>
          <?php if(!empty($reminder->typeiddate) || $reminder->typeiddate != null): ?>
          <?php if($a==1): ?>  
        <div class="col-md-12 pdhz">
            <div class="col-md-2 ">                             
               <input  class="datepicker form-control" id="identityreminderdatepicker"  type="text" value="<?= from_unix_date($reminder->typeiddate); ?>" name="identityreminderdatefield"  autocomplete="off" style="text-align: center;" />   
            </div>
            <div class="col-md-2 ">
             <input  class="form-control" id="identityremindertimepicker"  type="text" value="<?= $reminder->typeidtime ?>" name="identityremindertimefield"   style="width:100%;text-align: center;" />       
            </div>
            <div class="col-md-3 pdhz text-center" ><div class="col-md-10 col-md-offset-2 pdhz text-left">Identity Card Expired</div></div> 
            <div class="col-md-1 pdhz text-center" >
                       <a href="<?= base_url(); ?>admin/partners/reminder_pdf/<?= $reminder->id  ?>/<?= $partner->id ?>/1/1" target="_blank">
                         <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                       </a>
           </div>
           <div class="col-md-2 pdhz text-center" ><?= ($reminder->typeidissent == '1')?"Sent":"Pending"; ?></div>
            <div class="col-md-2 pdhz text-center" >
              <div id="identityremindersentloaderbtn" style="position: relative;display: none;">
                <img src="<?= base_url()?>assets/theme/default/images/bookloader.gif" style="width: 28px;height: 28px;position: absolute;left: -16px;top: 0px;">
              </div>
              <button type="button" class="btn btn-default" id="identityremindersentbtn" style="height: 30px;line-height: 1px;" onclick="addpartnerreminder('typeiddate','typeidtime','identityreminderdatepicker','identityremindertimepicker','identitysentloaderbtn','Identity Card Expired','#partnerremindermaindiv_1')">Send</button>
            </div>      
        </div>
        <?php $a++; ?>
         <?php endif;?>
          <?php endif; ?>
         <?php endforeach; ?> 
     
     <?php $a1=0; ?>
       <div class="col-md-12 pdhz" id="partnerremindermaindiv_1">
        <?php foreach ($partnerreminders as $reminder): ?>
          <?php if(!empty($reminder->typeiddate) || $reminder->typeiddate != null): ?>
            <?php if($a1 != 0): ?>
            <div class="col-md-12 pdhz">          
                 <div class="col-md-2 pdhz text-center" ><?= from_unix_date($reminder->typeiddate); ?></div>
                 <div class="col-md-2 pdhz text-center" ><?= $reminder->typeidtime ?></div>
                  <div class="col-md-3 pdhz text-center" ><div class="col-md-10 col-md-offset-2 pdhz text-left">Identity Card Expired</div></div> 
                  <div class="col-md-1 pdhz text-center" >
                             <a href="<?= base_url(); ?>admin/partners/reminder_pdf/<?= $reminder->id  ?>/<?= $partner->id ?>/1/1" target="_blank">
                               <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                             </a>
                 </div>
                 <div class="col-md-2 pdhz text-center" ><?= ($reminder->typeidissent == '1')?"Sent":"Pending"; ?></div>
            </div> 
             <?php endif;?>
            <?php $a1++; ?>
            <?php endif; ?>
         <?php endforeach; ?>  
         </div> 

          <!-- Identity Reminder -->   


          <!-- Lisence Reminder -->  
           <?php $b=1; ?>
          <?php foreach ($partnerreminders as $reminder): ?>
              <?php if(!empty($reminder->permitnumberdate) || $reminder->permitnumberdate != null): ?>  
                 <?php if($b==1): ?>

                 <div class="col-md-12 pdhz" style="margin-top: 10px;">
                     <div class="col-md-2 ">                             
                        <input  class="datepicker form-control" id="lisencereminderdatepicker"  type="text" value="<?= from_unix_date($reminder->permitnumberdate); ?>" name="lisencereminderdatefield"  autocomplete="off" style="text-align: center;" />   
                     </div>
                     <div class="col-md-2 ">
                      <input  class="form-control" id="lisenceremindertimepicker"  type="text" value="<?= $reminder->permitnumbertime ?>" name="lisenceremindertimefield"   style="width:100%;text-align: center;" />       
                     </div>
                     <div class="col-md-3 pdhz text-center" ><div class="col-md-10 col-md-offset-2 pdhz text-left">Partner License Expired</div></div> 
                      <div class="col-md-1 pdhz text-center" >
                                 <a href="<?= base_url(); ?>admin/partners/reminder_pdf/<?= $reminder->id  ?>/<?= $partner->id ?>/2/1" target="_blank">
                                   <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                                 </a>
                     </div>
                     <div class="col-md-2 pdhz text-center" ><?= ($reminder->permitnumberissent == '1')?"Sent":"Pending"; ?></div>
                     <div class="col-md-2 pdhz text-center" >
                       <div id="lisenceremindersentloaderbtn" style="position: relative;display: none;">
                         <img src="<?= base_url()?>assets/theme/default/images/bookloader.gif" style="width: 28px;height: 28px;position: absolute;left: -16px;top: 0px;">
                       </div>
                       <button type="button" class="btn btn-default" id="lisenceremindersentbtn" style="height: 30px;line-height: 1px;" onclick="addpartnerreminder('permitnumberdate','permitnumbertime','lisencereminderdatepicker','lisenceremindertimepicker','lisenceremindersentloaderbtn','Partner License Expired','#partnerremindermaindiv_2')">Send</button>
                     </div>      
                 </div>
                 <?php $b++; ?>
                  <?php endif;?>
                   <?php endif; ?>
                   <?php endforeach; ?>  

                  <?php $b1=0; ?>
                  <div class="col-md-12 pdhz" id="partnerremindermaindiv_2">
                   <?php foreach ($partnerreminders as $reminder): ?>
                    <?php if(!empty($reminder->permitnumberdate) || $reminder->permitnumberdate != null): ?>
                      <?php if($b1 != 0): ?>

                     <div class="col-md-12 pdhz">
                                  
                          <div class="col-md-2 pdhz text-center" ><?= from_unix_date($reminder->permitnumberdate); ?></div>
                          <div class="col-md-2 pdhz text-center" ><?= $reminder->permitnumbertime ?></div>
                          <div class="col-md-3 pdhz text-center" ><div class="col-md-10 col-md-offset-2 pdhz text-left">Partner License Expired</div></div> 
                           <div class="col-md-1 pdhz text-center" >
                                      <a href="<?= base_url(); ?>admin/partners/reminder_pdf/<?= $reminder->id  ?>/<?= $partner->id ?>/2/1" target="_blank">
                                        <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                                      </a>
                          </div>
                          <div class="col-md-2 pdhz text-center" ><?= ($reminder->permitnumberissent == '1')?"Sent":"Pending"; ?></div>
                     </div>
                      <?php endif;?>
                     <?php $b1++; ?>
                     <?php endif; ?>
                   <?php endforeach; ?>   
                 </div>
                  <!-- Lisence Reminder -->   

    <!-- Medical Reminder -->    
     <?php $c=1; ?>
    <?php foreach ($partnerreminders as $reminder): ?>
            <?php if(!empty($reminder->medicalcertificatedate) || $reminder->medicalcertificatedate != null): ?>
              <?php if($c==1): ?>
           <div class="col-md-12 pdhz" style="margin-top: 10px;">
               <div class="col-md-2 ">                             
                  <input  class="datepicker form-control" id="medicalreminderdatepicker"  type="text" value="<?= from_unix_date($reminder->medicalcertificatedate); ?>" name="medicalreminderdatefield"  autocomplete="off" style="text-align: center;" />   
               </div>
               <div class="col-md-2 ">
                <input  class="form-control" id="medicalremindertimepicker"  type="text" value="<?= $reminder->medicalcertificatetime ?>" name="medicalremindertimefield"   style="width:100%;text-align: center;" />       
               </div>
               <div class="col-md-3 pdhz text-center" ><div class="col-md-10 col-md-offset-2 pdhz text-left">Medical Certificate Expired</div></div> 
                <div class="col-md-1 pdhz text-center" >
                           <a href="<?= base_url(); ?>admin/partners/reminder_pdf/<?= $reminder->id  ?>/<?= $partner->id ?>/3/1" target="_blank">
                             <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                           </a>
               </div>
               <div class="col-md-2 pdhz text-center" ><?= ($reminder->medicalcertificateissent == '1')?"Sent":"Pending"; ?></div>
               <div class="col-md-2 pdhz text-center" >
                 <div id="medicalremindersentloaderbtn" style="position: relative;display: none;">
                   <img src="<?= base_url()?>assets/theme/default/images/bookloader.gif" style="width: 28px;height: 28px;position: absolute;left: -16px;top: 0px;">
                 </div>
                 <button type="button" class="btn btn-default" id="medicalremindersentbtn" style="height: 30px;line-height: 1px;" onclick="addpartnerreminder('medicalcertificatedate','medicalcertificatetime','medicalreminderdatepicker','medicalremindertimepicker','medicalremindersentloaderbtn','Medical Certificate Expired','#partnerremindermaindiv_3')">Send</button>
               </div>      
           </div>
           <?php $c++; ?>
            <?php endif;?>
            <?php endif; ?>
             <?php endforeach; ?> 

              <?php $c1=0; ?>
           <div class="col-md-12 pdhz" id="partnerremindermaindiv_3">
             <?php foreach ($partnerreminders as $reminder): ?>
               <?php if(!empty($reminder->medicalcertificatedate) || $reminder->medicalcertificatedate != null): ?>
                <?php if($c1 != 0): ?>
               <div class="col-md-12 pdhz">
                            
                    <div class="col-md-2 pdhz text-center" ><?= from_unix_date($reminder->medicalcertificatedate); ?></div>
                    <div class="col-md-2 pdhz text-center" ><?= $reminder->medicalcertificatetime ?></div>
                    <div class="col-md-3 pdhz text-center" ><div class="col-md-10 col-md-offset-2 pdhz text-left">Medical Certificate Expired</div></div> 
                     <div class="col-md-1 pdhz text-center" >
                                <a href="<?= base_url(); ?>admin/partners/reminder_pdf/<?= $reminder->id  ?>/<?= $partner->id ?>/3/1" target="_blank">
                                  <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                                </a>
                    </div>
                    <div class="col-md-2 pdhz text-center" ><?= ($reminder->medicalcertificateissent == '1')?"Sent":"Pending"; ?></div>
               </div>
                <?php endif;?>
               <?php $c1++; ?>
               <?php endif; ?>
             <?php endforeach; ?>   
           </div>
            <!-- Medical Reminder -->    


            <!-- PSC Reminder -->   
              <?php $d=1; ?>
             <?php foreach ($partnerreminders as $reminder): ?>
                      <?php if(!empty($reminder->psc1date) || $reminder->psc1date != null): ?> 
                         <?php if($d==1): ?>
                   <div class="col-md-12 pdhz" style="margin-top: 10px;">
                       <div class="col-md-2 ">                             
                          <input  class="datepicker form-control" id="pscreminderdatepicker"  type="text" value="<?= from_unix_date($reminder->psc1date); ?>" name="pscreminderdatefield"  autocomplete="off" style="text-align: center;" />   
                       </div>
                       <div class="col-md-2 ">
                        <input  class="form-control" id="pscremindertimepicker"  type="text" value="<?= $reminder->psc1time ?>" name="pscremindertimefield"   style="width:100%;text-align: center;" />       
                       </div>
                       <div class="col-md-3 pdhz text-center" ><div class="col-md-10 col-md-offset-2 pdhz text-left">PSC1 Expired</div></div>  
                        <div class="col-md-1 pdhz text-center" >
                                   <a href="<?= base_url(); ?>admin/partners/reminder_pdf/<?= $reminder->id  ?>/<?= $partner->id ?>/4/1" target="_blank">
                                     <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                                   </a>
                       </div>
                       <div class="col-md-2 pdhz text-center" ><?= ($reminder->psc1issent == '1')?"Sent":"Pending"; ?></div>
                       <div class="col-md-2 pdhz text-center" >
                         <div id="pscremindersentloaderbtn" style="position: relative;display: none;">
                           <img src="<?= base_url()?>assets/theme/default/images/bookloader.gif" style="width: 28px;height: 28px;position: absolute;left: -16px;top: 0px;">
                         </div>
                         <button type="button" class="btn btn-default" id="pscremindersentbtn" style="height: 30px;line-height: 1px;" onclick="addpartnerreminder('psc1date','psc1time','pscreminderdatepicker','pscremindertimepicker','pscremindersentloaderbtn','PSC1 Expired','#partnerremindermaindiv_4')">Send</button>
                       </div>      
                   </div>
                   <?php $d++; ?>
                    <?php endif;?>
                     <?php endif; ?>
                     <?php endforeach; ?> 

                     <?php $d1=0; ?>
                   <div class="col-md-12 pdhz" id="partnerremindermaindiv_4">
                     <?php foreach ($partnerreminders as $reminder): ?>
                      <?php if(!empty($reminder->psc1date) || $reminder->psc1date != null): ?>
                        <?php if($d1 != 0): ?>
                       <div class="col-md-12 pdhz">
                                   
                            <div class="col-md-2 pdhz text-center" ><?= from_unix_date($reminder->psc1date); ?></div>
                            <div class="col-md-2 pdhz text-center" ><?= $reminder->psc1time ?></div>
                            <div class="col-md-3 pdhz text-center" ><div class="col-md-10 col-md-offset-2 pdhz text-left">PSC1 Expired</div></div>  
                             <div class="col-md-1 pdhz text-center" >
                                        <a href="<?= base_url(); ?>admin/partners/reminder_pdf/<?= $reminder->id  ?>/<?= $partner->id ?>/4/1" target="_blank">
                                          <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                                        </a>
                            </div>
                            <div class="col-md-2 pdhz text-center" ><?= ($reminder->psc1issent == '1')?"Sent":"Pending"; ?></div>
                       </div>
                        <?php endif;?>
                       <?php $d1++; ?>
                       <?php endif; ?>
                     <?php endforeach; ?>  
                     </div> 
                    <!-- PSC Reminder -->   


      <!-- Medicine Travel Reminder -->    
        <?php $e=1; ?>
       <?php foreach ($partnerreminders as $reminder): ?>
                <?php if(!empty($reminder->medicineoftravaidate) || $reminder->medicineoftravaidate != null): ?>
                   <?php if($e==1): ?>
             <div class="col-md-12 pdhz" style="margin-top: 10px;">
                 <div class="col-md-2 ">                             
                    <input  class="datepicker form-control" id="medicinetravaireminderdatepicker"  type="text" value="<?= from_unix_date($reminder->medicineoftravaidate); ?>" name="medicinetravaireminderdatefield"  autocomplete="off" style="text-align: center;" />   
                 </div>
                 <div class="col-md-2 ">
                  <input  class="form-control" id="medicinetravairemindertimepicker"  type="text" value="<?= $reminder->medicineoftravaitime ?>" name="medicinetravairemindertimefield"   style="width:100%;text-align: center;" />       
                 </div>
                <div class="col-md-3 pdhz text-center" ><div class="col-md-10 col-md-offset-2 pdhz text-left">Work Medecine Expired</div></div>      
                 <div class="col-md-1 pdhz text-center" >
                            <a href="<?= base_url(); ?>admin/partners/reminder_pdf/<?= $reminder->id  ?>/<?= $partner->id ?>/5/1" target="_blank">
                              <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                            </a>
                </div>
                <div class="col-md-2 pdhz text-center" ><?= ($reminder->medicineoftravaiissent == '1')?"Sent":"Pending"; ?></div>
                 <div class="col-md-2 pdhz text-center" >
                   <div id="medicinetravairemindersentloaderbtn" style="position: relative;display: none;">
                     <img src="<?= base_url()?>assets/theme/default/images/bookloader.gif" style="width: 28px;height: 28px;position: absolute;left: -16px;top: 0px;">
                   </div>
                   <button type="button" class="btn btn-default" id="medicinetravairemindersentbtn" style="height: 30px;line-height: 1px;" onclick="addpartnerreminder('medicineoftravaidate','medicineoftravaitime','medicinetravaireminderdatepicker','medicinetravairemindertimepicker','medicinetravairemindersentloaderbtn','Work Medecine Expired','#partnerremindermaindiv_5')">Send</button>
                 </div>      
             </div>
             <?php $e++; ?>
              <?php endif;?>
               <?php endif; ?>
               <?php endforeach; ?> 

              <?php $e1=0; ?>
             <div class="col-md-12 pdhz" id="partnerremindermaindiv_5">
               <?php foreach ($partnerreminders as $reminder): ?>
                <?php if(!empty($reminder->medicineoftravaidate) || $reminder->medicineoftravaidate != null): ?>
                  <?php if($e1 != 0): ?>
                 <div class="col-md-12 pdhz">
                         
                      <div class="col-md-2 pdhz text-center" ><?= from_unix_date($reminder->medicineoftravaidate); ?></div>
                      <div class="col-md-2 pdhz text-center" ><?= $reminder->medicineoftravaitime ?></div>
                      <div class="col-md-3 pdhz text-center" ><div class="col-md-10 col-md-offset-2 pdhz text-left">Work Medecine Expired</div></div>      
                       <div class="col-md-1 pdhz text-center" >
                                  <a href="<?= base_url(); ?>admin/partners/reminder_pdf/<?= $reminder->id  ?>/<?= $partner->id ?>/5/1" target="_blank">
                                    <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                                  </a>
                      </div>
                      <div class="col-md-2 pdhz text-center" ><?= ($reminder->medicineoftravaiissent == '1')?"Sent":"Pending"; ?></div>
                 </div>
                  <?php endif;?>
                 <?php $e1++; ?>
                 <?php endif; ?>
               <?php endforeach; ?>  
               </div> 
              <!-- Medicine Travel Reminder -->   

    </div>

