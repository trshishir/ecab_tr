             <?php echo form_open_multipart('admin/partners/profileAdd'); ?>

                   <div class="col-md-12 pdhz">
                     <div class="col-md-9 pdhz">
                       <!-- Row 1 -->
                         <div class="col-md-12 pdhz" style="margin-top:10px;" >
                                 <div class="col-md-4">
                                     <div class="form-group">
                                         <div class="col-md-4" >
                                             <span style="font-weight: normal;">Statut : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;">

                                             <select class="form-control" name="statut" required>
                                                <option value="">Select</option>
                                                  <?php foreach ($partner_status_data as $item): ?>
                                             <option  value="<?= $item->id ?>"><?= $item->status ?></option>
                                             <?php endforeach; ?>
                                             </select>
                                         </div>
                                     </div>
                                 </div>

                                <div class="col-md-4">
                                        <div class="form-group">
                                              <div class="col-md-4" >
                                                  <span style="font-weight: normal;">Civility : </span>
                                              </div>
                                              <div class="col-md-8" style="padding: 0px;">
                                                  <select class="form-control" name="civilite" required>
                                                        <option value="">Select</option>
                                                      <?php foreach ($civilite_data as $item): ?>
                                                  <option value="<?= $item->id ?>"><?= $item->civilite ?></option>
                                                      <?php endforeach; ?>
                                                  </select>
                                              </div>
                                        </div>
                                </div>

                                 <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-4" >
                                            <span style="font-weight: normal;">First Name : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="prenom" class="form-control" type="text" required>
                                        </div>
                                    </div>
                                </div>
                              
                           </div>


 

                           <!-- Row 2 -->
                        <div class="col-md-12 pdhz" style="margin-top:10px;" >                          
                             <div class="col-md-4">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Last Name : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input name="nom" class="form-control" type="text" required>
                                     </div>

                                 </div>
                             </div>
                             <div class="col-md-4">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Category : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <select class="form-control" id="partner_category" name="category" required>     
                                             <option  value="0">Independent</option>
                                             <option  value="1">Company</option>
                                         </select>
                                     </div>
                                 </div>
                             </div>
                             <div class="col-md-4">
                                    <div class="form-group" id="partner_category_div" style="display: none;">
                                        <div class="col-md-4" >
                                            <span style="font-weight: normal;">Company : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="company" class="form-control" type="text" placeholder="Company Name">
                                        </div>
                                    </div>
                            </div>
                        </div>

                           <!-- Row 3 -->
                           <div class="col-md-12 pdhz" style="margin-top: 10px;">
                                <div class="col-md-4">
                                     <div class="form-group">
                                         <div class="col-md-4" >
                                             <span style="font-weight: normal;">Date of Birth : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;">
                                            <span id="dob_span"></span>
                                             <input name="dob" id="date_of_birth_field" class="form-control datepicker" value="<?php echo date('d/m/Y');?>"
                                                 type="text">
                                         </div>
                                     </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" >
                                         <div class="col-md-4" >
                                             <span style="font-weight: normal;">Email : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;">
                                             <input name="email" class="form-control" style="width:100%;" type="email" required>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                          <div class="col-md-4" >
                                              <span style="font-weight: normal;">Phone : </span>
                                          </div>
                                          <div class="col-md-8" style="padding: 0px;">
                                              <input name="phone" class="form-control" style="width:100%;" type="phone">
                                          </div>
                                      </div>
                                 </div>
                           </div>
                           <!-- Row 4 -->
                        <div class="col-md-12 pdhz" style="margin-top:10px;" >
                                <div class="col-md-4">  
                                     <div class="form-group">
                                         <div class="col-md-4" >
                                             <span style="font-weight: normal;">Address : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;">
                                             <input name="address" class="form-control" type="text" placeholder="address">
                                         </div>
                                     </div>
                                 </div>
                                <div class="col-md-4">  
                                     <div class="form-group">
                                         <div class="col-md-4" >
                                             <span style="font-weight: normal;">Address 2 : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;">
                                             <input name="address2" class="form-control" type="text" placeholder="address 2">
                                         </div>
                                     </div>
                                 </div>
                                <div class="col-md-4">
                                     <div class="form-group">
                                         <div class="col-md-4" >
                                             <span style="font-weight: normal;">Zip Code : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;">
                                             <input name="zipcode" class="form-control" type="text" >
                                         </div>
                                     </div>
                                 </div>
                               
                           </div> 
                        
                          </div>
                          <div class="col-md-3" style="text-align: center;margin-top:10px;">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                        
                                     </div>
                                     <div class="col-md-5" style="padding: 0px;position: relative;">
                                      <button type="button" id="defaultimgremovebtn" onclick="removedefaultpartnerimage(this);" class="minusrediconcustomreminder" style="display:none;z-index: 20;top: 0px;right: 15px;"><i class="fa fa-minus" style="margin:0px !important;"></i></button>
                                     <img id="preview_partner" alt="Preview Logo"
                                         src="<?= base_url() ?>/assets/images/no-preview.jpg"
                                         style="border: 1px solid #75b0d7;cursor: pointer;height: 170px;width: 170px;position: relative;z-index: 10;background-color: #fff;">
                                 
                                     </div>
                                 </div>      
                               </div>  
                           </div>      

                       
                                    <!-- Section 2 -->
                              <!-- Row 1 -->                                  
                            <div class="col-md-12 pdhz" style="margin-top: 10px;">
                               <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" >
                                        <span style="font-weight: normal;">Country : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                    <select required="required"  class="form-control" name="country" id="country_id" tabindex="1">
                                        <option value="">Select Country</option>
                                        <?php foreach($countries as $data):?>
                                            <option value="<?=$data->id;?>"><?=$data->name;?></option>
                                        <?php endforeach;?>
                                    </select>
                                    </div>
                                </div>
                            </div>  
                           <div class="col-md-3">
                               <div class="form-group">
                                   <div class="col-md-4" >
                                       <span style="font-weight: normal;">Region : </span>
                                   </div>
                                   <div class="col-md-8" style="padding: 0px;">
                                      <div class="form-group">
                                         
                                           <select required class="form-control" name="region" id="region_id"></select>
                                       </div>
                                   </div>

                               </div>
                           </div> 
                           <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" >
                                        <span style="font-weight: normal;">City : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                    <select required="required" class="form-control" name="city" id="cities_id" tabindex="1">
                                    </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;" id="defaultimgspan">Default Image : </span>
                                     </div>
                                     <div class="col-md-8">
                                         <input class="partner_image" name="profile_image" type="file" required>
                                     </div>

                                 </div>
                             </div>
                            </div>
                            <!-- Row 2 -->  
                            <div class="col-md-12 pdhz" style="margin-top: 10px;">

                                <div class="col-md-3">
                                  <div class="form-group">
                                        <div class="col-md-4" >
                                            <span style="font-weight: normal;">Mobile : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="mobile" class="form-control" style="width:100%;" type="phone">
                                        </div>
                                    </div>
                               </div>
                                <div class="col-md-3">
                                  <div class="form-group">
                                        <div class="col-md-4" >
                                            <span style="font-weight: normal;">Fax : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="fax" class="form-control" style="width:100%;" type="text">
                                        </div>
                                    </div>
                               </div>
                            </div>
                            
                        <!-- Row 2 -->


                         <div class="col-md-12 pdhz" style="margin-top: 10px;">
                             <div class="col-md-3">
                                  <div class="form-group">
                                      <div class="col-md-4" >
                                          <span style="font-weight: normal;">Source : </span>
                                      </div>
                                      <div class="col-md-8" style="padding: 0px;">
                                          <select class="form-control" name="source" required>
                                            <option value="">Select</option>
                                            <option value="xyz">xyz</option>
                                          </select>
                                      </div>
                                  </div>
                              </div>
                             <div class="col-md-3">
                                  <div class="form-group">
                                      <div class="col-md-4" >
                                          <span style="font-weight: normal;">Partner Advisor : </span>
                                      </div>
                                      <div class="col-md-8" style="padding: 0px;">
                                          <select class="form-control" name="advisor" required>
                                            <option value="">Select</option>
                                               <?php foreach($users_data as $item):?>
                                          <option  value="<?= $item['id'] ?>"><?= $item['civility']." ".$item['first_name']." ".$item['last_name'];?></option>
                                          <?php endforeach; ?>
                                          </select>
                                      </div>
                                  </div>
                              </div>

                              <div class="col-md-3">
                                   <div class="form-group">
                                       <div class="col-md-4" >
                                           <span style="font-weight: normal;">Partner Support : </span>
                                       </div>
                                       <div class="col-md-8" style="padding: 0px;">
                                           <select class="form-control" name="support" required>
                                            <option value="">Select</option>
                                                <?php foreach($users_data as $item):?>
                                           <option  value="<?= $item['id'] ?>"><?= $item['civility']." ".$item['first_name']." ".$item['last_name'];?></option>
                                           <?php endforeach; ?>
                                           </select>
                                       </div>
                                   </div>
                               </div>
                             
                         </div>

                        
                          <div class="col-md-12" style="padding-bottom: 20px;">                       
                              <!-- icons -->
                               <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                              <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelProfile()"><span class="fa fa-close"> Cancel </span></button>
                              <!-- icons -->  
                          </div>

                     <?php echo form_close(); ?>
 