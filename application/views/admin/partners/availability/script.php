<script type="text/javascript">
  $(document).ready(function() {

           $(document).on('change','#availability_country_id', function() {
                      availability_get_region_base_country(this);
                  });
           $(document).on('change','#availability_region_id', function() {
                      availability_get_cities_base_country_region(this);
                  });
           $(document).on('change','#edit_availability_country_id', function() {
                      edit_availability_get_region_base_country(this);
                  });
           $(document).on('change','#edit_availability_region_id', function() {
                      edit_availability_get_cities_base_country_region(this);
                  });

            $(document).on('change','input.chk-mainavailability-template', function() {
                      $('input.chk-mainavailability-template').not(this).prop('checked', false);
                      var id = $(this).attr('data-input');
                      $('.chk-Addavailability-btn').val(id);
                      $('#availabilitydeletid').val(id);
                  });        

  });
function availabilityAdd()
{
  setupDivAvailability();
  $(".Availabilityadd").show();
}

function cancelAvailability()
{
  setupDivAvailability();
  $(".ListAvailability").show();
}

function availabilityEdit()
{
    var val = $('.chk-Addavailability-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivAvailability();
        $(".availabilityEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/clients/get_ajax_availability_data'; ?>',
                data: {'availability_id': val},
                success: function (result) {
                  document.getElementsByClassName("availabilityEditajax")[0].innerHTML = result;
                
                }
            });
}
function availabilityidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivAvailability();
      $(".availabilityEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/clients/get_ajax_availability_data'; ?>',
              data: {'availability_id': val},
              success: function (result) {
                document.getElementsByClassName("availabilityEditajax")[0].innerHTML = result;
               
              }
          });
}
function availabilityDelete()
{
  var val = $('.chk-Addavailability-btn').val();
  if(val != "")
  {
  setupDivAvailability();
  $(".availabilityDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivAvailability()
{
  $(".Availabilityadd").hide();
  $(".availabilityEdit").hide();
  $(".ListAvailability").hide();
  $(".availabilityDelete").hide();

}
function setupDivAvailabilityConfig()
{
  $(".Availabilityadd").hide();
  $(".availabilityEdit").hide();
  $(".ListAvailability").show();
  $(".availabilityDelete").hide();

}


  function availability_get_region_base_country(region_id=null)
   {
     country_id=$('#availability_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
         html ='';
         html =html + '<option value="">Select Region</option>';
         
         $.each(response, function( index, value ) {  
         if(region_id==value.id)
         {
           html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
         }
         else{
           html =html + '<option value="'+value.id+'">'+value.name+'</option>';
         }
         });
         $('#availability_region_id').html(html);
       }
   }); 
}

function availability_get_cities_base_country_region(region_id=null,cities_id=null)
{

region_id=$('#availability_region_id').val();

$.ajax({
    url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
    method: 'post',
    data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
    dataType: 'json',
    success: function(response){
    console.log(response);
    html ='';
    html =html + ' <option value="">Select cities</option>';

    $.each(response, function( index, value ) {
    // console.log( value );
                

    if(cities_id==value.id)
    {
        html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';

    }
    else{
        html =html + '<option value="'+value.id+'">'+value.name+'</option>';

    }
    });

    $('#availability_cities_id').html(html);


    }


});
}


//For Edit Page

function edit_availability_get_cities_base_country_region(region_id=null,cities_id=null)
{
    region_id=$('#edit_availability_region_id').val();
    $.ajax({
        url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
        method: 'post',
        data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
        dataType: 'json',
        success: function(response){
            html ='';
            html =html + ' <option value="">Select cities</option>';

            $.each(response, function( index, value ) {
            if(cities_id==value.id)
            {
                html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
            }
            else{
                html =html + '<option value="'+value.id+'">'+value.name+'</option>';
            }
            });
            $('#edit_availability_cities_id').html(html);
        }
    });
}
   function edit_availability_get_region_base_country(region_id=null)
   { 
     country_id=$('#edit_availability_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
     html ='';
     html =html + '<option value="">Select Region</option>';
     
     $.each(response, function( index, value ) {
     if(region_id==value.id)
     {
       html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';  
     }
     else{
       html =html + '<option value="'+value.id+'">'+value.name+'</option>';
     }
     });
     $('#edit_availability_region_id').html(html);
     }   
     }); 
   }
//For Edit Page

//create form submit
$(document).on('submit',"form#addavailabilityform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivAvailability();
         $('.ListAvailability').empty();
         $('.ListAvailability').html(data.record);
         createinnerconfigtable();
         availabilityEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivAvailability();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListAvailability").show();
        }
     document.getElementById("addavailabilityform").reset();
      }
    });
    return false;
  });
//create form submit

//update form submit
$(document).on('submit',"form#updateavailabilityform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivAvailability();
         $('.ListAvailability').empty();
         $('.ListAvailability').html(data.record);
         createinnerconfigtable();
         availabilityEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivAvailability();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListAvailability").show();
        }
     document.getElementById("addavailabilityform").reset();
      }
    });
    return false;
  });
//update form submit

//delete form submit
$(document).on('submit',"form#deleteavailabilityform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivAvailability();
         $('.ListAvailability').empty();
         $('.ListAvailability').html(data.record);
         createinnerconfigtable();
         availabilityEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivAvailability();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListAvailability").show();
        }
     document.getElementById("addavailabilityform").reset();
      }
    });
    return false;
  });
//delete form submit
</script>
