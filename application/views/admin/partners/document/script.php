<script type="text/javascript">
  $(document).ready(function() {

           $(document).on('change','#document_country_id', function() {
                      document_get_region_base_country(this);
                  });
           $(document).on('change','#document_region_id', function() {
                      document_get_cities_base_country_region(this);
                  });
           $(document).on('change','#edit_document_country_id', function() {
                      edit_document_get_region_base_country(this);
                  });
           $(document).on('change','#edit_document_region_id', function() {
                      edit_document_get_cities_base_country_region(this);
                  });

            $(document).on('change','input.chk-maindocument-template', function() {
                      $('input.chk-maindocument-template').not(this).prop('checked', false);
                      var id = $(this).attr('data-input');
                      $('.chk-Adddocument-btn').val(id);
                      $('#documentdeletid').val(id);
                  });
      for(var j=1;j<6;j++){
         $(document).on('change','#editdatedexpiration'+j, function() {
       
           var exprieddate=$(this).val();
           exprieddate = exprieddate.split("/");
           exprieddate=exprieddate[2] + "-"+exprieddate[1]+"-"+exprieddate[0];
          
           var today = new Date();
           var dd = String(today.getDate()).padStart(2, '0');
           var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
           var yyyy = today.getFullYear();

           today =  yyyy + '-' + mm + '-'+ dd;
           var d1 = Date.parse(today);
           var d2 = Date.parse(exprieddate);
           if (d1 > d2) {
               $(this).removeClass("greenbg");
               $(this).addClass("redbg");
           }else{
               $(this).removeClass("redbg");
               $(this).addClass("greenbg");
           }
                 
       });
      }
      for(var j=1;j<6;j++){
         $(document).on('change','#datedexpiration'+j, function() {
           var exprieddate=$(this).val();
           exprieddate = exprieddate.split("/");
           exprieddate=exprieddate[2] + "-"+exprieddate[1]+"-"+exprieddate[0];
          
           var today = new Date();
           var dd = String(today.getDate()).padStart(2, '0');
           var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
           var yyyy = today.getFullYear();

           today =  yyyy + '-' + mm + '-'+ dd;
           var d1 = Date.parse(today);
           var d2 = Date.parse(exprieddate);
           if (d1 > d2) {
               $(this).removeClass("greenbg");
               $(this).addClass("redbg");
           }else{
               $(this).removeClass("redbg");
               $(this).addClass("greenbg");
           }
                 
       });
      }        

  });
function documentAdd()
{
  setupDivDocument();
  $(".documentAdd").show();
}

function cancelDocument()
{
  setupDivDocument();
  $(".ListDocument").show();
}

function documentEdit()
{
    var val = $('.chk-Adddocument-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivDocument();
        $(".documentEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/partners/documentAjax'; ?>',
                data: {'document_id': val},
                success: function (result) {
                  document.getElementsByClassName("documentEditajax")[0].innerHTML = result;
                   colorchange();
                
                }
            });
}
function documentidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivDocument();
      $(".documentEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/partners/documentAjax'; ?>',
              data: {'document_id': val},
              success: function (result) {
                document.getElementsByClassName("documentEditajax")[0].innerHTML = result;
                 colorchange();
               
              }
          });
}
function documentDelete()
{
  var val = $('.chk-Adddocument-btn').val();
  if(val != "")
  {
  setupDivDocument();
  $(".documentDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivDocument()
{
  $(".documentAdd").hide();
  $(".documentEdit").hide();
  $(".ListDocument").hide();
  $(".documentDelete").hide();

}
function setupDivDocumentConfig()
{
  $(".documentAdd").hide();
  $(".documentEdit").hide();
  $(".ListDocument").show();
  $(".documentDelete").hide();

}


  function document_get_region_base_country(region_id=null)
   {
     country_id=$('#document_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
         html ='';
         html =html + '<option value="">Select Region</option>';
         
         $.each(response, function( index, value ) {  
         if(region_id==value.id)
         {
           html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
         }
         else{
           html =html + '<option value="'+value.id+'">'+value.name+'</option>';
         }
         });
         $('#document_region_id').html(html);
       }
   }); 
}

function document_get_cities_base_country_region(region_id=null,cities_id=null)
{

region_id=$('#document_region_id').val();

$.ajax({
    url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
    method: 'post',
    data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
    dataType: 'json',
    success: function(response){
    console.log(response);
    html ='';
    html =html + ' <option value="">Select cities</option>';

    $.each(response, function( index, value ) {
    // console.log( value );
                

    if(cities_id==value.id)
    {
        html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';

    }
    else{
        html =html + '<option value="'+value.id+'">'+value.name+'</option>';

    }
    });

    $('#document_cities_id').html(html);


    }


});
}


//For Edit Page

function edit_document_get_cities_base_country_region(region_id=null,cities_id=null)
{
    region_id=$('#edit_document_region_id').val();
    $.ajax({
        url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
        method: 'post',
        data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
        dataType: 'json',
        success: function(response){
            html ='';
            html =html + ' <option value="">Select cities</option>';

            $.each(response, function( index, value ) {
            if(cities_id==value.id)
            {
                html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
            }
            else{
                html =html + '<option value="'+value.id+'">'+value.name+'</option>';
            }
            });
            $('#edit_document_cities_id').html(html);
        }
    });
}
   function edit_document_get_region_base_country(region_id=null)
   { 
     country_id=$('#edit_document_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
     html ='';
     html =html + '<option value="">Select Region</option>';
     
     $.each(response, function( index, value ) {
     if(region_id==value.id)
     {
       html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';  
     }
     else{
       html =html + '<option value="'+value.id+'">'+value.name+'</option>';
     }
     });
     $('#edit_document_region_id').html(html);
     }   
     }); 
   }
//For Edit Page

//create form submit
$(document).on('submit',"form#create_document_form",function(event){
   
     var str = new FormData(this);
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      processData: false,
      cache:false,
      contentType: false,
      success: function(data) {
        if(data.result == '200'){
          //alert(data.record);
         setupDivDocument();
         $('.ListDocument').empty();
         $('.ListDocument').html(data.record);
         createinnerconfigtable('.configInnerDocumentTable');
         documentEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivDocument();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListDocument").show();
        }
     document.getElementById("create_document_form").reset();
      }
    });
    return false;
  });
//create form submit

//update form submit
$(document).on('submit',"form#update_document_form",function(event){
    
      var str = new FormData(this);
     var action = $(this).attr('action'); 
     $.ajax({
    type: "POST",
    url: action,
    data:str,
    processData: false,
    cache:false,
    contentType: false,
      success: function(data) {
        if(data.result == '200'){
          
         setupDivDocument();
         $('.ListDocument').empty();
         $('.ListDocument').html(data.record);
         createinnerconfigtable('.configInnerDocumentTable');
         documentEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
           
          setupDivDocument();
          var msg=data.message;
          var msgType=data.messageType;
          var className=data.className;
          var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
           if ($("#inner_alert_div").hasClass("alert-success")) {
            $('#inner_alert_div').removeClass("alert-success");
          }
          if ($("#inner_alert_div").hasClass("alert-danger")) {
            $('#inner_alert_div').removeClass("alert-danger");
          }
          $('#inner_alert_div').addClass(className);
          $('#inner_message_div').empty();
          $('#inner_message_div').html(htmlContent);
          $('#inner_alert_div').show();
          $(".ListDocument").show();
        }
     document.getElementById("update_document_form").reset();
      }
    });
    return false;
  });
//update form submit

//delete form submit
$(document).on('submit',"form#deletedocumentform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){

         setupDivDocument();
         $('.ListDocument').empty();
         $('.ListDocument').html(data.record);
         createinnerconfigtable('.configInnerDocumentTable');
         documentEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivDocument();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListDocument").show();
        }
     document.getElementById("adddocumentform").reset();
      }
    });
    return false;
  });
//delete form submit
function colorchange(){
         for(var j=1;j<6;j++){
          
    
         var exprieddate=$('#editdatedexpiration'+j).val();
         exprieddate = exprieddate.split("/");
         exprieddate=exprieddate[2] + "-"+exprieddate[1]+"-"+exprieddate[0];
        
         var today = new Date();
         var dd = String(today.getDate()).padStart(2, '0');
         var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
         var yyyy = today.getFullYear();

         today =  yyyy + '-' + mm + '-'+ dd;
         var d1 = Date.parse(today);
         var d2 = Date.parse(exprieddate);

         if (d1 > d2) {
          
           $('#editdatedexpiration'+j).addClass("redbg");
           }else{
          
           $('#editdatedexpiration'+j).addClass("greenbg");
         }
               
       
    }
}
</script>
