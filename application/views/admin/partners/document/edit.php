            <?php
                                 $datedexpiration = strtotime($document->datedexpiration);
                                 $datedexpiration = date('d/m/Y',$datedexpiration);
                                 $datedexpiration = $datedexpiration;


                                 $datedexpiration2 = strtotime($document->datedexpiration2);
                                 $datedexpiration2 = date('d/m/Y',$datedexpiration2);
                                 $datedexpiration2 = $datedexpiration2;


                                 $datedexpiration3 = strtotime($document->datedexpiration3);
                                 $datedexpiration3 = date('d/m/Y',$datedexpiration3);
                                 $datedexpiration3 = $datedexpiration3;

                                 $datedexpiration4 = strtotime($document->datedexpiration4);
                                 $datedexpiration4 = date('d/m/Y',$datedexpiration4);
                                 $datedexpiration4 = $datedexpiration4;

                                 $datedexpiration5 = strtotime($document->datedexpiration5);
                                 $datedexpiration5 = date('d/m/Y',$datedexpiration5);
                                 $datedexpiration5 = $datedexpiration5;

                                 $datedelivrance1 = strtotime($document->datedelivrance1);
                                 $datedelivrance1 = date('d/m/Y',$datedelivrance1);
                                 $datedelivrance1 = $datedelivrance1;

                                 $datedelivrance2 = strtotime($document->datedelivrance2);
                                 $datedelivrance2 = date('d/m/Y',$datedelivrance2);
                                 $datedelivrance2 = $datedelivrance2;

                                 $datedelivrance3 = strtotime($document->datedelivrance3);
                                 $datedelivrance3 = date('d/m/Y',$datedelivrance3);
                                 $datedelivrance3 = $datedelivrance3;

                                 $datedelivrance4 = strtotime($document->datedelivrance4);
                                 $datedelivrance4 = date('d/m/Y',$datedelivrance4);
                                 $datedelivrance4 = $datedelivrance4;

           ?>



              <?php
               $attributes = array("name" => 'update_document_form', "id" => 'update_document_form'); 
               echo form_open_multipart('admin/partners/documentEdit',$attributes); ?>
                    <input type="hidden" value="<?= $document->id ?>" name="document_id">
                    <div class="col-md-12 pdhz" style="margin-top:10px;" >
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Statut : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                       <select class="form-control" name="statut">
                                           <option value="">Select</option>
                                           <option <?php if("1" == $document->statut){ echo "selected"; } ?> value="1">Show</option>
                                           <option <?php if("0" == $document->statut){ echo "selected"; } ?> value="0">Hide</option> 
                                       </select>
                                    </div>
                                </div>
                            </div>
                      </div>
                        <!--  Row 3 b-->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <span style="font-weight: normal;">Identity Category : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <select class="form-control" name="typedepiecedidentite" required>
                                             <option value="">Select</option>
                                                <?php foreach ($type_data as $item): ?>
                                            <option <?php if($document->typedepiecedidentite == $item->id){ echo "selected"; } ?> value="<?= $item->id ?>"><?= $item->type ?></option>
                                                <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <span style="font-weight: normal;">Upload : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <input  name="upload1" type="file">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-5" >
                                        <span style="font-weight: normal;">Identity Number :</span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <input required="" name="numerropiecedidentite" class="form-control"
                                            type="text" value="<?=  $document->numerropiecedidentite ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <span style="font-weight: normal;">Expiry Date : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <input  name="datedexpiration" id="editdatedexpiration1" class="form-control datepicker" type="text" value="<?=  $datedexpiration ?>">
                                    </div>

                                </div>
                            </div>

                        </div>

                      <!-- Row 4 -->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-5" style="padding-right: 0px;">
                                        <span style="font-weight: normal;">Driver Licence Number : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <input  name="pumeropermis" class="form-control" type="text" value="<?=  $document->pumeropermis ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <span style="font-weight: normal;">Upload : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <input  name="upload2" type="file">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <span style="font-weight: normal;">Delivery Date : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <input  name="datedelivrance1" class="form-control datepicker" type="text" value="<?=  $datedelivrance1 ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                            
                                    <div class="form-group">
                                        <div class="col-md-5">
                                            <span style="font-weight: normal;">Expiry Date : </span>
                                        </div>
                                        <div class="col-md-7" style="padding: 0px; ">
                                            <input  name="datedexpiration2" id="editdatedexpiration2" class="form-control datepicker" type="text" value="<?=  $datedexpiration2 ?>">
                                        </div>

                                    </div>
                            </div>

                        </div>

                      <!-- Row 5 -->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-5" style="padding-right: 0px;">
                                        <span style="font-weight: normal;">Medical Certificate : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <input  name="certificatmedicate" type="file">
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <span style="font-weight: normal;">Delivery Date : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <input  name="datedelivrance2" class="form-control datepicker" type="text" value="<?=  $datedelivrance2 ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <span style="font-weight: normal;">Expiry Date : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <input  name="datedexpiration3" id="editdatedexpiration3" class="form-control datepicker" type="text" value="<?=  $datedexpiration3 ?>">
                                    </div>

                                </div>
                            </div>
                             <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <span style="font-weight: normal;">Diploma1 : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <input  name="autrdeiplome1" type="file">
                                    </div>
                                </div>
                            </div>
                           
                        </div>

                        <!-- Row 6 -->
                       <div class="col-md-12 pdhz" style="margin-top: 10px;">

                          <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <span style="font-weight: normal;">PSC1 : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <input  name="psc1" type="file">
                                    </div>
                                </div>
                            </div>



                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <span style="font-weight: normal;">Delivery Date : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <input  name="datedelivrance3" class="form-control datepicker"  type="text" value="<?=  $datedelivrance3 ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <span style="font-weight: normal;">Expiry Date : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <input  name="datedexpiration4" id="editdatedexpiration4" class="form-control datepicker" type="text"  value="<?=  $datedexpiration4 ?>">
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <span style="font-weight: normal;">Diploma2 : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <input  name="autrdeiplome2" type="file">
                                    </div>
                                </div>
                            </div>
                           
                        </div>

                      <!-- Row 7 -->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">

                            
                      
                                    

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <span style="font-weight: normal;">Work Medecine : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <input  name="medecinedetravai" type="file">
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <span style="font-weight: normal;">Delivery Date : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <input  name="datedelivrance4" class="form-control datepicker"  type="text" value="<?=  $datedelivrance4 ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <span style="font-weight: normal;">Expiry Date : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <input  name="datedexpiration5" id="editdatedexpiration5" class="form-control  datepicker"  type="text" value="<?=  $datedexpiration5 ?>">
                                    </div>

                                </div>
                            </div>
                             <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <span style="font-weight: normal;">Diploma3 : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <input  name="autrdeiplome3" type="file">
                                    </div>

                                </div>
                            </div>

                        </div>
                        
                          <div class="col-md-12" style="padding: 30px;">                       
                              <!-- icons -->
                               <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                              <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelDocument()"><span class="fa fa-close"> Cancel </span></button>
                              <!-- icons -->  
                          </div>

                     <?php echo form_close(); ?>
 