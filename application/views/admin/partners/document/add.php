             <?php
              $attributes = array("name" => 'create_document_form', "id" => 'create_document_form'); 
              echo form_open_multipart('admin/partners/documentAdd',$attributes); ?>
                <input type="hidden" value="<?= $partner->id ?>" name="profile_id">
                      <!-- Row 1 -->
                        <div class="col-md-12 pdhz" style="margin-top:10px;" >
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-5">
                                            <span style="font-weight: normal;">Statut : </span>
                                        </div>
                                        <div class="col-md-7" style="padding: 0px;">
                                           <select class="form-control" name="statut">
                                               <option value="">Select</option>
                                               <option value="1">Show</option>
                                               <option value="0">Hide</option> 
                                           </select>
                                        </div>
                                    </div>
                                </div>
                          </div>
                      <!--  Row 3 b-->
                         <div class="col-md-12 pdhz" style="margin-top: 10px;">

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-5" >
                                         <span style="font-weight: normal;">Identity Category : </span>
                                     </div>
                                     <div class="col-md-7" style="padding: 0px;">
                                         <select class="form-control" name="typedepiecedidentite" required>
                                              <option value="">Select</option>
                                                 <?php foreach ($type_data as $item): ?>
                                             <option value="<?= $item->id ?>"><?= $item->type ?></option>
                                                 <?php endforeach; ?>
                                         </select>
                                     </div>
                                 </div>
                             </div>
                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-5" >
                                         <span style="font-weight: normal;">Upload : </span>
                                     </div>
                                     <div class="col-md-7" style="padding: 0px;">
                                         <input required="" name="upload1" type="file">
                                     </div>
                                 </div>
                             </div>

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-5" >
                                         <span style="font-weight: normal;">Identity Number : </span>
                                     </div>
                                     <div class="col-md-7" style="padding: 0px;">
                                         <input required="" name="numerropiecedidentite" class="form-control"
                                             type="text">
                                     </div>
                                 </div>
                             </div>

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-5" >
                                         <span style="font-weight: normal;">Expiry Date : </span>
                                     </div>
                                     <div class="col-md-7" style="padding: 0px;">
                                         <input  name="datedexpiration" id="datedexpiration1" class="form-control datepicker" value="<?php echo date('d/m/Y');?>"
                                             type="text">
                                     </div>

                                 </div>
                             </div>

                         </div>

                       <!-- Row 4 -->
                         <div class="col-md-12 pdhz" style="margin-top: 10px;">

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-5" style="padding-right: 0px;">
                                         <span style="font-weight: normal;">Driver Licence Number : </span>
                                     </div>
                                     <div class="col-md-7" style="padding: 0px;">
                                         <input  name="pumeropermis" class="form-control" type="text">
                                     </div>
                                 </div>
                             </div>
                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-5" >
                                         <span style="font-weight: normal;">Upload : </span>
                                     </div>
                                     <div class="col-md-7" style="padding: 0px;">
                                         <input  name="upload2" type="file">
                                     </div>
                                 </div>
                             </div>

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-5" >
                                         <span style="font-weight: normal;">Delivery Date : </span>
                                     </div>
                                     <div class="col-md-7" style="padding: 0px;">
                                         <input  name="datedelivrance1" class="form-control datepicker" value="<?php echo date('d/m/Y');?>"
                                             type="text">
                                     </div>
                                 </div>
                             </div>

                             <div class="col-md-3">
                             
                                     <div class="form-group">
                                         <div class="col-md-5" >
                                             <span style="font-weight: normal;">Expiry Date : </span>
                                         </div>
                                         <div class="col-md-7" style="padding: 0px;">
                                             <input  name="datedexpiration2" id="datedexpiration2" class="form-control datepicker" value="<?php echo date('d/m/Y');?>"
                                                 type="text">
                                         </div>

                                     </div>
                             </div>

                         </div>

                       <!-- Row 5 -->
                         <div class="col-md-12 pdhz" style="margin-top: 10px;">

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-5" style="padding-right: 0px;">
                                         <span style="font-weight: normal;">Medical Certificate : </span>
                                     </div>
                                     <div class="col-md-7" style="padding: 0px;">
                                         <input  name="certificatmedicate" type="file">
                                     </div>
                                 </div>
                             </div>


                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-5" >
                                         <span style="font-weight: normal;">Delivery Date : </span>
                                     </div>
                                     <div class="col-md-7" style="padding: 0px;">
                                         <input  name="datedelivrance2" class="form-control datepicker" value="<?php echo date('d/m/Y');?>"
                                             type="text">
                                     </div>
                                 </div>
                             </div>

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-5" >
                                         <span style="font-weight: normal;">Expiry Date : </span>
                                     </div>
                                     <div class="col-md-7" style="padding: 0px;">
                                         <input  name="datedexpiration3" id="datedexpiration3" class="form-control datepicker" value="<?php echo date('d/m/Y');?>"
                                             type="text">
                                     </div>

                                 </div>
                             </div>
                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-5" >
                                         <span style="font-weight: normal;">Diploma1 : </span>
                                     </div>
                                     <div class="col-md-7" style="padding: 0px;">
                                         <input  name="autrdeiplome1" type="file">
                                     </div>
                                 </div>
                             </div>
                            
                         </div>

                         <!-- Row 6 -->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">

                           <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-5" >
                                         <span style="font-weight: normal;">PSC1 : </span>
                                     </div>
                                     <div class="col-md-7" style="padding: 0px;">
                                         <input  name="psc1" type="file">
                                     </div>
                                 </div>
                             </div>



                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-5" >
                                         <span style="font-weight: normal;">Delivery Date : </span>
                                     </div>
                                     <div class="col-md-7" style="padding: 0px;">
                                         <input  name="datedelivrance3" class="form-control datepicker" value="<?php echo date('d/m/Y');?>"
                                             type="text">
                                     </div>
                                 </div>
                             </div>

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-5" >
                                         <span style="font-weight: normal;">Expiry Date : </span>
                                     </div>
                                     <div class="col-md-7" style="padding: 0px;">
                                         <input  name="datedexpiration4" id="datedexpiration4" class="form-control datepicker" value="<?php echo date('d/m/Y');?>"
                                             type="text">
                                     </div>

                                 </div>
                             </div>
                              <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-5" >
                                         <span style="font-weight: normal;">Diploma2 : </span>
                                     </div>
                                     <div class="col-md-7" style="padding: 0px;">
                                         <input  name="autrdeiplome2" type="file">
                                     </div>
                                 </div>
                             </div>
                            
                         </div>

                       <!-- Row 7 -->
                         <div class="col-md-12 pdhz" style="margin-top: 10px;">

                             
                  
                                     

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-5" >
                                         <span style="font-weight: normal;">Work Medecine : </span>
                                     </div>
                                     <div class="col-md-7" style="padding: 0px;">
                                         <input  name="medecinedetravai" type="file">
                                     </div>
                                 </div>
                             </div>


                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-5" >
                                         <span style="font-weight: normal;">Delivery Date : </span>
                                     </div>
                                     <div class="col-md-7" style="padding: 0px;">
                                         <input  name="datedelivrance4" class="form-control datepicker" value="<?php echo date('d/m/Y');?>"
                                             type="text">
                                     </div>
                                 </div>
                             </div>

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-5" >
                                         <span style="font-weight: normal;">Expiry Date : </span>
                                     </div>
                                     <div class="col-md-7" style="padding: 0px;">
                                         <input  name="datedexpiration5" id="datedexpiration5" class="form-control  datepicker"
                                           value="<?php echo date('d/m/Y');?>"   type="text">
                                     </div>

                                 </div>
                             </div>
                               <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-5" >
                                         <span style="font-weight: normal;">Diploma3 : </span>
                                     </div>
                                     <div class="col-md-7" style="padding: 0px;">
                                         <input  name="autrdeiplome3" type="file">
                                     </div>

                                 </div>
                             </div>

                         </div>
                       
                 
                          <div class="col-md-12" style="padding: 30px;">                       
                              <!-- icons -->
                               <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                              <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelDocument()"><span class="fa fa-close"> Cancel </span></button>
                              <!-- icons -->  
                          </div>

                     <?php echo form_close(); ?>
 