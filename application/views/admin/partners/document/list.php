  <input type="hidden" class="chk-Adddocument-btn" value="">
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configInnerDocumentTable cell-border  dataTable table data-table dataTable no-footer pdhz" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" style="width:2%;">#</th>
            <th class="text-center" ><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('time'); ?></th>
            <th class=" text-center">Identity Category</th>
            <th class="text-center">Identity Number</th>
            <th class=" text-center">Driver License Number</th>
            <th class="text-center">Statut</th>
            <th class="text-center">Since</th>
          </tr>
          </thead>
          <tbody id="documentlistdatabyid">
                   
                                        <?php 
                                        if (!empty($document_data)):  
                                        ?>
                                        <?php foreach($document_data as $key => $item):?>
                                            <tr>
                                              <td class="text-center">
                                                  <input type="checkbox" class="chk-maindocument-template" data-input="<?=$item->id?>"></td> 
                                              <td class="text-center"><a href="javascript:void()" onclick="documentidEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->created_at)) .'0000'. $item->id;?></a></td>
                                              <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                                              <td class="text-center"><?=date("H:i:s", strtotime($item->created_at))?></td>
                                      
                                              <td class="text-center"><?= $item->typedepiecedidentite ?></td>
                                              <td class="text-center"><?= $item->numerropiecedidentite ?></td>
                                              <td class="text-center"><?= $item->pumeropermis ?></td>

                                                                                                              
                                             <td class="text-center">
                                               <?php if(strtolower($item->statut) == "1" ): ?>
                                               <span class="label label-success">Show</span>
                                               <?php else: ?>
                                                 <span class="label label-danger">Hide</span>
                                               <?php endif; ?>
                                         
                                              </td>
                                                <td class="text-center"><?= timeDiff($item->created_at) ?></td>

                                            </tr>
                                           
                                        <?php endforeach; ?>
                                        <?php endif; ?>
          </tbody>
      </table>
      <br>
    </div>
  </div>