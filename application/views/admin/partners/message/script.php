<script type="text/javascript">
  $(document).ready(function() {

           $(document).on('change','#message_country_id', function() {
                      message_get_region_base_country(this);
                  });
           $(document).on('change','#message_region_id', function() {
                      message_get_cities_base_country_region(this);
                  });
           $(document).on('change','#edit_message_country_id', function() {
                      edit_message_get_region_base_country(this);
                  });
           $(document).on('change','#edit_message_region_id', function() {
                      edit_message_get_cities_base_country_region(this);
                  });

            $(document).on('change','input.chk-mainmessage-template', function() {
                      $('input.chk-mainmessage-template').not(this).prop('checked', false);
                      var id = $(this).attr('data-input');
                      $('.chk-Addmessage-btn').val(id);
                      $('#messagedeletid').val(id);
                  });        

  });
function messageAdd()
{
  setupDivMessage();
  $(".Messageadd").show();
}

function cancelMessage()
{
  setupDivMessage();
  $(".ListMessage").show();
}

function messageEdit()
{
    var val = $('.chk-Addmessage-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivMessage();
        $(".messageEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/clients/get_ajax_message_data'; ?>',
                data: {'message_id': val},
                success: function (result) {
                  document.getElementsByClassName("messageEditajax")[0].innerHTML = result;
                
                }
            });
}
function messageidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivMessage();
      $(".messageEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/clients/get_ajax_message_data'; ?>',
              data: {'message_id': val},
              success: function (result) {
                document.getElementsByClassName("messageEditajax")[0].innerHTML = result;
               
              }
          });
}
function messageDelete()
{
  var val = $('.chk-Addmessage-btn').val();
  if(val != "")
  {
  setupDivMessage();
  $(".messageDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivMessage()
{
  $(".Messageadd").hide();
  $(".messageEdit").hide();
  $(".ListMessage").hide();
  $(".messageDelete").hide();

}
function setupDivMessageConfig()
{
  $(".Messageadd").hide();
  $(".messageEdit").hide();
  $(".ListMessage").show();
  $(".messageDelete").hide();

}


  function message_get_region_base_country(region_id=null)
   {
     country_id=$('#message_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
         html ='';
         html =html + '<option value="">Select Region</option>';
         
         $.each(response, function( index, value ) {  
         if(region_id==value.id)
         {
           html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
         }
         else{
           html =html + '<option value="'+value.id+'">'+value.name+'</option>';
         }
         });
         $('#message_region_id').html(html);
       }
   }); 
}

function message_get_cities_base_country_region(region_id=null,cities_id=null)
{

region_id=$('#message_region_id').val();

$.ajax({
    url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
    method: 'post',
    data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
    dataType: 'json',
    success: function(response){
    console.log(response);
    html ='';
    html =html + ' <option value="">Select cities</option>';

    $.each(response, function( index, value ) {
    // console.log( value );
                

    if(cities_id==value.id)
    {
        html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';

    }
    else{
        html =html + '<option value="'+value.id+'">'+value.name+'</option>';

    }
    });

    $('#message_cities_id').html(html);


    }


});
}


//For Edit Page

function edit_message_get_cities_base_country_region(region_id=null,cities_id=null)
{
    region_id=$('#edit_message_region_id').val();
    $.ajax({
        url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
        method: 'post',
        data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
        dataType: 'json',
        success: function(response){
            html ='';
            html =html + ' <option value="">Select cities</option>';

            $.each(response, function( index, value ) {
            if(cities_id==value.id)
            {
                html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
            }
            else{
                html =html + '<option value="'+value.id+'">'+value.name+'</option>';
            }
            });
            $('#edit_message_cities_id').html(html);
        }
    });
}
   function edit_message_get_region_base_country(region_id=null)
   { 
     country_id=$('#edit_message_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
     html ='';
     html =html + '<option value="">Select Region</option>';
     
     $.each(response, function( index, value ) {
     if(region_id==value.id)
     {
       html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';  
     }
     else{
       html =html + '<option value="'+value.id+'">'+value.name+'</option>';
     }
     });
     $('#edit_message_region_id').html(html);
     }   
     }); 
   }
//For Edit Page

//create form submit
$(document).on('submit',"form#addmessageform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivMessage();
         $('.ListMessage').empty();
         $('.ListMessage').html(data.record);
         createinnerconfigtable();
         messageEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivMessage();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListMessage").show();
        }
     document.getElementById("addmessageform").reset();
      }
    });
    return false;
  });
//create form submit

//update form submit
$(document).on('submit',"form#updatemessageform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivMessage();
         $('.ListMessage').empty();
         $('.ListMessage').html(data.record);
         createinnerconfigtable();
         messageEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivMessage();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListMessage").show();
        }
     document.getElementById("addmessageform").reset();
      }
    });
    return false;
  });
//update form submit

//delete form submit
$(document).on('submit',"form#deletemessageform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivMessage();
         $('.ListMessage').empty();
         $('.ListMessage').html(data.record);
         createinnerconfigtable();
         messageEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivMessage();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListMessage").show();
        }
     document.getElementById("addmessageform").reset();
      }
    });
    return false;
  });
//delete form submit
</script>
