<style>

  .greenbg{
   background-color: #b2eeb2 !important;
  }
  .redbg{
    background-color: #f9b7b7  !important;
    
  }
  /*map styling*/
  .map-wrapper {
    height: 460px;
    width: 100% !important;
    padding-top: 15px;
    padding-bottom: 15px;
}
#map{
  height: 100%;
  border: 0.1px solid #66afe9;
}
   .gm-style-iw-c{
    background: linear-gradient(to bottom, #fafdff 0%, #e8f2f9 100%) !important;
    border-style: solid !important;
    border-color: #66afe9 !important;
    border-width: thin !important;
    color: #0d4f78 !important;
    
    }
    .gm-style-iw-c span{
      width: 300px !important;
    }
.gm-style .gm-style-iw-d::-webkit-scrollbar-track, .gm-style .gm-style-iw-d::-webkit-scrollbar-track-piece{
   background: none !important;
}
.gm-style-iw-d *{
     background: none !important;
      color: #0d4f78 !important;
}
.gm-style .gm-style-iw-t::after {
     background: linear-gradient(45deg,#e8f2f9 50%,rgba(255,255,255,0) 51%,rgba(255,255,255,0) 100%) !important;
}
.gm-ui-hover-effect img{
    display:none !important;
}

.gm-ui-hover-effect { 
    top: 1px !important;
    right: 1px !important;
    width: 15px !important;
    height: 15px !important;
    opacity: 0.6 !important;
    border-radius: 50% !important;
    border: 2px solid red !important;
}
.gm-ui-hover-effect:hover {
  opacity: 1;
}
.gm-ui-hover-effect:before, .gm-ui-hover-effect:after {
    position: absolute;
    right: 4px;
    top: 1px;
    content: ' ';
    height: 9px;
    width: 3px;
    background-color: red;
}
.gm-ui-hover-effect:before {
  transform: rotate(45deg);
}
.gm-ui-hover-effect:after {
  transform: rotate(-45deg);
}
.fw-900{
    font-weight: 900;
}
#mapmaincontentdiv{
  clear: both;
}
#mapmaincontentdiv #mapinnercontentdivleft{
  padding-right: 10px;
}
#mapmaincontentdiv #mapinnercontentdivright{
  /*float: right;*/
}
#mapmaincontentdiv #mapinnercontentdivleft img{
  border: 1px solid #75b0d7;
  cursor: pointer;
  height: 112px;
  width: 100%;
  background-color: #fff !important;
}
#mapmaincontentdiv #mapinnercontentdivright div{
 
  margin-top: 5px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
.gm-style .gm-style-iw-c{
  padding: 5px !important;
  border-radius: 2px !important;
}
.gm-style-iw-d{
  overflow: hidden !important;
}
  /*map styling*/
  .send-access-div{
    background-color: #ffffff !important;
    background: linear-gradient(#ffffff, #ffffff 6%, #d0d0d0) !important;
    color: #333 !important;
    border: 1px solid #ffffff;
    padding: 15px;
  }
  .send-access-div button{
    background-color: #ffffff !important;
  
    color: #333 !important;
    
   background: linear-gradient(to bottom, #ffffff 0%, #fafafa 39%,#c4c1c1 100%) !important;
    border: solid 1px #e6e6e6 !important;
  }
  .send-access-div button:hover{
    background-color: #ffffff !important;
    background: linear-gradient(to bottom, #c4c1c1 0%, #fafafa 39%,#ffffff 100%) !important;
    border: solid 1px #e6e6e6 !important;
    outline: 1px solid #e6e6e6 !important;
    color: #333 !important;
  }
  .send-access-div button:focus{
     outline: 1px solid #e6e6e6 !important;
  }
     .minusrediconcustomreminder{
     border: 1px solid red;
    cursor: pointer;
    overflow: hidden;
    outline: none;
    font-size: 15px;
    color: #ffffff;
    height: 22px;
    width: 20px;
    border-radius: 50%;
    background-color: red;
    text-align: center;
    margin: 0px;
    line-height: 20px;
    position: absolute;
    top: -5px;
    right: 0px;
}
.minusrediconcustomreminder > i{
    margin:0px;
}
  .plusgreeniconconfig {
    border: 1px solid green;
    cursor: pointer;
    overflow: hidden;
    outline: none;
    font-size: 16px;
    color: #ffffff;
    height: 25px;
    width: 24px;
    border-radius: 50%;
    background-color: green;
    padding: 0px;
    text-align: center;
    margin: 0px;
     position: absolute;
    top: 22px;
    right: 0px;
    z-index: 10;
}
  .zoom {
  transition: transform .2s; /* Animation */
  margin: 0 auto;
  z-index: 3;

}

.zoom:hover {
  transform: scale(4.5); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
}
 .zoommap {
  transition: transform .2s; /* Animation */
  margin: 0 auto;
  z-index: 3;

}

.zoommap:hover {
  transform: scale(1.2); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
}
  .pdlz{
    padding-left: 0px;
  }
    .pdrz{
    padding-right : 0px;
  }
  .pdhz{
    padding:0px;
  }
   
  .nav-tabs > li.active {
   background:linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%);
    border-bottom: none;
    }
    .nav-tabs>li.active>a{
         border: 1px solid #44c0e5!important;
    }
  .nav-tabs > li {
     background:linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%);
  
   border-left: 1px solid #44c0e5!important;
    height: 55px;
    margin: 0px !important;
    }

    .nav-tabs > li > a {

    background:linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%);
    color: #fff !important;
    font-size: 12px;
    padding-left: 10px;
    display: block !important;
    width: 100% !important;
    height: 100% !important;
    text-transform: uppercase;
    font-weight: bold;
    transition: 0.2s;
    outline: none;
    border: none;
    border-radius: 0px;
    line-height: 30px !important;
}
.dataTables_wrapper .dataTables_filter {
    float: left;
    text-align: left;
    margin-left: 10px;
}
input[type="search"]{
    border:1px solid #cccccc;
    padding-left: 5px;
    border-radius: 0px;
}
input[type="search"]:focus{
    border:1px solid #cccccc !important;
}
    
    .tab-pane{
        padding: 30px 30px 30px 14px;
    }
    .configTable{
        padding: 0px;
    }
    .dataTables_wrapper .dataTables_filter input {
    margin-right: 2px;
    margin-left: 0 !important;
    max-width: 100% !important;
    width: 100% !important;
    float: right;
    }
    .dataTables_wrapper{
        margin-top: 20px;
    }
   
    
  

</style>

<style>
 
.delete-icon {
    background: url(http://uniqueweb.co.in/project/ecabapp//assets/delete-icon.png) no-repeat left center !important;
    padding: 0px 0px 0px 22px;
}
.save-icon {
    background: url(http://uniqueweb.co.in/project/ecabapp//assets/save-icon.png) no-repeat left center !important;
    padding: 0px 0px 0px 22px;
}
#editnotworkingbtn,#notworkingbtn{
    background: linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%) !important;
}

.minusrediconconfig{
    border: 1px solid red;cursor: pointer;overflow: hidden;outline: none;font-size: 16px;
    color: #ffffff;
    height: 25px;
    width: 24px;
    border-radius: 50%;
    background-color: red;
    position: absolute;
    bottom: 9px;
    right: -20px;
    z-index: 10;
    text-align: center;
    margin: 0px;
}
.minusrediconconfig > i,.plusgreeniconconfig > i{
    margin:0px;
}
#editclientrestcost,#clientrestcost{
    background-color: #add8e63d !important;
}
.dataTables_wrapper .dataTables_paginate .paginate_button{
   color:#fff !important;
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current{
   color:#fff !important;
}

.dataTables_wrapper .dataTables_paginate .paginate_button:hover {
     background: linear-gradient(to bottom, #0d85a8 0%, #35add1 39%,#44c0e5 100%) !important;
   
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
     background: linear-gradient(to bottom, #0d85a8 0%, #35add1 39%,#44c0e5 100%) !important;
    color:#fff !important;
}

</style>
<style>
  .innertoolbar{
    float: right;
    margin-right: 4px;
  }

  
  .zoom {
  transition: transform .2s; /* Animation */
  margin: 0 auto;
  z-index: 3;

}

.zoom:hover {
  transform: scale(4.5); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
}
  .pdlz{
    padding-left: 0px;
  }
    .pdrz{
    padding-right : 0px;
  }
  .pdhz{
    padding:0px;
  }
  .truncate {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
    .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
  background:none;
}
 
.dataTables_wrapper .dataTables_filter {
    float: left;
    text-align: left;
    margin-left: 5px;
}
input[type="search"]{
    border:1px solid #cccccc;
    padding-left: 5px;
    border-radius: 0px;
}
input[type="search"]:focus{
    border:1px solid #cccccc !important;
}
     
    .tab-pane{
        padding: 30px 30px 30px 14px;
    }
    .configTable{
        padding: 0px;
    }
    .dataTables_wrapper .dataTables_filter input {
    margin-right: 2px;
    margin-left: 0 !important;
    max-width: 100% !important;
    width: 100% !important;
    float: right;
    }
    .dataTables_wrapper{
        margin-top: 20px;
    }
  
.dataTables_wrapper .dataTables_paginate .paginate_button{
   color:#fff !important;
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current{
   color:#fff !important;
}

.dataTables_wrapper .dataTables_paginate .paginate_button:hover {
     background: linear-gradient(to bottom, #0d85a8 0%, #35add1 39%,#44c0e5 100%) !important;
   
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
     background: linear-gradient(to bottom, #0d85a8 0%, #35add1 39%,#44c0e5 100%) !important;
    color:#fff !important;
}
#DataTables_Table_0_filter label{
    float: left;
}
#DataTables_Table_0_filter label input{
    border-style: solid !important;
    border-color: #66afe9 !important;
    border-width: thin !important;
}
#DataTables_Table_0_filter label input:focus {
   outline: none;
}
#searchform{
  display: inline-block;
  margin-left: 4px;
}

#searchform input,#searchform select{
  height: 27px !important;
  border-radius: 0px !important;
}
.partner_table_setting th{
padding-left: 4px !important;
padding-right: 4px !important;
}
.partner_table_setting td{
padding-left: 4px !important;
padding-right: 4px !important;
padding-bottom: 4px !important;
}
#partner_image_dsh{
    width: 45px !important;
    height: 45px !important;
    padding: 0px;
    margin: 0px;
}
.profileEditajax .nav-tabs{
  border: 0px !important;
}
#edit_dob_span,#dob_span{
  position: absolute;
  right: 10px;
  top: 5px;
  z-index: 20;
  color: #8e8e8e;
  background: transparent;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
</style>


<section id="content">
    <div class="listDiv"><!--col-md-10 padding white right-p-->
    <?php $this->load->view('admin/common/breadcrumbs');?>
        <?php $this->load->view('admin/common/alert');?>
        <?php echo $this->session->flashdata('message'); ?>
   <div>
<div class="row">
 
  <div class="ListProfile" >
  <input type="hidden" class="chk-Addprofile-btn" value="">
  <input type="hidden" class="Addprofilefullid" value="">
  <div class="col-md-12">
<div class="search_form_section" >
 <?php  include 'profile/search.php';?> 
</div>
  <div class="module-body table-responsive">
      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer partner_table_setting" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" style="padding-left: 4px !important;padding-right: 4px !important;">#</th>
            <th class="text-center" style="padding-left: 4px !important;padding-right: 4px !important;">id</th>
            <th class="column-date text-center" style="padding-left: 4px !important;padding-right: 4px !important;">Date</th>
            <th class="text-center" style="padding-left: 4px !important;padding-right: 4px !important;" >Time</th>
            <th class="text-center" style="padding-left: 4px !important;padding-right: 4px !important;">Photo</th> 
            <th class="text-center" style="padding-left: 4px !important;padding-right: 4px !important;" >Name</th>
            <th class="text-center" style="padding-left: 4px !important;padding-right: 4px !important;" >Category</th>           
            <th class="text-center" style="padding-left: 4px !important;padding-right: 4px !important;" >Email</th>
            <th class="text-center" style="padding-left: 4px !important;padding-right: 4px !important;" >Mobile</th>
            <th class="text-center" style="padding-left: 4px !important;padding-right: 4px !important;" >Payment Methode</th>
            <th class="text-center" style="padding-left: 4px !important;padding-right: 4px !important;" >Payment Delay</th>
            <th class="text-center" style="padding-left: 4px !important;padding-right: 4px !important;">Due Amount</th>
            <th class="text-center" style="padding-left: 4px !important;padding-right: 4px !important;" >Partner Advisor</th>
            <th class="text-center" style="padding-left: 4px !important;padding-right: 4px !important;" >Partner Support</th>
            <th class="text-center" style="padding-left: 4px !important;padding-right: 4px !important;" >Statut</th>
            <th class="text-center" style="padding-left: 4px !important;padding-right: 4px !important;">Since</th>

          </tr>
          </thead>
          <tbody>
                   
                                        <?php 
                                        if (!empty($partner_data)):  
                                        ?>
                                        <?php foreach($partner_data as $key => $item):?>
                                          <?php
                                            $civilite=$this->partners_model->getSingleRecord('vbs_partnercivilite',['id'=>$item->civilite])->civilite;
                                            if(strtolower($civilite) ==  'mr'){
                                               $defaultimg='male-default.jpg';
                                            }elseif(strtolower($civilite) ==  'mrs'){
                                              $defaultimg='female-default.jpg';
                                            }else{
                                              $defaultimg='male-default.jpg';
                                            }
                                           ?>
                                            <tr>
                                                <td class="text-center">
                                                  <input type="checkbox" class="chk-mainprofile-template" data-input="<?=$item->id?>"></td> 
                                                <td class="text-center"><a href="javascript:void()" onclick="profileidEdit('<?=$item->id?>','<?=date("dmY", strtotime($item->created_at)) .'0000'. $item->id;?>')"><?=date("dmY", strtotime($item->created_at)) .'0000'. $item->id;?></a></td>
                                                <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                                                <td class="text-center"><?=date("H:i:s", strtotime($item->created_at))?></td>
                                                <?php if($item->profile_image): ?>
                                                <td  class="text-center"><img src="<?= base_url() .'uploads/partners/'. $item->profile_image ?>" width='40px' class='img-thumbnail zoom' id="partner_image_dsh" /></td>
                                                <?php else: ?>
                                                  <td  class="text-center"><img src="<?= base_url() .'assets/images/'.$defaultimg ?>" width='40px' class='img-thumbnail zoom' id="partner_image_dsh" /></td>
                                                <?php endif; ?>
                                                <td class="text-center"><?= $civilite.' '.$item->prenom.' '.$item->nom ?></td>
                                               <td class="text-center"><?= (($item->category == '1')?'Company':'Independent'); ?></td>
                                                
                                                <td class="text-center"><?= (!empty($item->email))?$item->email:'-' ?></td>
                                                <td class="text-center"><?= (!empty($item->mobile))?$item->mobile:'-' ?></td>
                                                 
                                                <td class="text-center">-</td>
                                                <td class="text-center">-</td>
                                                <td class="text-center">-</td>
                                                <td class="text-center"><?= $this->partners_model->getuser($item->advisor); ?></td>
                                                <td class="text-center"><?= $this->partners_model->getuser($item->support); ?></td>
                                              <?php
                                                $status=$this->partners_model->getSingleRecord('vbs_partnerstatus',['id'=>$item->statut])->status; 
                                               
                                              ?>      
                                             <td class="text-center">
                                              <?php if(strtolower($status) == "pending"): ?>
                                               <span class="label label-danger">Pending</span>
                                              <?php elseif(strtolower($status) == "active"): ?>
                                                <span class="label label-success">Active</span>
                                              <?php else: ?>
                                                 <span class="label label-danger">Not Active</span>
                                              <?php endif; ?>
                                         
                                            </td>
                                                <td class="text-center"><?= timeDiff($item->created_at) ?></td>

                                            </tr>
                                           
                                        <?php endforeach; ?>
                                        <?php endif; ?>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>

<div class="Profileadd" style="display: none;" >
 <?php  include 'profile/add.php';?> 
</div>
 
<div class="profileEdit" style="display:none">
  <div class="profileEditajax">
  </div>  
</div>

 
<div class="col-md-12 profileDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
  <?= form_open("admin/partners/profileDelete") ?>
    <input  name="tablename" value="vbs_partnerprofile" type="hidden" >
    <input type="hidden" id="profiledeletid" name="delete_profile_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <button  class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>
    <a class="btn btn-default" style="cursor: pointer;" onclick="cancelProfile()"><span class="delete-icon">No</span></a>
  <?php echo form_close(); ?>
</div>
</div>
</div>
</div>
</section>
 

 <!--script -->
 <?php  include 'profile/map.php'; ?> 
 <?php  include 'profile/script.php'; ?> 
 <?php  include 'script.php'; ?> 
 <?php  include 'document/script.php'; ?>
 <!--script -->
  <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo  $googleapikey->api_key; ?>&libraries=places" async defer></script>