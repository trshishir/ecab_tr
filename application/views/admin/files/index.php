<?php $locale_info = localeconv(); ?>
<script type="text/javascript">
  function googleTranslateElementInit() {
    new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
  }
</script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js">
</script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js">
</script> 

<script type="text/javascript">
  $(document).ready(function() {
    $('#example #alert_field').each(function() {
      if($(this).html()=="On"){
        $(this).html($(this).attr("data-val"));
        
      }
    });

    htmlString = 
    '<a class="btn btn-sm btn-default editBtn" style="margin-right:5px;">File Manager</a>';
    $( ".toolbar" ).prepend( htmlString );
  });

</script> 

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<style>
  @media only screen and (min-width: 1400px){
    .table-filter input, .table-filter select{
      max-width: 9% !important;
    }
    #view{
      border: 1px solid #D1D1d1 !important;        
    }
    .table-filter {
      margin-bottom: -100px   !important;
    }
    table.dataTable thead th{

      border-bottom: none !important;
    }
    .table-filter select{
      max-width: 95px !important;
    }
    .table-filter .dpo {
      max-width: 90px !important;
    }
    #section{
      background: #eeeeee;
      margin: 0px -26px -10px !important;
    }
    table {
      display: block;
      overflow-x: auto;
      white-space: nowrap;
    }
    table tr {
      text-align: center;
    }
    table th {
      text-align: center !important;
    }
    [data-tag=Aprooved] span{
      min-width: 72px;
      color: #fff;
      text-align: center;
      display: inline-block;
      border-radius: 4px;
      line-height: 1;
      padding: 6px 10px 10px;
      font-weight: 400;
      box-shadow: 0 2px 2px rgba(0,0,0,0.1);
      background: rgb(0,117,17);
      background: linear-gradient(360deg, rgba(0,117,17,1) 0%, rgba(0,191,25,1) 100%);
      border: 1px solid #299f00;
      text-shadow: 0 -1px 0 #333333;
    }
    [data-tag=Pending] span  {
      color: #fff;
      text-align: center;
      display: inline-block;
      border-radius: 4px;
      line-height: 1;
      padding: 6px 10px 10px;
      font-weight: 400;
      box-shadow: 0 2px 2px rgba(0,0,0,0.1);
      background: orange;
      background: linear-gradient(360deg, rgba(255,82,2,1) 0%, rgba(255,141,0,1) 100%);
      border: 1px solid #ffd600;
      text-shadow: 0 -1px 0 #333333;
    }
    [data-tag=Rejected] span{
      color: #fff;
      text-align: center;
      display: inline-block;
      border-radius: 4px;
      line-height: 1;
      padding: 6px 10px 10px;
      font-weight: 400;
      box-shadow: 0 2px 2px rgba(0,0,0,0.1);
      background: rgb(196,0,0);
      background: linear-gradient(360deg, rgba(196,0,0,1) 0%, rgba(255,0,0,1) 100%);
      border: 1px solid #b90101;
      text-shadow: 0 -1px 0 #333333;
    }
    .dataTables_paginate .paginate_button.disabled{
      background: none !important;
    }
    .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
      background: none !important;
    }
    .dataTables_wrapper .dataTables_paginate .paginate_button{
      background: none !important;
    }
    .paginate_button .page-item .active .btn .btn-default{
     background: none !important;
   }
   .dataTables_wrapper .dataTables_paginate .paginate_button{
     box-shadow: none !important;
   }
   .pagination>li>a{
    background-image: linear-gradient(to bottom, #fff 0, #e0e0e0 100%) !important;
    border: 1px solid #999 !important;
    color: #333 !important;
  }
  .pagination>.active>a{
    background-image: linear-gradient(to bottom, #fff 0, #e0e0e0 100%) !important;
    border: 1px solid #999 !important;
    color: #333 !important;
    padding-left: 10px !important;
    padding-right: 10px !important;
    margin: -16px !important;
  }
  .contentDiv {
    background: #eee !important;
  }
}
</style>
<div class="col-md-12 contentDiv" ><!--col-md-10 padding white right-p-->
  <div class="content">
    <?php $this->load->view('admin/common/breadcrumbs'); ?>
    <div class="row"   id="section">
      <div class="col-md-12">
        <?php
        $flashAlert = $this->session->flashdata('alert');
        if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
          ?>
          <br>
          <div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
            <strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
            <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        <?php } ?>
        <div class="module">
          <?php echo $this->session->flashdata('message'); 
          ?>
          <div class="module-head">
          </div>
          <div class="module-body table-responsive">
            <table id="example" class="cell-border" cellspacing="0" width="100%" data-selected_id="">
              <thead>
                <tr>
                  <th class="no-sort text-center"><input type="checkbox" id="selectall"class="" ></th>
                  <th class="column-id"><?php echo $this->lang->line('id'); ?></th>
                  <th class="column-date"><?php echo $this->lang->line('date'); ?></th>
                  <th class="column-date"><?php echo $this->lang->line('time'); ?></th>
                  <th class="column-type"><?php echo $this->lang->line('type'); ?></th>
                  <th class="column-user_type" ><?php echo $this->lang->line('nature'); ?></th>
                  <th class="column-name"><?php echo $this->lang->line('filename'); ?></th>
                  <th class="column-sender_name"><?php echo $this->lang->line('file_date'); ?></th>
                  <th class="column-delay_date"><?php echo $this->lang->line('delay_date'); ?></th>
                  <th class="column-alert"><?php echo $this->lang->line('alert'); ?></th>
                  <th class="column-sender_name"><?php echo $this->lang->line('sender_department'); ?></th>
                  <th class="column-sender_name"><?php echo $this->lang->line('sender_user'); ?></th>
                  <th class="column-department"><?php echo $this->lang->line('destination'); ?></th>
                  <th class="column-user"><?php echo $this->lang->line('user'); ?></th>
                  <th class="column-priority"><?php echo $this->lang->line('priority'); ?></th>
                  <th class="column-file"><?php echo $this->lang->line('file'); ?></th>
                  <th class="column-status"><?php echo $this->lang->line('statut'); ?></th>
                  <th class="column-old"><?php echo $this->lang->line('old'); ?></th>
                </tr>
              </thead>
              <tbody>
                <?php if (isset($data) && !empty($data)): 
                $count=0;
                ?>
                <?php 
                for($i=0; $i<count($data);$i++): 
                  $count++;
                  ?>
                  <tr>
                    <td>
                      <input type="checkbox" data-id="<?= $data[$i]['id']; ?>" data-tags="" class="checkboxx" value="<?= $data[$i]['id']; ?>" >
                    </td>
                    <td>
                      <a href="<?=site_url("admin/files/".create_timestamp_uid($data[$i]['created_at'], $data[$i]['id'])."/edit")?>">
                      <?= create_timestamp_uid($data[$i]['created_at'], $data[$i]['id']); ?>
                      </a>
                    </td>
                    <td><?= from_unix_date($data[$i]['created_at']) ?></td>
                    <td><?= from_unix_time($data[$i]['created_at']) ?></td>
                    <td class="filters"><?= ucfirst($data[$i]['type']); ?></td>
                    <td><?= ucfirst($data[$i]['nature']) ?></td>
                    <td><?= ucfirst($data[$i]['name_selection']); ?></td>
                    <td><?= from_unix_date($data[$i]['date']) ?></td>
                    <td><?= from_unix_date($data[$i]['delay_date']) ?></td>
                    <td id="alert_field" data-val="<?= ucfirst($data[$i]['alert_before_day']) ?> Days"><?= ucfirst($data[$i]['alert']) ?></td>
                    <td><?= $data[$i]['sender_department'] ?></td>
                    <td><?= strtolower($data[$i]['selection']) == 'admin' ? "Mr Super " . $data[$i]['selection'] : ucfirst($data[$i]['selection']); ?></td>
                    <td><?= ucfirst($data[$i]['destination']) ?></td>
                    <td class="filters"><?= ucfirst($data[$i]['name']) ?></td>
                    <td><?= ucfirst($data[$i]['priority']) ?></td>

                    <td><button onclick="window.open('<?=base_url().'uploads/files/'.$data[$i]['file']?>','popUpWindow','height=400,width=600,left=10,top=10,,scrollbars=yes,menubar=no')" class="btn btn-default" id="view">
                    View</button></td>

                    <td data-tag="<?= ucfirst($data[$i]['status']) ?>" class="status"><span ><?= ucfirst($data[$i]['status']) ?></span></td>
                    <td style="white-space: nowrap; font-weight: bold; <?=dayDiff($data[$i]['date'],$data[$i]['delay_date'],$data[$i]['alert_before_day'],$data[$i]['alert'])?>"><?= timeDiff($data[$i]['last_action'],$data[$i]['delay_date']); ?></td>
                  </tr>
                  <?php 
                endfor;
                $count=0; ?>
              <?php endif; ?>
            </tbody>
          </table>
          <br>
        </div>
      </div>
    </div>
  </div>
  <!--/.module-->
</div>
<!--/.content-->
</div>
<div id="table-filter" class="hide">
  <select id="filter_type">
   <option  value="" data-name="type" class="form-control">
    <?= $this->lang->line('type') ?>
  </option>
  <?php for($i=0; $i<count($data);$i++) : 
    ?>
    <option  value="<?= ucfirst($data[$i]['type']); ?>" data-name="type" class="form-control">
     <?= ucfirst($data[$i]['type']); ?>
   </option>
 <?php endfor ?> 
</select>
<select id="filter_nature">
 <option  value="" data-name="nature" class="form-control">
  <?= $this->lang->line('nature') ?>
</option>
<?php for($i=0; $i<count($data);$i++) : 
  ?>
  <option  value="<?= ucfirst($data[$i]['nature']); ?>" data-name="nature" class="form-control">
   <?= ucfirst($data[$i]['nature']); ?>
 </option>
<?php endfor ?> 
</select>
<select id="filter_user">
  <option value="" data-name="name" class="form-control">
    <?= $this->lang->line('name') ?>
  </option>
  <?php for($i=0; $i<count($data);$i++) : 
    ?>
    <option value="<?= ucfirst($data[$i]['name']); ?>" data-name="type" class="form-control">
     <?= ucfirst($data[$i]['name']); ?>
   </option>
 <?php endfor ?>
</select >
<select id="filter_alert">
  <option value="" data-name="alert" class="form-control">
   <?= $this->lang->line('alert') ?>
 </option>
 <?php for($i=0; $i<count($data);$i++) : 
  ?>
  <option value="<?= ucfirst($data[$i]['alert']); ?>"data-name="type" class="form-control">
   <?= ucfirst($data[$i]['alert']); ?>
 </option>
<?php endfor ?>
</select>
<select id="filter_destination">
  <option value="" data-name="destination" class="form-control">
   <?= $this->lang->line('destination') ?>
 </option>
 <?php for($i=0; $i<count($data);$i++) : 
  ?>
  <option value="<?= ucfirst($data[$i]['destination']); ?>"data-name="type" class="form-control">
   <?= ucfirst($data[$i]['destination']); ?>
 </option>
<?php endfor ?>
</select>
<select id="filter_priority">
  <option value=""data-name="priority" class="form-control">
   <?= $this->lang->line('priority') ?>
 </option>
 <?php for($i=0; $i<count($data);$i++) : 
  ?>
  <option value="<?= ucfirst($data[$i]['priority']); ?>"data-name="type" class="form-control">
   <?= ucfirst($data[$i]['priority']); ?>
 </option>
<?php endfor ?>
</select >
<select id="filter_status">
  <option value=""data-name="status" class="form-control">
    Statut
  </option>
  <?php for($i=0; $i<count($data);$i++) : 
    ?>
    <option value="<?= ucfirst($data[$i]['status']); ?>"data-name="type" class="form-control">
     <?= ucfirst($data[$i]['status']); ?>
   </option>
 <?php endfor ?>
</select>
<input  id="filter_From_date" placeholder="From Date" class="textbox-n" type="text" onfocus="(this.type='date')" onblur="(this.type='text')" id="date" />
<input id="filter_To_date" placeholder="To Date" class="textbox-n" type="text" onfocus="(this.type='date')" onblur="(this.type='text')" id="date" />
<button id="searchbtn" class="btn btn-sm btn-default" style="margin-left: 5px;">Search</button>
</div>
