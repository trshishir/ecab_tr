<?php $locale_info = localeconv();
?>
<link href="<?php echo base_url(); ?>assets/system_design/css/bootstrap-datepicker.min.css" rel="stylesheet">
<style>
    .attach-div {
        width: 77%;
    }
    .form-control{
        color: #000 !important;
    }
    input[type="text"]{
        background: #e8e8e8 !important;
    }
    input[name="name_selection"]{
        background:#ffffff !important;
    }
    input[name="date"]{
        background:#ffffff !important;
    }
    input[name="delay_date"]{
        background:#ffffff !important;
    }
    
    input[name="note_added_by1[]"]{
        background: #e8e8e8 !important;
        margin-bottom: 15px;
    }
    
    input[type="text"][name="note_added_by[]"]:focus {

        box-shadow: none !important;
    }
    input[type="text"][name="note_added_by[]"] {
      /*  border: 0 none !important;*/
      margin-bottom: 10px;
  }
  .fa-save{
    font-size: 16px !important;
}
.fa-close{
    font-size: 16px !important;
}
#noteDIv{
    background-image: linear-gradient(to bottom, #fff -20%, #e0e0e0 140%);
    padding-top: 15px;
}
.contentDiv{
    background: #eee;
}
/*.previous_comments{
        margin-bottom: 5px;
        }*/
        #section{
            background: #eeeeee;
            margin: 0px -26px -10px !important;
        }
    /* .hide{
        display: none !important;
        }*/
    </style>
    <div class="col-md-12 contentDiv"><!--col-md-10 padding white right-p-->
        <div class="content">
            <?php $this->load->view('admin/common/breadcrumbs'); ?>
            <div class="row" id="section">
                <div class="col-md-12">
                    <?php $this->load->view('admin/common/alert'); ?>
                    <div class="module">
                        <?php echo $this->session->flashdata('message'); ?>
                        <div class="module-body">
                            <?= form_open_multipart("admin/files/store") ?>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                                    <div class="row">
                                       <div class="col-xs-12">
                                        <div class="form-group">
                                            <input name="note_added_by1[]" type="text" class="form-control" onchange="setNote1()" value="">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 hide" style="margin-top: 15px">
                                        <div class="form-group">

                                            <input type="hidden" onchange="setNote2()" class="form-control"  name="added_by_firstname" placeholder="" value="Mr <?= set_value('added_by_firstname', $user->first_name) ?>">

                                        </div>
                                    </div>

                                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 hide" style="margin-top: 15px">
                                        <div class="form-group">

                                            <input type="hidden" onchange="setNote2()" class="form-control"  name="added_by_lastname" placeholder="" value="<?= set_value('added_by_lastname', $user->last_name) ?>">

                                        </div>
                                    </div>


                                    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 ">
                                        <div class="form-group">


                                        </div>
                                    </div>

                                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2 hide" style="margin-top: 15px">
                                        <div class="form-group">

                                            <input type="hidden" class="bdatepicker-date form-control" name="send_date" placeholder="<?php echo $this->lang->line('date'); ?>" value="<?= set_value('date', $this->input->post('date')) ?>">
                                        </div>
                                    </div>

                                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2 hide" style="margin-top: 15px;max-width: 130px;min-width: 85px;padding-right: 0">
                                        <div class="form-group">

                                            <div class="input-group" style="padding-left:0px !important">
                                               <input type="hidden" onchange="setNote2()" class="form-control"  name="send_date_hour" placeholder="" value=" <?= set_value('send_date_hour',date("h:i:s"));?>">



                                           </div>
                                       </div>
                                   </div>


                               </div>

                               <div class="row">
                                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label><?= $this->lang->line('statut'); ?>*</label>
                                        <select class="form-control" name="status" required>
                                            <?php foreach (config_model::$file_status as $key => $statusName): ?>
                                                <option <?= set_value('status', $this->input->post('status')) == $statusName ? "selected" : "" ?> value="<?= $statusName ?>"><?= $statusName ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                                    <div class="form-group file_nature">
                                        <label><?= $this->lang->line('type'); ?>*</label>
                                        <select class="form-control" name="type" required>
                                            <?php foreach (config_model::$file_types as $key => $file_type): ?>
                                                <option <?= set_value('type', $this->input->post('file_type')) == $file_type ? "selected" : "" ?> value="<?= $file_type ?>"><?= $file_type ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label><?= $this->lang->line('nature'); ?>*</label>
                                        <select class="form-control nature_condition" name="nature" required>
                                            <?php foreach (config_model::$nature_types as $key => $nature): ?>
                                                <option <?= set_value('nature', $this->input->post('nature')) ==  $nature ? "selected" : "" ?> value="<?= $nature ?>"><?= $nature ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label><?= $this->lang->line('priority'); ?>*</label>
                                        <select class="form-control" name="priority" required>
                                            <?php foreach (config_model::$priority_types as $key => $priority_type): ?>
                                                <option <?= set_value('priority', $this->input->post('priority_type')) == $priority_type ? "selected" : "" ?> value="<?= $priority_type ?>"><?= $priority_type ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label><?= $this->lang->line('name_selection'); ?>*</label>
                                        <!--<input type="search" id="search_player_id" name="q" class="form-control search-input" placeholder="Search player" autocomplete="off">-->
                                        <input type="text" class="form-control" name="name_selection" placeholder="" value="<?= set_value('name_selection') ?>">
                                    </div>
                                </div>

                                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('date'); ?>*</label>
                                        <input type="text" class="bdatepicker-date form-control" name="date" onchange="setNote2()" placeholder="<?php echo $this->lang->line('date'); ?>" value="<?= set_value('date', $this->input->post('date')) ?>">
                                    </div>
                                </div>

                                <div class="col-xs-4 col-sm-2 col-md-2 col-lg-3">
                                    <div class="form-group">
                                        <label><?= $this->lang->line('name_of_sender'); ?>*</label>
                                        <div class="input-group">
                                          <input type="search" class="form-control search-input" name="q" placeholder="" value="<?= set_value('selection') ?>" autocomplete="off">
                                          <span class="input-group-addon normal-addon" style="background: white !important;"><button type="button" class="btn btn-circle btn-success btn-xs addSenderBtn"><i class="fa fa-plus"></i></button></span>
                                      </div>
                                      <div id="senderNameList" style="height: 70px; overflow: auto; overflow-x: hidden; background: white;">
                                          <ul style="list-style: none; padding: 5px" id = "senderul">
                                            <?php for($i=0; $i<count($senderNames);$i++) : ?>
                                              <li id="senderli" class="senderli<?= $i ?>">
                                                <div class="input-group">
                                                  <p id="senderNamep" style="width: 90px;"><?= $senderNames[$i] ?></p>
                                                  <span class="input-group-addon normal-addon" style="background: white !important;"><button type="button" class="btn btn-circle btn-danger btn-xs delSenderBtn" value="senderli<?= $i ?>"><i class="fa fa-minus"></i></button></span>
                                              </div>
                                          </li>
                                      <?php endfor; ?>
                                  </ul>
                              </div>
                          </div>
                      </div>
                      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                        <div class="form-group">
                            <label><?php echo $this->lang->line('delay_date'); ?>*</label>
                            <input type="text" class="bdatepicker-delay_date form-control" name="delay_date" placeholder="<?php echo $this->lang->line('delay_date'); ?>" value="<?= set_value('delay_date', $this->input->post('delay_date')) ?>">
                        </div>
                    </div>
                </div>

                <div class="row">

                    <?php $alert_on_flag = false; ?>
                    <?php foreach (config_model::$alert_types as $key => $alert_type): ?>
                        <?php if($alert_type == 'On') { $alert_on_flag = true;} ?>
                    <?php endforeach; ?>

                    <?php if($alert_on_flag): ?>
                        <div class="col-xs-4 col-sm-2 col-md-2 col-lg-3">
                            <div class="form-group">
                                <label><?= $this->lang->line('alert'); ?>*</label>
                                <select class="form-control shedule" name="alert" required>
                                    <?php foreach (config_model::$alert_types as $key => $alert_type): ?>
                                        <!--<option <?= set_value('alert', $this->input->post('alert_type')) == $alert_type ? "selected" : "" ?> value="<?= $alert_type ?>"><?= $alert_type ?></option>-->
                                        <option <?= "Off" == $alert_type ? "selected" : "" ?> value="<?= $alert_type ?>"><?= $alert_type ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="if-shedule"></div>
                    <?php endif; ?>

                    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                        <div class="form-group">
                            <label><?= $this->lang->line('destination'); ?>*</label>
                            <select  class="form-control destination" name="department" onchange="setNote2()" required>

                                <?php foreach ($department as $key => $department): ?>
                                    <option <?= set_value('dept', $this->input->post('dept')) == $department['name'] ? "selected" : "" ?> value="<?= $department['id'] ?>"><?= $department['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>




                    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                        <div class="form-group">
                            <label><?= $this->lang->line('user'); ?>*</label>
                            <select class="form-control users" name="name" required>

                                <option ></option>

                            </select>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-md-offset-5">
                        <label style="padding-top: 10px;"><?= $this->lang->line('add_files'); ?>*</label>
                        <div class="attach-div" style="" id="attachDiv">
                            <div class="attach-main">
                                <div class="attach-file">
                                    <input type="file" name="files[]" required="">
                                </div>
                                <div class="attach-buttons">
                                    <button type="button" class="btn btn-circle btn-success btn-sm addFile2"><iR class="fa fa-plus"></iR></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label><?= $this->lang->line('description'); ?>*</label>
                            <textarea rows="3" name="description" required class="form-control" style="resize: none;"><?= set_value('note', $this->input->post('note')) ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-xs-12">
                       <label><?= $this->lang->line('comment'); ?>*</label>
                       <textarea id="addComment" rows="2" class="form-control" onchange="commentby()" style="resize: none;"></textarea>
                   </div>
                   <div class="col-xs-12 text-right">
                    <button type="button" class="btn btn-circle btn-success btn-sm addNote2"><i class="fa fa-plus"></i></button>
                </div>
                                <!-- <div class="col-xs-12 text-right">
                                    <button type="button" class="btn btn-circle btn-success btn-sm addNote2"><i class="fa fa-plus"></i></button>
                                </div> -->
                                <div class="col-xs-12" id="noteDIv" style="max-height: 400px;overflow: scroll;overflow-x: hidden;margin-top: 15px;margin-bottom: 15px;">


                                       <!--  <div class="row">
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <input name="note_added_by[]" type="text" class="form-control" readonly="" value="">
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <textarea rows="2" name="note2[]" required class="form-control"  style="resize: none;"></textarea>
                                            </div>
                                            <div class="col-xs-12 text-right">
                                                <button type="button" class="btn btn-circle btn-success btn-sm addNote2"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-xs-12">
                                <div class="text-right">
                                    <a href="<?= base_url("admin/files") ?>" class="btn btn-default fa fa-close"> Cancel</a>
                                    <button class="fa fa-save btn btn-default "> Save</button>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.2.1/bloodhound.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.2.1/typeahead.jquery.min.js"></script>

<script type="text/javascript">

    function setNote2() {

        var firstname = $('[name="added_by_firstname"]').val();
        var from_lbl = "<?= $this->lang->line('from'); ?>";
        var from_lbl = "from";
        var lastname = $('[name="added_by_lastname"]').val();
        //var affected_department = $('[name="affected_department"]').val();
        var affected_department = $('[name="department"] option:selected').text();
        var date = $('[name="date"]').val();
        var date_hour = $('[name="send_date_hour"]').val();
        var loggedInUserDept = "<?= $loggedInUserDept ?>";

        var last_added_by = 'Added by : ' + firstname + ' ' + lastname + ' From Department : ' + loggedInUserDept +' At Date: ' + date + ' ' +' Time:' + date_hour ;
        $("input[name='note_added_by1[]']:last").val(last_added_by);
        var added=$('[name="note_added_by1[]"]').val();
        if($("input[name='note_added_by[]']").val()){
            $("input[name='note_added_by[]']").val(added);
        }

        $("input[name='note_added_by[]']").val(added);
        var added=$('[name="note_added_by1[]"]').val();

        $("input[name='note_added_by[]']").val(added);


    }
    function commentby(){
        var added=$('[name="note_added_by1[]"]').val();

        $("input[name='note_added_by[]']:last").val(added);
    }
    function setNote1(){
        var added=$('[name="note_added_by1[]"]').val();

        $("input[name='note_added_by[]']:last").val(added);
    }

    function showHideType() {
        var nature_val = $('.nature_condition').val();
        if (nature_val == "Letter") {
            $(".file_nature").show();
        } else {
            $(".file_nature").hide();
        }
    }

    $(document).ready(function () {
        showHideType();
        $("#noteDIv").hide();
        $(".bdatepicker-date").datepicker({
            format: "dd/mm/yyyy"
        }).datepicker("setDate", new Date());
        $(".bdatepicker-delay_date").datepicker({
            format: "dd/mm/yyyy"
        }).datepicker("setDate", new Date("<?= set_value('delay_date', $this->input->post('delay_date')) ?>"));
        $(".bdatepicker").datepicker({
            format: "dd/mm/yyyy",
        }).datepicker("setDate", new Date());
        $(document.body).on("click", ".addFile2", function () {
            $("#attachDiv").append('<div class="attach-main"> <div class="attach-file"> <input type="file" name="files[]"> </div> <div class="attach-buttons"> <button type="button" class="btn btn-circle btn-success btn-sm addFile2"><i class="fa fa-plus"></i></button> <button type="button" class="btn btn-circle btn-danger btn-sm delFile2"><i class="fa fa-minus"></i></button></div></div>');
        });

        $(document.body).on("click", ".delFile2", function () {
            $(this).closest('.attach-main').remove();
        });

        $(document.body).on("click", ".addNote2", function () {
            var value = $('#addComment').val();
            var note2 = '<div class="row">\n\
            <div class="col-xs-12">\n\
            <div class="form-group">\n\
            <input name="note_added_by[]" type="text" class="form-control" value="">\n\
            </div>\n\
            </div>\n\
            <div class="col-xs-12">\n\
            <textarea rows="2" name="note2[]" required class="form-control" readonly="" style="resize: none;">'+value+'</textarea>\n\
            </div>\n\
            <div class="col-xs-12 text-right">\n\
            \n\
            <button type="button" class="btn btn-circle btn-danger btn-sm delNote2"><i class="fa fa-minus"></i></button>\n\
            </div>\n\
            </div>';

            $("#noteDIv").append(note2);
            $("#noteDIv").show();
            $('#addComment').val('');
            setNote2();
        });

        $(document.body).on("click", ".delNote2", function () {
            $(this).closest('.row').remove();
        });
        $(document).on("change", ".nature_condition", function () {
            showHideType();
        });  



        $("select.destination").change(function(){
            var selected = $(this).children("option:selected").val();

            $.ajax({
                url: "<?php echo base_url(); ?>admin/filemanagement/get_user_by_destination",
                method: "GET",
                data: {selected: selected},
                error: function(xhr, status, error) {
                    // alert(xhr.responseText);
                    alert("Error: " + error);
                },
                success: function(data) {

                    $("select.users").empty();

                    var jsonData = JSON.parse(data);

                    for(var i in jsonData){
                        $("select.users").append("<option>" + jsonData[i] + "</option>");
                    }
                }
            });
        });

        $(document).ready(function() {
            var option=select.destination.value();
            var selected = option.children("option:selected").val();

            $.ajax({
                url: "<?php echo base_url(); ?>Filemanagement/get_user_by_destination",
                method: "GET",
                data: {selected: selected},
                error: function(xhr, status, error) {
                    // alert(xhr.responseText);
                    alert("Error: " + error);
                },
                success: function(data) {

                    $("select.users").empty();

                    var jsonData = JSON.parse(data);

                    for(var i in jsonData){
                        $("select.users").append("<option>" + jsonData[i] + "</option>");
                    }
                }
            });
        });

        $("#senderNameList").hide();
        $(document.body).on("click", ".search-input", function () {
            if ( $('#senderul li').length > 0 ) {
                $("#senderNameList").show();
            }
        });

        $(document.body).on("keyup", ".search-input", function () {
            var searchText = $(this).val();
            $('#senderul > li').each(function(){
                var currentLiText = $(this).text(),
                showCurrentLi = currentLiText.indexOf(searchText) !== -1;

                $(this).toggle(showCurrentLi);

            });  
        });

        $(document.body).on("click","#senderNamep", function(){
            $(".search-input").val($(this).text());
            $("#senderNameList").hide();
        });

        var count = "<?= count($senderNames); ?>";

        $(document.body).on("click",".addSenderBtn", function () {
            var text = $(".search-input").val();

            $.ajax({
                url: "<?php echo base_url(); ?>admin/filemanagement/add_sender",
                method: "GET",
                data: {data: text},
                error: function(xhr, status, error) {
                    // alert(xhr.responseText);
                    alert("Error: " + error);
                },
                success: function (msg) {
                    // alert("User added");
                }
            });

            if(text != '') {
                var list = '<li id="senderli" class="senderli'+count+'"><div class="input-group"><p id="senderNamep" style="width: 90px;">'+text+'</p><span class="input-group-addon normal-addon" style="background: white !important;"><button type="button" class="btn btn-circle btn-danger btn-xs delSenderBtn" value="senderli'+count+'"><i class="fa fa-minus"></i></button></span></div></li>';
                count++;
                $("#senderul").append(list);
                $("#senderNameList").show();
            } else {
                alert("Please enter value");
            }
        });

        $(document.body).on("click",".delSenderBtn", function () {

            var text = $("." + $(this).val()).text().trim();

            $.ajax({
                url: "<?php echo base_url(); ?>admin/filemanagement/delete_sender",
                method: "GET",
                data: {data: text},
                error: function(xhr, status, error) {
                    // alert(xhr.responseText);
                    alert("Error: " + error);
                },
                success: function (msg) {
                    // alert("User deleted");
                }
            });

            $("." + $(this).val()).remove();
            $("#senderNameList").show();
            if ( $('#senderul li').length == 0 ) {
                $("#senderNameList").hide();
            }

        });

        $(document).on("change", ".shedule", function () {
            var shedule_val = $(this).val();
            $('.if-shedule').empty();
            if (shedule_val == "On") {
                var if_schedule = '<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">' +
                '<div class="form-group">' +
                '<label>Number of days before</label>' +
                // '<div class="input-group">' +
                '<select class="form-control" name="alert_before_day" required><?php for ($i = 0; $i < 30; $i++): $time = $i > 9 ? $i : "0" . $i; ?> <option value="<?= $time ?>"><?= $time ?></option> <?php endfor; ?> </select> ' +
                // '<span class="input-group-addon normal-addon"><button type="button" class="btn btn-circle btn-success btn-sm"><i class="fa fa-plus"></i></button></span>' +
                // '</div>' +
                '</div>' +
                '</div>';
                $('.if-shedule').append(if_schedule);
            }
        });

        setNote2();

        // var find_url = "<?php echo site_url(); ?>admin/files/search_user";

        // // Set the Options for "Bloodhound" suggestion engine
        // // Referance URLs
        // // typeahead : https://scotch.io/tutorials/implementing-smart-search-with-laravel-and-typeahead-js
        // // https://github.com/twitter/typeahead.js/blob/master/doc/jquery_typeahead.md#options

        // var engine = new Bloodhound({
        //     remote: {
        //         url: find_url + '?q=%QUERY%',
        //         wildcard: '%QUERY%',
        //         replace: function (url, uriEncodedQuery) {
        //             var search_query = jQuery("input.search-input[name=q]").val();
        //             return url + '&query=' + encodeURIComponent(search_query)
        //         },
        //     },
        //     datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
        //     queryTokenizer: Bloodhound.tokenizers.whitespace
        // });

        // $(".search-input").typeahead({
        //     hint: true,
        //     highlight: true,
        //     minLength: 1
        // }, {
        //     source: engine.ttAdapter(),

        //     // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
        //     name: 'usersList',

        //     // the key from the array we want to display (name,id,email,etc...)
        //     templates: {
        //         empty: [
        //         '<div class="list-group search-results-dropdown"><div class="list-group-item">Nothing found.</div></div>'
        //         ],
        //         header: [
        //         '<div class="list-group search-results-dropdown">'
        //         ],
        //         suggestion: function (data) {
        //             //                var all_clusters = jQuery(".all_clusters option:selected").val();
        //             //                var all_hospitals = jQuery(".all_hospitals option:selected").val();
        //             //                var all_departments = jQuery(".all_departments option:selected").val();

        //             // return '<div class="input-group">' + 
        //             //     '<a href="javascript:void(0);" class="list-group-item senderNameAnchor">' + data + '</a>' +
        //             //     '<span class="input-group-addon normal-addon"><button type="button" class="btn btn-circle btn-danger btn-xs dltNameBtn"><i class="fa fa-minus"></i></button></span> ' +
        //             // '</div>';

        //             return '<a href="javascript:void(0);" class="list-group-item">' + data + '</a>'
        //         }
        //     }
        // });

        // $(document).on("click", ".deleteSenderNameBtn", function () {
        //     var anchor = $('.search-input').val();
        //     alert(anchor);
        // });
    });

</script>
