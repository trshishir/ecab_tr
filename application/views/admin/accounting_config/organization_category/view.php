<div class="tab-content responsive" style="width:100%;display: table;">
<?=form_open()?>
  <div class="driverStatusAdd organization_category_view" style="display: none;" >
    <div class="col-md-12">  
      <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;">Statut</span>
          <select class="form-control custom_disabled" name="statut" id="statut_organiztion_category_view" disabled>
            <option value="enabled">Enabled</option>
            <option value="disabled">Disabled</option>
          </select>
        </div>
      </div>  
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Organization Category</span>
            <input type="text" class="form-control custom_disabled" name="organiztion_category" id="organization_category_view" disabled>
          </div>
      </div>
    </div>
    <div class="col-md-12">  
        <button type="button" class="btn btn-default back_to_listing" style="float:right; margin-left:7px;"><span class="fa fa-close"> Cancel </span></button>
    </div>      
  </div>
<?php echo form_close(); ?>
</div>

