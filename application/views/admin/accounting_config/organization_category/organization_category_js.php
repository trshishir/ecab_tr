<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click','.add_organization_category',function(){
            $('.organization_category_index').css('display','none');
            $('.organization_category_edit').css('display','none');
            $('.organization_category_view').css('display','none');
            $('.organization_category_add').css('display','block');
        });
        $('body').on('click','.back_to_listing',function(){
            $('.organization_category_add').css('display','none');
            $('.organization_category_edit').css('display','none');
            $('.organization_category_view').css('display','none');
            $('.organization_category_index').css('display','block');
        });
    })
    $(document.body).on("click", ".edit_organization_category", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr')
                statut = row.find('input.organization_category_statut').val()
                organization_category = row.find('input.organization_category').val();
            $('#id_organization_category').val(checkBoxVal);
            $('#organization_category_statut').val(statut);
            $('#organization_category_update').val(organization_category);
            $('.organization_category_index').css('display','none');
            $('.organization_category_add').css('display','none');
            $('.organization_category_view').css('display','none');
            $('.organization_category_edit').css('display','block');
        } else {
            alert("Select atleast one record");
        }
    });
    function view_organization_category(id){
        var row = $('td.row_'+id).closest('tr')
            statut = row.find('input.organization_category_statut').val()
            organization_category = row.find('input.organization_category').val();
        $('#organization_category_statut_view').val(statut);
        $('#organization_category_view').val(organization_category);
        $('.organization_category_index').css('display','none');
        $('.organization_category_add').css('display','none');
        $('.organization_category_edit').css('display','none');
        $('.organization_category_view').css('display','block');
    }
    $(document.body).on("click", ".delete_organization_category", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr');
            var id = checkBoxVal;
            if(confirm('Are You sure to delete ?')){
                row.remove();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('admin/accounting_config/delete_config_record'); ?>",
                    data: {
                    'id': id,
                    'table':'vbs_account_organization_category',
                    },
                    dataType:'json',
                    success:function(res){
                        alert(res);
                        row.remove();
                    }
                }) 
            }
        } else {
            alert("Select atleast one record");
        }
    });
</script>