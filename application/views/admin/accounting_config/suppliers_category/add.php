<div class="tab-content responsive" style="width:100%;display: table;">
<?=form_open("admin/accounting_config/accounting_supplier_category_add")?>
  <div class="driverStatusAdd supplier_category_add" style="display: none;" >
    <div class="col-md-12">  
      <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;">Statut</span>
          <select class="form-control" name="statut" required>
            <option value="enabled">Enabled</option>
            <option value="disabled">Disabled</option>
          </select>
        </div>
      </div>   
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Supplier Category</span>
            <input type="text" class="form-control" name="supplier_category" required>
          </div>
      </div>
    </div>
    <div class="col-md-12">  
        <button type="submit" class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
        <button type="button" class="btn btn-default back_to_listing" style="float:right; margin-left:7px;"><span class="fa fa-close"> Cancel </span></button>
    </div>      
  </div>
<?php echo form_close(); ?>
</div>

