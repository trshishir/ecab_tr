<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click','.add_supplier_category',function(){
            $('.supplier_category_index').css('display','none');
            $('.supplier_category_edit').css('display','none');
            $('.supplier_category_view').css('display','none');
            $('.supplier_category_add').css('display','block');
        });
        $('body').on('click','.back_to_listing',function(){
            $('.supplier_category_add').css('display','none');
            $('.supplier_category_edit').css('display','none');
            $('.supplier_category_view').css('display','none');
            $('.supplier_category_index').css('display','block');
        });
    })
    $(document.body).on("click", ".edit_supplier_category", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr')
                statut = row.find('input.supplier_category_statut').val()
                supplier_category = row.find('input.supplier_category').val();
            $('#id_supplier_category').val(checkBoxVal);
            $('#supplier_category_statut').val(statut);
            $('#supplier_category_update').val(supplier_category);
            $('.supplier_category_index').css('display','none');
            $('.supplier_category_add').css('display','none');
            $('.supplier_category_view').css('display','none');
            $('.supplier_category_edit').css('display','block');
        } else {
            alert("Select atleast one record");
        }
    });
    function view_supplier_category(id){
        var row = $('td.row_'+id).closest('tr')
            statut = row.find('input.supplier_category_statut').val()
            supplier_category = row.find('input.supplier_category').val();
        $('#supplier_category_statut_view').val(statut);
        $('#supplier_category_update_view').val(supplier_category);
        $('.supplier_category_index').css('display','none');
        $('.supplier_category_add').css('display','none');
        $('.supplier_category_edit').css('display','none');
        $('.supplier_category_view').css('display','block');
    }
    $(document.body).on("click", ".delete_suppliers_category", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr');
            var id = checkBoxVal;
            if(confirm('Are You sure to delete ?')){
                row.remove();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('admin/accounting_config/delete_config_record'); ?>",
                    data: {
                    'id': id,
                    'table':'vbs_account_suppliers_category',
                    },
                    dataType:'json',
                    success:function(res){
                        alert(res);
                        row.remove();
                    }
                }) 
            }
        } else {
            alert("Select atleast one record");
        }
    });
</script>