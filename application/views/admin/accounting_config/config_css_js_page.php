<style type="text/css">
    /* Tab Navigation */
    
    .nav-tabs>li.active>a{
        border: none !important;
        background: linear-gradient(to bottom, #F7F7F7 0%, #D5D5D5 39%, #F7F7F7 100%) !important;
    }
    .nav-tabs > li {
        background:linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%);
        /*border-left: 1px solid #44c0e5!important;*/
        height: 55px;
        margin: 0px !important;
    }
    .nav-tabs > li > a {
        /*background:-webkit-gradient(linear, left top, left bottom, from(#f6f6f6), to(#f1f1f1));*/
        background:linear-gradient(to bottom, #F7F7F7 0%, #F8F8F8 39%, #D5D5D5 100%) !important;
        color: #007FBD !important;
        font-size: 10px;
        padding-left: 10px;
        display: block !important;
        width: 100% !important;
        height: 100% !important;
        text-transform: uppercase;
        font-weight: bold;
        transition: 0.2s;
        outline: none;
        border: 1px solid #D1D1D1;
        border-radius: 0px;
        line-height: 30px !important;
        border-bottom: 0px;
    }
    .tab-pane{
        border-color:#ccc;  
        border-style: solid;
        border-width: thin;
    }
    .table-filter{
        display: initial;
    }
    .delete-icon {
    background: url(http://uniqueweb.co.in/project/ecabapp//assets/delete-icon.png) no-repeat left center !important;
    padding: 0px 0px 0px 22px;
}
.save-icon {
    background: url(http://uniqueweb.co.in/project/ecabapp//assets/save-icon.png) no-repeat left center !important;
    padding: 0px 0px 0px 22px;
}
#editnotworkingbtn,#notworkingbtn{
    background: linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%) !important;
}
.plusgreeniconconfig{
    border: 1px solid green;cursor: pointer;overflow: hidden;outline: none;font-size: 16px;
    color: #ffffff;
    height: 25px;
    width: 24px;
    border-radius: 50%;
    background-color: green;
    position: absolute;
    top: 22px;
    right: -20px;
    z-index: 10;
    text-align: center;
    margin: 0px;
}
.minusrediconconfig{
    border: 1px solid red;cursor: pointer;overflow: hidden;outline: none;font-size: 16px;
    color: #ffffff;
    height: 25px;
    width: 24px;
    border-radius: 50%;
    background-color: red;
    position: absolute;
    bottom: 9px;
    right: -20px;
    z-index: 10;
    text-align: center;
    margin: 0px;
}
.minusrediconconfig > i,.plusgreeniconconfig > i{
    margin:0px;
}
#editdriverrestcost,#driverrestcost{
    background-color: #add8e63d !important;
}
.dataTables_wrapper .dataTables_filter {
    float: left;
    text-align: left;
    margin-left: 10px;
}
input[type="search"]{
    border:1px solid #cccccc;
    padding-left: 5px;
    border-radius: 0px;
}
input[type="search"]:focus{
    border:1px solid #cccccc !important;
}   
.configTable{
    padding: 0px;
}
.dataTables_wrapper .dataTables_filter input {
margin-right: 2px;
margin-left: 0 !important;
max-width: 100% !important;
width: 100% !important;
float: right;
}
.dataTables_wrapper{
    margin-top: 20px;
}
.listing th{
    text-align: center;
}
.listing td{
    text-align: center;
}
.display-none{
    display: none;
}
.custom_disabled{
        background-color: #F4F4F5 !important; 
    }

.toolbar_payemnt_methodes,.toolbar_payemnt_delays,.toolbar_bill_category,.toolbar_client_category,.toolbar_sales_category,.toolbar_supplier,.toolbar_organization,.toolbar_bank,.toolbar_factor,.toolbar_supplier_category,.toolbar_organization_category,.toolbar_vat,.toolbar_client_kind,.toolbar_garantee_fund,.toolbar_financement_delay{
    float: right;
    margin-right: 4px;
}
</style>
