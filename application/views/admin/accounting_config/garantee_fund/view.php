<div class="tab-content responsive" style="width:100%;display: table;">
<?=form_open("admin/accounting_config/accounting_statuts_update")?>
  <div class="driverStatusAdd garantee_fund_view" style="display: none;" >
    <div class="col-md-12">  
      <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;">Status</span>
          <select class="form-control custom_disabled" name="statut" id="garantee_fund_statut_acc_config_view" disabled>
            <option value="enabled">Enabled</option>
            <option value="disabled">Disabled</option>
          </select>
        </div>
      </div>   
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
          <span style="font-weight: bold;">Funds Garantee</span>
          <input type="text" class="form-control custom_disabled" id="garantee_fund_acc_config_view" disabled style="width: 95%;display: inline;"><span style="float: right;margin-top: 7px;">%</span>
          </div>
      </div>
    </div>
    <div class="col-md-12">  
        <button type="button" class="btn btn-default back_to_listing" style="float:right; margin-left:7px;"><span class="fa fa-close"> Cancel </span></button>
    </div>      
  </div>
<?php echo form_close(); ?>
</div>

