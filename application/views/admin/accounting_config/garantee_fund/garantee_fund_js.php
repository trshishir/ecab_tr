<script type="text/javascript">
	$(document).ready(function(){
        $('body').on('click','.add_config_garantee_fund',function(){
            $('.garantee_fund_index').css('display','none');
            $('.garantee_fund_edit').css('display','none');
            $('.garantee_fund_view').css('display','none');
            $('.garantee_fund_add').css('display','block');
        });
        $('body').on('click','.back_to_listing',function(){
            $('.garantee_fund_add').css('display','none');
            $('.garantee_fund_edit').css('display','none');
            $('.garantee_fund_view').css('display','none');
            $('.garantee_fund_index').css('display','block');
        });
    })
	$(document.body).on("click", ".edit_config_garantee_fund", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
        	var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr')
            	statut = row.find('input.garantee_fund_statut_acc_config').val()
            	payment_method = row.find('input.garantee_fund_acc_config').val();
            $('#garantee_fund_id_acc_config').val(checkBoxVal);
            $('#garantee_fund_statut_acc_config').val(statut);
            $('#garantee_fund_acc_config').val(payment_method);
        	$('.garantee_fund_index').css('display','none');
            $('.garantee_fund_add').css('display','none');
            $('.garantee_fund_view').css('display','none');
            $('.garantee_fund_edit').css('display','block');
        } else {
            alert("Select atleast one record");
        }
    });
	function view_garantee_fund(id){
		var row = $('td.row_'+id).closest('tr')
            statut = row.find('input.garantee_fund_statut_acc_config').val()
            garantee_fund = row.find('input.garantee_fund_acc_config').val();
        $('#garantee_fund_statut_acc_config_view').val(statut);
        $('#garantee_fund_acc_config_view').val(garantee_fund)
        $('.garantee_fund_index').css('display','none');
        $('.garantee_fund_add').css('display','none');
        $('.garantee_fund_edit').css('display','none');
        $('.garantee_fund_view').css('display','block');
	}
    $(document.body).on("click", ".delete_funds_garantee", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr');
            var id = checkBoxVal;
            if(confirm('Are You sure to delete ?')){
                row.remove();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('admin/accounting_config/delete_config_record'); ?>",
                    data: {
                    'id': id,
                    'table':'vbs_account_garantee_fund',
                    },
                    dataType:'json',
                    success:function(res){
                        alert(res);
                        row.remove();
                    }
                }) 
            }
        } else {
            alert("Select atleast one record");
        }
    });
</script>