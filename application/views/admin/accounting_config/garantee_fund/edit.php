<div class="tab-content responsive" style="width:100%;display: table;">
<?=form_open("admin/accounting_config/accounting_garantee_fund_update")?>
  <div class="driverStatusAdd garantee_fund_edit" style="display: none;" >
    <input type="hidden" name="id" id="garantee_fund_id_acc_config">
    <div class="col-md-12">  
      <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;">Statut</span>
          <select class="form-control" name="statut" id="garantee_fund_statut_acc_config" required>
            <option value="enabled">Enabled</option>
            <option value="disabled">Disabled</option>
          </select>
        </div>
      </div>   
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Funds Garantee</span>
            <input type="text" class="form-control" name="garantee_fund" id="garantee_fund_acc_config" required style="width: 95%;display: inline;"><span style="float: right;margin-top: 7px;">%</span>
          </div>
      </div>
    </div>
    <div class="col-md-12">  
        <button type="submit" class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
        <button type="button" class="btn btn-default back_to_listing" style="float:right; margin-left:7px;"><span class="fa fa-close"> Cancel </span></button>
    </div>      
  </div>
<?php echo form_close(); ?>
</div>

