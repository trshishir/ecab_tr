<div class="tab-content responsive" style="width:100%;display: table;">
<?=form_open("admin/accounting_config/accounting_organization_update")?>
  <div class="driverStatusAdd organization_edit" style="display: none;" >
    <input type="hidden" name="id" id="id_organization">
    <div class="col-md-12"> 
      <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;">Statut</span>
          <select class="form-control" name="statut" id="statut_organization">
            <option value="enabled">Enabled</option>
            <option value="disabled">Disabled</option>
          </select>
        </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Organization Category</span>
            <select class="form-control" name="org_category_id" id="org_category_id">
              <?php foreach($organization_category as $key => $value){ ?>
                <option value="<?= $value['id']; ?>"><?= $value['organization_category']; ?></option>
              <?php } ?>  
            </select>
          </div>
      </div> 
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Organization Name</span>
            <input type="text" class="form-control" name="org_name" id="org_name" required>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Phone</span>
            <input type="text" class="form-control" name="org_phone" id="org_phone">
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">FAX</span>
            <input type="text" class="form-control" name="org_fax" id="org_fax">
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Email</span>
            <input type="text" class="form-control" name="org_email" id="org_email">
          </div>
      </div>
      
    </div>
    <div class="col-md-12">
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Contact Name</span>
            <input type="text" class="form-control" name="org_contact_name" id="org_contact_name">
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Reference Number</span>
            <input type="text" class="form-control" name="org_reference_number" id="org_reference_number">
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Payment Delay</span>
            <select class="form-control" name="payment_delay_id" id="payment_delay_id">
              <?php foreach($payment_delay as $key => $value){ ?>
                <option value="<?=$value['id']; ?>"><?=$value['payment_delay']; ?></option>
              <?php } ?>
            </select>
          </div>
      </div>
      <div class="col-md-6" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Address</span>
            <textarea rows="1" class="form-control" name="org_address" id="org_address"></textarea>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="col-md-4" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Address</span>
            <textarea rows="1" class="form-control" name="org_address2" id="org_address2" placeholder="Address line 2"></textarea>
        </div>
      </div>
     <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Zip Code</span>
            <input type="text" class="form-control" name="org_zip_code" id="org_zip_code">
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">City</span>
            <input type="text" class="form-control" name="org_city" id="org_city">
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Country</span>
            <input type="text" class="form-control" name="org_country" id="org_country">
          </div>
      </div>
    </div>
    <div class="col-md-12">  
        <button type="submit" class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
        <button type="button" class="btn btn-default back_to_listing" style="float:right; margin-left:7px;"><span class="fa fa-close"> Cancel </span></button>
    </div>      
  </div>
<?php echo form_close(); ?>
</div>

