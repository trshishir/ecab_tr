  <div class="module-body table-responsive organization_index">
    <table id="organization_grid" class="cell-border listing" cellspacing="0" width="100%" data-selected_id="">
      <thead>
        <tr>
          <th class="no-sort text-center">#</th>
          <th class="column-id"><?php echo $this->lang->line('id');?></th>
          <th class="column-date">Date</th>
          <th class="column-date">Time</th>
          <th class="column-date">Added by</th>
          <th>From Deparment</th>
          <th>Organization Category</th>
          <th>Name</th>
          <th>Phone</th>
          <th>FAX</th>
          <th>Email</th>
          <th>Contact Name</th>
          <th>Statut</th>
          <th>Since</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($organization as $key => $value){ ?>
          <?php
              $timestamp=$value['created_at'];
              $date = new DateTime($timestamp);
              $date = $date->format('d/m/Y');
              $time = new DateTime($timestamp);
              $time = $time->format('H:i:s');
          ?>
        <tr>
          <input type="hidden" class="organization_statut" value="<?php echo $value['statut'];?>">
          <input type="hidden" class="org_name" value="<?php echo $value['org_name'];?>">
          <input type="hidden" class="org_phone" value="<?php echo $value['org_phone'];?>">
          <input type="hidden" class="org_fax" value="<?php echo $value['org_fax'];?>">
          <input type="hidden" class="org_email" value="<?php echo $value['org_email'];?>">
          <input type="hidden" class="org_contact_name" value="<?php echo $value['org_contact_name'];?>">
          <input type="hidden" class="org_reference_number" value="<?php echo $value['org_reference_number'];?>">
          <input type="hidden" class="payment_delay_id" value="<?php echo $value['payment_delay_id'];?>">
          <input type="hidden" class="org_address" value="<?php echo $value['org_address'];?>">
          <input type="hidden" class="org_address2" value="<?php echo $value['org_address2'];?>">
          <input type="hidden" class="org_zip_code" value="<?php echo $value['org_zip_code'];?>">
          <input type="hidden" class="org_city" value="<?php echo $value['org_city'];?>">
          <input type="hidden" class="org_country" value="<?php echo $value['org_country'];?>">
          <input type="hidden" class="org_category_id" value="<?php echo $value['org_category_id'];?>">
          <td class="row_<?=$value['id'];?>"><input type="checkbox" value="<?=$value['id'];?>" data-id="<?=$value['id'];?>" class="checkbox checkboxx singleSelect"></td>
            <td>
                <a class="view_statuts" onclick="view_organization('<?= $value['id']; ?>')">
                    <?=create_timestamp_uid($value['created_at'],$value['id']);?>
                </a>
            </td>
            <td><?=$date ?></td>
            <td><?= $time ?></td>
            <td><?= $value['civility'].' '.$value['first_name'].' '.$value['last_name']; ?></td>
            <td><?= ucwords($value['department_name']); ?></td>
            <td><?= ucwords($value['organization_category']); ?></td>
            <td><?= ucwords($value['org_name']); ?></td>
            <td><?= ucwords($value['org_phone']); ?></td>
            <td><?= ucwords($value['org_fax']); ?></td>
            <td><?= ucwords($value['org_email']); ?></td>
            <td><?= ucwords($value['org_contact_name']); ?></td>
            <td><?=$value['statut']=='enabled'?'<span class="label label-success">Enabled</span>':'<span class="label label-danger">Disabled</span>';?></td>
            <td style="white-space: nowrap;"><?=timeDiff($value['created_at']);?></td>
          </tr>
      <?php } ?>  
      </tbody>
    </table>
  </div>
<div id="table-filter" class="hide">
  <input type="text" placeholder="Search" data-name="short_code" class="form-control">
</div>

<?php $this->load->view('admin/accounting_config/organization/organization_js');?>