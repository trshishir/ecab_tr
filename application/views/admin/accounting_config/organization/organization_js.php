<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click','.add_organization',function(){
            $('.organization_index').css('display','none');
            $('.organization_edit').css('display','none');
            $('.organization_view').css('display','none');
            $('.organization_add').css('display','block');
        });
        $('body').on('click','.back_to_listing',function(){
            $('.organization_add').css('display','none');
            $('.organization_edit').css('display','none');
            $('.organization_view').css('display','none');
            $('.organization_index').css('display','block');
        });
    })
    $(document.body).on("click", ".edit_organization", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr')
                statut = row.find('input.organization_statut').val()
                org_name = row.find('input.org_name').val();
                org_phone = row.find('input.org_phone').val();
                org_fax = row.find('input.org_fax').val();
                org_email = row.find('input.org_email').val();
                org_address = row.find('input.org_address').val();
                org_address2 = row.find('input.org_address2').val();
                org_contact_name = row.find('input.org_contact_name').val();
                payment_delay_id = row.find('input.payment_delay_id').val();
                org_zip_code = row.find('input.org_zip_code').val();
                org_city = row.find('input.org_city').val();
                org_country = row.find('input.org_country').val();
                org_reference_number = row.find('input.org_reference_number').val();
                org_category_id = row.find('input.org_category_id').val();
            $('#id_organization').val(checkBoxVal);
            $('#organization_statut').val(statut);
            $('#org_name').val(org_name);
            $('#org_phone').val(org_phone);
            $('#org_fax').val(org_fax);
            $('#org_email').val(org_email);
            $('#org_address').val(org_address);
            $('#org_address2').val(org_address2);
            $('#org_contact_name').val(org_contact_name);
            $('#org_reference_number').val(org_reference_number);
            $('#payment_delay_id').val(payment_delay_id);
            $('#org_zip_code').val(org_zip_code);
            $('#org_city').val(org_city);
            $('#org_country').val(org_country);
            $('#org_category_id').val(org_category_id);
            $('.organization_index').css('display','none');
            $('.organization_add').css('display','none');
            $('.organization_view').css('display','none');
            $('.organization_edit').css('display','block');
        } else {
            alert("Select atleast one record");
        }
    });
    function view_organization(id){
        var row = $('td.row_'+id).closest('tr')
            statut = row.find('input.organization_statut').val()
            org_name = row.find('input.org_name').val();
            org_phone = row.find('input.org_phone').val();
            org_fax = row.find('input.org_fax').val();
            org_email = row.find('input.org_email').val();
            org_address = row.find('input.org_address').val();
            org_address2 = row.find('input.org_address2').val();
            org_contact_name = row.find('input.org_contact_name').val();
            payment_delay_id = row.find('input.payment_delay_id').val();
            org_zip_code = row.find('input.org_zip_code').val();
            org_city = row.find('input.org_city').val();
            org_country = row.find('input.org_country').val();
            org_reference_number = row.find('input.org_reference_number').val();
            org_category_id = row.find('input.org_category_id').val();
            $('#organization_statut_view').val(statut);
            $('#org_name_view').val(org_name);
            $('#org_phone_view').val(org_phone);
            $('#org_fax_view').val(org_fax);
            $('#org_email_view').val(org_email);
            $('#org_address_view').val(org_address);
            $('#org_address_view2').val(org_address2);
            $('#org_contact_name_view').val(org_contact_name);
            $('#org_reference_number_view').val(org_reference_number);
            $('#payment_delay_id_view').val(payment_delay_id);
            $('#org_zip_code_view').val(org_zip_code);
            $('#org_city_view').val(org_city);
            $('#org_country_view').val(org_country);
            $('#org_category_id_view').val(org_category_id);
        $('.organization_index').css('display','none');
        $('.organization_add').css('display','none');
        $('.organization_edit').css('display','none');
        $('.organization_view').css('display','block');
    }
    $(document.body).on("click", ".delete_organization", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr');
            var id = checkBoxVal;
            if(confirm('Are You sure to delete ?')){
                row.remove();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('admin/accounting_config/delete_config_record'); ?>",
                    data: {
                    'id': id,
                    'table':'vbs_account_organization',
                    },
                    dataType:'json',
                    success:function(res){
                        alert(res);
                        row.remove();
                    }
                }) 
            }
        } else {
            alert("Select atleast one record");
        }
    });
</script>