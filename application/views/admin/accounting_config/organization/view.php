<div class="tab-content responsive" style="width:100%;display: table;">
<?=form_open()?>
  <div class="driverStatusAdd organization_view" style="display: none;" >
    <div class="col-md-12">
      <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;">Statut</span>
          <select class="form-control custom_disabled custom_disabled" name="statut" id="statut_organization_view" disabled>
            <option value="enabled">Enabled</option>
            <option value="disabled">Disabled</option>
          </select>
        </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Organization Category</span>
            <select class="form-control custom_disabled" name="org_category_id " id="org_category_id_view" disabled>
              <?php foreach($organization_category as $key => $value){ ?>
                <option value="<?= $value['id']; ?>"><?= $value['organization_category']; ?></option>
              <?php } ?>  
            </select>
          </div>
      </div>  
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Organization Name</span>
            <input type="text" class="form-control custom_disabled" name="org_name" id="org_name_view" disabled>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Phone</span>
            <input type="text" class="form-control custom_disabled" name="org_phone" id="org_phone_view" disabled>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">FAX</span>
            <input type="text" class="form-control custom_disabled" name="org_fax" id="org_fax_view" disabled>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Email</span>
            <input type="text" class="form-control custom_disabled" name="org_email" id="org_email_view" disabled>
          </div>
      </div>
     
    </div>
    <div class="col-md-12">
       <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Contact Name</span>
            <input type="text" class="form-control custom_disabled" name="org_contact_name" id="org_contact_name_view" disabled>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Reference Number</span>
            <input type="text" class="form-control custom_disabled" name="org_reference_number" id="org_reference_number_view" disabled>
          </div>
      </div>
      <div class="col-md-4" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Address</span>
            <textarea rows="1" class="form-control custom_disabled" name="org_address" id="org_address_view" disabled></textarea>
        </div>
      </div>
    </div>
    <div class="col-md-12">
     <div class="col-md-4" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Address</span>
            <textarea rows="1" class="form-control custom_disabled" id="org_address_view2" disabled></textarea>
        </div>
      </div>
     <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Zip Code</span>
            <input type="text" class="form-control custom_disabled" name="org_zip_code" id="org_zip_code_view" disabled>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">City</span>
            <input type="text" class="form-control custom_disabled" name="org_city" id="org_city_view" disabled>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Country</span>
            <input type="text" class="form-control custom_disabled" name="org_country" id="org_country_view" disabled>
          </div>
      </div>
      
    </div>
    <div class="col-md-12">  
        <button type="button" class="btn btn-default back_to_listing" style="float:right; margin-left:7px;"><span class="fa fa-close"> Cancel </span></button>
    </div>      
  </div>
<?php echo form_close(); ?>
</div>

