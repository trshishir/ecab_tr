<?php $this->load->view('admin/accounting_config/config_css_js_page'); ?>
<?php 
  $vat = '';
  $vat_area = '';
  $active =  $this->session->flashdata('active_tab');
  if($active =='vat'){
    $vat = 'active';
    $vat_area = 'true';
  }elseif($active =='pay_methode'){
    $pay_methode = 'active';
    $pay_area = 'true';
  }elseif($active =='pay_delay'){
    $pay_delay = 'active';
    $pay_delay_area = 'true';
  }elseif($active =='bill_cat'){
    $bill_cat = 'active';
    $bill_cat_area = 'true';
  }elseif($active == 'sales_cat'){
    $sales_cat = 'active';
    $sales_cat_area = 'true';
  }elseif($active == 'client_cat'){
    $client_cat = 'active';
    $client_cat_area = 'true';
  }elseif($active == 'sup_cat'){
    $sup_cat = 'active';
    $sup_cat_area = 'true';
  }elseif($active =='supplier'){
    $supplier = 'active';
    $supplier_area = 'true';
  }elseif($active =='org_cat'){
    $org_cat = 'active';
    $org_cat_area = 'true';
  }elseif($active =='organization'){
    $organization = 'active';
    $organization_area = 'true';
  }elseif($active =='bank'){
    $bank = 'active';
    $bank_area = 'true';
  }elseif($active =='factors'){
    $factors = 'active';
    $factors_area = 'true';
  }elseif($active =='client_kind'){
    $client_kind = 'active';
    $client_kind_area = 'true';
  }elseif($active =='garantee_fund'){
    $garantee_fund = 'active';
    $garantee_fund_area = 'true';
  }elseif($active =='financement_delay'){
    $financement_delay = 'active';
    $financement_delay_area = 'true';
  }else{
    $statut = 'active';
    $statut_area = 'true';
  }
?>
<div class="col-md-12" style="height: calc(100vh - 140px);">
	<!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist" style="border-bottom: 0;">
      <li class="<?=$statut?>">
          <a href="#STATUTS_PAGE" role="tab" data-toggle="tab" aria-expanded="<?=$statut_area?>">
            <i class="fa fa-suitcase"></i>  STATUTS
          </a>
      </li>
      <li class="<?=$vat ?>">
          <a href="#VAT_PAGE" role="tab" data-toggle="tab" aria-expanded="<?=$vat_area?>">
            <i class="fa fa-percent"></i>  VAT
          </a>
      </li>
      <li class="<?=$garantee_fund ?>">
          <a href="#garantee_fund" role="tab" data-toggle="tab" aria-expanded="<?=$garantee_fund_area?>">
            <i class="fa fa-money"></i> Funds Garantee 
          </a>
      </li>
      
      <li class="<?=$pay_methode?>">
        <a href="#PAYMENT_METHODES_PAGE" role="tab" data-toggle="tab" aria-expanded="<?=$pay_area?>">
          	<i class="fa fa-money"></i> PAYMENT METHODES
          </a>
      </li>
      <li class="<?=$pay_delay;?>">
          <a href="#PAYMENT_DELAYS_PAGE" role="tab" data-toggle="tab" aria-expanded="<?=$pay_delay_area; ?>">
              <i class="fa fa-money"></i> PAYMENT DELAYS
          </a>
      </li>
      <li class="<?=$financement_delay;?>">
          <a href="#financement_delay" role="tab" data-toggle="tab" aria-expanded="<?=$financement_delay_area; ?>">
              <i class="fa fa-money"></i> Financement Delay
          </a>
      </li>
      
      <li class="<?=$bill_cat?>">
          <a href="#BILL_CATEGORY_PAGE" role="tab" data-toggle="tab"  aria-expanded="<?=$bill_cat_area; ?>">
              <i class="fa fa-money"></i> BILLS CATEGORYS
          </a>
      </li>
      <li class="<?= $sales_cat; ?>">
          <a href="#SALES_CATEGORYS_PAGE" role="tab" data-toggle="tab" aria-expanded="<?= $sales_cat_area; ?>">
              <i class="fa fa-money"></i> SERVICE CATEGORYS
          </a>
      </li>
      <li class="<?= $client_kind;?>">
          <a href="#CLIENT_KIND_PAGE" role="tab" data-toggle="tab" aria-expanded="<?= $client_kind_area; ?>">
              <i class="fa fa-user"></i> CLIENT KIND
          </a>
      </li>
      <li class="<?= $client_cat;?>">
          <a href="#CLIENTS_CATEGORYS_PAGE" role="tab" data-toggle="tab" aria-expanded="<?= $client_cat_area; ?>">
              <i class="fa fa-user"></i> CLIENTS CATEGORYS
          </a>
      </li>
      <li class="<?=$sup_cat?>">
          <a href="#SUPLLIERS_CATEGORYS_PAGE" role="tab" data-toggle="tab"  aria-expanded="<?= $sup_cat_area; ?>">
              <i class="fa fa-user"></i> SUPPLIERS CATEGORYS
          </a>
      </li>
      <li class="<?=$supplier?>">
          <a href="#SUPLLIERS_PAGE" role="tab" data-toggle="tab"  aria-expanded="<?= $supplier_area; ?>">
              <i class="fa fa-user"></i> SUPPLIERS
          </a>
      </li>
      <li class="<?=$org_cat?>">
          <a href="#ADMINISTRATIONS_CATEGORYS_PAGE" role="tab" data-toggle="tab" aria-expanded="<?= $org_cat; ?>">
              <i class="fa fa-building"></i> ORGANIZATIONS CATEGORYS
          </a>
      </li>
      <li class="<?=$organization; ?>">
          <a href="#ADMINISTRATIONS_PAGE" role="tab" data-toggle="tab" aria-expanded="<?= $organization_area; ?>">
              <i class="fa fa-building"></i> ORGANIZATIONS
          </a>
      </li>
      <li class="<?= $bank?>">
          <a href="#BANKS_PAGE" role="tab" data-toggle="tab"  aria-expanded="<?= $bank; ?>">
              <i class="fa fa-money"></i> BANKS
          </a>
      </li>
      <li class="<?= $factors?>">
          <a href="#FACTORS_PAGE" role="tab" data-toggle="tab" aria-expanded="<?= $factors; ?>">
              <i class="fa fa-calculator"></i> FACTORS
          </a>
      </li>
    </ul>
    <!-- Tab panes payment_methode-->
    <div class="tab-content">
      <div class="tab-pane fade <?php echo $statut; ?> in" id="STATUTS_PAGE">
          <?php $this->load->view('admin/accounting_config/statuts/index'); ?>
      </div>
      <div class="tab-pane fade <?php echo $vat; ?> in" id="VAT_PAGE">
          <?php $this->load->view('admin/accounting_config/vat/index'); ?>
      </div>
      <div class="tab-pane fade <?php echo $garantee_fund; ?> in" id="garantee_fund">
          <?php $this->load->view('admin/accounting_config/garantee_fund/index'); ?>
      </div>
      <div class="tab-pane fade <?php echo $pay_methode; ?> in" id="PAYMENT_METHODES_PAGE">
          <?php $this->load->view('admin/accounting_config/payment_methodes/index'); ?>
      </div>
      <div class="tab-pane fade <?=$pay_delay;?> in" id="PAYMENT_DELAYS_PAGE">
          <?php $this->load->view('admin/accounting_config/payment_delays/index'); ?>
      </div>
      <div class="tab-pane fade <?=$financement_delay;?> in" id="financement_delay">
          <?php $this->load->view('admin/accounting_config/financement_delay/index'); ?>
      </div>
      
      <div class="tab-pane fade <?= $bill_cat?> in" id="BILL_CATEGORY_PAGE">
          <?php $this->load->view('admin/accounting_config/bill_category/index'); ?>
      </div>
      <div class="tab-pane fade <?= $sales_cat?> in" id="SALES_CATEGORYS_PAGE">
          <?php $this->load->view('admin/accounting_config/sales_category/index'); ?>
      </div>
      <div class="tab-pane fade <?= $client_kind; ?> in" id="CLIENT_KIND_PAGE">
          <?php $this->load->view('admin/accounting_config/client_kind/index'); ?>
      </div>
      <div class="tab-pane fade <?= $client_cat; ?> in" id="CLIENTS_CATEGORYS_PAGE">
          <?php $this->load->view('admin/accounting_config/clients_category/index'); ?>
      </div>
      <div class="tab-pane fade <?= $sup_cat?> in" id="SUPLLIERS_CATEGORYS_PAGE">
          <?php $this->load->view('admin/accounting_config/suppliers_category/index'); ?>
      </div>
      <div class="tab-pane fade <?= $supplier?> in" id="SUPLLIERS_PAGE">
          <?php $this->load->view('admin/accounting_config/suppliers/index'); ?>
      </div>
      <div class="tab-pane fade <?= $org_cat; ?> in" id="ADMINISTRATIONS_CATEGORYS_PAGE">
          <?php $this->load->view('admin/accounting_config/organization_category/index'); ?>
      </div>
      <div class="tab-pane fade <?= $organization; ?> in" id="ADMINISTRATIONS_PAGE">
          <?php $this->load->view('admin/accounting_config/organization/index'); ?>
      </div>
      <div class="tab-pane fade <?= $bank; ?> in" id="BANKS_PAGE">
          <?php $this->load->view('admin/accounting_config/banks/index'); ?>
      </div>
      <div class="tab-pane fade <?= $factors; ?> in" id="FACTORS_PAGE">
          <?php $this->load->view('admin/accounting_config/factors/index'); ?>
      </div>
    </div>
</div>

