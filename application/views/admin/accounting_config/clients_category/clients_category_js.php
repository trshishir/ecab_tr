<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click','.add_client_category',function(){
            $('.client_category_index').css('display','none');
            $('.client_category_edit').css('display','none');
            $('.client_category_view').css('display','none');
            $('.client_category_add').css('display','block');
        });
        $('body').on('click','.back_to_listing',function(){
            $('.client_category_add').css('display','none');
            $('.client_category_edit').css('display','none');
            $('.client_category_view').css('display','none');
            $('.client_category_index').css('display','block');
        });
    })
    $(document.body).on("click", ".edit_client_category", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr')
                statut = row.find('input.client_category_statut').val()
                client_category = row.find('input.client_category').val();
                payment_method_id = row.find('input.payment_methode_id_client').val();
            $('#id_client_category').val(checkBoxVal);
            $('#client_category_statut').val(statut);
            $('#client_category_update').val(client_category);
            $('#payment_methode_id_client').val(payment_method_id);
            $('.client_category_index').css('display','none');
            $('.client_category_add').css('display','none');
            $('.client_category_view').css('display','none');
            $('.client_category_edit').css('display','block');
        } else {
            alert("Select atleast one record");
        }
    });
    function view_client_category(id){
        var row = $('td.row_'+id).closest('tr')
            statut = row.find('input.client_category_statut').val()
            client_category = row.find('input.client_category').val();
            payment_method_id = row.find('input.payment_methode_id_client').val();
        $('#client_category_statut_view').val(statut);
        $('#client_category_update_view').val(client_category);
        $('#payment_methode_id_view_client').val(payment_method_id);
        $('.client_category_index').css('display','none');
        $('.client_category_add').css('display','none');
        $('.client_category_edit').css('display','none');
        $('.client_category_view').css('display','block');
    }
    $(document.body).on("click", ".delete_client_category", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr');
            var id = checkBoxVal;
            if(confirm('Are You sure to delete ?')){
                row.remove();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('admin/accounting_config/delete_config_record'); ?>",
                    data: {
                    'id': id,
                    'table':'vbs_account_client_category',
                    },
                    dataType:'json',
                    success:function(res){
                        alert(res);
                        row.remove();
                    }
                }) 
            }
        } else {
            alert("Select atleast one record");
        }
    });
</script>