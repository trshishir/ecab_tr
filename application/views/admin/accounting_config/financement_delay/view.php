<div class="tab-content responsive" style="width:100%;display: table;">
<?=form_open("")?>
  <div class="driverStatusAdd financement_delay_view" style="display: none;" >
    <div class="col-md-12">  
      <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;">Status</span>
          <select class="form-control custom_disabled" name="statut" id="financement_delay_statut_acc_config_view" disabled>
            <option value="enabled">Enabled</option>
            <option value="disabled">Disabled</option>
          </select>
        </div>
      </div>   
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
          <span style="font-weight: bold;">Financement Delay</span>
          <input type="text" class="form-control custom_disabled" id="financement_delay_acc_config_view" disabled style="width: 86%;display: inline;"> <span style="float: right;margin-top: 7px;">Day(s)</span>
          </div>
      </div>
    </div>
    <div class="col-md-12">  
        <button type="button" class="btn btn-default back_to_listing" style="float:right; margin-left:7px;"><span class="fa fa-close"> Cancel </span></button>
    </div>      
  </div>
<?php echo form_close(); ?>
</div>

