<div class="tab-content responsive" style="width:100%;display: table;">
<?=form_open("admin/accounting_config/accounting_financement_delay_update")?>
  <div class="driverStatusAdd financement_delay_edit" style="display: none;" >
    <input type="hidden" name="id" id="financement_delay_id_acc_config">
    <div class="col-md-12">  
      <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;">Statut</span>
          <select class="form-control" name="statut" id="financement_delay_statut_acc_config" required>
            <option value="enabled">Enabled</option>
            <option value="disabled">Disabled</option>
          </select>
        </div>
      </div>   
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Financement Delay</span>
            <input type="number" class="form-control" name="financement_delay" id="financement_delay_acc_config" required style="width: 86%;display: inline;"> <span style="float: right;margin-top: 7px;">Day(s)</span>
          </div>
      </div>
    </div>
    <div class="col-md-12">  
        <button type="submit" class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
        <button type="button" class="btn btn-default back_to_listing" style="float:right; margin-left:7px;"><span class="fa fa-close"> Cancel </span></button>
    </div>      
  </div>
<?php echo form_close(); ?>
</div>

