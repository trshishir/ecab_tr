
  <div class="module-body table-responsive financement_delay_index">
    <table id="financement_delay_config_id" class="cell-border listing" cellspacing="0" width="100%" data-selected_id="">
      <thead>
        <tr>
          <th class="no-sort text-center">#</th>
          <th class="column-id"><?php echo $this->lang->line('id');?></th>
          <th class="column-date">Date</th>
          <th class="column-date">Time</th>
          <th class="column-date">Added by</th>
          <th class="column-date">Department</th>
          <th>Financement delay</th>
          <th>Status</th>
          <th>Since</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($financement_delay as $key => $value){ ?>
          <?php
              $timestamp=$value['created_at'];
              $date = new DateTime($timestamp);
              $date = $date->format('d/m/Y');
              $time = new DateTime($timestamp);
              $time = $time->format('H:i:s');
          ?>
        <tr>
         
          <input type="hidden" class="financement_delay_statut_acc_config" value="<?php echo $value['statut'];?>">
          <input type="hidden" class="financement_delay_acc_config" value="<?php echo $value['financement_delay'];?>">
          <td class="row_<?=$value['id'];?>"><input type="checkbox" value="<?=$value['id'];?>" data-id="<?=$value['id'];?>" class="checkbox checkboxx singleSelect"></td>
            <td>
                <a class="view_statuts" onclick="view_financement_delay('<?= $value['id']; ?>')">
                    <?=create_timestamp_uid($value['created_at'],$value['id']);?>
                </a>
            </td>
            <td><?=$date ?></td>
            <td><?= $time ?></td>
            
            <td><?= $value['civility'].' '.$value['first_name'].' '.$value['last_name']; ?></td>
            <td><?= $value['department_name'];?></td>
            <td><?= ucwords($value['financement_delay']); ?> Day(s)</td>
            <td><?=$value['statut']=='enabled'?'<span class="label label-success">Enabled</span>':'<span class="label label-danger">Disabled</span>';?></td>
            <td style="white-space: nowrap;"><?=timeDiff($value['created_at']);?></td>
          </tr>
      <?php } ?>  
      </tbody>
    </table>
  </div>
<div id="table-filter" class="hide">
  <input type="text" placeholder="Search" data-name="short_code" class="form-control">
<!--   <a href="#" class="btn btn-sm btn-default " onclick="setUpdateAction();"><i class="fa fa-search"></i> Search</a>
 --></div>

<?php $this->load->view('admin/accounting_config/financement_delay/financement_delay_js');?>