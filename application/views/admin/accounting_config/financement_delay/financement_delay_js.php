<script type="text/javascript">
	$(document).ready(function(){
        $('body').on('click','.add_config_financement_delay',function(){
            $('.financement_delay_index').css('display','none');
            $('.financement_delay_edit').css('display','none');
            $('.financement_delay_view').css('display','none');
            $('.financement_delay_add').css('display','block');
        });
        $('body').on('click','.back_to_listing',function(){
            $('.financement_delay_add').css('display','none');
            $('.financement_delay_edit').css('display','none');
            $('.financement_delay_view').css('display','none');
            $('.financement_delay_index').css('display','block');
        });
    })
	$(document.body).on("click", ".edit_config_financement_delay", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
        	var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr')
            	statut = row.find('input.financement_delay_statut_acc_config').val()
            	payment_method = row.find('input.financement_delay_acc_config').val();
            $('#financement_delay_id_acc_config').val(checkBoxVal);
            $('#financement_delay_statut_acc_config').val(statut);
            $('#financement_delay_acc_config').val(payment_method);
        	$('.financement_delay_index').css('display','none');
            $('.financement_delay_add').css('display','none');
            $('.financement_delay_view').css('display','none');
            $('.financement_delay_edit').css('display','block');
        } else {
            alert("Select atleast one record");
        }
    });
	function view_financement_delay(id){
		var row = $('td.row_'+id).closest('tr')
            statut = row.find('input.financement_delay_statut_acc_config').val()
            financement_delay = row.find('input.financement_delay_acc_config').val();
        $('#financement_delay_statut_acc_config_view').val(statut);
        $('#financement_delay_acc_config_view').val(financement_delay)
        $('.financement_delay_index').css('display','none');
        $('.financement_delay_add').css('display','none');
        $('.financement_delay_edit').css('display','none');
        $('.financement_delay_view').css('display','block');
	}
    $(document.body).on("click", ".delete_financement_delay", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr');
            var id = checkBoxVal;
            if(confirm('Are You sure to delete ?')){
                row.remove();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('admin/accounting_config/delete_config_record'); ?>",
                    data: {
                    'id': id,
                    'table':'vbs_account_financement_delay',
                    },
                    dataType:'json',
                    success:function(res){
                        alert(res);
                        row.remove();
                    }
                }) 
            }
        } else {
            alert("Select atleast one record");
        }
    });
</script>