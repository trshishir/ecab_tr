<div class="tab-content responsive" style="width:100%;display: table;">
<?=form_open("admin/accounting_config/accounting_factor_add")?>
  <div class="driverStatusAdd factor_add" style="display: none;" >
    <div class="col-md-12">  
      <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;">Statut</span>
          <select class="form-control" name="statut">
            <option value="enabled">Enabled</option>
            <option value="disabled">Disabled</option>
          </select>
        </div>
      </div>   
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Factor Name</span>
            <input type="text" class="form-control" name="factor_name" required>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Phone</span>
            <input type="text" class="form-control" name="factor_phone">
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">FAX</span>
            <input type="text" class="form-control" name="factor_fax">
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Email</span>
            <input type="text" class="form-control" name="factor_email">
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Contact Name</span>
            <input type="text" class="form-control" name="factor_contact_name">
          </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Reference Number</span>
            <input type="text" class="form-control" name="factor_reference_number">
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Financement Delay</span>
            <select class="form-control" name="finacement_delay_id">
              <?php foreach($financement_delay as $key => $value){ ?>
                <option value="<?= $value['id']; ?>"><?= $value['financement_delay']; ?></option>
              <?php } ?>  
            </select>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Garantee Funds</span>
            <select class="form-control" name="garantee_fund_id">
              <?php foreach($garantee_fund as $key => $value){ ?>
                <option value="<?= $value['id']; ?>"><?= $value['garantee_fund']; ?></option>
              <?php } ?>  
            </select>
          </div>
      </div>
      <div class="col-md-4" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Address</span>
            <textarea rows="1" class="form-control" name="factor_address" placeholder="Address line 1"></textarea>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="col-md-4" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Address</span>
            <textarea rows="1" class="form-control" name="factor_address2" placeholder="Address line 2"></textarea>
        </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Zip Code</span>
            <input type="text" class="form-control" name="factor_zip_code">
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">City</span>
            <input type="text" class="form-control" name="factor_city">
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Country</span>
            <input type="text" class="form-control" name="factor_country">
          </div>
      </div>
    </div> 
    <div class="col-md-12">  
        <button type="submit" class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
        <button type="button" class="btn btn-default back_to_listing" style="float:right; margin-left:7px;"><span class="fa fa-close"> Cancel </span></button>
    </div>      
  </div>
<?php echo form_close(); ?>
</div>

