<div class="tab-content responsive" style="width:100%;display: table;">
<?=form_open()?>
  <div class="driverStatusAdd factor_view" style="display: none;" >
    <div class="col-md-12">  
      <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;">Statut</span>
          <select class="form-control custom_disabled custom_disabled" name="statut" id="statut_factor_view" disabled>
            <option value="enabled">Enabled</option>
            <option value="disabled">Disabled</option>
          </select>
        </div>
      </div>  
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Banks Name</span>
            <input type="text" class="form-control custom_disabled" name="factor_name" id="factor_name_view" disabled>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Phone</span>
            <input type="text" class="form-control custom_disabled" name="factor_phone" id="factor_phone_view" disabled>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">FAX</span>
            <input type="text" class="form-control custom_disabled" name="factor_fax" id="factor_fax_view" disabled>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Email</span>
            <input type="text" class="form-control custom_disabled" name="factor_email" id="factor_email_view" disabled>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Contact Name</span>
            <input type="text" class="form-control custom_disabled" name="factor_contact_name" id="factor_contact_name_view" disabled>
          </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Reference Number</span>
            <input type="text" class="form-control custom_disabled" name="factor_reference_number" id="factor_reference_number_view" disabled>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Financement Delay</span>
            <select class="form-control custom_disabled" id="finacement_delay_id_view" disabled>
              <?php foreach($financement_delay as $key => $value){ ?>
                <option value="<?= $value['id']; ?>"><?= $value['financement_delay']; ?></option>
              <?php } ?>  
            </select>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Garantee Funds</span>
            <select class="form-control custom_disabled" id="garantee_fund_id_view" disabled>
              <?php foreach($garantee_fund as $key => $value){ ?>
                <option value="<?= $value['id']; ?>"><?= $value['garantee_fund']; ?></option>
              <?php } ?>  
            </select>
          </div>
      </div>
      <div class="col-md-4" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Address</span>
            <textarea rows="1" class="form-control custom_disabled" name="factor_address" id="factor_address_view" disabled></textarea>
        </div>
      </div>
      
    </div>
    <div class="col-md-12">
      <div class="col-md-4" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Address</span>
            <textarea rows="1" class="form-control custom_disabled" name="factor_address2" id="factor_address_view2" disabled></textarea>
        </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Zip Code</span>
            <input type="text" class="form-control custom_disabled" name="factor_zip_code" id="factor_zip_code_view" disabled>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">City</span>
            <input type="text" class="form-control custom_disabled" name="factor_city" id="factor_city_view" disabled>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Country</span>
            <input type="text" class="form-control custom_disabled" name="factor_country" id="factor_country_view" disabled>
          </div>
      </div>
    </div>
    <div class="col-md-12">  
        <button type="button" class="btn btn-default back_to_listing" style="float:right; margin-left:7px;"><span class="fa fa-close"> Cancel </span></button>
    </div>      
  </div>
<?php echo form_close(); ?>
</div>

