<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click','.add_factor',function(){
            $('.factor_index').css('display','none');
            $('.factor_edit').css('display','none');
            $('.factor_view').css('display','none');
            $('.factor_add').css('display','block');
        });
        $('body').on('click','.back_to_listing',function(){
            $('.factor_add').css('display','none');
            $('.factor_edit').css('display','none');
            $('.factor_view').css('display','none');
            $('.factor_index').css('display','block');
        });
    })
    $(document.body).on("click", ".edit_factor", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr')
                statut = row.find('input.factor_statut').val()
                factor_name = row.find('input.factor_name').val();
                factor_phone = row.find('input.factor_phone').val();
                factor_fax = row.find('input.factor_fax').val();
                factor_email = row.find('input.factor_email').val();
                factor_address = row.find('input.factor_address').val();
                factor_address2 = row.find('input.factor_address2').val();
                factor_contact_name = row.find('input.factor_contact_name').val();
                payment_delay_id = row.find('input.payment_delay_id').val();
                factor_zip_code = row.find('input.factor_zip_code').val();
                factor_city = row.find('input.factor_city').val();
                factor_country = row.find('input.factor_country').val();
                factor_reference_number = row.find('input.factor_reference_number').val();
                garantee_fund_id = row.find('input.garantee_fund_id').val();
                finacement_delay_id = row.find('input.finacement_delay_id').val();
            $('#id_factor').val(checkBoxVal);
            $('#factor_statut').val(statut);
            $('#factor_name').val(factor_name);
            $('#factor_phone').val(factor_phone);
            $('#factor_fax').val(factor_fax);
            $('#factor_email').val(factor_email);
            $('#factor_address').val(factor_address);
            $('#factor_address2').val(factor_address2);
            $('#factor_contact_name').val(factor_contact_name);
            $('#factor_reference_number').val(factor_reference_number);
            $('#payment_delay_id').val(payment_delay_id);
            $('#factor_zip_code').val(factor_zip_code);
            $('#factor_city').val(factor_city);
            $('#factor_country').val(factor_country);
            $('#garantee_fund_id').val(garantee_fund_id);
            $('#finacement_delay_id').val(finacement_delay_id);
            $('.factor_index').css('display','none');
            $('.factor_add').css('display','none');
            $('.factor_view').css('display','none');
            $('.factor_edit').css('display','block');
        } else {
            alert("Select atleast one record");
        }
    });
    function view_factor(id){
        var row = $('td.row_'+id).closest('tr')
            statut = row.find('input.factor_statut').val()
                factor_name = row.find('input.factor_name').val();
                factor_phone = row.find('input.factor_phone').val();
                factor_fax = row.find('input.factor_fax').val();
                factor_email = row.find('input.factor_email').val();
                factor_address = row.find('input.factor_address').val();
                factor_address2 = row.find('input.factor_address2').val();
                factor_contact_name = row.find('input.factor_contact_name').val();
                payment_delay_id = row.find('input.payment_delay_id').val();
                factor_zip_code = row.find('input.factor_zip_code').val();
                factor_city = row.find('input.factor_city').val();
                factor_country = row.find('input.factor_country').val();
                factor_reference_number = row.find('input.factor_reference_number').val();
                garantee_fund_id = row.find('input.garantee_fund_id').val();
                finacement_delay_id = row.find('input.finacement_delay_id').val();
            $('#factor_statut_view').val(statut);
            $('#factor_name_view').val(factor_name);
            $('#factor_phone_view').val(factor_phone);
            $('#factor_fax_view').val(factor_fax);
            $('#factor_email_view').val(factor_email);
            $('#factor_address_view').val(factor_address);
            $('#factor_address_view2').val(factor_address2);
            $('#factor_contact_name_view').val(factor_contact_name);
            $('#factor_reference_number_view').val(factor_reference_number);
            $('#payment_delay_id_view').val(payment_delay_id);
            $('#factor_zip_code_view').val(factor_zip_code);
            $('#factor_city_view').val(factor_city);
            $('#factor_country_view').val(factor_country);
            $('#garantee_fund_id_view').val(garantee_fund_id);
            $('#finacement_delay_id_view').val(finacement_delay_id);
        $('.factor_index').css('display','none');
        $('.factor_add').css('display','none');
        $('.factor_edit').css('display','none');
        $('.factor_view').css('display','block');
    }
    $(document.body).on("click", ".delete_factors", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr');
            var id = checkBoxVal;
            if(confirm('Are You sure to delete ?')){
                row.remove();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('admin/accounting_config/delete_config_record'); ?>",
                    data: {
                    'id': id,
                    'table':'vbs_account_factors',
                    },
                    dataType:'json',
                    success:function(res){
                        alert(res);
                        row.remove();
                    }
                }) 
            }
        } else {
            alert("Select atleast one record");
        }
    });
</script>