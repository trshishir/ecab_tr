  <div class="module-body table-responsive factor_index">
    <table id="factors_grid" class="cell-border listing" cellspacing="0" width="100%" data-selected_id="">
      <thead>
        <tr>
          <th class="no-sort text-center">#</th>
          <th class="column-id"><?php echo $this->lang->line('id');?></th>
          <th class="column-date">Date</th>
          <th class="column-date">Time</th>
          <th class="column-date">Added by</th>
          <th>From Deparment</th>
          <th>Financement Delay</th>
          <th>Garantee Fund</th>
          <th>Name</th>
          <th>Phone</th>
          <th>FAX</th>
          <th>Email</th>
          <th>Contact Name</th>
          <th>Zip Code</th>
          <th>Statut</th>
          <th>Since</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($factors as $key => $value){ ?>
          <?php
              $timestamp=$value['created_at'];
              $date = new DateTime($timestamp);
              $date = $date->format('d/m/Y');
              $time = new DateTime($timestamp);
              $time = $time->format('H:i:s');
          ?>
        <tr>
          <input type="hidden" class="factor_statut" value="<?php echo $value['statut'];?>">
          <input type="hidden" class="factor_name" value="<?php echo $value['factor_name'];?>">
          <input type="hidden" class="factor_phone" value="<?php echo $value['factor_phone'];?>">
          <input type="hidden" class="factor_fax" value="<?php echo $value['factor_fax'];?>">
          <input type="hidden" class="factor_email" value="<?php echo $value['factor_email'];?>">
          <input type="hidden" class="factor_contact_name" value="<?php echo $value['factor_contact_name'];?>">
          <input type="hidden" class="factor_reference_number" value="<?php echo $value['factor_reference_number'];?>">
          <input type="hidden" class="factor_address" value="<?php echo $value['factor_address'];?>">
          <input type="hidden" class="factor_address2" value="<?php echo $value['factor_address2'];?>">
          <input type="hidden" class="factor_zip_code" value="<?php echo $value['factor_zip_code'];?>">
          <input type="hidden" class="factor_city" value="<?php echo $value['factor_city'];?>">
          <input type="hidden" class="factor_country" value="<?php echo $value['factor_country'];?>">
          <input type="hidden" class="finacement_delay_id" value="<?php echo $value['finacement_delay_id'];?>">
          <input type="hidden" class="garantee_fund_id" value="<?php echo $value['garantee_fund_id'];?>">
          <td class="row_<?=$value['id'];?>"><input type="checkbox" value="<?=$value['id'];?>" data-id="<?=$value['id'];?>" class="checkbox checkboxx singleSelect"></td>
            <td>
                <a class="view_statuts" onclick="view_factor('<?= $value['id']; ?>')">
                    <?=create_timestamp_uid($value['created_at'],$value['id']);?>
                </a>
            </td>
            <td><?=$date ?></td>
            <td><?= $time ?></td>
            <td><?= $value['civility'].' '.$value['first_name'].' '.$value['last_name']; ?></td>
            <td><?= ucwords($value['department_name']); ?></td>
            <td><?= ucwords($value['financement_delay']); ?> Day(s)</td>
            <td><?= ucwords($value['garantee_fund']); ?> %</td>
            <td><?= ucwords($value['factor_name']); ?></td>
            <td><?= ucwords($value['factor_phone']); ?></td>
            <td><?= ucwords($value['factor_fax']); ?></td>
            <td><?= ucwords($value['factor_email']); ?></td>
            <td><?= ucwords($value['factor_contact_name']); ?></td>
            <td><?= ucwords($value['factor_zip_code']); ?></td>
            <td><?=$value['statut']=='enabled'?'<span class="label label-success">Enabled</span>':'<span class="label label-danger">Disabled</span>';?></td>
            <td style="white-space: nowrap;"><?=timeDiff($value['created_at']);?></td>
          </tr>
      <?php } ?>  
      </tbody>
    </table>
  </div>
<div id="table-filter" class="hide">
  <input type="text" placeholder="Search" data-name="short_code" class="form-control">
</div>

<?php $this->load->view('admin/accounting_config/factors/factors_js');?>