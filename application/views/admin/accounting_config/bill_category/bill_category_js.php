<script type="text/javascript">
	$(document).ready(function(){
        $('body').on('click','.add_bill_category',function(){
            $('.bill_category_index').css('display','none');
            $('.bill_category_edit').css('display','none');
            $('.bill_category_view').css('display','none');
            $('.bill_category_add').css('display','block');
        });
        $('body').on('click','.back_to_listing',function(){
            $('.bill_category_add').css('display','none');
            $('.bill_category_edit').css('display','none');
            $('.bill_category_view').css('display','none');
            $('.bill_category_index').css('display','block');
        });
    })
	$(document.body).on("click", ".edit_bill_category", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
        	var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr')
            	statut = row.find('input.bill_category_statut').val()
            	bill_category = row.find('input.bill_category').val();
                bill_category_vat = row.find('input.bill_category_vat').val();
                payment_methode_id = row.find('input.payment_methode_id').val();
            $('#id_bill_category').val(checkBoxVal);
            $('#bill_category_statut').val(statut);
            $('#bill_category_update').val(bill_category);
            $('#bill_category_vat').val(bill_category_vat);
            $('#payment_methode_id').val(payment_methode_id);
        	$('.bill_category_index').css('display','none');
            $('.bill_category_add').css('display','none');
            $('.bill_category_view').css('display','none');
            $('.bill_category_edit').css('display','block');
        } else {
            alert("Select atleast one record");
        }
    });
	function view_bill_category(id){
		var row = $('td.row_'+id).closest('tr')
            statut = row.find('input.bill_category_statut').val()
            bill_category = row.find('input.bill_category').val();
            bill_category_vat = row.find('input.bill_category_vat').val();
            payment_methode_id = row.find('input.payment_methode_id').val();
        $('#bill_category_statut_view').val(statut);
        $('#bill_category_update_view').val(bill_category);
        $('#bill_category_vat_view').val(bill_category_vat);
        $('#payment_methode_id_view').val(payment_methode_id);
        $('.bill_category_index').css('display','none');
        $('.bill_category_add').css('display','none');
        $('.bill_category_edit').css('display','none');
        $('.bill_category_view').css('display','block');
	}
    $(document.body).on("click", ".delete_bill_category", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr');
            var id = checkBoxVal;
            if(confirm('Are You sure to delete ?')){
                row.remove();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('admin/accounting_config/delete_config_record'); ?>",
                    data: {
                    'id': id,
                    'table':'vbs_account_bill_category',
                    },
                    dataType:'json',
                    success:function(res){
                        alert(res);
                        row.remove();
                    }
                }) 
            }
        } else {
            alert("Select atleast one record");
        }
    });
</script>