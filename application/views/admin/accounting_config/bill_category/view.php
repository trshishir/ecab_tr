<div class="tab-content responsive" style="width:100%;display: table;">
<?=form_open("admin/accounting_config/accounting_statuts_update")?>
  <div class="driverStatusAdd bill_category_view" style="display: none;" >
    <div class="col-md-12">  
      <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;">Statut</span>
          <select class="form-control custom_disabled" name="statut" id="statut_bill_category_view" disabled>
            <option value="enabled">Enabled</option>
            <option value="disabled">Disabled</option>
          </select>
        </div>
      </div>  
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Bill Category</span>
            <input type="text" class="form-control custom_disabled" name="bill_category" id="bill_category_update_view" disabled>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">VAT</span>
            <select class="form-control custom_disabled" id="bill_category_vat_view" disabled>
              <?php foreach($vat as $key =>$value){ ?>
                <option value="<?php echo $value['id']; ?>"><?php echo $value['vat']; ?></option>
              <?php } ?>
            </select>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Payment Method</span>
            <select class="form-control custom_disabled" id="payment_methode_id_view" disabled>
              <?php foreach($payment_methode as $key =>$value){ ?>
                <option value="<?php echo $value['id']; ?>"><?php echo $value['payment_methode']; ?></option>
              <?php } ?>
            </select>
          </div>
      </div>
    </div>
    <div class="col-md-12">  
        <button type="button" class="btn btn-default back_to_listing" style="float:right; margin-left:7px;"><span class="fa fa-close"> Cancel </span></button>
    </div>      
  </div>
<?php echo form_close(); ?>
</div>

