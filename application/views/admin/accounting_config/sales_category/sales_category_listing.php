  <div class="module-body table-responsive sales_category_index">
    <table id="sales_category_grid" class="cell-border listing" cellspacing="0" width="100%" data-selected_id="">
      <thead>
        <tr>
          <th class="no-sort text-center">#</th>
          <th class="column-id"><?php echo $this->lang->line('id');?></th>
          <th class="column-date">Date</th>
          <th class="column-date">Time</th>
          <th class="column-date">Added by</th>
          <th>From Deparment</th>
          <th>Service Category</th>
          <th>VAT</th>
          <th>Statut</th>
          <th>Since</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($sales_category as $key => $value){ ?>
          <?php
              $timestamp=$value['created_at'];
              $date = new DateTime($timestamp);
              $date = $date->format('d/m/Y');
              $time = new DateTime($timestamp);
              $time = $time->format('H:i:s');
          ?>
        <tr>
          <input type="hidden" class="sales_category_statut" value="<?php echo $value['statut'];?>">
          <input type="hidden" class="sales_category" value="<?php echo $value['sales_category'];?>">
          <input type="hidden" class="sales_category_vat" value="<?php echo $value['sales_category_vat'];?>">
          <input type="hidden" class="vat_id" value="<?php echo $value['vat_id'];?>">
          <td class="row_<?=$value['id'];?>"><input type="checkbox" value="<?=$value['id'];?>" data-id="<?=$value['id'];?>" class="checkbox checkboxx singleSelect"></td>
            <td>
                <a class="view_statuts" onclick="view_sales_category('<?= $value['id']; ?>')">
                    <?=create_timestamp_uid($value['created_at'],$value['id']);?>
                </a>
            </td>
            <td><?=$date ?></td>
            <td><?= $time ?></td>
            <td><?= $value['civility'].' '.$value['first_name'].' '.$value['last_name']; ?></td>
            <td><?= ucwords($value['department_name']); ?></td>
            <td><?= ucwords($value['sales_category']); ?></td>
            <td><?= ucwords($value['vat']); ?>%</td>
            <td><?=$value['statut']=='enabled'?'<span class="label label-success">Enabled</span>':'<span class="label label-danger">Disabled</span>';?></td>
            <td style="white-space: nowrap;"><?=timeDiff($value['created_at']);?></td>
          </tr>
      <?php } ?>  
      </tbody>
    </table>
  </div>
<div id="table-filter" class="hide">
  <input type="text" placeholder="Search" data-name="short_code" class="form-control">
</div>

<?php $this->load->view('admin/accounting_config/sales_category/sales_category_js');?>