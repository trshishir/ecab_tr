<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click','.add_sales_category',function(){
            $('.sales_category_index').css('display','none');
            $('.sales_category_edit').css('display','none');
            $('.sales_category_view').css('display','none');
            $('.sales_category_add').css('display','block');
        });
        $('body').on('click','.back_to_listing',function(){
            $('.sales_category_add').css('display','none');
            $('.sales_category_edit').css('display','none');
            $('.sales_category_view').css('display','none');
            $('.sales_category_index').css('display','block');
        });
    })
    $(document.body).on("click", ".edit_sales_category", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr')
                statut = row.find('input.sales_category_statut').val()
                sales_category = row.find('input.sales_category').val();
                vat_id = row.find('input.vat_id').val();
            $('#id_sales_category').val(checkBoxVal);
            $('#sales_category_statut').val(statut);
            $('#sales_category_update').val(sales_category);
            $('#vat_id_sales').val(vat_id);
            $('.sales_category_index').css('display','none');
            $('.sales_category_add').css('display','none');
            $('.sales_category_view').css('display','none');
            $('.sales_category_edit').css('display','block');
        } else {
            alert("Select atleast one record");
        }
    });
    function view_sales_category(id){
        var row = $('td.row_'+id).closest('tr')
            statut = row.find('input.sales_category_statut').val()
            sales_category = row.find('input.sales_category').val();
            vat_id = row.find('input.vat_id').val();
        $('#sales_category_statut_view').val(statut);
        $('#sales_category_update_view').val(sales_category);
        $('#vat_id_view_sales').val(vat_id);
        $('.sales_category_index').css('display','none');
        $('.sales_category_add').css('display','none');
        $('.sales_category_edit').css('display','none');
        $('.sales_category_view').css('display','block');
    }
    $(document.body).on("click", ".delete_sales_category", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr');
            var id = checkBoxVal;
            if(confirm('Are You sure to delete ?')){
                row.remove();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('admin/accounting_config/delete_config_record'); ?>",
                    data: {
                    'id': id,
                    'table':'vbs_account_sales_category',
                    },
                    dataType:'json',
                    success:function(res){
                        alert(res);
                        row.remove();
                    }
                }) 
            }
        } else {
            alert("Select atleast one record");
        }
    });
</script>