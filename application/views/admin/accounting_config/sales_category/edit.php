<div class="tab-content responsive" style="width:100%;display: table;">
<?=form_open("admin/accounting_config/accounting_sales_category_update")?>
  <div class="driverStatusAdd sales_category_edit" style="display: none;" >
    <input type="hidden" name="id" id="id_sales_category">
    <div class="col-md-12">  
      <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;">Statut</span>
          <select class="form-control" name="statut" id="statut_sales_category" required>
            <option value="enabled">Enabled</option>
            <option value="disabled">Disabled</option>
          </select>
        </div>
      </div>  
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">SERVICE Category</span>
            <input type="text" class="form-control" name="sales_category" id="sales_category_update" required>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">VAT</span>
            <select class="form-control" name="vat_id" id="vat_id_sales" required>
              <?php foreach($vat as $key =>$value){ ?>
                <option value="<?php echo $value['id']; ?>"><?php echo $value['vat']; ?></option>
              <?php } ?>
            </select>
          </div>
      </div>
    </div>
    <div class="col-md-12">  
        <button type="submit" class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
        <button type="button" class="btn btn-default back_to_listing" style="float:right; margin-left:7px;"><span class="fa fa-close"> Cancel </span></button>
    </div>      
  </div>
<?php echo form_close(); ?>
</div>

