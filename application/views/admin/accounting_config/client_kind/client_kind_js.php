<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click','.add_client_kind',function(){
            $('.client_kind_index').css('display','none');
            $('.client_kind_edit').css('display','none');
            $('.client_kind_view').css('display','none');
            $('.client_kind_add').css('display','block');
        });
        $('body').on('click','.back_to_listing',function(){
            $('.client_kind_add').css('display','none');
            $('.client_kind_edit').css('display','none');
            $('.client_kind_view').css('display','none');
            $('.client_kind_index').css('display','block');
        });
    })
    $(document.body).on("click", ".edit_client_kind", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr')
                statut = row.find('input.client_kind_statut').val()
                client_kind = row.find('input.client_kind').val();
                payment_method_id = row.find('input.payment_delay_id_client_kind').val();
            $('#id_client_kind').val(checkBoxVal);
            $('#client_kind_statut').val(statut);
            $('#client_kind_update').val(client_kind);
            $('#payment_delay_id_client_kind').val(payment_method_id);
            $('.client_kind_index').css('display','none');
            $('.client_kind_add').css('display','none');
            $('.client_kind_view').css('display','none');
            $('.client_kind_edit').css('display','block');
        } else {
            alert("Select atleast one record");
        }
    });
    function view_client_kind(id){
        var row = $('td.row_'+id).closest('tr')
            statut = row.find('input.client_kind_statut').val()
            client_kind = row.find('input.client_kind').val();
            payment_method_id = row.find('input.payment_delay_id_client_kind').val();
        $('#client_kind_statut_view').val(statut);
        $('#client_kind_update_view').val(client_kind);
        $('#payment_delay_id_view_client_kind').val(payment_method_id);
        $('.client_kind_index').css('display','none');
        $('.client_kind_add').css('display','none');
        $('.client_kind_edit').css('display','none');
        $('.client_kind_view').css('display','block');
    }
    $(document.body).on("click", ".delete_client_kind", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr');
            var id = checkBoxVal;
            if(confirm('Are You sure to delete ?')){
                row.remove();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('admin/accounting_config/delete_config_record'); ?>",
                    data: {
                    'id': id,
                    'table':'vbs_accounting_client_kind',
                    },
                    dataType:'json',
                    success:function(res){
                        alert(res);
                        row.remove();
                    }
                }) 
            }
        } else {
            alert("Select atleast one record");
        }
    });
</script>