<div class="tab-content responsive" style="width:100%;display: table;">
<?=form_open()?>
  <div class="driverStatusAdd client_kind_view" style="display: none;" >
    <div class="col-md-12">  
      <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;">Statut</span>
          <select class="form-control custom_disabled" name="statut" id="statut_client_kind_view" disabled>
            <option value="enabled">Enabled</option>
            <option value="disabled">Disabled</option>
          </select>
        </div>
      </div>  
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">client Category</span>
            <input type="text" class="form-control custom_disabled" name="client_kind" id="client_kind_update_view" disabled>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Payment Delay</span>
            <select class="form-control custom_disabled" id="payment_delay_id_view_client_kind" disabled>
              <?php foreach($payment_delay as $key =>$value){ ?>
                <option value="<?php echo $value['id']; ?>"><?php echo $value['payment_delay']; ?></option>
              <?php } ?>
            </select>
          </div>
      </div>
    </div>
    <div class="col-md-12">  
        <button type="button" class="btn btn-default back_to_listing" style="float:right; margin-left:7px;"><span class="fa fa-close"> Cancel </span></button>
    </div>      
  </div>
<?php echo form_close(); ?>
</div>

