<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click','.add_vat',function(){
            
            $('.vat_index').css('display','none');
            $('.vat_edit').css('display','none');
            $('.vat_view').css('display','none');
            $('.vat_add').css('display','block');
        });
        $('body').on('click','.back_to_listing',function(){
            $('.vat_add').css('display','none');
            $('.vat_edit').css('display','none');
            $('.vat_view').css('display','none');
            $('.vat_index').css('display','block');
        });
    })
    $(document.body).on("click", ".edit_vat", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr')
                statut = row.find('input.vat_statut').val()
                vat = row.find('input.vat').val();
            $('#id_vat').val(checkBoxVal);
            $('#vat_statut').val(statut);
            $('#vat_update').val(vat);
            $('.vat_index').css('display','none');
            $('.vat_add').css('display','none');
            $('.vat_view').css('display','none');
            $('.vat_edit').css('display','block');
        } else {
            alert("Select atleast one record");
        }
    });
    function view_vat(id){
        var row = $('td.row_'+id).closest('tr')
            statut = row.find('input.vat_statut').val()
            vat = row.find('input.vat').val();
        $('#vat_statut_view').val(statut);
        $('#vat_update_view').val(vat);
        $('.vat_index').css('display','none');
        $('.vat_add').css('display','none');
        $('.vat_edit').css('display','none');
        $('.vat_view').css('display','block');
    }
    $(document.body).on("click", ".delete_vat", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr');
            var id = checkBoxVal;
            if(confirm('Are You sure to delete ?')){
                row.remove();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('admin/accounting_config/delete_config_record'); ?>",
                    data: {
                    'id': id,
                    'table':'vbs_account_vat',
                    },
                    dataType:'json',
                    success:function(res){
                        alert(res);
                        row.remove();
                    }
                }) 
            }
        } else {
            alert("Select atleast one record");
        }
    });
</script>