<div class="tab-content responsive" style="width:100%;display: table;">
<?=form_open("admin/accounting_config/accounting_statuts_update")?>
  <div class="driverStatusAdd payment_delay_view" style="display: none;" >
    <div class="col-md-12">  
      <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;">Status</span>
          <select class="form-control custom_disabled" name="statut" id="payment_delay_statut_acc_config_view" disabled>
            <option value="enabled">Enabled</option>
            <option value="disabled">Disabled</option>
          </select>
        </div>
      </div>   
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
          <span style="font-weight: bold;">Payment Delay</span>
          <input type="text" class="form-control custom_disabled" id="payment_delay_acc_config_view" required>
          </div>
      </div>
    </div>
    <div class="col-md-12">  
        <button type="button" class="btn btn-default back_to_listing" style="float:right; margin-left:7px;"><span class="fa fa-close"> Cancel </span></button>
    </div>      
  </div>
<?php echo form_close(); ?>
</div>

