<script type="text/javascript">
	$(document).ready(function(){
        $('body').on('click','.add_config_payment_delay',function(){
            $('.payment_delay_index').css('display','none');
            $('.payment_delay_edit').css('display','none');
            $('.payment_delay_view').css('display','none');
            $('.payment_delay_add').css('display','block');
        });
        $('body').on('click','.back_to_listing',function(){
            $('.payment_delay_add').css('display','none');
            $('.payment_delay_edit').css('display','none');
            $('.payment_delay_view').css('display','none');
            $('.payment_delay_index').css('display','block');
        });
    })
	$(document.body).on("click", ".edit_config_payment_delay", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
        	var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr')
            	statut = row.find('input.payment_delay_statut_acc_config').val()
            	payment_method = row.find('input.payment_delay_acc_config').val();
            $('#payment_delay_id_acc_config').val(checkBoxVal);
            $('#payment_delay_statut_acc_config').val(statut);
            $('#payment_delay_acc_config').val(payment_method);
        	$('.payment_delay_index').css('display','none');
            $('.payment_delay_add').css('display','none');
            $('.payment_delay_view').css('display','none');
            $('.payment_delay_edit').css('display','block');
        } else {
            alert("Select atleast one record");
        }
    });
	function view_payment_delay(id){
		var row = $('td.row_'+id).closest('tr')
            statut = row.find('input.payment_delay_statut_acc_config').val()
            payment_delay = row.find('input.payment_delay_acc_config').val();
        $('#payment_delay_statut_acc_config_view').val(statut);
        $('#payment_delay_acc_config_view').val(payment_delay)
        $('.payment_delay_index').css('display','none');
        $('.payment_delay_add').css('display','none');
        $('.payment_delay_edit').css('display','none');
        $('.payment_delay_view').css('display','block');
	}
    $(document.body).on("click", ".delete_payment_delays", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr');
            var id = checkBoxVal;
            if(confirm('Are You sure to delete ?')){
                row.remove();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('admin/accounting_config/delete_config_record'); ?>",
                    data: {
                    'id': id,
                    'table':'vbs_accounting_payment_delays',
                    },
                    dataType:'json',
                    success:function(res){
                        alert(res);
                        row.remove();
                    }
                }) 
            }
        } else {
            alert("Select atleast one record");
        }
    });
</script>