
<script type="text/javascript">
  $(document).ready(function () {
    var datatables1 = $('#statuts_config').DataTable({
        columnDefs: [
        {className: "dt-body-center ", targets: "_all"},
        {targets: 'no-sort', orderable: false}
        ],
        dom: '<"table-filter">B<"toolbar">frtip',
        language: {search: "", searchPlaceholder: "Search"},
        buttons: [
        'excelHtml5',
        'csvHtml5',
        'pdfHtml5'
        ],
        initComplete: function () {
            $("div.toolbar")
            .html('<a class="btn btn-sm btn-default add_config_statuts"><i class="fa fa-plus"></i> Add</a> <span class="btn btn-sm btn-default edit_config_statuts"><i class="fa fa-pencil"></i> Edit</span> <a class="btn btn-sm btn-default delete_config_statuts"><i class="fa fa-trash"></i> Delete</a>');
            var filterInp = $("#table-filter");
            if (filterInp.length > 0) {
                var tableFilter = $("div.table-filter");
                tableFilter.html(filterInp.html());

                if ($(".table-filter .dpo").length > 0) {
                    $('.table-filter .dpo[data-name="date_from"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            minDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                    $('.table-filter .dpo[data-name="date_to"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            maxDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                }
            }

            this.api().columns().every(function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                .appendTo($(column.footer()).empty())
                .on('change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                        );

                    column
                    .search(val ? '^' + val + '$' : '', true, false)
                    .draw();
                });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });

        },
        "drawCallback": function () {
            $('.dataTables_paginate .paginate_button').addClass('btn btn-default');
        }
    });
  })
</script>


