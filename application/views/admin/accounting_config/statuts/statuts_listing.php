
  <div class="module-body table-responsive accounting_config_index">
    <table id="statuts_config" class="cell-border listing" cellspacing="0" width="100%" data-selected_id="">
      <thead>
        <tr>
          <th class="no-sort text-center">#</th>
          <th class="column-id"><?php echo $this->lang->line('id');?></th>
          <th class="column-date">Date</th>
          <th class="column-date">Time</th>
          <th class="column-date">Added by</th>
          <th>From Deparment</th>
          <th>Statut</th>
          <th>Statut</th>
          <th>Since</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($acc_statuts as $key => $value){ ?>
          <?php
              $timestamp=$value['created_at'];
              $date = new DateTime($timestamp);
              $date = $date->format('d/m/Y');
              $time = new DateTime($timestamp);
              $time = $time->format('H:i:s');
          ?>
        <tr>
          <input type="hidden" class="statuts_acc_config_id" value="<?php echo $value['id'];?>">
          <input type="hidden" class="statuts_acc_config" value="<?php echo $value['statuts'];?>">
          <input type="hidden" class="status_acc_config" value="<?php echo $value['status'];?>">
          <td class="row_<?=$value['id'];?>"><input type="checkbox" value="<?=$value['id'];?>" data-id="<?=$value['id'];?>" class="checkbox checkboxx singleSelect"></td>
            <td>
                <a class="view_statuts" onclick="view_statuts_account('<?= $value['id']; ?>')">
                    <?=create_timestamp_uid($value['created_at'],$value['id']);?>
                </a>
            </td>
            <td><?=$date ?></td>
            <td><?= $time ?></td>
            <td><?= $value['civility'].' '.$value['first_name'].' '.$value['last_name']; ?></td>
            <td><?= ucwords($value['department_name']); ?></td>
            <td><?= ucwords($value['statuts']); ?></td>
            <td><?=$value['status']=='enabled'?'<span class="label label-success">Enabled</span>':'<span class="label label-danger">Disabled</span>';?></td>
            <td style="white-space: nowrap;"><?=timeDiff($value['created_at']);?></td>
          </tr>
      <?php } ?>  
      </tbody>
    </table>
  </div>
<div id="table-filter" class="hide">
  <input type="text" placeholder="Search" data-name="short_code" class="form-control">
<!--   <a href="#" class="btn btn-sm btn-default " onclick="setUpdateAction();"><i class="fa fa-search"></i> Search</a>
 --></div>

<?php $this->load->view('admin/accounting_config/statuts/statuts_js');?>