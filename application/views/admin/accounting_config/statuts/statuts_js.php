<script type="text/javascript">
	$(document).ready(function(){
        $('body').on('click','.add_config_statuts',function(){
            $('.accounting_config_index').css('display','none');
            $('.accounting_config_edit').css('display','none');
            $('.accounting_config_view').css('display','none');
            $('.accounting_config_add').css('display','block');
        });
        $('body').on('click','.back_to_listing',function(){
            $('.accounting_config_add').css('display','none');
            $('.accounting_config_edit').css('display','none');
            $('.accounting_config_view').css('display','none');
            $('.accounting_config_index').css('display','block');
        });
    })
	$(document.body).on("click", ".edit_config_statuts", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
        	var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr')
            	status = row.find('input.status_acc_config').val()
            	statuts = row.find('input.statuts_acc_config').val();
            $('#id_acc_config').val(checkBoxVal);
            $('#status_acc_config').val(status);
            $('#statuts_acc_config').val(statuts);
        	$('.accounting_config_index').css('display','none');
            $('.accounting_config_add').css('display','none');
            $('.accounting_config_view').css('display','none');
            $('.accounting_config_edit').css('display','block');
        } else {
            alert("Select atleast one record");
        }
    });
	function view_statuts_account(id){
		var row = $('td.row_'+id).closest('tr')
            status = row.find('input.status_acc_config').val()
            statuts = row.find('input.statuts_acc_config').val();
        $('#status_acc_config_view').val(status);
        $('#statuts_acc_config_view').val(statuts)
        $('.accounting_config_index').css('display','none');
        $('.accounting_config_add').css('display','none');
        $('.accounting_config_edit').css('display','none');
        $('.accounting_config_view').css('display','block');
	}
    $(document.body).on("click", ".delete_config_statuts", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr');
            var id = checkBoxVal;
            if(confirm('Are You sure to delete ?')){
                row.remove();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('admin/accounting_config/delete_config_record'); ?>",
                    data: {
                    'id': id,
                    'table':'vbs_accounting_statuts',
                    },
                    dataType:'json',
                    success:function(res){
                        alert(res);
                        row.remove();
                    }
                }) 
            }
        } else {
            alert("Select atleast one record");
        }
    });
</script>