<?php 
	
	$this->load->view('admin/accounting_config/statuts/statuts_grid_js');
	$this->load->view('admin/accounting_config/vat/vat_grid_js');
	$this->load->view('admin/accounting_config/payment_methodes/payment_methodes_grid_js');
	$this->load->view('admin/accounting_config/payment_delays/payment_delays_grid_js');
	$this->load->view('admin/accounting_config/bill_category/bill_category_grid_js');
	$this->load->view('admin/accounting_config/sales_category/sales_category_grid_js');
	$this->load->view('admin/accounting_config/client_kind/client_kind_grid_js');
	$this->load->view('admin/accounting_config/clients_category/clients_category_grid_js');
	$this->load->view('admin/accounting_config/suppliers_category/supplier_category_grid_js');
	$this->load->view('admin/accounting_config/suppliers/suppliers_grid_js');
	$this->load->view('admin/accounting_config/organization_category/organization_category_grid_js');
	$this->load->view('admin/accounting_config/organization/organization_grid_js');
	$this->load->view('admin/accounting_config/banks/banks_grid_js');
	$this->load->view('admin/accounting_config/factors/factors_grid_js');
	$this->load->view('admin/accounting_config/financement_delay/financement_delay_grid_js');
	$this->load->view('admin/accounting_config/garantee_fund/garantee_fund_grid_js');

?>