<div class="tab-content responsive" style="width:100%;display: table;">
<?=form_open()?>
  <div class="driverStatusAdd supplier_view" style="display: none;" >
    <div class="col-md-12">  
      <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;">Statut</span>
          <select class="form-control custom_disabled custom_disabled" name="statut" id="statut_supplier_view" disabled>
            <option value="enabled">Enabled</option>
            <option value="disabled">Disabled</option>
          </select>
        </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Supplier Category</span>
            <select class="form-control custom_disabled" id="supplier_category_id_view" disabled>
              <?php foreach($supplier_category as $key => $value){?>
                <option value="<?= $value['id']; ?>"><?= $value['supplier_category']; ?></option>
              <?php } ?>
            </select>
          </div>
      </div>   
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Supplier Name</span>
            <input type="text" class="form-control custom_disabled" name="supplier_name" id="supplier_name_view" disabled>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">phone</span>
            <input type="text" class="form-control custom_disabled" name="phone" id="phone_view" disabled>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">email</span>
            <input type="text" class="form-control custom_disabled" name="email" id="email_view" disabled>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Contact Name</span>
            <input type="text" class="form-control custom_disabled" id="sup_contact_name_view" disabled>
          </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Payment Delay</span>
            <select class="form-control custom_disabled" id="Payment_delay_id_sup_view" disabled>
              <?php foreach($payment_delay as $key => $value){ ?>
                <option value="<?php echo $value['id']; ?>"><?php echo $value['payment_delay']; ?></option>
              <?php } ?>  
            </select>
          </div>
      </div>
      <div class="col-md-4" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">address</span>
            <input type="text" class="form-control custom_disabled" id="address_view"  disabled>
          </div>
      </div>
      <div class="col-md-4" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">address</span>
            <input type="text" class="form-control custom_disabled" id="address2_view" disabled>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Zip Code</span>
            <input type="text" class="form-control custom_disabled" id="sup_zip_code_view" disabled>
          </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="col-md-6"></div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">City</span>
            <input type="text" class="form-control custom_disabled" id="sup_city_view" disabled>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Country</span>
            <input type="text" class="form-control custom_disabled" id="sup_country_view" disabled>
          </div>
      </div>
    </div>
    <div class="col-md-12">  
        <button type="button" class="btn btn-default back_to_listing" style="float:right; margin-left:7px;"><span class="fa fa-close"> Cancel </span></button>
    </div>      
  </div>
<?php echo form_close(); ?>
</div>

