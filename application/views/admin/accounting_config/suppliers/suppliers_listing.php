  <div class="module-body table-responsive supplier_index">
    <table id="supplier_grid" class="cell-border listing" cellspacing="0" width="100%" data-selected_id="">
      <thead>
        <tr>
          <th class="no-sort text-center">#</th>
          <th class="column-id"><?php echo $this->lang->line('id');?></th>
          <th class="column-date">Date</th>
          <th class="column-date">Time</th>
          <th class="column-date">Added by</th>
          <th>From Deparment</th>
          <th>Supplier Category</th>
          <th>Supplier Name</th>
          <th>Phone</th>
          <th>email</th>
          <th>address</th>
          <th>Contact Name</th>
          <th>Payment Delay</th>
          <th>Statut</th>
          <th>Since</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($supplier as $key => $value){ ?>
          <?php
              $timestamp=$value['created_at'];
              $date = new DateTime($timestamp);
              $date = $date->format('d/m/Y');
              $time = new DateTime($timestamp);
              $time = $time->format('H:i:s');
          ?>
        <tr>
          <input type="hidden" class="supplier_statut" value="<?php echo $value['statut'];?>">
          <input type="hidden" class="supplier_category_id" value="<?php echo $value['supplier_category_id'];?>">
          <input type="hidden" class="supplier_name" value="<?php echo $value['supplier_name'];?>">
          <input type="hidden" class="phone" value="<?php echo $value['phone'];?>">
          <input type="hidden" class="email" value="<?php echo $value['email'];?>">
          <input type="hidden" class="sup_address" value="<?php echo $value['address'];?>">
          <input type="hidden" class="sup_contact_name" value="<?php echo $value['sup_contact_name'];?>">
          <input type="hidden" class="payment_delay_id_sup" value="<?php echo $value['payment_delay_id'];?>">
          <input type="hidden" class="sup_address2" value="<?php echo $value['address2'];?>">
          <input type="hidden" class="sup_zip_code" value="<?php echo $value['sup_zip_code'];?>">
          <input type="hidden" class="sup_city" value="<?php echo $value['sup_city'];?>">
          <input type="hidden" class="sup_country" value="<?php echo $value['sup_country'];?>">
          <td class="row_<?=$value['id'];?>"><input type="checkbox" value="<?=$value['id'];?>" data-id="<?=$value['id'];?>" class="checkbox checkboxx singleSelect"></td>
            <td>
                <a class="view_statuts" onclick="view_supplier('<?= $value['id']; ?>')">
                    <?=create_timestamp_uid($value['created_at'],$value['id']);?>
                </a>
            </td>
            <td><?=$date ?></td>
            <td><?= $time ?></td>
            <td><?= $value['civility'].' '.$value['first_name'].' '.$value['last_name']; ?></td>
            <td><?= ucwords($value['department_name']); ?></td>
            <td><?= ucwords($value['supplier_category']); ?></td>
            <td><?= ucwords($value['supplier_name']); ?></td>
            <td><?= ucwords($value['phone']); ?></td>
            <td><?= ucwords($value['email']); ?></td>
            <td><?= ucwords($value['address']); ?></td>
            <td><?= ucwords($value['sup_contact_name']); ?></td>
            <td><?= ucwords($value['payment_delay']); ?></td>
            <td><?=$value['statut']=='enabled'?'<span class="label label-success">Enabled</span>':'<span class="label label-danger">Disabled</span>';?></td>
            <td style="white-space: nowrap;"><?=timeDiff($value['created_at']);?></td>
          </tr>
      <?php } ?>  
      </tbody>
    </table>
  </div>
<div id="table-filter" class="hide">
  <input type="text" placeholder="Search" data-name="short_code" class="form-control">
</div>

<?php $this->load->view('admin/accounting_config/suppliers/suppliers_js');?>