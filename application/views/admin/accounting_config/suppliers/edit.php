<div class="tab-content responsive" style="width:100%;display: table;">
<?=form_open("admin/accounting_config/accounting_supplier_update")?>
  <div class="driverStatusAdd supplier_edit" style="display: none;" >
    <input type="hidden" name="id" id="id_supplier">
    <div class="col-md-12">  
      <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;">Statut</span>
          <select class="form-control" name="statut" id="statut_supplier">
            <option value="enabled">Enabled</option>
            <option value="disabled">Disabled</option>
          </select>
        </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Supplier Category</span>
            <select class="form-control" name="supplier_category_id" id="supplier_category_id">
              <?php foreach($supplier_category as $key => $value){?>
                <option value="<?= $value['id']; ?>"><?= $value['supplier_category']; ?></option>
              <?php } ?>
            </select>
          </div>
      </div>  
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Supplier Name</span>
            <input type="text" class="form-control" name="supplier_name" id="supplier_name" required>
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">phone</span>
            <input type="text" class="form-control" name="phone" id="phone">
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">email</span>
            <input type="text" class="form-control" name="email" id="email">
          </div>
      </div>
     <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Contact Name</span>
            <input type="text" class="form-control" name="sup_contact_name" id="sup_contact_name">
          </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Payment Delay</span>
            <select class="form-control" name="payment_delay_id" id="payment_delay_id_sup">
              <?php foreach($payment_delay as $key => $value){ ?>
                <option value="<?php echo $value['id']; ?>"><?php echo $value['payment_delay']; ?></option>
              <?php } ?>  
            </select>
          </div>
      </div>
      <div class="col-md-4" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">address</span>
            <input type="text" class="form-control" name="address" id="sup_address" placeholder="Address line 1">
          </div>
      </div>
      <div class="col-md-4" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">address</span>
            <input type="text" class="form-control" name="address2" id="sup_address2" placeholder="Address line 1">
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Zip Code</span>
            <input type="text" class="form-control" name="sup_zip_code" id="sup_zip_code">
          </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="col-md-6"></div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">City</span>
            <input type="text" class="form-control" name="sup_city" id="sup_city">
          </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
          <div class="form-group">
            <span style="font-weight: bold;">Country</span>
            <input type="text" class="form-control" name="sup_country" id="sup_country">
          </div>
      </div>
    </div>
    <div class="col-md-12">  
        <button type="submit" class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
        <button type="button" class="btn btn-default back_to_listing" style="float:right; margin-left:7px;"><span class="fa fa-close"> Cancel </span></button>
    </div>      
  </div>
<?php echo form_close(); ?>
</div>

