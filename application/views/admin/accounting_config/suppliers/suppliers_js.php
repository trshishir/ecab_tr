<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click','.add_supplier',function(){
            $('.supplier_index').css('display','none');
            $('.supplier_edit').css('display','none');
            $('.supplier_view').css('display','none');
            $('.supplier_add').css('display','block');
        });
        $('body').on('click','.back_to_listing',function(){
            $('.supplier_add').css('display','none');
            $('.supplier_edit').css('display','none');
            $('.supplier_view').css('display','none');
            $('.supplier_index').css('display','block');
        });
    })
    $(document.body).on("click", ".edit_supplier", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr')
                statut = row.find('input.supplier_statut').val()
                supplier_category_id = row.find('input.supplier_category_id').val();
                supplier_name = row.find('input.supplier_name').val();
                phone = row.find('input.phone').val();
                email = row.find('input.email').val();
                address = row.find('input.sup_address').val();
                sup_contact_name = row.find('input.sup_contact_name').val();
                payment_delay_sup = row.find('input.payment_delay_id_sup').val();
                sup_address2 = row.find('input.sup_address2').val();
                sup_zip_code = row.find('input.sup_zip_code').val();
                sup_city = row.find('input.sup_city').val();
                sup_country = row.find('input.sup_country').val();
            $('#id_supplier').val(checkBoxVal);
            $('#supplier_statut').val(statut);
            $('#supplier_category_id').val(supplier_category_id);
            $('#supplier_name').val(supplier_name);
            $('#phone').val(phone);
            $('#email').val(email);
            $('#sup_address').val(address);
            $('#sup_contact_name').val(sup_contact_name);
            $('#payment_delay_id_sup').val(payment_delay_sup);
            $('#sup_address2').val(sup_address2);
            $('#sup_zip_code').val(sup_zip_code);
            $('#sup_city').val(sup_city);
            $('#sup_country').val(sup_country);
            $('.supplier_index').css('display','none');
            $('.supplier_add').css('display','none');
            $('.supplier_view').css('display','none');
            $('.supplier_edit').css('display','block');
        } else {
            alert("Select atleast one record");
        }
    });
    function view_supplier(id){
        var row = $('td.row_'+id).closest('tr')
            statut = row.find('input.supplier_statut').val()
            supplier_category_id = row.find('input.supplier_category_id').val();
            supplier_name = row.find('input.supplier_name').val();
            phone = row.find('input.phone').val();
            email = row.find('input.email').val();
            address = row.find('input.sup_address').val();
            sup_contact_name = row.find('input.sup_contact_name').val();
            payment_delay_sup = row.find('input.payment_delay_id_sup').val();
            sup_address2 = row.find('input.sup_address2').val();
            sup_zip_code = row.find('input.sup_zip_code').val();
            sup_city = row.find('input.sup_city').val();
            sup_country = row.find('input.sup_country').val();
        $('#supplier_statut_view').val(statut);
        $('#supplier_category_id_view').val(supplier_category_id);
        $('#supplier_name_view').val(supplier_name);
        $('#phone_view').val(phone);
        $('#email_view').val(email);
        $('#address_view').val(address);
        $('#sup_contact_name_view').val(sup_contact_name);
        $('#payment_delay_id_sup').val(payment_delay_sup);
        $('#address2_view').val(sup_address2);
        $('#sup_zip_code_view').val(sup_zip_code);
        $('#sup_city_view').val(sup_city);
        $('#sup_country_view').val(sup_country);
        $('.supplier_index').css('display','none');
        $('.supplier_add').css('display','none');
        $('.supplier_edit').css('display','none');
        $('.supplier_view').css('display','block');
    }
    $(document.body).on("click", ".delete_suppliers", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr');
            var id = checkBoxVal;
            if(confirm('Are You sure to delete ?')){
                row.remove();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('admin/accounting_config/delete_config_record'); ?>",
                    data: {
                    'id': id,
                    'table':'vbs_account_supplier',
                    },
                    dataType:'json',
                    success:function(res){
                        alert(res);
                        row.remove();
                    }
                }) 
            }
        } else {
            alert("Select atleast one record");
        }
    });
</script>