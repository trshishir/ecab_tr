<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click','.add_bank',function(){
            $('.banks_index').css('display','none');
            $('.banks_edit').css('display','none');
            $('.banks_view').css('display','none');
            $('.banks_add').css('display','block');
        });
        $('body').on('click','.back_to_listing',function(){
            $('.banks_add').css('display','none');
            $('.banks_edit').css('display','none');
            $('.banks_view').css('display','none');
            $('.banks_index').css('display','block');
        });
    })
    $(document.body).on("click", ".edit_bank", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr')
                statut = row.find('input.banks_statut').val()
                banks_name = row.find('input.banks_name').val();
                banks_phone = row.find('input.banks_phone').val();
                banks_fax = row.find('input.banks_fax').val();
                banks_email = row.find('input.banks_email').val();
                banks_address = row.find('input.banks_address').val();
                banks_address2 = row.find('input.banks_address2').val();
                banks_contact_name = row.find('input.banks_contact_name').val();
                payment_delay_id = row.find('input.payment_delay_id').val();
                banks_zip_code = row.find('input.banks_zip_code').val();
                banks_city = row.find('input.banks_city').val();
                banks_country = row.find('input.banks_country').val();
                banks_reference_number = row.find('input.banks_reference_number').val();
            $('#id_bank').val(checkBoxVal);
            $('#banks_statut').val(statut);
            $('#banks_name').val(banks_name);
            $('#banks_phone').val(banks_phone);
            $('#banks_fax').val(banks_fax);
            $('#banks_email').val(banks_email);
            $('#banks_address').val(banks_address);
            $('#banks_address2').val(banks_address2);
            $('#banks_contact_name').val(banks_contact_name);
            $('#banks_reference_number').val(banks_reference_number);
            $('#payment_delay_id').val(payment_delay_id);
            $('#banks_zip_code').val(banks_zip_code);
            $('#banks_city').val(banks_city);
            $('#banks_country').val(banks_country);
            $('.banks_index').css('display','none');
            $('.banks_add').css('display','none');
            $('.banks_view').css('display','none');
            $('.banks_edit').css('display','block');
        } else {
            alert("Select atleast one record");
        }
    });
    function view_bank(id){
        var row = $('td.row_'+id).closest('tr')
            statut = row.find('input.banks_statut').val()
                banks_name = row.find('input.banks_name').val();
                banks_phone = row.find('input.banks_phone').val();
                banks_fax = row.find('input.banks_fax').val();
                banks_email = row.find('input.banks_email').val();
                banks_address = row.find('input.banks_address').val();
                banks_address2 = row.find('input.banks_address2').val();
                banks_contact_name = row.find('input.banks_contact_name').val();
                payment_delay_id = row.find('input.payment_delay_id').val();
                banks_zip_code = row.find('input.banks_zip_code').val();
                banks_city = row.find('input.banks_city').val();
                banks_country = row.find('input.banks_country').val();
                banks_reference_number = row.find('input.banks_reference_number').val();
            $('#banks_statut_view').val(statut);
            $('#banks_name_view').val(banks_name);
            $('#banks_phone_view').val(banks_phone);
            $('#banks_fax_view').val(banks_fax);
            $('#banks_email_view').val(banks_email);
            $('#banks_address_view').val(banks_address);
            $('#banks_address_view2').val(banks_address2);
            $('#banks_contact_name_view').val(banks_contact_name);
            $('#banks_reference_number_view').val(banks_reference_number);
            $('#payment_delay_id_view').val(payment_delay_id);
            $('#banks_zip_code_view').val(banks_zip_code);
            $('#banks_city_view').val(banks_city);
            $('#banks_country_view').val(banks_country);
        $('.banks_index').css('display','none');
        $('.banks_add').css('display','none');
        $('.banks_edit').css('display','none');
        $('.banks_view').css('display','block');
    }
    $(document.body).on("click", ".delete_banks", function () {
        var checkbox = $('.checkboxx:checked');
        if (checkbox.length > 0) {
            var checkBoxVal = [];
            $(checkbox).each(function() {
                var val = $(this).data('id');
                checkBoxVal.push(val);
            });
            var row = $('td.row_'+checkBoxVal).closest('tr');
            var id = checkBoxVal;
            if(confirm('Are You sure to delete ?')){
                row.remove();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('admin/accounting_config/delete_config_record'); ?>",
                    data: {
                    'id': id,
                    'table':'vbs_account_banks',
                    },
                    dataType:'json',
                    success:function(res){
                        alert(res);
                        row.remove();
                    }
                }) 
            }
        } else {
            alert("Select atleast one record");
        }
    });
</script>