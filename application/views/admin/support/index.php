<?php $locale_info = localeconv(); ?>
<style>
    @media only screen and (min-width: 1400px){
        .table-filter input, .table-filter select{
            max-width: 9% !important;
        }
        .table-filter select{
            max-width: 95px !important;
        }
        .table-filter .dpo {
            max-width: 90px !important;
        }
    }
</style>
<div class="col-md-12"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12">
                <?php
                $flashAlert =  $this->session->flashdata('alert');
                if(isset($flashAlert['message']) && !empty($flashAlert['message'])){?>
                    <br>
                    <div style="padding: 5px 12px" class="alert <?=$flashAlert['class']?>">
                        <strong><?=$flashAlert['type']?></strong> <?=$flashAlert['message']?>
                        <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>
                <div class="module">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="module-head">
                        <!--<h3> <?php if(isset($title)) echo $title;?></h3>-->
                    </div>
                    <div class="module-body table-responsive">
                        <table id="example" class="cell-border" cellspacing="0" width="100%" data-selected_id="">
                            <thead>
                            <tr>
                                <th class="no-sort text-center">#</th>
                                <th><?php echo $this->lang->line('id');?></th>
                                <th><?php echo $this->lang->line('date');?></th>
                                <th><?php echo $this->lang->line('time');?></th>
                                <th><?php echo $this->lang->line('civility');?></th>
                                <th><?php echo $this->lang->line('first_name');?></th>
                                <th><?php echo $this->lang->line('last_name');?></th>
                                <th><?php echo $this->lang->line('email');?></th>
                                <th><?php echo $this->lang->line('phone');?></th>
                                <th><?php echo $this->lang->line('mobile');?></th>
                                <th><?php echo $this->lang->line('department');?></th>
                                <th><?php echo $this->lang->line('priority');?></th>
                                <th><?php echo $this->lang->line('subject');?></th>
                                <th><?php echo $this->lang->line('message');?></th>
                                <th><?php echo $this->lang->line('attachments');?></th>
                                <th><?php echo $this->lang->line('status');?></th>
                                <th><?php echo $this->lang->line('since');?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(isset($data) && !empty($data)):?>
                                <?php foreach($data as $key => $item):?>
                                    <?php
                                    $timestamp = strtotime($item->created_on);
                                    $expiry_time = new DateTime($item->modified_on);
                                    $current_date = new DateTime();
                                    $diff = $expiry_time->diff($current_date);
									//$attachments = $obj->GetSupportAttachments($item->id);
                                    ?>
                                    <tr <?php if($item->unread == 1){ ?> class="unread" <?php }?>>
                                        <td>
                                            <input type="checkbox" data-id="<?=$item->id;?>" class="checkbox singleSelect">
                                        </td>
                                        <td>
                                            <a href="<?=site_url("admin/supports/".$item->id."/edit")?>">
                                                <?=create_timestamp_uid($item->created_on,$item->id);?>
                                            </a>
                                        </td>
                                        <td><?=date("d/m/Y", $timestamp)?></td>
                                        <td><?=date("H:i:s", $timestamp)?></td>
                                        <td><?=$item->p_title;?></td>
                                        <td><?=$item->fname;?></td>
                                        <td><?=$item->lname;?></td>
                                        <td><?=$item->email?></td>
                                        <td><?=$item->phone?></td>
                                        <td><?=$item->mobile?></td>
                                        <td>
											<?php
												switch ($item->department){
													case 10:
														echo 'Booking service';
														break;
													case 11:
														echo 'Clients Service';
														break;
													case 12:
														echo 'Driver Service';
														break;
													case 13:
														echo 'Accounting Service';
														break;
													case 14:
														echo 'Sales Department';
														break;
													case 15:
														echo 'Technical Service';
														break;
													case 16:
														echo 'Disclaimer Service';
														break;
													default:
														echo 'N/A';
												}
											?>
										</td>
                                        <td><?=$item->priority?></td>
                                        <td><?=substr($item->subject, 0, 10).'...';?></td>
                                        <td><?=substr($item->message, 0, 10).'...';?></td>
                                        <td>
											<?php if(!empty($attachments)){ ?>
											<?php foreach($attachments as $k=>$v){ ?>
											<a target="_blank" href="<?php echo 'http://ecab.app/uploads/contact_files/'.$v->filename; ?>"><?php echo $v->filename; ?></a><br/>
											<?php } ?>
											<?php } ?>
										</td>
                                        <td><?=$item->status?></td>
                                        <td><?=$diff->format('%D Days %H Hours %I Minutes');?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <br>
                    </div>
                </div>
            </div>
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<div id="table-filter" class="hide">
    <input type="text" placeholder="Name" class="form-control">
    <select class="form-control">
        <option>All Subject</option>
        <option>All Departments</option>
    </select>
    <input type="text" placeholder="Email" class="form-control">
    <input type="text" placeholder="Phone" class="form-control">
    <input type="text" placeholder="From" class="dpo">
    <input type="text" placeholder="To" class="dpo">
    <select class="form-control">
        <option>All Status</option>
    </select>
</div>