<?php $locale_info = localeconv(); ?>
<link href="<?php echo base_url(); ?>assets/system_design/css/simple-rating.css" rel="stylesheet">
<div class="col-md-12"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12">
                <?php $this->load->view('admin/common/alert');?>
                <div class="module">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="module-head">
                    </div>
                    <div class="module-body">
                        <?=form_open_multipart("admin/notifications/add")?>
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label>Notification Statut*</label>
                                    <select name="notification_status" id="notification_status" class="form-control" required >
                                        <option value="">---Select---</option>
                                        <option value="1">Enabled</option>
                                        <option value="2">Disabled</option>
                                    </select>
                                </div>
                            </div>
                          
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label>Module*</label>
                                    <select name="department" id="department" class="form-control" required onchange="modulechange()">
                                        <option value="">---Select---</option>
                                        <option value="1">Job Applications</option>
                                        <option value="2">Quotes</option>
                                        <option value="3">Calls</option>
                                        <option value="4">Invoices</option>
                                        <option value="5">Support</option>
                                        <option value="6">Booking</option>
                                        <option value="7">Drivers</option>
                                        <option value="8">Clients</option>
                                        <option value="9">Cars</option>
                                        <option value="10">Partners</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label>Statut*</label>
                                    <select name="status" id="status" class="form-control" required >
                                        <option value="">---Select---</option>
                                    </select>
                                </div>
                            </div>
                              <div class="col-xs-3">
                                <div class="form-group">
                                    <label>Name*</label>
                                    <input type="text" maxlength="100" class="form-control" required name="name" placeholder="Name*" value="<?=set_value('name',$this->input->post('name'))?>">
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                              <div class="col-xs-12"><label>Subject*</label></div>
                            <div class="col-xs-12" style="padding:0px;">
                            <div class="col-xs-6">
                               <div class="form-group">
                                  
                                    <input type="text" class="form-control" id="subject" required name="subject" placeholder="Subject*" value="">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label></label>
                                    <input type="checkbox" class="" id="send_copy_to_department_operator" style="margin: 17px 10px 11px 3px !important;"> <span style="display: inline-block;padding-top: 11px;" >Send copy to department operator</span>
                                    <input type="hidden" name="send_copy_to_department_operator" value="0">
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea rows="4" class="form-control message"  name="message" placeholder="Message"><?=set_value('message',$this->input->post('message'))?></textarea>
                                    <script>
                                        CKEDITOR.replace("message", {
                                            customConfig: "<?=base_url("assets/system_design/config.js")?>"
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group" id="userfullshortcode">
                                   
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-right">
                                     <a href="<?=base_url("admin/notifications")?>" class="btn btn-default"><span class="fa fa-close" style="margin-right: 3px;"></span>Cancel</a>
                                    <button class="btn btn-default"><span class="fa fa-save"></span>Save</button>
                                   
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/simple-rating.js"></script>

<script type="text/javascript">
var job="<p>Reply :&nbsp;{last_job_notification_user_reply}</p><hr /><p>Sent from :&nbsp;{job_notification_sender_email}</p><p>Date :&nbsp;{job_notification_date}, Time :&nbsp;{job_notification_time}</p><p>Sender Name :&nbsp;{job_notification_civility}&nbsp;{job_notification_first_name}&nbsp;{job_notification_last_name} Company :&nbsp;{job_notification_company_name}</p><p>Subject :&nbsp;{job_notification_subject}</p><p>Message :&nbsp;{job_notification_message}</p><hr />";
var quote="<p>Dear &nbsp;{client_civility}&nbsp;{client_firstname}&nbsp;{client_lastname} </p><p>we contact you about your quote {quote_id}done on {quote_date}</p>";
var call="<p>Reply :&nbsp;{last_call_notification_user_reply}</p><hr /><p>Sent from :&nbsp;{call_notification_sender_email}</p><p>Date :&nbsp;{call_notification_date}, Time :&nbsp;{call_notification_time}</p><p>Sender Name :&nbsp;{call_notification_civility}&nbsp;{call_notification_first_name}&nbsp;{call_notification_last_name} Company :&nbsp;{call_notification_company_name}</p><p>Subject :&nbsp;{call_notification_subject}</p><p>Message :&nbsp;{call_notification_message}</p><hr />";
var invoice="<p>Dear &nbsp;{client_civility}&nbsp;{client_firstname}&nbsp;{client_lastname} </p><p>we contact you about your invoice {invoice_id}done on {invoice_date}</p>";
var support="<p>Reply :&nbsp;{last_support_notification_user_reply}</p><hr /><p>Sent from :&nbsp;{support_notification_sender_email}</p><p>Date :&nbsp;{support_notification_date}, Time :&nbsp;{support_notification_time}</p><p>Sender Name :&nbsp;{support_notification_civility}&nbsp;{support_notification_first_name}&nbsp;{support_notification_last_name} Company :&nbsp;{support_notification_company_name}</p><p>Subject :&nbsp;{support_notification_subject}</p><p>Message :&nbsp;{support_notification_message}</p><hr />";
var booking="<p>Dear &nbsp;{client_civility}&nbsp;{client_firstname}&nbsp;{client_lastname} </p><p>we contact you about your booking {booking_id}done on {booking_date}</p>";
var driver="<p>Dear &nbsp;{user_civility}&nbsp;{user_firstname}&nbsp;{user_lastname} </p><p>we contact you about your  {driver_column_name} expired on {driver_column_expire_date}</p>";
var client="<p>Dear &nbsp;{user_civility}&nbsp;{user_firstname}&nbsp;{user_lastname} </p><p>we contact you about your informations. </p>";

var car="<p>Dear &nbsp;{user_civility}&nbsp;{user_firstname}&nbsp;{user_lastname} </p><p>we contact you about your car</p>";
var partner="<p>Dear &nbsp;{user_civility}&nbsp;{user_firstname}&nbsp;{user_lastname} </p><p>we contact you about your  {partner_column_name} expired on {partner_column_expire_date}</p>";

//usefull short code start section
var usecodejob='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{sender_name}\')" >{sender_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_name}\')" >{user_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_notification_status}\')" >{job_notification_status}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_notification_subject}\')" >{job_notification_subject}</a></span><span><a href="javascript:void();" onclick="appendText(\'{last_job_notification_user_reply}\')" >{last_job_notification_user_reply}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_notification_sender_email}\')" >{job_notification_sender_email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_notification_date}\')" >{job_notification_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_notification_time}\')" >{job_notification_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_notification_civility}\')" >{job_notification_civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_notification_first_name}\')" >{job_notification_first_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_notification_last_name}\')" >{job_notification_last_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_notification_company_name}\')" >{job_notification_company_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{job_notification_message}\')" >{job_notification_message}</a></span>';
var usecodequote='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{quote_id}\')" >{quote_id}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_date}\')" >{quote_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_notification_status}\')" >{quote_notification_status}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_notification_subject}\')" >{quote_notification_subject}</a></span><span><a href="javascript:void();" onclick="appendText(\'{last_quote_notification_user_reply}\')" >{last_quote_notification_user_reply}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_notification_sender_email}\')" >{quote_notification_sender_email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_notification_date}\')" >{quote_notification_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_notification_time}\')" >{quote_notification_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_civility}\')" >{client_civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_firstname}\')" >{client_firstname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_lastname}\')" >{client_lastname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_notification_company_name}\')" >{quote_notification_company_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{quote_notification_message}\')" >{quote_notification_message}</a></span>';
var usecodecall='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{sender_name}\')" >{sender_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_name}\')" >{user_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_notification_status}\')" >{call_notification_status}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_notification_subject}\')" >{call_notification_subject}</a></span><span><a href="javascript:void();" onclick="appendText(\'{last_call_notification_user_reply}\')" >{last_call_notification_user_reply}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_notification_sender_email}\')" >{call_notification_sender_email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_notification_date}\')" >{call_notification_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_notification_time}\')" >{call_notification_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_notification_civility}\')" >{call_notification_civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_notification_first_name}\')" >{call_notification_first_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_notification_last_name}\')" >{call_notification_last_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_notification_company_name}\')" >{call_notification_company_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{call_notification_message}\')" >{call_notification_message}</a></span>';
var usecodeinvoice='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{invoice_id}\')" >{invoice_id}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_date}\')" >{invoice_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_notification_status}\')" >{invoice_notification_status}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_notification_subject}\')" >{invoice_notification_subject}</a></span><span><a href="javascript:void();" onclick="appendText(\'{last_invoice_notification_user_reply}\')" >{last_invoice_notification_user_reply}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_notification_sender_email}\')" >{invoice_notification_sender_email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_notification_date}\')" >{invoice_notification_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_notification_time}\')" >{invoice_notification_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_civility}\')" >{client_civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_firstname}\')" >{client_firstname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_lastname}\')" >{client_lastname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_notification_company_name}\')" >{invoice_notification_company_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{invoice_notification_message}\')" >{invoice_notification_message}</a></span>';
var usecodesupport='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{sender_name}\')" >{sender_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_name}\')" >{user_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_notification_status}\')" >{support_notification_status}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_notification_subject}\')" >{support_notification_subject}</a></span><span><a href="javascript:void();" onclick="appendText(\'{last_support_notification_user_reply}\')" >{last_support_notification_user_reply}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_notification_sender_email}\')" >{support_notification_sender_email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_notification_date}\')" >{support_notification_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_notification_time}\')" >{support_notification_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_notification_civility}\')" >{support_notification_civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_notification_first_name}\')" >{support_notification_first_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_notification_last_name}\')" >{support_notification_last_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_notification_company_name}\')" >{support_notification_company_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{support_notification_message}\')" >{support_notification_message}</a></span>';
var usecodebooking='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{booking_id}\')" >{booking_id}</a></span><span><a href="javascript:void();" onclick="appendText(\'{booking_date}\')" >{booking_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{booking_notification_status}\')" >{booking_notification_status}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{booking_notification_subject}\')" >{booking_notification_subject}</a></span><span><a href="javascript:void();" onclick="appendText(\'{last_booking_notification_user_reply}\')" >{last_booking_notification_user_reply}</a></span><span><a href="javascript:void();" onclick="appendText(\'{booking_notification_sender_email}\')" >{booking_notification_sender_email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{booking_notification_date}\')" >{booking_notification_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{booking_notification_time}\')" >{booking_notification_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_civility}\')" >{client_civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_firstname}\')" >{client_firstname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_lastname}\')" >{client_lastname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{booking_notification_company_name}\')" >{booking_notification_company_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{booking_notification_message}\')" >{booking_notification_message}</a></span>';
var usecodedriver='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{driver_id}\')" >{driver_id}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_date}\')" >{driver_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_notification_status}\')" >{driver_notification_status}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_notification_subject}\')" >{driver_notification_subject}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_column_name}\')" >{driver_column_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_notification_sender_email}\')" >{driver_notification_sender_email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_notification_date}\')" >{driver_notification_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_notification_time}\')" >{driver_notification_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_civility}\')" >{user_civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_firstname}\')" >{user_firstname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_lastname}\')" >{user_lastname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{company_name}\')" >{company_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_notification_message}\')" >{driver_notification_message}</a></span><span><a href="javascript:void();" onclick="appendText(\'{driver_column_expire_date}\')" >{driver_column_expire_date}</a></span>';

var usecodeclient='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{client_id}\')" >{client_id}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_date}\')" >{client_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{client_time}\')" >{client_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{company}\')" >{company}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{email}\')" >{email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{phone}\')" >{phone}</a></span><span><a href="javascript:void();" onclick="appendText(\'{mobile}\')" >{mobile}</a></span><span><a href="javascript:void();" onclick="appendText(\'{fax}\')" >{fax}</a></span><span><a href="javascript:void();" onclick="appendText(\'{code_postal}\')" >{code_postal}</a></span><span><a href="javascript:void();" onclick="appendText(\'{civility}\')" >{civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_firstname}\')" >{user_firstname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{lastname}\')" >{lastname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date_of_birth}\')" >{date_of_birth}</a></span>';
var usecodecar='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{car_id}\')" >{car_id}</a></span><span><a href="javascript:void();" onclick="appendText(\'{marque}\')" >{marque}</a></span><span><a href="javascript:void();" onclick="appendText(\'{modele}\')" >{modele}</a></span><span><a href="javascript:void();" onclick="appendText(\'{car_statut}\')" >{car_statut}</a></span><span><a href="javascript:void();" onclick="appendText(\'{immatriculation}\')" >{immatriculation}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{nombre_de_place}\')" >{nombre_de_place}</a></span><span><a href="javascript:void();" onclick="appendText(\'{phone}\')" >{phone}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date_dimmatriculation}\')" >{date_dimmatriculation}</a></span><span><a href="javascript:void();" onclick="appendText(\'{age_du_vehicule}\')" >{age_du_vehicule}</a></span><span><a href="javascript:void();" onclick="appendText(\'{serie}\')" >{serie}</a></span><span><a href="javascript:void();" onclick="appendText(\'{boite_a_vitesse}\')" >{boite_a_vitesse}</a></span><span><a href="javascript:void();" onclick="appendText(\'{carburant}\')" >{carburant}</a></span><span><a href="javascript:void();" onclick="appendText(\'{car_type}\')" >{car_type}</a></span><span><a href="javascript:void();" onclick="appendText(\'{courroie}\')" >{courroie}</a></span><span><a href="javascript:void();" onclick="appendText(\'{couleur}\')" >{couleur}</a></span><span><a href="javascript:void();" onclick="appendText(\'{nature}\')" >{nature}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_date_dentree}\')" >{vendeur_date_dentree}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_kilometrage_dentree}\')" >{vendeur_kilometrage_dentree}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_prix_dachat}\')" >{vendeur_prix_dachat}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_civilite}\')" >{vendeur_civilite}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_first_name}\')" >{vendeur_first_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_last_name}\')" >{vendeur_last_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_image}\')" >{vendeur_image}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_societe}\')" >{vendeur_societe}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_societe_image}\')" >{vendeur_societe_image}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_address}\')" >{vendeur_address}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_address2}\')" >{vendeur_address2}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_code_postal}\')" >{vendeur_code_postal}</a></span><span><a href="javascript:void();" onclick="appendText(\'{vendeur_ville}\')" >{vendeur_ville }</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_date_dentree}\')" >{acheteur_date_dentree}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_kilometrage_dentree}\')" >{acheteur_kilometrage_dentree}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_prix_dachat}\')" >{acheteur_prix_dachat}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_civilite}\')" >{acheteur_civilite}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_first_name}\')" >{acheteur_first_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_last_name}\')" >{acheteur_last_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_image}\')" >{acheteur_image}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_societe}\')" >{acheteur_societe}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_societe_image}\')" >{acheteur_societe_image}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_address}\')" >{acheteur_address}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_address2}\')" >{acheteur_address2}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_code_postal}\')" >{acheteur_code_postal}</a></span><span><a href="javascript:void();" onclick="appendText(\'{acheteur_ville}\')" >{acheteur_ville }</a></span>';

var usecodepartner='<label>USEFUL CODES TO USE:</label><br><span><a href="javascript:void();" onclick="appendText(\'{partner_id}\')" >{partner_id}</a></span><span><a href="javascript:void();" onclick="appendText(\'{partner_date}\')" >{partner_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{department_name}\')" >{department_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{partner_notification_status}\')" >{partner_notification_status}</a></span><span><a href="javascript:void();" onclick="appendText(\'{date}\')" >{date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{time}\')" >{time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{partner_notification_subject}\')" >{partner_notification_subject}</a></span><span><a href="javascript:void();" onclick="appendText(\'{partner_column_name}\')" >{partner_column_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{partner_notification_sender_email}\')" >{partner_notification_sender_email}</a></span><span><a href="javascript:void();" onclick="appendText(\'{partner_notification_date}\')" >{partner_notification_date}</a></span><span><a href="javascript:void();" onclick="appendText(\'{partner_notification_time}\')" >{partner_notification_time}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_civility}\')" >{user_civility}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_firstname}\')" >{user_firstname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{user_lastname}\')" >{user_lastname}</a></span><span><a href="javascript:void();" onclick="appendText(\'{company_name}\')" >{company_name}</a></span><span><a href="javascript:void();" onclick="appendText(\'{partner_notification_message}\')" >{partner_notification_message}</a></span><span><a href="javascript:void();" onclick="appendText(\'{partner_column_expire_date}\')" >{partner_column_expire_date}</a></span>';

//usefull short code end section 

    $(document).ready(function() {
        $('#send_copy_to_department_operator').change(function() {
            if(this.checked) {
                $("input[name='send_copy_to_department_operator']").val("1");
            }else{
                $("input[name='send_copy_to_department_operator']").val("0");
            }
        });
    });

    function appendText(text){
        CKEDITOR.instances['message'].insertHtml(text);
    }
    function modulechange(){
        var module = $('#department').val();
        var html = '';
        var subject = '';
         $("#status").empty();
         $("#subject").empty();
         $("#userfullshortcode").empty();

            html = '<option value="">---Select---</option>';
            html +='<option value="1">New</option>';
            html +='<option value="2">Pending</option>';
            html +='<option value="3">Replied</option>';
            html +='<option value="4">Closed</option>';
       
  
          if(module == "1"){
            subject = "{job_notification_subject}";
             CKEDITOR.instances['message'].setData(job, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
             document.getElementById("userfullshortcode").innerHTML =  usecodejob;   
          }
          else if(module == "2"){
            html = '<option value="">---Select---</option>';
            html +='<option value="1">Pending</option>';
            html +='<option value="2">Accepted</option>';
            html +='<option value="3">Denied</option>';
            html +='<option value="4">Cancelled</option>';
            subject = "{quote_notification_subject}";
              CKEDITOR.instances['message'].setData(quote, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
          
            document.getElementById("userfullshortcode").innerHTML =  usecodequote;
          }
          else if(module == "3"){
            subject = "{call_notification_subject}";
             CKEDITOR.instances['message'].setData(call, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
             document.getElementById("userfullshortcode").innerHTML =  usecodecall;

          }
          else if(module == "4"){
            html = '<option value="">---Select---</option>';
            html +='<option value="1">Pending</option>';
            html +='<option value="2">Paid</option>';
            html +='<option value="3">Cancelled</option>';
            subject = "{invoice_notification_subject}";
             CKEDITOR.instances['message'].setData(invoice, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
             document.getElementById("userfullshortcode").innerHTML =  usecodeinvoice;
          }
           else if(module == "5"){
            subject = "{support_notification_subject}";
             CKEDITOR.instances['message'].setData(support, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
            document.getElementById("userfullshortcode").innerHTML =  usecodesupport;
          }
            else if(module == "6"){
            html = '<option value="">---Select---</option>';
            html +='<option value="1">Pending</option>';
            html +='<option value="2">Confirmed</option>';
            html +='<option value="3">Cancelled</option>';
            subject = "{booking_notification_subject}";
             CKEDITOR.instances['message'].setData(booking, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
            document.getElementById("userfullshortcode").innerHTML =  usecodebooking;
          }
           else if(module == "7"){
            html = '<option value="">---Select---</option>';
            html +='<option value="1">Expired</option>';
            subject = "{driver_notification_subject}";
             CKEDITOR.instances['message'].setData(driver, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
             document.getElementById("userfullshortcode").innerHTML =  usecodedriver;
          }
        else if(module == "8"){
            subject = "{client_notification_subject}";
             CKEDITOR.instances['message'].setData(client, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
            document.getElementById("userfullshortcode").innerHTML =  usecodeclient;
          }
          else if(module == "9"){
            subject = "{car_notification_subject}";
             CKEDITOR.instances['message'].setData(car, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
            document.getElementById("userfullshortcode").innerHTML =  usecodecar;
          }
           else if(module == "10"){
            html = '<option value="">---Select---</option>';
            html +='<option value="1">Expired</option>';
            subject = "{partner_notification_subject}";
             CKEDITOR.instances['message'].setData(partner, function()
                {
                      CKEDITOR.instances['message'].resetDirty();
                });
             document.getElementById("userfullshortcode").innerHTML =  usecodepartner;
          }
          else{
                html = '<option value="">---Select---</option>';

            }
         
                document.getElementById("status").innerHTML =  html;
                 document.getElementById("subject").value = subject;
      
    }
   
</script>