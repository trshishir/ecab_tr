<script>

    jQuery(function($){
        $.datepicker.regional['fr'] = {
            closeText: 'Fermer',
            prevText: '&#x3c;Préc',
            nextText: 'Suiv&#x3e;',
            currentText: 'Aujourd\'hui',
            monthNames: ['Janvier','Fevrier','Mars','Avril','Mai','Juin',
                'Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
            monthNamesShort: ['Jan','Fev','Mar','Avr','Mai','Jun',
                'Jul','Aou','Sep','Oct','Nov','Dec'],
            dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
            dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
            dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
            weekHeader: 'Sm',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: '',
            maxDate: '+12M +0D',
            showButtonPanel: true
        };
        $.datepicker.setDefaults($.datepicker.regional['fr']);
       
    });

</script>

<script>
 $(document).ready(function () {
   

    $(document).on('click','.profile_E', function() {
        profileEditEvent();
    });
    $(document).on('click','.contact_E', function() {
        contactEvent();
    });
    $(document).on('click','.methode_E', function() {
        methodeEvent();
    });
    $(document).on('click','.passenger_E', function() {
        passengerEvent();
    });
    $(document).on('click','.ride_E', function() {
        rideEvent();
    });
    $(document).on('click','.booking_E', function() {
        bookingEvent();
    });
    $(document).on('click','.quote_E', function() {
        quoteEvent();
    });
    $(document).on('click','.contract_E', function() {
        contractEvent();
    });
    $(document).on('click','.invoice_E', function() {
        invoiceEvent();
    });
    $(document).on('click','.payment_E', function() {
        paymentEvent();
    });
    $(document).on('click','.support_E', function() {
        supportEvent();
    });
    $(document).on('click','.message_E', function() {
        messageEvent();
    });

  


    
});
    function profileEditEvent(){
        removeAllResources();
        $('.removeinnerevent').remove();
    
    }

  function contactEvent(){
    
        removeAllResources();
        $('.removeinnerevent').remove();
        var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="contactAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="contactEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="contactDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addinnerevent').append(html);
    }


    function methodeEvent(){
        removeAllResources();
        $('.removeinnerevent').remove();
        var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="methodeAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="methodeEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="methodeDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addinnerevent').append(html);
    }


      function passengerEvent(){
        removeAllResources();
        $('.removeinnerevent').remove();
        var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="passengerAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="passengerEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="passengerDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addinnerevent').append(html);
    }


    function rideEvent(){
        removeAllResources();
        $('.removeinnerevent').remove();
        var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="rideAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="rideEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="rideDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addinnerevent').append(html);
    }

  function bookingEvent(){
    removeAllResources();
    $('.removeinnerevent').remove();
    var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="bookingAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="bookingEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="bookingDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addinnerevent').append(html);
    }


        function quoteEvent(){
        removeAllResources();
        $('.removeinnerevent').remove();
        var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="quoteAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="quoteEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="quoteDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addinnerevent').append(html);
    }

    function contractEvent(){
      removeAllResources();
      $('.removeinnerevent').remove();
      var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="contractAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
    <button class="dt-button buttons-copy buttons-html5" onclick="contractEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
    <button class="dt-button buttons-copy buttons-html5" onclick="contractDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
          $('.addinnerevent').append(html);
      }


     function invoiceEvent(){
      removeAllResources();
      $('.removeinnerevent').remove();
      var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="invoiceAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
    <button class="dt-button buttons-copy buttons-html5" onclick="invoiceEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
    <button class="dt-button buttons-copy buttons-html5" onclick="invoiceDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
          $('.addinnerevent').append(html);
      }



     function paymentEvent(){
      removeAllResources();
      $('.removeinnerevent').remove();
      var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="paymentAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
    <button class="dt-button buttons-copy buttons-html5" onclick="paymentEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
    <button class="dt-button buttons-copy buttons-html5" onclick="paymentDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
          $('.addinnerevent').append(html);
      }


     function supportEvent(){
      removeAllResources();
      $('.removeinnerevent').remove();
      var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="supportAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
    <button class="dt-button buttons-copy buttons-html5" onclick="supportEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
    <button class="dt-button buttons-copy buttons-html5" onclick="supportDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
          $('.addinnerevent').append(html);
      }


    function messageEvent(){
     removeAllResources();
     $('.removeinnerevent').remove();
     var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="messageAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
    <button class="dt-button buttons-copy buttons-html5" onclick="messageEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
    <button class="dt-button buttons-copy buttons-html5" onclick="messageDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
          $('.addinnerevent').append(html);
      }





 
 function removeAllResources(){
   //setupDivProfileConfig();
   setupDivContactConfig();
   /*setupDivProfileConfig();
   setupDivMethodeConfig();
   setupDivPassengerConfig();
   setupDivRideConfig();
   setupDivBookingConfig();
   setupDivQuoteConfig();
   setupDivContractConfig();
   setupDivInvoiceConfig();
   setupDivPaymentConfig();
   setupDivSupportConfig();
   setupDivMessageConfig();*/
   
 }
 function createinnerconfigtable(){
 
  // Date range filter
    var minDateFilter = "";
    var maxDateFilter = "";

    var minAgeFilter = "";
    var maxAgeFilter = "";
 
    var dateIndex = $(".configInnerTable thead").find(".column-date").index();
    $.fn.dataTableExt.afnFiltering.push(
            function (oSettings, aData, iDataIndex) {
                if ($("#table-filter .dpo").length > 0) {
                    if (typeof aData._date == 'undefined') {
                        if (aData[dateIndex]) {
                            aData._date = toDate(aData[dateIndex]).getTime();
                        }
                    }

                    if (minDateFilter && !isNaN(minDateFilter)) {
                        if (aData._date < minDateFilter) {
                            return false;
                        }
                    }

                    if (maxDateFilter && !isNaN(maxDateFilter)) {
                        if (aData._date > maxDateFilter) {
                            return false;
                        }
                    }
                }

                return true;
            }
    );

    var ageIndex = $(".configInnerTable thead").find(".column-age").index();
    $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                var min = parseInt($('.table-filter input[data-name="age_from"]').val(), 10);
                var max = parseInt($('.table-filter input[data-name="age_to"]').val(), 10);
                var age = parseFloat(data[ageIndex]) || 0; // use data for the age column

                if ((isNaN(min) && isNaN(max)) ||
                        (isNaN(min) && age <= max) ||
                        (min <= age && isNaN(max)) ||
                        (min <= age && age <= max))
                {
                    return true;
                }
                return false;
            }
    );

    if ($(".configInnerTable thead").find(".column-first_name").length > 0) {
        $.fn.dataTable.ext.search.push(
                function (settings, data, dataIndex) {
                    var firstNameIndex = $(".configInnerTable thead").find(".column-first_name").index();
                    var lastNameIndex = $(".configInnerTable thead").find(".column-last_name").index();
                    var sName = $(document.body).find('.table-filter input[data-name="name"]').val();
                    var fnRegex = new RegExp(sName, 'i');
                    var fName = data[firstNameIndex].replace(/[^\w\s]|_/g, function ($1) {
                        return ' ' + $1 + ' ';
                    }).replace(/[ ]+/g, ' ').split(' ');
                    if (lastNameIndex != -1) {
                        var lName = data[lastNameIndex].replace(/[^\w\s]|_/g, function ($1) {
                            return ' ' + $1 + ' ';
                        }).replace(/[ ]+/g, ' ').split(' ');
                    }


                    return fnRegex.test(fName[0]) || fnRegex.test(fName[1]) || fnRegex.test(fName[2]) || fnRegex.test(lName[0]) || fnRegex.test(lName[1]) || fnRegex.test(lName[1]);
                }
        );
    }

   

    var datatables = $('.configInnerTable').DataTable({
        columnDefs: [
            {targets: 'no-sort', orderable: false}
        ],
        dom: '<"table-filter">B<"innertoolbar">frtip',
        language: {search: "", class: "form-control", searchPlaceholder: "Search"},
        buttons: [
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        initComplete: function () {
            $("div.innertoolbar")
                    .html('<div class="addinnerevent"><div class="removeinnerevent"></div></div>');
            var filterInp = $("#table-filter");
            if (filterInp.length > 0) {
                var tableFilter = $("div.table-filter");
                tableFilter.html(filterInp.html());

                if ($(".table-filter .dpo").length > 0) {
                    $('.table-filter .dpo[data-name="date_from"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            minDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                    $('.table-filter .dpo[data-name="date_to"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            maxDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                }
            }

            this.api().columns().every(function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                    );

                            column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                        });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });

        },
        "drawCallback": function () {
            $('.dataTables_paginate .paginate_button').addClass('btn btn-default');
        }
    });
    var datatables = $('.configInnerTable_language').DataTable({
        columnDefs: [
            {targets: 'no-sort', orderable: false}
        ],
        dom: '<"table-filter">B<"innertoolbar">frtip',
        language: {search: "", searchPlaceholder: "Search"},
        buttons: [
            'copyHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        initComplete: function () {
            $("div.innertoolbar").html('<a href="<?= base_url("admin/$active_class/add.php") ?>" class="btn btn-sm btn-default"><i class="fa fa-plus"></i> Add</a> <a href="javascript:void(0);" class="btn btn-sm btn-default lang_edit" data-edit="0" onClick="setUpdateAction();"><i class="fa fa-pencil"></i> Edit</a> <a href="javascript:void(0);" class="btn btn-sm btn-default lang_edit" data-edit="0" onClick="setgoogletranslateAction();"><i class="fa fa-language"></i> Translate</a>  <a href="<?= base_url("admin/$active_class/importcsv.php") ?>" class="btn btn-sm btn-default delBtn"><i class="fa fa-upload"></i> Import Csv</a>');
            var filterInp = $("#table-filter");
            if (filterInp.length > 0) {
                var tableFilter = $("div.table-filter");
                tableFilter.html(filterInp.html());

                if ($(".table-filter .dpo").length > 0) {
                    $('.table-filter .dpo[data-name="date_from"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            minDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                    $('.table-filter .dpo[data-name="date_to"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            maxDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                }
            }
        },
        "drawCallback": function () {
            $('.dataTables_paginate .paginate_button').addClass('btn btn-default');
        }
    });
    $(document).ready(function () {

        if ($(".table-filter .dpo").length > 0) {
            $(".table-filter .dpo[data-name='date_from']").on("change", function () {
                minDateFilter = new Date(toDate(this.value)).getTime();
                datatables.draw();
            });
            $(".table-filter .dpo[data-name='date_to']").on("change", function () {
                maxDateFilter = new Date(toDate(this.value)).getTime();
                datatables.draw();
            });
        }
        $('.table-filter input[data-name="age_from"], .table-filter input[data-name="age_to"]').on("keyup change", function () {
            datatables.draw();
        });
        $(".table-filter input[data-name='name']").on("keyup", function () {
            datatables.draw();
        });
        $('.example').dataTable();
        $('.table-filter input').addClass('form-control'); // <-- add this line
        $(".singleSelect:checked").prop("checked", false);
        $(".singleSelect").click(function () {
            var that = $(this);
            var checked = that.is(":checked");
            $("table .singleSelect:checked").prop("checked", false);
            if (checked) {
                that.prop("checked", true);
                //console.log(that.data('id'));
                $(".configInnerTable").attr("data-selected_id", that.data('id'))
            } else
                $(".configInnerTable").attr("data-selected_id", "")
        });

        var filterInputs = $(".table-filter input, .table-filter select");
        filterInputs.each(function (x, y) {
            var name = $(y).data('name');
            //console.log(name)
            if (name != undefined) {
                $(y).on("change keyup", function () {
                    if ($.inArray(name, ['name', 'date_from', 'date_to', 'age_from', 'age_to']) === -1) {
                        var colIndex = $(".configInnerTable thead").find(".column-" + name).index();
                        if (datatables.column(colIndex).search() !== this.value) {
                            datatables.column(colIndex).search(this.value).draw();
                        }
                    }
                });
            }
        });

    });
    var dtable = $('.configInnerTable').DataTable();
    $('.filter').on('keyup change', function() {
  //clear global search values
        dtable.search('');
        dtable.column($(this).data('columnIndex')).search(this.value).draw();
    });

    $(".dataTables_filter input").on('keyup change', function() {
    //clear column search values
         dtable.columns().search('');
    //clear input values
        $('.filter').val('');
    });


                /* inner config*/
 }
</script>
