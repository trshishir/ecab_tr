<script type="text/javascript">
  $(document).ready(function() {

           $(document).on('change','#contact_country_id', function() {
                      contact_get_region_base_country(this);
                  });
           $(document).on('change','#contact_region_id', function() {
                      contact_get_cities_base_country_region(this);
                  });
           $(document).on('change','#edit_contact_country_id', function() {
                      edit_contact_get_region_base_country(this);
                  });
           $(document).on('change','#edit_contact_region_id', function() {
                      edit_contact_get_cities_base_country_region(this);
                  });

            $(document).on('change','input.chk-maincontact-template', function() {
                      $('input.chk-maincontact-template').not(this).prop('checked', false);
                      var id = $(this).attr('data-input');
                      $('.chk-Addcontact-btn').val(id);
                      $('#contactdeletid').val(id);
                  });        

  });
function contactAdd()
{
  setupDivContact();
  $(".Contactadd").show();
}

function cancelContact()
{
  setupDivContact();
  $(".ListContact").show();
}

function contactEdit()
{
    var val = $('.chk-Addcontact-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivContact();
        $(".contactEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/clients/get_ajax_contact_data'; ?>',
                data: {'contact_id': val},
                success: function (result) {
                  document.getElementsByClassName("contactEditajax")[0].innerHTML = result;
                
                }
            });
}
function contactidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivContact();
      $(".contactEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/clients/get_ajax_contact_data'; ?>',
              data: {'contact_id': val},
              success: function (result) {
                document.getElementsByClassName("contactEditajax")[0].innerHTML = result;
               
              }
          });
}
function contactDelete()
{
  var val = $('.chk-Addcontact-btn').val();
  if(val != "")
  {
  setupDivContact();
  $(".contactDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivContact()
{
  $(".Contactadd").hide();
  $(".contactEdit").hide();
  $(".ListContact").hide();
  $(".contactDelete").hide();

}
function setupDivContactConfig()
{
  $(".Contactadd").hide();
  $(".contactEdit").hide();
  $(".ListContact").show();
  $(".contactDelete").hide();

}


  function contact_get_region_base_country(region_id=null)
   {
     country_id=$('#contact_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
         html ='';
         html =html + '<option value="">Select Region</option>';
         
         $.each(response, function( index, value ) {  
         if(region_id==value.id)
         {
           html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
         }
         else{
           html =html + '<option value="'+value.id+'">'+value.name+'</option>';
         }
         });
         $('#contact_region_id').html(html);
       }
   }); 
}

function contact_get_cities_base_country_region(region_id=null,cities_id=null)
{

region_id=$('#contact_region_id').val();

$.ajax({
    url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
    method: 'post',
    data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
    dataType: 'json',
    success: function(response){
    console.log(response);
    html ='';
    html =html + ' <option value="">Select cities</option>';

    $.each(response, function( index, value ) {
    // console.log( value );
                

    if(cities_id==value.id)
    {
        html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';

    }
    else{
        html =html + '<option value="'+value.id+'">'+value.name+'</option>';

    }
    });

    $('#contact_cities_id').html(html);


    }


});
}


//For Edit Page

function edit_contact_get_cities_base_country_region(region_id=null,cities_id=null)
{
    region_id=$('#edit_contact_region_id').val();
    $.ajax({
        url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
        method: 'post',
        data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
        dataType: 'json',
        success: function(response){
            html ='';
            html =html + ' <option value="">Select cities</option>';

            $.each(response, function( index, value ) {
            if(cities_id==value.id)
            {
                html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
            }
            else{
                html =html + '<option value="'+value.id+'">'+value.name+'</option>';
            }
            });
            $('#edit_contact_cities_id').html(html);
        }
    });
}
   function edit_contact_get_region_base_country(region_id=null)
   { 
     country_id=$('#edit_contact_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
     html ='';
     html =html + '<option value="">Select Region</option>';
     
     $.each(response, function( index, value ) {
     if(region_id==value.id)
     {
       html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';  
     }
     else{
       html =html + '<option value="'+value.id+'">'+value.name+'</option>';
     }
     });
     $('#edit_contact_region_id').html(html);
     }   
     }); 
   }
//For Edit Page

//create form submit
$(document).on('submit',"form#addcontactform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivContact();
         $('.ListContact').empty();
         $('.ListContact').html(data.record);
         createinnerconfigtable();
         contactEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivContact();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListContact").show();
        }
     document.getElementById("addcontactform").reset();
      }
    });
    return false;
  });
//create form submit

//update form submit
$(document).on('submit',"form#updatecontactform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivContact();
         $('.ListContact').empty();
         $('.ListContact').html(data.record);
         createinnerconfigtable();
         contactEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivContact();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListContact").show();
        }
     document.getElementById("addcontactform").reset();
      }
    });
    return false;
  });
//update form submit

//delete form submit
$(document).on('submit',"form#deletecontactform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivContact();
         $('.ListContact').empty();
         $('.ListContact').html(data.record);
         createinnerconfigtable();
         contactEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivContact();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListContact").show();
        }
     document.getElementById("addcontactform").reset();
      }
    });
    return false;
  });
//delete form submit
</script>
