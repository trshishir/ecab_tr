  <input type="hidden" class="chk-Addcontact-btn" value="">
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configInnerTable cell-border  dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" style="width:2%;">#</th>
            <th class="text-center" ><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('added_by'); ?></th>
            <th class="text-center" >Profile id</th>
            <th class=" text-center">Name</th>
            <th class=" text-center">Statut</th>
            <th class="text-center" >Since</th>
          </tr>
          </thead>
          <tbody id="contactlistdatabyid">
                   
                                        <?php 
                                        if (!empty($client_contact_data)):  
                                        ?>
                                        <?php foreach($client_contact_data as $key => $item):?>
                                            <tr>
                                                <td class="text-center">
                                                  <input type="checkbox" class="chk-maincontact-template" data-input="<?=$item->id?>"></td> 
                                                <td class="text-center"><a href="javascript:void()" onclick="contactidEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->created_at)) .'0000'. $item->id;?></a></td>
                                                <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                                                <td class="text-center"><?=date("H:i:s", strtotime($item->created_at))?></td>
                                                <td class="text-center"><?= $this->clients_model->getuser($item->user_id);?></td>
                                                <td class="text-center" ><?= $item->profile_id ?> </td>
                                                <td class="text-center"><?= $this->clients_model->getSingleRecord('vbs_clientcivilite',['id'=>$item->civilite])->civilite.' '.$item->prenom.' '.$item->nom ?></td>

                                                 
                                                 <?php
                                                   $status=$this->clients_model->getSingleRecord('vbs_clientstatus',['id'=>$item->status])->name; 
                                                 ?>
                                                                      
                                             <td class="text-center">
                                               <?php if(strtolower($status) == "pending" || strtolower($status) == "not available"): ?>
                                               <span class="label label-danger"><?= $status ?></span>
                                               <?php else: ?>
                                                 <span class="label label-success"><?= $status ?></span>
                                               <?php endif; ?>
                                         
                                            </td>
                                                <td class="text-center"><?= timeDiff($item->created_at) ?></td>

                                            </tr>
                                           
                                        <?php endforeach; ?>
                                        <?php endif; ?>
          </tbody>
      </table>
      <br>
    </div>
  </div>