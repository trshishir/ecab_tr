             <?php
              $attributes = array("name" => 'update_contact_form', "id" => 'updatecontactform');   
              echo form_open_multipart('admin/clients/contactEdit',$attributes); ?>
                 <input type="hidden" value="<?= $contact->id ?>" name="contact_id">
                  <div class="col-md-12 pdlz">
                    
                      <!-- Row 1 -->
                        <div class="col-md-12 pdhz" style="margin-top:10px;" >
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Statut : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                         <select class="form-control" name="status" required>
                                           <option value="">Select</option>
                                            <?php foreach ($client_status_data as $item): ?>
                                             <option <?php if($item->id == $contact->status){ echo "selected"; } ?> value="<?= $item->id ?>"><?= $item->name ?></option>
                                             <?php endforeach; ?>
                                            
                                        </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Civilite : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <select class="form-control" name="civilite" required>
                                                  <option value="">Select</option>
                                                <?php foreach ($client_civilite_data as $item): ?>
                                            <option <?php if($item->id == $contact->civilite){ echo "selected"; } ?> value="<?= $item->id ?>"><?= $item->civilite ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                  <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Nom : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input name="nom" class="form-control" type="text" required value="<?= $contact->nom ?>">
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Prenom : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input name="pre_nom" class="form-control" type="text" required value="<?= $contact->pre_nom ?>">
                                    </div>
                                </div>
                            </div>

                            
                             
                          </div>


                          <!-- Row 2 -->
                          <div class="col-md-12 pdhz" style="margin-top:10px;" >
                              <div class="col-md-3">
                                   <div class="form-group" >
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Email : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="email" class="form-control" style="width:100%;" type="email" required value="<?= $contact->email ?>">
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-3">
                                  <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Phone : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="phone" class="form-control" style="width:100%;" type="phone" value="<?= $contact->phone ?>">
                                        </div>
                                    </div>
                                </div>
                            <div class="col-md-3">
                                  <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Mobile : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="mobile" class="form-control" style="width:100%;" type="text" value="<?= $contact->mobile ?>">
                                        </div>
                                    </div>
                                </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Fax : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input name="fax" class="form-control" type="text" value="<?= $contact->fax ?>">
                                    </div>
                                </div>
                            </div>

                          
                          </div>

                          <!-- Row 3 -->
                          <div class="col-md-12 pdhz" style="margin-top: 10px;">
                             <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Address 1 : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="address1" class="form-control" type="text" value="<?= $contact->address1 ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Address 2 : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="address2" class="form-control" type="text" value="<?= $contact->address2 ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Job : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="job" class="form-control" type="text" value="<?= $contact->job ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Department : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="department" class="form-control" type="text" value="<?= $contact->department ?>">
                                        </div>
                                    </div>
                                </div>
                               
                          </div>
                       
                           <!-- Row 4 -->
                           <div class="col-md-12 pdhz" style="margin-top:10px;" >
                              <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Code Postal : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="code_postal" class="form-control" type="text" value="<?= $contact->code_postal ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Country : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                        <select required="required"  class="form-control" name="country" id="edit_contact_country_id" tabindex="1">
                                            <option value="">Select Country</option>

                                            <?php foreach($countries as $data):?>
                                                <option <?php if($data->id == $contact->country){ echo "selected"; } ?> value="<?=$data->id;?>"><?=$data->name;?></option>
                                            <?php endforeach;?>
                                            </select>
                                           
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Region : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                           <div class="form-group">
                                              
                                                <select required class="form-control" name="region" id="edit_contact_region_id">
                                            <?php foreach($regions as $data):?>
                                                <option <?php if($contact->region == $data->id){ echo "selected"; } ?> value="<?=$data->id;?>"><?=$data->name;?></option>
                                            <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Ville : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                        <select required="required" class="form-control" name="ville" id="edit_contact_cities_id" tabindex="1">
                                              <?php foreach($cities as $data):?>
                                                <option <?php if($contact->ville == $data->id){ echo "selected"; } ?> value="<?=$data->id;?>"><?=$data->name;?></option>
                                            <?php endforeach;?>

                                        </select>
                                            
                                        </div>
                                    </div>
                                </div>  
                           </div>
                         </div>
                         
                       
                       
                           <div class="col-md-12" style="padding: 30px;">                       
                             <!-- icons -->
                              <button type="submit"  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                             <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelContact()"><span class="fa fa-close"> Cancel </span></button>
                             <!-- icons -->  
                          </div>

                     <?php echo form_close(); ?>
 