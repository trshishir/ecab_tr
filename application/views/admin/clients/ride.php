<div class="row">
  <div class="ListRide" >
  <input type="hidden" class="chk-Addride-btn" value="">
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" style="width:2%;">#</th>
            <th class="text-center" ><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('added_by'); ?></th>
            <th class=" text-center">Ride</th>
            <th class=" text-center">Statut</th>
            <th class="text-center" >Since</th>

          </tr>
          </thead>
          <tbody>
                  <tr>
                    <td class="no-sort text-center"style="width:2%;">
                        <input type="checkbox" class="chk-mainride-template" data-input="123">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="rideidEdit('123')">1212120000</a></td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>

                  </tr>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>

<div class="Rideadd" style="display: none;" >
 <?php  include 'ride/add.php';?> 
</div>
 
<div class="rideEdit" style="display:none">
  <?=form_open("admin/clients/rideEdit")?>
  <div class="rideEditajax">
  </div>
  <div class="col-md-12">                          
      <!-- icons -->
       <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
      <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelRide()"><span class="fa fa-close"> Cancel </span></button>
      <!-- icons -->   
   </div>  
</div>
 <?php echo form_close(); ?>
 
<div class="col-md-12 rideDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
  <?=form_open("admin/clients/rideDelete")?>
    <input  name="tablename" value="vbs_clientride" type="hidden" >
    <input type="hidden" id="ridedeletid" name="delet_ride_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

    <a class="btn btn-default" style="cursor: pointer;" onclick="cancelRide()"><span class="delete-icon">No</span></a>
  <?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
function rideAdd()
{
  setupDivRide();
  $(".Rideadd").show();
}

function cancelRide()
{
  setupDivRide();
  $(".ListRide").show();
}

function rideEdit()
{
    var val = $('.chk-Addride-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivRide();
        $(".rideEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/clients/get_ajax_ride_data'; ?>',
                data: {'ride_id': val},
                success: function (result) {
                  document.getElementsByClassName("rideEditajax")[0].innerHTML = result;
                }
            });
}
function rideidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivRide();
      $(".rideEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/clients/get_ajax_ride_data'; ?>',
              data: {'ride_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("rideEditajax")[0].innerHTML = result;
              }
          });
}
function rideDelete()
{
  var val = $('.chk-Addride-btn').val();
  if(val != "")
  {
  setupDivRide();
  $(".rideDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivRide()
{
  $(".Rideadd").hide();
  $(".rideEdit").hide();
  $(".ListRide").hide();
  $(".rideDelete").hide();

}
function setupDivRideConfig()
{
  $(".Rideadd").hide();
  $(".rideEdit").hide();
  $(".ListRide").show();
  $(".rideDelete").hide();

}
$('input.chk-mainride-template').on('change', function() {
  $('input.chk-mainride-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Addride-btn').val(id);
  $('#ridedeletid').val(id);
});

</script>
