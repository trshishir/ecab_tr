<div class="row">
  <div class="ListPayment" >
  <input type="hidden" class="chk-Addpayment-btn" value="">
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" style="width:2%;">#</th>
            <th class="text-center" ><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('added_by'); ?></th>
            <th class=" text-center">Payment</th>
            <th class=" text-center">Statut</th>
            <th class="text-center" >Since</th>

          </tr>
          </thead>
          <tbody>
                  <tr>
                    <td class="no-sort text-center"style="width:2%;">
                        <input type="checkbox" class="chk-mainpayment-template" data-input="123">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="paymentidEdit('123')">1212120000</a></td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>

                  </tr>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>

<div class="Paymentadd" style="display: none;" >
 <?php  include 'payment/add.php';?> 
</div>
 
<div class="paymentEdit" style="display:none">
  <?=form_open("admin/clients/paymentEdit")?>
  <div class="paymentEditajax">
  </div>
  <div class="col-md-12">                          
      <!-- icons -->
       <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
      <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelPayment()"><span class="fa fa-close"> Cancel </span></button>
      <!-- icons -->   
   </div>  
</div>
 <?php echo form_close(); ?>
 
<div class="col-md-12 paymentDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
  <?=form_open("admin/clients/paymentDelete")?>
    <input  name="tablename" value="vbs_clientpayment" type="hidden" >
    <input type="hidden" id="paymentdeletid" name="delet_payment_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

    <a class="btn btn-default" style="cursor: pointer;" onclick="cancelPayment()"><span class="delete-icon">No</span></a>
  <?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
function paymentAdd()
{
  setupDivPayment();
  $(".Paymentadd").show();
}

function cancelPayment()
{
  setupDivPayment();
  $(".ListPayment").show();
}

function paymentEdit()
{
    var val = $('.chk-Addpayment-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivPayment();
        $(".paymentEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/clients/get_ajax_payment_data'; ?>',
                data: {'payment_id': val},
                success: function (result) {
                  document.getElementsByClassName("paymentEditajax")[0].innerHTML = result;
                }
            });
}
function paymentidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivPayment();
      $(".paymentEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/clients/get_ajax_payment_data'; ?>',
              data: {'payment_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("paymentEditajax")[0].innerHTML = result;
              }
          });
}
function paymentDelete()
{
  var val = $('.chk-Addpayment-btn').val();
  if(val != "")
  {
  setupDivPayment();
  $(".paymentDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivPayment()
{
  $(".Paymentadd").hide();
  $(".paymentEdit").hide();
  $(".ListPayment").hide();
  $(".paymentDelete").hide();

}
function setupDivPaymentConfig()
{
  $(".Paymentadd").hide();
  $(".paymentEdit").hide();
  $(".ListPayment").show();
  $(".paymentDelete").hide();

}
$('input.chk-mainpayment-template').on('change', function() {
  $('input.chk-mainpayment-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Addpayment-btn').val(id);
  $('#paymentdeletid').val(id);
});

</script>
