<div class="row">
  <div class="ListMethode" >
  <input type="hidden" class="chk-Addmethode-btn" value="">
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" style="width:2%;">#</th>
            <th class="text-center" ><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('added_by'); ?></th>
            <th class=" text-center">Methode</th>
            <th class=" text-center">Statut</th>
            <th class="text-center" >Since</th>

          </tr>
          </thead>
          <tbody>
                  <tr>
                    <td class="no-sort text-center"style="width:2%;">
                        <input type="checkbox" class="chk-mainmethode-template" data-input="123">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="methodeidEdit('123')">1212120000</a></td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>

                  </tr>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>

<div class="Methodeadd" style="display: none;" >
 <?php  include 'methode/add.php';?> 
</div>
 
<div class="methodeEdit" style="display:none">
  <?=form_open("admin/clients/methodeEdit")?>
  <div class="methodeEditajax">
  </div>
  <div class="col-md-12">                          
      <!-- icons -->
       <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
      <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelMethode()"><span class="fa fa-close"> Cancel </span></button>
      <!-- icons -->   
   </div>  
</div>
 <?php echo form_close(); ?>
 
<div class="col-md-12 methodeDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
  <?=form_open("admin/clients/methodeDelete")?>
    <input  name="tablename" value="vbs_clientmethode" type="hidden" >
    <input type="hidden" id="methodedeletid" name="delet_methode_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

    <a class="btn btn-default" style="cursor: pointer;" onclick="cancelMethode()"><span class="delete-icon">No</span></a>
  <?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
function methodeAdd()
{
  setupDivMethode();
  $(".Methodeadd").show();
}

function cancelMethode()
{
  setupDivMethode();
  $(".ListMethode").show();
}

function methodeEdit()
{
    var val = $('.chk-Addmethode-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivMethode();
        $(".methodeEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/clients/get_ajax_methode_data'; ?>',
                data: {'methode_id': val},
                success: function (result) {
                  document.getElementsByClassName("methodeEditajax")[0].innerHTML = result;
                }
            });
}
function methodeidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivMethode();
      $(".methodeEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/clients/get_ajax_methode_data'; ?>',
              data: {'methode_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("methodeEditajax")[0].innerHTML = result;
              }
          });
}
function methodeDelete()
{
  var val = $('.chk-Addmethode-btn').val();
  if(val != "")
  {
  setupDivMethode();
  $(".methodeDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivMethode()
{
  $(".Methodeadd").hide();
  $(".methodeEdit").hide();
  $(".ListMethode").hide();
  $(".methodeDelete").hide();

}
function setupDivMethodeConfig()
{
  $(".Methodeadd").hide();
  $(".methodeEdit").hide();
  $(".ListMethode").show();
  $(".methodeDelete").hide();

}
$('input.chk-mainmethode-template').on('change', function() {
  $('input.chk-mainmethode-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Addmethode-btn').val(id);
  $('#methodedeletid').val(id);
});

</script>
