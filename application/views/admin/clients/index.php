<?php $locale_info = localeconv(); ?>

<section id="content">
    <div class="listDiv"><!--col-md-10 padding white right-p-->
    
   <div style="padding: 5px 12px;display: none;" id="inner_alert_div" class="alert">
           <span id="inner_message_div"></span>    
            <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
    </div>
    

            <div class="row-fluid"  id="ShowTabs" style="margin-bottom: 10px; margin-left:0px;">

                                <ul class="nav nav-tabs responsive">
                                            <li class="active"><a href="#PROFILE" class="profile_E" role="tab" data-toggle="tab">PROFIL</a></li>
                                            <li><a href="#CONTACT" class="contact_E" role="tab" data-toggle="tab">CONTACTS</a></li>
                                            <li><a href="#METHODE" class="methode_E" role="tab" data-toggle="tab">PAIEMENTS METHODES</a></li>
                                            <li><a href="#PASSENGER" role="tab" class="passenger_E" data-toggle="tab">PASSENGERS</a></li>
                                            <li><a href="#RIDE" class="ride_E" role="tab" data-toggle="tab">RIDES</a></li>
                                            <li><a href="#BOOKING" class="booking_E" role="tab" data-toggle="tab">BOOKINGS</a></li>
                                            <li><a href="#QUOTE" class="quote_E" role="tab" data-toggle="tab">QUOTES</a></li>
                                            <li><a href="#CONTRACT" class="contract_E" role="tab" data-toggle="tab">CONTRACTS</a></li>
                                            <li><a href="#INVOICE" class="invoice_E" role="tab" data-toggle="tab">INVOICES</a></li>
                                            <li><a href="#PAYMENT" class="payment_E" role="tab" data-toggle="tab">PAYMENTS</a></li>
                                            <li><a href="#SUPPORT" class="support_E" role="tab" data-toggle="tab">SUPPORT</a></li>
                                            <li><a href="#MESSAGE" class="message_E" role="tab" data-toggle="tab">MESSAGES</a></li>          
                                </ul>
                               
                <div class="tab-content responsive" style="width:100%;display: table;">
                        <!--status tab starts-->
                        <div class="tab-pane fade in active" id="PROFILE" style="border: 1px solid #75b0d7;">
                            <div class="row">
                            <?php include('profile/edit.php'); ?>
                        </div>
                        </div>

                        <div class="tab-pane fade" id="CONTACT" style="border: 1px solid #75b0d7;">
                        <?php include('contact.php'); ?>
                        </div>
                        <div class="tab-pane fade" id="METHODE" style="border: 1px solid #75b0d7;">
                        <?php include('methode.php'); ?>
                        </div>

                        <div class="tab-pane fade"  id="PASSENGER" style="border: 1px solid #75b0d7;">
                        <?php include('passenger.php'); ?>
                        </div>
                        <div class="tab-pane fade" id="RIDE" style="border: 1px solid #75b0d7;">
                        <?php include('ride.php'); ?>
                        </div>
                        <div class="tab-pane fade" id="BOOKING" style="border: 1px solid #75b0d7;">
                        <?php include('booking.php'); ?>
                        </div>
                        <div class="tab-pane fade" id="QUOTE" style="border: 1px solid #75b0d7;">
                        <?php include('quote.php'); ?>
                        </div>
                        <div class="tab-pane fade" id="CONTRACT" style="border: 1px solid #75b0d7;">
                        <?php include('contract.php'); ?>
                        </div>
                        <div class="tab-pane fade" id="INVOICE" style="border: 1px solid #75b0d7;">
                        <?php include('invoice.php'); ?>
                        </div>
                        <div class="tab-pane fade" id="PAYMENT" style="border: 1px solid #75b0d7;">
                        <?php include('payment.php'); ?>
                        </div>
                        <div class="tab-pane fade" id="SUPPORT" style="border: 1px solid #75b0d7;">
                        <?php include('support.php'); ?>
                        </div>
                         <div class="tab-pane fade" id="MESSAGE" style="border: 1px solid #75b0d7;">
                        <?php include('message.php'); ?>
                        </div>             
                    </div>
            <!--/.module-->
        </div>

    </div>
</section>
