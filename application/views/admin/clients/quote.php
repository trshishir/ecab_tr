<div class="row">
  <div class="ListQuote" >
  <input type="hidden" class="chk-Addquote-btn" value="">
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" style="width:2%;">#</th>
            <th class="text-center" ><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('added_by'); ?></th>
            <th class=" text-center">Quote</th>
            <th class=" text-center">Statut</th>
            <th class="text-center" >Since</th>

          </tr>
          </thead>
          <tbody>
                  <tr>
                    <td class="no-sort text-center"style="width:2%;">
                        <input type="checkbox" class="chk-mainquote-template" data-input="123">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="quoteidEdit('123')">1212120000</a></td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>

                  </tr>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>

<div class="Quoteadd" style="display: none;" >
 <?php  include 'quote/add.php';?> 
</div>
 
<div class="quoteEdit" style="display:none">
  <?=form_open("admin/clients/quoteEdit")?>
  <div class="quoteEditajax">
  </div>
  <div class="col-md-12">                          
      <!-- icons -->
       <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
      <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelQuote()"><span class="fa fa-close"> Cancel </span></button>
      <!-- icons -->   
   </div>  
</div>
 <?php echo form_close(); ?>
 
<div class="col-md-12 quoteDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
  <?=form_open("admin/clients/quoteDelete")?>
    <input  name="tablename" value="vbs_clientquote" type="hidden" >
    <input type="hidden" id="quotedeletid" name="delet_quote_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

    <a class="btn btn-default" style="cursor: pointer;" onclick="cancelQuote()"><span class="delete-icon">No</span></a>
  <?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
function quoteAdd()
{
  setupDivQuote();
  $(".Quoteadd").show();
}

function cancelQuote()
{
  setupDivQuote();
  $(".ListQuote").show();
}

function quoteEdit()
{
    var val = $('.chk-Addquote-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivQuote();
        $(".quoteEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/clients/get_ajax_quote_data'; ?>',
                data: {'quote_id': val},
                success: function (result) {
                  document.getElementsByClassName("quoteEditajax")[0].innerHTML = result;
                }
            });
}
function quoteidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivQuote();
      $(".quoteEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/clients/get_ajax_quote_data'; ?>',
              data: {'quote_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("quoteEditajax")[0].innerHTML = result;
              }
          });
}
function quoteDelete()
{
  var val = $('.chk-Addquote-btn').val();
  if(val != "")
  {
  setupDivQuote();
  $(".quoteDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivQuote()
{
  $(".Quoteadd").hide();
  $(".quoteEdit").hide();
  $(".ListQuote").hide();
  $(".quoteDelete").hide();

}
function setupDivQuoteConfig()
{
  $(".Quoteadd").hide();
  $(".quoteEdit").hide();
  $(".ListQuote").show();
  $(".quoteDelete").hide();

}
$('input.chk-mainquote-template').on('change', function() {
  $('input.chk-mainquote-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Addquote-btn').val(id);
  $('#quotedeletid').val(id);
});

</script>
