<div class="row">
  <div class="ListBooking" >
  <input type="hidden" class="chk-Addbooking-btn" value="">
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" style="width:2%;">#</th>
            <th class="text-center" ><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('added_by'); ?></th>
            <th class=" text-center">Booking</th>
            <th class=" text-center">Statut</th>
            <th class="text-center" >Since</th>

          </tr>
          </thead>
          <tbody>
                  <tr>
                    <td class="no-sort text-center"style="width:2%;">
                        <input type="checkbox" class="chk-mainbooking-template" data-input="123">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="bookingidEdit('123')">1212120000</a></td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>

                  </tr>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>

<div class="Bookingadd" style="display: none;" >
 <?php  include 'booking/add.php';?> 
</div>
 
<div class="bookingEdit" style="display:none">
  <?=form_open("admin/clients/bookingEdit")?>
  <div class="bookingEditajax">
  </div>
  <div class="col-md-12">                          
      <!-- icons -->
       <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
      <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelBooking()"><span class="fa fa-close"> Cancel </span></button>
      <!-- icons -->   
   </div>  
</div>
 <?php echo form_close(); ?>
 
<div class="col-md-12 bookingDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
  <?=form_open("admin/clients/bookingDelete")?>
    <input  name="tablename" value="vbs_clientbooking" type="hidden" >
    <input type="hidden" id="bookingdeletid" name="delet_booking_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

    <a class="btn btn-default" style="cursor: pointer;" onclick="cancelBooking()"><span class="delete-icon">No</span></a>
  <?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
function bookingAdd()
{
  setupDivBooking();
  $(".Bookingadd").show();
}

function cancelBooking()
{
  setupDivBooking();
  $(".ListBooking").show();
}

function bookingEdit()
{
    var val = $('.chk-Addbooking-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivBooking();
        $(".bookingEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/clients/get_ajax_booking_data'; ?>',
                data: {'booking_id': val},
                success: function (result) {
                  document.getElementsByClassName("bookingEditajax")[0].innerHTML = result;
                }
            });
}
function bookingidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivBooking();
      $(".bookingEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/clients/get_ajax_booking_data'; ?>',
              data: {'booking_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("bookingEditajax")[0].innerHTML = result;
              }
          });
}
function bookingDelete()
{
  var val = $('.chk-Addbooking-btn').val();
  if(val != "")
  {
  setupDivBooking();
  $(".bookingDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivBooking()
{
  $(".Bookingadd").hide();
  $(".bookingEdit").hide();
  $(".ListBooking").hide();
  $(".bookingDelete").hide();

}
function setupDivBookingConfig()
{
  $(".Bookingadd").hide();
  $(".bookingEdit").hide();
  $(".ListBooking").show();
  $(".bookingDelete").hide();

}
$('input.chk-mainbooking-template').on('change', function() {
  $('input.chk-mainbooking-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Addbooking-btn').val(id);
  $('#bookingdeletid').val(id);
});

</script>
