<div class="row">
  <div class="ListContract" >
  <input type="hidden" class="chk-Addcontract-btn" value="">
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" style="width:2%;">#</th>
            <th class="text-center" ><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('added_by'); ?></th>
            <th class=" text-center">Contract</th>
            <th class=" text-center">Statut</th>
            <th class="text-center" >Since</th>

          </tr>
          </thead>
          <tbody>
                  <tr>
                    <td class="no-sort text-center"style="width:2%;">
                        <input type="checkbox" class="chk-maincontract-template" data-input="123">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="contractidEdit('123')">1212120000</a></td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>

                  </tr>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>

<div class="Contractadd" style="display: none;" >
 <?php  include 'contract/add.php';?> 
</div>
 
<div class="contractEdit" style="display:none">
  <?=form_open("admin/clients/contractEdit")?>
  <div class="contractEditajax">
  </div>
  <div class="col-md-12">                          
      <!-- icons -->
       <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
      <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelContract()"><span class="fa fa-close"> Cancel </span></button>
      <!-- icons -->   
   </div>  
</div>
 <?php echo form_close(); ?>
 
<div class="col-md-12 contractDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
  <?=form_open("admin/clients/contractDelete")?>
    <input  name="tablename" value="vbs_clientcontract" type="hidden" >
    <input type="hidden" id="contractdeletid" name="delet_contract_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

    <a class="btn btn-default" style="cursor: pointer;" onclick="cancelContract()"><span class="delete-icon">No</span></a>
  <?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
function contractAdd()
{
  setupDivContract();
  $(".Contractadd").show();
}

function cancelContract()
{
  setupDivContract();
  $(".ListContract").show();
}

function contractEdit()
{
    var val = $('.chk-Addcontract-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivContract();
        $(".contractEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/clients/get_ajax_contract_data'; ?>',
                data: {'contract_id': val},
                success: function (result) {
                  document.getElementsByClassName("contractEditajax")[0].innerHTML = result;
                }
            });
}
function contractidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivContract();
      $(".contractEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/clients/get_ajax_contract_data'; ?>',
              data: {'contract_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("contractEditajax")[0].innerHTML = result;
              }
          });
}
function contractDelete()
{
  var val = $('.chk-Addcontract-btn').val();
  if(val != "")
  {
  setupDivContract();
  $(".contractDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivContract()
{
  $(".Contractadd").hide();
  $(".contractEdit").hide();
  $(".ListContract").hide();
  $(".contractDelete").hide();

}
function setupDivContractConfig()
{
  $(".Contractadd").hide();
  $(".contractEdit").hide();
  $(".ListContract").show();
  $(".contractDelete").hide();

}
$('input.chk-maincontract-template').on('change', function() {
  $('input.chk-maincontract-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Addcontract-btn').val(id);
  $('#contractdeletid').val(id);
});

</script>
