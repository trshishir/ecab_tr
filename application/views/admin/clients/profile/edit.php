             <?php echo form_open_multipart('admin/clients/profileEdit'); ?>
                 <input type="hidden" value="<?= $client->id ?>" name="profile_id">
                  <div class="col-md-12 pdlz">
                    <div class="col-md-9 pdhz">
                      <!-- Row 1 -->
                        <div class="col-md-12 pdhz" style="margin-top:10px;" >
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Statut : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                         <select class="form-control" name="status" required>
                                           <option value="">Select</option>
                                            <?php foreach ($client_status_data as $item): ?>
                                             <option <?php if($item->id == $client->status){ echo "selected"; } ?> value="<?= $item->id ?>"><?= $item->name ?></option>
                                             <?php endforeach; ?>
                                            
                                        </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Civility : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <select class="form-control" name="civilite" required>
                                                  <option value="">Select</option>
                                                <?php foreach ($client_civilite_data as $item): ?>
                                            <option <?php if($item->id == $client->civilite){ echo "selected"; } ?> value="<?= $item->id ?>"><?= $item->civilite ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                            <?php
                                 $birthday = strtotime($client->birthday);
                                 $birthday = date('d/m/Y',$birthday);
                                 $birthday= $birthday;

                             ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Date of Birth : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input name="birthday" class="form-control datepicker" value="<?= $birthday ?>" type="text">
                                    </div>
                                </div>
                            </div>

                                
                             
                          </div>


                          <!-- Row 2 -->
                          <div class="col-md-12 pdhz" style="margin-top:10px;" >
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Last Name : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input name="nom" class="form-control" type="text" required value="<?= $client->nom ?>">
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">First Name : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input name="pre_nom" class="form-control" type="text" required value="<?= $client->pre_nom ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Company : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input name="company" class="form-control" type="text" value="<?= $client->company ?>">
                                    </div>
                                </div>
                            </div>

                            
                          </div>

                          <!-- Row 3 -->
                          <div class="col-md-12 pdhz" style="margin-top: 10px;">
                             <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Address 1 : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="address1" class="form-control" type="text" value="<?= $client->address1 ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Address 2 : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="address2" class="form-control" type="text" value="<?= $client->address2 ?>">
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Code Postal : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="code_postal" class="form-control" type="text" value="<?= $client->code_postal ?>">
                                        </div>
                                    </div>
                                </div>
                             
                          </div>
                          <div class="col-md-12 pdhz" style="margin-top:10px;" >
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <div class="col-md-4" style="margin-top: 5px;">
                                          <span style="font-weight: normal;">Country : </span>
                                      </div>
                                      <div class="col-md-8" style="padding: 0px;">
                                      <select required="required"  class="form-control" name="country" id="edit_country_id" tabindex="1">
                                          <option value="">Select Country</option>

                                          <?php foreach($countries as $data):?>
                                              <option <?php if($data->id == $client->country){ echo "selected"; } ?> value="<?=$data->id;?>"><?=$data->name;?></option>
                                          <?php endforeach;?>
                                          </select>
                                         
                                      </div>

                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <div class="col-md-4" style="margin-top: 5px;">
                                          <span style="font-weight: normal;">Region : </span>
                                      </div>
                                      <div class="col-md-8" style="padding: 0px;">
                                         
                                            
                                              <select required class="form-control" name="region" id="edit_region_id">
                                          <?php foreach($regions as $data):?>
                                              <option <?php if($client->region == $data->id){ echo "selected"; } ?> value="<?=$data->id;?>"><?=$data->name;?></option>
                                          <?php endforeach;?>
                                              </select>
                                         
                                      </div>

                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <div class="col-md-4" style="margin-top: 5px;">
                                          <span style="font-weight: normal;">Ville : </span>
                                      </div>
                                      <div class="col-md-8" style="padding: 0px;">
                                      <select required="required" class="form-control" name="ville" id="edit_cities_id" tabindex="1">
                                            <?php foreach($cities as $data):?>
                                              <option <?php if($client->ville == $data->id){ echo "selected"; } ?> value="<?=$data->id;?>"><?=$data->name;?></option>
                                          <?php endforeach;?>

                                      </select>
                                          
                                      </div>
                                  </div>
                              </div>
                          </div>
                    
                         </div>
                         <div class="col-md-3" style="text-align: center;margin-top:10px;">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                       
                                    </div>
                                    <div class="col-md-5" style="padding: 0px;">
                                        <?php if($client->client_image): ?>
                                            <img id="edit_preview_client" alt="Preview Logo"
                                        src="<?= base_url() ?>/uploads/clients/<?= $client->client_image  ?>"
                                        style="border: 1px solid #75b0d7;cursor: pointer;height: 210px;width: 210px;position: relative;z-index: 10;">
                                        <?php else: ?>
                                            <img id="edit_preview_client" alt="Preview Logo"
                                        src="<?= base_url() ?>/assets/images/no-preview.jpg"
                                        style="border: 1px solid #75b0d7;cursor: pointer;height: 210px;width: 210px;position: relative;z-index: 10;">
                                        <?php endif; ?>
                                    </div>
                                </div>      
                              </div>  
                          </div>      


                      
                 <!-- Section 2 -->
                 <!-- Row 1 -->
                                 <!-- Row 4 -->
                           <div class="col-md-12 pdhz" style="margin-top:10px;" >
                             
                                   <div class="col-md-3">
                                  <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Phone : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="phone" class="form-control" style="width:100%;" type="phone" value="<?= $client->phone ?>">
                                        </div>
                                    </div>
                                </div>
                             
                             <div class="col-md-3">
                                  <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Mobile : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="mobile" class="form-control" style="width:100%;" type="text" value="<?= $client->mobile ?>">
                                        </div>
                                    </div>
                                </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Fax : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input name="fax" class="form-control" type="text" value="<?= $client->fax ?>">
                                    </div>
                                </div>
                            </div>
                             <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Client Image : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input class="edit_client_image" name="client_image" type="file" >
                                    </div>

                                </div>
                            </div>
                           </div>
                          <div class="col-md-12 pdhz" style="margin-top: 10px;">
                            <div class="col-md-3">
                                   <div class="form-group" >
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Email : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="email" class="form-control" style="width:100%;" type="email" required value="<?= $client->email ?>">
                                        </div>
                                    </div>
                              </div> 
                            <div class="col-md-3">
                              <div class="form-group" >
                                  <div class="col-md-4" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Password : </span>
                                  </div>
                                  <div class="col-md-8" style="padding: 0px;">
                                      <input name="password" class="form-control" type="text" value="<?= date("dmY", strtotime($client->birthday)); ?>">
                                  </div>
                              </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Disabled Access: </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <select class="form-control" name="disabled" required>
                                            <option <?php if("0" == $client->disabled){ echo "selected"; } ?> value="0">No</option>
                                            <option <?php if("1"== $client->disabled){ echo "selected"; } ?> value="1">Yes</option>
                                          
                                        </select>
                                    </div>
                                </div>
                            </div>
                             <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                       <button class="btn btn-default" type="button">Send Access</button>
                                    </div>
                                    
                                </div>
                            </div>
                          </div>
                           

                        

                       <!-- Row 2 -->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Category : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <select class="form-control" name="client_type" required>
                                            <option value="">Select</option>
                                            <?php  foreach ($client_type_data as $item): ?>
                                                <option <?php if($item->id == $client->client_type){ echo "selected"; } ?> value="<?= $item->id ?>"><?= $item->type ?></option>
                                            <?php endforeach; ?> 
                                          
                                        </select>
                                    </div>
                                </div>
                            </div>

                           <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Payment Methode : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <select class="form-control" name="payment_method" required>
                                            <option value="">Select</option>
                                            <?php  foreach ($client_payment_data as $item): ?>
                                                <option <?php if($item->id == $client->payment_method){ echo "selected"; } ?> value="<?= $item->id ?>"><?= $item->payment ?></option>
                                            <?php endforeach; ?> 
                                          
                                        </select>
                                    </div>
                                </div>
                            </div>
                          
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Delay of Payment : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <select class="form-control" name="payment_delay" required>
                                            <option value="">Select</option>
                                            <?php  foreach ($client_delay_data as $item): ?>
                                                <option <?php if($item->id == $client->payment_delay){ echo "selected"; } ?> value="<?= $item->id ?>"><?= $item->delay ?></option>
                                            <?php endforeach; ?> 
                                          
                                        </select>
                                    </div>
                                </div>
                            </div>

                              
                            
                        </div>

                        <!-- Row 3 -->
                         <div class="col-md-12 pdlz" style="margin-top: 10px;">
                             <div class="col-md-6">
                                 <div class="form-group">
                                     <div class="col-md-2" style="margin-top: 5px;">
                                         <span style="font-weight: normal;">Note : </span>
                                     </div>
                                     <div class="col-md-10" style="padding: 0px;">
                                         <textarea rows="3" class="form-control" placeholder="write a note" id="note" name="note"><?= $client->note ?></textarea>
                                     </div>
                                 </div>
                             </div>  
                             
                         </div>
                   
                       
                       
                           <div class="col-md-12" style="padding: 30px;">                       
                             <!-- icons -->
                              <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                             <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelProfile()"><span class="fa fa-close"> Cancel </span></button>
                             <!-- icons -->  
                        </div>

                     <?php echo form_close(); ?>
 