<script type="text/javascript">
  $(document).ready(function() {
             profileEvent();
       
          $(document).delegate("input[type=text].datepicker", "focusin", function(){
             $(this).datepicker({

                   format: "dd/mm/yyyy"
             });
          });
           $(document).on('change','.client_image', function() {
                      readURL(this);
                  });
           $(document).on('change','.edit_client_image', function() {
                        editreadURL(this);
                    });
           $(document).on('change','#country_id', function() {
                        get_region_base_country(this);
                    });
           $(document).on('change','#region_id', function() {
                        get_cities_base_country_region(this);
                    });
           $(document).on('change','#edit_country_id', function() {
                       edit_get_region_base_country(this);
                    });
           $(document).on('change','#edit_region_id', function() {
                        edit_get_cities_base_country_region(this);
                    });
  });

  function profileEvent(){
        //removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="profileAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="profileEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="profileDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
function profileAdd()
{
   $('#adddisbreadcrumb').remove();
   $(".breadcrumb").append("<span id='adddisbreadcrumb'> > Add Client</span>");
  setupDivProfile();
  $(".Profileadd").show();
}

function cancelProfile()
{
  $('#adddisbreadcrumb').remove();
  $('#editdisbreadcrumb').remove();
  setupDivProfile();
  $(".ListProfile").show();
}

function profileEdit()
{
    var val = $('.chk-Addprofile-btn').val();
   
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
         $('#editdisbreadcrumb').remove();
         var profileid=$('.Addprofilefullid').val();
         $(".breadcrumb").append("<span id='editdisbreadcrumb'> > "+profileid+"</span>");
        setupDivProfile();
        $(".profileEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/clients/get_ajax_profile_data'; ?>',
                data: {'profile_id': val},
                success: function (result) {
                  document.getElementsByClassName("profileEditajax")[0].innerHTML = result;
                   /* innner config*/
                createinnerconfigtable();
                  /* innner config*/
                 
                }
            });
}
function profileidEdit(id,fullid){
  var val = id;
 
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      $('#editdisbreadcrumb').remove();
      $(".breadcrumb").append("<span id='editdisbreadcrumb'> > "+fullid+"</span>");
      setupDivProfile();
      $(".profileEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/clients/get_ajax_profile_data'; ?>',
              data: {'profile_id': val},
              success: function (result) {
                document.getElementsByClassName("profileEditajax")[0].innerHTML = result;
                /* innner config*/
                createinnerconfigtable();
                  /* innner config*/
              }
          });
}
function profileDelete()
{
  var val = $('.chk-Addprofile-btn').val();
  if(val != "")
  {
  setupDivProfile();
  $(".profileDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivProfile()
{
  $(".Profileadd").hide();
  $(".profileEdit").hide();
  $(".ListProfile").hide();
  $(".profileDelete").hide();

}
function setupDivProfileConfig()
{
  $(".Profileadd").hide();
  $(".profileEdit").hide();
  $(".ListProfile").show();
  $(".profileDelete").hide();

}
$('input.chk-mainprofile-template').on('change', function() {
  $('input.chk-mainprofile-template').not(this).prop('checked', false);
   var parent= $(this).parent();
  var sibling=$(parent).next().html();
  var id = $(this).attr('data-input');
  $('.chk-Addprofile-btn').val(id);
  $('#profiledeletid').val(id);
  $('.Addprofilefullid').val(sibling);
});

  function get_region_base_country(region_id=null)
   {
     country_id=$('#country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
         html ='';
         html =html + '<option value="">Select Region</option>';
         
         $.each(response, function( index, value ) {  
         if(region_id==value.id)
         {
           html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
         }
         else{
           html =html + '<option value="'+value.id+'">'+value.name+'</option>';
         }
         });
         $('#region_id').html(html);
       }
   }); 
}

function get_cities_base_country_region(region_id=null,cities_id=null)
{

region_id=$('#region_id').val();

$.ajax({
    url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
    method: 'post',
    data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
    dataType: 'json',
    success: function(response){
    console.log(response);
    html ='';
    html =html + ' <option value="">Select cities</option>';

    $.each(response, function( index, value ) {
    // console.log( value );
                

    if(cities_id==value.id)
    {
        html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';

    }
    else{
        html =html + '<option value="'+value.id+'">'+value.name+'</option>';

    }
    });

    $('#cities_id').html(html);


    }


});
}

 function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#preview_client').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
  }
//For Edit Page

function edit_get_cities_base_country_region(region_id=null,cities_id=null)
{
    region_id=$('#edit_region_id').val();
    $.ajax({
        url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
        method: 'post',
        data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
        dataType: 'json',
        success: function(response){
            html ='';
            html =html + ' <option value="">Select cities</option>';

            $.each(response, function( index, value ) {
            if(cities_id==value.id)
            {
                html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
            }
            else{
                html =html + '<option value="'+value.id+'">'+value.name+'</option>';
            }
            });
            $('#edit_cities_id').html(html);
        }
    });
}
   function edit_get_region_base_country(region_id=null)
   { 
     country_id=$('#edit_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
     html ='';
     html =html + '<option value="">Select Region</option>';
     
     $.each(response, function( index, value ) {
     if(region_id==value.id)
     {
       html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';  
     }
     else{
       html =html + '<option value="'+value.id+'">'+value.name+'</option>';
     }
     });
     $('#edit_region_id').html(html);
     }   
     }); 
   }

 function editreadURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#edit_preview_client').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
  }
//For Edit Page
</script>
