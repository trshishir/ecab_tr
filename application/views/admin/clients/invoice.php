<div class="row">
  <div class="ListInvoice" >
  <input type="hidden" class="chk-Addinvoice-btn" value="">
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" style="width:2%;">#</th>
            <th class="text-center" ><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('added_by'); ?></th>
            <th class=" text-center">Invoice</th>
            <th class=" text-center">Statut</th>
            <th class="text-center" >Since</th>

          </tr>
          </thead>
          <tbody>
                  <tr>
                    <td class="no-sort text-center"style="width:2%;">
                        <input type="checkbox" class="chk-maininvoice-template" data-input="123">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="invoiceidEdit('123')">1212120000</a></td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>

                  </tr>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>

<div class="Invoiceadd" style="display: none;" >
 <?php  include 'invoice/add.php';?> 
</div>
 
<div class="invoiceEdit" style="display:none">
  <?=form_open("admin/clients/invoiceEdit")?>
  <div class="invoiceEditajax">
  </div>
  <div class="col-md-12">                          
      <!-- icons -->
       <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
      <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelInvoice()"><span class="fa fa-close"> Cancel </span></button>
      <!-- icons -->   
   </div>  
</div>
 <?php echo form_close(); ?>
 
<div class="col-md-12 invoiceDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
  <?=form_open("admin/clients/invoiceDelete")?>
    <input  name="tablename" value="vbs_clientinvoice" type="hidden" >
    <input type="hidden" id="invoicedeletid" name="delet_invoice_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

    <a class="btn btn-default" style="cursor: pointer;" onclick="cancelInvoice()"><span class="delete-icon">No</span></a>
  <?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
function invoiceAdd()
{
  setupDivInvoice();
  $(".Invoiceadd").show();
}

function cancelInvoice()
{
  setupDivInvoice();
  $(".ListInvoice").show();
}

function invoiceEdit()
{
    var val = $('.chk-Addinvoice-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivInvoice();
        $(".invoiceEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/clients/get_ajax_invoice_data'; ?>',
                data: {'invoice_id': val},
                success: function (result) {
                  document.getElementsByClassName("invoiceEditajax")[0].innerHTML = result;
                }
            });
}
function invoiceidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivInvoice();
      $(".invoiceEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/clients/get_ajax_invoice_data'; ?>',
              data: {'invoice_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("invoiceEditajax")[0].innerHTML = result;
              }
          });
}
function invoiceDelete()
{
  var val = $('.chk-Addinvoice-btn').val();
  if(val != "")
  {
  setupDivInvoice();
  $(".invoiceDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivInvoice()
{
  $(".Invoiceadd").hide();
  $(".invoiceEdit").hide();
  $(".ListInvoice").hide();
  $(".invoiceDelete").hide();

}
function setupDivInvoiceConfig()
{
  $(".Invoiceadd").hide();
  $(".invoiceEdit").hide();
  $(".ListInvoice").show();
  $(".invoiceDelete").hide();

}
$('input.chk-maininvoice-template').on('change', function() {
  $('input.chk-maininvoice-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Addinvoice-btn').val(id);
  $('#invoicedeletid').val(id);
});

</script>
