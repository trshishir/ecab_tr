<div class="row">
  <div class="ListSupport" >
  <input type="hidden" class="chk-Addsupport-btn" value="">
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" style="width:2%;">#</th>
            <th class="text-center" ><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('added_by'); ?></th>
            <th class=" text-center">Support</th>
            <th class=" text-center">Statut</th>
            <th class="text-center" >Since</th>

          </tr>
          </thead>
          <tbody>
                  <tr>
                    <td class="no-sort text-center"style="width:2%;">
                        <input type="checkbox" class="chk-mainsupport-template" data-input="123">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="supportidEdit('123')">1212120000</a></td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>

                  </tr>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>

<div class="Supportadd" style="display: none;" >
 <?php  include 'support/add.php';?> 
</div>
 
<div class="supportEdit" style="display:none">
  <?=form_open("admin/clients/supportEdit")?>
  <div class="supportEditajax">
  </div>
  <div class="col-md-12">                          
      <!-- icons -->
       <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
      <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelSupport()"><span class="fa fa-close"> Cancel </span></button>
      <!-- icons -->   
   </div>  
</div>
 <?php echo form_close(); ?>
 
<div class="col-md-12 supportDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
  <?=form_open("admin/clients/supportDelete")?>
    <input  name="tablename" value="vbs_clientsupport" type="hidden" >
    <input type="hidden" id="supportdeletid" name="delet_support_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

    <a class="btn btn-default" style="cursor: pointer;" onclick="cancelSupport()"><span class="delete-icon">No</span></a>
  <?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
function supportAdd()
{
  setupDivSupport();
  $(".Supportadd").show();
}

function cancelSupport()
{
  setupDivSupport();
  $(".ListSupport").show();
}

function supportEdit()
{
    var val = $('.chk-Addsupport-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivSupport();
        $(".supportEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/clients/get_ajax_support_data'; ?>',
                data: {'support_id': val},
                success: function (result) {
                  document.getElementsByClassName("supportEditajax")[0].innerHTML = result;
                }
            });
}
function supportidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivSupport();
      $(".supportEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/clients/get_ajax_support_data'; ?>',
              data: {'support_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("supportEditajax")[0].innerHTML = result;
              }
          });
}
function supportDelete()
{
  var val = $('.chk-Addsupport-btn').val();
  if(val != "")
  {
  setupDivSupport();
  $(".supportDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivSupport()
{
  $(".Supportadd").hide();
  $(".supportEdit").hide();
  $(".ListSupport").hide();
  $(".supportDelete").hide();

}
function setupDivSupportConfig()
{
  $(".Supportadd").hide();
  $(".supportEdit").hide();
  $(".ListSupport").show();
  $(".supportDelete").hide();

}
$('input.chk-mainsupport-template').on('change', function() {
  $('input.chk-mainsupport-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Addsupport-btn').val(id);
  $('#supportdeletid').val(id);
});

</script>
