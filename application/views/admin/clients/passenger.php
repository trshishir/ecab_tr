<div class="row">
  <div class="ListPassenger" >
  <input type="hidden" class="chk-Addpassenger-btn" value="">
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" style="width:2%;">#</th>
            <th class="text-center" ><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('added_by'); ?></th>
            <th class=" text-center">Passenger</th>
            <th class=" text-center">Statut</th>
            <th class="text-center" >Since</th>

          </tr>
          </thead>
          <tbody>
                  <tr>
                    <td class="no-sort text-center"style="width:2%;">
                        <input type="checkbox" class="chk-mainpassenger-template" data-input="123">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="passengeridEdit('123')">1212120000</a></td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>

                  </tr>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>

<div class="Passengeradd" style="display: none;" >
 <?php  include 'passenger/add.php';?> 
</div>
 
<div class="passengerEdit" style="display:none">
  <?=form_open("admin/clients/passengerEdit")?>
  <div class="passengerEditajax">
  </div>
  <div class="col-md-12">                          
      <!-- icons -->
       <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
      <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelPassenger()"><span class="fa fa-close"> Cancel </span></button>
      <!-- icons -->   
   </div>  
</div>
 <?php echo form_close(); ?>
 
<div class="col-md-12 passengerDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
  <?=form_open("admin/clients/passengerDelete")?>
    <input  name="tablename" value="vbs_clientpassenger" type="hidden" >
    <input type="hidden" id="passengerdeletid" name="delet_passenger_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

    <a class="btn btn-default" style="cursor: pointer;" onclick="cancelPassenger()"><span class="delete-icon">No</span></a>
  <?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
function passengerAdd()
{
  setupDivPassenger();
  $(".Passengeradd").show();
}

function cancelPassenger()
{
  setupDivPassenger();
  $(".ListPassenger").show();
}

function passengerEdit()
{
    var val = $('.chk-Addpassenger-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivPassenger();
        $(".passengerEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/clients/get_ajax_passenger_data'; ?>',
                data: {'passenger_id': val},
                success: function (result) {
                  document.getElementsByClassName("passengerEditajax")[0].innerHTML = result;
                }
            });
}
function passengeridEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivPassenger();
      $(".passengerEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/clients/get_ajax_passenger_data'; ?>',
              data: {'passenger_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("passengerEditajax")[0].innerHTML = result;
              }
          });
}
function passengerDelete()
{
  var val = $('.chk-Addpassenger-btn').val();
  if(val != "")
  {
  setupDivPassenger();
  $(".passengerDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivPassenger()
{
  $(".Passengeradd").hide();
  $(".passengerEdit").hide();
  $(".ListPassenger").hide();
  $(".passengerDelete").hide();

}
function setupDivPassengerConfig()
{
  $(".Passengeradd").hide();
  $(".passengerEdit").hide();
  $(".ListPassenger").show();
  $(".passengerDelete").hide();

}
$('input.chk-mainpassenger-template').on('change', function() {
  $('input.chk-mainpassenger-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Addpassenger-btn').val(id);
  $('#passengerdeletid').val(id);
});

</script>
