<div class="row">
  <div class="ListMessage" >
  <input type="hidden" class="chk-Addmessage-btn" value="">
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" style="width:2%;">#</th>
            <th class="text-center" ><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" ><?php echo $this->lang->line('added_by'); ?></th>
            <th class=" text-center">Message</th>
            <th class=" text-center">Statut</th>
            <th class="text-center" >Since</th>

          </tr>
          </thead>
          <tbody>
                  <tr>
                    <td class="no-sort text-center"style="width:2%;">
                        <input type="checkbox" class="chk-mainmessage-template" data-input="123">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="messageidEdit('123')">1212120000</a></td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>
                      <td class="text-center">#</td>

                  </tr>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>

<div class="Messageadd" style="display: none;" >
 <?php  include 'message/add.php';?> 
</div>
 
<div class="messageEdit" style="display:none">
  <?=form_open("admin/clients/messageEdit")?>
  <div class="messageEditajax">
  </div>
  <div class="col-md-12">                          
      <!-- icons -->
       <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
      <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelMessage()"><span class="fa fa-close"> Cancel </span></button>
      <!-- icons -->   
   </div>  
</div>
 <?php echo form_close(); ?>
 
<div class="col-md-12 messageDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
  <?=form_open("admin/clients/messageDelete")?>
    <input  name="tablename" value="vbs_clientmessage" type="hidden" >
    <input type="hidden" id="messagedeletid" name="delet_message_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

    <a class="btn btn-default" style="cursor: pointer;" onclick="cancelMessage()"><span class="delete-icon">No</span></a>
  <?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
function messageAdd()
{
  setupDivMessage();
  $(".Messageadd").show();
}

function cancelMessage()
{
  setupDivMessage();
  $(".ListMessage").show();
}

function messageEdit()
{
    var val = $('.chk-Addmessage-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivMessage();
        $(".messageEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/clients/get_ajax_message_data'; ?>',
                data: {'message_id': val},
                success: function (result) {
                  document.getElementsByClassName("messageEditajax")[0].innerHTML = result;
                }
            });
}
function messageidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivMessage();
      $(".messageEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/clients/get_ajax_message_data'; ?>',
              data: {'message_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("messageEditajax")[0].innerHTML = result;
              }
          });
}
function messageDelete()
{
  var val = $('.chk-Addmessage-btn').val();
  if(val != "")
  {
  setupDivMessage();
  $(".messageDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivMessage()
{
  $(".Messageadd").hide();
  $(".messageEdit").hide();
  $(".ListMessage").hide();
  $(".messageDelete").hide();

}
function setupDivMessageConfig()
{
  $(".Messageadd").hide();
  $(".messageEdit").hide();
  $(".ListMessage").show();
  $(".messageDelete").hide();

}
$('input.chk-mainmessage-template').on('change', function() {
  $('input.chk-mainmessage-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Addmessage-btn').val(id);
  $('#messagedeletid').val(id);
});

</script>
