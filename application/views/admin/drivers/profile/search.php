       <div id="searchform" style="display: none;">
        <?php
        $attributes = array("name" => 'driver_search_form',"class"=>'form-inline', "id" => 'driver_search_form');
        echo form_open('admin/drivers/get_drivers_bysearch',$attributes);
        ?>
            
               <div class="form-group">
                 <select name="typecontrat" style="float: left;width:150px !important;margin-right: 2px;" class="form-control" >
                  <option value="">Contract</option>
                <?php foreach ($contract_data as $item): ?>
                  <option <?php if($search_contract == $item->id){ echo "selected"; } ?>  value="<?= $item->id ?>"><?= $item->contract ?></option>
                <?php endforeach; ?>
               
                </select> 
              </div>

               <div class="form-group">
                 <select name="nombredheuremensuel" style="float: left;width:150px !important;margin-right: 2px;" class="form-control" >
                  <option value="">Monthly Hours</option>
                    <?php foreach ($hours_data as $item): ?>
                <option <?php if($search_hours == $item->id){ echo "selected"; } ?> value="<?= $item->id ?>"><?= $item->hours ?></option>
                    <?php endforeach; ?>
               
                </select> 
              </div>
           
              <div class="form-group">
                 <input type="text" name="from_period" class="bsearchdatepicker bdr form-control" placeholder="From" value="<?= $search_from_period; ?>" autocomplete="off"/> 
              </div>
              <div class="form-group">
                 <input type="text" name="to_period" class="bsearchdatepicker bdr form-control" placeholder="To"  value="<?= $search_to_period; ?>" autocomplete="off"/> 
              </div>
               
  
               <div class="form-group">
                 <select name="statut" style="float: left;width:150px !important;" class="form-control">
                  <option value="">Statut</option>
                 <?php foreach ($driver_status_data as $item): ?>
                   <option  <?php if($item->id == $search_status){ echo "selected"; } ?> value="<?= $item->id ?>"><?= $item->status ?></option>
                   <?php endforeach; ?>
               
                </select> 
              </div>
            <div class="form-group">
              <button type="submit" class="btn btn-default searchdriverbtn" style="height: 27px;line-height: 3px;outline: none;margin-left: 5px;">Search</button>
            </div>
           <?php echo form_close(); ?>
       </div>