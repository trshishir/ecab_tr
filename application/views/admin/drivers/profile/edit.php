                          <?php
                                 $dateenaissance = strtotime($driver->dateenaissance);
                                 $dateenaissance = date('d/m/Y',$dateenaissance);
                                 $dateenaissance = $dateenaissance;


                                 $datedexpiration = strtotime($driver->datedexpiration);
                                 $datedexpiration = date('d/m/Y',$datedexpiration);
                                 $datedexpiration = $datedexpiration;


                                 $datedexpiration2 = strtotime($driver->datedexpiration2);
                                 $datedexpiration2 = date('d/m/Y',$datedexpiration2);
                                 $datedexpiration2 = $datedexpiration2;


                                 $datedexpiration3 = strtotime($driver->datedexpiration3);
                                 $datedexpiration3 = date('d/m/Y',$datedexpiration3);
                                 $datedexpiration3 = $datedexpiration3;

                                 $datedexpiration4 = strtotime($driver->datedexpiration4);
                                 $datedexpiration4 = date('d/m/Y',$datedexpiration4);
                                 $datedexpiration4 = $datedexpiration4;

                                 $datedexpiration5 = strtotime($driver->datedexpiration5);
                                 $datedexpiration5 = date('d/m/Y',$datedexpiration5);
                                 $datedexpiration5 = $datedexpiration5;

                                 $datedelivrance1 = strtotime($driver->datedelivrance1);
                                 $datedelivrance1 = date('d/m/Y',$datedelivrance1);
                                 $datedelivrance1 = $datedelivrance1;

                                 $datedelivrance2 = strtotime($driver->datedelivrance2);
                                 $datedelivrance2 = date('d/m/Y',$datedelivrance2);
                                 $datedelivrance2 = $datedelivrance2;

                                 $datedelivrance3 = strtotime($driver->datedelivrance3);
                                 $datedelivrance3 = date('d/m/Y',$datedelivrance3);
                                 $datedelivrance3 = $datedelivrance3;

                                 $datedelivrance4 = strtotime($driver->datedelivrance4);
                                 $datedelivrance4 = date('d/m/Y',$datedelivrance4);
                                 $datedelivrance4 = $datedelivrance4;

                                 $datedentree = strtotime($driver->datedentree);
                                 $datedentree = date('d/m/Y',$datedentree);
                                 $datedentree = $datedentree;

                                 $datedesortie = strtotime($driver->datedesortie);
                                 $datedesortie = date('d/m/Y',$datedesortie);
                                 $datedesortie = $datedesortie;

                                 $car_affect_date = strtotime($driver->car_affect_date);
                                 $car_affect_date = date('d/m/Y',$car_affect_date);
                                 $car_affect_date = $car_affect_date;

                             ?>         






             <?php echo form_open_multipart('admin/drivers/profileEdit'); ?>
                 <input type="hidden" value="<?= $driver->id ?>" name="profile_id" id="driver_id">
                 <div class="col-md-12 pdhz">
                                     <div class="col-md-9 pdhz">
                      <!-- Row 1 -->
                        <div class="col-md-12 pdhz" style="margin-top:10px;" >
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <span style="font-weight: normal;">Statut : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <select class="form-control" name="status" required>
                                               <?php foreach ($driver_status_data as $item): ?>
                                             <option <?php if($item->id == $driver->status){ echo "selected"; } ?> value="<?= $item->id ?>"><?= $item->status ?></option>
                                             <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                  <div class="form-group">
                                      <div class="col-md-4">
                                          <span style="font-weight: normal;">Car : </span>
                                      </div>
                                      <div class="col-md-8" style="padding: 0px;">
                                          <select class="form-control" name="car_id" required>
                                             <option value="">Select Car</option>
                                               <?php foreach ($cars as $item): ?>
                                          <option <?php if($driver->car_id == $item->id){ echo "selected"; } ?>  value="<?= $item->id ?>"><?= $this->drivers_model->getsinglerecord('vbs_carmarque',['id'=>$item->marque])->marque .' '.$this->drivers_model->getsinglerecord('vbs_carmodele',['id'=>$item->modele])->modele.' '.$item->immatriculation ?></option>
                                          <?php endforeach; ?>
                                          </select>
                                      </div>
                                  </div>
                                </div>
                                <div class="col-md-4"> 
                                      <div class="form-group">
                                          <div class="col-md-4" >
                                              <span style="font-weight: normal;">Car Affectation : </span>
                                          </div>
                                            <div class="col-md-4 pdlz">
                                                <input name="car_affect_date" class="form-control datepicker text-center" value="<?= $car_affect_date ?>" type="text">
                                            </div>
                                            <div class="col-md-4 pdrz">
                                                <input name="car_affect_time" id="edit_car_affect_time" class="form-control text-center" value="<?= (($driver->car_affect_time)?$driver->car_affect_time:date('h : i')); ?>" type="text">
                                            </div>
                                      </div>
                                 </div>
                            
                          </div>


                          <!-- Row 2 -->
                          <div class="col-md-12 pdhz" style="margin-top:10px;" >
                           <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">Civility : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <select class="form-control" name="civilite" required>
                                        <option value="">Select</option>
                                        <?php  foreach ($civilite_data as $item): ?>
                                            <option  <?php if($driver->civilite == $item->id){ echo "selected"; } ?> value="<?= $item->id ?>"><?= $item->civilite ?></option>
                                        <?php endforeach; ?> 
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">First Name : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input name="prenom" class="form-control" type="text" required value="<?=  $driver->prenom ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">Last Name : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input name="nom" class="form-control" type="text" required value="<?=  $driver->nom ?>">
                                    </div>

                                </div>
                            </div>                  
                          </div>

                          <!-- Row 3 -->
                          <div class="col-md-12 pdhz" style="margin-top:10px;" >
                             <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <span style="font-weight: normal;">Address : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="address" class="form-control" type="text"  value="<?=  $driver->address ?>">
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <span style="font-weight: normal;">Zip Code : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input name="postalcode" class="form-control" type="text"  value="<?=  $driver->postalcode ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">City : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                    <select required="required" class="form-control" name="villedenaissance" id="edit_cities_id" tabindex="1">
                                         <option value="">Select City</option>

                                        <?php foreach($cities as $data):?>
                                            <option <?php if($driver->villedenaissance == $data->id){ echo "selected"; } ?> value="<?=$data->id;?>"><?=$data->name;?></option>
                                        <?php endforeach;?>

                                    </select>
                                        <!-- <input name="villedenaissance" required="" class="form-control" type="text"> -->
                                    </div>
                                </div>
                            </div>
                                 <!--<div class="col-md-4">
                                     <div class="form-group">
                                         <div class="col-md-4">
                                             <span style="font-weight: normal;">City : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;">
                                             <input name="ville" class="form-control" type="text" value="<?=  $driver->ville ?>">
                                         </div>
                                     </div>
                                 </div>-->
                                
                               
                                </div>
                                <div class="col-md-12 pdhz" style="margin-top:10px;" >
                                   <div class="col-md-4">
                                       <div class="form-group">
                                           <div class="col-md-4">
                                               <span style="font-weight: normal;">Address 2 : </span>
                                           </div>
                                           <div class="col-md-8" style="padding: 0px;">
                                               <input name="address2" class="form-control" type="text"  value="<?=  $driver->address2 ?>">
                                           </div>
                                       </div>
                                   </div> 
                                   <div class="col-md-4">
                                       <div class="form-group">
                                           <div class="col-md-4">
                                               <span style="font-weight: normal;">Country : </span>
                                           </div>
                                           <div class="col-md-8" style="padding: 0px;">
                                           <select required="required"  class="form-control" name="paysdenaissance" id="edit_country_id" tabindex="1">
                                               <option value="">Select Country</option>

                                               <?php foreach($countries as $data):?>
                                                   <option <?php if($driver->paysdenaissance == $data->id){ echo "selected"; } ?> value="<?=$data->id;?>"><?=$data->name;?></option>
                                               <?php endforeach;?>
                                               </select>
                                               <!-- <input name="paysdenaissance" class="form-control" type="text"> -->
                                           </div>

                                       </div>
                                   </div>
                                    <div class="col-md-4">
                                       <div class="form-group">
                                           <div class="col-md-4">
                                               <span style="font-weight: normal;">Region : </span>
                                           </div>
                                           <div class="col-md-8" style="padding: 0px;">

                                           <select required="required" class="form-control" name="region" id="edit_region_id" tabindex="1">
                                                <option value="">Select Region</option>
                                           
                                               <?php foreach($regions as $data):?>
                                                   <option <?php if($driver->region == $data->id){ echo "selected"; } ?> value="<?=$data->id;?>"><?=$data->name;?></option>
                                               <?php endforeach;?>

                                           </select>
                                               <!-- <input name="villedenaissance" required="" class="form-control" type="text"> -->
                                           </div>
                                       </div>
                                   </div>
                                
                                  
                                </div>
                                
                          </div>
                          
                       
                           
                     
                         <div class="col-md-3" style="text-align: center;margin-top:10px;">
                                <div class="form-group">
                                    <div class="col-md-4">
                                       
                                    </div>
                                    <div class="col-md-5" style="padding: 0px;">
                                        <?php if($driver->driverImg): ?>
                                            <img id="edit_preview_driver" alt="Preview Logo"
                                        src="<?= base_url() ?>/uploads/drivers/<?= $driver->driverImg  ?>"
                                        style="border: 1px solid #75b0d7;cursor: pointer;height: 170px;width: 170px;position: relative;z-index: 10;background-color: #fff;">
                                        <?php else: ?>
                                            <img id="edit_preview_driver" alt="Preview Logo"
                                        src="<?= base_url() ?>/assets/images/no-preview.jpg"
                                        style="border: 1px solid #75b0d7;cursor: pointer;height: 170px;width: 170px;position: relative;z-index: 10;">
                                        <?php endif; ?>
                                    
                                
                                    </div>
                                </div>      
                              </div>  
                          </div>      

                      
                 <!-- Section 2 -->
                 <!-- Row 1 -->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">

                             <div class="col-md-3">
                                     <div class="form-group">
                                         <div class="col-md-4">
                                             <span style="font-weight: normal;">Date of Birth : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;position: relative;">
                                             <span id="edit_dob_span"></span>
                                             <input name="dateenaissance" id="edit_date_of_birth_field"  class="form-control datepicker" type="text" value="<?=  $dateenaissance ?>">
                                         </div>
                                     </div>
                                 </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                      <div class="col-md-4">
                                          <span style="font-weight: normal;">Phone : </span>
                                      </div>
                                      <div class="col-md-8" style="padding: 0px;">
                                          <input name="phone" class="form-control" style="width:100%;" type="phone" value="<?=  $driver->phone ?>">
                                      </div>
                                  </div>
                            </div>
                             <div class="col-md-3">
                             <div class="form-group">
                                   <div class="col-md-4">
                                       <span style="font-weight: normal;">Mobile Phone : </span>
                                   </div>
                                   <div class="col-md-8" style="padding: 0px;">
                                       <input name="mobile" class="form-control" style="width:100%;" type="phone"  value="<?=  $driver->mobile ?>">
                                   </div>
                               </div>
                           </div>

                            

                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="padding-left: 5px;">
                                        <span style="font-weight: normal;">Driver Image : </span>
                                    </div>
                                    <div class="col-md-8" style="padding-left: 5px;">
                                        <input class="edit_driver_image" name="driverImg" type="file" >
                                    </div>

                                </div>
                            </div>
                        </div>
                          <div class="col-md-12 pdhz" style="margin-top:10px;" >
                            
                                    <div class="col-md-6" style="padding-left: 30px;">
                                     <div class="col-md-12 send-access-div">
                                    
                                     <div class="col-md-12 pdhz" style="margin-top: 5px;">
                                          <div class="col-md-5 pdhz">
                                                <div class="form-group">
                                                    <div class="col-md-4">
                                                        <span style="font-weight: normal;">Email : </span>
                                                    </div>
                                                    <div class="col-md-8" style="padding: 0px;">
                                                        <input name="email" id="driver_email"  class="form-control" type="email" value="<?=  $driver->email ?>">
                                                    </div>
                                                </div>
                                         </div>
                                         <div class="col-md-5 pdhz">
                                           <div class="form-group">
                                               <div class="col-md-4">
                                                   <span style="font-weight: normal;">Password : </span>
                                               </div>
                                               <div class="col-md-8" style="padding: 0px;">
                                                   <input name="password" id="driver_password"  class="form-control" type="password" value="<?= date("dmY", strtotime($driver->dateenaissance)) ?>">
                                               </div>
                                            </div>
                                          </div>
                                           <div class="col-md-2">
                                                <button type="button" class="btn btn-default" onclick="sendaccessdetail();">Send Access</button>
                                            </div>
                                     </div>
                                               
                                  </div>         
                             </div>
                         </div>

                       <!-- Row 2 -->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">Position : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <select class="form-control" name="poste" required>
                                            <option value="">Select</option>
                                            <?php  foreach ($post_data as $item): ?>
                                                <option  <?php if($driver->poste == $item->id){ echo "selected"; } ?> value="<?= $item->id ?>"><?= $item->post ?></option>
                                            <?php endforeach; ?> 
                                          
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">Entry date :</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="datedentree" class="form-control datepicker"
                                            type="text" value="<?=  $datedentree ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">Exit Date : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="datedesortie" class="form-control datepicker"
                                            type="text" value="<?=  $datedesortie ?>">
                                    </div>

                                </div>
                            </div>
                          
                            <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <span style="font-weight: normal;">Reason :</span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <select class="form-control" name="motif" required>
                                                <option value="">Select</option>
                                                <?php foreach ($pattern_data as $item): ?>
                                            <option <?php if($driver->motif == $item->id){ echo "selected"; } ?> value="<?= $item->id ?>"><?= $item->pattern ?></option>
                                                <?php endforeach; ?>
                                                
                                            </select>
                                        </div>
                                    </div>
                                    
                                </div> 
                            
                        </div>
                     <!-- Row 3 -->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="padding-right: 0px;font-size: 11px;">
                                        <span style="font-weight: normal;">Contract Category : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <select class="form-control" name="typecontrat" required>
                                             <option value="">Select</option>
                                                <?php foreach ($contract_data as $item): ?>
                                            <option <?php if($driver->typecontrat == $item->id){ echo "selected"; } ?>  value="<?= $item->id ?>"><?= $item->contract ?></option>
                                                <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">Contract Kind :</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <select class="form-control" name="natureducontrat" required>
                                            <option  value="">Select</option>
                                                <?php foreach ($nature_data as $item): ?>
                                            <option <?php if($driver->natureducontrat == $item->id){ echo "selected"; } ?> value="<?= $item->id ?>"><?= $item->nature ?></option>
                                                <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" >
                                        <span style="font-weight: normal;">Monthly Hours : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <select class="form-control" name="nombredheuremensuel" required>
                                           <option value="">Select</option>
                                                <?php foreach ($hours_data as $item): ?>
                                            <option <?php if($driver->nombredheuremensuel == $item->id){ echo "selected"; } ?> value="<?= $item->id ?>"><?= $item->hours ?></option>
                                                <?php endforeach; ?>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" style="padding-right: 0px;font-size: 11px;">
                                         <span style="font-weight: normal;">Working Days Per Week : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <select class="form-control" name="driverworkingdays" required>
                                            <option value="">Select</option>
                                            <option <?php if($driver->driverworkingdays =="1"){ echo "selected"; } ?> value="1">1</option>
                                            <option <?php if($driver->driverworkingdays =="2"){ echo "selected"; } ?> value="2">2</option>
                                            <option <?php if($driver->driverworkingdays =="3"){ echo "selected"; } ?> value="3">3</option>
                                            <option <?php if($driver->driverworkingdays =="4"){ echo "selected"; } ?> value="4">4</option>
                                            <option <?php if($driver->driverworkingdays =="5"){ echo "selected"; } ?> value="5">5</option>
                                            <option <?php if($driver->driverworkingdays =="6"){ echo "selected"; } ?> value="6">6</option>  
                                         </select>
                                     </div>

                                 </div>
                             </div>                                  
                        </div>

                        <!--Row 3 a 2 -->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="padding-right: 0px;font-size: 11px;">
                                        <span style="font-weight: normal;">Social Security Number :</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                            <input required="" name="numerodesecuritesociale" class="form-control" type="text" value="<?= $driver->numerodesecuritesociale ?>">
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">Upload : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input  name="numerodesecuritesocialefile" type="file">
                                    </div>
                                </div>
                            </div>
                           
                        </div>

                        <!--  Row 3 b-->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">Identity Category : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <select class="form-control" name="typedepiecedidentite" required>
                                             <option value="">Select</option>
                                                <?php foreach ($type_data as $item): ?>
                                            <option <?php if($driver->typedepiecedidentite == $item->id){ echo "selected"; } ?> value="<?= $item->id ?>"><?= $item->type ?></option>
                                                <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">Upload : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input  name="upload1" type="file">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" >
                                        <span style="font-weight: normal;">Identity Number :</span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input required="" name="numerropiecedidentite" class="form-control"
                                            type="text" value="<?=  $driver->numerropiecedidentite ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">Expiry Date : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input  name="datedexpiration" id="editdatedexpiration1" class="form-control datepicker" type="text" value="<?=  $datedexpiration ?>">
                                    </div>

                                </div>
                            </div>

                        </div>

                      <!-- Row 4 -->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="padding-right: 0px;font-size: 11px;">
                                        <span style="font-weight: normal;">Driver Licence Number : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input  name="pumeropermis" class="form-control" type="text" value="<?=  $driver->pumeropermis ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">Upload : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input  name="upload2" type="file">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">Delivery Date : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input  name="datedelivrance1" class="form-control datepicker" type="text" value="<?=  $datedelivrance1 ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                            
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <span style="font-weight: normal;">Expiry Date : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px; ">
                                            <input  name="datedexpiration2" id="editdatedexpiration2" class="form-control datepicker" type="text" value="<?=  $datedexpiration2 ?>">
                                        </div>

                                    </div>
                            </div>

                        </div>

                      <!-- Row 5 -->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="padding-right: 0px;">
                                        <span style="font-weight: normal;">Medical Certificate : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input  name="certificatmedicate" type="file">
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">Delivery Date : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input  name="datedelivrance2" class="form-control datepicker" type="text" value="<?=  $datedelivrance2 ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">Expiry Date : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input  name="datedexpiration3" id="editdatedexpiration3" class="form-control datepicker" type="text" value="<?=  $datedexpiration3 ?>">
                                    </div>

                                </div>
                            </div>
                             <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">Diploma1 : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input  name="autrdeiplome1" type="file">
                                    </div>
                                </div>
                            </div>
                           
                        </div>

                        <!-- Row 6 -->
                       <div class="col-md-12 pdhz" style="margin-top: 10px;">

                          <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">PSC1 : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input  name="psc1" type="file">
                                    </div>
                                </div>
                            </div>



                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">Delivery Date : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input  name="datedelivrance3" class="form-control datepicker"  type="text" value="<?=  $datedelivrance3 ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">Expiry Date : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input  name="datedexpiration4" id="editdatedexpiration4" class="form-control datepicker" type="text"  value="<?=  $datedexpiration4 ?>">
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">Diploma2 : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input  name="autrediplome2" type="file">
                                    </div>
                                </div>
                            </div>
                           
                        </div>

                      <!-- Row 7 -->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">

                            
 
                                    

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">Work Medecine : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input  name="medecinedetravai" type="file">
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">Delivery Date : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input  name="datedelivrance4" class="form-control datepicker"  type="text" value="<?=  $datedelivrance4 ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">Expiry Date : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input  name="datedexpiration5" id="editdatedexpiration5" class="form-control  datepicker"  type="text" value="<?=  $datedexpiration5 ?>">
                                    </div>

                                </div>
                            </div>
                             <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <span style="font-weight: normal;">Diploma3 : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                        <input  name="autrediplome3" type="file">
                                    </div>

                                </div>
                            </div>

                        </div>
                        <!-- Row 8 -->
                    

                        <div class="col-md-12 pdlz" style="margin-top: 20px;">
           <!--Reminder and Notification -->
    <div class="col-md-6" >
        <!-- Notification -->
            <?php  include 'notifications.php';?>
        <!-- Notification -->
        <!-- Reminders -->
            <?php  include 'reminders.php';?>
        <!-- Reminder -->
        <!-- Custom Reminders -->
            <?php  include 'customreminders.php';?>
        <!-- Custom Reminders -->
     </div>
     <!--Reminder and Notification -->


                     <div class="col-md-5 col-md-offset-1" style="padding-right: 0px;">
                         <div class="map-wrapper" >
                            <div id="map"></div>
                         </div>
                        <input id="mapaddress" type="hidden" value="<?= $driver->address ?>" />
                        <input id="mapaddress2" type="hidden" value="<?= $driver->address2 ?>" />
                        <input id="mapzipcode" type="hidden" value="<?= $driver->postalcode ?>" />
                        <input id="mapcity" type="hidden" value="<?= $this->drivers_model->getSingleRecord('vbs_cities',['id'=>$driver->villedenaissance])->name ?>" />
                        <input id="mapmobile" type="hidden" value="<?= $driver->mobile ?>" />
                        <input id="mapphone" type="hidden" value="<?= $driver->phone ?>" />
                        <input id="mapname" type="hidden" value="<?= $this->drivers_model->getSingleRecord('vbs_drivercivilite',['id'=>$driver->civilite])->civilite.' '.$driver->prenom.' '.$driver->nom ?>" />
                        <input id="mapdriverimg" type="hidden" value="<?= $driver->driverImg ?>" />
                        <input id="mapdriverdefaultimg" type="hidden" value="male-default.jpg" />
                    </div>
                </div>
               
                           <div class="col-md-12">                       
                             <!-- icons -->
                              <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                             <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelProfile()"><span class="fa fa-close"> Cancel </span></button>
                             <!-- icons -->  
                          </div>

                     <?php echo form_close(); ?>
 