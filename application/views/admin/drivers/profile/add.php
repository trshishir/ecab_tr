             <?php echo form_open_multipart('admin/drivers/profileAdd'); ?>

                   <div class="col-md-12 pdhz">
                     <div class="col-md-9 pdhz">
                       <!-- Row 1 -->
                         <div class="col-md-12 pdhz" style="margin-top:10px;" >
                                 <div class="col-md-4">
                                     <div class="form-group">
                                         <div class="col-md-4" >
                                             <span style="font-weight: normal;">Statut : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;">
                                             <select class="form-control" name="status" required>
                                                  <?php foreach ($driver_status_data as $item): ?>
                                             <option  value="<?= $item->id ?>"><?= $item->status ?></option>
                                             <?php endforeach; ?>
                                             </select>
                                         </div>
                                     </div>
                                 </div>

                                 

                                 <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-4" >
                                            <span style="font-weight: normal;">Car : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <select class="form-control" name="car_id" required>
                                               <option value="">Select Car</option>
                                                 <?php foreach ($cars as $item): ?>
                                            <option  value="<?= $item->id ?>"><?= $this->drivers_model->getsinglerecord('vbs_carmarque',['id'=>$item->marque])->marque .' '.$this->drivers_model->getsinglerecord('vbs_carmodele',['id'=>$item->modele])->modele.' '.$item->immatriculation ?></option>
                                            <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                 </div>
                                  <div class="col-md-4">
                                     <div class="form-group">
                                         <div class="col-md-4" >
                                             <span style="font-weight: normal;">Car Affectation : </span>
                                         </div>
                                           <div class="col-md-4 pdlz">
                                               <input name="car_affect_date" class="form-control datepicker text-center" value="<?php echo date('d/m/Y');?>" type="text">
                                           </div>
                                           <div class="col-md-4 pdrz">
                                               <input name="car_affect_time" id="car_affect_time" class="form-control text-center" value="<?= date('h : i'); ?>" type="text">
                                           </div>
                                     </div>
                                 </div>
                              
                           </div>


 

                           <!-- Row 2 -->
                           <div class="col-md-12 pdhz" style="margin-top:10px;" >
                            <div class="col-md-4">
                                     <div class="form-group">
                                         <div class="col-md-4" >
                                             <span style="font-weight: normal;">Civility : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;">
                                             <select class="form-control" name="civilite" required>
                                                   <option value="">Select</option>
                                                 <?php foreach ($civilite_data as $item): ?>
                                             <option value="<?= $item->id ?>"><?= $item->civilite ?></option>
                                                 <?php endforeach; ?>
                                             </select>
                                         </div>

                                     </div>
                                 </div>
                              <div class="col-md-4">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">First Name : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input name="prenom" class="form-control" type="text" required>
                                     </div>
                                 </div>
                             </div>
                             <div class="col-md-4">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Last Name : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input name="nom" class="form-control" type="text" required>
                                     </div>

                                 </div>
                             </div>

                           

                            
                           </div>

                           <!-- Row 3 -->
                           <div class="col-md-12 pdhz" style="margin-top: 10px;">
                              <div class="col-md-4">
                                  
                                     <div class="form-group">
                                         <div class="col-md-4" >
                                             <span style="font-weight: normal;">Address : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;">
                                             <input name="address" class="form-control" type="text" placeholder="address 1">
                                         </div>
                                     </div>
                              
                               
                                </div>
                              
                                 <div class="col-md-4">
                                     <div class="form-group">
                                         <div class="col-md-4" >
                                             <span style="font-weight: normal;">Zip Code : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;">
                                             <input name="postalcode" class="form-control" type="text" >
                                         </div>
                                     </div>
                                 </div>
                            <div class="col-md-4">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">City : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                     <select required="required" class="form-control" name="villedenaissance" id="cities_id" tabindex="1">

                                     </select>
                                         <!-- <input name="villedenaissance" required="" class="form-control" type="text"> -->
                                     </div>
                                 </div>
                             </div>
                                  <!--<div class="col-md-4">
                                     <div class="form-group">
                                         <div class="col-md-4" >
                                             <span style="font-weight: normal;">City : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;">
                                             <input name="ville" class="form-control" type="text" >
                                         </div>
                                     </div>
                                 </div>-->

                           </div>
                        <div class="col-md-12 pdhz" style="margin-top:10px;" >
                              <div class="col-md-4">  
                                     <div class="form-group">
                                         <div class="col-md-4" >
                                             <span style="font-weight: normal;">Address 2 : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;">
                                             <input name="address2" class="form-control" type="text" placeholder="address 2">
                                         </div>
                                     </div>
                                 </div>
                                  <div class="col-md-4">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Country : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                     <select required="required"  class="form-control" name="paysdenaissance" id="country_id" tabindex="1">
                                         <option value="">Select Country</option>

                                         <?php foreach($countries as $data):?>
                                             <option value="<?=$data->id;?>"><?=$data->name;?></option>
                                         <?php endforeach;?>
                                         </select>
                                         <!-- <input name="paysdenaissance" class="form-control" type="text"> -->
                                     </div>

                                 </div>
                             </div>
                             <div class="col-md-4">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Region : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                        <div class="form-group">
                                           
                                             <select required class="form-control" name="region" id="region_id"></select>
                                         </div>
                                     </div>

                                 </div>
                             </div>
                              

                              
                           </div> 
                        


                          </div>
                          <div class="col-md-3" style="text-align: center;margin-top:10px;">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                        
                                     </div>
                                     <div class="col-md-5" style="padding: 0px;">
                                     <img id="preview_driver" alt="Preview Logo"
                                         src="<?= base_url() ?>/assets/images/no-preview.jpg"
                                         style="border: 1px solid #75b0d7;cursor: pointer;height: 170px;width: 170px;position: relative;z-index: 10;background-color: #fff;">
                                 
                                     </div>
                                 </div>      
                               </div>  
                           </div>      

                       
                  <!-- Section 2 -->
                  <!-- Row 1 -->
                                              <!-- Row 4 -->
                            <div class="col-md-12 pdhz">
                                
                            <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Date of Birth : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                        <span id="dob_span"></span>
                                         <input name="dateenaissance" id="date_of_birth_field" class="form-control datepicker" value="<?php echo date('d/m/Y');?>"
                                             type="text">
                                     </div>
                                 </div>
                            </div>  
                            <div class="col-md-3">
                               <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Phone : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input name="phone" class="form-control" style="width:100%;" type="phone">
                                     </div>
                                 </div>
                            </div>
                            <div class="col-md-3">
                               <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Mobile Phone : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input name="mobile" class="form-control" style="width:100%;" type="phone">
                                     </div>
                                 </div>
                            </div>
                            <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Driver Image : </span>
                                     </div>
                                     <div class="col-md-8">
                                         <input class="driver_image" name="driverImg" type="file" required>
                                     </div>

                                 </div>
                             </div>
                            </div>
                            <div class="col-md-12 pdhz" style="margin-top: 10px;">
                                <div class="col-md-3">
                                    <div class="form-group" >
                                         <div class="col-md-4" >
                                             <span style="font-weight: normal;">Email : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;">
                                             <input name="email" class="form-control" style="width:100%;" type="email" required>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="col-md-3">
                                  
                                 </div>
                            </div>
                            
                        <!-- Row 2 -->


                         <div class="col-md-12 pdhz" style="margin-top: 10px;">

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Position : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <select class="form-control" name="poste" required>
                                             <option value="">Select</option>
                                             <?php  foreach ($post_data as $item): ?>
                                                 <option value="<?= $item->id ?>"><?= $item->post ?></option>
                                             <?php endforeach; ?> 
                                           
                                         </select>
                                     </div>
                                 </div>
                             </div>

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Entry date : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input required="" name="datedentree" class="form-control datepicker" value="<?php echo date('d/m/Y');?>"
                                             type="text">
                                     </div>
                                 </div>
                             </div>

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Exit Date : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input required="" name="datedesortie" class="form-control datepicker" value="<?php echo date('d/m/Y');?>"
                                             type="text">
                                     </div>

                                 </div>
                             </div>
                           
                             <div class="col-md-3">
                                     <div class="form-group">
                                         <div class="col-md-4" >
                                             <span style="font-weight: normal;">Reason : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;">
                                             <select class="form-control" name="motif" required>
                                                 <option value="">Select</option>
                                                 <?php foreach ($pattern_data as $item): ?>
                                             <option value="<?= $item->id ?>"><?= $item->pattern ?></option>
                                                 <?php endforeach; ?>
                                                 
                                             </select>
                                         </div>
                                     </div>
                                     
                                 </div> 
                             
                         </div>
                      <!-- Row 3 -->
                         <div class="col-md-12 pdhz" style="margin-top: 10px;">

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" style="padding-right: 0px;font-size: 11px;">
                                         <span style="font-weight: normal;">Contract Category : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <select class="form-control" name="typecontrat" required>
                                              <option value="">Select</option>
                                                 <?php foreach ($contract_data as $item): ?>
                                             <option value="<?= $item->id ?>"><?= $item->contract ?></option>
                                                 <?php endforeach; ?>
                                         </select>
                                     </div>
                                 </div>
                             </div>

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Contract Kind : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <select class="form-control" name="natureducontrat" required>
                                             <option value="">Select</option>
                                                 <?php foreach ($nature_data as $item): ?>
                                             <option value="<?= $item->id ?>"><?= $item->nature ?></option>
                                                 <?php endforeach; ?>
                                         </select>
                                     </div>
                                 </div>
                             </div>

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Monthly Hours : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <select class="form-control" name="nombredheuremensuel" required>
                                            <option value="">Select</option>
                                                 <?php foreach ($hours_data as $item): ?>
                                             <option value="<?= $item->id ?>"><?= $item->hours ?></option>
                                                 <?php endforeach; ?>
                                         </select>
                                     </div>

                                 </div>
                             </div>
                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" style="padding-right: 0px;font-size: 11px;">
                                         <span style="font-weight: normal;">Working Days Per Week : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <select class="form-control" name="driverworkingdays" required>
                                            <option value="">Select</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>  
                                         </select>
                                     </div>

                                 </div>
                             </div>

                            
                                                               
                         </div>
                         <!--Row 3 a 2 -->
                         <div class="col-md-12 pdhz" style="margin-top: 10px;">
                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" style="padding-right: 0px;font-size: 11px;">
                                         <span style="font-weight: normal;">Social Security Number : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                             <input required="" name="numerodesecuritesociale" class="form-control" type="text">
                                     </div>

                                 </div>
                             </div>
                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Upload : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input  name="numerodesecuritesocialefile" type="file">
                                     </div>
                                 </div>
                             </div>
                            
                         </div>

                         <!--  Row 3 b-->
                         <div class="col-md-12 pdhz" style="margin-top: 10px;">

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Identity Category : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <select class="form-control" name="typedepiecedidentite" required>
                                              <option value="">Select</option>
                                                 <?php foreach ($type_data as $item): ?>
                                             <option value="<?= $item->id ?>"><?= $item->type ?></option>
                                                 <?php endforeach; ?>
                                         </select>
                                     </div>
                                 </div>
                             </div>
                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Upload : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input required="" name="upload1" type="file">
                                     </div>
                                 </div>
                             </div>

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Identity Number : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input required="" name="numerropiecedidentite" class="form-control"
                                             type="text">
                                     </div>
                                 </div>
                             </div>

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Expiry Date : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input  name="datedexpiration" class="form-control datepicker" value="<?php echo date('d/m/Y');?>"
                                             type="text">
                                     </div>

                                 </div>
                             </div>

                         </div>

                       <!-- Row 4 -->
                         <div class="col-md-12 pdhz" style="margin-top: 10px;">

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" style="padding-right: 0px;font-size: 11px;">
                                         <span style="font-weight: normal;">Driver Licence Number : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input  name="pumeropermis" class="form-control" type="text">
                                     </div>
                                 </div>
                             </div>
                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Upload : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input  name="upload2" type="file">
                                     </div>
                                 </div>
                             </div>

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Delivery Date : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input  name="datedelivrance1" class="form-control datepicker" value="<?php echo date('d/m/Y');?>"
                                             type="text">
                                     </div>
                                 </div>
                             </div>

                             <div class="col-md-3">
                             
                                     <div class="form-group">
                                         <div class="col-md-4" >
                                             <span style="font-weight: normal;">Expiry Date : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;">
                                             <input  name="datedexpiration2" class="form-control datepicker" value="<?php echo date('d/m/Y');?>"
                                                 type="text">
                                         </div>

                                     </div>
                             </div>

                         </div>

                       <!-- Row 5 -->
                         <div class="col-md-12 pdhz" style="margin-top: 10px;">

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" style="padding-right: 0px;">
                                         <span style="font-weight: normal;">Medical Certificate : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input  name="certificatmedicate" type="file">
                                     </div>
                                 </div>
                             </div>


                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Delivery Date : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input  name="datedelivrance2" class="form-control datepicker" value="<?php echo date('d/m/Y');?>"
                                             type="text">
                                     </div>
                                 </div>
                             </div>

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Expiry Date : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input  name="datedexpiration3" class="form-control datepicker" value="<?php echo date('d/m/Y');?>"
                                             type="text">
                                     </div>

                                 </div>
                             </div>
                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Diploma1 : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input  name="autrdeiplome1" type="file">
                                     </div>
                                 </div>
                             </div>
                            
                         </div>

                         <!-- Row 6 -->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">

                           <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">PSC1 : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input  name="psc1" type="file">
                                     </div>
                                 </div>
                             </div>



                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Delivery Date : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input  name="datedelivrance3" class="form-control datepicker" value="<?php echo date('d/m/Y');?>"
                                             type="text">
                                     </div>
                                 </div>
                             </div>

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Expiry Date : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input  name="datedexpiration4" class="form-control datepicker" value="<?php echo date('d/m/Y');?>"
                                             type="text">
                                     </div>

                                 </div>
                             </div>
                              <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Diploma2 : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input  name="autrediplome2" type="file">
                                     </div>
                                 </div>
                             </div>
                            
                         </div>

                       <!-- Row 7 -->
                         <div class="col-md-12 pdhz" style="margin-top: 10px;">

                             
                  
                                     

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Work Medecine : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input  name="medecinedetravai" type="file">
                                     </div>
                                 </div>
                             </div>


                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Delivery Date : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input  name="datedelivrance4" class="form-control datepicker" value="<?php echo date('d/m/Y');?>"
                                             type="text">
                                     </div>
                                 </div>
                             </div>

                             <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Expiry Date : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input  name="datedexpiration5" class="form-control  datepicker"
                                           value="<?php echo date('d/m/Y');?>"   type="text">
                                     </div>

                                 </div>
                             </div>
                               <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" >
                                         <span style="font-weight: normal;">Diploma3 : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input  name="autrediplome3" type="file">
                                     </div>

                                 </div>
                             </div>

                         </div>
                         <!-- Row 8 -->
                        
                          <div class="col-md-12" style="padding-bottom: 20px;">                       
                              <!-- icons -->
                               <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                              <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelProfile()"><span class="fa fa-close"> Cancel </span></button>
                              <!-- icons -->  
                          </div>

                     <?php echo form_close(); ?>
 