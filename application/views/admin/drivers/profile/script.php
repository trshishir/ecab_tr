<script type="text/javascript">
 
   
  $(document).ready(function() {

     $("#searchform").appendTo("#DataTables_Table_0_filter");
     $("#searchform").show();
             profileEvent();
       
          $(document).delegate("input[type=text].datepicker", "focusin", function(){
             $(this).datepicker({

                   format: "dd/mm/yyyy"
             });
          });
            $(document).delegate("input[type=text]#customremindertimepicker", "focusin", function(){
             $(this).timepicki({
                   step_size_minutes:'5',
                    show_meridian:false,
                    min_hour_value:0,
                    max_hour_value:23,
                    overflow_minutes:true,
                    increase_direction:'up',
                    disable_keyboard_mobile: true
             });
          });
           var notificationsubject = ["identitytimepicker", "lisencetimepicker", "medicaltimepicker", "psctimepicker", "medicinetravaitimepicker"];
           for(var i=0;i<notificationsubject.length;i++){
              $(document).delegate("input[type=text]#"+notificationsubject[i], "focusin", function(){
               $(this).timepicki({
                     step_size_minutes:'5',
                      show_meridian:false,
                      min_hour_value:0,
                      max_hour_value:23,
                      overflow_minutes:true,
                      increase_direction:'up',
                      disable_keyboard_mobile: true
               });
            });
           }

           var notificationsubject = ["identityremindertimepicker", "lisenceremindertimepicker", "medicalremindertimepicker", "pscremindertimepicker", "medicinetravairemindertimepicker"];
           for(var i=0;i<notificationsubject.length;i++){
              $(document).delegate("input[type=text]#"+notificationsubject[i], "focusin", function(){
               $(this).timepicki({
                     step_size_minutes:'5',
                      show_meridian:false,
                      min_hour_value:0,
                      max_hour_value:23,
                      overflow_minutes:true,
                      increase_direction:'up',
                      disable_keyboard_mobile: true
               });
            });
           }
             $(document).delegate("input[type=text]#car_affect_time", "focusin", function(){
              $(this).timepicki({
                    step_size_minutes:'5',
                     show_meridian:false,
                     min_hour_value:0,
                     max_hour_value:23,
                     overflow_minutes:true,
                     increase_direction:'up',
                     disable_keyboard_mobile: true
              });
           });
               $(document).delegate("input[type=text]#edit_car_affect_time", "focusin", function(){
                $(this).timepicki({
                      step_size_minutes:'5',
                       show_meridian:false,
                       min_hour_value:0,
                       max_hour_value:23,
                       overflow_minutes:true,
                       increase_direction:'up',
                       disable_keyboard_mobile: true
                });
             });
           
           for(var j=1;j<6;j++){
              $(document).on('change','#editdatedexpiration'+j, function() {
                var exprieddate=$(this).val();
                exprieddate = exprieddate.split("/");
                exprieddate=exprieddate[2] + "-"+exprieddate[1]+"-"+exprieddate[0];
               
                var today = new Date();
                var dd = String(today.getDate()).padStart(2, '0');
                var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = today.getFullYear();

                today =  yyyy + '-' + mm + '-'+ dd;
                var d1 = Date.parse(today);
                var d2 = Date.parse(exprieddate);
                if (d1 > d2) {
                    $(this).removeClass("greenbg");
                    $(this).addClass("redbg");
                }else{
                    $(this).removeClass("redbg");
                    $(this).addClass("greenbg");
                }
                      
            });
           }
           for(var j=1;j<6;j++){
              $(document).on('change','#datedexpiration'+j, function() {
                var exprieddate=$(this).val();
                exprieddate = exprieddate.split("/");
                exprieddate=exprieddate[2] + "-"+exprieddate[1]+"-"+exprieddate[0];
               
                var today = new Date();
                var dd = String(today.getDate()).padStart(2, '0');
                var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = today.getFullYear();

                today =  yyyy + '-' + mm + '-'+ dd;
                var d1 = Date.parse(today);
                var d2 = Date.parse(exprieddate);
                if (d1 > d2) {
                    $(this).removeClass("greenbg");
                    $(this).addClass("redbg");
                }else{
                    $(this).removeClass("redbg");
                    $(this).addClass("greenbg");
                }
                      
            });
           }

          


           $(document).on('change','.driver_image', function() {
                      readURL(this);
                  });
           $(document).on('change','.edit_driver_image', function() {
                        editreadURL(this);
                    });
           $(document).on('change','#country_id', function() {
                        get_region_base_country(this);
                    });
           $(document).on('change','#region_id', function() {
                        get_cities_base_country_region(this);
                    });
           $(document).on('change','#edit_country_id', function() {
                       edit_get_region_base_country(this);
                    });
           $(document).on('change','#edit_region_id', function() {
                        edit_get_cities_base_country_region(this);
                    });
            $(document).on('change','#driver_kind', function() {
                     var driver_kind=document.getElementById('driver_kind').value;
                     if(driver_kind == "2"){
                      document.getElementById('driver_category_div').style.display="block";

                     }else{
                      document.getElementById('driver_category_div').style.display="none";
                     }
                  });
            $(document).on('change','#edit_driver_kind', function() {
                   var edit_driver_kind=document.getElementById('edit_driver_kind').value;
                   if(edit_driver_kind == "2"){
                    document.getElementById('edit_driver_category_div').style.display="block";
                    
                   }else{
                    document.getElementById('edit_driver_category_div').style.display="none";
                   }
              });
            $(document).on('change','#date_of_birth_field', function() {
                          var date   = $(this).val();
                          var age = getAge(date);
                          document.getElementById('dob_span').innerHTML=age;
                        
                    });
            $(document).on('change','#edit_date_of_birth_field', function() {
                          var date   = $(this).val();
                          var age = getAge(date);
                          document.getElementById('edit_dob_span').innerHTML=age;
                        
                    });
            

  });

  function profileEvent(){
        //removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="profileAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="profileEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="profileDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
function profileAdd()
{
   $('#adddisbreadcrumb').remove();
   $(".breadcrumb").append("<span id='adddisbreadcrumb'> > Add Driver</span>");
  setupDivProfile();
  $(".Profileadd").show();
}

function cancelProfile()
{
  $('#adddisbreadcrumb').remove();
  $('#editdisbreadcrumb').remove();
  setupDivProfile();
  $(".ListProfile").show();
}

function profileEdit()
{
    var val = $('.chk-Addprofile-btn').val();
   
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
         $('#editdisbreadcrumb').remove();
         var profileid=$('.Addprofilefullid').val();
         $(".breadcrumb").append("<span id='editdisbreadcrumb'> > "+profileid+"</span>");
        setupDivProfile();
        $(".profileEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/drivers/get_ajax_profile_data'; ?>',
                data: {'profile_id': val},
                success: function (result) {
                  document.getElementsByClassName("profileEditajax")[0].innerHTML = result;
                  colorchange();
                  var date   = $('#edit_date_of_birth_field').val();
                  var age = getAge(date);
                  document.getElementById('edit_dob_span').innerHTML=age;
                  initMap();
                 
                   /* innner config*/
                createinnerconfigtable();
                  /* innner config*/
                 
                }
            });
}
function profileidEdit(id,fullid){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      $('#editdisbreadcrumb').remove();
      $(".breadcrumb").append("<span id='editdisbreadcrumb'> > "+fullid+"</span>");
      setupDivProfile();
      $(".profileEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/drivers/get_ajax_profile_data'; ?>',
              data: {'profile_id': val},
              success: function (result) {

                document.getElementsByClassName("profileEditajax")[0].innerHTML = result;
                 colorchange();
                 var date   = $('#edit_date_of_birth_field').val();
                 var age = getAge(date);
                 document.getElementById('edit_dob_span').innerHTML=age;
                 initMap();
                  
                /* innner config*/
                createinnerconfigtable();
                  /* innner config*/
              }
          });
}
function profileDelete()
{
  var val = $('.chk-Addprofile-btn').val();
  if(val != "")
  {
  setupDivProfile();
  $(".profileDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivProfile()
{
  $(".Profileadd").hide();
  $(".profileEdit").hide();
  $(".ListProfile").hide();
  $(".profileDelete").hide();

}
function setupDivProfileConfig()
{
  $(".Profileadd").hide();
  $(".profileEdit").hide();
  $(".ListProfile").show();
  $(".profileDelete").hide();

}
$('input.chk-mainprofile-template').on('change', function() {
  $('input.chk-mainprofile-template').not(this).prop('checked', false);
   var parent= $(this).parent();
  var sibling=$(parent).next().html();
  var id = $(this).attr('data-input');
  $('.chk-Addprofile-btn').val(id);
  $('#profiledeletid').val(id);
  $('.Addprofilefullid').val(sibling);
});

  function get_region_base_country(region_id=null)
   {
     country_id=$('#country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
         html ='';
         html =html + '<option value="">Select Region</option>';
         
         $.each(response, function( index, value ) {  
         if(region_id==value.id)
         {
           html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
         }
         else{
           html =html + '<option value="'+value.id+'">'+value.name+'</option>';
         }
         });
         $('#region_id').html(html);
       }
   }); 
}

function get_cities_base_country_region(region_id=null,cities_id=null)
{

region_id=$('#region_id').val();

$.ajax({
    url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
    method: 'post',
    data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
    dataType: 'json',
    success: function(response){
    console.log(response);
    html ='';
    html =html + ' <option value="">Select cities</option>';

    $.each(response, function( index, value ) {
    // console.log( value );
                

    if(cities_id==value.id)
    {
        html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';

    }
    else{
        html =html + '<option value="'+value.id+'">'+value.name+'</option>';

    }
    });

    $('#cities_id').html(html);


    }


});
}

 function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#preview_driver').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
  }
//For Edit Page

function edit_get_cities_base_country_region(region_id=null,cities_id=null)
{
    region_id=$('#edit_region_id').val();
    $.ajax({
        url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
        method: 'post',
        data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
        dataType: 'json',
        success: function(response){
            html ='';
            html =html + ' <option value="">Select cities</option>';

            $.each(response, function( index, value ) {
            if(cities_id==value.id)
            {
                html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
            }
            else{
                html =html + '<option value="'+value.id+'">'+value.name+'</option>';
            }
            });
            $('#edit_cities_id').html(html);
        }
    });
}
   function edit_get_region_base_country(region_id=null)
   { 
     country_id=$('#edit_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
     html ='';
     html =html + '<option value="">Select Region</option>';
     
     $.each(response, function( index, value ) {
     if(region_id==value.id)
     {
       html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';  
     }
     else{
       html =html + '<option value="'+value.id+'">'+value.name+'</option>';
     }
     });
     $('#edit_region_id').html(html);
     }   
     }); 
   }

 function editreadURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#edit_preview_driver').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
  }
  //custom reminder
function addcustomreminderbox(){
 
  var title=$('#customremindertitlefield').val();
  var date=$('#customreminderdatefield').val();
  var time=$('#customremindertimepicker').val();
  var comment=$('#customremindercommentfield').val();
  var driver_id=$('#driver_id').val();
  
  if(title== '' || date== '' || time== '' || comment== ''){
    alert("Please fill the form");
  }else{
       $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/drivers/addcustomreminders'; ?>',
                data: {'driver_id': driver_id,'title':title,'date': date,'time':time,'comment':comment},
                success: function (data) {
                  
                   if(data.result=='200'){
                       
                           
                            $('#customreminderrecorded').append('<div class="customreminderbox col-md-12 pdhz" style="margin-top:5px;"><div class="col-md-10 pdhz"><div class="col-md-3 text-left pdlz">'+data.record.title+'</div><div class="col-md-3 text-left pdlz">'+data.record.date+'</div><div class="col-md-3 text-left pdlz">'+data.record.time+'</div><div class="col-md-3 text-left pdlz" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">'+data.record.comment+'</div></div><div class="col-md-2" style="position: relative;width: 8%;"><button type="button" onclick="removecustomreminderbox(this,'+data.record.customreminderid+');" class="minusrediconcustomreminder"><i class="fa fa-minus" style="margin:0px !important;"></i></button></div></div>');
                              $('#customremindertitlefield').val('');
                              $('#customremindercommentfield').val('');
                 }
                else{
                       
                        alert("something went wrong");
                   }
                  


                     
                }
       });
  }  
}
function removecustomreminderbox(e,customreminderid){
  if(customreminderid == ''){
    alert("something went wrong");
  }else{
    $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/drivers/removecustomreminders'; ?>',
                data: {'customreminderid': customreminderid},
                success: function (data) {
                   if(data.result=='200'){
                        $(e).closest(".customreminderbox").remove();                            
                   }
                   else{ 
                        alert("something went wrong");
                   }    
                }
       });
    }
}
//custom reminder
//send access detail
function sendaccessdetail(){
    var email=document.getElementById('driver_email').value;
    var password=document.getElementById('driver_password').value;
    var id=document.getElementById('driver_id').value;
    
    var form_Data=new FormData();
    form_Data.append('email',email);
    form_Data.append('password',password);
    form_Data.append('id',id);
    form_Data.append('<?php  echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash(); ?>');
  
    $.ajax({
      type: "POST",
      url: "<?= base_url() .'admin/drivers/sendaccessdetail' ?>",
      data:form_Data,
      processData: false,
      cache:false,
      contentType: false,
      success: function(data) {
        if(data.result=='200'){
             alert("Email Sent Successfully.");
        }else{
             alert("something went wrong");
        } 
      }
    });
  }
//send access detail
//add driver notification
function adddrivernotification(column1,column2,date_id,time_id,loader_id,title,maindivid){
  document.getElementById(loader_id).style.display = "block";
  var date=document.getElementById(date_id).value;
  var time=document.getElementById(time_id).value;
  var driver_id=document.getElementById('driver_id').value;
  var form_Data=new FormData();
  form_Data.append('date',date);
  form_Data.append('time',time);
  form_Data.append('driver_id',driver_id);
  form_Data.append('column1',column1);
  form_Data.append('column2',column2);
  form_Data.append('<?php  echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash(); ?>');
      $.ajax({
      type: "POST",
      url: "<?= base_url() .'admin/drivers/adddrivernotification' ?>",
      data:form_Data,
      processData: false,
      cache:false,
      contentType: false,
      success: function(data) {
           if(data.result=='200'){
               document.getElementById(loader_id).style.display = "none";
               $(maindivid).append('<div class="col-md-12 pdhz"><div class="col-md-2 pdhz text-center" >'+date+'</div><div class="col-md-2 pdhz text-center" >'+time+'</div><div class="col-md-3 pdhz text-center" ><div class="col-md-10 col-md-offset-2 pdhz text-left">'+title+'</div></div><div class="col-md-1 pdhz text-center" ><a href="<?= base_url(); ?>admin/drivers/notification_pdf/'+data.record.notification_id+'/<?= $driver->id ?>/1" target="_blank"><img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="pdf icon"></a></div><div class="col-md-2 pdhz text-center" >'+data.record.status+'</div></div>');
               if(data.record.status.toLowerCase() == "sent"){
                    alert("Notification sent via Email.");
               }else{
                 alert("Notification will be send via Emai at "+date+" "+time);
               }
         }
        else{
                document.getElementById(loader_id).style.display = "none";
                alert("something went wrong");
           }
      }
    });
}
//add driver notification
//add driver reminder
function adddriverreminder(column1,column2,date_id,time_id,loader_id,title,maindivid){
  document.getElementById(loader_id).style.display = "block";
  var date=document.getElementById(date_id).value;
  var time=document.getElementById(time_id).value;
  var driver_id=document.getElementById('driver_id').value;
  var form_Data=new FormData();
  form_Data.append('date',date);
  form_Data.append('time',time);
  form_Data.append('driver_id',driver_id);
  form_Data.append('column1',column1);
  form_Data.append('column2',column2);
  form_Data.append('<?php  echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash(); ?>');
      $.ajax({
      type: "POST",
      url: "<?= base_url() .'admin/drivers/adddriverreminder' ?>",
      data:form_Data,
      processData: false,
      cache:false,
      contentType: false,
      success: function(data) {
           if(data.result=='200'){
               document.getElementById(loader_id).style.display = "none";
               $(maindivid).append('<div class="col-md-12 pdhz"><div class="col-md-2 pdhz text-center" >'+date+'</div><div class="col-md-2 pdhz text-center" >'+time+'</div><div class="col-md-3 pdhz text-center" ><div class="col-md-10 col-md-offset-2 pdhz text-left">'+title+'</div></div><div class="col-md-1 pdhz text-center" ><a href="<?= base_url(); ?>admin/drivers/reminder_pdf/'+data.record.reminder_id+'/'+driver_id+'/1/1" target="_blank"><img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="pdf icon"></a></div><div class="col-md-2 pdhz text-center" >'+data.record.status+'</div></div>');
               if(data.record.status.toLowerCase() == "sent"){
                    alert("Reminder sent via Email.");
               }else{
                 alert("Reminder will be send via Emai at "+date+" "+time);
               }
         }
        else{
                document.getElementById(loader_id).style.display = "none";
                alert("something went wrong");
           }
      }
    });
}
//add driver reminder
function colorchange(){
         for(var j=1;j<6;j++){
          
    
         var exprieddate=$('#editdatedexpiration'+j).val();
         exprieddate = exprieddate.split("/");
         exprieddate=exprieddate[2] + "-"+exprieddate[1]+"-"+exprieddate[0];
        
         var today = new Date();
         var dd = String(today.getDate()).padStart(2, '0');
         var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
         var yyyy = today.getFullYear();

         today =  yyyy + '-' + mm + '-'+ dd;
         var d1 = Date.parse(today);
         var d2 = Date.parse(exprieddate);

         if (d1 > d2) {
          
           $('#editdatedexpiration'+j).addClass("redbg");
           }else{
          
           $('#editdatedexpiration'+j).addClass("greenbg");
         }
               
       
    }
}
                         
//For Edit Page
//calculate age

function getAge(date) {
  
   date   = date.split("/");
   days   = date[0];
   months = date[1];
   years  = date[2];
   dateString   = date[1]+"/"+date[0]+"/"+date[2]
 

  var now = new Date();
  var today = new Date(now.getYear(),now.getMonth(),now.getDate());

  var yearNow = now.getYear();
  var monthNow = now.getMonth();
  var dateNow = now.getDate();

  var dob = new Date(dateString.substring(6,10),
                     dateString.substring(0,2)-1,                   
                     dateString.substring(3,5)                  
                     );

  var yearDob = dob.getYear();
  var monthDob = dob.getMonth();
  var dateDob = dob.getDate();
  var age = {};
  var ageString = "";
  var yearString = "";
  var monthString = "";
  var dayString = "";


  yearAge = yearNow - yearDob;

  if (monthNow >= monthDob)
    var monthAge = monthNow - monthDob;
  else {
    yearAge--;
    var monthAge = 12 + monthNow -monthDob;
  }

  if (dateNow >= dateDob)
    var dateAge = dateNow - dateDob;
  else {
    monthAge--;
    var dateAge = 31 + dateNow - dateDob;

    if (monthAge < 0) {
      monthAge = 11;
      yearAge--;
    }
  }

  age = {
      years: yearAge,
      months: monthAge,
      days: dateAge
      };

  if ( age.years > 1 ) yearString = " years";
  else yearString = " year";
  if ( age.months> 1 ) monthString = " months";
  else monthString = " month";
  if ( age.days > 1 ) dayString = " days";
  else dayString = " day";


  if ( (age.years > 0) && (age.months > 0) && (age.days > 0) )
    ageString = age.years + yearString + ", " + age.months + monthString + ", and " + age.days + dayString;
  else if ( (age.years == 0) && (age.months == 0) && (age.days > 0) )
    ageString = age.days + dayString;
  else if ( (age.years > 0) && (age.months == 0) && (age.days == 0) )
    ageString = age.years + yearString;
  else if ( (age.years > 0) && (age.months > 0) && (age.days == 0) )
    ageString = age.years + yearString + " and " + age.months + monthString;
  else if ( (age.years == 0) && (age.months > 0) && (age.days > 0) )
    ageString = age.months + monthString + " and " + age.days + dayString;
  else if ( (age.years > 0) && (age.months == 0) && (age.days > 0) )
    ageString = age.years + yearString + " and " + age.days + dayString;
  else if ( (age.years == 0) && (age.months > 0) && (age.days == 0) )
    ageString = age.months + monthString;
  else ageString = "0";

  return ageString;
}
//calculate age
</script>
