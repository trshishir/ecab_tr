 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="col-md-12 pdhz" style="font-weight: 900;">NOTIFICATIONS : </div>
            <div class="col-md-12 pdhz">  
               <div class="col-md-2 pdhz text-center" style="font-weight: 900;">Date</div>
               <div class="col-md-2 pdhz text-center" style="font-weight: 900;">Time</div>
               <div class="col-md-3 pdhz text-center" style="font-weight: 900;">Subject</div>
               <div class="col-md-1 pdhz text-center" style="font-weight: 900;">File</div>
               <div class="col-md-2 pdhz text-center" style="font-weight: 900;">Statut</div>
               <div class="col-md-2 pdhz text-center" style="font-weight: 900;">Resend</div>
            </div>        


 <!-- Identity Notification -->  
  <?php $a=1; ?>
 <?php foreach ($drivernotifications as $notification): ?>
          <?php if(!empty($notification->typeiddate) || $notification->typeiddate != null ): ?>  
            <?php if($a==1): ?>
            
        <div class="col-md-12 pdhz">
            <div class="col-md-2 ">                             
             <input  class="datepicker form-control" id="identitydatepicker"  type="text" value="<?= from_unix_date($notification->typeiddate); ?>" name="identitydatefield"  autocomplete="off" style="text-align: center;" /> 
            </div>
            <div class="col-md-2 ">
             <input  class="form-control" id="identitytimepicker"  type="text" value="<?= $notification->typeidtime ?>" name="identitytimefield"   style="width:100%;text-align: center;" />   
            </div>
            <div class="col-md-3 pdhz text-center"><div class="col-md-10 col-md-offset-2 pdhz text-left">Identity Card Expired</div></div>
            <div class="col-md-1 pdhz text-center" >
                             <a href="<?= base_url(); ?>admin/drivers/notification_pdf/<?= $notification->id  ?>/<?= $driver->id ?>/1" target="_blank">
                               <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                             </a>
                 </div>
            <div class="col-md-2 pdhz text-center" ><?= ($notification->typeidissent == '1')?"Sent":"Pending"; ?></div>
            <div class="col-md-2 pdhz text-center" >
              <div id="identitysentloaderbtn" style="position: relative;display: none;">
                <img src="<?= base_url()?>assets/theme/default/images/bookloader.gif" style="width: 28px;height: 28px;position: absolute;left: -16px;top: 0px;">
              </div>
              <button type="button" class="btn btn-default" id="identitysentbtn" style="height: 30px;line-height: 1px;" onclick="adddrivernotification('typeiddate','typeidtime','identitydatepicker','identitytimepicker','identitysentloaderbtn','Identity Card Expired','#drivernotificationmaindiv_1')">Send</button>
            </div>      
        </div>
       <?php $a++; ?>
       <?php endif;?>
    <?php endif;?>
<?php endforeach; ?>

     <?php $a1=0; ?>
        <div class="col-md-12 pdhz" id="drivernotificationmaindiv_1">
        <?php foreach ($drivernotifications as $notification): ?>
          <?php if(!empty($notification->typeiddate) || $notification->typeiddate != null): ?>
            <?php if($a1 != 0): ?>
            <div class="col-md-12 pdhz">          
                 <div class="col-md-2 pdhz text-center" ><?= from_unix_date($notification->typeiddate); ?></div>
                 <div class="col-md-2 pdhz text-center" ><?= $notification->typeidtime ?></div>
                 <div class="col-md-3 pdhz text-center" ><div class="col-md-10 col-md-offset-2 pdhz text-left">Identity Card Expired</div></div> 
                  <div class="col-md-1 pdhz text-center" >
                             <a href="<?= base_url(); ?>admin/drivers/notification_pdf/<?= $notification->id  ?>/<?= $driver->id ?>/1" target="_blank">
                               <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                             </a>
                 </div>
                 <div class="col-md-2 pdhz text-center" ><?= ($notification->typeidissent == '1')?"Sent":"Pending"; ?></div>
            </div> 
            <?php endif;?>
            <?php $a1++; ?>
          <?php endif;?>
         <?php endforeach; ?> 
         </div>  

          <!-- Identity Notification -->   


          <!-- Lisence Notification -->  
            <?php $b=1; ?> 
           <?php foreach ($drivernotifications as $notification): ?>
                     <?php if(!empty($notification->permitnumberdate) || $notification->permitnumberdate != null): ?> 
                       <?php if($b==1): ?>
                 <div class="col-md-12 pdhz" style="margin-top: 10px;">
                     <div class="col-md-2 ">                             
                        <input  class="datepicker form-control" id="lisencedatepicker"  type="text" value="<?= from_unix_date($notification->permitnumberdate); ?>" name="lisencedatefield"  autocomplete="off" style="text-align: center;" />   
                     </div>
                     <div class="col-md-2 ">
                      <input  class="form-control" id="lisencetimepicker"  type="text" value="<?= $notification->permitnumbertime ?>" name="lisencetimefield"   style="width:100%;text-align: center;" />       
                     </div>
                     <div class="col-md-3 pdhz text-center" ><div class="col-md-10 col-md-offset-2 pdhz text-left">Driver Licence Expired</div></div> 
                     <div class="col-md-1 pdhz text-center" >
                     <a href="<?= base_url(); ?>admin/drivers/notification_pdf/<?= $notification->id  ?>/<?= $driver->id ?>/2" target="_blank">
                       <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                     </a>
                     </div>
                     <div class="col-md-2 pdhz text-center" ><?= ($notification->permitnumberissent == '1')?"Sent":"Pending"; ?></div>
                     <div class="col-md-2 pdhz text-center" >
                       <div id="lisencesentloaderbtn" style="position: relative;display: none;">
                         <img src="<?= base_url()?>assets/theme/default/images/bookloader.gif" style="width: 28px;height: 28px;position: absolute;left: -16px;top: 0px;">
                       </div>
                       <button type="button" class="btn btn-default" id="lisencesentbtn" style="height: 30px;line-height: 1px;" onclick="adddrivernotification('permitnumberdate','permitnumbertime','lisencedatepicker','lisencetimepicker','lisencesentloaderbtn','Driver Licence Expired','#drivernotificationmaindiv_2')">Send</button>
                     </div>      
                 </div>
                 <?php $b++; ?>
                   <?php endif;?>
                <?php endif;?>
            <?php endforeach; ?>

               <?php $a1=0; ?>
                  <div class="col-md-12 pdhz" id="drivernotificationmaindiv_2">
                   <?php foreach ($drivernotifications as $notification): ?>
                     <?php if(!empty($notification->permitnumberdate) || $notification->permitnumberdate != null): ?>
                      <?php if($a1 != 0): ?>
                     <div class="col-md-12 pdhz">
                                  
                          <div class="col-md-2 pdhz text-center" ><?= from_unix_date($notification->permitnumberdate); ?></div>
                          <div class="col-md-2 pdhz text-center" ><?= $notification->permitnumbertime ?></div>
                          <div class="col-md-3 pdhz text-center" ><div class="col-md-10 col-md-offset-2 pdhz text-left">Driver Licence Expired</div></div> 
                           <div class="col-md-1 pdhz text-center" >
                                      <a href="<?= base_url(); ?>admin/drivers/notification_pdf/<?= $notification->id  ?>/<?= $driver->id ?>/2" target="_blank">
                                        <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                                      </a>
                          </div>
                          <div class="col-md-2 pdhz text-center" ><?= ($notification->permitnumberissent == '1')?"Sent":"Pending"; ?></div>
                     </div>
                     <?php endif;?>
                    <?php $a1++; ?>
                   <?php endif; ?>
                   <?php endforeach; ?>  
                   </div> 
                  <!-- Lisence Notification -->   

    <!-- Medical Notification -->  
      <?php $c=1; ?>
    <?php foreach ($drivernotifications as $notification): ?>
               <?php if(!empty($notification->medicalcertificatedate) || $notification->medicalcertificatedate != null): ?>  
                 <?php if($c==1): ?>
           <div class="col-md-12 pdhz" style="margin-top: 10px;">
               <div class="col-md-2 ">                             
                  <input  class="datepicker form-control" id="medicaldatepicker"  type="text" value="<?= from_unix_date($notification->medicalcertificatedate); ?>" name="medicaldatefield"  autocomplete="off" style="text-align: center;" />   
               </div>
               <div class="col-md-2 ">
                <input  class="form-control" id="medicaltimepicker"  type="text" value="<?= $notification->medicalcertificatetime ?>" name="medicaltimefield"   style="width:100%;text-align: center;" />       
               </div>
             <div class="col-md-3 pdhz text-center" ><div class="col-md-10 col-md-offset-2 pdhz text-left">Medical Certificate Expired</div></div> 
              <div class="col-md-1 pdhz text-center" >
                         <a href="<?= base_url(); ?>admin/drivers/notification_pdf/<?= $notification->id  ?>/<?= $driver->id ?>/3" target="_blank">
                           <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                         </a>
             </div>
             <div class="col-md-2 pdhz text-center" ><?= ($notification->medicalcertificateissent == '1')?"Sent":"Pending"; ?></div>
               <div class="col-md-2 pdhz text-center" >
                 <div id="medicalsentloaderbtn" style="position: relative;display: none;">
                   <img src="<?= base_url()?>assets/theme/default/images/bookloader.gif" style="width: 28px;height: 28px;position: absolute;left: -16px;top: 0px;">
                 </div>
                 <button type="button" class="btn btn-default" id="medicalsentbtn" style="height: 30px;line-height: 1px;" onclick="adddrivernotification('medicalcertificatedate','medicalcertificatetime','medicaldatepicker','medicaltimepicker','medicalsentloaderbtn','Medical Certificate Expired','#drivernotificationmaindiv_3')">Send</button>
               </div>      
           </div>
              <?php $c++; ?>
             <?php endif;?>
           <?php endif; ?>
          <?php endforeach; ?>  


          <?php $c1=0; ?>
            <div class="col-md-12 pdhz" id="drivernotificationmaindiv_3">
             <?php foreach ($drivernotifications as $notification): ?>
               <?php if(!empty($notification->medicalcertificatedate) || $notification->medicalcertificatedate != null): ?>
                <?php if($c1 != 0): ?>
               <div class="col-md-12 pdhz">
                            
                    <div class="col-md-2 pdhz text-center" ><?= from_unix_date($notification->medicalcertificatedate); ?></div>
                    <div class="col-md-2 pdhz text-center" ><?= $notification->medicalcertificatetime ?></div>
                    <div class="col-md-3 pdhz text-center" ><div class="col-md-10 col-md-offset-2 pdhz text-left">Medical Certificate Expired</div></div> 
                     <div class="col-md-1 pdhz text-center" >
                                <a href="<?= base_url(); ?>admin/drivers/notification_pdf/<?= $notification->id  ?>/<?= $driver->id ?>/3" target="_blank">
                                  <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                                </a>
                    </div>
                    <div class="col-md-2 pdhz text-center" ><?= ($notification->medicalcertificateissent == '1')?"Sent":"Pending"; ?></div>
               </div>
               <?php endif;?>
            <?php $c1++; ?>
             <?php endif; ?>
             <?php endforeach; ?>   
           </div>
            <!-- Medical Notification -->    


            <!-- PSC Notification -->  
              <?php $d=1; ?> 
             <?php foreach ($drivernotifications as $notification): ?>
                       <?php if(!empty($notification->psc1date) || $notification->psc1date != null): ?> 
                         <?php if($d==1): ?>
                   <div class="col-md-12 pdhz" style="margin-top: 10px;">
                       <div class="col-md-2 ">                             
                          <input  class="datepicker form-control" id="pscdatepicker"  type="text" value="<?= from_unix_date($notification->psc1date); ?>" name="pscdatefield"  autocomplete="off" style="text-align: center;" />   
                       </div>
                       <div class="col-md-2 ">
                        <input  class="form-control" id="psctimepicker"  type="text" value="<?= $notification->psc1time ?>" name="psctimefield"   style="width:100%;text-align: center;" />       
                       </div>
                       <div class="col-md-3 pdhz text-center" ><div class="col-md-10 col-md-offset-2 pdhz text-left">PSC1 Expired</div></div>  
                        <div class="col-md-1 pdhz text-center" >
                                   <a href="<?= base_url(); ?>admin/drivers/notification_pdf/<?= $notification->id  ?>/<?= $driver->id ?>/4" target="_blank">
                                     <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                                   </a>
                       </div>
                       <div class="col-md-2 pdhz text-center" ><?= ($notification->psc1issent == '1')?"Sent":"Pending"; ?></div>
                       <div class="col-md-2 pdhz text-center" >
                         <div id="pscsentloaderbtn" style="position: relative;display: none;">
                           <img src="<?= base_url()?>assets/theme/default/images/bookloader.gif" style="width: 28px;height: 28px;position: absolute;left: -16px;top: 0px;">
                         </div>
                         <button type="button" class="btn btn-default" id="pscsentbtn" style="height: 30px;line-height: 1px;" onclick="adddrivernotification('psc1date','psc1time','pscdatepicker','psctimepicker','pscsentloaderbtn','PSC1 Expired','#drivernotificationmaindiv_4')">Send</button>
                       </div>      
                   </div>
                   <?php $d++; ?>
                    <?php endif;?>
                    <?php endif; ?>
                    <?php endforeach; ?>


                   <?php $d1=0; ?>
                    <div class="col-md-12 pdhz" id="drivernotificationmaindiv_4">
                     <?php foreach ($drivernotifications as $notification): ?>
                       <?php if(!empty($notification->psc1date) || $notification->psc1date != null): ?>
                        <?php if($d1 != 0): ?>
                       <div class="col-md-12 pdhz">
                                   
                            <div class="col-md-2 pdhz text-center" ><?= from_unix_date($notification->psc1date); ?></div>
                            <div class="col-md-2 pdhz text-center" ><?= $notification->psc1time ?></div>
                            <div class="col-md-3 pdhz text-center" ><div class="col-md-10 col-md-offset-2 pdhz text-left">PSC1 Expired</div></div>  
                             <div class="col-md-1 pdhz text-center" >
                                        <a href="<?= base_url(); ?>admin/drivers/notification_pdf/<?= $notification->id  ?>/<?= $driver->id ?>/4" target="_blank">
                                          <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                                        </a>
                            </div>
                            <div class="col-md-2 pdhz text-center" ><?= ($notification->psc1issent == '1')?"Sent":"Pending"; ?></div>
                       </div>
                       <?php endif;?>
                      <?php $d1++; ?>
                     <?php endif; ?>
                     <?php endforeach; ?>
                     </div>   
                    <!-- PSC Notification -->   


      <!-- Medicine Travel Notification -->    
      <?php $e=1; ?>
      <?php foreach ($drivernotifications as $notification): ?>
                      <?php if(!empty($notification->medicineoftravaidate) || $notification->medicineoftravaidate != null): ?>
                         <?php if($e==1): ?>
             <div class="col-md-12 pdhz" style="margin-top: 10px;">
                 <div class="col-md-2 ">                             
                    <input  class="datepicker form-control" id="medicinetravaidatepicker"  type="text" value="<?= from_unix_date($notification->medicineoftravaidate); ?>" name="medicinetravaidatefield"  autocomplete="off" style="text-align: center;" />   
                 </div>
                 <div class="col-md-2 ">
                  <input  class="form-control" id="medicinetravaitimepicker"  type="text" value="<?=  $notification->medicineoftravaitime ?>" name="medicinetravaitimefield"   style="width:100%;text-align: center;" />       
                 </div>
                 <div class="col-md-3 pdhz text-center" ><div class="col-md-10 col-md-offset-2 pdhz text-left">Work Medecine Expired</div></div>      
                  <div class="col-md-1 pdhz text-center" >
                             <a href="<?= base_url(); ?>admin/drivers/notification_pdf/<?= $notification->id  ?>/<?= $driver->id ?>/5" target="_blank">
                               <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                             </a>
                 </div>
                 <div class="col-md-2 pdhz text-center" ><?= ($notification->medicineoftravaiissent == '1')?"Sent":"Pending"; ?></div>
                 <div class="col-md-2 pdhz text-center" >
                   <div id="medicinetravaisentloaderbtn" style="position: relative;display: none;">
                     <img src="<?= base_url()?>assets/theme/default/images/bookloader.gif" style="width: 28px;height: 28px;position: absolute;left: -16px;top: 0px;">
                   </div>
                   <button type="button" class="btn btn-default" id="medicinetravaisentbtn" style="height: 30px;line-height: 1px;" onclick="adddrivernotification('medicineoftravaidate','medicineoftravaitime','medicinetravaidatepicker','medicinetravaitimepicker','medicinetravaisentloaderbtn','Work Medecine Expired','#drivernotificationmaindiv_5')">Send</button>
                 </div>      
             </div>
             <?php $e++; ?>
           <?php endif;?>
            <?php endif; ?>
               <?php endforeach; ?> 

               <?php $e1=0; ?>
              <div class="col-md-12 pdhz" id="drivernotificationmaindiv_5">
               <?php foreach ($drivernotifications as $notification): ?>
                 <?php if(!empty($notification->medicineoftravaidate) || $notification->medicineoftravaidate != null): ?>
                  <?php if($e1 != 0): ?>
                 <div class="col-md-12 pdhz">
                         
                      <div class="col-md-2 pdhz text-center" ><?= from_unix_date($notification->medicineoftravaidate); ?></div>
                      <div class="col-md-2 pdhz text-center" ><?= $notification->medicineoftravaitime ?></div>
                      <div class="col-md-3 pdhz text-center" ><div class="col-md-10 col-md-offset-2 pdhz text-left">Work Medecine Expired</div></div>      
                       <div class="col-md-1 pdhz text-center" >
                                  <a href="<?= base_url(); ?>admin/drivers/notification_pdf/<?= $notification->id  ?>/<?= $driver->id ?>/5" target="_blank">
                                    <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                                  </a>
                      </div>
                      <div class="col-md-2 pdhz text-center" ><?= ($notification->medicineoftravaiissent == '1')?"Sent":"Pending"; ?></div>
                 </div>
                  <?php endif;?>
                 <?php $e1++; ?>
               <?php endif; ?>
               <?php endforeach; ?>  
               </div> 
              <!-- Medicine Travel Notification -->   

    </div>

