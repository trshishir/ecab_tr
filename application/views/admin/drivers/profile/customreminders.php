                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 15px;">
                             <div class="col-md-12 pdhz" style="font-weight: 900;">CUSTOM REMINDERS : </div>
                             <div class="col-md-12 pdhz" id="customreminderrecorded">
                               <div class="col-md-10 pdhz">
                                <div class="col-md-3 pdlz" style="text-align: left;font-weight: 900;">Canal</div>
                                <div class="col-md-3 pdlz" style="text-align: left;font-weight: 900;">Date</div>
                                <div class="col-md-3 pdlz" style="text-align: left;font-weight: 900;">Time</div>
                                <div class="col-md-3 pdlz" style="text-align: left;font-weight: 900;">Comment</div>
                              </div>

                              <?php  foreach ($drivercustomreminders as $custom): 
                                     $date = strtotime($custom->date);
                                     $date = date('d/m/Y',$date);
                                     $date= $date;
                                  ?>

                                  <div class="customreminderbox col-md-12 pdhz" style="margin-top: 5px;">
                                    <div class="col-md-10 pdhz">
                                    <div class="col-md-3 text-left pdlz"><?= $custom->title  ?></div>
                                    <div class="col-md-3 text-left pdlz"><?= $custom->date  ?></div>
                                    <div class="col-md-3 text-left pdlz"><?= $custom->time  ?></div>
                                    <div class="col-md-3 text-left pdlz" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;"><?= $custom->comment  ?></div>
                                   </div>
                                    <div class="col-md-2" style="position: relative;width: 8%;">
                                      <button type="button" onclick="removecustomreminderbox(this,<?= $custom->id ?>);" class="minusrediconcustomreminder"><i class="fa fa-minus" style="margin:0px !important;"></i></button></div>
                                  </div>
                            <?php endforeach; ?>
                             </div>
                             <input type="hidden" id="customremindertotalcount" value="1">
                             <div class="maincustomreminderbox col-md-12 pdhz">
                              <div class="customreminderbox col-md-12 pdhz">
                                <div class="col-md-12 pdhz">
                                 <div class="col-md-10 pdhz" style="margin-top: 5px;">
                                        <div class="col-md-3 pdlz">
                                          
                                          <div class="form-group">
                                            
                                                   <select class="form-control" name="customremindertitlefield" id="customremindertitlefield">
                                                         <option value="">Select Canal</option>
                                                         <option value="Email">Email</option>
                                                         <option value="Phone">Phone</option>
                                                         <option value="Letter">Letter</option>
                                                         
                                                       
                                                   </select>
                                                        
                                                                                           
                                              </div>
                                        </div>


                                        <div class="col-md-3 pdlz">                             
                                          <input  class="pickup-dt-bkg datepicker form-control" id="customreminderdatefield"  type="text" value="<?= date('d/m/Y'); ?>" name="customreminderdatefield[]" placeholder="<?php echo 'Custom Date'; ?>" autocomplete="off" />     
                                        </div>
                                         <div class="col-md-3 pdlz">
                                          <input   id="customremindertimepicker" class="form-control"  type="text" value="<?= date('h : i'); ?>" name="customremindertimefield[]" placeholder="<?php echo 'Custom Time'; ?>"  style="width:100%" />       
                                        </div> 
                                 </div>
                                 <div class="col-md-2 pdhz" style="width: 8% !important;position: relative;">
                                    <button type="button" onclick="addcustomreminderbox();" class="plusgreeniconconfig"><i class="fa fa-plus" style="margin:0px !important;"></i></button>
                                 </div>
                               </div>
                               <div class="col-md-12 pdhz" style="margin-top:5px;">
                                 <div class="col-md-10 pdhz" style="width: 61% !important;">
                                   <textarea rows="3" class="form-control" placeholder="write a comment" id="customremindercommentfield" name="customremindercommentfield"></textarea>
                                 </div>
                               </div>
                             </div>
                             </div>
                        </div>