<?php $locale_info = localeconv(); ?>

<section id="content">
    <div class="listDiv"><!--col-md-10 padding white right-p-->
    
   <div style="padding: 5px 12px;display: none;" id="inner_alert_div" class="alert">
           <span id="inner_message_div"></span>    
            <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
    </div>
    

            <div class="row-fluid"  id="ShowTabs" style="margin-bottom: 10px; margin-left:0px;">

                                <ul class="nav nav-tabs responsive">
                                            <li class="active"><a href="#PROFILE" class="profile_E" role="tab" data-toggle="tab">PROFIL</a></li>
                                            <li><a href="#DOCUMENT" class="document_E" role="tab" data-toggle="tab">DOCUMENTS</a></li>
                                            <li><a href="#RIDE" class="ride_E" role="tab" data-toggle="tab">RIDES</a></li>
                                            <li><a href="#TIMESHEET" class="timesheet_E" role="tab" data-toggle="tab">TIMESHEET</a></li>
                                            <li><a href="#WAGE" class="wage_E" role="tab" data-toggle="tab">WAGES</a></li>
                                            <li><a href="#AVAILABILITY" role="tab" class="availability_E" data-toggle="tab">AVAILABILITY</a></li>
                                            <li><a href="#REQUEST" class="request_E" role="tab" data-toggle="tab">REQUESTS</a></li>
                                            <li><a href="#INFRACTION" class="infraction_E" role="tab" data-toggle="tab">INFRACTIONS</a></li>
                                            
                                            <li><a href="#SUPPORT" class="support_E" role="tab" data-toggle="tab">SUPPORT</a></li>
                                            <li><a href="#MESSAGE" class="message_E" role="tab" data-toggle="tab">MESSAGES</a></li>          
                                </ul>
                               
                <div class="tab-content responsive" style="width:100%;display: table;">
                        <!--status tab starts-->
                        <div class="tab-pane fade in active" id="PROFILE" style="border: 1px solid #75b0d7;">
                            <div class="row">
                            <?php include('profile/edit.php'); ?>
                           </div>
                        </div>

                        <div class="tab-pane fade" id="DOCUMENT" style="border: 1px solid #75b0d7;">
                        <?php include('document.php'); ?>
                        </div>
                        <div class="tab-pane fade" id="TIMESHEET" style="border: 1px solid #75b0d7;">
                        <?php include('timesheet.php'); ?>
                        </div>

                        <div class="tab-pane fade"  id="AVAILABILITY" style="border: 1px solid #75b0d7;">
                        <?php include('availability.php'); ?>
                        </div>
                        <div class="tab-pane fade" id="REQUEST" style="border: 1px solid #75b0d7;">
                        <?php include('request.php'); ?>
                        </div>
                        <div class="tab-pane fade" id="RIDE" style="border: 1px solid #75b0d7;">
                        <?php include('ride.php'); ?>
                        </div>
                        <div class="tab-pane fade" id="INFRACTION" style="border: 1px solid #75b0d7;">
                        <?php include('infraction.php'); ?>
                        </div>
                        <div class="tab-pane fade" id="WAGE" style="border: 1px solid #75b0d7;">
                        <?php include('wage.php'); ?>
                        </div>
                        <div class="tab-pane fade" id="SUPPORT" style="border: 1px solid #75b0d7;">
                        <?php include('support.php'); ?>
                        </div>
                         <div class="tab-pane fade" id="MESSAGE" style="border: 1px solid #75b0d7;">
                        <?php include('message.php'); ?>
                        </div>             
                    </div>
            <!--/.module-->
        </div>

    </div>
</section>
