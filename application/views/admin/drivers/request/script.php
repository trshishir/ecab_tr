<script type="text/javascript">
  $(document).ready(function() {

           $(document).on('change','#request_country_id', function() {
                      request_get_region_base_country(this);
                  });
           $(document).on('change','#request_region_id', function() {
                      request_get_cities_base_country_region(this);
                  });
           $(document).on('change','#edit_request_country_id', function() {
                      edit_request_get_region_base_country(this);
                  });
           $(document).on('change','#edit_request_region_id', function() {
                      edit_request_get_cities_base_country_region(this);
                  });

            $(document).on('change','input.chk-mainrequest-template', function() {
                      $('input.chk-mainrequest-template').not(this).prop('checked', false);
                      var id = $(this).attr('data-input');
                      $('.chk-Addrequest-btn').val(id);
                      $('#requestdeletid').val(id);
                  });        

  });
function requestAdd()
{
  setupDivRequest();
  $(".Requestadd").show();
}

function cancelRequest()
{
  setupDivRequest();
  $(".ListRequest").show();
}

function requestEdit()
{
    var val = $('.chk-Addrequest-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivRequest();
        $(".requestEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/clients/get_ajax_request_data'; ?>',
                data: {'request_id': val},
                success: function (result) {
                  document.getElementsByClassName("requestEditajax")[0].innerHTML = result;
                
                }
            });
}
function requestidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivRequest();
      $(".requestEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/clients/get_ajax_request_data'; ?>',
              data: {'request_id': val},
              success: function (result) {
                document.getElementsByClassName("requestEditajax")[0].innerHTML = result;
               
              }
          });
}
function requestDelete()
{
  var val = $('.chk-Addrequest-btn').val();
  if(val != "")
  {
  setupDivRequest();
  $(".requestDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivRequest()
{
  $(".Requestadd").hide();
  $(".requestEdit").hide();
  $(".ListRequest").hide();
  $(".requestDelete").hide();

}
function setupDivRequestConfig()
{
  $(".Requestadd").hide();
  $(".requestEdit").hide();
  $(".ListRequest").show();
  $(".requestDelete").hide();

}


  function request_get_region_base_country(region_id=null)
   {
     country_id=$('#request_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
         html ='';
         html =html + '<option value="">Select Region</option>';
         
         $.each(response, function( index, value ) {  
         if(region_id==value.id)
         {
           html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
         }
         else{
           html =html + '<option value="'+value.id+'">'+value.name+'</option>';
         }
         });
         $('#request_region_id').html(html);
       }
   }); 
}

function request_get_cities_base_country_region(region_id=null,cities_id=null)
{

region_id=$('#request_region_id').val();

$.ajax({
    url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
    method: 'post',
    data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
    dataType: 'json',
    success: function(response){
    console.log(response);
    html ='';
    html =html + ' <option value="">Select cities</option>';

    $.each(response, function( index, value ) {
    // console.log( value );
                

    if(cities_id==value.id)
    {
        html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';

    }
    else{
        html =html + '<option value="'+value.id+'">'+value.name+'</option>';

    }
    });

    $('#request_cities_id').html(html);


    }


});
}


//For Edit Page

function edit_request_get_cities_base_country_region(region_id=null,cities_id=null)
{
    region_id=$('#edit_request_region_id').val();
    $.ajax({
        url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
        method: 'post',
        data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
        dataType: 'json',
        success: function(response){
            html ='';
            html =html + ' <option value="">Select cities</option>';

            $.each(response, function( index, value ) {
            if(cities_id==value.id)
            {
                html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
            }
            else{
                html =html + '<option value="'+value.id+'">'+value.name+'</option>';
            }
            });
            $('#edit_request_cities_id').html(html);
        }
    });
}
   function edit_request_get_region_base_country(region_id=null)
   { 
     country_id=$('#edit_request_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
     html ='';
     html =html + '<option value="">Select Region</option>';
     
     $.each(response, function( index, value ) {
     if(region_id==value.id)
     {
       html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';  
     }
     else{
       html =html + '<option value="'+value.id+'">'+value.name+'</option>';
     }
     });
     $('#edit_request_region_id').html(html);
     }   
     }); 
   }
//For Edit Page

//create form submit
$(document).on('submit',"form#addrequestform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivRequest();
         $('.ListRequest').empty();
         $('.ListRequest').html(data.record);
         createinnerconfigtable();
         requestEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivRequest();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListRequest").show();
        }
     document.getElementById("addrequestform").reset();
      }
    });
    return false;
  });
//create form submit

//update form submit
$(document).on('submit',"form#updaterequestform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivRequest();
         $('.ListRequest').empty();
         $('.ListRequest').html(data.record);
         createinnerconfigtable();
         requestEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivRequest();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListRequest").show();
        }
     document.getElementById("addrequestform").reset();
      }
    });
    return false;
  });
//update form submit

//delete form submit
$(document).on('submit',"form#deleterequestform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivRequest();
         $('.ListRequest').empty();
         $('.ListRequest').html(data.record);
         createinnerconfigtable();
         requestEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivRequest();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListRequest").show();
        }
     document.getElementById("addrequestform").reset();
      }
    });
    return false;
  });
//delete form submit
</script>
