
  <?php
  $attributes = array("name" => 'delete_infraction_form', "id" => 'deleteinfractionform');   
  echo form_open_multipart('admin/clients/infractionDelete',$attributes); ?>
    <input  name="tablename" value="vbs_clientinfraction" type="hidden" >
    <input type="hidden" id="infractiondeletid" name="delete_infraction_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <button type="submit" class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>
    <a class="btn btn-default" style="cursor: pointer;" onclick="cancelContact()"><span class="delete-icon">No</span></a>
  <?php echo form_close(); ?>
