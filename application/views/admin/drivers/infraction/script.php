<script type="text/javascript">
  $(document).ready(function() {

           $(document).on('change','#infraction_country_id', function() {
                      infraction_get_region_base_country(this);
                  });
           $(document).on('change','#infraction_region_id', function() {
                      infraction_get_cities_base_country_region(this);
                  });
           $(document).on('change','#edit_infraction_country_id', function() {
                      edit_infraction_get_region_base_country(this);
                  });
           $(document).on('change','#edit_infraction_region_id', function() {
                      edit_infraction_get_cities_base_country_region(this);
                  });

            $(document).on('change','input.chk-maininfraction-template', function() {
                      $('input.chk-maininfraction-template').not(this).prop('checked', false);
                      var id = $(this).attr('data-input');
                      $('.chk-Addinfraction-btn').val(id);
                      $('#infractiondeletid').val(id);
                  });        

  });
function infractionAdd()
{
  setupDivInfraction();
  $(".Infractionadd").show();
}

function cancelInfraction()
{
  setupDivInfraction();
  $(".ListInfraction").show();
}

function infractionEdit()
{
    var val = $('.chk-Addinfraction-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivInfraction();
        $(".infractionEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/clients/get_ajax_infraction_data'; ?>',
                data: {'infraction_id': val},
                success: function (result) {
                  document.getElementsByClassName("infractionEditajax")[0].innerHTML = result;
                
                }
            });
}
function infractionidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivInfraction();
      $(".infractionEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/clients/get_ajax_infraction_data'; ?>',
              data: {'infraction_id': val},
              success: function (result) {
                document.getElementsByClassName("infractionEditajax")[0].innerHTML = result;
               
              }
          });
}
function infractionDelete()
{
  var val = $('.chk-Addinfraction-btn').val();
  if(val != "")
  {
  setupDivInfraction();
  $(".infractionDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivInfraction()
{
  $(".Infractionadd").hide();
  $(".infractionEdit").hide();
  $(".ListInfraction").hide();
  $(".infractionDelete").hide();

}
function setupDivInfractionConfig()
{
  $(".Infractionadd").hide();
  $(".infractionEdit").hide();
  $(".ListInfraction").show();
  $(".infractionDelete").hide();

}


  function infraction_get_region_base_country(region_id=null)
   {
     country_id=$('#infraction_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
         html ='';
         html =html + '<option value="">Select Region</option>';
         
         $.each(response, function( index, value ) {  
         if(region_id==value.id)
         {
           html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
         }
         else{
           html =html + '<option value="'+value.id+'">'+value.name+'</option>';
         }
         });
         $('#infraction_region_id').html(html);
       }
   }); 
}

function infraction_get_cities_base_country_region(region_id=null,cities_id=null)
{

region_id=$('#infraction_region_id').val();

$.ajax({
    url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
    method: 'post',
    data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
    dataType: 'json',
    success: function(response){
    console.log(response);
    html ='';
    html =html + ' <option value="">Select cities</option>';

    $.each(response, function( index, value ) {
    // console.log( value );
                

    if(cities_id==value.id)
    {
        html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';

    }
    else{
        html =html + '<option value="'+value.id+'">'+value.name+'</option>';

    }
    });

    $('#infraction_cities_id').html(html);


    }


});
}


//For Edit Page

function edit_infraction_get_cities_base_country_region(region_id=null,cities_id=null)
{
    region_id=$('#edit_infraction_region_id').val();
    $.ajax({
        url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
        method: 'post',
        data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
        dataType: 'json',
        success: function(response){
            html ='';
            html =html + ' <option value="">Select cities</option>';

            $.each(response, function( index, value ) {
            if(cities_id==value.id)
            {
                html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
            }
            else{
                html =html + '<option value="'+value.id+'">'+value.name+'</option>';
            }
            });
            $('#edit_infraction_cities_id').html(html);
        }
    });
}
   function edit_infraction_get_region_base_country(region_id=null)
   { 
     country_id=$('#edit_infraction_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
     html ='';
     html =html + '<option value="">Select Region</option>';
     
     $.each(response, function( index, value ) {
     if(region_id==value.id)
     {
       html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';  
     }
     else{
       html =html + '<option value="'+value.id+'">'+value.name+'</option>';
     }
     });
     $('#edit_infraction_region_id').html(html);
     }   
     }); 
   }
//For Edit Page

//create form submit
$(document).on('submit',"form#addinfractionform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivInfraction();
         $('.ListInfraction').empty();
         $('.ListInfraction').html(data.record);
         createinnerconfigtable();
         infractionEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivInfraction();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListInfraction").show();
        }
     document.getElementById("addinfractionform").reset();
      }
    });
    return false;
  });
//create form submit

//update form submit
$(document).on('submit',"form#updateinfractionform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivInfraction();
         $('.ListInfraction').empty();
         $('.ListInfraction').html(data.record);
         createinnerconfigtable();
         infractionEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivInfraction();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListInfraction").show();
        }
     document.getElementById("addinfractionform").reset();
      }
    });
    return false;
  });
//update form submit

//delete form submit
$(document).on('submit',"form#deleteinfractionform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivInfraction();
         $('.ListInfraction').empty();
         $('.ListInfraction').html(data.record);
         createinnerconfigtable();
         infractionEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivInfraction();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListInfraction").show();
        }
     document.getElementById("addinfractionform").reset();
      }
    });
    return false;
  });
//delete form submit
</script>
