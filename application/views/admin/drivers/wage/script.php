<script type="text/javascript">
  $(document).ready(function() {

           $(document).on('change','#wage_country_id', function() {
                      wage_get_region_base_country(this);
                  });
           $(document).on('change','#wage_region_id', function() {
                      wage_get_cities_base_country_region(this);
                  });
           $(document).on('change','#edit_wage_country_id', function() {
                      edit_wage_get_region_base_country(this);
                  });
           $(document).on('change','#edit_wage_region_id', function() {
                      edit_wage_get_cities_base_country_region(this);
                  });

            $(document).on('change','input.chk-mainwage-template', function() {
                      $('input.chk-mainwage-template').not(this).prop('checked', false);
                      var id = $(this).attr('data-input');
                      $('.chk-Addwage-btn').val(id);
                      $('#wagedeletid').val(id);
                  });        

  });
function wageAdd()
{
  setupDivWage();
  $(".Wageadd").show();
}

function cancelWage()
{
  setupDivWage();
  $(".ListWage").show();
}

function wageEdit()
{
    var val = $('.chk-Addwage-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivWage();
        $(".wageEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/clients/get_ajax_wage_data'; ?>',
                data: {'wage_id': val},
                success: function (result) {
                  document.getElementsByClassName("wageEditajax")[0].innerHTML = result;
                
                }
            });
}
function wageidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivWage();
      $(".wageEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/clients/get_ajax_wage_data'; ?>',
              data: {'wage_id': val},
              success: function (result) {
                document.getElementsByClassName("wageEditajax")[0].innerHTML = result;
               
              }
          });
}
function wageDelete()
{
  var val = $('.chk-Addwage-btn').val();
  if(val != "")
  {
  setupDivWage();
  $(".wageDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivWage()
{
  $(".Wageadd").hide();
  $(".wageEdit").hide();
  $(".ListWage").hide();
  $(".wageDelete").hide();

}
function setupDivWageConfig()
{
  $(".Wageadd").hide();
  $(".wageEdit").hide();
  $(".ListWage").show();
  $(".wageDelete").hide();

}


  function wage_get_region_base_country(region_id=null)
   {
     country_id=$('#wage_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
         html ='';
         html =html + '<option value="">Select Region</option>';
         
         $.each(response, function( index, value ) {  
         if(region_id==value.id)
         {
           html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
         }
         else{
           html =html + '<option value="'+value.id+'">'+value.name+'</option>';
         }
         });
         $('#wage_region_id').html(html);
       }
   }); 
}

function wage_get_cities_base_country_region(region_id=null,cities_id=null)
{

region_id=$('#wage_region_id').val();

$.ajax({
    url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
    method: 'post',
    data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
    dataType: 'json',
    success: function(response){
    console.log(response);
    html ='';
    html =html + ' <option value="">Select cities</option>';

    $.each(response, function( index, value ) {
    // console.log( value );
                

    if(cities_id==value.id)
    {
        html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';

    }
    else{
        html =html + '<option value="'+value.id+'">'+value.name+'</option>';

    }
    });

    $('#wage_cities_id').html(html);


    }


});
}


//For Edit Page

function edit_wage_get_cities_base_country_region(region_id=null,cities_id=null)
{
    region_id=$('#edit_wage_region_id').val();
    $.ajax({
        url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
        method: 'post',
        data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
        dataType: 'json',
        success: function(response){
            html ='';
            html =html + ' <option value="">Select cities</option>';

            $.each(response, function( index, value ) {
            if(cities_id==value.id)
            {
                html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
            }
            else{
                html =html + '<option value="'+value.id+'">'+value.name+'</option>';
            }
            });
            $('#edit_wage_cities_id').html(html);
        }
    });
}
   function edit_wage_get_region_base_country(region_id=null)
   { 
     country_id=$('#edit_wage_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
     html ='';
     html =html + '<option value="">Select Region</option>';
     
     $.each(response, function( index, value ) {
     if(region_id==value.id)
     {
       html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';  
     }
     else{
       html =html + '<option value="'+value.id+'">'+value.name+'</option>';
     }
     });
     $('#edit_wage_region_id').html(html);
     }   
     }); 
   }
//For Edit Page

//create form submit
$(document).on('submit',"form#addwageform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivWage();
         $('.ListWage').empty();
         $('.ListWage').html(data.record);
         createinnerconfigtable();
         wageEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivWage();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListWage").show();
        }
     document.getElementById("addwageform").reset();
      }
    });
    return false;
  });
//create form submit

//update form submit
$(document).on('submit',"form#updatewageform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivWage();
         $('.ListWage').empty();
         $('.ListWage').html(data.record);
         createinnerconfigtable();
         wageEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivWage();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListWage").show();
        }
     document.getElementById("addwageform").reset();
      }
    });
    return false;
  });
//update form submit

//delete form submit
$(document).on('submit',"form#deletewageform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivWage();
         $('.ListWage').empty();
         $('.ListWage').html(data.record);
         createinnerconfigtable();
         wageEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivWage();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListWage").show();
        }
     document.getElementById("addwageform").reset();
      }
    });
    return false;
  });
//delete form submit
</script>
