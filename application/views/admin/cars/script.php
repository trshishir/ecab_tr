<script>

    jQuery(function($){
        $.datepicker.regional['fr'] = {
            closeText: 'Fermer',
            prevText: '&#x3c;Préc',
            nextText: 'Suiv&#x3e;',
            currentText: 'Aujourd\'hui',
            monthNames: ['Janvier','Fevrier','Mars','Avril','Mai','Juin',
                'Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
            monthNamesShort: ['Jan','Fev','Mar','Avr','Mai','Jun',
                'Jul','Aou','Sep','Oct','Nov','Dec'],
            dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
            dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
            dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
            weekHeader: 'Sm',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: '',
            maxDate: '+12M +0D',
            showButtonPanel: true
        };
        $.datepicker.setDefaults($.datepicker.regional['fr']);
       
    });

</script>

<script>
 $(document).ready(function () {
   

    $(document).on('click','.profile_E', function() {
        profileEditEvent();
    });
    $(document).on('click','.assurance_E', function() {
        assuranceEvent();
    });
    $(document).on('click','.technique_E', function() {
        techniqueEvent();
    });
    $(document).on('click','.entretien_E', function() {
        entretienEvent();
    });
    $(document).on('click','.reparation_E', function() {
        reparationEvent();
    });
    $(document).on('click','.sinistre_E', function() {
        sinistreEvent();
    });
    $(document).on('click','.cout_E', function() {
        coutEvent();
    });
  
    
});
    function profileEditEvent(){
        removeAllResources();
        $('.removeinnerevent').remove();
    
    }

  function assuranceEvent(){
    
        removeAllResources();
        $('.removeinnerevent').remove();
        var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="assuranceAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="assuranceEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="assuranceDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addinnerevent').append(html);
    }


    function techniqueEvent(){
        removeAllResources();
        $('.removeinnerevent').remove();
        var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="techniqueAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="techniqueEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="techniqueDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addinnerevent').append(html);
    }


      function entretienEvent(){
        removeAllResources();
        $('.removeinnerevent').remove();
        var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="entretienAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="entretienEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="entretienDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addinnerevent').append(html);
    }


    function reparationEvent(){
        removeAllResources();
        $('.removeinnerevent').remove();
        var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="reparationAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="reparationEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="reparationDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addinnerevent').append(html);
    }

  function sinistreEvent(){
    removeAllResources();
    $('.removeinnerevent').remove();
    var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="sinistreAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="sinistreEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="sinistreDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addinnerevent').append(html);
    }


        function coutEvent(){
        removeAllResources();
        $('.removeinnerevent').remove();
        var html= '<div class="removeinnerevent"><button class="dt-button buttons-copy buttons-html5" onclick="coutAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="coutEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="coutDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addinnerevent').append(html);
    }

   



    


  




 
 function removeAllResources(){
    setupDivTechniqueConfig();
    setupDivAssuranceConfig();
    setupDivEntretienConfig();
    setupDivReparationConfig();
    setupDivSinistreConfig();
    setupDivCoutConfig();
    
    
   //setupDivProfileConfig();
   //setupDivResourceConfig();
   /*setupDivProfileConfig();
   setupDivMethodeConfig();
   setupDivPassengerConfig();
   setupDivRideConfig();
   setupDivBookingConfig();
   setupDivQuoteConfig();
   setupDivContractConfig();
   setupDivInvoiceConfig();
   setupDivPaymentConfig();
   setupDivSupportConfig();
   setupDivMessageConfig();*/
   
 }
 function createinnerconfigtable(tablename){
 
  // Date range filter
    var datatables = $(tablename).DataTable({
        columnDefs: [
            {targets: 'no-sort', orderable: false}
        ],
        dom: '<"table-filter">B<"innertoolbar">frtip',
        language: {search: "", class: "form-control", searchPlaceholder: "Search"},
        buttons: [
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        initComplete: function () {
            $("div.innertoolbar")
                    .html('<div class="addinnerevent"><div class="removeinnerevent"></div></div>');
            var filterInp = $("#table-filter");
            if (filterInp.length > 0) {
                var tableFilter = $("div.table-filter");
                tableFilter.html(filterInp.html());

                if ($(".table-filter .dpo").length > 0) {
                    $('.table-filter .dpo[data-name="date_from"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            minDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                    $('.table-filter .dpo[data-name="date_to"]').datepicker({
                        format: "dd/mm/yyyy",
                        "onSelect": function (date) {
                            maxDateFilter = new Date(date).getTime();
                            datatables.fnDraw();
                        }
                    });
                }
            }

            this.api().columns().every(function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                    );

                            column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                        });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });

        },
        "drawCallback": function () {
            $('.dataTables_paginate .paginate_button').addClass('btn btn-default');
        }
    });
 



                /* inner config*/
 }
</script>
