
    <div class="maineditcapacitybox col-md-12">

<div class="capacitybox" >
          <div class="col-md-11 innercapacitybox" style="width: 68%;"> 

             <div class="col-md-2 pdhz" style="width: 20%;margin-left:1%;">
                  <div class="col-md-6 pdhz" style="padding-top: 7px;">
                    <span>Passengers</span>
                 </div>
                  <div class="col-md-6 pdhz">
                  <select id="editpassengerfield" style="width:75px;" class="form-control" >
                     
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
                  </div>
             </div>
               <div class="col-md-2 pdhz" style="width: 18%;margin-left:1%;">
                  <div class="col-md-5  pdhz" style="padding-top: 7px;">
                    <span>Luggage</span>
                 </div>
                  <div class="col-md-7 pdhz">
 
                  <select id="editlugagefield" style="width:75px;" class="form-control" >
                     
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
                </div>
             </div>
             <div class="col-md-2 pdhz" style="width: 21%;margin-left:1%;">
                  <div class="col-md-6  pdhz" style="padding-top: 7px;">
                    <span>Wheelchairs</span>
                 </div>
                  <div class="col-md-6 pdhz">
                  <select id="editwheelchairfield" style="width:75px;" class="form-control" >
                    
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
                </div>
             </div>
               <div class="col-md-2 pdhz" style="width: 16%;margin-left:1%;">
                  <div class="col-md-4  pdhz" style="padding-top: 7px;">
                    <span>Babys</span>
                 </div>
                  <div class="col-md-8 pdhz">
                  <select id="editbabyfield" style="width:75px;" class="form-control" >
                     
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
              </div>
             </div>
               <div class="col-md-2 pdhz" style="width: 18%;margin-left:1%;">
                  <div class="col-md-5  pdhz" style="padding-top: 7px;">
                    <span>Animals</span>
                 </div>
                  <div class="col-md-7 pdhz">
                  <select id="editanimalfield" style="width:75px;" class="form-control" >
                    
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
               </div>
             </div>
          
           </div> 
            <div class="col-md-1" style="position: relative;">
              <button type="button"  onclick="updatebox();" class="plusgreeniconconfig" style="top:11px;left: 15px;"><i class="fa fa-plus"></i></button>
            </div>
         </div>
      
      <?php foreach ($car_configuration_data as $key => $config): ?>
        <div class="storebox">
        
         <div class="col-md-11 innerstorebox" style="width: 68%;"> 

             <div class="col-md-2 pdhz" style="width: 20%;margin-left:1%;">
                  <div class="col-md-6 pdhz" style="padding-top: 7px;">
                    <span>Passengers</span>
                 </div>
                  <div class="col-md-6 pdhz" >
                  <select name="passengercap[]" style="width:75px;pointer-events: none;background-color: #def4faab !important;" class="form-control" >
                     
                      <option <?php if($config->passengers  == "0"){ echo "selected"; } ?> value="0">0</option>
                      <option <?php if($config->passengers  == "1"){ echo "selected"; } ?> value="1">1</option>
                      <option <?php if($config->passengers  == "2"){ echo "selected"; } ?> value="2">2</option>
                      <option <?php if($config->passengers  == "3"){ echo "selected"; } ?> value="3">3</option>
                      <option <?php if($config->passengers  == "4"){ echo "selected"; } ?> value="4">4</option>
                      <option <?php if($config->passengers  == "5"){ echo "selected"; } ?> value="5">5</option>
                      <option <?php if($config->passengers  == "6"){ echo "selected"; } ?> value="6">6</option>
                      <option <?php if($config->passengers  == "6"){ echo "selected"; } ?> value="6">6</option>
                      <option <?php if($config->passengers  == "8"){ echo "selected"; } ?> value="8">8</option>
                      <option <?php if($config->passengers  == "9"){ echo "selected"; } ?> value="9">9</option>
                      <option <?php if($config->passengers  == "10"){ echo "selected"; } ?> value="10">10</option>
                   
                  </select> 

                </div>
             </div>
               <div class="col-md-2 pdhz" style="width: 18%;margin-left:1%;">
                  <div class="col-md-5 pdhz" style="padding-top: 7px;">
                    <span>Luggage</span>
                 </div>
                  <div class="col-md-7 pdhz" >

                  <select name="lugagescap[]" style="width:75px;pointer-events: none;background-color: #def4faab !important;" class="form-control" >    
                   <option <?php if($config->luggage  == "0"){ echo "selected"; } ?> value="0">0</option>
                   <option <?php if($config->luggage  == "1"){ echo "selected"; } ?> value="1">1</option>
                   <option <?php if($config->luggage  == "2"){ echo "selected"; } ?> value="2">2</option>
                   <option <?php if($config->luggage  == "3"){ echo "selected"; } ?> value="3">3</option>
                   <option <?php if($config->luggage  == "4"){ echo "selected"; } ?> value="4">4</option>
                   <option <?php if($config->luggage  == "5"){ echo "selected"; } ?> value="5">5</option>
                   <option <?php if($config->luggage  == "6"){ echo "selected"; } ?> value="6">6</option>
                   <option <?php if($config->luggage  == "6"){ echo "selected"; } ?> value="6">6</option>
                   <option <?php if($config->luggage  == "8"){ echo "selected"; } ?> value="8">8</option>
                   <option <?php if($config->luggage  == "9"){ echo "selected"; } ?> value="9">9</option>
                   <option <?php if($config->luggage  == "10"){ echo "selected"; } ?> value="10">10</option>
                  </select> 

                </div>
             </div>
             <div class="col-md-2 pdhz" style="width: 21%;margin-left:1%;">
                  <div class="col-md-6 pdhz" style="padding-top: 7px;">
                    <span>Wheelchairs</span>
                 </div>
                  <div class="col-md-6 pdhz" >
                  <select name="wheelchairscap[]" style="width:75px;pointer-events: none;background-color: #def4faab !important;" class="form-control" >
                
                     
                       <option <?php if($config->wheelchairs  == "0"){ echo "selected"; } ?> value="0">0</option>
                       <option <?php if($config->wheelchairs  == "1"){ echo "selected"; } ?> value="1">1</option>
                       <option <?php if($config->wheelchairs  == "2"){ echo "selected"; } ?> value="2">2</option>
                       <option <?php if($config->wheelchairs  == "3"){ echo "selected"; } ?> value="3">3</option>
                       <option <?php if($config->wheelchairs  == "4"){ echo "selected"; } ?> value="4">4</option>
                       <option <?php if($config->wheelchairs  == "5"){ echo "selected"; } ?> value="5">5</option>
                       <option <?php if($config->wheelchairs  == "6"){ echo "selected"; } ?> value="6">6</option>
                       <option <?php if($config->wheelchairs  == "6"){ echo "selected"; } ?> value="6">6</option>
                       <option <?php if($config->wheelchairs  == "8"){ echo "selected"; } ?> value="8">8</option>
                       <option <?php if($config->wheelchairs  == "9"){ echo "selected"; } ?> value="9">9</option>
                       <option <?php if($config->wheelchairs  == "10"){ echo "selected"; } ?> value="10">10</option>
                  </select> 

             </div>
             </div>
               <div class="col-md-2 pdhz" style="width: 16%;margin-left:1%;">
                  <div class="col-md-4 pdhz" style="padding-top: 7px;">
                    <span>Babys</span>
                 </div>
                  <div class="col-md-8 pdhz" >
                  <select name="babycap[]" style="width:75px;pointer-events: none;background-color: #def4faab !important;" class="form-control" >                 
                    <option <?php if($config->baby  == "0"){ echo "selected"; } ?> value="0">0</option>
                    <option <?php if($config->baby  == "1"){ echo "selected"; } ?> value="1">1</option>
                    <option <?php if($config->baby  == "2"){ echo "selected"; } ?> value="2">2</option>
                    <option <?php if($config->baby  == "3"){ echo "selected"; } ?> value="3">3</option>
                    <option <?php if($config->baby  == "4"){ echo "selected"; } ?> value="4">4</option>
                    <option <?php if($config->baby  == "5"){ echo "selected"; } ?> value="5">5</option>
                    <option <?php if($config->baby  == "6"){ echo "selected"; } ?> value="6">6</option>
                    <option <?php if($config->baby  == "6"){ echo "selected"; } ?> value="6">6</option>
                    <option <?php if($config->baby  == "8"){ echo "selected"; } ?> value="8">8</option>
                    <option <?php if($config->baby  == "9"){ echo "selected"; } ?> value="9">9</option>
                    <option <?php if($config->baby  == "10"){ echo "selected"; } ?> value="10">10</option>
                  </select> 

              </div>
             </div>
               <div class="col-md-2 pdhz" style="width: 18%;margin-left:1%;">
                  <div class="col-md-5 pdhz" style="padding-top: 7px;">
                    <span>Animals</span>
                 </div>
                  <div class="col-md-7 pdhz" >
                  <select name="animalscap[]" style="width:75px;pointer-events: none;background-color: #def4faab !important;" class="form-control" >
                      <option <?php if($config->animals  == "0"){ echo "selected"; } ?> value="0">0</option>
                      <option <?php if($config->animals  == "1"){ echo "selected"; } ?> value="1">1</option>
                      <option <?php if($config->animals  == "2"){ echo "selected"; } ?> value="2">2</option>
                      <option <?php if($config->animals  == "3"){ echo "selected"; } ?> value="3">3</option>
                      <option <?php if($config->animals  == "4"){ echo "selected"; } ?> value="4">4</option>
                      <option <?php if($config->animals  == "5"){ echo "selected"; } ?> value="5">5</option>
                      <option <?php if($config->animals  == "6"){ echo "selected"; } ?> value="6">6</option>
                      <option <?php if($config->animals  == "6"){ echo "selected"; } ?> value="6">6</option>
                      <option <?php if($config->animals  == "8"){ echo "selected"; } ?> value="8">8</option>
                      <option <?php if($config->animals  == "9"){ echo "selected"; } ?> value="9">9</option>
                      <option <?php if($config->animals  == "10"){ echo "selected"; } ?> value="10">10</option>
                  </select> 

              </div>
             </div>
                    
           </div>
          
                <div class="col-md-1" style="position: relative;">
                 <button type="button"   onclick="removeupdatebox(this);" class="minusrediconconfig" style="left:15px;top:11px;"><i class="fa fa-minus"></i></button>
               </div>
         
         </div>
        
     <?php endforeach; ?>

     </div>