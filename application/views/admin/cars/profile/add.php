             <?php echo form_open_multipart('admin/cars/profileAdd'); ?>
                <div class="col-md-12 pdhz">
                    <div class="col-md-9 pdhz">
                      <!-- Row 1 -->
                        <div class="col-md-12 pdhz" style="margin-top:10px;" >
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-5" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Statut : </span>
                                        </div>
                                        <div class="col-md-7" style="padding: 0px;">
                                           <select class="form-control" name="statut">
                                               <option value="">Select</option>
                                              
                                               <?php foreach ($car_status as $value): ?>
                                                <option  value="<?= $value->id ?>"><?= $value->name ?></option>
                                              <?php endforeach; ?>
                                               
                                           </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-5" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Immatriculation : </span>
                                        </div>
                                        <div class="col-md-7" style="padding: 0px;">
                                             <input type="text" name="immatriculation" class="form-control" required>
                                        </div>

                                    </div>
                                </div>

                               <div class="col-md-4">
                             
                              </div>
                              
                             
                          </div>


                          <!-- Row 2 -->
                          <div class="col-md-12 pdhz" style="margin-top:10px;" >
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Brand : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                       <select class="form-control" name="marque" required>
                                            <option value="">Select</option>
                                              <?php foreach ($car_brands as $value): ?>
                                                <option  value="<?= $value->id ?>"><?= $value->marque ?></option>
                                              <?php endforeach; ?>
                                          
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Modele : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <select class="form-control" name="modele" required>
                                            <option value="">Select</option>
                                             <?php foreach ($car_models as $value): ?>
                                                <option  value="<?= $value->id ?>"><?= $value->modele ?></option>
                                              <?php endforeach; ?>
                                           
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">

                            </div>
                          </div>

                          <!-- Row 3 -->
                          <div class="col-md-12 pdhz" style="margin-top: 10px;">
                             <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-5" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Date of Immatriculation : </span>
                                        </div>
                                        <div class="col-md-7" style="padding: 0px;">
                                             <input type="text" name="date_immatriculation" id="date_immatriculation" class="form-control datepicker" value="<?php echo date('d/m/Y');?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-5" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Car Old : </span>
                                        </div>
                                        <div class="col-md-7" style="padding: 0px;">
                                           <input type="text" name="car_old" id="car_old" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="form-group">
                                        <div class="col-md-5" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Serie : </span>
                                        </div>
                                        <div class="col-md-7" style="padding: 0px;">
                                           <select class="form-control" name="serie" required>
                                              <option value="">Select</option>
                                               <?php foreach ($car_series as $value): ?>
                                                <option  value="<?= $value->id ?>"><?= $value->serie ?></option>
                                              <?php endforeach; ?>
                                            
                                          </select>
                                        </div>
                                    </div>
                                </div>
                          </div>
                       
                           <!-- Row 4 -->
                           <div class="col-md-12 pdhz" style="margin-top:10px;" >
                              <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-5" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Gear Boxe : </span>
                                        </div>
                                        <div class="col-md-7" style="padding: 0px;">
                                              <select class="form-control" name="boite" required>
                                                    <option value="">Select</option>
                                                     <?php foreach ($car_gearboxes as $value): ?>
                                                      <option  value="<?= $value->id ?>"><?= $value->boite ?></option>
                                                    <?php endforeach; ?>
                                                  
                                                </select>
                                        </div>
                                    </div>
                                </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Carburant : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                      <select class="form-control" name="carburant" required>
                                          <option value="">Select</option>
                                           <?php foreach ($car_fuels as $value): ?>
                                            <option  value="<?= $value->id ?>"><?= $value->carburant ?></option>
                                          <?php endforeach; ?>
                                         
                                      </select>
                                        
                                    </div>
                                </div>
                            </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Kind : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                         <select class="form-control" name="type" required>
                                            <option value="">Select</option>
                                              <?php foreach ($car_types as $value): ?>
                                                    <option  value="<?= $value->id ?>"><?= $value->type_name ?></option>
                                                  <?php endforeach; ?>
                                          
                                        </select>
                                    </div>

                                </div>
                            </div>
                           </div>

                          

                      
                 <!-- Section 2 -->
                 <!-- Row 1 -->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">                           
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Time Belt : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                    <select class="form-control" name="courroie" required>
                                          <option value="">Select</option>
                                            <?php foreach ($car_belts as $value): ?>
                                            <option  value="<?= $value->id ?>"><?= $value->courroie ?></option>
                                          <?php endforeach; ?>
                                      
                                      </select>
                                        <!-- <input name="paysdenaissance" class="form-control" type="text"> -->
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Colour : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                            <select class="form-control" name="couleur" required>
                                                  <option value="">Select</option>
                                                    <?php foreach ($car_colors as $value): ?>
                                                    <option  value="<?= $value->id ?>"><?= $value->couleur ?></option>
                                                  <?php endforeach; ?>
                                                 
                                              </select>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Category : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                    <select class="form-control" name="nature" required>
                                          <option value="">Select</option>
                                           <?php foreach ($car_nature as $value): ?>
                                                    <option  value="<?= $value->id ?>"><?= $value->nature ?></option>
                                                  <?php endforeach; ?>
                                        
                                      </select>
                                    </div>
                                </div>
                            </div>
                          </div>


                     <!-- Row 3 -->
                    
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">
                          <div class="col-md-4">
                              <div class="form-group">
                                  <div class="col-md-5" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Access : </span>
                                  </div>
                                  <div class="col-md-7" style="padding: 0px;">
                                      <select class="form-control" name="car_access">
                                         <option value="1">Yes</option>
                                         <option value="2">No</option>
                                      </select>
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-4">
                              <div class="form-group">
                                  <div class="col-md-5" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Options : </span>
                                  </div>
                                  <div class="col-md-7" style="padding: 0px;">
                                       <input type="text" name="car_options" class="form-control" >
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-4">

                          </div>
                        </div>
                          <div class="col-md-12 pdhz" style="margin-top: 10px;">
                          <div class="col-md-2" style="width: 12%;padding-left: 28px;">
                           Car Configuration : 
                          </div>
                          <div class="col-md-10" style="width: 80%;">
                            <?php  include 'configuration_create.php';?>
                            <?php  include 'configuration_box.php';?>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 10px;">
                          <div class="col-md-6"><span style="font-weight: bold;font-size: 18px;">Previous Owner Details : </span></div>                                                              
                        </div>
                                               <!-- Row 2 -->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Entry Date & Time : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                      <div class="col-md-6 pdlz">
                                         <input type="text" name="date_dentree" class="form-control datepicker"  value="<?php echo date('d/m/Y');?>">
                                       </div>
                                       <div class="col-md-6 pdrz">
                                         <input name="time_dentree" id="time_dentree_picker" class="form-control text-center" value="<?= date('h : i'); ?>" type="text">
                                       </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Entry Kilometers : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                         <input type="text" name="kilometrage_dentree" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Bought Price : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input type="text" class="form-control" name="prix_dachat">
                                        </div>
                                        <div class="col-md-4" style="padding: 0px;">
                                            <select class="form-control" name="currency">
                                                <option value="TTC">TTC</option>
                                                <option value="HT">HT</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div> 

                        </div>

                        <!--  Row 3 b-->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">
                          <div class="col-md-4">
                                  <div class="form-group">
                                      <div class="col-md-5" style="margin-top: 5px;">
                                          <span style="font-weight: normal;">Contract : </span>
                                      </div>
                                      <div class="col-md-7" style="padding: 0px;">
                                          <input type="file" name="image1" class="form-control">
                                      </div>
                                  </div> 
                              </div> 
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Civility : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                         <select class="form-control" name="seller_civilite">
                                            <option value="">Select</option>
                                             <?php foreach ($civilite_data as $value): ?>
                                                    <option  value="<?= $value->id ?>"><?= $value->civilite ?></option>
                                                  <?php endforeach; ?>
                                          
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;padding-right: 0px;">
                                        <span style="font-weight: normal;">First Name : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                       <input type="text" name="seller_prenom" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>

                      <!-- Row 4 -->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">

                          <div class="col-md-4">
                              <div class="form-group">
                                  <div class="col-md-5" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Last Name : </span>
                                  </div>
                                  <div class="col-md-7" style="padding: 0px;">
                                    <input type="text" name="seller_nom" class="form-control">
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                  <div class="col-md-5" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Company : </span>
                                  </div>
                                <div class="col-md-7 pdhz">
                                  <div class="col-md-6" style="padding: 0px;">
                                      <input type="text" name="seller_societe" class="form-control">
                                  </div>
                                  <div class="col-md-6" style="padding: 0px;">
                                        <input type="file" name="image2"  class="form-control">
                                  </div>
                                </div> 
                              </div>
                          </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Address : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                         <input type="text" name="seller_address" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 pdhz" style="margin-top: 10px;">
                          <div class="col-md-4">
                              <div class="form-group">
                                  <div class="col-md-5" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Address 2 : </span>
                                  </div>
                                  <div class="col-md-7" style="padding: 0px;">
                                     <input type="text" name="seller_address2" class="form-control">
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                  <div class="col-md-5" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Zip Code : </span>
                                  </div>
                                  <div class="col-md-7" style="padding: 0px;">
                                       <input type="text" name="seller_postal_code" class="form-control">
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                          
                                  <div class="form-group">
                                      <div class="col-md-5" style="margin-top: 5px;">
                                          <span style="font-weight: normal;">City : </span>
                                      </div>
                                      <div class="col-md-7" style="padding: 0px;">
                                          <input type="text" name="ville" class="form-control">
                                      </div>

                                  </div>
                          </div>
                        </div>

                     <!-- Row 3 -->
                        <div class="col-md-12" style="margin-top: 10px;">
                          <div class="col-md-6"><span style="font-weight: bold;font-size: 18px;">New Owner Details : </span></div>                                                              
                        </div>
                      <div class="col-md-12 pdhz" style="margin-top: 10px;">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Exit Date : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                         <input type="text" name="date_de_sortie" class="form-control datepicker"  value="<?php echo date('d/m/Y');?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Exit Kilometers : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                         <input type="text" name="buyer_kilometrage" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Sold Price : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input type="text" class="form-control" name="buyer_prix_dachat">
                                        </div>
                                        <div class="col-md-4" style="padding: 0px;">
                                            <select class="form-control" name="buyer_currency">
                                                <option value="TTC">TTC</option>
                                                <option value="HT">HT</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            
                        </div>
                        

                        <!--  Row 3 b-->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">

                          <div class="col-md-4">
                                  <div class="form-group">
                                      <div class="col-md-5" style="margin-top: 5px;">
                                          <span style="font-weight: normal;">Contract : </span>
                                      </div>
                                      <div class="col-md-7" style="padding: 0px;">
                                          <input type="file" name="image3" class="form-control">
                                      </div>
                                  </div> 
                          </div> 

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Civility : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                         <select class="form-control" name="buyer_civilite">
                                            <option value="">Select</option>
                                             <?php foreach ($civilite_data as $value): ?>
                                                    <option  value="<?= $value->id ?>"><?= $value->civilite ?></option>
                                                  <?php endforeach; ?>
                                           
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;padding-right: 0px;">
                                        <span style="font-weight: normal;">First Name : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                       <input type="text" name="buyer_prenom" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>

                      <!-- Row 4 -->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">
                          <div class="col-md-4">
                              <div class="form-group">
                                  <div class="col-md-5" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Last Name : </span>
                                  </div>
                                  <div class="col-md-7" style="padding: 0px;">
                                    <input type="text" name="buyer_nom" class="form-control">
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                  <div class="col-md-5" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Company : </span>
                                  </div>
                                  <div class="col-md-7 pdhz">
                                  <div class="col-md-6" style="padding: 0px;">
                                      <input type="text" name="buyer_societe" class="form-control">
                                  </div>
                                  <div class="col-md-6" style="padding: 0px;">
                                        <input type="file" name="image4"  class="form-control">
                                   </div>
                                 </div>
                              </div>
                          </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Address : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                         <input type="text" name="buyer_address" class="form-control">
                                    </div>
                                </div>
                            </div>
            
                        </div>
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">
                          <div class="col-md-4">
                              <div class="form-group">
                                  <div class="col-md-5" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Address 2 : </span>
                                  </div>
                                  <div class="col-md-7" style="padding: 0px;">
                                     <input type="text" name="buyer_address2" class="form-control">
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                  <div class="col-md-5" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Zip Code : </span>
                                  </div>
                                  <div class="col-md-7" style="padding: 0px;">
                                       <input type="text" name="buyer_postal_code" class="form-control">
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                          
                                  <div class="form-group">
                                      <div class="col-md-5" style="margin-top: 5px;">
                                          <span style="font-weight: normal;">City : </span>
                                      </div>
                                      <div class="col-md-7" style="padding: 0px;">
                                          <input type="text" name="buyer_ville" class="form-control">
                                      </div>

                                  </div>
                          </div>
                        </div>
                      
                       </div>
                         <div class="col-md-3 pdhz" style="margin-top: 10px;">
                           <div class="col-md-12 pdhz">
                                <div class="form-group">
                                    <div class="col-md-3" style="margin-top: 5px;">
                                       
                                    </div>
                                    <div class="col-md-4" style="padding: 0px;position: relative;">
                                      <button type="button" id="defaultimgremovebtn" onclick="removedefaultcarimage(this);" class="minusrediconcustomreminder" style="display:none;"><i class="fa fa-minus" style="margin:0px !important;"></i></button>
                                    <img id="preview_car" alt="Preview Logo"
                                        src="<?= base_url() ?>/assets/images/no-preview.jpg"
                                        style="border: 1px solid #75b0d7;cursor: pointer;height: 170px;width: 170px;position: relative;z-index: 10;background-color:#fff;">

                                
                                    </div>
                                </div>      
                              </div> 
                              <div class="col-md-12 pdhz">
                                   <div class="form-group" >
                                        <div class="col-md-3 pdlz" style="margin-top: 5px;">
                                            <span style="font-weight: normal;" id="defaultimgspan">Default Image : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input type="file" class="car_image form-control"  name="car_image" required>
                                        </div>  
                                    </div>
                              </div> 
                              <div class="col-md-12 pdhz" style="margin-top: 10px;">
                                <div class="col-md-12 pdhz">
                                 <div class="form-group">
                                      <div class="col-md-3 pdlz" style="margin-top: 5px;">
                                          <span style="font-weight: normal;">Other Images : </span>
                                      </div>
                                      <div class="col-md-8" style="padding: 0px;">
                                          <input type="file" id="car_more_image" class="form-control"  name="car_more_image" >
                                          <button type="button" onclick="addcarImage();" class="plusgreeniconconfig" style="top:4px;right:-30px;"><i class="fa fa-plus" style="margin:0px !important;"></i></button>
                                      </div>
                                       
                                  </div>
                                </div>

                                <div class="col-md-8 col-md-offset-3 pdhz" id="car_images_div" style="margin-top:10px;">
                                  
                                </div>
                            </div>
                          </div>    
                      </div>
 
                      
                   
                        
                        
                          <div class="col-md-12" style="padding: 30px;">                       
                              <!-- icons -->
                               <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                              <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelProfile()"><span class="fa fa-close"> Cancel </span></button>
                              <!-- icons -->  
                          </div>

                     <?php echo form_close(); ?>
 