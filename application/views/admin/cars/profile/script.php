<script type="text/javascript">
  $(document).ready(function() {
    $("#searchform").appendTo("#DataTables_Table_0_filter");
    $("#searchform").show();
             profileEvent();
       
          $(document).delegate("input[type=text].datepicker", "focusin", function(){
             $(this).datepicker({

                   format: "dd/mm/yyyy"
             });
          });

$(document).delegate("input[type=text]#customremindertimepicker", "focusin", function(){
   $(this).timepicki({
         step_size_minutes:'5',
          show_meridian:false,
          min_hour_value:0,
          max_hour_value:23,
          overflow_minutes:true,
          increase_direction:'up',
          disable_keyboard_mobile: true
   });
});

          $(document).delegate("input[type=text]#notificationtimepicker", "focusin", function(){
             $(this).timepicki({
                   step_size_minutes:'5',
                    show_meridian:false,
                    min_hour_value:0,
                    max_hour_value:23,
                    overflow_minutes:true,
                    increase_direction:'up',
                    disable_keyboard_mobile: true
             });
          });
          for(var i=1;i<4;i++){
               $(document).delegate("input[type=text]#remindertimepicker", "focusin", function(){
               $(this).timepicki({
                     step_size_minutes:'5',
                      show_meridian:false,
                      min_hour_value:0,
                      max_hour_value:23,
                      overflow_minutes:true,
                      increase_direction:'up',
                      disable_keyboard_mobile: true
               });
            });
          }

          

          $(document).delegate("input[type=text]#time_dentree_picker", "focusin", function(){
             $(this).timepicki({
                   step_size_minutes:'5',
                    show_meridian:false,
                    min_hour_value:0,
                    max_hour_value:23,
                    overflow_minutes:true,
                    increase_direction:'up',
                    disable_keyboard_mobile: true
             });
          });

          $(document).delegate("input[type=text]#edit_time_dentree_picker", "focusin", function(){
             $(this).timepicki({
                   step_size_minutes:'5',
                    show_meridian:false,
                    min_hour_value:0,
                    max_hour_value:23,
                    overflow_minutes:true,
                    increase_direction:'up',
                    disable_keyboard_mobile: true
             });
          });
           $(document).on('change','.car_image', function() {
                      readURL(this);
                      document.getElementById('defaultimgremovebtn').style.display='block';
                      document.getElementById('defaultimgspan').innerHTML='Car Image : ';
                      
                  });
           $(document).on('change','.edit_car_image', function() {
                        editreadURL(this);
                        document.getElementById('editdefaultimgremovebtn').style.display='block';
                        document.getElementById('editdefaultimgspan').innerHTML='Car Image : ';
                    });
           $(document).on('change','#country_id', function() {
                        get_region_base_country(this);
                    });
           $(document).on('change','#region_id', function() {
                        get_cities_base_country_region(this);
                    });
           $(document).on('change','#edit_country_id', function() {
                       edit_get_region_base_country(this);
                    });
           $(document).on('change','#edit_region_id', function() {
                        edit_get_cities_base_country_region(this);
                    });
            $(document).on('change','#date_immatriculation', function() {
                       var date   = $(this).val();
                           date   = date.split("/");
                           days   = date[0];
                           months = date[1];
                           years  = date[2];
                           date   = date[1]+"/"+date[0]+"/"+date[2]
                          var age = getAge(date);
                          document.getElementById('car_old').value=age;
                         
                    });
             $(document).on('change','#edit_date_immatriculation', function() {
                       var date   = $(this).val();
                           date   = date.split("/");
                           days   = date[0];
                           months = date[1];
                           years  = date[2];
                           date   = date[1]+"/"+date[0]+"/"+date[2]
                          var age = getAge(date);
                          document.getElementById('edit_car_old').value=age;
                         
                    });
           
  });

  function profileEvent(){
        //removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="profileAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="profileEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="profileDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
function profileAdd()
{
   $('#adddisbreadcrumb').remove();
   $(".breadcrumb").append("<span id='adddisbreadcrumb'> > Add Car</span>");
  setupDivProfile();
  $(".Profileadd").show();
}

function cancelProfile()
{
  $('#adddisbreadcrumb').remove();
  $('#editdisbreadcrumb').remove();
  setupDivProfile();
  $(".ListProfile").show();
}

function profileEdit()
{
    var val = $('.chk-Addprofile-btn').val();
   
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
         $('#editdisbreadcrumb').remove();
         var profileid=$('.Addprofilefullid').val();
         $(".breadcrumb").append("<span id='editdisbreadcrumb'> > "+profileid+"</span>");
        setupDivProfile();
        $(".profileEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/cars/get_ajax_profile_data'; ?>',
                data: {'profile_id': val},
                success: function (result) {
                  document.getElementsByClassName("profileEditajax")[0].innerHTML = result;
                  initMap();
                   /* innner config*/
                  createinnerconfigtable(".configInnerAssuranceTable");
                  createinnerconfigtable(".configInnerTechniqueTable");
                  createinnerconfigtable(".configInnerEntretienTable");
                  createinnerconfigtable(".configInnerReparationTable");
                  createinnerconfigtable(".configInnerSinistreTable");
                  createinnerconfigtable(".configInnerCoutTable");
                  /* innner config*/
                 
                }
            });
}
function profileidEdit(id,fullid){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      $('#editdisbreadcrumb').remove();
      $(".breadcrumb").append("<span id='editdisbreadcrumb'> > "+fullid+"</span>");
      setupDivProfile();
      $(".profileEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/cars/get_ajax_profile_data'; ?>',
              data: {'profile_id': val},
              success: function (result) {

                document.getElementsByClassName("profileEditajax")[0].innerHTML = result;
                initMap();
                /* innner config*/
                createinnerconfigtable(".configInnerAssuranceTable");
                createinnerconfigtable(".configInnerTechniqueTable");
                createinnerconfigtable(".configInnerEntretienTable");
                createinnerconfigtable(".configInnerReparationTable");
                createinnerconfigtable(".configInnerSinistreTable");
                createinnerconfigtable(".configInnerCoutTable");
                /* innner config*/
              }
          });
}
function profileDelete()
{
  var val = $('.chk-Addprofile-btn').val();
  if(val != "")
  {
  setupDivProfile();
  $(".profileDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivProfile()
{
  $(".Profileadd").hide();
  $(".profileEdit").hide();
  $(".ListProfile").hide();
  $(".profileDelete").hide();

}
function setupDivProfileConfig()
{
  $(".Profileadd").hide();
  $(".profileEdit").hide();
  $(".ListProfile").show();
  $(".profileDelete").hide();

}
$('input.chk-mainprofile-template').on('change', function() {
  $('input.chk-mainprofile-template').not(this).prop('checked', false);
   var parent= $(this).parent();
  var sibling=$(parent).next().html();
  var id = $(this).attr('data-input');
  $('.chk-Addprofile-btn').val(id);
  $('#profiledeletid').val(id);
  $('.Addprofilefullid').val(sibling);
});

  function get_region_base_country(region_id=null)
   {
     country_id=$('#country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/cars/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
         html ='';
         html =html + '<option value="">Select Region</option>';
         
         $.each(response, function( index, value ) {  
         if(region_id==value.id)
         {
           html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
         }
         else{
           html =html + '<option value="'+value.id+'">'+value.name+'</option>';
         }
         });
         $('#region_id').html(html);
       }
   }); 
}

function get_cities_base_country_region(region_id=null,cities_id=null)
{

region_id=$('#region_id').val();

$.ajax({
    url: '<?php echo base_url().'admin/cars/ajax_get_cities_listing'; ?>',
    method: 'post',
    data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
    dataType: 'json',
    success: function(response){
    console.log(response);
    html ='';
    html =html + ' <option value="">Select cities</option>';

    $.each(response, function( index, value ) {
    // console.log( value );
                

    if(cities_id==value.id)
    {
        html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';

    }
    else{
        html =html + '<option value="'+value.id+'">'+value.name+'</option>';

    }
    });

    $('#cities_id').html(html);


    }


});
}

 function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#preview_car').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
  }
//For Edit Page

function edit_get_cities_base_country_region(region_id=null,cities_id=null)
{
    region_id=$('#edit_region_id').val();
    $.ajax({
        url: '<?php echo base_url().'admin/cars/ajax_get_cities_listing'; ?>',
        method: 'post',
        data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
        dataType: 'json',
        success: function(response){
            html ='';
            html =html + ' <option value="">Select cities</option>';

            $.each(response, function( index, value ) {
            if(cities_id==value.id)
            {
                html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
            }
            else{
                html =html + '<option value="'+value.id+'">'+value.name+'</option>';
            }
            });
            $('#edit_cities_id').html(html);
        }
    });
}
   function edit_get_region_base_country(region_id=null)
   { 
     country_id=$('#edit_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/cars/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
     html ='';
     html =html + '<option value="">Select Region</option>';
     
     $.each(response, function( index, value ) {
     if(region_id==value.id)
     {
       html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';  
     }
     else{
       html =html + '<option value="'+value.id+'">'+value.name+'</option>';
     }
     });
     $('#edit_region_id').html(html);
     }   
     }); 
   }

 function editreadURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#edit_preview_car').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
  }
  function addcarImage(){
    var property=document.getElementById('car_more_image').files[0];
    var form_Data=new FormData();
    form_Data.append('file',property);
    form_Data.append('<?php  echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash(); ?>');
  
    $.ajax({
      type: "POST",
      url: "<?= base_url() .'admin/cars/add_car_image' ?>",
      data:form_Data,
       processData: false,
       cache:false,
       contentType: false,
      success: function(data) {
        if(data.result=='200'){
         
        
          $('#car_images_div').append('<div class="col-md-6 main_car_div" style="padding:5px;"><div style="position:relative;"><input type="hidden"  name="car_extra_images[]" value="'+data.file+'"><img src="<?= base_url() ?>'+'/uploads/cars/'+ data.file+'" width="100%"  ><button type="button" onclick="removecarimage(this);" class="minusrediconcustomreminder"><i class="fa fa-minus" style="margin:0px !important;"></i></button></div></div>');
        } 
      }
    });
  }
   function editcarImage(){
    var property=document.getElementById('edit_car_more_image').files[0];
    var form_Data=new FormData();
    form_Data.append('file',property);
    form_Data.append('<?php  echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash(); ?>');
  
    $.ajax({
      type: "POST",
      url: "<?= base_url() .'admin/cars/add_car_image' ?>",
      data:form_Data,
       processData: false,
       cache:false,
       contentType: false,
      success: function(data) {
        if(data.result=='200'){
         
        
          $('#edit_car_images_div').append('<div class="col-md-6 main_car_div" style="padding:5px;"><div style="position:relative;"><input type="hidden"  name="car_extra_images[]" value="'+data.file+'"><img src="<?= base_url() ?>'+'/uploads/cars/'+ data.file+'" width="100%" ><button type="button" onclick="removecarimage(this);" class="minusrediconcustomreminder"><i class="fa fa-minus" style="margin:0px !important;"></i></button></div></div>');
        } 
      }
    });
  }

  function removecarimage(event){
    $(event).parents('div.main_car_div').remove();
  }
//For Edit Page

function getAge(dateString) {
  var now = new Date();
  var today = new Date(now.getYear(),now.getMonth(),now.getDate());

  var yearNow = now.getYear();
  var monthNow = now.getMonth();
  var dateNow = now.getDate();

  var dob = new Date(dateString.substring(6,10),
                     dateString.substring(0,2)-1,                   
                     dateString.substring(3,5)                  
                     );

  var yearDob = dob.getYear();
  var monthDob = dob.getMonth();
  var dateDob = dob.getDate();
  var age = {};
  var ageString = "";
  var yearString = "";
  var monthString = "";
  var dayString = "";


  yearAge = yearNow - yearDob;

  if (monthNow >= monthDob)
    var monthAge = monthNow - monthDob;
  else {
    yearAge--;
    var monthAge = 12 + monthNow -monthDob;
  }

  if (dateNow >= dateDob)
    var dateAge = dateNow - dateDob;
  else {
    monthAge--;
    var dateAge = 31 + dateNow - dateDob;

    if (monthAge < 0) {
      monthAge = 11;
      yearAge--;
    }
  }

  age = {
      years: yearAge,
      months: monthAge,
      days: dateAge
      };

  if ( age.years > 1 ) yearString = " years";
  else yearString = " year";
  if ( age.months> 1 ) monthString = " months";
  else monthString = " month";
  if ( age.days > 1 ) dayString = " days";
  else dayString = " day";


  if ( (age.years > 0) && (age.months > 0) && (age.days > 0) )
    ageString = age.years + yearString + ", " + age.months + monthString + ", and " + age.days + dayString;
  else if ( (age.years == 0) && (age.months == 0) && (age.days > 0) )
    ageString = age.days + dayString;
  else if ( (age.years > 0) && (age.months == 0) && (age.days == 0) )
    ageString = age.years + yearString;
  else if ( (age.years > 0) && (age.months > 0) && (age.days == 0) )
    ageString = age.years + yearString + " and " + age.months + monthString;
  else if ( (age.years == 0) && (age.months > 0) && (age.days > 0) )
    ageString = age.months + monthString + " and " + age.days + dayString;
  else if ( (age.years > 0) && (age.months == 0) && (age.days > 0) )
    ageString = age.years + yearString + " and " + age.days + dayString;
  else if ( (age.years == 0) && (age.months > 0) && (age.days == 0) )
    ageString = age.months + monthString;
  else ageString = "Oops! Could not calculate age!";

  return ageString;
}
/* configuration script */
function createbox(){
  
   var val_1=document.getElementById('passengerfield').value;
   var val_2=document.getElementById('lugagefield').value;
   var val_3=document.getElementById('wheelchairfield').value;
   var val_4=document.getElementById('babyfield').value;
   var val_5=document.getElementById('animalfield').value;

   $(".createdbox select:eq(0) option").removeAttr("selected");
   $(".createdbox select:eq(1) option").removeAttr("selected");
   $(".createdbox select:eq(2) option").removeAttr("selected");
   $(".createdbox select:eq(3) option").removeAttr("selected");
   $(".createdbox select:eq(4) option").removeAttr("selected");

   $('.createdbox select:eq(0) option[value="' + val_1 + '"]').attr('selected', 'selected');
   $('.createdbox select:eq(1) option[value="' + val_2 + '"]').attr('selected', 'selected');
   $('.createdbox select:eq(2) option[value="' + val_3 + '"]').attr('selected', 'selected');
   $('.createdbox select:eq(3) option[value="' + val_4 + '"]').attr('selected', 'selected');
   $('.createdbox select:eq(4) option[value="' + val_5 + '"]').attr('selected', 'selected');

   var text=document.getElementsByClassName('createdbox')[0].innerHTML;
   text = "<div class='storebox'>"+text+"</div>";
   $("div.maincapacitybox").append(text);
}
function removebox(e){
   $(e).closest(".storebox").remove();
}
function removeupdatebox(e){
   $(e).closest(".storebox").remove();
}
function updatebox(){
     var val_1=document.getElementById('editpassengerfield').value;
     var val_2=document.getElementById('editlugagefield').value;
     var val_3=document.getElementById('editwheelchairfield').value;
     var val_4=document.getElementById('editbabyfield').value;
     var val_5=document.getElementById('editanimalfield').value;

     $(".createdbox select:eq(0) option").removeAttr("selected");
     $(".createdbox select:eq(1) option").removeAttr("selected");
     $(".createdbox select:eq(2) option").removeAttr("selected");
     $(".createdbox select:eq(3) option").removeAttr("selected");
     $(".createdbox select:eq(4) option").removeAttr("selected");

     $('.createdbox select:eq(0) option[value="' + val_1 + '"]').attr('selected', 'selected');
     $('.createdbox select:eq(1) option[value="' + val_2 + '"]').attr('selected', 'selected');
     $('.createdbox select:eq(2) option[value="' + val_3 + '"]').attr('selected', 'selected');
     $('.createdbox select:eq(3) option[value="' + val_4 + '"]').attr('selected', 'selected');
     $('.createdbox select:eq(4) option[value="' + val_5 + '"]').attr('selected', 'selected');

     var text=document.getElementsByClassName('createdbox')[0].innerHTML;
     text = "<div class='storebox'>"+text+"</div>";
     $("div.maineditcapacitybox").append(text);
}
/* configuration script */

/*custom reminders*/
function addcustomreminderbox(){
 
  var title   = $('#customremindertitlefield').val();
  var subject = $('#customremindersubjectfield').val();
  var date    = $('#customreminderdatefield').val();
  var time    = $('#customremindertimepicker').val();
  var comment = $('#customremindercommentfield').val();
  var car_id  = $('#profile_id').val();
  
  if(title == '' || date == '' || time == '' || comment == '' || subject == ''){
    alert("Please fill the form");
  }else{
       $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/cars/addcustomreminders'; ?>',
                data: {'car_id': car_id,'title':title,'date': date,'time':time,'comment':comment,'subject':subject},
                success: function (data) {
                  
                   if(data.result=='200'){
                       
                           
                            $('#customreminderrecorded').append('<div class="customreminderbox col-md-12 pdhz" style="margin-top:5px;"><div class="col-md-2 text-left pdlz">'+data.record.title+'</div><div class="col-md-2 text-left pdlz">'+data.record.date+'</div><div class="col-md-2 text-left pdlz">'+data.record.time+'</div><div class="col-md-2 text-left pdlz">'+data.record.subject+'</div><div class="col-md-2 text-left pdlz" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">'+data.record.comment+'</div><div class="col-md-2" style="position: relative;width: 8%;"><button type="button" onclick="removecustomreminderbox(this,'+data.record.customreminderid+');" class="minusrediconcustomreminder"><i class="fa fa-minus" style="margin:0px !important;"></i></button></div></div>');
                              $('#customremindertitlefield').val('');
                              $('#customremindercommentfield').val('');
                 }
                else{
                       
                        alert("something went wrong");
                   }
                  


                     
                }
       });
  }  
}
function removecustomreminderbox(e,customreminderid){
  if(customreminderid == ''){
    alert("something went wrong");
  }else{
    $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/cars/removecustomreminders'; ?>',
                data: {'customreminderid': customreminderid},
                success: function (data) {
                   if(data.result=='200'){
                        $(e).closest(".customreminderbox").remove();                            
                   }
                   else{ 
                        alert("something went wrong");
                   }    
                }
       });
    }
}
/*custom reminder*/
/*add car notification*/
function addcarnotification(title){
  document.getElementById('notificationsentloaderbtn').style.display = "block";
  var date=document.getElementById('notificationdatepicker').value;
  var time=document.getElementById('notificationtimepicker').value;
  var car_id=document.getElementById('profile_id').value;

  var form_Data=new FormData();
  form_Data.append('date',date);
  form_Data.append('time',time);
  form_Data.append('car_id',car_id);

  form_Data.append('<?php  echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash(); ?>');
      $.ajax({
      type: "POST",
      url: "<?= base_url() .'admin/cars/addcarnotification' ?>",
      data:form_Data,
      processData: false,
      cache:false,
      contentType: false,
      success: function(data) {
           if(data.result=='200'){
               document.getElementById('notificationsentloaderbtn').style.display = "none";
               $('#carnotificationmaindiv').append('<div class="col-md-12 pdhz"><div class="col-md-2 pdhz text-center" >'+date+'</div><div class="col-md-2 pdhz text-center" >'+time+'</div><div class="col-md-3 pdhz text-center" ><div class="col-md-8 col-md-offset-4 pdhz text-left">'+title+'</div></div><div class="col-md-1 pdhz text-center" ><a href="<?= base_url(); ?>admin/cars/notification_pdf/'+data.record.notification_id+'/'+car_id+'" target="_blank"><img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="pdf icon"></a></div><div class="col-md-2 pdhz text-center" >'+data.record.status+'</div></div>');
               if(data.record.status.toLowerCase() == "sent"){
                    alert("Notification sent via Email.");
               }else{
                 alert("Notification will be send via Emai at "+date+" "+time);
               }
         }
        else{
                document.getElementById('notificationsentloaderbtn').style.display = "none";
                alert("something went wrong");
           }
      }
    });
}
/*add car notification*/
//add car reminder
function addcarreminder(title){
  document.getElementById('remindersentloaderbtn').style.display = "block";
  var date=document.getElementById('reminderdatepicker').value;
  var time=document.getElementById('remindertimepicker').value;
  var car_id=document.getElementById('profile_id').value;

  var form_Data=new FormData();
  form_Data.append('date',date);
  form_Data.append('time',time);
  form_Data.append('car_id',car_id);
  form_Data.append('<?php  echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash(); ?>');
      $.ajax({
      type: "POST",
      url: "<?= base_url() .'admin/cars/addcarreminder' ?>",
      data:form_Data,
      processData: false,
      cache:false,
      contentType: false,
      success: function(data) {
           if(data.result=='200'){
               document.getElementById('remindersentloaderbtn').style.display = "none";
               $('#carremindermaindiv').append('<div class="col-md-12 pdhz"><div class="col-md-2 pdhz text-center" >'+date+'</div><div class="col-md-2 pdhz text-center" >'+time+'</div><div class="col-md-3 pdhz text-center" ><div class="col-md-8 col-md-offset-4 pdhz text-left">'+title+'</div></div><div class="col-md-1 pdhz text-center" ><a href="<?= base_url(); ?>admin/cars/reminder_pdf/'+data.record.reminder_id+'/'+car_id+'" target="_blank"><img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="pdf icon"></a></div><div class="col-md-2 pdhz text-center" >'+data.record.status+'</div></div>');
               if(data.record.status.toLowerCase() == "sent"){
                    alert("Reminder sent via Email.");
               }else{
                 alert("Reminder will be send via Emai at "+date+" "+time);
               }
         }
        else{
                document.getElementById('remindersentloaderbtn').style.display = "none";
                alert("something went wrong");
           }
      }
    });
}
//add car reminder

function removedefaultcarimage(){
  $('.car_image').val(''); 
  $('#preview_car').attr('src', '<?php echo base_url() ?>/assets/images/no-preview.jpg ?>');
  document.getElementById('defaultimgremovebtn').style.display='none';
  document.getElementById('defaultimgspan').innerHTML='Default Image : ';
}
function editremovedefaultcarimage(){
  $('.edit_car_image').val(''); 
  $('#edit_preview_car').attr('src', '<?php echo base_url() ?>/assets/images/no-preview.jpg ?>');
  document.getElementById('editdefaultimgremovebtn').style.display='none';
  document.getElementById('editdefaultimgspan').innerHTML='Default Image : ';
  $('#car_image_pos').val('0'); 
  
}
</script>
