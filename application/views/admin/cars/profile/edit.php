                          <?php
                                 $date_immatriculation = strtotime($car->date_immatriculation);
                                 $date_immatriculation = date('d/m/Y',$date_immatriculation);
                                 $date_immatriculation = $date_immatriculation;


                                 $date_dentree = strtotime($car->date_dentree);
                                 $date_dentree = date('d/m/Y',$date_dentree);
                                 $date_dentree = $date_dentree;


                                 $date_de_sortie = strtotime($car->date_de_sortie);
                                 $date_de_sortie = date('d/m/Y',$date_de_sortie);
                                 $date_de_sortie = $date_de_sortie;

                              
                                 
                                 $car_affect_date = strtotime($driver->car_affect_date);
                                 $car_affect_date = date('d/m/Y',$car_affect_date);
                                 $car_affect_date = $car_affect_date;

                             ?>         


             <?php echo form_open_multipart('admin/cars/profileEdit'); ?>
                <div class="col-md-12 pdhz">
                    <div class="col-md-9 pdhz">
                      <!-- Row 1 -->
                        <div class="col-md-12 pdhz" style="margin-top:10px;" >
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-5" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Statut : </span>
                                        </div>
                                        <div class="col-md-7" style="padding: 0px;">
                                          <input type="hidden" name="profile_id" id="profile_id"  value="<?= $car->id ?>">
                                           <select class="form-control" name="statut">
                                               <option value="">Select</option>
                                               <?php foreach ($car_status as $value): ?>
                                                   <option <?php if($value->id == $car->statut){ echo "selected"; } ?>  value="<?= $value->id ?>"><?= $value->name ?></option>
                                            <?php endforeach; ?>
                                           </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-5" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Immatriculation : </span>
                                        </div>
                                        <div class="col-md-7" style="padding: 0px;">
                                             <input type="text" value="<?= $car->immatriculation ?>"  name="immatriculation" class="form-control" required>
                                        </div>

                                    </div>
                                </div>
                                 <div class="col-md-4">
                                  <div class="form-group">
                                      <div class="col-md-5" >
                                          <span style="font-weight: normal;">Driver Affectation : </span>
                                      </div>
                                      <div class="col-md-7 pdhz">
                                        <div class="col-md-6 pdlz">
                                            <input name="car_affect_date" class="form-control  text-center" value="<?= $car_affect_date ?>" type="text" style="pointer-events: none;background-color: #def4faab !important;">
                                        </div>
                                        <div class="col-md-6 pdrz">
                                            <input name="car_affect_time" id="edit_car_affect_time" class="form-control text-center" value="<?= (($driver->car_affect_time)?$driver->car_affect_time:date('h : i')); ?>" type="text" style="pointer-events: none;background-color: #def4faab !important;">
                                        </div>
                                      </div>
                                  </div>
                                </div>
                          </div>


                          <!-- Row 2 -->
                          <div class="col-md-12 pdhz" style="margin-top:10px;" >
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Brand  : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                       <select class="form-control" name="marque" required>
                                            <option  value="">Select</option>
                                            <?php
                                            foreach ($car_brands as $value): ?>
                                                <option <?php if($value->id == $car->marque){ echo "selected"; } ?> value="<?= $value->id ?>"><?= $value->marque ?></option>
                                           <?php endforeach; ?>
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Modele : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <select class="form-control" name="modele" required>
                                            <option  value="">Select</option>
                                            <?php
                                            foreach ($car_models as $key => $value):?>
                                                <option <?php if($value->id == $car->modele){ echo "selected"; } ?> value="<?= $value->id ?>"><?= $value->modele ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">

                            </div>
                          </div>

                          <!-- Row 3 -->
                          <div class="col-md-12 pdhz" style="margin-top: 10px;">
                             <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-5" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Date of Immatriculation : </span>
                                        </div>
                                        <div class="col-md-7" style="padding: 0px;">
                                             <input type="text" value="<?=  $date_immatriculation ?>"  name="date_immatriculation" id="edit_date_immatriculation" class="form-control datepicker" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-5" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Car Old : </span>
                                        </div>
                                        <div class="col-md-7" style="padding: 0px;">
                                           <input type="text" name="car_old" id="edit_car_old" class="form-control" value="<?= $car->car_old ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="form-group">
                                        <div class="col-md-5" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Serie : </span>
                                        </div>
                                        <div class="col-md-7" style="padding: 0px;">
                                           <select class="form-control" name="serie" required>
                                              <option  value="">Select</option>
                                              <?php
                                              foreach ($car_series as $key => $value): ?>
                                                  <option <?php if($value->id == $car->serie){ echo "selected"; } ?> value="<?= $value->id ?>"><?= $value->serie ?></option>
                                             <?php endforeach; ?>
                                          </select>
                                        </div>
                                    </div>
                                </div>
                          </div>
                       
                           <!-- Row 4 -->
                           <div class="col-md-12 pdhz" style="margin-top:10px;" >
                              <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-5" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Gear Boxe : </span>
                                        </div>
                                        <div class="col-md-7" style="padding: 0px;">
                                              <select class="form-control" name="boite" required>
                                                    <option  value="">Select</option>
                                                    <?php
                                                    foreach ($car_gearboxes as $key => $value): ?>
                                                      <option <?php if($value->id == $car->boite){ echo "selected"; } ?> value="<?= $value->id ?>"><?= $value->boite ?></option>
                                                   <?php endforeach; ?>
                                                </select>
                                        </div>
                                    </div>
                                </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Carburant : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                      <select class="form-control" name="carburant" required>
                                          <option  value="">Select</option>
                                          <?php
                                          foreach ($car_fuels as $key => $value) : ?>
                                              <option <?php if($value->id == $car->carburant){ echo "selected"; } ?> value="<?= $value->id ?>"><?= $value->carburant ?></option>
                                        <?php endforeach; ?>
                                      </select>
                                        
                                    </div>
                                </div>
                            </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Kind : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                         <select class="form-control" name="type" required>
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($car_types)) :
                                                foreach ($car_types as $key => $value): ?>
                                                    <option <?php if($value->id == $car->type){ echo "selected"; } ?> value="<?= $value->id ?>"><?= $value->type_name ?></option>
                                            <?php
                                            endforeach; 
                                            endif;
                                            ?>
                                        </select>
                                    </div>

                                </div>
                            </div>
                           </div>

                      
               
                 <!-- Row 5 -->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">

                           
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Time Belt  : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                    <select class="form-control" name="courroie" required>
                                          <option  value="">Select</option>
                                          <?php
                                          foreach ($car_belts as $key => $value): ?>
                                            <option <?php if($value->id == $car->courroie){ echo "selected"; } ?> value="<?= $value->id ?>"><?= $value->courroie ?></option>
                                         <?php endforeach; ?>
                                      </select>
                                        <!-- <input name="paysdenaissance" class="form-control" type="text"> -->
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Colour : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                 
                                          
                                            <select class="form-control" name="couleur" required>
                                                  <option  value="">Select</option>
                                                  <?php
                                                  foreach ($car_colors as $key => $value): ?>
                                                      <option <?php if($value->id == $car->couleur){ echo "selected"; } ?> value="<?= $value->id ?>"><?= $value->couleur ?></option>
                                                  <?php endforeach; ?>
                                              </select>
                                  
                                    </div>

                          </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Category : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                    <select class="form-control" name="nature" required>
                                          <option  value="">Select</option>
                                          <?php
                                          foreach ($car_nature as $key => $value): ?>
                                              <option <?php if($value->id == $car->nature){ echo "selected"; } ?> value="<?= $value->id ?>"><?= $value->nature ?></option>
                                           <?php endforeach; ?>
                                      </select>
                                    </div>
                                </div>
                            </div>                          
                        </div>


                        <div class="col-md-12 pdhz" style="margin-top: 10px;">
                          <div class="col-md-4">
                              <div class="form-group">
                                  <div class="col-md-5" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Access : </span>
                                  </div>
                                  <div class="col-md-7" style="padding: 0px;">
                                      <select class="form-control" name="car_access">
                                         <option <?php if("1" == $car->car_access){ echo "selected"; } ?> value="1">Yes</option>
                                         <option <?php if("2" == $car->car_access){ echo "selected"; } ?> value="2">No</option>
                                      </select>
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-4">
                              <div class="form-group">
                                  <div class="col-md-5" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Options : </span>
                                  </div>
                                  <div class="col-md-7" style="padding: 0px;">
                                       <input type="text" name="car_options" class="form-control" value="<?= $car->car_options ?>">
                                  </div>
                              </div>
                          </div>
                         <div class="col-md-4">

                         </div>
                        </div>
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">
                          <div class="col-md-2" style="width: 12%;padding-left: 28px;">
                           Car Configuration : 
                          </div>
                          <div class="col-md-10" style="width: 80%;">
                            <?php  include 'configuration_update.php';?>
                            <?php  include 'configuration_box.php';?>
                            </div>
                        </div>


                     <!-- Row 3 -->
                        <div class="col-md-12" style="margin-top: 10px;">
                          <div class="col-md-6"><span style="font-weight: bold;font-size: 18px;">Previous Owner Details :</span></div>                                                              
                        </div>
                        <!-- Row 6 -->
                         <div class="col-md-12 pdhz" style="margin-top: 10px;">

                             <div class="col-md-4">
                                 <div class="form-group">
                                     <div class="col-md-5" style="margin-top: 5px;">
                                         <span style="font-weight: normal;">Entry Date : </span>
                                     </div>
                                     <div class="col-md-7" style="padding: 0px;">
                                      <div class="col-md-6 pdlz">
                                          <input type="text" value="<?=  $date_dentree ?>"  name="date_dentree" class="form-control datepicker"  >
                                          </div>
                                          <div class="col-md-6 pdrz">
                                            <input name="time_dentree" id="edit_time_dentree_picker" class="form-control text-center" value="<?= (($car->time_dentree)?$car->time_dentree:date('h : i')); ?>" type="text">
                                          </div>
                                     </div>
                                 </div>
                             </div>

                             <div class="col-md-4">
                                 <div class="form-group">
                                     <div class="col-md-5" style="margin-top: 5px;">
                                         <span style="font-weight: normal;">Entry Kilometers : </span>
                                     </div>
                                     <div class="col-md-7" style="padding: 0px;">
                                          <input type="text" value="<?= $car->kilometrage_dentree ?>"  name="kilometrage_dentree" class="form-control">
                                     </div>
                                 </div>
                             </div>

                             <div class="col-md-4">
                                 <div class="form-group">
                                     <div class="col-md-5" style="margin-top: 5px;">
                                         <span style="font-weight: normal;">Bought Price : </span>
                                     </div>
                                     <div class="col-md-7" style="padding: 0px;">
                                         <div class="col-md-8" style="padding: 0px;">
                                             <input type="text" value="<?= $car->prix_dachat ?>"  class="form-control" name="prix_dachat">
                                         </div>
                                         <div class="col-md-4" style="padding: 0px;">
                                             <select class="form-control" name="currency">
                                                 <option <?php if("TTC" == $car->currency){ echo "selected"; } ?> value="TTC">TTC</option>
                                                 <option <?php if("HT" == $car->currency){ echo "selected"; } ?> value="HT">HT</option>
                                             </select>
                                         </div>
                                     </div>

                                 </div>
                             </div>  
                         </div>

                        <!--  Row 3 b-->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">
                          <div class="col-md-4">
                                  <div class="form-group">
                                      <div class="col-md-5" style="margin-top: 5px;">
                                          <span style="font-weight: normal;">Contract : </span>
                                      </div>
                                      <div class="col-md-7" style="padding: 0px;">
                                          <input type="file" name="image1" class="form-control">
                                      </div>
                                  </div>
                                  
                              </div> 
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Civility : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                         <select class="form-control" name="seller_civilite">
                                            <option value="">Select</option>
                                            <?php
                                            foreach ($civilite_data as $key => $value): ?>
                                                <option <?php if($value->id == $car->seller_civilite){ echo "selected"; } ?> value="<?= $value->id ?>"><?= $value->civilite ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                             <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;padding-right: 0px;">
                                        <span style="font-weight: normal;">First Name : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                       <input type="text" value="<?= $car->seller_prenom ?>"  name="seller_prenom" class="form-control">
                                    </div>
                                </div>
                            </div>


                        </div>

                      <!-- Row 4 -->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Last Name : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                      <input type="text" value="<?= $car->seller_nom ?>"  name="seller_nom" class="form-control">
                                    </div>
                                </div>
                            </div>

                            

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Company : </span>
                                    </div>
                                    <div class="col-md-7 pdhz">
                                    <div class="col-md-6" style="padding: 0px;">
                                        <input type="text" value="<?= $car->seller_societe ?>"  name="seller_societe" class="form-control">
                                    </div>
                                    <div class="col-md-6" style="padding: 0px;">
                                          <input type="file" name="image2"  class="form-control">
                                    </div>
                                  </div>

                                   
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Address : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                         <input type="text" value="<?= $car->seller_address ?>"  name="seller_address" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">
                          <div class="col-md-4">
                              <div class="form-group">
                                  <div class="col-md-5" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Address 2 : </span>
                                  </div>
                                  <div class="col-md-7" style="padding: 0px;">
                                     <input type="text" value="<?= $car->seller_address2 ?>"  name="seller_address2" class="form-control">
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                  <div class="col-md-5" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Zip Code : </span>
                                  </div>
                                  <div class="col-md-7" style="padding: 0px;">
                                       <input type="text" value="<?= $car->seller_postal_code ?>"  name="seller_postal_code" class="form-control">
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                                  <div class="form-group">
                                      <div class="col-md-5" style="margin-top: 5px;">
                                          <span style="font-weight: normal;">City : </span>
                                      </div>
                                      <div class="col-md-7" style="padding: 0px;">
                                          <input type="text" value="<?= $car->ville ?>"  name="ville" class="form-control">
                                      </div>
                                  </div>
                          </div>
                        </div>

                     <!-- Row 3 -->
                        <div class="col-md-12" style="margin-top: 10px;">
                          <div class="col-md-6"><span style="font-weight: bold;font-size: 18px;">New Owner Details : </span></div>                                                              
                        </div>
                    <div class="col-md-12 pdhz" style="margin-top: 10px;">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Exit Date : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                         <input type="text" value="<?=  $date_de_sortie ?>"  name="date_de_sortie" class="form-control datepicker"  >
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Exit Kilometers : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                         <input type="text" value="<?= $car->buyer_kilometrage ?>"  name="buyer_kilometrage" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Sold Price :  </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                        <div class="col-md-8" style="padding: 0px;">
                                            <input type="text" value="<?= $car->buyer_prix_dachat ?>"  class="form-control" name="buyer_prix_dachat">
                                        </div>
                                        <div class="col-md-4" style="padding: 0px;">
                                            <select class="form-control" name="buyer_currency">
                                                <option <?php if("TTC" == $car->buyer_currency){ echo "selected"; } ?> value="TTC">TTC</option>
                                                <option <?php if("HT" == $car->buyer_currency){ echo "selected"; } ?> value="HT">HT</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                           
                        </div>
                        

                        <!--  Row 3 b-->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">
                          <div class="col-md-4">
                                  <div class="form-group">
                                      <div class="col-md-5" style="margin-top: 5px;">
                                          <span style="font-weight: normal;">Contract : </span>
                                      </div>
                                      <div class="col-md-7" style="padding: 0px;">
                                          <input type="file" name="image3" class="form-control">
                                      </div>
                                  </div>
                                  
                              </div> 

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Civility : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                         <select class="form-control" name="buyer_civilite">
                                            <option value="">Select</option>
                                            <?php
                                            foreach ($civilite_data as $key => $value): ?>
                                                <option <?php if($value->id == $car->buyer_civilite){ echo "selected"; } ?> value="<?= $value->id ?>"><?= $value->civilite ?></option>
                                             <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;padding-right: 0px;">
                                        <span style="font-weight: normal;">First Name : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                       <input type="text" value="<?= $car->buyer_prenom ?>"  name="buyer_prenom" class="form-control">
                                    </div>
                                </div>
                            </div>
                          

                        </div>

                      <!-- Row 4 -->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">
                          <div class="col-md-4">
                              <div class="form-group">
                                  <div class="col-md-5" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Last Name : </span>
                                  </div>
                                  <div class="col-md-7" style="padding: 0px;">
                                    <input type="text" value="<?= $car->buyer_nom ?>"  name="buyer_nom" class="form-control">
                                  </div>
                              </div>
                          </div>


                          <div class="col-md-4">
                              <div class="form-group">
                                  <div class="col-md-5" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Company : </span>
                                  </div>
                                  <div class="col-md-7 pdhz">
                                  <div class="col-md-6" style="padding: 0px;">
                                      <input type="text" value="<?= $car->buyer_societe ?>"  name="buyer_societe" class="form-control">
                                  </div>
                                  <div class="col-md-6" style="padding: 0px;">
                                        <input type="file" name="image4"  class="form-control">
                                  </div>
                                </div>

                                 
                              </div>
                          </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-5" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Address : </span>
                                    </div>
                                    <div class="col-md-7" style="padding: 0px;">
                                         <input type="text" value="<?= $car->buyer_address ?>"  name="buyer_address" class="form-control">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">
                          <div class="col-md-4">
                              <div class="form-group">
                                  <div class="col-md-5" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Address 2 : </span>
                                  </div>
                                  <div class="col-md-7" style="padding: 0px;">
                                     <input type="text" value="<?= $car->buyer_address2 ?>"  name="buyer_address2" class="form-control">
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                  <div class="col-md-5" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Zip Code : </span>
                                  </div>
                                  <div class="col-md-7" style="padding: 0px;">
                                       <input type="text" value="<?= $car->buyer_postal_code ?>"  name="buyer_postal_code" class="form-control">
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                          
                                  <div class="form-group">
                                      <div class="col-md-5" style="margin-top: 5px;">
                                          <span style="font-weight: normal;">City : </span>
                                      </div>
                                      <div class="col-md-7" style="padding: 0px;">
                                          <input type="text" value="<?= $car->buyer_ville ?>"  name="buyer_ville" class="form-control">
                                      </div>

                                  </div>
                          </div>
                        </div>

                    </div>
                    <div class="col-md-3 pdhz" style="margin-top:10px;">
                       <div class="col-md-12 pdhz">
                           <div class="form-group">
                               <div class="col-md-3" style="margin-top: 5px;">
                                  
                               </div>
                               <div class="col-md-4" style="padding: 0px;position: relative;">
                                <input type="hidden" name="car_image_pos" id="car_image_pos" value="<?= (($car->car_image)?'1':'0'); ?>">
                                <button type="button" id="editdefaultimgremovebtn" onclick="editremovedefaultcarimage(this);" class="minusrediconcustomreminder" <?= (($car->car_image)?'style="display:block;"':'style="display:none;"');?> ><i class="fa fa-minus" style="margin:0px !important;"></i></button>
                                 <?php if($car->car_image): ?>
                               <img id="edit_preview_car" alt="Preview Logo"
                                   src="<?= base_url() .'/uploads/cars/'. $car->car_image ?>"
                                   style="border: 1px solid #75b0d7;cursor: pointer;height: 170px;width: 170px;background-color:#fff;position: relative;z-index: 10;">
                                    <?php else: ?>
                                     <img id="edit_preview_car" alt="Preview Logo"
                                   src="<?= base_url() ?>/assets/images/no-preview.jpg"
                                   style="border: 1px solid #75b0d7;cursor: pointer;height: 170px;width: 170px;background-color:#fff;position: relative;z-index: 10;">
                                  <?php endif; ?>
                           
                               </div>
                           </div>      
                         </div> 

                       
                           <div class="col-md-12 pdhz">
                                <div class="form-group" >
                                     <div class="col-md-3 pdlz" style="margin-top: 5px;">
                                         <span style="font-weight: normal;" id="editdefaultimgspan"> <?= (($car->car_image)?'Car Image : ':'Default Image : '); ?>  </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input type="file" class="edit_car_image form-control"  name="car_image" >
                                     </div>
                                 </div>
                             </div>
                              <div class="col-md-12 pdhz" style="margin-top: 10px;">
                                <div class="col-md-12 pdhz" >
                                <div class="form-group" >
                                     <div class="col-md-3 pdlz" style="margin-top: 5px;">
                                         <span style="font-weight: normal;">Other Images : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                         <input type="file" id="edit_car_more_image" class="form-control"  name="car_more_image">
                                     </div>
                                      <div class="col-md-1 pdhz" style="width: 8% !important;position: relative;">
                                       <button type="button" onclick="editcarImage();" class="plusgreeniconconfig" style="top:4px;"><i class="fa fa-plus" style="margin:0px !important;"></i></button>
                                    </div>
                                 </div>
                               </div>
                               <div class="col-md-8 col-md-offset-3 pdhz" id="edit_car_images_div" style="margin-top:10px;">
                                 <?php foreach ($carimages as $val): ?>
                                   <div class="col-md-6 main_car_div" style="padding:5px;"><div style="position:relative;"><input type="hidden"  name="car_extra_images[]" value="<?= $val->image ?>"><img src="<?= base_url() .'/uploads/cars/'. $val->image ?>" width="100%" ><button type="button" onclick="removecarimage(this);" class="minusrediconcustomreminder"><i class="fa fa-minus" style="margin:0px !important;"></i></button></div></div>
                                 <?php endforeach; ?>
                               </div>
                             </div>
                   
                      </div> 
                     </div>     
            <div class="col-md-12 pdlz" style="margin-top: 20px;">
                     <!--Reminder and Notification and custom reminder -->
              <div class="col-md-6" >
                  <!-- Notification -->
                      <?php  include 'notifications.php';?>
                  <!-- Notification -->
                  <!-- Reminders -->
                      <?php  include 'reminders.php';?>
                  <!-- Reminder -->
                  <!-- Custom Reminders -->
                      <?php  include 'customreminders.php';?>
                  <!-- Custom Reminders -->
               </div>
               <!--Reminder and Notification and custom reminder -->

               <!-- car map -->
              <div class="col-md-5 col-md-offset-1" style="padding-right: 0px;">
                  <div class="map-wrapper" >
                     <div id="map"></div>
                  </div>
                 <input id="mapaddress" type="hidden" value="<?= $driver->address ?>" />
                 <input id="mapaddress2" type="hidden" value="<?= $driver->address2 ?>" />
                 <input id="mapzipcode" type="hidden" value="<?= $driver->postalcode ?>" />
                 <input id="mapcity" type="hidden" value="<?= $this->cars_model->getSingleRecord('vbs_cities',['id'=>$driver->villedenaissance])->name ?>" />
                 <input id="mapmobile" type="hidden" value="<?= $driver->mobile ?>" />
                 <input id="mapphone" type="hidden" value="<?= $driver->phone ?>" />
                 <input id="mapname" type="hidden" value="<?= $this->cars_model->getSingleRecord('vbs_drivercivilite',['id'=>$driver->civilite])->civilite.' '.$driver->prenom.' '.$driver->nom ?>" />
                 <input id="mapdriverimg" type="hidden" value="<?= $driver->driverImg ?>" />
                  <input id="mapcarimg" type="hidden" value="<?= $car->car_image ?>" />
                  <input id="mapcarmodel" type="hidden" value="<?= $this->cars_model->getsinglerecord('vbs_carmodele',['id'=>$car->modele])->modele; ?>" />
                   <input id="mapcarbrand" type="hidden" value="<?=  $this->cars_model->getsinglerecord('vbs_carmarque',['id'=>$car->marque])->marque; ?>" />
                   <input id="mapimmatriculation" type="hidden" value="<?=  $car->immatriculation ?>" />
             </div>
               <!-- car map -->
            </div>   
                           <div class="col-md-12" style="padding: 30px;">                       
                             <!-- icons -->
                              <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                             <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelProfile()"><span class="fa fa-close"> Cancel </span></button>
                             <!-- icons -->  
                          </div>

                     <?php echo form_close(); ?>
 