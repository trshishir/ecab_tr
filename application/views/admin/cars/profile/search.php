       <div id="searchform" style="display: none;">
        <?php
        $attributes = array("name" => 'car_search_form',"class"=>'form-inline', "id" => 'car_search_form');
        echo form_open('admin/cars/get_cars_bysearch',$attributes);
        ?>
             <div class="form-group">
               <select name="carburant" style="float: left;width:150px !important;margin-right: 2px;" class="form-control" >
                <option value="">Carburant</option>
             <?php
                 foreach ($car_fuels as $key => $value) : ?>
                     <option  <?php if($value->id == $search_burant){ echo "selected"; } ?> value="<?= $value->id ?>"><?= $value->carburant ?></option>
               <?php endforeach; ?>
             
              </select> 
            </div>
            <div class="form-group">
               <input type="text" name="immatriculation" class="form-control" placeholder="Immatriculation" value="<?= $search_immatriculation ?>"/> 
            </div>
             <div class="form-group">
               <select name="car_access" style="float: left;width:150px !important;margin-right: 2px;" class="form-control" >
                <option value="">Access</option>
            
                   <option <?php if('1' == $search_access){ echo "selected"; } ?> value="1">Yes</option>
                   <option <?php if('2' == $search_access){ echo "selected"; } ?> value="2">No</option>
            
             
              </select> 
            </div>
           
              <div class="form-group">
                 <input type="text" name="from_period" class="bsearchdatepicker bdr form-control" placeholder="From" value="<?= $search_from_period; ?>" autocomplete="off"/> 
              </div>
              <div class="form-group">
                 <input type="text" name="to_period" class="bsearchdatepicker bdr form-control" placeholder="To"  value="<?= $search_to_period; ?>" autocomplete="off"/> 
              </div>
               
  
               <div class="form-group">
                 <select name="statut" style="float: left;width:150px !important;" class="form-control">
                  <option value="">Statut</option>
                 <?php foreach ($car_status as $item): ?>
                   <option  <?php if($item->id == $search_status){ echo "selected"; } ?> value="<?= $item->id ?>"><?= $item->name ?></option>
                   <?php endforeach; ?>
               
                </select> 
              </div>
            <div class="form-group">
              <button type="submit" class="btn btn-default searchcarbtn" style="height: 27px;line-height: 3px;outline: none;margin-left: 5px;">Search</button>
            </div>
           <?php echo form_close(); ?>
       </div>