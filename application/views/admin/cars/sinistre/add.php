             <?php
              $attributes = array("name" => 'create_sinistre_form', "id" => 'create_sinistre_form'); 
              echo form_open_multipart('admin/cars/sinistreAdd',$attributes); ?>
                <input type="hidden" value="<?= $car->id ?>" name="profile_id">
                      <!-- Row 1 -->
                        <div class="col-md-12 pdhz" style="margin-top:10px;" >
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Statut : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                           <select class="form-control" name="statut" required>
                                               <option value="">Select</option>
                                               <option value="1">Show</option>
                                               <option value="0">Hide</option> 
                                           </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                   <div class="form-group">
                                       <div class="col-md-4" style="margin-top: 5px;">
                                           <span style="font-weight: normal;">Date : </span>
                                       </div>
                                       <div class="col-md-8" style="padding: 0px;">
                                            <input type="text" name="sinistre_date" class="form-control datepicker" value="<?php echo date('d/m/Y');?>">
                                       </div>
                                   </div>
                                </div>

                               <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Garage : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                       <select class="form-control" name="garage" required>
                                           <option value="">Select</option>
                                           <option value="xyz">xyz</option>
                                       </select>
                                    </div>
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Responsability : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                         <input type="text" name="responsability" id="responsability" class="form-control">
                                    </div>
                                </div>
                              </div>
                          </div>
                          <!-- Row 2 -->
                          <div class="col-md-12 pdhz" style="margin-top: 10px;">
                             <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Constat : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                             <input type="file" name="constat"  class="form-control" required >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Report : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                           <input type="file" name="report" class="form-control" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                       <div class="form-group">
                                           <div class="col-md-4" style="margin-top: 5px;">
                                               <span style="font-weight: normal;">Invoice : </span>
                                           </div>
                                           <div class="col-md-8" style="padding: 0px;">
                                               <input type="file" name="invoice" class="form-control" required>
                                           </div>
                                       </div>
                                   </div>
                                   <div class="col-md-3">
                 
                                   </div>
                      
                          </div>


                        
                        
                          <div class="col-md-12" style="padding: 30px;">                       
                              <!-- icons -->
                               <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                              <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelSinistre()"><span class="fa fa-close"> Cancel </span></button>
                              <!-- icons -->  
                          </div>

                     <?php echo form_close(); ?>
 