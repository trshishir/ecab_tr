<script type="text/javascript">
  $(document).ready(function() {

           $(document).on('change','#sinistre_country_id', function() {
                      sinistre_get_region_base_country(this);
                  });
           $(document).on('change','#sinistre_region_id', function() {
                      sinistre_get_cities_base_country_region(this);
                  });
           $(document).on('change','#edit_sinistre_country_id', function() {
                      edit_sinistre_get_region_base_country(this);
                  });
           $(document).on('change','#edit_sinistre_region_id', function() {
                      edit_sinistre_get_cities_base_country_region(this);
                  });

            $(document).on('change','input.chk-mainsinistre-template', function() {
                      $('input.chk-mainsinistre-template').not(this).prop('checked', false);
                      var id = $(this).attr('data-input');
                      $('.chk-Addsinistre-btn').val(id);
                      $('#sinistredeletid').val(id);
                  });        

  });
function sinistreAdd()
{
  setupDivSinistre();
  $(".sinistreAdd").show();
}

function cancelSinistre()
{
  setupDivSinistre();
  $(".ListSinistre").show();
}

function sinistreEdit()
{
    var val = $('.chk-Addsinistre-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivSinistre();
        $(".sinistreEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/cars/sinistreAjax'; ?>',
                data: {'sinistre_id': val},
                success: function (result) {
                  document.getElementsByClassName("sinistreEditajax")[0].innerHTML = result;
                
                }
            });
}
function sinistreidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivSinistre();
      $(".sinistreEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/cars/sinistreAjax'; ?>',
              data: {'sinistre_id': val},
              success: function (result) {
                document.getElementsByClassName("sinistreEditajax")[0].innerHTML = result;
               
              }
          });
}
function sinistreDelete()
{
  var val = $('.chk-Addsinistre-btn').val();
  if(val != "")
  {
  setupDivSinistre();
  $(".sinistreDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivSinistre()
{
  $(".sinistreAdd").hide();
  $(".sinistreEdit").hide();
  $(".ListSinistre").hide();
  $(".sinistreDelete").hide();

}
function setupDivSinistreConfig()
{
  $(".sinistreAdd").hide();
  $(".sinistreEdit").hide();
  $(".ListSinistre").show();
  $(".sinistreDelete").hide();

}


  function sinistre_get_region_base_country(region_id=null)
   {
     country_id=$('#sinistre_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
         html ='';
         html =html + '<option value="">Select Region</option>';
         
         $.each(response, function( index, value ) {  
         if(region_id==value.id)
         {
           html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
         }
         else{
           html =html + '<option value="'+value.id+'">'+value.name+'</option>';
         }
         });
         $('#sinistre_region_id').html(html);
       }
   }); 
}

function sinistre_get_cities_base_country_region(region_id=null,cities_id=null)
{

region_id=$('#sinistre_region_id').val();

$.ajax({
    url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
    method: 'post',
    data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
    dataType: 'json',
    success: function(response){
    console.log(response);
    html ='';
    html =html + ' <option value="">Select cities</option>';

    $.each(response, function( index, value ) {
    // console.log( value );
                

    if(cities_id==value.id)
    {
        html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';

    }
    else{
        html =html + '<option value="'+value.id+'">'+value.name+'</option>';

    }
    });

    $('#sinistre_cities_id').html(html);


    }


});
}


//For Edit Page

function edit_sinistre_get_cities_base_country_region(region_id=null,cities_id=null)
{
    region_id=$('#edit_sinistre_region_id').val();
    $.ajax({
        url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
        method: 'post',
        data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
        dataType: 'json',
        success: function(response){
            html ='';
            html =html + ' <option value="">Select cities</option>';

            $.each(response, function( index, value ) {
            if(cities_id==value.id)
            {
                html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
            }
            else{
                html =html + '<option value="'+value.id+'">'+value.name+'</option>';
            }
            });
            $('#edit_sinistre_cities_id').html(html);
        }
    });
}
   function edit_sinistre_get_region_base_country(region_id=null)
   { 
     country_id=$('#edit_sinistre_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
     html ='';
     html =html + '<option value="">Select Region</option>';
     
     $.each(response, function( index, value ) {
     if(region_id==value.id)
     {
       html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';  
     }
     else{
       html =html + '<option value="'+value.id+'">'+value.name+'</option>';
     }
     });
     $('#edit_sinistre_region_id').html(html);
     }   
     }); 
   }
//For Edit Page

//create form submit
$(document).on('submit',"form#create_sinistre_form",function(event){
    
     var str = new FormData(this);
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      processData: false,
      cache:false,
      contentType: false,
      success: function(data) {
        if(data.result == '200'){
          //alert(data.record);
         setupDivSinistre();
         $('.ListSinistre').empty();
         $('.ListSinistre').html(data.record);
         createinnerconfigtable('.configInnerSinistreTable');
         sinistreEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivSinistre();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListSinistre").show();
        }
     document.getElementById("create_sinistre_form").reset();
      }
    });
    return false;
  });
//create form submit

//update form submit
$(document).on('submit',"form#update_sinistre_form",function(event){
    
     var str = new FormData(this);
     var action = $(this).attr('action'); 
     $.ajax({
      type: "POST",
      url: action,
      data:str,
      processData: false,
      cache:false,
      contentType: false,
      success: function(data) {
        if(data.result == '200'){
       
         setupDivSinistre();
         $('.ListSinistre').empty();
         $('.ListSinistre').html(data.record);
         createinnerconfigtable('.configInnerSinistreTable');
         sinistreEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
          
          setupDivSinistre();
          var msg=data.message;
          var msgType=data.messageType;
          var className=data.className;
          var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
           if ($("#inner_alert_div").hasClass("alert-success")) {
            $('#inner_alert_div').removeClass("alert-success");
          }
          if ($("#inner_alert_div").hasClass("alert-danger")) {
            $('#inner_alert_div').removeClass("alert-danger");
          }
          $('#inner_alert_div').addClass(className);
          $('#inner_message_div').empty();
          $('#inner_message_div').html(htmlContent);
          $('#inner_alert_div').show();
          $(".ListSinistre").show();
        }
     document.getElementById("update_sinistre_form").reset();
      }
    });
    return false;
  });
//update form submit

//delete form submit
$(document).on('submit',"form#deletesinistreform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivSinistre();
         $('.ListSinistre').empty();
         $('.ListSinistre').html(data.record);
         createinnerconfigtable('.configInnerSinistreTable');
         sinistreEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivSinistre();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListSinistre").show();
        }
     document.getElementById("addsinistreform").reset();
      }
    });
    return false;
  });
//delete form submit
</script>
