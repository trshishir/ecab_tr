            <?php
               $sinistre_date = strtotime($sinistre->sinistre_date);
               $sinistre_date = date('d/m/Y',$sinistre_date);
               $sinistre_date = $sinistre_date;
            
           ?>



              <?php
               $attributes = array("name" => 'update_sinistre_form', "id" => 'update_sinistre_form'); 
               echo form_open_multipart('admin/cars/sinistreEdit',$attributes); ?>
                    <input type="hidden" value="<?= $sinistre->id ?>" name="sinistre_id">
                      <!-- Row 1 -->
                    <div class="col-md-12 pdhz" style="margin-top:10px;" >
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Statut : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                       <select class="form-control" name="statut">
                                           <option value="">Select</option>
                                           <option <?php if("1" == $sinistre->statut){ echo "selected"; } ?> value="1">Show</option>
                                           <option <?php if("0" == $sinistre->statut){ echo "selected"; } ?> value="0">Hide</option> 
                                       </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                               <div class="form-group">
                                   <div class="col-md-4" style="margin-top: 5px;">
                                       <span style="font-weight: normal;">Date : </span>
                                   </div>
                                   <div class="col-md-8" style="padding: 0px;">
                                        <input type="text" name="sinistre_date" class="form-control datepicker" value="<?= $sinistre_date ?>">
                                   </div>
                               </div>
                            </div>

                           <div class="col-md-3">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 5px;">
                                    <span style="font-weight: normal;">Garage : </span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                   <select class="form-control" name="garage">
                                       <option value="">Select</option>
                                       <option <?php if("xyz" == $sinistre->garage){ echo "selected"; } ?> value="xyz">xyz</option>
                                   </select>
                                </div>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 5px;">
                                    <span style="font-weight: normal;">Responsability : </span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                     <input type="text" name="responsability" id="responsability" class="form-control" value="<?= $sinistre->responsability ?>">
                                </div>
                            </div>
                          </div>
                      </div>
                      <!-- Row 2 -->
                      <div class="col-md-12 pdhz" style="margin-top: 10px;">
                         <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Constat : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                         <input type="file" name="constat"  class="form-control" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Report : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                       <input type="file" name="report" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                   <div class="form-group">
                                       <div class="col-md-4" style="margin-top: 5px;">
                                           <span style="font-weight: normal;">Invoice : </span>
                                       </div>
                                       <div class="col-md-8" style="padding: 0px;">
                                           <input type="file" name="invoice" class="form-control">
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-3">
                    
                               </div>
                    
                      </div>

                        
                        
                          <div class="col-md-12" style="padding: 30px;">                       
                              <!-- icons -->
                               <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                              <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelSinistre()"><span class="fa fa-close"> Cancel </span></button>
                              <!-- icons -->  
                          </div>

                     <?php echo form_close(); ?>
 