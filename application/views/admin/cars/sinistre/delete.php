
  <?php
  $attributes = array("name" => 'delete_sinistre_form', "id" => 'deletesinistreform');   
  echo form_open('admin/cars/sinistreDelete',$attributes); ?>
    <input  name="tablename" value="vbs_clientsinistre" type="hidden" >
    <input type="hidden" id="sinistredeletid" name="delete_sinistre_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <button type="submit" class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>
    <a class="btn btn-default" style="cursor: pointer;" onclick="cancelSinistre()"><span class="delete-icon">No</span></a>
  <?php echo form_close(); ?>
