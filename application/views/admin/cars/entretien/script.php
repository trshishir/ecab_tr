<script type="text/javascript">
  $(document).ready(function() {

           $(document).on('change','#entretien_country_id', function() {
                      entretien_get_region_base_country(this);
                  });
           $(document).on('change','#entretien_region_id', function() {
                      entretien_get_cities_base_country_region(this);
                  });
           $(document).on('change','#edit_entretien_country_id', function() {
                      edit_entretien_get_region_base_country(this);
                  });
           $(document).on('change','#edit_entretien_region_id', function() {
                      edit_entretien_get_cities_base_country_region(this);
                  });

            $(document).on('change','input.chk-mainentretien-template', function() {
                      $('input.chk-mainentretien-template').not(this).prop('checked', false);
                      var id = $(this).attr('data-input');
                      $('.chk-Addentretien-btn').val(id);
                      $('#entretiendeletid').val(id);
                  });        

  });
function entretienAdd()
{
  setupDivEntretien();
  $(".entretienAdd").show();
}

function cancelEntretien()
{
  setupDivEntretien();
  $(".ListEntretien").show();
}

function entretienEdit()
{
    var val = $('.chk-Addentretien-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivEntretien();
        $(".entretienEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/cars/entretienAjax'; ?>',
                data: {'entretien_id': val},
                success: function (result) {
                  document.getElementsByClassName("entretienEditajax")[0].innerHTML = result;
                
                }
            });
}
function entretienidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivEntretien();
      $(".entretienEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/cars/entretienAjax'; ?>',
              data: {'entretien_id': val},
              success: function (result) {
                document.getElementsByClassName("entretienEditajax")[0].innerHTML = result;
               
              }
          });
}
function entretienDelete()
{
  var val = $('.chk-Addentretien-btn').val();
  if(val != "")
  {
  setupDivEntretien();
  $(".entretienDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivEntretien()
{
  $(".entretienAdd").hide();
  $(".entretienEdit").hide();
  $(".ListEntretien").hide();
  $(".entretienDelete").hide();

}
function setupDivEntretienConfig()
{
  $(".entretienAdd").hide();
  $(".entretienEdit").hide();
  $(".ListEntretien").show();
  $(".entretienDelete").hide();

}


  function entretien_get_region_base_country(region_id=null)
   {
     country_id=$('#entretien_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
         html ='';
         html =html + '<option value="">Select Region</option>';
         
         $.each(response, function( index, value ) {  
         if(region_id==value.id)
         {
           html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
         }
         else{
           html =html + '<option value="'+value.id+'">'+value.name+'</option>';
         }
         });
         $('#entretien_region_id').html(html);
       }
   }); 
}

function entretien_get_cities_base_country_region(region_id=null,cities_id=null)
{

region_id=$('#entretien_region_id').val();

$.ajax({
    url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
    method: 'post',
    data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
    dataType: 'json',
    success: function(response){
    console.log(response);
    html ='';
    html =html + ' <option value="">Select cities</option>';

    $.each(response, function( index, value ) {
    // console.log( value );
                

    if(cities_id==value.id)
    {
        html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';

    }
    else{
        html =html + '<option value="'+value.id+'">'+value.name+'</option>';

    }
    });

    $('#entretien_cities_id').html(html);


    }


});
}


//For Edit Page

function edit_entretien_get_cities_base_country_region(region_id=null,cities_id=null)
{
    region_id=$('#edit_entretien_region_id').val();
    $.ajax({
        url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
        method: 'post',
        data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
        dataType: 'json',
        success: function(response){
            html ='';
            html =html + ' <option value="">Select cities</option>';

            $.each(response, function( index, value ) {
            if(cities_id==value.id)
            {
                html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
            }
            else{
                html =html + '<option value="'+value.id+'">'+value.name+'</option>';
            }
            });
            $('#edit_entretien_cities_id').html(html);
        }
    });
}
   function edit_entretien_get_region_base_country(region_id=null)
   { 
     country_id=$('#edit_entretien_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
     html ='';
     html =html + '<option value="">Select Region</option>';
     
     $.each(response, function( index, value ) {
     if(region_id==value.id)
     {
       html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';  
     }
     else{
       html =html + '<option value="'+value.id+'">'+value.name+'</option>';
     }
     });
     $('#edit_entretien_region_id').html(html);
     }   
     }); 
   }
//For Edit Page

//create form submit
$(document).on('submit',"form#create_entretien_form",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
          //alert(data.record);
         setupDivEntretien();
         $('.ListEntretien').empty();
         $('.ListEntretien').html(data.record);
         createinnerconfigtable('.configInnerEntretienTable');
         entretienEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivEntretien();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListEntretien").show();
        }
     document.getElementById("create_entretien_form").reset();
      }
    });
    return false;
  });
//create form submit

//update form submit
$(document).on('submit',"form#update_entretien_form",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
     $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
       
         setupDivEntretien();
         $('.ListEntretien').empty();
         $('.ListEntretien').html(data.record);
         createinnerconfigtable('.configInnerEntretienTable');
         entretienEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
          
          setupDivEntretien();
          var msg=data.message;
          var msgType=data.messageType;
          var className=data.className;
          var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
           if ($("#inner_alert_div").hasClass("alert-success")) {
            $('#inner_alert_div').removeClass("alert-success");
          }
          if ($("#inner_alert_div").hasClass("alert-danger")) {
            $('#inner_alert_div').removeClass("alert-danger");
          }
          $('#inner_alert_div').addClass(className);
          $('#inner_message_div').empty();
          $('#inner_message_div').html(htmlContent);
          $('#inner_alert_div').show();
          $(".ListEntretien").show();
        }
     document.getElementById("update_entretien_form").reset();
      }
    });
    return false;
  });
//update form submit

//delete form submit
$(document).on('submit',"form#deleteentretienform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivEntretien();
         $('.ListEntretien').empty();
         $('.ListEntretien').html(data.record);
         createinnerconfigtable('.configInnerEntretienTable');
         entretienEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivEntretien();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListEntretien").show();
        }
     document.getElementById("addentretienform").reset();
      }
    });
    return false;
  });
//delete form submit
</script>
