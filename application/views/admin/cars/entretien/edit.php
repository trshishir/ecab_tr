            <?php
               $start_date = strtotime($entretien->start_date);
               $start_date = date('d/m/Y',$start_date);
               $start_date = $start_date;
               
               $end_date = strtotime($entretien->end_date);
               $end_date = date('d/m/Y',$end_date);
               $end_date = $end_date;

           ?>



              <?php
               $attributes = array("name" => 'update_entretien_form', "id" => 'update_entretien_form'); 
               echo form_open('admin/cars/entretienEdit',$attributes); ?>
                    <input type="hidden" value="<?= $entretien->id ?>" name="entretien_id">
                      <!-- Row 1 -->
                      <div class="col-md-12 pdhz" style="margin-top:10px;" >
                              <div class="col-md-3">
                                  <div class="form-group">
                                      <div class="col-md-4" style="margin-top: 5px;">
                                          <span style="font-weight: normal;">Statut : </span>
                                      </div>
                                      <div class="col-md-8" style="padding: 0px;">
                                         <select class="form-control" name="statut">
                                             <option value="">Select</option>
                                             <option <?php if("1" == $entretien->statut){ echo "selected"; } ?> value="1">Show</option>
                                             <option <?php if("0" == $entretien->statut){ echo "selected"; } ?> value="0">Hide</option> 
                                         </select>
                                      </div>
                                  </div>
                              </div>

                              <div class="col-md-3">
                                 <div class="form-group">
                                     <div class="col-md-4" style="margin-top: 5px;">
                                         <span style="font-weight: normal;">Entrerien : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                        <select class="form-control" name="entretien">
                                            <option value="">Select</option>
                                            <option <?php if("xyz" == $entretien->entretien){ echo "selected"; } ?> value="xyz">xyz</option>
                                        </select>
                                     </div>
                                 </div>
                              </div>

                             <div class="col-md-3">
                               <div class="form-group">
                                   <div class="col-md-4" style="margin-top: 5px;">
                                       <span style="font-weight: normal;">Price : </span>
                                   </div>
                                   <div class="col-md-8" style="padding: 0px;">
                                      <input type="text" name="price"  class="form-control" value="<?= $entretien->price ?>">
                                   </div>
                               </div>
                            </div>
                            <div class="col-md-3">
                               <div class="form-group">
                                   <div class="col-md-4" style="margin-top: 5px;">
                                       <span style="font-weight: normal;">TVA % : </span>
                                   </div>
                                   <div class="col-md-8" style="padding: 0px;">
                                      <select class="form-control" name="tva">
                                          <option value="">Select</option>
                                          <option  <?php if("10" == $entretien->tva){ echo "selected"; } ?> value="10">10</option>
                                          <option  <?php if("20" == $entretien->tva){ echo "selected"; } ?> value="20">20</option>
                                          <option  <?php if("30" == $entretien->tva){ echo "selected"; } ?> value="30">30</option>
                                          <option  <?php if("40" == $entretien->tva){ echo "selected"; } ?> value="40">40</option>
                                      </select>
                                   </div>
                               </div>
                            </div>
                        </div>
                        <!-- Row 2 -->
                        <div class="col-md-12 pdhz" style="margin-top: 10px;">

                           <div class="col-md-3">
                                  <div class="form-group">
                                      <div class="col-md-4" style="margin-top: 5px;">
                                          <span style="font-weight: normal;">Start Date : </span>
                                      </div>
                                      <div class="col-md-8" style="padding: 0px;">
                                           <input type="text" name="start_date" id="edit_entre_start_date" class="form-control datepicker" value="<?= $start_date ?>">
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-3">
                                  <div class="form-group">
                                      <div class="col-md-4" style="margin-top: 5px;">
                                          <span style="font-weight: normal;">Kilometer age : </span>
                                      </div>
                                      <div class="col-md-8" style="padding: 0px;">
                                         <input type="text" name="start_age" id="edit_entre_start_age" class="form-control" value="<?= $entretien->start_age  ?>">
                                      </div>
                                  </div>
                              </div>

                              <div class="col-md-3">
                                     <div class="form-group">
                                         <div class="col-md-4" style="margin-top: 5px;">
                                             <span style="font-weight: normal;">End Date : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;">
                                              <input type="text" name="end_date" id="edit_entre_end_date" class="form-control datepicker" value="<?= $end_date ?>">
                                         </div>
                                     </div>
                                 </div>
                                 <div class="col-md-3">
                                     <div class="form-group">
                                         <div class="col-md-4" style="margin-top: 5px;">
                                             <span style="font-weight: normal;">Kilometer age : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;">
                                            <input type="text" name="end_age" id="edit_entre_end_age" class="form-control" value="<?= $entretien->end_age  ?>">
                                         </div>
                                     </div>
                                 </div>                     
                        </div>
                   
                        
                        
                          <div class="col-md-12" style="padding: 30px;">                       
                              <!-- icons -->
                               <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                              <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelEntretien()"><span class="fa fa-close"> Cancel </span></button>
                              <!-- icons -->  
                          </div>

                     <?php echo form_close(); ?>
 