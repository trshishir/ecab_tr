
              <?php
               $attributes = array("name" => 'update_cout_form', "id" => 'update_cout_form'); 
               echo form_open('admin/cars/coutEdit',$attributes); ?>
                    <input type="hidden" value="<?= $cout->id ?>" name="cout_id">
                     <!-- Row 1 -->
                       <div class="col-md-12 pdhz" style="margin-top:10px;" >
                               <div class="col-md-3">
                                   <div class="form-group">
                                       <div class="col-md-4" style="margin-top: 5px;">
                                           <span style="font-weight: normal;">Statut : </span>
                                       </div>
                                       <div class="col-md-8" style="padding: 0px;">
                                          <select class="form-control" name="statut">
                                              <option value="">Select</option>
                                              <option <?php if("1" == $cout->statut){ echo "selected"; } ?> value="1">Show</option>
                                              <option <?php if("0" == $cout->statut){ echo "selected"; } ?> value="0">Hide</option> 
                                          </select>
                                       </div>
                                   </div>
                               </div>

                               <div class="col-md-3">
                                  <div class="form-group">
                                      <div class="col-md-4" style="margin-top: 5px;">
                                          <span style="font-weight: normal;">Cost : </span>
                                      </div>
                                      <div class="col-md-8" style="padding: 0px;">
                                         <input type="text" name="cout"  class="form-control" value="<?= $cout->cout ?>">
                                      </div>
                                  </div>
                               </div>

                              <div class="col-md-3">
                            
                             </div>
                             <div class="col-md-3">
                            
                             </div>
                         </div>
                        
                          <div class="col-md-12" style="padding: 30px;">                       
                              <!-- icons -->
                               <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                              <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelCout()"><span class="fa fa-close"> Cancel </span></button>
                              <!-- icons -->  
                          </div>

                     <?php echo form_close(); ?>
 