<script type="text/javascript">
  $(document).ready(function() {

           $(document).on('change','#cout_country_id', function() {
                      cout_get_region_base_country(this);
                  });
           $(document).on('change','#cout_region_id', function() {
                      cout_get_cities_base_country_region(this);
                  });
           $(document).on('change','#edit_cout_country_id', function() {
                      edit_cout_get_region_base_country(this);
                  });
           $(document).on('change','#edit_cout_region_id', function() {
                      edit_cout_get_cities_base_country_region(this);
                  });

            $(document).on('change','input.chk-maincout-template', function() {
                      $('input.chk-maincout-template').not(this).prop('checked', false);
                      var id = $(this).attr('data-input');
                      $('.chk-Addcout-btn').val(id);
                      $('#coutdeletid').val(id);
                  });        

  });
function coutAdd()
{
  setupDivCout();
  $(".coutAdd").show();
}

function cancelCout()
{
  setupDivCout();
  $(".ListCout").show();
}

function coutEdit()
{
    var val = $('.chk-Addcout-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivCout();
        $(".coutEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/cars/coutAjax'; ?>',
                data: {'cout_id': val},
                success: function (result) {
                  document.getElementsByClassName("coutEditajax")[0].innerHTML = result;
                
                }
            });
}
function coutidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivCout();
      $(".coutEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/cars/coutAjax'; ?>',
              data: {'cout_id': val},
              success: function (result) {
                document.getElementsByClassName("coutEditajax")[0].innerHTML = result;
               
              }
          });
}
function coutDelete()
{
  var val = $('.chk-Addcout-btn').val();
  if(val != "")
  {
  setupDivCout();
  $(".coutDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivCout()
{
  $(".coutAdd").hide();
  $(".coutEdit").hide();
  $(".ListCout").hide();
  $(".coutDelete").hide();

}
function setupDivCoutConfig()
{
  $(".coutAdd").hide();
  $(".coutEdit").hide();
  $(".ListCout").show();
  $(".coutDelete").hide();

}


  function cout_get_region_base_country(region_id=null)
   {
     country_id=$('#cout_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
         html ='';
         html =html + '<option value="">Select Region</option>';
         
         $.each(response, function( index, value ) {  
         if(region_id==value.id)
         {
           html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
         }
         else{
           html =html + '<option value="'+value.id+'">'+value.name+'</option>';
         }
         });
         $('#cout_region_id').html(html);
       }
   }); 
}

function cout_get_cities_base_country_region(region_id=null,cities_id=null)
{

region_id=$('#cout_region_id').val();

$.ajax({
    url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
    method: 'post',
    data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
    dataType: 'json',
    success: function(response){
    console.log(response);
    html ='';
    html =html + ' <option value="">Select cities</option>';

    $.each(response, function( index, value ) {
    // console.log( value );
                

    if(cities_id==value.id)
    {
        html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';

    }
    else{
        html =html + '<option value="'+value.id+'">'+value.name+'</option>';

    }
    });

    $('#cout_cities_id').html(html);


    }


});
}


//For Edit Page

function edit_cout_get_cities_base_country_region(region_id=null,cities_id=null)
{
    region_id=$('#edit_cout_region_id').val();
    $.ajax({
        url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
        method: 'post',
        data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
        dataType: 'json',
        success: function(response){
            html ='';
            html =html + ' <option value="">Select cities</option>';

            $.each(response, function( index, value ) {
            if(cities_id==value.id)
            {
                html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
            }
            else{
                html =html + '<option value="'+value.id+'">'+value.name+'</option>';
            }
            });
            $('#edit_cout_cities_id').html(html);
        }
    });
}
   function edit_cout_get_region_base_country(region_id=null)
   { 
     country_id=$('#edit_cout_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
     html ='';
     html =html + '<option value="">Select Region</option>';
     
     $.each(response, function( index, value ) {
     if(region_id==value.id)
     {
       html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';  
     }
     else{
       html =html + '<option value="'+value.id+'">'+value.name+'</option>';
     }
     });
     $('#edit_cout_region_id').html(html);
     }   
     }); 
   }
//For Edit Page

//create form submit
$(document).on('submit',"form#create_cout_form",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
          //alert(data.record);
         setupDivCout();
         $('.ListCout').empty();
         $('.ListCout').html(data.record);
         createinnerconfigtable('.configInnerCoutTable');
         coutEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivCout();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListCout").show();
        }
     document.getElementById("create_cout_form").reset();
      }
    });
    return false;
  });
//create form submit

//update form submit
$(document).on('submit',"form#update_cout_form",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
     $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
       
         setupDivCout();
         $('.ListCout').empty();
         $('.ListCout').html(data.record);
         createinnerconfigtable('.configInnerCoutTable');
         coutEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
          
          setupDivCout();
          var msg=data.message;
          var msgType=data.messageType;
          var className=data.className;
          var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
           if ($("#inner_alert_div").hasClass("alert-success")) {
            $('#inner_alert_div').removeClass("alert-success");
          }
          if ($("#inner_alert_div").hasClass("alert-danger")) {
            $('#inner_alert_div').removeClass("alert-danger");
          }
          $('#inner_alert_div').addClass(className);
          $('#inner_message_div').empty();
          $('#inner_message_div').html(htmlContent);
          $('#inner_alert_div').show();
          $(".ListCout").show();
        }
     document.getElementById("update_cout_form").reset();
      }
    });
    return false;
  });
//update form submit

//delete form submit
$(document).on('submit',"form#deletecoutform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivCout();
         $('.ListCout').empty();
         $('.ListCout').html(data.record);
         createinnerconfigtable('.configInnerCoutTable');
         coutEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivCout();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListCout").show();
        }
     document.getElementById("addcoutform").reset();
      }
    });
    return false;
  });
//delete form submit
</script>
