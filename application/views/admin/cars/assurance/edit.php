            <?php
               $start_date = strtotime($assurance->start_date);
               $start_date = date('d/m/Y',$start_date);
               $start_date = $start_date;
               
               $end_date = strtotime($assurance->end_date);
               $end_date = date('d/m/Y',$end_date);
               $end_date = $end_date;

           ?>



              <?php
               $attributes = array("name" => 'update_assurance_form', "id" => 'update_assurance_form'); 
               echo form_open('admin/cars/assuranceEdit',$attributes); ?>
                    <input type="hidden" value="<?= $assurance->id ?>" name="assurance_id">
                    <div class="col-md-12 pdhz" style="margin-top:10px;" >
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Statut : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                       <select class="form-control" name="statut">
                                           <option value="">Select</option>
                                           <option <?php if("1" == $assurance->statut){ echo "selected"; } ?> value="1">Show</option>
                                           <option <?php if("0" == $assurance->statut){ echo "selected"; } ?> value="0">Hide</option> 
                                       </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                  <div class="col-md-4" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Price : </span>
                                  </div>
                                  <div class="col-md-8" style="padding: 0px;">
                                  <input type="text" name="price"  class="form-control" value="<?= $assurance->price ?>">
                                  </div>
                              </div>
                            </div>

                           <div class="col-md-4">
                           <div class="form-group">
                               <div class="col-md-4" style="margin-top: 5px;">
                                   <span style="font-weight: normal;">TVA % : </span>
                               </div>
                               <div class="col-md-8" style="padding: 0px;">
                                  <select class="form-control" name="tva">
                                      <option  value="">Select</option>
                                      <option <?php if("10" == $assurance->tva){ echo "selected"; } ?> value="10">10</option>
                                      <option <?php if("20" == $assurance->tva){ echo "selected"; } ?> value="20">20</option>
                                      <option <?php if("30" == $assurance->tva){ echo "selected"; } ?> value="30">30</option>
                                      <option <?php if("40" == $assurance->tva){ echo "selected"; } ?> value="40">40</option> 
                                  </select>
                               </div>
                           </div>
                          </div>


                      </div>
                      <!-- Row 2 -->
                      <div class="col-md-12 pdhz" style="margin-top: 10px;">

                        <div class="col-md-4">
                         <div class="form-group">
                             <div class="col-md-4" style="margin-top: 5px;">
                                 <span style="font-weight: normal;">Contract Number : </span>
                             </div>
                             <div class="col-md-8" style="padding: 0px;">
                                  <input type="text" name="contract_number"  class="form-control" value="<?= $assurance->contract_number ?>">
                             </div>
                         </div>
                        </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                  <div class="col-md-4" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Insurance Company : </span>
                                  </div>
                                  <div class="col-md-8" style="padding: 0px;">
                                       <input type="text" name="insurance_company"  class="form-control" value="<?= $assurance->insurance_company ?>">
                                  </div>
                              </div>
                            </div>

                           <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 5px;">
                                    <span style="font-weight: normal;">Brie Glasse : </span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                   <select class="form-control" name="brie_glasse">
                                       <option value="">Select</option>
                                       <option <?php if("test" == $assurance->brie_glasse){ echo "selected"; } ?> value="test">Test</option> 
                                   </select>
                                </div>
                            </div>
                           </div>
                      </div>

                      <div class="col-md-12 pdhz" style="margin-top:10px;">
                        <div class="col-md-4">
                          <div class="form-group">
                              <div class="col-md-4" style="margin-top: 5px;">
                                  <span style="font-weight: normal;">Franchise : </span>
                              </div>
                              <div class="col-md-8" style="padding: 0px;">
                                   <input type="text" name="franchise"  class="form-control" value="<?= $assurance->franchise ?>">
                              </div>
                          </div>
                        </div>
                           <div class="col-md-4">
                              <div class="form-group">
                                  <div class="col-md-4" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Start Date : </span>
                                  </div>
                                  <div class="col-md-8" style="padding: 0px;">
                                       <input type="text" name="start_date" id="assurance_start_date" class="form-control datepicker" value="<?= $start_date ?>">
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-4">
                                 <div class="form-group">
                                     <div class="col-md-4" style="margin-top: 5px;">
                                         <span style="font-weight: normal;">End Date : </span>
                                     </div>
                                     <div class="col-md-8" style="padding: 0px;">
                                          <input type="text" name="end_date" id="assurance_end_date" class="form-control datepicker" value="<?= $end_date ?>">
                                     </div>
                                 </div>
                          </div>
                      </div>
                        
                          <div class="col-md-12" style="padding: 30px;">                       
                              <!-- icons -->
                               <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                              <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelAssurance()"><span class="fa fa-close"> Cancel </span></button>
                              <!-- icons -->  
                          </div>

                     <?php echo form_close(); ?>
 