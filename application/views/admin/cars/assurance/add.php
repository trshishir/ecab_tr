             <?php
              $attributes = array("name" => 'create_assurance_form', "id" => 'create_assurance_form'); 
              echo form_open('admin/cars/assuranceAdd',$attributes); ?>
                <input type="hidden" value="<?= $car->id ?>" name="profile_id">
                      <!-- Row 1 -->
                        <div class="col-md-12 pdhz" style="margin-top:10px;" >
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Statut : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                           <select class="form-control" name="statut">
                                               <option value="">Select</option>
                                               <option value="1">Show</option>
                                               <option value="0">Hide</option> 
                                           </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                  <div class="form-group">
                                      <div class="col-md-4" style="margin-top: 5px;">
                                          <span style="font-weight: normal;">Price : </span>
                                      </div>
                                      <div class="col-md-8" style="padding: 0px;">
                                           <input type="text" name="price"  class="form-control" >
                                      </div>
                                  </div>
                                </div>

                               <div class="col-md-4">
                               <div class="form-group">
                                   <div class="col-md-4" style="margin-top: 5px;">
                                       <span style="font-weight: normal;">TVA % : </span>
                                   </div>
                                   <div class="col-md-8" style="padding: 0px;">
                                      <select class="form-control" name="tva">
                                          <option value="">Select</option>
                                          <option value="10">10</option>
                                          <option value="20">20</option>
                                          <option value="30">30</option>
                                          <option value="40">50</option> 
                                      </select>
                                   </div>
                               </div>
                              </div>


                          </div>
                          <!-- Row 2 -->
                          <div class="col-md-12 pdhz" style="margin-top: 10px;">

                            <div class="col-md-4">
                             <div class="form-group">
                                 <div class="col-md-4" style="margin-top: 5px;">
                                     <span style="font-weight: normal;">Contract Number : </span>
                                 </div>
                                 <div class="col-md-8" style="padding: 0px;">
                                      <input type="text" name="contract_number"  class="form-control" >
                                 </div>
                             </div>
                            </div>

                                <div class="col-md-4">
                                  <div class="form-group">
                                      <div class="col-md-4" style="margin-top: 5px;">
                                          <span style="font-weight: normal;">Insurance Company : </span>
                                      </div>
                                      <div class="col-md-8" style="padding: 0px;">
                                           <input type="text" name="insurance_company"  class="form-control" >
                                      </div>
                                  </div>
                                </div>

                               <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-top: 5px;">
                                        <span style="font-weight: normal;">Brie Glasse : </span>
                                    </div>
                                    <div class="col-md-8" style="padding: 0px;">
                                       <select class="form-control" name="brie_glasse">
                                           <option value="">Select</option>
                                           <option value="test">Test</option> 
                                       </select>
                                    </div>
                                </div>
                               </div>
                          </div>

                          <div class="col-md-12 pdhz" style="margin-top:10px;">
                            <div class="col-md-4">
                              <div class="form-group">
                                  <div class="col-md-4" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Franchise : </span>
                                  </div>
                                  <div class="col-md-8" style="padding: 0px;">
                                       <input type="text" name="franchise"  class="form-control" >
                                  </div>
                              </div>
                            </div>
                               <div class="col-md-4">
                                  <div class="form-group">
                                      <div class="col-md-4" style="margin-top: 5px;">
                                          <span style="font-weight: normal;">Start Date : </span>
                                      </div>
                                      <div class="col-md-8" style="padding: 0px;">
                                           <input type="text" name="start_date" id="assurance_start_date" class="form-control datepicker" value="<?php echo date('d/m/Y');?>">
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-4">
                                     <div class="form-group">
                                         <div class="col-md-4" style="margin-top: 5px;">
                                             <span style="font-weight: normal;">End Date : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;">
                                              <input type="text" name="end_date" id="assurance_end_date" class="form-control datepicker" value="<?php echo date('d/m/Y');?>">
                                         </div>
                                     </div>
                              </div>
                          </div>
                       
                 
                          <div class="col-md-12" style="padding: 30px;">                       
                              <!-- icons -->
                               <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                              <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelAssurance()"><span class="fa fa-close"> Cancel </span></button>
                              <!-- icons -->  
                          </div>

                     <?php echo form_close(); ?>
 