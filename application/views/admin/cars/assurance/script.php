<script type="text/javascript">
  $(document).ready(function() {

           $(document).on('change','#assurance_country_id', function() {
                      assurance_get_region_base_country(this);
                  });
           $(document).on('change','#assurance_region_id', function() {
                      assurance_get_cities_base_country_region(this);
                  });
           $(document).on('change','#edit_assurance_country_id', function() {
                      edit_assurance_get_region_base_country(this);
                  });
           $(document).on('change','#edit_assurance_region_id', function() {
                      edit_assurance_get_cities_base_country_region(this);
                  });

            $(document).on('change','input.chk-mainassurance-template', function() {
                      $('input.chk-mainassurance-template').not(this).prop('checked', false);
                      var id = $(this).attr('data-input');
                      $('.chk-Addassurance-btn').val(id);
                      $('#assurancedeletid').val(id);
                  });        

  });
function assuranceAdd()
{
  setupDivAssurance();
  $(".assuranceAdd").show();
}

function cancelAssurance()
{
  setupDivAssurance();
  $(".ListAssurance").show();
}

function assuranceEdit()
{
    var val = $('.chk-Addassurance-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivAssurance();
        $(".assuranceEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/cars/assuranceAjax'; ?>',
                data: {'assurance_id': val},
                success: function (result) {
                  document.getElementsByClassName("assuranceEditajax")[0].innerHTML = result;
                
                }
            });
}
function assuranceidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivAssurance();
      $(".assuranceEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/cars/assuranceAjax'; ?>',
              data: {'assurance_id': val},
              success: function (result) {
                document.getElementsByClassName("assuranceEditajax")[0].innerHTML = result;
               
              }
          });
}
function assuranceDelete()
{
  var val = $('.chk-Addassurance-btn').val();
  if(val != "")
  {
  setupDivAssurance();
  $(".assuranceDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivAssurance()
{
  $(".assuranceAdd").hide();
  $(".assuranceEdit").hide();
  $(".ListAssurance").hide();
  $(".assuranceDelete").hide();

}
function setupDivAssuranceConfig()
{
  $(".assuranceAdd").hide();
  $(".assuranceEdit").hide();
  $(".ListAssurance").show();
  $(".assuranceDelete").hide();

}


  function assurance_get_region_base_country(region_id=null)
   {
     country_id=$('#assurance_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
         html ='';
         html =html + '<option value="">Select Region</option>';
         
         $.each(response, function( index, value ) {  
         if(region_id==value.id)
         {
           html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
         }
         else{
           html =html + '<option value="'+value.id+'">'+value.name+'</option>';
         }
         });
         $('#assurance_region_id').html(html);
       }
   }); 
}

function assurance_get_cities_base_country_region(region_id=null,cities_id=null)
{

region_id=$('#assurance_region_id').val();

$.ajax({
    url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
    method: 'post',
    data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
    dataType: 'json',
    success: function(response){
    console.log(response);
    html ='';
    html =html + ' <option value="">Select cities</option>';

    $.each(response, function( index, value ) {
    // console.log( value );
                

    if(cities_id==value.id)
    {
        html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';

    }
    else{
        html =html + '<option value="'+value.id+'">'+value.name+'</option>';

    }
    });

    $('#assurance_cities_id').html(html);


    }


});
}


//For Edit Page

function edit_assurance_get_cities_base_country_region(region_id=null,cities_id=null)
{
    region_id=$('#edit_assurance_region_id').val();
    $.ajax({
        url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
        method: 'post',
        data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
        dataType: 'json',
        success: function(response){
            html ='';
            html =html + ' <option value="">Select cities</option>';

            $.each(response, function( index, value ) {
            if(cities_id==value.id)
            {
                html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
            }
            else{
                html =html + '<option value="'+value.id+'">'+value.name+'</option>';
            }
            });
            $('#edit_assurance_cities_id').html(html);
        }
    });
}
   function edit_assurance_get_region_base_country(region_id=null)
   { 
     country_id=$('#edit_assurance_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
     html ='';
     html =html + '<option value="">Select Region</option>';
     
     $.each(response, function( index, value ) {
     if(region_id==value.id)
     {
       html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';  
     }
     else{
       html =html + '<option value="'+value.id+'">'+value.name+'</option>';
     }
     });
     $('#edit_assurance_region_id').html(html);
     }   
     }); 
   }
//For Edit Page

//create form submit
$(document).on('submit',"form#create_assurance_form",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
          //alert(data.record);
         setupDivAssurance();
         $('.ListAssurance').empty();
         $('.ListAssurance').html(data.record);
         createinnerconfigtable('.configInnerAssuranceTable');
         assuranceEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivAssurance();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListAssurance").show();
        }
     document.getElementById("create_assurance_form").reset();
      }
    });
    return false;
  });
//create form submit

//update form submit
$(document).on('submit',"form#update_assurance_form",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
     $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
          
         setupDivAssurance();
         $('.ListAssurance').empty();
         $('.ListAssurance').html(data.record);
         createinnerconfigtable('.configInnerAssuranceTable');
         assuranceEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
           
          setupDivAssurance();
          var msg=data.message;
          var msgType=data.messageType;
          var className=data.className;
          var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
           if ($("#inner_alert_div").hasClass("alert-success")) {
            $('#inner_alert_div').removeClass("alert-success");
          }
          if ($("#inner_alert_div").hasClass("alert-danger")) {
            $('#inner_alert_div').removeClass("alert-danger");
          }
          $('#inner_alert_div').addClass(className);
          $('#inner_message_div').empty();
          $('#inner_message_div').html(htmlContent);
          $('#inner_alert_div').show();
          $(".ListAssurance").show();
        }
     document.getElementById("update_assurance_form").reset();
      }
    });
    return false;
  });
//update form submit

//delete form submit
$(document).on('submit',"form#deleteassuranceform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){

         setupDivAssurance();
         $('.ListAssurance').empty();
         $('.ListAssurance').html(data.record);
         createinnerconfigtable('.configInnerAssuranceTable');
         assuranceEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivAssurance();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListAssurance").show();
        }
     document.getElementById("addassuranceform").reset();
      }
    });
    return false;
  });
//delete form submit
</script>
