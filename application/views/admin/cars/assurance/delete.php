
  <?php
  $attributes = array("name" => 'delete_assurance_form', "id" => 'deleteassuranceform');   
  echo form_open_multipart('admin/cars/assuranceDelete',$attributes); ?>
    <input  name="tablename" value="vbs_clientassurance" type="hidden" >
    <input type="hidden" id="assurancedeletid" name="delete_assurance_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <button type="submit" class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>
    <a class="btn btn-default" style="cursor: pointer;" onclick="cancelAssurance()"><span class="delete-icon">No</span></a>
  <?php echo form_close(); ?>
