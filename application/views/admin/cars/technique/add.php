             <?php
              $attributes = array("name" => 'create_technique_form', "id" => 'create_technique_form'); 
              echo form_open('admin/cars/techniqueAdd',$attributes); ?>
                <input type="hidden" value="<?= $car->id ?>" name="profile_id">
                      <!-- Row 1 -->
                        <div class="col-md-12 pdhz" style="margin-top:10px;" >
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Statut : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                           <select class="form-control" name="statut">
                                               <option value="">Select</option>
                                               <option value="1">Show</option>
                                               <option value="0">Hide</option> 
                                           </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                   
                                </div>

                               <div class="col-md-3">
                             
                              </div>
                              <div class="col-md-3">
                             
                              </div>
                          </div>
                          <!-- Row 2 -->
                          <div class="col-md-12 pdhz" style="margin-top: 10px;">
                             <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Start Date : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                             <input type="text" name="start_date" id="tech_start_date" class="form-control datepicker" value="<?php echo date('d/m/Y');?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Kilometer age : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                           <input type="text" name="start_age" id="tech_start_age" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                       <div class="form-group">
                                           <div class="col-md-4" style="margin-top: 5px;">
                                               <span style="font-weight: normal;">End Date : </span>
                                           </div>
                                           <div class="col-md-8" style="padding: 0px;">
                                                <input type="text" name="end_date" id="tech_end_date" class="form-control datepicker" value="<?php echo date('d/m/Y');?>">
                                           </div>
                                       </div>
                                   </div>
                                   <div class="col-md-3">
                                       <div class="form-group">
                                           <div class="col-md-4" style="margin-top: 5px;">
                                               <span style="font-weight: normal;">Kilometer age : </span>
                                           </div>
                                           <div class="col-md-8" style="padding: 0px;">
                                              <input type="text" name="end_age" id="tech_end_age" class="form-control">
                                           </div>
                                       </div>
                                   </div>
                      
                          </div>

                          <div class="col-md-12 pdhz" style="margin-top:10px;">
                            <div class="col-md-3">
                              <div class="form-group">
                                  <div class="col-md-4" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Note : </span>
                                  </div>
                                  <div class="col-md-8" style="padding: 0px;">
                                     <textarea rows="3" class="form-control" placeholder="write a note"  name="note" style="border-radius: 0px;"></textarea>
                                  </div>
                              </div>
                             
                            </div>
                          </div>
                       
                        
 
                      
                   
                        
                        
                          <div class="col-md-12" style="padding: 30px;">                       
                              <!-- icons -->
                               <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                              <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelTechnique()"><span class="fa fa-close"> Cancel </span></button>
                              <!-- icons -->  
                          </div>

                     <?php echo form_close(); ?>
 