            <?php
               $start_date = strtotime($technique->start_date);
               $start_date = date('d/m/Y',$start_date);
               $start_date = $start_date;
               
               $end_date = strtotime($technique->end_date);
               $end_date = date('d/m/Y',$end_date);
               $end_date = $end_date;

           ?>



              <?php
               $attributes = array("name" => 'update_technique_form', "id" => 'update_technique_form'); 
               echo form_open('admin/cars/techniqueEdit',$attributes); ?>
                    <input type="hidden" value="<?= $technique->id ?>" name="technique_id">
                      <!-- Row 1 -->
                        <div class="col-md-12 pdhz" style="margin-top:10px;" >
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Statut : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                           <select class="form-control" name="statut" required>
                                               <option value="">Select</option>
                                               <option <?php if("1" == $technique->statut){ echo "selected"; } ?> value="1">Show</option>
                                               <option <?php if("0" == $technique->statut){ echo "selected"; } ?> value="0">Hide</option> 
                                           </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                   
                                </div>

                               <div class="col-md-3">
                             
                              </div>
                              <div class="col-md-3">
                             
                              </div>
                          </div>
                          <!-- Row 2 -->
                          <div class="col-md-12 pdhz" style="margin-top: 10px;">
                             <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Start Date : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                             <input type="text" name="start_date" id="edit_tech_start_date" class="form-control datepicker" value="<?= $start_date ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Kilometer age : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                           <input type="text" name="start_age" id="edit_tech_start_age" class="form-control" value="<?= $technique->start_age ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                       <div class="form-group">
                                           <div class="col-md-4" style="margin-top: 5px;">
                                               <span style="font-weight: normal;">End Date : </span>
                                           </div>
                                           <div class="col-md-8" style="padding: 0px;">
                                                <input type="text" name="end_date" id="edit_tech_end_date" class="form-control datepicker" value="<?= $end_date ?>">
                                           </div>
                                       </div>
                                   </div>
                                   <div class="col-md-3">
                                       <div class="form-group">
                                           <div class="col-md-4" style="margin-top: 5px;">
                                               <span style="font-weight: normal;">Kilometer age : </span>
                                           </div>
                                           <div class="col-md-8" style="padding: 0px;">
                                              <input type="text" name="end_age" id="edit_tech_end_age" class="form-control" value="<?= $technique->end_age ?>">
                                           </div>
                                       </div>
                                   </div>
                      
                          </div>

                          <div class="col-md-12 pdhz" style="margin-top:10px;">
                            <div class="col-md-3">
                              <div class="form-group">
                                  <div class="col-md-4" style="margin-top: 5px;">
                                      <span style="font-weight: normal;">Note : </span>
                                  </div>
                                  <div class="col-md-8" style="padding: 0px;">
                                     <textarea rows="3" class="form-control" placeholder="write a note"  name="note" style="border-radius: 0px;"><?= $technique->note ?></textarea>
                                  </div>
                              </div>
                             
                            </div>
                          </div>
                       
                        
 
                      
                   
                        
                        
                          <div class="col-md-12" style="padding: 30px;">                       
                              <!-- icons -->
                               <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                              <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelTechnique()"><span class="fa fa-close"> Cancel </span></button>
                              <!-- icons -->  
                          </div>

                     <?php echo form_close(); ?>
 