<script type="text/javascript">
  $(document).ready(function() {

           $(document).on('change','#technique_country_id', function() {
                      technique_get_region_base_country(this);
                  });
           $(document).on('change','#technique_region_id', function() {
                      technique_get_cities_base_country_region(this);
                  });
           $(document).on('change','#edit_technique_country_id', function() {
                      edit_technique_get_region_base_country(this);
                  });
           $(document).on('change','#edit_technique_region_id', function() {
                      edit_technique_get_cities_base_country_region(this);
                  });

            $(document).on('change','input.chk-maintechnique-template', function() {
                      $('input.chk-maintechnique-template').not(this).prop('checked', false);
                      var id = $(this).attr('data-input');
                      $('.chk-Addtechnique-btn').val(id);
                      $('#techniquedeletid').val(id);
                  });        

  });
function techniqueAdd()
{
  setupDivTechnique();
  $(".techniqueAdd").show();
}

function cancelTechnique()
{
  setupDivTechnique();
  $(".ListTechnique").show();
}

function techniqueEdit()
{
    var val = $('.chk-Addtechnique-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivTechnique();
        $(".techniqueEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/cars/techniqueAjax'; ?>',
                data: {'technique_id': val},
                success: function (result) {
                  document.getElementsByClassName("techniqueEditajax")[0].innerHTML = result;
                
                }
            });
}
function techniqueidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivTechnique();
      $(".techniqueEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/cars/techniqueAjax'; ?>',
              data: {'technique_id': val},
              success: function (result) {
                document.getElementsByClassName("techniqueEditajax")[0].innerHTML = result;
               
              }
          });
}
function techniqueDelete()
{
  var val = $('.chk-Addtechnique-btn').val();
  if(val != "")
  {
  setupDivTechnique();
  $(".techniqueDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivTechnique()
{
  $(".techniqueAdd").hide();
  $(".techniqueEdit").hide();
  $(".ListTechnique").hide();
  $(".techniqueDelete").hide();

}
function setupDivTechniqueConfig()
{
  $(".techniqueAdd").hide();
  $(".techniqueEdit").hide();
  $(".ListTechnique").show();
  $(".techniqueDelete").hide();

}


  function technique_get_region_base_country(region_id=null)
   {
     country_id=$('#technique_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
         html ='';
         html =html + '<option value="">Select Region</option>';
         
         $.each(response, function( index, value ) {  
         if(region_id==value.id)
         {
           html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
         }
         else{
           html =html + '<option value="'+value.id+'">'+value.name+'</option>';
         }
         });
         $('#technique_region_id').html(html);
       }
   }); 
}

function technique_get_cities_base_country_region(region_id=null,cities_id=null)
{

region_id=$('#technique_region_id').val();

$.ajax({
    url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
    method: 'post',
    data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
    dataType: 'json',
    success: function(response){
    console.log(response);
    html ='';
    html =html + ' <option value="">Select cities</option>';

    $.each(response, function( index, value ) {
    // console.log( value );
                

    if(cities_id==value.id)
    {
        html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';

    }
    else{
        html =html + '<option value="'+value.id+'">'+value.name+'</option>';

    }
    });

    $('#technique_cities_id').html(html);


    }


});
}


//For Edit Page

function edit_technique_get_cities_base_country_region(region_id=null,cities_id=null)
{
    region_id=$('#edit_technique_region_id').val();
    $.ajax({
        url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
        method: 'post',
        data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
        dataType: 'json',
        success: function(response){
            html ='';
            html =html + ' <option value="">Select cities</option>';

            $.each(response, function( index, value ) {
            if(cities_id==value.id)
            {
                html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
            }
            else{
                html =html + '<option value="'+value.id+'">'+value.name+'</option>';
            }
            });
            $('#edit_technique_cities_id').html(html);
        }
    });
}
   function edit_technique_get_region_base_country(region_id=null)
   { 
     country_id=$('#edit_technique_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
     html ='';
     html =html + '<option value="">Select Region</option>';
     
     $.each(response, function( index, value ) {
     if(region_id==value.id)
     {
       html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';  
     }
     else{
       html =html + '<option value="'+value.id+'">'+value.name+'</option>';
     }
     });
     $('#edit_technique_region_id').html(html);
     }   
     }); 
   }
//For Edit Page

//create form submit
$(document).on('submit',"form#create_technique_form",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
          //alert(data.record);
         setupDivTechnique();
         $('.ListTechnique').empty();
         $('.ListTechnique').html(data.record);
         createinnerconfigtable('.configInnerTechniqueTable');
         techniqueEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivTechnique();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListTechnique").show();
        }
     document.getElementById("create_technique_form").reset();
      }
    });
    return false;
  });
//create form submit

//update form submit
$(document).on('submit',"form#update_technique_form",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
     $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
       
         setupDivTechnique();
         $('.ListTechnique').empty();
         $('.ListTechnique').html(data.record);
         createinnerconfigtable('.configInnerTechniqueTable');
         techniqueEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
          
          setupDivTechnique();
          var msg=data.message;
          var msgType=data.messageType;
          var className=data.className;
          var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
           if ($("#inner_alert_div").hasClass("alert-success")) {
            $('#inner_alert_div').removeClass("alert-success");
          }
          if ($("#inner_alert_div").hasClass("alert-danger")) {
            $('#inner_alert_div').removeClass("alert-danger");
          }
          $('#inner_alert_div').addClass(className);
          $('#inner_message_div').empty();
          $('#inner_message_div').html(htmlContent);
          $('#inner_alert_div').show();
          $(".ListTechnique").show();
        }
     document.getElementById("update_technique_form").reset();
      }
    });
    return false;
  });
//update form submit

//delete form submit
$(document).on('submit',"form#deletetechniqueform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivTechnique();
         $('.ListTechnique').empty();
         $('.ListTechnique').html(data.record);
         createinnerconfigtable('.configInnerTechniqueTable');
         techniqueEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivTechnique();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListTechnique").show();
        }
     document.getElementById("addtechniqueform").reset();
      }
    });
    return false;
  });
//delete form submit
</script>
