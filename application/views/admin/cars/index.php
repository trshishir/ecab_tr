<?php $locale_info = localeconv(); ?>

<section id="content">
    <div class="listDiv"><!--col-md-10 padding white right-p-->
    
   <div style="padding: 5px 12px;display: none;" id="inner_alert_div" class="alert">
           <span id="inner_message_div"></span>    
            <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
    </div>
    

            <div class="row-fluid"  id="ShowTabs" style="margin-bottom: 10px; margin-left:0px;">

                                <ul class="nav nav-tabs responsive">
                                            <li class="active"><a href="#PROFILE" class="profile_E" role="tab" data-toggle="tab">PROFIL</a></li>
                                            <li><a href="#ASSURANCE" class="assurance_E" role="tab" data-toggle="tab">INSURANCE</a></li>
                                            <li><a href="#TECHNIQUE" class="technique_E" role="tab" data-toggle="tab">TECHNICAL CONTROLE </a></li>
                                            <li><a href="#ENTRETIEN" class="entretien_E" role="tab" data-toggle="tab">ENTRETIENS</a></li>
                                            <li><a href="#REPARATION" role="tab" class="reparation_E" data-toggle="tab">REPARATIONS</a></li>
                                            <li><a href="#SINISTRE" class="sinistre_E" role="tab" data-toggle="tab">SINISTRES</a></li>
                                            <li><a href="#COUT" class="cout_E" role="tab" data-toggle="tab">COST</a></li>
                                       
                                </ul>
                               
                <div class="tab-content responsive" style="width:100%;display: table;">
                        <!--status tab starts-->
                        <div class="tab-pane fade in active" id="PROFILE" style="border: 1px solid #75b0d7;">
                            <div class="row">
                            <?php include('profile/edit.php'); ?>
                           </div>
                        </div>

                        <div class="tab-pane fade" id="ASSURANCE" style="border: 1px solid #75b0d7;">
                        <?php include('assurance.php'); ?>
                        </div>
                        <div class="tab-pane fade" id="ENTRETIEN" style="border: 1px solid #75b0d7;">
                        <?php include('entretien.php'); ?>
                        </div>

                        <div class="tab-pane fade"  id="REPARATION" style="border: 1px solid #75b0d7;">
                        <?php include('reparation.php'); ?>
                        </div>
                        <div class="tab-pane fade" id="SINISTRE" style="border: 1px solid #75b0d7;">
                        <?php include('sinistre.php'); ?>
                        </div>
                        <div class="tab-pane fade" id="TECHNIQUE" style="border: 1px solid #75b0d7;">
                        <?php include('technique.php'); ?>
                        </div>
                        <div class="tab-pane fade" id="COUT" style="border: 1px solid #75b0d7;">
                        <?php include('cout.php'); ?>
                        </div>
                        <div class="tab-pane fade" id="PAYMENT" style="border: 1px solid #75b0d7;">
                        <?php include('payment.php'); ?>
                        </div>
                        <div class="tab-pane fade" id="SUPPORT" style="border: 1px solid #75b0d7;">
                        <?php include('support.php'); ?>
                        </div>
                         <div class="tab-pane fade" id="MESSAGE" style="border: 1px solid #75b0d7;">
                        <?php include('message.php'); ?>
                        </div>             
                    </div>
            <!--/.module-->
        </div>

    </div>
</section>
