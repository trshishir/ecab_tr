
  <?php
  $attributes = array("name" => 'delete_reparation_form', "id" => 'deletereparationform');   
  echo form_open_multipart('admin/cars/reparationDelete',$attributes); ?>
    <input  name="tablename" value="vbs_clientreparation" type="hidden" >
    <input type="hidden" id="reparationdeletid" name="delete_reparation_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <button type="submit" class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>
    <a class="btn btn-default" style="cursor: pointer;" onclick="cancelReparation()"><span class="delete-icon">No</span></a>
  <?php echo form_close(); ?>
