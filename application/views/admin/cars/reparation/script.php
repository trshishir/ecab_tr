<script type="text/javascript">
  $(document).ready(function() {

           $(document).on('change','#reparation_country_id', function() {
                      reparation_get_region_base_country(this);
                  });
           $(document).on('change','#reparation_region_id', function() {
                      reparation_get_cities_base_country_region(this);
                  });
           $(document).on('change','#edit_reparation_country_id', function() {
                      edit_reparation_get_region_base_country(this);
                  });
           $(document).on('change','#edit_reparation_region_id', function() {
                      edit_reparation_get_cities_base_country_region(this);
                  });

            $(document).on('change','input.chk-mainreparation-template', function() {
                      $('input.chk-mainreparation-template').not(this).prop('checked', false);
                      var id = $(this).attr('data-input');
                      $('.chk-Addreparation-btn').val(id);
                      $('#reparationdeletid').val(id);
                  });        

  });
function reparationAdd()
{
  setupDivReparation();
  $(".reparationAdd").show();
}

function cancelReparation()
{
  setupDivReparation();
  $(".ListReparation").show();
}

function reparationEdit()
{
    var val = $('.chk-Addreparation-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivReparation();
        $(".reparationEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/cars/reparationAjax'; ?>',
                data: {'reparation_id': val},
                success: function (result) {
                  document.getElementsByClassName("reparationEditajax")[0].innerHTML = result;
                
                }
            });
}
function reparationidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivReparation();
      $(".reparationEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/cars/reparationAjax'; ?>',
              data: {'reparation_id': val},
              success: function (result) {
                document.getElementsByClassName("reparationEditajax")[0].innerHTML = result;
               
              }
          });
}
function reparationDelete()
{
  var val = $('.chk-Addreparation-btn').val();
  if(val != "")
  {
  setupDivReparation();
  $(".reparationDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivReparation()
{
  $(".reparationAdd").hide();
  $(".reparationEdit").hide();
  $(".ListReparation").hide();
  $(".reparationDelete").hide();

}
function setupDivReparationConfig()
{
  $(".reparationAdd").hide();
  $(".reparationEdit").hide();
  $(".ListReparation").show();
  $(".reparationDelete").hide();

}


  function reparation_get_region_base_country(region_id=null)
   {
     country_id=$('#reparation_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
         html ='';
         html =html + '<option value="">Select Region</option>';
         
         $.each(response, function( index, value ) {  
         if(region_id==value.id)
         {
           html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
         }
         else{
           html =html + '<option value="'+value.id+'">'+value.name+'</option>';
         }
         });
         $('#reparation_region_id').html(html);
       }
   }); 
}

function reparation_get_cities_base_country_region(region_id=null,cities_id=null)
{

region_id=$('#reparation_region_id').val();

$.ajax({
    url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
    method: 'post',
    data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
    dataType: 'json',
    success: function(response){
    console.log(response);
    html ='';
    html =html + ' <option value="">Select cities</option>';

    $.each(response, function( index, value ) {
    // console.log( value );
                

    if(cities_id==value.id)
    {
        html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';

    }
    else{
        html =html + '<option value="'+value.id+'">'+value.name+'</option>';

    }
    });

    $('#reparation_cities_id').html(html);


    }


});
}


//For Edit Page

function edit_reparation_get_cities_base_country_region(region_id=null,cities_id=null)
{
    region_id=$('#edit_reparation_region_id').val();
    $.ajax({
        url: '<?php echo base_url().'admin/drivers/ajax_get_cities_listing'; ?>',
        method: 'post',
        data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&region_id='+region_id,
        dataType: 'json',
        success: function(response){
            html ='';
            html =html + ' <option value="">Select cities</option>';

            $.each(response, function( index, value ) {
            if(cities_id==value.id)
            {
                html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';
            }
            else{
                html =html + '<option value="'+value.id+'">'+value.name+'</option>';
            }
            });
            $('#edit_reparation_cities_id').html(html);
        }
    });
}
   function edit_reparation_get_region_base_country(region_id=null)
   { 
     country_id=$('#edit_reparation_country_id').val();
     $.ajax({
       url:'<?php echo base_url();?>admin/drivers/ajax_get_region_listing.php',
       method: 'post',
       data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
       dataType: 'json',
       success: function(response){
     html ='';
     html =html + '<option value="">Select Region</option>';
     
     $.each(response, function( index, value ) {
     if(region_id==value.id)
     {
       html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';  
     }
     else{
       html =html + '<option value="'+value.id+'">'+value.name+'</option>';
     }
     });
     $('#edit_reparation_region_id').html(html);
     }   
     }); 
   }
//For Edit Page

//create form submit
$(document).on('submit',"form#create_reparation_form",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
          //alert(data.record);
         setupDivReparation();
         $('.ListReparation').empty();
         $('.ListReparation').html(data.record);
         createinnerconfigtable('.configInnerReparationTable');
         reparationEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivReparation();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListReparation").show();
        }
     document.getElementById("create_reparation_form").reset();
      }
    });
    return false;
  });
//create form submit

//update form submit
$(document).on('submit',"form#update_reparation_form",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
     $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
       
         setupDivReparation();
         $('.ListReparation').empty();
         $('.ListReparation').html(data.record);
         createinnerconfigtable('.configInnerReparationTable');
         reparationEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
          
          setupDivReparation();
          var msg=data.message;
          var msgType=data.messageType;
          var className=data.className;
          var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
           if ($("#inner_alert_div").hasClass("alert-success")) {
            $('#inner_alert_div').removeClass("alert-success");
          }
          if ($("#inner_alert_div").hasClass("alert-danger")) {
            $('#inner_alert_div').removeClass("alert-danger");
          }
          $('#inner_alert_div').addClass(className);
          $('#inner_message_div').empty();
          $('#inner_message_div').html(htmlContent);
          $('#inner_alert_div').show();
          $(".ListReparation").show();
        }
     document.getElementById("update_reparation_form").reset();
      }
    });
    return false;
  });
//update form submit

//delete form submit
$(document).on('submit',"form#deletereparationform",function(event){
    
     var str = $(this).serialize();
     var action = $(this).attr('action'); 
    $.ajax({
      type: "POST",
      url: action,
      data:str,
      success: function(data) {
        if(data.result == '200'){
         setupDivReparation();
         $('.ListReparation').empty();
         $('.ListReparation').html(data.record);
         createinnerconfigtable('.configInnerReparationTable');
         reparationEvent();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
         if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
        }else{
         setupDivReparation();
         var msg=data.message;
         var msgType=data.messageType;
         var className=data.className;
         var htmlContent='<strong>'+msgType+'</strong> : '+msg; 
          if ($("#inner_alert_div").hasClass("alert-success")) {
           $('#inner_alert_div').removeClass("alert-success");
         }
         if ($("#inner_alert_div").hasClass("alert-danger")) {
           $('#inner_alert_div').removeClass("alert-danger");
         }
         $('#inner_alert_div').addClass(className);
         $('#inner_message_div').empty();
         $('#inner_message_div').html(htmlContent);
         $('#inner_alert_div').show();
         $(".ListReparation").show();
        }
     document.getElementById("addreparationform").reset();
      }
    });
    return false;
  });
//delete form submit
</script>
