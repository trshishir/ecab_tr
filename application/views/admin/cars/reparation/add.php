             <?php
              $attributes = array("name" => 'create_reparation_form', "id" => 'create_reparation_form'); 
              echo form_open('admin/cars/reparationAdd',$attributes); ?>
                <input type="hidden" value="<?= $car->id ?>" name="profile_id">
                      <!-- Row 1 -->
                        <div class="col-md-12 pdhz" style="margin-top:10px;" >
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Statut : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                           <select class="form-control" name="statut">
                                               <option value="">Select</option>
                                               <option value="1">Show</option>
                                               <option value="0">Hide</option> 
                                           </select>
                                        </div>
                                    </div>
                                </div>

                                  <div class="col-md-3">
                                     <div class="form-group">
                                         <div class="col-md-4" style="margin-top: 5px;">
                                             <span style="font-weight: normal;">Reparation : </span>
                                         </div>
                                         <div class="col-md-8" style="padding: 0px;">
                                            <select class="form-control" name="reparation">
                                                <option value="">Select</option>
                                                <option value="xyz">xyz</option>
                                            </select>
                                         </div>
                                     </div>
                                  </div>

                                 <div class="col-md-3">
                                   <div class="form-group">
                                       <div class="col-md-4" style="margin-top: 5px;">
                                           <span style="font-weight: normal;">Price : </span>
                                       </div>
                                       <div class="col-md-8" style="padding: 0px;">
                                          <input type="text" name="price"  class="form-control">
                                       </div>
                                   </div>
                                </div>
                                <div class="col-md-3">
                                   <div class="form-group">
                                       <div class="col-md-4" style="margin-top: 5px;">
                                           <span style="font-weight: normal;">TVA % : </span>
                                       </div>
                                       <div class="col-md-8" style="padding: 0px;">
                                          <select class="form-control" name="tva">
                                              <option value="">Select</option>
                                              <option value="10">10</option>
                                              <option value="20">20</option>
                                              <option value="30">30</option>
                                              <option value="40">40</option>
                                          </select>
                                       </div>
                                   </div>
                                </div>
                          </div>
                          <!-- Row 2 -->
                          <div class="col-md-12 pdhz" style="margin-top: 10px;">
                             <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Date : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                             <input type="text" name="start_date" id="rep_start_date" class="form-control datepicker" value="<?php echo date('d/m/Y');?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-top: 5px;">
                                            <span style="font-weight: normal;">Kilometer age : </span>
                                        </div>
                                        <div class="col-md-8" style="padding: 0px;">
                                           <input type="text" name="start_age" id="rep_start_age" class="form-control">
                                        </div>
                                    </div>
                                </div>
                          </div>

                     
                          <div class="col-md-12" style="padding: 30px;">                       
                              <!-- icons -->
                               <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                              <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelReparation()"><span class="fa fa-close"> Cancel </span></button>
                              <!-- icons -->  
                          </div>

                     <?php echo form_close(); ?>
 