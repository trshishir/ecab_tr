<?php $locale_info = localeconv(); ?>
<div class="col-md-12"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12">
                <?php $this->load->view('admin/common/alert');?>
                <div class="module">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="module-head">
                    </div>
                    <div class="module-body">
                        <?=form_open("admin/quote_requests/add")?>

                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Statut*</label>
                                    <select class="form-control" name="status" required>
                                        <?php foreach(config_model::$status as $key => $status):?>
                                            <option <?=set_value('status',$this->input->post('status')) == $status ? "selected" : ""?> value="<?=$status?>"><?=$status?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Civility*</label>
                                    <select class="form-control" name="civility" required>
                                        <?php foreach(config_model::$civility as $key => $civil):?>
                                        <option <?=set_value('civility',$this->input->post('civility')) == $civil ? "selected" : ""?> value="<?=$civil?>"><?=$civil?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Nom*</label>
                                    <input type="text" maxlength="100" class="form-control" required name="name" placeholder="Nom*" value="<?=set_value('name',$this->input->post('name'))?>">
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Prenom*</label>
                                    <input type="text" maxlength="100" class="form-control" required name="prename" placeholder="Prenom*" value="<?=set_value('prename',$this->input->post('prename'))?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Entreprise ou Organisme (Optionnel)</label>
                                    <input type="text" class="form-control" name="company" placeholder="Entreprise ou Organisme (Optionnel)" value="<?=set_value('company',$this->input->post('company'))?>">
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Telephone*</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                        <input id="num2" maxlength="50" type="text" class="form-control" required name="tel" placeholder="Telephone" value="<?=set_value('tel',$this->input->post('tel'))?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Email*</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                        <input id="phone-email" maxlength="100" type="email" class="form-control" required name="email" placeholder="Votre email" value="<?=set_value('email',$this->input->post('email'))?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Votre Addresse du devis</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                        <input id="addresse" maxlength="50" type="text" class="form-control" name="address1" placeholder="Votre Addresse" value="<?=set_value('address1',$this->input->post('address1'))?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Votre Complément d'adresse</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                        <input id="address2" maxlength="50" type="text" class="form-control" name="address2" placeholder="Complément d'adresse" value="<?=set_value('address2',$this->input->post('address2'))?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>PostalCode*</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                        <input id="postal_code" maxlength="50" type="text" class="form-control" name="postal_code" placeholder="PostalCode" value="<?=set_value('zipcode',$this->input->post('postal_code'))?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>City*</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                        <input id="phone-city" maxlength="100" type="text" class="form-control" required name="ville" placeholder="Votre ville" value="<?=set_value('city',$this->input->post('city'))?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Service Category</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-taxi"></i></span>
                                        <select class="form-control" id="the-contact-form-service-cat" name="service_category">
                                            <option value="">Service Category</option>
                                            <?php
                                            $service_cat = $this->base_model->run_query("SELECT * FROM vbs_u_category_service");
                                            if($service_cat != false)
                                                foreach($service_cat as $key => $item){
                                                    ?>
                                                    <option <?php  if(isset($service_form['service_category']['value']) && !empty($service_form['service_category']['value'])){ echo ($service_form['service_category']['value']==$item->id)?"selected":"";} ?> value="<?= $item->id ?>"><?= $item->category_name ?></option>
                                                    <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Service</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-taxi"></i></span>
                                        <select class="form-control" name="service_id" id="the-contact-form-service">
                                            <option value="">Service</option>
                                            <?php
                                            $service = $this->base_model->run_query("SELECT * FROM vbs_u_service");
                                            if($service != false)
                                                foreach($service as $key => $item){
                                                    ?>
                                                    <option cat-data="<?=$item->service_category?>" value="<?= $item->id; ?>"><?= $item->service_name; ?></option>
                                                    <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>Subject*</label>
                                    <select class="form-control" name="msg_subject" required>
                                        <?php foreach(config_model::$subjects as $key => $subject):?>
                                            <?php $selected = $key == 1 ? "selected" : ""?>
                                            <option <?=$selected?> value="<?=$subject?>"><?=$subject?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>Message*</label>
                                    <textarea name="message" required class="form-control"><?=set_value('message',$this->input->post('message'))?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                            <a href="<?=base_url("admin/quote_requests")?>" class="btn btn-default"><i class="fa fa-times"></i> Cancel</a>
                            <button class="btn btn-default"><i class="fa fa-save"></i> Save</button>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $(".bdatepicker").datepicker({
            format: "dd/mm/yyyy"
        });
    });
</script>