</header>
<div class="">
    <div class="slider_wrapper slider_wrapper_1" id="sliderWrapper">
        <ul class="homepageSlider">
			<li>
                <img src="<?php echo base_url(); ?>assets/system_design/images/slider/visite-guidee-paris-360x.jpg" title="Visite Guidee Paris" alt="Visite Guidee Paris" style="height:360px" />
                <div class="divCaption">
                    <div class="ncaption one white"><?php echo $this->lang->line('transfer_paristour'); ?></div>
                    <div class="ncaption two white"><?php echo $this->lang->line('transfer_parkgarden'); ?></div>
                    <div class="ncaption three">
                        <a href="<?php echo site_url(); ?>/services/10" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a>
                        <a href="<?php echo site_url(); ?>/welcome/onlineReservation" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>
                    </div>
                </div>
            </li>
			<li>
                <img src="<?php echo base_url(); ?>assets/system_design/images/slider/gare-de-lyon-360x.jpg" title="Gare De Lyon" alt="Gare De Lyon" style="height:360px" />
                <div class="divCaption">
                    <div class="ncaption one white"><?php echo $this->lang->line('transfer_delyon'); ?></div>
                    <div class="ncaption two white"><?php echo $this->lang->line('transfer_railways'); ?></div>
                    <div class="ncaption three">
                        <a href="<?php echo site_url(); ?>/services/9" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a>
                        <a href="<?php echo site_url(); ?>/welcome/onlineReservation" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>
                    </div>
                </div>
            </li>
			<li>
                <img src="<?php echo base_url(); ?>assets/system_design/images/slider/gare-saint-lazare-360x.jpg" title="Gare Saint Lazare" alt="Gare Saint Lazare" style="height:360px" />
                <div class="divCaption">
                    <div class="ncaption one white"><?php echo $this->lang->line('transfer_saintlazard'); ?></div>
                    <div class="ncaption two white"><?php echo $this->lang->line('transfer_railways'); ?></div>
                    <div class="ncaption three">
                        <a href="<?php echo site_url(); ?>/services/8" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a>
                        <a href="<?php echo site_url(); ?>/welcome/onlineReservation" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>
                    </div>
                </div>
            </li>
            <li>
                <img src="<?php echo base_url(); ?>assets/system_design/images/slider/aeroport-dorly-360x.jpg" title="Aeroport D'Orly" alt="Aeroport D'Orly" style="height:360px" />
                <div class="divCaption">
                    <div class="ncaption one white"><?php echo $this->lang->line('transfer_dorly'); ?></div>
                    <div class="ncaption two white"><?php echo $this->lang->line('transfer_airport'); ?></div>
                    <div class="ncaption three">
                        <a href="<?php echo site_url(); ?>/services/3" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a>
                        <a href="<?php echo site_url(); ?>/welcome/onlineReservation" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>
                    </div>
                </div>
            </li>
            <li>
                <img src="<?php echo base_url(); ?>assets/system_design/images/slider/aeroport-de-roissy-360x.jpg" title="Aeroport De Roissy CDG" alt="Aeroport De Roissy CDG" style="height:360px" />
                <div class="divCaption">
                    <div class="ncaption one white"><?php echo $this->lang->line('transfer_cdg'); ?></div>
                    <div class="ncaption two white"><?php echo $this->lang->line('transfer_airport'); ?></div>
                    <div class="ncaption three">
                        <a href="<?php echo site_url(); ?>/services/4" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a>
                        <a href="<?php echo site_url(); ?>/welcome/onlineReservation" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>
                    </div>
                </div>
            </li>
            <li>
                <img src="<?php echo base_url(); ?>assets/system_design/images/slider/aeroport-beauvais-360x.jpg" title="Aeroport Beauvais" alt="Aeroport Beauvais" style="height:360px" />
                <div class="divCaption">
                    <div class="ncaption one white"><?php echo $this->lang->line('transfer_beauvais'); ?></div>
                    <div class="ncaption two white"><?php echo $this->lang->line('transfer_airport'); ?></div>
                    <div class="ncaption three">
                        <a href="<?php echo site_url(); ?>/services/5" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a>
                        <a href="<?php echo site_url(); ?>/welcome/onlineReservation" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>
                    </div>
                </div>
            </li>
            <li>
                <img src="<?php echo base_url(); ?>assets/system_design/images/slider/aeroport-lebourget-360x.jpg" title="Aeroport Le Bourget" alt="Aeroport Le Bourget" style="height:360px" />
                <div class="divCaption">
                    <div class="ncaption one white"><?php echo $this->lang->line('transfer_lebourget'); ?></div>
                    <div class="ncaption two white"><?php echo $this->lang->line('transfer_airport'); ?></div>
                    <div class="ncaption three">
                        <a href="<?php echo site_url(); ?>/services/5" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a>
                        <a href="<?php echo site_url(); ?>/welcome/onlineReservation" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>
                    </div>
                </div>
            </li>
            <li>
                <img src="<?php echo base_url(); ?>assets/system_design/images/slider/aeroport-parisvarty-360x.jpg" title="Aeroport Paris Varty" alt="Aeroport Paris Varty" style="height:360px" />
                <div class="divCaption">
                    <div class="ncaption one white"><?php echo $this->lang->line('transfer_parisvatry'); ?></div>
                    <div class="ncaption two white"><?php echo $this->lang->line('transfer_airport'); ?></div>
                    <div class="ncaption three">
                        <a href="<?php echo site_url(); ?>/services/5" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a>
                        <a href="<?php echo site_url(); ?>/welcome/onlineReservation" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>
                    </div>
                </div>
            </li>
            <li>
                <img src="<?php echo base_url(); ?>assets/system_design/images/slider/paris-gare-du-nord-360x.jpg" title="Paris Gare Du Nord" alt="Paris Gare Du Nord" style="height:360px" />
                <div class="divCaption">
                    <div class="ncaption one white"><?php echo $this->lang->line('transfer_dunord'); ?></div>
                    <div class="ncaption two white"><?php echo $this->lang->line('transfer_railways'); ?></div>
                    <div class="ncaption three">
                        <a href="<?php echo site_url(); ?>/services/6" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a>
                        <a href="<?php echo site_url(); ?>/welcome/onlineReservation" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>
                    </div>
                </div>
            </li>
            <li>
                <img src="<?php echo base_url(); ?>assets/system_design/images/slider/gare-de-montparnasse-360x.jpg" title="Gare De Montparnasse" alt="Gare De Montparnasse" style="height:360px" />
                <div class="divCaption">
                    <div class="ncaption one white"><?php echo $this->lang->line('transfer_montparnasse'); ?></div>
                    <div class="ncaption two white"><?php echo $this->lang->line('transfer_railways'); ?></div>
                    <div class="ncaption three">
                        <a href="<?php echo site_url(); ?>/services/7" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a>
                        <a href="<?php echo site_url(); ?>/welcome/onlineReservation" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>
                    </div>
                </div>
            </li>
            <li>
                <img src="<?php echo base_url(); ?>assets/system_design/images/slider/gare-de-lest-360x.jpg" title="Gare De Lest" alt="Gare De Lest" style="height:360px" />
                <div class="divCaption">
                    <div class="ncaption one white"><?php echo $this->lang->line('transfer_delest'); ?></div>
                    <div class="ncaption two white"><?php echo $this->lang->line('transfer_railways'); ?></div>
                    <div class="ncaption three">
                        <a href="<?php echo site_url(); ?>/services/21" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a>
                        <a href="<?php echo site_url(); ?>/welcome/onlineReservation" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>
                    </div>
                </div>
            </li>
            <li>
                <img src="<?php echo base_url(); ?>assets/system_design/images/slider/transfert-versailles-360x.jpg" title="Transfert Versailles" alt="Transfert Versailles" style="height:360px" />
                <div class="divCaption">
                    <div class="ncaption one white">Transfert Versailles</div>
                    <div class="ncaption two white"><?php echo $this->lang->line('transfer_parkgarden'); ?></div>
                    <div class="ncaption three">
                        <a href="<?php echo site_url(); ?>/services/23" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a>
                        <a href="<?php echo site_url(); ?>/welcome/onlineReservation" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>
                    </div>
                </div>
            </li>
            <li>
                <img src="<?php echo base_url(); ?>assets/system_design/images/slider/gare-d-austerlitz-360x.jpg" title="Gare D Austerlitz" alt="Gare D Austerlitz" style="height:360px" />
                <div class="divCaption">
                    <div class="ncaption one white">Transfert Gare Austerlitz</div>
                    <div class="ncaption two white"><?php echo $this->lang->line('transfer_railways'); ?></div>
                    <div class="ncaption three">
                        <a href="<?php echo site_url(); ?>/services/24" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a>
                        <a href="<?php echo site_url(); ?>/welcome/onlineReservation" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>
                    </div>
                </div>
            </li>
            <li>
                <img src="<?php echo base_url(); ?>assets/system_design/images/slider/parc-asterix-360x.jpg" title="Parc Asterix" alt="Parc Asterix" style="height:360px" />
                <div class="divCaption">
                    <div class="ncaption one white"><?php echo $this->lang->line('transfer_asterix'); ?></div>
                    <div class="ncaption two white"><?php echo $this->lang->line('transfer_parkgarden'); ?></div>
                    <div class="ncaption three">
                        <a href="<?php echo site_url(); ?>/services/25" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a>
                        <a href="<?php echo site_url(); ?>/welcome/onlineReservation" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>
                    </div>
                </div>
            </li>
            
            <!-- 12-4-16
            <li>
                <img src="<?php // echo base_url(); ?>assets/system_design/images/slider/chateaux.jpg" title="Chateaux" />
                <div class="ncaption one white">Châteaux de Versailles</span>
                <div class="ncaption two white">Visites Guidees</span>
                <div class="ncaption three"><a href="<?php // echo site_url(); ?>/services/11" class="button light_orange_button"><?php // echo $this->lang->line('learn_more'); ?></a></span>
            </li>-->
            <!-- 12-4-16
            <li>
                <img src="<?php // echo base_url(); ?>assets/system_design/images/slider/fontainebleau.jpg" title="Fontainebleau" />
                <div class="ncaption one white">Fontainebleau Tour</span>
                <div class="ncaption two white">Visites Guidees</span>
                <div class="ncaption three"><a href="<?php // echo site_url(); ?>/services/12" class="button light_orange_button"><?php // echo $this->lang->line('learn_more'); ?></a></span>
            </li>-->
            <!-- 12-4-16
            <li>
                <img src="<?php // echo base_url(); ?>assets/system_design/images/slider/chantilly.jpg" title="Chantilly" />
                <div class="ncaption one white">Chantilly Tour</span>
                <div class="ncaption two white">Visites Guidees</span>
                <div class="ncaption three"><a href="<?php // echo site_url(); ?>/services/22" class="button light_orange_button"><?php // echo $this->lang->line('learn_more'); ?></a></span>
            </li>-->
            <!-- 12-4-16<li>
                <img src="<?php // echo base_url(); ?>assets/system_design/images/slider/honfleur-deauville.jpg" title="Gare De Lyon" />
                <div class="ncaption one white">Honfleur et Deauville</span>
                <div class="ncaption two white">Circuits Touristiques</span>
                <div class="ncaption three"><a href="<?php // echo site_url(); ?>/services/13" class="button light_orange_button"><?php // echo $this->lang->line('learn_more'); ?></a></span>
            </li>-->
            <!-- 12-4-16
            <li>
                <img src="<?php // echo base_url(); ?>assets/system_design/images/slider/plages-du-debarquement.jpg" title="Plages des débarquements" />
                <div class="ncaption one white">Plages des débarquements</span>
                <div class="ncaption two white">Circuits Touristiques</span>
                <div class="ncaption three"> <a href="<?php // echo site_url(); ?>/services/14" class="button light_orange_button"><?php // echo $this->lang->line('learn_more'); ?></a></span>
            </li>-->
            <!-- 12-4-16
            <li>
                <img src="<?php // echo base_url(); ?>assets/system_design/images/slider/mont-saint-michel.jpg" title="Saint Malo & Mont Saint Michel" />
                <div class="ncaption one white">Saint Malo & Mont Saint Michel</span>
                <div class="ncaption two white">Circuits Touristiques</span>
                <div class="ncaption three"><a href="<?php // echo site_url(); ?>/services/15" class="button light_orange_button"><?php // echo $this->lang->line('learn_more'); ?></a></span>
            </li>-->
            <!-- <li>
                <img src="<?php // echo base_url(); ?>assets/system_design/images/slider/disneyland-paris.jpg" title="Disneyland Paris" />
                <div class="ncaption one white">Disneyland Paris Tour</span>
                <div class="ncaption two white">Visites Guidees</span>
            </li>
            
            
            <div class="ncaption one white">Honfleur & Deauville</span>
                    <div class="ncaption two white">Circuits Touristiques</span>
            <li>
                    <img src="<?php // echo base_url(); ?>assets/system_design/images/slider/parc-asterix.jpg" title="Parc Asterix" />
                    <div class="ncaption one white">Parc Asterix</span>
                    <div class="ncaption two white">Services at Parc Asterix</span>
            </li>
            
            <li><img src="<?php // echo base_url(); ?>assets/system_design/images/slider/parc-asterix-1.jpg" title="Parc Asterix" />
                    <div class="ncaption one white">Parc Asterix</span>
                    <div class="ncaption two white">Services at Parc Asterix</span>
            </li>-->
        </ul>
        <!-- FORM WRAPPER -->
    </div>
</div>

<section id="quality_section">
    <div class="container padding-p-0">
        <div class="quality_container">
            <!--<ul>
                <li class="title_bar"><h2>Our Commitments</h2></li>
                <li><h4><i class="fa fa-clock-o"></i> Punctuality</h4></li>
                <li><h4><i class="fa fa-lock"></i> Security</h4></li>
                <li><h4><i class="fa fa-thumbs-up"></i> Quality</h4></li>
                <li><h4><i class="fa fa-eur"></i> Economy</h4></li>
            </ul>-->
            <div class="col-md-4 title_bar">
                <h2><?php echo $this->lang->line("our_commitments") ?></h2>
            </div>
            <div class="col-md-2">
                <h4><i class="fa fa-clock-o"></i> <?php echo $this->lang->line("punctuality") ?></h4>
            </div>
            <div class="col-md-2">
                <h4><i class="fa fa-lock"></i> <?php echo $this->lang->line("security") ?></h4>
            </div>
            <div class="col-md-2">
                <h4><i class="fa fa-thumbs-up"></i> <?php echo $this->lang->line("quality") ?></h4>
            </div>
            <div class="col-md-2">
                <h4><i class="fa fa-eur"></i> <?php echo $this->lang->line("economy") ?></h4>
            </div>
        </div>
    </div>
</section>
<section id="testimonial_section">
    <div class="container padding-p-0">
        <div class="testimonial_container">
            <div class="col-md-4">
                <h2><?php echo $this->lang->line("testimonials_of_our_clients") ?></h2>
            </div>
            <div class="col-md-8">
                <div class="testimonial-slider-wrap">
                        <?php if (count($testimonials) > 0) { ?>
                        <ul class="testy-bxslider">
    <?php foreach ($testimonials as $row): ?>   
                                <li>
                                    <div class="testy-img">
                                        <img height="60" width="60" alt="Profile" title="Profile" src="<?php echo base_url(); ?>/uploads/testimonials_images/<?php
                                        if ($row->user_photo != "")
                                            echo $row->user_photo;
                                        else
                                            echo "dummy.jpg";
                                        ?>" class="img-responsive">
                                    </div>
                                    <div class="testy-content">
                                        <div class="testy-text"><?php echo substr($row->content, 0, 150); ?> ...</div>
                                        <h3 class="testimonial_name"><?php echo $row->title; ?></h3>
                                    </div>
                                </li>
    <?php endforeach; ?>
                            <!--<li>
                                <div class="testy-img"><img src="<?php echo base_url(); ?>assets/system_design/images/services/transfert-aeroport.jpg" width="65" height="65"/></div>
                                <div class="testy-content">
                                    <div class="testy-text">We were referred to Studio 7 Designs from colleagues of ours in London. Not only have we been constantly amazed at the depth of understanding </div>
                                    <h3 class="testimonial_name">Sarah Evens.. CEO, Network Software</h3>
                                </div>    
                            </li>
                            <li>
                                <div class="testy-img"><img src="<?php echo base_url(); ?>assets/system_design/images/services/transfert-gare.jpg" width="65" height="65"/></div>
                                <div class="testy-content">
                                    <div class="testy-text">There are design companies, and then there are user experience, design, consulting, interface design, brilliant, and professional. Studio 7 Designs</div>
                                    <h3 class="testimonial_name">Janet Heartly., CTO, Yahoo(Asia Pacific)</h3>
                                </div>    
                            </li>-->
                        </ul>
<?php
}
else {
    ?>
                        <div class="col-md-9">
                            <div class="left-side-cont">
                                <p align="center" style="margin:140px 0px">Coming Soon...</p>
                            </div>
                        </div>
<?php } ?>
                    <div class="testy-more"><a href="<?php echo site_url(); ?>/welcome/testimonials" class="button blue_button"><?php echo $this->lang->line('see_more'); ?></a></div>
                </div>
            </div>
        </div>
    </div>    
</section>
<section id="package_section">
    <div class="container-fluid body-bg">
        <div class="container body-border">
            <?php if (count($packages) > 0) : ?>
                <?php
                $sno = 1;
                foreach ($packages as $r) {
                        if (($sno % 3) == 1 || $sno == 1) : 
                ?>          <div class="row">         
                <?php   endif;
                    ?>  
                    
                                <div class="col-md-4" style="margin-top:15px;">
                                    <?php if ($sno >= 1 && $sno <= 3) :?>
                                    <div class="corner-left-ribbon" ></div>
                                    <?php endif;?>
                                    <div class="plans">
                                        <div class="plans_head">
                                                    <!--<div class="corner-left-ribbon top-left" ><span class="promo-txt">Promotion</span></div>-->
                                        <h3 style="margin-left:-11px;"><?php echo $r->name; ?></h3></div>
                                        <div class="corner-right-ribbon top-right">
                                            <?php
                                            $service_price = str_replace(".00", "", $r->charge_distance);
                                            if (strlen($service_price) > 2) {
                                                ?>          <span class="pricem"><?php echo $service_price; ?> <i class="fa fa-eur"></i></span>
                    <?php } else {
                        ?>
                                                <span class="price"><?php echo $service_price; ?> <i class="fa fa-eur"></i></span>
                    <?php } ?>
                                            <span class="startfrom"><?php echo $this->lang->line("start_from"); ?></span>
                                        </div>
                                        <!--<div class="corner-promo-ribbon top-right-promo"><span class="promo-service">Promotion</span></div>-->
                                        <div class="col-md-12 pkg_title_row"> 
                                                <div class="plans_img"><img src="<?php echo base_url(); ?>uploads/service_images/<?php if ($r->image != "") echo $r->service_image; else echo "default-car.jpg"; ?>" alt="<?php echo $r->vehicle_name . " - " . $r->model; ?>" title="<?php echo $r->vehicle_name . " - " . $r->model; ?>"></div>
                                        </div>
                                        <!--<div class="col-md-12 plan_title" id="plan_title_row">
                                            <p><?php //echo $this->lang->line('package_details_caps'); ?></p>
                                        </div>-->
                                        <div class="description">
                                        <?php   if ($this->lang->lang() == 'en') {
                                                    //  echo substr($r->terms_conditions, 0, 150);
                                                    echo $r->meta_description_en;
                                                } else if ($this->lang->lang() == 'fr') {
                                                    echo $r->meta_description_fr;
                                                } 
                                        ?>
                                        </div>
                                        <div class="button-row">
                    <?php if ($r->service_id != NULL): ?><div class="learn_button"><a href="<?php echo site_url(); ?>/services/<?php echo $r->service_id; ?>" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a></div><?php endif; ?>     
                                            <div class="orderbtn"><a href="<?php echo site_url(); ?>/welcome/onlineReservation" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a></div>
                                        </div>
                                    </div>
                                </div>
                <?php $sno++; 
                        if (($sno % 3) == 1) : 
                ?>          </div>
                <?php   endif;
                    } ?>
            <?php endif; ?>
                <!--<div class="span4">
                    <div class="plans">
                        <div class="plans_head"><h3>Transport AEROPORT</h3></div>
                       
                        <div class="corner-promo-ribbon top-right-promo"><span class="promo-service">Promotion</span></div>
                        <div class="col-md-12" id="plan_title_row">
                            <div class="col-md-6"><div class="plans_img"><img src="<?php echo base_url(); ?>assets/system_design/images/services/transfert-aeroport.jpg"></div></div>
                            <div class="col-md-6 plan_title">
                                    <p>VOUS CHERCHEZ UN TRANSPORT POUR VOUS RENDRE Ã L'AEROPORT ? ...</p>
                            </div>
                        </div>
                        <div class="description">Trans express vient vous chercher Ã  votre domicile ou Ã  la descente de vot ...</div>
                        <div>
                            <div class="learn_button"><a href="service.php?id=7" class="button blue_button">learn more</a></div>     
                            <div class="orderbtn"><a href="booking.php?Sid=7&amp;Cid=3" class="button green_button">Book now</a></div>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="plans">
                        <div class="plans_head"><h3>Transport Gare</h3></div>
                        <div class="corner-left-ribbon top-left" ><span class="head-txt1">Vente</span><span class="head-txt2">FLASH</span></div>
                        <div class="corner-right-ribbon top-right"><span class="price">$49</span><span class="startfrom">Start From</span></div>
                        <div class="corner-promo-ribbon top-right-promo"><span class="promo-service">Promotion</span></div>
                        <div class="col-md-12" id="plan_title_row">
                            <div class="col-md-6"><div class="plans_img"><img src="<?php echo base_url(); ?>assets/system_design/images/services/transfert-gare.jpg"></div></div>
                            <div class="col-md-6 plan_title">
                                <p>VOUS CHERCHEZ UN TRANSPORT POUR VOUS RENDRE Ã LA GARE ? OU ...</p>
                            </div>
                        </div>
                        <div class="description">Trans express ne vous fera jamais manquer votre train! Nous vous transportons rapidement dans toutes...</div>
                        <div>
                            <div class="learn_button"><a href="service.php?id=7" class="button blue_button">learn more</a></div>    
                            <div class="orderbtn"><a href="booking.php?Sid=7&amp;Cid=3" class="button green_button">Book now</a></div>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="plans">
                        <div class="plans_head"><h3>Transport Hotel</h3></div>
                        <div class="corner-left-ribbon top-left" ><span class="head-txt1">Vente</span><span class="head-txt2">FLASH</span></div>
                        <div class="corner-right-ribbon top-right"><span class="price">$49</span><span class="startfrom">Start From</span></div>
                        <div class="corner-promo-ribbon top-right-promo"><span class="promo-service">Promotion</span></div>
                        <div class="col-md-12" id="plan_title_row">
                            <div class="col-md-6"><div class="plans_img"><img src="<?php echo base_url(); ?>assets/system_design/images/services/transfert-hotel.jpg"></div></div>
                            <div class="col-md-6 plan_title">
                                <p>VOUS CHERCHEZ UN TRANSPORT POUR VOUS RENDRE Ã L'HOTEL ? OU ...</p>
                            </div>
                        </div>    
                        <div class="description">Trans express vous transporte jusqu'Ã  l'hÃ´tel parisien oÃ¹ vo ...</div>
                        <div>
                            <div class="learn_button"><a href="service.php?id=7" class="button blue_button">learn more</a></div>      
                            <div class="orderbtn"><a href="booking.php?Sid=7&amp;Cid=3" class="button green_button">Book now</a></div>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="plans">
                        <div class="plans_head"><h3>Transport Pro</h3></div>
                        <div class="corner-left-ribbon top-left" ><span class="head-txt1">Vente</span><span class="head-txt2">FLASH</span></div>
                        <div class="corner-right-ribbon top-right"><span class="price">$49</span><span class="startfrom">Start From</span></div>
                        <div class="corner-promo-ribbon top-right-promo"><span class="promo-service">Promotion</span></div>
                        <div class="col-md-12" id="plan_title_row">
                            <div class="col-md-6"><div class="plans_img"><img src="<?php echo base_url(); ?>assets/system_design/images/services/transfert-professionnel.jpg"></div></div>
                            <div class="col-md-6 plan_title">
                                <p>VOUS CHERCHEZ UN TRANSPORT POUR VOUS RENDRE Ã VOTRE TRAVAIL ...</p>
                            </div>
                        </div>    
                        <div class="description">C'est avec sÃ©rÃ©nitÃ© que vous pouvez faire appel Ã ...</div>
                        <div>
                            <div class="learn_button"><a href="service.php?id=7" class="button blue_button">learn more</a></div>      
                            <div class="orderbtn"><a href="booking.php?Sid=7&amp;Cid=3" class="button green_button">Book now</a></div>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="plans">
                        <div class="plans_head"><h3>Transport Institution</h3></div>
                        <div class="corner-left-ribbon top-left" ><span class="head-txt1">Vente</span><span class="head-txt2">FLASH</span></div>
                        <div class="corner-right-ribbon top-right"><span class="price">$49</span><span class="startfrom">Start From</span></div>
                        <div class="corner-promo-ribbon top-right-promo"><span class="promo-service">Promotion</span></div>
                        <div class="col-md-12" id="plan_title_row">
                            <div class="col-md-6"><div class="plans_img"><img src="<?php echo base_url(); ?>assets/system_design/images/services/transfert-institution.jpg"></div></div>
                            <div class="col-md-6 plan_title">
                                <p>VOUS CHERCHEZ UN TRANSPORT POUR LES MEMBRES DE VOTRE CENTER ...</p>
                            </div>
                        </div>    
                        <div class="description">EquipÃ© de vÃ©hicules qui permettent le transport de plusieurs personnes, ...</div>
                        <div>
                            <div class="learn_button"><a href="service.php?id=7" class="button blue_button">learn more</a></div>      
                            <div class="orderbtn"><a href="booking.php?Sid=7&amp;Cid=3" class="button green_button">Book now</a></div>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="plans">
                        <div class="plans_head"><h3>Transport Scolaire</h3></div>
                        <div class="corner-left-ribbon top-left" ><span class="head-txt1">Vente</span><span class="head-txt2">FLASH</span></div>
                        <div class="corner-right-ribbon top-right"><span class="price">$49</span><span class="startfrom">Start From</span></div>
                        <div class="corner-promo-ribbon top-right-promo"><span class="promo-service">Promotion</span></div>
                        <div class="col-md-12" id="plan_title_row">
                            <div class="col-md-6"><div class="plans_img"><img src="<?php echo base_url(); ?>assets/system_design/images/services/transfert-scolaire.jpg"></div></div>
                            <div class="col-md-6 plan_title">
                                <p>VOUS CHERCHEZ UN TRANSPORT POUR LES MEMBRES DE VOTRE CENTER ...</p>
                            </div>
                        </div>    
                        <div class="description">Trans express prend en charge vos enfants sur le trajet de l'Ã©cole. Ne vous inqui & Atild ...</div>
                        <div>
                            <div class="learn_button"><a href="service.php?id=7" class="button blue_button">learn more</a></div>      
                            <div class="orderbtn"><a href="booking.php?Sid=7&amp;Cid=3" class="button green_button">Book now</a></div>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="plans">
                        <div class="plans_head"><h3>Transport Private Tour</h3></div>
                        <div class="corner-left-ribbon top-left" ><span class="head-txt1">Vente</span><span class="head-txt2">FLASH</span></div>
                        <div class="corner-right-ribbon top-right"><span class="price">$49</span><span class="startfrom">Start From</span></div>
                        <div class="corner-promo-ribbon top-right-promo"><span class="promo-service">Promotion</span></div>
                        <div class="col-md-12" id="plan_title_row">
                            <div class="col-md-6"><div class="plans_img"><img src="<?php echo base_url(); ?>assets/system_design/images/services/transfert-private-tour-visite-guidee.jpg"></div></div>
                            <div class="col-md-6 plan_title">
                                <p>VOUS CHERCHEZ UN TRANSPORT POUR LES MEMBRES DE VOTRE CENTER ...</p>
                            </div>
                        </div>    
                        <div class="description">Trans express prend en charge vos enfants sur le trajet de l'Ã©cole. Ne vous inqui & Atild ...</div>
                        <div>
                            <div class="learn_button"><a href="service.php?id=7" class="button blue_button">learn more</a></div>      
                            <div class="orderbtn"><a href="booking.php?Sid=7&amp;Cid=3" class="button green_button">Book now</a></div>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="plans">
                        <div class="plans_head"><h3>Transport BUSINESS</h3></div>
                        <div class="corner-left-ribbon top-left" ><span class="head-txt1">Vente</span><span class="head-txt2">FLASH</span></div>
                        <div class="corner-right-ribbon top-right"><span class="price">$49</span><span class="startfrom">Start From</span></div>
                        <div class="corner-promo-ribbon top-right-promo"><span class="promo-service">Promotion</span></div>
                        <div class="col-md-12" id="plan_title_row">
                            <div class="col-md-6"><div class="plans_img"><img src="<?php echo base_url(); ?>assets/system_design/images/services/transfert-business.jpg"></div></div>
                            <div class="col-md-6 plan_title">
                                <p>VOUS CHERCHEZ UN TRANSPORT POUR LES MEMBRES DE VOTRE CENTER ...</p>
                            </div>
                        </div>    
                        <div class="description">Vous venez Ã  Paris en voyage d'affaires? Concentrez-vous sur le principal et confiez ...</div>
                        <div>
                            <div class="learn_button"><a href="service.php?id=7" class="button blue_button">learn more</a></div>      
                            <div class="orderbtn"><a href="booking.php?Sid=7&amp;Cid=3" class="button green_button">Book now</a></div>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="plans">
                        <div class="plans_head"><h3>Transport VOYAGE</h3></div>
                        <div class="corner-left-ribbon top-left" ><span class="head-txt1">Vente</span><span class="head-txt2">FLASH</span></div>
                        <div class="corner-right-ribbon top-right"><span class="price">$49</span><span class="startfrom">Start From</span></div>
                        <div class="corner-promo-ribbon top-right-promo"><span class="promo-service">Promotion</span></div>
                        <div class="col-md-12" id="plan_title_row">
                            <div class="col-md-6"><div class="plans_img"><img src="<?php echo base_url(); ?>assets/system_design/images/services/transfert-voyage.jpg"></div></div>
                            <div class="col-md-6 plan_title">
                                <p>VOUS CHERCHEZ UN TRANSPORT POUR LES MEMBRES DE VOTRE CENTER ...</p>
                            </div>
                        </div>    
                        <div class="description">Les pouvoirs de Trans express sont sans frontiÃ¨res! Nous partons avec vous en voyage, ...</div>
                        <div>
                            <div class="learn_button"><a href="service.php?id=7" class="button blue_button">learn more</a></div>      
                            <div class="orderbtn"><a href="booking.php?Sid=7&amp;Cid=3" class="button green_button">Book now</a></div>
                        </div>
                    </div>
                </div>-->
        </div>
    </div>
</section>

<script src="<?php echo base_url(); ?>assets/system_design/scripts/bookings.js"></script>
<script type="text/javascript">
    (function ($, W, D)
    {
        var JQUERY4U = {};

        JQUERY4U.UTIL =
                {
                    setupFormValidation: function ()
                    {
                        //Additional Methods			
                        //form validation rules
                        $("#online_booking_form").validate({
                            rules: {
                                pick_up: {
                                    required: true

                                },
                                drop_of: {
                                    required: true

                                },
                                pick_time: {
                                    required: true
                                }
                            },
                            messages: {
                                pick_up: {
                                    required: "<?php echo $this->lang->line('pick_location_valid'); ?>"
                                },
                                drop_of: {
                                    required: "<?php echo $this->lang->line('drop_location_valid'); ?>"
                                },
                                pick_time: {
                                    required: "<?php echo $this->lang->line('pick_time_valid'); ?>"
                                }
                            },
                            submitHandler: function (form) {
                                form.submit();
                            }
                        });
                    }
                }

        //when the dom has loaded setup form validation rules
        $(D).ready(function ($) {
            JQUERY4U.UTIL.setupFormValidation();
        });

    })(jQuery, window, document);


    $(function () {
        $('.ilike').click(function () {
            var $this = $(this);
            var p1 = $this.data('value');
            var arry = $this.data('arry');
            changeField(p1,arry);
        });
        $('.homepageSlider').bxSlider({
            mode: 'fade',
            auto: true,
            pager: true,
            autoControl: false,
            adaptiveHeight: true,
            easing: 'swing',
            responsive: true,
            preloadImages: 'all'
        });
        $(".slider_wrapper .bx-viewport").css("height", '360px');
        $('.testy-bxslider').bxSlider({
            mode: 'fade',
            controls: false,
            auto: true
        });

        $("#dtpicker").datepicker({dateFormat: "<?php echo strtolower($date_elem1); ?>-<?php echo strtolower($date_elem2); ?>-yy"});
        $("#dtpicker1").datepicker({dateFormat: "<?php echo strtolower($date_elem1); ?>-<?php echo strtolower($date_elem2); ?>-yy"});
    });
    function changeField(ss, divID)
    {
        if (ss == "pick_airport") {
            /*$('#local_pick').hide();
             $('#airport_pick').show();
             $('#airport_pick').attr('disabled', false);
             $('#local_pick').attr('disabled', true);
             $('#pick_airport_link').hide();
             $('#pick_type').val('A');
             $('#pick_local_link').show();*/
            showAirportPick();
        }
        else if (ss == "drop_airport") {
            /*$('#local_drop').hide();
             $('#airport_drop').show();
             $('#airport_drop').attr('disabled', false);
             $('#local_drop').attr('disabled', true);
             $('#drop_type').val('A');
             $('#drop_airport_link').hide();
             $('#drop_local_link').show();*/
            showAirportDrop(divID);

        }
        else if (ss == "pick_local") {
            /*$('#local_pick').show();
             $('#airport_pick').hide();
             $('#airport_pick').attr('disabled', true);
             $('#local_pick').attr('disabled', false);
             $('#pick_type').val('L');
             $('#pick_airport_link').show();
             $('#pick_local_link').hide();*/
            showLocalPick();
        }
        else if (ss == 'pick_train') {
            showTrainPick();
        }
        else if (ss == 'pick_hotel') {
            showHotelPick();
        }
        else if (ss == 'pick_park') {
            showParkPick();
        }
        else if (ss == "drop_local") {
            showLocalDrop(divID);
        }
        else if (ss == 'drop_train') {
            showTrainDrop(divID);
        }
        else if (ss == 'drop_hotel') {
            showHotelDrop(divID);
        }
        else if (ss == 'drop_park') {
            showParkDrop(divID);
        }

    }
</script>