<link href="<?=base_url();?>assets/css/timepicki.css" rel="stylesheet">
<script src="<?=base_url();?>assets/css/timepicki.js"></script>


<link href="<?=base_url();?>assets/css/jquery.multiselect.css" rel="stylesheet" />
<script src="<?=base_url();?>assets/css/jquery.multiselect.js"></script>



<script type="text/javascript">

			
//	Status
	function StatusAdd()
	{
		setupStatus();
		$(".StatusAdd").show();
	}
	
	function StatusCancel()
	{
		setupStatus();
		$(".listStatus").show();
	}
	
	
	
	function StatusEdit()
	{
		var val = $('.clicked-statut').val();
		
		if(val != "")
		{
			setupStatus();
			$(".StatusEdit").show();
			$("#StatusEditview"+val).show();
		}
	}
	
	function StatusDelete()
	{
		var val = $('.clicked-statut').val();
		if(val != "")
		{
			setupStatus();
			$(".StatusDelete").show();
			$("#statutid").val(val);
			
		}
	}
	
	
	function setupStatus()
	{
		$(".StatusAdd").hide();
		$(".StatusEdit").hide();
		$(".StatusEditEdit").hide();
		$(".StatusDelete").hide();
		$(".listStatus").hide();
	}	
	
	
//	TypeofContract
	function TypeofContractAdd()
	{
		setupTypeofContract();
		$(".TypeofContractAdd").show();
	}
	
	function TypeofContractCancel()
	{
		setupTypeofContract();
		$(".listTypeofContract").show();
	}
	
	function TypeofContractEdit()
	{
		var val = $('.clicked-typeofcontract').val();
		
		if(val != "")
		{
			setupTypeofContract();
			$(".TypeofContractEdit").show();
			$("#TypeofContractEditview"+val).show();
		}
	}
	
	function TypeofContractDelete()
	{
		var val = $('.clicked-typeofcontract').val();
		if(val != "")
		{
			setupTypeofContract();
			$(".TypeofContractDelete").show();
			$("#typeofcontractid").val(val);
			
		}
	}
	
	function setupTypeofContract()
	{
		$(".TypeofContractAdd").hide();
		$(".TypeofContractEdit").hide();
		$(".TypeofContractDelete").hide();
		$(".listTypeofContract").hide();
		$(".EditTypeofContractEdit").hide();
	}	
	
	
//	HoursPerMonth
	function HoursPerMonthAdd()
	{
		setupHoursPerMonth();
		$(".HoursPerMonthAdd").show();
	}
	
	function HoursPerMonthCancel()
	{
		setupHoursPerMonth();
		$(".listHoursPerMonth").show();
	}
	
	function HoursPerMonthEdit()
	{
		var val = $('.clicked-hourspermonth').val();
		
		if(val != "")
		{
			setupHoursPerMonth();
			$(".HoursPerMonthEdit").show();
			$("#HoursPerMonthEditview"+val).show();
		}
	}
	
	function HoursPerMonthDelete()
	{
		var val = $('.clicked-hourspermonth').val();
		if(val != "")
		{
			setupHoursPerMonth();
			$(".HoursPerMonthDelete").show();
			$("#hourspermonthid").val(val);
			
		}
	}
	
	function setupHoursPerMonth()
	{
		$(".HoursPerMonthAdd").hide();
		$(".HoursPerMonthEdit").hide();
		$(".EditHoursPerMonthEdit").hide();
		$(".HoursPerMonthDelete").hide();
		$(".listHoursPerMonth").hide();
	}	
	
//	Job
	function JobAdd()
	{
		setupJob();
		$(".JobAdd").show();
	}
	
	function JobCancel()
	{
		setupJob();
		$(".listJob").show();
	}
	
	function JobEdit()
	{
		var val = $('.clicked-job').val();
		
		if(val != "")
		{
			setupJob();
			$(".JobEdit").show();
			$("#JobEditview"+val).show();
		}
	}
	
	function JobDelete()
	{
		var val = $('.clicked-job').val();
		if(val != "")
		{
			setupJob();
			$(".JobDelete").show();
			$("#jobid").val(val);
			
		}
	}
	
	function setupJob()
	{
		$(".JobAdd").hide();
		$(".JobEdit").hide();
		$(".JobDelete").hide();
		$(".JobEditEdit").hide();
		$(".listJob").hide();
	}	
	
//	NatureofContract
	function NatureofContractAdd()
	{
		setupNatureofContract();
		$(".NatureofContractAdd").show();
	}
	
	function NatureofContractCancel()
	{
		setupNatureofContract();
		$(".listNatureofContract").show();
	}
	
	function NatureofContractEdit()
	{
		var val = $('.clicked-natureofcontract').val();
		
		if(val != "")
		{
			setupNatureofContract();
			$(".NatureofContractEdit").show();
			$("#NatureofContractEditview"+val).show();
		}
	}
	
	function NatureofContractDelete()
	{
		var val = $('.clicked-natureofcontract').val();
		if(val != "")
		{
			setupNatureofContract();
			$(".NatureofContractDelete").show();
			$("#natureofcontractid").val(val);
			
		}
	}
	
	function setupNatureofContract()
	{
		$(".NatureofContractAdd").hide();
		$(".NatureofContractEdit").hide();
		$(".NatureofContractEditEdit").hide();
		$(".NatureofContractDelete").hide();
		$(".listNatureofContract").hide();
	}	
	
//	WorkingPlace
	function WorkingPlaceAdd()
	{
		setupWorkingPlace();
		$(".WorkingPlaceAdd").show();
	}
	
	function WorkingPlaceCancel()
	{
		setupWorkingPlace();
		$(".listWorkingPlace").show();
	}
	
	function WorkingPlaceEdit()
	{
		var val = $('.clicked-workingplace').val();
		
		if(val != "")
		{
			setupWorkingPlace();
			$(".WorkingPlaceEdit").show();
			$("#WorkingPlaceEditview"+val).show();
		}
	}
	
	function WorkingPlaceDelete()
	{
		var val = $('.clicked-workingplace').val();
		if(val != "")
		{
			setupWorkingPlace();
			$(".WorkingPlaceDelete").show();
			$("#workingplaceid").val(val);
			
		}
	}
	
	function setupWorkingPlace()
	{
		$(".WorkingPlaceAdd").hide();
		$(".WorkingPlaceEdit").hide();
		$(".WorkingPlaceEditEdit").hide();
		$(".WorkingPlaceDelete").hide();
		$(".listWorkingPlace").hide();
	}	
	
//	RequiredExperiance
	function RequiredExperianceAdd()
	{
		setupRequiredExperiance();
		$(".RequiredExperianceAdd").show();
	}
	
	function RequiredExperianceCancel()
	{
		setupRequiredExperiance();
		$(".listRequiredExperiance").show();
	}
	
	
	function RequiredExperianceEdit()
	{
		var val = $('.clicked-requiredexperiance').val();
		
		if(val != "")
		{
			setupRequiredExperiance();
			$(".RequiredExperianceEdit").show();
			$("#RequiredExperianceEditview"+val).show();
		}
	}
	
	function RequiredExperianceDelete()
	{
		var val = $('.clicked-requiredexperiance').val();
		if(val != "")
		{
			setupRequiredExperiance();
			$(".RequiredExperianceDelete").show();
			$("#requiredexperianceid").val(val);
			
		}
	}
	
	
	function setupRequiredExperiance()
	{
		$(".RequiredExperianceAdd").hide();
		$(".RequiredExperianceEdit").hide();
		$(".RequiredExperianceEditEdit").hide();
		$(".RequiredExperianceDelete").hide();
		$(".listRequiredExperiance").hide();
	}	
	
	
//	RequiredDiploma
	function RequiredDiplomaAdd()
	{
		setupRequiredDiploma();
		$(".RequiredDiplomaAdd").show();
	}
	
	function RequiredDiplomaCancel()
	{
		setupRequiredDiploma();
		$(".listRequiredDiploma").show();
	}
	
	
	function RequiredDiplomaEdit()
	{
		var val = $('.clicked-requireddiploma').val();
		
		if(val != "")
		{
			setupRequiredDiploma();
			$(".RequiredDiplomaEdit").show();
			$("#RequiredDiplomaEditview"+val).show();
		}
	}
	
	function RequiredDiplomaDelete()
	{
		var val = $('.clicked-requireddiploma').val();
		if(val != "")
		{
			setupRequiredDiploma();
			$(".RequiredDiplomaDelete").show();
			$("#requireddiplomaid").val(val);
			
		}
	}
	
	function setupRequiredDiploma()
	{
		$(".RequiredDiplomaAdd").hide();
		$(".RequiredDiplomaEdit").hide();
		$(".RequiredDiplomaEditEdit").hide();
		$(".RequiredDiplomaDelete").hide();
		$(".listRequiredDiploma").hide();
	}	
	
//	RequiredDocument
	function RequiredDocumentAdd()
	{
		setupRequiredDocument();
		$(".RequiredDocumentAdd").show();
	}
	
	function RequiredDocumentCancel()
	{
		setupRequiredDocument();
		$(".listRequiredDocument").show();
	}
	
	function RequiredDocumentEdit()
	{
		var val = $('.clicked-requireddocument').val();
		
		if(val != "")
		{
			setupRequiredDocument();
			$(".RequiredDocumentEdit").show();
			$("#RequiredDocumentEditview"+val).show();
		}
	}
	
	function RequiredDocumentDelete()
	{
		var val = $('.clicked-requireddocument').val();
		if(val != "")
		{
			setupRequiredDocument();
			$(".RequiredDiplomaDelete").show();
			$("#requireddocumentid").val(val);
			
		}
	}
	
	function setupRequiredDocument()
	{
		$(".RequiredDocumentAdd").hide();
		$(".RequiredDocumentEdit").hide();
		$(".RequiredDocumentEditEdit").hide();
		$(".RequiredDocumentDelete").hide();
		$(".listRequiredDocument").hide();
	}	
	
//	JobCategory
	function JobCategoryAdd()
	{
		setupJobCategory();
		$(".JobCategoryAdd").show();
	}
	
	function JobCategoryCancel()
	{
		setupJobCategory();
		$(".listJobCategory").show();
	}
	
	
	function JobCategoryEdit()
	{
		var val = $('.clicked-jobcategory').val();
		
		if(val != "")
		{
			setupJobCategory();
			$(".JobCategoryEdit").show();
			$("#JobCategoryEditview"+val).show();
		}
	}
	
	function JobCategoryDelete()
	{
		var val = $('.clicked-jobcategory').val();
		if(val != "")
		{
			setupJobCategory();
			$(".JobCategoryDelete").show();
			$("#jobcategoryid").val(val);
			
		}
	}
	
	
	function setupJobCategory()
	{
		$(".JobCategoryAdd").hide();
		$(".JobCategoryEdit").hide();
		$(".JobCategoryEditEdit").hide();
		$(".JobCategoryDelete").hide();
		$(".listJobCategory").hide();
	}	
	
	
//	Childrens
	function ChildrensAdd()
	{
		setupChildrens();
		$(".ChildrensAdd").show();
	}
	
	function ChildrensCancel()
	{
		setupChildrens();
		$(".listChildrens").show();
	}
	
	function setupChildrens()
	{
		$(".ChildrensAdd").hide();
		$(".ChildrensEdit").hide();
		$(".ChildrensDelete").hide();
		$(".listChildrens").hide();
	}	
	
	
//	Car
	function CarAdd()
	{
		setupCar();
		$(".CarAdd").show();
	}
	
	function CarCancel()
	{
		setupCar();
		$(".listCar").show();
	}
	
	function setupCar()
	{
		$(".CarAdd").hide();
		$(".CarEdit").hide();
		$(".CarDelete").hide();
		$(".listCar").hide();
	}	
	
//	SituatonProfessionnelle
	function SituatonProfessionnelleAdd()
	{
		setupSituatonProfessionnelle();
		$(".SituatonProfessionnelleAdd").show();
	}
	
	function SituatonProfessionnelleCancel()
	{
		setupSituatonProfessionnelle();
		$(".listSituatonProfessionnelle").show();
	}
	
	function setupSituatonProfessionnelle()
	{
		$(".SituatonProfessionnelleAdd").hide();
		$(".SituatonProfessionnelleEdit").hide();
		$(".SituatonProfessionnelleDelete").hide();
		$(".listSituatonProfessionnelle").hide();
	}	
	
//	SituationFamiliate
	function SituationFamiliateAdd()
	{
		setupSituationFamiliate();
		$(".SituationFamiliateAdd").show();
	}
	
	function SituationFamiliateCancel()
	{
		setupSituationFamiliate();
		$(".listSituationFamiliate").show();
	}
	
	function setupSituationFamiliate()
	{
		$(".SituationFamiliateAdd").hide();
		$(".SituationFamiliateEdit").hide();
		$(".SituationFamiliateDelete").hide();
		$(".listSituationFamiliate").hide();
	}	
	
//	JobsCancel
	function JobsAdd()
	{
		setupJobs();
		$(".JobsAdd").show();
	}
	
	function JobsCancel()
	{
		setupJobs();
		$(".listJobs").show();
	}
	
	
	function JobsEdit()
	{
		var val = $('.clicked-mainjob').val();
		
		if(val != "")
		{
			setupJobs();
			$(".JobsEdit").show();
			$("#JobsEditview"+val).show();
		}
	}
	
	function JobsDelete()
	{
		var val = $('.clicked-mainjob').val();
		if(val != "")
		{
			setupJobs();
			$(".JobsDelete").show();
			$("#mainjobid").val(val);
			
		}
	}
	
	
	function setupJobs()
	{
		$(".JobsAdd").hide();
		$(".JobsEdit").hide();
		$(".JobsEditEdit").hide();
		$(".JobsDelete").hide();
		$(".listJobs").hide();
	}	
	
	
$(document).ready(function(){
	
	
		$('input.chk-typeofcontract-template').on('change', function() {
			$('input.chk-typeofcontract-template').not(this).prop('checked', false);
			var id = $(this).attr('data-input');
			$('.clicked-typeofcontract').val(id);
		});
		
		$('input.chk-hourspermonth-template').on('change', function() {
			$('input.chk-hourspermonth-template').not(this).prop('checked', false);
			var id = $(this).attr('data-input');
			$('.clicked-hourspermonth').val(id);
		});
		
		$('input.chk-job-template').on('change', function() {
			$('input.chk-job-template').not(this).prop('checked', false);
			var id = $(this).attr('data-input');
			$('.clicked-job').val(id);
		});
		
		$('input.chk-natureofcontract-template').on('change', function() {
			$('input.chk-natureofcontract-template').not(this).prop('checked', false);
			var id = $(this).attr('data-input');
			$('.clicked-natureofcontract').val(id);
		});
		
		$('input.chk-workingplace-template').on('change', function() {
			$('input.chk-workingplace-template').not(this).prop('checked', false);
			var id = $(this).attr('data-input');
			$('.clicked-workingplace').val(id);
		});
		
		$('input.chk-requiredexperiance-template').on('change', function() {
			$('input.chk-requiredexperiance-template').not(this).prop('checked', false);
			var id = $(this).attr('data-input');
			$('.clicked-requiredexperiance').val(id);
		});
		
		$('input.chk-requireddiploma-template').on('change', function() {
			$('input.chk-requireddiploma-template').not(this).prop('checked', false);
			var id = $(this).attr('data-input');
			$('.clicked-requireddiploma').val(id);
		});
		
		$('input.chk-requireddocument-template').on('change', function() {
			$('input.chk-requireddocument-template').not(this).prop('checked', false);
			var id = $(this).attr('data-input');
			$('.clicked-requireddocument').val(id);
		});
		
		$('input.chk-jobcategory-template').on('change', function() {
			$('input.chk-jobcategory-template').not(this).prop('checked', false);
			var id = $(this).attr('data-input');
			$('.clicked-jobcategory').val(id);
		});
		
		
		$('input.chk-statut-template').on('change', function() {
			$('input.chk-statut-template').not(this).prop('checked', false);
			var id = $(this).attr('data-input');
			$('.clicked-statut').val(id);
		});
		
		$('input.chk-mainjob-template').on('change', function() {
			$('input.chk-mainjob-template').not(this).prop('checked', false);
			var id = $(this).attr('data-input');
			$('.clicked-mainjob').val(id);
		});
		
		
});
		
	
	
</script>


<link href="<?=base_url();?>assets/css/timepicki.css" rel="stylesheet">
<script src="<?=base_url();?>assets/css/timepicki.js"></script>

<style type="text/css">
	.timepicker_wrap 
	{
				width: 124px !important;
				/*left:-40px !important;*/
				top:35px !important;
				
	}
</style>

<style>
	.ms-options-wrap button {
		width: 100% !important;
		border-color: #fff #fff #fff #fff;
		border-color: #bfbfbf;
		text-shadow: 0 0px 0 rgba(255, 255, 255, 0.7);
		color: #555555;
		background-color: #fff;
		-webkit-box-shadow: inset 0 0px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
		-moz-box-shadow: inset 0 0px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
		box-shadow: inset 0 0px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
		background: transparent linear-gradient(#ffffff, #ffffff, #ffffff) repeat scroll 0% 0%;
		background-color: transparent;
		background-image: #ffffff;
		height: 34px;
		font-size: 17px;
	}
	.ms-options ul
	{
		margin-left: -16px;
		background: #fff !important;
	}
	.mynav_2{
	 background: linear-gradient(to bottom, #fbfbfb 0%, #ececec 39%, #ececec 39%, #c1c1c1 100%);    margin-bottom: 15px;   
	}
</style>


<div class="col-md-12"><!--col-md-10 padding white right-p-->
    <div class="content">
	<input type="hidden" value="<?=date('d-m-Y')?>" id="CurrentDate">
	<input type="hidden" value="<?=date('H:i')?>" id="CurrentTime">
        <div class="col-md-12">
            <div id="breadcrumb">
            <a href="<?php echo base_url();?>admin/dashboard" class="tip-bottom" data-original-title="Go to Home"><i class="fa fa-home"></i> Home / </a> 
            <?php echo $title?>
        </div>
        </div>
        
            <div class="collapse navbar-collapse res-menu">
                <ul class="nav navbar-nav menu mynav_2" style="">
					<li><a href="#statut" role="tab" data-toggle="tab">Statut</a></li>

					<li><a href="#Category" role="tab" data-toggle="tab">Category</a></li>
					<li><a href="#Destination" role="tab" data-toggle="tab">Destination</a></li>
					<li><a href="#Document" role="tab" data-toggle="tab">Document</a></li>
					<li><a href="#Citys" role="tab" data-toggle="tab">Citys</a></li>
					<li><a href="#Signature" role="tab" data-toggle="tab">Signatures</a></li>
					<li><a href="#Departments" role="tab" data-toggle="tab">Departments</a></li>
					<li><a href="#Managers" role="tab" data-toggle="tab">Managers</a></li>
                </ul>
                 <div class="tab-content responsive" >
				
					 
					 
					<div class="tab-pane fade in  active" id="statut"> 
						<div class="listStatus" >
								<div class="filter-group">
								    <div class="col-md-12" style="padding-left: 0px;">
										<div class="page-action" style="float: right">
											<a class="btn btn-default" onclick="StatusAdd()"><span class="add-icon"> Add</span></a>&nbsp;
											<a class="btn btn-default" onclick="StatusEdit()"><span class="edit-icon"> Edit</span></a>&nbsp;
											<a class="btn btn-default" onclick="StatusDelete()"><span class="delete-icon">Delete</a>&nbsp;
										</div>
									</div>
								</div>
								<table class="table table-bordered data-table dataTable">
									<input type="hidden" class="clicked-statut" value="">
									<thead>
										<tr>
											<th></th>
											<th>ID</th>
											<th>Date</th>
											<th>Time</th>
											<th>Name</th>
											<th>Status</th>
											<th>Since</th>
										</tr>
									</thead>
									<tbody>
										<?php
											foreach ($doc_statut as $data) {
												$id = create_timestamp_uid($data->create_date, $data->id);
												$create_date = date('d/m/Y', strtotime($data->create_date));
	            								$create_time = date('H:i:s', strtotime($data->create_date)); 
            									$since = timeDiff($data->create_date);
										?>
											<tr>
												<td><input class="chk-statut-template" data-input="<?=$data->id?>" type="checkbox"></td>
												<td><?=$id?></td>
												<td><?=$create_date?></td>
												<td><?=$create_time?></td>
												<td><?=$data->doc_status?></td>
												<td><?=$data->status?></td>
												<td><?=$since?></td>
											</tr>
										<?php }
										?>
									</tbody>
								</table>
							</div>
			

					
						
					<div class="col-md-12 StatusDelete" style="border:1px solid #ccc;padding: 0px; display: none;">
						<form method="post" class="DeleteAccountRecord" action="" style="padding: 30px;">
							<input  name="tablename" value="document_status" type="hidden" >
							<input type="hidden" id="statutid" name="id" value="">
							<div style="display: inline-block;float:left;margin-right: 10px;"> Are you sure you want to delete selected?</div>
							<img class="displayimageloader" style="float: left; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
							<input class="btn" type="submit" id="search_by" value="Yes" style="float:left;width: 50px;margin-top: -11px;margin-right: 10px;" />
							<a class="btn" style="margin-top: -11px;cursor: pointer;" onclick="StatusCancel()">No</a>
						</form>
					</div>	
					
						
					<div class="StatusEdit" style="display: none;">	
					<?php
					foreach ($doc_statut as $statut)
					{ ?>
						<div id="StatusEditview<?=$statut->id?>" class="col-md-12 StatusEditEdit" style="display: none;">
							<form  class="SaveEditForm" method="post" style="width: 100%;background-image: none;" enctype="multipart/form-data">
								<div class="row" style="margin-top: 10px;">
									<div class="col-md-3">
										<div class="form-group">
											<div class="col-md-4" style="margin-top: 10px;" >
												<span style="font-weight: bold;">Name</span><br />
												<span style="font-weight: bold;">Status</span>
											</div>
											<div class="col-md-8" style="padding: 0px;">
												<input required="" class="form-control" value="<?=$statut->doc_status?>" name="fieldvalue" type="text" >
												<select name="status" class="form-control">
													<option value="Active" <?php if($statut->status == 'Active') echo 'selected'; ?>>Active</option>
													<option value="Inactive" <?php if($statut->status == 'Inactive') echo 'selected'; ?>>Inactive</option>
												</select>
												<input  name="tablename" value="vbs_document_status" type="hidden" >
												<input  name="fieldname" value="doc_status" type="hidden" >
												<input  name="fieldname2" value="status" type="hidden" >
												<input type="hidden" value="<?=$statut->id?>" name="id">
											</div>
										</div>
									</div>
								</div>

								<div class="row" style="margin-bottom: 10px;margin-top: 20px;">
									<div class="col-md-3">
										<a class="btn" style="float: right;cursor: pointer;"  onclick="StatusCancel()"><span class="delete-icon" >Cancel</span></a>
										<button style="padding: 3px 13px;margin-right: 10px;float:right;" type="submit" class="btn"><span class="save-icon"></span> Save</button>
										<img class="displayimageloader" style="float: right; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
									</div>
								</div>

							</form>
						</div>
					<?php
					}
					?>
					</div>
						
						
						<div class="col-md-12 StatusAdd" style="border:1px solid #ccc;padding-left:3px;margin-bottom: 10px;display: none; ">
						  <form action="" class="ConfigServiceForm" method="post" style="width: 100%;background-image: none;">

									<div class="row" style="margin-top: 10px;">
										
										<div class="col-md-3">
											<div class="form-group">
												<div class="col-md-4" style="margin-top: 10px;" >
													<span style="font-weight: bold;">Name</span><br/>
													<span style="font-weight: bold;">Status</span>
												</div>
												<div class="col-md-8" style="padding: 0px;">
													<input required="" class="form-control" name="fieldvalue" type="text" >
													<select name="status" class="form-control">
														<option value="Active">Active</option>
														<option value="Inactive">Inactive</option>
													</select>
													<input  name="tablename" value="vbs_document_status" type="hidden" >
													<input  name="fieldname" value="doc_status" type="hidden" >
													<input  name="fieldname2" value="status" type="hidden" >
												</div>

											</div>
										</div>
										
							

									</div>


								<div class="row" style="margin-bottom: 10px;margin-top: 20px;">
									<div class="col-md-3">
										<a class="btn" style="float: right;cursor: pointer;" onclick="StatusCancel()"><span class="delete-icon" >Cancel</span></a>
										<button style="padding: 3px 13px;margin-right: 10px;float:right;" type="submit" class="btn"><span class="save-icon"></span> Save</button>
										<img class="displayimageloader" style="float: right; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
									</div>
								</div>

							</form>

						</div>
                    </div>
               
					<div class="tab-pane fade" id="Category"> 
						<div class="listTypeofContract" >
								<div class="filter-group">
								 <!--  <div class="col-md-8" style="padding-left: 0px;">
									  <form  method="get" action="" style="padding-top: 0px;background: none;" >
											<span style="float: left;padding: 3px;padding-top: 10px">Search keyword</span>
												<input type="text" name="searchword"  class="form-control" style="width: 20%;float:left; "  value="<?=!empty($this->input->get('searchword'))?$this->input->get('searchword'):''?>" />
											<span style="float: left;padding: 3px;padding-top: 10px">From</span>
												<input type="text" name="from_period" class="datepicker" style="width: 13%;float:left;height: 34px; "  value="<?=!empty($this->input->get('from_period'))?$this->input->get('from_period'):''?>" /> 
											<span style="float: left;padding: 3px;padding-top: 10px">To</span>
												<input type="text" name="to_period" class="datepicker" style="width: 13%;float:left;height: 34px; " value="<?=!empty($this->input->get('to_period'))?$this->input->get('to_period'):''?>"/>
											<input class="btn" type="submit"  value="Search" style="float:left;width: 8%;" />
											<a class="btn" style="float: left" href="<?=base_url()?>admin/quotes.php"> Reset </a>
										</form>
									</div> -->

								    <div class="col-md-12" style="padding-left: 0px;">
										<div class="page-action" style="float: right">
											<a class="btn btn-default" onclick="TypeofContractAdd()"><span class="add-icon"> Add</span></a>&nbsp;
											<a class="btn btn-default" onclick="TypeofContractEdit()"><span class="edit-icon"> Edit</span></a>&nbsp;
											<a class="btn btn-default" onclick="TypeofContractDelete()"><span class="delete-icon">Delete</a>&nbsp;
										</div>
									</div>
								</div>
								<table class="table table-bordered data-table dataTable">
									<input type="hidden" class="clicked-typeofcontract" value="">
									<thead>
										<tr>
											<th></th>
											<th>ID</th>
											<th>Date</th>
											<th>Time</th>
											<th>Category</th>
											<th>Status</th>
											<th>Since</th>
										</tr>
									</thead>
									<tbody>
									<?php
										foreach ($doc_category as $data)
										{ 

											$id = create_timestamp_uid($data->create_date, $data->id);
											$create_date = date('d/m/Y', strtotime($data->create_date));
            								$create_time = date('H:i:s', strtotime($data->create_date)); 
        									$since = timeDiff($data->create_date);
									?>
											<tr>
												<td><input class="chk-typeofcontract-template" data-input="<?=$data->id?>" type="checkbox"></td>
												<td><?=$id?></td>
												<td><?=$create_date?></td>
												<td><?=$create_time?></td>
												<td><?=$data->doc_category?></td>
												<td><?=$data->status?></td>
												<td><?=$since?></td>
											</tr>
										<?php }
										?>
									</tbody>
								</table>
							</div>
			

						
						
						<div class="col-md-12 TypeofContractDelete" style="border:1px solid #ccc;padding: 0px; display: none;">
							<form method="post" class="DeleteAccountRecord" action="" style="padding: 30px;">
								<input  name="tablename" value="document_category" type="hidden" >
								<input type="hidden" id="typeofcontractid" name="id" value="">
								<div style="display: inline-block;float:left;margin-right: 10px;"> Are you sure you want to delete selected?</div>
								<img class="displayimageloader" style="float: left; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
								<input class="btn" type="submit" id="search_by" value="Yes" style="float:left;width: 50px;margin-top: -11px;margin-right: 10px;" />
								<a class="btn" href="#" style="margin-top: -11px;" onclick="TypeofContractCancel()">No</a>
							</form>
						</div>
						
						
					<div class="TypeofContractEdit" style="display: none;">	
					<?php
					
					foreach ($doc_category as $data)
					{
					?>
						<div id="TypeofContractEditview<?=$data->id?>" class="col-md-12 EditTypeofContractEdit" style="display: none;">
							<form  class="SaveEditForm" method="post" style="width: 100%;background-image: none;" enctype="multipart/form-data">
								
								<div class="row" style="margin-top: 10px;">
									<div class="col-md-3">
										<div class="form-group">
											<div class="col-md-4" style="margin-top: 10px;" >
												<span style="font-weight: bold;">Type of Contract</span><br />
												<span style="font-weight: bold;">Status</span>
											</div>
											<div class="col-md-8" style="padding: 0px;">
												<input required="" class="form-control" value="<?=$data->doc_category?>" name="fieldvalue" type="text" >
												<select name="status" class="form-control">
													<option value="Active" <?php if($data->status == 'Active') echo 'selected'; ?>>Active</option>
													<option value="Inactive" <?php if($data->status == 'Inactive') echo 'selected'; ?>>Inactive</option>
												</select>
												<input  name="tablename" value="document_category" type="hidden" >
												<input  name="fieldname" value="doc_category" type="hidden" >
												<input  name="fieldname2" value="status" type="hidden" >
												<input type="hidden" value="<?=$data->id?>" name="id">
											</div>
										</div>
									</div>
								</div>

								<div class="row" style="margin-bottom: 10px;margin-top: 20px;">
									<div class="col-md-3">
										<a class="btn" style="float: right;" href="#" onclick="TypeofContractCancel()"><span class="delete-icon" >Cancel</span></a>
										<button style="padding: 3px 13px;margin-right: 10px;float:right;" type="submit" class="btn"><span class="save-icon"></span> Save</button>
										<img class="displayimageloader" style="float: right; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
									</div>
								</div>

							</form>
						</div>
					<?php
					}
					?>
					</div>


						<div class="col-md-12 TypeofContractAdd" style="border:1px solid #ccc;padding-left:3px;margin-bottom: 10px;display: none; ">
						<form action="" class="ConfigServiceForm" method="post" style="width: 100%;background-image: none;">
									<div class="row" style="margin-top: 10px;">
										<div class="col-md-3">
											<div class="form-group">
												<div class="col-md-4" style="margin-top: 10px;" >
													<span style="font-weight: bold;">Category</span><br />
													<span style="font-weight: bold;">Status</span>
												</div>
												<div class="col-md-8" style="padding: 0px;">
													<input required="" class="form-control" name="fieldvalue" type="text" >
													<select name="status" class="form-control">
														<option value="Active">Active</option>
														<option value="Inactive">Inactive</option>
													</select>
													<input  name="tablename" value="document_category" type="hidden" >
													<input  name="fieldname" value="doc_category" type="hidden" >
													<input  name="fieldname2" value="status" type="hidden" >
												</div>

											</div>
										</div>
									</div>


								<div class="row" style="margin-bottom: 10px;margin-top: 20px;">
									<div class="col-md-3">
										<a class="btn" style="float: right;" href="#" onclick="TypeofContractCancel()"><span class="delete-icon" >Cancel</span></a>
										<button style="padding: 3px 13px;margin-right: 10px;float:right;" type="submit" class="btn"><span class="save-icon"></span> Save</button>
										<img class="displayimageloader" style="float: right; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
									</div>
								</div>

							</form>

						</div>
                    </div>
					
					<div class="tab-pane fade" id="Destination"> 
						<div class="listHoursPerMonth" >
								<div class="filter-group">
								  <!-- <div class="col-md-8" style="padding-left: 0px;">
									  <form  method="get" action="" style="padding-top: 0px;background: none;" >
											<span style="float: left;padding: 3px;padding-top: 10px">Search keyword</span>
												<input type="text" name="searchword"  class="form-control" style="width: 20%;float:left; "  value="<?=!empty($this->input->get('searchword'))?$this->input->get('searchword'):''?>" />
											<span style="float: left;padding: 3px;padding-top: 10px">From</span>
												<input type="text" name="from_period" class="datepicker" style="width: 13%;float:left;height: 34px; "  value="<?=!empty($this->input->get('from_period'))?$this->input->get('from_period'):''?>" /> 
											<span style="float: left;padding: 3px;padding-top: 10px">To</span>
												<input type="text" name="to_period" class="datepicker" style="width: 13%;float:left;height: 34px; " value="<?=!empty($this->input->get('to_period'))?$this->input->get('to_period'):''?>"/>
											<input class="btn" type="submit"  value="Search" style="float:left;width: 8%;" />
											<a class="btn" style="float: left" href="<?=base_url()?>admin/quotes.php"> Reset </a>
										</form>
									</div> -->

									<!-- <div class="col-md-4">
										<div class="page-action"> -->
								    <div class="col-md-12" style="padding-left: 0px;">
										<div class="page-action" style="float: right">
											<a class="btn btn-default" onclick="HoursPerMonthAdd()"><span class="add-icon"> Add</span></a>&nbsp;
											<a class="btn btn-default" onclick="HoursPerMonthEdit()"><span class="edit-icon"> Edit</span></a>&nbsp;
											<a class="btn btn-default" onclick="HoursPerMonthDelete()"><span class="delete-icon">Delete</a>&nbsp;
										</div>
									</div>
								</div>
								<table class="table table-bordered data-table dataTable">
									<input type="hidden" class="clicked-hourspermonth" value="">
									<thead>
										<tr>
											<th></th>
											<th>ID</th>
											<th>Date</th>
											<th>Time</th>
											<th>Destination</th>
											<th>Status</th>
											<th>Since</th>
										</tr>
									</thead>
									<tbody>
									<?php
										foreach ($doc_destination as $data)
										{ 
											$id = create_timestamp_uid($data->create_date, $data->id);
											$create_date = date('d/m/Y', strtotime($data->create_date));
            								$create_time = date('H:i:s', strtotime($data->create_date)); 
        									$since = timeDiff($data->create_date);
									?>
											<tr>
												<td><input class="chk-hourspermonth-template" data-input="<?=$data->id?>" type="checkbox"></td>
												<td><?=$id?></td>
												<td><?=$create_date?></td>
												<td><?=$create_time?></td>
												<td><?=$data->doc_destination?></td>
												<td><?=$data->status?></td>
												<td><?=$since?></td>
											</tr>
										<?php }
										?>
									</tbody>
								</table>
							</div>
			

							
						<div class="col-md-12 HoursPerMonthDelete" style="border:1px solid #ccc;padding: 0px; display: none;">
							<form method="post" class="DeleteAccountRecord" action="" style="padding: 30px;">
								<input  name="tablename" value="document_destination" type="hidden" >
								<input type="hidden" id="hourspermonthid" name="id" value="">
								<div style="display: inline-block;float:left;margin-right: 10px;"> Are you sure you want to delete selected?</div>
								<img class="displayimageloader" style="float: left; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
								<input class="btn" type="submit" id="search_by" value="Yes" style="float:left;width: 50px;margin-top: -11px;margin-right: 10px;" />
								<a class="btn" style="margin-top: -11px;cursor: pointer;" onclick="HoursPerMonthCancel()">No</a>
							</form>
						</div>

						
					<div class="HoursPerMonthEdit" style="display: none;">	
					<?php
					foreach ($doc_destination as $data)
					{ ?>
						<div id="HoursPerMonthEditview<?=$data->id?>" class="EditHoursPerMonthEdit" style="display: none;">
							<form  class="SaveEditForm" method="post" style="width: 100%;background-image: none;" enctype="multipart/form-data">
								
								<div class="row" style="margin-top: 10px;">
									<div class="col-md-3">
										<div class="form-group">
											<div class="col-md-4" style="margin-top: 10px;" >
												<span style="font-weight: bold;">Destination</span><br />
												<span style="font-weight: bold;">Status</span>
											</div>
											<div class="col-md-8" style="padding: 0px;">
												<input required="" class="form-control" value="<?=$data->doc_destination?>" name="fieldvalue" type="text" >
												<select name="status" class="form-control">
													<option value="Active" <?php if($data->status == 'Active') echo 'selected'; ?>>Active</option>
													<option value="Inactive" <?php if($data->status == 'Inactive') echo 'selected'; ?>>Inactive</option>
												</select>
												<input  name="tablename" value="document_destination" type="hidden" >
												<input  name="fieldname" value="doc_destination" type="hidden" >
												<input  name="fieldname2" value="status" type="hidden" >
												<input type="hidden" value="<?=$data->id?>" name="id">
											</div>
										</div>
									</div>
								</div>

								<div class="row" style="margin-bottom: 10px;margin-top: 20px;">
									<div class="col-md-3">
										<a class="btn" style="float: right;cursor: pointer;"  onclick="HoursPerMonthCancel()"><span class="delete-icon" >Cancel</span></a>
										<button style="padding: 3px 13px;margin-right: 10px;float:right;" type="submit" class="btn"><span class="save-icon"></span> Save</button>
										<img class="displayimageloader" style="float: right; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
									</div>
								</div>

							</form>
						</div>
					<?php
					}
					?>
					</div>

						<div class="col-md-12 HoursPerMonthAdd" style="border:1px solid #ccc;padding-left:3px;margin-bottom: 10px;display: none; ">
						   <form action="" class="ConfigServiceForm" method="post" style="width: 100%;background-image: none;">

									<div class="row" style="margin-top: 10px;">
										
										<div class="col-md-3">
											<div class="form-group">
												<div class="col-md-4" style="margin-top: 10px;" >
													<span style="font-weight: bold;">Destination</span><br />
													<span style="font-weight: bold;">Status</span>
												</div>
												<div class="col-md-8" style="padding: 0px;">
													<input required="" class="form-control" name="fieldvalue" type="text" >
													<select name="status" class="form-control">
														<option value="Active">Active</option>
														<option value="Inactive">Inactive</option>
													</select>
													<input  name="tablename" value="document_destination" type="hidden" >
													<input  name="fieldname" value="doc_destination" type="hidden" >
													<input  name="fieldname2" value="status" type="hidden" >
												</div>

											</div>
										</div>
										
							

									</div>


								<div class="row" style="margin-bottom: 10px;margin-top: 20px;">
									<div class="col-md-3">
										<a class="btn" style="float: right;" href="#" onclick="HoursPerMonthCancel()"><span class="delete-icon" >Cancel</span></a>
										<button style="padding: 3px 13px;margin-right: 10px;float:right;" type="submit" class="btn"><span class="save-icon"></span> Save</button>
										<img class="displayimageloader" style="float: right; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
									</div>
								</div>

							</form>

						</div>
                    </div>
					
					<div class="tab-pane fade" id="Document"> 
						<div class="listJob" >
								<div class="filter-group">
								  <!-- <div class="col-md-8" style="padding-left: 0px;">
									  <form  method="get" action="" style="padding-top: 0px;background: none;" >
											<span style="float: left;padding: 3px;padding-top: 10px">Search keyword</span>
												<input type="text" name="searchword"  class="form-control" style="width: 20%;float:left; "  value="<?=!empty($this->input->get('searchword'))?$this->input->get('searchword'):''?>" />
											<span style="float: left;padding: 3px;padding-top: 10px">From</span>
												<input type="text" name="from_period" class="datepicker" style="width: 13%;float:left;height: 34px; "  value="<?=!empty($this->input->get('from_period'))?$this->input->get('from_period'):''?>" /> 
											<span style="float: left;padding: 3px;padding-top: 10px">To</span>
												<input type="text" name="to_period" class="datepicker" style="width: 13%;float:left;height: 34px; " value="<?=!empty($this->input->get('to_period'))?$this->input->get('to_period'):''?>"/>
											<input class="btn" type="submit"  value="Search" style="float:left;width: 8%;" />
											<a class="btn" style="float: left" href="<?=base_url()?>admin/quotes.php"> Reset </a>
										</form>
									</div> -->

								    <div class="col-md-12" style="padding-left: 0px;">
										<div class="page-action" style="float: right">
											<a class="btn btn-default" onclick="JobAdd()"><span class="add-icon"> Add</span></a>&nbsp;
											<a class="btn btn-default" onclick="JobEdit()"><span class="edit-icon"> Edit</span></a>&nbsp;
											<a class="btn btn-default" onclick="JobDelete()"><span class="delete-icon">Delete</a>&nbsp;
										</div>
									</div>
								</div>
								<table class="table table-bordered data-table dataTable">
									<input type="hidden" class="clicked-job" value="">
									<thead>
										<tr>
											<th></th>
											<th>ID</th>
											<th>Date</th>
											<th>Time</th>
											<th>Document Name</th>
											<th>Status</th>
											<th>Since</th>
										</tr>
									</thead>
									<tbody>
										<?php
											foreach ($document_docs as $data) {
												$id = create_timestamp_uid($data->create_date, $data->id);
												$create_date = date('d/m/Y', strtotime($data->create_date));
	            								$create_time = date('H:i:s', strtotime($data->create_date)); 
            									$since = timeDiff($data->create_date);
										?>
											<tr>
												<td><input class="chk-job-template" data-input="<?=$data->id?>" type="checkbox"></td>
												<td><?=$id?></td>
												<td><?=$create_date?></td>
												<td><?=$create_time?></td>
												<td><?=$data->doc_name?></td>
												<td><?=$data->status?></td>
												<td><?=$since?></td>
											</tr>
										<?php }
										?>
									</tbody>
								</table>
							</div>
			

								
						<div class="col-md-12 JobDelete" style="border:1px solid #ccc;padding: 0px; display: none;">
							<form method="post" class="DeleteAccountRecord" action="" style="padding: 30px;">
								<input  name="tablename" value="document_docs" type="hidden" >
								<input type="hidden" id="jobid" name="id" value="">
								<div style="display: inline-block;float:left;margin-right: 10px;"> Are you sure you want to delete selected?</div>
								<img class="displayimageloader" style="float: left; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
								<input class="btn" type="submit" id="search_by" value="Yes" style="float:left;width: 50px;margin-top: -11px;margin-right: 10px;" />
								<a class="btn" style="margin-top: -11px;cursor: pointer;" onclick="JobCancel()">No</a>
							</form>
						</div>

						
					<div class="JobEdit" style="display: none;">	
					<?php
					foreach ($document_docs as $data)
					{ ?>
						<div id="JobEditview<?=$data->id?>" class="col-md-12 JobEditEdit" style="display: none;">
							<form  class="SaveEditForm" method="post" style="width: 100%;background-image: none;" enctype="multipart/form-data">
								
								<div class="row" style="margin-top: 10px;">
									<div class="col-md-3">
										<div class="form-group">
											<div class="col-md-4" style="margin-top: 10px;" >
												<span style="font-weight: bold;">Document Name</span><br />
												<span style="font-weight: bold;">Status</span>
											</div>
											<div class="col-md-8" style="padding: 0px;">
												<input required="" class="form-control" value="<?=$data->doc_name?>" name="fieldvalue" type="text" >
												<select name="status" class="form-control">
													<option value="Active" <?php if($data->status == 'Active') echo 'selected'; ?>>Active</option>
													<option value="Inactive" <?php if($data->status == 'Inactive') echo 'selected'; ?>>Inactive</option>
												</select>
												<input  name="tablename" value="document_docs" type="hidden" >
												<input  name="fieldname" value="doc_name" type="hidden" >
												<input  name="fieldname2" value="status" type="hidden" >
												<input type="hidden" value="<?=$data->id?>" name="id">
											</div>
										</div>
									</div>
								</div>

								<div class="row" style="margin-bottom: 10px;margin-top: 20px;">
									<div class="col-md-3">
										<a class="btn" style="float: right;cursor: pointer;"  onclick="JobCancel()"><span class="delete-icon" >Cancel</span></a>
										<button style="padding: 3px 13px;margin-right: 10px;float:right;" type="submit" class="btn"><span class="save-icon"></span> Save</button>
										<img class="displayimageloader" style="float: right; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
									</div>
								</div>

							</form>
						</div>
					<?php
					}
					?>
					</div>



						<div class="col-md-12 JobAdd" style="border:1px solid #ccc;padding-left:3px;margin-bottom: 10px;display: none; ">
							<form action="" class="ConfigServiceForm" method="post" style="width: 100%;background-image: none;">
									<div class="row" style="margin-top: 10px;">
										
										<div class="col-md-3">
											<div class="form-group">
												<div class="col-md-4" style="margin-top: 10px;" >
													<span style="font-weight: bold;">Document Name</span><br />
													<span style="font-weight: bold;">Status</span>
												</div>
												<div class="col-md-8" style="padding: 0px;">
													<input required="" class="form-control" name="fieldvalue" type="text" >
													<select name="status" class="form-control">
														<option value="Active">Active</option>
														<option value="Inactive">Inactive</option>
													</select>
													<input  name="tablename" value="document_docs" type="hidden" >
													<input  name="fieldname" value="doc_name" type="hidden" >
													<input  name="fieldname2" value="status" type="hidden" >
												</div>
										</div>
									</div>
									</div>


								<div class="row" style="margin-bottom: 10px;margin-top: 20px;">
									<div class="col-md-3">
										<a class="btn" style="float: right;" href="#" onclick="JobCancel()"><span class="delete-icon" >Cancel</span></a>
										<button style="padding: 3px 13px;margin-right: 10px;float:right;" type="submit" class="btn"><span class="save-icon"></span> Save</button>
										<img class="displayimageloader" style="float: right; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
									</div>
								</div>

							</form>

						</div>
                    </div>
					
					
					<div class="tab-pane fade" id="Citys"> 
						<div class="listNatureofContract" >
								<div class="filter-group">
								  <!-- <div class="col-md-8" style="padding-left: 0px;">
									  <form  method="get" action="" style="padding-top: 0px;background: none;" >
											<span style="float: left;padding: 3px;padding-top: 10px">Search keyword</span>
												<input type="text" name="searchword"  class="form-control" style="width: 20%;float:left; "  value="<?=!empty($this->input->get('searchword'))?$this->input->get('searchword'):''?>" />
											<span style="float: left;padding: 3px;padding-top: 10px">From</span>
												<input type="text" name="from_period" class="datepicker" style="width: 13%;float:left;height: 34px; "  value="<?=!empty($this->input->get('from_period'))?$this->input->get('from_period'):''?>" /> 
											<span style="float: left;padding: 3px;padding-top: 10px">To</span>
												<input type="text" name="to_period" class="datepicker" style="width: 13%;float:left;height: 34px; " value="<?=!empty($this->input->get('to_period'))?$this->input->get('to_period'):''?>"/>
											<input class="btn" type="submit"  value="Search" style="float:left;width: 8%;" />
											<a class="btn" style="float: left" href="<?=base_url()?>admin/quotes.php"> Reset </a>
										</form>
									</div> -->

								    <div class="col-md-12" style="padding-left: 0px;">
										<div class="page-action" style="float: right">
											<a class="btn btn-default" onclick="NatureofContractAdd()"><span class="add-icon"> Add</span></a>&nbsp;
											<a class="btn btn-default" onclick="NatureofContractEdit()"><span class="edit-icon"> Edit</span></a>&nbsp;
											<a class="btn btn-default" onclick="NatureofContractDelete()"><span class="delete-icon">Delete</a>&nbsp;
										</div>
									</div>
								</div>
								<table class="table table-bordered data-table dataTable">
									<input type="hidden" class="clicked-natureofcontract" value="">
									<thead>
										<tr>
											<th></th>
											<th>ID</th>
											<th>Date</th>
											<th>Time</th>
											<th>City </th>
											<th>Status</th>
											<th>Since</th>
										</tr>
									</thead>
									<tbody>
									<?php
										foreach ($doc_cities as $data)
										{ 
											$id = create_timestamp_uid($data->create_date, $data->id);
											$create_date = date('d/m/Y', strtotime($data->create_date));
            								$create_time = date('H:i:s', strtotime($data->create_date)); 
        									$since = timeDiff($data->create_date);
									?>
											<tr>
												<td><input class="chk-natureofcontract-template" data-input="<?=$data->id?>" type="checkbox"></td>
												<td><?=$id?></td>
												<td><?=$create_date?></td>
												<td><?=$create_time?></td>
												<td><?=$data->city?></td>
												<td><?=$data->status?></td>
												<td><?=$since?></td>
											</tr>
										<?php }
										?>
									</tbody>
								</table>
							</div>
			

						<div class="col-md-12 NatureofContractDelete" style="border:1px solid #ccc;padding: 0px; display: none;">
							<form method="post" class="DeleteAccountRecord" action="" style="padding: 30px;">
								<input  name="tablename" value="document_cities" type="hidden" >
								<input type="hidden" id="natureofcontractid" name="id" value="">
								<div style="display: inline-block;float:left;margin-right: 10px;"> Are you sure you want to delete selected?</div>
								<img class="displayimageloader" style="float: left; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
								<input class="btn" type="submit" id="search_by" value="Yes" style="float:left;width: 50px;margin-top: -11px;margin-right: 10px;" />
								<a class="btn" style="margin-top: -11px;cursor: pointer;" onclick="NatureofContractCancel()">No</a>
							</form>
						</div>

						
					<div class="NatureofContractEdit" style="display: none;">	
					<?php
					foreach ($doc_cities as $data)
					{ ?>
						<div id="NatureofContractEditview<?=$data->id?>" class="col-md-12 NatureofContractEditEdit" style="display: none;">
							<form  class="SaveEditForm" method="post" style="width: 100%;background-image: none;" enctype="multipart/form-data">
								
								<div class="row" style="margin-top: 10px;">
									<div class="col-md-3">
										<div class="form-group">
											<div class="col-md-4" style="margin-top: 10px;" >
												<span style="font-weight: bold;">City</span><br />
												<span style="font-weight: bold;">Status</span>
											</div>
											<div class="col-md-8" style="padding: 0px;">
												<input required="" class="form-control" value="<?=$data->city?>" name="fieldvalue" type="text" >
												<select name="status" class="form-control">
													<option value="Active" <?php if($data->status == 'Active') echo 'selected'; ?>>Active</option>
													<option value="Inactive" <?php if($data->status == 'Inactive') echo 'selected'; ?>>Inactive</option>
												</select>
												<input  name="tablename" value="document_cities" type="hidden" >
												<input  name="fieldname" value="city" type="hidden" >
												<input  name="fieldname2" value="status" type="hidden" >
												<input type="hidden" value="<?=$data->id?>" name="id">
											</div>
										</div>
									</div>
								</div>

								<div class="row" style="margin-bottom: 10px;margin-top: 20px;">
									<div class="col-md-3">
										<a class="btn" style="float: right;cursor: pointer;"  onclick="NatureofContractCancel()"><span class="delete-icon" >Cancel</span></a>
										<button style="padding: 3px 13px;margin-right: 10px;float:right;" type="submit" class="btn"><span class="save-icon"></span> Save</button>
										<img class="displayimageloader" style="float: right; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
									</div>
								</div>

							</form>
						</div>
					<?php
					}
					?>
					</div>



						<div class="col-md-12 NatureofContractAdd" style="border:1px solid #ccc;padding-left:3px;margin-bottom: 10px;display: none; ">
						   <form action="" class="ConfigServiceForm" method="post" style="width: 100%;background-image: none;">
								<div class="row" style="margin-top: 10px;">
									<div class="col-md-3">
										<div class="form-group">
											<div class="col-md-4" style="margin-top: 5px;" >
												<span style="font-weight: bold;">City </span><br />
												<span style="font-weight: bold;">Status</span>
											</div>
											<div class="col-md-8" style="padding: 0px;">
												<input required="" class="form-control" name="fieldvalue" type="text" >
												<select name="status" class="form-control">
													<option value="Active">Active</option>
													<option value="Inactive">Inactive</option>
												</select>
												<input  name="tablename" value="document_cities" type="hidden" >
												<input  name="fieldname" value="city" type="hidden" >
												<input  name="fieldname2" value="status" type="hidden" >
											</div>
										</div>
									</div>
								</div>

								<div class="row" style="margin-bottom: 10px;margin-top: 20px;">
									<div class="col-md-3">
										<a class="btn" style="float: right;" href="#" onclick="NatureofContractCancel()"><span class="delete-icon" >Cancel</span></a>
										<button style="padding: 3px 13px;margin-right: 10px;float:right;" type="submit" class="btn"><span class="save-icon"></span> Save</button>
										<img class="displayimageloader" style="float: right; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
									</div>
								</div>

							</form>

						</div>
                    </div>
					
					<div class="tab-pane fade" id="Signature"> 
						<div class="listWorkingPlace" >
								<div class="filter-group">
								  <!-- <div class="col-md-8" style="padding-left: 0px;">
									  <form  method="get" action="" style="padding-top: 0px;background: none;" >
											<span style="float: left;padding: 3px;padding-top: 10px">Search keyword</span>
												<input type="text" name="searchword"  class="form-control" style="width: 20%;float:left; "  value="<?=!empty($this->input->get('searchword'))?$this->input->get('searchword'):''?>" />
											<span style="float: left;padding: 3px;padding-top: 10px">From</span>
												<input type="text" name="from_period" class="datepicker" style="width: 13%;float:left;height: 34px; "  value="<?=!empty($this->input->get('from_period'))?$this->input->get('from_period'):''?>" /> 
											<span style="float: left;padding: 3px;padding-top: 10px">To</span>
												<input type="text" name="to_period" class="datepicker" style="width: 13%;float:left;height: 34px; " value="<?=!empty($this->input->get('to_period'))?$this->input->get('to_period'):''?>"/>
											<input class="btn" type="submit"  value="Search" style="float:left;width: 8%;" />
											<a class="btn" style="float: left" href="<?=base_url()?>admin/quotes.php"> Reset </a>
										</form>
									</div> -->

								    <div class="col-md-12" style="padding-left: 0px;">
										<div class="page-action" style="float: right">
											<a class="btn btn-default" onclick="WorkingPlaceAdd()"><span class="add-icon"> Add</span></a>&nbsp;
											<a class="btn btn-default" onclick="WorkingPlaceEdit()"><span class="edit-icon"> Edit</span></a>&nbsp;
											<a class="btn btn-default" onclick="WorkingPlaceDelete()"><span class="delete-icon">Delete</a>&nbsp;
										</div>
									</div>
								</div>
								<table class="table table-bordered data-table dataTable">
									<input type="hidden" class="clicked-workingplace" value="">
									<thead>
										<tr>
											<th></th>
											<th>ID</th>
											<th>Date</th>
											<th>Time</th>
											<th>Signatures</th>
											<th>Status</th>
											<th>Since</th>
										</tr>
									</thead>
									<tbody>
									<?php
										foreach ($doc_signature as $data)
										{ 
											$id = create_timestamp_uid($data->create_date, $data->id);
											$create_date = date('d/m/Y', strtotime($data->create_date));
            								$create_time = date('H:i:s', strtotime($data->create_date)); 
        									$since = timeDiff($data->create_date);
									?>
											<tr>
												<td><input class="chk-workingplace-template" data-input="<?=$data->id?>" type="checkbox"></td>
												<td><?=$id?></td>
												<td><?=$create_date?></td>
												<td><?=$create_time?></td>
												<td><?=$data->doc_signature?></td>
												<td><?=$data->status?></td>
												<td><?=$since?></td>
											</tr>
										<?php }
										?>
									</tbody>
								</table>
							</div>
			

						<div class="col-md-12 WorkingPlaceDelete" style="border:1px solid #ccc;padding: 0px; display: none;">
							<form method="post" class="DeleteAccountRecord" action="" style="padding: 30px;">
								<input  name="tablename" value="document_signature" type="hidden" >
								<input type="hidden" id="workingplaceid" name="id" value="">
								<div style="display: inline-block;float:left;margin-right: 10px;"> Are you sure you want to delete selected?</div>
								<img class="displayimageloader" style="float: left; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
								<input class="btn" type="submit" id="search_by" value="Yes" style="float:left;width: 50px;margin-top: -11px;margin-right: 10px;" />
								<a class="btn" style="margin-top: -11px;cursor: pointer;" onclick="WorkingPlaceCancel()">No</a>
							</form>
						</div>

						
					<div class="WorkingPlaceEdit" style="display: none;">	
					<?php
					foreach ($doc_signature as $data)
					{ ?>
						<div id="WorkingPlaceEditview<?=$data->id?>" class="col-md-12 WorkingPlaceEditEdit" style="display: none;">
							<form  class="SaveEditForm" method="post" style="width: 100%;background-image: none;" enctype="multipart/form-data">
								
								<div class="row" style="margin-top: 10px;">
									<div class="col-md-3">
										<div class="form-group">
											<div class="col-md-4" style="margin-top: 10px;" >
												<span style="font-weight: bold;">Signature</span><br />
												<span style="font-weight: bold;">Status</span>
											</div>
											<div class="col-md-8" style="padding: 0px;">
												<input required="" class="form-control" value="<?=$data->doc_signature?>" name="fieldvalue" type="text" >
												<select name="status" class="form-control">
													<option value="Active" <?php if($data->status == 'Active') echo 'selected'; ?>>Active</option>
													<option value="Inactive" <?php if($data->status == 'Inactive') echo 'selected'; ?>>Inactive</option>
												</select>
												<input  name="tablename" value="document_signature" type="hidden" >
												<input  name="fieldname" value="doc_signature" type="hidden" >
												<input  name="fieldname2" value="status" type="hidden" >
												<input type="hidden" value="<?=$data->id?>" name="id">
											</div>
										</div>
									</div>
								</div>

								<div class="row" style="margin-bottom: 10px;margin-top: 20px;">
									<div class="col-md-3">
										<a class="btn" style="float: right;cursor: pointer;"  onclick="WorkingPlaceCancel()"><span class="delete-icon" >Cancel</span></a>
										<button style="padding: 3px 13px;margin-right: 10px;float:right;" type="submit" class="btn"><span class="save-icon"></span> Save</button>
										<img class="displayimageloader" style="float: right; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
									</div>
								</div>

							</form>
						</div>
					<?php
					}
					?>
					</div>
						
						
						
						<div class="col-md-12 WorkingPlaceAdd" style="border:1px solid #ccc;padding-left:3px;margin-bottom: 10px;display: none; ">
						   <form action="" class="ConfigServiceForm" method="post" style="width: 100%;background-image: none;">
									<div class="row" style="margin-top: 10px;">
										
										<div class="col-md-3">
											<div class="form-group">
												<div class="col-md-4" style="margin-top: 5px;" >
													<span style="font-weight: bold;">Signature</span><br />
													<span style="font-weight: bold;">Status</span>
												</div>
												<div class="col-md-8" style="padding: 0px;">
													<input required="" class="form-control" name="fieldvalue" type="text" >
													<select name="status" class="form-control">
														<option value="Active">Active</option>
														<option value="Inactive">Inactive</option>
													</select>
													<input  name="tablename" value="document_signature" type="hidden" >
													<input  name="fieldname" value="doc_signature" type="hidden" >
													<input  name="fieldname2" value="status" type="hidden" >
												</div>

											</div>
										</div>
										
							

									</div>


								<div class="row" style="margin-bottom: 10px;margin-top: 20px;">
									<div class="col-md-3">
										<a class="btn" style="float: right;" href="#" onclick="WorkingPlaceCancel()"><span class="delete-icon" >Cancel</span></a>
										<button style="padding: 3px 13px;margin-right: 10px;float:right;" type="submit" class="btn"><span class="save-icon"></span> Save</button>
										<img class="displayimageloader" style="float: right; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
									</div>
								</div>

							</form>

						</div>
                    </div>
					
					<div class="tab-pane fade" id="Departments"> 
						<div class="listRequiredExperiance" >
								<div class="filter-group">
								  <!-- <div class="col-md-8" style="padding-left: 0px;">
									  <form  method="get" action="" style="padding-top: 0px;background: none;" >
											<span style="float: left;padding: 3px;padding-top: 10px">Search keyword</span>
												<input type="text" name="searchword"  class="form-control" style="width: 20%;float:left; "  value="<?=!empty($this->input->get('searchword'))?$this->input->get('searchword'):''?>" />
											<span style="float: left;padding: 3px;padding-top: 10px">From</span>
												<input type="text" name="from_period" class="datepicker" style="width: 13%;float:left;height: 34px; "  value="<?=!empty($this->input->get('from_period'))?$this->input->get('from_period'):''?>" /> 
											<span style="float: left;padding: 3px;padding-top: 10px">To</span>
												<input type="text" name="to_period" class="datepicker" style="width: 13%;float:left;height: 34px; " value="<?=!empty($this->input->get('to_period'))?$this->input->get('to_period'):''?>"/>
											<input class="btn" type="submit"  value="Search" style="float:left;width: 8%;" />
											<a class="btn" style="float: left" href="<?=base_url()?>admin/quotes.php"> Reset </a>
										</form>
									</div> -->

								    <div class="col-md-12" style="padding-left: 0px;">
										<div class="page-action" style="float: right">
											<a class="btn btn-default" onclick="RequiredExperianceAdd()"><span class="add-icon"> Add</span></a>&nbsp;
											<a class="btn btn-default" onclick="RequiredExperianceEdit()"><span class="edit-icon"> Edit</span></a>&nbsp;
											<a class="btn btn-default" onclick="RequiredExperianceDelete()"><span class="delete-icon">Delete</a>&nbsp;
										</div>
									</div>
								</div>
								<table class="table table-bordered data-table dataTable">
									<input type="hidden" class="clicked-requiredexperiance" value="">
									<thead>
										<tr>
											<th></th>
											<th>ID</th>
											<th>Date</th>
											<th>Time</th>
											<th>Departments</th>
											<th>Status</th>
											<th>Since</th>
										</tr>
									</thead>
									<tbody>
									<?php
										foreach ($document_dept as $data)
										{ 
											$id = create_timestamp_uid($data->create_date, $data->id);
											$create_date = date('d/m/Y', strtotime($data->create_date));
            								$create_time = date('H:i:s', strtotime($data->create_date)); 
        									$since = timeDiff($data->create_date);
									?>
											<tr>
												<td><input class="chk-requiredexperiance-template" data-input="<?=$data->id?>" type="checkbox"></td>
												<td><?=$id?></td>
												<td><?=$create_date?></td>
												<td><?=$create_time?></td>
												<td><?=$data->dept_name?></td>
												<td><?=$data->status?></td>
												<td><?=$since?></td>
											</tr>
										<?php }
										?>
									</tbody>
								</table>
							</div>
			

						<div class="col-md-12 RequiredExperianceDelete" style="border:1px solid #ccc;padding: 0px; display: none;">
							<form method="post" class="DeleteAccountRecord" action="" style="padding: 30px;">
								<input  name="tablename" value="document_departments" type="hidden" >
								<input type="hidden" id="requiredexperianceid" name="id" value="">
								<div style="display: inline-block;float:left;margin-right: 10px;"> Are you sure you want to delete selected?</div>
								<img class="displayimageloader" style="float: left; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
								<input class="btn" type="submit" id="search_by" value="Yes" style="float:left;width: 50px;margin-top: -11px;margin-right: 10px;" />
								<a class="btn" style="margin-top: -11px;cursor: pointer;" onclick="RequiredExperianceCancel()">No</a>
							</form>
						</div>

						
					<div class="RequiredExperianceEdit" style="display: none;">	
						<?php
						foreach ($document_dept as $data)
						{ ?>
						<div id="RequiredExperianceEditview<?=$data->id?>" class="col-md-12 RequiredExperianceEditEdit" style="display: none;">
							<form  class="SaveEditForm" method="post" style="width: 100%;background-image: none;" enctype="multipart/form-data">
								
								<div class="row" style="margin-top: 10px;">
									<div class="col-md-3">
										<div class="form-group">
											<div class="col-md-4" style="margin-top: 5px;" >
												<span style="font-weight: bold;">Department Name</span><br />
												<span style="font-weight: bold;">Status</span>
											</div>
											<div class="col-md-8" style="padding: 0px;">
													<input required="" class="form-control" value="<?=$data->dept_name?>" name="fieldvalue" type="text" >
													<select name="status" class="form-control">
														<option value="Active" <?php if($data->status == 'Active') echo 'selected'; ?>>Active</option>
														<option value="Inactive" <?php if($data->status == 'Inactive') echo 'selected'; ?>>Inactive</option>
													</select>
													<input  name="tablename" value="document_departments" type="hidden" >
													<input  name="fieldname" value="dept_name" type="hidden" >
													<input  name="fieldname2" value="status" type="hidden" >
													<input type="hidden" value="<?=$data->id?>" name="id">
											</div>
										</div>
									</div>
								</div>

								<div class="row" style="margin-bottom: 10px;margin-top: 20px;">
									<div class="col-md-3">
										<a class="btn" style="float: right;cursor: pointer;"  onclick="RequiredExperianceCancel()"><span class="delete-icon" >Cancel</span></a>
										<button style="padding: 3px 13px;margin-right: 10px;float:right;" type="submit" class="btn"><span class="save-icon"></span> Save</button>
										<img class="displayimageloader" style="float: right; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
									</div>
								</div>

							</form>
						</div>
					<?php
					}
					?>
					</div>
						


						<div class="col-md-12 RequiredExperianceAdd" style="border:1px solid #ccc;padding-left:3px;margin-bottom: 10px;display: none; ">
								<form action="" class="ConfigServiceForm" method="post" style="width: 100%;background-image: none;">
									<div class="row" style="margin-top: 10px;">
										<div class="col-md-3">
											<div class="form-group">
												<div class="col-md-4" style="margin-top: 5px;" >
													<span style="font-weight: bold;">Department Name</span><br />
													<span style="font-weight: bold;">Status</span>
												</div>
												<div class="col-md-8" style="padding: 0px;">
													<input required="" class="form-control" name="fieldvalue" type="text" >
													<select name="status" class="form-control">
														<option value="Active">Active</option>
														<option value="Inactive">Inactive</option>
													</select>
													<input  name="tablename" value="document_departments" type="hidden" >
													<input  name="fieldname" value="dept_name" type="hidden" >
													<input  name="fieldname2" value="status" type="hidden" >
												</div>

											</div>
										</div>
										
							

									</div>


								<div class="row" style="margin-bottom: 10px;margin-top: 20px;">
									<div class="col-md-3">
										<a class="btn" style="float: right;" href="#" onclick="RequiredExperianceCancel()"><span class="delete-icon" >Cancel</span></a>
										<button style="padding: 3px 13px;margin-right: 10px;float:right;" type="submit" class="btn"><span class="save-icon"></span> Save</button>
										<img class="displayimageloader" style="float: right; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
									</div>
								</div>

							</form>

						</div>
                    </div>
					
					
					<div class="tab-pane fade" id="Managers"> 
						<div class="listRequiredDiploma" >
								<div class="filter-group">
								  <!-- <div class="col-md-8" style="padding-left: 0px;">
									  <form  method="get" action="" style="padding-top: 0px;background: none;" >
											<span style="float: left;padding: 3px;padding-top: 10px">Search keyword</span>
												<input type="text" name="searchword"  class="form-control" style="width: 20%;float:left; "  value="<?=!empty($this->input->get('searchword'))?$this->input->get('searchword'):''?>" />
											<span style="float: left;padding: 3px;padding-top: 10px">From</span>
												<input type="text" name="from_period" class="datepicker" style="width: 13%;float:left;height: 34px; "  value="<?=!empty($this->input->get('from_period'))?$this->input->get('from_period'):''?>" /> 
											<span style="float: left;padding: 3px;padding-top: 10px">To</span>
												<input type="text" name="to_period" class="datepicker" style="width: 13%;float:left;height: 34px; " value="<?=!empty($this->input->get('to_period'))?$this->input->get('to_period'):''?>"/>
											<input class="btn" type="submit"  value="Search" style="float:left;width: 8%;" />
											<a class="btn" style="float: left" href="<?=base_url()?>admin/quotes.php"> Reset </a>
										</form>
									</div> -->

								    <div class="col-md-12" style="padding-left: 0px;">
										<div class="page-action" style="float: right">
											<a class="btn btn-default" onclick="RequiredDiplomaAdd()"><span class="add-icon"> Add</span></a>&nbsp;
											<a class="btn btn-default" onclick="RequiredDiplomaEdit()"><span class="edit-icon"> Edit</span></a>&nbsp;
											<a class="btn btn-default" onclick="RequiredDiplomaDelete()"><span class="delete-icon">Delete</a>&nbsp;
										</div>
									</div>
								</div>
								<table class="table table-bordered data-table dataTable">
									<input type="hidden" class="clicked-requireddiploma" value="">
									<thead>
										<tr>
											<th></th>
											<th>ID</th>
											<th>Date</th>
											<th>Time</th>
											<th>Departments</th>
											<th>Status</th>
											<th>Since</th>
										</tr>
									</thead>
									<tbody>
									<?php
										foreach ($document_manag as $data)
										{ 
											$id = create_timestamp_uid($data->create_date, $data->id);
											$create_date = date('d/m/Y', strtotime($data->create_date));
            								$create_time = date('H:i:s', strtotime($data->create_date)); 
        									$since = timeDiff($data->create_date);
									?>
											<tr>
												<td><input class="chk-requireddiploma-template" data-input="<?=$data->id?>" type="checkbox"></td>
												<td><?=$id?></td>
												<td><?=$create_date?></td>
												<td><?=$create_time?></td>
												<td><?=$data->manager_name?></td>
												<td><?=$data->status?></td>
												<td><?=$since?></td>
											</tr>
										<?php }
										?>
									</tbody>
								</table>
							</div>
			

					
					<div class="col-md-12 RequiredDiplomaDelete" style="border:1px solid #ccc;padding: 0px; display: none;">
						<form method="post" class="DeleteAccountRecord" action="" style="padding: 30px;">
							<input  name="tablename" value="document_managers" type="hidden" >
							<input type="hidden" id="requireddiplomaid" name="id" value="">
							<div style="display: inline-block;float:left;margin-right: 10px;"> Are you sure you want to delete selected?</div>
							<img class="displayimageloader" style="float: left; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
							<input class="btn" type="submit" id="search_by" value="Yes" style="float:left;width: 50px;margin-top: -11px;margin-right: 10px;" />
							<a class="btn" style="margin-top: -11px;cursor: pointer;" onclick="RequiredDiplomaCancel()">No</a>
						</form>
					</div>

						
					<div class="RequiredDiplomaEdit" style="display: none;">	
						<?php
						foreach ($document_manag as $data)
						{ ?>
						<div id="RequiredDiplomaEditview<?=$data->id?>" class="col-md-12 RequiredDiplomaEditEdit" style="display: none;">
							<form  class="SaveEditForm" method="post" style="width: 100%;background-image: none;" enctype="multipart/form-data">
								<div class="row" style="margin-top: 10px;">
									<div class="col-md-3">
										<div class="form-group">
											<div class="col-md-4" style="margin-top: 5px;" >
												<span style="font-weight: bold;">Manager Name</span><br />
												<span style="font-weight: bold;">Status</span>
											</div>
											<div class="col-md-8" style="padding: 0px;">
												<input required="" class="form-control" value="<?=$data->manager_name?>" name="fieldvalue" type="text" >
												<select name="status" class="form-control">
													<option value="Active" <?php if($data->status == 'Active') echo 'selected'; ?>>Active</option>
													<option value="Inactive" <?php if($data->status == 'Inactive') echo 'selected'; ?>>Inactive</option>
												</select>
												<input  name="tablename" value="document_managers" type="hidden" >
												<input  name="fieldname" value="manager_name" type="hidden" >
												<input  name="fieldname2" value="status" type="hidden" >
												<input type="hidden" value="<?=$data->id?>" name="id">
											</div>
										</div>
									</div>
								</div>

								<div class="row" style="margin-bottom: 10px;margin-top: 20px;">
									<div class="col-md-3">
										<a class="btn" style="float: right;cursor: pointer;"  onclick="RequiredDiplomaCancel()"><span class="delete-icon" >Cancel</span></a>
										<button style="padding: 3px 13px;margin-right: 10px;float:right;" type="submit" class="btn"><span class="save-icon"></span> Save</button>
										<img class="displayimageloader" style="float: right; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
									</div>
								</div>

							</form>
						</div>
					<?php
					}
					?>
					</div>
						

						<div class="col-md-12 RequiredDiplomaAdd" style="border:1px solid #ccc;padding-left:3px;margin-bottom: 10px;display: none; ">
						   <form action="" class="ConfigServiceForm" method="post" style="width: 100%;background-image: none;">

									<div class="row" style="margin-top: 10px;">

										<div class="col-md-3">
											<div class="form-group">
												<div class="col-md-4" style="margin-top: 5px;" >
													<span style="font-weight: bold;">Manager Name</span><br />
												<span style="font-weight: bold;">Status</span>
												</div>
												<div class="col-md-8" style="padding: 0px;">
													<input required="" class="form-control" name="fieldvalue" type="text" >
													<select name="status" class="form-control">
														<option value="Active">Active</option>
														<option value="Inactive">Inactive</option>
													</select>
													<input  name="tablename" value="document_managers" type="hidden" >
													<input  name="fieldname" value="manager_name" type="hidden" >
													<input  name="fieldname2" value="status" type="hidden" >
												</div>
											</div>
										</div>
									</div>


								<div class="row" style="margin-bottom: 10px;margin-top: 20px;">
									<div class="col-md-3">
										<a class="btn" style="float: right;" href="#" onclick="RequiredDiplomaCancel()"><span class="delete-icon" >Cancel</span></a>
										<button style="padding: 3px 13px;margin-right: 10px;float:right;" type="submit" class="btn"><span class="save-icon"></span> Save</button>
										<img class="displayimageloader" style="float: right; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />
									</div>
								</div>

							</form>

						</div>
                    </div>
					
                </div>


<script src="<?=base_url()?>assets/css/tinymce/js/tinymce/tinymce.min.js"></script>
 
<script>
	
	
$('document').ready(function(){
		
	$('.multiselect').multiselect({
		columns: 1,
		placeholder: 'Select',
		search: true,
		selectAll: true
	});
	
	$(".UpdateMainJob").on('submit',(function(e) {
			e.preventDefault();
			$('.displayimageloader').show();
		formData.append('<?php echo $this->security->get_csrf_token_name();?>','<?php echo $this->security->get_csrf_hash();?>');	
			$.ajax({
				url:"<?=base_url();?>Rectruitement_config/UpdateSaveJob",
				type: "POST",
				data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){
					$('.displayimageloader').hide();
			location.reload();
				},
		   });
		}));
	
	
	
	$("#JobFormSave").on('submit',(function(e) {
			e.preventDefault();
			$('.displayimageloader').show();
				var formData =new FormData(this);
			formData.append('<?php echo $this->security->get_csrf_token_name();?>','<?php echo $this->security->get_csrf_hash();?>');
			$.ajax({
					url:"<?=base_url();?>Rectruitement_config/SaveJob",
				type: "POST",
				data:  formData,
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){
					$('.displayimageloader').hide();
					location.reload();
					
				},
		   });
		}));
	
	});	

	
	$('body').delegate('.getJobStatutDynamic','change',function(){
		var id = $(this).attr('id');
		var  value = $(this).find("option:selected").text();
		var CurrentDate = $('#CurrentDate').val();
		var CurrentTime = $('#CurrentTime').val();
		if(value == "Published")
		{
			$('#PublishedDate'+id).val(CurrentDate);
			$('#PublishedTime'+id).val(CurrentTime);
//			$('#ToDate'+id).prop('disabled', true);
//			$('#ToTime'+id).prop('disabled', true);
			$('#ToDate'+id).val('');
			$('#ToTime'+id).val('');
		}
		else
		{
			
			$('#PublishedDate'+id).val("");
			$('#PublishedTime'+id).val("");
//			$('#ToDate'+id).prop('disabled', false);
//			$('#ToTime'+id).prop('disabled', false);
		}
	});
	
	
	$('body').delegate('.getJobStatut','change',function(){
		var  value = $(".getJobStatut option:selected").text();
		var CurrentDate = $('#CurrentDate').val();
		var CurrentTime = $('#CurrentTime').val();
		if(value == "Published")
		{
			$('#PublishedDate').val(CurrentDate);
			$('#PublishedTime').val(CurrentTime);
//			$('#ToDate').prop('disabled', true);
//			$('#ToTime').prop('disabled', true);
			$('#ToDate').val('');
			$('#ToTime').val('');
		}
		else
		{
			
			$('#PublishedDate').val("");
			$('#PublishedTime').val("");
//			$('#ToDate').prop('disabled', false);
//			$('#ToTime').prop('disabled', false);
		}
	});
	

	
	$('.DeleteAccountRecord').submit(function(e){
			e.preventDefault();
			var formData = new FormData();
			var contact = $(this).serializeArray();
			$.each(contact, function (key, input) {
				formData.append(input.name, input.value);
			});
			
			$('.displayimageloader').show();
		formData.append('<?php echo $this->security->get_csrf_token_name();?>','<?php echo $this->security->get_csrf_hash();?>');
			$.ajax({
					type:'POST',
					url:"<?=base_url();?>Rectruitement_config/ConfigDelete",
					data: formData,
					cache: false,
					contentType: false,
					processData: false,
					dataType: 'JSON',
					success:function(data){
						$('.displayimageloader').hide();
						location.reload();
					}
			});
	  });
	  
	  

	$('.ConfigServiceForm').submit(function(e){
			e.preventDefault();
			var formData = new FormData();
			var contact = $(this).serializeArray();
			$.each(contact, function (key, input) {
				formData.append(input.name, input.value);
			});
		
	formData.append('<?php echo $this->security->get_csrf_token_name();?>','<?php echo $this->security->get_csrf_hash();?>');
			$('.displayimageloader').show();
		formData.append('<?php echo $this->security->get_csrf_token_name();?>','<?php echo $this->security->get_csrf_hash();?>');
			$.ajax({
					type:'POST',
					url:"<?=base_url();?>Documents_config/ConfigPost",
					data: formData,
					cache: false,
					contentType: false,
					processData: false,
					dataType: 'JSON',
					success:function(data){
						$('.displayimageloader').hide();
						location.reload();
					}
			});
	  });

		
	$('.SaveEditForm').submit(function(e){
			e.preventDefault();
			var formData = new FormData();
			var contact = $(this).serializeArray();
			$.each(contact, function (key, input) {
				formData.append(input.name, input.value);
			});
			
			$('.displayimageloader').show();
		formData.append('<?php echo $this->security->get_csrf_token_name();?>','<?php echo $this->security->get_csrf_hash();?>');
			$.ajax({
					type:'POST',
					url:"<?=base_url();?>Documents_config/ConfigUpdate",
						data: formData,
					cache: false,
					contentType: false,
					processData: false,
					dataType: 'JSON',
					success:function(data){
						$('.displayimageloader').hide();
						location.reload();
					}
			});
	  });



  tinymce.init({ 
        selector:'.texteditor_term',
        plugins:'link code image textcolor',
        toolbar: [
        "undo redo | styleselect | bold italic | link image",
        "alignleft aligncenter alignright Justify | forecolor backcolor",
        "fullscreen"
    ]
  });
  
  jQuery(function($){
		$.datepicker.regional['fr'] = {
			closeText: 'Fermer',
			prevText: '&#x3c;Préc',
			nextText: 'Suiv&#x3e;',
			currentText: 'Aujourd\'hui',
			monthNames: ['Janvier','Fevrier','Mars','Avril','Mai','Juin',
			'Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
			monthNamesShort: ['Jan','Fev','Mar','Avr','Mai','Jun',
			'Jul','Aou','Sep','Oct','Nov','Dec'],
			dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
			dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
			dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
			weekHeader: 'Sm',
			firstDay: 1,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: '',
			maxDate: '+12M +0D',
			showButtonPanel: true
			};
		$.datepicker.setDefaults($.datepicker.regional['fr']);
	});
  	
	$( ".datepicker" ).datepicker({
			dateFormat: "dd-mm-yy",
			regional: "fr"
	});
</script>
<script>
    $(document).ready(function(){
	$('.timepicker1').timepicki({
		show_meridian:false,
		min_hour_value:0,
		max_hour_value:23,
		overflow_minutes:true,
		increase_direction:'up',
		
	});
		
		

		
        $("#from_period").wl_Date({ dateFormat: 'dd/mm/yy' });
        $("#to_period").wl_Date({ dateFormat: 'dd/mm/yy' });

        $.fn.stickyTabs = function( options ) {
            var context = this

            var settings = $.extend({
                getHashCallback: function(hash, btn) { return hash }
            }, options );

            // Show the tab corresponding with the hash in the URL, or the first tab.
            var showTabFromHash = function() {
              var hash = window.location.hash;
              var selector = hash ? 'a[href="' + hash + '"]' : 'li.active > a';
              $(selector, context).tab('show');
            }

            // We use pushState if it's available so the page won't jump, otherwise a shim.
            var changeHash = function(hash) {
              if (history && history.pushState) {
                history.pushState(null, null, '#' + hash);
              } else {
                scrollV = document.body.scrollTop;
                scrollH = document.body.scrollLeft;
                window.location.hash = hash;
                document.body.scrollTop = scrollV;
                document.body.scrollLeft = scrollH;
              }
            }

            // Set the correct tab when the page loads
            showTabFromHash(context)

            // Set the correct tab when a user uses their back/forward button
            $(window).on('hashchange', showTabFromHash);

            // Change the URL when tabs are clicked
            $('a', context).on('click', function(e) {
              var hash = this.href.split('#')[1];
              var adjustedhash = settings.getHashCallback(hash, this);
              changeHash(adjustedhash);
            });

            return this;
        };

        $('.nav-tabs').stickyTabs();

    });

</script>
