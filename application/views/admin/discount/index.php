
<style>
  .truncate {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
    .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
  background:none;
}
  .nav-tabs > li.active {
    background: linear-gradient(#ffffff, #ffffff 25%, #d0d0d0) !important;
    border-bottom: none;
    }
  .nav-tabs > li {
    background-image: linear-gradient(to bottom, #fff 0, #e0e0e0 100%);
    border-left: 1px solid #ccc !important;
    height: 55px;
    margin: 0px !important;
    }

    .nav-tabs > li > a {

    color: #616161 !important;
    font-size: 12px;
    padding-left: 10px;
    display: block !important;
    width: 100% !important;
    height: 100% !important;
    text-transform: uppercase;
    font-weight: bold;
    transition: 0.2s;
    outline: none;
    border: none;
    border-radius: 0px;
}
.dataTables_wrapper .dataTables_filter {
    float: left;
    text-align: left;
    margin-left: 5px;
}
input[type="search"]{
    border:1px solid #cccccc;
    padding-left: 5px;
    border-radius: 0px;
}
input[type="search"]:focus{
    border:1px solid #cccccc !important;
}
     
    .tab-pane{
        padding: 30px 30px 30px 14px;
    }
    .configTable{
        padding: 0px;
    }
    .dataTables_wrapper .dataTables_filter input {
    margin-right: 2px;
    margin-left: 0 !important;
    max-width: 100% !important;
    width: 100% !important;
    float: right;
    }
    .dataTables_wrapper{
        margin-top: 20px;
    }
    .nav-tabs li a {
    color: #616161 !important;
    font-size: 11px;
    margin: 0px !important;
    padding: 15px;
    display: block !important;
    width: 100% !important;
    height: 100% !important;
    text-transform: uppercase;
    font-weight: bold;
    transition: 0.2s;
    outline: none;
}
.nav-tabs {
    border-bottom: none;
}
    
  .nav-tabs li a:hover,.nav-tabs li a:focus {
    /*background: linear-gradient(to bottom, #ececec 0%, #ececec 39%, #f0efef 39%, #b7b3b3 100%) !important;
    background:linear-gradient(to bottom,  #c1c1c1 0%, #ececec 39%, #ececec 39%, #fbfbfb 100%) !important;*/
    background: linear-gradient(to bottom, #c1c1c1 4%,#ececec 30%,#ececec 50%,#b7b3b3 100%) !important;
    color: #616161 !important;
    cursor: pointer;
}
.nav-tabs li.active a {
    /*background:linear-gradient(to bottom,  #c1c1c1 0%, #ececec 39%, #ececec 39%, #fbfbfb 100%) !important;*/
    background: linear-gradient(to bottom, #c1c1c1 4%,#ececec 30%,#ececec 50%,#b7b3b3 100%) !important;
}
/*
button.btn:hover, input.btn:hover{
    background-color: #e0e0e0;
    background-position: 0 -15px;
} */
.dataTables_wrapper .dataTables_paginate .paginate_button{
   color:#fff !important;
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current{
   color:#fff !important;
}

.dataTables_wrapper .dataTables_paginate .paginate_button:hover {
     background: linear-gradient(to bottom, #0d85a8 0%, #35add1 39%,#44c0e5 100%) !important;
   
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
     background: linear-gradient(to bottom, #0d85a8 0%, #35add1 39%,#44c0e5 100%) !important;
    color:#fff !important;
}
</style>


<section id="content">
    <div class="listDiv"><!--col-md-10 padding white right-p-->
    <?php $this->load->view('admin/common/breadcrumbs');?>
        <?php $this->load->view('admin/common/alert');?>
        <?php echo $this->session->flashdata('message'); ?>
 <div>
<div class="row">
  <div class="ListDiscount" >
  <input type="hidden" class="chk-Adddiscount-btn" value="">
   <input type="hidden" class="Adddiscountfullid" value="">
  <!-- <div class="col-md-12">
      <div class="toolbar"style="float: right; margin-bottom:5px;">
        <button class="btn btn-sm btn-default" onclick="Discountadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>&nbsp;
        <button class="btn btn-sm btn-default" onclick="DiscountEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>&nbsp;
        <button class="btn btn-sm btn-default" onclick="DiscountDelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button>&nbsp;
      </div>
    </div> -->
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" >#</th>
            <th class="text-center"><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center"><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center"><?php echo $this->lang->line('time'); ?></th>
             <th class="text-center" style="width: 10% !important;"><?php echo $this->lang->line('added_by'); ?></th>
          
             <th class="text-center">Category</th>
            <th class="column-first_name text-center" style="width: 10%  !important;"><?php echo $this->lang->line('name'); ?></th>
               <th class="text-center" style="width: 10% !important;">Service Category</th>
               <th class="text-center" style="width: 10% !important;">Service Name</th>
               <th class="text-center">Discount Code</th>
               <th class="text-center">From Date</th>
               <th class="text-center">To Date</th>
               <th class="text-center">From Time</th>
               <th class="text-center">To Time</th>
            <th class="column-date text-center"><?php echo $this->lang->line('discount'); ?>Discount %</th>
            <th class="text-center">After No of Bookings</th>
            <th class="column-time text-center" >Statut</th>
            <th class="text-center" style="width:30%  !important;">Since</th>

          </tr>
          </thead>
          <tbody>
              <?php if (!empty($discount_data)): ?>
              <?php $cnt=1; ?>
              <?php foreach($discount_data as $key => $item):?>
                  <tr>
                    <td class="no-sort text-center"style="width:2%;">
                        <input type="checkbox" class="chk-maindiscount-template" data-input="<?=$item->id?>">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="DiscountidEdit('<?=$item->id?>','<?=date("dmY", strtotime($item->created_at)) .'0000'. $item->id;?>')"><?=date("dmY", strtotime($item->created_at)) .'0000'. $item->id;?></a></td>
                      <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                      <td class="text-center"><?=date("H:i:s", strtotime($item->since))?></td>
                       <td class="text-center truncate"><?php
                        $user=$this->bookings_config_model->getuser($item->user_id);
                        $user=str_replace(".", " ", $user);
                        echo $user;
                        ?></td>
                      <td class="text-center">
                      
                        <?php
                            if($item->discount_category==0){
                              echo "welcome";
                            }
                            elseif($item->discount_category==1){
                              echo "Reward";
                            }
                             elseif($item->discount_category==2){
                              echo "Periode";
                            }
                             elseif($item->discount_category==3){
                              echo "Promo Code";
                            }
                         ?>
                          
                        </td>
                      <td class="text-center truncate"><?=$item->name_of_discount;?></td>
                      <td class="text-center truncate">
                        <?php if(!empty($item->servicecategory_id) && $item->servicecategory_id!=0 && $item->servicecategory_id!=null){
                             echo $this->discount_model->get_service_category($item->servicecategory_id);
                        }else{echo "All";}?>
                    
                      </td>
                      <td class="text-center truncate">
                         <?php if(!empty($item->service_id) && $item->service_id!=0 && $item->service_id!=null){
                             echo $this->discount_model->get_service($item->service_id);
                        }else{echo "All";}?>
                    
                      </td>
                      <td class="text-center">
                         <?php
                          if($item->discount_category==3){
                             echo $item->promo_code;
                           }else{
                            echo "";
                           }
                          ?>
                        </td>
                       <td class="text-center">
                       
                        <?php
                          if($item->discount_category==2){
                              $discountdatefrom=$item->datefrom;
                              $discountdatefrom=str_replace('-', '/', $discountdatefrom);
                              $discountdatefrom=strtotime($discountdatefrom);
                              $datefrom = date('d/m/Y',$discountdatefrom);
                              echo $datefrom;
                          }else{
                            echo "";
                          }
                         ?>
                          
                        </td>
                      <td class="text-center">
                       
                        <?php
                          if($item->discount_category==2){
                                 $discountdateto=$item->dateto;
                                 $discountdateto=str_replace('-', '/', $discountdateto);
                                 $discountdateto=strtotime($discountdateto);
                                 $dateto = date('d/m/Y',$discountdateto);
                              echo $dateto;
                          }else{
                            echo "";
                          }
                         ?>
                          
                        </td>
                      <td class="text-center">
                       
                        <?php
                          if($item->discount_category==2){
                            echo $item->timefrom;
                          }else{
                            echo "";
                          }
                         ?>
                          
                        </td>
                      <td class="text-center">
                       
                        <?php
                          if($item->discount_category==2){
                            echo $item->timeto;
                          }else{
                            echo "";
                          }
                         ?>
                          
                        </td>
                      <td class="text-center"><?=$item->discount; ?></td>

                      <td class="text-center">  <?php
                          if($item->discount_category==1){echo $item->no_of_booking; }else{echo "0";}?></td>
                     
                      <td class="text-center"><?=$item->statut=='1'?'<span class="label label-success">Active</span>':'<span class="label label-danger">Hide</span>';?></td>
                      <td class="text-center truncate"><?= timeDiff($item->since) ?></td>

                  </tr>
                  <?php $cnt++; ?>
              <?php endforeach; ?>
              <?php endif; ?>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>
<?=form_open("admin/discounts/discountadd")?>
<!-- <form action="<?=base_url();?>admin/discounts/vatadd" class="" method="post"  enctype="multipart/form-data"> -->
<div class="Discountadd" style="display: none;" >
  <div class="col-md-12">
  <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span>Statut</span>
      <select class="form-control" name="discount_statut" required>
       
        <option value="1">Show</option>
        <option value="2">Hide</option>
      </select>
    </div>
  </div>
  <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span>Discount Category</span>
      <select class="form-control" id="discount_category" name="discount_category" required onchange="checkCategory()">
        <option value="0">Welcome</option>
        <option value="1">Reward</option>
        <option value="2">Periode</option>
        <option value="3">Promo Code</option>
      </select>
    </div>
  </div>
  <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span>Name Of Discount</span>
        <input type="text" class="form-control" name="name_of_discount" placeholder="" value="">
    </div>
  </div>
 

</div>
<div class="col-md-12">
   <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span>Service Category</span>
      <select class="form-control" id="servicecategory_id" name="servicecategory_id" onchange="addservice()">
          <option value="">All</option>
       <?php foreach($service_cat as $key => $cat):?>
        <option value="<?= $cat->id ?>"><?= $cat->category_name ?></option>
      <?php endforeach; ?>
      </select>
    </div>
  </div>
  <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span>Services</span>
      <select class="form-control" id="service_id" name="service_id" >
        <option value="">All</option>
      
      </select>
    </div>
  </div>

  <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span>Discount %</span>
        <input type="text" class="form-control" name="discount" placeholder="" value="">
    </div>
  </div>
</div>
<div class="col-md-12" id="discountrewardnodiv">
    <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span>After Number of Bookings</span>
        <input type="number" class="form-control" name="no_of_booking" placeholder="" value="0" min="0" max="1000">
    </div>
  </div>
</div>
<div class="col-md-12" id="discountpromocodediv">
    <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span>Promo Code</span>
        <input type="text" class="form-control" name="promo_code" placeholder="" value="">
    </div>
  </div>
</div>
<div class="col-md-12" id="discountperiodsdiv">
  <div class="col-md-2">
    
      <div class="col-md-6" style="padding:2px;">  
       <div class="form-group">
       <span>From Date</span>  
         <input   id="discountdatefrom" class="bdatepicker" name="discountdatefrom" type="text" value="<?php echo date('d/m/Y');?>"  style="width:100%;" />
      </div>
     </div>   
      
      <div class="col-md-6" style="padding:2px;">
         <div class="form-group">
           <span>To Date</span>
        <input   id="discountdateto" class="bdatepicker" name="discountdateto" type="text" value="<?php echo date('d/m/Y');?>" style="width:100%;" />
        </div>
     </div>
</div>

  <div class="col-md-2">
     
      <div class="col-md-6" style="padding:2px;">
        <div class="form-group">
           <span>From Time</span>
           <input   id="discounttimefrom" name="discounttimefrom" type="text" value="<?php echo date("h : i") ?>" style="width:100%;" />
      </div>
    </div>

      
      <div class="col-md-6" style="padding:2px;">
        <div class="form-group">
           <span>To Time</span>
           <input   id="discounttimeto" name="discounttimeto" type="text" value="<?php echo date("h : i") ?>" style="width:100%;" />
      </div>
    </div>
       
</div>

</div>

  <div class="col-md-12 clearfix" style="padding-right: 70px;">
  <button  class="btn"style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
    <button type="button" class="btn" style="float:right; margin-left:7px;height:34px;" onclick="cancelDiscount()"><span class="fa fa-close"> Cancel </span></button>

  <?php echo form_close(); ?>
</div>
</div>
<div class="discountEdit" style="display:none">
  <?=form_open("admin/discounts/discountedit")?>
  <div class="discountEditajax">
  </div>
  <div class="col-md-12 clearfix" style="padding-right: 70px;">
  <button  class="btn"style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
    <button type="button" class="btn" style="float:right; margin-left:7px;height:34px;" onclick="cancelDiscount()"><span class="fa fa-close"> Cancel </span></button>

  </div>
  <?php echo form_close(); ?>
</div>
<div class="col-md-12 discountDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
  <?=form_open("admin/discounts/deletediscount")?>
    <input  name="tablename" value="vbs_job_statut" type="hidden" >
    <input type="hidden" id="discountdeletid" name="delet_discount_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn"style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

     <button type="button" class="btn" style="cursor: pointer;" onclick="cancelDiscount()">No</button>
  <?php echo form_close(); ?>
</div>
</div>
</div>
</div>
</section>
<script>
  function addservice(){
    var val=$("#servicecategory_id").val();
    if (val!='')
    {
         $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/discounts/get_service'; ?>',
                data: {'servicecategory_id': val},
                success: function (result) {
                
                  $("#service_id").empty();
                  document.getElementById("service_id").innerHTML =result;
                }
              });
    }
   
  }
   function editaddservice(){
    var val=$("#editservice_category").val();
    if (val!='')
    {
         $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/discounts/get_service'; ?>',
                data: {'servicecategory_id': val},
                success: function (result) {
                 
                  $("#editservice").empty();
                  document.getElementById("editservice").innerHTML =result;
                }
              });
    }
   
  }
  function Discountadd()
{
   $('#adddisbreadcrumb').remove();
   $(".breadcrumb").append("<span id='adddisbreadcrumb'> > Add discount</span>");
 setupDivDiscount();
 $(".Discountadd").show();
}

function cancelDiscount()
{
  $('#adddisbreadcrumb').remove();
   $('#editdisbreadcrumb').remove();
  setupDivDiscount();
  $(".ListDiscount").show();
}
// function DiscountEdit()
// {
//   var val = $('.chk-Adddiscount-btn').val();
//   if(val != "")
//   {
//     setupDivDiscount();
//     $(".discountEdit").show();
//     $("#discountEditview"+val).show();
//   }
// }

function DiscountEdit()
{
   
    var val = $('.chk-Adddiscount-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }

    $('#editdisbreadcrumb').remove();
  var discountid=$('.Adddiscountfullid').val();

   $(".breadcrumb").append("<span id='editdisbreadcrumb'> > "+discountid+"</span>");
        setupDivDiscount();
        $(".discountEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/discounts/get_ajax_discount'; ?>',
                data: {'discount_id': val},
                success: function (result) {
                   

                  document.getElementsByClassName("discountEditajax")[0].innerHTML = result;
                   $('#editdiscounttimefrom').timepicki({
                      step_size_minutes:'5',
                      show_meridian: false,
                      min_hour_value: 0,
                      max_hour_value: 23,
                      overflow_minutes: true,
                      increase_direction: 'up',
                      disable_keyboard_mobile: true});
                    $('#editdiscounttimeto').timepicki({
                          step_size_minutes:'5',
                        show_meridian: false,
                        min_hour_value: 0,
                        max_hour_value: 23,
                        overflow_minutes: true,
                        increase_direction: 'up',
                        disable_keyboard_mobile: true});
                    $('.bdatepicker').datepicker({
                          format: "dd/mm/yyyy"
                      });
                      var x = document.getElementById("editdiscount_category").value;
  if(x==0){
  document.getElementById("editdiscountperiodsdiv").style.display = "none"; 
  document.getElementById("editdiscountpromocodediv").style.display = "none";
  document.getElementById("editdiscountrewardnodiv").style.display = "none";

  }
  else if (x==1){ 
  document.getElementById("editdiscountperiodsdiv").style.display = "none";
  document.getElementById("editdiscountpromocodediv").style.display = "none";
  document.getElementById("editdiscountrewardnodiv").style.display = "block";
  }
  else if(x==2){ 
  document.getElementById("editdiscountperiodsdiv").style.display = "block"; 
  document.getElementById("editdiscountpromocodediv").style.display = "none";
  document.getElementById("editdiscountrewardnodiv").style.display = "none";
  }
  else if(x==3){
  document.getElementById("editdiscountperiodsdiv").style.display = "none"; 
  document.getElementById("editdiscountpromocodediv").style.display = "block";
  document.getElementById("editdiscountrewardnodiv").style.display = "none";
  }
                }
            });
}
function DiscountidEdit(id,fullid){

  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      $('#editdisbreadcrumb').remove();
   

   $(".breadcrumb").append("<span id='editdisbreadcrumb'> > "+fullid+"</span>");
        setupDivDiscount();
        $(".discountEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/discounts/get_ajax_discount'; ?>',
                data: {'discount_id': val},
                success: function (result) {
                   

                  document.getElementsByClassName("discountEditajax")[0].innerHTML = result;
                   $('#editdiscounttimefrom').timepicki({
                      step_size_minutes:'5',
                      show_meridian: false,
                      min_hour_value: 0,
                      max_hour_value: 23,
                      overflow_minutes: true,
                      increase_direction: 'up',
                      disable_keyboard_mobile: true});
                    $('#editdiscounttimeto').timepicki({
                          step_size_minutes:'5',
                        show_meridian: false,
                        min_hour_value: 0,
                        max_hour_value: 23,
                        overflow_minutes: true,
                        increase_direction: 'up',
                        disable_keyboard_mobile: true});
                    $('.bdatepicker').datepicker({
                          format: "dd/mm/yyyy"
                      });
                      var x = document.getElementById("editdiscount_category").value;
  if(x==0){
  document.getElementById("editdiscountperiodsdiv").style.display = "none"; 
  document.getElementById("editdiscountpromocodediv").style.display = "none";
  document.getElementById("editdiscountrewardnodiv").style.display = "none";

  }
  else if (x==1){ 
  document.getElementById("editdiscountperiodsdiv").style.display = "none";
  document.getElementById("editdiscountpromocodediv").style.display = "none";
  document.getElementById("editdiscountrewardnodiv").style.display = "block";
  }
  else if(x==2){ 
  document.getElementById("editdiscountperiodsdiv").style.display = "block"; 
  document.getElementById("editdiscountpromocodediv").style.display = "none";
  document.getElementById("editdiscountrewardnodiv").style.display = "none";
  }
  else if(x==3){
  document.getElementById("editdiscountperiodsdiv").style.display = "none"; 
  document.getElementById("editdiscountpromocodediv").style.display = "block";
  document.getElementById("editdiscountrewardnodiv").style.display = "none";
  }
                }
            });
}
function DiscountDelete()
{
  var val = $('.chk-Adddiscount-btn').val();
  if(val != "")
  {
  setupDivDiscount();
  $(".discountDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivDiscount()
{
  // $(".discountEditEdit").hide();
  $(".Discountadd").hide();
  $(".discountEdit").hide();
  $(".ListDiscount").hide();
  $(".discountDelete").hide();

}
$('input.chk-maindiscount-template').on('change', function() {
  $('input.chk-maindiscount-template').not(this).prop('checked', false);
   var parent= $(this).parent();
  var sibling=$(parent).next().html();
  var id = $(this).attr('data-input');
  $('.chk-Adddiscount-btn').val(id);
  $('#discountdeletid').val(id);
 
   $('.Adddiscountfullid').val(sibling);
});

//start dicount periods and promo code
  var x = document.getElementById("discount_category").value;
  if(x==0){
  document.getElementById("discountperiodsdiv").style.display = "none"; 
  document.getElementById("discountpromocodediv").style.display = "none";
 document.getElementById("discountrewardnodiv").style.display = "none";
  }
  else if (x==1){ 
  document.getElementById("discountperiodsdiv").style.display = "none";
  document.getElementById("discountpromocodediv").style.display = "none";
  document.getElementById("discountrewardnodiv").style.display = "block";
  }
  else if(x==2){ 
  document.getElementById("discountperiodsdiv").style.display = "block"; 
  document.getElementById("discountpromocodediv").style.display = "none";
  document.getElementById("discountrewardnodiv").style.display = "none";
  }
  else if(x==3){
  document.getElementById("discountperiodsdiv").style.display = "none"; 
  document.getElementById("discountpromocodediv").style.display = "block";
  document.getElementById("discountrewardnodiv").style.display = "none";
  }
 

function checkCategory() {
 var x = document.getElementById("discount_category").value;
  if(x==0){
  document.getElementById("discountperiodsdiv").style.display = "none"; 
  document.getElementById("discountpromocodediv").style.display = "none";
 document.getElementById("discountrewardnodiv").style.display = "none";
  }
  else if (x==1){ 
  document.getElementById("discountperiodsdiv").style.display = "none";
  document.getElementById("discountpromocodediv").style.display = "none";
  document.getElementById("discountrewardnodiv").style.display = "block";
  }
  else if(x==2){ 
  document.getElementById("discountperiodsdiv").style.display = "block"; 
  document.getElementById("discountpromocodediv").style.display = "none";
  document.getElementById("discountrewardnodiv").style.display = "none";
  }
  else if(x==3){
  document.getElementById("discountperiodsdiv").style.display = "none"; 
  document.getElementById("discountpromocodediv").style.display = "block";
  document.getElementById("discountrewardnodiv").style.display = "none";
  }
 
}
//end discount period and promo code

//edit dicount periods and promo code
 
 

function editcheckCategory() {
    var x = document.getElementById("editdiscount_category").value;
  if(x==0){
  document.getElementById("editdiscountperiodsdiv").style.display = "none"; 
  document.getElementById("editdiscountpromocodediv").style.display = "none";
  document.getElementById("editdiscountrewardnodiv").style.display = "none";

  }
  else if (x==1){ 
  document.getElementById("editdiscountperiodsdiv").style.display = "none";
  document.getElementById("editdiscountpromocodediv").style.display = "none";
  document.getElementById("editdiscountrewardnodiv").style.display = "block";
  }
  else if(x==2){ 
  document.getElementById("editdiscountperiodsdiv").style.display = "block"; 
  document.getElementById("editdiscountpromocodediv").style.display = "none";
  document.getElementById("editdiscountrewardnodiv").style.display = "none";
  }
  else if(x==3){
  document.getElementById("editdiscountperiodsdiv").style.display = "none"; 
  document.getElementById("editdiscountpromocodediv").style.display = "block";
  document.getElementById("editdiscountrewardnodiv").style.display = "none";
  }
 
}
//edit discount period and promo code
</script>
<script>
  $(document).ready(function () {
    $(window).on('load', function() {
       discountEvent();
      
  
      });
     function discountEvent(){
     
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button type="button" id="discountaddfunc" class="dt-button buttons-copy buttons-html5"  onclick="Discountadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="DiscountEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="DiscountDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
   
    
  
function adddiscountperiod(){
         var datefrom=$("#discountdatefrom").val();
         var dateto=$("#discountdateto").val();
        var timefrom=$("#discounttimefrom").val();
         var timeto=$("#discounttimeto").val();
        
             $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/discounts/add_discount_periods'; ?>',
                data: {'datefrom': datefrom,'dateto':dateto,'timefrom':timefrom,'timeto':timeto},
                success: function (data) {
                   $('#discountperioddiv').empty();
          $.each(data.periods, function(k, v) {
           $("#discountperioddiv").append('<li class="text-left" style="display:inline-block;margin-right:5px;"><button type="button" style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;outline:none;color:#4ca6cf;" class="dateremovebtn" onclick="removediscountperiod('+k+');"><i class="fa fa-close"></i></button>'+v["discountdatefrom"]+' to '+v["discountdateto"]+' '+v["discounttimefrom"]+' to '+v["discounttimeto"]+'</li>');
          
           });
                }
            });
}
function removediscountperiod(val){

     $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/discounts/remove_discount_periods'; ?>',
                data: {'index': val},
                success: function (data) {
                   $('#discountperioddiv').empty();
          $.each(data.periods, function(k, v) {
           $("#discountperioddiv").append('<li class="text-left" style="display:inline-block;margin-right:5px;"><button type="button" style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;outline:none;color:#4ca6cf;" class="dateremovebtn" onclick="removediscountperiod('+k+');"><i class="fa fa-close"></i></button>'+v["discountdatefrom"]+' to '+v["discountdateto"]+' '+v["discounttimefrom"]+' to '+v["discounttimeto"]+'</li>');
          
           });
                }
            });
}
function editdiscountperiod(){
         var datefrom=$("#editdiscountdatefrom").val();
         var dateto=$("#editdiscountdateto").val();
        var timefrom=$("#editdiscounttimefrom").val();
         var timeto=$("#editdiscounttimeto").val();
        
             $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/discounts/add_discount_periods'; ?>',
                data: {'datefrom': datefrom,'dateto':dateto,'timefrom':timefrom,'timeto':timeto},
                success: function (data) {
                   $('#editdiscountperioddiv').empty();
          $.each(data.periods, function(k, v) {
           $("#editdiscountperioddiv").append('<li class="text-left" style="display:inline-block;margin-right:5px;"><button type="button" style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;outline:none;color:#4ca6cf;" class="dateremovebtn" onclick="editremovediscountperiod('+k+');"><i class="fa fa-close"></i></button>'+v["discountdatefrom"]+' to '+v["discountdateto"]+' '+v["discounttimefrom"]+' to '+v["discounttimeto"]+'</li>');
          
           });
          
                      }
            });
}
function editremovediscountperiod(val){

     $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/discounts/remove_discount_periods'; ?>',
                data: {'index': val},
                success: function (data) {
                   $('#editdiscountperioddiv').empty();
          $.each(data.periods, function(k, v) {
           $("#editdiscountperioddiv").append('<li class="text-left" style="display:inline-block;margin-right:5px;"><button type="button" style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;outline:none;color:#4ca6cf;" class="dateremovebtn" onclick="editremovediscountperiod('+k+');"><i class="fa fa-close"></i></button>'+v["discountdatefrom"]+' to '+v["discountdateto"]+' '+v["discounttimefrom"]+' to '+v["discounttimeto"]+'</li>');
          
           });
                }
            });
}

  });

</script>
