
                            <div class="row">
                         
                          
                            <!-- <form action="<?=base_url();?>admin/booking_config/statutadd" class="" method="post"  enctype="multipart/form-data"> -->
                    <?php  if($invoice): ?>

                            <div class="invoiceedit" >
                                  <?=form_open_multipart("admin/booking_config/invoicestatusedit")?>
                                <input type="hidden" name="id" value="<?= $invoice->id ?>">

                             <div class="col-md-12">   
                            <div class="col-md-2" style="margin-top: 5px;padding-left:0px;">
                                <div class="form-group">
                                <span>Statut</span>
                     <select class="form-control" name="statut" required>
                            
                            <option  <?php if ($invoice->statut == "1") { echo "selected" ; } ?>
                             value="1">Show</option>
                           <option  <?php if ($invoice->statut == "0") { echo "selected" ; } ?>
                             value="0">Hide</option>
                          
                        </select>
                            </div>
                              </div>  
                              
                               <div class="col-md-2" style="margin-top: 5px;">
                                <div class="form-group">
                                  <span>Invoice Validity Delay (days)</span>
                                    <select class="form-control"  name="invoicedelay" required>
                                      <option value="">Select</option>
                                      <option <?php if ($invoice->invoicedelay  == "15") {echo "selected"; } ?> value="15">15</option>
                                      <option <?php if ($invoice->invoicedelay  == "30") {echo "selected"; } ?>  value="30">30</option>
                                      <option <?php if ($invoice->invoicedelay  == "45") {echo "selected"; } ?> value="45">45</option>
                                      <option <?php if ($invoice->invoicedelay  == "60") {echo "selected"; } ?> value="60">60</option>
                                    </select>
                                  
                                </div>
                              </div>
                               <div class="col-md-2" style="margin-top: 5px;">
                                 <?php if(!empty($invoice->customicon)): ?>
                                   <div class="col-md-12"> 
                                       <img  id="invoicecustomicon_logo_preview" src="<?=base_url($invoice->customicon); ?>" style="width: 200px; height: 150px;border:1px solid #888;" >
                                      </div>
                                <?php else: ?>
                                   <div class="col-md-12">
                                    <img  id="invoicecustomicon_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                <?php endif; ?>
                                 
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="customicon" class="form-control-file" id="invoicecustomicon" onChange="invoicecustompreview(this)"   style="width:100%;">
                                  
                                  </div>
                                 </div>
                                                                  <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="customiconwidth" id="invoicecustomiconwidth" class="form-control" value="<?= $invoice->customiconwidth ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="customiconheight" id="invoicecustomiconheight" class="form-control" value="<?= $invoice->customiconheight ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Invoice Custom Image</span>
                                 </div>
                              </div>
                              <div class="col-md-2" style="margin-top: 5px;">
                                <?php if(!empty($invoice->statutpendingicon )): ?>
                                   <div class="col-md-12"> 
                                       <img  id="invoicestatutpending_logo_preview" src="<?=base_url($invoice->statutpendingicon ); ?>" style="width: 200px; height: 150px;border:1px solid #888;" >
                                      </div>
                                <?php else: ?>
                                   <div class="col-md-12">
                                    <img  id="invoicestatutpending_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                <?php endif; ?>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="statutpendingicon" class="form-control-file" id="editstatutpendingicon" onChange="invoicestatutpendingpreview(this)"   style="width:100%;">
                                   
                                  </div>
                                 </div>
                                                                  <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="pendingiconwidth" id="invoicependingiconwidth" class="form-control" value="<?= $invoice->pendingiconwidth ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="pendingiconheight" id="invoicependingiconheight" class="form-control" value="<?= $invoice->pendingiconheight ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Statut Pending Image</span>
                                 </div>
                              </div>
                               <div class="col-md-2" style="margin-top: 5px;">
                                <?php if(!empty($invoice->statutpaidicon )): ?>
                                   <div class="col-md-12"> 
                                       <img  id="invoicestatutpaid_logo_preview" src="<?=base_url($invoice->statutpaidicon ); ?>" style="width: 200px; height: 150px;border:1px solid #888;" >
                                      </div>
                                <?php else: ?>
                                   <div class="col-md-12">
                                    <img  id="invoicestatutpaid_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                <?php endif; ?>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="statutpaidicon" class="form-control-file" id="editstatutpaidicon" onChange="invoicestatutpaidpreview(this)"   style="width:100%;">
                                   
                                  </div>
                                 </div>
                                                                  <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="paidiconwidth" id="paidiconwidth" class="form-control" value="<?= $invoice->paidiconwidth ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="paidiconheight" id="paidiconheight" class="form-control" value="<?= $invoice->paidiconheight ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Statut Paid Image</span>
                                 </div>

                              </div>
                             
                                  <div class="col-md-2" style="margin-top: 5px;">
                                <?php if(!empty($invoice->statutcancelledicon)): ?>
                                   <div class="col-md-12"> 
                                       <img  id="invoicestatutcancelled_logo_preview" src="<?=base_url($invoice->statutcancelledicon ); ?>" style="width: 200px; height: 150px;border:1px solid #888;" >
                                      </div>
                                <?php else: ?>
                                   <div class="col-md-12">
                                    <img  id="invoicestatutcancelled_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                <?php endif; ?>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="statutcancelledicon" class="form-control-file" id="editstatutcancelledicon" onChange="invoicestatutcancelledpreview(this)"   style="width:100%;">
                                  
                                  </div>
                                 </div>
                                                                  <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                  <input type="number" name="cancellediconwidth" id="invoicecancellediconwidth" class="form-control" value="<?= $invoice->cancellediconwidth ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="cancellediconheight" id="invoicecancellediconheight" class="form-control" value="<?= $invoice->cancellediconheight ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Statut Cancelled Image</span>
                                 </div>
                              </div>
                             
                          
                            
                           
                              </div>
                              <div class="col-md-12" style="padding:0px;">
                             
                              <div class="col-md-6" style="margin-top: 5px;"  >
                                <div class="form-group">
                                  <span>Invoice Custom Texte</span>
                                  <textarea id="invoicecustomtextcontent" class="form-control" name="customtext" rows="8" cols="80"><?= $invoice->customtext ?></textarea>
                                </div>
                              </div>
                            </div>
                              <div class="col-md-12">   
                                                       
                                <button  class="btn btn-default"style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                                <a  href="<?= base_url(); ?>admin/booking_config" type="button" class="btn btn-default" style="float:right; margin-left:7px;">
                                  <span class="fa fa-close"> Cancel </span></a>
                           
                               </div> 
                                
                            <?php echo form_close(); ?>
                          
                            </div>
                        <?php else: ?>
                             <div class="invoiceadd" >
                                  <?=form_open_multipart("admin/booking_config/invoicestatusadd")?>
                              

                             <div class="col-md-12">   
                            <div class="col-md-2" style="margin-top: 5px;padding-left:0px;">
                                <div class="form-group">
                                <span>Statut</span>
                     <select class="form-control" name="statut" required>
                            
                            <option 
                             value="1">Show</option>
                           <option  
                             value="0">Hide</option>
                          
                        </select>
                            </div>
                              </div>  
                              
                              <div class="col-md-2" style="margin-top: 5px;">
                                <div class="form-group">

                                  <span>Invoice Validity Delay (days)</span>
                                    <select class="form-control"  name="invoicedelay" required>
                                      <option value="">Select</option>
                                      <option value="15">15</option>
                                      <option value="30">30</option>
                                      <option value="45">45</option>
                                      <option value="60">60</option>
                                    </select>
                                   
                                </div>
                              </div>
                               <div class="col-md-2" style="margin-top: 5px;">
                                  <div class="col-md-12">
                                    <img  id="invoicecustomicon_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="customicon" class="form-control-file" id="invoicecustomicon" onChange="invoicecustompreview(this)"  required style="width:100%;">
                                   
                                  </div>
                                 </div>
                                <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="customiconwidth" id="invoicecustomiconwidth" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="customiconheight" id="invoicecustomiconheight" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Invoice Custom Image</span>
                                 </div>
                              </div>
                              <div class="col-md-2" style="margin-top: 5px;">
                                  <div class="col-md-12">
                                    <img  id="invoicestatutpending_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="statutpendingicon" class="form-control-file" id="editstatutpendingicon" onChange="invoicestatutpendingpreview(this)"  required style="width:100%;">
                                   
                                  </div>
                                 </div>
                                                                  <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="pendingiconwidth" id="invoicependingiconwidth" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="pendingiconheight" id="invoicependingiconheight" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Statut Pending Image</span>
                                 </div>
                              </div>
                               <div class="col-md-2" style="margin-top: 5px;">
                                  <div class="col-md-12">
                                    <img  id="invoicestatutpaid_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="statutpaidicon" class="form-control-file" id="editstatutpaidicon" onChange="invoicestatutpaidpreview(this)"  required style="width:100%;">
                                  
                                  </div>
                                 </div>
                                                                  <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="paidiconwidth" id="paidiconwidth" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="paidiconheight" id="paidiconheight" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Statut Paid Image</span>
                                 </div>
                              </div>
                              <div class="col-md-2" style="margin-top: 5px;">
                                  <div class="col-md-12">
                                    <img  id="invoicestatutcancelled_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="statutcancelledicon" class="form-control-file" id="editstatutcancelledicon" onChange="invoicestatutcancelledpreview(this)"  required style="width:100%;">
                                    
                                  </div>
                                 </div>
                                                                  <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="cancellediconwidth" id="invoicecancellediconwidth" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="cancellediconheight" id="invoicecancellediconheight" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Statut Cancelled Image</span>
                                 </div>
                              </div>
                              
                          
                            
                           
                              </div>
                            <div class="col-md-12" style="padding:0px;">
                              
                               <div class="col-md-6" style="margin-top: 5px;"  id="pricemessage">
                                <div class="form-group">
                                  <span>Invoice Custom Texte</span>
                                  <textarea id="invoicecustomtextcontent" class="form-control" name="customtext" rows="8" cols="80"></textarea>
                                </div>
                              </div>
                            </div>
                              <div class="col-md-12">   
                                                       
                                <button  class="btn btn-default"style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                                <a  href="<?= base_url(); ?>admin/booking_config" type="button" class="btn btn-default" style="float:right; margin-left:7px;">
                                  <span class="fa fa-close"> Cancel </span></a>
                           
                               </div> 
                                
                            <?php echo form_close(); ?>
                          
                            </div>
                        <?php  endif; ?>
                      
                        </div>
           


<script type="text/javascript">
CKEDITOR.replace('invoicecustomtextcontent');
  
    
function invoicecustompreview(input) {

 if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
            var img = new Image;
            img.onload = function() {
              $('#invoicecustomiconwidth').val('');
               $('#invoicecustomiconwidth').val(img.width);
               $('#invoicecustomiconheight').val('');
               $('#invoicecustomiconheight').val(img.height);
            };
            img.src = reader.result;
            document.querySelector('#invoicecustomicon_logo_preview').setAttribute('src', reader.result);
          };
          reader.readAsDataURL(input.files[0]);
        }
}
function invoicestatutpendingpreview(input) {
 if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
            var img = new Image;
            img.onload = function() {
              $('#invoicependingiconwidth').val('');
               $('#invoicependingiconwidth').val(img.width);
               $('#invoicependingiconheight').val('');
               $('#invoicependingiconheight').val(img.height);
            };
            img.src = reader.result;
            document.querySelector('#invoicestatutpending_logo_preview').setAttribute('src', reader.result);
          };
          reader.readAsDataURL(input.files[0]);
        }
}
function invoicestatutpaidpreview(input) {
  if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
            var img = new Image;
            img.onload = function() {
              $('#paidiconwidth').val('');
               $('#paidiconwidth').val(img.width);
               $('#paidiconheight').val('');
               $('#paidiconheight').val(img.height);
            };
            img.src = reader.result;
            document.querySelector('#invoicestatutpaid_logo_preview').setAttribute('src', reader.result);
          };
          reader.readAsDataURL(input.files[0]);
        }
}
function invoicestatutcancelledpreview(input) {
  if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
            var img = new Image;
            img.onload = function() {
              $('#invoicecancellediconwidth').val('');
               $('#invoicecancellediconwidth').val(img.width);
               $('#invoicecancellediconheight').val('');
               $('#invoicecancellediconheight').val(img.height);
            };
            img.src = reader.result;
            document.querySelector('#invoicestatutcancelled_logo_preview').setAttribute('src', reader.result);
          };
          reader.readAsDataURL(input.files[0]);
        }
}
             </script>