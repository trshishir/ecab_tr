<div class="row">
  <div class="ListRideCat">
    <!-- <div class="col-md-12">
      <div class="page-action"style="float: right; margin-bottom:5px;" >
        <button class="btn btn-sm btn-default" onclick="rideCatAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>&nbsp;
        <button class="btn btn-sm btn-default" onclick="rideCatEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>&nbsp;
        <button class="btn btn-sm btn-default" onclick="rideCatDelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button>&nbsp;
      </div>
    </div> -->
  <input type="hidden" class="chk-AddRideCat-btn" value="">
  <div class="col-md-12">
    <div class="module-body table-responsive">
        <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
            <thead>
            <tr>
              <th class="no-sort text-center"style="width:2%;">#</th>
              <th class="text-center" style="width:3% !important;"><?php echo $this->lang->line('id'); ?></th>
              <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('date'); ?></th>
              <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('time'); ?></th>
              <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('added_by'); ?></th>
              <th class="column-first_name text-center" style="width:8%; ">Name</th>
              <th class="column-time text-center"style="width:8%;">Statut</th>
              <th class="text-center"style="width:8%;">Since</th>
              
              

            </tr>
            </thead>
            <tbody>
                <?php if (!empty($ride_category)): ?>
                <?php $cnt=1; ?>
                <?php foreach($ride_category as $key => $item):?>
                    <tr>
                        <td class="text-center"><input type="checkbox" class="chk-mainRideCat-template" data-input="<?=$item->id?>"></td>
                        <td class="text-center"><a href="javascript:void()" onclick="ridecatidEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->created_at)) .'0000'. $item->id;?></a></td>
                        <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                        <td class="text-center"><?=date("H:i:s", strtotime($item->since))?></td>
                      <td class="text-center"> 
                       <?php 
                                  $user=$this->bookings_config_model->getuser($item->user_id);
                                  $user=str_replace(".", " ", $user);
                                  echo $user;
                                  ?></td>
                        <td class="text-center"><?=$item->ride_cat_name;?></td>
                        
                        <td class="text-center"><?=$item->statut=='1'?'<span class="label label-success">Active</span>':'<span class="label label-danger">Hide</span>';?></td>
                        <td class="text-center"><?= timeDiff($item->since) ?></td>

                    </tr>
                    <?php $cnt++; ?>
                <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
        <br>
      </div>
  </div>
</div>

  <?=form_open("admin/booking_config/rideCatadd")?>
  <!-- <form action="<?=base_url();?>admin/booking_config/vatadd" class="" method="post"  enctype="multipart/form-data"> -->
  <div class="rideCatAdd" style="display: none;">
    <div class="col-md-12">
       <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;">Statut</span>
          <select class="form-control" name="ride_cat_statut" required>
          
            <option value="1">Show</option>
            <option value="2">Hide</option>
          </select>
        </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span style="font-weight: bold;"> Poi Category Name </span>
            <input type="text" class="form-control" name="ride_cat_name" placeholder="" value="">
        </div>
      </div>
     </div>


      <div class="col-md-12" style="margin-top: 5px;">
        <div class="form-group" style="float:right;">
        <br>
           <button type="button" class="btn btn-default"  onclick="cancelRideCat()"><span class="fa fa-close"> Cancel </span></button>
        <button  class="btn btn-default"style="margin-left:7px;"><span class="fa fa-save"></span> Save </button>
     
      </div>
    </div>
  </div>
  <?php echo form_close(); ?>
  <div class="rideCatEdit"  style="display:none;">
    <?=form_open("admin/booking_config/rideCatedit")?>
    <div class="rideCatEditajax">
    </div>
    <div class="col-md-12" style="margin-top: 5px;">
      <div class="form-group" style="float:right;">
      <br>
      <button type="button" class="btn btn-default"  onclick="cancelRideCat()"><span class="fa fa-close"> Cancel </span></button>
      <button  class="btn btn-default"style="margin-left:7px;"><span class="fa fa-save"></span> Save </button>
      
      </div>
    </div>
    <?php echo form_close(); ?>
  </div>
  <div class="col-md-12 rideCatDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
    <?=form_open("admin/booking_config/deleterideCat")?>
      <input  name="tablename" value="vbs_job_statut" type="hidden" >
      <input type="hidden" id="rideCatdeletid" name="delete_ride_id" value="">
      <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
      <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
      <button  class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>
      <a class="btn btn-default" style="cursor: pointer;" onclick="cancelRideCat()">No</a>
    <?php echo form_close(); ?>
  </div>
</div>


<script type="text/javascript">
function rideCatAdd()
{
  setupDivRideCat();
  $(".rideCatAdd").show();
}

function cancelRideCat()
{
  setupDivRideCat();
  $(".ListRideCat").show();
}

function rideCatEdit()
{
    var val = $('.chk-AddRideCat-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivRideCat();
        $(".rideCatEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/booking_config/get_ajax_ride_cat'; ?>',
                data: {'ride_cat_id': val},
                success: function (result) {
                  // alert(result);
                  document.getElementsByClassName("rideCatEditajax")[0].innerHTML = result;
                }
            });
}
function ridecatidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivRideCat();
      $(".rideCatEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/booking_config/get_ajax_ride_cat'; ?>',
              data: {'ride_cat_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("rideCatEditajax")[0].innerHTML = result;
              }
          });
}
function rideCatDelete()
{
  var val = $('.chk-AddRideCat-btn').val();
  if(val != "")
  {
  setupDivRideCat();
  $(".rideCatDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivRideCat()
{
  // $(".carCatEditEdit").hide();
  $(".rideCatAdd").hide();
  $(".rideCatEdit").hide();
  $(".ListRideCat").hide();
  $(".rideCatDelete").hide();
}
function setupDivRideCatConfig()
{
 $(".ListRideCat").show();
  $(".rideCatAdd").hide();
  $(".rideCatEdit").hide();
  $(".rideCatDelete").hide();
}
$('input.chk-mainRideCat-template').on('change', function() {
  $('input.chk-mainRideCat-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-AddRideCat-btn').val(id);
  $('#rideCatdeletid').val(id);
});

</script>
