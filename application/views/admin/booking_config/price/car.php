<?php
 if($this->session->userdata['freedates']){
 	$freedates=$this->session->userdata['freedates'];
 }



?>
<div class="row">
<div class="col-md-12 ListCar" >

	<input type="hidden" class="chk-AddCar-btn" value="">
	<div class="col-md-12" style="padding:0px;">
		<div class="module-body table-responsive">
			<table class="configTable table table-bordered table-striped  table-hover dataTable no-footer" cellspacing="0" data-selected_id="">
					<thead>
					<tr>
						<th class="no-sort text-center"style="width:2%;">#</th>
						<th class="column-id text-center" ><?php echo $this->lang->line('id'); ?></th>
						<th class="column-id text-center" ><?php echo $this->lang->line('date'); ?></th>
						<th class="column-id text-center" ><?php echo $this->lang->line('time'); ?></th>
						<th class="column-id text-center" ><?php echo $this->lang->line('added_by'); ?></th>
						<th class="column-id text-center" >Car Category</th>
						<th class="column-time text-center" >Distance: Price / Km</th>
						<th class="column-time text-center" >Time : Price / Minute</th>
						<th class="column-time text-center" >Approche Rate :  Price / Km</th>
						<th class="column-time text-center" >Waiting Time : PRICE / Minute</th>
						<th class="column-time text-center" >Pickup Fee</th>
						<th class="column-date text-center" style="width:3% !important;">Statut</th>
						<th class="text-center">Since</th>
            			
					</tr>
					</thead>
					<tbody>
							<?php $cnt=1; ?>
							<?php foreach($price_car as $key => $item):?>
									<tr>
										<td class="text-center">
												<input type="checkbox" class="chk-mainCar-template" data-input="<?=$item->id?>">
										</td>
											<td class="text-center"><a href="javascript:void()" onclick="caridEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->created_at)) .'0000'. $item->id;?></a></td>
											<td class="text-center"><?=from_unix_date($item->created_at)?></td>
											<td class="text-center"><?=date("H:i:s", strtotime($item->since));?></td>
											<td class="text-center"> 
											 <?php 
					                        $user=$this->bookings_config_model->getuser($item->user_id);
					                        $user=str_replace(".", " ", $user);
					                        echo $user;
					                        ?></td>
											<td class="text-center"><?php foreach($car_category as $value):  if($value->id === $item->car_category) {echo $value->car_cat_name;} endforeach ?> </td>
											<td class="text-center"><?=$item->price_km_approche?></td>
											<td class="text-center"><?=$item->price_trajet;?></td>
											<td class="text-center"><?=$item->price_trajet_retour;?></td>
											<td class="text-center"><?=$item->attente;?></td>
											<td class="text-center"><?=$item->pickup_fee;?></td>
											<td class="text-center"><?=$item->statut=='1'?'<span class="label label-success">Active</span>':'<span class="label label-danger">Hide</span>';?></td>
											<td class="text-center"><?= timeDiff($item->since) ?></td>

									</tr>
									<?php $cnt++; ?>
							<?php endforeach; ?>
					</tbody>
			</table>
			<br>
		</div>
	</div>
</div>
<div class="col-md-12 DeleteCar" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
	<?=form_open("admin/booking_config/carpricedelete")?>
		<!-- <input  name="tablename" value="vbs_job_statut" type="hidden" > -->
		<input type="hidden" id="cardeletid" name="delet_car_id" value="">
		<div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
		<button  class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> <?php echo $this->lang->line('yes'); ?> </button>
		<a class="btn btn-default" style="cursor: pointer;" onclick="cancelCar()"><?php echo $this->lang->line('no'); ?></a>
	<?php echo form_close(); ?>
</div>
<div class="col-md-12 AddCar" style="margin-bottom: 10px; display:none;">
					   <?=form_open("admin/booking_config/carpriceadd")?>
              <!-- <input type="hidden" name="price_car_id" value="<?=$value->id?>"> -->
                           <div class="row" style="margin-top: 10px;">
                           		<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span >Statut</span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
											
												<select class="form-control" name="statut" required="">
													
													<option value="1">Show</option>
													<option value="0">Hide</option>
												</select>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;padding-left: 10px;">
											<span >Car Category</span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<select class="form-control" name="car_category" required="">
												<option value="">Select</option>
												<?php foreach($car_category as $key => $value):?>
													<option value="<?=$value->id;?>"><?=$value->car_cat_name;?></option>
												<?php endforeach; ?>																				
											</select>
										</div>
									</div>
								</div>
                           </div>
                           <div class="row" style="margin-top: 10px;">
                           	<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span >Car Place</span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
											<select class="form-control" name="car_place" required="">
												<option value="">Select</option>
												<option value="0">Agency</option>
												<option value="1">Driver</option>
										  </select>
										</div>
									</div>
								</div>
                           </div>
							<div class="row" style="margin-top: 10px;">
							
								<div class="col-md-4">
								
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span >Distance : Price / Km </span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
												<input type="text"  name="price_km_approche"class="form-control" value="" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span>Night Fee</span>
										</div>
										<div class="col-md-2" style="padding: 0px;">
											<select style="width:100%" class="form-control" name="nightfeetype" required="">
												<option value="0">%</option>
												<option value="1">Fix</option>
											</select>
										</div>
										<div class="col-md-4" style="padding: 0px;">
											<input type="text" name="majoration_de_nuit"value=""  class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span>Driver Meal Cost</span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="panier_repas"value=""  class="form-control" required="">
										</div>
									</div>
								</div>
							</div>
							<div class="row" style="margin-top: 10px;">
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span >Time : Price / Minute</span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
											<input type="text" name="price_trajet"value=""  class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span>Sundays and Not working day fee</span>
										</div>
										<div class="col-md-2" style="padding: 0px;">
											<select style="width:100%" class="form-control" name="notworkfeetype" required="">
												<option value="0">%</option>
												<option value="1">Fix</option>
											</select>
										</div>
										<div class="col-md-4" style="padding: 0px;">
											<input type="text" name="supplement_jour_ferie"value=""  class="form-control" required="">
										</div>
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span>Driver Rest Cost</span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" id="driverrestcost" name="repos_compensateur"value=""  class="form-control" required="" readonly>
										</div>
									</div>
								</div>
							</div>
							
						   <div class="row" style="margin-top: 10px;">
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span>Approche Rate : Price / Km</span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
											<input type="text" name="price_trajet_retour" value="" class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span>1st Mai and 25 December Fee</span>
										</div>
										<div class="col-md-2" style="padding: 0px;">
											<select style="width:100%" class="form-control" name="decfeetype" required="">
												<option value="0">%</option>
												<option value="1">Fix</option>
											</select>
										</div>
										<div class="col-md-4"  style="padding: 0px;">
											<input type="text" name="supplement_1er" value="" class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									
								</div>
							</div>
							<div class="row" style="margin-top: 10px;">
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span>Return Rate : Price / Km</span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
											<input type="text" name="return_rate" value="" class="form-control" required="">
										</div>
									</div>
								</div>
							</div>
						   <div class="row" style="margin-top: 10px;">
						   
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span >Waiting Time : PRICE / Minute</span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
											<input type="text" name="attente" value="" class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4"  style="margin-top: 5px;">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 5px;">
											<span >Night Time</span>
										</div>
										<div class="col-md-6" style="padding: 0px;">

											<div class="col-md-2" style="padding:10px 0px 0px;">
										      <label for="nighttimefrom"><?php echo 'From'; ?></label>
										    </div>
										    <div class="col-md-4" style="padding-left:0px;">
										    <input   id="nighttimefrom" name="nighttimefrom" type="text" value="<?php echo date("h : i") ?>" style="width:120%;" />
										    </div>
										    <div class="col-md-2" style="padding-top:10px;">
										      <label for="nighttimeto"><?php echo 'To'; ?></label>
										    </div>
										    <div class="col-md-4" style="padding-left:0px;">
										    <input   id="nighttimeto" name="nighttimeto" type="text" value="<?php echo date("h : i") ?>" style="width:120%;" />
										    </div>
										</div>	
										
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span >Driver Rest (Hours)</span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											  
											
											  <div class="col-md-12" style="padding: 0px;">
											  	 <input type="number" name="driverresttimehour" min="0"   value=""   
											  	 class="form-control" id="driverresttimehour" required onchange="calDriverCost()">
											  	
											  </div>
										</div>
									</div>
								</div>
							</div>
						 
						   <div class="row" style="margin-top: 10px;">
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span>Pickup Fee</span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
											<input type="text" name="pickup_fee" value="" class="form-control" required="">
										</div>

									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span >Not working days</span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
										
											
									    	<div class="col-md-4" style="padding: 0px 0px 0px 5px;">
									    	   <select name="notworkingday" id="notworkingday" style="width:100%;" class="form-control" >
						                               <option value="1">1</option>
						                               <option value="2">2</option>
						                               <option value="3">3</option>
						                               <option value="4">4</option>
						                               <option value="5">5</option>
						                               <option value="6">6</option>
						                               <option value="7">7</option>
						                               <option value="8">8</option>
						                               <option value="9">9</option>
						                               <option value="10">10</option>
						                               <option value="11">11</option>
						                               <option value="12">12</option>
						                               <option value="13">13</option>
						                               <option value="14">14</option>
						                               <option value="15">15</option>
						                               <option value="16">16</option>
						                               <option value="17">17</option>
						                               <option value="18">18</option>
						                               <option value="19">19</option>
						                               <option value="20">20</option>
						                               <option value="21">21</option>
						                               <option value="22">22</option>
						                               <option value="23">23</option>
						                               <option value="24">24</option>
						                               <option value="25">25</option>
						                               <option value="26">26</option>
						                               <option value="27">27</option>
						                               <option value="28">28</option>
						                               <option value="29">29</option>
						                               <option value="30">30</option>
						                               <option value="31">31</option>


						                                 


						                       </select> 
									    	</div>
									    	<div class="col-md-5" style="padding: 0px 0px 0px 5px;">
											   <select name="notworkingmonth" id="notworkingmonth" style="width:100%;" class="form-control">
					                                <option value="1">January</option>
					                                <option value="2">February</option>
					                                <option value="3">March</option>
					                                <option value="4">April</option>
					                                <option value="5">May</option>
					                                <option value="6">June</option>
					                                <option value="7">July</option>
					                                <option value="8">August</option>
					                                <option value="9">September</option>
					                                <option value="10">October</option>
					                                <option value="11">November</option>
					                                <option value="12">December</option>
					                            </select> 
									    	</div>
									    	<div class="col-md-3" style="padding: 0px 0px 0px 5px;">
										        <button  type="button" id="notworkingbtn" class="btn btn-default btn-md" style="min-width:100%;">Add</button>
										    </div>

										</div>
										<div class="col-md-6 col-md-offset-5">
											<ul id="notworkingdaysdiv" style="border:0px;display:block!important;list-style-type:none;">
											<?php if($this->session->userdata['freedates']): ?>	
												<?php foreach($freedates as $key => $date): ?>
													<li class="text-left"  style="display:inline-block;margin-right:5px;"><button type="button" style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;outline:none;color:#4ca6cf;" class="dateremovebtn" onclick="freedaysremove(<?= $key ?>);"><i class="fa fa-close"></i>
													</button><?=$date["day"]?>  <?=$date["month"]?></li>
											    <?php endforeach; ?>
											    <?php endif; ?>  
											</ul>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span>Driver Working Hour Cost</span>
										</div>
										
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" name="diverbonusfee"value=""  class="form-control" id="driverworkinghourcost" required="" onkeyup="calDriverCost()">
										</div>
									</div>
								</div>
							</div>
							  <div class="row" style="margin-top: 10px;">
						     	<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span>Minimum Ride Price HT</span>
										</div>
										<div class="col-md-5" style="padding: 0px;">
											<input type="text" name="minimum_fee"value=""  class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span>Driver Meal</span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<div class="col-md-2" style="padding:10px 0px 0px;">
										      <label for="drivermealfrom"><?php echo 'From'; ?></label>
										    </div>
										    <div class="col-md-4" style="padding-left:0px;">
										    <input   id="drivermealfrom" name="drivermealfrom" type="text" value="<?php echo date("h : i") ?>" style="width:120%;" />
										    </div>
										    <div class="col-md-2" style="padding-top:10px;">
										      <label for="drivermealto"><?php echo 'To'; ?></label>
										    </div>
										    <div class="col-md-4" style="padding-left:0px;">
										    <input   id="drivermealto" name="drivermealto" type="text" value="<?php echo date("h : i") ?>" style="width:120%;" />
										    </div>
										   
										</div>
										<div class="col-md-6 col-md-offset-5" style="padding: 0px">
											 <div class="col-md-2" style="padding:10px 0px 0px;">
										      <label for="driverothermealfrom"><?php echo 'From'; ?></label>
										    </div>
										    <div class="col-md-4" style="padding-left:0px;">
										    <input   id="driverothermealfrom" name="driverothermealfrom" type="text" value="<?php echo date("h : i") ?>" style="width:120%;" />
										    </div>
										    <div class="col-md-2" style="padding-top:10px;">
										      <label for="driverothermealto"><?php echo 'To'; ?></label>
										    </div>
										    <div class="col-md-4" style="padding-left:0px;">
										    <input   id="driverothermealto" name="driverothermealto" type="text" value="<?php echo date("h : i") ?>" style="width:120%;" />
										    </div>
										</div>
									</div>
								</div>
						         <div class="col-md-4">
									<div class="form-group">
										<div class="col-md-5" style="margin-top: 10px;">
											<span >Driver Working Hours (sunday or not working day) to earn a rest day</span>
										</div>
										<div class="col-md-6" style="padding: 0px;">
											  
											
											  <div class="col-md-12" style="padding: 0px;">
											<input type="number" name="driverworkinghour" min="0"   value=""   
											  	 class="form-control" required >
											  	
											  </div>
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="row" style="margin-bottom: 10px;margin-top: 20px;">
								<div class="col-md-12" style="padding-right: 55px;">
									<div class="form-group clearfix">
										<button  class="btn btn-default" style="float:right;"><span class="fa fa-save"></span> <?php echo $this->lang->line('save'); ?> </button>
										<button type="button" class="btn btn-default" style="float:right; margin-right:10px;" onclick="cancelCar()"><span class="fa fa-close"> <?php echo $this->lang->line('cancel'); ?> </span></button>
										
									</div>
								</div>
							</div>
						<?php echo form_close(); ?>
						
	</div>
	<div class="col-md-12 carEdit" style="margin-bottom: 10px; display:none">
	  <?=form_open("admin/booking_config/carpriceedit")?>
	  <div class="carEditajax">
	  </div>

		<div class="clearfix"></div>
		<div class="row" style="margin-bottom: 10px;margin-top: 20px;">
			<div class="col-md-12" style="padding-right: 55px;">
				<div class="form-group">
					<button  class="btn btn-default" style="float:right;"><span class="fa fa-save"></span> <?php echo $this->lang->line('save'); ?> </button>
				<button type="button" class="btn btn-default " style="margin-right:10px; float:right;" onclick="cancelCar()"><span class="fa fa-close"> <?php echo $this->lang->line('cancel'); ?> </span></button>
			
					
				</div>
			</div>
		</div>
	  <?php echo form_close(); ?>
	
	</div>
</div>
	<script type="text/javascript">
    function calDriverCost(){
      
         var hour=$("#driverresttimehour").val();
         var cost=$("#driverworkinghourcost").val();
         if(cost=="" || cost ==undefined){
            cost="0";
         }
      
         if(hour=="" || hour ==undefined){
         	hour="0";
         }
       
         hour=parseInt(hour);
         cost=parseInt(cost);
	    
	     var  hours=hour;
	     var bonuscost=hours*cost;
	     document.getElementById('driverrestcost').value=bonuscost;
         //alert("day:"+day+"hour:"+hour+"cost:"+cost+"total hours:"+hours);
    }
     function editcalDriverCost(){
        
         var hour=$("#editdriverresttimehour").val();
         var cost=$("#editdriverworkinghourcost").val();
         if(cost=="" || cost ==undefined){
            cost="0";
         }
        
         if(hour=="" || hour ==undefined){
         	hour="0";
         }
       
         hour=parseInt(hour);
         cost=parseInt(cost);
	  
	     var  hours=hour;
	     var bonuscost=hours*cost;
	     document.getElementById('editdriverrestcost').value=bonuscost;
         //alert("day:"+day+"hour:"+hour+"cost:"+cost+"total hours:"+hours);
    }

    $('#notworkingbtn').click(function() {
    	 var months=$("#notworkingmonth").val();
         var days=$("#notworkingday").val();
        
             $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/booking_config/add_notworking_days'; ?>',
                data: {'month': months,'day':days},
                success: function (data) {
                   $('#notworkingdaysdiv').empty();
          $.each(data.dates, function(k, v) {
           $("#notworkingdaysdiv").append('<li class="text-left" style="display:inline-block;margin-right:5px;"><button type="button" style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;outline:none;color:#4ca6cf;" class="dateremovebtn" onclick="freedaysremove('+k+');"><i class="fa fa-close"></i></button>'+v["day"]+" "+v["month"]+'</li>');
          
           });
                }
            });
    //	 $('form#testform').submit();
    });

function editfreeworkingdates(){
	 var months=$("#editnotworkingmonth").val();
         var days=$("#editnotworkingday").val();
        
             $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/booking_config/add_notworking_days'; ?>',
                data: {'month': months,'day':days},
                success: function (data) {
                   $('#editnotworkingdaysdiv').empty();
          $.each(data.dates, function(k, v) {
           $("#editnotworkingdaysdiv").append('<li class="text-left" style="display:inline-block;margin-right:5px;"><button type="button" style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;outline:none;color:#4ca6cf;" class="dateremovebtn" onclick="editfreedaysremove('+k+');"><i class="fa fa-close"></i></button>'+v["day"]+" "+v["month"]+'</li>');
          
           });
                }
            });
}

  function freedaysremove(val){
 
  	 $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/booking_config/remove_notworking_days'; ?>',
                data: {'index': val},
                success: function (data) {
                   $('#notworkingdaysdiv').empty();
          $.each(data.dates, function(k, v) {
           $("#notworkingdaysdiv").append('<li class="text-left" style="display:inline-block;margin-right:5px;"><button type="button" style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;outline:none;color:#4ca6cf;" class="dateremovebtn" onclick="freedaysremove('+k+');"><i class="fa fa-close"></i></button>'+v["day"]+" "+v["month"]+'</li>');
          
           });
                }
            });
  }
function editfreedaysremove(val){
 
  	 $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/booking_config/remove_notworking_days'; ?>',
                data: {'index': val},
                success: function (data) {
                   $('#editnotworkingdaysdiv').empty();
          $.each(data.dates, function(k, v) {
           $("#editnotworkingdaysdiv").append('<li class="text-left" style="display:inline-block;margin-right:5px;"><button type="button" style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;outline:none;color:#4ca6cf;" class="dateremovebtn" onclick="editfreedaysremove('+k+');"><i class="fa fa-close"></i></button>'+v["day"]+" "+v["month"]+'</li>');
          
           });
                }
            });
  }



</script>

	
	<script type="text/javascript">
		
	function carAdd()
	{
	  setupDivCar();
	  $(".AddCar").show();
	}
	function cancelCar()
	{
	  setupDivCar();
	  $(".ListCar").show();
	}
	function setupDivCar()
	{
		$(".ListCar").hide();
		$(".AddCar").hide();
		$(".DeleteCar").hide();
		$(".carEdit").hide();
	}
	function setupDivCarConfig()
	{
		$(".ListCar").show();
		$(".AddCar").hide();
		$(".DeleteCar").hide();
		$(".carEdit").hide();
	}
	function CarDelete()
	{
	  var val = $('.chk-AddCar-btn').val();
	  if(val != "")
	  {
	  setupDivCar();
	  $(".DeleteCar").show();
	  }else{
	    alert('Please select a row to delete.');
	  }
	}
	function CarEdit()
	{
	    var val = $('.chk-AddCar-btn').val();
	    if (val=='')
	    {
	        alert("Please select record!");
	        return false;
	    }
	        setupDivCar();
	        $(".carEdit").show();
	        $.ajax({
	                type: "GET",
	                url: '<?php echo base_url().'admin/booking_config/get_ajax_price_car'; ?>',
	                data: {'car_id': val},
	                success: function (result) {
	                  // alert(result);
	                  document.getElementsByClassName("carEditajax")[0].innerHTML = result;
	                    $('#editnighttimefrom').timepicki({
					          step_size_minutes:'5',
					        show_meridian: false,
					        min_hour_value: 0,
					        max_hour_value: 23,
					        overflow_minutes: true,
					        increase_direction: 'up',
					        disable_keyboard_mobile: true});
	                    $('#editnighttimeto').timepicki({
					          step_size_minutes:'5',
					        show_meridian: false,
					        min_hour_value: 0,
					        max_hour_value: 23,
					        overflow_minutes: true,
					        increase_direction: 'up',
					        disable_keyboard_mobile: true});
	                     $('#editdrivermealfrom').timepicki({
					          step_size_minutes:'5',
					        show_meridian: false,
					        min_hour_value: 0,
					        max_hour_value: 23,
					        overflow_minutes: true,
					        increase_direction: 'up',
					        disable_keyboard_mobile: true});
					    $('#editdrivermealto').timepicki({
					          step_size_minutes:'5',
					        show_meridian: false,
					        min_hour_value: 0,
					        max_hour_value: 23,
					        overflow_minutes: true,
					        increase_direction: 'up',
					        disable_keyboard_mobile: true});
					    $('#editdriverothermealfrom').timepicki({
					          editstep_size_minutes:'5',
					        show_meridian: false,
					        min_hour_value: 0,
					        max_hour_value: 23,
					        overflow_minutes: true,
					        increase_direction: 'up',
					        disable_keyboard_mobile: true});
					    $('#editdriverothermealto').timepicki({
					          step_size_minutes:'5',
					        show_meridian: false,
					        min_hour_value: 0,
					        max_hour_value: 23,
					        overflow_minutes: true,
					        increase_direction: 'up',
					        disable_keyboard_mobile: true});
	                }
	            });
	}
	$('input.chk-mainCar-template').on('change', function() {
	  $('input.chk-mainCar-template').not(this).prop('checked', false);
	  var id = $(this).attr('data-input');
	  $('.chk-AddCar-btn').val(id);
	  $('#cardeletid').val(id);
	});
		function caridEdit(id)
	{
	   var val = id;
	    if (val=='')
	    {
	        alert("Please select record!");
	        return false;
	    }
	        setupDivCar();
	        $(".carEdit").show();
	        $.ajax({
	                type: "GET",
	                url: '<?php echo base_url().'admin/booking_config/get_ajax_price_car'; ?>',
	                data: {'car_id': val},
	                success: function (result) {
	                  // alert(result);
	                  document.getElementsByClassName("carEditajax")[0].innerHTML = result;
	                    $('#editnighttimefrom').timepicki({
					          step_size_minutes:'5',
					        show_meridian: false,
					        min_hour_value: 0,
					        max_hour_value: 23,
					        overflow_minutes: true,
					        increase_direction: 'up',
					        disable_keyboard_mobile: true});
	                    $('#editnighttimeto').timepicki({
					          step_size_minutes:'5',
					        show_meridian: false,
					        min_hour_value: 0,
					        max_hour_value: 23,
					        overflow_minutes: true,
					        increase_direction: 'up',
					        disable_keyboard_mobile: true});
	                     $('#editdrivermealfrom').timepicki({
					          step_size_minutes:'5',
					        show_meridian: false,
					        min_hour_value: 0,
					        max_hour_value: 23,
					        overflow_minutes: true,
					        increase_direction: 'up',
					        disable_keyboard_mobile: true});
					    $('#editdrivermealto').timepicki({
					          step_size_minutes:'5',
					        show_meridian: false,
					        min_hour_value: 0,
					        max_hour_value: 23,
					        overflow_minutes: true,
					        increase_direction: 'up',
					        disable_keyboard_mobile: true});
					    $('#editdriverothermealfrom').timepicki({
					          editstep_size_minutes:'5',
					        show_meridian: false,
					        min_hour_value: 0,
					        max_hour_value: 23,
					        overflow_minutes: true,
					        increase_direction: 'up',
					        disable_keyboard_mobile: true});
					    $('#editdriverothermealto').timepicki({
					          step_size_minutes:'5',
					        show_meridian: false,
					        min_hour_value: 0,
					        max_hour_value: 23,
					        overflow_minutes: true,
					        increase_direction: 'up',
					        disable_keyboard_mobile: true});
	                }
	            });
	}
	$('input.chk-mainCar-template').on('change', function() {
	  $('input.chk-mainCar-template').not(this).prop('checked', false);
	  var id = $(this).attr('data-input');
	  $('.chk-AddCar-btn').val(id);
	  $('#cardeletid').val(id);
	});
	</script>
