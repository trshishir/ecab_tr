<?php $locale_info = localeconv(); ?>
<style>
   
   
  .nav-tabs > li.active {
   background:linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%);
    border-bottom: none;
    }
    .nav-tabs>li.active>a{
         border: 1px solid #44c0e5!important;
    }
  .nav-tabs > li {
     background:linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%);
  
   border-left: 1px solid #44c0e5!important;
    height: 55px;
    margin: 0px !important;
    }

    .nav-tabs > li > a {

    background:linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%);
    color: #fff !important;
    font-size: 12px;
    padding-left: 10px;
    display: block !important;
    width: 100% !important;
    height: 100% !important;
    text-transform: uppercase;
    font-weight: bold;
    transition: 0.2s;
    outline: none;
    border: none;
    border-radius: 0px;
    line-height: 30px !important;
}
.dataTables_wrapper .dataTables_filter {
    float: left;
    text-align: left;
    margin-left: 10px;
}
input[type="search"]{
    border:1px solid #cccccc;
    padding-left: 5px;
    border-radius: 0px;
}
input[type="search"]:focus{
    border:1px solid #cccccc !important;
}
    
    .tab-pane{
        padding: 30px 30px 30px 14px;
    }
    .configTable{
        padding: 0px;
    }
    .dataTables_wrapper .dataTables_filter input {
    margin-right: 2px;
    margin-left: 0 !important;
    max-width: 100% !important;
    width: 100% !important;
    float: right;
    }
    .dataTables_wrapper{
        margin-top: 20px;
    }
   
    
  

</style>

<style>
 
.delete-icon {
    background: url(http://uniqueweb.co.in/project/ecabapp//assets/delete-icon.png) no-repeat left center !important;
    padding: 0px 0px 0px 22px;
}
.save-icon {
    background: url(http://uniqueweb.co.in/project/ecabapp//assets/save-icon.png) no-repeat left center !important;
    padding: 0px 0px 0px 22px;
}
#editnotworkingbtn,#notworkingbtn{
    background: linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%) !important;
}
.plusgreeniconconfig{
    border: 1px solid green;cursor: pointer;overflow: hidden;outline: none;font-size: 16px;
    color: #ffffff;
    height: 25px;
    width: 24px;
    border-radius: 50%;
    background-color: green;
    position: absolute;
    top: 22px;
    right: -20px;
    z-index: 10;
    text-align: center;
    margin: 0px;
}
.minusrediconconfig{
    border: 1px solid red;cursor: pointer;overflow: hidden;outline: none;font-size: 16px;
    color: #ffffff;
    height: 25px;
    width: 24px;
    border-radius: 50%;
    background-color: red;
    position: absolute;
    bottom: 9px;
    right: -20px;
    z-index: 10;
    text-align: center;
    margin: 0px;
}
.minusrediconconfig > i,.plusgreeniconconfig > i{
    margin:0px;
}
#editdriverrestcost,#driverrestcost{
    background-color: #add8e63d !important;
}
.dataTables_wrapper .dataTables_paginate .paginate_button{
   color:#fff !important;
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current{
   color:#fff !important;
}

.dataTables_wrapper .dataTables_paginate .paginate_button:hover {
     background: linear-gradient(to bottom, #0d85a8 0%, #35add1 39%,#44c0e5 100%) !important;
   
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
     background: linear-gradient(to bottom, #0d85a8 0%, #35add1 39%,#44c0e5 100%) !important;
    color:#fff !important;
}

</style>


<?php $active_status =  $this->session->flashdata('alert');?>
<?php if (!isset($active_status['price_stu_id']) ) {
  $active_status['price_stu_id']='';
} ?>
<?php if (!isset($active_status['status_id'])) {
  $active_status['status_id']='';
} ?>
<?php if ($active_status['status_id']!='') { $status=$active_status['status_id']; $tabstatus=$active_status['status_id']; }else{$status='13'; $tabstatus='13';} ?>
<?php if ($active_status['price_stu_id']!='') { $price_stu=$active_status['price_stu_id']; }else{$price_stu='1';} ?>
<section id="content"  style="min-height: auto !important;">
    <div class="listDiv"><!--col-md-10 padding white right-p-->
    <?php $this->load->view('admin/common/breadcrumbs');?>
        <?php $this->load->view('admin/common/alert');?>
        <?php echo $this->session->flashdata('message'); ?>
            <div class="row-fluid"  id="ShowTabs" style="margin-bottom: 10px; margin-left:0px;">
                <ul class="nav nav-tabs responsive">
                                            <li <?=$status==13? 'class="active"':''?>><a href="#STATUT" class="statut_E" role="tab" data-toggle="tab">STATUT </a></li>
                                            <li <?=$status==1? 'class="active"':''?>><a href="#VAT" class="vat_E" role="tab" data-toggle="tab">VAT</a></li>
                                           
                                            <li <?=$status==3? 'class="active"':''?>><a href="#SERVICE_CATEGORY" class="serviceCar_E" role="tab" data-toggle="tab">SERVICE CATEGORY</a></li>
                                            <li <?=$status==4? 'class="active"':''?>><a href="#SERVICES"role="tab" class="service_E" data-toggle="tab">SERVICES</a></li>
                                            <li <?=$status==5? 'class="active"':''?>><a href="#CLIENT_CATEGORY" class="clientCat_E" role="tab" data-toggle="tab">CLIENT CATEGORY</a></li>
                                            <li <?=$status==6? 'class="active"':''?>><a href="#CAR_CATEGORY" class="carCat_E" role="tab" data-toggle="tab">CAR CATEGORY</a></li>
                                            <li <?=$status==12? 'class="active"':''?>><a href="#PRICES" class="prices_E" role="tab" data-toggle="tab">Prices</a></li>
                                            <li <?=$status==9? 'class="active"':''?>><a href="#RIDE_CATEGORY" class="rideCat_E" role="tab" data-toggle="tab">POI CATEGORY</a></li>
                                            <li <?=$status==7? 'class="active"':''?>><a href="#POI" class="price_E" role="tab" data-toggle="tab">POI</a></li>
                                            <li <?=$status==8? 'class="active"':''?>><a href="#PACKAGES" class="package_E" role="tab" data-toggle="tab">PACKAGES</a></li>
                                             <li <?=$status==14? 'class="active"':''?>><a href="#DISABLECAT" class="disablecat_E" role="tab" data-toggle="tab">Disables Category </a></li>
                                            <li <?=$status==11? 'class="active"':''?>><a href="#PAYMENT_METHOD" class="paymentMethod_E" role="tab" data-toggle="tab">Payment Method</a></li>
                                            <li <?=$status==10? 'class="active"':''?>><a href="#PRICE_STATUS" class="pricestatus_E" role="tab" data-toggle="tab">Quote</a></li>
                                            <li <?=$status==15? 'class="active"':''?>><a href="#INVOICE_STATUS" class="invoicestatus_E" role="tab" data-toggle="tab">Invoice</a></li>

                                           
                                           
                                </ul>
                <div class="tab-content responsive" style="width:100%;display: table;">
                        <!--status tab starts-->
                        <div class="tab-pane fade <?=$status==1? 'in active':''?>" id="VAT" style="border: 1px solid #ccc;">
                            <div class="row">
                            <div class="Listvat">
                                <input type="hidden" class="chk-Addvat-btn" value="">
                            <!-- <div class="col-md-12 text-right">
                            <div style="margin-bottom: 5px;"><a href="javascript:void()" onclick="Vatadd()" class="btn btn-sm btn-default"><i class="fa fa-plus"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Add</font></font></a>
                            <a href="javascript:void()" onclick="VatEdit()" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Edit</font></font></a>
                            <a href="javascript:void()" onclick="VatDelete()" class="btn btn-sm btn-default"><i class="fa fa-trash"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Delete</font></font></a></div>
                                                            </div> -->
                            <div class="col-md-12">
                            <div class="module-body table-responsive">
                                <table class="configTable table table-bordered table-striped  table-hover dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
                                    <thead>
                                    <tr>
                                        <th class="no-sort text-center"style="width:2% !important;">#</th>
                                        <th class="text-center" style="width:3% !important;"><?php echo $this->lang->line('id'); ?></th>
                                        <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('date'); ?></th>
                                        <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('time'); ?></th>
                                        <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('added_by'); ?></th>
                                        <th class="column-first_name text-center" style="width:6% !important;"><?php echo $this->lang->line('name'); ?></th>
                                        <th class="text-center"style="width:8%;">Vat(%)</th>
                                        <th class="text-center"style="width:8%;">Statut</th>
                                        <th class="text-center"style="width:10%;">Since</th>
                                        
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php $cnt=1; ?>
                                        <?php if (!empty($vat_data)): ?>
                                        <?php foreach($vat_data as $key => $item):?>
                                            <tr>
                                                <td class="text-center">
                                                    <input type="checkbox" class="chk-mainvat-template" data-input="<?=$item->id?>"> 
                                                </td>
                                                <td class="text-center"><a href="javascript:void()" onclick="VatidEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->created_at)) .'0000'. $item->id;?></a></td>
                                                <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                                                <td class="text-center"><?=date("H:i:s", strtotime($item->since))?></td>
                                               <td class="text-center"> 
                                             <?php 
                                            $user=$this->bookings_config_model->getuser($item->user_id);
                                            $user=str_replace(".", " ", $user);
                                            echo $user;
                                            ?></td>
                                                <td class="text-center"><?=$item->name_of_var;?></td>
                                                <td class="text-center"><?=$item->vat; ?></td>
                                                <td class="text-center"><?=$item->statut=='1'?'<span class="label label-success">Active</span>':'<span class="label label-danger">Hide</span>';?></td>
                                                <td class="text-center"><?= timeDiff($item->since) ?></td>

                                            </tr>
                                            <?php $cnt++; ?>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                                <br>
                                </div>
                            </div>
                                                    </div>
                            <?=form_open("admin/booking_config/vatadd")?>
                            <!-- <form action="<?=base_url();?>admin/booking_config/vatadd" class="" method="post"  enctype="multipart/form-data"> -->
                            <div class="Vatadd" style="display: none;" >
                            <div class="col-md-12">
                             <div class="col-md-2" style="margin-top: 5px;">
                                <div class="form-group">
                                <span>Statut</span>
                                <select class="form-control" name="vat_statut" required>
                                   
                                    <option value="1">Show</option>
                                    <option value="2">Hide</option>
                                </select>
                                </div>
                            </div>
                            <div class="col-md-3" style="margin-top: 5px;">
                                <div class="form-group">
                                <span>Name Of Vat</span>
                                    <input type="text" class="form-control" name="name_of_vat" placeholder="" value="" required>
                                </div>
                            </div>
                            <div class="col-md-2" style="margin-top: 5px;">
                                <div class="form-group">
                                <span>Vat %</span>
                                    <input type="text" class="form-control" name="vat" placeholder="" value="" required>
                                </div>
                            </div>
                           
                           </div>
                           <div class="col-md-12">
                          
                                <button  class="btn btn-default"style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                                <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelVat()"><span class="fa fa-close"> Cancel </span></button>
                              
                             </div> 
                            <?php echo form_close(); ?>
                           
                            </div>
                            <div class="vatEdit" style="display:none">
                            <?=form_open("admin/booking_config/vatedit")?>
                            <div class="vatEditajax">
                            </div>
                             <div class="col-md-12">
                           
                            <button  class="btn btn-default"style=" float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                                <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelVat()"><span class="fa fa-close"> Cancel </span></button>
                             
                            </div>
                            <?php echo form_close(); ?>
                            </div>
                            <div class="col-md-12 vatDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
                                        <?=form_open("admin/booking_config/deleteVat")?>
                                            <input  name="tablename" value="vbs_job_statut" type="hidden" >
                                            <input type="hidden" id="vatdeletid" name="delet_vat_id" value="">
                                            <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10px;"> Are you sure you want to delete selected?</div>
                                            <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
                                <button  class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

                                            <a class="btn btn-default" style="cursor: pointer;" onclick="cancelVat()">No</a>
                                        <?php echo form_close(); ?>
                                    </div>
                        </div>
                        </div>
                        
                          <div class="tab-pane fade <?=$status== '14'? 'in active':''?>" id="DISABLECAT" style="border: 1px solid #ccc;">
                        <?php include('disables_category.php'); ?>
                        </div>
                         <div class="tab-pane fade <?=$status== '13'? 'in active':''?>" id="STATUT" style="border: 1px solid #ccc;">
                        <?php include('booking_status.php'); ?>
                        </div>
                       
                        <div class="tab-pane fade <?=$status== '3'? 'in active':''?>" id="SERVICE_CATEGORY" style="border: 1px solid #ccc;">
                        <?php include('service_category.php'); ?>
                        </div>

                        <div class="tab-pane fade <?=$status== '4'? 'in active':''?>"  id="SERVICES" style="border: 1px solid #ccc;">
                        <?php include('service.php'); ?>
                        </div>
                        <div class="tab-pane fade <?=$status== '5'? 'in active':''?>" id="CLIENT_CATEGORY" style="border: 1px solid #ccc;">
                        <?php include('client_category.php'); ?>
                        </div>
                        <div class="tab-pane fade <?=$status== '6'? 'in active':''?>" id="CAR_CATEGORY" style="border: 1px solid #ccc;">
                        <?php include('car_category.php'); ?>
                        </div>
                        <div class="tab-pane fade <?=$status== '9'? 'in active':''?>" id="RIDE_CATEGORY" style="border: 1px solid #ccc;">
                        <?php include('ride_category.php'); ?>
                        </div>
                        <div class="tab-pane fade <?=$status== '7'? 'in active':''?>" id="POI" style="border: 1px solid #ccc;">
                        <?php include('book_poi.php')?>
                        </div>
                         <div class="tab-pane fade <?=$status== '11'? 'in active':''?>" id="PAYMENT_METHOD" style="border: 1px solid #ccc;">
                        <?php include('payment_method.php')?>
                        </div>
                        <div class="tab-pane fade <?=$status== '10'? 'in active':''?>" id="PRICE_STATUS" style="border: 1px solid #ccc;">
                        <?php include('quoteprice.php')?>
                        </div>
                        <div class="tab-pane fade <?=$status== '15'? 'in active':''?>" id="INVOICE_STATUS" style="border: 1px solid #ccc;">
                        <?php include('invoices.php')?>
                        </div>
                        <div class="tab-pane fade <?=$status== '12'? 'in active':''?>" id="PRICES" style="border: 1px solid #ccc;">
                        <?php include('price/car.php')?>
                        </div>
                        <div class="tab-pane fade <?=$status== '8'? 'in active':''?>" id="PACKAGES" style="border: 1px solid #ccc;">
                        <?php include('book_package.php')?>
                        </div>
                        
                       

            </div>
            <!--/.module-->
        </div>

    </div>
</section>
<script>
    jQuery(function($){
        $.datepicker.regional['fr'] = {
            closeText: 'Fermer',
            prevText: '&#x3c;Pr�c',
            nextText: 'Suiv&#x3e;',
            currentText: 'Aujourd\'hui',
            monthNames: ['Janvier','Fevrier','Mars','Avril','Mai','Juin',
                'Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
            monthNamesShort: ['Jan','Fev','Mar','Avr','Mai','Jun',
                'Jul','Aou','Sep','Oct','Nov','Dec'],
            dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
            dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
            dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
            weekHeader: 'Sm',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: '',
            maxDate: '+12M +0D',
            showButtonPanel: true
        };
        $.datepicker.setDefaults($.datepicker.regional['fr']);
       
    });

    $(document).ready(function(){
        $( ".datepicker" ).datepicker({
            dateFormat: "dd-mm-yy",
            regional: "fr"
        });
        $('#MainAdd').click(function(){

            $('#MainTab').hide();
            $('#ShowTabs').attr('class','row');

        });

    });

     function removeAllResources()
    {
      setupDivCarConfig();
      setupDivPackageConfig();
      setupDivPoiConfig();
      setupDivStatutConfig();
      setupDivDisablecatConfig();
      setupDivCarCatConfig();
      setupDivClientCatConfig();
      setupDivPayment_methodConfig();
      setupDivRideCatConfig();
      setupDivServiceConfig();
      setupDivService_catConfig();
      setupDivVatConfig();
     



    }
    // Passengers part

    function AddPassengers()
    {
        setupDivPassengers();
        $(".AddPassengers").show();
    }

    function cancelPassengers()
    {
        setupDivPassengers();
        $(".ListPassengers").show();
    }

    function setupDivPassengers()
    {
        $(".AddPassengers").hide();
        $(".ListPassengers").hide();
        $(".EditPassengers").hide();
        $(".DeletePassengers").hide();
    }


    // Vat part

    function Vatadd()
    {
        setupDivVat();
        $(".Vatadd").show();
    }

    function cancelVat()
    {
        setupDivVat();
        $(".Listvat").show();
    }
    function VatDelete()
    {
        var val = $('.chk-Addvat-btn').val();
        if(val != "")
        {
            setupDivVat();
            $(".vatDelete").show();
        }else{
            alert('Please select a row to delete.');
        }
    }
    function VatEdit()
    {
        var val = $('.chk-Addvat-btn').val();
        if (val=='')
        {
            alert("Please select record!");
            return false;
        }
        setupDivVat();
        $(".vatEdit").show();
        $.ajax({
            type: "GET",
            url: '<?php echo base_url().'admin/booking_config/get_ajax_vat'; ?>',
            data: {'vat_id': val},
            success: function (result) {
                // alert(result);
                document.getElementsByClassName("vatEditajax")[0].innerHTML = result;
            }
        });
    }
    function VatidEdit(id){
        var val = id;
        if (val=='')
        {
            alert("Please select record!");
            return false;
        }
        setupDivVat();
        $(".vatEdit").show();
        $.ajax({
            type: "GET",
            url: '<?php echo base_url().'admin/booking_config/get_ajax_vat'; ?>',
            data: {'vat_id': val},
            success: function (result) {
                // alert(result);
                document.getElementsByClassName("vatEditajax")[0].innerHTML = result;
            }
        });
    }
    function setupDivVat()
    {
        $(".Vatadd").hide();
        $(".vatEdit").hide();
        $(".Listvat").hide();
        $(".vatDelete").hide();

    }
      function setupDivVatConfig()
    {
          $(".Listvat").show();
        $(".Vatadd").hide();
        $(".vatEdit").hide();
        $(".vatDelete").hide();

    }
    // $('input.chk-mainvat-template').on('change', function() {
    //     $('input.chk-mainvat-template').not(this).prop('checked', false);
    //     var id = $(this).attr('data-input');
    //     console.log(id)
    //     $('.chk-Addvat-btn').val(id);
    //     $('#vatdeletid').val(id);
    // });
    $(document).on('change', 'input.chk-mainvat-template', function() {
        $('input.chk-mainvat-template').not(this).prop('checked', false);
        var id = $(this).attr('data-input');
        console.log(id)
        $('.chk-Addvat-btn').val(id);
        $('#vatdeletid').val(id);
    });

    function getCaraddes(){
        $.ajax({
            url:"<?php echo base_url('admin/getCaradded') ?>",
            method:"GET",
            success:function(success){
                $('#tableBody').html(success);
            },error:function(xhr){
                console.log(xhr.responseText);
            }
        });
    }


    $('#save').on('click',function(){
        vatevent();
    })

</script>
<script>
   
$(document).ready(function () {
    $(window).on('load', function() {
        <?php if ($tabstatus=='1') { ?>
        vatEvent();
        <?php }else if ($tabstatus=='2'){ ?>
        discountEvent();
        <?php }else if ($tabstatus=='3'){ ?>
        serviceCatEvent();
        <?php }else if ($tabstatus=='4'){ ?>
        serviceEvent();
        <?php }else if ($tabstatus=='5'){ ?>
        clientCatEvent();
        <?php }else if ($tabstatus=='6'){ ?>
        carCatEvent();
        <?php }else if ($tabstatus=='7'){ ?>
        poiCatEvent();
        <?php }else if ($tabstatus=='8'){ ?>
        packageCatEvent();
        <?php }else if ($tabstatus=='9'){ ?>
        rideCatEvent();
        <?php }else if ($tabstatus=='11'){ ?>
            paymentMethodEvent()
        <?php }else if ($tabstatus=='10'){ ?>
            pricestatusEvent()
        <?php }else if ($tabstatus=='12'){ ?>
            pricesEvent()
        <?php }else if ($tabstatus=='13'){ ?>
        statutEvent();
        <?php }else if ($tabstatus=='14'){ ?>
        disablecategoryEvent();
        <?php }else if ($tabstatus=='15'){  ?>
         invoicestatusEvent();
        <?php } ?>

    });
    $('.vat_E').click(function(){
        vatEvent();
    });
    
     $('.disablecat_E').click(function(){
        disablecategoryEvent();
    });
    $('.statut_E').click(function(){
        statutEvent();
    });
    $('.discount_E').click(function(){
        discountEvent();
    });
    $('.serviceCar_E').click(function(){
        serviceCatEvent();
    });
    $('.service_E').click(function(){
        serviceEvent();
    });
    $('.quoteconfig_E').click(function(){
        quoteconfigEvent();
    });
    $('.clientCat_E').click(function(){
        clientCatEvent();
    });
    $('.carCat_E').click(function(){
        carCatEvent();
    });
    $('.price_E').click(function(){
        poiCatEvent();
    });
    $('.package_E').click(function(){
        packageCatEvent();
    });
    $('.rideCat_E').click(function(){
        rideCatEvent();
    });
    $('.paymentMethod_E').click(function(){
        paymentMethodEvent();
    });
    $('.pricestatus_E').click(function(){
        pricestatusEvent();
    });
     $('.invoicestatus_E').click(function(){
        invoicestatusEvent();
    });
    $('.prices_E').click(function(){
        pricesEvent();
    });
    function vatEvent(){
        removeAllResources();
        console.log("vat clicked");
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Vatadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="VatEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="VatDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
    
     function statutEvent(){
        removeAllResources();
        $('.removeevent').remove();
         var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Statutadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="StatutEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="StatutDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
     }
      function disablecategoryEvent(){
        removeAllResources();
        $('.removeevent').remove();
         var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Disablecatadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="DisablecatEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="DisablecatDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
     }
    function discountEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Discountadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="DiscountEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="DiscountDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
    function serviceCatEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="ServiceCatAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="ServiceCatEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="ServiceCatDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
    function serviceEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="ServiceAdd()"><i class="fa fa-plus"></i>  <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="ServiceEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="ServiceDelete()"><i class="fa fa-trash"></i>Delete</span></button></div>';
        $('.addevent').append(html);
    }
     function quoteconfigEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="QuoteconfigAdd()"><i class="fa fa-plus"></i>  <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="QuoteconfigEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="QuoteconfigDelete()"><i class="fa fa-trash"></i>Delete</span></button></div>';
        $('.addevent').append(html);
    }
    function clientCatEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="ClientCatAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="ClientCatEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="ClientCatDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }

    function carCatEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="carCatAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="carCatEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="carCatDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
    function rideCatEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="rideCatAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="rideCatEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="rideCatDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
    function poiCatEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Poiadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="poiEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="PoiDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
    function packageCatEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="packageadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="packageedit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="packagedelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
    function paymentMethodEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="PaymentMethodAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="PaymentMethodEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="PaymentMethodDelete()"><i class="fa fa-trash"></i>Delete</span></button></div>';
        $('.addevent').append(html);
    }
    function pricestatusEvent(){
        removeAllResources();
        $('.removeevent').remove();
        
       
    }
     function invoicestatusEvent(){
        removeAllResources();
         $('.removeevent').remove();    
    }
    function pricesEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="carAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="CarEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="CarDelete()"><i class="fa fa-trash"></i>Delete</span></button></div>';
        $('.addevent').append(html);
    }
});

</script>
