 <div class="row">
  <div class="col-md-12 ListPoi">
    <input type="hidden" class="chk-Addpoi-btn" value="">
    <!-- <div class="col-md-12">
        <div class="toolbar"style="float: right; margin-bottom:5px;">
          <button class="btn btn-sm btn-default" onclick="Poiadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>&nbsp;
          <button class="btn btn-sm btn-default" onclick="poiEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>&nbsp;
          <button class="btn btn-sm btn-default" onclick="PoiDelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button>&nbsp;
        </div>
    </div> -->
    <div class="col-md-12" style="padding:0px">
      <div class="module-body table-responsive">
        <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center"style="width:2%;">#</th>
            <th class="text-center" style="width:3% !important;"><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('added_by'); ?></th>
            <th class="column-first_name text-center" style="width:8%;"><?php echo $this->lang->line('name'); ?></th>
            <th class="column-date text-center"style="width:8%;"><?php echo $this->lang->line('category'); ?></th>
            <th class="column-time text-center"style="width:8%;"><?php echo $this->lang->line('address'); ?></th>
            <th class="column-time text-center"style="width:8%;">Zip code</th>
            
            <th class="column-time text-center"style="width:8%;"><?php echo $this->lang->line('city'); ?></th>
            <th class="column-time text-center"style="width:8%;">Statut</th>
            <th class="text-center"style="width:8%;">Since</th>

          </tr>
          </thead>
          <tbody>
              <?php if (!empty($poi_data)): ?>
              <?php $cnt=1; ?>
              <?php foreach($poi_data as $key => $item):?>
                  <tr>
                    <td class="text-center">
                        <input type="checkbox" class="chk-mainpoi-template" data-input="<?=$item->id?>">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="poiidEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->created_at)) .'0000'. $item->id;?></a></td>
                      <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                      <td class="text-center"><?=date("H:i:s", strtotime($item->since))?></td>
                     <td class="text-center"> 
                                <?php 
                                  $user=$this->bookings_config_model->getuser($item->user_id);
                                  $user=str_replace(".", " ", $user);
                                  echo $user;
                                  ?></td>
                      <td class="text-center"><?=$item->name;?></td>
                      <td class="text-center"><?= $this->bookings_config_model->getpoicatgorybyid('vbs_u_ride_category',$item->category_id);?></td>
                      <td class="text-center"><?=$item->address;?></td>
                      <td class="text-center"><?=$item->postal; ?></td>
                      <td class="text-center"><?=$item->ville;?></td>
                       <td class="text-center"><?=$item->statut=='1'?'<span class="label label-success">Active</span>':'<span class="label label-danger">Hide</span>';?></td>
                      <td class="text-center"><?= timeDiff($item->since) ?></td>
                  </tr>
                  <?php $cnt++; ?>
              <?php endforeach; ?>
              <?php endif; ?>
          </tbody>
      </table>
      </div>
    </div>
  </div>
  <div class="Poiadd" style="display: none;padding-left: 20px;" >
      <?=form_open("admin/booking_config/poiadd")?>
      <div class="row" style="margin-top: 10px;">
         <div class="col-md-2" style="margin-top: 5px;">
            <div class="form-group">
              <span>Statut</span>
              <select class="form-control" name="poi_cat_statut" required>
                <option value="1">Show</option>
                <option value="2">Hide</option>
              </select>
            </div>
          </div>
              <div class="col-md-2" style="margin-top: 5px;">
                <div class="form-group">
                  <span>Poi Category</span>
                  <select class="form-control" name="category" required style="background: #fff !important;">
                    <?php foreach ($poi_categories as $key => $item):?>
                      <option value="<?= $item->id ?>"><?= $item->ride_cat_name ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
            </div>
           <div class="col-md-2" style="margin-top: 5px;">
               <div class="form-group">
                 <span >Name of POI</span>
                  <input type="text" class="form-control" name="name" placeholder="" value="" required>
                </div>
            </div>
      
       
      </div>
      <div class="row" style="margin-top: 10px;">
        <div class="col-md-2" style="margin-top: 5px;">
            <div class="form-group">
              <span><?php echo $this->lang->line('address'); ?></span>
              <input type="text" class="form-control" name="address" placeholder="" value="" required>
            </div>
        </div>
         <div class="col-md-2" style="margin-top: 5px;">
            <div class="form-group">
                <span>Zip code</span>
                <input type="text" class="form-control" name="postal" placeholder="" value="" required>
            </div>
        </div>
        <div class="col-md-2" style="margin-top: 5px;">
            <div class="form-group">
                <span ><?php echo $this->lang->line('city'); ?></span>
                <input type="text" class="form-control" name="ville" placeholder="" value="" required>
            </div>
        </div>
      </div>
      
      <div class="row" style="margin-top: 10px;">
        <div class="col-md-12" style="margin-top: 5px;">
        <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
            <a href="javascript:void()" class="btn btn-default" style="float:right; margin-left:7px;color:#555;" onclick="cancelPoi()"><span class="fa fa-close"> Cancel </span></a>
      
        </div>
      </div>
    <?php echo form_close(); ?>
  </div>
  <?=form_open("admin/booking_config/poiedit")?>
<div class="poiEdit" style="display:none;padding-left: 20px;">
  <div class="poiEditajax">
  </div>
  <div class="row" style="margin-top: 10px;">
			<div class="col-md-12" style="margin-top: 5px;">
      <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
        <a href="javascript:void()" class="btn btn-default" style="float:right; margin-left:7px;color:#555;" onclick="cancelPoi()"><span class="fa fa-close"> Cancel </span></a>
				
			</div>
		</div>
</div>
<?php echo form_close(); ?>
<div class="col-md-12 PoiDelete" style="padding:20px  0px;margin-left: 15px;margin-top: 15px; display: none;">
    <?=form_open("admin/booking_config/poidel")?>
    <input  name="tablename" value="vbs_job_statut" type="hidden" >
    <input type="hidden" id="poideleteid" name="poideleteid" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?
    </div>
    <button  class="btn"style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

    <a class="btn" style="cursor: pointer;" onclick="cancelPoi()">No</a>
  <?php echo form_close(); ?>
</div>
</div>
<script type="text/javascript">
function Poiadd()
{
  setupDivPoi();
  $(".Poiadd").show();
}

function cancelPoi()
{
  setupDivPoi();
  $(".ListPoi").show();
}
function poiEdit()
{
    var val = $('.chk-Addpoi-btn').val();

    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivPoi();
        $(".poiEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/booking_config/get_ajax_poi'; ?>',
                data: {'poi_id': val},
                success: function (result) {
                  document.getElementsByClassName("poiEditajax")[0].innerHTML = result;
                }
            });
}
function poiidEdit(id){
  var val = id;

  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivPoi();
      $(".poiEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/booking_config/get_ajax_poi'; ?>',
              data: {'poi_id': val},
              success: function (result) {
                document.getElementsByClassName("poiEditajax")[0].innerHTML = result;
              }
          });
}
function PoiDelete()
{
  var val = $('.chk-Addpoi-btn').val();
  if (val=='')
    {
        alert("Please select record!");
        return false;
    }
  $('#poideleteid').val(val);
  setupDivPoi();
  $(".PoiDelete").show();
}

function setupDivPoi()
{
  $(".poiEdit").hide();
  $('.Poiadd').hide();
  $('.ListPoi').hide();
  $('.PoiDelete').hide();
}
function setupDivPoiConfig()
{
   $('.ListPoi').show();
  $(".poiEdit").hide();
  $('.Poiadd').hide();
  $('.PoiDelete').hide();
}
$('input.chk-mainpoi-template').on('change', function() {
  $('input.chk-mainpoi-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Addpoi-btn').val(id);

});

</script>
