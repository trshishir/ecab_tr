
                            <div class="row">
                            <div class="Liststatut">
                                <input type="hidden" class="chk-Addstatut-btn" value="">
                   
                            <div class="col-md-12">
                            <div class="module-body table-responsive">
                                <table class="configTable table table-bordered table-striped  table-hover dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
                                    <thead>
                                    <tr>
                                        <th class="no-sort text-center"style="width:2% !important;">#</th>
                                        <th class="text-center" style="width:3% !important;"><?php echo $this->lang->line('id'); ?></th>
                                        <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('date'); ?></th>
                                        <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('time'); ?></th>
                                        <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('added_by'); ?></th>
                                        <th class="column-first_name text-center" style="width:6% !important;"><?php echo $this->lang->line('name'); ?></th>
                                        <th class="text-center"style="width:8%;">Statut</th>
                                        <th class="text-center"style="width:10%;">Since</th>
                                        
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php $cnt=1; ?>
                                        <?php if (!empty($statut_data)): ?>
                                        <?php foreach($statut_data as $key => $item):?>
                                            <tr>
                                                <td class="text-center">
                                                    <input type="checkbox" class="chk-mainstatut-template" data-input="<?=$item->id?>"> 
                                                </td>
                                                <td class="text-center"><a href="javascript:void()" onclick="StatutidEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->created_at)) .'0000'. $item->id;?></a></td>
                                                <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                                                <td class="text-center"><?=date("H:i:s", strtotime($item->since))?></td>
                                               <td class="text-center"> 
                                             <?php 
                                            $user=$this->bookings_config_model->getuser($item->user_id);
                                            $user=str_replace(".", " ", $user);
                                            echo $user;
                                            ?></td>
                                                <td class="text-center"><?=$item->name_of_var;?></td>
                                                <td class="text-center"><?=$item->statut=='1'?'<span class="label label-success">Active</span>':'<span class="label label-danger">Hide</span>';?></td>
                                                <td class="text-center"><?= timeDiff($item->since) ?></td>

                                            </tr>
                                            <?php $cnt++; ?>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                                <br>
                                </div>
                            </div>
                                                    </div>
                            <?=form_open("admin/booking_config/statutadd")?>
                            <!-- <form action="<?=base_url();?>admin/booking_config/statutadd" class="" method="post"  enctype="multipart/form-data"> -->
                            <div class="Statutadd" style="display: none;" >


                             <div class="col-md-12">   
                            <div class="col-md-2" style="margin-top: 5px;">
                                <div class="form-group">
                                <span>Statut</span>
                                <select class="form-control" name="statut" required>
                                    
                                    <option value="1">Show</option>
                                    <option value="2">Hide</option>
                                </select>
                            </div>
                              </div>  
                            <div class="col-md-3" style="margin-top: 5px;">
                                <div class="form-group">
                                <span>Name Of Statut</span>
                                    <input type="text" class="form-control" name="name_of_statut" placeholder="" value="">
                                </div>
                            </div>
                            
                           
                              </div>
                              <div class="col-md-12">   
                                                       
                                <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                                <button  type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelStatut()"><span class="fa fa-close"> Cancel </span></button>
                           
                               </div> 
                                
                            <?php echo form_close(); ?>
                          
                            </div>
                            <div class="statutEdit" style="display:none">
                            <?=form_open("admin/booking_config/statutedit")?>
                            <div class="statutEditajax">
                            </div>
                             <div class="col-md-12">   
                            
                            <button  class="btn btn-default" style=" float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                                <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelStatut()"><span class="fa fa-close"> Cancel </span></button>
                    
                           
                        </div>
                            <?php echo form_close(); ?>
                            </div>
                            <div class="col-md-12 statutDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
                                        <?=form_open("admin/booking_config/deleteStatut")?>
                                            <input  name="tablename" value="vbs_job_statut" type="hidden" >
                                            <input type="hidden" id="statutdeletid" name="delet_statut_id" value="">
                                            <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10px;"> Are you sure you want to delete selected?</div>
                                            <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
                                <button  class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

                                            <a class="btn btn-default" style="cursor: pointer;" onclick="cancelStatut()">No</a>
                                        <?php echo form_close(); ?>
                                    </div>
                        </div>
           


             <script type="text/javascript">
                 
                    function Statutadd()
    {
        setupDivStatut();
        $(".Statutadd").show();
    }

    function cancelStatut()
    {
        setupDivStatut();
        $(".Liststatut").show();
    }
    function StatutDelete()
    {
        var val = $('.chk-Addstatut-btn').val();
        if(val != "")
        {
            setupDivStatut();
            $(".statutDelete").show();
        }else{
            alert('Please select a row to delete.');
        }
    }
    function StatutEdit()
    {
        var val = $('.chk-Addstatut-btn').val();
        if (val=='')
        {
            alert("Please select record!");
            return false;
        }
        setupDivStatut();
        $(".statutEdit").show();
        $.ajax({
            type: "GET",
            url: '<?php echo base_url().'admin/booking_config/get_ajax_statut'; ?>',
            data: {'statut_id': val},
            success: function (result) {
                // alert(result);
                document.getElementsByClassName("statutEditajax")[0].innerHTML = result;
            }
        });
    }
    function StatutidEdit(id){
        var val = id;
        if (val=='')
        {
            alert("Please select record!");
            return false;
        }
        setupDivStatut();
        $(".statutEdit").show();
        $.ajax({
            type: "GET",
            url: '<?php echo base_url().'admin/booking_config/get_ajax_statut'; ?>',
            data: {'statut_id': val},
            success: function (result) {
                // alert(result);
                document.getElementsByClassName("statutEditajax")[0].innerHTML = result;
            }
        });
    }
    function setupDivStatut()
    {
        $(".Statutadd").hide();
        $(".statutEdit").hide();
        $(".Liststatut").hide();
        $(".statutDelete").hide();

    }
     function setupDivStatutConfig()
    {
        $(".Statutadd").hide();
        $(".statutEdit").hide();
         $(".Liststatut").show();
        $(".statutDelete").hide();

    }
    // $('input.chk-mainstatut-template').on('change', function() {
    //     $('input.chk-mainstatut-template').not(this).prop('checked', false);
    //     var id = $(this).attr('data-input');
    //     console.log(id)
    //     $('.chk-Addstatut-btn').val(id);
    //     $('#statutdeletid').val(id);
    // });
    $(document).on('change', 'input.chk-mainstatut-template', function() {
        $('input.chk-mainstatut-template').not(this).prop('checked', false);
        var id = $(this).attr('data-input');
        console.log(id)
        $('.chk-Addstatut-btn').val(id);
        $('#statutdeletid').val(id);
    });

             </script>
