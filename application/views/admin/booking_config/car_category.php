<div class="row">
  <div class="ListCartCat">
    <!-- <div class="col-md-12">
      <div class="page-action"style="float: right; margin-bottom:5px;" >
        <button class="btn btn-sm btn-default" onclick="carCatAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>&nbsp;
        <button class="btn btn-sm btn-default" onclick="carCatEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>&nbsp;
        <button class="btn btn-sm btn-default" onclick="carCatDelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button>&nbsp;
      </div>
    </div> -->
  <input type="hidden" class="chk-AddCarCat-btn" value="">
  <div class="col-md-12">
    <div class="module-body table-responsive">
        <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
            <thead>
            <tr>
              <th class="no-sort text-center"style="width:2%;">#</th>
              <th class="text-center" style="width:3% !important;"><?php echo $this->lang->line('id'); ?></th>
              <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('date'); ?></th>
              <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('time'); ?></th>
              <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('added_by'); ?></th>
              <th class="column-first_name text-center" style="width:6% !important; ">Car Category Name</th>
              <th class="column-first_name text-center" style="width:6% !important; ">Wheelchair Access</th>
              <th class="column-first_name text-center" style="width:6% !important; ">Icon</th>
              
              <th class="column-time text-center"style="width:8%;">Statut</th>
              <th class="text-center"style="width:8%;">Since</th>
            </tr>
            </thead>
            <tbody>
                <?php if (!empty($car_category)): ?>
                <?php $cnt=1; ?>
                <?php foreach($car_category as $key => $item):?>
                    <tr>
                        <td class="text-center"><input type="checkbox" class="chk-mainCarCat-template" data-input="<?=$item->id?>"></td>
                        <td class="text-center"><a href="javascript:void()" onclick="carcatidEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->created_at)) .'0000'. $item->id;?></a></td>
                        <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                        <td class="text-center"><?=date("H:i:s", strtotime($item->since))?></td>
                        <td class="text-center">
                          <?php 
                        $user=$this->bookings_config_model->getuser($item->user_id);
                        $user=str_replace(".", " ", $user);
                        echo $user;
                        ?></td>
                        <td class="text-center"><?=$item->car_cat_name;?></td>
                        <td class="text-center"><?= ($item->cartype==1)?"Yes":"No"; ?></td>
                        <?php if(!empty($item->caricon)): ?>
                        <td class="text-center"><img src="<?=base_url($item->caricon);?>" width="60" height="30" ></td>
                        <?php else: ?>
                          <td class="text-center">Null</td>
                        <?php endif; ?> 
                        <td class="text-center"><?=$item->statut=='1'?'<span class="label label-success">Active</span>':'<span class="label label-danger">Hide</span>';?></td>
                        <td class="text-center"><?= timeDiff($item->since) ?></td>

                    </tr>
                    <?php $cnt++; ?>
                <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
        <br>
      </div>
  </div>
</div>

  <?=form_open_multipart("admin/booking_config/carCatadd")?>
  <!-- <form action="<?=base_url();?>admin/booking_config/vatadd" class="" method="post"  enctype="multipart/form-data"> -->
  <div class="carCatAdd" style="display: none;">
    <div class="col-md-12">
      <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span >Statut</span>
          <select class="form-control" name="car_cat_statut" required>
            
            <option value="1">Show</option>
            <option value="0">Hide</option>
          </select>
        </div>
      </div>
      <div class="col-md-2" style="margin-top: 5px;">
        <div class="form-group">
          <span> Car Category</span>
            <input type="text" class="form-control" name="car_cat_name" placeholder="" value="" required>
        </div>
      </div>
       <div class="col-md-2" style="margin-top: 5px;">
         <span>Wheelchair Access</span>
           <select class="form-control" name="cartype" required>
           
            <option value="1">Yes</option>
            <option value="0">No</option>
          </select>
       </div> 
        <div class="col-md-2" style="margin-top: 5px;">
          <div class="col-md-12">
            <img  id="car_logo_preview" src="<?php echo base_url(); ?>uploads/vehicle_images/default_car.png" style="width: 200px; height: 150px;">
          </div>
          <div class="col-md-12">
          <div class="form-group">
            
            <input type="file" name="caricon" class="form-control-file" id="caricon" onChange="carpreview(this)"  required style="width:100%;">
            <span>Car Icone</span>
          </div>
        </div>
       </div>
      
    </div>
  
    <div class="maincapacitybox">
    <div class="col-md-6" style="position:relative;width: 54%;">
       <button type="button" onclick="addboxes();" class="plusgreeniconconfig"><i class="fa fa-plus" style="margin:0px !important;"></i></button>
     </div>
       <div class="capacitybox" >
          <div class="col-md-6" style="margin-top:10px;margin-left: 1.5%;background-image: linear-gradient(to bottom, #fff 0, #e0e0e0 100%);border-color: #ccc;padding: 6px 12px;"> 

             <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6 text-right" style="padding-top: 7px;">
                    <span>Passengers</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="passengercap[]" style="width:100%;" class="form-control" required>
                     
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
                  </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Luggage</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
 
                  <select name="lugagescap[]" style="width:100%;" class="form-control" required>
                     
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
                </div>
             </div>
             <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Wheelchairs</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="wheelchairscap[]" style="width:100%;" class="form-control" required>
                    
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
                </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Babys</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="babycap[]" style="width:100%;" class="form-control" required>
                     
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
              </div>
             </div>
               <div class="col-md-2" style="width: 20%;padding:0px;">
                  <div class="col-md-6  text-right" style="padding-top: 7px;">
                    <span>Animals</span>
                 </div>
                  <div class="col-md-6" style="padding-left:0px;">
                  <select name="animalscap[]" style="width:100%;" class="form-control" required>
                    
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select> 
               </div>
             </div>
          
           </div>  
         </div>  

    </div>
      <div class="col-md-12 clearfix" style="margin-top: 17px;">
        <div class="form-group" style="float:right;">
         <button type="button" class="btn btn-default"  onclick="cancelCarCat()"><span class="fa fa-close"> Cancel </span></button>
         <button  class="btn btn-default" style="margin-left: 5px;"><span class="fa fa-save"></span> Save </button>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?>
  <div class="carCatEdit"  style="display:none;">
  <!-- <div id="carCatEditview<?php // echo $value->id; ?>" class="carCatEditEdit" style="display: none;"> -->
    <?=form_open_multipart("admin/booking_config/carCatedit")?>
    <div class="carCatEditajax">
    </div>
   <div class="col-md-12 clearfix" style="margin-top: 17px;">
      <div class="form-group" style="float:right;">
       <button type="button" class="btn btn-default"  onclick="cancelCarCat()"><span class="fa fa-close"> Cancel </span></button>
       <button  class="btn btn-default" style="margin-left: 5px;"><span class="fa fa-save"></span> Save </button>
     
      </div>
    </div>
    <?php echo form_close(); ?>
  </div>
  <div class="col-md-12 carCatDelete" style="padding:20px  0px;margin-left: 15px;margin-top: 15px; display: none;">
    <?=form_open("admin/booking_config/deletecarCat")?>
      <input  name="tablename" value="vbs_job_statut" type="hidden" >
      <input type="hidden" id="carCatdeletid" name="delete_car_id" value="">
      <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
      <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
      <button  class="btn  btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>
      <a class="btn btn-default" style="cursor: pointer;" onclick="cancelCarCat()">No</a>
    <?php echo form_close(); ?>
  </div>
</div>


<script type="text/javascript">
 
function carpreview(e) {
  if (e.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e){
      document.querySelector('#car_logo_preview').setAttribute('src', e.target.result);
    }
    reader.readAsDataURL(e.files[0]);
  }
}
function editcarpreview(e) {
  if (e.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e){
      document.querySelector('#editcar_logo_preview').setAttribute('src', e.target.result);
    }
    reader.readAsDataURL(e.files[0]);
  }
}
 
function addboxes(){
   $("div.maincapacitybox").append($("div.capacitybox:first").clone(true).append('<div class="col-md-6" style="position:relative;width: 54%;"><button type="button"  onclick="removebox(this);" class="minusrediconconfig"><i class="fa fa-minus" style="margin:0px;"></i></button></div>'));
}
function removebox(e){
   $(e).closest(".capacitybox").remove();
}
function editremovebox(e){
   $(e).closest(".capacitybox").remove();
}
function editboxes(){
   $("div.maineditcapacitybox").append($("div.capacitybox:first").clone(true).append('<div class="col-md-6" style="position:relative;width: 54%;"><button type="button"  onclick="editremovebox(this);" class="minusrediconconfig"><i class="fa fa-minus"></i></button></div>'));
}
function deleteconfigbox(e){
   $(e).closest(".capacitybox").remove();
}

function carCatAdd()
{
  setupDivCarCat();
  $(".carCatAdd").show();
}

function cancelCarCat()
{
  setupDivCarCat();
  $(".ListCartCat").show();
}

function carCatEdit()
{
    var val = $('.chk-AddCarCat-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivCarCat();
        $(".carCatEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/booking_config/get_ajax_car_cat'; ?>',
                data: {'car_cat_id': val},
                success: function (result) {
                  // alert(result);
                  document.getElementsByClassName("carCatEditajax")[0].innerHTML = result;
                }
            });
}
function carcatidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivCarCat();
      $(".carCatEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/booking_config/get_ajax_car_cat'; ?>',
              data: {'car_cat_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("carCatEditajax")[0].innerHTML = result;
              }
          });
}
function carCatDelete()
{
  var val = $('.chk-AddCarCat-btn').val();
  if(val != "")
  {
  setupDivCarCat();
  $(".carCatDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivCarCat()
{
 
  $(".carCatAdd").hide();
  $(".carCatEdit").hide();
  $(".ListCartCat").hide();
  $(".carCatDelete").hide();
}
function setupDivCarCatConfig()
{
  $(".ListCartCat").show();
  $(".carCatAdd").hide();
  $(".carCatEdit").hide();
  $(".carCatDelete").hide();
}
$('input.chk-mainCarCat-template').on('change', function() {
  $('input.chk-mainCarCat-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-AddCarCat-btn').val(id);
  $('#carCatdeletid').val(id);
});

</script>
