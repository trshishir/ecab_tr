<style>
.table thead > tr > th {
    background: linear-gradient(#ffffff, #ffffff 25%, #d0d0d0) !important;
    padding: 7px;
    color: #2c2c2c;
    font-weight: bolder;
    font-size: 12px;
}
</style>
  <div class="ListPrices">
    <input type="hidden" class="chk-Addprices-btn" value="">
    <!-- <div class="col-md-12">
        <div class="toolbar"style="float: right; margin-bottom:5px;">
          <button class="btn btn-sm btn-default" onclick="pricesadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>&nbsp;
          <button class="btn btn-sm btn-default" onclick="pricesedit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>&nbsp;
          <button class="btn btn-sm btn-default" onclick="pricesdelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button>&nbsp;
        </div>
    </div> -->
    <div class="col-md-12" style="padding:0px">
      <div class="module-body table-responsive">
        <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center"style="width:2%;">#</th>
            <th class="text-center" style="width:2% !important;"><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" style="width:5% !important;"><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" style="width:10% !important;"><?php echo $this->lang->line('added_by'); ?></th>
            <th class="text-center" style="width:6% !important; text-align:left !important;">Prices Name</th>
            
            <th class="column-time text-center"style="width:2%;">Depart Type</th>
            <th class="column-time text-center"style="width:2%;">Departure</th>
            <th class="column-time text-center"style="width:3%;">Destination Type</th>
            <th class="column-time text-center"style="width:2%;">Destination</th>
            <th class="column-time text-center"style="width:2%;">Price(Car)</th>
            <th class="column-time text-center"style="width:2%;">Price(Minibus)</th>
            <th class="column-date text-center"style="width:3%;">Statut</th>
            <th class="text-center"style="width:12%  !important;">Since</th>
            
          </tr>
          </thead>
          <tbody>
              <?php $cnt=1; ?>
              <?php
              $prices_data = $this->bookings_config_model->booking_Config_getAll('vbs_u_prices');
              if (!empty($prices_data)):
              foreach($prices_data as $key => $item):
                    $status=$item->status=='0'?"Show":"Hide";
                    $d_type=$this->bookings_config_model->d_type($item->d_type);
                    $departure=$this->bookings_config_model->departure($item->departure);
                    $destination_type=$this->bookings_config_model->destination_type($item->destination_type);
                    $destination=$this->bookings_config_model->destination($item->destination);
              ?>
                  <tr>
                    <td class="text-center">
                        <input type="checkbox" class="chk-mainpoi-template" data-input="<?=$item->id?>">
                    </td>
                    <td class="text-center"><a href="javascript:void()" onclick="pricesidEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->created_at)) .'0000'. $item->id;?></a></td>
                    <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                    <td class="text-center"><?=date("H:i:s", strtotime($item->since))?></td>
                   <td class="text-center"> 
                       <?php 
                                  $user=$this->bookings_config_model->getuser($item->user_id);
                                  $user=str_replace(".", " ", $user);
                                  echo $user;
                                  ?></td>
                      <td><?=$item->name?></td>
                      
                      <td class="text-center"><?=$d_type;?></td>
                      <td class="text-center"><?=$departure;?></td>
                      <td class="text-center"><?=$destination_type;?></td>
                      <td class="text-center"><?=$destination;?></td>
                      <td class="text-center"><?=$item->p_car;?></td>
                      <td class="text-center"><?=$item->p_minibus;?></td>
                      
                      <td class="text-center"><?=$item->statut=='1'?'<span class="label label-success">Active</span>':'<span class="label label-danger">Hide</span>';?></td>
                      <td class="text-center"><?= timeDiff($item->since) ?></td>
                  </tr>
                  <?php $cnt++; ?>
              <?php endforeach; ?>
              <?php endif; ?>
          </tbody>
      </table>
      </div>
    </div>
  </div>
  <div class="pricesadd" style="display: none;" >
      <?=form_open("admin/booking_config/pricesadd")?>
      <div class="row" style="margin-top: 10px;">
           <div class="col-md-3" style="margin-top: 5px;">
               <div class="form-group">
                 <span style="font-weight: bold;">Prices Name</span>
                  <input type="text" class="form-control" name="name" placeholder="" value="" required>
                </div>
            </div>
            <div class="col-md-3" style="margin-top: 5px;">
                <div class="form-group">
                  <span style="font-weight: bold;">Statut</span>
                  <select class="form-control" name="status" required style="background: #fff !important;">
                      <option value="0">Show</option>
                      <option value="1">Hide</option>
                  </select>
                </div>
            </div>
            <div class="col-md-3" style="margin-top: 5px;">
                <div class="form-group">
                  <span style="font-weight: bold;">Depart Type</span>
                  <select class="form-control" name="d_type" required style="background: #fff !important;">
                      <option value="">Select</option>
                      <option value="0">POI</option>
                      <option value="1">City</option>
                      <option value="2">Region</option>
                  </select>
                </div>
            </div>
      </div>
      <div class="row" style="margin-top: 10px;">
            <div class="col-md-3" style="margin-top: 5px;">
                <div class="form-group">
                  <span style="font-weight: bold;">Departure</span>
                  <select class="form-control" name="departure" required style="background: #fff !important;">
                      <option value="">Select</option>
                      <option value="0">CDG</option>
                      <option value="1">Park</option>
                      <option value="2">Hotel</option>
                  </select>
                </div>
            </div>
            <div class="col-md-3" style="margin-top: 5px;">
                <div class="form-group">
                  <span style="font-weight: bold;">Destination Type</span>
                  <select class="form-control" name="destination_type" required style="background: #fff !important;">
                      <option value="">Select</option>
                      <option value="0">POI</option>
                      <option value="1">City</option>
                      <option value="2">Region</option>
                  </select>
                </div>
            </div>
            <div class="col-md-3" style="margin-top: 5px;">
                <div class="form-group">
                  <span style="font-weight: bold;">Destination</span>
                  <select class="form-control" name="destination" required style="background: #fff !important;">
                      <option value="">Select</option>
                      <option value="0">CDG</option>
                      <option value="1">Park</option>
                      <option value="2">Hotel</option>
                  </select>
                </div>
            </div>
      </div>
      <div class="row" style="margin-top: 10px;">
          <div class="col-md-3" style="margin-top: 5px;">
              <div class="form-group">
                  <span style="font-weight: bold;">Price(Car)</span>
                  <input type="number" class="form-control" name="p_car" placeholder="" value="" required>
              </div>
          </div>
          <div class="col-md-3" style="margin-top: 5px;">
              <div class="form-group">
                  <span style="font-weight: bold;">Price(Minibus)</span>
                  <input type="number" class="form-control" name="p_minibus" placeholder="" value="" required>
              </div>
          </div>
          <div class="col-md-3" style="margin-top: 5px;">
              <div class="form-group">
                  <span style="font-weight: bold;">Price(Van)</span>
                  <input type="number" class="form-control" name="p_van" placeholder="" value="" required>
              </div>
          </div>
      </div>
      <div class="row" style="margin-top: 10px;">
          <div class="col-md-3" style="margin-top: 5px;">
              <div class="form-group">
                  <span style="font-weight: bold;">Price TPMR(Car)</span>
                  <input type="number" class="form-control" name="p_tpmr_car" placeholder="" value="" required>
              </div>
          </div>
          <div class="col-md-3" style="margin-top: 5px;">
              <div class="form-group">
                  <span style="font-weight: bold;">Price TPMR(Minibus)</span>
                  <input type="number" class="form-control" name="p_tpmr_mini" placeholder="" value="" required>
              </div>
          </div>
          <div class="col-md-3" style="margin-top: 5px;">
              <div class="form-group">
                  <span style="font-weight: bold;">Price TPMR(Van)</span>
                  <input type="number" class="form-control" name="p_tpmr_van" placeholder="" value="" required>
              </div>
          </div>
      </div>
      <div class="row" style="margin-top: 10px;">
        <div class="col-md-9" style="margin-top: 5px;">
        <button  class="btn btn-default"style="float:right; margin-left:7px;"><span class="save-icon"></span> Save </button>
            <a href="javascript:void()" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelPrices()"><span class="delete-icon"> Cancel </span></a>
            
        </div>
      </div>
    <?php echo form_close(); ?>
  </div>
  <?=form_open("admin/booking_config/pricesedit")?>
<div class="pricesedit" style="display:none">
  <div class="priceseditajax">
  </div>
  <div class="row" style="margin-top: 10px;">
			<div class="col-md-9" style="margin-top: 5px;">
      <button  class="btn btn-default"style="float:right; margin-left:7px;"><span class="save-icon"></span> Update </button>
        <a href="javascript:void()" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelPrices()"><span class="delete-icon"> Cancel </span></a>
				
			</div>
		</div>
</div>
<?php echo form_close(); ?>
<div class="col-md-12 pricesdelete" style="padding:20px  0px;margin-left: 15px;margin-top: 15px; display: none;">
    <?=form_open("admin/booking_config/pricesdelete")?>
    <input  name="tablename" value="vbs_job_statut" type="hidden" >
    <input type="hidden" id="pricesdeleteid" name="pricesdeleteid" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?
    </div>
    <button  class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>
    <a class="btn btn-default" style="cursor: pointer;" onclick="cancelPrices()">No</a>
  <?php echo form_close(); ?>
</div>
<!-- <script src="http://unique/ecabapp/assets/system_design/scripts/jquery.js"></script> -->
<script type="text/javascript">
  function pricesadd()
  {
    setupDivPrices();
    $(".pricesadd").show();
  }
  function cancelPrices()
  {
    setupDivPrices();
    $(".ListPrices").show();
  }
  function pricesedit()
  {
      var val = $('.chk-Addprices-btn').val();
      if (val=='')
      {
          alert("Please select record!");
          return false;
      }
      setupDivPrices();
      $(".pricesedit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/booking_config/get_ajax_prices'; ?>',
              data: {'prices_id': val},
              success: function (result) {
                document.getElementsByClassName("priceseditajax")[0].innerHTML = result;
              }
          });
  }
  function pricesidEdit(id){
    var val = id;
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
    setupDivPrices();
    $(".pricesedit").show();
    $.ajax({
            type: "GET",
            url: '<?php echo base_url().'admin/booking_config/get_ajax_prices'; ?>',
            data: {'prices_id': val},
            success: function (result) {
              document.getElementsByClassName("priceseditajax")[0].innerHTML = result;
            }
        });
  }
  function pricesdelete()
  {
    var val = $('.chk-Addprices-btn').val();
    if (val=='')
      {
          alert("Please select record!");
          return false;
      }
    $('#pricesdeleteid').val(val);
    setupDivPrices();
    $(".pricesdelete").show();
  }

  function setupDivPrices()
  {
    $(".pricesedit").hide();
    $('.pricesadd').hide();
    $('.ListPrices').hide();
    $('.pricesdelete').hide();
  }
   function setupDivPricesConfig()
  {
    $(".pricesedit").hide();
    $('.pricesadd').hide();
    $('.ListPrices').show();
    $('.pricesdelete').hide();
  }

  $('input.chk-mainpoi-template').on('change', function() {
    $('input.chk-mainpoi-template').not(this).prop('checked', false);
    var id = $(this).attr('data-input');
    $('.chk-Addprices-btn').val(id);
  });
</script>
