
                            <div class="row">
                         
                          
                            <!-- <form action="<?=base_url();?>admin/booking_config/statutadd" class="" method="post"  enctype="multipart/form-data"> -->
                    <?php  if($pricestatus): ?>

                            <div class="pricestatusedit" >
                                  <?=form_open_multipart("admin/booking_config/pricestatusedit")?>
                                <input type="hidden" name="id" value="<?= $pricestatus->id ?>">

                             <div class="col-md-12">   
                            <div class="col-md-2" style="margin-top: 5px;padding-left:0px;">
                                <div class="form-group">
                                <span>Statut</span>
                     <select class="form-control" name="statut" required>
                            
                            <option  <?php if ($pricestatus->statut == "1") { echo "selected" ; } ?>
                             value="1">Show</option>
                           <option  <?php if ($pricestatus->statut == "0") { echo "selected" ; } ?>
                             value="0">Hide</option>
                          
                        </select>
                            </div>
                              </div>  
                              <div class="col-md-2" style="margin-top: 5px;">
                                <div class="form-group">
                                <span>Front Map</span>
                                <select class="form-control" id="pricestatus" name="pricestatus" required onchange="showpricemessage()">
                                    <option  <?php if ($pricestatus->pricestatus  == "1") {echo "selected"; } ?>
                                     value="1">Show</option>
                                       <option  <?php if ($pricestatus->pricestatus  == "0") {echo "selected"; } ?>
                                     value="0">Hide</option>
        
                                </select>
                            </div>
                              </div> 
                               <div class="col-md-2" style="margin-top: 5px;">
                                <div class="form-group">
                                  <span>Quote Validity Delay (days)</span>
                                    <select class="form-control"  name="quotedelay" required>
                                      <option value="">Select</option>
                                      <option <?php if ($pricestatus->quotedelay  == "15") {echo "selected"; } ?> value="15">15</option>
                                      <option <?php if ($pricestatus->quotedelay  == "30") {echo "selected"; } ?>  value="30">30</option>
                                      <option <?php if ($pricestatus->quotedelay  == "45") {echo "selected"; } ?> value="45">45</option>
                                      <option <?php if ($pricestatus->quotedelay  == "60") {echo "selected"; } ?> value="60">60</option>
                                    </select>
                                  
                                </div>
                              </div>
                              
                              </div>
                              <div class="col-md-12" style="padding: 0px">
                                 <div class="col-md-2" style="margin-top: 5px;">
                                 <?php if(!empty($pricestatus->customicon)): ?>
                                   <div class="col-md-12"> 
                                       <img  id="customicon_logo_preview" src="<?=base_url($pricestatus->customicon); ?>" style="width: 200px; height: 150px;border:1px solid #888;" >
                                      </div>
                                <?php else: ?>
                                   <div class="col-md-12">
                                    <img  id="customicon_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                <?php endif; ?>
                                 
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="customicon" class="form-control-file" id="customicon" onChange="custompreview(this)"   style="width:100%;">
                                   
                                  </div>
                                 </div>
                                 <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="customiconwidth" id="customiconwidth" class="form-control" value="<?= $pricestatus->customiconwidth ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="customiconheight" id="customiconheight" class="form-control"  value="<?= $pricestatus->customiconheight ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Quote Custom Image</span>
                                 </div>
                              </div>
                              <div class="col-md-2" style="margin-top: 5px;">
                                <?php if(!empty($pricestatus->statutpendingicon )): ?>
                                   <div class="col-md-12"> 
                                       <img  id="statutpending_logo_preview" src="<?=base_url($pricestatus->statutpendingicon ); ?>" style="width: 200px; height: 150px;border:1px solid #888;" >
                                      </div>
                                <?php else: ?>
                                   <div class="col-md-12">
                                    <img  id="statutpending_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                <?php endif; ?>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="statutpendingicon" class="form-control-file" id="statutpendingicon" onChange="statutpendingpreview(this)"   style="width:100%;">
                                  
                                  </div>
                                 </div>
                                                                  <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="pendingiconwidth" id="pendingiconwidth" class="form-control" value="<?= $pricestatus->pendingiconwidth ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="pendingiconheight" id="pendingiconheight" class="form-control"  value="<?= $pricestatus->pendingiconheight ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Statut Pending Image</span>
                                 </div>
                              </div>
                             
                              <div class="col-md-2" style="margin-top: 5px;">
                                <?php if(!empty($pricestatus->statutapprovedicon)): ?>
                                   <div class="col-md-12"> 
                                       <img  id="statutapproved_logo_preview" src="<?=base_url($pricestatus->statutapprovedicon ); ?>" style="width: 200px; height: 150px;border:1px solid #888;" >
                                      </div>
                                <?php else: ?>
                                   <div class="col-md-12">
                                    <img  id="statutapproved_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                <?php endif; ?>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="statutapprovedicon" class="form-control-file" id="statutapprovedicon" onChange="statutapprovedpreview(this)"   style="width:100%;">
                                   
                                  </div>
                                 </div>
                                                                  <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="approvediconwidth" id="approvediconwidth" class="form-control" value="<?= $pricestatus->approvediconwidth ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="approvediconheight" id="approvediconheight" class="form-control"  value="<?= $pricestatus->approvediconheight ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Statut Accepted Image</span>
                                 </div>
                              </div>
                              <!-- Denied Image -->
                               <div class="col-md-2" style="margin-top: 5px;">
                                <?php if(!empty($pricestatus->statutdeniedicon)): ?>
                                   <div class="col-md-12"> 
                                       <img  id="statutdenied_logo_preview" src="<?=base_url($pricestatus->statutdeniedicon ); ?>" style="width: 200px; height: 150px;border:1px solid #888;" >
                                      </div>
                                <?php else: ?>
                                   <div class="col-md-12">
                                    <img  id="statutdenied_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                <?php endif; ?>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="statutdeniedicon" class="form-control-file" id="statutdeniedicon" onChange="statutdeniedpreview(this)"   style="width:100%;">
                                   
                                  </div>
                                 </div>
                              <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="deniediconwidth" id="deniediconwidth" class="form-control" value="<?= $pricestatus->deniediconwidth ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="deniediconheight" id="deniediconheight" class="form-control"  value="<?= $pricestatus->deniediconheight ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Statut Denied Image</span>
                                 </div>
                              </div>
                              <!-- Denied Image -->
                              <!-- Cancelled Image -->
                               <div class="col-md-2" style="margin-top: 5px;">
                                <?php if(!empty($pricestatus->statutcancelledicon)): ?>
                                   <div class="col-md-12"> 
                                       <img  id="statutcancelled_logo_preview" src="<?=base_url($pricestatus->statutcancelledicon ); ?>" style="width: 200px; height: 150px;border:1px solid #888;" >
                                      </div>
                                <?php else: ?>
                                   <div class="col-md-12">
                                    <img  id="statutcancelled_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                <?php endif; ?>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="statutcancelledicon" class="form-control-file" id="statutcancelledicon" onChange="statutcancelledpreview(this)"   style="width:100%;">
                                  
                                  </div>
                                 </div>
                                <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="cancellediconwidth" id="cancellediconwidth" class="form-control" value="<?= $pricestatus->cancellediconwidth ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="cancellediconheight" id="cancellediconheight" class="form-control"  value="<?= $pricestatus->cancellediconheight ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Statut Cancelled Image</span>
                                 </div>
                              </div>
                              <!-- Cancelled Image -->
                              </div>
                                                        <div class="col-md-12" style="padding: 0px">
                                 <div class="col-md-2" style="margin-top: 5px;">
                                 <?php if(!empty($pricestatus->bookingcustomicon)): ?>
                                   <div class="col-md-12"> 
                                       <img  id="booking_customicon_logo_preview" src="<?=base_url($pricestatus->bookingcustomicon); ?>" style="width: 200px; height: 150px;border:1px solid #888;" >
                                      </div>
                                <?php else: ?>
                                   <div class="col-md-12">
                                    <img  id="booking_customicon_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                <?php endif; ?>
                                 
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="bookingcustomicon" class="form-control-file" id="bookingcustomicon" onChange="bookingcustompreview(this)"   style="width:100%;">
                                   
                                  </div>
                                 </div>
                                 <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="bookingcustomiconwidth" id="bookingcustomiconwidth" class="form-control" value="<?= $pricestatus->bookingcustomiconwidth ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="bookingcustomiconheight" id="bookingcustomiconheight" class="form-control"  value="<?= $pricestatus->bookingcustomiconheight ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Booking Custom Image</span>
                                 </div>
                              </div>
                              <div class="col-md-2" style="margin-top: 5px;">
                                <?php if(!empty($pricestatus->bookingstatutpendingicon )): ?>
                                   <div class="col-md-12"> 
                                       <img  id="booking_statutpending_logo_preview" src="<?=base_url($pricestatus->bookingstatutpendingicon ); ?>" style="width: 200px; height: 150px;border:1px solid #888;" >
                                      </div>
                                <?php else: ?>
                                   <div class="col-md-12">
                                    <img  id="booking_statutpending_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                <?php endif; ?>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="bookingstatutpendingicon" class="form-control-file" id="bookingstatutpendingicon" onChange="bookingstatutpendingpreview(this)"   style="width:100%;">
                                  
                                  </div>
                                 </div>
                                                                  <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="bookingpendingiconwidth" id="bookingpendingiconwidth" class="form-control" value="<?= $pricestatus->bookingpendingiconwidth ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="bookingpendingiconheight" id="bookingpendingiconheight" class="form-control"  value="<?= $pricestatus->bookingpendingiconheight ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Booking Statut Pending Image</span>
                                 </div>
                              </div>
                             
                              <div class="col-md-2" style="margin-top: 5px;">
                                <?php if(!empty($pricestatus->bookingstatutconfirmedicon)): ?>
                                   <div class="col-md-12"> 
                                       <img  id="booking_statutconfirmed_logo_preview" src="<?=base_url($pricestatus->bookingstatutconfirmedicon ); ?>" style="width: 200px; height: 150px;border:1px solid #888;" >
                                      </div>
                                <?php else: ?>
                                   <div class="col-md-12">
                                    <img  id="booking_statutconfirmed_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                <?php endif; ?>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="bookingstatutconfirmedicon" class="form-control-file" id="bookingstatutconfirmedicon" onChange="bookingstatutconfirmedpreview(this)"   style="width:100%;">
                                   
                                  </div>
                                 </div>
                                  <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="bookingconfirmediconwidth" id="bookingconfirmediconwidth" class="form-control" value="<?= $pricestatus->bookingconfirmediconwidth ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="bookingconfirmediconheight" id="bookingconfirmediconheight" class="form-control"  value="<?= $pricestatus->bookingconfirmediconheight ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Booking Statut Confirmed Image</span>
                                 </div>
                              </div>
                              
                              <!-- Cancelled Image -->
                               <div class="col-md-2" style="margin-top: 5px;">
                                <?php if(!empty($pricestatus->bookingstatutcancelledicon)): ?>
                                   <div class="col-md-12"> 
                                       <img  id="booking_statutcancelled_logo_preview" src="<?=base_url($pricestatus->bookingstatutcancelledicon ); ?>" style="width: 200px; height: 150px;border:1px solid #888;" >
                                      </div>
                                <?php else: ?>
                                   <div class="col-md-12">
                                    <img  id="booking_statutcancelled_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                <?php endif; ?>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="bookingstatutcancelledicon" class="form-control-file" id="bookingstatutcancelledicon" onChange="bookingstatutcancelledpreview(this)"   style="width:100%;">
                                  
                                  </div>
                                 </div>
                                <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="bookingcancellediconwidth" id="bookingcancellediconwidth" class="form-control" value="<?= $pricestatus->bookingcancellediconwidth ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="bookingcancellediconheight" id="bookingcancellediconheight" class="form-control"  value="<?= $pricestatus->bookingcancellediconheight ?>" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Booking Statut Cancelled Image</span>
                                 </div>
                              </div>
                              <!-- Cancelled Image -->
                              </div>
                              <div class="col-md-12" style="padding:0px;">
                               <?php if ($pricestatus->pricestatus  == "0"):?>
                              <div class="col-md-6" style="margin-top: 5px;"  id="pricemessage">
                                <div class="form-group">
                                  <span>Front Quote Price Texte</span>
                                  <textarea id="pricemessagecontent" class="form-control" name="message" rows="8" cols="80"><?= $pricestatus->message ?>
                                  </textarea>
                                </div>
                              </div>
                              <?php else: ?>
                                <div class="col-md-6" style="margin-top: 5px;display: none;"  id="pricemessage">
                                <div class="form-group">
                                  <span>Front Quote Price Texte</span>
                                  <textarea id="pricemessagecontent" class="form-control" name="message" rows="8" cols="80"><?= $pricestatus->message ?>
                                  </textarea>
                                </div>
                              </div>
                              <?php endif; ?>
                              <div class="col-md-6" style="margin-top: 5px;" >
                                <div class="form-group">
                                  <span>Quote Custom Texte</span>
                                  <textarea id="customtextcontent" class="form-control" name="customtext" rows="8" cols="80"><?= $pricestatus->customtext ?></textarea>
                                </div>
                              </div>
                            </div>
                              <div class="col-md-12">   
                                                       
                                <button  class="btn btn-default"style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                                 <a  href="<?= base_url(); ?>admin/booking_config" type="button" class="btn btn-default" style="float:right; margin-left:7px;">
                                  <span class="fa fa-close"> Cancel </span></a>
                                
                           
                               </div> 
                                
                            <?php echo form_close(); ?>
                          
                            </div>
                        <?php else: ?>
                             <div class="pricestatusadd" >
                                  <?=form_open_multipart("admin/booking_config/pricestatusadd")?>
                              

                             <div class="col-md-12">   
                            <div class="col-md-2" style="margin-top: 5px;padding-left:0px;">
                                <div class="form-group">
                                <span>Statut</span>
                     <select class="form-control" name="statut" required>
                            
                            <option 
                             value="1">Show</option>
                           <option  
                             value="0">Hide</option>
                          
                        </select>
                            </div>
                              </div>  
                              <div class="col-md-2" style="margin-top: 5px;">
                                <div class="form-group">
                                <span>Front Map</span>
                                <select class="form-control" id="pricestatus" name="pricestatus" required onchange="showpricemessage()">
                                    <option 
                                     value="1">Show</option>
                                       <option  
                                     value="0">Hide</option>
        
                                 </select>
                                </div>
                              </div> 
                              <div class="col-md-2" style="margin-top: 5px;">
                                <div class="form-group">

                                  <span>Quote Validity Delay (days)</span>
                                    <select class="form-control"  name="quotedelay" required>
                                      <option value="">Select</option>
                                      <option value="15">15</option>
                                      <option value="30">30</option>
                                      <option value="45">45</option>
                                      <option value="60">60</option>
                                    </select>
                                   
                                </div>
                              </div>
                               
                              
                              </div>
                            <div class="col-md-12" style="padding: 0px">
                              <div class="col-md-2" style="margin-top: 5px;">
                                  <div class="col-md-12">
                                    <img  id="customicon_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="customicon" class="form-control-file" id="customicon" onChange="custompreview(this)"  required style="width:100%;">
                                 
                                  </div>
                                 </div>
                                <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="customiconwidth" id="customiconwidth" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="customiconheight" id="customiconheight" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Quote Custom Image</span>
                                 </div>
                              </div>
                              <div class="col-md-2" style="margin-top: 5px;">
                                  <div class="col-md-12">
                                    <img  id="statutpending_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="statutpendingicon" class="form-control-file" id="statutpendingicon" onChange="statutpendingpreview(this)"  required style="width:100%;">
                                    <span>Statut Pending Image</span>
                                  </div>
                                 </div>
                                <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="pendingiconwidth" id="pendingiconwidth" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="pendingiconheight" id="pendingiconheight" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Statut Pending Image</span>
                                 </div>
                              </div>
                              <div class="col-md-2" style="margin-top: 5px;">
                                  <div class="col-md-12">
                                    <img  id="statutapproved_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="statutapprovedicon" class="form-control-file" id="statutapprovedicon" onChange="statutapprovedpreview(this)"  required style="width:100%;">
                                    <span>Statut Accepted Image</span>
                                  </div>
                                 </div>
                                  <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="approvediconwidth" id="approvediconwidth" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="approvediconheight" id="approvediconheight" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Statut Accepted Image</span>
                                 </div>
                              </div>
                                  <!-- Denied Image -->
                               <div class="col-md-2" style="margin-top: 5px;">
                                  <div class="col-md-12">
                                    <img  id="statutdenied_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="statutdeniedicon" class="form-control-file" id="statutdeniedicon" onChange="statutdeniedpreview(this)"  required style="width:100%;">
                                    <span>Statut Denied Image</span>
                                  </div>
                                 </div>
                                                                  <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="deniediconwidth" id="deniediconwidth" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="deniediconheight" id="deniediconheight" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Statut Denied Image</span>
                                 </div>
                              </div>
                        <!-- Denied Image -->
                                              <!-- Cancelled Image -->
                                <div class="col-md-2" style="margin-top: 5px;">
                                  <div class="col-md-12">
                                    <img  id="statutcancelled_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="statutcancelledicon" class="form-control-file" id="statutcancelledicon" onChange="statutcancelledpreview(this)"  required style="width:100%;">
                                    <span>Statut Cancelled Image</span>
                                  </div>
                                 </div>
                                  <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="cancellediconwidth" id="cancellediconwidth" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="cancellediconheight" id="cancellediconheight" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Statut Cancelled Image</span>
                                 </div>
                              </div>
                        <!-- Cancelled Image -->
                            </div>
                                                       <div class="col-md-12" style="padding: 0px">
                              <div class="col-md-2" style="margin-top: 5px;">
                                  <div class="col-md-12">
                                    <img  id="booking_customicon_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="bookingcustomicon" class="form-control-file" id="bookingcustomicon" onChange="bookingcustompreview(this)"  required style="width:100%;">
                                 
                                  </div>
                                 </div>
                                <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="bookingcustomiconwidth" id="bookingcustomiconwidth" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="bookingcustomiconheight" id="bookingcustomiconheight" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Booking Custom Image</span>
                                 </div>
                              </div>
                              <div class="col-md-2" style="margin-top: 5px;">
                                  <div class="col-md-12">
                                    <img  id="booking_statutpending_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="bookingstatutpendingicon" class="form-control-file" id="bookingstatutpendingicon" onChange="bookingstatutpendingpreview(this)"  required style="width:100%;">
                                    <span>Booking Statut Pending Image</span>
                                  </div>
                                 </div>
                                <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="bookingpendingiconwidth" id="bookingpendingiconwidth" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="bookingpendingiconheight" id="bookingpendingiconheight" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Booking Statut Pending Image</span>
                                 </div>
                              </div>
                              <div class="col-md-2" style="margin-top: 5px;">
                                  <div class="col-md-12">
                                    <img  id="booking_statutconfirmed_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="bookingstatutconfirmedicon" class="form-control-file" id="bookingstatutconfirmedicon" onChange="bookingstatutconfirmedpreview(this)"  required style="width:100%;">
                                    <span>Booking Statut Confirmed Image</span>
                                  </div>
                                 </div>
                                  <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="bookingconfirmediconwidth" id="bookingconfirmediconwidth" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="bookingconfirmediconheight" id="bookingconfirmediconheight" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Booking Statut Confirmed Image</span>
                                 </div>
                              </div>

                              <!-- Cancelled Image -->
                                <div class="col-md-2" style="margin-top: 5px;">
                                  <div class="col-md-12">
                                    <img  id="booking_statutcancelled_logo_preview" src="<?php echo base_url(); ?>assets/images/no-preview.jpg" style="width: 200px; height: 150px;border:1px solid #888;">
                                  </div>
                                  <div class="col-md-12">
                                  <div class="form-group">
                                    
                                    <input type="file" name="bookingstatutcancelledicon" class="form-control-file" id="bookingstatutcancelledicon" onChange="bookingstatutcancelledpreview(this)"  required style="width:100%;">
                                    <span>Booking Statut Cancelled Image</span>
                                  </div>
                                 </div>
                                  <div class="col-md-9">
                                   <div class="col-md-6" style="padding-left:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>W : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="bookingcancellediconwidth" id="bookingcancellediconwidth" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right:0px;">
                                     <div class="col-md-3" style="padding:0px;">
                                     <label>H : </label>
                                     </div>
                                      <div class="col-md-9" style="padding:0px;">
                                        <input type="number" name="bookingcancellediconheight" id="bookingcancellediconheight" class="form-control" required style="height:25px;padding-right: 0px;">
                                      </div>
                                   </div>
                                 </div>
                                 <div class="col-md-12">
                                    <span>Booking Statut Cancelled Image</span>
                                 </div>
                              </div>
                        <!-- Cancelled Image -->
                            </div>
                            <div class="col-md-12" style="padding:0px;">
                              <div class="col-md-6" style="margin-top: 5px;display: none;"  id="pricemessage">
                                <div class="form-group">
                                  <span>Front Quote Price Texte</span>
                                  <textarea id="pricemessagecontent" class="form-control" name="message" rows="8" cols="80"></textarea>
                                </div>
                              </div>
                               <div class="col-md-6" style="margin-top: 5px;">
                                <div class="form-group">
                                  <span>Quote Custom Texte</span>
                                  <textarea id="customtextcontent" class="form-control" name="customtext" rows="8" cols="80"></textarea>
                                </div>
                              </div>
                            </div>
                              <div class="col-md-12">   
                                                       
                                <button  class="btn btn-default"style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                                 <a  href="<?= base_url(); ?>admin/booking_config" type="button" class="btn btn-default" style="float:right; margin-left:7px;">
                                  <span class="fa fa-close"> Cancel </span></a>
                              
                           
                               </div> 
                                
                            <?php echo form_close(); ?>
                          
                            </div>
                        <?php  endif; ?>
                      
                        </div>
           


<script type="text/javascript">
CKEDITOR.replace('pricemessagecontent');
CKEDITOR.replace('customtextcontent');
   function showpricemessage(){
  
        var status=document.getElementById('pricestatus').value;
      
        if(status==0){
            
            document.getElementById('pricemessage').style.display='block';
        }else{
            document.getElementById('pricemessage').style.display='none';
        }
    }
    
function custompreview(input) {

        if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
            var img = new Image;
            img.onload = function() {
              $('#customiconwidth').val('');
               $('#customiconwidth').val(img.width);
               $('#customiconheight').val('');
               $('#customiconheight').val(img.height);
            };
            img.src = reader.result;
            document.querySelector('#customicon_logo_preview').setAttribute('src', reader.result);
          };
          reader.readAsDataURL(input.files[0]);
        }
}
function statutpendingpreview(input) {
   if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
            var img = new Image;
            img.onload = function() {
              $('#pendingiconwidth').val('');
               $('#pendingiconwidth').val(img.width);
               $('#pendingiconheight').val('');
               $('#pendingiconheight').val(img.height);
            };
            img.src = reader.result;
            document.querySelector('#statutpending_logo_preview').setAttribute('src', reader.result);
          };
          reader.readAsDataURL(input.files[0]);
        }
}
function statutapprovedpreview(input) {
   if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
            var img = new Image;
            img.onload = function() {
              $('#approvediconwidth').val('');
               $('#approvediconwidth').val(img.width);
               $('#approvediconheight').val('');
               $('#approvediconheight').val(img.height);
            };
            img.src = reader.result;
            document.querySelector('#statutapproved_logo_preview').setAttribute('src', reader.result);
          };
          reader.readAsDataURL(input.files[0]);
        }
}
function statutdeniedpreview(input) {
   if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
            var img = new Image;
            img.onload = function() {
              $('#deniediconwidth').val('');
               $('#deniediconwidth').val(img.width);
               $('#deniediconheight').val('');
               $('#deniediconheight').val(img.height);
            };
            img.src = reader.result;
            document.querySelector('#statutdenied_logo_preview').setAttribute('src', reader.result);
          };
          reader.readAsDataURL(input.files[0]);
        }
}
function statutcancelledpreview(input) {
   if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
            var img = new Image;
            img.onload = function() {
              $('#cancellediconwidth').val('');
               $('#cancellediconwidth').val(img.width);
               $('#cancellediconheight').val('');
               $('#cancellediconheight').val(img.height);
            };
            img.src = reader.result;
            document.querySelector('#statutcancelled_logo_preview').setAttribute('src', reader.result);
          };
          reader.readAsDataURL(input.files[0]);
        }
}
function bookingcustompreview(input) {

        if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
            var img = new Image;
            img.onload = function() {
               $('#bookingcustomiconwidth').val('');
               $('#bookingcustomiconwidth').val(img.width);
               $('#bookingcustomiconheight').val('');
               $('#bookingcustomiconheight').val(img.height);
            };
            img.src = reader.result;
            document.querySelector('#booking_customicon_logo_preview').setAttribute('src', reader.result);
          };
          reader.readAsDataURL(input.files[0]);
        }
}
function bookingstatutpendingpreview(input) {
   if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
            var img = new Image;
            img.onload = function() {
              $('#bookingpendingiconwidth').val('');
               $('#bookingpendingiconwidth').val(img.width);
               $('#bookingpendingiconheight').val('');
               $('#bookingpendingiconheight').val(img.height);
            };
            img.src = reader.result;
            document.querySelector('#booking_statutpending_logo_preview').setAttribute('src', reader.result);
          };
          reader.readAsDataURL(input.files[0]);
        }
}
function bookingstatutconfirmedpreview(input) {
   if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
            var img = new Image;
            img.onload = function() {
              $('#bookingconfirmediconwidth').val('');
               $('#bookingconfirmediconwidth').val(img.width);
               $('#bookingconfirmediconheight').val('');
               $('#bookingconfirmediconheight').val(img.height);
            };
            img.src = reader.result;
            document.querySelector('#booking_statutconfirmed_logo_preview').setAttribute('src', reader.result);
          };
          reader.readAsDataURL(input.files[0]);
        }
}
function bookingstatutcancelledpreview(input) {
   if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
            var img = new Image;
            img.onload = function() {
               $('#bookingcancellediconwidth').val('');
               $('#bookingcancellediconwidth').val(img.width);
               $('#bookingcancellediconheight').val('');
               $('#bookingcancellediconheight').val(img.height);
            };
            img.src = reader.result;
            document.querySelector('#booking_statutcancelled_logo_preview').setAttribute('src', reader.result);
          };
          reader.readAsDataURL(input.files[0]);
        }
}
             </script>
             