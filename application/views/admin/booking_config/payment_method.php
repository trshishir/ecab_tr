<div class="row">
  <div class="ListPayment_method" >
    <!-- <div class="col-md-12">
      <div class="page-action"style="float: right; margin-bottom:5px;" >
        <button class="btn btn-sm btn-default" onclick="PaymentMethodAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>&nbsp;
        <button class="btn btn-sm btn-default" onclick="PaymentMethodEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>&nbsp;
        <button class="btn btn-sm btn-default" onclick="PaymentMethodDelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button>&nbsp;
      </div>
    </div> -->
  <input type="hidden" class="chk-Add-payment_method-btn" value="">
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center"style="width:2%;">#</th>
            <th class="text-center" style="width:3% !important;"><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('added_by'); ?></th>
            <th class="column-time text-center"style="width:8%;">Client Category</th>
            <th class="column-time text-center"style="width:8%;">Name</th>
            <th class="column-time text-center"style="width:8%;">Description</th>  
            <th class="column-time text-center"style="width:8%;">Order By</th>        
            <th class="column-time text-center"style="width:8%;">Status</th>
            <th class="text-center"style="width:8%;">Since</th>
          </tr>
          </thead>
          <tbody>
              <?php if (!empty($payment_method)): ?>
              <?php $cnt=1; ?>
              <?php foreach($payment_method as $key => $item):?>
                  <tr>
                    <td class="text-center">
                        <input type="checkbox" class="chk-mainservice-cat-template" data-input="<?=$item->id?>">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="Payment_methodidEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->created_at)) .'0000'. $item->id;?></a></td>
                      <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                      <td class="text-center"><?=date("H:i:s", strtotime($item->since))?></td>
                     <td class="text-center"> 
                       <?php 
                                  $user=$this->bookings_config_model->getuser($item->user_id);
                                  $user=str_replace(".", " ", $user);
                                  echo $user;
                                  ?></td>
                         <td class="text-center">
                          <?php if($item->category_client== '1'): ?>
                            <span>Credit Card</span>
                          <?php elseif($item->category_client== '2'): ?>
                              <span>Bank Direct Debit</span>
                          <?php else: ?>
                              <span>Others</span>
                          <?php endif; ?>
                         
                            
                          </td>
                      <td class="text-center"><?=$item->name;?></td>
                       <td class="text-center" style="white-space: nowrap;overflow: hidden !important;text-overflow: ellipsis;max-width:100px;"><?= htmlspecialchars($item->description);?></td>
           
                    
                       <td class="text-center"><?=$item->paymentorder;?></td>
                    
                      <td class="text-center"><?=$item->status=='1'?'<span class="label label-success">Active</span>':'<span class="label label-danger">Hide</span>';?></td>
                      <td class="text-center"><?= timeDiff($item->since) ?></td>
                  </tr>
                  <?php $cnt++; ?>
              <?php endforeach; ?>
              <?php endif; ?>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>
<?=form_open("admin/booking_config/paymentmethodadd")?>
<!-- <form action="<?=base_url();?>admin/booking_config/vatadd" class="" method="post"  enctype="multipart/form-data"> -->
<div class="Payment_methodadd" style="display: none;" >
  <div class="col-md-12">
   <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span>Status</span>
      <select class="form-control" name="payment_method_statut" required>
        
        <option value="1">Show</option>
        <option value="0">Hide</option>
      </select>
    </div>
  </div>
  <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span>Client Category </span>
      <select class="form-control" name="category_client" required id="category_client" onchange="checkclientCategory()">
      
        <option value="0">Others</option>
        <option value="1">Credit Card</option>
        <option value="2">Bank Direct Debit</option>
      </select>
    </div>
  </div>
 

  <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span> Name </span>
        <input type="text" class="form-control" name="name" placeholder="" value="" required>
    </div>
  </div>

  <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span>Order By </span>
      <select class="form-control" name="paymentorder" required >
      
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
      </select>
    </div>
  </div>
  
  </div>
  <div class="col-md-12" style="margin-top: 5px;"  id="description_categories">
        <div class="form-group">
          <span>Description</span>
          <textarea id="textareacontent" class="form-control" name="description" rows="8" cols="80"></textarea>
        </div>
      </div>

  
  <div class="col-md-12" style="margin-top: 5px;">
  <button  class="btn btn-default"style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
    <button type="button" class="btn btn-default" style="float:right" onclick="cancle_Payment_method()"><span class="fa fa-close"> Cancel </span></button>
    
    <?php echo form_close(); ?>
  </div>
</div>
<div class="payment_methodEdit" style="display:none">
  <?=form_open_multipart("admin/booking_config/paymentmethodedit")?>
  <div class="payment_methodajax">
  </div>

  <div class="col-md-12" style="margin-top: 5px;">
  <button  class="btn btn-default"style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
    <a class="btn btn-default" style="margin-right:7px; float:right; margin-left:7px;color:#555;" onclick="cancle_Payment_method()"><span class="fa fa-close"> Cancel </span></a>
  
  </div>
  <?php echo form_close(); ?>
</div>
<div class="col-md-12 payment_methodDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
  <?=form_open("admin/booking_config/deletepaymentmethod")?>
    <input  name="tablename" value="vbs_job_statut" type="hidden" >
    <input type="hidden" id="payment_method-deletid" name="delet_payment_method_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn btn-default" style=" float:left;margin-right: 10px;"> Yes </button>
    <a class="btn btn-default" style="cursor: pointer;" onclick="cancle_Payment_method()">No</a>
  <?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">

  $(document).ready(function(){
    var csrf_token=$("input[name=csrf_test_name]").val();
    console.log("csrf_token:"+csrf_token);
    var x = document.getElementById("category_client").value;
  if(x==1 || x==2){
   document.getElementById("description_categories").style.display = "none"; 
  
  }else{
   
    document.getElementById("description_categories").style.display = "block"; 
  }
CKEDITOR.replace('textareacontent');
  });
  function checkclientCategory() {
 var x = document.getElementById("category_client").value;
  if(x==1 || x==2){
   document.getElementById("description_categories").style.display = "none"; 
  
  }else{
    document.getElementById("description_categories").style.display = "block"; 
  }
  }
    function editcheckclientCategory() {
var x = document.getElementById("editcategory_client").value;
  if(x==1 || x==2){
  document.getElementById("editdescription_categories").style.display = "none";
  }else{
     document.getElementById("editdescription_categories").style.display = "block";
  }
  }
</script>

<script type="text/javascript">
function PaymentMethodAdd()
{
  
  setupDivPayment_method();
  $(".Payment_methodadd").show();
}

function cancle_Payment_method()
{
  setupDivPayment_method();
  $(".ListPayment_method").show();
}
// function PaymentMethodEdit()
// {
//   var val = $('.chk-Add-payment_method-btn').val();
//   if(val != "")
//   {
//     setupDivPayment_method();
//     // $(".payment_methodEdit").show();
//     $("#payment_methodEditview"+val).show();
//   }else{
//     alert('Please select a row to edit.');
//   }
// }
function PaymentMethodEdit()
{
    var val = $('.chk-Add-payment_method-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivPayment_method();
        $(".payment_methodEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/booking_config/get_ajax_payment_method'; ?>',
                data: {'payment_method_id': val},
                success: function (result) {
                   var csrf_token=$("input[name=csrf_test_name]").val();
                  document.getElementsByClassName("payment_methodajax")[0].innerHTML = result;
          CKEDITOR.replace('edittextareacontent');        
         var x = document.getElementById("editcategory_client").value;
                 if(x==1 || x==2){
                  document.getElementById("editdescription_categories").style.display = "none";
                  }else{
                     document.getElementById("editdescription_categories").style.display = "block";
                  }
                   $('.bdatepicker').datepicker({
                          format: "dd/mm/yyyy"
                      });
                }
            });
}
function Payment_methodidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivPayment_method();
      $(".payment_methodEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/booking_config/get_ajax_payment_method'; ?>',
              data: {'payment_method_id': val},
              success: function (result) {
               var csrf_token=$("input[name=csrf_test_name]").val();
                document.getElementsByClassName("payment_methodajax")[0].innerHTML = result;
                  CKEDITOR.replace('edittextareacontent');
                   var x = document.getElementById("editcategory_client").value;
                 if(x==1 || x==2){
                  document.getElementById("editdescription_categories").style.display = "none";
                  }else{
                     document.getElementById("editdescription_categories").style.display = "block";
                  }
                   $('.bdatepicker').datepicker({
                          format: "dd/mm/yyyy"
                      });
              }
          });
}
function PaymentMethodDelete()
{
  var val = $('.chk-Add-payment_method-btn').val();
  if(val != "")
  {
  setupDivPayment_method();
  $(".payment_methodDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivPayment_method()
{
  // $(".payment_methodEditEdit").hide();
  $(".Payment_methodadd").hide();
  $(".payment_methodEdit").hide();
  $(".ListPayment_method").hide();
  $(".payment_methodDelete").hide();

}
function setupDivPayment_methodConfig()
{
  $(".ListPayment_method").show();
  $(".Payment_methodadd").hide();
  $(".payment_methodEdit").hide();
  $(".payment_methodDelete").hide();

}
$('input.chk-mainservice-cat-template').on('change', function() {
  $('input.chk-mainservice-cat-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Add-payment_method-btn').val(id);
  $('#payment_method-deletid').val(id);
});

</script>
