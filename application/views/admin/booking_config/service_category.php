<style>



#checkboxes {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes label {
  display: block;
}

#checkboxes label:hover {
  background-color: #1e90ff;
}
#editcheckboxes {
  display: none;
  border: 1px #dadada solid;
}

#editcheckboxes label {
  display: block;
}

#editcheckboxes label:hover {
  background-color: #1e90ff;
}
</style>
<div class="row">
  <div class="ListService_cat" >
    <!-- <div class="col-md-12">
      <div class="page-action"style="float: right; margin-bottom:5px;" >
        <button class="btn btn-sm btn-default" onclick="ServiceCatAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>&nbsp;
        <button class="btn btn-sm btn-default" onclick="ServiceCatEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>&nbsp;
        <button class="btn btn-sm btn-default" onclick="ServiceCatDelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button>&nbsp;
      </div>
    </div> -->
  <input type="hidden" class="chk-Add-service_cat-btn" value="">
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center"style="width:2%;">#</th>
            <th class="text-center" style="width:3% !important;"><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('added_by'); ?></th>
            <th class="column-first_name text-center" style="width:6% !important; ">Category</th>
            <th class="column-first_name text-center" style="width:6% !important; ">Payment Methodes</th>
            <th class="column-time text-center"style="width:8%;">Invoice periode</th>

            <th class="column-time text-center"style="width:8%;">Statut</th>
            <th class="text-center"style="width:8%;">Since</th>
 
          </tr>
          </thead>
          <tbody>
              <?php if (!empty($service_cat)): ?>
              <?php $cnt=1; ?>
              <?php foreach($service_cat as $key => $item):?>
                  <tr>
                    <td class="text-center">
                        <input type="checkbox" class="chk-mainservice-cat-template" data-input="<?=$item->id?>">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="Service_catidEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->created_at)) .'0000'. $item->id;?></a></td>
                      <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                      <td class="text-center"><?=date("H:i:s", strtotime($item->since))?></td>
                    <td class="text-center"> 
                       <?php 
                                  $user=$this->bookings_config_model->getuser($item->user_id);
                                  $user=str_replace(".", " ", $user);
                                  echo $user;
                                  ?></td>
                      <td class="text-center"><?=$item->category_name;?></td>
                       <td class="text-center"><?php
                       $methodstr="";
                        $paymentmethods=$this->bookings_config_model->getpaymentmethodsbycategory($item->id);
                        foreach ($paymentmethods as $key => $method) {
                         $methodstr.=$method->name.",";
                        }
                        if(!empty($methodstr)){
                             echo rtrim($methodstr,",");
                        }else{
                          echo $methodstr;
                        }
                       ?></td>
                       <td class="text-center"><?=$item->invoice_periode =='1'?'<span>Instantly</span>':'<span>Monthly</span>';?></td>
         
                      
                      <td class="text-center"><?=$item->statut=='1'?'<span class="label label-success">Active</span>':'<span class="label label-danger">Hide</span>';?></td>
                      <td class="text-center"><?= timeDiff($item->since) ?></td>
                  </tr>
                  <?php $cnt++; ?>
              <?php endforeach; ?>
              <?php endif; ?>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>
<?=form_open("admin/booking_config/servicecatadd")?>
<!-- <form action="<?=base_url();?>admin/booking_config/vatadd" class="" method="post"  enctype="multipart/form-data"> -->
<div class="Service_catadd" style="display: none;" >
   <div class="col-md-12">
   <div class="col-md-3" style="margin-top: 5px;">
    <div class="form-group">
      <span>Statut</span>
      <select class="form-control" name="service_cat_statut" required>
        <option value="1">Show</option>
        <option value="2">Hide</option>
      </select>
    </div>
  </div>

   <div class="col-md-3" style="margin-top: 5px;">
    <div class="form-group">
      <span> Service Category </span>
        <input type="text" class="form-control" name="name_of_category" placeholder="" value="">
    </div>
  </div>
   </div>
    <div class="col-md-12">
  <div class="col-md-3" style="margin-top: 5px;">
   <div class="multiselect">
    <div class="selectBox" onclick="showCheckboxes()">
       <span> Payment Methodes </span>
      <select class="form-control">
        <option disabled="disabled" selected="selected" style="display:none;">Payment Methodes</option>
      </select>
     
    </div>
    <div id="checkboxes">
        <?php foreach($payment_method as $key => $value):?>
      <label for="one" style="background-color:white;margin-bottom:0px;border-bottom: 1px solid #eeeeee;">
        <input type="checkbox" name="paymentmethod[]" value="<?=$value->id;?>" style="float:none;margin:10px;"/><?=$value->name;?></label>
        <?php endforeach; ?>
    </div>
  </div>
  </div>
    <div class="col-md-3" style="margin-top: 5px;">
        <div class="form-group">
          <span> Invoice Periode </span>
          <select class="form-control" name="invoice_periode" required>
            <option value="1">Instantly</option>
            <option value="2">Monthly</option>
          </select>
        </div>
      </div>


     </div> 
 

 
  <div class="col-md-12" style="margin-top: 5px;">
  <button  class="btn  btn-default"style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
    <button type="button" class="btn btn-default" style="float:right" onclick="cancle_Service_cat()"><span class="fa fa-close"> Cancel </span></button>
    
    <?php echo form_close(); ?>
  </div>
</div>
<div class="service_catEdit" style="display:none">
  <?=form_open("admin/booking_config/servicecatedit")?>
  <div class="service_catajax">
  </div>
  
  <div class="col-md-12" style="margin-top: 5px;">
  <button  class="btn btn-default"style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
    <button type="button" class="btn btn-default" style="margin-right:7px; float:right; margin-left:7px;" onclick="cancle_Service_cat()"><span class="fa fa-close"> Cancel </span></button>
  
  </div>
  <?php echo form_close(); ?>
</div>
<div class="col-md-12 service_catDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
  <?=form_open("admin/booking_config/deleteservicecat")?>
    <input  name="tablename" value="vbs_job_statut" type="hidden" >
    <input type="hidden" id="service_cat-deletid" name="delet_service_cat_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn btn-default" style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>
    <a class="btn btn-default" style="cursor: pointer;" onclick="cancle_Service_cat()">No</a>
  <?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
  var expanded = false;

function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}
 var editexpanded = false;

function editshowCheckboxes() {
  var checkboxes = document.getElementById("editcheckboxes");
  if (!editexpanded) {
    checkboxes.style.display = "block";
    editexpanded = true;
  } else {
    checkboxes.style.display = "none";
    editexpanded = false;
  }
}
function ServiceCatAdd()
{
  setupDivService_cat();
  $(".Service_catadd").show();
}

function cancle_Service_cat()
{
  setupDivService_cat();
  $(".ListService_cat").show();
}
// function ServiceCatEdit()
// {
//   var val = $('.chk-Add-service_cat-btn').val();
//   if(val != "")
//   {
//     setupDivService_cat();
//     // $(".service_catEdit").show();
//     $("#service_catEditview"+val).show();
//   }else{
//     alert('Please select a row to edit.');
//   }
// }
function ServiceCatEdit()
{
    var val = $('.chk-Add-service_cat-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivService_cat();
        $(".service_catEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/booking_config/get_ajax_service_cat'; ?>',
                data: {'service_cat_id': val},
                success: function (result) {
                  // alert(result);
                  document.getElementsByClassName("service_catajax")[0].innerHTML = result;
                }
            });
}
function Service_catidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivService_cat();
      $(".service_catEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/booking_config/get_ajax_service_cat'; ?>',
              data: {'service_cat_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("service_catajax")[0].innerHTML = result;
              }
          });
}
function ServiceCatDelete()
{
  var val = $('.chk-Add-service_cat-btn').val();
  if(val != "")
  {
  setupDivService_cat();
  $(".service_catDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivService_cat()
{
  // $(".service_catEditEdit").hide();
  $(".Service_catadd").hide();
  $(".service_catEdit").hide();
  $(".ListService_cat").hide();
  $(".service_catDelete").hide();

}
function setupDivService_catConfig()
{
 $(".ListService_cat").show();
  $(".Service_catadd").hide();
  $(".service_catEdit").hide();
  $(".service_catDelete").hide();

}
$('input.chk-mainservice-cat-template').on('change', function() {
  $('input.chk-mainservice-cat-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Add-service_cat-btn').val(id);
  $('#service_cat-deletid').val(id);
});

</script>
