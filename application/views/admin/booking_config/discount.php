
<div class="row">
  <div class="ListDiscount" >
  <input type="hidden" class="chk-Adddiscount-btn" value="">
  <!-- <div class="col-md-12">
      <div class="toolbar"style="float: right; margin-bottom:5px;">
        <button class="btn btn-sm btn-default" onclick="Discountadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>&nbsp;
        <button class="btn btn-sm btn-default" onclick="DiscountEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>&nbsp;
        <button class="btn btn-sm btn-default" onclick="DiscountDelete()"><i class="fa fa-trash"></i> <span class="delete-icon">Delete</span></button>&nbsp;
      </div>
    </div> -->
  <div class="col-md-12">
  <div class="module-body table-responsive">

      <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
          <thead>
          <tr>
            <th class="no-sort text-center" style="width:2%;">#</th>
            <th class="text-center" style="width:10px !important;"><?php echo $this->lang->line('id'); ?></th>
            <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('date'); ?></th>
            <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('time'); ?></th>
            <th class="text-center" style="width:10% !important;"><?php echo $this->lang->line('added_by'); ?></th>
             <th class="text-center" style="width:6% !important;">Category</th>
            <th class="column-first_name text-center" style="width:8% !important; "><?php echo $this->lang->line('name'); ?></th>
               <th class="text-center" style="width:6% !important;">From Date</th>
               <th class="text-center" style="width:6% !important;">To Date</th>
               <th class="text-center" style="width:6% !important;">From Time</th>
               <th class="text-center" style="width:6% !important;">To Time</th>
            <th class="column-date text-center"  style="width:5% !important;"><?php echo $this->lang->line('discount'); ?>%</th>
            
            <th class="column-time text-center" style="width:12% !important;"><?php echo $this->lang->line('status'); ?></th>
            <th class="text-center" style="width:12% !important;">Since</th>

          </tr>
          </thead>
          <tbody>
              <?php if (!empty($discount_data)): ?>
              <?php $cnt=1; ?>
              <?php foreach($discount_data as $key => $item):?>
                  <tr>
                    <td class="no-sort text-center"style="width:2%;">
                        <input type="checkbox" class="chk-maindiscount-template" data-input="<?=$item->id?>">
                    </td>
                      <td class="text-center"><a href="javascript:void()" onclick="DiscountidEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->created_at)) .'0000'. $item->id;?></a></td>
                      <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                      <td class="text-center"><?=date("H:i:s", strtotime($item->since))?></td>
                      <td class="text-center"><?= $this->bookings_config_model->getuser($item->user_id);?></td>
                      <td class="text-center">
                      
                        <?php
                            if($item->discount_category==0){
                              echo "welcome";
                            }
                            elseif($item->discount_category==1){
                              echo "Reward";
                            }
                             elseif($item->discount_category==2){
                              echo "Periode";
                            }
                             elseif($item->discount_category==3){
                              echo "Promo Code";
                            }
                         ?>
                          
                        </td>
                      <td class="text-center"><?=$item->name_of_discount;?></td>
                       <td class="text-center">
                       
                        <?php
                          if($item->discount_category==2){
                              $discountdatefrom=$item->datefrom;
                              $discountdatefrom=str_replace('-', '/', $discountdatefrom);
                              $discountdatefrom=strtotime($discountdatefrom);
                              $datefrom = date('d/m/Y',$discountdatefrom);
                              echo $datefrom;
                          }else{
                            echo "0000/00/00";
                          }
                         ?>
                          
                        </td>
                      <td class="text-center">
                       
                        <?php
                          if($item->discount_category==2){
                                 $discountdateto=$item->dateto;
                                 $discountdateto=str_replace('-', '/', $discountdateto);
                                 $discountdateto=strtotime($discountdateto);
                                 $dateto = date('d/m/Y',$discountdateto);
                              echo $dateto;
                          }else{
                            echo "0000/00/00";
                          }
                         ?>
                          
                        </td>
                      <td class="text-center">
                       
                        <?php
                          if($item->discount_category==2){
                            echo $item->timefrom;
                          }else{
                            echo "00:00";
                          }
                         ?>
                          
                        </td>
                      <td class="text-center">
                       
                        <?php
                          if($item->discount_category==2){
                            echo $item->timeto;
                          }else{
                            echo "00:00";
                          }
                         ?>
                          
                        </td>
                      <td class="text-center"><?=$item->discount; ?></td>
                     
                      <td class="text-center"><?=$item->statut=='1'?'<span class="label label-success">Active</span>':'<span class="label label-danger">Hide</span>';?></td>
                      <td class="text-center"><?= timeDiff($item->since) ?></td>

                  </tr>
                  <?php $cnt++; ?>
              <?php endforeach; ?>
              <?php endif; ?>
          </tbody>
      </table>
      <br>
    </div>
  </div>
</div>
<?=form_open("admin/booking_config/discountadd")?>
<!-- <form action="<?=base_url();?>admin/booking_config/vatadd" class="" method="post"  enctype="multipart/form-data"> -->
<div class="Discountadd" style="display: none;" >
  <div class="col-md-12">
  <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span>Statut</span>
      <select class="form-control" name="discount_statut" required>
        <option value="">Statut</option>
        <option value="1">Show</option>
        <option value="2">Hide</option>
      </select>
    </div>
  </div>
  <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span>Discount Category</span>
      <select class="form-control" id="discount_category" name="discount_category" required onchange="checkCategory()">
        <option value="0">Welcome</option>
        <option value="1">Reward</option>
        <option value="2">Periode</option>
        <option value="3">Promo Code</option>
      </select>
    </div>
  </div>
  <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span>Name Of Discount</span>
        <input type="text" class="form-control" name="name_of_discount" placeholder="" value="">
    </div>
  </div>
  <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span>Discount%</span>
        <input type="text" class="form-control" name="discount" placeholder="" value="">
    </div>
  </div>

</div>
<div class="col-md-12" id="discountpromocodediv">
    <div class="col-md-2" style="margin-top: 5px;">
    <div class="form-group">
      <span>Promo Code</span>
        <input type="text" class="form-control" name="promo_code" placeholder="" value="">
    </div>
  </div>
</div>
<div class="col-md-12" id="discountperiodsdiv">
  <div class="col-md-2">
    
      <div class="col-md-6" style="padding:2px;">  
       <div class="form-group">
       <span>From Date</span>  
         <input   id="discountdatefrom" class="bdatepicker" name="discountdatefrom" type="text" value="<?php echo date('d/m/Y');?>"  style="width:100%;" />
      </div>
     </div>   
      
      <div class="col-md-6" style="padding:2px;">
         <div class="form-group">
           <span>To Date</span>
        <input   id="discountdateto" class="bdatepicker" name="discountdateto" type="text" value="<?php echo date('d/m/Y');?>" style="width:100%;" />
        </div>
     </div>
</div>

  <div class="col-md-2">
     
      <div class="col-md-6" style="padding:2px;">
        <div class="form-group">
           <span>From Time</span>
           <input   id="discounttimefrom" name="discounttimefrom" type="text" value="<?php echo date("h : i") ?>" style="width:100%;" />
      </div>
    </div>

      
      <div class="col-md-6" style="padding:2px;">
        <div class="form-group">
           <span>To Time</span>
           <input   id="discounttimeto" name="discounttimeto" type="text" value="<?php echo date("h : i") ?>" style="width:100%;" />
      </div>
    </div>
       
</div>

</div>

  <div class="col-md-12 clearfix">
  <button  class="btn"style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
    <button type="button" class="btn" style="float:right; margin-left:7px;" onclick="cancelDiscount()"><span class="fa fa-close"> Cancel </span></button>

  <?php echo form_close(); ?>
</div>
</div>
<div class="discountEdit" style="display:none">
  <?=form_open("admin/booking_config/discountedit")?>
  <div class="discountEditajax">
  </div>
  <div class="col-md-12 clearfix">
  <button  class="btn"style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
    <button type="button" class="btn" style="float:right; margin-left:7px;" onclick="cancelDiscount()"><span class="fa fa-close"> Cancel </span></button>

  </div>
  <?php echo form_close(); ?>
</div>
<div class="col-md-12 discountDelete" style="border:1px solid #ccc;padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
  <?=form_open("admin/booking_config/deletediscount")?>
    <input  name="tablename" value="vbs_job_statut" type="hidden" >
    <input type="hidden" id="discountdeletid" name="delet_discount_id" value="">
    <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
    <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
    <button  class="btn"style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

    <a class="btn" style="cursor: pointer;" onclick="cancelDiscount()">No</a>
  <?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
function adddiscountperiod(){
         var datefrom=$("#discountdatefrom").val();
         var dateto=$("#discountdateto").val();
        var timefrom=$("#discounttimefrom").val();
         var timeto=$("#discounttimeto").val();
        
             $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/booking_config/add_discount_periods'; ?>',
                data: {'datefrom': datefrom,'dateto':dateto,'timefrom':timefrom,'timeto':timeto},
                success: function (data) {
                   $('#discountperioddiv').empty();
          $.each(data.periods, function(k, v) {
           $("#discountperioddiv").append('<li class="text-left" style="display:inline-block;margin-right:5px;"><button type="button" style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;outline:none;color:#4ca6cf;" class="dateremovebtn" onclick="removediscountperiod('+k+');"><i class="fa fa-close"></i></button>'+v["discountdatefrom"]+' to '+v["discountdateto"]+' '+v["discounttimefrom"]+' to '+v["discounttimeto"]+'</li>');
          
           });
                }
            });
}
function removediscountperiod(val){

     $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/booking_config/remove_discount_periods'; ?>',
                data: {'index': val},
                success: function (data) {
                   $('#discountperioddiv').empty();
          $.each(data.periods, function(k, v) {
           $("#discountperioddiv").append('<li class="text-left" style="display:inline-block;margin-right:5px;"><button type="button" style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;outline:none;color:#4ca6cf;" class="dateremovebtn" onclick="removediscountperiod('+k+');"><i class="fa fa-close"></i></button>'+v["discountdatefrom"]+' to '+v["discountdateto"]+' '+v["discounttimefrom"]+' to '+v["discounttimeto"]+'</li>');
          
           });
                }
            });
}
function editdiscountperiod(){
         var datefrom=$("#editdiscountdatefrom").val();
         var dateto=$("#editdiscountdateto").val();
        var timefrom=$("#editdiscounttimefrom").val();
         var timeto=$("#editdiscounttimeto").val();
        
             $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/booking_config/add_discount_periods'; ?>',
                data: {'datefrom': datefrom,'dateto':dateto,'timefrom':timefrom,'timeto':timeto},
                success: function (data) {
                   $('#editdiscountperioddiv').empty();
          $.each(data.periods, function(k, v) {
           $("#editdiscountperioddiv").append('<li class="text-left" style="display:inline-block;margin-right:5px;"><button type="button" style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;outline:none;color:#4ca6cf;" class="dateremovebtn" onclick="editremovediscountperiod('+k+');"><i class="fa fa-close"></i></button>'+v["discountdatefrom"]+' to '+v["discountdateto"]+' '+v["discounttimefrom"]+' to '+v["discounttimeto"]+'</li>');
          
           });
          
                      }
            });
}
function editremovediscountperiod(val){

     $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/booking_config/remove_discount_periods'; ?>',
                data: {'index': val},
                success: function (data) {
                   $('#editdiscountperioddiv').empty();
          $.each(data.periods, function(k, v) {
           $("#editdiscountperioddiv").append('<li class="text-left" style="display:inline-block;margin-right:5px;"><button type="button" style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;outline:none;color:#4ca6cf;" class="dateremovebtn" onclick="editremovediscountperiod('+k+');"><i class="fa fa-close"></i></button>'+v["discountdatefrom"]+' to '+v["discountdateto"]+' '+v["discounttimefrom"]+' to '+v["discounttimeto"]+'</li>');
          
           });
                }
            });
}
function Discountadd()
{
  setupDivDiscount();
  $(".Discountadd").show();
}

function cancelDiscount()
{
  setupDivDiscount();
  $(".ListDiscount").show();
}
// function DiscountEdit()
// {
//   var val = $('.chk-Adddiscount-btn').val();
//   if(val != "")
//   {
//     setupDivDiscount();
//     $(".discountEdit").show();
//     $("#discountEditview"+val).show();
//   }
// }

function DiscountEdit()
{
    var val = $('.chk-Adddiscount-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivDiscount();
        $(".discountEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/booking_config/get_ajax_discount'; ?>',
                data: {'discount_id': val},
                success: function (result) {
                  // alert(result);
                  document.getElementsByClassName("discountEditajax")[0].innerHTML = result;
                   $('#editdiscounttimefrom').timepicki({
                      step_size_minutes:'5',
                      show_meridian: false,
                      min_hour_value: 0,
                      max_hour_value: 23,
                      overflow_minutes: true,
                      increase_direction: 'up',
                      disable_keyboard_mobile: true});
                    $('#editdiscounttimeto').timepicki({
                          step_size_minutes:'5',
                        show_meridian: false,
                        min_hour_value: 0,
                        max_hour_value: 23,
                        overflow_minutes: true,
                        increase_direction: 'up',
                        disable_keyboard_mobile: true});
                    $('.bdatepicker').datepicker({
                          format: "dd/mm/yyyy"
                      });
                     var x = document.getElementById("editdiscount_category").value;
  if(x==0){
  document.getElementById("editdiscountperiodsdiv").style.display = "none"; 
  document.getElementById("editdiscountpromocodediv").style.display = "none";
  }
  else if (x==1){ 
  document.getElementById("editdiscountperiodsdiv").style.display = "none";
  document.getElementById("editdiscountpromocodediv").style.display = "none";
  }
  else if(x==2){ 
  document.getElementById("editdiscountperiodsdiv").style.display = "block"; 
  document.getElementById("editdiscountpromocodediv").style.display = "none";
  }
  else if(x==3){
  document.getElementById("editdiscountperiodsdiv").style.display = "none"; 
  document.getElementById("editdiscountpromocodediv").style.display = "block";
  }
                }
            });
}
function DiscountidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivDiscount();
      $(".discountEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/booking_config/get_ajax_discount'; ?>',
              data: {'discount_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("discountEditajax")[0].innerHTML = result;
              }
          });
}
function DiscountDelete()
{
  var val = $('.chk-Adddiscount-btn').val();
  if(val != "")
  {
  setupDivDiscount();
  $(".discountDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivDiscount()
{
  // $(".discountEditEdit").hide();
  $(".Discountadd").hide();
  $(".discountEdit").hide();
  $(".ListDiscount").hide();
  $(".discountDelete").hide();

}
$('input.chk-maindiscount-template').on('change', function() {
  $('input.chk-maindiscount-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Adddiscount-btn').val(id);
  $('#discountdeletid').val(id);
});

//start dicount periods and promo code
  var x = document.getElementById("discount_category").value;
  if(x==0){
  document.getElementById("discountperiodsdiv").style.display = "none"; 
  document.getElementById("discountpromocodediv").style.display = "none";
  }
  else if (x==1){ 
  document.getElementById("discountperiodsdiv").style.display = "none";
  document.getElementById("discountpromocodediv").style.display = "none";
  }
  else if(x==2){ 
  document.getElementById("discountperiodsdiv").style.display = "block"; 
  document.getElementById("discountpromocodediv").style.display = "none";
  }
  else if(x==3){
  document.getElementById("discountperiodsdiv").style.display = "none"; 
  document.getElementById("discountpromocodediv").style.display = "block";
  }
 

function checkCategory() {
var x = document.getElementById("discount_category").value;
  if(x==0){
  document.getElementById("discountperiodsdiv").style.display = "none"; 
  document.getElementById("discountpromocodediv").style.display = "none";
  }
  else if (x==1){ 
  document.getElementById("discountperiodsdiv").style.display = "none";
  document.getElementById("discountpromocodediv").style.display = "none";
  }
  else if(x==2){ 
  document.getElementById("discountperiodsdiv").style.display = "block"; 
  document.getElementById("discountpromocodediv").style.display = "none";
  }
  else if(x==3){
  document.getElementById("discountperiodsdiv").style.display = "none"; 
  document.getElementById("discountpromocodediv").style.display = "block";
  }
 
}
//end discount period and promo code

//edit dicount periods and promo code
 
 

function editcheckCategory() {
  var x = document.getElementById("editdiscount_category").value;
  if(x==0){
  document.getElementById("editdiscountperiodsdiv").style.display = "none"; 
  document.getElementById("editdiscountpromocodediv").style.display = "none";
  }
  else if (x==1){ 
  document.getElementById("editdiscountperiodsdiv").style.display = "none";
  document.getElementById("editdiscountpromocodediv").style.display = "none";
  }
  else if(x==2){ 
  document.getElementById("editdiscountperiodsdiv").style.display = "block"; 
  document.getElementById("editdiscountpromocodediv").style.display = "none";
  }
  else if(x==3){
  document.getElementById("editdiscountperiodsdiv").style.display = "none"; 
  document.getElementById("editdiscountpromocodediv").style.display = "block";
  }
 
}
//edit discount period and promo code
</script>
