<div class="row">
  <div class="ListQuoteconfig" >
    <input type="hidden" class="chk-Addquoteconfig-btn" value="">
    <div class="col-md-12">
    <div class="module-body table-responsive">
        <table class="configTable cell-border configTable dataTable table data-table dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
            <thead>
            <tr>
              <th class="no-sort text-center"style="width:2%;">#</th>
              <th class="text-center" style="width:3% !important;"><?php echo $this->lang->line('id'); ?></th>
              <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('date'); ?></th>
              <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('time'); ?></th>
              <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('added_by'); ?></th>
              <th class="column-date text-center"style="width:8%;">Service Category</th>
              <th class="column-first_name text-center" style="width:6% !important; ">Available Date (days)</th> 
               <th class="column-first_name text-center" style="width:6% !important; ">Custom Icon</th> 
              
              <th class="text-center"style="width:8%;">Statut</th>
              <th class="text-center"style="width:8%;">Since</th>
              
            </tr>
            </thead>
            <tbody>
                <?php if (!empty($quoteconfig)): ?>
                <?php $cnt=1; ?>
                <?php foreach($quoteconfig as $key => $item):?>
                    <tr>
                      <td class="text-center">
                          <input type="checkbox" class="chk-mainquoteconfig-template" data-input="<?=$item->id?>">
                      </td>
                        <td class="text-center"><a href="javascript:void()" onclick="QuoteconfigidEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->created_at)) .'0000'. $item->id;?></a></td>
                        <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                        <td class="text-center"><?=date("H:i:s", strtotime($item->since))?></td>
                       <td class="text-center"> 
                       <?php 
                                  $user=$this->bookings_config_model->getuser($item->user_id);
                                  $user=str_replace(".", " ", $user);
                                  echo $user;
                                  ?></td>
                        <td class="text-center"><?= $this->bookings_config_model->getservicecategorybyid('vbs_u_category_service',$item->servicecategoryid );?></td>
                        <td class="text-center"><?= $item->availabledate  ?> days</td>
                        <?php if(!empty($item->customicon)): ?>
                        <td class="text-center"><img src="<?=base_url($item->customicon);?>" width="60" height="30" ></td>
                        <?php else: ?>
                          <td class="text-center">Null</td>
                        <?php endif; ?> 
                        <td class="text-center"><?=$item->statut=='1'?'<span class="label label-success">Active</span>':'<span class="label label-danger">Hide</span>';?></td>
                        <td class="text-center"><?= timeDiff($item->since) ?></td>

                    </tr>
                    <?php $cnt++; ?>
                <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
        <br>
      </div>
    </div>
  </div>
    <?=form_open_multipart("admin/booking_config/quoteconfigadd")?>
  <!-- <form action="<?=base_url();?>admin/booking_config/vatadd" class="" method="post"  enctype="multipart/form-data"> -->
  <div class="Quoteconfigadd" style="display: none;" >
<div class="col-md-12">
    <div class="col-md-2" style="margin-top: 5px;">
      <div class="form-group">
        <span>Statut</span>
        <select class="form-control" name="statut" required>
          <option value="1">Show</option>
          <option value="0">Hide</option>
        </select>
      </div>
      
    </div>
     <div class="col-md-2" style="margin-top: 5px;">
      <div class="form-group">
        <span>Service Category</span>
        <select class="form-control" name="servicecategoryid" required>
          <option value="">Select</option>
          <?php foreach($service_cat as $key => $value): ?>
          <option value="<?=$value->id;?>"><?=$value->category_name;?></option>
          <?php endforeach; ?>
        </select>
      </div>
    </div>
      <div class="col-md-2" style="margin-top: 5px;">
      <div class="form-group">
        <span>Available Date (days)</span>
          <input type="text" class="form-control" name="availabledate" placeholder="" value="" required>
      </div>
    </div>
    <div class="col-md-2" style="margin-top: 5px;">
          <div class="col-md-12">
            <img  id="custom_logo_preview" src="<?php echo base_url(); ?>uploads/custom_images/default_custom.png" style="width: 200px; height: 150px;border:1px solid #888;">
          </div>
          <div class="col-md-12">
          <div class="form-group">
            
            <input type="file" name="customicon" class="form-control-file" id="customicon" onChange="custompreview(this)"  required style="width:100%;">
            <span>Car Icone</span>
          </div>
        </div>
  </div>
  
 
  
  
  </div>
  <div class="col-md-12" style="margin-top: 5px;">
        <div class="form-group">
          <span>Comments</span>
          <textarea id="textareaquotecontent" class="form-control" name="comment" rows="8" cols="80"></textarea>
        </div>
      </div>
  
    <div class="col-md-12">
    <button  class="btn"style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
      <button type="button" class="btn" style="float:right; margin-left:7px;" onclick="cancelQuoteconfig()"><span class="fa fa-close"> Cancel </span></button>

      <?php echo form_close(); ?>
    </div>

  </div>
  <div class="quoteconfigEdit"  style="display:none;">
    <?=form_open_multipart("admin/booking_config/quoteconfigedit")?>
    <div class="quoteconfigEditajax">
    </div>
    <div class="col-md-12">
    <button  class="btn"style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
      <button type="button" class="btn" style="float:right; margin-left:7px;" onclick="cancelQuoteconfig()"><span class="fa fa-close"> Cancel </span></button>

    </div>
    <?php echo form_close(); ?>
  </div>
  <div class="col-md-12 quoteconfigDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
    <?=form_open("admin/booking_config/deletequoteconfig")?>
      <input  name="tablename" value="vbs_job_statut" type="hidden" >
      <input type="hidden" id="quoteconfigdeletid" name="delet_quoteconfig_id" value="">
      <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10;"> Are you sure you want to delete selected?</div>
      <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
      <button  class="btn"style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

      <a class="btn" style="cursor: pointer;" onclick="cancelQuoteconfig()">No</a>
    <?php echo form_close(); ?>
  </div>
</div>

<script type="text/javascript">
  CKEDITOR.replace('textareaquotecontent');
function QuoteconfigAdd()
{
  setupDivQuoteconfiginner();
  $(".Quoteconfigadd").show();
}

function cancelQuoteconfig()
{
  setupDivQuoteconfiginner();
  $(".ListQuoteconfig").show();
}


function QuoteconfigEdit()
{
    var val = $('.chk-Addquoteconfig-btn').val();
    if (val=='')
    {
        alert("Please select record!");
        return false;
    }
        setupDivQuoteconfiginner();
        $(".quoteconfigEdit").show();
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/booking_config/get_ajax_quoteconfig'; ?>',
                data: {'quoteconfig_id': val},
                success: function (result) {
                  // alert(result);
                  document.getElementsByClassName("quoteconfigEditajax")[0].innerHTML = result;
                  CKEDITOR.replace('edittextareaquotecontent');
                }
            });
}
function QuoteconfigidEdit(id){
  var val = id;
  if (val=='')
  {
      alert("Please select record!");
      return false;
  }
      setupDivQuoteconfiginner();
      $(".quoteconfigEdit").show();
      $.ajax({
              type: "GET",
              url: '<?php echo base_url().'admin/booking_config/get_ajax_quoteconfig'; ?>',
              data: {'quoteconfig_id': val},
              success: function (result) {
                // alert(result);
                document.getElementsByClassName("quoteconfigEditajax")[0].innerHTML = result;
                CKEDITOR.replace('edittextareaquotecontent');
              }
          });
}
function QuoteconfigDelete()
{
  var val = $('.chk-Addquoteconfig-btn').val();
  if(val != "")
  {
  setupDivQuoteconfiginner();
  $(".quoteconfigDelete").show();
  }else{
    alert('Please select a row to delete.');
  }
}

function setupDivQuoteconfiginner()
{
  // $(".quoteconfigEditEdit").hide();
  $(".Quoteconfigadd").hide();
  $(".quoteconfigEdit").hide();
  $(".ListQuoteconfig").hide();
  $(".quoteconfigDelete").hide();

}
function setupDivQuoteConfig()
{
   $(".ListQuoteconfig").show();
  $(".Quoteconfigadd").hide();
  $(".quoteconfigEdit").hide();
  $(".quoteconfigDelete").hide();

}
   function custompreview(e) {
  if (e.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e){
      document.querySelector('#custom_logo_preview').setAttribute('src', e.target.result);
    }
    reader.readAsDataURL(e.files[0]);
  }
}
function editcustompreview(e) {
  if (e.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e){
      document.querySelector('#editcustom_logo_preview').setAttribute('src', e.target.result);
    }
    reader.readAsDataURL(e.files[0]);
  }
}
$('input.chk-mainquoteconfig-template').on('change', function() {
  $('input.chk-mainquoteconfig-template').not(this).prop('checked', false);
  var id = $(this).attr('data-input');
  $('.chk-Addquoteconfig-btn').val(id);
  $('#quoteconfigdeletid').val(id);
});

</script>
