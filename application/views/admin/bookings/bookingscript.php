<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo  $googleapikey->api_key; ?>&libraries=places&callback=initAutocomplete" async defer></script>

<script type="text/javascript">
 $(document).ready(function(){
      calwaitingtime();
    $('.go-table').hide();
    $('.back-table').hide();
    $('.tmpr_txt').hide();
    $('#timeslotsrtn').hide();
   
  }); 

    $('.regular_check').change(function() {
      if($(this).is(":checked")) {
          $('#timeslotsrtn').show();
          $('.go-table').show();
          $(".pickupreturn_datetime_div").hide();    
          $(".startend_datetime_div").show();   
          if($('.return_car').is(":checked")){
              $(".back-table").show();
          }      
      }else{         
          $('.go-table').hide();
          $('#timeslotsrtn').hide();
          $('.back-table').hide();
          $(".pickupreturn_datetime_div").show();  
          $(".startend_datetime_div").hide();        
      }
  });


  $('.return_car').change(function() {     
      if($(this).is(":checked")){        
          $(".return_datetime_div").show();        
          if($('.regular_check').is(":checked")){
               $('#timeslotsrtn').show();
               $('.back-table').show();
          }       
      }else{          
           $(".return_datetime_div").hide();
           if($('.regular_check').is(":checked")){
              $('.back-table').hide();
            }
      }    
  });

  $('.wheelchair_carssh').change(function() {
      if($(this).is(":checked")) {
          $('#wheelchairdiv').show();
          $('#nonwheelchairdiv').hide();
          document.getElementById("carstypes").value='';
          document.getElementById("carstypes").value='1';
      }else{
          $('#nonwheelchairdiv').show();
          $('#wheelchairdiv').hide();
          document.getElementById("carstypes").value='';
          document.getElementById("carstypes").value='0';      
      }
   });

  function calwaitingtime() {
    var time1= document.getElementById("timepicker1").value;
    var time2= document.getElementById("waitingtimepicker").value
    var a=time1.split(":");
    var b=time2.split(":");
    var h=parseInt(a[0])+parseInt(b[0]);
    var m=parseInt(a[1])+parseInt(b[1]);

    if(m >= 60){
      m = m - 60;
      if(m < 10){
        m = "0" + m; 
      }
      h= h + 1;
    }else{
       if(m < 10){
        m = "0" + m; 
      }
    }

    if(h >= 24){    
      h = h - 24;
      if(h < 10){
        h = "0" + h;
       }
    }else{
       if(h < 10){
        h = "0" + h;
       }
    }
      document.getElementById("timepicker2").value = h.toString()+" : "+m.toString();
 }  

        $(".weekdays_go").click(function(){
            var day = $(this).val();
            var weekdays = $(this);
  
            if ($(weekdays).is(":checked")) {
                $("#go_time_"+day).show();
            }
            else {
                $("#go_time_"+day).hide();
            }
        });
        
        $(".weekdays_back").click(function(){
            var day = $(this).val();
            var weekdays = $(this);

            if ($(weekdays).is(":checked")) {
                $("#back_time_"+day).show();
            }
            else {
                $("#back_time_"+day).hide();
            }
        });


        $("#pickaddresscategorybtn").click(function(){
          document.getElementById("pickupcategory").value='';
          document.getElementById("pickupcategory").value='address';
        });
        $("#pickpackagecategorybtn").click(function(){
          document.getElementById("pickupcategory").value='';
          document.getElementById("pickupcategory").value='package';
        });
        $("#pickairportcategorybtn").click(function(){
          document.getElementById("pickupcategory").value='';
          document.getElementById("pickupcategory").value='airport';
        });
        $("#picktraincategorybtn").click(function(){
          document.getElementById("pickupcategory").value='';
          document.getElementById("pickupcategory").value='train';
        });
        $("#pickhotelcategorybtn").click(function(){
          document.getElementById("pickupcategory").value='';
          document.getElementById("pickupcategory").value='hotel';
        });
        $("#pickparkcategorybtn").click(function(){
          document.getElementById("pickupcategory").value='';
          document.getElementById("pickupcategory").value='park';
        });

          $("#dropaddresscategorybtn").click(function(){
          document.getElementById("dropoffcategory").value='';
          document.getElementById("dropoffcategory").value='address';
        });
        $("#droppackagecategorybtn").click(function(){
          document.getElementById("dropoffcategory").value='';
          document.getElementById("dropoffcategory").value='package';
        });
        $("#dropairportcategorybtn").click(function(){
          document.getElementById("dropoffcategory").value='';
          document.getElementById("dropoffcategory").value='airport';
        });
        $("#droptraincategorybtn").click(function(){
          document.getElementById("dropoffcategory").value='';
          document.getElementById("dropoffcategory").value='train';
        });
        $("#drophotelcategorybtn").click(function(){
          document.getElementById("dropoffcategory").value='';
          document.getElementById("dropoffcategory").value='hotel';
        });
        $("#dropparkcategorybtn").click(function(){
          document.getElementById("dropoffcategory").value='';
          document.getElementById("dropoffcategory").value='park';
        });
    // functions of bookings add
        function getpassengers(){
           var val=document.getElementById('userisclient').value;
           if (val=='')
            {
              val=0;
             }
         $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/bookings/getpassengersbyclient'; ?>',
                data: {'client_id': val},
                success: function (result) {
                
                  $("#passengerselectdiv").empty();
                  document.getElementById("passengerselectdiv").innerHTML =result;
                }
              });   
    }

    function addpassengers()
    {
      var count=0;
      var val = $('#passengerselectdiv').val();
      if (val!='')
      {
           $.ajax({
                type: "GET",
                url: '<?php echo base_url().'savepassenger'; ?>',
                data: {'passenger_id': val},
                success: function (data) {
                   $('#passengerdiv').empty();
                      $.each(data.passengers, function(k, v) {
                       $("#passengerdiv").append('<li class="text-left clearfix"><div style="float:left;"><span class="fa fa-user" style="margin-right:5px;"></span><span>'+v['name']+'</span>, <span class="fa fa-mobile" style="margin-right:5px;"></span><span>'+v['phone']+'</span>, <span class="fa fa-phone" style="margin-right:5px;"></span><span>'+v['home']+'</span>, <span class="fa fa-wheelchair" style="margin-right:5px;"></span><span>'+v['disable']+'</span></div><div style="float:left;"><button type="button" class="minusredicon" onclick="removepassengers('+v['id']+')"><i class="fa fa-minus"></i></button></div></li>');
                       });
                }
            });
        }
    }   



  function removepassengers(val)
  {
        var count=0;
      if (val!='')
      {
             $.ajax({
                type: "GET",
                url: '<?php echo base_url().'removepassenger'; ?>',
                data: {'passenger_id': val},
                success: function (data) {
                   $('#passengerdiv').empty();
                   if(data.result =='200'){
                      $.each(data.passengers, function(k, v) {
                       $("#passengerdiv").append('<li class="text-left clearfix"><div style="float:left;"><span class="fa fa-user" style="margin-right:5px;"></span><span>'+v['name']+'</span>, <span class="fa fa-mobile" style="margin-right:5px;"></span><span>'+v['phone']+'</span>, <span class="fa fa-phone" style="margin-right:5px;"></span><span>'+v['home']+'</span>, <span class="fa fa-wheelchair" style="margin-right:5px;"></span><span>'+v['disable']+'</span></div><div style="float:left;"><button type="button" class="minusredicon" onclick="removepassengers('+v['id']+')"><i class="fa fa-minus"></i></button></div></li>');
                       });
                   }                   
                }
            });
        }
    }


  function addservice(){
    var val=$("#pickservicecategoryinput").val(); 
    var selectedtext=$("#pickservicecategoryinput").find(":selected").text();
    selectedtext=selectedtext.trim();
    if(selectedtext.toLowerCase()=="reccurent"){
      if($(".regular_check").is(":checked")) {
          
      }else{
        $(".regular_check").trigger("click");
      }         
    }
    else{
       if($(".regular_check").is(":checked")) {
           $(".regular_check").trigger("click");
      }
    }
    if (val=='')
    {
      val=0;
    }
         $.ajax({
                type: "GET",
                url: '<?php echo base_url().'getbookingservices'; ?>',
                data: {'category_id': val},
                success: function (result) {
                
                  $("#pickserviceinput").empty();
                  document.getElementById("pickserviceinput").innerHTML =result;
                }
              });   
    }
      //for index search
     function addsearchservice(){
    var val=$("#searchservicecategoryfield").val(); 
    
    if (val=='')
    {
      val=0;
    }
         $.ajax({
                type: "GET",
                url: '<?php echo base_url().'getbookingservices'; ?>',
                data: {'category_id': val},
                success: function (result) {
                
                  $("#searchservicefield").empty();
                  document.getElementById("searchservicefield").innerHTML =result;
                }
              });   
    }
    //for index search

    function addpoidata(){
      var val=$("#pickpackagefield").val();    
      $("#droppackagecategorybtn").trigger("click");
      $('#droppackagefield').val(val); 
      if (val=='')
      {          
       val=0;
      }     
         $.ajax({
                type: "GET",
                url: '<?php echo base_url().'getpoidatabypackage'; ?>',
                data: {'package_id': val},
                success: function (data) {
                if(data.result=='200'){

                  $('#packagedepartaddress').val(data.poiaddress);
                  $('#packagedepartcategoryname').val(data.poicategoryname);
                  $('#packagedepartname').val(data.poiname);
                  $('#packagedepartzipcode').val(data.zipcode);
                  $('#packagedepartcity').val(data.city);
               
                  $('#packagedestinationaddress').val(data.destinationpoiaddress);
                  $('#packagedestinationcategoryname').val(data.destinationpoicategoryname);
                  $('#packagedestinationname').val(data.destinationpoiname);
                  $('#packagedestinationzipcode').val(data.destinationzipcode);
                  $('#packagedestinationcity').val(data.destinationcity);
                }
                else{

                  $('#packagedepartaddress').val('');
                  $('#packagedepartcategoryname').val('');
                  $('#packagedepartname').val('');
                  $('#packagedepartzipcode').val('');
                  $('#packagedepartcity').val('');


                  $('#packagedestinationaddress').val('');
                  $('#packagedestinationcategoryname').val('');
                  $('#packagedestinationname').val('');
                  $('#packagedestinationzipcode').val('');
                  $('#packagedestinationcity').val('');

                }

              }
            });   
    }

   function adddroppoidata(){
     var val=$("#droppackagefield").val();
      if (val=='')
      {           
        val=0;
       } 
    
         $.ajax({
                type: "GET",
                url: '<?php echo base_url().'getdroppoidatabypackage'; ?>',
                data: {'package_id': val},
                success: function (data) {
                if(data.result=='200'){

                  $('#packagedestinationaddress').val(data.poiaddress);
                  $('#packagedestinationcategoryname').val(data.poicategoryname);
                  $('#packagedestinationname').val(data.poiname);
                  $('#packagedestinationzipcode').val(data.zipcode);
                  $('#packagedestinationcity').val(data.city);
  

                }
                else{
                               
                  $('#packagedestinationaddress').val('');
                  $('#packagedestinationcategoryname').val('');
                  $('#packagedestinationname').val('');
                  $('#packagedestinationzipcode').val('');
                  $('#packagedestinationcity').val('');


                  }

                }
            });   
   }

   function getpickcategorydata(e){
       $pickcategory= document.getElementById("pickupcategory").value;
       var val=$(e).val(); 
     if (val=='')
      {           
        val=0;
      } 
    
         $.ajax({
                type: "GET",
                url: '<?php echo base_url().'getpoidatabycategory'; ?>',
                data: {'poi_id': val},
                success: function (data) {
                if(data.result=='200'){
                  if($pickcategory=="airport"){
                     $('#pickairportaddress').val(data.address);
                     $('#pickairportzipcode').val(data.zipcode);
                     $('#pickairportcity').val(data.city);
                  }
                  else if($pickcategory=="train"){
                     $('#picktrainaddress').val(data.address);
                     $('#picktrainzipcode').val(data.zipcode);
                     $('#picktraincity').val(data.city);
                  }
                  else if($pickcategory=="hotel"){
                     $('#pickhoteladdress').val(data.address);
                     $('#pickhotelzipcode').val(data.zipcode);
                     $('#pickhotelcity').val(data.city);
                  }
                  else if($pickcategory=="park"){
                     $('#pickparkaddress').val(data.address);
                     $('#pickparkzipcode').val(data.zipcode);
                     $('#pickparkcity').val(data.city);
                  }
                }
                else{               
                   if($pickcategory=="airport"){
                     $('#pickairportaddress').val('');
                     $('#pickairportzipcode').val('');
                     $('#pickairportcity').val('');
                    }
                  else if($pickcategory=="train"){
                     $('#picktrainaddress').val('');
                     $('#picktrainzipcode').val('');
                     $('#picktraincity').val('');
                  }
                  else if($pickcategory=="hotel"){
                     $('#pickhoteladdress').val('');
                     $('#pickhotelzipcode').val('');
                     $('#pickhotelcity').val('');
                  }
                  else if($pickcategory=="park"){
                     $('#pickparkaddress').val('');
                     $('#pickparkzipcode').val('');
                     $('#pickparkcity').val('');
                  }
                
                }

               }
            });   
    }

  function getdropcategorydata(e){
       $dropcategory= document.getElementById("dropoffcategory").value;
       var val=$(e).val(); 
     if (val=='')
      {           
        val=0;
      } 
    
         $.ajax({
                type: "GET",
                url: '<?php echo base_url().'getpoidatabycategory'; ?>',
                data: {'poi_id': val},
                success: function (data) {
                if(data.result=='200'){
                  if($dropcategory=="airport"){
                     $('#dropairportaddress').val(data.address);
                     $('#dropairportzipcode').val(data.zipcode);
                     $('#dropairportcity').val(data.city);
                  }
                  else if($dropcategory=="train"){
                     $('#droptrainaddress').val(data.address);
                     $('#droptrainzipcode').val(data.zipcode);
                     $('#droptraincity').val(data.city);
                  }
                  else if($dropcategory=="hotel"){
                     $('#drophoteladdress').val(data.address);
                     $('#drophotelzipcode').val(data.zipcode);
                     $('#drophotelcity').val(data.city);
                  }
                  else if($dropcategory=="park"){
                     $('#dropparkaddress').val(data.address);
                     $('#dropparkzipcode').val(data.zipcode);
                     $('#dropparkcity').val(data.city);
                  }
                }
                else{
                    if($dropcategory=="airport"){
                      $('#dropairportaddress').val('');
                      $('#dropairportzipcode').val('');
                      $('#dropairportcity').val('');
                    }
                    else if($dropcategory=="train"){
                     $('#droptrainaddress').val('');
                     $('#droptrainzipcode').val('');
                     $('#droptraincity').val('');
                    }
                    else if($dropcategory=="hotel"){
                     $('#drophoteladdress').val('');
                     $('#drophotelzipcode').val('');
                     $('#drophotelcity').val('');
                    }
                  else if($dropcategory=="park"){
                     $('#dropparkaddress').val('');
                     $('#dropparkzipcode').val('');
                     $('#dropparkcity').val('');
                  }
                
                  }

                }
              });  
    }
    // end functions of bookings add

    //get current position section
      var currentpickaddress='';
      var currentpickzipcode='';
      var currentpickcity='';

      var currentdropaddress='';
      var currentdropzipcode='';
      var currentdropcity='';

    //setpickcurrentlocation method
     function setPickCurrentLocation(){
          if(currentpickcity!=''){               
                   document.getElementById("pickup_city").value='';
                   document.getElementById("pickup_city").value = currentpickcity;
              }
          if(currentpickzipcode!=''){
                   document.getElementById("pickup_code").value='';
                   document.getElementById("pickup_code").value = currentpickzipcode;
              }
            
            //set address
            var address=$('#currentuserpoiinput').val();
            $('#pickupaddress').val(address);
            $('#pickupautocomplete').val('');

            var city=currentpickcity+',';
            city = new RegExp(city,"g");
            var newaddress = address.replace(city, "");
            
            var country="france";
            country=new RegExp(country,"i");
            newaddress=newaddress.replace(country,"");

            var zipcode=currentpickzipcode;
            zipcode = new RegExp(zipcode,"g");
            newaddress = newaddress.replace(zipcode, "");

            newaddress = newaddress.replace(/, ,/g, ",");

            if(newaddress.search(" ")==0){
                newaddress = newaddress.replace(" " , "");
            }
              newaddress = newaddress.replace(/^,\s*/, "");
              newaddress = newaddress.replace(/,\s*$/, "");
              newaddress=newaddress.replace(/  +/g, ' ');
            $('#pickupautocomplete').val(newaddress);
            //set address

            $("#currentuserpoidiv").hide();
         }
     //getcurrentlocation method
      async function getCurrentLocation(){
       if(currentpickaddress==''){
           document.getElementById("locationloaderdiv").style.display = "block";
           let m1= await getPickCurrentLocation();      
           document.getElementById("currentuserpoiinput").value='';
           document.getElementById("currentuserpoiinput").value =m1.address;
           document.getElementById("locationloaderdiv").style.display = "none";
       }     
          $("#currentuserpoidiv").toggle();
     }
     
    function getPickCurrentLocation(){
         return  new Promise((resolve,reject)=>{
         setTimeout(()=>{
         if(navigator.geolocation){              
                navigator.geolocation.getCurrentPosition(function(position){                
                 var lat = position.coords.latitude;
                 var lang = position.coords.longitude;           
                 var latlng = new  google.maps.LatLng(lat, lang);           
                 var geocoder  = new google.maps.Geocoder();
                 geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {                         
                      //get postal code
                      var i;
                      var typeIdx;
                       for (i = 0; i < results[0].address_components.length; i++) {
                          var types = results[0].address_components[i].types;

                          for (typeIdx = 0; typeIdx < types.length; typeIdx++) {
                              if (types[typeIdx] == 'postal_code') {
                                   currentpickzipcode=results[0].address_components[i].short_name;
                              }
                               //get city
                                 if (types[typeIdx] == 'locality') {
                                   currentpickcity=results[0].address_components[i].short_name;
                              }
                      
                          }
                      }

                      currentpickaddress=results[0].formatted_address;
                      var addressinfo = {address:currentpickaddress, city:currentpickcity, zipcode:currentpickzipcode};
                       resolve(addressinfo);
                             
                    }
                 }
                });
               });
            } else{
               alert("Sorry, browser does not support geolocation!");
            }           
      },5000);
      });
                      
    }


    // drop address field
    function setDropCurrentLocation(){
               if(currentdropcity!=''){                         
                       document.getElementById("dropoff_city").value='';
                       document.getElementById("dropoff_city").value = currentdropcity;
                  }
                  if(currentdropzipcode!=''){
                       document.getElementById("dropoff_code").value='';
                       document.getElementById("dropoff_code").value = currentdropzipcode;
                  }

                
                //set address
                var address=$('#dropcurrentuserpoiinput').val();
                $('#dropoffaddress').val(address);
                $('#dropoffautocomplete').val('');

                var city=currentdropcity+',';
                city = new RegExp(city,"g");
                var newaddress = address.replace(city, "");
                
                var country="france";
                country=new RegExp(country,"i");
                newaddress=newaddress.replace(country,"");

                var zipcode=currentdropzipcode;
                zipcode = new RegExp(zipcode,"g");
                newaddress = newaddress.replace(zipcode, "");

                newaddress = newaddress.replace(/, ,/g, ",");

                if(newaddress.search(" ")==0){
                    newaddress = newaddress.replace(" " , "");
                }
                  newaddress = newaddress.replace(/^,\s*/, "");
                  newaddress = newaddress.replace(/,\s*$/, "");
                  newaddress=newaddress.replace(/  +/g, ' ');
                $('#dropoffautocomplete').val(newaddress);
                //set address
                $("#dropcurrentuserpoidiv").hide();
     }
                    
    async function getDropCurrentLocation(){
      
             if(currentdropaddress==''){                     
                 document.getElementById("droplocationloaderdiv").style.display = "block";
                 let m1= await getDropFullCurrentLocation();
             
                 document.getElementById("dropcurrentuserpoiinput").value='';
                 document.getElementById("dropcurrentuserpoiinput").value =m1.address;
                  document.getElementById("droplocationloaderdiv").style.display = "none";
             }                   
                $("#dropcurrentuserpoidiv").toggle();   
    }

  function getDropFullCurrentLocation(){
     return  new Promise((resolve,reject)=>{
     setTimeout(()=>{
     if(navigator.geolocation){              
          navigator.geolocation.getCurrentPosition(function(position){                
           var lat = position.coords.latitude;
           var lang = position.coords.longitude;           
           var latlng =   new  google.maps.LatLng(lat, lang);           
           var geocoder  = new google.maps.Geocoder();
           geocoder.geocode({ 'latLng': latlng }, function (results, status) {
           if (status == google.maps.GeocoderStatus.OK) {
             if (results[0]) {                         
                      //get postal code
                      var i;
                      var typeIdx;
                         for (i = 0; i < results[0].address_components.length; i++) {
                            var types = results[0].address_components[i].types;

                            for (typeIdx = 0; typeIdx < types.length; typeIdx++) {
                                if (types[typeIdx] == 'postal_code') {
                                     currentdropzipcode=results[0].address_components[i].short_name;
                                }
                                 //get city
                                   if (types[typeIdx] == 'locality') {
                                     currentdropcity=results[0].address_components[i].short_name;
                                }                        
                            }
                        }

                currentdropaddress=results[0].formatted_address;
                var addressinfo = {address:currentdropaddress, city:currentdropcity, zipcode:currentdropzipcode};
                resolve(addressinfo);                             
                  }
                }
              });
            });
            } else{
               alert("Sorry, browser does not support geolocation!");
            }           
      },5000);
   });        
  }
    //end section of current position
var n=0;
var l=0;


var stopper=new Array();
var editstopper=new Array();
    // add autocomplete google map section
     var placeSearch, autocomplete, autodropcomplete, editautocomplete, editautodropcomplete;

var componentForm = {
  locality: 'long_name',
  postal_code: 'short_name'
};

function initAutocomplete() {

   var input = document.getElementById('pickupautocomplete');
  autocomplete = new google.maps.places.Autocomplete(
      document.getElementById('pickupautocomplete'), {types: ['geocode'],componentRestrictions: {country: "fr"}});
  autodropcomplete = new google.maps.places.Autocomplete(
      document.getElementById('dropoffautocomplete'), {types: ['geocode'],componentRestrictions: {country: "fr"}});
  editautocomplete = new google.maps.places.Autocomplete(
      document.getElementById('editpickupautocomplete'), {types: ['geocode'],componentRestrictions: {country: "fr"}});
  editautodropcomplete = new google.maps.places.Autocomplete(
      document.getElementById('editdropoffautocomplete'), {types: ['geocode'],componentRestrictions: {country: "fr"}});

  autocomplete.setFields(['address_component','geometry']);
  autocomplete.addListener('place_changed', fillInAddress);

  autodropcomplete.setFields(['address_component','geometry']);
  autodropcomplete.addListener('place_changed', fillInDropAddress);

   editautocomplete.setFields(['address_component','geometry']);
   editautocomplete.addListener('place_changed', editfillInAddress);

   editautodropcomplete.setFields(['address_component','geometry']);
   editautodropcomplete.addListener('place_changed', editfillInDropAddress);

   //stoper address
 let i;
 if(n > 0){
   for(i=1;i <= n;i++){
   
     if(stopper.includes(i)){
  stopaddr = new google.maps.places.Autocomplete(
      document.getElementById('stopaddr_'+i), {types: ['geocode'],componentRestrictions: {country: "fr"}});
  stopaddr.setFields(['address_component','geometry']);
  stopaddr.addListener('place_changed', fillInStopAddress);
    }
   }
 }
 
 //stoper address

 //edit stoper address
   let j;
 if(l==0){
  if($('#stopcountfield').length){
    l=document.getElementById('stopcountfield').value;
    for(var s=1;s <= l;s++){
       editstopper.push(s);
    }
 }
}
 if(l!=0){
   for(j=1;j <= l;j++){
   
     if(editstopper.includes(j)){
  editstopaddr = new google.maps.places.Autocomplete(
      document.getElementById('editstopaddr_'+j), {types: ['geocode'],componentRestrictions: {country: "fr"}});
  editstopaddr.setFields(['address_component','geometry']);
  editstopaddr.addListener('place_changed', fillInEditStopAddress);
    }
   }

 }
 //edit stoper address


}
function fillInEditStopAddress(){
   // Get the place details from the autocomplete object.
  var place = editstopaddr.getPlace();
  if(place.address_components){
   
     for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
  }

  }
}
function fillInStopAddress(){
   // Get the place details from the autocomplete object.
  var place = stopaddr.getPlace();
  if(place.address_components){
   
     for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
  }

  }
}

function fillInAddress() {
  // Get the place details from the autocomplete object.
       var place = autocomplete.getPlace();

      document.getElementById("pickup_city").value = '';
      document.getElementById("pickup_code").value = '';
     
      document.getElementById('pickloc_lat').value='';
      document.getElementById('pickloc_long').value='';

  if(place.address_components){
   
     for (var i = 0; i < place.address_components.length; i++) {
         var addressType = place.address_components[i].types[0];

         if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
             if(addressType=="locality"){
               document.getElementById("pickup_city").value = val;          
              }
            else if(addressType=="postal_code"){
              document.getElementById("pickup_code").value = val;           
             }     
          }
       }

          document.getElementById('pickloc_lat').value = place.geometry.location.lat();
          document.getElementById('pickloc_long').value = place.geometry.location.lng();          
  }

        var address=$('#pickupautocomplete').val();
        $('#pickupaddress').val(address);
        $('#pickupautocomplete').val('');

        var city=($('#pickup_city').val())+',';
        city = new RegExp(city,"g");
        var newaddress = address.replace(city, "");

        var country="france";
        country=new RegExp(country,"i");
        newaddress=newaddress.replace(country,"");

        var zipcode=($('#pickup_code').val());
        zipcode = new RegExp(zipcode,"g");
        newaddress = newaddress.replace(zipcode, "");
        newaddress = newaddress.replace(/, ,/g, ",");

        if(newaddress.search(" ")==0){
            newaddress = newaddress.replace(" " , "");
         }
        newaddress = newaddress.replace(/^,\s*/, "");
        newaddress = newaddress.replace(/,\s*$/, "");
        newaddress=newaddress.replace(/  +/g, ' ');

        $('#pickupautocomplete').val(newaddress);
}

function fillInDropAddress() {
   // Get the place details from the autocomplete object.
      var place = autodropcomplete.getPlace();
  
      document.getElementById("dropoff_city").value = '';
      document.getElementById("dropoff_code").value = '';
      
      document.getElementById('droploc_lat').value='';
      document.getElementById('droploc_long').value='';

  
  if(place.address_components){
   
     for (var i = 0; i < place.address_components.length; i++) {
         var addressType = place.address_components[i].types[0];

          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            if(addressType=="locality"){
             document.getElementById("dropoff_city").value = val;          
             }
            else if(addressType=="postal_code"){
             document.getElementById("dropoff_code").value = val;          
             }     
          }
      }
 
          document.getElementById('droploc_lat').value = place.geometry.location.lat();
          document.getElementById('droploc_long').value = place.geometry.location.lng();
  }
          var address=$('#dropoffautocomplete').val();
          $('#dropoffaddress').val(address);
          $('#dropoffautocomplete').val('');

          var city=($('#dropoff_city').val())+',';
          city = new RegExp(city,"g");
          var newaddress = address.replace(city, "");

          var country="france";
          country=new RegExp(country,"i");
          newaddress=newaddress.replace(country,"");

         var zipcode=$('#dropoff_code').val();
         zipcode = new RegExp(zipcode,"g");
         newaddress = newaddress.replace(zipcode, "");
         newaddress = newaddress.replace(/, ,/g, ",");
         if(newaddress.search(" ")==0){
            newaddress = newaddress.replace(" " , "");
          }
         newaddress = newaddress.replace(/^,\s*/, "");
         newaddress = newaddress.replace(/,\s*$/, "");
         newaddress=newaddress.replace(/  +/g, ' ');
         $('#dropoffautocomplete').val(newaddress);
   }

function geolocate() {

  if (navigator.geolocation) {
     navigator.geolocation.getCurrentPosition(function(position) {
     var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
     
      var circle = new google.maps.Circle(
          {center: geolocation, radius: position.coords.accuracy});
      autocomplete.setBounds(circle.getBounds());
    });
  }
} 
    // end autocomplete google map section



    //Edit Booking Section 
     function editfillInAddress(){
     
      var place = editautocomplete.getPlace();

      document.getElementById("editpickup_city").value = '';
      document.getElementById("editpickup_code").value = '';
     
      document.getElementById('editpickloc_lat').value='';
      document.getElementById('editpickloc_long').value='';

      if(place.address_components){   
       for (var i = 0; i < place.address_components.length; i++) {
         var addressType = place.address_components[i].types[0];
         if (componentForm[addressType]) {
         var val = place.address_components[i][componentForm[addressType]];
         if(addressType=="locality"){
            document.getElementById("editpickup_city").value = val; 
         }
         else if(addressType=="postal_code"){
             document.getElementById("editpickup_code").value = val;
         }       
      }
    }

        document.getElementById('editpickloc_lat').value = place.geometry.location.lat();
        document.getElementById('editpickloc_long').value = place.geometry.location.lng();
    }

    var address=$('#editpickupautocomplete').val();
    $('#editpickupaddressfield').val(address);
    $('#editpickupautocomplete').val('');

    var city=($('#editpickup_city').val())+',';
    city = new RegExp(city,"g");
    var newaddress = address.replace(city, "");

    var country="france";
    country=new RegExp(country,"i");
    newaddress=newaddress.replace(country,"");

    var zipcode=($('#editpickup_code').val());
    zipcode = new RegExp(zipcode,"g");
    newaddress = newaddress.replace(zipcode, "");
    newaddress = newaddress.replace(/, ,/g, ",");

    if(newaddress.search(" ")==0){
        newaddress = newaddress.replace(" " , "");
    }
    newaddress = newaddress.replace(/^,\s*/, "");
    newaddress = newaddress.replace(/,\s*$/, "");
    newaddress=newaddress.replace(/  +/g, ' ');

    $('#editpickupautocomplete').val(newaddress);
    }

function editfillInDropAddress() {
 
    var place = editautodropcomplete.getPlace();  
    document.getElementById("editdropoff_city").value = '';
    document.getElementById("editdropoff_code").value = '';
   
    document.getElementById('editdroploc_lat').value='';
    document.getElementById('editdroploc_long').value='';

    if(place.address_components){  
     for (var i = 0; i < place.address_components.length; i++) {
     var addressType = place.address_components[i].types[0];

    if (componentForm[addressType]){
      var val = place.address_components[i][componentForm[addressType]];
      if(addressType=="locality"){
        document.getElementById("editdropoff_city").value = val;        
       }
       else if(addressType=="postal_code"){
           document.getElementById("editdropoff_code").value = val;     
       }     
     }
    }
        document.getElementById('editdroploc_lat').value = place.geometry.location.lat();
        document.getElementById('editdroploc_long').value = place.geometry.location.lng();
    }

  var address=$('#editdropoffautocomplete').val();
  $('#editdropoffaddressfield').val(address);
  $('#editdropoffautocomplete').val('');

  var city=($('#editdropoff_city').val())+',';
  city = new RegExp(city,"g");
  var newaddress = address.replace(city, "");

  var country="france";
  country=new RegExp(country,"i");
  newaddress=newaddress.replace(country,"");

  var zipcode=$('#editdropoff_code').val();
  zipcode = new RegExp(zipcode,"g");
  newaddress = newaddress.replace(zipcode, "");
  newaddress = newaddress.replace(/, ,/g, ",");
  if(newaddress.search(" ")==0){
      newaddress = newaddress.replace(" " , "");
  }
  newaddress = newaddress.replace(/^,\s*/, "");
  newaddress = newaddress.replace(/,\s*$/, "");
  newaddress=newaddress.replace(/  +/g, ' ');
  $('#editdropoffautocomplete').val(newaddress);
 }
//regular, return, wheelchair check section
 
function edit_regular_check(){
      if($('.edit_regular_check').is(":checked")) {
          $('#edittimeslotsrtn').show();
          $('.go-table').show();
          $(".pickupreturn_datetime_div").hide();    
          $(".startend_datetime_div").show();   
          if($('.edit_return_car').is(":checked")){
              $(".back-table").show();
          }      
      }else{         
          $('.go-table').hide();
          $('#edittimeslotsrtn').hide();
          $('.back-table').hide();
          $(".pickupreturn_datetime_div").show();  
          $(".startend_datetime_div").hide();        
      }
  }


  
    function edit_return_car(){
  
      if($('.edit_return_car').is(":checked")){        
          $(".return_datetime_div").show();        
          if($('.edit_regular_check').is(":checked")){
               $('#edittimeslotsrtn').show();
               $('.back-table').show();
          }       
      }else{          
           $(".return_datetime_div").hide();
           if($('.edit_regular_check').is(":checked")){
              $('.back-table').hide();
            }
      }    
 }

 
    function edit_wheelchair_carssh(){

      if($('.edit_wheelchair_carssh').is(":checked")) {

          $('#editwheelchairdiv').show();
          $('#editnonwheelchairdiv').hide();
          document.getElementById("editcarstypes").value='';
          document.getElementById("editcarstypes").value='1';
      }else{
          $('#editnonwheelchairdiv').show();
          $('#editwheelchairdiv').hide();
          document.getElementById("editcarstypes").value='';
          document.getElementById("editcarstypes").value='0';      
      }
   }

    function edit_weekdays_go(e){
     
            var day = $(e).val();
            var weekdays = $(e);
           
            if ($(weekdays).is(":checked")) {
                $("#editgo_time_"+day).show();
            }
            else {
                $("#editgo_time_"+day).hide();
            }
        }
        
       
        function edit_weekdays_back(e){
            var day = $(e).val();
            var weekdays = $(e);

            if ($(weekdays).is(":checked")) {
                $("#editback_time_"+day).show();
            }
            else {
                $("#editback_time_"+day).hide();
            }
        }
    function editcalwaitingtime() {
    var time1= document.getElementById("edittimepicker1").value;
    var time2= document.getElementById("editwaitingtimepicker").value
    var a=time1.split(":");
    var b=time2.split(":");
    var h=parseInt(a[0])+parseInt(b[0]);
    var m=parseInt(a[1])+parseInt(b[1]);

    if(m >= 60){
      m = m - 60;
      if(m < 10){
        m = "0" + m; 
      }
      h= h + 1;
    }else{
       if(m < 10){
        m = "0" + m; 
      }
    }

    if(h >= 24){    
      h = h - 24;
      if(h < 10){
        h = "0" + h;
       }
    }else{
       if(h < 10){
        h = "0" + h;
       }
    }
      document.getElementById("edittimepicker2").value = h.toString()+" : "+m.toString();
 }  
 function editaddwaitingtime() {
      var time1= document.getElementById("edittimepicker1").value;
      var time2= document.getElementById("editwaitingtimepicker").value
      var a=time1.split(":");
      var b=time2.split(":");
      var h=parseInt(a[0])+parseInt(b[0]);
      var m=parseInt(a[1])+parseInt(b[1]);
      if(m>=60){
        m=m-60;
        if(m<10){
          m="0"+m; 
      }
      h=h+1;
  }else{
     if(m<10){
      m="0"+m; 
  }
}

if(h >= 24){

    h=h-24;
    if(h<10){
      h="0"+h;
  }
}else{
 if(h<10){
  h="0"+h;
}
}
document.getElementById("edittimepicker2").value = h.toString()+" : "+m.toString();
}
//end regular, return, wheelchair check section

//get package, airport, train, hotel, park
function editgetpassengers(){
           var val=document.getElementById('edituserisclient').value;
           if (val=='')
            {
              val=0;
             }
         $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/bookings/getpassengersbyclient'; ?>',
                data: {'client_id': val},
                success: function (result) {
                
                  $("#editpassengerselectdiv").empty();
                  document.getElementById("editpassengerselectdiv").innerHTML =result;
                }
              });   
    }

  function editaddpassengers()
  {
      var count=0;
      var val = $('#editpassengerselectdiv').val();
      if (val!='')
      {
           $.ajax({
                type: "GET",
                url: '<?php echo base_url().'savepassenger'; ?>',
                data: {'passenger_id': val},
                success: function (data) {
                   $('#editpassengerdiv').empty();
                      $.each(data.passengers, function(k, v) {
                       $("#editpassengerdiv").append('<li class="text-left clearfix"><div style="float:left;"><span class="fa fa-user" style="margin-right:5px;"></span><span>'+v['name']+'</span>, <span class="fa fa-mobile" style="margin-right:5px;"></span><span>'+v['phone']+'</span>, <span class="fa fa-phone" style="margin-right:5px;"></span><span>'+v['home']+'</span>, <span class="fa fa-wheelchair" style="margin-right:5px;"></span><span>'+v['disable']+'</span></div><div style="float:left;"><button type="button" class="minusredicon" onclick="editremovepassengers('+v['id']+')"><i class="fa fa-minus"></i></button></div></li>');
                       });
                }
            });
        }
    }   



  function editremovepassengers(val)
  {
        var count=0;
      if (val!='')
      {
             $.ajax({
                type: "GET",
                url: '<?php echo base_url().'removepassenger'; ?>',
                data: {'passenger_id': val},
                success: function (data) {
                   $('#editpassengerdiv').empty();
                   if(data.result =='200'){
                      $.each(data.passengers, function(k, v) {
                       $("#editpassengerdiv").append('<li class="text-left clearfix"><div style="float:left;"><span class="fa fa-user" style="margin-right:5px;"></span><span>'+v['name']+'</span>, <span class="fa fa-mobile" style="margin-right:5px;"></span><span>'+v['phone']+'</span>, <span class="fa fa-phone" style="margin-right:5px;"></span><span>'+v['home']+'</span>, <span class="fa fa-wheelchair" style="margin-right:5px;"></span><span>'+v['disable']+'</span></div><div style="float:left;"><button type="button" class="minusredicon" onclick="editremovepassengers('+v['id']+')"><i class="fa fa-minus"></i></button></div></li>');
                       });
                   }                   
                }
            });
        }
    }

  function cancelupdatebooking(){
    setupDivBooking()
    document.getElementsByClassName("bookingeditajaxsection")[0].innerHTML='';
    $('#editdisbreadcrumb').remove();
    $(".ListBooking").show();
  }
  function editaddservice(){
    var val=$("#editpickservicecategoryinput").val(); 
    var selectedtext=$("#editpickservicecategoryinput").find(":selected").text();
    selectedtext=selectedtext.trim();
    if(selectedtext.toLowerCase()=="reccurent"){
      if($(".edit_regular_check").is(":checked")) {
          
      }else{
        $(".edit_regular_check").trigger("click");
      }         
    }
    else{
       if($(".edit_regular_check").is(":checked")) {
           $(".edit_regular_check").trigger("click");
      }
    }
    if (val=='')
    {
      val=0;
    }
         $.ajax({
                type: "GET",
                url: '<?php echo base_url().'getbookingservices'; ?>',
                data: {'category_id': val},
                success: function (result) {
                
                  $("#editpickserviceinput").empty();
                  document.getElementById("editpickserviceinput").innerHTML =result;
                }
              });   
    }

    function editaddpoidata(){
      var val=$("#editpickpackagefield").val();    
      $("#editdroppackagecategorybtn").trigger("click");
      $('#editdroppackagefield').val(val); 
      if (val=='')
      {          
       val=0;
      }     
         $.ajax({
                type: "GET",
                url: '<?php echo base_url().'getpoidatabypackage'; ?>',
                data: {'package_id': val},
                success: function (data) {
                if(data.result=='200'){

                  $('#editpackagedepartaddress').val(data.poiaddress);
                  $('#editpackagedepartcategoryname').val(data.poicategoryname);
                  $('#editpackagedepartname').val(data.poiname);
                  $('#editpackagedepartzipcode').val(data.zipcode);
                  $('#editpackagedepartcity').val(data.city);
               
                  $('#editpackagedestinationaddress').val(data.destinationpoiaddress);
                  $('#editpackagedestinationcategoryname').val(data.destinationpoicategoryname);
                  $('#editpackagedestinationname').val(data.destinationpoiname);
                  $('#editpackagedestinationzipcode').val(data.destinationzipcode);
                  $('#editpackagedestinationcity').val(data.destinationcity);
                }
                else{

                  $('#editpackagedepartaddress').val('');
                  $('#editpackagedepartcategoryname').val('');
                  $('#editpackagedepartname').val('');
                  $('#editpackagedepartzipcode').val('');
                  $('#editpackagedepartcity').val('');


                  $('#editpackagedestinationaddress').val('');
                  $('#editpackagedestinationcategoryname').val('');
                  $('#editpackagedestinationname').val('');
                  $('#editpackagedestinationzipcode').val('');
                  $('#editpackagedestinationcity').val('');

                }

              }
            });   
    }

   function editadddroppoidata(){
     var val=$("#editdroppackagefield").val();
      if (val=='')
      {           
        val=0;
       } 
    
         $.ajax({
                type: "GET",
                url: '<?php echo base_url().'getdroppoidatabypackage'; ?>',
                data: {'package_id': val},
                success: function (data) {
                if(data.result=='200'){

                  $('#editpackagedestinationaddress').val(data.poiaddress);
                  $('#editpackagedestinationcategoryname').val(data.poicategoryname);
                  $('#editpackagedestinationname').val(data.poiname);
                  $('#editpackagedestinationzipcode').val(data.zipcode);
                  $('#editpackagedestinationcity').val(data.city);
  

                }
                else{
                               
                  $('#editpackagedestinationaddress').val('');
                  $('#editpackagedestinationcategoryname').val('');
                  $('#editpackagedestinationname').val('');
                  $('#editpackagedestinationzipcode').val('');
                  $('#editpackagedestinationcity').val('');


                  }

                }
            });   
   }

   function editgetpickcategorydata(e){
       $pickcategory= document.getElementById("editpickupcategory").value;
       var val=$(e).val(); 
     if (val=='')
      {           
        val=0;
      } 
    
         $.ajax({
                type: "GET",
                url: '<?php echo base_url().'getpoidatabycategory'; ?>',
                data: {'poi_id': val},
                success: function (data) {
                if(data.result=='200'){
                  if($pickcategory=="airport"){
                     $('#editpickairportaddress').val(data.address);
                     $('#editpickairportzipcode').val(data.zipcode);
                     $('#editpickairportcity').val(data.city);
                  }
                  else if($pickcategory=="train"){
                     $('#editpicktrainaddress').val(data.address);
                     $('#editpicktrainzipcode').val(data.zipcode);
                     $('#editpicktraincity').val(data.city);
                  }
                  else if($pickcategory=="hotel"){
                     $('#editpickhoteladdress').val(data.address);
                     $('#editpickhotelzipcode').val(data.zipcode);
                     $('#editpickhotelcity').val(data.city);
                  }
                  else if($pickcategory=="park"){
                     $('#editpickparkaddress').val(data.address);
                     $('#editpickparkzipcode').val(data.zipcode);
                     $('#editpickparkcity').val(data.city);
                  }
                }
                else{               
                   if($pickcategory=="airport"){
                     $('#editpickairportaddress').val('');
                     $('#editpickairportzipcode').val('');
                     $('#editpickairportcity').val('');
                    }
                  else if($pickcategory=="train"){
                     $('#editpicktrainaddress').val('');
                     $('#editpicktrainzipcode').val('');
                     $('#editpicktraincity').val('');
                  }
                  else if($pickcategory=="hotel"){
                     $('#editpickhoteladdress').val('');
                     $('#editpickhotelzipcode').val('');
                     $('#editpickhotelcity').val('');
                  }
                  else if($pickcategory=="park"){
                     $('#editpickparkaddress').val('');
                     $('#editpickparkzipcode').val('');
                     $('#editpickparkcity').val('');
                  }
                
                }

               }
            });   
    }

  function editgetdropcategorydata(e){
       $dropcategory= document.getElementById("editdropoffcategory").value;
       var val=$(e).val(); 
     if (val=='')
      {           
        val=0;
      } 
    
         $.ajax({
                type: "GET",
                url: '<?php echo base_url().'getpoidatabycategory'; ?>',
                data: {'poi_id': val},
                success: function (data) {
                if(data.result=='200'){
                  if($dropcategory=="airport"){
                     $('#editdropairportaddress').val(data.address);
                     $('#editdropairportzipcode').val(data.zipcode);
                     $('#editdropairportcity').val(data.city);
                  }
                  else if($dropcategory=="train"){
                     $('#editdroptrainaddress').val(data.address);
                     $('#editdroptrainzipcode').val(data.zipcode);
                     $('#editdroptraincity').val(data.city);
                  }
                  else if($dropcategory=="hotel"){
                     $('#editdrophoteladdress').val(data.address);
                     $('#editdrophotelzipcode').val(data.zipcode);
                     $('#editdrophotelcity').val(data.city);
                  }
                  else if($dropcategory=="park"){
                     $('#editdropparkaddress').val(data.address);
                     $('#editdropparkzipcode').val(data.zipcode);
                     $('#editdropparkcity').val(data.city);
                  }
                }
                else{
                    if($dropcategory=="airport"){
                      $('#editdropairportaddress').val('');
                      $('#editdropairportzipcode').val('');
                      $('#editdropairportcity').val('');
                    }
                    else if($dropcategory=="train"){
                     $('#editdroptrainaddress').val('');
                     $('#editdroptrainzipcode').val('');
                     $('#editdroptraincity').val('');
                    }
                    else if($dropcategory=="hotel"){
                     $('#editdrophoteladdress').val('');
                     $('#editdrophotelzipcode').val('');
                     $('#editdrophotelcity').val('');
                    }
                  else if($dropcategory=="park"){
                     $('#editdropparkaddress').val('');
                     $('#editdropparkzipcode').val('');
                     $('#editdropparkcity').val('');
                  }
                
                  }

                }
              });  
    }
//get package, airport, train, hotel, park
//get Current Location
     var editcurrentpickaddress='';
      var editcurrentpickzipcode='';
      var editcurrentpickcity='';

      var editcurrentdropaddress='';
      var editcurrentdropzipcode='';
      var editcurrentdropcity='';

    //setpickcurrentlocation method
     function editsetPickCurrentLocation(){
          if(editcurrentpickcity!=''){     
                   
                   document.getElementById("editpickup_city").value='';
                   document.getElementById("editpickup_city").value = editcurrentpickcity;
              }
          if(editcurrentpickzipcode!=''){
                   document.getElementById("editpickup_code").value='';
                   document.getElementById("editpickup_code").value = editcurrentpickzipcode;
              }
            
            //set address
            var address=$('#editcurrentuserpoiinput').val();
            $('#editpickupaddressfield').val(address);
            $('#editpickupautocomplete').val('');

            var city=editcurrentpickcity+',';
            city = new RegExp(city,"g");
            var newaddress = address.replace(city, "");
            
            var country="france";
            country=new RegExp(country,"i");
            newaddress=newaddress.replace(country,"");

            var zipcode=editcurrentpickzipcode;
            zipcode = new RegExp(zipcode,"g");
            newaddress = newaddress.replace(zipcode, "");

            newaddress = newaddress.replace(/, ,/g, ",");

            if(newaddress.search(" ")==0){
                newaddress = newaddress.replace(" " , "");
            }
              newaddress = newaddress.replace(/^,\s*/, "");
              newaddress = newaddress.replace(/,\s*$/, "");
              newaddress=newaddress.replace(/  +/g, ' ');
            $('#editpickupautocomplete').val(newaddress);
            //set address

            $("#editcurrentuserpoidiv").hide();
         }
     //getcurrentlocation method
      async function editgetCurrentLocation(){
       if(editcurrentpickaddress==''){
           document.getElementById("editlocationloaderdiv").style.display = "block";
           let m1= await editgetPickCurrentLocation();      
           document.getElementById("editcurrentuserpoiinput").value='';
           document.getElementById("editcurrentuserpoiinput").value =m1.address;
           document.getElementById("editlocationloaderdiv").style.display = "none";
       }     
          $("#editcurrentuserpoidiv").toggle();
     }
     
    function editgetPickCurrentLocation(){
         return  new Promise((resolve,reject)=>{
         setTimeout(()=>{
         if(navigator.geolocation){              
                navigator.geolocation.getCurrentPosition(function(position){                
                 var lat = position.coords.latitude;
                 var lang = position.coords.longitude;           
                 var latlng = new  google.maps.LatLng(lat, lang);           
                 var geocoder  = new google.maps.Geocoder();
                 geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {                         
                      //get postal code
                      var i;
                      var typeIdx;
                       for (i = 0; i < results[0].address_components.length; i++) {
                          var types = results[0].address_components[i].types;

                          for (typeIdx = 0; typeIdx < types.length; typeIdx++) {
                              if (types[typeIdx] == 'postal_code') {
                                   editcurrentpickzipcode=results[0].address_components[i].short_name;
                              }
                               //get city
                                 if (types[typeIdx] == 'locality') {
                                   editcurrentpickcity=results[0].address_components[i].short_name;
                              }
                      
                          }
                      }

                      editcurrentpickaddress=results[0].formatted_address;
                      var addressinfo = {address:editcurrentpickaddress, city:editcurrentpickcity, zipcode:editcurrentpickzipcode};
                       resolve(addressinfo);
                             
                    }
                 }
                });
               });
            } else{
               alert("Sorry, browser does not support geolocation!");
            }           
      },5000);
      });
                      
    }


    // drop address field
    function editsetDropCurrentLocation(){
               if(editcurrentdropcity!=''){                         
                       document.getElementById("editdropoff_city").value='';
                       document.getElementById("editdropoff_city").value = editcurrentdropcity;
                  }
                  if(editcurrentdropzipcode!=''){
                       document.getElementById("editdropoff_code").value='';
                       document.getElementById("editdropoff_code").value = editcurrentdropzipcode;
                  }

                
                //set address
                var address=$('#editdropcurrentuserpoiinput').val();
                $('#editdropoffaddressfield').val(address);
                $('#editdropoffautocomplete').val('');

                var city=editcurrentdropcity+',';
                city = new RegExp(city,"g");
                var newaddress = address.replace(city, "");
                
                var country="france";
                country=new RegExp(country,"i");
                newaddress=newaddress.replace(country,"");

                var zipcode=editcurrentdropzipcode;
                zipcode = new RegExp(zipcode,"g");
                newaddress = newaddress.replace(zipcode, "");

                newaddress = newaddress.replace(/, ,/g, ",");

                if(newaddress.search(" ")==0){
                    newaddress = newaddress.replace(" " , "");
                }
                  newaddress = newaddress.replace(/^,\s*/, "");
                  newaddress = newaddress.replace(/,\s*$/, "");
                  newaddress=newaddress.replace(/  +/g, ' ');
                $('#editdropoffautocomplete').val(newaddress);
                //set address
                $("#editdropcurrentuserpoidiv").hide();
     }
                    
    async function editgetDropCurrentLocation(){
      
             if(editcurrentdropaddress==''){                     
                 document.getElementById("editdroplocationloaderdiv").style.display = "block";
                 let m1= await editgetDropFullCurrentLocation();
             
                 document.getElementById("editdropcurrentuserpoiinput").value='';
                 document.getElementById("editdropcurrentuserpoiinput").value =m1.address;
                  document.getElementById("editdroplocationloaderdiv").style.display = "none";
             }                   
                $("#editdropcurrentuserpoidiv").toggle();   
    }

  function editgetDropFullCurrentLocation(){
     return  new Promise((resolve,reject)=>{
     setTimeout(()=>{
     if(navigator.geolocation){              
          navigator.geolocation.getCurrentPosition(function(position){                
           var lat = position.coords.latitude;
           var lang = position.coords.longitude;           
           var latlng =   new  google.maps.LatLng(lat, lang);           
           var geocoder  = new google.maps.Geocoder();
           geocoder.geocode({ 'latLng': latlng }, function (results, status) {
           if (status == google.maps.GeocoderStatus.OK) {
             if (results[0]) {                         
                      //get postal code
                      var i;
                      var typeIdx;
                         for (i = 0; i < results[0].address_components.length; i++) {
                            var types = results[0].address_components[i].types;

                            for (typeIdx = 0; typeIdx < types.length; typeIdx++) {
                                if (types[typeIdx] == 'postal_code') {
                                     editcurrentdropzipcode=results[0].address_components[i].short_name;
                                }
                                 //get city
                                   if (types[typeIdx] == 'locality') {
                                     editcurrentdropcity=results[0].address_components[i].short_name;
                                }                        
                            }
                        }

                editcurrentdropaddress=results[0].formatted_address;
                var addressinfo = {address:editcurrentdropaddress, city:editcurrentdropcity, zipcode:editcurrentdropzipcode};
                resolve(addressinfo);                             
                  }
                }
              });
            });
            } else{
               alert("Sorry, browser does not support geolocation!");
            }           
      },5000);
   });        
  }
//get Current Location
//check validation
//pickup validation
function checkPickupAddressCategory(pickupAddress,pickupAddressCity,pickupAddressCode){
  let valid=true;
  const pickupAddressValue     = pickupAddress.value.trim();
  const pickupAddressCityValue = pickupAddressCity.value.trim();
  const pickupAddressCodeValue = pickupAddressCode.value.trim();
  if(pickupAddressValue === '') {
      setErrorFor(pickupAddress, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(pickupAddress);
  }
  if(pickupAddressCityValue === '') {
      setErrorFor(pickupAddressCity, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(pickupAddressCity);
  }
  if(pickupAddressCodeValue === '') {
      setErrorFor(pickupAddressCode, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(pickupAddressCode);
  }
  return valid;
}
function checkPickupAirportCategory(pickupAirport,pickupAirportFlightNo){
  let valid=true;
  const pickupAirportValue     = pickupAirport.value.trim();
  const pickupAirportFlightNoValue = pickupAirportFlightNo.value.trim();
  
  if(pickupAirportValue === '') {
      setErrorFor(pickupAirport, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(pickupAirport);
  }
  if(pickupAirportFlightNoValue === '') {
      setErrorFor(pickupAirportFlightNo, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(pickupAirportFlightNo);
  }
  
  return valid;
}
function checkPickupTrainCategory(pickupTrain,pickupTrainNo){
  let valid=true;
  const pickupTrainValue   = pickupTrain.value.trim();
  const pickupTrainNoValue = pickupTrainNo.value.trim();
  
  if(pickupTrainValue === '') {
      setErrorFor(pickupTrain, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(pickupTrain);
  }
  if(pickupTrainNoValue === '') {
      setErrorFor(pickupTrainNo, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(pickupTrainNo);
  }
  
  return valid;
}
function checkPickupPackageCategory(pickupPackage){
  let valid=true;
  const pickupPackageValue = pickupPackage.value.trim();
  if(pickupPackageValue === '') {
      setErrorFor(pickupPackage, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(pickupPackage);
  }
  return valid;
}
function checkPickupHotelCategory(pickupHotel){
  let valid=true;
  const pickupHotelValue = pickupHotel.value.trim();
  if(pickupHotelValue === '') {
      setErrorFor(pickupHotel, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(pickupHotel);
  }
  return valid;
}
function checkPickupParkCategory(pickupPark){
  let valid=true;
  const pickupParkValue = pickupPark.value.trim();
  if(pickupParkValue === '') {
      setErrorFor(pickupPark, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(pickupPark);
  }
  return valid;
}
//pickup validation
//dropoff validation
function checkDropoffAddressCategory(dropoffAddress,dropoffAddressCity,dropoffAddressCode){
  let valid=true;
  const dropoffAddressValue     = dropoffAddress.value.trim();
  const dropoffAddressCityValue = dropoffAddressCity.value.trim();
  const dropoffAddressCodeValue = dropoffAddressCode.value.trim();
  if(dropoffAddressValue === '') {
      setErrorFor(dropoffAddress, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(dropoffAddress);
  }
  if(dropoffAddressCityValue === '') {
      setErrorFor(dropoffAddressCity, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(dropoffAddressCity);
  }
  if(dropoffAddressCodeValue === '') {
      setErrorFor(dropoffAddressCode, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(dropoffAddressCode);
  }
  return valid;
}
function checkDropoffAirportCategory(dropoffAirport,dropoffAirportFlightNo){
  let valid=true;
  const dropoffAirportValue     = dropoffAirport.value.trim();
  const dropoffAirportFlightNoValue = dropoffAirportFlightNo.value.trim();
  
  if(dropoffAirportValue === '') {
      setErrorFor(dropoffAirport, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(dropoffAirport);
  }
  if(dropoffAirportFlightNoValue === '') {
      setErrorFor(dropoffAirportFlightNo, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(dropoffAirportFlightNo);
  }
  
  return valid;
}
function checkDropoffTrainCategory(dropoffTrain,dropoffTrainNo){
  let valid=true;
  const dropoffTrainValue   = dropoffTrain.value.trim();
  const dropoffTrainNoValue = dropoffTrainNo.value.trim();
  
  if(dropoffTrainValue === '') {
      setErrorFor(dropoffTrain, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(dropoffTrain);
  }
  if(dropoffTrainNoValue === '') {
      setErrorFor(dropoffTrainNo, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(dropoffTrainNo);
  }
  
  return valid;
}
function checkDropoffPackageCategory(dropoffPackage){
  let valid=true;
  const dropoffPackageValue = dropoffPackage.value.trim();
  if(dropoffPackageValue === '') {
      setErrorFor(dropoffPackage, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(dropoffPackage);
  }
  return valid;
}
function checkDropoffHotelCategory(dropoffHotel){
  let valid=true;
  const dropoffHotelValue = dropoffHotel.value.trim();
  if(dropoffHotelValue === '') {
      setErrorFor(dropoffHotel, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(dropoffHotel);
  }
  return valid;
}
function checkDropoffParkCategory(dropoffPark){
  let valid=true;
  const dropoffParkValue = dropoffPark.value.trim();
  if(dropoffParkValue === '') {
      setErrorFor(dropoffPark, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(dropoffPark);
  }
  return valid;
}
//dropoff validation
function setErrorFor(input, message) {
 let formControl=$(input).parents('.edit-cardform-control');
 let small= formControl.children('small');
 $(formControl).addClass("errors");
 $(small).html(message);
}
function setSuccessFor(input){
  let formControl=$(input).parents('.edit-cardform-control');
  let small= formControl.children('small');
 if($(formControl).hasClass("errors")){
   $(formControl).removeClass("errors");
 } 
}
function editbookingaddsectiondispaly(){
  if($('#checkforaddsection').is(":checked")){                  
      $('#editbookingaddsection').show();
   }       
  else{        
        $('#editbookingaddsection').hide();  
     } 
  } 
//check valiation
//display map quote
    
      var picklat='';
      var picklong='';
      var droplat='';
      var droplong='';
      var map;
    function othereditinitMap(editpickupaddress,editdropoffaddress,editpickotheraddress,editdropotheraddress,symbolOne,symbolThree) {
       
        var directionsService = new google.maps.DirectionsService;
        var  directionsDisplay = new google.maps.DirectionsRenderer({
            polylineOptions: {
              strokeColor: "#87ceeb",
               icons: [
            {
              icon: symbolOne,
              offset: "0%"
            },
            {
              icon: symbolThree,
              offset: "100%"
            }
          ],
            }
            
          });
          directionsDisplay.setOptions( { suppressMarkers: true} );
         

         map = new google.maps.Map(document.getElementById('editmap'), {
          zoom: 8,
          center: {lat: 41.85, lng: -87.65}
        });
        directionsDisplay.setMap(map);
        
        editcalculateAndDisplayRoute(directionsService, directionsDisplay,editpickupaddress,editdropoffaddress,editpickotheraddress,editdropotheraddress,symbolOne,symbolThree);
     
      }
      function editgetpickdroplatlang(editpickupaddress,editdropoffaddress,editpickotheraddress,editdropotheraddress,symbolOne,symbolThree){
        return  new Promise((resolve,reject)=>{
 setTimeout(()=>{
         var geocoder = new google.maps.Geocoder();      
            geocoder.geocode( { 'address': editpickupaddress}, function(results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                   picklat = results[0].geometry.location.lat();
                   picklong = results[0].geometry.location.lng();
                    console.log("latitude"+picklat);
                    console.log("longitude"+picklong);
                }
              });

        var geocoderdrop = new google.maps.Geocoder();
            geocoder.geocode( { 'address': editdropoffaddress}, function(results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                    droplat = results[0].geometry.location.lat();
                    droplong = results[0].geometry.location.lng();
                    console.log("latitude"+droplat);
                    console.log("longitude"+droplong);
                    resolve("resolve");
                }
              });
          },1000);
 });
      }
   async function editcalculateAndDisplayRoute(directionsService,directionsDisplay,editpickupaddress,editdropoffaddress,editpickotheraddress,editdropotheraddress,symbolOne,symbolThree) {
   
        //get lang and longitude
           document.getElementById("editmapsearchloader").style.display = "block";

        let m1= await editgetpickdroplatlang(editpickupaddress,editdropoffaddress,editpickotheraddress,editdropotheraddress,symbolOne,symbolThree);

         //end lang and longitude
        directionsService.route({
          origin: editpickupaddress,
          destination: editdropoffaddress,
          travelMode: 'DRIVING'
        }, function(response, status) {
          if (status === 'OK') {
            directionsDisplay.setDirections(response);
            //set start marker
           
             var startmarker = new google.maps.Marker({
              map: map,
              position:  {
              lat: picklat,
              lng: picklong
            },
           draggable: false,
            icon: {url:'<?= base_url(); ?>/assets/theme/default/images/redicon.png',scaledSize: new google.maps.Size(45, 45),labelOrigin: new google.maps.Point(23, 18)},
            label: {text: 'A', color: "white",fontWeight:'900'},
            clickable:true
         });
       
        var startinfowindow=new google.maps.InfoWindow({
             content:"<span style='white-space: wrap;overflow: hidden;width: 150px;display: inline-block;'>"+editpickupaddress+"<br>"+editpickotheraddress+"</span>",
              disableAutoPan: true
        });
        startinfowindow.open(map,startmarker);
          startmarker.addListener('click',function(){
          startinfowindow.open(map,startmarker);
        });
            //end start marker
              //set end marker
             var endmarker = new google.maps.Marker({
            map: map,
            position:  {
            lat: droplat,
            lng: droplong
          },
             draggable: false,
            icon: {url:'<?= base_url(); ?>/assets/theme/default/images/greenicon.png',scaledSize: new google.maps.Size(45, 45),labelOrigin: new google.maps.Point(23.5, 18)},
            label: {text: 'B', color: "white",fontWeight:'900'},
            clickable:true
    });

        
        var endinfowindow=new google.maps.InfoWindow({
             content:"<span style='white-space: wrap;overflow: hidden;width: 150px;display: inline-block;'>"+editdropoffaddress+"<br>"+editdropotheraddress+"</span>",
              disableAutoPan: true
        });
         endinfowindow.open(map,endmarker);
        endmarker.addListener('click',function(){
          endinfowindow.open(map,endmarker);
        });
         document.getElementById("editmapsearchloader").style.display = "none";
            //finish end marker
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
      }
    //quote map display
    //payment validation
    function checkInputs(cardname,cardnumber,cardmonth,cardyear,cardcvc) {
  
  var validate=true;
  // trim to remove the whitespaces
  let cardnameValue = cardname.value.trim();
  let cardnumberValue = cardnumber.value.trim();
  let cardmonthValue = cardmonth.value.trim();
  let cardyearValue = cardyear.value.trim();
  let cardcvcValue = cardcvc.value.trim();
 
  if(cardnameValue === '') {
    paymentsetErrorFor(cardname, 'REQUIRED');
    validate=false;
  } else {
    paymentsetSuccessFor(cardname);
  }
  
  if(cardnumberValue === '') {
    paymentsetErrorFor(cardnumber, 'REQUIRED');
    validate=false;
  } else {
    paymentsetSuccessFor(cardnumber);
  }
  
  if(cardmonthValue === '') {
    paymentsetErrorFor(cardmonth, 'REQUIRED');
    validate=false;
  } else {
    paymentsetSuccessFor(cardmonth);
  }
  
  if(cardyearValue === '') {
    paymentsetErrorFor(cardyear, 'REQUIRED');
    validate=false;
  } else{
    paymentsetSuccessFor(cardyear);
  }

   if(cardcvcValue === '') {
    paymentsetErrorFor(cardcvc, 'REQUIRED');
    validate=false;
  } else{
    paymentsetSuccessFor(cardcvc);
  }
  return validate;
}
function checkDebitInputs(civility_debit,fname_debit,lname_debit,bankname_debit,agencyname_debit,agencyaddr_debit,otheraddr_debit,agencyzipcode_debit,agencycity_debit,ibnnum_debit,swiftcode_debit,agreement_debit) {
  
  var validate=true;
  // trim to remove the whitespaces
  const civility_debitValue= civility_debit.value.trim();
  const fname_debitValue= fname_debit.value.trim();
  const lname_debitValue= lname_debit.value.trim();
  const bankname_debitValue= bankname_debit.value.trim();
  const agencyname_debitValue= agencyname_debit.value.trim();
  const agencyaddr_debitValue= agencyaddr_debit.value.trim();
  const otheraddr_debitValue= otheraddr_debit.value.trim();
  const agencyzipcode_debitValue= agencyzipcode_debit.value.trim();
  const agencycity_debitValue= agencycity_debit.value.trim();
  const ibnnum_debitValue= ibnnum_debit.value.trim();
  const swiftcode_debitValue= swiftcode_debit.value.trim();
  const agreement_debitValue= agreement_debit.value.trim();
 
  if(civility_debitValue === '') {
    paymentsetErrorFor(civility_debit, 'REQUIRED');
    validate=false;
  } else {
    paymentsetSuccessFor(civility_debit);
  }
  
  if(fname_debitValue === '') {
    paymentsetErrorFor(fname_debit, 'REQUIRED');
    validate=false;
  } else {
    paymentsetSuccessFor(fname_debit);
  }
  
  if(lname_debitValue === '') {
    paymentsetErrorFor(lname_debit, 'REQUIRED');
    validate=false;
  } else {
    paymentsetSuccessFor(lname_debit);
  }
  
  if(bankname_debitValue === '') {
    paymentsetErrorFor(bankname_debit, 'REQUIRED');
    validate=false;
  } else{
    paymentsetSuccessFor(bankname_debit);
  }

   if(agencyname_debitValue === '') {
    paymentsetErrorFor(agencyname_debit, 'REQUIRED');
    validate=false;
  } else{
    paymentsetSuccessFor(agencyname_debit);
  }
  if(agencyaddr_debitValue === '') {
    paymentsetErrorFor(agencyaddr_debit, 'REQUIRED');
    validate=false;
  } else {
    paymentsetSuccessFor(agencyaddr_debit);
  }
  
  if(otheraddr_debitValue === '') {
    paymentsetErrorFor(otheraddr_debit, 'REQUIRED');
    validate=false;
  } else{
    paymentsetSuccessFor(otheraddr_debit);
  }

   if(agencyzipcode_debitValue === '') {
    paymentsetErrorFor(agencyzipcode_debit, 'REQUIRED');
    validate=false;
  } else{
    paymentsetSuccessFor(agencyzipcode_debit);
  }
   if(agencycity_debitValue === '') {
    paymentsetErrorFor(agencycity_debit, 'REQUIRED');
    validate=false;
  } else {
    paymentsetSuccessFor(agencycity_debit);
  }
  
  if(otheraddr_debitValue === '') {
    paymentsetErrorFor(otheraddr_debit, 'REQUIRED');
    validate=false;
  } else{
    paymentsetSuccessFor(otheraddr_debit);
  }

   if(ibnnum_debitValue === '') {
    paymentsetErrorFor(ibnnum_debit, 'REQUIRED');
    validate=false;
  } else{
    paymentsetSuccessFor(ibnnum_debit);
  }
  if(swiftcode_debitValue === '') {
    paymentsetErrorFor(swiftcode_debit, 'REQUIRED');
    validate=false;
  } else{
    paymentsetSuccessFor(swiftcode_debit);
  }
  if($(agreement_debit).is(":checked")){
  
   paymentsetSuccessFor(agreement_debit);
   
  } else{

    paymentsetErrorFor(agreement_debit, 'REQUIRED');
    validate=false;
  }
  return validate;
}
function paymentsetErrorFor(input, message) {
  const formControl = input.parentElement;
  const small = formControl.querySelector('small');
  formControl.className = 'cardform-control errors';
  small.innerText = message;
}

function paymentsetSuccessFor(input) {
  const formControl = input.parentElement;
  formControl.className = 'cardform-control success';
}
    //payment validation
    //End Edit Booking Section
    //get promo code discount
    function getpromocodediscount(val1){
   var count=0;
    var val =val1;
    if (val!='')
    {
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'getpromodiscount'; ?>',
                data: {'promo_code': val},
                success: function (data) {
                   $('#promo_code_div').empty();
                   if(data.result=='200'){
                       
                    $('.amountdetaillastdiv .dd2_amount span').html(data.discount['totalprice']+'€');
                    $('#promo_code_div').append(' <div class="col-md-5" style="padding: 0px;"><div class="dd1_amount"><span>Code Discount :</span></div>  </div><div class="col-md-3" style="padding: 0px;text-align: center;"><span>'+data.discount['promocodediscount']+'%</span></div><div class="col-md-4" style="padding: 0px;"><div class="dd2_amount"><span>-  '+data.discount['promocodediscountfee']+' €</span></div></div>');
                     }
                    else{
                      alert("invalid discount code");
                      $('#promo_code_div').append('<div class="col-md-4" style="padding: 5px 0px 5px 0px;">Discount Code : </div><div class="col-md-6" style="padding: 5px 0px 5px 0px;"><input type="password" placeholder="discount code" id="promo_code" class="form-control" style="height:35px !important;"></div><div class="col-md-2" style="padding: 5px 0px 5px 5px;"><button type="button" class="btn bookingquoteapplybtn" onclick="getpromocodediscount()">APPLY</button></div>');
                    }


                     
                }
                });
    }
}
    //get promo code discount
     //add custom price
function addcustomeprice(eventbtn,customprice,bookingid){

 if (bookingid != '' && customprice != '' && customprice != 0 ){
  $("#custompricefield").val('');
  //$("#custompricefield").val(customprice); 

  //get new price
  $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/bookings/update_booking_customprice'; ?>',
                data: {'bookingid': bookingid,'customprice': customprice},
                success: function (data) {
              
                   if(data.result=='200'){
                       
                   $('#bookingpricedivbtn').empty();
                    $('#totalpricettcdiv').empty(); 
                    $('#bookingpricedivbtn').html('Estimated Price : '+data.price+' € HT');
                    $('#totalpricettcdiv').html(data.totalprice+' € TTC');
                    $('#bookingpricevaluesdetailsdiv').html('');
                    $('#bookingpricevaluesdetailsdiv').html(data.priceview);
                   
                     }
                     else{
                     alert("something went wrong");
                   }
                  


                     
                }
       });
  //get new price

}else{
  alert("something went wrong");
}
}
function addautocustomeprice(eventbtn,bookingid){
  var customprice=$('#custompricefield').val();
 if (bookingid != '' && customprice != '' && customprice != 0 ){
 // $("#custompricefield").val('');
  //$("#custompricefield").val(customprice); 

  //get new price
  $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/bookings/update_booking_customprice'; ?>',
                data: {'bookingid': bookingid,'customprice': customprice},
                success: function (data) {
                  
                   if(data.result=='200'){
                    $('#bookingpricedivbtn').empty();
                    $('#totalpricettcdiv').empty(); 
                    $('#bookingpricedivbtn').html('Estimated Price : '+data.price+' € HT');
                    $('#totalpricettcdiv').html(data.totalprice+' € TTC');
                    $('#bookingpricevaluesdetailsdiv').html('');
                    $('#bookingpricevaluesdetailsdiv').html(data.priceview);
                     
                   
                     }
                     else{
                     alert("something went wrong");
                   }
                  


                     
                }
       });
  //get new price

}else{
  alert("something went wrong");
}
}
//add custom price
   //add stop boxes in add booking
    function addstopboxes(){
    n=n+1;
    $("#stopaddressdiv").append('<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 stopinnerdiv" style="padding:0px;"><div class="col-lg-7 col-md-7 col-sm-7 col-xs-12" style="padding:0px;"><div class="form-group"><div class="input-group" style="position: relative;"><span class="input-group-addon"><i class="fa fa-map-marker"></i></span><input type="text" name="stopaddress[]" placeholder="Addresse line"  class="pick_up_txtbox_bkg" id="stopaddr_'+n+'"   onFocus="geolocate()" value=""  style="padding-right: 30px;height: 30px !important;border-radius: 0px"></div><div class="currentstoplocationbtn" onclick="getStopCurrentLocation('+n+')"><img src="<?= base_url()?>assets/theme/default/images/target-a.svg"></div><div class="currentstoplocationdiv" id="stopercurrentlocationdiv_'+n+'"><div class="form-group"><div class="inner-addon left-addon"><i class="glyphicon glyphicon-map-marker" style="top: -2px;"></i><input type="text" readonly id="stopercurrentlocationfield_'+n+'" class="currentstoppoiinput" onclick="setStopCurrentLocation('+n+')" style="height:30px !important;"></div></div></div><div class="currentstoplocationloader" id="stoperlocationloader_'+n+'"><img src="<?= base_url()?>assets/theme/default/images/loader_2.gif" ></div> </div></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding-right:0px;"><div class="col-lg-7 col-md-7 col-sm-7 col-xs-12" style="padding-right:0px;text-align:right;margin-left: 16%;"><label class="pickup_time pickup_time_label" style="margin:0px !important;padding-top: 9px !important;font-size:13px !important;"><?php echo 'Waiting Time'; ?></label></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="padding-right:0px;text-align:right;padding-left: 5px;"><input class="stopwaitingtimepicker" id="stopwaitingtimepicker_'+n+'"  name="stopwaittime[]" type="text" value="<?php echo date('00 : 00') ?>" style="width:100%;height: 30px !important;border-radius: 0px" /></div></div><div class="col-lg-1 col-md-1 col-sm-1" style="position:relative;"><button type="button"  onclick="removestopboxes(this,'+n+');" class="minusstopicon"><i class="fa fa-minus" style="margin:0px;"></i></button></div></div>');
 
    $("#stopwaitingtimepicker_"+n).timepicki({
         type_of_timezone:"zero",
        step_size_minutes:'5',
        show_meridian:false,
        min_hour_value:0,
        max_hour_value:23,
        overflow_minutes:true,
        increase_direction:'up',
        disable_keyboard_mobile: true});
        stopper.push(n);
        initAutocomplete();
     
   }
   function removestopboxes(e,current){
   
     $(e).closest(".stopinnerdiv").remove();
      const index = stopper.indexOf(current);
      if (index > -1) {
        stopper.splice(index, 1);
      }
   }
    var currentstopaddress='';
      async function getStopCurrentLocation(currentstoper){
              
                     if(currentstopaddress==''){
                         document.getElementById("stoperlocationloader_"+currentstoper).style.display = "block";
                         let m1= await getStopperCurrentLocation();
                     
                         document.getElementById("stopercurrentlocationfield_"+currentstoper).value='';
                         document.getElementById("stopercurrentlocationfield_"+currentstoper).value =m1.address;
                         document.getElementById("stoperlocationloader_"+currentstoper).style.display = "none";
                     }else{
                      document.getElementById("stopercurrentlocationfield_"+currentstoper).value =currentstopaddress;
                     }

                   
                        $("#stopercurrentlocationdiv_"+currentstoper).toggle();
   
                   }

     function getStopperCurrentLocation(){

       return  new Promise((resolve,reject)=>{
       setTimeout(()=>{
       if(navigator.geolocation){
                    
                      navigator.geolocation.getCurrentPosition(function(position){
                      
                         var lat = position.coords.latitude;
                         var lang = position.coords.longitude;
                 
                  var latlng =   new  google.maps.LatLng(lat, lang);
                 
                  var geocoder  = new google.maps.Geocoder();
           geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                      if (status == google.maps.GeocoderStatus.OK) {

                          if (results[0]) {
                               
                            var i;
                            var typeIdx;
                            currentstopaddress=results[0].formatted_address;
                            var addressinfo = {address:currentstopaddress};
                            resolve(addressinfo);
                                   
                          }
                      }
                      });
                     });
                  } else{
                     alert("Sorry, browser does not support geolocation!");
                  }
              
       },2000);
       });
}

 function  setStopCurrentLocation(currentstoper){                                           
                      //set address
                      var address=$('#stopercurrentlocationfield_'+currentstoper).val();
                      $('#stopaddr_'+currentstoper).val(address);
                      $("#stopercurrentlocationdiv_"+currentstoper).hide();
           }
    //add stop boxes in add booking
    //edit stop boxes in edit booking
      function editaddstopboxes(){
       
    l++;
   
     $("#editstopaddressdiv").append('<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 stopinnerdiv" style="padding:0px;"><div class="col-lg-7 col-md-7 col-sm-7 col-xs-12" style="padding:0px;"><div class="form-group"><div class="input-group" style="position: relative;"><span class="input-group-addon"><i class="fa fa-map-marker"></i></span><input type="text" name="stopaddress[]" placeholder="Addresse line"  class="pick_up_txtbox_bkg" id="editstopaddr_'+l+'"   onFocus="geolocate()" value=""  style="padding-right: 30px;height: 30px !important;border-radius: 0px"></div><div class="currentstoplocationbtn" onclick="editgetStopCurrentLocation('+l+')"><img src="<?= base_url()?>assets/theme/default/images/target-a.svg"></div><div class="currentstoplocationdiv" id="editstopercurrentlocationdiv_'+l+'"><div class="form-group"><div class="inner-addon left-addon"><i class="glyphicon glyphicon-map-marker" style="top: -2px;"></i><input type="text" readonly id="editstopercurrentlocationfield_'+l+'" class="currentstoppoiinput" onclick="editsetStopCurrentLocation('+l+')" style="height:30px !important;"></div></div></div><div class="currentstoplocationloader" id="editstoperlocationloader_'+l+'"><img src="<?= base_url()?>assets/theme/default/images/loader_2.gif" ></div> </div></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding-right:0px;"><div class="col-lg-7 col-md-7 col-sm-7 col-xs-12" style="padding-right:0px;text-align:right;"><label class="pickup_time pickup_time_label" style="margin:0px !important;padding-top: 9px !important;font-size:13px !important;"><?php echo 'Waiting Time'; ?></label></div><div class="col-lg-5 col-md-5 col-sm-5 col-xs-12" style="padding-right:0px;text-align:right;padding-left: 5px;"><input class="stopwaitingtimepicker" id="editstopwaitingtimepicker_'+l+'"  name="stopwaittime[]" type="text" value="<?php echo date('00 : 00') ?>" style="width:100%;height: 30px !important;border-radius: 0px" /></div></div><div class="col-lg-1 col-md-1 col-sm-1" style="position:relative;"><button type="button"  onclick="editremovestopboxes(this,'+l+');" class="minusstopicon"><i class="fa fa-minus" style="margin:0px;"></i></button></div></div>');
 
    $("#editstopwaitingtimepicker_"+l).timepicki({
         type_of_timezone:"zero",
        step_size_minutes:'5',
        show_meridian:false,
        min_hour_value:0,
        max_hour_value:23,
        overflow_minutes:true,
        increase_direction:'up',
        disable_keyboard_mobile: true});
        editstopper.push(l);
        initAutocomplete();
     
   }
   function editremovestopboxes(e,current){
   
     $(e).closest(".stopinnerdiv").remove();
      const index = editstopper.indexOf(current);
      if (index > -1) {
        editstopper.splice(index, 1);
      }
   }
   var editcurrentstopaddress='';
      async function editgetStopCurrentLocation(currentstoper){
              
                     if(editcurrentstopaddress==''){
                         document.getElementById("editstoperlocationloader_"+currentstoper).style.display = "block";
                         let m1= await editgetStopperCurrentLocation();
                     
                         document.getElementById("editstopercurrentlocationfield_"+currentstoper).value='';
                         document.getElementById("editstopercurrentlocationfield_"+currentstoper).value =m1.address;
                         document.getElementById("editstoperlocationloader_"+currentstoper).style.display = "none";
                     }else{
                      document.getElementById("editstopercurrentlocationfield_"+currentstoper).value =editcurrentstopaddress;
                     }

                   
                        $("#editstopercurrentlocationdiv_"+currentstoper).toggle();
   
                   }

     function editgetStopperCurrentLocation(){

       return  new Promise((resolve,reject)=>{
       setTimeout(()=>{
       if(navigator.geolocation){
                    
                      navigator.geolocation.getCurrentPosition(function(position){
                      
                         var lat = position.coords.latitude;
                         var lang = position.coords.longitude;
                 
                  var latlng =   new  google.maps.LatLng(lat, lang);
                 
                  var geocoder  = new google.maps.Geocoder();
           geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                      if (status == google.maps.GeocoderStatus.OK) {

                          if (results[0]) {
                               
                            var i;
                            var typeIdx;
                            editcurrentstopaddress=results[0].formatted_address;
                            var addressinfo = {address:editcurrentstopaddress};
                            resolve(addressinfo);
                                   
                          }
                      }
                      });
                     });
                  } else{
                     alert("Sorry, browser does not support geolocation!");
                  }
              
       },2000);
       });
}

 function  editsetStopCurrentLocation(currentstoper){                                           
                      //set address
                      var address=$('#editstopercurrentlocationfield_'+currentstoper).val();
                      $('#editstopaddr_'+currentstoper).val(address);
                      $("#editstopercurrentlocationdiv_"+currentstoper).hide();
           }
    //edit stop boxes in edit booking
function addcustomreminderbox(){
 
  var title=$('#customremindertitlefield').val();
  var date=$('#customreminderdatefield').val();
  var time=$('#customremindertimepicker_1').val();
  var comment=$('#customremindercommentfield').val();
  var bookingid=$('#bookingidfieldvalue').val();
  
  if(title== '' || date== '' || time== '' || comment== ''){
    alert("Please fill the form");
  }else{
       $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/bookings/addcustomreminders'; ?>',
                data: {'bookingid': bookingid,'title':title,'date': date,'time':time,'comment':comment},
                success: function (data) {
                  
                   if(data.result=='200'){
                       
                           
                            $('#customreminderrecorded').append('<div class="customreminderbox col-md-12 pdz"><div class="col-md-3 text-center pdz">'+data.record.title+'</div><div class="col-md-2 text-center pdz">'+data.record.date+'</div><div class="col-md-2 text-center pdz">'+data.record.time+'</div><div class="col-md-3 text-center pdz" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">'+data.record.comment+'</div><div class="col-md-2" style="padding-left: 52px;padding-top: 7px;"><button type="button" onclick="removecustomreminderbox(this,'+data.record.customreminderid+');" class="minusrediconcustomreminder"><i class="fa fa-minus" style="margin:0px !important;"></i></button></div></div>');
                              $('#customremindertitlefield').val('');
                              $('#customremindercommentfield').val('');
                 }
                else{
                       
                        alert("something went wrong");
                   }
                  


                     
                }
       });
  }  
}
function removecustomreminderbox(e,customreminderid){
  if(customreminderid == ''){
    alert("something went wrong");
  }else{
    $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/bookings/removecustomreminders'; ?>',
                data: {'customreminderid': customreminderid},
                success: function (data) {
                   if(data.result=='200'){
                        $(e).closest(".customreminderbox").remove();                            
                   }
                   else{ 
                        alert("something went wrong");
                   }    
                }
       });
    }
}

//add notification in database and send mail 
function addbookingnotification(bookingid){
  
  document.getElementById('notificationsentloaderbtn').style.display = "block";
  //document.getElementById('notificationsentbtn').style.pointerEvents = "none";
  var date=$('#notificationdatefield').val();
  var time=$('#notificationtimepicker').val();
   $.ajax({
                type: "GET",
                url: '<?php echo base_url().'admin/bookings/add_booking_notification'; ?>',
                data: {'bookingid': bookingid,'date': date,'time':time},
                success: function (data) {
                  
                   if(data.result=='200'){
                       document.getElementById('notificationsentloaderbtn').style.display = "none";
                       //document.getElementById('notificationsentbtn').style.pointerEvents = "none";
                       //document.getElementById('notificationdatefield').style.pointerEvents = "none";
                       //document.getElementById('notificationtimepicker').style.pointerEvents = "none";
                      // document.getElementById('notificationsentstatus').innerHTML='';
                       //document.getElementById('notificationsentstatus').innerHTML=data.status;
                      $('#notificationmaindiv').append('<div class="col-md-12 pdz"><div class="col-md-2 pdz text-center" style="width: 20%;">'+data.date+'</div><div class="col-md-2 pdz text-center" style="width: 20%;">'+data.time+'</div> <div class="col-md-2 pdz text-center" style="width: 20%;"><a href="<?= base_url(); ?>admin/bookings/notification_pdf/'+data.id+'/'+data.bookingid+'" target="_blank"><img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form"></a></div><div class="col-md-2 pdz text-center" style="width: 20%;">'+data.status+'</div><div class="col-md-2 pdz text-center" style="width: 20%;"></div></div> ');

                       if(data.status.toLowerCase() == "sent"){
                            alert("Notification sent via Email.");
                       }else{
                         alert("Notification will be send via Emai at "+data.date+" "+data.time);
                       }
                 }
                else{
                        document.getElementById('notificationsentloaderbtn').style.display = "none";
                        //document.getElementById('notificationsentbtn').style.pointerEvents = "auto";
                        alert("something went wrong");
                   }
                  


                     
                }
       });
}
</script>