            <div class="card">              
                <div class="card-body bg-light">
                  <input type="hidden" id="creditcardbtnnum" value="<?= $paymentmethodcount ?>">
                    <?php if (validation_errors()): ?>
                        <div class="alert alert-danger" role="alert">
                            <strong>Oops!</strong>
                            <?php echo validation_errors() ;?> 
                        </div>  
                    <?php endif ?>
                    <div id="payment-errors"></div>  
                       <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                        <div class="form-group cardform-control" >
                          <input type="text" list="cardnames" placeholder="Name on card"  name="name_<?= $cardtype ?>" id="name_<?= $cardtype ?>" autocomplete="off" value="">
                            <datalist id="cardnames">
                              <option value="Visa">
                              <option value="Mastercard">
                              <option value="Amex">
                              <option value="Unionpay">
                              <option value="Jcb">
                              <option value="Discover">
                              <option value="Diners">
                            </datalist>
                           
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                              
                             </small>
                        </div>  
                      </div>
                    </div>

                      
                      <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                         <div class="form-group cardform-control">
                         
                            <input type="text" name="card_num_<?= $cardtype ?>" id="card_num_<?= $cardtype ?>" class="form-control" placeholder="Card Number" autocomplete="off" value="" >
                             <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                   
                              </small>
                        </div>
                      </div>
                    </div>
                       
                        
                        <div class="row" style="padding-bottom: 20px;">

                            <div class="col-sm-8">
                                 <div class="row">
                                    <div class="col-sm-6">
                                  <div class="form-group cardform-control" >
                                        <select name="exp_month_<?= $cardtype ?>" id="exp_month_<?= $cardtype ?>">
                                          <option value="">Month</option>
                                          <?php for($i=1;$i<13;$i++){ ?>
                                             <option  value="<?= $i ?>"><?= ($i<10)?'0'.$i:$i; ?></option>
                                          <?php } ?>
                                        </select>
                                          
                                             <i class="fa fa-check-circle"></i>
                                              <i class="fa fa-exclamation-circle"></i>
                                              <small>
                                             
                                              </small>
                                          
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                    <div class="form-group cardform-control">
                                          <select name="exp_year_<?= $cardtype ?>" id="exp_year_<?= $cardtype ?>">
                                          <option value="">Year</option>
                                          <?php for($j=20;$j<36;$j++){ ?>
                                            <option  value="<?= '20'.$j ?>"><?= '20'.$j ?></option>
                                          <?php } ?>
                                          
                                        
                                        </select>
                                          
                                             <i class="fa fa-check-circle"></i>
                                              <i class="fa fa-exclamation-circle"></i>
                                              <small>
                                             
                                              </small>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group cardform-control">
                                    <input type="text" name="cvc_<?= $cardtype ?>" id="cvc_<?= $cardtype ?>" maxlength="4" class="form-control" autocomplete="off" placeholder="CVC" value="" >
                                     <i class="fa fa-check-circle"></i>
                                      <i class="fa fa-exclamation-circle"></i>
                                      <small>
                                      
                                      </small>
                                </div>
                            </div>
                        </div>                   
                   </div>
               </div>