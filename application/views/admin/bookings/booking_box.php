<?php     
 $attributes = array("name" => 'booking_form', "id" => 'booking_form');
  echo form_open('admin/bookings/booking_save',$attributes);
?>
 <div class="row booknow-content new-box-type" style="margin-top: 10px;">
   <?php if(!empty($this->session->flashdata('pickerror'))):  ?>
     <div class="col-md-12" style="padding: 0px;">
        
                          <div class="alertbooking">
                                <span class="closebookingbtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                                <strong>Error!</strong> <?= $this->session->flashdata("pickerror") ?>
                              </div>
            
          
       </div> 
    <?php endif; ?>    
     <?php if(!empty($this->session->flashdata('picksuccess'))):  ?>
     <div class="col-md-12" style="padding: 0px;">
        
                          <div class="alertbookingsuccess">
                                <span class="closebookingbtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                                <strong>Congratulation!</strong> <?= $this->session->flashdata("picksuccess") ?>
                              </div>
            
          
       </div> 
    <?php endif; ?> 
  <div class="col-md-12" style="width: 100%;">
    <div class="col-md-5" style="padding: 0px;width: 46%;">
      <div class="col-md-12" style="padding:0px;">
        <div class="booking-tab">
           <input type="hidden" value="address" id="pickupcategory" name="pickupcategory">
         
        
          <ul class="nav nav-tabs">
           
             <li><button class="btn btn-default extra-pad" style="font-weight:900;pointer-events: none;padding-bottom: 11px;border-radius:0px;width:100%;"> <?php echo 'Pickup'; ?></button></li>
              <li  class="active" style="width:14.5%;"><a id="pickaddresscategorybtn" data-toggle="tab" href="#pick_address"><i class="fa fa-map-marker"></i> <?php echo 'Address'; ?></a></li>
             <li style="width:16%;"><a id="pickpackagecategorybtn" data-toggle="tab" href="#pick_package"><i class="fa fa-suitcase"></i> <?php echo 'Package'; ?></a></li>
             <li style="width:15.5%;"><a id="pickairportcategorybtn" data-toggle="tab" href="#pick_airport"><i class="fa fa-plane"></i> <?php echo 'Airport'; ?></a></li>
             <li><a id="picktraincategorybtn" data-toggle="tab" href="#pick_train"><i class="fa fa-train"></i> <?php echo 'Train'; ?></a></li>
             <li><a id="pickhotelcategorybtn" data-toggle="tab" href="#pick_hotel"><i class="fa fa-hotel"></i> <?php echo 'Hotel'; ?></a></li>
             <li><a id="pickparkcategorybtn" data-toggle="tab" href="#pick_park"><i class="fa fa-tree"></i> <?php echo 'Park'; ?></a></li>
              <li style="width:26.5%;"><a id="pickservicecategorybtn" data-toggle="tab" href="#pickservicecategory"><i class="fa fa-cube"></i> <?php echo ' Service Category'; ?></a></li>
               <li style="width:14.5%;"><a id="pickservicebtn" data-toggle="tab" href="#pickservice"><i class="fa fa-certificate"></i> <?php echo ' Service'; ?></a></li>
         </ul>
        </div>
      </div>
      <div class="col-md-12" style="padding:0px;">
        <div class="tab-content">
          <div id="pickservicecategory" class="tab-pane fade">
           <div class="row">
              <div class="col-md-1">
                  <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
              </div>
              <div class="col-md-11">
                  <select name="pickservicecategory" class="servicecategoryinput" id="pickservicecategoryinput"  style="background-image: url(<?php echo base_url();?>assets/theme/default/images/servicecategory.png) !important;background-repeat:no-repeat !important;background-position:10px 9px !important;background-size: 15px;" >
                      <option value="">Select</option>
                                <?php
                                  foreach($service_cat as $key => $item){   
                                 ?>
                                   <option value="<?= $item->id ?>"><?= $item->category_name ?></option>
                                   <?php 
                                   
                                      }    
                                     ?>
                  </select>
              </div>
            </div>
          </div>
           <div id="pickservice" class="tab-pane fade">
           <div class="row">
              <div class="col-md-1">
                  <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
              </div>
              <div class="col-md-11">
                  <select name="pickservice" class="pickserviceinput" id="pickserviceinput"  style="background-image: url(<?php echo base_url();?>assets/theme/default/images/services.png) !important;background-repeat:no-repeat !important;background-position:10px 9px !important;background-size: 15px;" >
                      <option value="">Select</option>
                               <?php
                                   foreach($service as $key => $item){
                                 
                                 ?>
                                   <option value="<?= $item->id ?>"><?= $item->service_name ?></option>
                                   <?php 
                                   
                                      }    
                                     ?>
                  </select>
              </div>
            </div>
          </div>
            <div id="pick_address" class="tab-pane fade  active in">
               <input type="hidden" id="pickloc_lat" />
               <input type="hidden" id="pickloc_long" />
                <div class="row">
                  <div class="col-md-1">
                   <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;margin:8px 0px 0px -5px;"></i>
                 </div>
                 <div class="col-md-11">
                   <div class="form-group">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-map-marker"></i>
                        </span>
                         <input type="text" name="pickaddressfield" placeholder="Addresse line"  class="pick_up_txtbox_bkg" id="pickupautocomplete"   onFocus="geolocate()">
                      </div>
                   </div>
                 </div>
               </div>

              <div class="row" style="margin-top: 5px;">
                  <div class="col-md-1">

                  </div>
                 <div class="col-md-11">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text" name="pickotheraddressfield"  placeholder="Complément d'adresse"  class="pick_up_txtbox_bkg" style="padding-left:43px !important;">
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-md-1">

                </div>
                <div class="col-md-11">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-6" style="padding: 0px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                               <input type="text" name="pickzipcodefield" placeholder="Zip code"  class="pick_up_txtbox_bkg" id="pickup_code">
                            </div>
                          </div>
                         </div>
                         <div class="col-md-6" style="padding: 0px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text" name="pickcityfield" placeholder="City"  class="pick_up_txtbox_bkg" id="pickup_city">
                                    </div>
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
            </div>
          <div id="pick_package" class="tab-pane fade">
              <div class="row">
                  <div class="col-md-1">
                     <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                  </div>
                  <div class="col-md-11">
                     <select name="pickpackagefield" id="pickpackagefield" style="background-image: url(<?php echo base_url();?>assets/theme/default/images/fa-package.png) !important;background-repeat:no-repeat !important;background-position:10px 6px !important;" class="packagebox_pick_bkg ">
                       <option value="">Select</option>
                 
                       <?php
                        foreach($poi_package as $package){
                         ?>
                       <option value="<?= $package->id ?>"><?= $package->name ?></option>
                       <?php  }  ?>
                      </select>
                  </div>
              </div> 
          </div>


          <div id="pick_airport" class="tab-pane fade">
               <div class="row">
                  <div class="col-md-1">
                      <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                  </div>
                  <div class="col-md-11">
                      <select name="pickairportfield" id="airport_pick" style="background-image: url(<?php echo base_url();?>assets/theme/default/images/fa-plane.png) !important;background-repeat:no-repeat !important;background-position:10px 6px !important;" class="airlocation_pick_bkg ">
                       <option value="">Select</option>
                 
                       <?php
                        foreach($poi_data as $airport){
                          if(strtolower($airport->category)=="airport"){
                         ?>
                       <option value="<?= $airport->id ?>"><?= $airport->name ?></option>
                       <?php 
                             }
                              }    
                             ?>
                      </select>
                  </div>
               </div>
               <div class="row" style="margin-top:5px;">
                  <div class="col-md-1"></div>
                  <div class="col-md-4">
                      <input type="text" value="" name="pickairportflightnumberfield" placeholder="Flight No" class="pickup_flight_no_bkg " id="pickup_flight_no">
                  </div>
                  <div class="col-md-5 col-md-offset-2" style="padding: 0px;">
                    <div class="col-md-4" style="margin-top: 9px;padding:0px;">
                      <label for="timepicker_pick_fly">Arrival Time</label>
                    </div>
                     <div class="col-md-8">
                         <input name="pickairportarrivaltimefield" type="text" id="timepicker_pick_fly"  value="<?php echo date('h : i'); ?>"  class="pickup_flight_time_bkg" />
                      </div>
                  </div>
               </div>
          </div>

          <div id="pick_train" class="tab-pane fade">
                  <div class="row">
                      <div class="col-md-1">
                          <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                      </div>
                      <div class="col-md-11">
                          <select name="picktrainfield" class="trainlocation_pick_bkg " id="train_pick" style="background-image: url(<?php echo base_url();?>assets/theme/default/images/fa-train.png) !important;background-repeat:no-repeat !important;background-position:10px 6px !important;">
                            <option value="">Select</option>
                              <?php
                                foreach($poi_data as $train){
                                  if(strtolower($train->category)=="train"){
                                 ?>
                                   <option value="<?= $train->id ?>"><?= $train->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                          </select>
                      </div>
                  </div>
                  <div class="row" style="margin-top:5px;">
                      <div class="col-md-1"></div>
                      <div class="col-md-4">
                          <input id="pickup_train_no" class="pickup_train_no_bkg" type="text" value="" name="picktrainnumberfield" placeholder="Train No">
                      </div>
                      <div class="col-md-5 col-md-offset-2" style="padding: 0px">
                        <div class="col-md-4" style="margin-top: 9px;padding:0px;">
                          <label for="timepicker_pick_stn">Arrival Time</label>
                        </div>
                         <div class="col-md-8">
                           <input  class="pickup_flight_time_bkg" id="timepicker_pick_stn" name="picktrainarrivaltimefield" type="text" value="<?php echo date('h : i'); ?>" />
                          </div> 
                      </div>
                  </div>
          </div>


          <div id="pick_hotel" class="tab-pane fade">
           <div class="row">
              <div class="col-md-1">
                  <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
              </div>
              <div class="col-md-11">
                  <select name="pickhotelfield" class="hotellocation_pick_bkg" id="hotel_pick" style="background-image: url(<?php echo base_url();?>assets/theme/default/images/fa-hotel.png) !important;background-repeat:no-repeat !important;background-position:10px 6px !important;">
                      <option value="">Select</option>
                      <?php
                                foreach($poi_data as $hotel){
                                  if(strtolower($hotel->category)=="hotel"){
                                 ?>
                                   <option value="<?= $hotel->id ?>"><?= $hotel->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                  </select>
              </div>
            </div>
          </div>


          <div id="pick_park" class="tab-pane fade">
           <div class="row">
              <div class="col-md-1">
                  <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i> 
              </div>
              <div class="col-md-11">
                  <select name="pickparkfield" class="parklocation_pick_bkg " id="park_pick" style="background-image: url(<?php echo base_url();?>assets/theme/default/images/fa-tree.png) !important;background-repeat:no-repeat !important;background-position:10px 6px !important;">
                   <option value="">Select</option>
                   <?php
                                foreach($poi_data as $park){
                                  if(strtolower($park->category)=="park"){
                                 ?>
                                   <option value="<?= $park->id ?>"><?= $park->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                 </select>
                </div>
              </div>
          </div>
       </div>
      </div>
       
    </div>

    <div class="col-md-2" style="text-align:center;font-size:60px;padding-top:50px;width: 8%;"> <i class="fa fa-arrow-right"></i> </div>
    <div class="col-md-5" style="padding:0px;width: 46%;">
      <div class="col-md-12" style="padding:0px;">
          <div class="booking-tab">
             <input type="hidden" value="address" id="dropoffcategory" name="dropoffcategory">
            <ul class="nav nav-tabs">

             <li><button class="btn btn-default extra-pad" style="font-weight:900;pointer-events: none;padding-bottom: 11px;border-radius:0px;width:100%;"><?php echo 'Dropoff'; ?></button></li>
               <li class="active" style="width:14.5%;"><a id="dropaddresscategorybtn" data-toggle="tab" href="#drop_address"><i class="fa fa-map-marker"></i> <?php echo 'Address'; ?></a></li>
             <li  style="width:16%;"><a id="droppackagecategorybtn" data-toggle="tab" href="#drop_package"><i class="fa fa-suitcase"></i> <?php echo 'Package'; ?></a></li>

             <li style="width:15.5%;"><a id="dropairportcategorybtn" data-toggle="tab" href="#drop_airport"><i class="fa fa-plane"></i> <?php echo 'Airport'; ?></a></li>
             <li><a id="droptraincategorybtn" data-toggle="tab" href="#drop_train"><i class="fa fa-train"></i> <?php echo 'Train'; ?></a></li>
             <li><a id="drophotelcategorybtn" data-toggle="tab" href="#drop_hotel"><i class="fa fa-hotel"></i> <?php echo 'Hotel'; ?></a></li>
             <li><a id="dropparkcategorybtn" data-toggle="tab" href="#drop_park"><i class="fa fa-tree"></i> <?php echo 'Park'; ?></a></li>
           </ul>
          </div>
      </div>
    <div class="col-md-12" style="padding: 0px;">
     <div class="tab-content">
        <div id="drop_address" class="tab-pane fade  active in">
              <input type="hidden" id="droploc_lat" />
               <input type="hidden" id="droploc_long" />
               <div class="row">
                  <div class="col-md-1">
                   <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;margin:8px 0px 0px -5px;"></i>
                 </div>
                 <div class="col-md-11">
                   <div class="form-group">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-map-marker"></i>
                        </span>
                         <input type="text" name="dropaddressfield" placeholder="Addresse line"  class="pick_up_txtbox_bkg" id="dropoffautocomplete" onFocus="geolocate()">
                      </div>
                   </div>
                 </div>
               </div>

              <div class="row" style="margin-top: 5px;">
                  <div class="col-md-1">

                  </div>
                 <div class="col-md-11">
                    <div class="form-group">
                     <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                        <input type="text" name="dropotheraddressfield" placeholder="Complément d'adresse"  class="pick_up_txtbox_bkg" style="padding-left:43px !important;">
                      </div>
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-md-1">

                </div>
                <div class="col-md-11">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-6" style="padding: 0px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                               <input type="text" name="dropzipcodefield" placeholder="Zip code"  class="pick_up_txtbox_bkg" id="dropoff_code">
                            </div>
                          </div>
                         </div>
                         <div class="col-md-6" style="padding: 0px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text" name="dropcityfield" placeholder="City"  class="pick_up_txtbox_bkg" id="dropoff_city">
                                    </div>
                               </div>
                          </div>
                       </div>
                   </div>
              </div>
       </div>
        <div id="drop_package" class="tab-pane fade">
            <div class="row">
                <div class="col-md-1">
                <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                </div>
                <div class="col-md-11">
                  <select name="droppackagefield" id="droppackagefield" style="background-image: url(<?php echo base_url();?>assets/theme/default/images/fa-package.png) !important;background-repeat:no-repeat !important;background-position:10px 6px !important;" class="packagebox_pick_bkg ">
                       <option value="">Select</option>
                 
                       <?php
                        foreach($poi_package as $package){
                         ?>
                       <option value="<?= $package->id ?>"><?= $package->name ?></option>
                       <?php  }  ?>
                      </select>
                </div>
            </div>  
        </div>
        <div id="drop_airport" class="tab-pane fade">
                   <div class="row">
                      <div class="col-md-1">
                          <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                      </div>
                      <div class="col-md-11">
                          <select name="dropairportfield" id="airport_drop" style="background-image: url(<?php echo base_url();?>assets/theme/default/images/fa-plane.png) !important;background-repeat:no-repeat !important;background-position:10px 6px !important;" class="airlocation_pick_bkg ">
                           <option value="">Select</option>
                            <?php
                                foreach($poi_data as $airport){
                                  if(strtolower($airport->category)=="airport"){
                                 ?>
                                   <option value="<?= $airport->id ?>"><?= $airport->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                          </select>
                    </div>
                  </div>
                  <div class="row" style="margin-top:5px;">
                      <div class="col-md-1"></div>
                      <div class="col-md-4">
                          <input type="text" value="" name="dropairportflightnumberfield" placeholder="Flight No" class="pickup_flight_no_bkg " id="dropof_flight_no">
                      </div>
                      <div class="col-md-5 col-md-offset-2" style="padding: 0px;">
                        <div class="col-md-4" style="margin-top: 9px;padding:0px;">
                          <label for="timepicker_drop_fly_1">Arrival Time</label>
                        </div>
                         <div class="col-md-8">
                         <input  class="pickup_flight_time_bkg" id="timepicker_drop_fly_1" name="dropairportarrivaltimefield" type="text" value="<?php echo date('h : i'); ?>" />
                       </div>
                      </div>
                  </div>
        </div>
        <div id="drop_train" class="tab-pane fade">
                     <div class="row">
                        <div class="col-md-1">
                            <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                        </div>
                        <div class="col-md-11">
                            <select name="droptrainfield" id="train_drop" class="trainlocation_pick_bkg" style="background-image: url(<?php echo base_url();?>assets/theme/default/images/fa-train.png) !important;background-repeat:no-repeat !important;background-position:10px 6px !important;">
                              <option value="">Select</option>
                                <?php
                                foreach($poi_data as $train){
                                  if(strtolower($train->category)=="train"){
                                 ?>
                                   <option value="<?= $train->id ?>"><?= $train->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                          </select>
                      </div>
                    </div>
                    <div class="row" style="margin-top:5px;">
                        <div class="col-md-1"></div>
                        <div class="col-md-4">
                            <input  class="pickup_train_no_bkg" type="text" value="" name="droptrainnumberfield" placeholder="Train No">
                        </div>
                        <div class="col-md-5 col-md-offset-2" style="padding: 0px;">
                          <div class="col-md-4" style="margin-top: 9px;padding:0px;">
                              <label for="pickup_train_time_bkg">Arrival Time</label>
                            </div>
                          <div class="col-md-8">
                            <input style="width:100% !important;" class="pickup_train_time_bkg" id="timepicker_drop_stn" name="droptrainarrivaltimefield" type="text" value="<?php echo date('h : i'); ?>" />
                          </div>
                        </div>
                    </div>
        </div>
        <div id="drop_hotel" class="tab-pane fade">
         <div class="row">
            <div class="col-md-1">
                <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
            </div>
            <div class="col-md-11">
                <select name="drophotelfield" class="hotellocation_pick_bkg" id="hotel_drop"  style="background-image: url(<?php echo base_url();?>assets/theme/default/images/fa-hotel.png) !important;background-repeat:no-repeat !important;background-position:10px 6px !important;">
                    <option value="">Select</option>
                    <?php
                                foreach($poi_data as $hotel){
                                  if(strtolower($hotel->category)=="hotel"){
                                 ?>
                                   <option value="<?= $hotel->id ?>"><?= $hotel->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                </select>
            </div>
         </div>
        </div>
        <div id="drop_park" class="tab-pane fade">
             <div class="row">
                <div class="col-md-1">
                    <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>

                </div>
                <div class="col-md-11">
                    <select name="dropparkfield" class="parklocation_pick_bkg" id="park_drop" style="background-image: url(<?php echo base_url();?>assets/theme/default/images/fa-tree.png) !important;background-repeat:no-repeat !important;background-position:10px 6px !important;">
                 <option value="">Select</option>
                   <?php
                                foreach($poi_data as $park){
                                  if(strtolower($park->category)=="park"){
                                 ?>
                                   <option value="<?= $park->id ?>"><?= $park->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                 </select>
                </div>
            </div>

        </div>



    </div>
  </div>
 
</div>

   
  

  
    </div>
</div>

    <div class="date-time row" style="margin-top: 10px;margin-bottom:10px;">
      <div class="col-md-6">
        <div class="pickupreturn_datetime_div">
              <div id="pickup_datetime_div" >
                <div class="pickdt-bkg" style="margin-right:10px;text-align:center;"> 
                    <label class="pickup_date pickup_dt_label" style="margin:0px !important;"> <?php echo 'Pickup Date'; ?> </label>
                    
                    <input id="dtpicker" class="pickup-dt-bkg bdatepicker"  type="text" value="<?php echo date('d/m/Y'); echo set_value('pick_date'); ?>" name="pickdatefield" placeholder="<?php echo 'Pickup Date'; ?>"  style="width: 90px !important;"/>
                </div>
                <div class="picktime-bkg" style="margin-right: 10px;text-align:center;"> 
                   <label class="pickup_time pickup_time_label" style="margin:0px !important;"><?php echo 'Pickup Time'; ?></label>
                    
                    <input   id="timepicker1" name="picktimefield" type="text" value="<?php echo date("h : i"); ?>" style="width:100%" />
                </div>
              </div>
          
              <div class="return_datetime_div" style="display: none;">

                <div class="picktime-bkg" style="margin-right: 10px;text-align:center;"> 
                   <label class="pickup_time pickup_time_label" style="margin:0px !important;"><?php echo 'Waiting Time'; ?></label>
                  
                    
                    <input   id="waitingtimepicker" name="waitingtimefield" type="text" value="<?php echo date('00 : 00') ?>" style="width:100%" />
                </div>

                <div class="dropdt-bkg" style="margin-right: 10px;text-align:center;">
                     <label style="margin:0px !important;" class="dropof_date dropof_dt_label"> <?php echo 'Return Date'; ?> </label>
                    

                   
                    <input id="dtpicker1 " class="bdatepicker"  type="text" value="<?php echo date('d/m/Y'); echo set_value('drop_date'); ?>" name="returndatefield" placeholder="<?php echo 'Drop Date'; ?>" style="width: 90px !important;" />
                </div>

                <div class="droptime-bkg" style="text-align:center;">
                    <label class="dropof_time dropof_time_label" style="margin:0px !important;"><?php echo 'Return Time'; ?></label>
                   
            
                    <input   id="timepicker2" name="returntimefield" type="text" value="<?php echo date('h : i') ?>" style="width:100%;margin-top: 0px !important;" />
                </div>
              </div> 
        </div>
        
          <div class="startend_datetime_div" style="display: none;">

             <div class="pickdt-bkg" style="margin-right:10px;text-align:center;"> 
                    <label class="pickup_date start_dt_label" style="margin:0px !important;"> <?php echo 'Start Date'; ?> </label>
                 
                  <input  class="pickup-dt-bkg bdatepicker"  type="text" value="<?php echo date('d/m/Y'); echo set_value('pick_date'); ?>" name="startdatefield" placeholder="<?php echo 'Start Date'; ?>"  style="width: 90px !important;"/>
              </div>
              <div class="picktime-bkg" style="margin-right: 10px;text-align:center;"> 
               
                  <label class="pickup_time start_time_label" style="margin:0px !important;"><?php echo 'Start Time'; ?></label>
                  
                  <input   id="starttimepicker" name="starttimefield" type="text" value="<?php echo date("h : i"); ?>" style="width:100%" />
              </div>

              <div class="dropdt-bkg" style="margin-right: 10px;text-align:center;">       
                  <label style="margin:0px !important;" class="dropof_date endof_dt_label"> <?php echo 'End Date'; ?> </label>
           
                  <input class="bdatepicker"  type="text" value="<?php echo date('d/m/Y'); echo set_value('drop_date'); ?>" name="enddatefield" placeholder="<?php echo 'End Date'; ?>" style="width: 90px !important;" />
              </div>

              <div class="droptime-bkg" style="text-align:center;">
      
                  <label class="dropof_time endof_time_label" style="margin:0px !important;"><?php echo 'End Time'; ?></label>
          
                  <input   id="endtimepicker" name="endtimefield" type="text" value="<?php echo date('h : i') ?>" style="width:100%;margin-top: 0px !important;" />
              </div>
          </div>
      </div>
      <div class="col-md-6" style="padding-top:40px;padding-left:60px;">
         <div id="return_chairwheel" >
                    <div  style="float:left; margin-right:20px;">
                        <span>Regular </span><input class="regular_check" type="checkbox" name="regularcheckfield" value="1" style="margin:0px 10px;float:none;">
                    </div>
                    <div style="float:left; margin-right:20px;">
                        <span>Return </span><input class="return_car" type="checkbox" name="returncheckfield" value="1" style="margin:0px 10px;float:none;">
                    </div>
                     <div>
                        <span>Wheelchair </span><input class="wheelchair_carssh" type="checkbox" name="wheelcarcheckfield" value="1" style="margin:0px 10px;float:none;">
                    </div>
                    
                    <div class="clearfix"></div>
             </div>
      </div>
             
    </div>
  <div class="row" id="timeslotsrtn"style="display: none;" >
    <div class="col-md-12">
  
      <table cellpadding="0" cellspacing="0" 
      style="width:100%;" align="center" id="timesspantable">
        <tr style="text-align:left;" id="returnpicktimetable">
          <td>&nbsp;</td>
          <td><?php echo 'Monday'; ?></td>
          <td><?php echo 'Tuesday'; ?></td>
          <td><?php echo 'Wednesday'; ?></td>
          <td><?php echo 'Thursday'; ?></td>
          <td><?php echo 'Friday'; ?></td>
          <td><?php echo 'Saturday'; ?></td>
          <td><?php echo 'Sunday'; ?></td>
        </tr>
        <tr class="go-table" style="text-align:left;">
          <td><?php echo 'Pickup Time'; ?></td>
          <td class="weekdays_none_floating">
            <input name="pickmondaycheckfield" class="weekdays_go" type="checkbox" value="1">
            <input  class="wkdays_time_pick " id="go_time_1" name="pickmondayfield" type="text" value="<?php echo date('h : i'); ?>" style="float:none !important;" />
          </td>
          <td class="weekdays_none_floating"><input name="picktuesdaycheckfield" class="weekdays_go" type="checkbox" value="2">
            <input  class="wkdays_time_pick " id="go_time_2" name="picktuesdayfield" type="text" value="<?php echo date('h : i'); ?>" style="float:none !important;"/>
          </td>
          <td class="weekdays_none_floating"><input name="pickwednesdaycheckfield" class="weekdays_go" type="checkbox" value="3">
            <input  class="wkdays_time_pick " id="go_time_3" name="pickwednesdayfield" type="text" value="<?php echo date('h : i'); ?>" style="float:none !important;" />
          </td class="weekdays_none_floating">
          <td class="weekdays_none_floating"><input name="pickthursdaycheckfield" class="weekdays_go" type="checkbox" value="4">
            <input  class="wkdays_time_pick " id="go_time_4" name="pickthursdayfield" type="text" value="<?php echo date('h : i'); ?>"style="float:none !important;" />
          </td>
          <td class="weekdays_none_floating"><input name="pickfridaycheckfield" class="weekdays_go" type="checkbox" value="5">
            <input  class="wkdays_time_pick " id="go_time_5" name="pickfridayfield" type="text" value="<?php echo date('h : i'); ?>" style="float:none !important;" />
          </td>
          <td class="weekdays_none_floating"><input name="picksaturdaycheckfield" class="weekdays_go" type="checkbox" value="6">
            <input  class="wkdays_time_pick " id="go_time_6" name="picksaturdayfield" type="text" value="<?php echo date('h : i'); ?>"style="float:none !important;" />
          </td>
          <td class="weekdays_none_floating"><input name="picksundaycheckfield" class="weekdays_go" type="checkbox" value="7">
            <input  class="wkdays_time_pick " id="go_time_7" name="picksundayfield" type="text" value="<?php echo date('h : i'); ?>"style="float:none !important;" />
          </td>
        </tr>
        <tr class="back-table" style="text-align:left;">
          <td style="width:10%;text-align: left;"><?php echo 'Return Time'; ?></td>
          <td class="weekdays_none_floating">
            <input name="returnmondaycheckfield" class="weekdays_back" type="checkbox" value="1">
            <input  class="wkdays_time_pick " id="back_time_1" name="returnmondayfield" type="text" value="<?php echo date('h : i'); ?>" style="float: none !important; "/>
          </td>
          <td class="weekdays_none_floating">
            <input name="returntuesdaycheckfield" class="weekdays_back" type="checkbox" value="2">
            <input  class="wkdays_time_pick " id="back_time_2" name="returntuesdayfield" type="text" value="<?php echo date('h : i'); ?>" style="float:none !important;" />
          </td>
          <td class="weekdays_none_floating">
            <input name="returnwednesdaycheckfield" class="weekdays_back" type="checkbox" value="3">
            <input  class="wkdays_time_pick " id="back_time_3" name="returnwednesdayfield" type="text" value="<?php echo date('h : i'); ?>" style="float:none !important;"/>
          </td>
          <td class="weekdays_none_floating">
            <input name="returnthursdaycheckfield" class="weekdays_back" type="checkbox" value="4">
            <input  class="wkdays_time_pick " id="back_time_4" name="returnthursdayfield" type="text" value="<?php echo date('h : i'); ?>" style="float:none !important;"/>
          </td>
          <td class="weekdays_none_floating">
            <input name="returnfridaycheckfield" class="weekdays_back" type="checkbox" value="5">
            <input  class="wkdays_time_pick " id="back_time_5" name="returnfridayfield" type="text" value="<?php echo date('h : i'); ?>" style="float:none !important;"/>
          </td>
          <td class="weekdays_none_floating">
            <input name="returnsaturdaycheckfield" class="weekdays_back" type="checkbox" value="6">
            <input  class="wkdays_time_pick " id="back_time_6" name="returnsaturdayfield" type="text" value="<?php echo date('h : i'); ?>" style="float:none !important;"/>
          </td>
          <td class="weekdays_none_floating">
            <input name="returnsundaycheckfield" class="weekdays_back" type="checkbox" value="7">
            <input  class="wkdays_time_pick " id="back_time_7" name="returnsundayfield" type="text" value="<?php echo date('h : i'); ?>" style="float:none !important;"/>
          </td>
        </tr>
      </table>
    </div>
  </div>
  
<?php  include 'wheelchairdiv.php';?> 


     
              

          <div class="row submit-row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary btn-xs right btn-booking-next" style="margin-right: -13px !important;font-weight:normal;">Next <i class="fa fa-arrow-circle-o-right"></i> 
               </button>
            </div>
        </div>
     <?php echo form_close(); ?>
 <script type="text/javascript">
           $(document).ready(function(){
                $('.go-table').hide();
                $('.back-table').hide();
                $('.tmpr_txt').hide();
                $('#timeslotsrtn').hide();
              
                
         

             $('.regular_check').change(function() {
                if($(this).is(":checked")) {
                    $('#timeslotsrtn').show();
                    $('.go-table').show();
                    $(".pickupreturn_datetime_div").hide();    
                    $(".startend_datetime_div").show();     
               
                }else{
                   
                    $('.go-table').hide();
                    $('#timeslotsrtn').hide();
                    $('.back-table').hide();
                    $(".pickupreturn_datetime_div").show();  
                    $(".startend_datetime_div").hide();        
                }
             });
             $('.return_car').change(function() {
               
                 if($(this).is(":checked")) {
                   
                    $(".return_datetime_div").show();
                   
                    if($('.regular_check').is(":checked")){
                         $('#timeslotsrtn').show();
                         $('.back-table').show();
                      }
                  
                }else{
                    
                     $(".return_datetime_div").hide();
                     if($('.regular_check').is(":checked")){
                         $('.back-table').hide();
                      }
                }
                 
              
               
             });
              $('.wheelchair_carssh').change(function() {
                if($(this).is(":checked")) {
                    $('#wheelchairdiv').show();
                     $('#nonwheelchairdiv').hide();
                    document.getElementById("carstypes").value='';
                    document.getElementById("carstypes").value='wheelchaircar';
                }else{
                    $('#nonwheelchairdiv').show();
                     $('#wheelchairdiv').hide();
                      document.getElementById("carstypes").value='';
                      document.getElementById("carstypes").value='normalcar';
                 
                }
             });

            });    
        </script>
    
     <script>
// This sample uses the Autocomplete widget to help the user select a
// place, then it retrieves the address components associated with that
// place, and then it populates the form fields with those details.
// This sample requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script
// src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

var placeSearch, autocomplete;

var componentForm = {

  locality: 'long_name',
  postal_code: 'short_name'
};

function initAutocomplete() {
  // Create the autocomplete object, restricting the search predictions to
  // geographical location types.
  autocomplete = new google.maps.places.Autocomplete(
      document.getElementById('pickupautocomplete'), {types: ['geocode']});
  autodropcomplete = new google.maps.places.Autocomplete(
      document.getElementById('dropoffautocomplete'), {types: ['geocode']});

  // Avoid paying for data that you don't need by restricting the set of
  // place fields that are returned to just the address components.
  autocomplete.setFields(['address_component','geometry']);

  // When the user selects an address from the drop-down, populate the
  // address fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);

  autodropcomplete.setFields(['address_component','geometry']);
  autodropcomplete.addListener('place_changed', fillInDropAddress);


}

function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();
console.log(place);
  
      document.getElementById("pickup_city").value = '';
      document.getElementById("pickup_code").value = '';
      //document.getElementById("pickup_city").disabled = false;
      //document.getElementById("pickup_code").disabled = false;
      document.getElementById('pickloc_lat').value='';
      document.getElementById('pickloc_long').value='';

  // Get each component of the address from the place details,
  // and then fill-in the corresponding field on the form.
  if(place.address_components){
   
     for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];

    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
       if(addressType=="locality"){
          document.getElementById("pickup_city").value = val;
         //document.getElementById("pickup_city").disabled = true;
          
       }
       else if(addressType=="postal_code"){
           document.getElementById("pickup_code").value = val;
           //document.getElementById("pickup_code").disabled = true;
       }
     
    }
  }
  console.log(place);
          document.getElementById('pickloc_lat').value = place.geometry.location.lat();
          document.getElementById('pickloc_long').value = place.geometry.location.lng();
  }
 
}

function fillInDropAddress() {
   // Get the place details from the autocomplete object.
  var place = autodropcomplete.getPlace();
console.log(place);
  
      document.getElementById("dropoff_city").value = '';
      document.getElementById("dropoff_code").value = '';
      //document.getElementById("dropoff_city").disabled = false;
      //document.getElementById("dropoff_code").disabled = false;
      document.getElementById('droploc_lat').value='';
      document.getElementById('droploc_long').value='';

  // Get each component of the address from the place details,
  // and then fill-in the corresponding field on the form.
  if(place.address_components){
   
     for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];

    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
       if(addressType=="locality"){
          document.getElementById("dropoff_city").value = val;
         //document.getElementById("dropoff_city").disabled = true;
          
       }
       else if(addressType=="postal_code"){
           document.getElementById("dropoff_code").value = val;
          // document.getElementById("dropoff_code").disabled = true;
       }
     
    }
  }
  console.log(place);
          document.getElementById('droploc_lat').value = place.geometry.location.lat();
          document.getElementById('droploc_long').value = place.geometry.location.lng();
  }
 
}
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      console.log("geolocation"+geolocation);
      var circle = new google.maps.Circle(
          {center: geolocation, radius: position.coords.accuracy});
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
    </script>
<script type="text/javascript">
  $(document).ready(function(){
 calwaitingtime();

 

  function calwaitingtime() {
  var time1= document.getElementById("timepicker1").value;
  var time2= document.getElementById("waitingtimepicker").value
  var a=time1.split(":");
  var b=time2.split(":");
  var h=parseInt(a[0])+parseInt(b[0]);
  var m=parseInt(a[1])+parseInt(b[1]);
  if(m>=60){
    m=m-60;
    if(m<10){
      m="0"+m; 
    }
    h=h+1;
  }else{
     if(m<10){
      m="0"+m; 
    }
  }

  if(h >= 24){
  
    h=h-24;
    if(h<10){
      h="0"+h;
     }
  }else{
     if(h<10){
      h="0"+h;
     }
  }
  document.getElementById("timepicker2").value = h.toString()+" : "+m.toString();
}
  });
</script>
<script type="text/javascript">
         $(document).ready(function(){
         $(".weekdays_go").click(function(){
            var day = $(this).val();
            var weekdays = $(this);
            console.log(day);
            console.log($(weekdays).is(":checked"));
            if ($(weekdays).is(":checked")) {
                $("#go_time_"+day).show();
            }
            else {
                $("#go_time_"+day).hide();
            }
        });
        
        $(".weekdays_back").click(function(){
            var day = $(this).val();
            var weekdays = $(this);
            console.log(day);
            console.log($(weekdays).is(":checked"));
            if ($(weekdays).is(":checked")) {
                $("#back_time_"+day).show();
            }
            else {
                $("#back_time_"+day).hide();
            }
        });
    });
    </script>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#pickaddresscategorybtn").click(function(){
          document.getElementById("pickupcategory").value='';
          document.getElementById("pickupcategory").value='address';
        });
        $("#pickpackagecategorybtn").click(function(){
          document.getElementById("pickupcategory").value='';
          document.getElementById("pickupcategory").value='package';
        });
        $("#pickairportcategorybtn").click(function(){
          document.getElementById("pickupcategory").value='';
          document.getElementById("pickupcategory").value='airport';
        });
        $("#picktraincategorybtn").click(function(){
          document.getElementById("pickupcategory").value='';
          document.getElementById("pickupcategory").value='train';
        });
        $("#pickhotelcategorybtn").click(function(){
          document.getElementById("pickupcategory").value='';
          document.getElementById("pickupcategory").value='hotel';
        });
        $("#pickparkcategorybtn").click(function(){
          document.getElementById("pickupcategory").value='';
          document.getElementById("pickupcategory").value='park';
        });

          $("#dropaddresscategorybtn").click(function(){
          document.getElementById("dropoffcategory").value='';
          document.getElementById("dropoffcategory").value='address';
        });
        $("#droppackagecategorybtn").click(function(){
          document.getElementById("dropoffcategory").value='';
          document.getElementById("dropoffcategory").value='package';
        });
        $("#dropairportcategorybtn").click(function(){
          document.getElementById("dropoffcategory").value='';
          document.getElementById("dropoffcategory").value='airport';
        });
        $("#droptraincategorybtn").click(function(){
          document.getElementById("dropoffcategory").value='';
          document.getElementById("dropoffcategory").value='train';
        });
        $("#drophotelcategorybtn").click(function(){
          document.getElementById("dropoffcategory").value='';
          document.getElementById("dropoffcategory").value='hotel';
        });
        $("#dropparkcategorybtn").click(function(){
          document.getElementById("dropoffcategory").value='';
          document.getElementById("dropoffcategory").value='park';
        });
      });
    </script>
 
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDHfyx2yLyj13Aiu5S29REC5GuKtbqzd6A&libraries=places&callback=initAutocomplete"
        async defer></script>

        <script type="text/javascript">
          $(document).ready(function() {
          $('form#booking_form').submit(function(e) {
            var level=0;
            var service=document.getElementById("pickserviceinput").value;
            var servicecategory=document.getElementById("pickservicecategoryinput").value;
            var val= document.getElementById("pickupcategory").value;
             var dropval= document.getElementById("dropoffcategory").value;
            if(val=="address"){
               var address=document.getElementById("pickupautocomplete").value;
               if(address==""){
                alert("please enter address");
                 e.preventDefault();
               }else{
                level=1;
               } 

            }
            else if(val=="package"){
               var package=document.getElementById("pickpackagefield").value;
               if(package==""){
                alert("please select package");
                 e.preventDefault();
               }else{
                level=1;
               }     
            }
             else if(val=="airport"){
               var airport=document.getElementById("airport_pick").value;
               if(airport==""){
                alert("please select airport");
                 e.preventDefault();
               }else{
                level=1;
               }     
            }

             else if(val=="train"){
               var train=document.getElementById("train_pick").value;
               if(train==""){
                alert("please select train");
                 e.preventDefault();
               }else{
                level=1;
               }     
            }

             else if(val=="hotel"){
               var hotel=document.getElementById("hotel_pick").value;
               if(hotel==""){
                alert("please select hotel");
                 e.preventDefault();
               }else{
                level=1;
               }     
            }
             else if(val=="park"){
               var park=document.getElementById("park_pick").value;
               if(park==""){
                alert("please select park");
                 e.preventDefault();
               }else{
                level=1;
               }     
            }
            if(level==1){
               if(service==""){
                alert("please select service");
                 e.preventDefault();
               }
               else if(servicecategory==""){
                alert("please select service category");
                 e.preventDefault();
               }else{
                level=2;
               }

            }
            if(level==2){
               if(dropval=="address"){
               var address=document.getElementById("dropoffautocomplete").value;
               if(address==""){
                alert("please enter address");
                 e.preventDefault();
               }    
            }
            else if(dropval=="package"){
               var package=document.getElementById("droppackagefield").value;
               if(package==""){
                alert("please select package");
                 e.preventDefault();
               }    
            }
             else if(dropval=="airport"){
               var airport=document.getElementById("airport_drop").value;
               if(airport==""){
                alert("please select airport");
                 e.preventDefault();
               }    
            }

             else if(dropval=="train"){
               var train=document.getElementById("train_drop").value;
               if(train==""){
                alert("please select train");
                 e.preventDefault();
               }    
            }

             else if(dropval=="hotel"){
               var hotel=document.getElementById("hotel_drop").value;
               if(hotel==""){
                alert("please select hotel");
                 e.preventDefault();
               }    
            }
             else if(dropval=="park"){
               var park=document.getElementById("park_drop").value;
               if(park==""){
                alert("please select park");
                 e.preventDefault();
               }    
            }
            }

             
           
                
              
          



          });
           });

        
        </script>
