 <?php   
$displaypassengers=$this->session->userdata['passengers'];

$ispickmonday='0';
$ispicktuesday='0';
$ispickwednesday='0'; 
$ispickthursday='0';
$ispickfriday='0';
$ispicksaturday='0';
$ispicksunday='0';

$pickmondaytime=date('h : i');
$picktuesdaytime=date('h : i');
$pickwednesdaytime=date('h : i');
$pickthursdaytime=date('h : i');
$pickfridaytime=date('h : i');
$picksaturdaytime=date('h : i');
$picksundaytime=date('h : i');


$isreturnmonday='0';
$isreturntuesday='0';
$isreturnwednesday='0'; 
$isreturnthursday='0';
$isreturnfriday='0';
$isreturnsaturday='0';
$isreturnsunday='0';

$returnmondaytime=date('h : i');
$returntuesdaytime=date('h : i');
$returnwednesdaytime=date('h : i');
$returnthursdaytime=date('h : i');
$returnfridaytime=date('h : i');
$returnsaturdaytime=date('h : i');
$returnsundaytime=date('h : i');

$pickairportarrivaltime=(empty($dbpickairportdata->arrivaltime))?date('h : i'):$dbpickairportdata->arrivaltime;
$picktrainarrivaltime=(empty($dbpicktraindata->arrivaltime))?date('h : i'):$dbpicktraindata->arrivaltime;
$dropairportarrivaltime=(empty($dbdropairportdata->arrivaltime))?date('h : i'):$dbdropairportdata->arrivaltime;
$droptrainarrivaltime=(empty($dbdroptraindata->arrivaltime))?date('h : i'):$dbdroptraindata->arrivaltime;


$start_date=date('d/m/Y');
$end_date=date('d/m/Y');
$pick_date=date('d/m/Y');
$return_date=date('d/m/Y');

$start_time=date('h : i');
$end_time=date('h : i');
$pick_time=date('h : i');
$return_time=date('h : i');
$waiting_time=date('h : i');

$client_id=$dbbookingdata->user_id;
$driver_id=$dbbookingdata->driver_id;
$pick_status="1";
$drop_status="1";
$servicecat_id=$dbbookingdata->service_category_id;
$packagedepart_id=$dbbookingdata->pickuppackage_id;
$packagedestincation_id=$dbbookingdata->droppackage_id;
$pickairportpoiid=$dbpickairportdata->poi_id;
$picktrainpoiid=$dbpicktraindata->poi_id;
$pickparkpoiid=$dbbookingdata->pickuppark_id;
$pickhotelpoiid=$dbbookingdata->pickuphotel_id;
$dropairportpoiid=$dbdropairportdata->poi_id;
$droptrainpoiid=$dbdroptraindata->poi_id;
$dropparkpoiid=$dbbookingdata->droppark_id;
$drophotelpoiid=$dbbookingdata->drophotel_id;
$pick_form =  $this->session->flashdata('pick_form');
$drop_form =  $this->session->flashdata('drop_form');
$service_form =  $this->session->flashdata('service_form');
$car_name=$this->bookings_model->getcarname($dbbookingdata->car_id);
$client=$this->bookings_model->getclientdatabyid($dbbookingdata->user_id);
$servicedata= $this->bookings_model->getservicesbyid('vbs_u_service', $dbbookingdata->service_id);
$vatdata= $this->bookings_model->getservicesbyid('vbs_u_vat', $servicedata->tva);
$vatname=$vatdata->name_of_var;

     
      $pickairportpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$pickairportpoiid);
      $picktrainpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$picktrainpoiid);
      $pickhotelpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$pickhotelpoiid);
      $pickparkpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$pickparkpoiid);


        
      $dropairportpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$dropairportpoiid);
      $droptrainpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$droptrainpoiid);
      $drophotelpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$drophotelpoiid);
      $dropparkpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$dropparkpoiid);

      
       $packagedata=$this->bookings_model->poidatarecord('vbs_u_package',$packagedepart_id);
       $packagepoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$packagedata['departure']);
       $poicategoryrecord=$this->bookings_model->poidatarecord('vbs_u_ride_category',$packagepoirecord['category_id']);


       $droppackagedata=$this->bookings_model->poidatarecord('vbs_u_package',$packagedestincation_id);
       $droppoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$droppackagedata['destination']);
       $poicategoryrecord=$this->bookings_model->poidatarecord('vbs_u_ride_category',$droppoirecord['category_id']);
             
               

       if(!empty($dbbookingdata->start_date) || $dbbookingdata->start_date != null){ 
         $time = strtotime($dbbookingdata->start_date);
         $newformat = date('d/m/Y',$time);
         $start_date= $newformat;
       }
       if(!empty($dbbookingdata->end_date) || $dbbookingdata->end_date != null){ 
         $time = strtotime($dbbookingdata->end_date);
         $newformat = date('d/m/Y',$time);
         $end_date= $newformat;
       }
       if(!empty($dbbookingdata->pick_date) || $dbbookingdata->pick_date != null){ 
         $time = strtotime($dbbookingdata->pick_date);
         $newformat = date('d/m/Y',$time);
         $pick_date= $newformat;
       }
       if(!empty($dbbookingdata->return_date) || $dbbookingdata->return_date != null){ 
         $time = strtotime($dbbookingdata->return_date);
         $newformat = date('d/m/Y',$time);
         $return_date= $newformat;
       }

       foreach($dbpickregulardata as $regular){
        if(strtolower($regular->day)=='monday'){
             $ispickmonday='1';
             $pickmondaytime=$regular->time;
           }
          elseif(strtolower($regular->day)=='tuesday'){
             $ispicktuesday='1';
             $picktuesdaytime=$regular->time;
           }
          elseif(strtolower($regular->day)=='wednesday'){
             $ispickwednesday='1';
             $pickwednesdaytime=$regular->time;
           }
          elseif(strtolower($regular->day)=='thursday'){
             $ispickthursday='1';
             $pickthursdaytime=$regular->time;
           }
          elseif(strtolower($regular->day)=='friday'){
             $ispickfriday='1';
             $pickfridaytime=$regular->time;
           }
          elseif(strtolower($regular->day)=='saturday'){
             $ispicksaturday='1';
             $picksaturdaytime=$regular->time;
           }
          elseif(strtolower($regular->day)=='sunday'){
             $ispicksunday='1';
             $picksundaytime=$regular->time;
           }         
      }
    foreach($dbreturndata as $return){
      if(strtolower($return->day)=='monday'){
           $isreturnmonday='1';
           $returnmondaytime=$return->time;
         }
        elseif(strtolower($return->day)=='tuesday'){
           $isreturntuesday='1';
           $returntuesdaytime=$return->time;
         }
        elseif(strtolower($return->day)=='wednesday'){
           $isreturnwednesday='1';
           $returnwednesdaytime=$return->time;
         }
        elseif(strtolower($return->day)=='thursday'){
           $isreturnthursday='1';
           $returnthursdaytime=$return->time;
         }
        elseif(strtolower($return->day)=='friday'){
           $isreturnfriday='1';
           $returnfridaytime=$return->time;
         }
        elseif(strtolower($return->day)=='saturday'){
           $isreturnsaturday='1';
           $returnsaturdaytime=$return->time;
         }
        elseif(strtolower($return->day)=='sunday'){
           $isreturnsunday='1';
           $returnsundaytime=$return->time;
         }
    }
    if(!empty($dbbookingdata->start_time) || $dbbookingdata->start_time != null){   
       $start_time= $dbbookingdata->start_time;
     }
     if(!empty($dbbookingdata->end_time) || $dbbookingdata->end_time != null){  
       $end_time= $dbbookingdata->end_time;
     }
     if(!empty($dbbookingdata->pick_time) || $dbbookingdata->pick_time != null){    
       $pick_time= $dbbookingdata->pick_time;
     }
     if(!empty($dbbookingdata->return_time) || $dbbookingdata->return_time != null){  
       $return_time= $dbbookingdata->return_time;
     }
     if(!empty($dbbookingdata->waiting_time) || $dbbookingdata->waiting_time != null){   
       $waiting_time= $dbbookingdata->waiting_time;
     }
  $bookingstatus="Pending";
   if($dbbookingdata->is_conformed=='0'){
     $bookingstatus="Pending";
   }
   elseif($dbbookingdata->is_conformed=='1'){
    $bookingstatus="Confirmed";
   }
   elseif($dbbookingdata->is_conformed=='2'){
     $bookingstatus="Cancelled";
   }

   
?>
  <div class="col-md-12 online">
    <div id="bookinginformationlistdiv">
        <div class="col-md-7">
            <div class="row">
              <div class="col-md-12 pdz">
                <div class="col-md-10 pdz">
                  <div class="col-md-3 pdz">
                  <span><strong>Statut : </strong><?=  $bookingstatus ?></span>
                </div>
                  <div class="col-md-3 pdz">
                  <span><strong>Date : </strong><?= from_unix_date($dbbookingdata->bookdate); ?></span>
                </div>
                  <div class="col-md-3 pdz">
                  <span><strong>Time : </strong><?= $dbbookingdata->booktime; ?></span>
                </div>
                 <div class="col-md-3 pdz">
                  <span><strong>Since : </strong><?=timeDiff($dbbookingdata->last_action);?></span>
                </div>

                </div>
              </div>
              <div class="col-md-12" style="padding: 0px;">
               <div class="col-md-6" style="padding: 0px">
              
                <p style="font-size:13px !important;"><span style="margin-right:5px;font-weight: 900;">Client : </span><br>
                  <?php echo "<span style='font-weight:900;'>".$client->civility.' '.$client->first_name.' '.$client->last_name.'</span><br>'.$client->address.'<br>';?>
                 <?php if(!empty($client->address1) || $client->address1 != null):  ?>
                  <?php echo $client->address1.'<br>'; ?>
                 <?php endif; ?>
                 <?php echo $client->zipcode.'  '.$client->city;?></p>
               </div> 
            <div class="col-md-6" style="padding: 0px">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xs-12" style="padding:0px;">
                <ul>
                  <li class="text-left" style="font-weight: 900;">Passengers :</li>
                </ul>
                   <ul class="ulbdnone" id="editpassengerdiv" style="list-style-type:none;padding-left:0px;">
                     <?php foreach($displaypassengers as $passenger): ?>
                       <li class="text-left" style="position: relative;"><span class="fa fa-user" style="margin-right:5px;"></span><span><?php echo $passenger['name'];?></span>, <span class="fa fa-mobile" style="margin-right:5px;"></span><span><?php echo $passenger['phone'];?></span>, <span class="fa fa-phone" style="margin-right:5px;"></span><span><?php echo $passenger['home'];?></span>, <span class="fa fa-wheelchair" style="margin-right:5px;"></span><span><?= $passenger['disable'] ?></span>
                       
                          
                      </li>

                      <?php endforeach; ?>   
                       
                   </ul>
             
              </div>
            </div>                       
            </div>
                           
                        
<div class="col-md-12" style="padding:0px;">

  <div class="col-md-6" style="padding:0px;">
    <ul class="ulbdnone" >
    <li class="text-left" ><strong>Service Category : </strong>
      <?php  
     $servicecategory= $this->bookings_model->getservicesbyid('vbs_u_category_service', $dbbookingdata->service_category_id);
       echo $servicecategory->category_name;
       ?></li>
      </ul>
    <ul class="ulbdnone bts-1">
    <li style="border-bottom:0px;">
        <strong>Pick Up Category : </strong><?php echo ucwords($dbbookingdata->pickupcategory);?>
        <ul class="ulbdnone  clearfix" >             
        
          <?php if($dbbookingdata->pickupcategory == "address"): ?>
            <?php
               $pickshortaddress='';
               $pickshortaddress=$dbpickaddressdata->address;
               $pickshortaddress= str_replace(($dbpickaddressdata->city), "", $pickshortaddress);
               $pickshortaddress= str_replace(($dbpickaddressdata->zipcode), "",  $pickshortaddress);
               $pickshortaddress=str_ireplace("france","",$pickshortaddress);
               $pickshortaddress=rtrim($pickshortaddress,' ');
               $pickshortaddress=rtrim($pickshortaddress,',');
               $pickshortaddress = preg_replace('!\s+!', ' ', $pickshortaddress);
             ?>
         
        <li class="text-left"><strong>Complément d'adresse : </strong><?php echo $dbpickaddressdata->otheraddress; ?></li>
        
        <li class="text-left"><strong>Addresse : </strong><?php echo $pickshortaddress;?></li>
        <li class="text-left" style="float:left;border:0px;padding:0px;"><strong>Zip Code : </strong><?php echo $dbpickaddressdata->zipcode ;?> </li>
         <li class="text-left" style="float:left;border:0px;padding:0px 10px;"><strong>City : </strong><?php echo $dbpickaddressdata->city ; ?></li>
         
        
         <?php elseif($dbbookingdata->pickupcategory == "package"): ?>
         <li class="text-left"><strong>Name : </strong><?= $packagedata['name']; ?></li>
         <li class="text-left"><strong>Addresse : </strong><?= $packagepoirecord['address'];  ?></li>
          <li class="text-left" style="float:left;border:0px;padding:0px;"><strong>Zip Code : </strong><?= $packagepoirecord['postal']; ?></li>
         <li class="text-left" style="float:left;border:0px;padding:0px 10px;"><strong>City : </strong><?= $packagepoirecord['ville']; ?></li>

         <?php elseif($dbbookingdata->pickupcategory == "airport"): ?>
         <li class="text-left"><strong>Name : </strong> <?= $pickairportpoirecord['name'] ?></li>
          <li class="text-left"><strong>Flight Number : </strong><?= $dbpickairportdata->flightnumber; ?>
           <strong style="margin-left: 5px;">Arrival Time : </strong><?= $dbpickairportdata->arrivaltime ?></li>
         <li class="text-left"><strong>Addresse : </strong><?= $pickairportpoirecord['address'] ?></li>
          <li class="text-left" style="float:left;border:0px;padding:0px;"><strong>Zip Code : </strong><?= $pickairportpoirecord['postal']; ?></li>
         <li class="text-left" style="float:left;border:0px;padding:0px 10px;"><strong>City : </strong><?= $pickairportpoirecord['ville']; ?></li>

         <?php elseif($dbbookingdata->pickupcategory == "train"): ?>
         <li class="text-left"><strong>Name : </strong><?= $picktrainpoirecord['name'] ?></li>
         <li class="text-left"><strong>Train Number : </strong><?= $dbpicktraindata->trainnumber ?>
         <strong style="margin-left: 5px;">Arrival Time : </strong><?= $dbpicktraindata->arrivaltime ?></li>
         <li class="text-left"><strong>Addresse : </strong><?= $picktrainpoirecord['address'] ?></li>
          <li class="text-left" style="float:left;border:0px;padding:0px;"><strong>Zip Code : </strong><?= $picktrainpoirecord['postal']; ?></li>
         <li class="text-left" style="float:left;border:0px;padding:0px 10px;"><strong>City : </strong><?= $picktrainpoirecord['ville']; ?></li>
         
          <?php elseif($dbbookingdata->pickupcategory == "hotel"): ?>
         <li class="text-left"><strong>Name : </strong><?= $pickhotelpoirecord['name'] ?></li>
         <li class="text-left"><strong>Addresse : </strong><?= $pickhotelpoirecord['address'] ?></li>
          <li class="text-left" style="float:left;border:0px;padding:0px;"><strong>Zip Code : </strong><?= $pickhotelpoirecord['postal']; ?></li>
         <li class="text-left" style="float:left;border:0px;padding:0px 10px;"><strong>City : </strong><?= $pickhotelpoirecord['ville']; ?></li>
          <?php elseif($dbbookingdata->pickupcategory == "park"): ?>
         <li class="text-left"><strong>Name : </strong><?= $pickparkpoirecord['name'] ?></li>
         <li class="text-left"><strong>Addresse : </strong><?= $pickparkpoirecord['address'] ?></li>
          <li class="text-left" style="float:left;border:0px;padding:0px;"><strong>Zip Code : </strong><?= $pickparkpoirecord['postal']; ?></li>
         <li class="text-left" style="float:left;border:0px;padding:0px 10px;"><strong>City : </strong><?= $pickparkpoirecord['ville']; ?></li>
        <?php endif; ?>
        
        
       </ul>
    </li>
    </ul>
      
    <?php
     $car_configuration_data= $this->bookings_model->booking_Configuration('vbs_car_configuration',['car_id' => $dbbookingdata->car_id]);
     ?>
   
    
        
   </div>
   <div class="col-md-6" style="padding:0px;">
    <ul class="ulbdnone" >
       <li class="text-left"><strong>Service : </strong>
      <?php
     $service= $this->bookings_model->getservicesbyid('vbs_u_service', $dbbookingdata->service_id);
     echo $service->service_name;
       ?>
      </li>
    </ul>
  <ul  class="ulbdnone" >
    <li style="border-bottom:0px;">
        <strong >Drop Off Category : </strong><?php echo ucwords($dbbookingdata->dropoffcategory);?>
        <ul  class="ulbdnone clearfix">             
       
          <?php if($dbbookingdata->dropoffcategory=="address"): ?>
          <?php 
             $dropshortaddress='';
             $dropshortaddress=$dbdropaddressdata->address;
             $dropshortaddress= str_replace(($dbdropaddressdata->city), "", $dropshortaddress);
             $dropshortaddress= str_replace(($dbdropaddressdata->zipcode), "",  $dropshortaddress);
              $dropshortaddress=str_ireplace("france","",$dropshortaddress);
               $dropshortaddress=rtrim($dropshortaddress,' ');
               $dropshortaddress=rtrim($dropshortaddress,',');
             $dropshortaddress = preg_replace('!\s+!', ' ', $dropshortaddress);


          ?>
                <li class="text-left"><strong>Complément d'adresse : </strong><?php echo $dbdropaddressdata->otheraddress; ?></li>
          
         <li class="text-left"><strong>Addresse : </strong><?php echo $dropshortaddress;?></li>
         <li class="text-left" style="float:left;border:0px;padding:0px;"><strong>Zip Code : </strong><?php echo $dbdropaddressdata->zipcode;?></li>
        <li class="text-left" style="float:left;border:0px;padding:0px 10px;"><strong>City : </strong><?php echo $dbdropaddressdata->city;?></li>
       
         <?php elseif($dbbookingdata->dropoffcategory=="package"): ?>
         <li class="text-left"><strong>Name : </strong><?= $droppackagedata['name']; ?></li>
         <li class="text-left"><strong>Addresse : </strong><?= $droppoirecord['address'];  ?></li>
        <li class="text-left" style="float:left;border:0px;padding:0px;"><strong>Zip Code : </strong> <?= $droppoirecord['postal'];  ?></li>
        <li class="text-left" style="float:left;border:0px;padding:0px 10px;"><strong>City : </strong><?= $droppoirecord['ville'];  ?></li>

         <?php elseif($dbbookingdata->dropoffcategory=="airport"): ?>
         <li class="text-left"><strong>Name : </strong><?= $dropairportpoirecord['name']; ?></li>
         <li class="text-left"><strong>Flight Number : </strong><?= $dbdropairportdata->flightnumber; ?>
         <strong style="margin-left: 5px;">Arrival Time : </strong><?= $dbdropairportdata->arrivaltime ?></li>
         <li class="text-left"><strong>Addresse : </strong><?= $dropairportpoirecord['address'];  ?></li>
         <li class="text-left" style="float:left;border:0px;padding:0px;"><strong>Zip Code : </strong> <?= $dropairportpoirecord['postal'];  ?></li>
        <li class="text-left" style="float:left;border:0px;padding:0px 10px;"><strong>City : </strong><?= $dropairportpoirecord['ville'];  ?></li>

         <?php elseif($dbbookingdata->dropoffcategory=="train"): ?>
         <li class="text-left"><strong>Name : </strong><?= $droptrainpoirecord['name']; ?></li>
           <li class="text-left"><strong>Train Number : </strong><?= $dbdroptraindata->trainnumber; ?>
           <strong style="margin-left: 5px;">Arrival Time : </strong><?= $dbdroptraindata->arrivaltime; ?></li>
         <li class="text-left"><strong>Addresse : </strong><?= $droptrainpoirecord['address'];  ?></li>
         <li class="text-left" style="float:left;border:0px;padding:0px;"><strong>Zip Code : </strong> <?= $droptrainpoirecord['postal'];  ?></li>
        <li class="text-left" style="float:left;border:0px;padding:0px 10px;"><strong>City : </strong><?= $droptrainpoirecord['ville'];  ?></li>
          <?php elseif($dbbookingdata->dropoffcategory=="hotel"): ?>
         <li class="text-left"><strong>Name : </strong><?= $drophotelpoirecord['name']; ?></li>
         <li class="text-left"><strong>Addresse : </strong><?= $drophotelpoirecord['address'];  ?></li>
         <li class="text-left" style="float:left;border:0px;padding:0px;"><strong>Zip Code : </strong> <?= $drophotelpoirecord['postal'];  ?></li>
        <li class="text-left" style="float:left;border:0px;padding:0px 10px;"><strong>City : </strong><?= $drophotelpoirecord['ville'];  ?></li>
          <?php elseif($dbbookingdata->dropoffcategory=="park"): ?>
         <li class="text-left"><strong>Name : </strong><?= $dropparkpoirecord['name']; ?></li>
         <li class="text-left"><strong>Addresse : </strong><?= $dropparkpoirecord['address'];  ?>></li>
         <li class="text-left" style="float:left;border:0px;padding:0px;"><strong>Zip Code : </strong> <?= $dropparkpoirecord['postal'];  ?></li>
        <li class="text-left" style="float:left;border:0px;padding:0px 10px;"><strong>City : </strong><?= $dropparkpoirecord['ville'];  ?></li>
        <?php endif; ?>
      
       </ul>
    </li>
    </ul>
           
   </div>
 </div>
      <?php if(count($dbstopsdata)>0): ?>   
  <div class="col-md-12" style="padding: 0px;">
   
      <ul class="ulbdnone bts-1">             
         <li class="text-left" style="border-bottom:0px;"><strong>Stops : </strong>
          <?php foreach ($dbstopsdata as $stop): ?>
          <ul class="ulbdnone clearfix">
             <li class="text-left" style="width:100%;"><span style="display: inline-block;width:50%;"><strong>Addresse : </strong><?php echo  $stop->stopaddress;?></span><span style="display: inline-block;width:50%;"><strong>Waiting Time : </strong><?php 
             $waitarray = explode(":", $stop->stopwaittime);
             if($waitarray[0] > 1 && $waitarray[1] > 1){
               echo $waitarray[0]." hours ".$waitarray[1]." minutes";
             }
             elseif($waitarray[0] < 2 && $waitarray[1] < 2){
               echo $waitarray[0]." hour ".$waitarray[1]." minute";
             }
             elseif($waitarray[0] > 1 && $waitarray[1] < 2){
               echo $waitarray[0]." hours ".$waitarray[1]." minute";
             }
             elseif($waitarray[0] < 2 && $waitarray[1] > 1){
               echo $waitarray[0]." hour ".$waitarray[1]." minutes";
             }
            
              ?></span></li>
          </ul>
        <?php endforeach; ?>
      
         </li>
       </ul> 
  
 </div>     
   <?php endif; ?>
   <div class="col-md-12" style="padding: 0px;border-bottom:1px solid #ececec;">
    <?php if($dbbookingdata->regular != 1):?>
     <div class="col-md-6" style="padding:0px;">
        <ul class="ulbdnone clearfix" >             
        <li class="text-left" style="float: left;padding:0px !important;border:0px;"><strong >Pick Up Date : </strong><?php echo  $pick_date; ?></li>
         <li class="text-left"  style="float: left;padding: 0px !important;margin-left: 13px;border:0px;"><strong>Pick Up Time : </strong><?php echo  $pick_time; ?></li>
       </ul> 
     </div>
     <?php endif;?>
      <div class="col-md-6" style="padding:0px;">
                 <div  style="float:left; margin-right:5px;">
                    
                        <span><strong>Reccurent : </strong></span><?php echo ($dbbookingdata->regular == 1)?"Yes":'No';  ?>,
                    
                    </div>

                    <div style="float:left; margin-right:5px;">
                       
                        <span><strong>Return : </strong></span> <?php echo ($dbbookingdata->returncheck ==1)?"Yes":'No';  ?>,
                    
                    </div>
                     <div style="float:left; margin-right:5px;">
                     
                        <span><strong>Wheelchair : </strong> </span> <?php echo ($dbbookingdata->wheelchair == 1)?"Yes":'No';  ?>
                     
                    </div>
             </div>
        
   </div>
 
          <?php if($dbbookingdata->returncheck == 1 && $dbbookingdata->regular != 1 ):?>
     <div class="col-md-12" style="padding:0px;">     
        <ul class="ulbdnone clearfix datetimequotediv">             
            <li class="text-left"  ><strong>Waiting Time : </strong>
              <?php
                $waitarray=explode(":", $waiting_time);
                if($waitarray[0] > 1 && $waitarray[1] > 1){
                   echo $waitarray[0]." hours and ".$waitarray[1]." minutes ";
                 }
                 elseif($waitarray[0] > 1 && $waitarray[1] <= 1){
                   echo $waitarray[0]." hours and ".$waitarray[1]." minute ";
                 }
                  elseif($waitarray[0] <= 1 && $waitarray[1] > 1){
                   echo $waitarray[0]." hour and ".$waitarray[1]." minutes ";
                 }
                 else{
                  echo $waitarray[0]." hour and ".$waitarray[1]." minute ";
                 }
               
                        ?></li>
            <li class="text-left"><strong>Return Date : </strong><?php echo $return_date; ?></li>
            <li class="text-left"><strong>Return Time : </strong><?php echo $return_time; ?></li>
   
       </ul>
    </div>
       <?php endif; ?>
   <?php if($dbbookingdata->regular == 1):?>
             <div class="col-md-12 bbts-1">
              <div class="col-md-6" style="padding:0px;">
                  <span><strong>Start Date : </strong><?php echo  $start_date; ?>, </span>
                    <span><strong>Start Time : </strong><?php echo  $start_time; ?> </span>
               </div>     
                <div class="col-md-6" style="padding:0px;">
                    <span><strong>End Date : </strong><?php echo  $end_time; ?>, </span>
                  <span><strong>End Time : </strong><?php echo  $end_time; ?></span>
                </div>
                </div>
    <div class="col-md-12" style="padding: 0px;">
      <table cellpadding="0" cellspacing="0" 
      style="width:100%;" align="center" id="timesspantable">
        <tr  class="quotepicktimecol fw-900">
          <td>&nbsp;</td>
        
         
                          
           
              <td>Monday</td>
           
              <td>Tuesday</td>
          
               <td>Wednesday</td>
           
       
              <td>Thursday</td>
        
         
               <td>Friday</td>
       
        
              <td>Saturday</td>
          
               <td>Sunday</td>
            

          
          
         
          
         
          
         
        </tr>
        <tr class="quotepicktimecol">
          <td style="font-size: 11px;font-weight: 900;text-align: left;">PickUp Time</td>
            <?php if($ispickmonday!='0'): ?>
              <td> <?php echo $pickmondaytime;?> </td>
            <?php else: ?>  
              <td>No Ride</td>                 
            <?php endif; ?>
            <?php if($ispicktuesday!='0'): ?>
              <td> <?php echo $picktuesdaytime;?> </td>
            <?php else: ?>  
              <td>No Ride</td>                 
            <?php endif; ?>
            <?php if($ispickwednesday!='0'): ?>
              <td> <?php echo $pickwednesdaytime;?> </td>
            <?php else: ?>  
              <td>No Ride</td>                 
            <?php endif; ?>
            <?php if($ispickthursday!='0'): ?>
              <td> <?php echo $pickthursdaytime;?> </td>
            <?php else: ?>  
              <td>No Ride</td>                 
            <?php endif; ?>
            <?php if($ispickfriday!='0'): ?>
              <td> <?php echo $pickfridaytime;?> </td>
            <?php else: ?>  
              <td>No Ride</td>                 
            <?php endif; ?>
            <?php if($ispicksaturday!='0'): ?>
              <td> <?php echo $picksaturdaytime;?> </td>
            <?php else: ?>  
              <td>No Ride</td>                 
            <?php endif; ?>
            <?php if($ispicksunday!='0'): ?>
              <td> <?php echo $picksundaytime;?> </td>
            <?php else: ?>  
              <td>No Ride</td>                 
            <?php endif; ?>
         
        </tr>
          <?php if($dbbookingdata->returncheck ==1):?>
        <tr class="quotepicktimecol">
          <td style="font-size: 11px;font-weight: 900;text-align: left;">Return Time</td>
            <?php if($isreturnmonday!='0'): ?>
              <td> <?php echo $returnmondaytime;?> </td>
            <?php else: ?>  
              <td>No Ride</td>                 
            <?php endif; ?>
            <?php if($isreturntuesday!='0'): ?>
              <td> <?php echo $returntuesdaytime;?> </td>
            <?php else: ?>  
              <td>No Ride</td>                 
            <?php endif; ?>
            <?php if($isreturnwednesday!='0'): ?>
              <td> <?php echo $returnwednesdaytime;?> </td>
            <?php else: ?>  
              <td>No Ride</td>                 
            <?php endif; ?>
            <?php if($isreturnthursday!='0'): ?>
              <td> <?php echo $returnthursdaytime;?> </td>
            <?php else: ?>  
              <td>No Ride</td>                 
            <?php endif; ?>
            <?php if($isreturnfriday!='0'): ?>
              <td> <?php echo $returnfridaytime;?> </td>
            <?php else: ?>  
              <td>No Ride</td>                 
            <?php endif; ?>
            <?php if($isreturnsaturday!='0'): ?>
              <td> <?php echo $returnsaturdaytime;?> </td>
            <?php else: ?>  
              <td>No Ride</td>                 
            <?php endif; ?>
            <?php if($isreturnsunday!='0'): ?>
              <td> <?php echo $returnsundaytime;?> </td>
            <?php else: ?>  
              <td>No Ride</td>                 
            <?php endif; ?>
        </tr>
       <?php endif; ?>
      </table>
    </div>
     <?php endif; ?>
<div class="col-md-12" style="padding:0px;">
   <div class="col-md-6" style="padding:0px;">
     <ul class="ulbdnone"  id="carquotestab">
    <li style="border-bottom:0px;">
        <strong>Car Category : </strong><?php echo $car_name  ?>
        <ul class="ulbdnone"  >   
          
          <?php
          $configno=0;
           foreach ($car_configuration_data as $config): 
            $configno++;
            ?>     
         <li class="text-left">
          <span><strong>Configuration <?= $configno ?> :  </strong></span>
          <span><?php echo $config->passengers;?><strong> Passengers,</strong></span>
          <span><?php echo $config->luggage;?><strong> Luggage,</strong></span>
          <span><?php echo $config->wheelchairs;?><strong> Wheelchairs,</strong></span>
          <span><?php echo $config->baby;?><strong> Babys,</strong></span>
          <span><?php echo $config->animals;?><strong> Animals</strong></span>
        </li>
        
           <?php endforeach; ?> 
       
       </ul>
    </li>
    </ul>
   </div> 
     
       </div>  
<div class="col-md-12 pdz">
<!-- Reminder Notification Custom Reminder  Section -->
     <div class="col-md-6 pdz">

           <!-- Notification -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pdz">
          <div class="col-md-12 pdz" style="font-weight: 900;">Notification : </div>
            <div class="col-md-12 pdz"> 
              
               <div class="col-md-2 pdz text-center" style="font-weight: 900;width: 25%;">Date</div>
               <div class="col-md-2 pdz text-center" style="font-weight: 900;width: 25%;">Time</div>
               <div class="col-md-2 pdz text-center" style="font-weight: 900;width: 25%;">File</div>
               <div class="col-md-2 pdz text-center" style="font-weight: 900;width: 25%;">Statut</div>
              
            </div>
           

          <div class="col-md-12 pdz">
            <?php foreach ($dbbooknotifications as $notification): ?>
            <div class="col-md-12 pdz">
                              
                 <div class="col-md-2 pdz text-center" style="width: 25%;"><?= from_unix_date($notification->date); ?></div>
                 <div class="col-md-2 pdz text-center" style="width: 25%;"><?= $notification->time ?></div>
                 <div class="col-md-2 pdz text-center" style="width: 25%;">
                             <a href="<?= base_url(); ?>admin/bookings/notification_pdf/<?= $notification->id  ?>/<?= $dbbookingdata->id ?>" target="_blank">
                               <img src="<?= base_url(); ?>assets/theme/default/images/pdf-icon1.png" width="20" height="20"  alt="Submit Form">
                             </a>
                 </div>
                 <div class="col-md-2 pdz text-center" style="width: 25%;"><?= ($notification->issent == '1')?"Sent":"Pending"; ?></div>
               
             
            </div>    
             <?php endforeach; ?>
          </div>
        </div>
          <!-- Notification -->
          <!-- Custom Reminders -->
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pdz" style="margin-top: 15px;">
               <div class="col-md-12 pdz" style="font-weight: 900;">Custom Reminders : </div>
               <div class="col-md-12 pdz" id="customreminderrecorded">
                 <div class="col-md-12 pdz">
                  <div class="col-md-3 pdz" style="text-align: center;font-weight: 900;width: 25%;">Canal</div>
                  <div class="col-md-2 pdz" style="text-align: center;font-weight: 900;width: 25%;">Date</div>
                  <div class="col-md-2 pdz" style="text-align: center;font-weight: 900;width: 25%;">Time</div>
                  <div class="col-md-3 pdz" style="text-align: center;font-weight: 900;width: 25%;">Comment</div>
                </div>
                <?php  foreach ($bookingcustomreminders as $custom): 
                       $date = strtotime($custom->date);
                       $date = date('d/m/Y',$date);
                       $date= $date;
                    ?>

                    <div class="customreminderbox col-md-12 pdz">
                      <div class="col-md-3 text-center pdz" style="width: 25%;"><?= $custom->title  ?></div>
                      <div class="col-md-2 text-center pdz" style="width: 25%;"><?= $custom->date  ?></div>
                      <div class="col-md-2 text-center pdz" style="width: 25%;"><?= $custom->time  ?></div>
                      <div class="col-md-3 text-center pdz"  style="width: 25%;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;"><?= $custom->comment  ?></div>
                    
                    </div>
              <?php endforeach; ?>
               </div>
          </div>
          <!-- Custom Reminders -->
     </div>
     <!-- Reminder Notification Custom Reminder  Section -->
<div class="col-md-6 pdz">
   <!-- Customer Comment -->
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pdz">
        <div class="form-group">
            <label style="font-weight: 900;margin-bottom: 10px;">Comment : (by customer)</label>
            <textarea class="form-control" rows="4" name="booking_comment"  id="editcommentbox" placeholder="Write a comment here..." style="background-color: #def4faab !important;pointer-events: none;"><?= $dbbookingdata->booking_comment; ?></textarea>
    
        </div>
       </div>
    <!-- Customer Comment -->
   
 </div>  
</div>                   
 </div>
</div>
                
                             <div class="col-md-5" style="margin-top:15px;padding-right:0px;">
                                  
                              <div class="col-md-12" style="padding:0px;">
                               <div class="map-wrapper" >
                                  <div id="editmapheader" style="border-top-left-radius: 15px;border-top-right-radius: 15px;"><h4>Ride Map View</h4></div>
                                   <?php
                                   $k=1;
                                   foreach ($dbstopsdata as $stop):
                                    ?>
                                      <input type="hidden" id="otherstopaddr_<?= $k ?>" value="<?= $stop->stopaddress; ?>">
                                   <?php
                                    $k++;
                                    endforeach;
                                     ?>
                                   <input type="hidden" id="stoppercount" value="<?= count($dbstopsdata); ?>">
                                    <input type="hidden" id="editpickupaddress" value="<?=  $dbbookingdata->pick_point; ?>">
                                    <input type="hidden" id="editdropoffaddress" value="<?= $dbbookingdata->drop_point ?>">
                                    <input type="hidden" id="editpickotheraddress" value="<?= $dbpickaddressdata->otheraddress; ?>">
                                     <input type="hidden" id="editdropotheraddress" value="<?=  $dbdropaddressdata->otheraddress; ?>">
                                  

                                     <div id="editmapsearchloader" style="position: absolute;top: 50%;left: 45%;z-index: 1200;"> 
                                            <img src="<?= base_url()?>assets/theme/default/images/loader_2.gif" >
                                      </div>
                                      
                                         <div id="editmap" >
                                     
                                   
                                    </div>                                     
                                  </div>
                                  </div>
                                  <div class="col-md-12" style="margin-top: 46px;padding:0px;">
                                   <button id="distancebtn" class="btn  grbtn brz" style="border-bottom-left-radius: 15px;text-align:left;">Estimated Distance : <?= $dbbookingdata->distance ?> km</button>
                                   <button id="timebtn" class="btn  grbtn blz brz">Estimated Time : <?= str_replace('min', 'minute', $dbbookingdata->time_of_journey) ?> </button>
                                    <button class="btn  grbtn blz" id="bookingpricedivbtn" style="border-bottom-right-radius: 15px;text-align:right;">Estimated Price : <?= $dbbookingdata->price ?> € HT</button>
                                  </div>
                                  <!--Discount and fee -->
                                   <div class="col-md-12"  style="padding: 0px;">
                                     <!-- Fees -->
                                     <div class="col-md-12  clearfix amoutndetaildiv" >

                                        <div class="col-md-5" style="padding: 0px;">
                                          <div class="dd1_amount">
                                           <span style="font-weight:900;">Fees</span>
                                          </div>
                                       </div>
                                         <div class="col-md-3" style="padding: 0px;text-align: center;">
                                            <span style="font-weight:900;">%</span>
                                         </div>
                                       <div class="col-md-4" style="padding: 0px;">
                                        <div class="dd2_amount">
                                         <span style="font-weight:900;">Amount</span>
                                       </div>
                                     </div>
                                        
                                      </div>
                                       <?php if($dbdiscountandfeedata->drivermealfee!=0): ?>
                                     <div class="col-md-12 clearfix amoutndetaildiv" >
                                       <div class="col-md-5" style="padding: 0px;">
                                        <div class="dd1_amount">
                                         <span>Driver Meal Fee :</span>
                                        </div>
                                      </div>
                                       <div class="col-md-3" style="padding: 0px;text-align:center;">
                                       
                                          <span></span>
                                       
                                        </div>
                                       <div class="col-md-4" style="padding: 0px;">
                                        <div class="dd2_amount">
                                         <span>+   <?php echo $dbdiscountandfeedata->drivermealfee;?> €</span>
                                       </div>  
                                       </div>       
                                      </div>
                                    <?php endif; ?>
                                      <?php if($dbdiscountandfeedata->driverrestfee!=0): ?>
                                     <div class="col-md-12 clearfix amoutndetaildiv" >
                                       <div class="col-md-5" style="padding: 0px;">
                                        <div class="dd1_amount">
                                         <span>Driver Rest Fee :</span>
                                        </div>
                                      </div>
                                       <div class="col-md-3" style="padding: 0px;text-align:center;">
                                       
                                          <span></span>
                                       
                                        </div>
                                       <div class="col-md-4" style="padding: 0px;">
                                        <div class="dd2_amount">
                                         <span>+   <?php echo $dbdiscountandfeedata->driverrestfee;?> €</span>
                                       </div>  
                                       </div>       
                                      </div>
                                    <?php endif; ?>
                                     <?php if($dbdiscountandfeedata->nighttimefee!=0): ?>
                                     <div class="col-md-12 clearfix amoutndetaildiv" >
                                       <div class="col-md-5" style="padding: 0px;">
                                        <div class="dd1_amount">
                                         <span>Night Fee :</span>
                                        </div>
                                      </div>
                                       <div class="col-md-3" style="padding: 0px;text-align:center;">
                                        <?php if($dbdiscountandfeedata->nighttimediscount!=0):  ?>
                                          <span><?php echo $dbdiscountandfeedata->nighttimediscount;?>%</span>
                                        <?php endif; ?>
                                        </div>
                                       <div class="col-md-4" style="padding: 0px;">
                                        <div class="dd2_amount">
                                         <span>+   <?php echo $dbdiscountandfeedata->nighttimefee;?> €</span>
                                       </div>  
                                       </div>       
                                      </div>
                                    <?php endif; ?>

                                      <?php if($dbdiscountandfeedata->notworkingdayfee!=0): ?>
                                     <div class="col-md-12 clearfix amoutndetaildiv" >
                                       <div class="col-md-5" style="padding: 0px;">
                                        <div class="dd1_amount">
                                         <span>Sunday And Not Working Day Fee :</span>
                                        </div>
                                      </div>
                                       <div class="col-md-3" style="padding: 0px;text-align: center;">
                                        <?php if($dbdiscountandfeedata->notworkingdaydiscount!=0):  ?>
                                          <span><?php echo $dbdiscountandfeedata->notworkingdaydiscount;?>%</span>
                                        <?php endif; ?>
                                       </div>
                                       <div class="col-md-4" style="padding: 0px;">
                                        <div class="dd2_amount">
                                         <span>+   <?php echo $dbdiscountandfeedata->notworkingdayfee;?> €</span>
                                       </div>  
                                       </div>       
                                      </div>
                                    <?php endif; ?>


                                     <?php if($dbdiscountandfeedata->maydecdayfee!=0): ?>
                                     <div class="col-md-12 clearfix amoutndetaildiv" >
                                       <div class="col-md-5" style="padding: 0px;">
                                        <div class="dd1_amount">
                                         <span>1st Mai And 25 December Fee :</span>
                                        </div>
                                      </div>
                                       <div class="col-md-3" style="padding: 0px;text-align: center;">
                                         <?php if($dbdiscountandfeedata->maydecdaydiscount!=0):  ?>
                                          <span><?php echo $dbdiscountandfeedata->maydecdaydiscount;?>%</span>
                                        <?php endif; ?>
                                       </div>
                                       <div class="col-md-4" style="padding: 0px;">
                                        <div class="dd2_amount">
                                         <span>+   <?php echo $dbdiscountandfeedata->maydecdayfee;?> €</span>
                                       </div>   
                                       </div>      
                                      </div>
                                    <?php endif; ?>
                                     <!--Fees -->
                             <?php if($dbdiscountandfeedata->vatdiscount!=0): ?>
                              <div class="col-md-12  clearfix amoutndetaildiv" >

                              <div class="col-md-5" style="padding: 0px;">
                                <div class="dd1_amount">
                                 <span style="font-weight:900;">Vat</span>
                                </div>
                             </div>
                               <div class="col-md-3" style="padding: 0px;text-align: center;">
                                  <span style="font-weight:900;">%</span>
                               </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span style="font-weight:900;">Amount</span>
                             </div>
                           </div>
                              
                            </div>

                            <div class="col-md-12 clearfix amoutndetaildiv" >
                               <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span><?= $vatname ?> : </span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $dbdiscountandfeedata->vatdiscount;?>%</span></div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $dbdiscountandfeedata->vatfee;?> €</span>
                             </div>
                           </div>  
                            </div>
                             <?php endif; ?>
                             <div class="col-md-12  clearfix amoutndetaildiv" >

                              <div class="col-md-5" style="padding: 0px;">
                                <div class="dd1_amount">
                                 <span style="font-weight:900;">Discounts</span>
                                </div>
                             </div>
                               <div class="col-md-3" style="padding: 0px;text-align: center;">
                                  <span style="font-weight:900;">%</span>
                               </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span style="font-weight:900;">Amount</span>
                             </div>
                           </div>
                              
                            </div>
                          
                         
                          <?php if($dbdiscountandfeedata->welcomediscount!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                            <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Welcome Discount :</span>
                              </div>
                            </div>
                            <div class="col-md-3" style="padding: 0px; text-align: center;">
                             
                               <span><?php echo $dbdiscountandfeedata->welcomediscount;?>%</span>
                             
                            </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>-  <?php echo $dbdiscountandfeedata->welcomediscountfee;?> €</span>
                             </div>
                           </div>
                              
                            </div>
                          <?php endif; ?>
                        
                           <?php if($dbdiscountandfeedata->rewarddiscount!=0): ?>
                            <div class="col-md-12 clearfix amoutndetaildiv" >
                            <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Reward Discount :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $dbdiscountandfeedata->rewarddiscount;?>%</span></div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>-  <?php echo $dbdiscountandfeedata->rewarddiscountfee;?> €</span>
                             </div>
                           </div>
                              
                            </div>
                             <?php endif; ?>
                           <?php if($dbdiscountandfeedata->perioddiscount!=0): ?>
                            <div class="col-md-12 clearfix amoutndetaildiv" >
                               <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Period Discount :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $dbdiscountandfeedata->perioddiscount;?>%</span></div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>-   <?php echo $dbdiscountandfeedata->perioddiscountfee;?> €</span>
                             </div>
                           </div>
                              
                            </div>
                             <?php endif; ?>
                             <div class="col-md-12 clearfix amoutndetaildiv" id="promo_code_div" >
                               <?php if($dbdiscountandfeedata->promocodediscount!=0): ?>
                              
                            <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Discount Code :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $dbdiscountandfeedata->promocodediscount;?>%</span></div>
                              <div class="col-md-4" style="padding: 0px;">
                               <div class="dd2_amount">
                                 <span>-   <?php echo $dbdiscountandfeedata->promocodediscountfee;?> €</span>
                               </div>
                             </div>
                            <?php endif; ?>
                             
                              
                            </div>
                            
                          
                            <div class="col-md-12 clearfix grdiv amountdetaillastdiv">
                             <div class="col-md-3" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Price To Pay :</span>
                              </div>
                            </div>
                             <div class="col-md-5" style="padding: 0px;text-align: center;">
                              
                              </div>
                              <div class="col-md-4" style="padding: 0px;">
                               <div class="dd2_amount">
                                <span style="font-weight: 900;"><?php echo $dbbookingdata->totalprice;?> € TTC</span>
                               </div>
                             </div>
                            
                           </div>
                            <?php if($dbbookingdata->realridetotalprice !=0 ): ?>
                                                     
                            <div class="col-md-12 clearfix grdiv amountdetaillastdiv" style="margin-top: 10px;">
                             <div class="dd1_amount">
                               <span style="font-weight: 900;">Suggested Price :</span>
                              </div>
                              <div class="dd2_amount">
                               <span style="font-weight: 900;"><?php echo $dbbookingdata->realridetotalprice ;?> € </span>
                             </div>
                           </div>
                            <?php endif; ?>

                          </div>
                                  <!--Discount and fee -->
                                  <!-- payment -->
              <div class="col-md-12 paymentmethodboxdiv" style="margin-top: 10px;">
                      
                      <div class="col-md-12" style="padding:0px;" >

                          <button class="btn payment-method-header-title" style="pointer-events:none;">
                        Payment Methode         
                          </button>        
                      </div>
                               
                              
                  <div class="col-md-12" style="padding:0px;">             
                            <ul role="tablist" id="paymentmethodsullist" class="nav nav-tabs on-bo-he  payement" style="width: 100%;">
                              <?php
                               $methodcount=1;
                               foreach ($paymentmethods as $key => $method):
                                 if($methodcount<7):
                              ?>

                            <li class="payment-method-buttons-list" style="padding: 0px !important;width: 16.65%;margin:0px !important;border:0px;">         
                            <input type="radio" name="payment_method" style="pointer-events: none;top: 8px !important;"  value="<?= $method->id ?>" id="methodbtn<?= $methodcount ?>" <?= ($dbbookingdata->payment_method_id==$method->id)?'checked':''; ?> >
                            <button type="button" class="btn grpbtn payment-method-buttons-btn" style="pointer-events: none;padding: 6px 0px 0px 22px !important;"><span style="font-size: 11.5px;"><?= $method->name ?></span>
                            </button>
                            </li>

                              <?php
                              $methodcount++;
                               endif;
                               endforeach;
                              ?>
                            </ul>
                   </div>
                              
                      <div class="col-md-12" id="paymentmethoddiv" >
                              <?php
                                   $paymentmethodcount=1;
                                   $cardtype="credit";
                                   foreach ($paymentmethods as $key => $method):
                                    if($paymentmethodcount<7):
                                  ?>
                                  <div class="payments" id="paymentmethod<?= $paymentmethodcount ?>" style="text-align: unset;">
                                  <h4 style="font-size:18px;font-weight:900;"> Pay With <?= $method->name ?> </h4>
                                  <!-- Credit Card Start -->
                                <?php
                                         if($method->category_client == 1): 
                                         $cardtype="credit";
                                        
                                ?>         

                                         <?php  include 'detailcreditcard.php';?> 
                                <!-- Credit Card End -->
                                    
                                 <!-- Bank Debit Card Start -->
                                <?php
                                        elseif($method->category_client == 2): 
                                         $cardtype="debit";
                                ?>
                                         <?php  include 'detaildebitcard.php';?>
                                 <!-- Bank Debit Card End -->

                                 <!-- Other Payment ways Start -->
                                <?php else: ?>
                                  <p><?= $method->description ?></p>
                                <?php endif; ?>
                                <!-- Other Payment ways End -->
                                                                
                                 </div>
                                 <?php
                                  $paymentmethodcount++;
                                   endif;
                                   endforeach;
                                  ?>
                        </div>
                                    <!--<div class="down-btn">-->
                      

                    </div>
                                  <!-- payment -->
                                </div> 
    <div class="row submit-row">
      <div class="col-md-12" style="padding:0px;margin-top:10px;">                               
        
            <a href="<?= base_url(); ?>admin/bookings"  class="btn btn-default" style="float:right; margin-left:7px;font-size: 18px !important;" onclick="cancelupdatebooking()" id="bookingviewcancelbtn"><span class="fa fa-close"> Cancel </span></a>      
      </div>         
    </div>
                              </div>
                            </div>
         
     
        <div class="clearfix"></div>
       
       

      
  
