<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" >


  <!--client passenger driver cars -->
 
      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 booking_serivce_inner_tab" style="padding-left:0px;width: 19%;">
      <div class="form-group">
        <input type="hidden" name="booking_id" id="bookingidfieldvalue" value="<?= $dbbookingdata->id ?>">
             <select name="booking_status"  class="servicecategoryinput" required>
                   <option value="">Statut</option>
                  
                   <option <?= ($dbbookingdata->is_conformed=='0' ? 'selected' :''); ?> value="0">Pending</option>
                   <option <?= ($dbbookingdata->is_conformed=='1' ? 'selected' :''); ?> value="1">Confirmed</option>
                   <option <?= ($dbbookingdata->is_conformed=='2' ? 'selected' :''); ?> value="2">Cancelled</option>
                 
             </select>
                  
              <?php if(isset($service_form['driver']['error']) && !empty($service_form['driver']['error'])): ?>  
                  <div class='invalid-error'><?= $service_form['driver']['error']?></div>
              <?php endif; ?>
                  
        </div>
    </div>
 
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 booking_serivce_inner_tab" style="padding-left:0px;width: 19%;">
      <div class="form-group">
       
             <select name="client_field"  class="servicecategoryinput" id="edituserisclient"  onchange="editgetpassengers()" required>
                   <option value="">&#xf007; Clients</option>
                  <?php foreach($clients_data as $key => $item): ?>
                   <option <?= ($client_id==$item->id)?"selected":""; ?> value="<?= $item->id ?>">&#xf007; <?= $item->civility.' '.$item->first_name.' '.$item->last_name; ?></option>
                   <?php endforeach; ?>
             </select>
                  
              <?php if(isset($service_form['client']['error']) && !empty($service_form['client']['error'])): ?>  
                  <div class='invalid-error'><?= $service_form['client']['error']?></div>
              <?php endif; ?>
                  
        </div>
    </div> 
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-10 booking_serivce_inner_tab" style="padding-left:0px;width: 19%;">
      <div class="form-group">
       
             <select name="passenger"  class="servicecategoryinput" id="editpassengerselectdiv">
                   <option value="">&#xf007; Passengers</option>
                   <?php
                       $passengers_data=$this->bookings_model->booking_passengers('vbs_passengers',$client_id);
                      foreach($passengers_data as $key => $item): 
                     ?>
                   <option  value="<?= $item->id ?>">&#xf007; <?= $item->civility.' '.$item->fname.' '.$item->lname; ?></option>
                   <?php endforeach; ?>
             </select>
                  
              <?php if(isset($service_form['passengers']['error']) && !empty($service_form['passengers']['error'])): ?>  
                  <div class='invalid-error'><?= $service_form['passengers']['error']?></div>
              <?php endif; ?>
                  
        </div>
    </div> 
    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-2" style="margin-top: 10px;width: 5%;padding-left:0px;">
     <button type="button" class="plusgreeniconconfig"  onclick="editaddpassengers()"><i class="fa fa-plus"></i></button>
    </div>


 

  <!--client passenger driver cars -->
</div>
