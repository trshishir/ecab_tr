<?php echo form_open_multipart('cms/add_infobull', array("id" => "add_backoffice_infobull")); ?>

<?php extract($data); ?>


<div class="col-md-12 admin_border" style="margin-top: 5px;">
	<h3 class=""><strong>Client Section</strong></h3>

	<div class="col-md-2">
		<div class="form-group" style="margin-left: 18px;">
			<div class="checkbox">
		
				<lebel class="checkbox-inline">
					<input id="client_status" name="client_status" type="checkbox" <?php echo ($client_status==1)?'checked':'';?>  data-toggle="toggle" data-on="Enabled" data-off="Disabled"> 
				</lebel>

			</div>
		</div>
	</div>
	<div class="col-md-10">
		<div style="    padding-left: 1.5%;" class="col-xs-12">
			<div class="form-group">
				<label>Description*</label>
				<textarea class="ckeditor" required="required" id="editor1_client_desc" name="client_desc" cols="100" rows="5">
					<?php echo $client_desc;?>
				</textarea>
			</div>
		</div>
	</div>
</div>
<!-- section end -->




<div class="col-md-12 admin_border" style="margin-top: 5px;">
	<h3 class=""><strong>Driver Section</strong></h3>

	<div class="col-md-2">
		<div class="form-group" style="margin-left: 18px;">
			<div class="checkbox">
		
				<lebel class="checkbox-inline">
					<input id="driver_status" name="driver_status" type="checkbox" <?php echo ($driver_status==1)?'checked':'';?>  data-toggle="toggle" data-on="Enabled" data-off="Disabled"> 
				</lebel>

			</div>
		</div>
	</div>
	<div class="col-md-10">
		<div style="    padding-left: 1.5%;" class="col-xs-12">
			<div class="form-group">
				<label>Description*</label>
				<textarea class="ckeditor" required="required" id="editor1_driver_desc" name="driver_desc" cols="100" rows="5">
					<?php echo $driver_desc;?>

				</textarea>
			</div>
		</div>
	</div>
</div>
<!-- section end -->




<div class="col-md-12 admin_border" style="margin-top: 5px;">
	<h3 class=""><strong>Job Section</strong></h3>

	<div class="col-md-2">
		<div class="form-group" style="margin-left: 18px;">
			<div class="checkbox">
		
				<lebel class="checkbox-inline">
					<input id="job_status" name="job_status" type="checkbox" <?php echo ($job_status==1)?'checked':'';?>  data-toggle="toggle" data-on="Enabled" data-off="Disabled"> 
				</lebel>

			</div>
		</div>
	</div>
	<div class="col-md-10">
		<div style="    padding-left: 1.5%;" class="col-xs-12">
			<div class="form-group">
				<label>Description*</label>
				<textarea class="ckeditor" required="required" id="editor1_job_desc" name="job_desc" cols="100" rows="5">
					<?php echo $job_desc;?>

				</textarea>
			</div>
		</div>
	</div>
</div>
<!-- section end -->



<div class="col-md-12 admin_border" style="margin-top: 5px;">
	<h3 class=""><strong>Partner Section</strong></h3>

	<div class="col-md-2">
		<div class="form-group" style="margin-left: 18px;">
			<div class="checkbox">
		
				<lebel class="checkbox-inline">
					<input id="aff_status" name="aff_status" type="checkbox" <?php echo ($aff_status==1)?'checked':'';?>  data-toggle="toggle" data-on="Enabled" data-off="Disabled"> 
				</lebel>

			</div>
		</div>
	</div>
	<div class="col-md-10">
		<div style="    padding-left: 1.5%;" class="col-xs-12">
			<div class="form-group">
				<label>Description*</label>
				<textarea class="ckeditor" required="required" id="editor1_aff_desc" name="aff_desc" cols="100" rows="5">
					<?php echo $aff_desc;?>

				</textarea>
			</div>
		</div>
	</div>
</div>
<!-- section end -->








<div class="row pull-right" style="margin-right:0.1%; margin-top: 5px;">
	<input type="button" class="btn btn-default fa fa-input" value="&#xf05e Cancel" onclick="return hideAdd();" tabindex="17" />  
	<input type="submit" class="btn btn-default fa fa-input" id="save" value="&#xf0c7 Save" tabindex="16" /> 

</div>
<?php echo form_close(); ?>



<script type="text/javascript">




	$(function() {


		$('#client_status').bootstrapToggle({
			on: 'Enabled',
			off: 'Disabled'
		});



		$('#job_status').bootstrapToggle({
			on: 'Enabled',
			off: 'Disabled'
		});



		$('#driver_status').bootstrapToggle({
			on: 'Enabled',
			off: 'Disabled'
		});



		$('#aff_status').bootstrapToggle({
			on: 'Enabled',
			off: 'Disabled'
		});



	})

</script>