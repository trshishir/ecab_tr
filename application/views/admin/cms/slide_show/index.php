<section id="content">
    <div class="listDiv">
        <?php $this->load->view('admin/common/breadcrumbs'); ?>
        <div class="row-fluid">
            <div class="col-md-12">
                <lebel style="padding-left: 4px;" class="client_header_title">Slider Section </lebel>
                <?php echo form_open_multipart('admin/cms/add_cms_slider', array("id" => "slider_show")); ?>

                <div class="col-md-3 form-group" style="margin-left: -13px;">
                    <select class="form-control admin_border " name="section" onchange="getSlides(this)" id="select_page_section" tabindex="1" required>
                        <option value=""> Select Page Section</option>
                        <option data-type="service" value="home">Home</option>
                        <option data-type="news" value="affiliate">Affiliate</option>
                        <option data-type="news" value="driver">Driver</option>
                        <option data-type="news" value="partner">Partner</option>
                        <option data-type="job" value="jobs">Jobs</option>
                    </select>
                </div>

                <div class="clearfix"></div>
                <div id="slider-list">

                </div>
                <div class="col-md-8" id="action_id" style="padding-right: 13px;">
                    <div class="pull-right" style="">
                        <input type="button" class="btn btn-default fa fa-input" value="&#xf05e Cancel" onclick="return hideAdd();" tabindex="17" />
                        <input type="submit" class="btn btn-default fa fa-input" id="save" value="&#xf0c7 Save" tabindex="16" />
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</section>
<?php include "slide_forms.php"?>
<script type="text/javascript">

    $("#slider_show").submit(function(e) {
        e.preventDefault();

        var slides = $(this).find(".add_new_slide");
        var formData = new FormData();
        var section = $(this).find("#select_page_section").val();
        formData.append("<?php echo $this->security->get_csrf_token_name();?>","<?php echo $this->security->get_csrf_hash();?>")
        slides.each(function (x, y) {
            var photo = $(y).find("input[name='photo']");
            var leftIcon = $(y).find("input[name='leftIcon']");
            var rightIcon = $(y).find("input[name='rightIcon']");
            var sid = $(y).find("input[name='sid']");
            var photoAdd = photo.length > 0 && photo[0].files.length > 0 ? photo[0].files[0] : 0;
            var leftIconAdd = leftIcon.length > 0 && leftIcon[0].files.length > 0 ? leftIcon[0].files[0] : 0;
            var rightIconAdd = rightIcon.length > 0 && rightIcon[0].files.length > 0 ? rightIcon[0].files[0] : 0;
            //console.log(sid.length)
            formData.append("category__"+ x +"", $(y).find("select[name='category']").val());
            formData.append("sub_category__"+ x +"", $(y).find("select[name='sub_category']").val());
            formData.append("sid__"+ x +"", sid.length > 0 ? sid.val() : 0);
            formData.append("photo__"+ x +"", photoAdd !== 0 ? 1 : 0);
            formData.append("photoFile__"+ x +"", photoAdd);
            formData.append("leftIcon__"+ x +"", leftIconAdd !== 0 ? 1 : 0);
            formData.append("leftIconFile__"+ x +"", leftIconAdd);
            formData.append("rightIcon__"+ x +"", rightIconAdd !== 0 ? 1 : 0);
            formData.append("rightIconFile__"+ x +"", rightIconAdd);
            formData.append("section__"+x, section);
        });


        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: formData,
            success: function (data) {
//                alert(data);
                location.reload();

            },
            cache: false,
            contentType: false,
            processData: false
        });
    });

    function getSlides(ele) {
        var that = $(ele);
        var select_item = that.children("option:selected").val();
        var select_type = that.children("option:selected").data('type');

        if(select_item === ""){
            $( "#slider-list" ).empty()
            return false;
        }
        $.ajax({
            url:'ajax_get_cms_sliders.php',
            method: 'post',
            data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&section='+select_item,
            dataType: 'json',
            success: function(response){
                console.log(response!='' && response.length > 0)
                if(response!='' && response.length > 0) {
                    html = ''
                    count =1;
                    $.each(response, function( index, value ) {
                        console.log(value);
                        action_html='';
                        if(count==1) {
                            action_html='<span><button type="button" class="btn btn-default"  style="" onclick="add_slide()" >+</button></span><br></br>';
                        } else {
                            action_html='<span><button type="button" class="btn btn-default" onclick="remove_slide(this,'+value.id+')" >-</button></span>';
                        }
                        var clone = $($("#"+select_type+"-form").html());
                        clone.data('id', value.id);
                        clone.find(".slider-image").html("<img style='width: 100%; height: 135px' " +
                            "src='<?php echo base_url().'uploads/cms/slider/';?>" + value.img + "'>" +
                            " <input type='hidden' name='sid' value='"+value.id+"'>");
                        //console.log(value.category_id);
                        //console.log(value.service_id);
                        clone.find(".category option[value='"+value.category_id+"']").attr("selected","selected");
                        clone.find(".sub_category option").not("[data-sec='"+select_item+"'][data-cat='"+value.category_id+"'], [value='']").hide();
                        clone.find(".sub_category option[data-sec='"+select_item+"'][data-cat='"+value.category_id+"'][value='"+value.service_id+"']").attr("selected","selected");
                        clone.find(".right-icon").after(value.right_icon);
                        clone.find(".left-icon").after(value.left_icon);
                        clone.find(".buttons").html(action_html);
                        html += clone.html();
                        count++;
                    });
                    $( "#slider-list" ).html(html);
                } else {
                    add_slide("plus")
                }
            }
        });
    }

    function remove_slide(elm,id)
    {
        if(id == 0){
            $(elm).parent().parent().parent().remove();
        } else if(confirm("Are you sure you want to delete this?")){
            $.ajax({
                url:'ajax_delete_cms_sliders.php',
                method: 'post',
                data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&id='+id,
                dataType: 'json',
                success: function(response){
                    if(response!='')
                    {
                        $(elm).parent().parent().parent().remove();
                        var count_class = $("#slider-list > div").length;
                        if(count_class==0) {
                            add_more('plus');
                        }
                    }
                }
            });
        } else
            return false;
    }
    function getSubcats(ele, id=""){
        var that = $(ele);
        var section = $("#select_page_section").val();
        var cat_id = that.children("option:selected").val();
        var subcat = that.parent().parent().parent().find(".sub_category");
        //console.log(cat_id);
        //console.log(subcat);
        subcat.val("");
        subcat.find("option").not("[value='']").hide();
        subcat.find("option[data-sec='"+section+"'][data-cat='"+cat_id+"']").show();
        subcat.trigger("change");
    }

    function add_slide(action = "")
    {
        var section = $("#select_page_section").children("option:selected").data('type');
        var clone = $($("#"+section+"-form").html());

        if(action === "plus") {
            action_html='<span><button class="btn btn-default" style="" type="button" onclick="add_slide()" >+</button></span>';
            $( "#slider-list" ).html("")
        } else{
            action_html='<span><button class="btn btn-default"   style="" type="button" onclick="remove_slide(this)" >-</button></span>';
        }
        clone.find(".sub_category option").hide();
        clone.find(".buttons").html(action_html);
        //console.log(clone)
        $( "#slider-list" ).append(clone);
    }

    $(function () {
        getSlides($("#select_page_section"));
    });
</script>