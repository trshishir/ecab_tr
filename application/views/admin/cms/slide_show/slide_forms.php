<div class="hidden" id="service-form">
    <div class="row">
        <div class="col-md-12 form-group add_new_slide" style="display:block;margin-left: -11px;">
            <div class="col-md-8 admin_border custom_gray_bg" style="border: 1px solid; padding-top: 20px;">
                <div class="row">
                    <div class="col-md-6 slider-image">
                        <div class="form-group">
                            <label>Banner Image*</label>
                            <input type="file" name="photo" style=" min-height: 5px;border: 1px solid lightgray; width: 100%" required>
                        </div>
                    </div>
                    <div class="col-md-6 slider-option">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Category*</label>
                                <select class="form-control category" onchange="getSubcats(this)" name="category" required>
                                    <option value="">Select Category</option>
                                    <?php
                                    if(isset($category))
                                        foreach($category as $cat):?>
                                            <option value="<?=$cat->id;?>"><?=$cat->name;?></option>
                                        <?php endforeach;?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Service*</label>
                                <select class="form-control sub_category" name="sub_category" required>
                                    <option value="">Select Service</option>
                                    <?php
                                    if(isset($services))
                                        foreach($services as $cat):?>
                                            <option data-sec="<?=$cat->news_location;?>" data-cat="<?=$cat->category_id?>" value="<?=$cat->id;?>"><?=$cat->title;?></option>
                                        <?php endforeach;?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Left Icon</label>
                                <input type="file" class="left-icon" name="leftIcon" style=" min-height: 5px;border: 1px solid lightgray; width: 100%">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Right Icon</label>
                                <input type="file" class="right-icon" name="rightIcon" style=" min-height: 5px;border: 1px solid lightgray; width: 100%">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 buttons"></div>
        </div>
    </div>
</div>
<div class="hidden" id="news-form">
    <div class="row">
        <div class="col-md-12 form-group add_new_slide" style="display:block;margin-left: -11px;">
            <div class="col-md-8 admin_border custom_gray_bg" style="border: 1px solid; padding-top: 20px;">
                <div class="row">
                    <div class="col-md-6 slider-image">
                        <div class="form-group">
                            <label>Banner Image*</label>
                            <input type="file" name="photo" style=" min-height: 5px;border: 1px solid lightgray; width: 100%" required>
                        </div>
                    </div>
                    <div class="col-md-6 slider-option">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Category*</label>
                                <select class="form-control category" onchange="getSubcats(this)" name="category" required>
                                    <option value="">Select Category</option>
                                    <?php
                                    if(isset($news_category))
                                        foreach($news_category as $cat):?>
                                            <option value="<?=$cat->id;?>"><?=$cat->name;?></option>
                                        <?php endforeach;?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>News*</label>
                                <select class="form-control sub_category" name="sub_category" required>
                                    <option value="">Select News</option>
                                    <?php
                                    if(isset($news))
                                        foreach($news as $cat):?>
                                            <option data-sec="<?=$cat->news_location;?>" data-cat="<?=$cat->category_id?>" value="<?=$cat->id;?>"><?=$cat->title;?></option>
                                        <?php endforeach;?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Left Icon</label>
                                <input type="file" class="left-icon" name="leftIcon" style=" min-height: 5px;border: 1px solid lightgray; width: 100%">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Right Icon</label>
                                <input type="file" class="right-icon" name="rightIcon" style=" min-height: 5px;border: 1px solid lightgray; width: 100%">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 buttons"></div>
        </div>
    </div>
</div>
<div class="hidden" id="job-form">
    <div class="row">
        <div class="col-md-12 form-group add_new_slide" style="display:block;margin-left: -11px;">
            <div class="col-md-8 admin_border custom_gray_bg" style="border: 1px solid; padding-top: 20px;">
                <div class="row">
                    <div class="col-md-6 slider-image">
                        <div class="form-group">
                            <label>Banner Image*</label>
                            <input type="file" name="photo" style=" min-height: 5px;border: 1px solid lightgray; width: 100%" required>
                        </div>
                    </div>
                    <div class="col-md-6 slider-option">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Category*</label>
                                <select class="form-control category" onchange="getSubcats(this)" name="category" required>
                                    <option value="">Select Category</option>
                                    <?php
                                    if(isset($get_alls_category))
                                        foreach($get_alls_category as $cat):?>
                                            <option value="<?=$cat->id;?>"><?=$cat->jobcategory;?></option>
                                        <?php endforeach;?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Job*</label>
                                <select class="form-control sub_category" name="sub_category" required>
                                    <option value="">Select Job</option>
                                    <?php
                                    if(isset($jobs_listing))
                                        foreach($jobs_listing as $cat):?>
                                            <option data-sec="jobs" data-cat="<?=$cat->jobcategory_id?>" value="<?=$cat->id;?>"><?=$cat->job;?></option>
                                        <?php endforeach;?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Left Icon</label>
                                <input type="file" class="left-icon" name="leftIcon" style=" min-height: 5px;border: 1px solid lightgray; width: 100%">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Right Icon</label>
                                <input type="file" class="right-icon" name="rightIcon" style=" min-height: 5px;border: 1px solid lightgray; width: 100%">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 buttons"></div>
        </div>
    </div>
</div>