<?php $locale_info = localeconv(); ?>
<div class="col-md-12"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12">
                <?php $this->load->view('admin/common/alert');?>
                <div class="module">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="module-head">
                    </div>
                    <div class="module-body">
                        <?=form_open("cms/add")?>
						<div class="row">
						<div class="col-xs-8 p-2" style="padding:28px;">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Page Type*</label>
                                    <select class="form-control" name="status" required>
                                        <option value="1">Services Page</option>
                                        <option value="2">Zones Page</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Status*</label>
									 <select class="form-control" name="status" required>
                                        <option value="1">Enabled</option>
                                        <option value="2">Disabled</option>
                                    </select>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Language*</label>                
                                        <input id="subject" maxlength="200" type="text" class="form-control" required name="Language" placeholder="Language" value="<?=set_value('sub',$this->input->post('sub'))?>">
                                    <!--</div>-->
                                </div>
                            </div>
							 <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Category of File*</label>
                                        <input id="subject" maxlength="200" type="text" class="form-control" required name="Category of File" placeholder="Category of File" value="<?=set_value('sub',$this->input->post('sub'))?>">
                                    <!--</div>-->
                                </div>
                            </div>
							<div class="col-xs-4">
                                <div class="form-group">
                                    <label>File Name*</label>
                                        <input id="subject" maxlength="200" type="text" class="form-control" required name="File Name" placeholder="File Name" value="<?=set_value('sub',$this->input->post('sub'))?>">
                                    <!--</div>-->
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>Message*</label>
                                    <textarea class="ckeditor" id="editor1" name="message" cols="100" rows="10">
									Message here...
                                        <?= set_value('Message', $this->input->post('message')) ?>
                                    </textarea>
                                </div>
                            </div>
                        </div>
						 <div class="row">
                            <div class="col-xs-12">
                                <div class="btn-group" style="float:right!important">
                                                <button type="button" class="btn btn-success">
                                                    <i class="fas fa-upload"></i> Upload <i class="fa fa-plus" aria-hidden="true"></i>
                                                </button>
                                                
                                            </div>
                            </div>
                        </div>
                        </div>
						<div class="col-xs-4 p-2"  style="padding:28px;">
						<div class="row">
						<div class="col-xs-offset-2  col-xs-8 col-sm-offset-2">
                                <div class="form-group"> 
                                    <label>Link Extension*</label>
                                        <input id="subject" maxlength="200" type="text" class="form-control" required name="subject" placeholder="Link Extension" value="<?=set_value('sub',$this->input->post('sub'))?>">
                                    <!--</div>-->
                                </div>
                            </div>
                        </div>
						<div class="row">
						<div class="col-xs-offset-2  col-xs-8 col-sm-offset-2">
                                <div class="form-group"> 
                                    <label>Meta Tags*</label>
                                       <textarea name="message" cols="100" rows="10">Meta Tags...
                                        <?= set_value('Meta Tags', $this->input->post('message')) ?>
                                    </textarea>
                                    <!--</div>-->
                                </div>
                            </div>
                        </div>
						<div class="row">
						<div class="col-xs-offset-2  col-xs-8 col-sm-offset-2">
                                <div class="form-group"> 
                                    <label>Mega Description*</label>
                                        <textarea name="message" cols="100" rows="10">Mega Description...
                                        <?= set_value('Mega Description', $this->input->post('message')) ?>
                                    </textarea>
                                </div>
                            </div>
                        </div>
						<div class="row">
						<div class="col-xs-offset-2  col-xs-8 col-sm-offset-2">
						<div class="text-right" style="position: relative;bottom: -58px;">
                            <button class="btn btn-default">Save</button>
                            <a href="<?=base_url("cms/index")?>" class="btn btn-default">Cancel</a>
                        </div>
						</div>
						</div>
						</div>
						</div>
						
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $(".bdatepicker").datepicker({
            format: "dd/mm/yyyy"
        });
    });
</script>