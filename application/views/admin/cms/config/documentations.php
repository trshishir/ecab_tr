


<section id="content">
  <?php $this->load->view('admin/common/breadcrumbs'); ?>
<?php include_once('config_menu.php');?>

  <div class="listDiv">

    <div class="row-fluid">



      <div class="module">
        <?php echo $this->session->flashdata('message'); ?>

             <div class="clearfix"></div>
             <div class="module-body table-responsive">
              <table id="cms_listing" class="table table-bordered table-striped  table-hover dataTable" cellspacing="0" width="100%" data-selected_id="">
               <thead>
                <tr>
                     <th class="no-sort text-center"><input type='checkbox' name='allClients' id="allClients" value='' onclick="Javascript:selectAllClients();" /></th>
                     <th class="column-id"><?php echo $this->lang->line('id'); ?></th>
                     <th class="column-id"><?php echo $this->lang->line('date'); ?></th>
                     <th class="column-id"><?php echo $this->lang->line('time'); ?></th>
                     <th class="column-status">Added By</th>
                     <th class="column-name"><?php echo $this->lang->line('name'); ?></th>
                     <th class="column-status"><?php echo $this->lang->line('status'); ?></th>
                     <th class="column-status"><?php echo $this->lang->line('since'); ?></th>                                 
                 </tr>
               </thead>
              </table>
             </div>
            </div>
          </div>
       </div>
             <div class="addDiv" style="display:none; padding: 1%;">

              <?php echo form_open_multipart('cms/add_new_services', array("id" => "new_service_form")); ?>

              <div class="row">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-3 form-group">  
                      <label> Status </label>                                
                      <select class="form-control" name="status" id="status" tabindex="1">
                        <option value="1">Enable</option>
                        <option value="2">Disable</option>
                      </select>
                    </div>




                    <?php 

                    $data = [
                      'type'  => 'hidden',
                      'name'  => 'page_type',
                      'id'    => 'page_type',
                      'value' => 'documentations_cat',
                    ];

                    echo form_input($data);


                          echo form_input($data);
                          $data = [
                              'type'  => 'hidden',
                              'name'  => 'id',
                              'id'    => 'id',
                              'value' => '',
                          ];

                          echo form_input($data);

                          $data = [
                              'type'  => 'hidden',
                              'name'  => 'is_category',
                              'id'    => 'is_category',
                              'value' => 1,
                          ];

                          echo form_input($data);


                          ?> 


                        <!--        <?php /*end hidden field*/   ?>                            -->


                       <div class="col-md-9 form-group">  
                          <label> Name* </label>                                
                          <?php 

                          $data = [
                           'type'  => 'text',
                           'name'  => 'name',
                           'id'    => 'name',
                           'value' => '',
                           'placeholder' => 'Name*',
                           'required'=>'required',

                           'class' => 'form-control'
                       ];

                       echo form_input($data);

                       ?> 
                   </div>





   </div>
</div>











</div>

</div>

<div class="row">
    <div class="col-md-12"> 
     <div class="pull-right" style="padding-right: 1%">
     <input type="button" class="btn btn-default fa fa-input" value="&#xf05e Cancel" onclick="return hideAdd();" tabindex="17" />  
      <input type="submit" class="btn btn-default fa fa-input" id="save" value="&#xf0c7 Save" tabindex="16" /> 


  </div>
</div>
</div>
<?php echo form_close(); ?>
</div>
</section>
</div>

<script>

 function showAdd() {
  hideForms();
  $('.addDiv').show();
  $('.breadcrumb').append('<span id="subtitle_custom"> > Add New Category</span>');

        //showAddHead();
    }
    function hideForms() {
        $('.addDiv').hide();
        $('.listDiv').hide();
        $('#subtitle_custom').remove(); 

    }
    function hideAdd() {
        hideForms();
        $('.listDiv').show();
    }



    function delete_item(id=null)
    {
     if(id==null)
     {

        $.each($("input[name='listCheck']:checked"), function(){
         id= $(this).val();
     });

    }

    if (id === null)
    {
        alert('No row item selected!!');
        return false;
    }


    if(confirm("Are you sure to delete?"))
    {

        $.ajax({
         url:'ajax_delete_cms_listing.php',
         method: 'post',
         data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&id='+id,
         dataType: 'json',
         success: function(response){

           location.reload();

       }


   });
    }


}






function edit_item(id=null,display_id=null)
{
 if(id==null)
 {

    $.each($("input[name='listCheck']:checked"), function(){
     id= $(this).val();
     display_id = $(this).data("id");
 });

}

if (id === null)
{
    alert('No row item selected!!');
    return false;
}


hideForms();

    //  var $form = $('.addDiv').clone();
    //  $('.edit_div').html($form);
    // $('.edit_div').show();
    $('.addDiv').show();
    $('.breadcrumb').append('<span id="subtitle_custom"> > '+display_id+'</span>');


    $('#new_service_form').attr('action','ajax_update_cms_listing.php');
    $('#img').removeAttr('required');

 


    $.ajax({
        url:'ajax_get_cms_listing.php',
        method: 'post',
        data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&post_type=documentations_cat&id='+id,
        dataType: 'json',
        success: function(response){
            console.log(response);

            $('#name').val(response.name);

            // $('#title').val(response.title);
            $('#id').val(response.id);
            $('#status').val(response.status);


        }


    });

}












    // DATATABLE

    function get_all_list()
    {

      var oTable = $('#cms_listing').on('preXhr.dt', function (e, settings, data) {
          $("#loaderTable").show();
      }).DataTable({
        'processing': true,
        'serverSide': true, 
        "dom": "<'row'<'col-sm-4'l><'col-sm-4 statusDropdown'><'col-sm-4'f>>" +
        "<'row'<'col-sm-12't>>" +
        "<'row'<'col-sm-6'i><'col-sm-6'p>>",
        'ajax': {
          url:'services_list_ajax_data_set',
          type: 'post',
                // data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&post_type=documentations_cat',

                data: {
                  'post_type': 'documentations_cat',
                  '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',
                },
                dataType: 'json',
            },"initComplete":function( settings, json){
              $("#loaderTable").hide();
            },
            'columns': [
            { data: 'created_date' },
            { data: 'id' },
            { data: 'date' },
            { data: 'time' },
            { data: 'username' },

            { data: 'name' },
            { data: 'status' },
            { data: 'since' },            
            ],
            columnDefs: [
            { className: "text-center", targets: "_all" },
            ],

            dom: 'lBfrtip',

            buttons: [


            {
             text: ' <span href="javascript:;" class="" onclick="showAdd();"><i class="fa fa-plus"></i> Add</span> ',
             title: '',
         },


         {
             text: ' <span href="javascript:;" class="" onclick="edit_item()"><i class="fa fa-pencil"></i> Edit</span> ',
             title: '',
         },


         {
             text: ' <span href="javascript:;" class="" onclick="delete_item()"><i class="fa fa-trash"></i> Delete</span> ',
             title: '',
         },

    

         {
           extend: 'excel',
           text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel Export',
           filename: 'members',
           title: '',
           exportOptions: {
              modifier: {
                 search: 'applied',
                 order: 'applied',
                 page: 'current'
             },
             columns: [1, 2, 3]
         }
     },
     {
       extend: 'csv',
       text: '<i class="fa fa-file-text-o" aria-hidden="true"></i> Export CSV',
       filename: 'members',
       title: '',
       exportOptions: {
          modifier: {
             search: 'applied',
             order: 'applied',
             page: 'current'
         },
         columns: [1, 2, 3, 4, 5]
     }
 },
 {
   extend: 'pdf',
   text: '<i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF',
   filename: 'members',
   title: '',
   exportOptions: {
      modifier: {
         search: 'applied',
         order: 'applied',
         page: 'current'
     },
     columns: [1, 2, 3, 4, 5]
 }
},



],
"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]




});  
    } 




    $(document).ready(function() {
     var frm = $('#new_service_form');

     frm.submit(function (e) {

        $("#loaderDiv").show();
        $(".pull-left").hide();


        e.preventDefault();


        $.ajax({
           type: frm.attr('method'),
           url: frm.attr('action'),
                        // data: frm.serialize(),
                        data: new FormData(this),
                        contentType: false, //must, tell jQuery not to process the data
                        processData: false,
                        cache:false,
                        async:false,

                        success: function (data) {
                            $("#loaderDiv").hide();
                            $(".pull-left").show();

                            // console.log('Submission was successful.');
                            console.log(data);
                            hideForms();
                            location.reload();

                        },
                        error: function (data) {
                          console.log('An error occurred.');
                          console.log(data);
                      },
                  });
    });



     get_all_list();




     $(".dataTables_filter ").addClass('pull-left');
     $(".dataTables_filter input").addClass('admin_border');

     $(".dataTables_filter").css("display", "block");
     $(".dataTables_filter input").css("max-width", "78% !important");
     $(".dataTables_filter input").css("width", "100% !important");




 });


    function select_check(checkbox)
    {
        var checkboxes = document.getElementsByName('listCheck')
        checkboxes.forEach((item) => {
            if (item !== checkbox) item.checked = false
        })
    }


</script>