

<section id="content">
	<div class="listDiv">
		<?php $this->load->view('admin/common/breadcrumbs'); ?>

		<div class="row-fluid">
			<?php   $flashAlert = $this->session->flashdata('alert');
			if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
				?>
				<br>
				<div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
					<strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
					<button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			<?php   } ?>


			<div class="module">
				<?php echo $this->session->flashdata('message'); ?>

       <div class="clearfix"></div>
       <div class="module-body table-responsive">
        <table id="cms_listing" class="table table-bordered table-striped  table-hover dataTable" cellspacing="0" width="100%" data-selected_id="">
         <thead>
          <tr>
           <th class="no-sort text-center"><input type='checkbox' name='allClients' id="allClients" value='' onclick="Javascript:selectAllClients();" /></th>
           <th class="column-id"><?php echo $this->lang->line('id'); ?></th>
           <th class="column-id">Modification Date</th>
           <th class="column-status">Added By</th>
           <th class="column-service_category" ><?php echo $this->lang->line('service_category'); ?></th>
           <th class="column-type">Type</th>
           <th class="column-name">Package Price</th>
           <th class="column-image">Minimum Price</th>
           <th class="column-image">Night Price</th>
           <th class="column-image">No Working Price</th>
           <th class="column-status"><?php echo $this->lang->line('status'); ?></th>
           <th class="column-id">Publish Date</th>

           <th class="column-status"><?php echo $this->lang->line('since'); ?></th>                         				
         </tr>
       </thead>


                         		<!-- <tbody id="list_services">

                         		</tbody> -->
                         	</table>
                         </div>
                       </div>
                     </div>
                   </div>
                   <div class="addDiv" style="display:none; padding-left: 1%;
padding-right: 1%;">
                  
                    <?php echo form_open_multipart('cms/add_new_services', array("id" => "new_service_form")); ?>

                    <div class="row">
                     <div class="col-md-12">
                      <div class="row">


                  <?php 

                  $data = [
                    'type'  => 'hidden',
                    'name'  => 'page_type',
                    'id'    => 'page_type',
                    'value' => 'prices',
                  ];

                  echo form_input($data);


                  $data = [
                    'type'  => 'hidden',
                    'name'  => 'id',
                    'id'    => 'id',
                    'value' => '',
                  ];

                  echo form_input($data);

                  $data = [
                    'type'  => 'hidden',
                    'name'  => 'services_id',
                    'id'    => 'services_id',
                    'value' => '',
                  ];

                  echo form_input($data);

                  ?> 

                       <div class="col-md-3 form-group">  
                        <label> Status </label>                                
                        <select class="form-control" name="status" id="status" tabindex="1">
                         <option value="1">Enable</option>
                         <option value="2">Disable</option>
                       </select>
                     </div>
                       <div class="col-md-3 form-group">  
                        <label> Type </label>                                
                        <select required class="form-control" name="price_type" id="price_type" tabindex="1">
                         <option value="" >Select type</option>
                         <option value="price">Price</option>
                         <option value="description">Description</option>
                       </select>
                     </div>

                     <div id="dynamic_box">
                       
                     </div>





           <div class="row description_box" style="display:none">
            <div style="padding-left: 1.5%;padding-right:1.7%;" class="col-xs-12">
              <div class="form-group">
                <label>Description*</label>
                <textarea class="ckeditor" required="required" id="editor1" name="description" cols="100" rows="12">
                </textarea>
              </div>
            </div>
          </div>




        </div>
      </div>











    </div>


    <div class="row">
      <div class="col-md-12"> 
       <div class="pull-right" style="">
        <input type="button" class="btn btn-default fa fa-input" value="&#xf05e Cancel" onclick="return hideAdd();" tabindex="17" />  
        <input type="submit" class="btn btn-default fa fa-input" id="save" value="&#xf0c7 Save" tabindex="16" /> 


      </div>
    </div>
  </div>
  <?php echo form_close(); ?>
</div>
</section>
</div>




<div class="price_box" style="display:none">
  
                     <div class="col-md-3 form-group">  
                      <label for="category_id"> Service Category </label>                                
                      <select required="required" class="form-control" name="category_id" id="category_id" tabindex="1">
                       <option >Select Category</option>    

                       <?php

                       foreach ($category as $key => $value)
                       {
                        echo '<option value="'.$value->id.'">'.$value->name.'</option>';
                      }

                      ?>  
                    </select>
                  </div>


                  <div class="col-md-3 form-group">  
                    <label> Service </label>                                
                    <?php 
                    $services = array("select");
                    echo form_dropdown('services', $services,'', 'id="services" class="form-control services" required ');

                    ?> 
                  </div>








                  <div class="col-md-3 form-group">  
                    <label> Language </label>                                
                    <?php 
                    $language = array("en" => "English", "fr" => "Francais ");
                    echo form_dropdown('language', $language,'', 'id="language" class="form-control language"');

                    ?> 
                  </div>






                  <div class="col-md-3 form-group">  
                    <label> Package Price</label>                                
                    <?php 

                    $data = [
                     'type'  => 'number',
                     'name'  => 'p_price',
                     'id'    => 'p_price',
                     'value' => '',
                     'required'=>'required',
                     'step'=>".01",

                     'class' => 'form-control p_price'
                   ];

                   echo form_input($data);

                   ?> 
                 </div> 



                 <div class="col-md-3 form-group">  
                  <label> Minimum Price</label>                                
                  <?php 

                  $data = [
                   'type'  => 'number',
                   'name'  => 'm_price',
                   'id'    => 'm_price',
                   'value' => '',
                   'required'=>'required',
                   'step'=>".01",

                   'class' => 'form-control m_price'
                 ];

                 echo form_input($data);

                 ?> 
               </div> 



               <div class="col-md-3 form-group">  
                <label> Night Price</label>                                
                <?php 

                $data = [
                 'type'  => 'number',
                 'name'  => 'n_price',
                 'id'    => 'n_price',
                 'value' => '',
                 'required'=>'required',
                 'step'=>".01",

                 'class' => 'form-control n_price'
               ];

               echo form_input($data);

               ?> 
             </div> 



             <div class="col-md-3 form-group">  
              <label> No Working Price</label>                                
              <?php 

              $data = [
               'type'  => 'number',
               'name'  => 'nw_price',
               'id'    => 'nw_price',
               'value' => '',
               'required'=>'required',
               'step'=>".01",

               'class' => 'form-control nw_price'
             ];

             echo form_input($data);

             ?> 
           </div> 

</div>

<script>

 function showAdd() {
  hideForms();
  $('.addDiv').trigger('reset');

  $('.addDiv').show();
  $('.breadcrumb').append('<span id="subtitle_custom"> > Price Add page</span>');

        //showAddHead();
      }

   function hideForms() {
        $('.addDiv').hide();
        $('.listDiv').find('.row-fluid').hide();
        $('#subtitle_custom').remove(); 

    }
    function hideAdd() {
        hideForms();
        $('.listDiv').find('.row-fluid').show();
    }



      function delete_item(id=null)
      {
       if(id==null)
       {

        $.each($("input[name='listCheck']:checked"), function(){
         id= $(this).val();
       });

      }

      if (id === null)
      {
        alert('No row item selected!!');
        return false;
      }


      if(confirm("Are you sure to delete?"))
      {

        $.ajax({
         url:'ajax_delete_cms_listing.php',
         method: 'post',
         data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&id='+id,
         dataType: 'json',
         success: function(response){

           location.reload();

         }


       });
      }


    }






    function edit_item(id=null,display_id=null)
    {
     if(id==null)
     {

      $.each($("input[name='listCheck']:checked"), function(){
       id= $(this).val();
       display_id = $(this).data("id");
       
     });

    }

    if (id === null)
    {
      alert('No row item selected!!');
      return false;
    }


    hideForms();

    //  var $form = $('.addDiv').clone();
    //  $('.edit_div').html($form);
    // $('.edit_div').show();
    $('.addDiv').show();
    $('.breadcrumb').append('<span id="subtitle_custom"> > '+display_id+'</span>');



    $('#new_service_form').attr('action','ajax_update_cms_listing.php');
    $('#img').removeAttr('required');


    services_id=$('#services_id').val();
    $.ajax({
      url:'ajax_get_cms_listing.php',
      method: 'post',
      data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&post_type=prices&id='+id+'&services_id='+services_id,
      dataType: 'json',
      success: function(response){

        console.log(response);

        CKEDITOR.instances.editor1.setData(response.description);
        $('#name').val(response.name);

            // $('#title').val(response.title);
            $('#language').val(response.language);
            $('#link').val(response.link);
            $('#id').val(response.id);


            // $('#category_id').val(response.category_id);
            
            $("#category_id").val(response.category_id).attr("selected","selected");

            get_services_base_category(response.services_id);


            $('#p_price').val(response.p_price);
            $('#m_price').val(response.m_price);
            $('#n_price').val(response.n_price);
            $('#nw_price').val(response.nw_price);

            $('#status').val(response.status);
            $('#price_type').val(response.price_type);

            if(response.price_type=='price')
            {
               var price_box=$(".price_box div").clone();
              $('#dynamic_box').html(price_box);
              $('.description_box').hide();
            }
            if(response.price_type=='description')
            {
              $('#dynamic_box').html('');
             
              $('.description_box').show();
            }






          }


        });

  }



  $(document).ready(function() {




// ajax_get_all_cms_listing

$('#category_id').change(function(){

  get_services_base_category();

});



})


  function get_services_base_category(services_select=null)
  {

    cat_id=$('#category_id').val();


    $.ajax({
      url:'ajax_get_cms_listing.php',
      method: 'post',
      data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&post_type=services&cat_id='+cat_id,
      dataType: 'json',
      success: function(response){
        // console.log(response);
        html ='';

        $.each(response, function( index, value ) {
          // console.log( value );
          if(services_select==value.id)
          {
            html =html + '<option selected="true" value="'+value.id+'">'+value.title+'</option>';

          }
          else{
            html =html + '<option selected="true" value="'+value.id+'">'+value.title+'</option>';

          }
        });

        $('#services').html(html);


      }


    });

  }




    // DATATABLE

    function get_all_list()
    {

    	var oTable = $('#cms_listing').on('preXhr.dt', function (e, settings, data) {
            $("#loaderTable").show();
        }).DataTable({
    		'processing': true,
    		'serverSide': true, 
    		"dom": "<'row'<'col-sm-4'l><'col-sm-4 statusDropdown'><'col-sm-4'f>>" +
    		"<'row'<'col-sm-12't>>" +
    		"<'row'<'col-sm-6'i><'col-sm-6'p>>",
    		'ajax': {
    			url:'services_list_ajax_data_set',
    			type: 'post',

          data: {
           'post_type': 'prices',
           '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',
         },
         dataType: 'json',
       },
        "initComplete":function( settings, json){
            $("#loaderTable").hide();
        },
       'columns': [
       { data: 'created_date' },
       { data: 'id' },
       { data: 'modification_date' },
       { data: 'username' },
       { data: 'category_name' },
       { data: 'price_type' },
       { data: 'p_price' },
       { data: 'm_price' },
       { data: 'n_price' },
       { data: 'nw_price' },

       { data: 'status' },
       { data: 'publish_date' },

       { data: 'since' },            
       ],
       columnDefs: [
       { className: "text-center", targets: "_all" },
       ],

       dom: 'lBfrtip',

       buttons: [


       {
         text: ' <span href="javascript:;" class="" onclick="showAdd();"><i class="fa fa-plus"></i> Add</span> ',
         title: '',
       },


       {
         text: ' <span href="javascript:;" class="" onclick="edit_item()"><i class="fa fa-pencil"></i> Edit</span> ',
         title: '',
       },


       {
         text: ' <span href="javascript:;" class="" onclick="delete_item()"><i class="fa fa-trash"></i> Delete</span> ',
         title: '',
       },



       {
         extend: 'excel',
         text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel Export',
         filename: 'members',
         title: '',
         exportOptions: {
          modifier: {
           search: 'applied',
           order: 'applied',
           page: 'current'
         },
         columns: [1, 2, 3]
       }
     },
     {
       extend: 'csv',
       text: '<i class="fa fa-file-text-o" aria-hidden="true"></i> Export CSV',
       filename: 'members',
       title: '',
       exportOptions: {
        modifier: {
         search: 'applied',
         order: 'applied',
         page: 'current'
       },
       columns: [1, 2, 3, 4, 5]
     }
   },
   {
     extend: 'pdf',
     text: '<i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF',
     filename: 'members',
     title: '',
     exportOptions: {
      modifier: {
       search: 'applied',
       order: 'applied',
       page: 'current'
     },
     columns: [1, 2, 3, 4, 5]
   }
 },


 ],
 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]




});  
    } 




    function old_get_all_list()
    {

    	$.ajax({
    		url:'ajax_get_all_cms_listing.php',
    		method: 'post',
    		data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&post_type=services',
    		dataType: 'json',
    		success: function(response){

    			var html ='';
    			var count =1;
    			response.forEach(function(item) {
    				var odd_even =(count%2==0)?'even':'odd';
    				var active =(item.status=="1")?'Enabled':'Disabled';
    				var name =$.trim(item.name);
    				console.log(name);
    				var edit_form ='onclick="edit_category_form('+item.id+')"';

    				html =html +'<tr role="row" class="'+odd_even+'">'+
    				'<td class="text-center"><input type="checkbox" class="ccheckbox" value="'+item.id+'"></td>'+
    				'<td class="text-center">'+item.id+'</td>'+

    				'<td class="text-center">'+item.category_name+'</td>'+
    				'<td class="text-center">'+item.title+'</td>'+
    				'<td class="text-center">'+item.name+'</td>'+
    				'<td class="text-center">'+
    				'<img width="50px" alter="No Image" src="<?php echo base_url();?>/uploads/cms/'+item.image+'">'+
    				'</td>'+
    				'<td class="text-center">'+item.username+'</td>'+


    				'<td class="text-center"><span class="label label-success">'+active+'</span></td>'+
    				'<td class="text-center"><button '+edit_form+' type="button" class="btn btn-primary">EDIT</button> <button onclick="delete_item('+item.id+')"  type="button" class="btn btn-danger">Delete</button></td>'+														
    				'</tr>'
    				;



    				count++;
    			});

    			$('#list_services').html(html);

    		}


    	});

    }



    $(document).ready(function() {
     var frm = $('#new_service_form');

     frm.submit(function (e) {

         var select_item = $("#services").children("option:selected").val();

        $("#services_id").val(select_item);


      $("#loaderDiv").show();
      $(".pull-left").hide();

      $('#editor1').val(CKEDITOR.instances.editor1.getData());

      e.preventDefault();


      $.ajax({
       type: frm.attr('method'),
       url: frm.attr('action'),
                        // data: frm.serialize(),
                        data: new FormData(this),
                        contentType: false, //must, tell jQuery not to process the data
                        processData: false,
                        cache:false,
                        async:false,

                        success: function (data) {
                          $("#loaderDiv").hide();
                          $(".pull-left").show();

                            // console.log('Submission was successful.');
                            console.log(data);
                            hideForms();
                            location.reload();

                          },
                          error: function (data) {
                            console.log('An error occurred.');
                            console.log(data);
                          },
                        });
    });



     get_all_list();




     $(".dataTables_filter ").addClass('pull-left');
     $(".dataTables_filter input").addClass('admin_border');

     $(".dataTables_filter").css("display", "block");
     $(".dataTables_filter input").css("max-width", "78% !important");
     $(".dataTables_filter input").css("width", "100% !important");





   });


    function select_check(checkbox)
    {
      var checkboxes = document.getElementsByName('listCheck')
      checkboxes.forEach((item) => {
        if (item !== checkbox) item.checked = false
      })
    }



    $(document).ready(function(){

      $("#services").change(function(){

        $("#services").removeAttr("selected");

        var select_item = $(this).children("option:selected").val();

        $("#services_id").val(select_item);

      });
      $("#price_type").change(function(){

        $("#price_type").removeAttr("selected");

        var select_item = $(this).children("option:selected").val();
if(select_item=='price')
{
  // $('.price_box').show();
  $('.description_box').hide();
  var price_box=$(".price_box div").clone();
  $('#dynamic_box').html(price_box);

}
if(select_item=='description')
{
              $('#dynamic_box').html('');
  
  // $('.price_box').hide();
  $('.description_box').show();
}
      });

    });



  </script>