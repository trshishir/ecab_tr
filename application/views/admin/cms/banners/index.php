
<?php

 extract((array)$banners);

?>
<section id="content">
	<div class="listDiv">
		<?php $this->load->view('admin/common/breadcrumbs'); ?>

		<div class="row-fluid">


			<div class="col-md-12 admin_border custom_gray_bg" style="margin-top:5px; margin-bottom:5px">

				<h3 class="client_header_title">Client Section</h3>
				<?php echo form_open_multipart('cms/add_cms_banner', array("id" => "client_banner_form")); ?>

	
				<?php $client_section= ($client_section)? base_url('uploads/cms').'/'.$client_section:'';?>

             				<?php 

             				$data = [
             					'type'  => 'hidden',
             					'name'  => 'section_name',
             					'id'    => 'section_name',
             					'value' => 'client_section ',
             				];

             				echo form_input($data);
             				?>

				<div class="col-md-12 form-group">  

					<div class="form-group col-md-2">
						<input type="file" id="img" name="img" required="required">
						<span><?php echo basename($client_section);?></span>

					</div>
					<div class="form-group col-md-2">
						<input type="submit" class="btn btn-default fa fa-input" id="save" value="&#xf0c7 Upload banner" tabindex="16" /> 
					</div>
			<!-- 		<div class="form-group col-md-4">
						<label for="description">Link: </label>
						<span><a href="<?php echo $client_section;?>"><?php echo $client_section;?></a></span>
					</div> -->
				</div>

				<div class="col-md-12">
					<img class="admin_border" id="img_preview" style="min-width: 100% !important;  width: 100%;" src="<?php echo $client_section;?>">

				</div>



				<?php echo form_close(); ?>

				
			</div>



			<div class="col-md-12 admin_border custom_gray_bg" style="margin-top:5px; margin-bottom:5px">

				<h3 class="client_header_title">Driver Section</h3>
				<?php echo form_open_multipart('cms/add_cms_banner', array("id" => "client_banner_form")); ?>

	
				<?php $driver_section= ($driver_section)? base_url('uploads/cms').'/'.$driver_section:'';?>

             				<?php 

             				$data = [
             					'type'  => 'hidden',
             					'name'  => 'section_name',
             					'id'    => 'section_name',
             					'value' => 'driver_section ',
             				];

             				echo form_input($data);
             				?>

				<div class="col-md-12 form-group">  

					<div class="form-group col-md-2">
						<input type="file" id="img" name="img" required="required">
						<span><?php echo basename($driver_section);?></span>

					</div>
					<div class="form-group col-md-2">
						<input type="submit" class="btn btn-default fa fa-input" id="save" value="&#xf0c7 Upload banner" tabindex="16" /> 

					</div>
<!-- 					<div class="form-group col-md-4">
						<label for="description">Link: </label>
						<span><a href="<?php echo $driver_section;?>"><?php echo $driver_section;?></a></span>
					</div> -->
				</div>

				<div class="col-md-12">
					<img class="admin_border" id="img_preview" style="min-width: 100% !important; width: 100%;" src="<?php echo $driver_section;?>">

				</div>



				<?php echo form_close(); ?>

				
			</div>



			<div class="col-md-12 admin_border custom_gray_bg" style="margin-top:5px; margin-bottom:5px">

				<h3 class="client_header_title">Partner Section</h3>
				<?php echo form_open_multipart('cms/add_cms_banner', array("id" => "client_banner_form")); ?>

	
				<?php $partner_section= ($partner_section)? base_url('uploads/cms').'/'.$partner_section:'';?>

             				<?php 

             				$data = [
             					'type'  => 'hidden',
             					'name'  => 'section_name',
             					'id'    => 'section_name',
             					'value' => 'partner_section ',
             				];

             				echo form_input($data);
             				?>

				<div class="col-md-12 form-group">  

					<div class="form-group col-md-2">
						<input type="file" id="img" name="img" required="required">
						<span><?php echo basename($partner_section);?></span>

					</div>
					<div class="form-group col-md-2">
						<input type="submit" class="btn btn-default fa fa-input" id="save" value="&#xf0c7 Upload banner" tabindex="16" /> 

					</div>
<!-- 					<div class="form-group col-md-4">
						<label for="description">Link: </label>
						<span><a href="<?php echo $partner_section;?>"><?php echo $partner_section;?></a></span>
					</div> -->
				</div>

				<div class="col-md-12">
					<img class="admin_border" id="img_preview" style="min-width: 100% !important; width: 100%;" src="<?php echo $partner_section;?>">

				</div>



				<?php echo form_close(); ?>

				
			</div>



			<div class="col-md-12 admin_border custom_gray_bg" style="margin-top:5px; margin-bottom:5px">

				<h3 class="client_header_title">Jobs Section</h3>
				<?php echo form_open_multipart('cms/add_cms_banner', array("id" => "client_banner_form")); ?>

	
				<?php $jobs_section= ($jobs_section)? base_url('uploads/cms').'/'.$jobs_section:'';?>

             				<?php 

             				$data = [
             					'type'  => 'hidden',
             					'name'  => 'section_name',
             					'id'    => 'section_name',
             					'value' => 'jobs_section ',
             				];

             				echo form_input($data);
             				?>

				<div class="col-md-12 form-group ">  

					<div class="form-group col-md-2">
						<input type="file" id="img" name="img" required="required">
						<span><?php echo basename($jobs_section);?></span>

					</div>
					<div class="form-group col-md-2">
						<input type="submit" class="btn btn-default fa fa-input" id="save" value="&#xf0c7 Upload banner" tabindex="16" /> 

					</div>
<!-- 					<div class="form-group col-md-4">
						<label for="description">Link: </label>
						<span><a href="<?php echo $jobs_section;?>"><?php echo $jobs_section;?></a></span>
					</div> -->
				</div>

				<div class="col-md-12">
					<img class="admin_border" id="img_preview" style="min-width: 100% !important;  width: 100%;" src="<?php echo $jobs_section;?>">

				</div>



				<?php echo form_close(); ?>

				
			</div>



		</div>
	</div>
</section>