
<?php

 extract((array)$flashnews);

?>
<section id="content">

	<div class="listDiv">
		<?php $this->load->view('admin/common/breadcrumbs'); ?>

    <div class="row" style="left: 1%;">
      <div class="col-md-12">
                    <?php $this->load->view('admin/common/alert');?>
      </div>
    </div>



		<div class="row">


			<div class="col-md-12">

				<?php echo form_open_multipart('cms/add_cms_flash_news', array("id" => "client_banner_form")); ?>

	


                  <div class="col-md-12 form-group">  
                    <label> Home Page </label>                                
                    <?php 

                    $data = [
                     'type'  => 'text',
                     'name'  => 'home',
                     'id'    => 'home',
                     'value' => $home,
                     'placeholder' => 'Home page flash news',

                     'class' => 'form-control title'
                   ];

                   echo form_input($data);

                   ?> 
                 </div> 


                  <div class="col-md-12 form-group">  
                    <label> Driver Page </label>                                
                    <?php 

                    $data = [
                     'type'  => 'text',
                     'name'  => 'driver',
                     'id'    => 'driver',
                     'value' => $driver,
                     'placeholder' => 'Driver page flash news',

                     'class' => 'form-control title'
                   ];

                   echo form_input($data);

                   ?> 
                 </div> 


                  <div class="col-md-12 form-group">  
                    <label> Partner Page </label>                                
                    <?php 

                    $data = [
                     'type'  => 'text',
                     'name'  => 'partner',
                     'id'    => 'partner',
                     'value' => $partner,
                     'placeholder' => 'Partner page flash news',

                     'class' => 'form-control title'
                   ];

                   echo form_input($data);

                   ?> 
                 </div> 


                  <div class="col-md-12 form-group">  
                    <label> Jobs Page </label>                                
                    <?php 

                    $data = [
                     'type'  => 'text',
                     'name'  => 'jobs',
                     'id'    => 'jobs',
                     'value' => $jobs,
                     'placeholder' => 'Jobs page flash news',

                     'class' => 'form-control title'
                   ];

                   echo form_input($data);

                   ?> 
                 </div> 

                 <div class="row">
    <div class="col-md-12"> 
     <div class="pull-right" style="padding-right: 1%">
        <!-- <input type="button" class="btn btn-default fa fa-input" value="&#xf05e Cancel" onclick="return hideAdd();" tabindex="17" />   -->
      <input type="submit" class="btn btn-default fa fa-input" id="save" value="&#xf0c7 Save" tabindex="16" /> 

  </div>
</div>
</div>


				<?php echo form_close(); ?>

				
			</div>

			</div>
			</div>

		</section>