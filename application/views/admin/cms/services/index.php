<style type="text/css">.btn span.glyphicon {
        opacity: 0;
    }
    .btn.active span.glyphicon {
        opacity: 1;
    }</style>

<section id="content">
    <div class="listDiv">
        <?php $this->load->view('admin/common/breadcrumbs'); ?>


        <div class="row-fluid">
            <?php   $flashAlert = $this->session->flashdata('alert');
            if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
                ?>
                <br>
                <div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
                    <strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
                    <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php   } ?>



            <div class="module">
                <?php echo $this->session->flashdata('message'); ?>

                <div class="clearfix"></div>
                <div class="module-body table-responsive">
                    <table id="cms_listing" class="table table-bordered table-striped  table-hover dataTable" cellspacing="0" width="100%" data-selected_id="">
                        <thead>
                        <tr>
                            <th class="no-sort text-center"><input type='checkbox' name='allClients' id="allClients" value='' onclick="Javascript:selectAllClients();" /></th>
                            <th class="column-id"><?php echo $this->lang->line('id'); ?></th>
                            <th class="column-id"><?php echo $this->lang->line('date'); ?></th>
                            <th class="column-id"><?php echo $this->lang->line('time'); ?></th>
                            <th class="column-status">Added By</th>

                            <th class="column-service_category" ><?php echo $this->lang->line('service_category'); ?></th>
                            <th class="column-name"><?php echo $this->lang->line('name'); ?></th>
                            <th class="column-image"><?php echo $this->lang->line('image'); ?></th>
                            <th class="column-status"><?php echo $this->lang->line('status'); ?></th>
                            <th class="column-status"><?php echo $this->lang->line('since'); ?></th>
                        </tr>
                        </thead>


                        <!-- <tbody id="list_services">

                        </tbody> -->
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="addDiv" style="display:none; padding-right:1%;">


        <?php echo form_open_multipart('cms/add_new_services', array("id" => "new_service_form")); ?>

        <div class="row">
            <div class="col-md-12">
                <div class="col-md-8">
                    <div class="col-md-3 form-group">
                        <label> Status </label>
                        <select class="form-control" name="status" id="status" tabindex="1">
                            <option value="1">Enable</option>
                            <option value="2">Disable</option>
                        </select>
                    </div>

                    <div class="col-md-3 form-group">
                        <label for="category_id"> Service Category </label>
                        <select class="form-control" name="category_id" id="category_id" tabindex="1">
                            <option disabled>--Select Category</option>

                            <?php

                            foreach ($category as $key => $value)
                            {
                                echo '<option value="'.$value->id.'">'.$value->name.'</option>';
                            }

                            ?>
                        </select>
                    </div>


                    <?php

                    $data = [
                        'type'  => 'hidden',
                        'name'  => 'page_type',
                        'id'    => 'page_type',
                        'value' => 'services',
                    ];

                    echo form_input($data);


                    echo form_input($data);
                    $data = [
                        'type'  => 'hidden',
                        'name'  => 'id',
                        'id'    => 'id',
                        'value' => '',
                    ];

                    echo form_input($data);

                    ?>
                    <!--                           <div class="col-md-3 form-group">
                              <label> Title </label>                                
                              <?php

                    //    $data = [
                    //     'type'  => 'text',
                    //     'name'  => 'title',
                    //     'id'    => 'title',
                    //     'value' => '',
                    //     'placeholder' => 'Title *',
                    //     'required'=>'required',
                    //     'class' => 'title'
                    // ];

                    // echo form_input($data);

                    ?>
                         </div> -->




                    <div class="col-md-3 form-group">
                        <label> Language </label>
                        <?php
                        $language = array("en" => "English", "fr" => "Francais ");
                        echo form_dropdown('language', $language,'', 'id="language" class="form-control language"');

                        ?>
                    </div>



                    <div class="col-md-3 " style="margin-left: 0px;">
                        <label> Rubens </label>
                        <div class="form-group btn-group rubens" >


                            <label class="btn btn-success " style="padding-right: 36px; background:#018201">
                                <input id="rubens_new" name="rubens_new" style="margin: 18%;" type="checkbox" >
                                <span class="">NEW</span>
                            </label>

                            <label class="btn btn-primary" style="padding-right: 36px; background:#FF0000">
                                <input id="rubens_promo" name=" rubens_promo" style="margin: 12%;" type="checkbox">
                                <span class="">PROMO</span>
                            </label>

                        </div>
                    </div>


                    <div class="col-md-3 form-group">
                        <label> Name* </label>
                        <?php

                        $data = [
                            'type'  => 'text',
                            'name'  => 'name',
                            'id'    => 'name',
                            'value' => '',
                            'placeholder' => 'Name*',
                            'required'=>'required',

                            'class' => ''
                        ];

                        echo form_input($data);

                        ?>
                    </div>

                    <div class="col-md-4 form-group">
                        <label> Title* </label>
                        <?php

                        $data = [
                            'type'  => 'text',
                            'name'  => 'title',
                            'id'    => 'title',
                            'value' => '',
                            'placeholder' => 'Title*',
                            'required'=>'required',

                            'class' => 'title'
                        ];

                        echo form_input($data);

                        ?>
                    </div>



                    <div class="">




                        <div class="col-md-2 form-group ">
                            <label> Price Start From </label>
                            <div class="form-group">
                                <select class="form-control" name="is_price_start_from" id="is_price_start_from" tabindex="1">
                                    <option value="1">Enable</option>
                                    <option value="2">Disable</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-2 form-group" style="display:block;">
                            <label> Price </label>
                            <div class="form-group">
                                <input type="number" name="start_from_price" id="start_from_price">
                            </div>
                        </div>

                        <div class="col-md-12 form-group">
                            <label> Short Description* </label>
                            <?php

                            $data = [
                                'name'        => 'short_description',
                                'id'          => 'short_description',
                                'value'       => set_value(''),
                                'rows'        => '5',
                                'cols'        => '50',
                                'style'       => 'width:100%',
                                'required'=>'required',

                                'class'       => 'form-control'
                            ];

                            echo form_textarea($data);

                            ?>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>Description*</label>
                                <textarea class="ckeditor" required="required" id="editor1" name="description" cols="100" rows="12">
                      </textarea>
                            </div>
                        </div>


                    </div>


                </div>





                <div class="col-md-4">


                    <div class="col-md-12 form-group">
                        <label> Link </label>

                        <input type="text" name="link_url" id="link_url">

                    </div>



                    <div class="col-md-12 form-group">
                        <label> Extension*(.php/.HTML)</label>
                        <?php
                        $link = array(".html" => ".html", ".php" => ".php ");
                        echo form_dropdown('link', $link,'', 'id="link" class="form-control link" required="required" ');

                        ?>
                    </div>


                    <div style=" margin-top: -1px;" class="col-xs-12">

                        <div class="col-md-12 form-group">

                            <div class="form-group">
                                <img id="img_preview" width="175px" style="min-height:100px" src="<?php echo base_url();?>uploads/cms/no-preview.jpg"><br>

                                <br>
                                <label for="description">Upload*</label>
                                <input type="file" id="img" name="img" required="required">
                            </div>
                        </div>


                        <div class="col-md-12 form-group">
                            <label> Meta Description* </label>
                            <?php

                            $data = [
                                'name'        => 'meta_description',
                                'id'          => 'meta_description',
                                'value'       => set_value(''),
                                'rows'        => '5',
                                'cols'        => '50',
                                'style'       => 'width:100%',
                                'required'=>'required',

                                'class'       => 'form-control'
                            ];

                            echo form_textarea($data);

                            ?>
                        </div>



                        <div class="col-md-12 form-group">
                            <label> Meta Tag* </label>
                            <?php

                            $data = [
                                'name'        => 'meta_tag',
                                'id'          => 'meta_tag',
                                'value'       => set_value(''),
                                'rows'        => '2',
                                'cols'        => '100',
                                'style'       => 'width:100%',
                                'required'=>'required',

                                'class'       => 'form-control'
                            ];

                            echo form_textarea($data);

                            ?>
                        </div>




                    </div>
                </div>


            </div>

            <div class="">
                <div class="col-md-12">
                    <div class="pull-right" style="padding-right:1.5%;">
                        <input type="button" class="btn btn-default fa fa-input" value="&#xf05e Cancel" onclick="return hideAdd();" tabindex="17" />
                        <input type="submit" class="btn btn-default fa fa-input" id="save" value="&#xf0c7 Save" tabindex="16" />


                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
</section>
</div>

<script>

    function showAdd() {
        hideForms();
        $('#new_service_form').trigger("reset");
        CKEDITOR.instances.editor1.setData("");
        $('.addDiv').show();
        $('#img_preview').attr('src','<?php echo base_url();?>uploads/cms/no-preview.jpg');
        $('.breadcrumb').append('<span id="subtitle_custom"> > Add New service</span>');

        //showAddHead();
    }
    function hideForms() {
        $('.addDiv').hide();
        $('.listDiv').find('.row-fluid').hide();
        $('#subtitle_custom').remove();

    }
    function hideAdd() {
        hideForms();
        $('.listDiv').find('.row-fluid').show();
    }



    function delete_item(id=null)
    {
        if(id==null)
        {

            $.each($("input[name='listCheck']:checked"), function(){
                id= $(this).val();
            });

        }

        if (id === null)
        {
            alert('No row item selected!!');
            return false;
        }


        if(confirm("Are you sure to delete?"))
        {

            $.ajax({
                url:'ajax_delete_cms_listing.php',
                method: 'post',
                data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&id='+id,
                dataType: 'json',
                success: function(response){

                    location.reload();

                }


            });
        }


    }






    function edit_item(id=null,display_id=null)
    {
        if(id==null)
        {

            $.each($("input[name='listCheck']:checked"), function(){
                id= $(this).val();
                display_id = $(this).data("id");
            });

        }

        if (id === null)
        {
            alert('No row item selected!!');
            return false;
        }


        hideForms();

        //  var $form = $('.addDiv').clone();
        //  $('.edit_div').html($form);
        // $('.edit_div').show();
        $('.addDiv').show();
        $('.breadcrumb').append('<span id="subtitle_custom"> > '+display_id+'</span>');


        $('#new_service_form').attr('action','ajax_update_cms_listing.php');
        $('#img').removeAttr('required');




        $.ajax({
            url:'ajax_get_cms_listing.php',
            method: 'post',
            data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&post_type=services&id='+id,
            dataType: 'json',
            success: function(response){
                console.log(response);

                CKEDITOR.instances.editor1.setData(response.description);
                $('#name').val(response.name);
                $('#title').val(response.title);

                // $('#title').val(response.title);
                $('#language').val(response.language);
                $('#link').val(response.link);
                $('#link_url').val(response.link_url);
                $('#id').val(response.id);
                $('#meta_tag').val(response.meta_tag);
                $('#meta_description').val(response.meta_description);
                $('#short_description').val(response.short_description);
                var base_url ="<?php echo  base_url('uploads/cms').'/';?>";
                $('#img_preview').attr('src','<?php echo base_url();?>uploads/cms/'+response.image);

                $('#category_id').val(response.category_id);
                // $('#rubens_new').val(response.rubens_new);
                // $('#rubens_promo').val(response.rubens_promo);

                $('#rubens_new').removeAttr('checked')
                $('#rubens_promo').removeAttr('checked')
                if(response.rubens_new==1)
                {
                    $('#rubens_new').each(function(){ this.checked = true; });
                }
                else{
                    $('#rubens_new').each(function(){ this.checked = false; });

                }
                if(response.rubens_promo==1)
                {
                    $('#rubens_promo').each(function(){ this.checked = true; });
                }
                else{
                    $('#rubens_promo').each(function(){ this.checked = false; });

                }


                $('#is_price_start_from').val(response.is_price_start_from);
                $('#start_from_price').val(response.start_from_price);
                $('#status').val(response.status);


            }


        });

    }












    // DATATABLE

    function get_all_list()
    {

        var oTable = $('#cms_listing').on('preXhr.dt', function (e, settings, data) {
            $("#loaderTable").show();
        }).DataTable({
            'processing': true,
            'serverSide': true,
            "dom": "<'row'<'col-sm-4'l><'col-sm-4 statusDropdown'><'col-sm-4'f>>" +
                "<'row'<'col-sm-12't>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            'ajax': {
                url:'services_list_ajax_data_set',
                type: 'post',
                // data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&post_type=services',

                data: {
                    'post_type': 'services',
                    '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',
                },
                dataType: 'json',
            },
            "initComplete":function( settings, json){
                $("#loaderTable").hide();
            },
            'columns': [
                { data: 'created_date' },
                { data: 'id' },
                { data: 'date' },
                { data: 'time' },
                { data: 'username' },

                { data: 'category_name' },
                { data: 'name' },
                { data: 'image' },
                { data: 'status' },
                { data: 'since' },
            ],
            columnDefs: [
                { className: "text-center", targets: "_all" },
            ],

            dom: 'lBfrtip',

            buttons: [


                {
                    text: ' <span href="javascript:;" class="" onclick="showAdd();"><i class="fa fa-plus"></i> Add</span> ',
                    title: '',
                },


                {
                    text: ' <span href="javascript:;" class="" onclick="edit_item()"><i class="fa fa-pencil"></i> Edit</span> ',
                    title: '',
                },


                {
                    text: ' <span href="javascript:;" class="" onclick="delete_item()"><i class="fa fa-trash"></i> Delete</span> ',
                    title: '',
                },



                {
                    extend: 'excel',
                    text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel Export',
                    filename: 'members',
                    title: '',
                    exportOptions: {
                        modifier: {
                            search: 'applied',
                            order: 'applied',
                            page: 'current'
                        },
                        columns: [1, 2, 3]
                    }
                },
                {
                    extend: 'csv',
                    text: '<i class="fa fa-file-text-o" aria-hidden="true"></i> Export CSV',
                    filename: 'members',
                    title: '',
                    exportOptions: {
                        modifier: {
                            search: 'applied',
                            order: 'applied',
                            page: 'current'
                        },
                        columns: [1, 2, 3, 4, 5]
                    }
                },
                {
                    extend: 'pdf',
                    text: '<i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF',
                    filename: 'members',
                    title: '',
                    exportOptions: {
                        modifier: {
                            search: 'applied',
                            order: 'applied',
                            page: 'current'
                        },
                        columns: [1, 2, 3, 4, 5]
                    }
                },



            ],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]




        });
    }




    function old_get_all_list()
    {

        $.ajax({
            url:'ajax_get_all_cms_listing.php',
            method: 'post',
            data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&post_type=services',
            dataType: 'json',
            success: function(response){

                var html ='';
                var count =1;
                response.forEach(function(item) {
                    var odd_even =(count%2==0)?'even':'odd';
                    var active =(item.status=="1")?'Enabled':'Disabled';
                    var name =$.trim(item.name);
                    console.log(name);
                    var edit_form ='onclick="edit_category_form('+item.id+')"';

                    html =html +'<tr role="row" class="'+odd_even+'">'+
                        '<td class="text-center"><input type="checkbox" class="ccheckbox" value="'+item.id+'"></td>'+
                        '<td class="text-center">'+item.id+'</td>'+

                        '<td class="text-center">'+item.category_name+'</td>'+
                        '<td class="text-center">'+item.title+'</td>'+
                        '<td class="text-center">'+item.name+'</td>'+
                        '<td class="text-center">'+
                        '<img width="50px" alter="No Image" src="<?php echo base_url();?>/uploads/cms/'+item.image+'">'+
                        '</td>'+
                        '<td class="text-center">'+item.username+'</td>'+


                        '<td class="text-center"><span class="label label-success">'+active+'</span></td>'+
                        '<td class="text-center"><button '+edit_form+' type="button" class="btn btn-primary">EDIT</button> <button onclick="delete_item('+item.id+')"  type="button" class="btn btn-danger">Delete</button></td>'+
                        '</tr>'
                    ;



                    count++;
                });

                $('#list_services').html(html);

            }


        });

    }



    $(document).ready(function() {
        var frm = $('#new_service_form');

        frm.submit(function (e) {

            $("#loaderDiv").show();
            $(".pull-left").hide();

            $('#editor1').val(CKEDITOR.instances.editor1.getData());

            e.preventDefault();


            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                // data: frm.serialize(),
                data: new FormData(this),
                contentType: false, //must, tell jQuery not to process the data
                processData: false,
                cache:false,
                async:false,

                success: function (data) {
                    $("#loaderDiv").hide();
                    $(".pull-left").show();

                    // console.log('Submission was successful.');
                    console.log(data);
                    hideForms();
                    location.reload();

                },
                error: function (data) {
                    console.log('An error occurred.');
                    console.log(data);
                },
            });
        });



        get_all_list();



        $(".dataTables_filter ").addClass('pull-left');
        $(".dataTables_filter input").addClass('admin_border');

        $(".dataTables_filter").css("display", "block");
        $(".dataTables_filter input").css("max-width", "78% !important");
        $(".dataTables_filter input").css("width", "100% !important");




    });


    function select_check(checkbox)
    {
        var checkboxes = document.getElementsByName('listCheck')
        checkboxes.forEach((item) => {
            if (item !== checkbox) item.checked = false
        })
    }




    $(document).ready(function(){

        $("#is_price_start_from").change(function(){

            var select_item = $(this).children("option:selected").val();

            if(select_item==1){
                $('#start_from_price').parent().parent().css('display','block');
            }
            else{
                $('#start_from_price').parent().parent().css('display','none');

            }
        });

    });



</script>