
<section id="content">
	<div class="listDiv">
		<?php $this->load->view('admin/common/breadcrumbs'); ?>


		<div class="row-fluid">
			<?php   $flashAlert = $this->session->flashdata('alert');
			if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
				?>
				<br>
				<div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
					<strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
					<button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			<?php   } ?>


			<div class="row-fluid" id="category_form">

			</div>



			<div class="module-body table-responsive">
				<table id="manageClientTable" class="table table-bordered table-striped  table-hover dataTable" cellspacing="0" width="100%" data-selected_id="">
					<thead>
						<tr>
							<th class="no-sort text-center"><input type='checkbox' name='allClients' id="allClients" value='' onclick="Javascript:selectAllClients();" /></th>
							<th class="column-id"><?php echo $this->lang->line('id'); ?></th>
							<!-- <th class="column-service_category" ><?php echo $this->lang->line('service_category'); ?></th> -->
							<th class="column-name"><?php echo $this->lang->line('name'); ?></th>
							<th class="column-status"><?php echo $this->lang->line('status'); ?></th>
							<th class="column-action"><?php echo $this->lang->line('action'); ?></th>
						</tr>
					</thead>

					<tbody id="list_category">
						
					</tbody>
				</table>
			</div>

		</div>
	</div>


</section>

<script type="text/javascript">


	function get_all_category()
	{

		$.ajax({
			url:'ajax_get_category.php',
			method: 'post',
			data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&slug=services',
			dataType: 'json',
			success: function(response){

				var html ='';
				var count =1;
				response.forEach(function(item) {
					var odd_even =(count%2==0)?'even':'odd';
					var active =(item.status=="1")?'Active':'Deactive';
					var name =$.trim(item.name);
					console.log(name);
					var edit_form ='onclick="edit_category_form('+item.id+')"';

					html =html +'<tr role="row" class="'+odd_even+'">'+
					'<td class="text-center"><input type="checkbox" class="ccheckbox" value="'+item.id+'"></td>'+
					'<td class="text-center">'+item.id+'</td>'+
					'<td class="text-center">'+item.name+'</td>'+
					'<td class="text-center"><span class="label label-success">'+active+'</span></td>'+
					'<td class="text-center"><button '+edit_form+' type="button" class="btn btn-primary">EDIT</button></td>'+														
					'</tr>'
					;



					count++;
				});

				$('#list_category').html(html);


			}

		});

	}





	function edit_category_form(id)
	{


		$.ajax({
			url:'ajax_get_category.php',
			method: 'post',
			data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&id='+id,
			dataType: 'json',
			success: function(response){


				var checked = (response[0].status=='1')?'checked':'';
				var html ='	<form class="form" role="search">'+
				'<input type="hidden" class="form-control input--style-1" name="category_id" id="category_id" value="'+response[0].id+'">'+
				'<input type="hidden" class="form-control input--style-1" name="action_name" id="action_name" value="update">'+

				'<input type="text" class="form-control input--style-1" placeholder="Category Name" name="category_name" id="category_name" value="'+response[0].name+'" >'+
				'<lebel class="checkbox-inline"><input id="status" type="checkbox" '+checked+' data-toggle="toggle" data-on="Enabled" data-off="Disabled"> </lebel>'+
				'<button onclick="add_new()" type="button" class="btn btn-success"><?php echo $this->lang->line('update'); ?></button>'+
				'</form>';

				$('#category_form').html(html);



				$('#status').bootstrapToggle({
					on: 'Enabled',
					off: 'Disabled'
				});

			}

		});

		
	}



	function add_new_category_form()
	{
		var html ='	<form class="form" role="search">'+
		'<input type="hidden" class="form-control input--style-1" name="action_name" id="action_name" value="add">'+

		'<input type="text" class="form-control input--style-1" placeholder="Category Name" name="category_name" id="category_name" >'+
		'<lebel class="checkbox-inline"><input id="status" type="checkbox" checked data-toggle="toggle" data-on="Enabled" data-off="Disabled"> </lebel>'+
		'<button onclick="add_new()" type="button" class="btn btn-success"><?php echo $this->lang->line('add'); ?></button>'+
		'</form>';

		$('#category_form').html(html);
	}

	function add_new()
	{
		var name = $('#category_name').val();
		var status = $('#status').prop('checked');
		var action_name =$('#action_name').val();
		var id ='';
		if(action_name=='update')
		{
			id =$('#category_id').val();
		}

		$.ajax({
			url:'ajax_service_category.php',
			method: 'post',
			data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&name='+name+'&status='+status+'&action='+action_name+'&id='+id,
			dataType: 'json',
			success: function(response){

				console.log(response);
				get_all_category();

			}

		});

	}







	$(function() {


		$('#status').bootstrapToggle({
			on: 'Enabled',
			off: 'Disabled'
		});



	})



	get_all_category();
	add_new_category_form();

</script>