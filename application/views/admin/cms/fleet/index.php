<style type="text/css">
  .admin_border{
    border:1px solid #ccc;
  }
</style>

<section id="content">
	<div class="listDiv">
		<?php $this->load->view('admin/common/breadcrumbs'); ?>

		<div class="row-fluid">
			<?php   $flashAlert = $this->session->flashdata('alert');
			if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
				?>
				<br>
				<div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
					<strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
					<button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			<?php   } ?>




			<div class="module">
				<?php echo $this->session->flashdata('message'); ?>

       <div class="clearfix"></div>
       <div class="module-body table-responsive">
        <table id="cms_listing" class="table table-bordered table-striped  table-hover dataTable" cellspacing="0" width="100%" data-selected_id="">
         <thead>
          <tr>
           <th class="no-sort text-center"><input type='checkbox' name='allClients' id="allClients" value='' onclick="Javascript:selectAllClients();" /></th>
           <th class="column-id"><?php echo $this->lang->line('id'); ?></th>
           <th class="column-date"><?php echo $this->lang->line('date'); ?></th>
           <th class="column-time"><?php echo $this->lang->line('time'); ?></th>
           <th class="column-status">Added By</th>

           <th class="column-title"><?php echo $this->lang->line('name'); ?> </th>
           <th class="column-model"><?php echo $this->lang->line('model'); ?></th>
           <th class="column-climatisation"><?php echo 'Climatisation'; ?></th>
           <th class="column-wheelChair"><?php echo 'WheelChair'; ?></th>

           <th class="column-image"><?php echo $this->lang->line('image'); ?></th>
           <th class="column-status"><?php echo $this->lang->line('status'); ?></th>
           <th class="column-since"><?php echo $this->lang->line('since'); ?></th>                         				
         </tr>
       </thead>


                         		<!-- <tbody id="list_services">

                         		</tbody> -->
                         	</table>
                         </div>
                       </div>
                     </div>
                   </div>
                   <div class="addDiv" style="display:none;">

                    <?php echo form_open_multipart('cms/add_new_services', array("id" => "new_service_form")); ?>

                    <div class="row">
                     <div class="col-md-12">
                      <div class="row">



                        <div class="col-md-9">

                         <div class="col-md-4 form-group">  
                          <label> Status </label>                                
                          <select class="form-control" name="status" id="status" tabindex="1">
                           <option value="1">Enable</option>
                           <option value="2">Disable</option>
                         </select>
                       </div>



<!-- 
                    <div class="col-md-3 form-group">  
                      <label for="category_id"> fleet Category </label>                                
                      <select class="form-control" name="category_id" id="category_id" tabindex="1">
                        <option disabled>--Select Category</option>    

                        <?php

                        // foreach ($category as $key => $value)
                        // {
                        //  echo '<option value="'.$value->id.'">'.$value->name.'</option>';
                        // }

                        ?>  
                      </select>
                    </div>
                  -->

                  <?php 

                  $data = [
                    'type'  => 'hidden',
                    'name'  => 'page_type',
                    'id'    => 'page_type',
                    'value' => 'fleet',
                  ];

                  echo form_input($data);


                  $data = [
                    'type'  => 'hidden',
                    'name'  => 'id',
                    'id'    => 'id',
                    'value' => '',
                  ];

                  echo form_input($data);

                  ?> 


                  <div class="col-md-4 form-group">  
                    <label> Name* </label>                                
                    <?php 

                    $data = [
                     'type'  => 'text',
                     'name'  => 'name',
                     'id'    => 'name',
                     'value' => '',
                     'placeholder' => 'Name*',
                     'required'=>'required',

                     'class' => 'form-control '
                   ];

                   echo form_input($data);

                   ?> 
                 </div> 

                 <div class="col-md-4 form-group">  
                  <label> Model* </label>                                
                  <?php 

                  $data = [
                   'type'  => 'text',
                   'name'  => 'model',
                   'id'    => 'model',
                   'value' => '',
                   'placeholder' => 'model*',
                   'required'=>'required',

                   'class' => 'form-control model'
                 ];

                 echo form_input($data);

                 ?> 
               </div> 

               <div class="col-md-4 form-group">  
                <label> WheelChair Access* </label>                                
                <select class="form-control" name="wheelchairaccess" id="wheelchairaccess" tabindex="1">
                 <option value="1">Yes</option>
                 <option value="2">No</option>
               </select>
             </div>

             <div class="col-md-4 form-group">  
              <label>Car Category* </label>                                
              <select class="form-control" name="car_category" id="car_category" tabindex="1">
                <?php foreach($car_category as $data):?>
                 <option value="<?=$data->id;?>"><?=$data->name;?></option>
               <?php endforeach;?>

             </select>
           </div>



<!-- 
            <div class="col-md-4 form-group">  
              <label> Car Type* </label>                                
              <?php 

             //  $data = [
             //   'type'  => 'text',
             //   'name'  => 'fuel_type',
             //   'id'    => 'fuel_type',
             //   'value' => '',
             //   'placeholder' => 'fuel_type*',
             //   'required'=>'required',

             //   'class' => 'form-control fuel_type'
             // ];

             // echo form_input($data);

             ?> 
           </div>  -->




           <div class="col-md-4 form-group">  
            <label> Climatisation* </label>                                
            <select class="form-control" name="climatisation" id="climatisation" tabindex="1">
             <option value="1">Yes</option>
             <option value="2">No</option>
           </select>
         </div>




         <div id="capacite" class="col-md-12 form-group">  
          <label> Car Configurations* </label>   

          <div id="capacite_field" class="col-md-12" >
<?php /*    

            <div style="margin-left: -15px;" class="col-md-2 form-group">
             <label>Passengers</label>    
             <?php 

             $data = [
               'type'  => 'number',
               'name'  => 'passengers[]',
               'id'    => 'name',
               'value' => '',
               'placeholder' => 'Passengers',
               'class' => 'form-control'
             ];
             echo form_input($data);
             ?> 
           </div>  
            <div style="margin-left: -15px;" class="col-md-2 form-group">
             <label>Wheelchairs</label>    
             <?php 

             $data = [
               'type'  => 'number',
               'name'  => 'wheelchairs[]',
               'id'    => 'wheelchairs',
               'value' => '',
               'placeholder' => 'wheelchairs',
               'class' => 'form-control'
             ];
             echo form_input($data);
             ?> 
           </div>  
            <div style="margin-left: -15px;" class="col-md-2 form-group">
             <label>Babyseats</label>    
             <?php 

             $data = [
               'type'  => 'number',
               'name'  => 'babyseats[]',
               'id'    => 'babyseats',
               'value' => '',
               'placeholder' => 'babyseats',
               'class' => 'form-control'
             ];
             echo form_input($data);
             ?> 
           </div>  
            <div style="margin-left: -15px;" class="col-md-2 form-group">
             <label>Animals</label>    
             <?php 

             $data = [
               'type'  => 'number',
               'name'  => 'animals[]',
               'id'    => 'animals',
               'value' => '',
               'placeholder' => 'animals',
               'class' => 'form-control'
             ];
             echo form_input($data);
             ?> 
           </div>  

            <div style="margin-left: -15px;" class="col-md-2 form-group">
             <label>Lugages</label>    
             <?php 

             $data = [
               'type'  => 'number',
               'name'  => 'lugages[]',
               'id'    => 'lugages',
               'value' => '',
               'placeholder' => 'lugages',
               'class' => 'form-control'
             ];
             echo form_input($data);
             ?> 
           </div>  

*/ ?>


</div>  


</div> 




</div>

<div class="col-md-3">



 <div class="col-md-12 form-group">  

  <div class="form-group">
   <img style="margin-top: 30px; min-width:200px; min-height:100px" class="admin_border" id="img_preview"  width="200px" src="<?php echo base_url();?>uploads/cms/no-preview.jpg">
   <br>
   <label for="description">Upload*</label>
   <input type="file" id="img" name="img" required="required">
 </div>
</div>

</div>

<div class="col-md-11"> 
 <div class="pull-right" style="padding-right: 4%">
  <input type="button" class="btn btn-default fa fa-input" value="&#xf05e Cancel" onclick="return hideAdd();" tabindex="17" />  
  <input type="submit" class="btn btn-default fa fa-input" id="save" value="&#xf0c7 Save" tabindex="16" /> 

</div>
</div>



</div>
</div>

</div>

<?php echo form_close(); ?>
</div>
</section>
</div>

<script>

 function showAdd() {
  hideForms();
  $('.addDiv').show();
  $('.breadcrumb').append('<span id="subtitle_custom"> > Add Car</span>');

  var config_box =configuration_box();
  $('#capacite_field').html(config_box);
        //showAddHead();
      }

      function hideForms() {
        $('.addDiv').hide();
        $('.listDiv').find('.row-fluid').hide();
        $('#subtitle_custom').remove(); 

      }
      function hideAdd() {
        hideForms();
        $('.listDiv').find('.row-fluid').show();
      }



      function delete_item(id=null)
      {
       if(id==null)
       {

        $.each($("input[name='listCheck']:checked"), function(){
         id= $(this).val();
       });

      }

      if (id === null)
      {
        alert('No row item selected!!');
        return false;
      }


      if(confirm("Are you sure to delete?"))
      {

        $.ajax({
         url:'ajax_delete_cms_listing.php',
         method: 'post',
         data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&id='+id,
         dataType: 'json',
         success: function(response){

           location.reload();

         }


       });
      }


    }






    function edit_item(id=null,display_id=null)
    {
     if(id==null)
     {

      $.each($("input[name='listCheck']:checked"), function(){
       id= $(this).val();
       display_id = $(this).data("id");

     });

    }

    if (id === null)
    {
      alert('No row item selected!!');
      return false;
    }

    hideForms();

    //  var $form = $('.addDiv').clone();
    //  $('.edit_div').html($form);
    // $('.edit_div').show();
    $('.addDiv').show();
    $('.breadcrumb').append('<span id="subtitle_custom"> > '+display_id+'</span>');


    $('#new_service_form').attr('action','ajax_update_cms_listing.php');
    $('#img').removeAttr('required');



    $.ajax({
      url:'ajax_get_cms_listing.php',
      method: 'post',
      data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&post_type=fleet&id='+id,
      dataType: 'json',
      success: function(response){
        console.log(response);

        $('#name').val(response.name);

        $('#model').val(response.model);


        $('#fuel_type').val(response.fuel_type);

        $('#climatisation ').val(response.climatisation );


        $('#id').val(response.id);
        $('#img_preview').attr('src','<?php echo base_url();?>uploads/cms/'+response.image);

        $('#category_id').val(response.category_id);
        $('#status').val(response.status);
        $('#wheelchairaccess').val(response.wheelchairaccess);
        $('#car_category').val(response.car_category);

        if(response.passengers==null)
        {
            html =configuration_box();
            $( "#capacite_field" ).html(html);
        }
      else{


        var confirmation_box =edit_configuration_box(response.passengers,response.wheelchairs,response.babyseats,response.animals,response.lugages);

      $( "#capacite_field" ).html(confirmation_box);


      }


      }


    });

  }












    // DATATABLE

    function get_all_list()
    {

    	var oTable = $('#cms_listing').on('preXhr.dt', function (e, settings, data) {
            $("#loaderTable").show();
        }).DataTable({
    		'processing': true,
    		'serverSide': true, 
    		"dom": "<'row'<'col-sm-4'l><'col-sm-4 statusDropdown'><'col-sm-4'f>>" +
    		"<'row'<'col-sm-12't>>" +
    		"<'row'<'col-sm-6'i><'col-sm-6'p>>",
    		'ajax': {
    			url:'services_list_ajax_data_set',
    			type: 'post',
                // data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&post_type=services',

                data: {
                	'post_type': 'fleet',
                	'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',
                },
                dataType: 'json',
              },
            "initComplete":function( settings, json){
                $("#loaderTable").hide();
            },
              'columns': [
              { data: 'created_date' },
              { data: 'id' },
              { data: 'date' },
              { data: 'time' },
              { data: 'username' },

              { data: 'name' },
              { data: 'model' },
              { data: 'climatisation' },
              { data: 'wheelchairaccess' },

              { data: 'image' },
              { data: 'status' },
              { data: 'since' },            
              ],
              columnDefs: [
              { className: "text-center", targets: "_all" },
              ],

              dom: 'lBfrtip',
              buttons: [



              {
               text: ' <span href="javascript:;" class="" onclick="showAdd();"><i class="fa fa-plus"></i> Add</span> ',
               title: '',
             },


             {
               text: ' <span href="javascript:;" class="" onclick="edit_item()"><i class="fa fa-pencil"></i> Edit</span> ',
               title: '',
             },


             {
               text: ' <span href="javascript:;" class="" onclick="delete_item()"><i class="fa fa-trash"></i> Delete</span> ',
               title: '',
             },



             {
               extend: 'excel',
               text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel Export',
               filename: 'members',
               title: '',
               exportOptions: {
                modifier: {
                 search: 'applied',
                 order: 'applied',
                 page: 'current'
               },
               columns: [1, 2, 3]
             }
           },
           {
             extend: 'csv',
             text: '<i class="fa fa-file-text-o" aria-hidden="true"></i> Export CSV',
             filename: 'members',
             title: '',
             exportOptions: {
              modifier: {
               search: 'applied',
               order: 'applied',
               page: 'current'
             },
             columns: [1, 2, 3, 4, 5]
           }
         },
         {
           extend: 'pdf',
           text: '<i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF',
           filename: 'members',
           title: '',
           exportOptions: {
            modifier: {
             search: 'applied',
             order: 'applied',
             page: 'current'
           },
           columns: [1, 2, 3, 4, 5]
         }
       },

       ],
       "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]




     });  
    } 





    $(document).ready(function() {
     var frm = $('#new_service_form');

     frm.submit(function (e) {

      $("#loaderDiv").show();
      $(".pull-left").hide();


      e.preventDefault();


      $.ajax({
       type: frm.attr('method'),
       url: frm.attr('action'),
                        // data: frm.serialize(),
                        data: new FormData(this),
                        contentType: false, //must, tell jQuery not to process the data
                        processData: false,
                        cache:false,
                        async:false,

                        success: function (data) {
                          $("#loaderDiv").hide();
                          $(".pull-left").show();

                            // console.log('Submission was successful.');
                            console.log(data);
                            hideForms();
                            location.reload();

                          },
                          error: function (data) {
                            console.log('An error occurred.');
                            console.log(data);
                          },
                        });
    });



     get_all_list();



     $(".dataTables_filter ").addClass('pull-left');
     $(".dataTables_filter input").addClass('admin_border');

     $(".dataTables_filter").css("display", "block");
     $(".dataTables_filter input").css("max-width", "78% !important");
     $(".dataTables_filter input").css("width", "100% !important");





   });



    function select_check(checkbox)
    {
      var checkboxes = document.getElementsByName('listCheck')
      checkboxes.forEach((item) => {
        if (item !== checkbox) item.checked = false
      })
    }








    function edit_configuration_box(passengers,wheelchairs,babyseats,animals,lugages)
    {

passengers = jQuery.parseJSON(passengers);
wheelchairs = jQuery.parseJSON(wheelchairs);
babyseats = jQuery.parseJSON(babyseats);
animals = jQuery.parseJSON(animals);
lugages = jQuery.parseJSON(lugages);
var count =0;

var html ='';
$.each(passengers, function(index,value){



if(count==0)
{
    // var button ='<div class="col-md-1 pull-right" style=""><span><button type="button" class="btn btn-default" style="" onclick="add_more()">+</button></span><br><br><span><button class="btn btn-default" style="" type="button" onclick="delete_box_item(this)">-</button></span></div>';
    var button ='<div class="col-md-1 pull-right" style=""><span><button type="button" class="btn btn-default" style="" onclick="add_more()">+</button></span></div>';
}
else{
  
  var button ='<div class="col-md-1 pull-right" style="padding-top: 2%;"><span><button class="btn btn-default" style="" type="button" onclick="delete_box_item(this)">-</button></span></div>';

}
  var select_drop_down='';
  var select_drop_down_passengers='';
  var wheelchairs_drop_down='';
  var babyseats_drop_down='';
  var animals_drop_down='';
  var lugages_drop_down='';

for (i = 0; i < 10; i++) {

  select_drop_down_passengers +=(i==value)?'<option selected="true" value="'+i+'">'+i+'</option>':'<option value="'+i+'">'+i+'</option>';
  wheelchairs_drop_down +=(i==wheelchairs[index])?'<option selected="true" value="'+i+'">'+i+'</option>':'<option value="'+i+'">'+i+'</option>';
  babyseats_drop_down +=(i==babyseats[index])?'<option selected="true" value="'+i+'">'+i+'</option>':'<option value="'+i+'">'+i+'</option>';
  animals_drop_down +=(i==animals[index])?'<option selected="true" value="'+i+'">'+i+'</option>':'<option value="'+i+'">'+i+'</option>';
  lugages_drop_down +=(i==lugages[index])?'<option selected="true" value="'+i+'">'+i+'</option>':'<option value="'+i+'">'+i+'</option>';

}


      html =html+' <div style="margin-left: -15px;" id="capacite_field" > <div style="background: #efefef linear-gradient(#efefef, #ECECEC, #CECECE) repeat scroll 0% 0%;min-height: 95px;  margin-bottom: 1%;" class="col-md-11 admin_border">'+

      '<div style="margin-left: -15px;" class="col-md-2 form-group">'+
      '<label>Passengers</label>    '+
      '<select name="passengers[]" id="passengers" class="form-control">'+select_drop_down_passengers+'</select>'+

      // '<input type="number" min="1" max="10" name="passengers[]" value="'+value+'" id="name" placeholder="FROM 0 TO 9 TO SELECT " class="form-control"> '+
      '</div>  '+
      '<div style="margin-left: -15px;" class="col-md-2 form-group">'+
      '<label>Wheelchairs</label>    '+
      '<select name="wheelchairs[]" id="wheelchairs" class="form-control">'+wheelchairs_drop_down+'</select>'+

      // '<input type="number" min="1" max="10"  name="wheelchairs[]" value="'+wheelchairs[index]+'" id="wheelchairs" placeholder="FROM 0 TO 9 TO SELECT " class="form-control"> '+
      '</div>  '+
      '<div style="margin-left: -15px;" class="col-md-2 form-group">'+
      '<label>Babyseats</label>   '+ 
      '<select name="babyseats[]" id="babyseats" class="form-control">'+babyseats_drop_down+'</select>'+

      // '<input type="number" min="1" max="10"  name="babyseats[]" value="'+babyseats[index]+'" id="babyseats" placeholder="FROM 0 TO 9 TO SELECT " class="form-control"> '+
      '</div>  '+
      '<div style="margin-left: -15px;" class="col-md-2 form-group">'+
      '<label>Animals</label>    '+
      '<select name="animals[]" id="animals" class="form-control">'+animals_drop_down+'</select>'+

      // '<input type="number" min="1" max="10"  name="animals[]" value="'+animals[index]+'" id="animals" placeholder="FROM 0 TO 9 TO SELECT " class="form-control"> '+
      '</div>  '+

      '<div style="margin-left: -15px;" class="col-md-2 form-group">'+
      '<label>Lugages</label>    '+
      '<select name="lugages[]" id="lugages" class="form-control">'+lugages_drop_down+'</select>'+

      // '<input type="number" min="1" max="10"  name="lugages[]" value="'+lugages[index]+'" id="Lugages" placeholder="FROM 0 TO 9 TO SELECT " class="form-control">'+ 
      '</div></div>'+button+  
      '</div>';



count++;

});


    

      return html;
    }




    function configuration_box(action)
    {


      if(action=='plus')
      {


        var button ='<div class="col-md-1 pull-right" style="padding-top: 2%;"><span><button class="btn btn-default" style="" type="button" onclick="delete_box_item(this)">-</button></span></div>';
      }
      else{

        // var button ='<div class="col-md-1 pull-right" style="padding-top: 2%;"><span><button class="btn btn-default" style="" type="button" onclick="delete_box_item(this)">-</button></span></div>';

        var button ='<div class="col-md-1 pull-right" style=""><span><button type="button" class="btn btn-default" style="" onclick="add_more()">+</button></span></div>';

      }

var select_drop_down='';

for (i = 0; i < 10; i++) {
  select_drop_down +='<option value="'+i+'">'+i+'</option>';
}



      var html ='<div style="margin-left: -15px;" id="capacite_field" > <div style="background: #efefef linear-gradient(#efefef, #ECECEC, #CECECE) repeat scroll 0% 0%;min-height: 95px;  margin-bottom: 1%;" class="col-md-11 admin_border">'+

      '<div style="margin-left: -15px;" class="col-md-2 form-group">'+
      '<label>Passengers</label>    '+
      // '<input type="number" min="1" max="10" name="passengers[]" value="" id="name" placeholder=" FROM 0 TO 9 TO SELECT" class="form-control"> '+
      '<select name="passengers[]" id="passengers" class="form-control">'+select_drop_down+'</select>'+
      '</div>  '+
      '<div style="margin-left: -15px;" class="col-md-2 form-group">'+
      '<label>Wheelchairs</label>    '+
      '<select name="wheelchairs[]" id="wheelchairs" class="form-control">'+select_drop_down+'</select>'+

      // '<input type="number" min="1" max="10"  name="wheelchairs[]" value="" id="wheelchairs" placeholder="FROM 0 TO 9 TO SELECT" class="form-control"> '+
      '</div>  '+
      '<div style="margin-left: -15px;" class="col-md-2 form-group">'+
      '<label>Babyseats</label>   '+ 
      '<select name="babyseats[]" id="babyseats" class="form-control">'+select_drop_down+'</select>'+

      // '<input type="number" min="1" max="10"  name="babyseats[]" value="" id="babyseats" placeholder="FROM 0 TO 9 TO SELECT" class="form-control"> '+
      '</div>  '+
      '<div style="margin-left: -15px;" class="col-md-2 form-group">'+
      '<label>Animals</label>    '+
      '<select name="animals[]" id="animals" class="form-control">'+select_drop_down+'</select>'+

      // '<input type="number" min="1" max="10"  name="animals[]" value="" id="animals" placeholder="FROM 0 TO 9 TO SELECT" class="form-control"> '+
      '</div>  '+

      '<div style="margin-left: -15px;" class="col-md-2 form-group">'+
      '<label>Lugages</label>    '+
      '<select name="lugages[]" id="lugages" class="form-control">'+select_drop_down+'</select>'+

      // '<input type="number" min="1" max="10"  name="lugages[]" value="" id="Lugages" placeholder="FROM 0 TO 9 TO SELECT" class="form-control">'+ 
      '</div></div>'+button+  
      '</div>';

      return html;
    }



    function add_more(action='plus')
    {
      html =configuration_box(action);
      $( "#capacite_field" ).append(html);

    }


  function delete_box_item(elm)
  {
    var count = $( "#capacite_field" ).children().length;
    $(elm).parent().parent().parent().remove();
    if(count==1){
       html =configuration_box();
      $( "#capacite_field" ).append(html);
    }

  }




  </script>
