

<?php

extract((array)$seo);

?>

<section id="content">
	<div class="listDiv">
		<?php $this->load->view('admin/common/breadcrumbs'); ?>

		<div class="row">



			<div class="">
<div class="col-md-12">
			</div>
				<?php echo form_open_multipart('cms/add_cms_seo', array("id" => "client_banner_form")); ?>


				<div class="col-md-6">


					<div class="col-md-12 form-group">  
						<label>Meta Title</label>                                
						<?php 

						$data = [
							'type'  => 'text',
							'name'  => 'title',
							'id'    => 'title',
							'value' => $title,
							'placeholder' => 'Meta title.',

							'class' => 'form-control title'
						];

						echo form_input($data);

						?> 
					</div> 

               <div class="col-md-12 form-group">  
                  <label> Meta Description* </label>                                
                  <?php 

                  $data = [
                   'name'        => 'meta_description',
                   'id'          => 'meta_description',
                   'value'       => $meta_description,
                   'rows'        => '5',
                   'cols'        => '50',
                   'style'       => 'width:100%',
                   'required'=>'required',

                   'class'       => 'form-control'
               ];

               echo form_textarea($data);

               ?> 
           </div>



           <div class="col-md-12 form-group">  
              <label> Meta Tag* </label>                                
              <?php 

              $data = [
               'name'        => 'meta_tag',
               'id'          => 'meta_tag',
               'value'       => $meta_tag,
               'rows'        => '2',
               'cols'        => '100',
               'style'       => 'width:100%',
               'required'=>'required',

               'class'       => 'form-control'
           ];

           echo form_textarea($data);

           ?> 
       </div>

				</div>

				<div class="col-md-6"> 

					<div class="col-md-12 form-group">  
						<label>Google Analytique Code</label>                                
						<?php 

						$data = [
							'type'  => 'text',
							'name'  => 'google_analytics',
							'id'    => 'google_analytics',
							'value' => $google_analytics,
							'placeholder' => 'Google Analytique',

							'class' => 'form-control google_analytics '
						];

						echo form_input($data);

						?> 
					</div> 



					<div class="col-md-12 form-group">  
						<label>Microsoft Ads Code</label>                                
						<?php 

						$data = [
							'type'  => 'text',
							'name'  => 'microsoft_ads',
							'id'    => 'microsoft_ads',
							'value' => $microsoft_ads,
							'placeholder' => 'Microsoft Ads ',

							'class' => 'form-control microsoft_ads  '
						];

						echo form_input($data);

						?> 
					</div> 



					<div class="col-md-12 form-group">  
						<label>Facebook Ads Code Code</label>                                
						<?php 

						$data = [
							'type'  => 'text',
							'name'  => 'facebook_ads',
							'id'    => 'facebook_ads',
							'value' => $facebook_ads,
							'placeholder' => 'Facebook Ads',

							'class' => 'form-control facebook_ads  '
						];

						echo form_input($data);

						?> 
					</div> 



					<div class="col-md-12 form-group">  
						<label>Google Ads Code</label>                                
						<?php 

						$data = [
							'type'  => 'text',
							'name'  => 'google_ads',
							'id'    => 'google_ads',
							'value' => $google_ads,
							'placeholder' => 'Google Ads Code',

							'class' => 'form-control google_ads  '
						];

						echo form_input($data);

						?> 
					</div> 
				</div> 



					<div class="col-md-12"> 
						<div class="pull-right" style="padding-right: 1%">
							<!-- <input type="button" class="btn btn-default fa fa-input" value="&#xf05e Cancel" onclick="return hideAdd();" tabindex="17" />   -->
							<input type="submit" class="btn btn-default fa fa-input" id="save" value="&#xf0c7 Save" tabindex="16" /> 

						</div>
					</div>


				<?php echo form_close(); ?>

				
			</div>



		</div>
	</div>

</section>