<section id="content">
	<div class="listDiv">
		<?php $this->load->view('admin/common/breadcrumbs'); ?>
		<div class="row-fluid">
			<?php
			$flashAlert = $this->session->flashdata('alert');
			if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
			?>
			<br>
			<div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
				<strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
				<button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<?php
			}
			// var_dump($details);
			?>
			<div class="module">
				<?php echo $this->session->flashdata('message'); ?>
				<div class="clearfix"></div>
				<div class="module-body table-responsive">
					<?php extract($details);?>
					<div class="row">
						<div class="col-lg-4 col-md-4 col-xs-12">
							<img src="<?php echo base_url(); ?>uploads/cms/<?php echo $details->image; ?>" class="news_image img-responsive">
						</div>
						<div class="col-lg-8 col-md-8 col-xs-12">
							
							<div class="detail-block">
								<div class="row">
									
									<div class="col-lg-12 col-md-12 col-xs-12">
										<h6>
											<span><strong>Category : <?php echo $details->category_name;?></strong></span>
											<span style="float: right;margin-top: 4px;"><strong>Author:</strong>
												&nbsp;<?= $details->civility . ' ' .$details->username; ?>&nbsp;											<span class="lbl-text">Publish Date:</span> &nbsp;<?= ($details->created_date != "") ? date('d/m/Y H:i',strtotime($details->created_date)) : ''; ?>
											</span>
										</h6>
									</div>
									<div class="col-lg-12 col-md-12 col-xs-12 newsdetail_page">
										<h2><?php echo $details->title;?></h2>
									</div>
								</div>
							</div>
							<?php
							$text= $details->description;
							$text= strip_tags($text);
							$desc1= implode(' ', array_slice(explode(' ', $text), 0, 185));
							$desc2= implode(' ', array_slice(explode(' ', $text), 186, -1));
							?>
							
							<div class="row">
								<div class="col-lg-12 col-md-12 col-xs-12 newsdetail_page">   <br>
									<!-- <h3><strong style="font-size: medium;">Description</strong></h3> -->
									<div style="padding:10px 10px 0px 0px">
										<?= $desc1;?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 col-md-12 col-xs-12 newsdetail_page">   <br>
							<div style="padding:10px 10px 0px 0px">
								<?= $desc2;?>
								
							</div>
						</div>
					</div>
				</div>
				<!-- 				<div class="row">
						<div class="col-lg-12 col-md-12 col-xs-12 newsdetail_page">   <br>
								<h2>Description</h2>
								<div style="padding:10px">
							<?= $details->description;?>
							
						</div>
					</div> -->
				</div>
				<style type="text/css">
					.news_image{
						height: 412px;
						width: 100%;
						border: solid 1px #66afe9;
						background: #fff;
						padding: 10px;
					}
					.newsdetail_page h2{
						font-size: 24px;
						font-weight: 600;
						margin: 0px 0px 15px 0px;
						color: var(--default-text-color);
					}
				</style>
				<script type="text/javascript">
					$(document).ready(function () {
						// get_details();
					})
					function get_details()
					{
						$.ajax({
				url: '<?php echo base_url(); ?>admin/cms/ajax_get_cms_listing.php',
				method: 'post',
				data: '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&post_type=tutorials&id='+<?php echo $id;?>,
				dataType: 'json',
				success: function (response) {
				console.log(response);
				// CKEDITOR.instances.editor1.setData(response.description);
				// $('#title').val(response.title);
				// $('#img_preview').attr('src', '<?php echo base_url(); ?>uploads/cms/' + response.image);
				// $('#link').html('<a target="_blank" href="<?php echo base_url(); ?>uploads/cms/' + response.image + '">tutorials</a>');
				// $('#id').val(response.id);
				// $('#status').val(response.status);
				// $('#category_id').val(response.category_id);
				}
				});
				}
				</script>
			</div>
		</div>
	</div>
</div>
</section>