<?php $locale_info = localeconv(); ?>

<style>
   
   
  .nav-tabs > li.active {
   background:linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%);
    border-bottom: none;
    }
    .nav-tabs>li.active>a{
         border: 1px solid #44c0e5!important;
    }
  .nav-tabs > li {
     background:linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%);
  
   border-left: 1px solid #44c0e5!important;
    height: 55px;
    margin: 0px !important;
    }

    .nav-tabs > li > a {

    background:linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%);
    color: #fff !important;
    font-size: 12px;
    padding-left: 10px;
    display: block !important;
    width: 100% !important;
    height: 100% !important;
    text-transform: uppercase;
    font-weight: bold;
    transition: 0.2s;
    outline: none;
    border: none;
    border-radius: 0px;
    line-height: 30px !important;
}
.dataTables_wrapper .dataTables_filter {
    float: left;
    text-align: left;
    margin-left: 10px;
}
input[type="search"]{
    border:1px solid #cccccc;
    padding-left: 5px;
    border-radius: 0px;
}
input[type="search"]:focus{
    border:1px solid #cccccc !important;
}
    
    .tab-pane{
        padding: 30px 30px 30px 14px;
    }
    .configTable{
        padding: 0px;
    }
    .dataTables_wrapper .dataTables_filter input {
    margin-right: 2px;
    margin-left: 0 !important;
    max-width: 100% !important;
    width: 100% !important;
    float: right;
    }
    .dataTables_wrapper{
        margin-top: 20px;
    }
   
    
  

</style>

<style>
 
.delete-icon {
    background: url(http://uniqueweb.co.in/project/ecabapp//assets/delete-icon.png) no-repeat left center !important;
    padding: 0px 0px 0px 22px;
}
.save-icon {
    background: url(http://uniqueweb.co.in/project/ecabapp//assets/save-icon.png) no-repeat left center !important;
    padding: 0px 0px 0px 22px;
}
#editnotworkingbtn,#notworkingbtn{
    background: linear-gradient(to bottom, #44c0e5 0%, #35add1 39%,#0d85a8 100%) !important;
}
.plusgreeniconconfig{
    border: 1px solid green;cursor: pointer;overflow: hidden;outline: none;font-size: 16px;
    color: #ffffff;
    height: 25px;
    width: 24px;
    border-radius: 50%;
    background-color: green;
    position: absolute;
    top: 22px;
    right: -20px;
    z-index: 10;
    text-align: center;
    margin: 0px;
}
.minusrediconconfig{
    border: 1px solid red;cursor: pointer;overflow: hidden;outline: none;font-size: 16px;
    color: #ffffff;
    height: 25px;
    width: 24px;
    border-radius: 50%;
    background-color: red;
    position: absolute;
    bottom: 9px;
    right: -20px;
    z-index: 10;
    text-align: center;
    margin: 0px;
}
.minusrediconconfig > i,.plusgreeniconconfig > i{
    margin:0px;
}
#editdriverrestcost,#driverrestcost{
    background-color: #add8e63d !important;
}
.dataTables_wrapper .dataTables_paginate .paginate_button{
   color:#fff !important;
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current{
   color:#fff !important;
}

.dataTables_wrapper .dataTables_paginate .paginate_button:hover {
     background: linear-gradient(to bottom, #0d85a8 0%, #35add1 39%,#44c0e5 100%) !important;
   
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
     background: linear-gradient(to bottom, #0d85a8 0%, #35add1 39%,#44c0e5 100%) !important;
    color:#fff !important;
}

</style>



<?php $active_status =  $this->session->flashdata('alert');?>
<?php if (!isset($active_status['price_stu_id']) ) {
  $active_status['price_stu_id']='';
} ?>
<?php if (!isset($active_status['status_id'])) {
  $active_status['status_id']='';
} ?>
<?php if ($active_status['status_id']!='') { $status=$active_status['status_id']; $tabstatus=$active_status['status_id']; }else{$status='1'; $tabstatus='1';} ?>
<?php if ($active_status['price_stu_id']!='') { $price_stu=$active_status['price_stu_id']; }else{$price_stu='1';} ?>
<section id="content">
    <div class="listDiv"><!--col-md-10 padding white right-p-->
    <?php $this->load->view('admin/common/breadcrumbs');?>
        <?php $this->load->view('admin/common/alert');?>
        <?php echo $this->session->flashdata('message'); ?>
            <div class="row-fluid"  id="ShowTabs" style="margin-bottom: 10px; margin-left:0px;">
                <ul class="nav nav-tabs responsive">
                                            <li <?=$status==1? 'class="active"':''?>><a href="#STATUS" class="status_E" role="tab" data-toggle="tab">STATUT</a></li>
                                            <li <?=$status==2? 'class="active"':''?>><a href="#CIVILITY" class="clivility_E" role="tab" data-toggle="tab">CIVILITE</a></li>
                                            <li <?=$status==3? 'class="active"':''?>><a href="#POST" class="post_E" role="tab" data-toggle="tab">POSTE</a></li>
                                            <li <?=$status==4? 'class="active"':''?>><a href="#PATTERN"role="tab" class="pattern_E" data-toggle="tab">MOTIF</a></li>
                                            <li <?=$status==5? 'class="active"':''?>><a href="#CONTRACT_TYPE" class="contractType_E" role="tab" data-toggle="tab">TYPE CONTRAT</a></li>
                                            <li <?=$status==6? 'class="active"':''?>><a href="#NATURE_OF_THE_CONTRACT" class="natureOfContract_E" role="tab" data-toggle="tab">NATURE DU CONTRAT</a></li>
                                            <li <?=$status==7? 'class="active"':''?>><a href="#NUMBER_OF_HOURS_PER_MONTH" class="numberOfHoursPerMonth_E" role="tab" data-toggle="tab">NOMBRE DHEURE MENSUEL</a></li>
                                            <li <?=$status==8? 'class="active"':''?>><a href="#TYPE_ID_PIECE_OF_IDENTITY" class="type_E" role="tab" data-toggle="tab">TYPE DE PIECE DIDENTITE</a></li>
                                </ul>
                <div class="tab-content responsive" style="width:100%;display: table;">
                        <!--status tab starts-->
                        <div class="tab-pane fade <?=$status==1? 'in active':''?>" id="STATUS" style="border: 1px solid #ccc;">
                            <div class="row">
                            <div class="ListdriverStatus">
                                <input type="hidden" class="chk-AddDriverStatus-btn" value="">
                     
                            <div class="col-md-12">
                            <div class="module-body table-responsive">
                                <table class="configTable table table-bordered table-striped  table-hover dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
                                    <thead>
                                    <tr>
                                        <th class="no-sort text-center"style="width:2% !important;">#</th>
                                        <th class="text-center" style="width:3% !important;"><?php echo $this->lang->line('id'); ?></th>
                                        <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('date'); ?></th>
                                        <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('time'); ?></th>
                                        <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('added_by'); ?></th>
                                        <th class="column-first_name text-center" style="width:6% !important;">Name</th>
                                         <th class="column-first_name text-center" style="width:6% !important;">Statut</th>
                                        <th class="text-center" style="width:10%;">Since</th>
                                        
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php $cnt=1; ?>
                                        <?php 
                                        if (!empty($driver_status_data)): 
                                            //print_r($driver_status_data);
                                            //exit();
                                        ?>
                                        <?php foreach($driver_status_data as $key => $item):?>
                                            <tr>
                                                <td class="text-center">
                                                  <input type="checkbox" class="chk-mainvat-template" data-input="<?=$item->id?>"></td> 
                                                <td class="text-center"><a href="javascript:void()" onclick="driverStatusidEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->create_date)) .'0000'. $item->id;?></a></td>
                                                <td class="text-center"><?=from_unix_date($item->create_date)?></td>
                                                <td class="text-center"><?=date("H:i:s", strtotime($item->create_date))?></td>
                                                <td class="text-center"><?= $this->drivers_model->getuser($item->user_id);?></td>
                                                <td class="text-center"><?= $item->status; ?></td>
                                                 <td class="text-center"><?=$item->statut=='0'?'<span class="label label-success">Active</span>':'<span class="label label-danger">Hide</span>';?></td>

                                                <td class="text-center"><?= timeDiff($item->create_date) ?></td>

                                            </tr>
                                            <?php $cnt++; ?>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                                <br>
                                </div>
                            </div>
                                                    </div>
                            <?=form_open("admin/driver_config/driverStatusadd")?>
                            <div class="driverStatusAdd" style="display: none;" >
                        <div class="col-md-12">  
                        <div class="col-md-2" style="margin-top: 5px;">
                              <div class="form-group">
                                <span style="font-weight: bold;">Statut</span>
                                <select class="form-control" name="statut" required>
                                  <option value="">Statut</option>
                                  <option value="0">Show</option>
                                  <option value="1">Hide</option>
                                </select>
                              </div>
                            </div>   
                            <div class="col-md-2" style="margin-top: 5px;">
                                <div class="form-group">
                                <span style="font-weight: bold;">Name</span>
                                    <input type="text" class="form-control" name="status" placeholder="" value="" required="">
                                </div>
                            </div>
                        </div>
                            <div class="col-md-12">  
                            
                                <!-- icons -->
                                 <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                                <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelDriverStatus()"><span class="fa fa-close"> Cancel </span></button>
                                <!-- icons -->
                               
                             </div>      
                            </div>
                            <?php echo form_close(); ?>
                            <div class="driverStatusEdit" style="display:none">
                            <?=form_open("admin/driver_config/driverStatusEdit")?>
                            <div class="driverStatusEditajax">
                            </div>
                             <div class="col-md-12">  
                            
                                <!-- icons -->
                                 <button  class="btn btn-default" style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                                <button type="button" class="btn btn-default" style="float:right; margin-left:7px;" onclick="cancelDriverStatus()"><span class="fa fa-close"> Cancel </span></button>
                                <!-- icons -->
                               
                             </div>  
                            
                            <?php echo form_close(); ?>
                            </div>
                            <div class="col-md-12 driverStatusDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
                                        <?=form_open("admin/driver_config/deleteDriverStatus")?>
                                            <input  name="tablename" value="vbs_job_statut" type="hidden" >
                                            <input type="hidden" id="driverStatusid" name="delet_driver_status_id" value="">
                                            <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10px;"> Are you sure you want to delete selected?</div>
                                            <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
                                <button  class="btn btn-default"style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

                                            <a class="btn btn-default" style="cursor: pointer;" onclick="cancelDriverStatus()"><span class="delete-icon">No</span> </a>
                                        <?php echo form_close(); ?>
                                    </div>
                        </div>
                        </div>
                        <div class="tab-pane fade <?=$status== '2'? 'in active':''?>" id="CIVILITY" style="border: 1px solid #ccc;">
                        <?php include('civilite.php'); ?>
                        </div>
                        <div class="tab-pane fade <?=$status== '3'? 'in active':''?>" id="POST" style="border: 1px solid #ccc;">
                        <?php include('post.php'); ?>
                        </div>

                        <div class="tab-pane fade <?=$status== '4'? 'in active':''?>"  id="PATTERN" style="border: 1px solid #ccc;">
                        <?php include('pattern.php'); ?>
                        </div>
                        <div class="tab-pane fade <?=$status== '5'? 'in active':''?>" id="CONTRACT_TYPE" style="border: 1px solid #ccc;">
                        <?php include('contract.php'); ?>
                        </div>
                        <div class="tab-pane fade <?=$status== '6'? 'in active':''?>" id="NATURE_OF_THE_CONTRACT" style="border: 1px solid #ccc;">
                        <?php include('nature.php'); ?>
                        </div>
                        <div class="tab-pane fade <?=$status== '7'? 'in active':''?>" id="NUMBER_OF_HOURS_PER_MONTH" style="border: 1px solid #ccc;">
                        <?php include('hours.php'); ?>
                        </div>
                        <div class="tab-pane fade <?=$status== '8'? 'in active':''?>" id="TYPE_ID_PIECE_OF_IDENTITY" style="border: 1px solid #ccc;">
                        <?php include('type.php')?>
                        </div>
                     
            </div>
            <!--/.module-->
        </div>

    </div>
</section>
<script>
    jQuery(function($){
        $.datepicker.regional['fr'] = {
            closeText: 'Fermer',
            prevText: '&#x3c;Préc',
            nextText: 'Suiv&#x3e;',
            currentText: 'Aujourd\'hui',
            monthNames: ['Janvier','Fevrier','Mars','Avril','Mai','Juin',
                'Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
            monthNamesShort: ['Jan','Fev','Mar','Avr','Mai','Jun',
                'Jul','Aou','Sep','Oct','Nov','Dec'],
            dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
            dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
            dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
            weekHeader: 'Sm',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: '',
            maxDate: '+12M +0D',
            showButtonPanel: true
        };
        $.datepicker.setDefaults($.datepicker.regional['fr']);
       
    });

    $(document).ready(function(){
        $( ".datepicker" ).datepicker({
            dateFormat: "dd-mm-yy",
            regional: "fr"
        });
        $('#MainAdd').click(function(){

            $('#MainTab').hide();
            $('#ShowTabs').attr('class','row');

        });

    });
    // Passengers part

    function AddPassengers()
    {
        setupDivPassengers();
        $(".AddPassengers").show();
    }

    function cancelPassengers()
    {
        setupDivPassengers();
        $(".ListPassengers").show();
    }

    function setupDivPassengers()
    {
        $(".AddPassengers").hide();
        $(".ListPassengers").hide();
        $(".EditPassengers").hide();
        $(".DeletePassengers").hide();
    }


    // Vat part

    function driverStatusAdd()
    {
        setupDriverStatus();
        $(".driverStatusAdd").show();
    }

    function cancelDriverStatus()
    {
        setupDriverStatus();
        $(".ListdriverStatus").show();
    }
    function VatDelete()
    {
        var val = $('.chk-AddDriverStatus-btn').val();
        if(val != "")
        {
            setupDriverStatus();
            $(".driverStatusDelete").show();
        }else{
            alert('Please select a row to delete.');
        }
    }
    function driverStatusEdit()
    {
        var val = $('.chk-AddDriverStatus-btn').val();
        if (val=='')
        {
            alert("Please select record!");
            return false;
        }
        setupDriverStatus();
        $(".driverStatusEdit").show();
        $.ajax({
            type: "GET",
            url: '<?php echo base_url().'admin/driver_config/get_ajax_driverStatus'; ?>',
            data: {'driver_status_id': val},
            success: function (result) {
                // alert(result);
                document.getElementsByClassName("driverStatusEditajax")[0].innerHTML = result;
            }
        });
    }
    function driverStatusidEdit(id){
        var val = id;
        if (val=='')
        {
            alert("Please select record!");
            return false;
        }
        setupDriverStatus();
        $(".driverStatusEdit").show();
        $.ajax({
            type: "GET",
            url: '<?php echo base_url().'admin/driver_config/get_ajax_driverStatus'; ?>',
            data: {'driver_status_id': val},
            success: function (result) {
                // alert(result);
                document.getElementsByClassName("driverStatusEditajax")[0].innerHTML = result;
            }
        });
    }
    function setupDriverStatus()
    {
        $(".driverStatusAdd").hide();
        $(".driverStatusEdit").hide();
        $(".ListdriverStatus").hide();
        $(".driverStatusDelete").hide();

    }
      function setupDriverStatusConfig()
    {
        $(".driverStatusAdd").hide();
        $(".driverStatusEdit").hide();
        $(".ListdriverStatus").show();
        $(".driverStatusDelete").hide();

    }
    // $('input.chk-mainvat-template').on('change', function() {
    //     $('input.chk-mainvat-template').not(this).prop('checked', false);
    //     var id = $(this).attr('data-input');
    //     console.log(id)
    //     $('.chk-AddDriverStatus-btn').val(id);
    //     $('#driverStatusid').val(id);
    // });
    $(document).on('change', 'input.chk-mainvat-template', function() {
        $('input.chk-mainvat-template').not(this).prop('checked', false);
        var id = $(this).attr('data-input');
        console.log(id)
        $('.chk-AddDriverStatus-btn').val(id);
        $('#driverStatusid').val(id);
    });

    function getCaraddes(){
        $.ajax({
            url:"<?php echo base_url('admin/getCaradded') ?>",
            method:"GET",
            success:function(success){
                $('#tableBody').html(success);
            },error:function(xhr){
                console.log(xhr.responseText);
            }
        });
    }


    $('#save').on('click',function(){
        driverStatusEvent();
    })

</script>
<script>
$(document).ready(function () {
    $(window).on('load', function() {
        <?php if ($tabstatus=='1') { ?>
        driverStatusEvent();
        <?php }else if ($tabstatus=='2'){ ?>
        civiliteEvent();
        <?php }else if ($tabstatus=='3'){ ?>
        postEvent();
        <?php }else if ($tabstatus=='4'){ ?>
        patternEvent();
        <?php }else if ($tabstatus=='5'){ ?>
        contractEvent();
        <?php }else if ($tabstatus=='6'){ ?>
        NatureEvent();
        <?php }else if ($tabstatus=='7'){ ?>
            HoursEvent();
        <?php }else if ($tabstatus=='8'){ ?>
        HoursEvent();
        <?php }else if ($tabstatus=='8'){ ?>
        TypeEvent();
        <?php } ?>

    });
    $('.status_E').click(function(){
        driverStatusEvent();
    });
    $('.clivility_E').click(function(){
      civiliteEvent();
    });
    $('.post_E').click(function(){
        postEvent();
    });
    $('.pattern_E').click(function(){
        patternEvent();
    });
    $('.contractType_E').click(function(){
        contractEvent();
    });
    $('.natureOfContract_E').click(function(){
        NatureEvent();
    });
    $('.numberOfHoursPerMonth_E').click(function(){
        HoursEvent();
    });
    $('.type_E').click(function(){
        TypeEvent();
    });
    function driverStatusEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="driverStatusAdd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="driverStatusEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="VatDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }

    function civiliteEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Civiliteadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="CiviliteEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="CiviliteDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
    function postEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Postadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="PostEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="PostDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
    function patternEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Patternadd()"><i class="fa fa-plus"></i>  <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="PatternEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="PatternDelete()"><i class="fa fa-trash"></i>Delete</span></button></div>';
        $('.addevent').append(html);
    }
    function contractEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Contractadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="ContractEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="ContractDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }

    function NatureEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Natureadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="NatureEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="NatureDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
    function HoursEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Hoursadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="HoursEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="HoursDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }

    function TypeEvent(){
        removeAllResources();
        $('.removeevent').remove();
        var html= '<div class="removeevent"><button class="dt-button buttons-copy buttons-html5" onclick="Typeadd()"><i class="fa fa-plus"></i> <span class="add-icon">Add</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="TypeEdit()"><i class="fa fa-pencil"></i> <span class="edit-icon">Edit</span></button>\n\
  <button class="dt-button buttons-copy buttons-html5" onclick="TypeDelete()"><i class="fa fa-trash"></i> Delete</span></button></div>';
        $('.addevent').append(html);
    }
 function removeAllResources(){
    setupDivCiviliteConfig();
    setupDivContractConfig();
    setupDivHoursConfig();
    setupDivNatureConfig();
    setupDivPatternConfig();
    setupDivTypeConfig();
    setupDivPostConfig();
    setupDriverStatusConfig();
 }
    
});

</script>
