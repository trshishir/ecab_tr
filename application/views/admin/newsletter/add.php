<?php $locale_info = localeconv(); ?>
<link href="<?php echo base_url(); ?>assets/system_design/css/bootstrap-datepicker.min.css" rel="stylesheet">
<style type="text/css">
    .border_cs {
        border:1px solid #999 !important;
    }
</style>
<div class="col-md-12"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs'); ?>
        <div class="row">
            <div class="col-md-6">
                <?php $this->load->view('admin/common/alert'); ?>
                <div class="module">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="module-head">
                    </div>
                    <div class="module-body">
                        <?= form_open("admin/newsletters/add") ?>
                        <div class="row">
                            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                                <div class="form-group">
                                    <label><?= $this->lang->line('status'); ?>*</label>
                                    <select class="form-control border_cs" name="status" required>
                                        <?php foreach (config_model::$newsletter_status as $key => $statusName): ?>
                                            <option <?= set_value('status', $this->input->post('status')) == $statusName ? "selected" : "" ?> value="<?= $statusName ?>"><?= $statusName ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                                <div class="form-group">
                                    <label><?= $this->lang->line('users'); ?>*</label>
                                    <select class="form-control border_cs" name="user_type" required>
<!--                                         <option value="0"><?= $this->lang->line('select_all'); ?></option>
 -->                                        <?php foreach (config_model::$user_types as $key => $user_type): ?>
                                            <option <?= set_value('user_type', $this->input->post('user_type')) == $user_type['id'] ? "selected" : "" ?> value="<?= $user_type['id'] ?>"><?= $user_type['label'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                                <div class="form-group">
                                    <label><?= $this->lang->line('category'); ?>*</label>
                                    <select class="form-control border_cs" name="category" required>
                                        <?php foreach (config_model::$categories as $key => $category): ?>
                                            <option <?= set_value('category', $this->input->post('category')) == $category ? "selected" : "" ?> value="<?= $category ?>"><?= $category ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                                <div class="form-group">
                                    <label><?= $this->lang->line('status'); ?>*</label>
                                    <select class="form-control border_cs" name="user_status" required>
                                        <?php foreach (config_model::$user_status as $key => $u_status): ?>
                                            <option <?= set_value('user_status', $this->input->post('user_status')) == $category ? "selected" : "" ?> value="<?= $u_status ?>"><?= $u_status ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label><?= $this->lang->line('subject'); ?>*</label>
                                    <input type="text" class="form-control border_cs" name="subject" placeholder="Subject" value="<?= set_value('subject', $this->input->post('subject')) ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 15px;">
                                <div class="form-group">                    
                                    <label><?php echo $this->lang->line('newsletter'); ?></label>
                                    <textarea class="ckeditor" id="editor1" name="description" cols="100" rows="10">
                                        <?= set_value('description', $this->input->post('description')) ?>
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <!--<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3" style="margin-bottom: 15px">-->
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" style="margin-bottom: 15px">
                                <div class="form-group">
                                    <input type="text" class="form-control border_cs" id = "email" name="email" value="<?php echo $data->email; ?>" placeholder="Test Email">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2" style="margin-bottom: 15px">
                                <div class="form-group">
                                    <a href="" class="btn btn-default">Send Test</a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-4 col-lg-4" style="margin-bottom: 15px;min-width: 150px;">
                                <div class="form-group">
                                    <input type="hidden" name="send_date_minute" value="">
                                    <input type="hidden" name="template_id" id="template_id" value="">
                                    <select class="form-control shedule border_cs" name="type" required>
                                        <option value="Send now">Send now</option>
                                        <option value="Sheduled">Sheduled</option>
                                    </select>
                                </div>
                                <div class="if-shedule">
                                </div>
                            </div>

                        </div>
                        <?php echo form_close(); ?>

                        <div class="row">
                        </div>                        
                            
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="module">
                    <div class="module-head" style="height: 30px;">
                    </div>
                    <div class="module-body">

                        <div class="row">
                            <div class="col-md-1" style="margin-top: 5px;">
                                <label><?= $this->lang->line('preview'); ?>:</label>
                            </div>
                            <div class="col-xs-12 col-md-4" style="margin-bottom: 15px">
                                <div class="text-right">
                                    <select class="form-control border_cs" name="email_template" id="email_template">
                                    <?php foreach ($config_template as $key => $u_status): ?>
                                            <option <?= set_value('email_template', $this->input->post('template_id')) == $u_status->id ? "selected" : "" ?> value="<?= $u_status->id ?>"><?= $u_status->template_name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4" style="margin-bottom: 15px;min-width: 150px">
                                    <!--<label>Date*</label>-->
                                <button class="btn btn-default" id="apply">Apply</button>
                            </div>
                        </div>
                        <div class="row border_cs" style="background: #fff; height: 480px; padding: 20px; margin-left: 0px; margin-right: 20px;">
                            <div id="select_file"></div>
                        </div>
                        <div class="row" style="margin-right: 20px;">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin: 15px">
                                <div class="text-right">
                                    <a href="<?= base_url("admin/newsletters") ?>" class="btn btn-default fa fa-input">&#xf05e Cancel</a>
                                    <button class="btn btn-default fa fa-input">&#xf0c7 Send</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>        
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>

<script type="text/javascript">
    var temp_id =0;
    $(document).ready(function () {
        $("#email").change(function(){
            $("#email1").val($(this).val());
        });
        $(document).on("change", ".shedule", function () {
            var shedule_val = $(this).val();

            $('.if-shedule').empty();

            if (shedule_val == "Sheduled") {
                var if_schedule = '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="margin-bottom: 15px;max-width: 130px;">' +
                        '<div class="form-group">' +
                        '<input type="text" class="bdatepicker form-control" name="send_date" placeholder="Date" autocomplete="off">' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="margin-bottom: 15px;max-width: 130px;">' +
                        '<div class="form-group">' +
                        '<div class="input-group">' +
                        '<select class="form-control" name="send_date_hour" required><?php for ($i = 0; $i < 24; $i++): $time = $i > 9 ? $i : "0" . $i; ?> <option <?= set_value('send_date_hour', $data->send_date_hour) == $time ? "selected" : "" ?>  value="<?= $time ?>"><?= $time ?></option> <?php endfor; ?> </select> ' +
                        '<span class="input-group-addon normal-addon">H</span>' +
                        '</div>' +
                        '</div>' +
                        '</div>';

                $('.if-shedule').append(if_schedule);

                $(".bdatepicker").datepicker({
                    format: "dd/mm/yyyy",
                }).datepicker("setDate", new Date("<?= set_value('send_date', $data->send_date) ?>"));
            }
        });
        $(".bdatepicker").datepicker({
            format: "dd/mm/yyyy"
        }).datepicker("setDate", new Date("<?= set_value('send_date', $this->input->post('send_date')) ?>"));

        $("#apply").click(function(){
            var vasl = $('#email_template').val();
            // var vasl = $('#').html();
            //  var comment = $.trim($("#editor1").html());
            // console.log(vasl);
            // console.log(desc);
            $.ajax({  
                type: 'GET',  
                url: 'fetchTemplateDescription', 
                data: { id: vasl },
                dataType: "text",
                success: function(response) {
                    // temp_id = vasl;
                    $('#template_id').val(vasl);
                    data = $.parseJSON(response);
                    $('#select_file').html(data);
                    CKEDITOR.instances.editor1.setData(data);
                }
            });

        });
    });
</script>