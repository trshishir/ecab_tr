<?php $locale_info = localeconv(); ?>
<link href="<?php echo base_url(); ?>assets/system_design/css/bootstrap-datepicker.min.css" rel="stylesheet">
<style type="text/css">
    .border_cs {
        border:1px solid #999 !important;
    }
    .dataTables_filter {
        display: block;
        float: left !important;
    }
</style>
<div class="col-md-12"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs'); ?>
        <div class="row">
            <div class="col-md-12">
                <?php $this->load->view('admin/common/alert'); ?>
                <div class="module">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="module-head">
                    </div>
                    <div class="module-body">
                        <?= form_open("admin/newsletters/config") ?>
                        <div class="row">
                            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                                <div class="form-group">
                                    <label><?= $this->lang->line('status'); ?>*</label>
                                    <select class="form-control border_cs" name="status" required>
                                        <?php foreach (config_model::$newsletter_status as $key => $statusName): ?>
                                            <option <?= set_value('status', $this->input->post('status')) == $statusName ? "selected" : "" ?> value="<?= $statusName ?>"><?= $statusName ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                                <div class="form-group">
                                    <label><?= $this->lang->line('template_name'); ?>*</label>
                                    <input type="text" class="form-control border_cs" name="template_name" placeholder="Template Name" value="<?= set_value('template_name', $this->input->post('template_name')) ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 15px;">
                                <div class="form-group">                    
                                    <label><?php echo $this->lang->line('newsletter_template'); ?></label>
                                    <textarea class="ckeditor" id="editor1" name="description" cols="100" rows="10">
                                        <?= set_value('description', $this->input->post('description')) ?>
                                    </textarea>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="margin-top: 15px;">
                                <div class="form-group">                    
                                    <label><?php echo $this->lang->line('shortcode'); ?> 1</label>
                                    <input type="text" class="form-control border_cs" name="shortcode_1" placeholder="" value="<?= set_value('shortcode_1', $this->input->post('template_name')) ?>">
                                </div>
                                <div class="form-group">                    
                                    <label><?php echo $this->lang->line('shortcode'); ?> 2</label>
                                    <input type="text" class="form-control border_cs" name="shortcode_1" placeholder="" value="<?= set_value('shortcode_1', $this->input->post('template_name')) ?>">
                                </div>
                                <div class="form-group">                    
                                    <label><?php echo $this->lang->line('shortcode'); ?> 3</label>
                                    <input type="text" class="form-control border_cs" name="shortcode_1" placeholder="" value="<?= set_value('shortcode_1', $this->input->post('template_name')) ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 15px">
                                <div class="text-right">
                                    <a href="<?= base_url("admin/newsletters/config") ?>" class="btn btn-default fa fa-input">&#xf05e Cancel</a>
                                    <button class="btn btn-default fa fa-input"> &#xf0c7 Save</button>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            
                        </div>                        
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
                    
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
