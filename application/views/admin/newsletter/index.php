<?php $locale_info = localeconv(); ?>
<script type="text/javascript">
    // function googleTranslateElementInit() {
    //     new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
    // }
</script>

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<style>
    /*@media only screen and (min-width: 1400px){*/
    /*    .table-filter input, .table-filter select{*/
    /*        max-width: 9% !important;*/
    /*    }*/
    /*    .table-filter select{*/
    /*        max-width: 95px !important;*/
    /*    }*/
    /*    .table-filter .dpo {*/
    /*        max-width: 90px !important;*/
    /*    }*/
    /*}*/
</style>
<!--<div class="col-md-12">  !--col-md-10 padding white right-p-->
    <!-- <div class="content"> -->
<section id="content">
    <div class="listDiv">
        <?php $this->load->view('admin/common/breadcrumbs'); ?>

        <!--<div class="row">-->
        <!--    <div class="col-md-12">-->
        <!--        <br>-->
        <!--        <div id="google_translate_element"></div>-->
        <!--        <br>-->
        <!--    </div>-->
        <!--</div>-->
        
        <div class="row-fluid">

        <!--<div class="row">
                <div class="col-md-12"> -->
            <?php
                $flashAlert = $this->session->flashdata('alert');
                if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
                    ?>
                    <br>
                    <div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
                        <strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
                        <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>
                <div class="module">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="module-head">
                        <div class="module-filter">
                            <div class="col-md-8">
    <?php echo form_open('newsletters', array("id" => "search_filter")); ?>
        <div class="row  row-no-gutters padding-b">
            <div class="col-md-1">
                <select class="form-control" name="user_type" required>
                    <option value="0"><?= $this->lang->line('select_all'); ?></option>
                    <?php foreach (config_model::$user_types as $key => $user_type): ?>
                        <option <?= set_value('user_type', $this->input->post('user_type')) == $user_type['id'] ? "selected" : "" ?> value="<?= $user_type['id'] ?>"><?= $user_type['label'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-md-1">
                <select class="form-control" name="category" required>
                    <option value="0"><?= $this->lang->line('select_all'); ?></option>
                    <?php foreach (config_model::$categories as $key => $category): ?>
                        <option <?= set_value('category', $this->input->post('category')) == $category ? "selected" : "" ?> value="<?= $category ?>"><?= $category ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-md-1">
                <select class="form-control" name="status" required>
                    <option value="0"><?= $this->lang->line('select_all'); ?></option>
                    <?php foreach (config_model::$newsletter_status as $key => $statusName): ?>
                        <option <?= set_value('status', $this->input->post('status')) == $statusName ? "selected" : "" ?> value="<?= $statusName ?>"><?= $statusName ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-md-1">
                <input type="text" name="date_from" id="date_from" placeholder="From" data-name="date_from" class="dpo" autocomplete="off">
            </div>
            <div class="col-md-1">
                <input type="text" name="date_to" id="date_to" placeholder="To" data-name="date_to" class="dpo" autocomplete="off">
            </div>
            <div class="col-md-2">
                <input type="submit" name="search" id="search" value="Search" class="btn btn-sm btn-default" />
            </div>
         </div>
    <?php echo form_close(); ?>
</div>

                        </div>
                        <!--<h3> <?php if (isset($title)) echo $title; ?></h3>-->
                    </div>
                                    <div class="clearfix"></div>

                    <div class="module-body table-responsive">
                        <table id="newsletter_datatable" class="cell-border table table-bordered table-striped  table-hover dataTable" cellspacing="0" width="100%" data-selected_id="">
                            <thead>
                                <tr>
                                    <th class="no-sort text-center">#</th>
                                    <th class="column-id"><?php echo $this->lang->line('id'); ?></th>
                                    <th class="column-date"><?php echo $this->lang->line('date'); ?></th>
                                    <th class="column-time"><?php echo $this->lang->line('time'); ?></th>
                                    <th class="column-added_by"><?php echo $this->lang->line('added_by'); ?></th>
                                    <th class="column-send_date"><?php echo $this->lang->line('send_date'); ?></th>
                                    <th class="column-send_time"><?php echo $this->lang->line('send_time'); ?></th>
                                    <th class="column-email"><?php echo $this->lang->line('test_email'); ?></th>
                                    <th class="column-template">Template</th>
                                    <th class="column-user_type" ><?php echo $this->lang->line('user_type'); ?></th>
                                    <th class="column-category"><?php echo $this->lang->line('category'); ?></th>
                                    <th class="column-user_status"><?php echo $this->lang->line('user_status'); ?></th>
                                    <th class="column-status"><?php echo $this->lang->line('status'); ?></th>
                                    <th class="column-since"><?php echo $this->lang->line('since'); ?></th>
                                </tr>
                            </thead>

                        </table>
                        <br>
                    </div>
                </div>
            <!-- </div> -->
        </div>
        <!--/.module-->
    </div>
</section>
    <!--/.content-->
<div id="table-filter" class="hide">
</div>

 <script>
    $(document).ready(function() {
        var manageTable = $('#newsletter_datatable').DataTable({
            'ajax': 'newsletters/fetchNewslettersData',
            'order': [],
            'paging': true,
            'pageLength': 15,
            'columnDefs': [ { "targets": 0, "className": "text-center", "width": "2%" },
                            { "targets": 1, "className": "text-center", "width": "2%" },
                            { "targets": 2, "className": "text-center", "width": "3%" },
                            { "targets": 3, "className": "text-center", "width": "3%" },
                            { "targets": 4, "className": "text-center", "width": "3%" },
                            { "targets": 5, "className": "text-center", "width": "4%" },
                            { "targets": 6, "className": "text-center", "width": "4%" },
                            { "targets": 7, "className": "text-center", "width": "4%" },
                            { "targets": 8, "className": "text-center", "width": "4%" },
                            { "targets": 9, "className": "text-center", "width": "10%" },
                            { "targets": 10, "className": "text-center", "width": "8%" },
                            { "targets": 11, "className": "text-center", "width": "4%" },
                            { "targets": 12, "className": "text-center", "width": "5%" },
                            { "targets": 13, "className": "text-center", "width": "8%" }
                          ] ,
            dom: '<"table-filter">B<"toolbar">frtip',
            language: {search: "", searchPlaceholder: "Search"},
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            initComplete: function () {
                $("div.toolbar")
                        .html('<a href="<?= base_url("admin/$active_class/add.php") ?>" class="btn btn-sm btn-default"><i class="fa fa-plus"></i> Add</a> <a href="#" class="btn btn-sm btn-default editBtn"><i class="fa fa-pencil"></i> Edit</a> <a href="#" class="btn btn-sm btn-default delBtn"><i class="fa fa-trash"></i> Delete</a>');
                var filterInp = $("#table-filter");
                if (filterInp.length > 0) {
                    var tableFilter = $("div.table-filter");
                    tableFilter.html(filterInp.html());

                    if ($(".module-filter .dpo").length > 0) {
                        $('.module-filter .dpo[data-name="date_from"]').datepicker({
                            format: "dd-mm-yyyy",
                            "onSelect": function (date) {
                                minDateFilter = new Date(date).getTime();
                                datatables.fnDraw();
                            }
                        });
                        $('.module-filter .dpo[data-name="date_to"]').datepicker({
                            format: "dd-mm-yyyy",
                            "onSelect": function (date) {
                                maxDateFilter = new Date(date).getTime();
                                datatables.fnDraw();
                            }
                        });
                    }
                }

            },
        });
        
        $(".pdfDownload").on('click',function() {
           alert('PDF Download inprogress...'); 
        });
        $(".csvDownload").on('click',function() {
           alert('CSV Download inprogress...'); 
        });
        $(".excelDownload").on('click',function() {
           alert('Excel Download inprogress...'); 
        });
        
        // Search form validation rules
        $("#search_filter").validate({
            rules: {
                search_name: { required:false }
            },
            messages: {
                search_name: {
                    required:"Please enter the name"
                }
            },
            submitHandler: function(form) {
                $.ajax({
                        url: 'newsletters/searchNewslettersData',
                        type: 'post',
                        data: $("#search_filter").serialize(), // /converting the form data into array and sending it to server
                        dataType: 'json',
                        success: function (response) {
                            console.log(response.data);
                            //  $("#manageTable tbody").empty();
                            //if (response.success === true) {
                            console.log("Table" + $.fn.dataTable.isDataTable('#newsletter_datatable'));

                            if ($.fn.dataTable.isDataTable('#newsletter_datatable') == false) {
                                $('#newsletter_datatable').DataTable({
                                    'data': response.data,
                                    'order': [],
                                    'paging': true,
                                    'pageLength': 15,
                                    'columnDefs': [ { "targets": 0, "className": "text-center", "width": "2%" },
                                                    { "targets": 1, "className": "text-center", "width": "2%" },
                                                    { "targets": 2, "className": "text-center", "width": "3%" },
                                                    { "targets": 3, "className": "text-center", "width": "3%" },
                                                    { "targets": 4, "className": "text-center", "width": "3%" },
                                                    { "targets": 5, "className": "text-center", "width": "4%" },
                                                    { "targets": 6, "className": "text-center", "width": "4%" },
                                                    { "targets": 7, "className": "text-center", "width": "4%" },
                                                    { "targets": 8, "className": "text-center", "width": "4%" },
                                                    { "targets": 9, "className": "text-center", "width": "10%" },
                                                    { "targets": 10, "className": "text-center", "width": "8%" },
                                                    { "targets": 11, "className": "text-center", "width": "4%" },
                                                    { "targets": 12, "className": "text-center", "width": "5%" },
                                                    { "targets": 13, "className": "text-center", "width": "8%" }
                                                  ] ,
                                                  dom: '<"table-filter">B<"toolbar">frtip',
                                                language: {search: "", searchPlaceholder: "Search"},
                                                buttons: [
                                                    'copyHtml5',
                                                    'excelHtml5',
                                                    'csvHtml5',
                                                    'pdfHtml5'
                                                ],
                                                initComplete: function () {
                                                    $("div.toolbar")
                                                            .html('<a href="<?= base_url("admin/$active_class/add.php") ?>" class="btn btn-sm btn-default"><i class="fa fa-plus"></i> Add</a> <a href="#" class="btn btn-sm btn-default editBtn"><i class="fa fa-pencil"></i> Edit</a> <a href="#" class="btn btn-sm btn-default delBtn"><i class="fa fa-trash"></i> Delete</a>');
                                                    var filterInp = $("#table-filter");
                                                    if (filterInp.length > 0) {
                                                        var tableFilter = $("div.table-filter");
                                                        tableFilter.html(filterInp.html());

                                                        if ($(".module-filter .dpo").length > 0) {
                                                            $('.module-filter .dpo[data-name="date_from"]').datepicker({
                                                                format: "dd-mm-yyyy",
                                                                "onSelect": function (date) {
                                                                    minDateFilter = new Date(date).getTime();
                                                                    datatables.fnDraw();
                                                                }
                                                            });
                                                            $('.module-filter .dpo[data-name="date_to"]').datepicker({
                                                                format: "dd-mm-yyyy",
                                                                "onSelect": function (date) {
                                                                    maxDateFilter = new Date(date).getTime();
                                                                    datatables.fnDraw();
                                                                }
                                                            });
                                                        }
                                                    }

                                                },
                                });
                            } else {
                                var table = $('#newsletter_datatable').DataTable();
                                table.destroy();
                                $('#newsletter_datatable').DataTable({
                                    'data': response.data,
                                    'order': [],
                                    'paging': true,
                                    'pageLength': 15,
                                    'columnDefs': [ { "targets": 0, "className": "text-center", "width": "2%" },
                                                    { "targets": 1, "className": "text-center", "width": "2%" },
                                                    { "targets": 2, "className": "text-center", "width": "3%" },
                                                    { "targets": 3, "className": "text-center", "width": "3%" },
                                                    { "targets": 4, "className": "text-center", "width": "3%" },
                                                    { "targets": 5, "className": "text-center", "width": "4%" },
                                                    { "targets": 6, "className": "text-center", "width": "4%" },
                                                    { "targets": 7, "className": "text-center", "width": "4%" },
                                                    { "targets": 8, "className": "text-center", "width": "4%" },
                                                    { "targets": 9, "className": "text-center", "width": "10%" },
                                                    { "targets": 10, "className": "text-center", "width": "8%" },
                                                    { "targets": 11, "className": "text-center", "width": "4%" },
                                                    { "targets": 12, "className": "text-center", "width": "5%" },
                                                    { "targets": 13, "className": "text-center", "width": "8%" }
                                                  ] ,
                                                dom: '<"table-filter">B<"toolbar">frtip',
                                                language: {search: "", searchPlaceholder: "Search"},
                                                buttons: [
                                                    'copyHtml5',
                                                    'excelHtml5',
                                                    'csvHtml5',
                                                    'pdfHtml5'
                                                ],
                                                initComplete: function () {
                                                    $("div.toolbar")
                                                            .html('<a href="<?= base_url("admin/$active_class/add.php") ?>" class="btn btn-sm btn-default"><i class="fa fa-plus"></i> Add</a> <a href="#" class="btn btn-sm btn-default editBtn"><i class="fa fa-pencil"></i> Edit</a> <a href="#" class="btn btn-sm btn-default delBtn"><i class="fa fa-trash"></i> Delete</a>');
                                                    var filterInp = $("#table-filter");
                                                    if (filterInp.length > 0) {
                                                        var tableFilter = $("div.table-filter");
                                                        tableFilter.html(filterInp.html());

                                                        if ($(".module-filter .dpo").length > 0) {
                                                            $('.module-filter .dpo[data-name="date_from"]').datepicker({
                                                                format: "dd-mm-yyyy",
                                                                "onSelect": function (date) {
                                                                    minDateFilter = new Date(date).getTime();
                                                                    datatables.fnDraw();
                                                                }
                                                            });
                                                            $('.module-filter .dpo[data-name="date_to"]').datepicker({
                                                                format: "dd-mm-yyyy",
                                                                "onSelect": function (date) {
                                                                    maxDateFilter = new Date(date).getTime();
                                                                    datatables.fnDraw();
                                                                }
                                                            });
                                                        }
                                                    }

                                                },
                                });
                            }
                        }
                    })
            }
        });
    });
</script>