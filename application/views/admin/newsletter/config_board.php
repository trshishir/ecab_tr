<?php $locale_info = localeconv(); ?>
<script type="text/javascript">
    // function googleTranslateElementInit() {
    //     new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
    // }
</script>

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<style>
    /*@media only screen and (min-width: 1400px){*/
    /*    .table-filter input, .table-filter select{*/
    /*        max-width: 9% !important;*/
    /*    }*/
    /*    .table-filter select{*/
    /*        max-width: 95px !important;*/
    /*    }*/
    /*    .table-filter .dpo {*/
    /*        max-width: 90px !important;*/
    /*    }*/
    /*}*/
</style>
<!--<div class="col-md-12">  !--col-md-10 padding white right-p-->
    <!-- <div class="content"> -->
<section id="content">
    <div class="listDiv">
        <?php $this->load->view('admin/common/breadcrumbs'); ?>

        <!--<div class="row">-->
        <!--    <div class="col-md-12">-->
        <!--        <br>-->
        <!--        <div id="google_translate_element"></div>-->
        <!--        <br>-->
        <!--    </div>-->
        <!--</div>-->
        
        <div class="row-fluid">

        <!--<div class="row">
                <div class="col-md-12"> -->
            <?php
                $flashAlert = $this->session->flashdata('alert');
                if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
                    ?>
                    <br>
                    <div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
                        <strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
                        <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>
                <div class="module">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="module-head">
                        <div class="module-filter">


                        </div>
                        <!--<h3> <?php if (isset($title)) echo $title; ?></h3>-->
                    </div>
                                    <div class="clearfix"></div>

                    <div class="module-body table-responsive">
                        <table id="newsletter_config" class="cell-border table table-bordered table-striped  table-hover dataTable" cellspacing="0" width="100%" data-selected_id="">
                            <thead>
                                <tr>
                                    <th class="no-sort text-center">#</th>
                                    <th class="column-id"><?php echo $this->lang->line('id'); ?></th>
                                    <th class="column-date"><?php echo $this->lang->line('date'); ?></th>
                                    <th class="column-time"><?php echo $this->lang->line('time'); ?></th>
                                    <th class="column-added_by"><?php echo $this->lang->line('added_by'); ?></th>
                                    <th class="column-template_name"><?php echo $this->lang->line('template_name'); ?></th>
                                    <th class="column-status"><?php echo $this->lang->line('status'); ?></th>
                                    <th class="column-since"><?php echo $this->lang->line('since'); ?></th>
                                </tr>
                            </thead>

                        </table>
                        <br>
                    </div>
                </div>
            <!-- </div> -->
        </div>
        <!--/.module-->
    </div>
</section>
    <!--/.content-->
<div id="table-filter" class="hide">
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var manageTable = $('#newsletter_config').DataTable({
            'ajax': 'newsletterConfigData',
            'order': [],
            'paging': true,
            "searching": true,
            'pageLength': 15,
            'columnDefs': [ { "targets": 0, "className": "text-center", "width": "2%" },
                            { "targets": 1, "className": "text-center", "width": "5%" },
                            { "targets": 2, "className": "text-center", "width": "5%" },
                            { "targets": 3, "className": "text-center", "width": "5%" },
                            { "targets": 4, "className": "text-center", "width": "5%" },
                            { "targets": 5, "className": "text-center", "width": "10%" },
                            { "targets": 6, "className": "text-center", "width": "5%" },
                            { "targets": 7, "className": "text-center", "width": "10%" }
                          ] ,
            dom: '<"table-filter">B<"toolbar">frtip',
            language: {search: "", searchPlaceholder: "Search"},
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            initComplete: function () {
                $("div.toolbar")
                        .html('<a href="<?= base_url("admin/$active_class/config.php") ?>" class="btn btn-sm btn-default"><i class="fa fa-plus"></i> Add</a> <a href="#" class="btn btn-sm btn-default editBtn"><i class="fa fa-pencil"></i> Edit</a> <a href="#" class="btn btn-sm btn-default delBtn"><i class="fa fa-trash"></i> Delete</a>');
                var filterInp = $("#table-filter");
                if (filterInp.length > 0) {
                    var tableFilter = $("div.table-filter");
                    tableFilter.html(filterInp.html());

                    // if ($(".module-filter .dpo").length > 0) {
                    //     $('.module-filter .dpo[data-name="date_from"]').datepicker({
                    //         format: "dd-mm-yyyy",
                    //         "onSelect": function (date) {
                    //             minDateFilter = new Date(date).getTime();
                    //             datatables.fnDraw();
                    //         }
                    //     });
                    //     $('.module-filter .dpo[data-name="date_to"]').datepicker({
                    //         format: "dd-mm-yyyy",
                    //         "onSelect": function (date) {
                    //             maxDateFilter = new Date(date).getTime();
                    //             datatables.fnDraw();
                    //         }
                    //     });
                    // }
                }

            },
        });
    });
</script>