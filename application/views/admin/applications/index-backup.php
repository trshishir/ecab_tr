<script type="text/javascript">
    function showAdd() {
        hideForms();
        $('.addDiv').show();
        //showAddHead();
    }
    function hideForms() {
        $('.addDiv').hide();
        $('.listDiv').hide();
    }
    function hideAdd() {
        hideForms();
        $('.listDiv').show();
    }
    (function ($, W, D) {
        var JQUERY4U = {};

        JQUERY4U.UTIL =
                {
                    setupFormValidation: function ()
                    {
                        //Additional Methods            
                        $.validator.addMethod("lettersonly", function (a, b) {
                            return this.optional(b) || /^[a-z ]+$/i.test(a)
                        }, "<?php echo $this->lang->line('valid_name'); ?>");

                        $.validator.addMethod("phoneNumber", function (uid, element) {
                            return (this.optional(element) || uid.match(/^([0-9]*)$/));
                        }, "<?php echo $this->lang->line('valid_phone_number'); ?>");


                        $.validator.addMethod("pwdmatch", function (repwd, element) {
                            var pwd = $('#password').val();
                            return (this.optional(element) || repwd == pwd);
                        }, "<?php echo $this->lang->line('valid_passwords'); ?>");

                        //form validation rules
                        $("#jobseeker_addform").validate({
                            rules: {
                                statut: {
                                    required:true
                                },
                                company: {
                                    required:  function(){
                                                    return $("#statut").val() == "2";
                                              }
                                },
                                first_name: {
                                    required: true,
                                    lettersonly: true
                                },
                                email: {
                                    required: true,
                                    email: true
                                },
                                phone:{
                                        required: true,
                                        phoneNumber: true,
                                        rangelength: [10, 11]
                                },
                                password:{
                                        required: true,
                                        rangelength: [8, 30]
                                },
                                confirm_password: {
                                    required:true,
                                    pwdmatch:true
                                }
                            },
                            messages: {
                                statut: {
                                    required:"Please select the statut"
                                },
                                company: {
                                    required:"Please enter the company"
                                },
                                first_name: {
                                    required: "<?php echo $this->lang->line('first_name_valid');?>"
                                },
                                email:{
                                    required: "<?php echo $this->lang->line('email_valid');?>"
                                },
                                phone:{
                                        required: "<?php echo $this->lang->line('phone_valid');?>"
                                },
                                password: {
                                    required: "<?php echo $this->lang->line('password_valid');?>"
                                },
                                password_confirm:{
                                        required: "<?php echo $this->lang->line('confirm_password_valid');?>"
                                }
                            }, 
                            submitHandler: function(form) {
                                form.submit();
                            }
                        });
                    }
                }

        //when the dom has loaded setup form validation rules
        $(D).ready(function ($) {
            JQUERY4U.UTIL.setupFormValidation();
        });

    })(jQuery, window, document);
    
</script>
<section id="content">
    <div class="listDiv">
        <?php $this->load->view('admin/common/breadcrumbs'); ?>
        <div class="row-fluid">
            <?php   $flashAlert = $this->session->flashdata('alert');
                    if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
            ?>
                <br>
                <div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
                    <strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
                    <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php   } ?>

            <div class="module">
                <?php echo $this->session->flashdata('message'); ?>
                <div class="module-head">
                       <div class="module-filter">
                           <div class="col-md-8">
                                <?php echo form_open('jobseekers', array("id" => "search_filter")); ?>
                                    <div class="row  row-no-gutters padding-b">
                                        <div class="col-md-2">
                                             <input type="text" name="search_name" id="search_name" placeholder="Name" class="input-large form-control" data-name="name">
                                        </div>
                                        <div class="col-md-2">
                                             <input type="text" name="search_email" id="search_email" placeholder="Email" data-name="email" class="form-control">
                                        </div>
                                        <div class="col-md-2">
                                             <input type="text" name="search_phone" id="search_phone" placeholder="Phone" data-name="phone" class="form-control">
                                        </div>
                                        <div class="col-md-1">
                                            <input type="text" name="date_from" id="date_from" placeholder="From" data-name="date_from" class="dpo">
                                        </div>
                                        <div class="col-md-1">
                                            <input type="text" name="date_to" id="date_to" placeholder="To" data-name="date_to" class="dpo">
                                        </div>
                                        <div class="col-md-1">
                                            <select class="form-control" name="status" id="status" data-name="status">
                                                 <option value="">All Status</option>
                                                 <option value="1">Active</option>
                                                 <option value="0">Inactive</option>
                                             </select>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="submit" name="search" id="search" value="Search" class="btn btn-sm btn-default" />
                                        </div>
                                     </div>
                                <?php echo form_close(); ?>
                            </div>
                           <div class="col-md-4 no-right-pad">
                               <div class="dt-buttons">
                                   <a href="javascript:;" class="btn btn-sm btn-default" onclick="showAdd();"><i class="fa fa-plus"></i> Add</a> 
                                   <a href="Javascript:;" class="btn btn-sm btn-default editBtn"><i class="fa fa-pencil"></i> Edit</a> 
                                   <a href="Javascript:;" class="btn btn-sm btn-default delBtn"><i class="fa fa-trash"></i> Delete</a>
                                   <!--<a href="#" class="btn btn-sm btn-default delBtn"><i class="fa fa-download"></i> Import</a>-->
                                   <a href="Javascript:;" class="btn btn-sm btn-default excelDownload"><i class="fa fa-download"></i> Excel</a>
                                   <a href="Javascript:;" class="btn btn-sm btn-default csvDownload"><i class="fa fa-download"></i> CSV</a>
                                   <a href="Javascript:;" class="btn btn-sm btn-default pdfDownload"><i class="fa fa-download"></i> PDF</a>
                               </div>
                           </div>
                        </div>
                </div>
                <div class="clearfix"></div>
                <div class="module-body table-responsive">
                    <table id="example" class="table table-bordered table-striped  table-hover dataTable" cellspacing="0" width="100%" data-selected_id="">
                        <thead>
                            <tr>
                                <th class="no-sort text-center"><input type='checkbox' name='allJobseekers' id="allJobseekers" value='' onclick="Javascript:selectAllJobseekers();" /></th>
                                <th class="column-id"><?php echo $this->lang->line('id'); ?></th>
                                <th class="column-date"><?php echo "Date"; ?></th>
                                <th class="column-date"><?php echo "Time"; ?></th>
                                <th class="column-name" ><?php echo $this->lang->line('name'); ?></th>
                                <th class="column-email"><?php echo $this->lang->line('email'); ?></th>
                                <th class="column-phone"><?php echo $this->lang->line('phone'); ?></th>
                                <th class="column-status"><?php echo $this->lang->line('status'); ?></th>
                                <th class="column-since"><?php echo "Since"; ?></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="addDiv" style="display:none;">
        <?php $this->data['show_subtitle'] = true ; $this->load->view('admin/common/breadcrumbs'); ?>
        <div class="row-fluid">
            <?php   $flashAlert = $this->session->flashdata('alert');
                    if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
            ?>
                <br>
                <div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
                    <strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
                    <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php   } ?>
        </div>
        <?php echo form_open('clients/add', array("id" => "client_addform")); ?>
        <div class="col-md-12">
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-3" >
                    <div class="form-group">
                        <div class="col-md-4" style="margin-top: 10px;">
                            <span style="font-weight: bold;">Statut</span>
                        </div>
                        <div class="col-md-8" style="padding: 0px;">
                            <select class="form-control" name="status">
                                <option value="0">show</option>
                                <option value="1">hide</option>
                            </select>
                        </div>

        <?php echo form_open('applications/add', array("id" => "jobseeker_addform")); ?>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4 form-group">  
                        <label> Statut </label>                                
                        <select class="form-control" name="statut" id="statut" tabindex="1">
                            <option value="1">Independent</option>
                            <option value="2">Company</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3" >
                    <div class="form-group">
                        <div class="col-md-4" style="margin-top: 10px; ">
                            <span style="font-weight: bold;">Civilite</span>
                        </div>
                        <div class="col-md-8" style="padding: 0px;">
                            <select class="form-control" name="civilite">
                                <option value="Mr">Mr</option>
                                <option value="Miss">Miss</option>
                                <option value="Mme">Mme</option>
                            </select>
                        </div>

                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <div class="col-md-4" style="margin-top: 5px;">
                            <span style="font-weight: bold;">Nom</span>
                        </div>
                        <div class="col-md-8" style="padding: 0px;">
                            <input name="nom" required="required"  class="form-control" type="text">
                        </div>

                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <div class="col-md-4" style="margin-top: 10px;">
                            <span style="font-weight: bold;">Prenom</span>
                        </div>
                        <div class="col-md-8" style="padding: 0px;">
                            <input name="pre_nom" required="required"  class="form-control" type="text">
                        </div>

                    </div>
                </div>

            
    </div>
    
    <div class="row" style="margin-top: 10px;">
        <div class="col-md-3">
                <div class="form-group">
                    <div class="col-md-4" style="margin-top: 10px;">
                        <span style="font-weight: bold;">Company</span>
                    </div>
                    <div class="col-md-8" style="padding: 0px;">
                        <input name="company" required="required"  class="form-control" type="text">
                
                <div class="row">
                    <div class="col-md-6 form-group"> 
                        <label>Location</label>
                        <?php echo form_input($location); ?>
                        <?php echo form_error('location'); ?> 
                    </div>
                    <div class="col-md-6 form-group"> 
                        <label>Date of Birth</label>
                        <?php echo form_input($dob); ?>
                        <?php echo form_error('dob'); ?> 
                    </div>
                </div>
            </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="col-md-4" style="margin-top: 10px;">
                    <span style="font-weight: bold;">Address</span>
                </div>
                <div class="col-md-8" style="padding: 0px;">
                    <input name="address1" required="required"  class="form-control" type="text">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="col-md-4" style="margin-top: 10px;">
                    <span style="font-weight: bold;">Address </span>
                </div>
                <div class="col-md-8" style="padding: 0px;">
                    <input name="address2" required="required"  class="form-control" type="text">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="col-md-4" style="margin-top: 10px;">
                    <span style="font-weight: bold;">Email</span>
                </div>
                <div class="col-md-8" style="padding: 0px;">
                    <input name="email" required="required"  class="form-control" type="text">
                </div>
            </div>
        </div>

        
    </div>

    <div class="row" style="margin-top: 10px;">
        <div class="col-md-3">
            <div class="form-group">
                <div class="col-md-4" style="margin-top: 10px;">
                    <span style="font-weight: bold;">Phone</span>
                </div>
                <div class="col-md-8" style="padding: 0px;">
                    <input name="phone" required="required"  class="form-control" type="text">
                </div>
            </div>
        </div>
        
        
        <div class="col-md-3">
            <div class="form-group">
                <div class="col-md-4" style="margin-top: 10px;">
                    <span style="font-weight: bold;">Mobile Phone</span>
                </div>
                <div class="col-md-8" style="padding: 0px;">
                    <input name="mobile" required="required"  class="form-control" type="text">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="col-md-4" style="margin-top: 10px;">
                    <span style="font-weight: bold;">Fax</span>
                </div>
                <div class="col-md-8" style="padding: 0px;">
                    <input name="fax" required="required"  class="form-control" type="text">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="col-md-4" style="margin-top: 10px;">
                <span style="font-weight: bold;">Code Postal</span>
                </div>
                <div class="col-md-8" style="padding: 0px;">
                <input name="code_postal" required="required"  class="form-control" type="text">
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-top: 10px;">
        <div class="col-md-3">
            <div class="form-group">
                
                    <div class="col-md-4" style="margin-top: 10px;">
                        <span style="font-weight: bold;">Ville</span>
                    </div>
                    <div class="col-md-8" style="padding: 0px;">
                        <input name="ville" required="required"  class="form-control" type="text">
                    </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="col-md-4" style="margin-top: 10px;">
                    <span style="font-weight: bold;">Type Of Client</span>
                </div>
                <div class="col-md-8" style="padding: 0px;">
                    
                    <select required="required"  class="form-control" name="client_type" tabindex="1">
                        <option value="">Select Client Type</option>

                        <?php foreach($clienttype as $data):?>
                            <option value="<?=$data->id;?>"><?=$data->type;?></option>
                        <?php endforeach;?>
                    </select>
                        <!-- <input name="paysdenaissance" class="form-control" type="text"> -->
                    
                </div>

            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="col-md-4" style="margin-top: 10px;">
                    <span style="font-weight: bold;">Birthday</span>
                </div>
                <div class="col-md-8" style="padding: 0px;">
                    <input name="birthday" class="form-control datepicker hasDatepicker" required type="text" id="dp1591642843460">
                </div>

            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="col-md-4" style="margin-top: 10px;">
                    <span style="font-weight: bold;">Disabled</span>
                <div class="row">
                    <div class="col-md-6 form-group"> 
                        <label>City</label>
                        <?php echo form_input($city); ?>
                        <?php echo form_error('city'); ?> 
                    </div>
                    <div class="col-md-6 form-group"> 
                        <label>Zipcode</label>
                        <?php echo form_input($zipcode); ?>
                        <?php echo form_error('zipcode'); ?> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group"> 
                        <label>Mobile</label>
                        <?php echo form_input($mobile); ?>
                        <?php echo form_error('mobile'); ?> 
                    </div> 
                    <div class="col-md-6 form-group"> 
                        <label>Fax</label>
                        <?php echo form_input($fax); ?>
                        <?php echo form_error('fax'); ?> 
                    </div>
                </div>
                <div class="col-md-8" style="padding: 0px;">
                    <select class="form-control ClientDisabled" required name="disabled">
                        <option value="No">No</option>
                        <option value="Yes">Yes</option>
                    </select>
                </div>

            </div>
        </div>
        </div>
            <div class="row" style="margin-top: 10px;">

                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-md-4" style="margin-top: 5px;">
                                <span style="font-weight: bold;">Payment Methode</span>
                            </div>
                            <div class="col-md-8" style="padding: 0px;">
                                <select class="form-control" required name="payment_method">
                                    <option value="">Select</option>
                                    <?php foreach($clientpayment as $data):?>
                                        <option value="<?=$data->id;?>"><?=$data->payment;?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>

                        </div>
                    </div>
                
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-md-4" style="margin-top: 5px;">
                                <span style="font-weight: bold;">Delay of Payment</span>
                            </div>
                            <div class="col-md-8" style="padding: 0px;">
                                <select class="form-control"  name="payment_delay">
                                    <option value="">Select</option>
                                   
                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-3">
                            <div class="form-group">
                                <div class="col-md-4" style="margin-top: 10px;">
                                    <span style="font-weight: bold;">Password</span>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                    <input type="password" name="password" required class="form-control">
                                </div>
                            </div>
                        </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-md-4" style="margin-top: 10px;">
                                <span style="font-weight: bold;">Note</span>
                            </div>
                            <div class="col-md-8" style="padding: 0px;">
                                <textarea class="form-control" name="note"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
    
         <div style="clear: both"></div>
        <div class="row">
                <div class="col-md-12"> 
                    <div class="pull-right">
                        <input type="submit" class="btn btn-default" value="Save" tabindex="16" />  
                        <input type="button" class="btn btn-default" value="Cancel" onclick="return hideAdd();" tabindex="17" />  
                    </div>
            <div class="col-md-12"> 
                <div class="pull-right">
                    <input type="submit" class="btn btn-default" value="Save" tabindex="18" />  
                    <input type="button" class="btn btn-default" value="Cancel" onclick="return hideAdd();" tabindex="17" />  
                </div>
            </div>
        </div>
        
        <?php echo form_close(); ?>
    </div>
</section>
</div>
 <script>
    $(document).ready(function() {
        var manageTable = $('#manageJobseekerTable').DataTable({
            'ajax': 'applications/fetchJobseekersData',
            'order': [],
            'paging': true,
            'pageLength': 15,
            'columnDefs': [ { "targets": 0, "className": "text-center", "width": "4%" },
                            { "targets": 1, "className": "text-center", "width": "4%" },
                            { "targets": 2, "className": "text-center", "width": "4%" },
                            { "targets": 3, "className": "text-center", "width": "4%" },
                            { "targets": 4, "className": "text-center", "width": "10%" },
                            { "targets": 5, "className": "text-center", "width": "8%" },
                            { "targets": 6, "className": "text-center", "width": "4%" },
                            { "targets": 7, "className": "text-center", "width": "5%" },
                            { "targets": 8, "className": "text-center", "width": "8%" }
                          ] 
        });
        
        $(".pdfDownload").on('click',function() {
           alert('PDF Download inprogress...'); 
        });
        $(".csvDownload").on('click',function() {
           alert('CSV Download inprogress...'); 
        });
        $(".excelDownload").on('click',function() {
           alert('Excel Download inprogress...'); 
        });
        
        // Search form validation rules
        $("#search_filter").validate({
            rules: {
                search_name: { required:false }
            },
            messages: {
                search_name: {
                    required:"Please enter the name"
                }
            },
            submitHandler: function(form) {
                $.ajax({
                        url: 'applications/jobseekerSearchData',
                        type: 'post',
                        data: $("#search_filter").serialize(), // /converting the form data into array and sending it to server
                        dataType: 'json',
                        success: function (response) {
                            console.log(response.data);
                            //  $("#manageTable tbody").empty();
                            //if (response.success === true) {
                            console.log("Table" + $.fn.dataTable.isDataTable('#manageJobseekerTable'));

                            if ($.fn.dataTable.isDataTable('#manageJobseekerTable') == false) {
                                $('#manageJobseekerTable').DataTable({
                                    'data': response.data,
                                    'order': [],
                                    'paging': true,
                                    'pageLength': 15,
                                    'columnDefs': [ { "targets": 0, "className": "text-center", "width": "4%" },
                                                    { "targets": 1, "className": "text-center", "width": "4%" },
                                                    { "targets": 2, "className": "text-center", "width": "4%" },
                                                    { "targets": 3, "className": "text-center", "width": "4%" },
                                                    { "targets": 4, "className": "text-center", "width": "10%" },
                                                    { "targets": 5, "className": "text-center", "width": "8%" },
                                                    { "targets": 6, "className": "text-center", "width": "4%" },
                                                    { "targets": 7, "className": "text-center", "width": "5%" },
                                                    { "targets": 8, "className": "text-center", "width": "8%" }
                                                  ]
                                });
                            } else {
                                var table = $('#manageJobseekerTable').DataTable();
                                table.destroy();
                                $('#manageJobseekerTable').DataTable({
                                    'data': response.data,
                                    'order': [],
                                    'paging': true,
                                    'pageLength': 15,
                                    'columnDefs': [ { "targets": 0, "className": "text-center", "width": "4%" },
                                                    { "targets": 1, "className": "text-center", "width": "4%" },
                                                    { "targets": 2, "className": "text-center", "width": "4%" },
                                                    { "targets": 3, "className": "text-center", "width": "4%" },
                                                    { "targets": 4, "className": "text-center", "width": "10%" },
                                                    { "targets": 5, "className": "text-center", "width": "8%" },
                                                    { "targets": 6, "className": "text-center", "width": "4%" },
                                                    { "targets": 7, "className": "text-center", "width": "5%" },
                                                    { "targets": 8, "className": "text-center", "width": "8%" }
                                                  ]
                                });
                            }
                        }
                    })
            }
        });
    });
</script>