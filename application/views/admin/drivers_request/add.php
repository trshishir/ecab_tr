<?php $locale_info = localeconv(); ?>

<div class="col-md-12"><!--col-md-10 padding white right-p-->

    <div class="content">

        <?php $this->load->view('admin/common/breadcrumbs'); ?>
        <div class="row">

            <div class="col-md-12">

                <?php $this->load->view('admin/common/alert'); ?>

                <div class="module" style="height: 75vh;">

                    <?php echo $this->session->flashdata('message'); ?>
                    <?php echo validation_errors(); ?>

                    <div class="module-head">

                    </div>

                    <div class="module-body">

                        <div class="row">

                            <div class="col-xs-3">

                                <div class="form-group">

                                    <label>Please select request type*</label>

                                    <select class="form-control" name="request_type" id="request_type" required>
                                        <option value="">--- Select request type ---</option>
                                        <option value="absence_request">Absence Request</option>
                                        <option value="vacation_request">Medical Vacation Request</option>
                                        <option value="salary_advance_request">Salary Advance Request</option>
                                        <option value="notes_refund_request" selected>Notes Refund Request</option>
                                    </select>

                                </div>

                            </div>

                        </div>
                        <?php $attributes = array("enctype" => 'multipart/form-data'); ?>
                        <div id="absence_request" class="drivers_request_div" style="display: none;">

                            <?= form_open("admin/drivers_requests/absence_request_store", $attributes) ?>
                            <input type="hidden" name="type" value="absence_request">
                            <input type="hidden" class="form-control" name="request" readonly required value="Absence">
                            <div class="row">

                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>Status*</label>

                                        <select class="form-control" name="status" id="status1" required>
                                            <option value="">--- Select status ---</option>
                                            <!--                                            <option value="1">New</option>-->
                                            <option value="2" selected>Pending</option>
                                            <option value="3">Approved</option>
                                            <option value="4">Declined</option>
                                            <option value="5">Closed</option>
                                        </select>

                                    </div>

                                </div>

                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>Driver*</label>

                                        <select class="form-control" name="driver_name">
                                            <option>Select Driver</option>
                                            <?php foreach ($drivers as $driver) { ?>
                                                <option value="<?php echo $driver->id ?>"><?php echo $driver->prenom . ' ' . $driver->nom ?></option>
                                            <?php } ?>
                                        </select>

                                    </div>

                                </div>

                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>From*</label>

                                        <input type="text" class="form-control bdatepicker" name="from_date"
                                               autocomplete="off" required>

                                    </div>

                                </div>
                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>Day*</label>

                                        <select class="form-control" name="from_morning" required>
                                            <option value="">--- Select ---</option>
                                            <option value="all_day" selected>All day</option>
                                            <option value="morning">Morning</option>
                                            <option value="evening">Evening</option>
                                        </select>

                                    </div>

                                </div>

                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>To*</label>

                                        <input type="text" class="form-control bdatepicker" name="to_date"
                                               autocomplete="off" required>

                                    </div>

                                </div>

                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>Day*</label>

                                        <select class="form-control" name="to_morning" required>
                                            <option value="">--- Select ---</option>
                                            <option value="all_day" selected>All day</option>
                                            <option value="morning">Morning</option>
                                            <option value="evening">Evening</option>
                                        </select>

                                    </div>

                                </div>

                            </div>

                            <div class="row">
                                <div class="col-xs-6">

                                    <div class="form-group">

                                        <label>Description*</label>

                                        <textarea class="form-control" name="text" rows="4" required></textarea>

                                    </div>

                                </div>

                                <div class="clearfix"></div>

                                <div class="col-xs-6" id="reason1" style="display:none;">

                                    <div class="form-group">

                                        <label>Raison*</label>

                                        <textarea class="form-control" name="comment" id="comment1" disabled rows="4"
                                                  required></textarea>

                                    </div>

                                </div>

                                <div class="clearfix"></div>

                                <div class="col-md-3" style="padding-top: 5px;width:230px; white-space: nowrap">
                                    Add proof document files (optional):
                                </div>
                                <div class="col-md-6" id="attachDiv">
                                    <div class="row">
                                        <div class="col-xs-6" style="overflow: hidden">
                                        <label for="1"><i class="btn">Choose file</i><i class="name"></i></label>
                                            <input id="1" type="file" name="attachment[]" style="display:none">
                                        </div>
                                        <div class="col-xs-4">
                                            <button type="button"
                                                    class="btn btn-circle btn-success btn-sm addFile custombtn"><i
                                                        class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <br>
                                <div class="col-md-12">
                                    <div class="text-right">
                                        <a href="<?= base_url("admin/drivers_requests") ?>" class="btn btn-default"><i
                                                    class="fa fa-times"></i> Cancel</a>
                                        <button class="btn btn-default"><i class="fa fa-save"></i> Save</button>

                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <!-- </form> -->
                            <?php echo form_close(); ?>
                        </div>


                        <div id="vacation_request" class="drivers_request_div" style="display: none;">
                            <?= form_open("admin/drivers_requests/vacation_request_store", $attributes) ?>
                            <input type="hidden" name="type" value="vacation_request">
                            <div class="row">

                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>Status*</label>

                                        <select class="form-control" name="status" id="status2" required>
                                            <option value="">--- Select status ---</option>
                                            <!--                                            <option value="1">New</option>-->
                                            <option value="2" selected>Pending</option>
                                            <option value="3">Approved</option>
                                            <option value="4">Declined</option>
                                            <option value="5">Closed</option>
                                        </select>

                                    </div>

                                </div>

                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>Driver*</label>

                                        <select class="form-control" name="driver_name">
                                            <option>Select Driver</option>
                                            <?php foreach ($drivers as $driver) { ?>
                                                <option value="<?php echo $driver->id ?>"><?php echo $driver->prenom . ' ' . $driver->nom ?></option>
                                            <?php } ?>
                                        </select>

                                    </div>

                                </div>

                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>From*</label>

                                        <input type="text" class="form-control bdatepicker" name="from_date"
                                               autocomplete="off" required>

                                    </div>

                                </div>

                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>Day*</label>

                                        <select class="form-control" name="from_morning" required>
                                            <option value="">--- Select ---</option>
                                            <option value="all_day" selected>All day</option>
                                            <option value="morning">Morning</option>
                                            <option value="evening">Evening</option>
                                        </select>

                                    </div>

                                </div>

                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>To*</label>

                                        <input type="text" class="form-control bdatepicker" name="to_date"
                                               autocomplete="off" required>

                                    </div>

                                </div>

                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>Day*</label>

                                        <select class="form-control" name="to_morning" required>
                                            <option value="">--- Select ---</option>
                                            <option value="all_day" selected>All day</option>
                                            <option value="morning">Morning</option>
                                            <option value="evening">Evening</option>
                                        </select>

                                    </div>

                                </div>
                            </div>

                            <div class="row">

                                <div class="col-xs-6">

                                    <div class="form-group">

                                        <label>Description*</label>

                                        <textarea class="form-control" name="text" rows="4" required></textarea>

                                    </div>

                                </div>

                                <div class="clearfix"></div>

                                <div class="col-xs-6" id="reason2" style="display:none;">

                                    <div class="form-group">

                                        <label>Raison*</label>

                                        <textarea class="form-control" name="comment" id="comment2" disabled rows="4"
                                                  required></textarea>

                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-3" style="padding-top: 5px;width:230px; white-space: nowrap;">
                                    Add Medical proof document files*
                                </div>
                                <div class="col-md-6" id="attachDiv2">
                                    <div class="row">
                                        <div class="col-xs-6" style="overflow: hidden">
                                        <label for="2"><i class="btn">Choose file</i><i class="name"></i></label>

                                            <input id=2 type="file" name="attachment[]" required style="display: none;">
                                        </div>
                                        <div class="col-xs-4">
                                            <button type="button"
                                                    class="btn btn-circle btn-success btn-sm addFile2 custombtn"><i
                                                        class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="text-right">
                                        <a href="<?= base_url("admin/drivers_requests") ?>" class="btn btn-default"><i
                                                    class="fa fa-times"></i> Cancel</a>
                                        <button class="btn btn-default"><i class="fa fa-save"></i> Save</button>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>

                        <div id="salary_advance_request" class="drivers_request_div" style="display: none;">
                            <?= form_open("admin/drivers_requests/salary_request_store", $attributes) ?>
                            <input type="hidden" class="form-control" name="request" readonly required
                                   value="Salary Advance">
                            <div class="row">
                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>Status*</label>

                                        <select class="form-control" name="status" id="status3" required>
                                            <option value="">--- Select status ---</option>
                                            <!--                                            <option value="1">New</option>-->
                                            <option value="2" selected>Pending</option>
                                            <option value="3">Approved</option>
                                            <option value="4">Declined</option>
                                            <option value="5">Closed</option>
                                        </select>

                                    </div>
                                </div>

                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>Driver*</label>

                                        <select class="form-control" name="driver_name">
                                            <option>Select Driver</option>
                                            <?php foreach ($drivers as $driver) { ?>
                                                <option value="<?php echo $driver->id ?>"><?php echo $driver->prenom . ' ' . $driver->nom ?></option>
                                            <?php } ?>
                                        </select>

                                    </div>

                                </div>

                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>Requested Amount*</label>

                                        <input type="number" class="form-control" name="amount" id="amount" required>

                                    </div>

                                </div>


                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>Times*</label>

                                        <select class="form-control" name="time_deduce" id="time_deduce" required>
                                            <option value="">--- Times ---</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                        </select>

                                    </div>

                                </div>


                                <div class="clearfix"></div>

                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>Amount to Deduce</label>

                                        <input type="number" class="form-control" readonly id="paid_amount" required>

                                    </div>

                                </div>
                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>From*</label>
                                        <input type="hidden" id="month_number" value="<?php echo date('n'); ?>">
                                        <input type="text" class="form-control" name="from_month" readonly required
                                               value="<?php echo date('F', strtotime('first day of +1 month')); ?>">

                                    </div>

                                </div>

                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>To*</label>

                                        <input type="text" class="form-control" name="to_month" id="to_month" readonly>

                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>Paid Amount By Employeer</label>

                                        <input type="number" class="form-control" name="paid_by_employeer"
                                               id="paid_by_employeer">

                                    </div>

                                </div>
                                <!-- <div class="clearfix"></div>  -->


                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>Date</label>

                                        <input type="text" class="form-control bdatepicker" name="date_employeer"
                                               autocomplete="off">

                                    </div>

                                </div>

                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>Payment Method</label>

                                        <select class="form-control" name="payment_method_employeer">
                                            <option value="">--- Select payment method ---</option>
                                            <option value="cash">Cash</option>
                                            <option value="cheque">Cheque</option>
                                            <option value="credit_card">Credit Card</option>
                                            <option value="Bank Wire">Bank Wire</option>
                                            <option value="Bank Debit">Bank Debit</option>
                                        </select>

                                    </div>
                                </div>

                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>By Employer*</label>

                                        <input type="text" class="form-control" readonly id="due_employeer"
                                               name="rest_due_employeer" required>

                                    </div>

                                </div>

                                <div class="clearfix"></div>


                                <div class="driver-payment-group">
                                    <div class="col-xs-2">

                                        <div class="form-group">

                                            <label>Paid Amount By Driver</label>

                                            <input type="number" class="form-control" name="paid_by_driver"
                                                   id="paid_by_driver">

                                        </div>

                                    </div>
                                    <div class="col-xs-2">

                                        <div class="form-group">

                                            <label>Date</label>

                                            <input type="text" class="form-control bdatepicker" name="date"
                                                   autocomplete="off">

                                        </div>

                                    </div>
                                    <div class="col-xs-2">

                                        <div class="form-group">

                                            <label>Payment Method</label>

                                            <select class="form-control" name="payment_method">
                                                <option value="">--- Select payment method ---</option>
                                                <option value="cash">Cash</option>
                                                <option value="cheque">Cheque</option>
                                                <option value="credit_card">Credit Card</option>
                                                <option value="Bank Wire">Bank Wire</option>
                                                <option value="Bank Debit">Bank Debit</option>
                                            </select>

                                        </div>
                                    </div>

                                </div>
                                <div class="col-xs-2" id="restToPay">
                                    <button class="add-field">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                    <div class="form-group" style="margin-left: 30px">
                                        <label>By Driver*</label>
                                        <input type="text" class="form-control" readonly id="due" name="rest_due"
                                               required>
                                    </div>
                                </div>
                                <div id="driverPaymentGroup">
                                    <div class="clearfix"></div>
                                </div>


                                <div class="clearfix"></div>

                                <div class="col-xs-6">

                                    <div class="form-group">

                                        <label>Description*</label>

                                        <textarea class="form-control" name="text" rows="4" required></textarea>

                                    </div>

                                </div>

                                <div class="clearfix"></div>

                                <div class="col-xs-6" id="reason3" style="display:none;">

                                    <div class="form-group">

                                        <label>Raison*</label>

                                        <textarea class="form-control" name="comment" id="comment3" disabled rows="4"
                                                  required></textarea>

                                    </div>

                                </div>

                                <div class="clearfix"></div>
                                <!-- <div class="col-xs-4">

                                    <div class="form-group">

                                        <label>Add proof document files (optional)</label>

                                        <input type="file" class="form-control" name="attachments[]" multiple >

                                    </div>

                                </div>   -->

                                <div class="col-md-3" style="padding-top: 5px;width:230px;white-space: nowrap">
                                    Add proof document files (optional)
                                </div>
                                <div class="col-md-6" id="attachDiv3">
                                    <div class="row">
                                        <div class="col-xs-6" style="overflow: hidden">
                                        <label for="3"><i class="btn">Choose file</i><i class="name"></i></label>

                                            <input id=3 type="file" name="attachment[]" style="display: none;">
                                        </div>
                                        <div class="col-xs-4">
                                            <button type="button"
                                                    class="btn btn-circle btn-success btn-sm addFile3 custombtn"><i
                                                        class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-xs-12">

                                    <div class="form-group">

                                        <input type="submit" class="btn btn-primary" value="Submit">

                                    </div>

                                </div> -->
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="text-right">
                                        <a href="<?= base_url("admin/drivers_requests") ?>" class="btn btn-default"><i
                                                    class="fa fa-times"></i> Cancel</a>
                                        <button class="btn btn-default"><i class="fa fa-save"></i> Save</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>

                    <div id="notes_refund_request" class="drivers_request_div">
                        <?= form_open("admin/drivers_requests/notes_request_store", $attributes) ?>
                        <div class="row">
                            <div class="col-xs-2">

                                <div class="form-group">

                                    <label>Status*</label>

                                    <select class="form-control" name="status" id="status4" required>
                                        <option value="">--- Select status ---</option>
                                        <!--                                        <option value="1">New</option>-->
                                        <option value="2" selected>Pending</option>
                                        <option value="3">Approved</option>
                                        <option value="4">Declined</option>
                                        <!--                                        <option value="5">Closed</option>-->
                                    </select>

                                </div>
                            </div>

                            <div class="col-xs-2">

                                <div class="form-group">

                                    <label>Driver*</label>
                                    
                                        <select class="form-control" name="driver_name">
                                            <option>Select Driver</option>
                                            <?php foreach ($drivers as $driver) { ?>
                                                <option value="<?php echo $driver->id ?>"><?php echo $driver->prenom . ' ' . $driver->nom ?></option>
                                            <?php } ?>
                                        </select>

                                </div>

                            </div>

                            <div class="col-xs-2">

                                <div class="form-group">

                                    <label>Date*</label>

                                    <input type="text" class="form-control bdatepicker" autocomplete="off" name="date"
                                           required>

                                </div>

                            </div>

                            <div class="col-xs-2">

                                <div class="form-group">

                                    <label>Time*</label>

                                    <input type="text" id="time" class="form-control" name="time">

                                </div>

                            </div>

                            <div class="col-xs-2">

                                <div class="form-group">

                                    <label>Category*</label>

                                    <select class="form-control" name="category" required>
                                        <option value="">--- Select category ---</option>
                                        <option value="carburant">Carburant</option>
                                        <option value="parking">Parking</option>
                                        <option value="car wash">Car Wash</option>
                                        <option value="others">Others</option>

                                    </select>

                                </div>
                            </div>
                            <!--                             <div class="clearfix"></div>-->
                            <div class="col-xs-2">
                                <div class="form-group">
                                    <label>Amount*</label>
                                    <input type="number" class="form-control" id="notes_amount" name="notes_amount"
                                           required>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="notes-payment-group">
                                <div class="col-xs-2">
                                    <div class="form-group">
                                        <label>Paid Amount</label>
                                        <input type="number" class="form-control" id="notes_paid_amount"
                                               name="notes_paid_amount">
                                    </div>
                                </div>
                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>Date of payment</label>

                                        <input type="text" class="form-control bdatepicker" autocomplete="off"
                                               name="payment_date">

                                    </div>

                                </div>
                                <div class="col-xs-2">

                                    <div class="form-group">

                                        <label>Payment Method</label>

                                        <select class="form-control" name="payment_method">
                                            <option value="">--- Select payment method ---</option>
                                            <option value="cash">Cash</option>
                                            <option value="cheque">Cheque</option>
                                            <option value="credit_card">Credit Card</option>
                                            <option value="Bank Wire">Bank Wire</option>
                                        </select>

                                    </div>
                                </div>

                            </div>
                            <div class="col-xs-2" id="restNotes">
                                <button class="add-field">
                                    <i class="fa fa-plus"></i>
                                </button>
                                <div class="form-group" style="padding-left: 35px">
                                    <label>Rest Due*</label>
                                    <input type="text" class="form-control" id="notes_rest_due" name="notes_rest_due"
                                           readonly required>
                                </div>
                            </div>
                            <div id="notesPaymentGroup">
                                <div class="clearfix"></div>
                            </div>


                            <div class="clearfix"></div>

                            <div class="col-xs-6">

                                <div class="form-group">

                                    <label>Description*</label>

                                    <textarea class="form-control" name="text" rows="4" required></textarea>

                                </div>

                            </div>

                            <div class="clearfix"></div>

                            <div class="col-xs-6" id="reason4" style="display:none;">

                                <div class="form-group">

                                    <label>Raison*</label>

                                    <textarea class="form-control" name="comment" id="comment4" disabled rows="4"
                                              required></textarea>

                                </div>

                            </div>
                            <div class="clearfix"></div>

                            <div class="col-md-3" style="padding-top: 5px;width:230px;white-space: nowrap">
                                Add bills proof document files*
                            </div>
                            <div class="col-md-6" id="attachDiv4">
                                <div class="row">
                                    <div class="col-xs-6" style="overflow: hidden">
                                    <label for="4"  required><i class="btn">Choose file</i><i class="name"></i></label>
                                        <input id=4 type="file" name="attachment[]" required style="width: 1px;height:1px">
                                    </div>
                                    <div class="col-xs-4">
                                        <button type="button"
                                                class="btn btn-circle btn-success btn-sm addFile4 custombtn"><i
                                                    class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <!-- <div class="col-xs-4"> -->

                            <div class="col-md-12">
                                <div class="text-right">
                                    <a href="<?= base_url("admin/drivers_requests") ?>"
                                       class="btn btn-default"><i class="fa fa-times"></i> Cancel</a>
                                    <button class="btn btn-default"><i class="fa fa-save"></i> Save</button>
                                </div>
                            </div>

                            <!-- </div> -->


                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>


            </div>

        </div>

    </div>

</div>

<!--/.module-->

</div>

<!--/.content-->

</div>

<script type="text/javascript">
    let i = 2;
    $(document).ready(function () {
        $(document).on("change",':file',function() {
        var file = $(this)[0].files[0].name;
        $(this).prev('label').find("i.name").text(file);
        });

        $(".bdatepicker").datepicker({

            format: "dd/mm/yyyy"
//            format: "mm/dd/yyyy"

        });
        $("#request_type").on("change", function () {
            let request = $(this).val();
            console.log("#" + request);
            $(".drivers_request_div").hide();
            $("#" + request).show();
        });

        $("#time_deduce").on("change", function () {
            let amount = $("#amount").val();
            let times = $(this).val();
            var paid_by_driver = 0;
            var driverAmount = $('input[name="paid_by_driver"]');
            driverAmount.each(function () {
                if ($(this).val() != '') {
                    paid_by_driver += parseInt($(this).val());
                }
            });
//            paid_by_driver = $("#paid_by_driver").val();
            let month_number = $("#month_number").val();

            let total_month_count = parseInt(month_number) + parseInt(times);
            // console.log(total_month_count+" check");
            if (amount > 0 && times > 0) {
                let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                let to_month = months[total_month_count - 1];
                $("#to_month").val(to_month);

                let paid_amount = amount / times;
                $("#paid_amount").val(paid_amount.toFixed(2));


                if (paid_by_driver != "" && paid_by_driver > 0) {
                    let due = amount - paid_by_driver;
                    $("#due").val(due.toFixed(2));
                } else {
                    // alert("aa");
                    let due = amount;
                    // console.log(due+"ee");
                    $("#due").val(parseFloat(amount).toFixed(2));
                }


            }
        });

        $("#amount").on("change", function () {
            let amount = $(this).val();
            let times = $("#time_deduce").val();
            var paid_by_driver = 0;
            var driverAmount = $('input[name="paid_by_driver"]');
            driverAmount.each(function () {
                if ($(this).val() != '') {
                    paid_by_driver += parseInt($(this).val());
                }
            });
            let month_number = $("#month_number").val();
            let total_month_count = parseInt(month_number) + parseInt(times);
            if (amount > 0 && times > 0) {
                let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                let to_month = months[total_month_count];
                $("#to_month").val(to_month);

                let paid_amount = amount / times;
                $("#paid_amount").val(paid_amount.toFixed(2));


                if (paid_by_driver != "" && paid_by_driver > 0) {
                    let due = amount - paid_by_driver;
                    $("#due").val(due.toFixed(2));
                } else {
                    // alert("aa");
                    let due = amount;
                    $("#due").val(parseFloat(due).toFixed(2));
                }
            }

        });


        $(document.body).on("change", "input[name='paid_by_driver']", function () {
            let amount = $("#amount").val();
            let times = $("#time_deduce").val();
            var paid_by_driver = 0;
            var driverAmount = $('input[name="paid_by_driver"]');
            driverAmount.each(function () {
                if ($(this).val() != '') {
                    paid_by_driver += parseInt($(this).val());
                }
            });

            if (amount > 0 && times > 0 && paid_by_driver > 0) {

                if (paid_by_driver != "" && paid_by_driver > 0) {
                    let due = amount - paid_by_driver;
                    $("#due").val(due.toFixed(2));
                } else {
                    // alert("aa");
                    let due = amount;
                    $("#due").val(parseFloat(due).toFixed(2));
                }
            }

        });
        $(document.body).on("change", '#paid_by_employeer, #amount', function () {
            let amount = $('#amount').val();
            let paid_by_employeer = $('#paid_by_employeer').val();
            $('#due_employeer').val(amount - paid_by_employeer)
        });

        $("#notes_amount").on("change", function () {
            let amount = $(this).val();
            var notes_paid_amount = 0;
            var notesAmount = $('input[name="notes_paid_amount"]');
            notesAmount.each(function () {
                if ($(this).val() != '') {
                    notes_paid_amount += parseInt($(this).val());
                }
            });
            let due = amount - notes_paid_amount;
            $("#notes_rest_due").val(due.toFixed(2));
        });

        $(document.body).on("change", "input[name='notes_paid_amount']", function () {
//            let notes_paid_amount = $(this).val();
//            let notes_amount = $("#notes_amount").val();
//            let due = notes_amount - notes_paid_amount;
//            $("#notes_rest_due").val(due.toFixed(2));
            $("#notes_amount").trigger('change');

        });

        $(document.body).on("click", ".addFile2", function () {
            id=Math.ceil(Math.random(0,1)*1000);
            input =  '<label for="'+id+'"><i class="btn">Choose file</i><i class="name"></i></label><input id="'+id+'" type="file" name="attachment[]" style="display:none">';
            $("#attachDiv2").append('<div class="attach-main"> <div class="attach-file"> ' + input + ' </div> <div class="attach-buttons"> <button type="button" class="btn btn-circle btn-success btn-sm addFile2"><i class="fa fa-plus"></i></button> <button type="button" class="btn btn-circle btn-danger btn-sm delFile2"><i class="fa fa-minus"></i></button></div></div>');
        });

        $(document.body).on("click", ".delFile2", function () {
            $(this).closest('.attach-main').remove();
        });

        $(document.body).on("click", ".addFile3", function () {
            id=Math.ceil(Math.random(0,1)*1000);
            input =  '<label for="'+id+'"><i class="btn">Choose file</i><i class="name"></i></label><input id="'+id+'" type="file" name="attachment[]" style="display:none">';
            $("#attachDiv3").append('<div class="attach-main"> <div class="attach-file"> '+ input +' </div> <div class="attach-buttons"> <button type="button" class="btn btn-circle btn-success btn-sm addFile3"><i class="fa fa-plus"></i></button> <button type="button" class="btn btn-circle btn-danger btn-sm delFile3"><i class="fa fa-minus"></i></button></div></div>');
        });
        $(document.body).on("click", "#restToPay .add-field", function (e) {
            e.preventDefault();
            $("#driverPaymentGroup").append('<div class="driver-payment-group">\n' +
                '                                        <div class="col-xs-2">\n' +
                '\n' +
                '                                            <div class="form-group">\n' +
                '\n' +
                '                                                <label>Paid Amount By Driver</label>\n' +
                '\n' +
                '                                                <input type="number" class="form-control" name="paid_by_driver" id="paid_by_driver" >\n' +
                '\n' +
                '                                            </div>\n' +
                '\n' +
                '                                        </div>\n' +
                '                                        <div class="col-xs-2">\n' +
                '\n' +
                '                                            <div class="form-group">\n' +
                '\n' +
                '                                                <label>Date</label>\n' +
                '\n' +
                '                                                <input type="text" class="form-control bdatepicker" name="date" autocomplete="off">\n' +
                '\n' +
                '                                            </div>\n' +
                '\n' +
                '                                        </div>\n' +
                '                                        <div class="col-xs-2">\n' +
                '\n' +
                '                                            <div class="form-group">\n' +
                '\n' +
                '                                                <label>Payment Method</label>\n' +
                '\n' +
                '                                                <select class="form-control" name="payment_method">\n' +
                '                                                    <option value="">--- Select payment method ---</option>\n' +
                '                                                    <option value="cash">Cash</option>\n' +
                '                                                    <option value="cheque">Cheque</option>\n' +
                '                                                    <option value="credit_card">Credit Card</option>\n' +
                '                                                    <option value="Bank Wire">Bank Wire</option>\n' +
                '                                                    <option value="Bank Debit">Bank Debit</option>\n' +
                '                                                </select>\n' +
                '\n' +
                '                                            </div>\n' +
                '                                        </div>\n' +
                '                                        <div class="driver-buttons">\n' +
                '                                            <i class="fa fa-minus remove-group"></i>\n' +
                '                                            <i class="fa fa-edit"></i>\n' +
                '                                        </div>\n' +
                '                                        <div class="clearfix"></div>\n' +
                '                                    </div>');
        });

        $(document.body).on("click", "#restNotes .add-field", function (e) {
            e.preventDefault();
            $("#notesPaymentGroup").append('   <div class="notes-payment-group">\n' +
                '                                    <div class="col-xs-2">\n' +
                '\n' +
                '                                        <div class="form-group">\n' +
                '\n' +
                '                                            <label>Paid Amount</label>\n' +
                '\n' +
                '                                            <input type="number" class="form-control" id="notes_paid_amount"\n' +
                '                                                   name="notes_paid_amount">\n' +
                '\n' +
                '                                        </div>\n' +
                '\n' +
                '                                    </div>\n' +
                '                                    <div class="col-xs-2">\n' +
                '\n' +
                '                                        <div class="form-group">\n' +
                '\n' +
                '                                            <label>Date of payment</label>\n' +
                '\n' +
                '                                            <input type="text" class="form-control bdatepicker" autocomplete="off"\n' +
                '                                                   name="payment_date">\n' +
                '\n' +
                '                                        </div>\n' +
                '\n' +
                '                                    </div>\n' +
                '                                    <div class="col-xs-2">\n' +
                '\n' +
                '                                        <div class="form-group">\n' +
                '\n' +
                '                                            <label>Payment Method</label>\n' +
                '\n' +
                '                                            <select class="form-control" name="payment_method" >\n' +
                '                                                <option value="">--- Select payment method ---</option>\n' +
                '                                                <option value="cash">Cash</option>\n' +
                '                                                <option value="cheque">Cheque</option>\n' +
                '                                                <option value="credit_card">Credit Card</option>\n' +
                '                                                <option value="Bank Wire">Bank Wire</option>\n' +
                '                                            </select>\n' +
                '\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '                                    <div class="notes-buttons">\n' +
                '                                        <i class="fa fa-minus remove-group"></i>\n' +
                '                                        <i class="fa fa-edit"></i>\n' +
                '                                    </div>\n' +
                '                                    <div class="clearfix"></div>\n' +
                '                                </div>');
        });

        $(document.body).on("click", ".driver-buttons .remove-group", function () {
            $(this).parents('.driver-payment-group').remove();
            $("input[name='paid_by_driver']").trigger('change');
        });

        $(document.body).on("click", ".notes-buttons .remove-group", function () {
            $(this).parents('.notes-payment-group').remove();
            $("#notes_amount").trigger('change');
        });

        $(document.body).on("click", ".delFile3", function () {
            $(this).closest('.attach-main').remove();
        });

        $(document.body).on("click", ".addFile4", function () {
            id=Math.ceil(Math.random(0,1)*1000);
        input =  '<label for="'+id+'"><i class="btn">Choose file</i><i class="name"></i></label><input id="'+id+'" type="file" name="attachment[]" style="display:none">';
            $("#attachDiv4").append('<div class="attach-main"> <div class="attach-file"> ' + input + ' </div> <div class="attach-buttons"> <button type="button" class="btn btn-circle btn-success btn-sm addFile4"><i class="fa fa-plus"></i></button> <button type="button" class="btn btn-circle btn-danger btn-sm delFile4"><i class="fa fa-minus"></i></button></div></div>');
        });

        $(document.body).on("click", ".delFile4", function () {
            $(this).closest('.attach-main').remove();
        });

        $("#status1").on("change", function () {
            let status = $(this).val();
            if (status == 4) {
                $("#reason1").show();
                $("#comment1").attr("disabled", false);
            } else {
                $("#reason1").hide();
                $("#comment1").attr("disabled", true);
            }
        });

        $("#status2").on("change", function () {
            let status = $(this).val();
            if (status == 4) {
                $("#reason2").show();
                $("#comment2").attr("disabled", false);
            } else {
                $("#reason2").hide();
                $("#comment2").attr("disabled", true);
            }
        });

        $("#status3").on("change", function () {
            let status = $(this).val();
            if (status == 4) {
                $("#reason3").show();
                $("#comment3").attr("disabled", false);
            } else {
                $("#reason3").hide();
                $("#comment3").attr("disabled", true);
            }
        });

        $("#status4").on("change", function () {
            let status = $(this).val();
            if (status == 4) {
                $("#reason4").show();
                $("#comment4").attr("disabled", false);
            } else {
                $("#reason4").hide();
                $("#comment4").attr("disabled", true);
            }
        });

        $('#time').timepicker({'timeFormat': 'H:i'});

    });

</script>