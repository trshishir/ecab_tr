<?php $locale_info = localeconv(); ?>
<div class="col-md-12"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12">
                <?php $this->load->view('admin/common/alert');?>
                <div class="module">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="module-head">
                    </div>
                    <div class="module-body">
                        <?=form_open("pagetype/add")?>
                        <div class="row" style="padding:20px;">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Status*</label>
                                    <select class="form-control" name="status" required>
                                        <?php //endforeach;?>
                                        <option value="1">Enabled</option>
                                        <option value="2">Disabled</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Name*</label>
                                    <input type="text" maxlength="100" class="form-control" required name="name" placeholder="Name*" value="<?=set_value('name',$this->input->post('name'))?>">
                                </div>
                            </div>
							<div class="col-xs-4">
                                <div class="form-group">
                                    <label>Title*</label>
                                    <input type="text" maxlength="100" class="form-control" required name="title" placeholder="Title*" value="<?=set_value('title',$this->input->post('title'))?>">
                                </div>
                            </div>
                        </div>
					
                        <!--<div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>Description*</label>
                                        <input id="subject" maxlength="200" type="text" class="form-control" required name="description" placeholder="Description" value="<?=set_value('description',$this->input->post('description'))?>">
                                  
                                </div>
                            </div>
                        </div>   --->
                        <div class="text-right">
                            <button class="btn btn-default">Save</button>
                            <a href="<?=base_url("pagetype/index")?>" class="btn btn-default">Cancel</a>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $(".bdatepicker").datepicker({
            format: "dd/mm/yyyy"
        });
    });
</script>