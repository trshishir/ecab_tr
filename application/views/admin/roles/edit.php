<?php $locale_info = localeconv(); ?>
<style type="text/css">
    body {
     /* background: #efefef !important; */
     background: linear-gradient(to bottom,#fbfbfb 0%,#ececec 39%,#ececec 39%,#c1c1c1 100%)!important;
        min-height: 100vh !important;
    }
    input[type="checkbox"] {
        display: block !important;
        margin: 0 auto !important;
        float: none !important;
    }
    .text-label{
        margin-bottom: 16px !important;
    }
    label{
        font-weight: 600;
    }
    .breadcrumb a:nth-child(2){
        color: #337ab7;
    }
    .container {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: 14px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        }

        /* Hide the browser's default checkbox */
        .container input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
        }

        /* Create a custom checkbox */
        .checkmark {
        background: linear-gradient(white, #f3f0f0) !important;
        position: absolute;
        top: 33px;
        left: 50%;
        height: 15px;
        width: 15px;
        border-radius:4px;
        border:1px solid #a9a9a9;
        margin-top: 8px;
        }

        /* On mouse-over, add a grey background color */
        .container:hover input ~ .checkmark {
        background: linear-gradient(white, #f3f0f0) !important;
        }

        /* When the checkbox is checked, add a blue background */
        .container input:checked ~ .checkmark {
        background: linear-gradient(#88caff, #2196f3) !important;
        border:none;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
        content: "";
        position: absolute;
        display: none;
        }

        /* Show the checkmark when checked */
        .container input:checked ~ .checkmark:after {
        display: block;
        }

        /* Style the checkmark/indicator */
        .container .checkmark:after {
        left: 5px;
        top: 1px;
        width: 5px;
        height: 10px;
        border: solid white;
        border-width: 0 2px 2px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
        }

</style>
<div class="col-md-12" style="min-height: 265px;background: transparent;"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12">
                <?php $this->load->view('admin/common/alert');?>
                <div class="module">
                    <?php
                        echo $this->session->flashdata('message');
                        $validation_erros = $this->session->flashdata('validation_errors');
                        if(!empty($validation_erros)){
                    ?>
                        <div class="form-validation-errors alert alert-danger">
                            <h3 style="font-size: 20px; text-align: center;">Validation Errors</h3>
                            <?php echo $validation_erros; ?>
                        </div>
                    <?php } ?>
                    <div class="module-head">
                    </div>
                    <div class="module-body">
                        <?=form_open("admin/roles/".$row_data['id']."/update")?>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Status*</label>
                                            <select class="form-control" name="status">
                                                <option value="active" <?php if($row_data['status'] == 'active'){ echo 'selected="selected"'; } ?>>Active</option>
                                                <option value="disable" <?php if($row_data['status'] == 'disable'){ echo 'selected="selected"'; } ?>>Disable</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Role Name*</label>
                                            <input type="text" class="form-control" required name="name" value="<?=$row_data['name']?>" placeholder="Department Name">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group text-center">
                                            <!-- <label class="text-label">View</label>
                                            <input type="checkbox" value="yes" name="role_view" <?php if($row_data['role_view'] == 'yes'){ echo 'checked="checked"'; } ?>> -->
                                            <label class="container">
                                                <input type="checkbox" value="yes" name="role_view" <?php if($row_data['role_view'] == 'yes'){ echo 'checked="checked"'; } ?>>
                                                <span class="checkmark"></span>
                                                View
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group text-center">
                                            <!-- <label class="text-label">Add</label>
                                            <input type="checkbox" value="yes" name="role_add" <?php if($row_data['role_add'] == 'yes'){ echo 'checked="checked"'; } ?>> -->
                                            <label class="container">
                                                <input type="checkbox" value="yes" name="role_add" <?php if($row_data['role_add'] == 'yes'){ echo 'checked="checked"'; } ?>>
                                                <span class="checkmark"></span>
                                                Add
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group text-center">
                                            <!-- <label class="text-label">Edit</label>
                                            <input type="checkbox" value="yes" name="role_edit" <?php if($row_data['role_edit'] == 'yes'){ echo 'checked="checked"'; } ?>> -->
                                            <label class="container">
                                                <input type="checkbox" value="yes" name="role_edit" <?php if($row_data['role_edit'] == 'yes'){ echo 'checked="checked"'; } ?>>
                                                <span class="checkmark"></span>
                                                Edit
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group text-center">
                                            <!-- <label class="text-label">Delete</label>
                                            <input type="checkbox" value="yes" name="role_delete" <?php if($row_data['role_delete'] == 'yes'){ echo 'checked="checked"'; } ?>> -->
                                            <label class="container">
                                                <input type="checkbox" value="yes" name="role_delete" <?php if($row_data['role_delete'] == 'yes'){ echo 'checked="checked"'; } ?>>
                                                <span class="checkmark"></span>
                                                Delete
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row">
                                    
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-right">
                                    <a href="<?=base_url("admin/roles")?>" class="btn btn-default"><i class="fa fa-times"></i> Cancel</a>
                                    <button class="btn btn-default"><i class="fa fa-floppy-o"></i> Save</button>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div><?php $locale_info = localeconv(); ?>
<style>
    @media only screen and (min-width: 1400px){
        .table-filter input, .table-filter select{
            max-width: 9% !important;
        }
        .table-filter select{
            max-width: 95px !important;
        }
        .table-filter .dpo {
            max-width: 90px !important;
        }
    }
</style>