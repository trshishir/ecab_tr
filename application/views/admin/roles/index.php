<?php $locale_info = localeconv(); ?>
<style>
    body {
        background: #efefef !important;
    }
    .table-filter{
        margin-bottom: -30px !important;   
    }
    .table-filter .dpo {
        max-width: 62px;
    }
    .table-filter span {
        margin: 4px 2px 0 3px;
    }
    .table-filter input[type="number"]{
        max-width: 48px;
    }
    .table-filter input[type="text"]{
        max-width: 125px;
    }
    @media only screen and (min-width: 1400px){
        .table-filter input, .table-filter select{
            max-width: 15% !important;
        }
        .table-filter select{
            max-width: 85px !important;
        }
        .table-filter .dpo {
            max-width: 90px !important;
        }
    }
</style>
<!-- background: -webkit-linear-gradient(#efefef, #ECECEC, #CECECE); -->
<div class="col-md-12" style="background: #efefef;"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12">
                <?php
                $flashAlert =  $this->session->flashdata('alert');
                if(isset($flashAlert['message']) && !empty($flashAlert['message'])){?>
                    <br>
                    <div style="padding: 5px 12px" class="alert <?=$flashAlert['class']?>">
                        <strong><?=$flashAlert['type']?></strong> <?=$flashAlert['message']?>
                        <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>
                <div class="module">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="module-head">
                        <!--<h3> <?php if(isset($title)) echo $title;?></h3>-->
                    </div>
                    <div class="module-body table-responsive">
                        <table id="example" class="cell-border text-center" cellspacing="0" width="100%" data-selected_id="">
                            <thead>
                            <tr>
                                <th class="no-sort text-center"><input type='checkbox' name='allRoles' id="allRoles" value='' onclick="Javascript:selectAllRoles();" /></th>
                                <th class="text-center"><?php echo $this->lang->line('id');?></th>
								<th class="text-center" class="column-date"><?php echo $this->lang->line('date');?></th>
                                <th class="text-center" class="column-date"><?php echo $this->lang->line('time');?></th>
                                <th class="text-center" class="column-name" ><?php echo $this->lang->line('added_by');?></th>
                                <th class="text-center">Role Name</th>
                                <th class="text-center">View</th>
                                <th class="text-center">Add</th>
                                <th class="text-center">Edit</th>
                                <th class="text-center">Delete</th>
								<th class="text-center">Statut</th>
								<th class="text-center">Since</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach($roles as $item):?>
                                    <tr>
                                        <td>
                                            <input type="checkbox" data-id="<?=$item['id'];?>" class="checkbox singleSelect">
                                        </td>
                                        <td>
                                            <a href="<?=site_url("admin/roles/".$item['id']."/edit")?>"><?=create_timestamp_uid($item['created_at'],$item['id']);?></a>
                                        </td>
										<td><?=from_unix_date($item['created_at'], "d/m/Y")?></td>
                                        <td><?=from_unix_time($item['created_at'])?></td>
                                        <td>
											<?php
												echo 'Mr Super Admin';
											?>
										</td>
                                        <td><?=$item['name'];?></td>
                                        <td><?php if($item['role_view'] == 'yes'){ echo 'Yes'; }else{ echo 'No'; } ?></td>
                                        <td><?php if($item['role_add'] == 'yes'){ echo 'Yes'; }else{ echo 'No'; } ?></td>
                                        <td><?php if($item['role_edit'] == 'yes'){ echo 'Yes'; }else{ echo 'No'; } ?></td>
                                        <td><?php if($item['role_delete'] == 'yes'){ echo 'Yes'; }else{ echo 'No'; } ?></td>
										<td><?php if($item['status'] == 'active'){echo '<span class="label label-success">Active</span>';}else{echo '<span class="label label-danger">Disabled</span>';} ?></td>
										<td style="white-space: nowrap"><?=timeDiff($item['created_at']);?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <!-- <br> -->
                    </div>
                </div>
            </div>
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<!-- <div id="table-filter" class="hide">
    <select class="form-control">
        <option>All Jobs</option>
    </select>
    <span class="pull-left">Age:</span>
    <input type="number" placeholder="From" class="form-control">
    <input type="number" placeholder="To" class="form-control">
    <select class="form-control">
        <option>Civility</option>
    </select>
    <select class="form-control dep-filter">
        <option>Department</option>
    </select>
    <input type="text" placeholder="From" class="dpo">
    <input type="text" placeholder="To" class="dpo">
    <select class="form-control">
        <option>All Status</option>
    </select>
</div> -->
<div id="table-filter" class="hide">
    <input type="text" id="txtSearch" value="<?=$search_text?>" placeholder="Search" class="form-control">
    <a href="#" id="search" class="btn btn-sm btn-default">Search</a>
</div>
<script>
    $(document).ready(function(){
        $(document).on("click", "#search", function() {
            if($("#txtSearch").val() != ""){
				var base_url = "<?php echo base_url();?>";
                $.ajax({
                    url: base_url+"admin/roles/set_search_session",
                    method: 'GET',
                    contentType: "application/json; charset:utf-8",
                    data: {'text': $("#txtSearch").val()},
                    dataType: 'json',
                    success: function (response) {
                        location.reload();
                    }
                });
            }else{
                alert("Please enter search text!");
				location.reload();
            }
        });
    });
</script>