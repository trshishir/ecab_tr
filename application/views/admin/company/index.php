<?php $locale_info = localeconv(); ?>
<style type="text/css">
    label{font-weight: 600; font-size: 13px;}
    .custom_style_row{margin-top: 30px; margin-bottom: 30px;}
</style>
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: #018001;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #ccc ;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
        background: #09f509;
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
    .radio, .checkbox {
        padding-top: 0px;
        padding-bottom: 0px;
    }
    .radio, .checkbox {
        margin-top: 0px;
    }
    .nestedCols .col-xs-1 {
        width: 9%
    }
    .nestedCols .col-xs-1.unit {
        width: 6%
    }
    .nestedCols .col-xs-1.switches {
        padding-top: 30px;
        width: 3.5%;
        padding-left: 10px;
    }


</style>

<section id="content">

    <div class="col-md-12"><!--col-md-10 padding white right-p-->
        <div class="content">
            <?php $this->load->view('admin/common/breadcrumbs');?>
            <div class="row">
                <div class="col-md-12">
                    <?php $this->load->view('admin/common/alert');?>
                    <div class="module">
                        <?php
                        echo $this->session->flashdata('message');
                        $validation_erros = $this->session->flashdata('validation_errors');
                        if(!empty($validation_erros)){
                            ?>
                            <div class="form-validation-errors alert alert-danger">
                                <h3 style="font-size: 20px; text-align: center;">Validation Errors</h3>
                                <?php echo $validation_erros; ?>
                            </div>
                        <?php } ?>
                        <div class="module-head">
                        </div>
                        <div class="module-body">
                            <?=form_open_multipart("admin/company/save")?>
                            <div class="row">
                                <div class="col-xs-8">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>Type*</label>
                                                <select class="form-control" name="type" required>
                                                    <option disabled="" selected="">-- Select Type --</option>
                                                    <option value="SAS" <?php if($company['type'] == 'SAS'){ echo 'selected=""'; } ?>>SAS</option>
                                                    <option value="SARL" <?php if($company['type'] == 'SARL'){ echo 'selected=""'; } ?>>SARL</option>
                                                    <option value="SA" <?php if($company['type'] == 'SA'){ echo 'selected=""'; } ?>>SA</option>
                                                    <option value="LLC" <?php if($company['type'] == 'LLC'){ echo 'selected=""'; } ?>>LLC</option>
                                                    <option value="LIMITED" <?php if($company['type'] == 'LIMITED'){ echo 'selected=""'; } ?>>LIMITED</option>
                                                    <option value="INCORPORATION" <?php if($company['type'] == 'INCORPORATION'){ echo 'selected=""'; } ?>>INCORPORATION</option>
                                                    <option value="GMBH" <?php if($company['type'] == 'GMBH'){ echo 'selected=""'; } ?>>GMBH</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>Company Name*</label>
                                                <input type="text" class="form-control" required name="name" placeholder="Name*" value="<?= $company['name']; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-xs-8">

                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>Email*</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                    <input maxlength="100" type="email" class="form-control" required name="email" placeholder="Companu Email" value="<?= $company['email']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>Telephone*</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                    <input maxlength="50" type="text" class="form-control" required name="phone" placeholder="Telephone" value="<?= $company['phone']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>Fax</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                    <input maxlength="50" type="text" class="form-control" name="fax" placeholder="Fax" value="<?= $company['fax']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>Domain Name</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-link"></i></span>
                                                    <input type="text" class="form-control" name="website" placeholder="Website URL" value="<?= $company['website']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-xs-2 text-center " style="vertical-align: center;  margin-top:-65px; padding-bottom:5px">
                                    <!-- company logo will be here -->
                                    <img class="admin_border" id="company_logo_preview"
                                        <?php if(isset($company['logo']) && !empty($company['logo'])){ ?>
                                            src="<?= base_url('uploads/company/').'/'.$company['logo']; ?>"
                                        <?php } ?>
                                         style="width: 210px; margin: 4% 4% 0% 4%; height: 142px; padding: 5px ;border: 1px solid #ccc !important;">
                                    <label style="margin:0px;">Company Logo</label>
                                    <input type="file" name="logo" id="company_logo">
                                    <input type="hidden" name="old_logo" value="<?= $company['logo']; ?>">
                                </div>

                                <?php
                                // var_dump($company);
                                ?>
                                <div class="col-xs-2 text-center " style="vertical-align: center;  margin-top:-65px; padding-bottom:5px">
                                    <!-- company logo will be here -->
                                    <img class="admin_border" id="footer_logo_preview"
                                        <?php if(isset($company['footer_logo']) && !empty($company['footer_logo'])){ ?>
                                            src="<?= base_url('uploads/company/').'/'.$company['footer_logo']; ?>"
                                        <?php } ?>
                                         style="width: 210px; margin: 4% 4% 0% 4%;; height: 142px; padding: 5px;border: 1px solid #ccc !important;">
                                    <label style="margin:0px;">Company Footer Logo </label>
                                    <input type="file" name="footer_logo" id="footer_logo">
                                    <input type="hidden" name="footer_logo" value="<?= $company['footer_logo']; ?>">
                                </div>

                                <div class="col-xs-4">


                                    <!--                                 <div class="form-group">
                                    <label>Company Logo</label>
                                    <input type="file" name="logo" id="company_logo">
                                    <input type="hidden" name="old_logo" value="<?= $company['logo']; ?>">
                                  </div> -->
                                </div>
                            </div>
                            <div class="row custom_style_row">
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <!-- <label>Number + Street</label> -->
                                        <label>Adress</label>
                                        <input type="text" class="form-control"  name="address" placeholder="Address" value="<?= $company['address']; ?>">
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label>Adress 2</label>
                                        <input type="text" class="form-control"  name="address2" placeholder="Address second" value="<?= $company['address2']; ?>">
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <div class="form-group">
                                        <label>Zip Code</label>
                                        <input type="text" class="form-control"  name="zip_code" placeholder="Zip Code" value="<?= $company['zip_code']; ?>">
                                    </div>
                                </div>

                                <div class="col-xs-2">
                                    <div class="form-group">
                                        <label>Country</label>
                                        <select required="required"  class="form-control" name="country" id="country_id" tabindex="1">
                                            <option value="">Select Country</option>

                                            <?php foreach($countries as $data):?>
                                                <option <?php echo ($company['country']==$data->id)?'selected':'';?>  value="<?=$data->id;?>"><?=$data->name;?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row custom_style_row">


                                <div class="col-xs-2">
                                    <div class="form-group">
                                        <label>Region</label>

                                        <select required="required"  class="form-control" name="region" id="region_id" tabindex="1">

                                        </select>
                                        <!-- <input type="text" class="form-control"  name="region" placeholder="City" value="<?= $company['region']; ?>"> -->
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <div class="form-group">
                                        <label>City</label>

                                        <select required="required" class="form-control" name="city" id="cities_id" tabindex="1">

                                        </select>
                                    </div>
                                </div>

                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label>SIRET</label>
                                        <input type="text" class="form-control"  name="sirft" placeholder="SIRFT" value="<?= $company['sirft']; ?>">
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label>RCS</label>
                                        <input type="text" class="form-control"  name="rcs" placeholder="RCS" value="<?= $company['rcs']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row custom_style_row">
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label>Licence Number</label>
                                        <input type="text" class="form-control"  name="licence" placeholder="Licence" value="<?= $company['licence']; ?>">
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label>NUMERO TVA</label>
                                        <input type="text" class="form-control"  name="numero_tva" placeholder="NUMERO TVA" value="<?= $company['numero_tva']; ?>">
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label>Capital</label>
                                        <input type="text" class="form-control"  name="capital" placeholder="Capital" value="<?= $company['capital']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row custom_style_row">
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label>Facebook</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                                            <input type="text" class="form-control"  name="facebook_link" placeholder="Facebook URL" value="<?= $company['facebook_link']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label>Twitter</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                                            <input type="text" class="form-control"  name="twitter_link" placeholder="Twitter URL" value="<?= $company['twitter_link']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label>LinkedIn</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
                                            <input type="text" class="form-control"  name="linkedin_link" placeholder="LinkedIn URL" value="<?= $company['linkedin_link']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label>Youtube</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-youtube"></i></span>
                                            <input type="text" class="form-control"  name="youtube_link" placeholder="Youtube URL" value="<?= $company['youtube_link']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label>Instagram</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-instagram"></i></span>
                                            <input type="text" class="form-control"  name="instagram_link" placeholder="Instagram URL" value="<?= $company['instagram_link']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="form-group">
                                        <label>Radio link</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-youtube-play"></i></span>
                                            <input type="text" class="form-control"  name="radio_link" placeholder="Radio Link" value="<?= $company['radio_link']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <div class="form-group" style="margin-left: 70px;">
                                        <label>Radio Auto play</label>
                                        <div class="checkbox">
                                            <label class="switch">
                                                <input id="radio_autoplay" type="checkbox" name="radio_autoplay" value="<?php echo ($company['radio_autoplay'] ==1)?1:0;?>"  <?php echo ($company['radio_autoplay'] ==1)?'checked':'';?> >
                                                <span class="slider round"></span>
                                            </label>

                                        </div>
                                    </div>

                                    <script type="text/javascript">
                                        $(function() {
                                            $('#radio_autoplay').on('change.bootstrapSwitch', function(e) {
                                                // console.log(e.target.checked);
                                                $('#radio_autoplay').val((e.target.checked)?1:0);
                                            });
                                        })

                                    </script>
                                </div>


                                <div class="col-xs-2 " >
                                    <!-- company logo will be here -->
                                    <label>Front Player </label><br>
                                    <!-- <input type="file" name="audio_file" id="audio_file">
                                  <input type="hidden" name="audio_file" value="<?= $company['audio_file']; ?>"> -->
                                    <?php if($company['audio_file']): ?>

                                        <label for="audio_file" class="btn custom_gray_bg"> Upload File... </label>
                                        <a ><span><?=$company['audio_file'];?></span></a>

                                        <input id="audio_file" name="audio_file" style="visibility:hidden;" type="file">
                                    <?php else:?>

                                        <label for="audio_file" class="btn custom_gray_bg"> Upload Audio... </label>

                                        <input id="audio_file" name="audio_file" style="visibility:hidden;" type="file">

                                    <?php endif;?>
                                    <input type="hidden" name="audio_file" value="<?= $company['audio_file']; ?>">


                                    <script type="text/javascript">
                                        $("#audio_file").change(function() {
                                            filename = this.files[0].name
                                            // console.log(filename);
                                        });
                                    </script>
                                </div>




                                <div class="col-xs-2">
                                    <div class="form-group" style="margin-left: 70px;">
                                        <label>Audio Auto play</label>
                                        <div class="checkbox">
                                            <label class="switch">
                                                <input id="front_autoplay" type="checkbox" name="front_autoplay" value="<?php echo ($company['front_autoplay'] ==1)?1:0;?>"  <?php echo ($company['front_autoplay'] ==1)?'checked':'';?> >
                                                <span class="slider round"></span>
                                            </label>

                                        </div>
                                    </div>

                                    <script type="text/javascript">
                                        $(function() {
                                            $('#front_autoplay').on('change.bootstrapSwitch', function(e) {
                                                // console.log(e.target.checked);
                                                $('#front_autoplay').val((e.target.checked)?1:0);
                                            });
                                        })

                                    </script>
                                </div>

                                <div class="col-xs-2">
                                    <div class="form-group" style="margin-left: 70px;">
                                        <label>Audio Repeat play</label>
                                        <div class="checkbox">
                                            <label class="switch">
                                                <input id="front_autoplay_repeat" type="checkbox" name="front_autoplay_repeat" value="<?php echo ($company['front_autoplay_repeat'] ==1)?1:0;?>"  <?php echo ($company['front_autoplay_repeat'] ==1)?'checked':'';?> >
                                                <span class="slider round"></span>
                                            </label>

                                        </div>
                                    </div>

                                    <script type="text/javascript">



                                        $(function() {




                                            $('#front_autoplay_repeat').on('change.bootstrapSwitch', function(e) {
                                                // console.log(e.target.checked);
                                                $('#front_autoplay_repeat').val((e.target.checked)?1:0);
                                            });



                                        })

                                    </script>
                                </div>
                                <div class="col-xs-2 " >
                                    <!-- company logo will be here -->
                                    <label>Website BG</label><br>

                                    <input type="hidden" name="website_bg" value="<?= $company['website_bg']; ?>">

                                    <?php if($company['website_bg']): ?>

                                        <label for="website_bg" class="btn custom_gray_bg"> Upload File... </label>
                                        <a><span><?=$company['website_bg'];?></span></a>

                                        <input id="website_bg" name="website_bg" style="visibility:hidden;" type="file">
                                    <?php else:?>

                                        <label for="website_bg" class="btn custom_gray_bg"> Upload BG... </label>

                                        <input id="website_bg" name="website_bg" style="visibility:hidden;" type="file">

                                    <?php endif;?>
                                    <input type="hidden" name="website_bg" value="<?= $company['website_bg']; ?>">


                                    <script type="text/javascript">
                                        $("#website_bg").change(function() {
                                            filename = this.files[0].name
                                            // console.log(filename);
                                        });
                                    </script>
                                </div>

                                <!--                         <div class="col-xs-2">
                            <div class="form-group">
                                <label>Weather wedget location</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-cloud"></i></span>
                                    <input type="text" class="form-control"  name="weather_widget_location" placeholder="Weather widget location" value="<?= $company['weather_widget_location']; ?>">
                                </div>
                            </div>
                          </div> -->
                                <!--<div class="col-xs-2">
                            <div class="form-group">
                              <label>Default Admin language</label>               


                              <select class="form-control" name="default_admin_language" required>
                                <option disabled="" selected="">-- Select Default Admin language --</option>
                                <option value="en" <?php /*if($company['default_admin_language'] == 'en'){ echo 'selected=""'; } */?>>English</option>
                                <option value="fr" <?php /*if($company['default_admin_language'] == 'fr'){ echo 'selected=""'; } */?>>French</option>
                              </select>

                            </div>
                          </div>-->
                                <?php
                                $languages = $this->db->where('status', 1)->order_by('title', 'ASC')->get('vbs_languages')->result();
                                ?>

                            </div>
                            <div class="row nestedCols" style="display: none;">

                                <div class="col-xs-1">
                                    <div class="form-group">
                                        <label>Default Front language</label>
                                        <select class="form-control" name="default_front_language" required>
                                            <option disabled="" >-- Select Default Front language --</option>
                                            <?php
                                            foreach ($languages as $lang) :
                                            ?>

                                            <option value="<?=$lang->short_code?>" <?php if($company['default_front_language'] == $lang->short_code){ echo 'selected=""'; } ?>><?= ucwords($lang->title); ?></option>

                                            <?php
                                            endforeach;
                                            ?>

                                        </select>

                                    </div>
                                </div>
                                <div class="col-xs-1 switches">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label class="switch">
                                                <input id="default_front_language_status" type="checkbox" name="default_front_language_status" value="<?php echo ($company['default_front_language_status'] ==1)?1:0;?>"  <?php echo ($company['default_front_language_status'] ==1)?'checked':'';?> >
                                                <span class="slider round"></span>
                                            </label>

                                        </div>
                                    </div>

                                    <script type="text/javascript">
                                        $(function() {
                                            $('#default_front_language_status').on('change.bootstrapSwitch', function(e) {
                                                // console.log(e.target.checked);
                                                $('#default_front_language_status').val((e.target.checked)?1:0);
                                            });
                                        });
                                    </script>
                                </div>

                                <div class="col-xs-1">
                                    <div class="form-group">
                                        <label>Default Front currency</label>
                                        <select class="form-control" name="default_front_currency" required>
                                            <option disabled="" >-- Select Default currency --</option>
                                            <option value="usd" <?php if($company['default_front_currency'] == 'usd'){ echo 'selected=""'; } ?>>$ USD</option>
                                            <option value="euro" <?php if($company['default_front_currency'] == 'euro'){ echo 'selected=""'; } ?>>€ EURO</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-1 switches">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label class="switch">
                                                <input id="default_front_currency_status" type="checkbox" name="default_front_currency_status" value="<?php echo ($company['default_front_currency_status'] ==1)?1:0;?>"  <?php echo ($company['default_front_currency_status'] ==1)?'checked':'';?> >
                                                <span class="slider round"></span>
                                            </label>

                                        </div>
                                    </div>

                                    <script type="text/javascript">
                                        $(function() {
                                            $('#default_front_currency_status').on('change.bootstrapSwitch', function(e) {
                                                // console.log(e.target.checked);
                                                $('#default_front_currency_status').val((e.target.checked)?1:0);
                                            });
                                        });
                                    </script>
                                </div>
                                <div class="col-xs-1">
                                    <div class="form-group">
                                        <label>Default Admin language</label>
                                        <select class="form-control" name="default_admin_language" required>
                                            <option disabled="" >-- Select Default Front language --</option>
                                            <?php
                                            foreach ($languages as $lang) :
                                                ?>

                                                <option value="<?=$lang->short_code?>" <?php if($company['default_admin_language'] == $lang->short_code){ echo 'selected=""'; } ?>><?= ucwords($lang->title); ?></option>

                                            <?php
                                            endforeach;
                                            ?>
                                             </select>

                                    </div>
                                </div>
                                <div class="col-xs-1 switches">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label class="switch">
                                                <input id="default_admin_language_status" type="checkbox" name="default_admin_language_status" value="<?php echo ($company['default_admin_language_status'] ==1)?1:0;?>"  <?php echo ($company['default_admin_language_status'] ==1)?'checked':'';?> >
                                                <span class="slider round"></span>
                                            </label>

                                        </div>
                                    </div>

                                    <script type="text/javascript">
                                        $(function() {
                                            $('#default_admin_language_status').on('change.bootstrapSwitch', function(e) {
                                                // console.log(e.target.checked);
                                                $('#default_admin_language_status').val((e.target.checked)?1:0);
                                            });
                                        });
                                    </script>
                                </div>
                                <div class="col-xs-1">
                                    <div class="form-group">
                                        <label>Default Admin currency</label>
                                        <select class="form-control" name="default_admin_currency" required>
                                            <option disabled="" >-- Select Default currency --</option>
                                            <option value="usd" <?php if($company['default_admin_currency'] == 'usd'){ echo 'selected=""'; } ?>>$ USD</option>
                                            <option value="euro" <?php if($company['default_admin_currency'] == 'euro'){ echo 'selected=""'; } ?>>€ EURO</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-1 switches">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label class="switch">
                                                <input id="default_admin_currency_status" type="checkbox" name="default_admin_currency_status" value="<?php echo ($company['default_admin_currency_status'] ==1)?1:0;?>"  <?php echo ($company['default_admin_currency_status'] ==1)?'checked':'';?> >
                                                <span class="slider round"></span>
                                            </label>

                                        </div>
                                    </div>

                                    <script type="text/javascript">
                                        $(function() {
                                            $('#default_admin_currency_status').on('change.bootstrapSwitch', function(e) {
                                                // console.log(e.target.checked);
                                                $('#default_admin_currency_status').val((e.target.checked)?1:0);
                                            });
                                        });
                                    </script>
                                </div>
                                <div class="col-xs-1">
                                    <div class="form-group">
                                        <label>Default Area language</label>
                                        <select class="form-control" name="default_area_language" required>
                                            <option disabled="" >-- Select Default Front language --</option>
                                            <?php
                                            foreach ($languages as $lang) :
                                                ?>

                                                <option value="<?=$lang->short_code?>" <?php if($company['default_area_language'] == $lang->short_code){ echo 'selected=""'; } ?>><?= ucwords($lang->title); ?></option>

                                            <?php
                                            endforeach;
                                            ?>
                                             </select>

                                    </div>
                                </div>
                                <div class="col-xs-1 switches">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label class="switch">
                                                <input id="default_area_language_status" type="checkbox" name="default_area_language_status" value="<?php echo ($company['default_area_language_status'] ==1)?1:0;?>"  <?php echo ($company['default_area_language_status'] ==1)?'checked':'';?> >
                                                <span class="slider round"></span>
                                            </label>

                                        </div>
                                    </div>

                                    <script type="text/javascript">
                                        $(function() {
                                            $('#default_area_language_status').on('change.bootstrapSwitch', function(e) {
                                                // console.log(e.target.checked);
                                                $('#default_area_language_status').val((e.target.checked)?1:0);
                                            });
                                        });
                                    </script>
                                </div>
                                <div class="col-xs-1">
                                    <div class="form-group">
                                        <label>Default Area currency</label>
                                        <select class="form-control" name="default_area_currency" required>
                                            <option disabled="" >-- Select Default currency --</option>
                                            <option value="usd" <?php if($company['default_area_currency'] == 'usd'){ echo 'selected=""'; } ?>>$ USD</option>
                                            <option value="euro" <?php if($company['default_area_currency'] == 'euro'){ echo 'selected=""'; } ?>>€ EURO</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-1 switches">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label class="switch">
                                                <input id="default_area_currency_status" type="checkbox" name="default_area_currency_status" value="<?php echo ($company['default_area_currency_status'] ==1)?1:0;?>"  <?php echo ($company['default_area_currency_status'] ==1)?'checked':'';?> >
                                                <span class="slider round"></span>
                                            </label>

                                        </div>
                                    </div>

                                    <script type="text/javascript">
                                        $(function() {
                                            $('#default_area_currency_status').on('change.bootstrapSwitch', function(e) {
                                                // console.log(e.target.checked);
                                                $('#default_area_currency_status').val((e.target.checked)?1:0);
                                            });
                                        });
                                    </script>
                                </div>
                                <div class="col-xs-1 unit">
                                    <div class="form-group">
                                        <label>Default unity</label>

                                        <select class="form-control" name="default_unity" required>
                                            <option disabled="" selected="">-- Select Default unity --</option>
                                            <option value="km" <?php if($company['default_unity'] == 'km'){ echo 'selected=""'; } ?>>KM</option>
                                            <option value="mile" <?php if($company['default_unity'] == 'mile'){ echo 'selected=""'; } ?>>Mile</option>
                                        </select>


                                    </div>
                                </div>
                                <div class="col-xs-1">
                                    <div class="form-group">
                                        <label>Quote method</label>
                                        <!--                                 <div class="input-group">
                                    <input type="text" class="form-control"  name="price_calculation_method" placeholder="Price calculation" value="<?= $company['price_calculation_method']; ?>">
                                  </div> -->
                                        <div class="form-group">
                                            <select class="form-control" name="price_calculation_method" required>
                                                <option disabled="" selected="">-- Select Quote Method --</option>
                                                <option value="manual" <?php if($company['price_calculation_method'] == 'manual'){ echo 'selected=""'; } ?>>Manual</option>
                                                <option value="autometic" <?php if($company['price_calculation_method'] == 'autometic'){ echo 'selected=""'; } ?>>Autometic</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-1">
                                    <div class="form-group">
                                        <label>Payment method</label>


                                            <select class="form-control" name="payment_gatways" required>
                                                <option disabled="" selected="">Select payment method --</option>
                                                <option value="credit_card" <?php if($company['payment_gatways'] == 'credit_card'){ echo 'selected=""'; } ?>>Credit Card</option>
                                                <option value="check" <?php if($company['payment_gatways'] == 'check'){ echo 'selected=""'; } ?>>Check</option>
                                                <option value="bank_wire" <?php if($company['payment_gatways'] == 'bank_wire'){ echo 'selected=""'; } ?>>Direct bank Wire</option>
                                                <option value="cash" <?php if($company['payment_gatways'] == 'cash'){ echo 'selected=""'; } ?>>Cash to Driver</option>
                                            </select>


                                    </div>
                                </div>
                                <!--                         <div class="col-xs-2">
                            <div class="form-group">
                                <label>Audio File</label>
                                <div class="form-group">
                                    <select class="form-control" name="audio_file" required>
                                        <option disabled="" selected="">Select Audio File type --</option>
                                        <option value="mp3" <?php if($company['audio_file'] == 'mp3'){ echo 'selected=""'; } ?>>MP3</option>
                                        <option value="wav" <?php if($company['audio_file'] == 'wav'){ echo 'selected=""'; } ?>>WAV</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                      -->

                                <!--               <div class="col-xs-2">
                        <div class="form-group">
                            <label>Website BG</label>
                            <div class="input-group">
                                <input type="text" class="form-control"  name="website_bg" placeholder="Put size here" value="<?= $company['website_bg']; ?>">
                            </div>
                        </div>
                      </div> -->






                            </div>

                            <div class="row">

                                <div class="col-xs-3 ">
                                    <div class="form-group">
                                        <div>
                                            <label>IOS Client</label>

                                            <input type="text" class="form-control"  name="ios_client" placeholder="" value="<?= $company['ios_client']; ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-3 ">
                                    <div class="form-group">

                                        <div >
                                            <label>IOS Driver</label>

                                            <input type="text" class="form-control"  name="ios_driver" placeholder="" value="<?= $company['ios_client']; ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-3 ">
                                    <div class="form-group">
                                        <div>
                                            <label>Android Client</label>

                                            <input type="text" class="form-control"  name="android_client" placeholder="" value="<?= $company['android_client']; ?>">
                                        </div>

                                    </div>
                                </div>

                                <div class="col-xs-3 ">
                                    <div class="form-group">
                                        <div >
                                            <label>Android Driver</label>

                                            <input type="text" class="form-control"  name="android_driver" placeholder="" value="<?= $company['android_driver']; ?>">
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="row">
                                <div class=""id="dynamic_box" style="padding-top: 50px;"></div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="text-right">
                                        <button class="btn btn-default">Update</button>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.module-->
        </div>
        <!--/.content-->
    </div>

</section>

    <?php extract($company);?>



    <?php $locale_info = localeconv(); ?>
    <style>
        @media only screen and (min-width: 1400px){
            .table-filter input, .table-filter select{
                max-width: 9% !important;
            }
            .table-filter select{
                max-width: 95px !important;
            }
            .table-filter .dpo {
                max-width: 90px !important;
            }
        }
    </style>

    <script type="text/javascript">
        $(document).on('change','#company_logo',function(e)
        {
            if(this.files)
            {
                var reader = new FileReader();
                reader.onload = function (e){ $('#company_logo_preview').attr('src', reader.result); }
                reader.readAsDataURL(this.files[0]);
            }
            else
            {
                console.log('No Image');
            }
        });
        $(document).on('change','#footer_logo',function(e)
        {
            if(this.files)
            {
                var reader = new FileReader();
                reader.onload = function (e){ $('#footer_logo_preview').attr('src', reader.result); }
                reader.readAsDataURL(this.files[0]);
            }
            else
            {
                console.log('No Image');
            }
        });
    </script>




    <script type="text/javascript">





        function get_region_base_country(region_id=null)
        {

            country_id=$('#country_id').val();


            $.ajax({
                url:'<?php base_url();?>cms/ajax_get_region_listing.php',
                method: 'post',
                data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id,
                dataType: 'json',
                success: function(response){
                    // console.log(response);
                    html ='';
                    html =html + '<option value="">Select Region</option>';

                    $.each(response, function( index, value ) {
                        // console.log( value, region_id );


                        if(region_id==value.id)
                        {
                            html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';

                        }
                        else{
                            html =html + '<option value="'+value.id+'">'+value.name+'</option>';

                        }
                    });

                    $('#region_id').html(html);


                }


            });

        }



        function get_cities_base_country_region(region_id=null,cities_id=null)
        {

            country_id=$('#country_id').val();
            if(region_id==null)
            {
                region_id=$('#region_id').val();

            }


            $.ajax({
                url:'<?php base_url();?>cms/ajax_get_cities_listing.php',
                method: 'post',
                data: '<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>&country_id='+country_id+'&region_id='+region_id,
                dataType: 'json',
                success: function(response){
                    // console.log(response);
                    html ='';
                    html =html + ' <option value="">Select cities</option>';

                    $.each(response, function( index, value ) {
                        // console.log( value , cities_id);


                        if(cities_id==value.id)
                        {
                            html =html + '<option selected="true" value="'+value.id+'">'+value.name+'</option>';

                        }
                        else{
                            html =html + '<option value="'+value.id+'">'+value.name+'</option>';

                        }
                    });

                    $('#cities_id').html(html);


                }


            });

        }



        $(document).ready(function() {

            // ajax_get_all_cms_listing

            $('#country_id').change(function(){
                get_region_base_country();

            });
            $('#region_id').change(function(){

                get_cities_base_country_region();

            });

            get_region_base_country(<?php echo $company['region'];?>);
            get_cities_base_country_region(<?php echo $company['region'];?>,<?php echo $company['city'];?>);

        })

    </script>




    <script type="text/javascript">

        function add_more()
        {
            var html =office_time_box();
            $('#dynamic_box').append(html);
            // console.log($( document ).height());
            $("#content").height($( document ).height()-700);
        }


        function delete_item(elm)
        {
            $(elm).parent().parent().remove();
            // console.log($( document ).height());
            $("#content").height($( document ).height()-1000);
        }




        function office_time_box(action=null)
        {

            option_days ='                                          <option value="Sunday">Sunday</option>                                           <option value="Monday">Monday</option>                                           <option value="Tuesday">Tuesday</option>                                         <option value="Wednesday">Wedneshday</option>                                    <option value="Thursday">Thursday</option>                                               <option value="Friday" selected="">Friday</option>                                                      <option value="Saturday">Saturday</option>    ';

            var opening_day_from ='<div class="col-xs-2 custom_gray_bg"><div class="form-group"><label>Office Opening Days <br> From</label> <div class="form-group"><select class="form-control" name="from_day[]" required=""> <option disabled="" selected=""> --Select from Day--</option>'+option_days+'</select></div></div></div>';

            var opening_day_to='<div class="col-xs-2 custom_gray_bg">'+
                '<div class="form-group">                            <label> <br>to</label>                            <div class="form-group">                                                              <select class="form-control" name="to_day[]" required="">                                    <option disabled="" selected="">--Select to Day--</option> '+
                option_days
                +'</select>                                                                           </div></div>'+
                '</div>';

            var time_option='';
            for(i=1; i<=24; i++)
            {
                time_option =time_option+'<option value="'+i+'">'+i+':00</option>';
            }

            var opening_hour_from_1 ='<div class="col-xs-2 custom_gray_bg">'+
                ' <div class="form-group">                                                              <label>Office Opening Hours <br>From</label>  <div class="form-group"><select class="form-control" name="start_time_1[]" required=""><option disabled="" selected="">--Select start Hour--</option>'+
                time_option+
                '</select></div></div>'+
                '</div>';
            var opening_hour_to_1 ='<div class="col-xs-2 custom_gray_bg">'+
                ' <div class="form-group">                                                              <label><br>To</label>  <div class="form-group"><select class="form-control" name="end_time_1[]" required=""><option disabled="" selected="">--Select start Hour--</option>'+
                time_option+
                '</select></div></div>'+
                '</div>';
            var opening_hour_from_2 ='<div class="col-xs-2 custom_gray_bg">'+
                ' <div class="form-group">                                                              <label>AND <br>From</label>  <div class="form-group"><select class="form-control" name="start_time_2[]" required=""><option disabled="" selected="">--Select start Hour--</option>'+
                time_option+
                '</select></div></div>'+
                '</div>';
            var opening_hour_to_2 ='<div class="col-xs-2 custom_gray_bg">'+
                ' <div class="form-group">                                                              <label><br>To</label>  <div class="form-group"><select class="form-control" name="end_time_2[]" required=""><option disabled="" selected="">--Select start Hour--</option>'+
                time_option+
                '</select></div></div>'+
                '</div>';
            button_controll='';
            if(action=='plus')
            {
                var button_controll ='<div class="col-xs-1 "><button type="button" class="btn btn-default" style="" onclick="add_more()">+</button></div>';
            }
            else
            {
                var button_controll ='<div class="col-xs-1 "><button type="button" class="btn btn-default" onclick="delete_item(this)">-</button></div>';
            }




            return html ='<div id="js_time_box"><div class="col-md-10" style="margin-bottom:30px;">'+opening_day_from+opening_day_to+opening_hour_from_1+opening_hour_to_1+opening_hour_from_2+opening_hour_to_2+'</div>'+button_controll+'</div>';

        }

        function office_time_box_edit(action=null)
        {

            var from_day =<?php echo $from_day;?>;
            var to_day =<?php echo $to_day;?>;
            var start_time_1 =<?php echo $start_time_1;?>;
            var end_time_1 =<?php echo $end_time_1;?>;
            var start_time_2 =<?php echo $start_time_2;?>;
            var end_time_2 =<?php echo $end_time_2;?>;

            var count_box=0;
            var day_array =['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

            result_html ='';

            if(from_day != null)
            $.each(from_day,function(day_index, day_value){

                option_days='';
                option_days_to='';

                $.each(day_array,function(index,value){
                    selected_item =(value==day_value)?'selected':'';
                    option_days +='<option '+selected_item+' value="'+value+'" >'+value+'</option>';

                    selected_item_to =(value==to_day[count_box])?'selected':'';
                    option_days_to +='<option '+selected_item_to+' value="'+value+'" >'+value+'</option>';


                });



                option_start_time_1 ='';
                option_end_time_1 ='';

                option_start_time_2 ='';
                option_end_time_2 ='';

                for(i=1; i<=24; i++)
                {
                    selected_option_start_time_1=(i==start_time_1[count_box])?'selected':'';
                    selected_option_end_time_1=(i==end_time_1[count_box])?'selected':'';
                    option_start_time_1 =option_start_time_1+'<option '+selected_option_start_time_1+' value="'+i+'">'+i+':00</option>';

                    option_end_time_1 =option_end_time_1+'<option '+selected_option_end_time_1+' value="'+i+'">'+i+':00</option>';


                    selected_option_start_time_2=(i==start_time_2[count_box])?'selected':'';
                    selected_option_end_time_2=(i==end_time_2[count_box])?'selected':'';


                    option_start_time_2 =option_start_time_2+'<option '+selected_option_start_time_2+' value="'+i+'">'+i+':00</option>';
                    option_end_time_2 =option_end_time_2+'<option '+selected_option_end_time_2+' value="'+i+'">'+i+':00</option>';

                }



                // console.log(option_days);
                // console.log(option_days_to);
                // console.log(option_start_time_1);
                // console.log(option_end_time_1);

                // console.log(option_start_time_2);
                // console.log(option_end_time_2);

                var opening_day_from ='<div class="col-xs-2 custom_gray_bg"><div class="form-group"><label>Office Opening Days <br> From</label> <div class="form-group"><select class="form-control" name="from_day[]" required=""> <option disabled="" selected=""> --Select from Day--</option>'+option_days+'</select></div></div></div>';


                var opening_day_to='<div class="col-xs-2 custom_gray_bg">'+
                    '<div class="form-group">                            <label> <br>to</label>                            <div class="form-group">                                                              <select class="form-control" name="to_day[]" required="">                                    <option disabled="" selected="">--Select to Day--</option> '+
                    option_days_to
                    +'</select>                                                                           </div></div>'+
                    '</div>';



                var opening_hour_from_1 ='<div class="col-xs-2 custom_gray_bg">'+
                    ' <div class="form-group">                                                              <label>Office Opening Hours <br>From</label>  <div class="form-group"><select class="form-control" name="start_time_1[]" required=""><option disabled="" selected="">--Select start Hour--</option>'+
                    option_start_time_1+
                    '</select></div></div>'+
                    '</div>';


                var opening_hour_to_1 ='<div class="col-xs-2 custom_gray_bg">'+
                    ' <div class="form-group">                                                              <label><br>To</label>  <div class="form-group"><select class="form-control" name="end_time_1[]" required=""><option disabled="" selected="">--Select start Hour--</option>'+
                    option_end_time_1+
                    '</select></div></div>'+
                    '</div>';


                var opening_hour_from_2 ='<div class="col-xs-2 custom_gray_bg">'+
                    ' <div class="form-group">                                                              <label>AND <br>From</label>  <div class="form-group"><select class="form-control" name="start_time_2[]" required=""><option disabled="" selected="">--Select start Hour--</option>'+
                    option_start_time_2+
                    '</select></div></div>'+
                    '</div>';
                var opening_hour_to_2 ='<div class="col-xs-2 custom_gray_bg">'+
                    ' <div class="form-group">                                                              <label><br>To</label>  <div class="form-group"><select class="form-control" name="end_time_2[]" required=""><option disabled="" selected="">--Select start Hour--</option>'+
                    option_end_time_2+
                    '</select></div></div>'+
                    '</div>';



                button_controll='';
                if(count_box==0)
                {
                    var button_controll ='<div class="col-xs-1 "><button type="button" class="btn btn-default" style="" onclick="add_more()">+</button></div>';
                }
                else
                {
                    var button_controll ='<div class="col-xs-1 "><button type="button" class="btn btn-default" onclick="delete_item(this)">-</button></div>';
                }
                result_html =result_html+'<div id="js_time_box"><div class="col-md-10" style="margin-bottom:30px;">'+opening_day_from+opening_day_to+opening_hour_from_1+opening_hour_to_1+opening_hour_from_2+opening_hour_to_2+'</div>'+button_controll+'</div>';

                count_box++;
            });


            return result_html;

        }

        $(document).ready(function(){
            office_time_box_html =office_time_box_edit();
            $('#dynamic_box').html(office_time_box_html);

            $("#content").height($( document ).height()-700);


        });

    </script>


    <style type="text/css">
        input[type="text"] {float:none;}
    </style>


