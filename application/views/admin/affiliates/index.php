<script type="text/javascript">
    function showAdd() {
        hideForms();
        $('.addDiv').show();
        //showAddHead();
    }
    function hideForms() {
        $('.addDiv').hide();
        $('.listDiv').hide();
    }
    function hideAdd() {
        hideForms();
        $('.listDiv').show();
    }
    (function ($, W, D) {
        var JQUERY4U = {};

        JQUERY4U.UTIL =
                {
                    setupFormValidation: function ()
                    {
                        //Additional Methods            
                        $.validator.addMethod("lettersonly", function (a, b) {
                            return this.optional(b) || /^[a-z ]+$/i.test(a)
                        }, "<?php echo $this->lang->line('valid_name'); ?>");

                        $.validator.addMethod("phoneNumber", function (uid, element) {
                            return (this.optional(element) || uid.match(/^([0-9]*)$/));
                        }, "<?php echo $this->lang->line('valid_phone_number'); ?>");


                        $.validator.addMethod("pwdmatch", function (repwd, element) {
                            var pwd = $('#password').val();
                            return (this.optional(element) || repwd == pwd);
                        }, "<?php echo $this->lang->line('valid_passwords'); ?>");

                        //form validation rules
                        $("#affiliate_addform").validate({
                            rules: {
                                statut: {
                                    required: true
                                },
                                company: {
                                    required: function () {
                                        return $("#statut").val() == "2";
                                    }
                                },
                                first_name: {
                                    required: true,
                                    lettersonly: true
                                },
                                email: {
                                    required: true,
                                    email: true
                                },
                                phone: {
                                    required: true,
                                    phoneNumber: true,
                                    rangelength: [10, 11]
                                },
                                password: {
                                    required: true,
                                    rangelength: [8, 30]
                                },
                                confirm_password: {
                                    required: true,
                                    pwdmatch: true
                                }
                            },
                            messages: {
                                statut: {
                                    required: "Please select the statut"
                                },
                                company: {
                                    required: "Please enter the company"
                                },
                                first_name: {
                                    required: "<?php echo $this->lang->line('first_name_valid'); ?>"
                                },
                                email: {
                                    required: "<?php echo $this->lang->line('email_valid'); ?>"
                                },
                                phone: {
                                    required: "<?php echo $this->lang->line('phone_valid'); ?>"
                                },
                                password: {
                                    required: "<?php echo $this->lang->line('password_valid'); ?>"
                                },
                                password_confirm: {
                                    required: "<?php echo $this->lang->line('confirm_password_valid'); ?>"
                                }
                            },
                            submitHandler: function (form) {
                                form.submit();
                            }
                        });
                    }
                }

        //when the dom has loaded setup form validation rules
        $(D).ready(function ($) {
            JQUERY4U.UTIL.setupFormValidation();
        });

    })(jQuery, window, document);

</script>
<section id="content">
    <?php $this->load->view('admin/common/breadcrumbs'); ?>
    <div class="row">
        <div class="col-md-12">
            <?php
            $flashAlert = $this->session->flashdata('alert');
            if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
                ?>
                <br>
                <div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
                    <strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
                    <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php } ?>

            <div class="module">
                <?php echo $this->session->flashdata('message'); ?>
                <div class="module-head">
                    
                </div>
                <div class="clearfix"></div>
                <div class="module-body table-responsive">
                    <table id="example" class="table table-bordered table-striped  table-hover dataTable" cellspacing="0" width="100%" data-selected_id="">
                        <thead>
                            <tr>
                                <th class="no-sort text-center"><input type='checkbox' name='allAffiliates' id="allAffiliates" value='' onclick="Javascript:selectAllAffiliates();" /></th>
                                <th class="column-id"><?php echo $this->lang->line('id'); ?></th>
                                <th class="column-date"><?php echo "Date"; ?></th>
                                <th class="column-date"><?php echo "Time"; ?></th>
                                <th class="column-name" ><?php echo $this->lang->line('name'); ?></th>
                                <th class="column-email"><?php echo $this->lang->line('email'); ?></th>
                                <th class="column-phone"><?php echo $this->lang->line('phone'); ?></th>
                                <th class="column-phone"><?php echo 'Website'; ?></th>
                                <th class="column-phone"><?php echo 'Commissions'; ?></th>
                                <th class="column-phone"><?php echo 'Payout'; ?></th>
                                <th class="column-phone"><?php echo 'Due'; ?></th>
                                <th class="column-status"><?php echo $this->lang->line('status'); ?></th>
                                <th class="column-since"><?php echo "Since"; ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($data) && !empty($data)): ?>
                                <?php foreach ($data as $key => $item): ?>
                                    <?php ?>
                                    <tr>
                                        <td align="center"><input type='checkbox' name='affiliates[]' class='ccheckbox' value="<?php echo $item['id']; ?>" /></td>
                                        <td><?php echo $item['link']; ?></td>
                                        <td><?php echo $item['date']; ?></td>
                                        <td><?php echo $item['time']; ?></td>
                                        <td><?php echo $item['name']; ?></td>
                                        <td><?php echo $item['email']; ?></td>
                                        <td><?php echo $item['phone']; ?></td>
                                        <td><?php echo $item['website']; ?></td>
                                        <td><?php echo $item['commissions']; ?></td>
                                        <td><?php echo $item['payout']; ?></td>
                                        <td><?php echo $item['due']; ?></td>
                                        <td><?php echo $item['status']; ?></td>
                                        <td><?php echo $item['since']; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="table-filter" class="hide">
    <?php echo form_open('admin/affiliates', array("id" => "search_filter")); ?>
    <input type="text" placeholder="Name" class="form-control" name="search_name" id="search_name">
    <input type="text" placeholder="Email" class="form-control" name="search_email" id="search_email" >
    <input type="text" placeholder="Phone" name="search_phone" id="search_phone" class="form-control">
    <input type="text" placeholder="From" name="date_from" id="date_from" class="dpo">
    <input type="text" placeholder="To" name="date_to" id="date_to" class="dpo">
    <select class="form-control" name="status" id="status">
        <option value="">All Status</option>
        <?php foreach(config_model::$status as $key => $status):?>
            <option <?=set_value('status',$this->input->post('status')) == $status ? "selected" : ""?> value="<?=$status?>"><?=$status?></option>
        <?php endforeach;?>
    </select>
    <input type="submit" name="search" id="search" value="Search" class="btn btn-sm btn-default" />
    <?php echo form_close(); ?>
</div>
<script>
    $(document).ready(function() { 
        // Search form validation rules
        $("#search_filter").validate({
            rules: {
                search_name: { required:false }
            },
            messages: {
                search_name: {
                    required:"Please enter the name"
                }
            },
            submitHandler: function(form) {
                /*
                $.ajax({
                        url: 'affiliates/affiliateSearchData',
                        type: 'post',
                        data: $("#search_filter").serialize(), // /converting the form data into array and sending it to server
                        dataType: 'json',
                        success: function (response) {
                            console.log(response.data);
                            //  $("#manageTable tbody").empty();
                            //if (response.success === true) {
                            console.log("Table" + $.fn.dataTable.isDataTable('#example'));

                            if ($.fn.dataTable.isDataTable('#example') == false) {
                                $('#example').DataTable({
                                    'data': response.data,
                                    'order': [],
                                    'paging': true,
                                    'pageLength': 15,
                                    'columnDefs': [ { "targets": 0, "className": "text-center", "width": "4%" },
                                                    { "targets": 1, "className": "text-center", "width": "4%" },
                                                    { "targets": 2, "className": "text-center", "width": "4%" },
                                                    { "targets": 3, "className": "text-center", "width": "4%" },
                                                    { "targets": 4, "className": "text-center", "width": "10%" },
                                                    { "targets": 5, "className": "text-center", "width": "8%" },
                                                    { "targets": 6, "className": "text-center", "width": "4%" },
                                                    { "targets": 7, "className": "text-center", "width": "5%" },
                                                    { "targets": 8, "className": "text-center", "width": "8%" }
                                                  ]
                                });
                            } else {
                                var table = $('#example').DataTable();
                                table.destroy();
                                $('#example').DataTable({
                                    'data': response.data,
                                    'order': [],
                                    'paging': true,
                                    'pageLength': 15,
                                    'columnDefs': [ { "targets": 0, "className": "text-center", "width": "4%" },
                                                    { "targets": 1, "className": "text-center", "width": "4%" },
                                                    { "targets": 2, "className": "text-center", "width": "4%" },
                                                    { "targets": 3, "className": "text-center", "width": "4%" },
                                                    { "targets": 4, "className": "text-center", "width": "10%" },
                                                    { "targets": 5, "className": "text-center", "width": "8%" },
                                                    { "targets": 6, "className": "text-center", "width": "4%" },
                                                    { "targets": 7, "className": "text-center", "width": "5%" },
                                                    { "targets": 8, "className": "text-center", "width": "8%" }
                                                  ]
                                });
                            }
                        }
                    });*/
                    form.submit();
            }
        });
    });
</script>