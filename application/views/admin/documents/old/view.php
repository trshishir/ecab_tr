<script type="text/javascript">
    function showAdd() {
        hideForms();
        $('.addDiv').show();
        //showAddHead();
    }
    function hideForms() {
        $('.addDiv').hide();
        $('.listDiv').hide();
    }
    function hideAdd() {
        hideForms();
        $('.listDiv').show();
    }
    (function ($, W, D) {
        var JQUERY4U = {};

        JQUERY4U.UTIL =
                {
                    setupFormValidation: function ()
                    {
                        //Additional Methods            
                        $.validator.addMethod("lettersonly", function (a, b) {
                            return this.optional(b) || /^[a-z ]+$/i.test(a)
                        }, "<?php echo $this->lang->line('valid_name'); ?>");

                        //form validation rules
                        $("#document_addform").validate({
                            rules: {
                                dcsa1: {
                                    required:true
                                },
                                dcsa: {
                                    required:true
                                },
                                doc_category: {
                                    required:true
                                },
                                doc_title: {
                                    required:true
                                },
                                created_for: {
                                    required:true
                                },
                                document_body: {
                                    required:true
                                },
                                status: {
                                    required:true
                                }
                            },
                            messages: {
                                dcsa1: {
                                    required:"Please select the one option"
                                },
                                dcsa: {
                                    required:"Please select the one option"
                                },
                                doc_category: {
                                    required:"Please select the category"
                                },
                                doc_title: {
                                    required:"Please provide the document title"
                                },
                                created_for: {
                                    required:"Please provide the created for"
                                },
                                document_body: {
                                    required:"Please provide the document body"
                                },
                                status: {
                                    required:"Please select the status"
                                }
                            }, 
                            submitHandler: function(form) {
                                form.submit();
                            }
                        });
                    }
                }

        //when the dom has loaded setup form validation rules
        $(D).ready(function ($) {
            JQUERY4U.UTIL.setupFormValidation();
        });

    })(jQuery, window, document);
    
</script>
<style type="text/css">
    .ckeditor_div{
      all: unset;
    }
    h1{
      all: unset;
    }
</style>
<section id="content">
    <div class="listDiv">
        <?php $this->load->view('admin/common/breadcrumbs'); ?>
        <div class="row-fluid">
            <?php   $flashAlert = $this->session->flashdata('alert');
                    if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
            ?>
                <br>
                <div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
                    <strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
                    <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php   } ?>

            <div class="module" style="margin: 0px;">
                <?php echo $this->session->flashdata('message'); ?>
                <div class="module-head">
                        <div class="module-filter">
                            <div class="col-md-12">
                                <div class="col-md-12" style="padding: 0px;">
                                    <div class="row">
                                        <div class="col-md-12 row" style="float: none !important; padding: 0px; margin: 0 auto !important; display: table;">
                                            <div class="chart-outside" style="border: 0px;">
                                                <div class="handle" style="float: right; background: transparent !important; height: 48px !important;">
                                                    <button class="btn btn-default"><i class="fa fa-download"></i>&nbsp;&nbsp;Download</button>
                                                    <button class="btn btn-default"><i class="fa fa-print"></i>&nbsp;&nbsp;Print</button>
                                                    <div class="col-md-3" style="padding: 0px; margin-top: 2px;">
                                                        <input type="text" name="email_to" placeholder="Email To Send To" />
                                                    </div>
                                                    <div class="col-md-2" style="padding-right: 0px !important; margin-left: -10px;">
                                                        <button class="btn btn-default"><i class="fa fa-envelope"></i>&nbsp;&nbsp;Send</button>
                                                    </div>
                                                    <button class="btn btn-default"><i class="fa fa-file"></i>&nbsp;&nbsp;Word</button>
                                                    <button class="btn btn-default"><i class="fa fa-file"></i>&nbsp;&nbsp;PDF</button>
                                                </div>
                                                <!-- <h3 class="handle" style="font-weight: bold; font-size: 18px !important;"><?php echo 'Document Viewer';?></h3> -->
                                            </div>
                                            <div class="panel panel-default col-md-12" style="border: 3px solid #ccc; overflow-y: scroll;">
                                              <!-- <div class="panel-heading"></div> -->
                                              <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="col-md-4">
                                                            <img src="http://localhost:8080/ecab/assets/system_design/images/logo-admin.png" />
                                                            <h4>
                                                                <strong>Company Name<br />
                                                                Company Address<br />
                                                                Company Phone<br />
                                                                Company Fax<br />
                                                                Company Email<br />
                                                                Company Siteweb</strong>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12" style="text-align: right">
                                                        <h4>
                                                            <strong>Destination Name<br />
                                                            Destination Address<br /></strong>
                                                        </h4>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <h4>
                                                            Letter recommandee AR,
                                                        </h4>
                                                        <h4>
                                                            Pret: 12345 / 12345 / 123456 / 123 / 12
                                                        </h4>
                                                        <h4>
                                                            Madam/Sir, Name here,
                                                        </h4>
                                                        <h3>
                                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br />
                                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                                                        </h3>
                                                    </div>
                                                    <div class="col-md-12" style="text-align: right">
                                                        <h4>
                                                            <strong>Doc City<br />
                                                            Doc Date<br />
                                                            Department Name<br />
                                                            Manager Name<br />
                                                            Signature<br /></strong>
                                                            <img src="http://localhost:8080/ecab/assets/system_design/images/logo-admin.png" />
                                                        </h4>
                                                    </div>
                                                    <div class="col-md-12" style="text-align: center">
                                                        <h4>
                                                            <strong>Company info will be show here</strong>
                                                        </h4>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row" style="float: right;">
                                                <div class="col-md-12">
                                                    <a href="<?php echo base_url(); ?>admin/documents.php" class="btn btn-default"><i class="fa fa-ban"></i>&nbsp;&nbsp;Cancel</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="addDiv" style="display:none;">
        <?php $this->data['show_subtitle'] = true ; $this->load->view('admin/common/breadcrumbs'); ?>
        <div class="row-fluid">
            <?php   $flashAlert = $this->session->flashdata('alert');
                    if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
            ?>
                <br>
                <div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
                    <strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
                    <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php   } ?>
        </div>
        <?php echo form_open('documents/add', array("id" => "document_addform")); ?>
        <div class="row">
            <div class="col-md-12">
                <!-- <div class="row">
                    <div class="col-md-2 form-group"> 
                        <label> Civility </label>                                
                        <select class="form-control" name="civility" tabindex="3">
                            <option value="Mr">Mr</option>
                            <option value="Miss">Miss</option>
                            <option value="Mrs">Mrs</option>
                            <option value="Mme">Mme</option>
                        </select>
                    </div>
                </div> -->
                <div class="row" style="margin-top: 100px;">
                    <div class="col-md-4 form-group"> 
                        <label> Category </label>                                
                        <select class="form-control" name="dcsa1" id="dcsa1" tabindex="1">
                            <option value="">Select Option</option>
                            <option value="Option 1">Option 1</option>
                            <option value="Option 2">Option 2</option>
                            <option value="Option 3">Option 3</option>
                        </select>
                    </div>
                    <div class="col-md-4 form-group"> 
                        <!-- <label>Name</label> -->
                        <label> List E </label>
                        <select class="form-control" name="dcsa" id="dcsa" tabindex="2">
                            <option value="">Select Option</option>
                            <option value="Option 1">Option 1</option>
                            <option value="Option 2">Option 2</option>
                            <option value="Option 3">Option 3</option>
                        </select>
                    </div>
                    <div class="col-md-4 form-group"> 
                        <label> Category </label> 
                        <select class="form-control" name="doc_category" id="doc_category" tabindex="3">
                            <option value="">Select Category</option>
                            <option value="1">Category 1</option>
                            <option value="2">Category 2</option>
                            <option value="3">Category 3</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 form-group"> 
                        <label>Document Title</label>
                        <?php echo form_input($doc_title); ?>
                        <?php echo form_error('doc_title'); ?> 
                    </div>
                    <div class="col-md-4 form-group"> 
                        <label>Created For</label>
                        <?php echo form_input($created_for); ?>
                        <?php echo form_error('created_for'); ?> 
                    </div>
                    <div class="col-md-4">
                        <label>Status</label>
                        <select class="form-control"  name="status" id="status" tabindex="6" data-name="status">
                             <option value="">All Status</option>
                             <option value="1">Active</option>
                             <option value="0">Inactive</option>
                         </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label>Document Body</label>
                        <textarea id="editor" name="document_body" id="document_body" tabindex="7">
                        </textarea>
                        <!-- <textarea id='document_body' name='document_body' style='display:block; height: 0px; width: 0px; margin-top: -60; background: #eee; border: 0px; overflow: hidden;'></textarea> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <br />
            <div class="col-md-12"> 
                <div class="pull-right">
                    <!-- <input type="submit" class="btn btn-default" value="Save" tabindex="8" />   -->
                    <!-- <input type="button" class="btn btn-default" value="Cancel" onclick="return hideAdd();" tabindex="9" /> -->
                    <button type="submit" class="btn btn-default" tabindex="8"><i class="fa fa-plus"></i>&nbsp;&nbsp;Save</button>
                    <button type="button" class="btn btn-default" tabindex="9" onclick="return hideAdd();"><i class="fa fa-close"></i>&nbsp;&nbsp;Cancel</button>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</section>
</div>
<!--
<div class="filter-search" style="display: none;">
    <?php // echo form_open('clients', array("id" => "search_filter")); ?>
    <form action="<?php echo base_url();?>clients" method="post" accept-charset="utf-8" name="search_filter" id="search_filter">
        <div class="row  row-no-gutters padding-b">
            <div class="col-md-2">
                 <input type="text" name="search_name" id="search_name" placeholder="Name" class="input-large form-control" data-name="name">
            </div>
            <div class="col-md-2">
                 <input type="text" name="search_email" id="search_email" placeholder="Email" data-name="email" class="form-control">
            </div>
            <div class="col-md-2">
                 <input type="text" name="search_phone" id="search_phone" placeholder="Phone" data-name="phone" class="form-control">
            </div>
            <div class="col-md-1">
                <input type="text" name="date_from" id="date_from" placeholder="From" data-name="date_from" class="dpo">
            </div>
            <div class="col-md-1">
                <input type="text" name="date_to" id="date_to" placeholder="To" data-name="date_to" class="dpo">
            </div>
            <div class="col-md-1">
                <select class="form-control" name="status" id="status" data-name="status" style="width:100%!important;">
                     <option value="">All Status</option>
                     <option value="1">Active</option>
                     <option value="0">Inactive</option>
                 </select>
            </div>
            <div class="col-md-2">
                <input type="submit" name="search" id="search" value="Search" class="btn btn-sm btn-default" />
            </div>
         </div>
    <?php // echo form_close(); ?>
    </div>
</div>
-->
 <script>
    initSample();
    $(document).ready(function() {
        CKEDITOR.replace('editor');
        // $("#document_addform").on("submit", function () {
        //     var hvalue = $('#editor').text();
        //     var data = CKEDITOR.instances.editor.getData();
        //     // $(this).append("<textarea id='document_body' name='document_body' style='display:none'>"+hvalue+"</textarea>");
        //     var removeElements = function(data) {
        //         var wrapped = $("<div>" + data + "</div>");
        //         wrapped.find('script').remove();
        //         return wrapped.html();
        //     }
        //     $("#document_body").val(removeElements);
        //     alert("submittedd " + data);
        // });
        var manageTable = $('#manageDocumentsTable').DataTable({
            'ajax': 'documents/fetchClientsData',
            'order': [],
            'paging': true,
            'pageLength': 15,
            'columnDefs': [ { "targets": 0, "className": "text-center", "width": "4%" },
                            { "targets": 1, "className": "text-center", "width": "4%" },
                            { "targets": 2, "className": "text-center", "width": "4%" },
                            { "targets": 3, "className": "text-center", "width": "4%" },
                            { "targets": 4, "className": "text-center", "width": "10%" },
                            { "targets": 5, "className": "text-center", "width": "8%" },
                            { "targets": 6, "className": "text-center", "width": "4%" },
                            { "targets": 7, "className": "text-center", "width": "5%" },
                            { "targets": 8, "className": "text-center", "width": "5%" },
                            { "targets": 9, "className": "text-center", "width": "5%" },
                            { "targets": 10, "className": "text-center", "width": "5%" },
                            { "targets": 11, "className": "text-center", "width": "5%" },
                            { "targets": 12, "className": "text-center", "width": "5%" },
                            { "targets": 13, "className": "text-center", "width": "8%" }
                          ] 
        });
        
        $(".pdfDownload").on('click',function() {
           alert('PDF Download inprogress...'); 
        });
        $(".csvDownload").on('click',function() {
           alert('CSV Download inprogress...'); 
        });
        $(".excelDownload").on('click',function() {
           // console.log('Excel Download inprogress...'); 
           $.ajax({
                        url: 'clients/clientExcelDownload',
                        type: 'post',
                        data: $("#search_filter").serialize(), // /converting the form data into array and sending it to server
                        dataType: 'json',
                        success: function (response) {
                            //console.log(response.data);
                        }
                 });
        });
        
        // var client_html = "<div class='filter-search col-md-8'>  <form action='' method='post' accept-charset='utf-8' name='search_filter' id='search_filter'> <div class='row  row-no-gutters padding-b'> <div class='col-md-2'> <input type='text' name='search_name' id='search_name' placeholder='Name' class='input-large form-control' data-name='name'> </div> <div class='col-md-2'> <input type='text' name='search_email' id='search_email' placeholder='Email' data-name='email' class='form-control'> </div> <div class='col-md-2'> <input type='text' name='search_phone' id='search_phone' placeholder='Phone' data-name='phone' class='form-control'> </div> <div class='col-md-1'> <input type='text' name='date_from' id='date_from' placeholder='From' data-name='date_from' class='dpo'> </div> <div class='col-md-1'> <input type='text' name='date_to' id='date_to' placeholder='To' data-name='date_to' class='dpo'> </div> <div class='col-md-1'> <select class='form-control' name='status' id='status' data-name='status' style='width:100%!important;'> <option value=''>All Status</option> <option value='1'>Active</option> <option value='0'>Inactive</option> </select> </div> <div class='col-md-2'> <input type='submit' name='search' id='search' value='Search' class='btn btn-sm btn-default' /> </div> </div> </form> </div>";
        // $('.dt-buttons').before(client_html);
        // $('.buttons-excel').before('<a href="javascript:;" class="btn btn-sm btn-default" onclick="showAdd();"><i class="fa fa-plus"></i> Add</a> ');
        
        // Search form validation rules
        $("#search_filter").validate({
            rules: {
                search_name: { required:false }
            },
            messages: {
                search_name: {
                    required:"Please enter the name"
                }
            },
            submitHandler: function(form) {
                $.ajax({
                        url: 'clients/clientSearchData',
                        type: 'post',
                        data: $("#search_filter").serialize(), // /converting the form data into array and sending it to server
                        dataType: 'json',
                        success: function (response) {
                            console.log(response.data);
                            //  $("#manageTable tbody").empty();
                            //if (response.success === true) {
                            console.log("Table" + $.fn.dataTable.isDataTable('#manageDocumentsTable'));

                            if ($.fn.dataTable.isDataTable('#manageDocumentsTable') == false) {
                                $('#manageDocumentsTable').DataTable({
                                    'data': response.data,
                                    'order': [],
                                    'paging': true,
                                    'pageLength': 15,
                                    'columnDefs': [ { "targets": 0, "className": "text-center", "width": "4%" },
                                                    { "targets": 1, "className": "text-center", "width": "4%" },
                                                    { "targets": 2, "className": "text-center", "width": "4%" },
                                                    { "targets": 3, "className": "text-center", "width": "4%" },
                                                    { "targets": 4, "className": "text-center", "width": "10%" },
                                                    { "targets": 5, "className": "text-center", "width": "8%" },
                                                    { "targets": 6, "className": "text-center", "width": "4%" },
                                                    { "targets": 7, "className": "text-center", "width": "5%" },
                                                    { "targets": 8, "className": "text-center", "width": "5%" },
                                                    { "targets": 9, "className": "text-center", "width": "5%" },
                                                    { "targets": 10, "className": "text-center", "width": "5%" },
                                                    { "targets": 11, "className": "text-center", "width": "5%" },
                                                    { "targets": 12, "className": "text-center", "width": "5%" },
                                                    { "targets": 13, "className": "text-center", "width": "8%" }
                                                  ]
                                });
                            } else {
                                var table = $('#manageDocumentsTable').DataTable();
                                table.destroy();
                                $('#manageDocumentsTable').DataTable({
                                    'data': response.data,
                                    'order': [],
                                    'paging': true,
                                    'pageLength': 15,
                                    'columnDefs': [ { "targets": 0, "className": "text-center", "width": "4%" },
                                                    { "targets": 1, "className": "text-center", "width": "4%" },
                                                    { "targets": 2, "className": "text-center", "width": "4%" },
                                                    { "targets": 3, "className": "text-center", "width": "4%" },
                                                    { "targets": 4, "className": "text-center", "width": "10%" },
                                                    { "targets": 5, "className": "text-center", "width": "8%" },
                                                    { "targets": 6, "className": "text-center", "width": "4%" },
                                                    { "targets": 7, "className": "text-center", "width": "5%" },
                                                    { "targets": 8, "className": "text-center", "width": "5%" },
                                                    { "targets": 9, "className": "text-center", "width": "5%" },
                                                    { "targets": 10, "className": "text-center", "width": "5%" },
                                                    { "targets": 11, "className": "text-center", "width": "5%" },
                                                    { "targets": 12, "className": "text-center", "width": "5%" },
                                                    { "targets": 13, "className": "text-center", "width": "8%" }
                                                  ]
                                });
                            }
                        }
                    })
            }
        });
    });
</script>