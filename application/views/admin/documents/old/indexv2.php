<script type="text/javascript">

    var inc_var = 0;
    var values,initials;

    function showAdd() {
        hideForms();
        $('.addDiv').show();
        //showAddHead();
    }
    function hideForms() {
        $('.addDiv').hide();
        $('.editDiv').hide();
        $('.listDiv').hide();

        // alert(initials);
        var str = initials+'';
        var result = str.split(',');
        // alert("legnth:  alredted " + result.length);
        for(var i = 0; i < result.length; i++) {
            // alert( "value " + result[i] );
            $('#editform'+result[i]).hide();
        }
        // for(var i = 1; i <= inc_var; i++) {
        //     $('#editform'+i).hide();
        // }
    }
    function hideAdd() {
        hideForms();
        $('.listDiv').show();
    }
    (function ($, W, D) {
        var JQUERY4U = {};

        JQUERY4U.UTIL =
                {
                    setupFormValidation: function ()
                    {
                        //Additional Methods            
                        $.validator.addMethod("lettersonly", function (a, b) {
                            return this.optional(b) || /^[a-z ]+$/i.test(a)
                        }, "<?php echo $this->lang->line('valid_name'); ?>");

                        //form validation rules
                        $("#document_addform").validate({
                            rules: {
                                status: {
                                    required:true
                                },
                                dcsaadd: {
                                    required:true
                                },
                                dcsa: {
                                    required:true
                                },
                                doc_category: {
                                    required:true
                                },
                                doc_date: {
                                    required:true
                                },
                                city: {
                                    required:true
                                },
                                doc_title: {
                                    required:true
                                }
                            },
                            messages: {
                                status: {
                                    required:"Please select the status"
                                },
                                dcsaadd: {
                                    required:"Please select the one option"
                                },
                                dcsa: {
                                    required:"Please select the one option"
                                },
                                doc_category: {
                                    required:"Please select the category"
                                },
                                doc_date: {
                                    required:"Please select date"
                                },
                                city: {
                                    required:"Please select city"
                                },
                                doc_title: {
                                    required:"Please select document name"
                                }
                            }, 
                            submitHandler: function(form) {
                                form.submit();
                            }
                        });
                    }
                }

        //when the dom has loaded setup form validation rules
        $(D).ready(function ($) {
            JQUERY4U.UTIL.setupFormValidation();
        });

    })(jQuery, window, document);
    
</script>
<style type="text/css">
    .dt-button {
        margin-left: 0px;
        border: 1px solid #999;
        padding: 5px 10px;
        font-size: 12px;
        line-height: 1.5;
        border-radius: 3px;
        color: #333;
        background-color: #fff;
        border-color: #ccc;
        margin-left: 0px;
        border: 1px solid #999;
        text-decoration: none !important;
        background-image: linear-gradient(to bottom, #fff 0, #e0e0e0 100%);
        box-shadow: inset 0 1px 0 rgba(255,255,255,0.15), 0 1px 1px rgba(0,0,0,0.075);
        display: inline-block;
        margin-bottom: 0;
        font-weight: normal;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        user-select: none;
    }
</style>
<section id="content">
    <div class="listDiv">
        <?php $this->load->view('admin/common/breadcrumbs'); ?>
        <div class="row-fluid">
            <?php   $flashAlert = $this->session->flashdata('alert');
                    if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
            ?>
                <br>
                <div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
                    <strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
                    <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php   } ?>

            <div class="module">
                <?php echo $this->session->flashdata('message'); ?>
                <div class="module-head">
                       <div class="module-filter">
                           <div class="col-md-8">
                                <?php echo form_open('clients', array("id" => "search_filter")); ?>
                                    <div class="row  row-no-gutters padding-b">
                                        <!-- <div class="col-md-2">
                                             <input type="text" name="search_name" id="search_name" placeholder="Name" class="input-large form-control" data-name="name">
                                        </div> -->
                                        <!--<div class="col-md-2">
                                             <select class="form-control" data-name="subject">
                                                 <option>All Subject</option>
                                                 <option value="Demande de Rappel ecab.app">Demande de Rappel ecab.app</option>
                                                 <option selected="" value="Demande de Devis ecab.app">Demande de Devis ecab.app</option>
                                                 <option value="Candidature Chauffeur ecab.app">Candidature Chauffeur ecab.app</option>
                                             </select>
                                        </div>-->
                                        <!-- <div class="col-md-2">
                                             <input type="text" name="search_email" id="search_email" placeholder="Email" data-name="email" class="form-control">
                                        </div>
                                        <div class="col-md-2">
                                             <input type="text" name="search_phone" id="search_phone" placeholder="Phone" data-name="phone" class="form-control">
                                        </div> -->
                                        <div class="col-md-1">
                                            <input type="text" name="date_from" id="date_from" placeholder="From" data-name="date_from" class="dpo">
                                        </div>
                                        <div class="col-md-1">
                                            <input type="text" name="date_to" id="date_to" placeholder="To" data-name="date_to" class="dpo">
                                        </div>
                                        <div class="col-md-1">
                                            <select class="form-control" name="status" id="status" data-name="status">
                                                 <option value="">All Status</option>
                                                 <option value="1">Active</option>
                                                 <option value="0">Inactive</option>
                                             </select>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="submit" name="search" id="search" value="Search" class="btn btn-sm btn-default" />
                                        </div>
                                     </div>
                                <?php echo form_close(); ?>
                            </div>
                           <!-- <div class="col-md-4 no-right-pad">
                               <div class="dt-buttons">
                                   <a href="javascript:;" class="btn btn-sm btn-default" onclick="showAdd();"><i class="fa fa-plus"></i> Add</a> 
                                   <a href="Javascript:;" class="btn btn-sm btn-default editBtn" onclick="return editClick();"><i class="fa fa-pencil"></i> Edit</a> 
                                   <a href="Javascript:;" class="btn btn-sm btn-default delBtn" onclick="return deleteClick();"><i class="fa fa-trash"></i> Delete</a>
                                   <a href="#" class="btn btn-sm btn-default delBtn"><i class="fa fa-download"></i> Import</a>
                                   <a href="Javascript:;" class="btn btn-sm btn-default excelDownload"><i class="fa fa-download"></i> Excel</a>
                                   <a href="Javascript:;" class="btn btn-sm btn-default csvDownload"><i class="fa fa-download"></i> CSV</a>
                                   <a href="Javascript:;" class="btn btn-sm btn-default pdfDownload"><i class="fa fa-download"></i> PDF</a>
                               </div>
                           </div> -->
                        </div>
                </div>
                <div class="clearfix"></div>
                <div class="module-body table-responsive">
                    <table id="manageDocumentsTable" class="table table-bordered table-striped  table-hover dataTable" cellspacing="0" width="100%" data-selected_id="">
                        <thead>
                            <tr>
                                <th class="no-sort text-center"><input type='checkbox' name='allClients' id="allClients" value='' onclick="Javascript:selectAllClients();" /></th>
                                <th class="column-id"><?php echo $this->lang->line('id'); ?></th>
                                <th class="column-date"><?php echo "Date"; ?></th>
                                <th class="column-time"><?php echo "Time"; ?></th>
                                <th class="column-addedby" ><?php echo "Added By"; ?></th>
                                <th class="column-usercategory" ><?php echo "User Category"; ?></th>
                                <th class="column-username" ><?php echo "Username"; ?></th>
                                <th class="column-doccategory" ><?php echo "Doc Category"; ?></th>
                                <th class="column-docname" ><?php echo "Doc Name"; ?></th>
                                <th class="column-docdate" ><?php echo "Doc Date"; ?></th>
                                <th class="column-view"><?php echo $this->lang->line('view'); ?></th>
                                <th class="column-status"><?php echo $this->lang->line('status'); ?></th>
                                <th class="column-since"><?php echo "Since"; ?></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="addDiv" style="display:none;">
        <?php $this->data['show_subtitle'] = true ; $this->load->view('admin/common/breadcrumbs'); ?>
        <div class="row-fluid">
            <?php   $flashAlert = $this->session->flashdata('alert');
                    if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
            ?>
                <br>
                <div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
                    <strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
                    <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php   } ?>
        </div>
        <?php echo form_open('documents/add', array("id" => "document_addform")); ?>
        <div class="row">
            <div class="col-md-12">
                <!-- <div class="row">
                    <div class="col-md-2 form-group"> 
                        <label> Civility </label>                                
                        <select class="form-control" name="civility" tabindex="3">
                            <option value="Mr">Mr</option>
                            <option value="Miss">Miss</option>
                            <option value="Mrs">Mrs</option>
                            <option value="Mme">Mme</option>
                        </select>
                    </div>
                </div> -->
                <div class="row" style="margin-top: 100px;">
                    <div class="col-md-4">
                        <label>Statut</label>
                        <select class="form-control"  name="status" id="status" tabindex="6" data-name="status">
                            <option value="Enabled">Enabled</option>
                            <option value="Disabled">Disabled</option>
                         </select>
                    </div>
                    <div class="col-md-4 form-group"> 
                        <label> User Category <img class="displayimageloader" style="float: left; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" /> </label>                                
                        <select class="form-control" name="dcsaadd" id="dcsaadd" tabindex="1" onchange="return onUserGroupChange(this.value,0);">
                            <option value="">Select User Category</option>
                    <?php foreach($groups as $g) { ?>
                            <option value="<?=$g->id;?>"><?=$g->description;?></option>
                    <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-4 form-group"> 
                        <!-- <label>Name</label> -->
                        <label> Username </label>
                        <select class="form-control" name="dcsa" id="dcsa" tabindex="2">
                            <option value="">-- Select User Category First --</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 form-group"> 
                        <label> Document Category </label> 
                        <select class="form-control" name="doc_category" id="doc_category" tabindex="3">
                            <option value="">Select Category</option>
                    <?php foreach($categories as $cat) { ?>
                            <option value="<?=$cat->id;?>"><?=$cat->doc_category;?></option>
                    <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-4 form-group"> 
                        <label>Select Document Name</label>
                        <?php //echo form_input($doc_title); ?>
                        <select class="form-control" name="doc_title" id="doc_title" tabindex="1">
                            <option value="">Select Document Name</option>
                    <?php foreach($docs as $d) { ?>
                            <option value="<?=$d->id;?>"><?=$d->doc_name;?></option>
                    <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-4 form-group">
                        <label> Date </label>
                        <input class="form-control" type="date" name="doc_date" value="<?php echo Date('Y-m-d'); ?>" id="doc_date" style="height: 36px;">
                    </div>
                    <!-- <div class="col-md-4 form-group"> 
                        <label>Select Department</label>
                        <select class="form-control" name="doc_dept" id="doc_dept" tabindex="1">
                            <option value="">Select Department</option>
                    <?php foreach($departments as $d) { ?>
                            <option value="<?=$d->id;?>"><?=$d->dept_name;?></option>
                    <?php } ?>
                        </select>
                    </div> -->
                </div>
                <div class="row">
                    <!-- <div class="col-md-4 form-group"> 
                        <label>Select Manager</label>
                        <select class="form-control" name="doc_manager" id="doc_manager" tabindex="1">
                            <option value="">Select Manager</option>
                    <?php foreach($managers as $m) { ?>
                            <option value="<?=$m->id;?>"><?=$m->manager_name;?></option>
                    <?php } ?>
                        </select>
                    </div> -->
                    <!-- <div class="col-md-4 form-group"> 
                        <label> Citys </label>                                
                        <select class="form-control" name="city" id="city" tabindex="1">
                            <option value="">Select City</option>
                    <?php foreach($cities as $c) { ?>
                            <option value="<?=$c->id;?>"><?=$c->city;?></option>
                    <?php } ?>
                        </select>
                    </div> -->
                </div>
                <!-- <div class="row">
                    <div class="col-md-4 form-group"> 
                        <label> Signature </label>
                        <select class="form-control" name="signature" id="signature" tabindex="1">
                            <option value="">Select Signature</option>
                    <?php foreach($signatures as $s) { ?>
                            <option value="<?=$s->id;?>"><?=$s->doc_signature;?></option>
                    <?php } ?>
                        </select>
                    </div>
                </div> -->
                <!-- <div class="row">
                    <div class="col-md-12">
                        <label>Document Body</label>
                        <textarea class="form-control" name="document_body" id="document_body" rows="20" placeholder="Document Body"></textarea>
                        <textarea id='document_body' name='document_body' style='display:block; height: 0px; width: 0px; margin-top: -60; background: #eee; border: 0px; overflow: hidden;'></textarea>
                    </div>
                </div> -->
            </div>
        </div>
        <div class="row">
            <br />
            <div class="col-md-12"> 
                <div class="pull-right">
                    <!-- <input type="submit" class="btn btn-default" value="Save" tabindex="8" />   -->
                    <!-- <input type="button" class="btn btn-default" value="Cancel" onclick="return hideAdd();" tabindex="9" /> -->
                    <button type="button" class="btn btn-default" tabindex="9" onclick="return hideAdd();"><i class="fa fa-ban"></i>&nbsp;&nbsp;Cancel</button>
                    <button type="submit" class="btn btn-default" tabindex="8"><i class="fa fa-save"></i>&nbsp;&nbsp;Save</button>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
    <div class="editDiv" style="display:none;">
        <?php $this->data['show_subtitle'] = true ; $this->load->view('admin/common/breadcrumbs'); ?>
        <div class="row-fluid">
            <?php   $flashAlert = $this->session->flashdata('alert');
                    if (isset($flashAlert['message']) && !empty($flashAlert['message'])) {
            ?>
                <br>
                <div style="padding: 5px 12px" class="alert <?= $flashAlert['class'] ?>">
                    <strong><?= $flashAlert['type'] ?></strong> <?= $flashAlert['message'] ?>
                    <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php   } ?>
        </div>
    <?php 
        echo form_open('documents/edit', array("id" => "document_editform")); 

        $n = 1;
        foreach ($all_records as $key => $value) {
    ?>
        <div class="row" id="editform<?=$value['did']?>">
            <div class="col-md-12">
                <!-- <div class="row">
                    <div class="col-md-2 form-group"> 
                        <label> Civility </label>                                
                        <select class="form-control" name="civility" tabindex="3">
                            <option value="Mr">Mr</option>
                            <option value="Miss">Miss</option>
                            <option value="Mrs">Mrs</option>
                            <option value="Mme">Mme</option>
                        </select>
                    </div>
                </div> -->
                <div class="row" style="margin-top: 100px;">
                    <div class="col-md-4">
                        <input type="hidden" name="dids[]" value="<?= $value['did']; ?>">
                        <label>Status</label>
                        <select class="form-control"  name="status[]" id="status" tabindex="6" data-name="status">
                            <option value="">All Status</option>
                            <option value="Enabled" <?php if($value['doc_status'] == "Enabled") echo 'selected'; ?>>Enabled</option>
                            <option value="Disabled" <?php if($value['doc_status'] == "Disabled") echo 'selected'; ?>>Disabled</option>
                         </select>
                    </div>
                    <div class="col-md-4 form-group"> 
                        <label> User Category <img class="displayimageloader<?=$n?>" style="float: left; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" /> </label>                                
                        <select class="form-control" name="dcsa1[]" id="dcsa<?=$n?>" tabindex="1" onchange="return onUserGroupChange(this.value,<?=$n?>);">
                            <option value="">Select User Category</option>
                    <?php foreach($groups as $g) { ?>
                            <option value="<?=$g->id;?>" <?php if($value['usergroup'] == $g->id) echo 'selected'; ?>><?=$g->description;?></option>
                    <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-4 form-group"> 
                        <!-- <label>Name</label> -->
                        <label> Username </label>
                        <select class="form-control" name="dcsa[]" id="edcsa<?=$n?>" tabindex="2">
                            <option value="">-- Select User Category First --</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 form-group"> 
                        <label> Document Category </label> 
                        <select class="form-control" name="doc_category[]" id="doc_category" tabindex="3">
                            <option value="">Select Category</option>
                    <?php foreach($categories as $cat) { ?>
                            <option value="<?=$cat->id;?>" <?php if($value['doccategory'] == $cat->id) echo 'selected'; ?>><?=$cat->doc_category;?></option>
                    <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-4 form-group"> 
                        <label>Select Document Name</label>
                        <?php //echo form_input($doc_title); ?>
                        <select class="form-control" name="doc_title[]" id="doc_title" tabindex="1">
                            <option value="">Select Document Name</option>
                    <?php foreach($docs as $d) { ?>
                            <option value="<?=$d->id;?>" <?php if($value['doc_title'] == $d->id) echo 'selected'; ?>><?=$d->doc_name;?></option>
                    <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-4 form-group"> 
                        <label> Current Date / Custom Date </label>
                        <input class="form-control" type="date" name="doc_date[]" value="<?=$value['doc_date'];?>" id="doc_date" style="height: 36px;">
                    </div>
                    <!-- <div class="col-md-4 form-group"> 
                        <label> Citys </label>                                
                        <select class="form-control" name="city[]" id="city" tabindex="1">
                            <option value="">Select City</option>
                    <?php foreach($cities as $c) { ?>
                            <option value="<?=$c->id;?>" <?php if($value['doccity'] == $c->id) echo 'selected'; ?>><?=$c->city;?></option>
                    <?php } ?>
                        </select>
                    </div> -->
                    <!-- <div class="col-md-4 form-group"> 
                        <label>Select Department</label>
                        <select class="form-control" name="doc_dept[]" id="doc_dept" tabindex="1">
                            <option value="">Select Department</option>
                    <?php foreach($departments as $d) { ?>
                            <option value="<?=$d->id;?>" <?php if($value['department'] == $d->id) echo 'selected'; ?>><?=$d->dept_name;?></option>
                    <?php } ?>
                        </select>
                    </div> -->
                </div>
                <div class="row">
                    <!-- <div class="col-md-4 form-group"> 
                        <label>Select Manager</label>
                        <select class="form-control" name="doc_manager[]" id="doc_manager" tabindex="1">
                            <option value="">Select Manager</option>
                    <?php foreach($managers as $m) { ?>
                            <option value="<?=$m->id;?>" <?php if($value['manager'] == $m->id) echo 'selected'; ?>><?=$m->manager_name;?></option>
                    <?php } ?>
                        </select>
                    </div> -->
                </div>
                <!-- <div class="row">
                    <div class="col-md-4 form-group"> 
                        <label> Signature </label>
                        <select class="form-control" name="signature[]" id="signature" tabindex="1">
                            <option value="">Select Signature</option>
                    <?php foreach($signatures as $s) { ?>
                            <option value="<?=$s->id;?>" <?php if($value['signature'] == $s->id) echo 'selected'; ?>><?=$s->doc_signature;?></option>
                    <?php } ?>
                        </select>
                    </div>
                </div> -->
                <!-- <div class="row">
                    <div class="col-md-12">
                        <label>Document Body</label>
                        <textarea class="form-control" name="document_body" id="document_body" rows="20" placeholder="Document Body"></textarea>
                        <textarea id='document_body' name='document_body' style='display:block; height: 0px; width: 0px; margin-top: -60; background: #eee; border: 0px; overflow: hidden;'></textarea>
                    </div>
                </div> -->
            </div>
        </div>
        <script type="text/javascript"> inc_var = <?php echo $n; ?>; initials = <?php echo $value['did']; ?>+","+initials;</script>
    <?php 
            $n++;
        }
    ?>
        <div class="row">
            <br />
            <div class="col-md-12"> 
                <div class="pull-right">
                    <!-- <input type="submit" class="btn btn-default" value="Save" tabindex="8" />   -->
                    <!-- <input type="button" class="btn btn-default" value="Cancel" onclick="return hideAdd();" tabindex="9" /> -->
                    <button type="button" class="btn btn-default" tabindex="9" onclick="return hideAdd();"><i class="fa fa-ban"></i>&nbsp;&nbsp;Cancel</button>
                    <button type="submit" class="btn btn-default" tabindex="8"><i class="fa fa-save"></i>&nbsp;&nbsp;Update</button>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</section>
</div>
<!--
<div class="filter-search" style="display: none;">
    <?php // echo form_open('clients', array("id" => "search_filter")); ?>
    <form action="<?php echo base_url();?>clients" method="post" accept-charset="utf-8" name="search_filter" id="search_filter">
        <div class="row  row-no-gutters padding-b">
            <div class="col-md-2">
                 <input type="text" name="search_name" id="search_name" placeholder="Name" class="input-large form-control" data-name="name">
            </div>
            <div class="col-md-2">
                 <input type="text" name="search_email" id="search_email" placeholder="Email" data-name="email" class="form-control">
            </div>
            <div class="col-md-2">
                 <input type="text" name="search_phone" id="search_phone" placeholder="Phone" data-name="phone" class="form-control">
            </div>
            <div class="col-md-1">
                <input type="text" name="date_from" id="date_from" placeholder="From" data-name="date_from" class="dpo">
            </div>
            <div class="col-md-1">
                <input type="text" name="date_to" id="date_to" placeholder="To" data-name="date_to" class="dpo">
            </div>
            <div class="col-md-1">
                <select class="form-control" name="status" id="status" data-name="status" style="width:100%!important;">
                     <option value="">All Status</option>
                     <option value="1">Active</option>
                     <option value="0">Inactive</option>
                 </select>
            </div>
            <div class="col-md-2">
                <input type="submit" name="search" id="search" value="Search" class="btn btn-sm btn-default" />
            </div>
         </div>
    <?php // echo form_close(); ?>
    </div>
</div>
-->
 <script>
    // initSample();
    function editClick(){
        // alert("edit clicked");
        values = $("input:checkbox:checked")
              .map(function(){return $(this).val();}).get();
        if(values != "") {
            hideForms();
            $('.editDiv').show();
            // alert(values);
            var str = values+'', n = 0;;
            var result = str.split(',');
            // alert("legnth:  " + result.length);
            for(var i = 0; i < result.length; i++) {
                n = i + 1;
                // alert( "value " + result[i] );
                $('#editform'+result[i]).show();
            }
        } else {
            alert("Please select at least one record.");
        }
        // window.location.href="<?php echo base_url();?>admin/library/edit.php";
    }

    function deleteClick(){

        values = $("input:checkbox:checked")
              .map(function(){return $(this).val();}).get();

        if(values != "") {

        } else {
            alert("Please select at least one record.");
        }
    }
    $(document).ready(function() {
        // CKEDITOR.replace('editor');
        // $("#document_addform").on("submit", function () {
        //     var hvalue = $('#editor').text();
        //     var data = CKEDITOR.instances.editor.getData();
        //     // $(this).append("<textarea id='document_body' name='document_body' style='display:none'>"+hvalue+"</textarea>");
        //     var removeElements = function(data) {
        //         var wrapped = $("<div>" + data + "</div>");
        //         wrapped.find('script').remove();
        //         return wrapped.html();
        //     }
        //     $("#document_body").val(removeElements);
        //     alert("submittedd " + data);
        // });
        var manageTable = $('#manageDocumentsTable').DataTable({
            'ajax': 'documents/fetchDocumentsData',
            'order': [],
            'paging': true,
            'pageLength': 15,
            'columnDefs': [ { "targets": 0, "className": "text-center", "width": "4%" },
                            { "targets": 1, "className": "text-center", "width": "4%" },
                            { "targets": 2, "className": "text-center", "width": "4%" },
                            { "targets": 3, "className": "text-center", "width": "4%" },
                            { "targets": 4, "className": "text-center", "width": "10%" },
                            { "targets": 5, "className": "text-center", "width": "8%" },
                            { "targets": 6, "className": "text-center", "width": "4%" },
                            { "targets": 7, "className": "text-center", "width": "5%" },
                            { "targets": 8, "className": "text-center", "width": "5%" },
                            { "targets": 9, "className": "text-center", "width": "5%" },
                            { "targets": 10, "className": "text-center", "width": "5%" },
                            { "targets": 11, "className": "text-center", "width": "5%" },
                            { "targets": 12, "className": "text-center", "width": "8%" }
                          ] ,
            dom: 'Bfrtip',
            buttons: [
                {
                    text: '<i class="fa fa-plus"></i>&nbsp;Add',
                    action: function (e, dt, node, config) {
                        
                        hideForms();
                        $('.addDiv').show();
                        // alert("button working");
                    }
                },
                {
                    text: '<i class="fa fa-pencil"></i>&nbsp;Edit',
                    action: function (e, dt, node, config) {
                        editClick();
                        // alert("editing working");
                    }
                },
                {
                    text: '<i class="fa fa-trash"></i>&nbsp;Delete',
                    action: function (e, dt, node, config) {
                        deleteClick();
                        // alert("deleting working");
                    }
                },
                {
                    extend:    'excelHtml5',
                    text:      '<i class="fa fa-download"></i>&nbsp;Excel',
                    titleAttr: 'Excel'
                },
                {
                    extend:    'csvHtml5',
                    text:      '<i class="fa fa-download"></i>&nbsp;CSV',
                    titleAttr: 'CSV'
                },
                {
                    extend:    'pdfHtml5',
                    text:      '<i class="fa fa-download"></i>&nbsp;PDF',
                    titleAttr: 'PDF'
                }
            ]
        });
        
        $(".pdfDownload").on('click',function() {
           alert('PDF Download inprogress...'); 
        });
        $(".csvDownload").on('click',function() {
           alert('CSV Download inprogress...'); 
        });
        $(".excelDownload").on('click',function() {
           // console.log('Excel Download inprogress...'); 
           $.ajax({
                        url: 'clients/clientExcelDownload',
                        type: 'post',
                        data: $("#search_filter").serialize(), // /converting the form data into array and sending it to server
                        dataType: 'json',
                        success: function (response) {
                            //console.log(response.data);
                        }
                 });
        });
        
        // var client_html = "<div class='filter-search col-md-8'>  <form action='' method='post' accept-charset='utf-8' name='search_filter' id='search_filter'> <div class='row  row-no-gutters padding-b'> <div class='col-md-2'> <input type='text' name='search_name' id='search_name' placeholder='Name' class='input-large form-control' data-name='name'> </div> <div class='col-md-2'> <input type='text' name='search_email' id='search_email' placeholder='Email' data-name='email' class='form-control'> </div> <div class='col-md-2'> <input type='text' name='search_phone' id='search_phone' placeholder='Phone' data-name='phone' class='form-control'> </div> <div class='col-md-1'> <input type='text' name='date_from' id='date_from' placeholder='From' data-name='date_from' class='dpo'> </div> <div class='col-md-1'> <input type='text' name='date_to' id='date_to' placeholder='To' data-name='date_to' class='dpo'> </div> <div class='col-md-1'> <select class='form-control' name='status' id='status' data-name='status' style='width:100%!important;'> <option value=''>All Status</option> <option value='1'>Active</option> <option value='0'>Inactive</option> </select> </div> <div class='col-md-2'> <input type='submit' name='search' id='search' value='Search' class='btn btn-sm btn-default' /> </div> </div> </form> </div>";
        // $('.dt-buttons').before(client_html);
        // $('.buttons-excel').before('<a href="javascript:;" class="btn btn-sm btn-default" onclick="showAdd();"><i class="fa fa-plus"></i> Add</a> ');
        
        // Search form validation rules
        $("#search_filter").validate({
            rules: {
                search_name: { required:false }
            },
            messages: {
                search_name: {
                    required:"Please enter the name"
                }
            },
            submitHandler: function(form) {
                $.ajax({
                        url: 'clients/clientSearchData',
                        type: 'post',
                        data: $("#search_filter").serialize(), // /converting the form data into array and sending it to server
                        dataType: 'json',
                        success: function (response) {
                            console.log(response.data);
                            //  $("#manageTable tbody").empty();
                            //if (response.success === true) {
                            console.log("Table" + $.fn.dataTable.isDataTable('#manageDocumentsTable'));

                            if ($.fn.dataTable.isDataTable('#manageDocumentsTable') == false) {
                                $('#manageDocumentsTable').DataTable({
                                    'data': response.data,
                                    'order': [],
                                    'paging': true,
                                    'pageLength': 15,
                                    'columnDefs': [ { "targets": 0, "className": "text-center", "width": "4%" },
                                                    { "targets": 1, "className": "text-center", "width": "4%" },
                                                    { "targets": 2, "className": "text-center", "width": "4%" },
                                                    { "targets": 3, "className": "text-center", "width": "4%" },
                                                    { "targets": 4, "className": "text-center", "width": "10%" },
                                                    { "targets": 5, "className": "text-center", "width": "8%" },
                                                    { "targets": 6, "className": "text-center", "width": "4%" },
                                                    { "targets": 7, "className": "text-center", "width": "5%" },
                                                    { "targets": 8, "className": "text-center", "width": "5%" },
                                                    { "targets": 9, "className": "text-center", "width": "5%" },
                                                    { "targets": 10, "className": "text-center", "width": "5%" },
                                                    { "targets": 11, "className": "text-center", "width": "5%" },
                                                    { "targets": 12, "className": "text-center", "width": "8%" }
                                                  ]
                                });
                            } else {
                                var table = $('#manageDocumentsTable').DataTable();
                                table.destroy();
                                $('#manageDocumentsTable').DataTable({
                                    'data': response.data,
                                    'order': [],
                                    'paging': true,
                                    'pageLength': 15,
                                    'columnDefs': [ { "targets": 0, "className": "text-center", "width": "4%" },
                                                    { "targets": 1, "className": "text-center", "width": "4%" },
                                                    { "targets": 2, "className": "text-center", "width": "4%" },
                                                    { "targets": 3, "className": "text-center", "width": "4%" },
                                                    { "targets": 4, "className": "text-center", "width": "10%" },
                                                    { "targets": 5, "className": "text-center", "width": "8%" },
                                                    { "targets": 6, "className": "text-center", "width": "4%" },
                                                    { "targets": 7, "className": "text-center", "width": "5%" },
                                                    { "targets": 8, "className": "text-center", "width": "5%" },
                                                    { "targets": 9, "className": "text-center", "width": "5%" },
                                                    { "targets": 10, "className": "text-center", "width": "5%" },
                                                    { "targets": 11, "className": "text-center", "width": "5%" },
                                                    { "targets": 12, "className": "text-center", "width": "8%" }
                                                  ]
                                });
                            }
                        }
                    })
            }
        });
    });

    
    function onUserGroupChange(v,t) {
        // var v = $("#dcsa1").val();
        // alert("working " + v + " " + t);
        if(t == 0) {
            $(".displayimageloader").show();
        } else {
            $(".displayimageloader"+t).show();
        }

        $.ajax({
            url: "documents/getUsers",
            type: 'get',
            data: {user_cat : v}, // /converting the form data into array and sending it to server
            success: function (response) {
                if(response) {
                    if(t == 0) {
                        $('#dcsa').find('option').remove();
                        $('#dcsa').append(response);
                        $(".displayimageloader").hide();
                    } else {
                        $('#edcsa'+t).find('option').remove();
                        $('#edcsa'+t).append(response);
                        $(".displayimageloader"+t).hide();
                    }
                }
                // alert("yes working " + response);
            }
        });
    }

    $(document).ready(function() {

        // $("#dcsa1").on('change', function(){

        //     var v = $("#dcsa1").val();
        //     // alert("working " + v);

        //     $.ajax({
        //         url: "documents/getUsers",
        //         type: 'get',
        //         data: {user_cat : v}, // /converting the form data into array and sending it to server
        //         success: function (response) {
        //             if(response) {
        //                 $('#dcsa').find('option').remove()
        //                 $('#dcsa').append(response);
        //             }
        //             // alert("yes working " + response);
        //         }
        //     });
        // });
    });
</script>