<?php $locale_info = localeconv(); ?>
<link href="<?php echo base_url(); ?>assets/system_design/css/simple-rating.css" rel="stylesheet">
<div class="col-md-12" style="height: calc(100vh - 140px)"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12">
                <?php $this->load->view('admin/common/alert');?>
                <div class="module">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="module-head">
                    </div>
                    <div class="module-body">
                        <?=form_open_multipart("admin/documents/store_document")?>
                        <div class="row">
                            <div class="col-xs-2">
                                <div class="form-group">
                                    <label>Statut *</label>
                                    <select name="doc_statut" id="doc_statut" class="form-control" required>
                                        <option value="1">Enabled</option>
                                        <option value="2">Disabled</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-xs-2">
                                <div class="form-group">
                                    <label>User Category *</label>
                                    <select name="module" class="form-control" id="select_module" required >
                                        <option value="">---Select---</option>
                                        <?php foreach($modules as $key => $value){?>
                                            <option value="<?= $value['id']; ?>"><?= ucfirst($value['name']);?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="form-group">
                                    <label>User Name </label>
                                    <select name="name" class="form-control" id="username">
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="form-group">
                                   <label>Document Name</label>
                                   <input type="text" maxlength="100" class="form-control" required name="doc_name" placeholder="Name" value="">
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="form-group">
                                   <label>Date</label>
                                   <input type="text" maxlength="100" class="form-control" required name="doc_date" id="doc_date" value="<?=date("d/m/Y");?>" style="height: 34px;">
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="form-group">
                                   <label>Custom Department</label>
                                   <input type="text" maxlength="100" class="form-control" name="custom_department"  placeholder="Custom Department" value="" style="height: 34px;" required>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-xs-2">
                                <div class="form-group">
                                   <label>Custom User</label>
                                   <input type="text" maxlength="100" class="form-control" name="custom_user" value="" placeholder="Custom User" style="height: 34px;" required>
                                </div>
                            </div>
                            <div class="col-md-10" style="margin-top: 25px;">
                                <div class="form-group">
                                    <div class="text-right">
                                         <a href="<?=base_url("admin/documents")?>" class="btn btn-default"><span class="fa fa-close" style="margin-right: 3px;"></span>Cancel</a>
                                        <button class="btn btn-default"><span class="fa fa-save"></span>Save</button>
                                       
                                    </div>
                                </div>    
                            </div>  
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/simple-rating.js"></script>

<script type="text/javascript">
   function modulechange(obj) {
       var id = obj.value;
       var post_url = '<?=base_url();?>admin/documents/get_users_against_category';
       $.ajax({
            url:post_url,
            type:'POST',
            data:{'id':id},
            dataType:'json',
            success:function(res){
                alert(res);
            }
       })
   }
   $('#select_module').change(function(){
       // event.preventDefault(); 
       var id = $(this).val();
       $.ajax({
            type: "post",
            cache: false,
            url: "<?php echo base_url("admin/documents/get_users_against_category"); ?>",
            data:{
                'id':id
            },
            success:function(res){
                
            }
       })
   });
</script>