<?php $locale_info = localeconv(); ?>
<link href="<?php echo base_url(); ?>assets/system_design/css/simple-rating.css" rel="stylesheet">
<div class="col-md-12" style="height: calc(100vh - 140px);"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12">
                <?php $this->load->view('admin/common/alert');?>
                <div class="module">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="module-head">
                    </div>
                    <div class="module-body">
                        <?=form_open("admin/documents/".$data->id."/update_document")?>
                        <input type="hidden" name="id" value="<?= $document['id'];?>">
                        <div class="row">
                            <div class="col-xs-2">
                                <div class="form-group">
                                    <label>Statut *</label>
                                    <select name="doc_statut" class="form-control" required >
                                        <option value="">---Select---</option>
                                        <option <?php if($document['doc_statut'] == 1){echo 'selected';} ?> value="1">Enabled</option>
                                        <option <?php if($document['doc_statut'] == 2){echo 'selected';} ?> value="2">Disabled</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="form-group">
                                    <label>User Category *</label>
                                    <select name="module" id="department" class="form-control" required onchange="modulechange()">
                                        <option value="">---Select---</option>
                                        <option <?php if($document['module'] == 1){echo 'selected';} ?> value="1">Job Applications</option>
                                        <option <?php if($document['module'] == 2){echo 'selected';} ?> value="2">Quotes</option>
                                        <option <?php if($document['module'] == 3){echo 'selected';} ?> value="3">Calls</option>
                                        <option <?php if($document['module'] == 4){echo 'selected';} ?> value="4">Invoices</option>
                                        <option <?php if($document['module'] == 5){echo 'selected';} ?> value="5">Support</option>
                                        <option <?php if($document['module'] == 6){echo 'selected';} ?> value="6">Booking</option>
                                    </select>
                                </div>
                            </div>
                             <div class="col-xs-2">
                                <div class="form-group">
                                    <label>User Name </label>
                                    <input type="text" maxlength="100" class="form-control" required name="name" placeholder="Name*" value="<?=$document['name']?>">
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="form-group">
                                    <label>Document Name </label>
                                    <input type="text" maxlength="100" class="form-control" required name="doc_name" placeholder="Name" value="<?=$document['doc_name']?>">
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="form-group">
                                    <label>Date</label>
                                    <input type="text" maxlength="100" class="form-control" required name="doc_date" placeholder="Name" value="<?=$document['doc_date']?>"  style="height: 34px;">
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="form-group">
                                    <label>Custom Department</label>
                                    <input type="text" maxlength="100" class="form-control" required name="custom_department" placeholder="Name" value="<?=$document['custom_department']?>"  style="height: 34px;">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-2">
                                <div class="form-group">
                                    <label>Custom User</label>
                                    <input type="text" maxlength="100" class="form-control" required name="custom_user" placeholder="Name" value="<?=$document['custom_user']?>"  style="height: 34px;">
                                </div>
                            </div>
                            <div class="col-md-10" style="margin-top: 25px;">
                                <div class="text-right">
                                    <a href="<?=base_url("admin/documents")?>" class="btn btn-default"><span class="fa fa-close" style="margin-right: 3px;"></span>Cancel</a>
                                    <button class="btn btn-default"><span class="fa fa-save"></span>Save</button>
                                  
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/simple-rating.js"></script>