<div class="col-md-12" style="margin-top: -22px;">
    <div class="col-md-3">
        <div class="chart-outside" style="margin-top: 20px;">
            <h3 class="handle"><span><i class="fa fa-paper-plane"></i></span> <?php echo $this->lang->line('quote_requests');?></h3>
            <div class="charts myChart_g1">
                <canvas id="myChart" width="200px" height="200px"></canvas>
            </div>
            <div class="charts pie-chart_g1" style="display: none;">
                <canvas id="pie-chart" width="200px" height="200px"></canvas>
            </div>
            <div class="charts line-chart_g1" style="display: none;">
                <canvas id="line-chart" width="200px" height="200px"></canvas>
            </div>
            <a class="btn btn-default btn-mar font-13 default_g1">Default</a>
            <a class="btn btn-default btn-mar font-13 evolution_g1">Evolution</a>
            <a class="btn btn-default btn-mar font-13 composition_g1">Composition</a>
            <a class="btn btn-default btn-mar font-13">PDF</a>
            <a href="<?=base_url("admin/quote_requests")?>" class="btn btn-default btn-mar font-13">Quotes Requests</a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="chart-outside" style="margin-top: 20px;">
            <h3 class="handle"><span><i class="fa fa-cab"></i></span> <?php echo "Call Back Requests";?></h3>
            <div class="charts myChart_g2">
                <canvas id="myChart1" width="200px" height="200px"></canvas>
            </div>
            <div class="charts pie-chart_g2" style="display: none;">
                <canvas id="pie-chart2" width="200px" height="200px"></canvas>
            </div>
            <div class="charts line-chart_g2" style="display: none;">
                <canvas id="line-chart1" width="200px" height="200px"></canvas>
            </div>
            <a class="btn btn-default btn-mar font-13 default_g2">Default</a>
            <a class="btn btn-default btn-mar font-13 evolution_g2">Evolution</a>
            <a class="btn btn-default btn-mar font-13 composition_g2">Composition</a>
            <a class="btn btn-default btn-mar font-13">PDF</a>
            <a href="<?=base_url("admin/calls")?>" class="btn btn-default btn-mar font-13">Call Back Requests</a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="chart-outside" style="margin-top: 20px;">
            <h3 class="handle"><span><i class="fa fa-paper-plane"></i></span> <?php echo $this->lang->line('job_applications');?></h3>
            <div class="charts myChart_g3">
                <canvas id="myChart2" width="200px" height="200px"></canvas>
            </div>
            <div class="charts pie-chart_g3" style="display: none;">
                <canvas id="pie-chart3" width="200px" height="200px"></canvas>
            </div>
            <div class="charts line-chart_g3" style="display: none;">
                <canvas id="line-chart2" width="200px" height="200px"></canvas>
            </div>
            <a class="btn btn-default btn-mar font-13 default_g3">Default</a>
            <a class="btn btn-default btn-mar font-13 evolution_g3">Evolution</a>
            <a class="btn btn-default btn-mar font-13 composition_g3">Composition</a>
            <a class="btn btn-default btn-mar font-13">PDF</a>
            <a href="<?=base_url("admin/applications")?>" class="btn btn-default btn-mar font-13">Job Applications</a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="chart-outside" style="margin-top: 20px;">
            <h3 class="handle"><span><i class="fa fa-headphones"></i></span> <?php echo $this->lang->line('support_tickets');?></h3>
            <div class="charts myChart_g4">
                <canvas id="myChart3" width="200px" height="200px"></canvas>
            </div>
            <div class="charts pie-chart_g4" style="display: none;">
                <canvas id="pie-chart4" width="200px" height="200px"></canvas>
            </div>
            <div class="charts line-chart_g4" style="display: none;">
                <canvas id="line-chart3" width="200px" height="200px"></canvas>
            </div>
            <a class="btn btn-default btn-mar font-13 default_g4">Default</a>
            <a class="btn btn-default btn-mar font-13 evolution_g4">Evolution</a>
            <a class="btn btn-default btn-mar font-13 composition_g4">Composition</a>
            <a class="btn btn-default btn-mar font-13">PDF</a>
            <a href="<?=base_url("admin/supports")?>" class="btn btn-default btn-mar font-13">Support Tickets</a>
        </div>
    </div>
</div>

<script>
    var data = {
        datasets: [{
            data: [
                <?=isset($request['new']) ? $request['new'] : 0?>,
                <?=isset($request['pending']) ? $request['pending'] : 0?>,
                <?=isset($request['replied']) ? $request['replied'] : 0?>,
                <?=isset($request['closed']) ? $request['closed'] : 0?>
            ],
            backgroundColor: [
                "#e67e22",
                "#e74c3c",
                "#2ecc71",
                "#95a5a6"

            ],
            label: 'My dataset' // for legend
        }],
        labels: [
            "New",
            "Pending",
            "Replied",
            "Closed"
        ]

    };

    var pieOptions = {
        legend: {
            display: true,
            position: "top",
            labels: {
                fontSize: 12
            }
        },
        events: false,
        animation: {
            duration: 500,
            easing: "easeOutQuart",
            onComplete: function () {
                var ctx = this.chart.ctx;
                ctx.font = Chart.helpers.fontString(16, 'bold', Chart.defaults.global.defaultFontFamily);
                ctx.textAlign = 'center';
                ctx.textBaseline = 'bottom';

                this.data.datasets.forEach(function (dataset) {

                    for (var i = 0; i < dataset.data.length; i++) {
                        var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                            total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                            mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
                            start_angle = model.startAngle,
                            end_angle = model.endAngle,
                            mid_angle = start_angle + (end_angle - start_angle)/2;

                        var x = mid_radius * Math.cos(mid_angle);
                        var y = mid_radius * Math.sin(mid_angle);

                        ctx.fillStyle = '#fff';
                        if (i == 3){ // Darker text color for lighter background
                            ctx.fillStyle = '#444';
                        }
                        var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
                        ctx.fillText(dataset.data[i], model.x + x, model.y + y);
                        // Display percent in another line, line break doesn't work for fillText
                        ctx.fillText(percent, model.x + x, model.y + y + 15);
                    }
                });
            }
        }
    };

    var pieChartCanvas = $("#pie-chart");
    var pieChart1 = new Chart(pieChartCanvas, {
        type: 'pie', // or doughnut
        data: data,
        options: pieOptions

    });
   //console.log(pieChart1)

    // Second Pie Chart

    var data = {
        datasets: [{
            data: [
                <?=isset($calls['new']) ? $calls['new'] : 0?>,
                <?=isset($calls['pending']) ? $calls['pending'] : 0?>,
                <?=isset($calls['replied']) ? $calls['replied'] : 0?>,
                <?=isset($calls['closed']) ? $calls['closed'] : 0?>

            ],
            backgroundColor: [
                "#e67e22",
                "#e74c3c",
                "#2ecc71",
                "#95a5a6"

            ],
            label: 'My dataset' // for legend
        }],
        labels: [
            "New",
            "Pending",
            "Replied",
            "Closed"
        ]
    };

    var pieOptions = {
        legend: {
            display: true,
            fullWidth: true,
            position: "top",
            align: "start",
            labels: {
                fontSize: 12
            }
        },
        events: false,
        animation: {
            duration: 500,
            easing: "easeOutQuart",
            onComplete: function () {
                var ctx = this.chart.ctx;
                ctx.font = Chart.helpers.fontString(16, 'bold', Chart.defaults.global.defaultFontFamily);
                ctx.textAlign = 'center';
                ctx.textBaseline = 'bottom';

                this.data.datasets.forEach(function (dataset) {

                    for (var i = 0; i < dataset.data.length; i++) {
                        var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                            total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                            mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
                            start_angle = model.startAngle,
                            end_angle = model.endAngle,
                            mid_angle = start_angle + (end_angle - start_angle)/2;

                        var x = mid_radius * Math.cos(mid_angle);
                        var y = mid_radius * Math.sin(mid_angle);

                        ctx.fillStyle = '#fff';
                        if (i == 3){ // Darker text color for lighter background
                            ctx.fillStyle = '#444';
                        }
                        var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
                        ctx.fillText(dataset.data[i], model.x + x, model.y + y);
                        // Display percent in another line, line break doesn't work for fillText
                        ctx.fillText(percent, model.x + x, model.y + y + 15);
                    }
                });
            }
        }
    };

    var pieChartCanvas = $("#pie-chart2");
    var pieChart2 = new Chart(pieChartCanvas, {
        type: 'pie', // or doughnut
        data: data,
        options: pieOptions
    });


    //Third Pie Chart
    var data = {
        datasets: [{
            data: [
                <?=isset($jobs['new']) ? $jobs['new'] : 0?>,
                <?=isset($jobs['pending']) ? $jobs['pending'] : 0?>,
                <?=isset($jobs['meeting']) ? $jobs['meeting'] : 0?>,
                <?=isset($calls['accepting']) ? $jobs['accepting'] : 0?>,
                <?=isset($jobs['denied']) ? $jobs['denied'] : 0?>
            ],
            backgroundColor: [
                "#e67e22",
                "#e74c3c",
                "#2ecc71",
                "#95a5a6",
                "#36A2EB"
            ],
            label: 'My dataset' // for legend
        }],
        labels: [
            "New",
            "Pending",
            "Meeting",
            "Accepted",
            "Denied"
        ]
    };

    var pieOptions = {
        legend: {
            display: true,
            position: "top",
            labels: {
                fontSize: 12
            }
        },
        events: false,
        animation: {
            duration: 500,
            easing: "easeOutQuart",
            onComplete: function () {
                var ctx = this.chart.ctx;
                ctx.font = Chart.helpers.fontString(16, 'bold', Chart.defaults.global.defaultFontFamily);
                ctx.textAlign = 'center';
                ctx.textBaseline = 'bottom';

                this.data.datasets.forEach(function (dataset) {

                    for (var i = 0; i < dataset.data.length; i++) {
                        var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                            total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                            mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
                            start_angle = model.startAngle,
                            end_angle = model.endAngle,
                            mid_angle = start_angle + (end_angle - start_angle)/2;

                        var x = mid_radius * Math.cos(mid_angle);
                        var y = mid_radius * Math.sin(mid_angle);

                        ctx.fillStyle = '#fff';
                        if (i == 3){ // Darker text color for lighter background
                            ctx.fillStyle = '#444';
                        }
                        var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
                        ctx.fillText(dataset.data[i], model.x + x, model.y + y);
                        // Display percent in another line, line break doesn't work for fillText
                        ctx.fillText(percent, model.x + x, model.y + y + 15);
                    }
                });
            }
        }
    };

    var pieChartCanvas = $("#pie-chart3");
    var pieChart3 = new Chart(pieChartCanvas, {
        type: 'pie', // or doughnut
        data: data,
        options: pieOptions
    });
    
    //Fourth Pie Chart

    var data = {
        datasets: [{
            data: [
                <?=isset($support['new']) ? $support['new'] : 0?>,
                <?=isset($support['pending']) ? $support['pending'] : 0?>,
                <?=isset($support['replied']) ? $support['replied'] : 0?>,
                <?=isset($support['closed']) ? $support['closed'] : 0?>
            ],
            backgroundColor: [
                "#e67e22",
                "#e74c3c",
                "#2ecc71",
                "#95a5a6"
            ],
            label: 'My dataset' // for legend
        }],
        labels: [
            "New",
            "Pending",
            "Replied",
            "Closed",
        ]
    };

    var pieOptions = {
        legend: {
            display: true,
            position: "top",
            labels: {
                fontSize: 12
            }
        },
        events: false,
        animation: {
            duration: 500,
            easing: "easeOutQuart",
            onComplete: function () {
                var ctx = this.chart.ctx;
                ctx.font = Chart.helpers.fontString(16, 'bold', Chart.defaults.global.defaultFontFamily);
                ctx.textAlign = 'center';
                ctx.textBaseline = 'bottom';

                this.data.datasets.forEach(function (dataset) {

                    for (var i = 0; i < dataset.data.length; i++) {
                        var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                            total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                            mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
                            start_angle = model.startAngle,
                            end_angle = model.endAngle,
                            mid_angle = start_angle + (end_angle - start_angle)/2;

                        var x = mid_radius * Math.cos(mid_angle);
                        var y = mid_radius * Math.sin(mid_angle);

                        ctx.fillStyle = '#fff';
                        if (i == 3){ // Darker text color for lighter background
                            ctx.fillStyle = '#444';
                        }
                        var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
                        ctx.fillText(dataset.data[i], model.x + x, model.y + y);
                        // Display percent in another line, line break doesn't work for fillText
                        ctx.fillText(percent, model.x + x, model.y + y + 15);
                    }
                });
            }
        }
    };

    var pieChartCanvas = $("#pie-chart4");
    var pieChart4 = new Chart(pieChartCanvas, {
        type: 'pie', // or doughnut
        data: data,
        options: pieOptions
    });
    
    
    function chartFformSubmit(e){
        e.preventDefault();
        var url = "<?php echo base_url('index.php/request/filterby'); ?>";
        var form = $('#chartFilterForm');
        $.get(url+'?'+form.serialize(),function(res,b,c) {
            const r = JSON.parse(res);
            pieChart1.data.datasets[0].data = r.chart1
            pieChart2.data.datasets[0].data = r.chart2
            pieChart3.data.datasets[0].data = r.chart3
            pieChart4.data.datasets[0].data = r.chart4
            pieChart1.update();
            pieChart2.update();
            pieChart3.update();
            pieChart4.update();
            // console.log(JSON.parse(res));
        });
    }
    
    
    
    
    
    
    
    
    
    
    //First Bar Chart
    $(function() {
        var cData = JSON.parse(`<?php echo $chart_data; ?>`);
        //console.log(cData);
        var ctx = document.getElementById("myChart");
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: cData.label,
                datasets: [{
                    label: 'Quote Requests',
                    data: cData.data,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)',
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                scales: {
                    xAxes: [{
                        ticks: {
                            // maxRotation: 90,
                            // minRotation: 80
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    });
    //Second Bar Chart
    var callData = JSON.parse(`<?php echo $call_chart_data; ?>`);
    var ctx = document.getElementById("myChart1");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: callData.label,
            datasets: [{
                label: 'Call Back Requests',
                data: callData.data,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            scales: {
                xAxes: [{
                    ticks: {
                        // maxRotation: 90,
                        // minRotation: 80
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    //Third Bar Chart
    var jobsData = JSON.parse(`<?php echo $jobs_chart_data; ?>`);
    var ctx = document.getElementById("myChart2");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: jobsData.label,
            datasets: [{
                label: 'Job Applications',
                data: jobsData.data,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            scales: {
                xAxes: [{
                    ticks: {
                        // maxRotation: 90,
                        // minRotation: 80
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
    
    //Fourth Bar Chart
    var callData = JSON.parse(`<?php echo $support_chart_data; ?>`);
    var ctx = document.getElementById("myChart3");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: callData.label,
            datasets: [{
                label: 'Support Tickets',
                data: callData.data,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            scales: {
                xAxes: [{
                    ticks: {
                        // maxRotation: 90,
                        // minRotation: 80
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    //First Line Chart

    var serries = JSON.parse(`<?php echo $qoute_line_data; ?>`);
    //console.log(serries);
    new Chart(document.getElementById("line-chart"), {
        type: 'line',
        data: {
            labels:serries.day,
            datasets: [{

                data:serries.count,
                label: "Quote Requests",
                borderColor: "#3e95cd",
                fill: false
            }

            ]
        },
        options: {
            title: {
                display: false,
                 text: 'Quote Requests'
            }
        }
    });


    //Second Line Chart
    var chartData = JSON.parse(`<?php echo $calls_line_data; ?>`);
    new Chart(document.getElementById("line-chart1"), {
        type: 'line',
        data: {
            labels: chartData.day,
            datasets: [{
                data: chartData.count,
                label: "Call Back Requests",
                borderColor: "#3e95cd",
                fill: false
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'Call Back Requests'
            }
        }
    });


    //Third Line Chart
    var chartDatas = JSON.parse(`<?php echo $jobs_line_data; ?>`);
    new Chart(document.getElementById("line-chart2"), {
        type: 'line',
        data: {
            labels:chartDatas.day,
            datasets: [{
                data: chartDatas.count,
                label: "Jobs Applications",
                borderColor: "#3e95cd",
                fill: false
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'Job Applications'
            }
        }
    });
    
    //Fourth Line Chart
    var chartData = JSON.parse(`<?php echo $support_line_data; ?>`);
    new Chart(document.getElementById("line-chart3"), {
        type: 'line',
        data: {
            labels: chartData.day,
            datasets: [{
                data: chartData.count,
                label: "Support Tickets",
                borderColor: "#3e95cd",
                fill: false
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'Support'
            }
        }
    });

</script>
