<?php 
$locale_info = localeconv(); ?>
<style type="text/css">
    label{font-weight: 600; font-size: 13px;}
    .custom_style_row{margin-top: 30px; margin-bottom: 30px;}
    .table-filter .dpo {
        max-width: 62px;
    }
    .table-filter span {
        margin: 4px 2px 0 3px;
    }
    .table-filter input[type="number"]{
        max-width: 48px;
    }
    .listing th{
        text-align: center;
    }
    .listing td{
        text-align: center;
    }
    @media only screen and (min-width: 1400px){
        .table-filter input, .table-filter select{
            max-width: 6% !important;
        }
        .table-filter select{
            max-width: 85px !important;
        }
        .table-filter .dpo {
            max-width: 90px !important;
        }
    }
</style>
<div class="col-md-12" style="height: calc(100vh - 140px);"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12">
                <?php $this->load->view('admin/common/alert');?>
                <div class="module">
                    <?php
                        echo $this->session->flashdata('message');
                        $validation_erros = $this->session->flashdata('validation_errors');
                        if(!empty($validation_erros)){
                    ?>
                        <div class="form-validation-errors alert alert-danger">
                            <h3 style="font-size: 20px; text-align: center;">Validation Errors</h3>
                            <?php echo $validation_erros; ?>
                        </div>
                    <?php } ?>
                    <div class="module-head">
                    </div>
                    <div class="module-body table-responsive">
                      
                  <table id="example" class="cell-border listing" cellspacing="0" width="100%" data-selected_id="">
                    <thead>
                      <tr>
                        <th class="no-sort text-center">#</th>
                        <th class="column-id"><?php echo $this->lang->line('id');?></th>
                        <th class="column-date">Date</th>
                        <th class="column-date">Time</th>
                        <th class="column-date">Added by</th>
                        <th>Unity</th>                        
                        <th>Symbole</th>                        
                        <th>Default</th>
                        <th>Allow Change In Front</th>
                        <th>Allow Change In Admin</th>
                        <th>Allow Change In Areas</th>
                        <!-- <th>File Name</th> -->
                        <!-- <th>View</th> -->
                        <!-- <th>Edit</th> -->
                        <th>Statut</th>
                        <th>Since</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($unitys as $key => $value){ ?>
                          <?php
                              $timestamp=$value['created_at'];
                              $date = new DateTime($timestamp);
                              $date = $date->format('d/m/Y');
                              $time = new DateTime($timestamp);
                              $time = $time->format('H:i:s');
                          ?>
                        <tr>
                          <td><input type="checkbox" value="<?=$value['id'];?>" data-id="<?=$value['id'];?>" class="checkbox checkboxx singleSelect"></td>
                            <td>
                                <a href="<?=site_url("admin/unitys/".$value['id']."/view")?>">
                                    <?=create_timestamp_uid($value['created_at'],$value['id']);?>
                                </a>
                            </td>
                            <td><?=$date ?></td>
                            <td><?= $time ?></td>
                            <td><?= $value['civility'].' '.$value['first_name'].' '.$value['last_name']; ?></td>
                            <td><?= ucwords($value['unity_name']); ?></td>
                            <td><?= ucwords($value['symbole']); ?></td>
                            <td><?= ucwords($value['default_status']); ?></td>
                            <td> <?= ucwords($value['allow_front_change']); ?> </td>
                            <td> <?= ucwords($value['allow_admin_change']); ?> </td>
                            <td> <?= ucwords($value['allow_areas_change']);?></td>
                            <!-- <td>  </td> -->
                            <!-- <td></td> -->
                            <td>
                              <?php if($value['statut'] == 'hide'){ ?>
                                    <span class="label label-danger">Hided</span>
                              <?php }elseif($value['statut'] == 'published'){ ?>
                                    <span class="label label-success">Published</span>
                              <?php }else{ ?>
                                    <span class="label label-primary">show</span>
                              <?php } ?>
                            </td>
                            <td style="white-space: nowrap"><?=timeDiff($value['created_at']);?></td>
                          </tr>
                      <?php } ?>  
                    </tbody>
                  </table>
                    </div>
                </div>
            </div>
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<div id="table-filter" class="hide">
    <span class="pull-left">Search</span>
    <input type="text" placeholder="Search" data-name="short_code" class="form-control">
    <a href="#" class="btn btn-sm btn-default " onclick="setUpdateAction();"><i class="fa fa-search"></i> Search</a>
</div>
<?php $locale_info = localeconv(); ?>
<style>
    @media only screen and (min-width: 1400px){
        .table-filter input, .table-filter select{
            max-width: 9% !important;
        }
        .table-filter select{
            max-width: 95px !important;
        }
        .table-filter .dpo {
            max-width: 90px !important;
        }
    }
</style>

<script type="text/javascript">
</script>