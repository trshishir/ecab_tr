<?php
$locale_info = localeconv(); ?>
<style type="text/css">
    label{font-weight: 600; font-size: 13px;}
    .custom_style_row{margin-top: 30px; margin-bottom: 30px;}
    input[type=submit], button[type=submit]{
        color: #333;
        cursor: pointer;
        height: 34px;
        font-size: 16px;
        padding: 1px 28px 3px;
        border-radius: 5px;
        text-transform: none;
        font-family: inherit;
        font-weight: 500;
    }
    .body-border{
        min-height: 500px;
    }
</style>
<div class="col-md-12" style="height: calc(100vh - 140px);"><!--col-md-10 padding white right-p-->
    <div class="content">
        <?php $this->load->view('admin/common/breadcrumbs');?>
        <div class="row">
            <div class="col-md-12">
                <?php $this->load->view('admin/common/alert');?>
                <div class="module">
                    <?php
                    echo $this->session->flashdata('message');
                    $validation_erros = $this->session->flashdata('validation_errors');
                    if(!empty($validation_erros)){
                        ?>
                        <div class="form-validation-errors alert alert-danger">
                            <h3 style="font-size: 20px; text-align: center;">Validation Errors</h3>
                            <?php echo $validation_erros; ?>
                        </div>
                    <?php } ?>
                    <div class="module-head">
                    </div>
                    <div class="module-body">
                        <?=form_open_multipart("admin/unitys/update_unity")?>
                        <input type="hidden" name="id" value="<?= ($unity['id'])?>">
                        <div class="col-xs-12">
                            <div class="form-group col-xs-1" style="padding-left: 0px;">
                                <label for="exampleInputEmail1">Statut</label>
                                <select class="form-control" name="statut" required>
                                    <option value="show" <?= ($unity['statut'] == 'show' ? 'selected':'') ?>>Show</option>
                                    <option value="hide" <?= ($unity['statut'] == 'hide' ? 'selected':'') ?>>Hide</option>
                                    <option value="published" <?= ($unity['statut'] == 'published' ? 'selected':'')?>>Published</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-1">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Unity Name</label>
                                <input type="text" class="form-control" name="unity_name" value='<?= ($unity['unity_name'])?>' required>
                            </div>
                        </div>
                         <div class="col-xs-1">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Short Code</label>
                                <input type="text" class="form-control" name="unity_code" value='<?= ($unity['unity_code'])?>' required>
                            </div>
                        </div>
                         <div class="col-xs-1">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Conversion</label>
                                <input type="text" class="form-control" name="conversion" value='<?= ($unity['conversion'])?>' required>
                            </div>
                        </div>
                        <div class="col-xs-1" style="margin-top: 36px;text-align: right;width: 39px;">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mile</label>
                            </div>
                        </div>
                        <div class="col-xs-1">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Symbole</label>
                                <input type="text" class="form-control" name="symbole" value='<?= ($unity['symbole'])?>' required>
                            </div>
                        </div>
                        <div class="col-xs-1">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Default</label>
                                <select class="form-control" id="default_status" name="default_status" required>
                                    <option value="no"<?= ($unity['default_status'] == 'no' ? 'selected':'') ?>>No</option>
                                    <option value="yes"<?= ($unity['default_status'] == 'yes' ? 'selected':'') ?>>Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Allow Front Change</label>
                                <select class="form-control" name="allow_front_change" required>
                                    <option value="no" <?= ($unity['allow_front_change'] == 'no' ? 'selected':'') ?>>No</option>
                                    <option value="yes" <?= ($unity['allow_front_change'] == 'yes' ? 'selected':'') ?>>Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Allow Admin Change</label>
                                <select class="form-control" name="allow_admin_change" required>
                                 <option value="yes" <?= ($unity['allow_admin_change'] == 'yes' ? 'selected':'') ?>>Yes</option>
                                 <option value="no" <?= ($unity['allow_admin_change'] == 'no' ? 'selected':'') ?>>No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Allow Areas Change</label>
                                <select class="form-control" name="allow_areas_change" required>
                                    <option value="no" <?= ($unity['allow_areas_change'] == 'no' ? 'selected':'') ?>>No</option>
                                    <option value="yes" <?= ($unity['allow_areas_change'] == 'yes' ? 'selected':'') ?>>Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 text-right">
                            <div class="form-group">
                                <a href="<?= base_url('admin/unitys'); ?>" class="btn btn-default"><i class="fa fa-times"></i>Cancel</a>
                                <button type="submit" class="btn btn-default"><i class="fa fa-floppy-o"></i> Save</button>
                            </div>
                        </div>
                        <!-- </form> -->
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div><?php $locale_info = localeconv(); ?>
<style>
    @media only screen and (min-width: 1400px){
        .table-filter input, .table-filter select{
            max-width: 9% !important;
        }
        .table-filter select{
            max-width: 95px !important;
        }
        .table-filter .dpo {
            max-width: 90px !important;
        }
    }
</style>
