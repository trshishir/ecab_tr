<?php
$job = $job_detail->row();
// echo "<PRE>";print_r($job);echo "</PRE>";
?>
</header>
<div class="container body-border jobdetail_page"> 
    <div class="breadcrumb">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <aside class="nav-links">
                    <ul>
                        <li> <a href="<?php echo site_url(); ?>"><i class="fa fa-home"></i>   <?php echo $this->lang->line('home_page'); ?>  </a> </li>
                        <li><a href="<?php echo base_url();?>jobs.php">Jobs</a></li>
                        <li class="active"><a href="javascript:void(0)">&nbsp;<?php if (isset($subtitle)) echo $subtitle; ?> </a></li>
                    </ul>
                </aside>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-6 col-md-6 col-xs-12">
            <img src="<?= base_url('uploads/job/' . $job->picture) ?>" class="job_image img-responsive">
        </div>
        <div class="col-lg-6 col-md-6 col-xs-12">
            <div class="detail-block">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="" style="font-weight: 14px;float:left;">
                            <span class="lbl-text">Job Category:</span> <?= $job->jobcategory ?>
                        </div> 
                        <div class="" style="float:right;">
                            <span class="lbl-text">Publish Date:</span> <?= ($job->published_date ) ? date('d/m/Y',strtotime($job->published_date)) . ' ' . $job->published_time : ''; //,  ?>
                            <span class="lbl-text">To Date Time:</span> <?= ($job->to_date != "" ) ? date('d/m/Y',strtotime($job->to_date)) : ''; // $job->to_time ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-x-12">
                        <h2><?= $job->job ?></h2>
                    </div>
                </div>
                <ul class="job_info">
                    <!--<li class=""><span class="pull-left">Job Title</span> <span class="pull-right"><?= $job->job_title ?></span> </li>-->
                    <li class=""><div class="pull-left"><span class="job_attr">Living Place:</span> &nbsp;<?= $job->living_place ?> </div></li>
                    <li class=""><div class="pull-left"><span class="job_attr">Working Place:</span> &nbsp;<?= $job->workingplace ?> </div></li>
                    <li class=""><div class=" pull-left"><span class="job_attr">Type of Contract:</span> &nbsp;<?= $job->typeofcontract ?> </div></li>
                    <li class=""><div class=" pull-left"><span class="job_attr">Nature of Contract:</span> &nbsp;<?= $job->natureofcontract ?> </div></li>
                    <li class=""><div class=" pull-left"><span class="job_attr">Hours Per Month:</span> &nbsp;<?= $job->hourspermonth ?> hours </div></li>
                    <li class=""><div class=" pull-left"><span class="job_attr">Required Experience:</span> &nbsp;<?= $job->requiredexperiance ?> </div></li>
                    <!--<li class=""><span class="pull-left">Published Date</span> <span class="pull-right"><?= $job->published_date, ' ' . $job->published_time ?></span> </li>
                    <li class=""><span class="pull-left">To date Time</span> <span class="pull-right"><?= $job->to_date, ' ' . $job->to_time ?></span> </li>
                    <li class=""><div class=" pull-left"><span class="job_attr">Job Category:</span> &nbsp;<?= $job->jobcategory ?> </div></li>-->
                </ul>
                <p class="apply_now"> <a href="Javascript:;" class="button light_orange_button apply_job">Apply Now</a></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <ul class="nav nav-tabs">
                <li class="nav active"><a data-toggle="tab" href="#desc">Description</a></li>
                <li class="nav"><a data-toggle="tab" href="#living_zone">Living Zone</a></li>
                <li class="nav"><a data-toggle="tab" href="#work_zone">Work Zone</a></li>
                <li class="nav"><a data-toggle="tab" href="#reqd_exp">Required Experience</a></li>
                <li class="nav"><a data-toggle="tab" href="#reqd_qual">Required Qualities</a></li>
                <li class="nav"><a data-toggle="tab" href="#reqd_cert">Required Certificates</a></li>
                <li class="nav"><a data-toggle="tab" href="#reqd_dipl">Required Diplomas</a></li>
                <li class="nav"><a data-toggle="tab" href="#reqd_docs">Required Documents</a></li>
            </ul>
            <div class="tab-content">
              <!-- Show this tab by adding `active` class -->
              <div class="tab-pane fade in active" id="desc">
                    <p><?= $job->description ?> </p>
              </div>
              <div class="tab-pane fade in" id="living_zone">
                    <p>Living Zone</p>
              </div>
              <div class="tab-pane fade in" id="work_zone">
                    <p>Work Zone</p>
              </div>
              <div class="tab-pane fade in" id="reqd_exp">
                    <p>Required Experience</p>
              </div>
              <div class="tab-pane fade in" id="reqd_qual">
                    <p>Required Qualities</p>
              </div>
              <div class="tab-pane fade in" id="reqd_cert">
                    <p>Required Certificate</p>
              </div>
              <div class="tab-pane fade in" id="reqd_dipl">
                    <p>Required Diplomas</p>
              </div>
              <div class="tab-pane fade in" id="reqd_docs">
                    <p>Required Documents</p>
              </div>
            </div>
        </div>
    </div>
    <br/>
</div> 
