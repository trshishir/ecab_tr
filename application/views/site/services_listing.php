</header>

    
<div class="container body-border"> 
    <div class="breadcrumb">
        <div class="row">
            <aside class="nav-links">
                <ul>
                    <li> <a href="<?php echo site_url(); ?>"> <?php echo $this->lang->line('home_page'); ?>  </a> </li>
                    <li class="active"><a href="javascript:void(0)">&nbsp;<?php if (isset($sub_heading)) echo $sub_heading; ?> </a></li>
                </ul>
            </aside>
        </div>
    </div>

            <div class="row">

                <?php
                foreach($services as $item):

                    ?>



                    <div class="col-md-4 col-sm-12 col-xs-12" style="margin-top:15px;">
                        <div class="ribbon">
                          <?php if($item->rubens_new==1):?>

                            <div class="wrap">
                             <div class="ribbon6"><span class="promo-service">NEW</span></div>
                         </div>

                     <?php endif;?>
                     <?php if(($item->rubens_new!=1) && ($item->is_price_start_from==1)):?>
                     <div class="corner-promo-ribbon top-right-corner">

                        <span class="price"><?=$item->start_from_price;?> <i class="fa fa-<?php echo ($company_data['default_front_currency']=='usd')?'usd':'eur' ;?>"></i></span>
                        <span class="startfrom">Start from</span>
                    </div>

                <?php endif;?>

                                 <?php if($item->rubens_promo==1):?>
                                     <!-- <div class="corner-promo-ribbon top-left-promo"><span class="promo-service">Promotion</span></div> -->
                                         <div class="wrap1">
                                             <div class="ribbon5"><span class="promo-service">Promotion</span></div>
                                         </div>
                                 <?php endif;?>


                <?php if(($item->rubens_promo==1) || ($item->rubens_new==1)):?>
                    <div class="plans" style="width:98%; margin-top:3px;margin-bottom:3px; margin-left:3px;">


                        <?php else:?>
                            <div class="plans" style="width:100%; margin-top:0px;">

                            <?php endif;?>


                            <!-- <div class="plans"> -->

                                <div class="plans_head">

                                    <h3 style="margin-left:-11px;"><?=$item->title;?></h3>
                                    <div class="plans_img"><img src="<?php echo base_url();?>uploads/cms/<?php echo $item->image?>" alt="FIAT SCUDO TPMR - Fiat ScudoTpmr" title="FIAT SCUDO TPMR - Fiat ScudoTpmr"></div>
                   
                                        <!--<div class="col-md-12 plan_title" id="plan_title_row">
                                            <p></p>
                                        </div>-->
                                        <div class="description">
                                            <?php

                                            $text= $item->description;
                                            $text= strip_tags($text); 

                                            echo implode(' ', array_slice(explode(' ', $text), 0, 23)).' ...';


                                            ?>                                        </div>
                                            <div class="row"> <!--button-row-->

                                               <div class="learn_button">  

                                                  <!-- <a href="<?php echo base_url();?>handi-pro.php" class="button light_green_button">Learn More</a></div> -->
                                                  <a href="<?php echo base_url("services/{$item->category_link}/{$item->link_url}{$item->link}"); ?>" class="button light_green_button">Know More</a></div>


                                                  <div class="orderbtn"><a href="<?php echo site_url(); ?>booking.php" class="button light_orange_button">Book Now</a></div>
                                              </div>           
                                          </div>


                                      </div>
                                  </div>
                              </div>


                          <?php endforeach;?>   


                      </div>


        </div>
        </div>


    <script src="<?php echo base_url(); ?>assets/system_design/scripts/bookings.js"></script>
    <script type="text/javascript">
        (function ($, W, D)
        {
            var JQUERY4U = {};

            JQUERY4U.UTIL =
            {
                setupFormValidation: function ()
                {
                        //Additional Methods			
                        //form validation rules
                        $("#online_booking_form").validate({
                            rules: {
                                pick_up: {
                                    required: true
                                },
                                drop_of: {
                                    required: true
                                },
                                pick_time: {
                                    required: true
                                }
                            },
                            messages: {
                                pick_up: {
                                    required: "<?php echo $this->lang->line('pick_location_valid'); ?>"
                                },
                                drop_of: {
                                    required: "<?php echo $this->lang->line('drop_location_valid'); ?>"
                                },
                                pick_time: {
                                    required: "<?php echo $this->lang->line('pick_time_valid'); ?>"
                                }
                            },
                            submitHandler: function (form) {
                                form.submit();
                            }
                        });
                    }
                }

        //when the dom has loaded setup form validation rules
        $(D).ready(function ($) {
            JQUERY4U.UTIL.setupFormValidation();
        });

    })(jQuery, window, document);

    $(function () {
        $('.ilike').click(function () {
            var $this = $(this);
            var p1 = $this.data('value');
            var arry = $this.data('arry');
            changeField(p1,arry);
        });
        $('.homepageSlider').bxSlider({ 
            mode: 'fade',
            auto: true,
            pager: true,
            autoControl: false,
            adaptiveHeight: true,
            easing: 'swing',
            responsive: true,
            preloadImages: 'all'
        });
        $(".slider_wrapper .bx-viewport").css("height", '360px');
        $('.testy-bxslider').bxSlider({
            mode: 'fade',
            controls: false,
            auto: true
        });

        $("#dtpicker").datepicker({dateFormat: "<?php echo strtolower($date_elem1); ?>-<?php echo strtolower($date_elem2); ?>-yy"});
        $("#dtpicker1").datepicker({dateFormat: "<?php echo strtolower($date_elem1); ?>-<?php echo strtolower($date_elem2); ?>-yy"});
    });
    function changeField(ss, divID)
    {
        if (ss == "pick_airport") {
            /*$('#local_pick').hide();
             $('#airport_pick').show();
             $('#airport_pick').attr('disabled', false);
             $('#local_pick').attr('disabled', true);
             $('#pick_airport_link').hide();
             $('#pick_type').val('A');
             $('#pick_local_link').show();*/
             showAirportPick();
         }
         else if (ss == "drop_airport") {
            /*$('#local_drop').hide();
             $('#airport_drop').show();
             $('#airport_drop').attr('disabled', false);
             $('#local_drop').attr('disabled', true);
             $('#drop_type').val('A');
             $('#drop_airport_link').hide();
             $('#drop_local_link').show();*/
             showAirportDrop(divID);

         }
         else if (ss == "pick_local") {
            /*$('#local_pick').show();
             $('#airport_pick').hide();
             $('#airport_pick').attr('disabled', true);
             $('#local_pick').attr('disabled', false);
             $('#pick_type').val('L');
             $('#pick_airport_link').show();
             $('#pick_local_link').hide();*/
             showLocalPick();
         }
         else if (ss == 'pick_train') {
            showTrainPick();
        }
        else if (ss == 'pick_hotel') {
            showHotelPick();
        }
        else if (ss == 'pick_park') {
            showParkPick();
        }
        else if (ss == "drop_local") {
            showLocalDrop(divID);
        }
        else if (ss == 'drop_train') {
            showTrainDrop(divID);
        }
        else if (ss == 'drop_hotel') {
            showHotelDrop(divID);
        }
        else if (ss == 'drop_park') {
            showParkDrop(divID);
        }

    }
</script>