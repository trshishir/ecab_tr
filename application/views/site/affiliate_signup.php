<link href="<?php echo base_url(); ?>assets/system_design/css/login.css" rel="stylesheet">
<script type="text/javascript"> 
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: "help-block",
        highlight: function(element) {
            jQuery(element).parent().removeClass('has-success').addClass('has-error');
        },
        unhighlight: function(element) {
            jQuery(element).parent().removeClass('has-error').addClass('has-success');
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "nonMemberType") //not needed on this screen, but keeping placeholder syntax
                error.insertAfter("#selectNonMemberValidationBlock"); //not needed, see above
            else if (element.attr('type') == "checkbox") {
                // error.insertAfter(element);
                error.insertAfter( element.parent("label"));
            }
            else {
                error.appendTo( element.parent("div").parent() );

            }
        }
    });
 (function($,W,D)
 {
    var JQUERY4U = {};
 
    JQUERY4U.UTIL =
    {
        setupFormValidation: function()
        {
            //Additional Methods            
        $.validator.addMethod("lettersonly",function(a,b){return this.optional(b)||/^[a-z ]+$/i.test(a)},"<?php echo $this->lang->line('valid_name');?>");
 
        $.validator.addMethod("phoneNumber", function(uid, element) {
            return (this.optional(element) || uid.match(/^([0-9]*)$/));
        },"<?php echo $this->lang->line('valid_phone_number');?>");
        
 
        $.validator.addMethod("pwdmatch", function(repwd, element) {
            var pwd= $('#password').val();
            return (this.optional(element) || repwd==pwd);
        },"<?php echo $this->lang->line('valid_passwords');?>");
         
        
        //form validation rules
        $("#affiliate_signupform").validate({
                ignore: [], // DON'T IGNORE PLUGIN HIDDEN SELECTS, CHECKBOXES AND RADIOS!!!
                rules: {
                    statut: {
                        required:true
                    },
                    company: {
                        required:  function(){
                                        return $("#statut").val() == "2";
                                  }
                    },
                    first_name: {
                        required: true,
                        lettersonly: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    phone:{
                            required: true,
                            phoneNumber: true,
                            rangelength: [10, 11]
                    },
                    password:{
                            required: true,
                            rangelength: [8, 30]
                    },
                    confirm_password: {
                        required:true,
                        pwdmatch:true
                    },
                    tos: {
                        required: true
                    },
                    pp: {
                        required: true
                    }
                },
                messages: {
                    statut: {
                        required:"Please select the statut"
                    },
                    company: {
                        required:"Please enter the company"
                    },
                    first_name: {
                        required: "<?php echo $this->lang->line('first_name_valid');?>"
                    },
                    email:{
                        required: "<?php echo $this->lang->line('email_valid');?>"
                    },
                    phone:{
                            required: "<?php echo $this->lang->line('phone_valid');?>"
                    },
                    password: {
                        required: "<?php echo $this->lang->line('password_valid');?>"
                    },
                    password_confirm:{
                            required: "<?php echo $this->lang->line('confirm_password_valid');?>"
                    },
                    tos: {
                        required: "You must agree our terms of service"
                    },
                    pp: {
                        required: "You must agree our privacy policy"
                    }
                }, 
                submitHandler: function(form) {
                    form.submit();
                }
            });
        }
    }
 
    //when the dom has loaded setup form validation rules
    $(D).ready(function($) {
        JQUERY4U.UTIL.setupFormValidation();
    });
     
 })(jQuery, window, document);
</script>
</header>
<style>
    .slider_wrapper {
        height:530px !important;
    }
    .slider_wrapper img {
        height: 545px !important;
    }
    select { margin-top:0px !important; }
    select.form-control, input[type="password"] { height:34px !important;} 
</style>
  
 
<div class="container body-border"> 
    <div class="breadcrumb">
        <div class="row">
            <aside class="nav-links">
                <ul>
                    <li><a href="<?php echo site_url(); ?>/"><i class="fa fa-home"></i>  <?php echo $this->lang->line('home_page');?> </a></li>
                    <li class="active"><a href="javascript:void(0)"><?php echo $title ?> </a></li>
                </ul>
            </aside>
        </div> 
    </div>
    <div class="clearfix"></div>  
    <div class="row">
        <div class="col-md-6">
            <?php echo form_open('affiliate/signup', array("method"=>"post", "name"=>"affiliate_signupform","id"=>"affiliate_signupform"));?>
            <div class="row">
                <div class="col-md-6 form-group">  
                    <label> Statut </label>                                
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user-md"></i></span>
                        <select class="form-control" name="statut" id="statut">
                            <option value="1">Independent</option>
                            <option value="2">Company</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 form-group"> 
                    <div class="divCompany" style="display:none;">
                        <label>Company</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <?php echo form_input($company); ?>
                            <?php echo form_error('company'); ?>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 form-group"> 
                    <label> Civility </label>                                
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <select class="form-control" name="civility">
                            <option value="Mr">Mr</option>
                            <option value="Mrs">Mrs</option>
                        </select> 
                    </div>
                </div>
                <div class="col-md-5 form-group"> 
                    <label>First Name</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <?php echo form_input($first_name); ?>
                        <?php echo form_error('first_name'); ?> 
                    </div>
                </div>
                <div class="col-md-4 form-group"> 
                    <label>Last Name</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <?php echo form_input($last_name); ?>
                        <?php echo form_error('last_name'); ?> 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group"> 
                    <label>Email</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <?php echo form_input($email); ?>
                        <?php echo form_error('email'); ?> 
                    </div>
                </div>
                <div class="col-md-6 form-group"> 
                    <label>Phone</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-phone-square"></i></span>
                        <?php echo form_input($phone); ?>
                        <?php echo form_error('phone'); ?> 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group"> 
                    <label>Address</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                        <?php echo form_textarea($address); ?>
                        <?php echo form_error('address'); ?> 
                    </div>
                </div>
                <div class="col-md-6 form-group"> 
                    <label>&nbsp;</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                        <?php echo form_textarea($address1); ?>
                        <?php echo form_error('address1'); ?> 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group"> 
                    <label>City</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                        <?php echo form_input($city); ?>
                        <?php echo form_error('city'); ?> 
                    </div>
                </div>
                <div class="col-md-6 form-group"> 
                    <label>Zipcode</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                        <?php echo form_input($zipcode); ?>
                        <?php echo form_error('zipcode'); ?> 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group"> 
                    <label>Mobile</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-phone-square"></i></span>
                        <?php echo form_input($mobile); ?>
                        <?php echo form_error('mobile'); ?> 
                    </div>
                </div> 
                <div class="col-md-6 form-group"> 
                    <label>Fax</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-phone-square"></i></span>
                        <?php echo form_input($fax); ?>
                        <?php echo form_error('fax'); ?> 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group"> 
                    <label>Password</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <?php echo form_input($password); ?>
                        <?php echo form_error('password'); ?>
                    </div>
                </div> 
                <div class="col-md-6 form-group"> 
                    <label>Confirm Password</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <?php echo form_input($confirm_password); ?>
                        <?php echo form_error('confirm_password'); ?> 
                    </div>
                </div>   
            </div>
            <div class="row">
                <div class="col-md-12 form-group"> 
                    <label class="chkbox_container"><input type="checkbox" name="tos" id="tos" class="" checked="checked"/> <span class="checkmark"></span>I approve the <a href="<?php echo base_url();?>legals/terms-of-service.php" target="_BLANK">Terms of Services</a></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 form-group"> 
                    <label class="chkbox_container"><input type="checkbox" name="pp" id="pp" class="" checked="checked"/></span> <span class="checkmark"></span>I approve the <a href="<?php echo base_url();?>legals/privacy-policy.php" target="_BLANK">Privacy Policy</a></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12"> 
                    <input type="submit" class="button green_button pull-right" value="Register"/>  
                </div>
            </div>
            <?php echo form_close();?>
        </div>
        <div class="col-md-1 affiliate login-page-divider">&nbsp;</div>
        <div class="col-md-5 benefits" style="padding-left: 60px;">
            <div class="titleBox">
                <h4>Benefits</h4>
                <h6>Why Choose <?php echo $company_name;?> for your transfers ?</h6>
            </div>
            <div class="infoBox">
                <div class="col-md-2 wallet"><i class="fa fa-money"></i></div>
                <div class="col-md-10">
                    <div class="wallet">
                        <h5>Value for Money</h5>
                        <h6>Quality Journeys at discount prices</h6>
                    </div>
                </div>
            </div>
            <div class="infoBox">
                <div class="col-md-2 customer-service"><i class="fa fa-headphones"></i></div>
                <div class="col-md-10">
                    <div class="wallet">
                        <h5>Customer Service</h5>
                        <h6>Quality Client Service to assist you by phone, by email or by live chat support.</h6>
                    </div>
                </div>
            </div>
            <div class="infoBox">
                <div class="col-md-2 easy-use"><i class="fa fa-leaf"></i></div>
                <div class="col-md-10">
                    <div class="wallet">
                        <h5>Ease of Use</h5>
                        <h6>4 steps only easy booking, client area to follow your bookings online</h6>
                    </div>
                </div>
            </div>
        </div>  
    </div>
    
</div>
<script type="text/javascript">
    $(function () { 
         $("#statut").on('change',function() { 
           var statut_id = $(this).val();
           if (statut_id == 2) {
               $(".divCompany").show();
           }
           else { 
               $(".divCompany").hide();
           }
        });
	});
</script>