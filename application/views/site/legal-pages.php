</header>
<div class="container body-border legal-page"> 
    <div class="breadcrumb">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <aside class="nav-links">
                    <ul>
                        <li> <a href="<?php echo site_url(); ?>"> <?php echo $this->lang->line('home_page'); ?>  </a> </li>
                        <li class="active"><a href="javascript:void(0)">&nbsp;<?php if (isset($sub_heading)) echo $sub_heading; ?> </a></li>
                    </ul>
                </aside>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" style="text-align: left;">
            <?php echo $page_content->description;?>
        </div>
    </div>
</div>