</header>
<div class="container body-border service_page"> 
    <div class="breadcrumb">
        <div class="row">
            <aside class="nav-links">
                <ul>
                    <li> <a href="<?php echo site_url(); ?>"> <?php echo $this->lang->line('home_page'); ?>  </a> </li>
                    <li><a href="javascript:void(0)">Book Now</a></li>
                </ul>
            </aside>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-wizard">
                <li class="active"><span class="steps">1</span><a>Journey Details</a></li>
                 <li><span class="steps">2</span><a>Identification</a></li>
                 <li><span class="steps">3</span><a>Quote</a></li>
                 <li><span class="steps">4</span><a>Payment Details</a></li>
           </ul>
        </div>  
    </div>
    <div class="row booknow-content new-box-type">
      <div class="col-md-12">
		<div class="col-md-5" style="boder: 1px solid #a6a6a6;border-radius: 20px;">
			<div class="booking-tab" style="margin-top:10px;">
				<ul class="nav nav-tabs">
	                                <li class="active"><a data-toggle="tab" href="#package"><i class="fa fa-umbrella"></i> Package</a></li>
					<li><a data-toggle="tab" href="#pick_address"><i class="fa fa-map-marker"></i> Address</a></li>
					<li><a data-toggle="tab" href="#pick_airport"><i class="fa fa-plane"></i> Airport</a></li>
					<li><a data-toggle="tab" href="#pick_train"><i class="fa fa-train"></i> Train</a></li>
					<li><a data-toggle="tab" href="#pick_hotel"><i class="fa fa-hotel"></i> Hotel</a></li>
					<li><a data-toggle="tab" href="#pick_park"><i class="fa fa-tree"></i> Park</a></li>
				</ul>
			</div>
			<div class="tab-content">
		        <div id="package" class="tab-pane fade in active">
					<div class="row" style="margin-top: 5px;">
						<div class="col-md-1">
							<i class="fa fa-hand-o-right" style="font-size:25px;color:#333;margin:8px 0px 0px -5px;"></i>
						</div>
						<div class="col-md-11">
							<input type="text" class="grad-bg">
						</div>
					</div>

					<div class="row" style="margin-top: 5px;">
						<div class="col-md-1">

						</div>
						<div class="col-md-11">
							<input type="text" class="grad-bg">
						</div>
					</div>
					<div class="row" style="margin-top: 5px;margin-bottom: 5px;">
						<div class="col-md-1">

						</div>
						<div class="col-md-11">
							<div class="col-md-12" style="padding: 0px;">
								<div class="col-md-6" style="padding: 0px;">
									<input type="text" class="grad-bg">
								</div>
								<div class="col-md-6" style="padding: 0px;">
									<input type="text" class="grad-bg">
								</div>
							</div>
						</div>
					</div>
		        </div>
				<div id="pick_address" class="tab-pane fade">
					<i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
					<input type="text" style="width: 77% !important; background-image: url(&quot;https://maps.gstatic.com/mapfiles/api-3/images/icon_error.png&quot;);" placeholder="Oops! Something went wrong." class="location pick_up_txtbox_bkg gm-err-autocomplete" id="local_pick" name="pick_up" autocomplete="off" disabled="">
					<div id="add_extra_stop" class="extra-stop-home-en extra-stop-bkg-en"><a class="plus add-location"><img src="https://www.cab-booking-script.com/demo/v1/template/theme/default/images/add.png"></a>
		                                <div></div>
		                            </div>
		                            <div class="clear"></div>
				</div>
				<div id="pick_airport" class="tab-pane fade">
					<i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
					<select name="pick_up" class="airlocation_pick_bkg grad-bg" id="airport_pick" style="float:right;width:95% !important;margin-right:-10px;">
						<option value="">Select</option>
											</select>
					<div id="airport_pick_flight">
						<input id="pickup_flight_no" style="width: 33% !important;" class="pickup_flight_no_bkg grad-bg" type="text" value="" name="pickup_flight_no" placeholder="Flight No">
						<div class="time_pick"><input style="width: 56% !important;" class="grad-bg pickup_flight_time_bkg" id="timepicker_pick_fly" name="pickup_flight_time" type="text" value="01 : 13"><div class="timepicker_wrap " style="top: 45px;"><div class="arrow_top"></div><div class="time-pick"><div class="prev-picker action-next"></div><div class="ti_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div><div class="mins"><div class="prev-picker action-next"></div><div class="mi_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div></div></div>
					</div>
				</div>
				<div id="pick_train" class="tab-pane fade">
					<i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
					<select name="pick_up" class="trainlocation_pick_bkg grad-bg" id="train_pick" style="float:right;width:95% !important;margin-right:-10px;">
						<option value="">Select</option>
											</select>
					<div id="station_pick_train">
						<input id="pickup_train_no" style="width: 66% !important" class="pickup_train_no_bkg grad-bg" type="text" value="" name="pickup_train_no" placeholder="Train No">
						<input style="width:17% !important" class="grad-bg pickup_train_time_bkg" id="timepicker_pick_stn" name="pickup_train_time" type="text" value="01 : 13">
					</div>
				</div>
				<div id="pick_hotel" class="tab-pane fade">
					<i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
					<select name="pick_up" class="hotellocation_pick_bkg grad-bg" id="hotel_pick" style="float:right;width:95% !important;margin-right:-10px;">
						<option value="">Select</option>
											</select>
				</div>
				<div id="pick_park" class="tab-pane fade">
					<i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
					<select name="pick_up" class="parklocation_pick_bkg grad-bg" id="park_pick" style="float:right;width:95% !important;margin-right:-10px;">
						<option value="">Select</option>
											</select>
				</div>
				<input type="hidden" id="pick_type" name="pick_type" value="L">
			</div>
			<div class="clearfix"></div>
    	</div>
	    <div class="col-md-2" style="text-align:center;font-size:60px;padding-top:50px;"> <i class="fa fa-arrow-right"></i> </div>
		<div class="col-md-5 drop_of_location_div_1" style="bordr: 1px solid #a6a6a6;border-radius: 20px;">
			<div class="booking-tab">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#drop_address"><i class="fa fa-map-marker"></i> Drop Address</a></li>
					<li><a data-toggle="tab" href="#drop_airport"><i class="fa fa-plane"></i> Airport</a></li>
					<li><a data-toggle="tab" href="#drop_train"><i class="fa fa-train"></i> Train</a></li>
					<li><a data-toggle="tab" href="#drop_hotel"><i class="fa fa-hotel"></i> Hotel</a></li>
					<li><a data-toggle="tab" href="#drop_park"><i class="fa fa-tree"></i> Park</a></li>
				</ul>
			</div>
			<div class="tab-content">
				<div id="drop_address" class="tab-pane fade in active">
						<div class="row" style="margin-top: 5px;">
								<div class="col-md-1">
									<i class="fa fa-hand-o-right" style="font-size:25px;color:#333;margin:8px 0px 0px -5px;"></i>
								</div>
								<div class="col-md-11">
									<input type="text" class="grad-bg">
								</div>
							</div>
							<div class="row" style="margin-top: 5px;">
								<div class="col-md-1">
								</div>
								<div class="col-md-11">
									<input type="text" class="grad-bg">
								</div>
							</div>
							<div class="row" style="margin-top: 5px;margin-bottom: 5px;">
								<div class="col-md-1">
								</div>
								<div class="col-md-11">
									<div class="col-md-12" style="padding: 0px;">
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" class="grad-bg">
										</div>
										<div class="col-md-6" style="padding: 0px;">
											<input type="text" class="grad-bg">
										</div>
									</div>
								</div>
							</div>
				</div>
				<div id="drop_airport" class="tab-pane fade">
					<i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
					<select name="drop_of" class="airlocation_bkg grad-bg" id="airport_drop_1" style="float:right;width:95% !important;margin-right:-10px;">
						<option value="">Select</option>
											</select>
					<div id="airport_drop_flight_1">
						<input id="dropof_flight_no_1" style="width: 50%!important" class="dropof_flight_no_bkg grad-bg" type="text" value="" name="dropof_flight_no" placeholder="Flight No">
						<div class="time_pick"><input class="grad-bg dropof_flight_time_bkg" style="width: 48% !important" id="timepicker_drop_fly_1" name="dropof_flight_time" type="text" value="01 : 13"><div class="timepicker_wrap " style="top: 45px;"><div class="arrow_top"></div><div class="time-pick"><div class="prev-picker action-next"></div><div class="ti_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div><div class="mins"><div class="prev-picker action-next"></div><div class="mi_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div></div></div>
					</div>
				</div>
				<div id="drop_train" class="tab-pane fade">
					<i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
					<select name="drop_of" class="trainlocation_bkg grad-bg" id="train_drop_1" style="float:right;width:95%!important;margin-right:-10px;">
						<option value="">Select</option>
											</select>
					<div id="station_drop_train_1">
						<input id="dropof_train_no_1" class="dropof_train_no grad-bg" style="width: 66%!important" type="text" value="" name="dropof_train_no" placeholder="Train No">
						<input class="grad-bg dropof_train_time" id="timepicker_drop_stn" style="width: 17%!important" name="dropof_train_time" type="text" value="01 : 13">
					</div>
				</div>
				<div id="drop_hotel" class="tab-pane fade">
					<i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
					<select name="drop_of" class="hotellocation_bkg grad-bg" id="hotel_drop_1" style="float:right;width:95%!important;margin-right:-10px;">
						<option value="">Select</option>
											</select>
				</div>
				<div id="drop_park" class="tab-pane fade">
					<i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
					<select name="drop_of" class="parklocation_bkg grad-bg" id="park_drop_1" style="float:right;width:95%!important;margin-right:-10px;">
						<option value="">Select</option>
											</select>
				</div>
					<input type="hidden" id="drop_type_1" name="drop_type" value="L">
			</div>
			<div class="clearfix"></div>
    		Return?    <input class="return_car" type="checkbox" name="car_return" value="car">
		    Wheelchair <input class="wheel_chair" type="checkbox" name="wheel_chair" value="car">
    	</div>

		<div class="date-time row">
	            <div class="pickdt-bkg"> <!---->
	                <label class="pickup_date pickup_dt_label"> Pick-Up Date </label>
	                <label class="start_date start_dt_label"> Start Date </label>
	                <input id="dtpicker" class="dte pickup-dt-bkg hasDatepicker" type="text" value="" name="pick_date" placeholder="Start Date">
	            </div>
	            <div class="picktime-bkg"> <!---->
	                <label class="pickup_time pickup_time_label">Pick-Up Time</label>
	                <label class="start_time start_time_label">Start Time</label>
	                <div class="time_pick"><input class="grad-bg" id="timepicker1" name="pick_time" type="text" value="01 : 13" style="width:50%"><div class="timepicker_wrap " style="top: 45px;"><div class="arrow_top"></div><div class="time-pick"><div class="prev-picker action-next"></div><div class="ti_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div><div class="mins"><div class="prev-picker action-next"></div><div class="mi_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div></div></div>
	            </div>

	            <!--div id="dropOfWrapper" style="display:none">
	                <div class="dropdt-bkg"> 
	                    <label style="font-size:13px;" class="dropof_date"> Drop-Of Date </label>
	                    <label style="font-size:13px;" class="end_date"> End Date </label>
	                    <input id="dtpicker1" class="dte"  type="text" value="" name="drop_date" placeholder="Drop Date"  />
	                </div>
	                <div class="droptime-bkg"> 
	                    <label class="dropof_time dropof_time_label">Drop-Of Time</label>
	                    <label class="end_time end_time_label">End Time</label>
	                    <input  class="grad-bg" id="timepicker2" name="drop_time" type="text" value="01 : 13" style="width:50%" />
	                </div>     
	            </div-->
	            <div class="return_car_div" style="display:none">
	                <div class="dropdt-bkg">
	                    <label style="font-size:13px;" class="dropof_date"> Return Date </label>
	                    <label style="font-size:13px;" class="end_date"> End Date </label>
	                    <input id="dtpicker1" class="dte hasDatepicker" type="text" value="" name="drop_date" placeholder="Drop Date">
	                </div>
	                <div class="droptime-bkg">
	                    <label class="dropof_time dropof_time_label">Return Time</label>
	                    <label class="end_time end_time_label">End Time</label>
	                    <div class="time_pick"><input class="grad-bg" id="timepicker2" name="drop_time" type="text" value="01 : 13" style="width:50%"><div class="timepicker_wrap " style="top: 45px;"><div class="arrow_top"></div><div class="time-pick"><div class="prev-picker action-next"></div><div class="ti_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div><div class="mins"><div class="prev-picker action-next"></div><div class="mi_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div></div></div>
	                </div>
	            </div>
		</div>
		<div class="col-md-12 regular_options">
			<table style="width:100%;" cellspacing="0" cellpadding="0" align="center">
				<tbody><tr>
					<td style="width:10%;">&nbsp;</td>
					<td>Monday</td>
					<td>Tuesday</td>
					<td>Wednesday</td>
					<td>Thursday</td>
					<td>Friday</td>
					<td>Saturday</td>
					<td>Sunday</td>
				</tr>
				<tr class="go-table">
					<td style="width:10%;text-align: left;">Go</td>
					<td>
						<input name="weekdays_go" class="weekdays_go" type="checkbox" value="1">
						<div class="time_pick"><input class="wkdays_time_pick grad-bg" id="go_time_1" name="go_time_1" type="text" value="01 : 13" style=""><div class="timepicker_wrap " style="top: 45px;"><div class="arrow_top"></div><div class="time-pick"><div class="prev-picker action-next"></div><div class="ti_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div><div class="mins"><div class="prev-picker action-next"></div><div class="mi_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div></div></div>
					</td>
					<td><input name="weekdays_go" class="weekdays_go" type="checkbox" value="2">
						<div class="time_pick"><input class="wkdays_time_pick grad-bg" id="go_time_2" name="go_time_2" type="text" value="01 : 13" style=""><div class="timepicker_wrap " style="top: 45px;"><div class="arrow_top"></div><div class="time-pick"><div class="prev-picker action-next"></div><div class="ti_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div><div class="mins"><div class="prev-picker action-next"></div><div class="mi_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div></div></div>
					</td>
					<td><input name="weekdays_go" class="weekdays_go" type="checkbox" value="3">
						<div class="time_pick"><input class="wkdays_time_pick grad-bg" id="go_time_3" name="go_time_3" type="text" value="01 : 13" style=""><div class="timepicker_wrap " style="top: 45px;"><div class="arrow_top"></div><div class="time-pick"><div class="prev-picker action-next"></div><div class="ti_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div><div class="mins"><div class="prev-picker action-next"></div><div class="mi_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div></div></div>
					</td>
					<td><input name="weekdays_go" class="weekdays_go" type="checkbox" value="4">
						<div class="time_pick"><input class="wkdays_time_pick grad-bg" id="go_time_4" name="go_time_4" type="text" value="01 : 13" style=""><div class="timepicker_wrap " style="top: 45px;"><div class="arrow_top"></div><div class="time-pick"><div class="prev-picker action-next"></div><div class="ti_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div><div class="mins"><div class="prev-picker action-next"></div><div class="mi_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div></div></div>
					</td>
					<td><input name="weekdays_go" class="weekdays_go" type="checkbox" value="5">
						<div class="time_pick"><input class="wkdays_time_pick grad-bg" id="go_time_5" name="go_time_5" type="text" value="01 : 13" style=""><div class="timepicker_wrap " style="top: 45px;"><div class="arrow_top"></div><div class="time-pick"><div class="prev-picker action-next"></div><div class="ti_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div><div class="mins"><div class="prev-picker action-next"></div><div class="mi_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div></div></div>
					</td>
					<td><input name="weekdays_go" class="weekdays_go" type="checkbox" value="6">
						<div class="time_pick"><input class="wkdays_time_pick grad-bg" id="go_time_6" name="go_time_6" type="text" value="01 : 13" style=""><div class="timepicker_wrap " style="top: 45px;"><div class="arrow_top"></div><div class="time-pick"><div class="prev-picker action-next"></div><div class="ti_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div><div class="mins"><div class="prev-picker action-next"></div><div class="mi_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div></div></div>
					</td>
					<td><input name="weekdays_go" class="weekdays_go" type="checkbox" value="7">
						<div class="time_pick"><input class="wkdays_time_pick grad-bg" id="go_time_7" name="go_time_7" type="text" value="01 : 13" style=""><div class="timepicker_wrap " style="top: 45px;"><div class="arrow_top"></div><div class="time-pick"><div class="prev-picker action-next"></div><div class="ti_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div><div class="mins"><div class="prev-picker action-next"></div><div class="mi_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div></div></div>
					</td>
				</tr>
				<tr class="back-table">
					<td style="width:10%;text-align: left;">Back</td>
					<td>
						<input name="weekdays_back" class="weekdays_back" type="checkbox" value="1">
						<div class="time_pick"><input class="wkdays_time_pick grad-bg" id="back_time_1" name="back_time_1" type="text" value="01 : 13" style=""><div class="timepicker_wrap " style="top: 45px;"><div class="arrow_top"></div><div class="time-pick"><div class="prev-picker action-next"></div><div class="ti_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div><div class="mins"><div class="prev-picker action-next"></div><div class="mi_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div></div></div>
					</td>
					<td>
						<input name="weekdays_back" class="weekdays_back" type="checkbox" value="2">
						<div class="time_pick"><input class="wkdays_time_pick grad-bg" id="back_time_2" name="back_time_2" type="text" value="01 : 13" style=""><div class="timepicker_wrap " style="top: 45px;"><div class="arrow_top"></div><div class="time-pick"><div class="prev-picker action-next"></div><div class="ti_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div><div class="mins"><div class="prev-picker action-next"></div><div class="mi_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div></div></div>
					</td>
					<td>
						<input name="weekdays_back" class="weekdays_back" type="checkbox" value="3">
						<div class="time_pick"><input class="wkdays_time_pick grad-bg" id="back_time_3" name="back_time_3" type="text" value="01 : 13" style=""><div class="timepicker_wrap " style="top: 45px;"><div class="arrow_top"></div><div class="time-pick"><div class="prev-picker action-next"></div><div class="ti_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div><div class="mins"><div class="prev-picker action-next"></div><div class="mi_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div></div></div>
					</td>
					<td>
						<input name="weekdays_back" class="weekdays_back" type="checkbox" value="4">
						<div class="time_pick"><input class="wkdays_time_pick grad-bg" id="back_time_4" name="back_time_4" type="text" value="01 : 13" style=""><div class="timepicker_wrap " style="top: 45px;"><div class="arrow_top"></div><div class="time-pick"><div class="prev-picker action-next"></div><div class="ti_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div><div class="mins"><div class="prev-picker action-next"></div><div class="mi_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div></div></div>
					</td>
					<td>
						<input name="weekdays_back" class="weekdays_back" type="checkbox" value="5">
						<div class="time_pick"><input class="wkdays_time_pick grad-bg" id="back_time_5" name="back_time_5" type="text" value="01 : 13" style=""><div class="timepicker_wrap " style="top: 45px;"><div class="arrow_top"></div><div class="time-pick"><div class="prev-picker action-next"></div><div class="ti_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div><div class="mins"><div class="prev-picker action-next"></div><div class="mi_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div></div></div>
					</td>
					<td>
						<input name="weekdays_back" class="weekdays_back" type="checkbox" value="6">
						<div class="time_pick"><input class="wkdays_time_pick grad-bg" id="back_time_6" name="back_time_6" type="text" value="01 : 13" style=""><div class="timepicker_wrap " style="top: 45px;"><div class="arrow_top"></div><div class="time-pick"><div class="prev-picker action-next"></div><div class="ti_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div><div class="mins"><div class="prev-picker action-next"></div><div class="mi_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div></div></div>
					</td>
					<td>
						<input name="weekdays_back" class="weekdays_back" type="checkbox" value="7">
						<div class="time_pick"><input class="wkdays_time_pick grad-bg" id="back_time_7" name="back_time_7" type="text" value="01 : 13" style=""><div class="timepicker_wrap " style="top: 45px;"><div class="arrow_top"></div><div class="time-pick"><div class="prev-picker action-next"></div><div class="ti_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div><div class="mins"><div class="prev-picker action-next"></div><div class="mi_tx"><input type="text" class="timepicki-input"></div><div class="next12 action-prev"></div></div></div></div>
					</td>
				</tr>
			</tbody></table>
		</div>
		<script type="text/javascript">
           $(document).ready(function(){
           	    $('.wheelchair').hide();
           	    $('.return_car_div').hide();
           });

		     $('.wheel_chair').change(function() {
		        if($(this).is(":checked")) {
		        	$('.wheelchair').show();
		        	$('.non-wheelchair').hide();
		        }else{
		        	$('.wheelchair').hide();
		        	$('.non-wheelchair').show();
		        }
		     });
		     $('.return_car').change(function() {
		        if($(this).is(":checked")) {
		        	$('.return_car_div').show();
		        }else{
		        	$('.return_car_div').show();
		        }
		     });
		</script>
           <div class="wheelchair" style="display: none;">
	    <div class="other_options" style="display:table;margin:0 auto;">
				<div class="pull-left" style="border:1px solid grey;height: 50px;padding: 15px;text-align: center;margin-right: 2px;">
	               <input type="radio" name="vehicle" class="car_tpmr_veh" value="car_tpmr"><i class="fa fa-car" aria-hidden="true"></i>   Car TPMR &nbsp;
				</div>
				<div class="pull-left" style="border:1px solid grey;height: 50px;padding: 15px;text-align: center;margin-right: 2px;">
	               <input type="radio" name="vehicle" class="van_tpmr_veh" value="van_tpmr"><i class="fa fa-bus" aria-hidden="true"></i>   Van TPMR &nbsp;
				</div>
				<div class="pull-left" style="border:1px solid grey;height: 50px;padding: 15px;text-align: center;margin-right: 2px;">
	               <input type="radio" name="vehicle" class="minibus_tpmr_veh" value="minibus_tpmr" checked=""><i class="fa fa-train" aria-hidden="true"></i>   Minibus TPMR &nbsp;
				</div>
       </div>
			</div>
			<div class="non-wheelchair">
	    <div class="other_options" style="display:table;margin:0 auto;">
		        <div class="pull-left" style="border:1px solid grey;height: 50px;padding: 15px;text-align: center;margin-right: 2px;">
	               <input type="radio" name="vehicle" class="car_veh" value="car"><i class="fa fa-car" aria-hidden="true"></i>  Car(4) &nbsp;
				</div>
				<div class="pull-left" style="border:1px solid grey;height: 50px;padding: 15px;text-align: center;margin-right: 2px;">
	               <input type="radio" name="vehicle" class="van_veh" value="van">
					<i class="fa fa-bus" aria-hidden="true"></i> Van(6) &nbsp;
				</div>
				<div class="pull-left" style="border:1px solid grey;height: 50px;padding: 15px;text-align: center;margin-right: 2px;">
	               <input type="radio" name="vehicle" class="minibus_veh" value="minibus" checked="">
					<i class="fa fa-train" aria-hidden="true"></i> Minibus(8) &nbsp;
				</div>
		</div>
			</div>
    	</div>

	<div class="down-form" id="journey_details" style="display: block;">
		<div class="">
			<div class="scrooll" id="cars_data_list">
							</div>
		</div>
	</div>

	<div class="clearfix"></div>
	<div class="row">
		<div class="" style="margin-left:100px;">
						<!--<input type="submit" style="margin-top:10px;margin-right:30px;margin-bottom: 0px;float:left;" id="btnbooknow" value="Book Now" class="button light_orange_button" >-->
		</div>
	</div>
	<input type="hidden" name="waitingTime" id="waitingTime" value="No Waiting">
	<input type="hidden" name="waitingCost" id="waitingCost" value="0">
	<div class="col-md-3">
		<input type="hidden" name="package_type" value="-">
		<input type="hidden" name="booking_ref" value="200312131345">
		<input type="hidden" name="car_cost" id="car_cost" value="0.00">
		<input type="hidden" name="total_cost" id="total_cost" value="0.00">
		<input type="hidden" name="total_time" id="total_time" value="0">
		<input type="hidden" name="total_distance" id="total_distance" value="0">
		<input type="hidden" name="car_name" id="car_name">
	</div>
            </div>
</div>