<?php
 $payment_method_form =  $this->session->flashdata('payment_method_form');
$booking=$this->session->userdata['bookingdata'];
$bookingregular=$this->session->userdata['bookingpickregular'];
$bookingreturn=$this->session->userdata['bookingreturnregular'];
$client=$this->session->userdata['user'];
$displaypassengers=$this->session->userdata['passengers'];
$stopsdata=$this->session->userdata['stops'];
$map= $this->session->userdata['mapdatainfo'];
if(!empty($booking['pickupdate'])){
 $bookingdate=str_replace('/', '-', $booking['pickupdate']);
}
else{
  $bookingdate=str_replace('/', '-', $booking['startdate']);
}
$pickupaddress1=str_replace((', '.$booking['dropcity']), "", $pickupaddress);
$dropoffaddress1=str_replace((', '.$booking['dropcity']), "", $dropoffaddress);
?>
</header>
<div class="container">
<div class="row">
    <div class="body-container body-border"><!--padding-p-0 -->
        <div id="breadcrumb">
            <a href="<?php echo site_url(); ?>/" style="color:#4ca6cf;"><i class="fa fa-home"></i> Home </a>
            <a href="javascript:void(0)">&nbsp;<?php echo $page_title ?> </a>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-wizard">
                    <li ><span class="steps">1</span><a style="color:#fff;">Journey Details</a></li>
                    <li ><span class="steps">2</span><a style="color:#fff;">Identification</a></li>
                    <li ><span class="steps">3</span><a style="color:#fff;">Quote</a></li>
                    <li class="active"><span class="steps">4</span><a>Payment Details</a></li>
                </ul>
            </div>  
        </div>
        <div class="row booknow-content">
            <div class="col-md-12 online">
                <div class="booking_head">
                  Booking Number : <?=create_timestampdmy_uid($bookingdate,($booking['bookingid']+1));?></div>

                <div id="bookinginformationlistdiv">
                   <div class="col-md-7" style="margin-top:15px;">
                        <div class="row">
                          <div class="col-md-12" style="padding: 0px;">
                           <div class="col-md-4" style="padding: 0px">
                          
                            <p style="font-size:13px !important;"><span style="margin-right:5px;font-weight: 900;">Client : </span><br>
                              <?php echo "<span style='font-weight:900;'>".$client->civility.' '.$client->first_name.' '.$client->last_name.'</span><br>'.$client->address.'<br>';?>
                             <?php if(!empty($client->address1) || $client->address1 != null):  ?>
                              <?php echo $client->address1.'<br>'; ?>
                             <?php endif; ?>
                             <?php echo $client->zipcode.'  '.$client->city;?></p>
                           </div>
                            <div class="col-md-8 " style="padding: 0px;">
                                  
                                
                                  <div class="col-md-12" style="padding:0px;margin-top:-5px;width: 87%;margin-left: 13%;">

                                       <ul class="ulbdnone" id="passengerdiv" >
                                         <li style="font-weight: 900;border-bottom:0px;">Passengers :</li>
                                          <?php foreach($displaypassengers as $passenger): ?>
                                           <li class="text-left" style="position: relative;"><span class="fa fa-user" style="margin-right:5px;"></span><span><?php echo $passenger['name'];?></span>, <span class="fa fa-mobile" style="margin-right:5px;"></span><span><?php echo $passenger['phone'];?></span>, <span class="fa fa-phone" style="margin-right:5px;"></span><span><?php echo $passenger['home'];?></span>, <span class="fa fa-wheelchair" style="margin-right:5px;"></span><span><?= $passenger['disable'] ?></span>
                                           
                                               
                                          </li>

                                          <?php endforeach; ?>    
                                           
                                       </ul>

                                    </div>
                                 
                         
                                
                                </div>
                            </div>
                           
                         
<!-- Booking Details  Start --> 
 <?php  include 'bookingdetails.php';?> 
 <!-- Booking Details End -->  

                        
 </div>
</div>
              
          
                 <div class="col-md-5" style="margin-top:15px;padding-right: 0px;">
                  <?php
                      $attributes = array("name" => 'booking_form', "id" => 'bookingform');       
                    echo form_open('confirmbooking',$attributes);
                  ?> 
                        <input type="hidden" id="pricestatuslevel" name="paymentstatus" value="<?= ($pricestatus_data!=null && $pricestatus_data->pricestatus==0)?'0':'1'; ?>">
                        <input type="hidden" id="paymentmethodlevel" name="paymentmethodlevel" value="<?php  if(isset($payment_method_form['level']['step']) && !empty($payment_method_form['level']['step'])){echo $payment_method_form['level']['step'];}else{echo "1";} ?>">
                  <div class="col-md-12 paymentmethodboxdiv <?= ($pricestatus_data!=null && $pricestatus_data->pricestatus==0)?'removebbr':''; ?>">
                      
                      <div class="col-md-12" style="padding:0px;" >
                          <button class="btn payment-method-header-title" style="pointer-events:none;">
                          Select your payment methode         
                          </button>        
                      </div>
                               
                              
                  <div class="col-md-12" style="padding:0px;">             
                            <ul role="tablist" id="paymentmethodsullist" class="nav nav-tabs on-bo-he  payement" style="width: 100%;">
                              <?php
                               $methodcount=1;
                               foreach ($paymentmethods as $key => $method):
                                 if($methodcount<7):
                              ?>

                            <li class="payment-method-buttons-list" style="padding: 0px !important;width: 16.65%;margin:0px !important;border:0px;">         
                            <input type="radio" name="payment_method"  value="<?= $method->id ?>" id="methodbtn<?= $methodcount ?>" style="top:8px !important;">
                            <button type="button" class="btn btn-default extra-pad payment-method-buttons-btn" style="padding: 6px 0px 0px 22px !important;"><span style="font-size: 9px;"><?= $method->name ?></span>
                            </button>
                            </li>

                              <?php
                              $methodcount++;
                               endif;
                               endforeach;
                              ?>
                            </ul>
                   </div>
                            
                    <div class="col-md-12" id="paymentmethoddiv">
                          <?php
                           $paymentmethodcount=1;
                           $cardtype="credit";
                           foreach ($paymentmethods as $key => $method):
                            if($paymentmethodcount<7):
                          ?>

                          <div class="payments <?= ($pricestatus_data!=null && $pricestatus_data->pricestatus==0)?'hidepaymentmethodboxes':''; ?>" id="paymentmethod<?= $paymentmethodcount ?>" style="text-align: unset;">
                                <h4 style="font-size:18px;font-weight:900;"> Pay With <?= $method->name ?> </h4>
                                <!-- Credit Card Start -->
                                <?php
                                         if($method->category_client == 1): 
                                         $cardtype="credit";
                                ?>         

                                         <?php  include 'creditcard.php';?> 
                                <!-- Credit Card End -->
                                    
                                 <!-- Bank Debit Card Start -->
                                <?php
                                        elseif($method->category_client == 2): 
                                         $cardtype="debit";
                                ?>
                                         <?php  include 'debitcard.php';?>
                                 <!-- Bank Debit Card End -->

                                 <!-- Other Payment ways Start -->
                                <?php else: ?>
                                  <p><?= $method->description ?></p>
                                <?php endif; ?>
                                <!-- Other Payment ways End -->
                                
                            </div>
                        <?php
                          $paymentmethodcount++;
                           endif;
                           endforeach;
                        ?>
                  </div>
                                    <!--<div class="down-btn">-->
                        <?php if($pricestatus_data ==null || $pricestatus_data->pricestatus!=0 ): ?>
                         <div class="col-md-12" style="padding: 0px;">
                             <button id="distancebtn" class="btn  grbtn brz" style="border-bottom-left-radius: 15px;text-align:left;font-size: 11px;border:0px !important;">Estimated Distance : <?= $map['distance'] ?> km</button>
                             <button id="timebtn" class="btn  grbtn brz blz" style="font-size: 11px;border:0px !important;">Estimated Time : <?= str_replace('min', 'minute', $map['time']) ?> </button>
                              <button class="btn  grbtn blz" id="bookingpricedivbtn" style="border-bottom-right-radius: 15px;text-align:right;font-size: 11px;border:0px !important;">Estimated Price : <?= $map['price'] ?> € HT</button>
                           </div>   
                         <?php endif; ?>

                    </div>
                                                   <?php echo form_close(); ?>
                    <div class="row">
                        <?php if($pricestatus_data ==null || $pricestatus_data->pricestatus!=0 ): ?>
                         <div class="col-md-12" >
                          <!-- Fees -->
                           <div class="col-md-12  clearfix amoutndetaildiv" >

                              <div class="col-md-5" style="padding: 0px;">
                                <div class="dd1_amount">
                                 <span style="font-weight:900;">Fees</span>
                                </div>
                             </div>
                               <div class="col-md-3" style="padding: 0px;text-align: center;">
                                  <span style="font-weight:900;">%</span>
                               </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span style="font-weight:900;">Amount</span>
                             </div>
                           </div>
                              
                            </div>
                               <?php if($map['drivermealfee']!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                             <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Driver Meal Fee :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align:center;">
                             
                                <span></span>
                             
                              </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $map['drivermealfee'];?> €</span>
                             </div>  
                             </div>       
                            </div>
                          <?php endif; ?>
                            <?php if($map['drivermealfee']!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                             <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Driver Rest Fee :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align:center;">
                             
                                <span></span>
                             
                              </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $map['driverrestfee'];?> €</span>
                             </div>  
                             </div>       
                            </div>
                          <?php endif; ?>
                           <?php if($map['nighttimefee']!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                             <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Night Fee :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align:center;">
                              <?php if($map['nighttimediscount']!=0):  ?>
                                <span><?php echo $map['nighttimediscount'];?>%</span>
                              <?php endif; ?>
                              </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $map['nighttimefee'];?> €</span>
                             </div>  
                             </div>       
                            </div>
                          <?php endif; ?>

                            <?php if($map['notworkingdayfee']!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                             <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Sunday And Not Working Day Fee :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;">
                              <?php if($map['notworkingdaydiscount']!=0):  ?>
                                <span><?php echo $map['notworkingdaydiscount'];?>%</span>
                              <?php endif; ?>
                             </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $map['notworkingdayfee'];?> €</span>
                             </div>  
                             </div>       
                            </div>
                          <?php endif; ?>


                           <?php if($map['maydecdayfee']!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                             <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>1st Mai And 25 December Fee :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;">
                               <?php if($map['maydecdaydiscount']!=0):  ?>
                                <span><?php echo $map['maydecdaydiscount'];?>%</span>
                              <?php endif; ?>
                             </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $map['maydecdayfee'];?> €</span>
                             </div>   
                             </div>      
                            </div>
                          <?php endif; ?>
                          <!-- Fees -->
                          <?php if($map['vatdiscount']!=0): ?>
                              <div class="col-md-12  clearfix amoutndetaildiv" >

                              <div class="col-md-5" style="padding: 0px;">
                                <div class="dd1_amount">
                                 <span style="font-weight:900;">Vat</span>
                                </div>
                             </div>
                               <div class="col-md-3" style="padding: 0px;text-align: center;">
                                  <span style="font-weight:900;">%</span>
                               </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span style="font-weight:900;">Amount</span>
                             </div>
                           </div>
                              
                            </div>

                            <div class="col-md-12 clearfix amoutndetaildiv" >
                               <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span><?= $map['vatname'] ?> : </span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $map['vatdiscount'];?>%</span></div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $map['vatfee'];?> €</span>
                             </div>
                           </div>  
                            </div>
                             <?php endif; ?>
                             <div class="col-md-12  clearfix amoutndetaildiv" >
                              <div class="col-md-5" style="padding: 0px;">
                                <div class="dd1_amount">
                                 <span style="font-weight:900;">Discounts</span>
                                </div>
                             </div>
                               <div class="col-md-3" style="padding: 0px;text-align: center;">
                                  <span style="font-weight:900;">%</span>
                               </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span style="font-weight:900;">Amount</span>
                             </div>
                           </div>
                              
                            </div>
                          


                      

                         
                          <?php if($map['welcomediscount']!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                            <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Welcome Discount :</span>
                              </div>
                            </div>
                            <div class="col-md-3" style="padding: 0px; text-align: center;">
                             
                               <span><?php echo $map['welcomediscount'];?>%</span>
                             
                            </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>-  <?php echo $map['welcomediscountfee'];?> €</span>
                             </div>
                           </div>
                              
                            </div>
                          <?php endif; ?>
                        
                           <?php if($map['rewarddiscount']!=0): ?>
                            <div class="col-md-12 clearfix amoutndetaildiv" >
                            <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Reward Discount :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $map['rewarddiscount'];?>%</span></div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>-  <?php echo $map['rewarddiscountfee'];?> €</span>
                             </div>
                           </div>
                              
                            </div>
                             <?php endif; ?>
                           <?php if($map['perioddiscount']!=0): ?>
                            <div class="col-md-12 clearfix amoutndetaildiv" >
                               <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Period Discount :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $map['perioddiscount'];?>%</span></div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>-   <?php echo $map['perioddiscountfee'];?> €</span>
                             </div>
                           </div>
                              
                            </div>
                             <?php endif; ?>
                             <div class="col-md-12 clearfix amoutndetaildiv" id="promo_code_div" >
                               <?php if($map['promocodediscount']!=0): ?>
                             
                            <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Code Discount :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $map['promocodediscount'];?>%</span></div>
                              <div class="col-md-4" style="padding: 0px;">
                               <div class="dd2_amount">
                                 <span>-   <?php echo $map['promocodediscountfee'];?> €</span>
                               </div>
                             </div>
                            <?php endif; ?>
                             
                              
                            </div>
                           
                           <div class="col-md-12 clearfix grdiv amountdetaillastdiv">
                             <div class="dd1_amount">
                               <span style="font-weight: 900;">Price To Pay</span>
                              </div>
                              <div class="dd2_amount">
                               <span style="font-weight: 900;"><?php echo $map['totalprice'];?> € TTC</span>
                             </div>
                           </div>

                          </div>
                           <?php else: ?>
                              <div class="col-md-12" style="margin-top: -1px;">
                             <button id="distancebtn" class="btn  grbtn brz" style="border-bottom-left-radius: 15px;text-align:left;width: 50%;font-size: 12px !important;">Estimated Distance : <?= $map['distance'] ?> km</button>
                             <button id="timebtn" class="btn  grbtn blz" style="border-bottom-right-radius: 15px;width:50%;text-align: right;font-size: 12px !important;overflow: hidden;">Estimated Time : <?= str_replace('min', 'minute', $map['time']) ?> </button>
                             
                           </div>
                        <div class="col-md-12" style="margin-top: 10px;">     
                         <div>
                            <div>                            
                              <div class="pricemessagediv">
                                   <p><?= strip_tags($pricestatus_data->message); ?></p>

                                       
                                   
                          </div>                           
                        </div>
                    </div>
                  </div>
                    <?php endif; ?>
                              
                          </div>
                        </div>
                        

                    </div>
                    
                </div>
            </div>
     
        <div class="clearfix"></div>
        <div class="online">
            <div class="col-md-12" style="margin-bottom: 10px;padding:0px;">
                <div class="col-md-6">
                    <a href="<?php echo base_url() ?>booking_quote.php" class="btn btn-default extra-pad" style="font-weight: 900;margin-left: -13px;"> <i class="fa fa-arrow-circle-o-left"></i> Back To Quote  </a>
                </div>
                <div class="col-md-6">
                    <button type="submit" form="bookingform" class="btn btn-default extra-pad right"  id="confirmpaymentbookingbtn"  
                    style="text-transform: none;color: #fff;cursor: pointer;font-weight: normal;height: 34px;font-size: 14px;padding: 6px 12px;border-radius: 5px;">Confirm Payment Methode <i class="fa fa-arrow-circle-o-right"></i> </button> 
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
</div>

<script>
    $(function() {
        $('#card_num_credit').validateCreditCard(function(result) {
          if(result.card_type!=null){
             $("#name_credit").val(result.card_type.name);
          }
         
           
        });
    });
</script>
<script>
   $(document).ready(function(){
           //var l=$("#paymentmethodsullist>li").length;
            //var w=(100/l);
            //w=w.toFixed(2);
            $("#methodbtn1").attr("checked", "checked");
           // $("#paymentmethodsullist>li").css("width", w+"%");
            var paymentmethodlevel= document.getElementById('paymentmethodlevel').value;
           document.getElementById("paymentmethod1").style.display ='none';
           document.getElementById("paymentmethod2").style.display  ='none';
           document.getElementById("paymentmethod3").style.display = 'none';
           document.getElementById("paymentmethod4").style.display = 'none';
           document.getElementById("paymentmethod5").style.display = 'none';
           document.getElementById("paymentmethod6").style.display = 'none';
            if(paymentmethodlevel=='1'){
             
              document.getElementById("methodbtn1").checked = true;
              document.getElementById("paymentmethod1").style.display ='block';
            }
            else if(paymentmethodlevel=='2'){
               document.getElementById("methodbtn2").checked = true;
              document.getElementById("paymentmethod2").style.display  ='block';
            }
            else if(paymentmethodlevel=='3'){
               document.getElementById("methodbtn3").checked = true;
              document.getElementById("paymentmethod3").style.display = 'block';
            }
            else if(paymentmethodlevel=='4'){
               document.getElementById("methodbtn4").checked = true;
               document.getElementById("paymentmethod4").style.display = 'block';
            }
            else if(paymentmethodlevel=='5'){
               document.getElementById("methodbtn5").checked = true;
                 document.getElementById("paymentmethod5").style.display = 'block';
            }
            else if(paymentmethodlevel=='6'){
               document.getElementById("methodbtn6").checked = true;
                 document.getElementById("paymentmethod6").style.display = 'block';
            }
            
           
           
          
          
          


     $(".payment-method-buttons-list button").click(function(){
     
       $(this).siblings().trigger("click");
     });

          $("#methodbtn1").click(function(){
           document.getElementById("paymentmethod1").style.display ='block';
           document.getElementById("paymentmethod2").style.display  ='none';
           document.getElementById("paymentmethod3").style.display = 'none';
           document.getElementById("paymentmethod4").style.display = 'none';
           document.getElementById("paymentmethod5").style.display = 'none';
           document.getElementById("paymentmethod6").style.display = 'none';
        });

        $("#methodbtn2").click(function(){
          document.getElementById("paymentmethod1").style.display ='none';
           document.getElementById("paymentmethod2").style.display  ='block';
           document.getElementById("paymentmethod3").style.display = 'none';
           document.getElementById("paymentmethod4").style.display = 'none';
            document.getElementById("paymentmethod5").style.display = 'none';
             document.getElementById("paymentmethod6").style.display = 'none';
        });

         $("#methodbtn3").click(function(){
           document.getElementById("paymentmethod1").style.display ='none';
           document.getElementById("paymentmethod2").style.display  ='none';
           document.getElementById("paymentmethod3").style.display = 'block';
           document.getElementById("paymentmethod4").style.display = 'none';
            document.getElementById("paymentmethod5").style.display = 'none';
             document.getElementById("paymentmethod6").style.display = 'none';
        });

       $("#methodbtn4").click(function(){
            document.getElementById("paymentmethod1").style.display ='none';
           document.getElementById("paymentmethod2").style.display  ='none';
           document.getElementById("paymentmethod3").style.display = 'none';
           document.getElementById("paymentmethod4").style.display = 'block';
            document.getElementById("paymentmethod5").style.display = 'none';
             document.getElementById("paymentmethod6").style.display = 'none';
       });

       $("#methodbtn5").click(function(){
             document.getElementById("paymentmethod1").style.display ='none';
           document.getElementById("paymentmethod2").style.display  ='none';
           document.getElementById("paymentmethod3").style.display = 'none';
           document.getElementById("paymentmethod4").style.display = 'none';
            document.getElementById("paymentmethod5").style.display = 'block';
             document.getElementById("paymentmethod6").style.display = 'none';
       });
       $("#methodbtn6").click(function(){
             document.getElementById("paymentmethod1").style.display ='none';
           document.getElementById("paymentmethod2").style.display  ='none';
           document.getElementById("paymentmethod3").style.display = 'none';
           document.getElementById("paymentmethod4").style.display = 'none';
            document.getElementById("paymentmethod5").style.display = 'none';
             document.getElementById("paymentmethod6").style.display = 'block';
       });

          $('#credit1').click(function() {
                if($('#radio1').is(":checked")) {
                   $('#radio1').prop('checked', true);
                }else{
                    $('#radio1').prop('checked', false);
                 
                }
             });

             $('#check1').click(function() {
                if($('#radio5').is(":checked")) {
                   $('#radio5').prop('checked', true);
                }else{
                    $('#radio5').prop('checked', false);
                 
                }
             });

                $('#cashdriver1').click(function() {
                if($('#radio6').is(":checked")) {
                   $('#radio6').prop('checked', true);
                }else{
                    $('#radio6').prop('checked', false);
                 
                }
             });

               $('#bankwire1').click(function() {
                if($('#radio4').is(":checked")) {
                   $('#radio4').prop('checked', true);
                }else{
                    $('#radio4').prop('checked', false);
                 
                }
             });

                 $('#directbank1').click(function() {
                if($('#radio3').is(":checked")) {
                   $('#radio3').prop('checked', true);
                }else{
                    $('#radio3').prop('checked', false);
                 
                }
             });

   });
</script>
 <script type="text/javascript">
const form = document.getElementById('bookingform');
const cardname  = document.getElementById('name_credit');
const cardnumber = document.getElementById('card_num_credit');
const cardmonth = document.getElementById('exp_month_credit');
const cardyear = document.getElementById('exp_year_credit');
const cardcvc = document.getElementById('cvc_credit');

const civility_debit  = document.getElementById('civility_debit');
const fname_debit = document.getElementById('fname_debit');
const lname_debit = document.getElementById('lname_debit');
const bankname_debit = document.getElementById('bankname_debit');
const agencyname_debit = document.getElementById('agencyname_debit');
const agencyaddr_debit  = document.getElementById('agencyaddr_debit');
const otheraddr_debit = document.getElementById('otheraddr_debit');
const agencyzipcode_debit = document.getElementById('agencyzipcode_debit');
const agencycity_debit = document.getElementById('agencycity_debit');
const ibnnum_debit = document.getElementById('ibnnum_debit');
const swiftcode_debit  = document.getElementById('swiftcode_debit');
const agreement_debit = document.getElementById('agreement_debit');


 

form.addEventListener('submit', e => {
  e.preventDefault();
  var pricestatus=document.getElementById('pricestatuslevel').value;
   var creditcardbtnnum=$('#creditcardbtnnum').val();
   var debitcardbtnnum=$('#debitcardbtnnum').val();
  if(pricestatus == 0){
     e.currentTarget.submit();
  }
  if($('#methodbtn'+creditcardbtnnum).is(":checked")) {
  
    var valid=checkInputs();
    if(valid){
       e.currentTarget.submit();
    }
  }
 else if($('#methodbtn'+debitcardbtnnum).is(":checked")) {
   
    var valid=checkDebitInputs();
    if(valid){
       e.currentTarget.submit();
    }
  }
else{
 
    e.currentTarget.submit();
}


});

function checkInputs() {
  
  var validate=true;
  // trim to remove the whitespaces
  const cardnameValue = cardname.value.trim();
  const cardnumberValue = cardnumber.value.trim();
  const cardmonthValue = cardmonth.value.trim();
  const cardyearValue = cardyear.value.trim();
   const cardcvcValue = cardcvc.value.trim();
 
  if(cardnameValue === '') {
    setErrorFor(cardname, 'REQUIRED');
    validate=false;
  } else {
    setSuccessFor(cardname);
  }
  
  if(cardnumberValue === '') {
    setErrorFor(cardnumber, 'REQUIRED');
    validate=false;
  } else {
    setSuccessFor(cardnumber);
  }
  
  if(cardmonthValue === '') {
    setErrorFor(cardmonth, 'REQUIRED');
    validate=false;
  } else {
    setSuccessFor(cardmonth);
  }
  
  if(cardyearValue === '') {
    setErrorFor(cardyear, 'REQUIRED');
    validate=false;
  } else{
    setSuccessFor(cardyear);
  }

   if(cardcvcValue === '') {
    setErrorFor(cardcvc, 'REQUIRED');
    validate=false;
  } else{
    setSuccessFor(cardcvc);
  }
  return validate;
}
function checkDebitInputs() {
  
  var validate=true;
  // trim to remove the whitespaces
  const civility_debitValue= civility_debit.value.trim();
  const fname_debitValue= fname_debit.value.trim();
  const lname_debitValue= lname_debit.value.trim();
  const bankname_debitValue= bankname_debit.value.trim();
  const agencyname_debitValue= agencyname_debit.value.trim();
  const agencyaddr_debitValue= agencyaddr_debit.value.trim();
  const otheraddr_debitValue= otheraddr_debit.value.trim();
  const agencyzipcode_debitValue= agencyzipcode_debit.value.trim();
  const agencycity_debitValue= agencycity_debit.value.trim();
  const ibnnum_debitValue= ibnnum_debit.value.trim();
  const swiftcode_debitValue= swiftcode_debit.value.trim();
  const agreement_debitValue= agreement_debit.value.trim();
 
  if(civility_debitValue === '') {
    setErrorFor(civility_debit, 'REQUIRED');
    validate=false;
  } else {
    setSuccessFor(civility_debit);
  }
  
  if(fname_debitValue === '') {
    setErrorFor(fname_debit, 'REQUIRED');
    validate=false;
  } else {
    setSuccessFor(fname_debit);
  }
  
  if(lname_debitValue === '') {
    setErrorFor(lname_debit, 'REQUIRED');
    validate=false;
  } else {
    setSuccessFor(lname_debit);
  }
  
  if(bankname_debitValue === '') {
    setErrorFor(bankname_debit, 'REQUIRED');
    validate=false;
  } else{
    setSuccessFor(bankname_debit);
  }

   if(agencyname_debitValue === '') {
    setErrorFor(agencyname_debit, 'REQUIRED');
    validate=false;
  } else{
    setSuccessFor(agencyname_debit);
  }
  if(agencyaddr_debitValue === '') {
    setErrorFor(agencyaddr_debit, 'REQUIRED');
    validate=false;
  } else {
    setSuccessFor(agencyaddr_debit);
  }
  
  if(otheraddr_debitValue === '') {
    setErrorFor(otheraddr_debit, 'REQUIRED');
    validate=false;
  } else{
    setSuccessFor(otheraddr_debit);
  }

   if(agencyzipcode_debitValue === '') {
    setErrorFor(agencyzipcode_debit, 'REQUIRED');
    validate=false;
  } else{
    setSuccessFor(agencyzipcode_debit);
  }
   if(agencycity_debitValue === '') {
    setErrorFor(agencycity_debit, 'REQUIRED');
    validate=false;
  } else {
    setSuccessFor(agencycity_debit);
  }
  
  if(otheraddr_debitValue === '') {
    setErrorFor(otheraddr_debit, 'REQUIRED');
    validate=false;
  } else{
    setSuccessFor(otheraddr_debit);
  }

   if(ibnnum_debitValue === '') {
    setErrorFor(ibnnum_debit, 'REQUIRED');
    validate=false;
  } else{
    setSuccessFor(ibnnum_debit);
  }
  if(swiftcode_debitValue === '') {
    setErrorFor(swiftcode_debit, 'REQUIRED');
    validate=false;
  } else{
    setSuccessFor(swiftcode_debit);
  }
  if($(agreement_debit).is(":checked")){
  
   setSuccessFor(agreement_debit);
   
  } else{

    setErrorFor(agreement_debit, 'REQUIRED');
    validate=false;
  }
  return validate;
}

function setErrorFor(input, message) {
  const formControl = input.parentElement;
  const small = formControl.querySelector('small');
  formControl.className = 'cardform-control errors';
  small.innerText = message;
}

function setSuccessFor(input) {
  const formControl = input.parentElement;
  formControl.className = 'cardform-control success';
}
  
 </script>
