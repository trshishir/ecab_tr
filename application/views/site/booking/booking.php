</header>
<div class="container">
<div class="row">
   <div class="body-container body-border"><!--padding-p-0 -->
    <div id="breadcrumb">
        <a href="<?php echo site_url(); ?>/" style="color:#4ca6cf;"><i class="fa fa-home"></i> Home </a>
        <a href="javascript:void(0)">&nbsp;<?php echo $page_title ?> </a>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-wizard">
                <li class="active"><span class="steps">1</span><a>Journey Details</a></li>
                 <li style="color:#fff !important;"><span class="steps">2</span><a>Identification</a></li>
                 <li style="color:#fff !important;"><span class="steps">3</span><a>Quote</a></li>
                 <li style="color:#fff !important;"><span class="steps">4</span><a>Payment Details</a></li>
           </ul>
        </div>  
    </div>

 <!-- BOOKING BOX TYPE  -->
    <?php  include 'booking_box_new.php';?>


   
        <div class="clearfix"></div>
    </div>
</div>
</div>
