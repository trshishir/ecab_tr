    <div class="card">  
      <div class="card-body bg-light">
         <input type="hidden" id="debitcardbtnnum" value="<?= $paymentmethodcount ?>">
                      <?php if (validation_errors()): ?>
                        <div class="alert alert-danger" role="alert">
                          <strong>Oops!</strong>
                          <?php echo validation_errors() ;?> 
                        </div>  
                      <?php endif ?>


                      <div id="payment-errors"></div>  

                      <div class="row" style="padding-bottom: 20px;">
                          <div class="col-md-4">
                            <div class="form-group cardform-control <?php if(isset($payment_method_form['civility_debit']['error'])){
                               echo (!empty($payment_method_form['civility_debit']['error']))?'errors':'success';
                             } ?> ">
                            <select class="form-control" name="civility_<?= $cardtype ?>" id="civility_<?= $cardtype ?>" style="height:40px;">
                              <option <?php  if(isset($payment_method_form['civility_debit']['value']) && !empty($payment_method_form['civility_debit']['value'])){ echo ($payment_method_form['civility_debit']['value']=='Mr')?"selected":"";} ?> value="Mr">Mr</option>
                              <option <?php  if(isset($payment_method_form['civility_debit']['value']) && !empty($payment_method_form['civility_debit']['value'])){ echo ($payment_method_form['civility_debit']['value']=='Mrs')?"selected":"";} ?> value="Mrs">Mrs</option>

                            </select>

                            <i class="fa fa-check-circle"></i>
                            <i class="fa fa-exclamation-circle"></i>
                            <small>
                             <?php
                             if(isset($payment_method_form['civility_debit']['error']) && !empty($payment_method_form['civility_debit']['error'])){     echo strip_tags($payment_method_form['civility_debit']['error']);
                             }
                             ?>
                            </small>
                           </div>  
                         </div>



                        <div class="col-md-4">
                          <div class="form-group cardform-control <?php if(isset($payment_method_form['fname_debit']['error'])){
                             echo (!empty($payment_method_form['fname_debit']['error']))?'errors':'success';
                           } ?> ">

                           <input type="text" name="fname_<?= $cardtype ?>" id="fname_<?= $cardtype ?>" class="form-control" placeholder="First Name" value="<?php
                           if(isset($payment_method_form['fname_debit']['value']) && !empty($payment_method_form['fname_debit']['value'])){     echo $payment_method_form['fname_debit']['value'];
                           }
                           ?>" >
                           <i class="fa fa-check-circle"></i>
                           <i class="fa fa-exclamation-circle"></i>
                           <small>
                             <?php
                             if(isset($payment_method_form['fname_debit']['error']) && !empty($payment_method_form['fname_debit']['error'])){     echo strip_tags($payment_method_form['fname_debit']['error']);
                           }
                           ?>
                           </small>
                          </div>  
                        </div>


                       <div class="col-md-4">
                        <div class="form-group cardform-control <?php if(isset($payment_method_form['lname_debit']['error'])){
                           echo (!empty($payment_method_form['lname_debit']['error']))?'errors':'success';
                         } ?> ">

                         <input type="text" name="lname_<?= $cardtype ?>" id="lname_<?= $cardtype ?>" class="form-control" placeholder="Last Name" value="<?php
                         if(isset($payment_method_form['lname_debit']['value']) && !empty($payment_method_form['lname_debit']['value'])){     echo $payment_method_form['lname_debit']['value'];
                         }
                         ?>" >
                         <i class="fa fa-check-circle"></i>
                         <i class="fa fa-exclamation-circle"></i>
                         <small>
                           <?php
                           if(isset($payment_method_form['lname_debit']['error']) && !empty($payment_method_form['lname_debit']['error'])){     echo strip_tags($payment_method_form['lname_debit']['error']);
                         }
                         ?>
                         </small>
                     </div>  
                    </div>
                  </div>

                  <div class="row" style="padding-bottom: 20px;">
                    <div class="col-md-12">
                      <div class="form-group cardform-control <?php if(isset($payment_method_form['bankname_debit']['error'])){
                         echo (!empty($payment_method_form['bankname_debit']['error']))?'errors':'success';
                         } ?> ">

                         <input type="text" name="bankname_<?= $cardtype ?>" id="bankname_<?= $cardtype ?>" class="form-control" placeholder="Bank Name" value="<?php
                         if(isset($payment_method_form['bankname_debit']['value']) && !empty($payment_method_form['bankname_debit']['value'])){     echo $payment_method_form['bankname_debit']['value'];
                       }
                       ?>" >
                       <i class="fa fa-check-circle"></i>
                       <i class="fa fa-exclamation-circle"></i>
                       <small>
                         <?php
                         if(isset($payment_method_form['bankname_debit']['error']) && !empty($payment_method_form['bankname_debit']['error'])){     echo strip_tags($payment_method_form['bankname_debit']['error']);
                       }
                       ?>
                      </small>
                    </div>  
                  </div>
                </div>


                 <div class="row" style="padding-bottom: 20px;">
                  <div class="col-md-12">
                    <div class="form-group cardform-control <?php if(isset($payment_method_form['agencyname_debit']['error'])){
                       echo (!empty($payment_method_form['agencyname_debit']['error']))?'errors':'success';
                       } ?> ">

                       <input type="text" name="agencyname_<?= $cardtype ?>" id="agencyname_<?= $cardtype ?>" class="form-control" placeholder="Agency Name" value="<?php
                       if(isset($payment_method_form['agencyname_debit']['value']) && !empty($payment_method_form['agencyname_debit']['value'])){     echo $payment_method_form['agencyname_debit']['value'];
                     }
                     ?>" >
                     <i class="fa fa-check-circle"></i>
                     <i class="fa fa-exclamation-circle"></i>
                     <small>
                       <?php
                       if(isset($payment_method_form['agencyname_debit']['error']) && !empty($payment_method_form['agencyname_debit']['error'])){     echo strip_tags($payment_method_form['agencyname_debit']['error']);
                     }
                     ?>
                     </small>
                    </div>  
                 </div>
                </div>



                  <div class="row" style="padding-bottom: 20px;">
                    <div class="col-md-12">
                      <div class="form-group cardform-control <?php if(isset($payment_method_form['agencyaddr_debit']['error'])){
                         echo (!empty($payment_method_form['agencyaddr_debit']['error']))?'errors':'success';
                       } ?> ">

                       <input type="text" name="agencyaddr_<?= $cardtype ?>" id="agencyaddr_<?= $cardtype ?>" class="form-control" placeholder="Agency Addresse" value="<?php
                       if(isset($payment_method_form['agencyaddr_debit']['value']) && !empty($payment_method_form['agencyaddr_debit']['value'])){     echo $payment_method_form['agencyaddr_debit']['value'];
                       }
                       ?>" >
                       <i class="fa fa-check-circle"></i>
                       <i class="fa fa-exclamation-circle"></i>
                       <small>
                         <?php
                         if(isset($payment_method_form['agencyaddr_debit']['error']) && !empty($payment_method_form['agencyaddr_debit']['error'])){     echo strip_tags($payment_method_form['agencyaddr_debit']['error']);
                       }
                       ?>
                       </small>
                    </div>  
                  </div>
                </div>



                <div class="row" style="padding-bottom: 20px;">
                  <div class="col-md-12">
                    <div class="form-group cardform-control <?php if(isset($payment_method_form['otheraddr_debit']['error'])){
                       echo (!empty($payment_method_form['otheraddr_debit']['error']))?'errors':'success';
                     } ?> ">

                     <input type="text" name="otheraddr_<?= $cardtype ?>" id="otheraddr_<?= $cardtype ?>" class="form-control" placeholder="Addresse" value="<?php
                     if(isset($payment_method_form['otheraddr_debit']['value']) && !empty($payment_method_form['otheraddr_debit']['value'])){     echo $payment_method_form['otheraddr_debit']['value'];
                   }
                   ?>" >
                   <i class="fa fa-check-circle"></i>
                   <i class="fa fa-exclamation-circle"></i>
                  <small>
                     <?php
                     if(isset($payment_method_form['otheraddr_debit']['error']) && !empty($payment_method_form['otheraddr_debit']['error'])){     echo strip_tags($payment_method_form['otheraddr_debit']['error']);
                   }
                   ?>
                  </small>
                  </div>  
                </div>
              </div>



               <div class="row" style="padding-bottom: 20px;">

               <div class="col-md-6">
                  <div class="form-group cardform-control <?php if(isset($payment_method_form['agencyzipcode_debit']['error'])){
                     echo (!empty($payment_method_form['agencyzipcode_debit']['error']))?'errors':'success';
                   } ?> ">

                   <input type="text" name="agencyzipcode_<?= $cardtype ?>" id="agencyzipcode_<?= $cardtype ?>" class="form-control" placeholder="Zip Code" value="<?php
                   if(isset($payment_method_form['agencyzipcode_debit']['value']) && !empty($payment_method_form['agencyzipcode_debit']['value'])){     echo $payment_method_form['agencyzipcode_debit']['value'];
                  }
                  ?>" >
                  <i class="fa fa-check-circle"></i>
                  <i class="fa fa-exclamation-circle"></i>
                  <small>
                   <?php
                   if(isset($payment_method_form['agencyzipcode_debit']['error']) && !empty($payment_method_form['agencyzipcode_debit']['error'])){     echo strip_tags($payment_method_form['agencyzipcode_debit']['error']);
                  }
                  ?>
                  </small>
                </div>  
              </div>

              <div class="col-md-6">
                <div class="form-group cardform-control <?php if(isset($payment_method_form['agencycity_debit']['error'])){
                   echo (!empty($payment_method_form['agencycity_debit']['error']))?'errors':'success';
                 } ?> ">

                 <input type="text" name="agencycity_<?= $cardtype ?>" id="agencycity_<?= $cardtype ?>" class="form-control" placeholder="City" value="<?php
                 if(isset($payment_method_form['agencycity_debit']['value']) && !empty($payment_method_form['agencycity_debit']['value'])){     echo $payment_method_form['agencycity_debit']['value'];
                }
                ?>" >
                <i class="fa fa-check-circle"></i>
                <i class="fa fa-exclamation-circle"></i>
                <small>
                 <?php
                 if(isset($payment_method_form['agencycity_debit']['error']) && !empty($payment_method_form['agencycity_debit']['error'])){     echo strip_tags($payment_method_form['agencycity_debit']['error']);
                }
                ?>
                </small>
                </div>  
              </div>

            </div>


            <div class="row" style="padding-bottom: 20px;">
              <div class="col-md-12">
                <div class="form-group cardform-control <?php if(isset($payment_method_form['ibnnum_debit']['error'])){
                   echo (!empty($payment_method_form['ibnnum_debit']['error']))?'errors':'success';
                 } ?> ">

                 <input type="text" name="ibnnum_<?= $cardtype ?>" id="ibnnum_<?= $cardtype ?>" class="form-control" placeholder="IBAN Number" value="<?php
                 if(isset($payment_method_form['ibnnum_debit']['value']) && !empty($payment_method_form['ibnnum_debit']['value'])){     echo $payment_method_form['ibnnum_debit']['value'];
               }
               ?>" >
               <i class="fa fa-check-circle"></i>
               <i class="fa fa-exclamation-circle"></i>
               <small>
                 <?php
                 if(isset($payment_method_form['ibnnum_debit']['error']) && !empty($payment_method_form['ibnnum_debit']['error'])){     echo strip_tags($payment_method_form['ibnnum_debit']['error']);
               }
               ?>
              </small>
              </div>  
            </div>
          </div>


          <div class="row" style="padding-bottom: 20px;">
            <div class="col-md-12">
              <div class="form-group cardform-control <?php if(isset($payment_method_form['swiftcode_debit']['error'])){
                   echo (!empty($payment_method_form['swiftcode_debit']['error']))?'errors':'success';
                 } ?> ">

                 <input type="text" name="swiftcode_<?= $cardtype ?>" id="swiftcode_<?= $cardtype ?>" class="form-control" placeholder="Swift Code" value="<?php
                 if(isset($payment_method_form['swiftcode_debit']['value']) && !empty($payment_method_form['swiftcode_debit']['value'])){     echo $payment_method_form['swiftcode_debit']['value'];
               }
               ?>" >
               <i class="fa fa-check-circle"></i>
               <i class="fa fa-exclamation-circle"></i>
               <small>
                 <?php
                 if(isset($payment_method_form['swiftcode_debit']['error']) && !empty($payment_method_form['swiftcode_debit']['error'])){     echo strip_tags($payment_method_form['swiftcode_debit']['error']);
               }
               ?>
              </small>
            </div>  
          </div>
        </div>



          <div class="row" style="padding-bottom: 20px;">
            <div class="col-md-12">
             <div class="col-md-1" style="padding:0px;width: 4% !important;">
              <div class="form-group cardform-control <?php if(isset($payment_method_form['agreement_debit']['error'])){
               echo (!empty($payment_method_form['agreement_debit']['error']))?'errors':'success';
               } ?> ">

               <input class="agreement_check" id="agreement_<?= $cardtype ?>" type="checkbox" name="agreement_<?= $cardtype ?>" value="1" <?php  if(isset($payment_method_form['agreement_debit']['value']) && !empty($payment_method_form['agreement_debit']['value'])){ echo ($payment_method_form['agreement_debit']['value']=='1')?"checked":"";} ?> style="margin:6px 0px;" >   

               <small>
                <?php
                if(isset($payment_method_form['agreement_debit']['error']) && !empty($payment_method_form['agreement_debit']['error'])){     echo strip_tags($payment_method_form['agreement_debit']['error']);
              }
              ?>
              </small>  
            </div>
            </div>  
            <div class="col-md-11" style="padding:0px;width: 96% !important;">
              <p style="font-size: 13px;">I allow company under to debit my account every time with amount of my bills.</p>
            </div>
          </div>
        </div>



          <div class="row" style="padding-bottom: 20px;">
            <div class="col-md-12">
              <p style="font-size:13px !important;"><?php echo strtoupper($companydata->name).' '.strtoupper($companydata->type).'<br>'.$companydata->address2.'<br>'.$companydata->zip_code.' '.$companydata->city;?></p>


            </div>
          </div>

  </div>
</div>