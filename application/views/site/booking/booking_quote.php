<?php
 $passenger_form =  $this->session->flashdata('passenger_error');
$booking=$this->session->userdata['bookingdata'];
$bookingregular=$this->session->userdata['bookingpickregular'];
$bookingreturn=$this->session->userdata['bookingreturnregular'];
$client=$this->session->userdata['user'];
$displaypassengers=$this->session->userdata['passengers'];
$stopsdata=$this->session->userdata['stops'];


$map= $this->session->userdata['mapdatainfo'];
if(!empty($booking['pickupdate'])){
 $bookingdate=str_replace('/', '-', $booking['pickupdate']);
}
else{
  $bookingdate=str_replace('/', '-', $booking['startdate']);
}
 $pickupaddress1=str_replace((', '.$booking['dropcity']), "", $pickupaddress);
 $dropoffaddress1=str_replace((', '.$booking['dropcity']), "", $dropoffaddress);
?>
</header>
<div class="container">
<div class="row">
    <div class="body-container body-border"><!--padding-p-0 -->
        <div id="breadcrumb">
            <a href="<?php echo site_url(); ?>/" style="color:#4ca6cf;"><i class="fa fa-home"></i> Home </a>
            <a href="javascript:void(0)">&nbsp;<?php echo $page_title ?> </a>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-wizard">
                    <li><span class="steps">1</span><a style="color:#fff;">Journey Details</a></li>
                    <li><span class="steps">2</span ><a style="color:#fff;">Identification</a></li>
                    <li class="active"><span class="steps">3</span><a>Quote</a></li>
                    <li><span class="steps">4</span><a>Payment Details</a></li>
                </ul>
            </div>  
        </div>
        <div class="row booknow-content">
            <div class="col-md-12 online">
                <div class="booking_head">
                    Booking Number : <?=create_timestampdmy_uid($bookingdate,($booking['bookingid']+1));?>  </div>

                <div id="bookinginformationlistdiv">
                    <div class="col-md-7" style="margin-top:15px;">
                        <div class="row">
                          <div class="col-md-12" style="padding: 0px;">
                           <div class="col-md-4" style="padding: 0px">
                          
                            <p style="font-size:13px !important;"><span style="margin-right:5px;font-weight: 900;">Client : </span><br>
                              <?php echo "<span style='font-weight:900;'>".$client->civility.' '.$client->first_name.' '.$client->last_name.'</span><br>'.$client->address.'<br>';?>
                             <?php if(!empty($client->address1) || $client->address1 != null):  ?>
                              <?php echo $client->address1.'<br>'; ?>
                             <?php endif; ?>
                             <?php echo $client->zipcode.'  '.$client->city;?></p>
                           </div>
                            <div class="col-md-8 " style="padding: 0px;">
                                  
                                
                                  <div class="col-md-12" style="padding: 0px;width: 87%;margin-left: 13%;">
                                     <div class="col-md-6" style="padding:0px;width: 54%;">
                                      <div class="col-md-12" style="padding:0px;">
                                      <span style="font-weight: 900;">Passengers :</span>
                                          <select name="passengerfield" id="passengers" style="text-align: center;height: 36px !important;" class="fa fa-user">
                                           <option value="">&#xf007; Passengers</option>
                                     
                                           <?php foreach($passengers as $passenger): ?>
                                             
                                           <option value="<?= $passenger->id ?>"> &#xf007;  <?php echo $passenger->civility.' '.$passenger->fname.' '.$passenger->lname; ?></option>
                                           <?php  endforeach; ?>
                                          </select>
                                         
                                      </div>
                                       <div class="col-md-12" style="padding:0px;">
                                          <?php  if(isset($passenger_form['error']) && !empty($passenger_form['error'])): ?>
                                          <span style="color:red;font-weight: 900;"><?= strtoupper( $passenger_form['error']); ?></span>
                                        <?php endif; ?>
                                       </div>
                                     </div>
                                    <div class="col-md-2" style="padding-left: 10px;margin-top: 35px;width: 12.66%;">
                                     <button type="button" class="plusgreeniconconfig"  onclick="addpassengers()"><i class="fa fa-plus"></i></button>
                                      </div>
                                       <div class="col-md-4" style="margin-top: 28px;position: relative;padding: 0px;">
                                       <button type="button" id="createpassengerid"  class="btn bookingquotecreatebtn" style="width: 100%;" onclick="showpassengermodal();">Create New</button>
                                     
                                    </div>
                                  </div>
                                  <div class="col-md-12" style="padding:0px;">
                                      <!-- client passengers add modal -->
                  <div  id="clientpassengeraddsection" >
                      <div>
                        <div class="modal-content" id="clientpassengercontentsection">
                        
                          <!-- Modal Header -->
                          <div class="modal-header">
                            <h4 class="modal-title" style="float:left;font-size:18px;font-weight:900;">+ Create New Passenger</h4>
                          
                           <button type="button" class="close-passenger-btn" onclick="hidepassengermodal()"><i class="fa fa-times-circle-o"></i></button>
                          </div>
                          <form action="<?=site_url('add_client_passenger')?>" id="addclientform">
                          <!-- Modal body -->
                          <div class="modal-body">    
                               <div class="row" id="client_form_error" style="display: none;">
                               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                  <div style="padding: 5px 12px" class="alert alert-danger">
                                      <strong>Error: </strong>Something went wrong.
                                      <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                      </button>
                                  </div>
                                </div> 
                         </div>
                         <div class="row" id="client_form_success" style="display: none;">
                               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                  <div style="padding: 5px 12px" class="alert alert-success">
                                      <strong>Success: </strong>The Passenger has been added.
                                      <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                      </button>
                                  </div>
                                </div> 
                         </div>
                         <div class="row">
                             <div class="col-md-12">
                              <div class="col-md-1" style="padding-right:0px;width:6% !important;">
                                <input type="checkbox" style="float:none;margin:0px;padding:0px;" onchange="addclientaspassenger(this);">
                              </div>
                              <div class="col-md-11">
                                <span style="font-weight:900;">Client is himself a passenger</span>
                              </div>
                           </div>
                          </div>    
                             <div class="row">
                             <div class="col-md-12">
                            <div class="col-md-4" style="margin-top: 5px;">
                              <div class="form-group clientform_control"> 
                                  <span> Civility </span>                                
                                  <select class="form-control" name="civility" id="client_civility"  style="height: 40px;">
                                      <option value="Mr">Mr</option>
                                      <option value="Mrs">Mrs</option>
                                  </select> 
                                    <i class="fa fa-check-circle"></i>
                                    <i class="fa fa-exclamation-circle"></i>
                                    <small>Error</small>
                              </div>
                            </div>
                             <div class="col-md-4" style="margin-top: 5px;">
                              <div class="form-group clientform_control">
                                <span>First Name</span>
                                  <input type="text" class="form-control" name="fname" id="client_fname" placeholder="" value="" >
                                    <i class="fa fa-check-circle"></i>
                                    <i class="fa fa-exclamation-circle"></i>
                                    <small>Error</small>
                              </div>
                            </div>
                            <div class="col-md-4" style="margin-top: 5px;">
                              <div class="form-group clientform_control">
                                <span>Last Name</span>
                                  <input type="text" class="form-control" name="lname" id="client_lname" placeholder="" value="" >
                                    <i class="fa fa-check-circle"></i>
                                    <i class="fa fa-exclamation-circle"></i>
                                    <small>Error</small>
                              </div>
                            </div>
                          </div>
                          </div>
                          <div class="row">
                          <div class="col-md-12">
                             <div class="col-md-4" style="margin-top: 5px;">
                              <div class="form-group clientform_control">
                                <span>Mobile Phone</span>
                                  <input type="text" class="form-control" name="mobilephone" id="client_mobile" placeholder="" value="" >
                                    <i class="fa fa-check-circle"></i>
                                    <i class="fa fa-exclamation-circle"></i>
                                    <small>Error</small>
                              </div>
                            </div>
                             <div class="col-md-4" style="margin-top: 5px;">
                              <div class="form-group clientform_control">
                                <span>Home Phone</span>
                                  <input type="text" class="form-control" name="homephone" id="client_home" placeholder="" value="" >
                                    <i class="fa fa-check-circle"></i>
                                    <i class="fa fa-exclamation-circle"></i>
                                    <small>Error</small>
                              </div>
                            </div>

                            <div class="col-md-4" style="margin-top: 5px;">
                              <div class="form-group clientform_control">
                                <span>Category of Disable</span>
                                <select class="form-control"  name="disablecatid" id="client_disable"  style="height: 40px;">
                                  <option value="">Select</option>
                                  <?php foreach ($disablecategories as $disable): ?>
                                    <option value="<?= $disable->id;  ?>"><?= $disable->name_of_disablecategory;  ?></option>
                                  <?php endforeach; ?>

                                </select>
                                  <i class="fa fa-check-circle"></i>
                                    <i class="fa fa-exclamation-circle"></i>
                                    <small>Error</small>
                              </div>
                            </div>
                          </div>
                          </div>
                       
                            
                          </div>
                          
                          <!-- Modal footer -->
                          <div class="modal-footer" style="padding:15px 15px 5px 15px;">
                             <button type="button"  class="btn bookingquotecreatebtn" onclick="hidepassengermodal()" style="height: 34px;font-size: 14px;padding: 1px 21px 3px;">Cancel</button>
                            <button type="submit" class="btn bookingquotecreatebtn" style="height: 34px;">Save</button>
                          </div>

                          </form>
                          
                        </div>
                      </div>
                    </div>
<!-- end client passengers modal -->
                                  </div>
                                  <div class="col-md-12" style="padding:0px;margin-top:10px;width: 87%;margin-left: 13%;">

                                       <ul class="ulbdnone" id="passengerdiv" >
                                        
                                          <?php foreach($displaypassengers as $passenger): ?>
                                           <li class="text-left" style="position: relative;"><span class="fa fa-user" style="margin-right:5px;"></span><span><?php echo $passenger['name'];?></span>, <span class="fa fa-mobile" style="margin-right:5px;"></span><span><?php echo $passenger['phone'];?></span>, <span class="fa fa-phone" style="margin-right:5px;"></span><span><?php echo $passenger['home'];?></span>, <span class="fa fa-wheelchair" style="margin-right:5px;"></span><span><?= $passenger['disable'] ?></span>
                                           
                                                 <button type="button" class="minusredicon" onclick="removepassengers(<?= $passenger['id'] ?>)"><i class="fa fa-minus"></i></button>
                                          </li>

                                          <?php endforeach; ?>    
                                           
                                       </ul>

                                    </div>
                         
                                
                                </div>
                            </div>
                           
 <!-- Booking Details  Start --> 
 <?php  include 'bookingdetails.php';?> 
 <!-- Booking Details End -->                        

                       
 </div>
</div>
                
                  
                  
                    <div class="col-md-5" style="margin-top:15px;padding-right:0px;">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="map-wrapper" >
                                  <div id="mapheader" style="border-top-left-radius: 15px;border-top-right-radius: 15px;"><h4>Ride Map View</h4></div>
                                   <?php for($i=0;$i<=count($stopsdata);$i++): ?>
                                      <input type="hidden" id="stopaddr_<?= $i+1; ?>" value="<?= $stopsdata[$i]['stopaddress']; ?>">
                                   <?php endfor; ?>
                                   <input type="hidden" id="stoppercount" value="<?= count($stopsdata); ?>">
                                    <input type="hidden" id="pickupaddress" value="<?= $pickupaddress ?>">
                                    <input type="hidden" id="dropoffaddress" value="<?= $dropoffaddress ?>">
                                    <input type="hidden" id="pickotheraddress" value="<?= $booking['otheraddress'] ?>">
                                     <input type="hidden" id="dropotheraddress" value="<?= $booking['dropotheraddress'] ?>">
                                  

                                     <div id="mapsearchloader" style="position: absolute;top: 50%;left: 45%;z-index: 1200;display: none;"> 
                                            <img src="<?= base_url()?>assets/theme/default/images/bookloader.gif" >
                                      </div>
                                      <?php if($pricestatus_data!=null && $pricestatus_data->pricestatus==0 ): ?>
                                         <div id="map" >
                                      <?php else: ?>
                                         <div id="map">
                                      <?php endif; ?>
                                   
                                    </div>
                                     
                                </div>
                             </div>  
                             <?php if($pricestatus_data!=null && $pricestatus_data->pricestatus==0 ): ?>
                          <div class="col-md-12" style="margin-top: 46px;">
                             <button id="distancebtn" class="btn grbtn brz" style="border-bottom-left-radius: 15px;text-align:left;width: 50%;font-size: 12px !important;">Estimated Distance : <?= $map['distance'] ?> km</button>
                             <button id="timebtn" class="btn grbtn blz" style="border-bottom-right-radius: 15px;width:50%;text-align: right;font-size: 12px !important;overflow: hidden;">Estimated Time : <?= str_replace('min', 'minute', $map['time']) ?> </button>
                             
                           </div>
                  <div class="col-md-12" style="margin-top: 10px;">     
                         <div>
                            <div>
                            
                                <div class="pricemessagediv">
                            

                                   <p><?= strip_tags($pricestatus_data->message); ?></p>

                                       
                                   
                                </div>
                           
                        </div>
                    </div>
                  </div>
                         
                         <?php else: ?>
                            <div class="col-md-12" style="margin-top: 46px;">
                             <button id="distancebtn" class="btn grbtn brz" style="border-bottom-left-radius: 15px;text-align:left;">Estimated Distance : <?= $map['distance'] ?> km</button>
                             <button id="timebtn" class="btn grbtn brz blz" style="">Estimated Time : <?= str_replace('min', 'minute', $map['time']) ?> </button>
                              <button class="btn grbtn blz" id="bookingpricedivbtn" style="border-bottom-right-radius: 15px;text-align:right;">Estimated Price : <?= $map['price'] ?> € HT</button>
                           </div>
                      
                         
                      
                          <div class="col-md-12" >
                             <!-- Fees -->
                           <div class="col-md-12  clearfix amoutndetaildiv" >

                              <div class="col-md-5" style="padding: 0px;">
                                <div class="dd1_amount">
                                 <span style="font-weight:900;">Fees</span>
                                </div>
                             </div>
                               <div class="col-md-3" style="padding: 0px;text-align: center;">
                                  <span style="font-weight:900;">%</span>
                               </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span style="font-weight:900;">Amount</span>
                             </div>
                           </div>
                              
                            </div>
                              <?php if($map['drivermealfee']!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                             <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Driver Meal Fee :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align:center;">
                             
                                <span></span>
                             
                              </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $map['drivermealfee'];?> €</span>
                             </div>  
                             </div>       
                            </div>
                          <?php endif; ?>
                            <?php if($map['driverrestfee']!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                             <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Driver Rest Fee :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align:center;">
                             
                                <span></span>
                             
                              </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $map['driverrestfee'];?> €</span>
                             </div>  
                             </div>       
                            </div>
                          <?php endif; ?>
                           <?php if($map['nighttimefee']!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                             <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Night Fee :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align:center;">
                              <?php if($map['nighttimediscount']!=0):  ?>
                                <span><?php echo $map['nighttimediscount'];?>%</span>
                              <?php endif; ?>
                              </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $map['nighttimefee'];?> €</span>
                             </div>  
                             </div>       
                            </div>
                          <?php endif; ?>

                            <?php if($map['notworkingdayfee']!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                             <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Sunday And Not Working Day Fee :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;">
                              <?php if($map['notworkingdaydiscount']!=0):  ?>
                                <span><?php echo $map['notworkingdaydiscount'];?>%</span>
                              <?php endif; ?>
                             </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $map['notworkingdayfee'];?> €</span>
                             </div>  
                             </div>       
                            </div>
                          <?php endif; ?>


                           <?php if($map['maydecdayfee']!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                             <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>1st Mai And 25 December Fee :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;">
                               <?php if($map['maydecdaydiscount']!=0):  ?>
                                <span><?php echo $map['maydecdaydiscount'];?>%</span>
                              <?php endif; ?>
                             </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $map['maydecdayfee'];?> €</span>
                             </div>   
                             </div>      
                            </div>
                          <?php endif; ?>
                          <!-- Fees -->
                             <?php if($map['vatdiscount']!=0): ?>
                              <div class="col-md-12  clearfix amoutndetaildiv" >

                              <div class="col-md-5" style="padding: 0px;">
                                <div class="dd1_amount">
                                 <span style="font-weight:900;">Vat</span>
                                </div>
                             </div>
                               <div class="col-md-3" style="padding: 0px;text-align: center;">
                                  <span style="font-weight:900;">%</span>
                               </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span style="font-weight:900;">Amount</span>
                             </div>
                           </div>
                              
                            </div>

                            <div class="col-md-12 clearfix amoutndetaildiv" >
                               <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span><?= $map['vatname'] ?> : </span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $map['vatdiscount'];?>%</span></div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>+   <?php echo $map['vatfee'];?> €</span>
                             </div>
                           </div>  
                            </div>
                             <?php endif; ?>
                             <div class="col-md-12  clearfix amoutndetaildiv" >

                              <div class="col-md-5" style="padding: 0px;">
                                <div class="dd1_amount">
                                 <span style="font-weight:900;">Discounts</span>
                                </div>
                             </div>
                               <div class="col-md-3" style="padding: 0px;text-align: center;">
                                  <span style="font-weight:900;">%</span>
                               </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span style="font-weight:900;">Amount</span>
                             </div>
                           </div>
                              
                            </div>
                         


                      

                         
                          <?php if($map['welcomediscount']!=0): ?>
                           <div class="col-md-12 clearfix amoutndetaildiv" >
                            <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Welcome Discount :</span>
                              </div>
                            </div>
                            <div class="col-md-3" style="padding: 0px; text-align: center;">
                             
                               <span><?php echo $map['welcomediscount'];?>%</span>
                             
                            </div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>-  <?php echo $map['welcomediscountfee'];?> €</span>
                             </div>
                           </div>
                              
                            </div>
                          <?php endif; ?>
                        
                           <?php if($map['rewarddiscount']!=0): ?>
                            <div class="col-md-12 clearfix amoutndetaildiv" >
                            <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Reward Discount :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $map['rewarddiscount'];?>%</span></div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>-  <?php echo $map['rewarddiscountfee'];?> €</span>
                             </div>
                           </div>
                              
                            </div>
                             <?php endif; ?>
                           <?php if($map['perioddiscount']!=0): ?>
                            <div class="col-md-12 clearfix amoutndetaildiv" >
                               <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Period Discount :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $map['perioddiscount'];?>%</span></div>
                             <div class="col-md-4" style="padding: 0px;">
                              <div class="dd2_amount">
                               <span>-   <?php echo $map['perioddiscountfee'];?> €</span>
                             </div>
                           </div>
                              
                            </div>
                             <?php endif; ?>
                             <div class="col-md-12 clearfix amoutndetaildiv" id="promo_code_div" >
                               <?php if($map['promocodediscount']==0): ?>
                               <div class="col-md-4" style="padding: 5px 0px 5px 0px;">Discount Code : </div> 
                              <div class="col-md-6" style="padding: 5px 0px 5px 0px;">
                               <input type="password" placeholder="discount code" id="promo_code" class="form-control" style="height:35px !important;">
                             </div>
                             <div class="col-md-2" style="padding: 5px 0px 5px 5px;">
                               <button type="button" class="btn bookingquoteapplybtn" onclick="getpromocodediscount()">APPLY</button>
                              </div>
                              <?php else: ?>
                            <div class="col-md-5" style="padding: 0px;">
                              <div class="dd1_amount">
                               <span>Code Discount :</span>
                              </div>
                            </div>
                             <div class="col-md-3" style="padding: 0px;text-align: center;"><span><?php echo $map['promocodediscount'];?>%</span></div>
                              <div class="col-md-4" style="padding: 0px;">
                               <div class="dd2_amount">
                                 <span>-   <?php echo $map['promocodediscountfee'];?> €</span>
                               </div>
                             </div>
                            <?php endif; ?>
                             
                              
                            </div>
                            
                           <div class="col-md-12 clearfix grdiv amountdetaillastdiv">
                             <div class="dd1_amount">
                               <span style="font-weight: 900;">Price To Pay</span>
                              </div>
                              <div class="dd2_amount">
                               <span style="font-weight: 900;"><?php echo $map['totalprice'];?> € TTC</span>
                             </div>
                           </div>

                          </div>
                                   <?php endif; ?>
                         
                          
                        </div>
                         
                    </div>
                </div>
            </div>
   </div>
        
     
        <div class="clearfix"></div>
       
        <div class="online">
            <div class="col-md-12" style="margin-bottom: 10px;padding:0px;">
                <div class="col-md-6">
                        <a href="<?php echo base_url()?>booking.php" class="btn btn-default extra-pad" style="font-weight:900;margin-left: -15px;"> <i class="fa fa-arrow-circle-o-left"></i> Back To Journey Details  </a>
                </div>
                <div class="col-md-6">
                    <a  href="<?php echo base_url()?>booking_payment.php" class="btn btn-default extra-pad right" id="nextpaymentdetailsbtns">Next Payment Details <i class="fa fa-arrow-circle-o-right"></i> </a> 
                </div>
           </div>
       </div>
        <div class="clearfix"></div>
    </div>
  </div>
</div>

<script type="text/javascript">
const form = document.getElementById('addclientform');
const client_civility  = document.getElementById('client_civility');
const client_fname = document.getElementById('client_fname');
const client_lname = document.getElementById('client_lname');
const client_mobile = document.getElementById('client_mobile');
const client_home = document.getElementById('client_home');
const client_disable = document.getElementById('client_disable');
form.addEventListener('submit', e => {
  e.preventDefault();
  
    var valid=checkInputs();
    if(valid){
    
     // e.currentTarget.submit();
      var client_civilityValue = client_civility.value.trim();
      var client_fnameValue = client_fname.value.trim();
      var client_lnameValue = client_lname.value.trim();
      var client_mobileValue = client_mobile.value.trim();
      var client_homeValue = client_home.value.trim();
      var client_disableValue = client_disable.value.trim();
      $.ajax({
                type: "GET",
                url: '<?php echo base_url().'add_client_passenger'; ?>',
                data: {'civility': client_civilityValue,'fname':client_fnameValue,'lname':client_lnameValue,'mobile':client_mobileValue,
                'home':client_homeValue,'disable':client_disableValue},
                success: function (data) {
                  if(data.result=='200'){
                       $('.clientform_control').removeClass('success');
                        $('.clientform_control input').val('');
                        $("#client_form_error").hide();
                       $("#client_form_success").show();
                      $('#passengers').empty();
                      $("#passengers").append('<option value="">&#xf007; Passengers</option>');
                      $.each(data.passengers, function(k, v) {

                       $("#passengers").append('<option value="'+v['id']+'">&#xf007; '+v['civility']+' '+v['fname']+' '+v['lname']+'</option>');
                       });
                  }else{
                    $('.clientform_control').removeClass('success');
                    $('.clientform_control input').val('');
                    $('#passengers').empty();
                     $("#client_form_success").hide();
                    $("#client_form_error").show();
                  }
                 
                }
           });
          // e.currentTarget.submit();
    }
 });


function checkInputs() {
  
  var validate=true;
  // trim to remove the whitespaces
  const client_civilityValue = client_civility.value.trim();
  const client_fnameValue = client_fname.value.trim();
  const client_lnameValue = client_lname.value.trim();
  const client_mobileValue = client_mobile.value.trim();
  const client_homeValue = client_home.value.trim();
  const client_disableValue = client_disable.value.trim();
 
  if(client_civilityValue === '') {
    setErrorFor(client_civility, 'REQUIRED');
    validate=false;
  } else {
    setSuccessFor(client_civility);
  }
  
  if(client_fnameValue === '') {
    setErrorFor(client_fname, 'REQUIRED');
    validate=false;
  } else {
    setSuccessFor(client_fname);
  }
  
  if(client_lnameValue === '') {
    setErrorFor(client_lname, 'REQUIRED');
    validate=false;
  } else {
    setSuccessFor(client_lname);
  }
  
  if(client_mobileValue === '') {
    setErrorFor(client_mobile, 'REQUIRED');
    validate=false;
  } else{
    setSuccessFor(client_mobile);
  }

   if(client_homeValue === '') {
    setErrorFor(client_home, 'REQUIRED');
    validate=false;
  } else{
    setSuccessFor(client_home);
  }
  if(client_disableValue === '') {
    setErrorFor(client_disable, 'REQUIRED');
    validate=false;
  } else{
    setSuccessFor(client_disable);
  }
  return validate;
}
function setErrorFor(input, message) {
  const formControl = input.parentElement;
  const small = formControl.querySelector('small');
  formControl.className = 'clientform_control errors';
  small.innerText = message;
}

function setSuccessFor(input) {
  const formControl = input.parentElement;
  formControl.className = 'clientform_control success';
}
 
function addclientaspassenger(e){
   if($(e).is(":checked")) {
     $.ajax({
                type: "GET",
                url: '<?php echo base_url().'add_client_aspassenger'; ?>',
                success: function (data) {
                  if(data.result=='200'){
                      client_civility.value=data.client['civility'];
                      client_fname.value=data.client['f_name'];
                      client_lname.value=data.client['l_name'];
                      client_mobile.value=data.client['mobile'];
                      client_home.value=data.client['phone'];
                      
                   
                  }else{
                   alert("something went wrong");
                  }
                 
                }
           });
   }else{
                      client_civility.value="Mr";
                      client_fname.value="";
                      client_lname.value="";
                      client_mobile.value="";
                      client_home.value="";
   }
                   
 }

function addpassengers()
{
    var count=0;
    var val = $('#passengers').val();
    if (val!='')
    {
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'savepassenger'; ?>',
                data: {'passenger_id': val},
                success: function (data) {
                   $('#passengerdiv').empty();
                      $.each(data.passengers, function(k, v) {

                       $("#passengerdiv").append('<li class="text-left"  style="position:relative;"><span class="fa fa-user" style="margin-right:5px;"></span><span>'+v['name']+'</span>, <span class="fa fa-mobile" style="margin-right:5px;"></span><span>'+v['phone']+'</span>, <span class="fa fa-phone" style="margin-right:5px;"></span><span>'+v['home']+'</span>, <span class="fa fa-wheelchair" style="margin-right:5px;"></span><span>'+v['disable']+'</span><button type="button" class="minusredicon" onclick="removepassengers('+v['id']+')"><i class="fa fa-minus"></i></button></li>');
                       });
                }
                });
    }
 }   
function removepassengers(val)
{
    var count=0;
    if (val!='')
    {
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'removepassenger'; ?>',
                data: {'passenger_id': val},
                success: function (data) {
                   $('#passengerdiv').empty();
                   if(data.result =='200'){

                      $.each(data.passengers, function(k, v) {

                       $("#passengerdiv").append('<li class="text-left" style="position:relative;"><span class="fa fa-user" style="margin-right:5px;"></span><span>'+v['name']+'</span>, <span class="fa fa-mobile" style="margin-right:5px;"></span><span>'+v['phone']+'</span>, <span class="fa fa-phone" style="margin-right:5px;"></span><span>'+v['phone']+'</span>, <span class="fa fa-wheelchair" style="margin-right:5px;"></span><span>'+v['disable']+'</span></span><button type="button" class="minusredicon" onclick="removepassengers('+v['id']+')"><i class="fa fa-minus"></i></button></li>');
                       });
                   }
                  
                   
                }
                });
    }
 }
 
function showpassengermodal(){
   $("#client_form_success").hide();
   $("#client_form_error").hide();
   if($("#clientpassengeraddsection").is(":visible")){
     $("#clientpassengeraddsection").hide();
   }
    else{
       $("#clientpassengeraddsection").show();
    }
 
}
function hidepassengermodal(){
  $("#clientpassengeraddsection").hide();
}

function getpromocodediscount(){
   var count=0;
    var val = $('#promo_code').val();
    if (val!='')
    {
        $.ajax({
                type: "GET",
                url: '<?php echo base_url().'getpromodiscount'; ?>',
                data: {'promo_code': val},
                success: function (data) {
                   $('#promo_code_div').empty();
                   if(data.result=='200'){
                       
                    $('.amountdetaillastdiv .dd2_amount span').html(data.discount['totalprice']+'€');
                    $('#promo_code_div').append(' <div class="col-md-5" style="padding: 0px;"><div class="dd1_amount"><span>Code Discount :</span></div>  </div><div class="col-md-3" style="padding: 0px;text-align: center;"><span>'+data.discount['promocodediscount']+'%</span></div><div class="col-md-4" style="padding: 0px;"><div class="dd2_amount"><span>-  '+data.discount['promocodediscountfee']+' €</span></div></div>');
                     }
                    else{
                      alert("invalid discount code");
                      $('#promo_code_div').append('<div class="col-md-4" style="padding: 5px 0px 5px 0px;">Discount Code : </div><div class="col-md-6" style="padding: 5px 0px 5px 0px;"><input type="password" placeholder="discount code" id="promo_code" class="form-control" style="height:35px !important;"></div><div class="col-md-2" style="padding: 5px 0px 5px 5px;"><button type="button" class="btn bookingquoteapplybtn" onclick="getpromocodediscount()">APPLY</button></div>');
                    }


                     
                }
                });
    }
}
</script>

    <script>
    const pickupaddress= document.getElementById('pickupaddress').value;
     const dropoffaddress= document.getElementById('dropoffaddress').value;
     const pickotheraddress=document.getElementById('pickotheraddress').value;
     const dropotheraddress=document.getElementById('dropotheraddress').value;
     const stoppercount=document.getElementById('stoppercount').value;
     var faddr=1;
     var delay = 100;
     var nextAddress = 0;
     var stopper=new Array();    
     var addresses=new Array();
     var addresscoordinates= new Array();
     var alphaticlatter=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
     var alphaticnumber=0;
     var n=0;
       var symbolOne = {
          path: google.maps.SymbolPath.CIRCLE,
          strokeColor: "#F00",
          fillColor: "#F00",
          fillOpacity: 1
        };
        var symbolTwo = {
          path: google.maps.SymbolPath.CIRCLE,
          strokeColor: "#ff9600",
          fillColor: "#ff9600",
          fillOpacity: 1
        };
       
         var symbolThree = {
          path: google.maps.SymbolPath.CIRCLE,
          strokeColor: "#292",
          fillColor: "#292",
          fillOpacity: 1
        };
      var directionsDisplay;
      var directionsService = new google.maps.DirectionsService();
      var map;
      var bounds;
      var picklat,picklong,droplat,droplong;
       stopper.push(pickupaddress)
         for(var i=1;i<=stoppercount;i++){
          var currentaddr=document.getElementById('stopaddr_'+i).value
          stopper.push(currentaddr);
         }
         stopper.push(dropoffaddress);
         for(var j=0;j<(stopper.length-1);j++){
           addresses.push([stopper[j], stopper[j+1]]);
         }
     initMap();
     theNext();
    
    
     
   function initMap() {
    document.getElementById("mapsearchloader").style.display = "block";
    directionsDisplay = new google.maps.DirectionsRenderer();
    var basel = new google.maps.LatLng(41.850033, -87.6500523);
    var mapOptions = {
      zoom: 7,
      center: basel
    }
    map = new google.maps.Map(document.getElementById('map'), mapOptions);
    directionsDisplay.setMap(map);
    bounds = new google.maps.LatLngBounds();
   }
    
   async function calcRoute(start, end, next) {
  if((nextAddress) == addresses.length){
    let m1= await getendpickdroplatlang(start,end);
  }else{
   let m1= await  getstartpickdroplatlang(start);
  }
 var numOfIcon=1;
 var imageicon1;
 var imageicon2;
 var redicon='<?php base_url()?>assets/theme/default/images/redicon3.png';
 var greenicon='<?php base_url()?>assets/theme/default/images/greenicon3.png';
  var yellowicon='<?php base_url()?>assets/theme/default/images/yellowicon3.png';
 var imageText1;
 var imageText2;
  var request = {
    origin: start,
    destination: end,
    travelMode: 'DRIVING'
  };
  if(addresses.length==1){
    symbolOne=symbolOne;
    symbolThree=symbolThree;
    numOfIcon=2;
    imageicon1=redicon;
    imageicon2=greenicon;
    imageText1='A';
    imageText2='B';
    start=start+"<br>"+pickotheraddress;
    end=end+"<br>"+dropotheraddress;
  }else{
      if((nextAddress) == addresses.length){
        symbolOne=symbolTwo;
        symbolThree=symbolThree;
        numOfIcon=2;
        imageicon1=yellowicon;
        imageicon2=greenicon;
        imageText1=alphaticlatter[alphaticnumber];
        imageText2=alphaticlatter[alphaticnumber+1];
        end=end+"<br>"+dropotheraddress;
      }else{
        if(faddr==1){

             symbolOne=symbolOne;
             imageicon1=redicon;
             imageText1=alphaticlatter[alphaticnumber];
            start=start+"<br>"+pickotheraddress;
           
        }else{
             symbolOne=symbolTwo;
             imageicon1=yellowicon;
             imageText1=alphaticlatter[alphaticnumber];
            
        }
      }
  }

  directionsService.route(request,function(result, status) {
      if (status == 'OK') {
        //set icon in the start of stroke line
          if(numOfIcon==2){
            directionsDisplay = new google.maps.DirectionsRenderer({
              suppressMarkers: true,
              preserveViewport: true,
                polylineOptions: {
                  strokeColor:"#5DADE2",
                  strokeWeight:4,
                   icons: [
                {
                  icon: symbolOne,
                  offset: "0%"
                },
                {
                  icon: symbolThree,
                  offset: "100%"
                }
              ],
                }
            });
          }else{
              directionsDisplay = new google.maps.DirectionsRenderer({
                    suppressMarkers: true,
                    preserveViewport: true,
                      polylineOptions: {
                        strokeColor:"#5DADE2",
                        strokeWeight:4,
                         icons: [
                      {
                        icon: symbolOne,
                        offset: "0%"
                      }
                    ],
                      }
                  });
               }
        //set icon in the start of stroke line      
        directionsDisplay.setMap(map);
        directionsDisplay.setDirections(result);
      
        if(numOfIcon==2){
             var image1 = {
                url: imageicon1, // url
                scaledSize: new google.maps.Size(26, 38), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(12, 38), // anchor
                labelOrigin: new google.maps.Point(12.5, 12.5)
              };
                var startmarker = new google.maps.Marker({
                map: map,
                position:  new google.maps.LatLng(picklat,picklong),
               draggable: false,
               icon: image1,
               label: {text: imageText1, color: "white",fontWeight:'900'},
                clickable:true
               });
           
            var startinfowindow=new google.maps.InfoWindow({
                content:"<span style='white-space: wrap;overflow: hidden;width: 150px;display: inline-block;'>"+start+"</span>",
                  disableAutoPan: true
            });
            startinfowindow.open(map,startmarker);
            startmarker.addListener('click',function(){
            startinfowindow.open(map,startmarker);
            });
               //set end marker
            var image2 = {
              url: imageicon2, // url
              scaledSize: new google.maps.Size(26, 38), // scaled size
              origin: new google.maps.Point(0, 0), // origin
              anchor: new google.maps.Point(12, 38), // anchor
              labelOrigin: new google.maps.Point(12.5, 12.5)
              };
            var endmarker = new google.maps.Marker({
            map: map,
            position:  new google.maps.LatLng(droplat, droplong),            
            draggable: false,
            icon: image2,
            label: {text: imageText2, color: "white",fontWeight:'900'},
            clickable:true
    });        
        var endinfowindow=new google.maps.InfoWindow({
             content:"<span style='white-space: wrap;overflow: hidden;width: 150px;display: inline-block;'>"+end+"</span>",
              disableAutoPan: true
        });
        endinfowindow.open(map,endmarker);
        endmarker.addListener('click',function(){
        endinfowindow.open(map,endmarker);
        });
            //finish end marker
        }else{
           var image1 = {
                url: imageicon1, // url
                scaledSize: new google.maps.Size(26, 38), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(12, 38), // anchor
                labelOrigin: new google.maps.Point(12.5, 12.5)
              };
                var startmarker = new google.maps.Marker({
                map: map,
                position:  new google.maps.LatLng(picklat,picklong),
               draggable: false,
               icon: image1,
               label: {text: imageText1, color: "white",fontWeight:'900'},
                clickable:true
               });
           
            var startinfowindow=new google.maps.InfoWindow({
                content:"<span style='white-space: wrap;overflow: hidden;width: 150px;display: inline-block;'>"+start+"</span>",
                  disableAutoPan: true
            });
            startinfowindow.open(map,startmarker);
            startmarker.addListener('click',function(){
            startinfowindow.open(map,startmarker);
            });
        }
            faddr++;
            alphaticnumber++;
        // combine the bounds of the responses
        bounds.union(result.routes[0].bounds);
        // zoom and center the map to show all the routes
        map.fitBounds(bounds);
      }
      next();
    });
}
function getstartpickdroplatlang(start){
return  new Promise((resolve,reject)=>{
 setTimeout(()=>{
  var geocoder = new google.maps.Geocoder();      
            geocoder.geocode( { 'address': start}, function(results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                   picklat = results[0].geometry.location.lat();
                   picklong = results[0].geometry.location.lng();
                    resolve("resolve");
                }
              });
              },1000);
 });
}
function getendpickdroplatlang(start,end){
  return  new Promise((resolve,reject)=>{
 setTimeout(()=>{
   var geocoder = new google.maps.Geocoder();      
            geocoder.geocode( { 'address': start}, function(results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                   picklat = results[0].geometry.location.lat();
                   picklong = results[0].geometry.location.lng();
                }
              });
  var geocoder = new google.maps.Geocoder();      
            geocoder.geocode( { 'address': end}, function(results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                   droplat = results[0].geometry.location.lat();
                   droplong = results[0].geometry.location.lng();
                    resolve("resolve");
                }
              });
                },1000);
 });
}
function theNext() {
  if (nextAddress < addresses.length) {
    setTimeout('calcRoute("' + addresses[nextAddress][0] + '","' + addresses[nextAddress][1] + '",theNext)', delay);
    nextAddress++;
  } else {
    map.fitBounds(bounds);
    document.getElementById("mapsearchloader").style.display = "none";
  }
}
    </script>
<script type="text/javascript">
  $(function() {
    function reposition() {
        var modal = $(this),
            dialog = modal.find('.modal-dialog');
        modal.css('display', 'block');
        
        // Dividing by two centers the modal exactly, but dividing by three 
        // or four works better for larger screens.
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
    }
    // Reposition when a modal is shown
    $('.modal').on('show.bs.modal', reposition);
    // Reposition when the window is resized
    $(window).on('resize', function() {
        $('.modal:visible').each(reposition);
    });
});
</script>
 