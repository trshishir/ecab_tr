<?php   
 $booking=$this->session->userdata['bookingdata'];     
$stopsdata=$this->session->userdata['stops'];
$stopcount=count($stopsdata);
$ispickmonday='0';
$ispicktuesday='0';
$ispickwednesday='0'; 
$ispickthursday='0';
$ispickfriday='0';
$ispicksaturday='0';
$ispicksunday='0';

$pickmondaytime=date('h : i');
$picktuesdaytime=date('h : i');
$pickwednesdaytime=date('h : i');
$pickthursdaytime=date('h : i');
$pickfridaytime=date('h : i');
$picksaturdaytime=date('h : i');
$picksundaytime=date('h : i');


$isreturnmonday='0';
$isreturntuesday='0';
$isreturnwednesday='0'; 
$isreturnthursday='0';
$isreturnfriday='0';
$isreturnsaturday='0';
$isreturnsunday='0';

$returnmondaytime=date('h : i');
$returntuesdaytime=date('h : i');
$returnwednesdaytime=date('h : i');
$returnthursdaytime=date('h : i');
$returnfridaytime=date('h : i');
$returnsaturdaytime=date('h : i');
$returnsundaytime=date('h : i');

$pickairportarrivaltime=(empty($booking["flightarrivaltime"]))?date('h : i'):$booking["flightarrivaltime"];
$picktrainarrivaltime=(empty($booking["trainarrivalnumber"]))?date('h : i'):$booking["trainarrivalnumber"];
$dropairportarrivaltime=(empty($booking["dropflightarrivaltime"]))?date('h : i'):$booking["dropflightarrivaltime"];
$droptrainarrivaltime=(empty($booking["droptrainarrivalnumber"]))?date('h : i'):$booking["droptrainarrivalnumber"];


$start_date=date('d/m/Y');
$end_date=date('d/m/Y');
$pick_date=date('d/m/Y');
$return_date=date('d/m/Y');

$start_time=date('h : i');
$end_time=date('h : i');
$pick_time=date('h : i');
$return_time=date('h : i');
$waiting_time=date('00 : 00');


$pick_status="1";
$drop_status="1";
$servicecat_id=$booking['servicecategory'];
$packagedepart_id=$booking['package'];
$packagedestincation_id=$booking['droppackage'];
$pickairportpoiid=$booking["airport"];
$picktrainpoiid=$booking["train"];
$pickparkpoiid=$booking['park'];
$pickhotelpoiid=$booking['hotel'];
$dropairportpoiid=$booking["dropairport"];
$droptrainpoiid=$booking["droptrain"];
$dropparkpoiid=$booking['droppark'];
$drophotelpoiid=$booking['drophotel'];
$pick_form =  $this->session->flashdata('pick_form');
$drop_form =  $this->session->flashdata('drop_form');
$service_form =  $this->session->flashdata('service_form');
$pickupcategory=($booking['categorytype'])?$booking['categorytype']:'address';
$dropoffcategory=($booking['dropcategorytype'])?$booking['dropcategorytype']:'address';
$servicedata= $this->bookings_model->getservicesbyid('vbs_u_service', $booking['service']);


     
      $pickairportpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$pickairportpoiid);
      $picktrainpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$picktrainpoiid);
      $pickhotelpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$pickhotelpoiid);
      $pickparkpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$pickparkpoiid);


        
      $dropairportpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$dropairportpoiid);
      $droptrainpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$droptrainpoiid);
      $drophotelpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$drophotelpoiid);
      $dropparkpoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$dropparkpoiid);

      
       $packagedata=$this->bookings_model->poidatarecord('vbs_u_package',$packagedepart_id);
       $packagepoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$packagedata['departure']);
       $poicategoryrecord=$this->bookings_model->poidatarecord('vbs_u_ride_category',$packagepoirecord['category_id']);


       $droppackagedata=$this->bookings_model->poidatarecord('vbs_u_package',$packagedestincation_id);
       $droppoirecord=$this->bookings_model->poidatarecord('vbs_u_poi',$droppackagedata['destination']);
       $poidropcategoryrecord=$this->bookings_model->poidatarecord('vbs_u_ride_category',$droppoirecord['category_id']);
             
               

       if(!empty($booking['startdate']) || $booking['startdate'] != null){ 
         $start_date= $booking['startdate'];
       }
       if(!empty($booking['enddate']) || $booking['enddate'] != null){ 
         $end_date= $booking['enddate'];
       }
       if(!empty($booking['pickupdate']) || $booking['pickupdate'] != null){ 
         $pick_date= $booking['pickupdate'];
       }
       if(!empty($booking['returndate']) || $booking['returndate'] != null){ 
         $return_date= $booking['returndate'];
       }
  if($this->session->userdata['bookingpickregular']){
    $bookingregular=$this->session->userdata['bookingpickregular'];
       foreach($bookingregular as $regular){
         if($regular["type"]==1){
        if(strtolower($regular["day"])=='monday'){
             $ispickmonday='1';
             $pickmondaytime=$regular["time"];
           }
          elseif(strtolower($regular["day"])=='tuesday'){
             $ispicktuesday='1';
             $picktuesdaytime=$regular["time"];
           }
          elseif(strtolower($regular["day"])=='wednesday'){
             $ispickwednesday='1';
             $pickwednesdaytime=$regular["time"];
           }
          elseif(strtolower($regular["day"])=='thursday'){
             $ispickthursday='1';
             $pickthursdaytime=$regular["time"];
           }
          elseif(strtolower($regular["day"])=='friday'){
             $ispickfriday='1';
             $pickfridaytime=$regular["time"];
           }
          elseif(strtolower($regular["day"])=='saturday'){
             $ispicksaturday='1';
             $picksaturdaytime=$regular["time"];
           }
          elseif(strtolower($regular["day"])=='sunday'){
             $ispicksunday='1';
             $picksundaytime=$regular["time"];
           }   
         }        
      }
    }
    if($this->session->userdata['bookingreturnregular']){
        $pickreturn=$this->session->userdata['bookingreturnregular'];
    foreach($pickreturn as $return){
       if($return["type"]==1){
      if(strtolower($return["day"])=='monday'){
           $isreturnmonday='1';
           $returnmondaytime=$return["time"];
         }
        elseif(strtolower($return["day"])=='tuesday'){
           $isreturntuesday='1';
           $returntuesdaytime=$return["time"];
         }
        elseif(strtolower($return["day"])=='wednesday'){
           $isreturnwednesday='1';
           $returnwednesdaytime=$return["time"];
         }
        elseif(strtolower($return["day"])=='thursday'){
           $isreturnthursday='1';
           $returnthursdaytime=$return["time"];
         }
        elseif(strtolower($return["day"])=='friday'){
           $isreturnfriday='1';
           $returnfridaytime=$return["time"];
         }
        elseif(strtolower($return["day"])=='saturday'){
           $isreturnsaturday='1';
           $returnsaturdaytime=$return["time"];
         }
        elseif(strtolower($return["day"])=='sunday'){
           $isreturnsunday='1';
           $returnsundaytime=$return["time"];
         }
       }
    }
  }
    if(!empty($booking['starttime']) || $booking['starttime'] != null){   
       $start_time= $booking['starttime'];
     }
     if(!empty($booking['endtime']) || $booking['endtime'] != null){  
       $end_time= $booking['endtime'];
     }
     if(!empty($booking['pickuptime']) || $booking['pickuptime'] != null){    
       $pick_time= $booking['pickuptime'];
     }
     if(!empty($booking['returntime']) || $booking['returntime'] != null){  
       $return_time= $booking['returntime'];
     }
     if(!empty($booking['waitingtime']) || $booking['waitingtime'] != null){   
       $waiting_time= $booking['waitingtime'];
     }
   $attributes = array("name" => 'booking_form', "id" => 'bookingform');   
  echo form_open('booking_save',$attributes);
?>
 <div class="row booknow-content new-box-type" style="margin-top: 10px;">
   <?php
$flashAlert =  $this->session->flashdata('alert');
if(isset($flashAlert['message']) && !empty($flashAlert['message'])){?>
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div style="padding: 5px 12px" class="alert <?=$flashAlert['class']?>">
        <strong><?=$flashAlert['type']?></strong> <?=$flashAlert['message']?>
        <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
  </div>
<?php } ?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="width:100%;">
  <!--Service and Service Category -->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0px;width: 100%;margin-bottom: 20px;">
     <div class="col-lg-5 col-md-5 col-sm-12 booking_service_tab" style="padding:0px;width: 46%;">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 booking_serivce_inner_tab" style="padding-left:0px;">
      <div class="form-group edit-cardform-control">
         <div class="input-group">
            <span class="input-group-addon">
            <i class="fa fa-car"></i>
            </span>
        <select name="pickservicecategory"  class="servicecategoryinput" id="pickservicecategoryinput" onchange="addservice()" >
                      <option value="">Service Category</option>
                                <?php
                                  foreach($service_cat as $key => $item){   
                                 ?>
                                   <option <?= ($servicecat_id==$item->id)?"selected":""; ?> value="<?= $item->id ?>"><?= $item->category_name ?></option>
                                   <?php 
                                   
                                      }    
                                     ?>
                  </select>
                   </div>
              <small>Required</small> 
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 booking_serivce_inner_tab" style="padding-right:0px;">
      <div class="form-group edit-cardform-control">
           <div class="input-group">
                <span class="input-group-addon">
                <i class="fa fa-car"></i>
                </span>
          <select name="pickservice"   class="pickserviceinput" id="pickserviceinput" >
                      <option value="">Service</option>
                               <?php
                                     $service=$this->bookings_model->get_services('vbs_u_service',['service_category' => $servicecat_id]);
                                   foreach($service as $key => $item){
                                 
                                 ?>
                                   <option <?= ($booking['service']==$item->id)?"selected":""; ?> value="<?= $item->id ?>"><?= $item->service_name ?></option>
                                   <?php 
                                   
                                      }    
                                     ?>
                  </select>
          </div> 
          <small>Required</small>                
       </div>
    </div>
</div>
  </div>
  <!-- Service and Service Category -->
  <!--Pick up Section -->
   <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12  booking_tab_size" style="padding: 0px;width: 46%;">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
        <div class="booking-tab">
           <input type="hidden"  id="pickupcategory" name="pickupcategory" value="<?= $pickupcategory; ?>">
         
        
          <ul class="nav nav-tabs">
           
             <li><button type="button" class="btn btn-default extra-pad booking" style="font-weight:900;pointer-events: none;padding-bottom: 11px;border-radius:0px;width:100%; background-image: linear-gradient(rgb(255, 255, 255) 0px, rgb(224, 224, 224) 100%) !important;color: #616161 !important;"> <?php echo 'Pickup'; ?></button></li>
              <li <?= $pickupcategory=="address" ? 'class="active"':''?> style="width:14.5%;"><a id="pickaddresscategorybtn" data-toggle="tab" href="#pick_address"><i class="fa fa-map-marker"></i> <?php echo 'Address'; ?></a></li>
             <li <?=$pickupcategory=="package"? 'class="active"':''?> style="width:16%;"><a id="pickpackagecategorybtn" data-toggle="tab" href="#pick_package"><i class="fa fa-suitcase"></i> <?php echo 'Package'; ?></a></li>
             <li <?=$pickupcategory=="airport"? 'class="active"':''?> style="width:15.5%;"><a id="pickairportcategorybtn" data-toggle="tab" href="#pick_airport"><i class="fa fa-plane"></i> <?php echo 'Airport'; ?></a></li>
             <li <?=$pickupcategory=="train"? 'class="active"':''?>><a id="picktraincategorybtn" data-toggle="tab" href="#pick_train"><i class="fa fa-train"></i> <?php echo 'Train'; ?></a></li>
             <li <?=$pickupcategory=="hotel"? 'class="active"':''?>><a id="pickhotelcategorybtn" data-toggle="tab" href="#pick_hotel"><i class="fa fa-hotel"></i> <?php echo 'Hotel'; ?></a></li>
             <li <?=$pickupcategory=="park"? 'class="active"':''?>><a id="pickparkcategorybtn" data-toggle="tab" href="#pick_park"><i class="fa fa-tree"></i> <?php echo 'Park'; ?></a></li>
             
         </ul>
        </div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
        <div class="tab-content">

            <div id="pick_address" class="tab-pane fade  <?=$pickupcategory== 'address'? 'in active':''?>">
               <input type="hidden" id="pickloc_lat" />
               <input type="hidden" id="pickloc_long" />
                <div class="row">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                   <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;margin:8px 0px 0px -5px;"></i>
                 </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                   <div class="form-group edit-cardform-control">
                      <div class="input-group" style="position: relative;">
                        <span class="input-group-addon">
                        <i class="fa fa-map-marker"></i>
                        </span>
                        
                         <input type="hidden" name="pickaddressfield" placeholder="Addresse line"  class="pick_up_txtbox_bkg" id="pickupaddress"
                          value="<?= $booking["address"]; ?>" >
                          <?php
                          $pickshortaddress='';
                            
                              $pickshortaddress=$booking["address"];                           
                              $pickshortaddress= str_replace(($booking["city"].','), "", $pickshortaddress);
                                                                                                
                              $pickshortaddress= str_replace(($booking["zipcode"].','), "",  $pickshortaddress);
                                
                            
                                  $pickshortaddress=str_ireplace("france","",$pickshortaddress);
                                  $pickshortaddress=rtrim($pickshortaddress,' ');
                                  $pickshortaddress=rtrim($pickshortaddress,',');
                                  $pickshortaddress = preg_replace('!\s+!', ' ', $pickshortaddress);
                                 
                              

                           ?>

                         <input type="text" placeholder="Addresse line"  class="pick_up_txtbox_bkg" id="pickupautocomplete"   onFocus="geolocate()"
                          value="<?php echo $pickshortaddress  ?>"  style="padding-right: 30px;">
                        <div id="currentuserpoibtn" onclick="getCurrentLocation()">
                          <img src="<?= base_url()?>assets/theme/default/images/target-a.svg">
                        </div>

                       
                      <div id="currentuserpoidiv">
                      <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker" style="top: 4px;"></i>
                         <input type="text" readonly id="currentuserpoiinput" onclick="setPickCurrentLocation()">
                          
                         
                      </div> 
                   </div>
                      </div>
                      <div id="locationloaderdiv">
                        <img src="<?= base_url()?>assets/theme/default/images/loader_2.gif" >
                      </div>
                         
                      </div>
                       

                          <small>Required</small>
                   </div>
                 </div>
               </div>

              <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text" name="pickotheraddressfield"  placeholder="Complément d'adresse"  class="pick_up_txtbox_bkg" value="<?= $booking["otheraddress"] ?>" style="padding-left:43px !important;">
                       
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px 5px 0px 0px;">
                          <div class="form-group edit-cardform-control">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                               <input type="text" name="pickzipcodefield" placeholder="Zip code"  class="pick_up_txtbox_bkg" id="pickup_code" value="<?= $booking["zipcode"]; ?>">
                                
                            </div>
                            
                                 <small>Required</small> 
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px 0px 0px 5px;">
                              <div class="form-group edit-cardform-control">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text" name="pickcityfield" placeholder="City"  class="pick_up_txtbox_bkg" id="pickup_city" value="<?=  $booking["city"] ?>">
                                         
                                    </div>
                                  
                                      <small>Required</small>    
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
            </div>
          <div id="pick_package" class="tab-pane fade <?=$pickupcategory== 'package'? 'in active':''?>">
              <div class="row">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                     <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                  </div>
                  <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-suitcase"></i>
                        </span>
                        
                     <select name="pickpackagefield" id="pickpackagefield"  class="packagebox_pick_bkg " onchange="addpoidata()">
                       <option value="">Select</option>
                 
                       <?php
                        foreach($poi_package as $package){
                         ?>
                       <option <?=  ($booking['package']==$package->id)?"selected":""; ?> value="<?= $package->id ?>"><?= $package->name ?></option>
                       <?php  }  ?>
                      </select>
                    </div>
                               
                                  <small>Required</small>
                      </div>
                  </div>
              </div> 
              
                <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-bullseye"></i>
                              </span>
                             <input type="text"   id="packagedepartcategoryname" value="<?= $poicategoryrecord['ride_cat_name']  ?>" disabled style="background-color: #f6f6f6;" >
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-bullseye"></i>
                                      </span>
                                       <input type="text"   id="packagedepartname" value="<?= $packagepoirecord['name']  ?>" disabled style="background-color: #f6f6f6;" >
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="packagedepartaddress" placeholder="Addresse" value="<?= $packagepoirecord['address']  ?>" disabled style="background-color: #f6f6f6;" >
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
               <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="packagedepartzipcode" placeholder="Zip code" value="<?= $packagepoirecord['postal']  ?>" disabled style="background-color: #f6f6f6;" >
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="packagedepartcity" placeholder="City" value="<?= $packagepoirecord['ville']  ?>" disabled style="background-color: #f6f6f6;" >
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
             
          </div>


          <div id="pick_airport" class="tab-pane fade <?=$pickupcategory== 'airport'? 'in active':''?>">
               <div class="row">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                      <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                  </div>
                  <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                     <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-plane"></i>
                        </span>

                      <select name="pickairportfield" id="airport_pick"  class="airlocation_pick_bkg " onchange="getpickcategorydata(this)">
                       <option value="">Select</option>
                 
                       <?php
                        foreach($poi_data as $airport){ 
                       $category=$this->bookings_model->getcategory('vbs_u_ride_category',$airport->category_id);
                                 
                          if(strtolower($category)=="airport"){
                         ?>
                       <option <?php echo ($pickairportpoiid==$airport->id)?"selected":"";?> value="<?= $airport->id ?>"><?= $airport->name ?></option>
                       <?php 
                             }
                              }    
                             ?>
                      </select>
                    </div>

                                  <small>Required</small>
                     </div>             
                  </div>
               </div>
               <div class="row" style="margin-top:5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2"></div>
                  <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small edit-cardform-control" style="padding: 0px;padding-right:5px;">
                      <input type="text"  name="pickairportflightnumberfield" placeholder="Flight No" class="pickup_flight_no_bkg " id="pickup_flight_no" 
                           value="<?= $booking["flightnumber"]; ?>">
                           
                         <small style="margin-left:0px !important;">Required</small>         
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small col-arrival" style="padding: 0px;padding-left:5px;">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding:0px;padding-top: 9px;">
                      <label for="timepicker_pick_fly">Arrival Time</label>
                    </div>
                     <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                         <input name="pickairportarrivaltimefield" type="text" id="timepicker_pick_fly"  value="<?= $pickairportarrivaltime ?>"  class="pickup_flight_time_bkg" />
                         
                      </div>
                  </div>
                </div>
                </div>
               </div>
                <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="pickairportaddress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $pickairportpoirecord['address'] ?>" >
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="pickairportzipcode" placeholder="Zip code" disabled style="background-color: #f6f6f6;"  value="<?= $pickairportpoirecord['postal'] ?>">
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="pickairportcity" placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $pickairportpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>

          </div>

          <div id="pick_train" class="tab-pane fade <?=$pickupcategory== 'train'? 'in active':''?>">
                  <div class="row">
                      <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                          <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                      </div>
                      <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                      <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-train"></i>
                        </span>
                          <select name="picktrainfield" class="trainlocation_pick_bkg " id="train_pick"  onchange="getpickcategorydata(this)">
                            <option value="">Select</option>
                              <?php
                                foreach($poi_data as $train){
                                   $category=$this->bookings_model->getcategory('vbs_u_ride_category',$train->category_id);
                                 
                                  if(strtolower($category)=="train"){
                                 ?>
                            <option <?= ($picktrainpoiid==$train->id)?"selected":""; ?> value="<?= $train->id ?>"><?= $train->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                          </select>
                        </div>
                        
                                  <small>Required</small>
                          </div>        
                      </div>
                  </div>
                  <div class="row" style="margin-top:5px;">
                    <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2"></div>
                     <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small edit-cardform-control" style="padding: 0px;padding-right:5px;">
                            <input id="pickup_train_no" class="pickup_train_no_bkg" type="text"  name="picktrainnumberfield" placeholder="Train No" value="<?= $booking["trainnumber"]; ?>">
                            
                              <small style="margin-left: 0px !important;"></small>      
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small col-arrival" style="padding: 0px;padding-left:5px;">
                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding:0px;padding-top: 9px;">
                            <label for="timepicker_pick_stn">Arrival Time</label>
                          </div>
                           <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                             <input  class="pickup_flight_time_bkg" id="timepicker_pick_stn" name="picktrainarrivaltimefield" type="text" value="<?= $picktrainarrivaltime ?>" />
                             
                            </div> 
                        </div>
                      </div>
                    </div>
                  </div>

               <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="picktrainaddress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $picktrainpoirecord['address'] ?>">
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="picktrainzipcode" placeholder="Zip code"  disabled style="background-color: #f6f6f6;" value="<?= $picktrainpoirecord['postal'] ?>" >
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="picktraincity" placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $picktrainpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
          </div>


          <div id="pick_hotel" class="tab-pane fade <?=$pickupcategory== 'hotel'? 'in active':''?>">
           <div class="row">
              <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                  <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
              </div>
              <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                 <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-hotel"></i>
                        </span>
                  <select name="pickhotelfield" class="hotellocation_pick_bkg" id="hotel_pick"  onchange="getpickcategorydata(this)">
                      <option value="">Select</option>
                      <?php
                            foreach($poi_data as $hotel){
                               $category=$this->bookings_model->getcategory('vbs_u_ride_category',$hotel->category_id);

                              if(strtolower($category)=="hotel"){
                             ?>
                           <option <?=  ($pickhotelpoiid==$hotel->id)?"selected":""; ?> value="<?= $hotel->id ?>"><?= $hotel->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                  </select>
                </div>
                    
                       <small style="margin-left: 0px !important;">Required</small>           
              </div>
            </div>
            </div>
             <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="pickhoteladdress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $pickhotelpoirecord['address'] ?>">
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="pickhotelzipcode" placeholder="Zip code"  disabled style="background-color: #f6f6f6;" value="<?= $pickhotelpoirecord['postal'] ?>">
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="pickhotelcity"  placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $pickhotelpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
          </div>


          <div id="pick_park" class="tab-pane fade <?=$pickupcategory== 'park'? 'in active':''?>">
           <div class="row">
              <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                  <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i> 
              </div>
              <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                  <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-tree"></i>
                        </span>
                  <select name="pickparkfield" class="parklocation_pick_bkg " id="park_pick"  onchange="getpickcategorydata(this)">
                   <option value="">Select</option>
                   <?php
                                foreach($poi_data as $park){
                                  $category=$this->bookings_model->getcategory('vbs_u_ride_category',$park->category_id);
                                 
                          if(strtolower($category)=="park"){
                                 ?>
                                   <option <?=  ($pickparkpoiid==$park->id)?"selected":""; ?> value="<?= $park->id ?>"><?= $park->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                 </select>
               </div>
                   <?php
                                 if(isset($pick_form['pick_park']['error']) && !empty($pick_form['pick_park']['error'])){     
                                      echo "<div class='invalid-error'>".$pick_form['pick_park']['error']."</div>";
                                        }
                                  ?>
                           <small style="margin-left: 0px !important;">Required</small>       
                </div>
              </div>
              </div>
               <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="pickparkaddress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $pickparkpoirecord['address'] ?>">
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="pickparkzipcode"  placeholder="Zip code" disabled style="background-color: #f6f6f6;" value="<?= $pickparkpoirecord['postal'] ?>">
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="pickparkcity" placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $pickparkpoirecord['ville'] ?>">
                                         
                                    </div>                                   
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                     </div>
                  </div>    
                </div>
  <!--Pickup up Section -->
  <!-- Loader Section -->
   <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 booking_tab_size booking_icon_tab" style="width: 8%;position:relative;"> <img src="<?= base_url()?>assets/theme/default/images/carloader2.gif" width="125" height="125" style="right: -14px;position:absolute;z-index: 10;    top: 0px;"> </div>
  <!-- Loader Section -->
  <!--Drop off Section -->  
    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 booking_tab_size " style="padding:0px;width: 46%;">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
          <div class="booking-tab">
             <input type="hidden" value="<?= $dropoffcategory ?>" id="dropoffcategory" name="dropoffcategory">
            <ul class="nav nav-tabs">

             <li><button type="button" class="btn btn-default extra-pad" style="font-weight:900;pointer-events: none;padding-bottom: 11px;border-radius:0px;width:100%;background-image: linear-gradient(rgb(255, 255, 255) 0px, rgb(224, 224, 224) 100%) !important;color: #616161 !important;"><?php echo 'Dropoff'; ?></button></li>
               <li  <?= $dropoffcategory=="address" ? 'class="active"':''?> style="width:14.5%;"><a id="dropaddresscategorybtn" data-toggle="tab" href="#drop_address"><i class="fa fa-map-marker"></i> <?php echo 'Address'; ?></a></li>
             <li  <?= $dropoffcategory=="package" ? 'class="active"':''?>  style="width:16%;"><a id="droppackagecategorybtn" data-toggle="tab" href="#drop_package"><i class="fa fa-suitcase"></i> <?php echo 'Package'; ?></a></li>

             <li  <?= $dropoffcategory=="airport" ? 'class="active"':''?> style="width:15.5%;"><a id="dropairportcategorybtn" data-toggle="tab" href="#drop_airport"><i class="fa fa-plane"></i> <?php echo 'Airport'; ?></a></li>
             <li  <?= $dropoffcategory=="train" ? 'class="active"':''?>><a id="droptraincategorybtn" data-toggle="tab" href="#drop_train"><i class="fa fa-train"></i> <?php echo 'Train'; ?></a></li>
             <li  <?= $dropoffcategory=="hotel" ? 'class="active"':''?>><a id="drophotelcategorybtn" data-toggle="tab" href="#drop_hotel"><i class="fa fa-hotel"></i> <?php echo 'Hotel'; ?></a></li>
             <li  <?= $dropoffcategory=="park" ? 'class="active"':''?>><a id="dropparkcategorybtn" data-toggle="tab" href="#drop_park"><i class="fa fa-tree"></i> <?php echo 'Park'; ?></a></li>
           </ul>
          </div>
      </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
     <div class="tab-content">
        <div id="drop_address" class="tab-pane fade  <?= $dropoffcategory=="address" ? 'in active':''?>">
              <input type="hidden" id="droploc_lat" />
               <input type="hidden" id="droploc_long" />
               <div class="row">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                   <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;margin:8px 0px 0px -5px;"></i>
                 </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                   <div class="form-group edit-cardform-control">
                      <div class="input-group" style="position: relative;">
                        <span class="input-group-addon">
                        <i class="fa fa-map-marker"></i>
                        </span>
                         <input type="hidden" name="dropaddressfield" placeholder="Addresse line"  class="pick_up_txtbox_bkg" id="dropoffaddress" onFocus="geolocate()"  value="<?= $booking["dropaddress"]; ?>">
                          <?php
                          $dropshortaddress='';
                           
                                 $dropshortaddress=$booking["dropaddress"];
                                
                                  $dropshortaddress= str_replace(($booking["dropcity"].','), "", $dropshortaddress);
                                  
                             
                                
                                  $dropshortaddress= str_replace(($booking["dropzipcode"].','), "",  $dropshortaddress);
                                
                             
                                 $dropshortaddress=str_ireplace("france","",$dropshortaddress);
                                  $dropshortaddress=rtrim($dropshortaddress,' ');
                                  $dropshortaddress=rtrim($dropshortaddress,',');
                                 $dropshortaddress = preg_replace('!\s+!', ' ', $dropshortaddress);
                              

                           ?>
                              <input type="text"  placeholder="Addresse line"  class="pick_up_txtbox_bkg" id="dropoffautocomplete" onFocus="geolocate()"  value="<?php echo $dropshortaddress ?>" style="padding-right: 30px;">

                        <div id="dropcurrentuserpoibtn" onclick="getDropCurrentLocation()">
                           <img src="<?= base_url()?>assets/theme/default/images/target-a.svg">
                        </div>
                       <div id="dropcurrentuserpoidiv">
                        <div class="form-group">
                        <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker" style="top: 4px;"></i>
                         <input type="text" readonly id="dropcurrentuserpoiinput" onclick="setDropCurrentLocation()">
                          
                         
                          </div> 
                        </div>
                      </div>
                       <div id="droplocationloaderdiv">
                        <img src="<?= base_url()?>assets/theme/default/images/loader_2.gif" >
                      </div>
                      </div>
                       
                          <small>Required</small>
                   </div>
                 </div>
               </div>

              <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                     <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                        <input type="text" name="dropotheraddressfield" placeholder="Complément d'adresse"  class="pick_up_txtbox_bkg" style="padding-left:43px !important;" value="<?= $booking["dropotheraddress"]; ?>">
                        
                      </div>
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small col-zip-code" style="padding: 0px 5px 0px 0px;">
                          <div class="form-group edit-cardform-control">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                               <input type="text" name="dropzipcodefield" placeholder="Zip code"  class="pick_up_txtbox_bkg" id="dropoff_code" 
                               value="<?= $booking["dropzipcode"]; ?>">

                            </div>
                           
                           <small>Required</small>
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small col-city" style="padding: 0px 0px 0px 5px;">
                              <div class="form-group edit-cardform-control">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text" name="dropcityfield" placeholder="City"  class="pick_up_txtbox_bkg" id="dropoff_city"   value="<?=  $booking["dropcity"] ?>">
                                    </div>
                                       
                                        <small>Required</small>
                               </div>
                          </div>
                       </div>
                   </div>
              </div>
       </div>
        <div id="drop_package" class="tab-pane fade  <?= $dropoffcategory=="package" ? 'in active':''?>">
            <div class="row">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                   <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-suitcase"></i>
                        </span>
                  <select name="droppackagefield" id="droppackagefield" class="packagebox_pick_bkg " onchange="adddroppoidata()">
                       <option value="">Select</option>
                 
                       <?php
                        foreach($poi_package as $package){
                         ?>
                       <option <?=  ($booking['droppackage']==$package->id)?"selected":""; ?> value="<?= $package->id ?>"><?= $package->name ?></option>
                       <?php  }  ?>
                      </select>
                    </div>
                       
                          <small>Required</small>
                </div>
              </div>
            </div>  
            
                  <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-bullseye"></i>
                              </span>
                             <input type="text"   id="packagedestinationcategoryname" value="<?= $poidropcategoryrecord['ride_cat_name']  ?>" disabled style="background-color: #f6f6f6;" >
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-bullseye"></i>
                                      </span>
                                       <input type="text"   id="packagedestinationname" value="<?= $droppoirecord['name']  ?>" disabled style="background-color: #f6f6f6;" >
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text" placeholder="Addresse"   id="packagedestinationaddress" value="<?= $droppoirecord['address']  ?>" disabled style="background-color: #f6f6f6;">
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
           
                   <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text" placeholder="Zip code"   id="packagedestinationzipcode" value="<?= $droppoirecord['postal']  ?>" disabled style="background-color: #f6f6f6;" >
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text" placeholder="City"  id="packagedestinationcity" value="<?= $droppoirecord['ville']  ?>" disabled style="background-color: #f6f6f6;" >
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
        </div>
        <div id="drop_airport" class="tab-pane fade  <?= $dropoffcategory=="airport" ? 'in active':''?>">
                   <div class="row">
                      <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                          <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                      </div>
                      <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                         <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-plane"></i>
                        </span>
                          <select name="dropairportfield" id="airport_drop"  class="airlocation_pick_bkg " onchange="getdropcategorydata(this)">
                           <option value="">Select</option>
                            <?php
                                foreach($poi_data as $airport){
                                   $category=$this->bookings_model->getcategory('vbs_u_ride_category',$airport->category_id);
                                  if(strtolower($category)=="airport"){
                                 ?>
                                   <option <?= ($dropairportpoiid==$airport->id)?"selected":""; ?> value="<?= $airport->id ?>"><?= $airport->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                          </select>
                        </div>
                          
                          <small>Required</small>
                    </div>
                  </div>
                  </div>
                  <div class="row" style="margin-top:5px;">
                      <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2"></div>
                        <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small edit-cardform-control" style="padding: 0px;padding-right:5px;">
                          <input type="text" name="dropairportflightnumberfield" placeholder="Flight No" class="pickup_flight_no_bkg " id="dropof_flight_no" value="<?= $booking["dropflightnumber"]; ?>">
                        
                          <small style="margin-left: 0px !important;">Required</small>
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small col-arrival" style="padding: 0px;padding-left:5px;">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding:0px;padding-top: 9px;">
                          <label for="timepicker_drop_fly_1">Arrival Time</label>
                        </div>
                         <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                         <input  class="pickup_flight_time_bkg" id="timepicker_drop_fly_1" name="dropairportarrivaltimefield" type="text" value="<?= $dropairportarrivaltime ?>" />
                         
                       </div>
                          </div>
                     </div>
                    </div>
                  </div>

                   <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="dropairportaddress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $dropairportpoirecord['address'] ?>" >
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="dropairportzipcode" placeholder="Zip code"  disabled style="background-color: #f6f6f6;"  value="<?= $dropairportpoirecord['postal'] ?>">
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="dropairportcity" placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $dropairportpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>




              
        </div>
        <div id="drop_train" class="tab-pane fade  <?=$dropoffcategory=="train" ? 'in active':''?>">
                     <div class="row">
                        <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                            <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
                        </div>
                        <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                           <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-train"></i>
                        </span>
                            <select name="droptrainfield" id="train_drop" class="trainlocation_pick_bkg" onchange="getdropcategorydata(this)">
                              <option value="">Select</option>
                                <?php
                                foreach($poi_data as $train){
                                   $category=$this->bookings_model->getcategory('vbs_u_ride_category',$train->category_id);
                                  if(strtolower($category)=="train"){
                                 ?>
                          <option  <?= ($droptrainpoiid==$train->id)?"selected":""; ?> value="<?= $train->id ?>"><?= $train->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                          </select>
                        </div>
                          
                          <small>Required</small>
                      </div>
                    </div>
                    </div>
                    <div class="row" style="margin-top:5px;">
                       <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2"></div>
                      <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">                      
                        <div class="ccol-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small edit-cardform-control" style="padding: 0px;padding-right:5px;">
                            <input  class="pickup_train_no_bkg" type="text"  name="droptrainnumberfield" placeholder="Train No" id="dropoff_train_no"  value="<?= $booking["trainnumber"]; ?>">

                          <small style="margin-left: 0px !important;">Required</small>
                        </div>
                        <div class="ccol-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small col-arrival" style="padding: 0px;padding-left:5px;">
                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding:0px;padding-top: 9px;">
                              <label for="pickup_train_time_bkg">Arrival Time</label>
                            </div>
                          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                            <input style="width:100% !important;" class="pickup_train_time_bkg" id="timepicker_drop_stn" name="droptrainarrivaltimefield" type="text" value="<?= $droptrainarrivaltime ?>" />

                          </div>
                        </div>
                    </div>
                  </div>
                </div>

                     <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="droptrainaddress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $droptrainpoirecord['address'] ?>">
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="droptrainzipcode" placeholder="Zip code"  disabled style="background-color: #f6f6f6;" value="<?= $droptrainpoirecord['postal'] ?>" >
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="droptraincity" placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $droptrainpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
        </div>
        <div id="drop_hotel" class="tab-pane fade  <?= $dropoffcategory=="hotel" ? 'in active':''?>">
         <div class="row">
            <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>
            </div>
            <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
               <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-hotel"></i>
                        </span>
                <select name="drophotelfield" class="hotellocation_pick_bkg" id="hotel_drop" onchange="getdropcategorydata(this)">
                    <option value="">Select</option>
                    <?php
                                foreach($poi_data as $hotel){
                                  $category=$this->bookings_model->getcategory('vbs_u_ride_category',$hotel->category_id);
                                  if(strtolower($category)=="hotel"){
                                 ?>
                                   <option <?=  ($drophotelpoiid==$hotel->id)?"selected":""; ?> value="<?= $hotel->id ?>"><?= $hotel->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                </select>
              </div>
                   
                          <small>Required</small>
            </div>
          </div>
         </div>
           <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="drophoteladdress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $drophotelpoirecord['address'] ?>">
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="drophotelzipcode" placeholder="Zip code"  disabled style="background-color: #f6f6f6;" value="<?= $drophotelpoirecord['postal'] ?>">
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="drophotelcity" placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $drophotelpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
        </div>
        <div id="drop_park" class="tab-pane fade  <?= $dropoffcategory=="park" ? 'in active':''?>">
             <div class="row">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                    <i class="fa fa-hand-o-right" style="font-size:25px;color:#333;float:left;margin:8px 0px 0px -5px;"></i>

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                   <div class="form-group edit-cardform-control">
                      <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-tree"></i>
                        </span>
                    <select name="dropparkfield" class="parklocation_pick_bkg" id="park_drop"  onchange="getdropcategorydata(this)">
                 <option value="">Select</option>
                   <?php
                                foreach($poi_data as $park){
                                   $category=$this->bookings_model->getcategory('vbs_u_ride_category',$park->category_id);
                                  if(strtolower($category)=="park"){
                                 ?>
                                   <option <?=  ($dropparkpoiid==$park->id)?"selected":""; ?> value="<?= $park->id ?>"><?= $park->name ?></option>
                                   <?php 
                                     }
                                      }    
                                     ?>
                 </select>
               </div>

                          <small>Required</small>
                </div>
              </div>
            </div>
             <div class="row" style="margin-top: 5px;">
                  <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                  </div>
                 <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="form-group">
                      <div class="inner-addon left-addon">
                          <i class="glyphicon glyphicon-map-marker"></i>
                         <input type="text"   id="dropparkaddress" placeholder="Addresse"  disabled style="background-color: #f6f6f6;" value="<?= $dropparkpoirecord['address'] ?>">
                          
                         
                      </div>
                      
                   </div>
                 </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">

                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-right:5px;">
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon">
                              <i class="fa fa-map-marker"></i>
                              </span>
                             <input type="text"   id="dropparkzipcode" placeholder="Zip code"  disabled style="background-color: #f6f6f6;" value="<?= $dropparkpoirecord['postal'] ?>">
                                
                            </div>
                             
                          </div>
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-extra-small" style="padding: 0px;padding-left:5px;">
                              <div class="form-group">
                                 <div class="input-group">
                                      <span class="input-group-addon">
                                      <i class="fa fa-map-marker"></i>
                                      </span>
                                       <input type="text"   id="dropparkcity" placeholder="City"  disabled style="background-color: #f6f6f6;" value="<?= $dropparkpoirecord['ville'] ?>">
                                         
                                    </div>
                                   
                                  </div>
                          </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
</div>
  <!--Drop off Section -->
  
</div>
   
<!-- comment and checks section -->
    <div class="date-time" style="margin-top: 10px;margin-bottom:10px;">
     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="width: 46%;padding:0px;">
        
     <div class="col-lg-12 col-md-12 col-sm-12" id="stopaddressdiv" style="padding:0px;margin-top: 10px;">
      <input type="hidden" id="stopcountfield" value="<?= $stopcount; ?>">
      <?php

      $n=1;
       foreach ($stopsdata as $stop):
        ?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 stopinnerdiv" style="padding:0px;">
  <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12" style="padding:0px;width: 67%;">
    <div class="form-group"><div class="input-group" style="position: relative;">
      <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
      <input type="text" name="stopaddress[]" placeholder="Addresse line"  class="pick_up_txtbox_bkg" id="stopaddr_<?= $n ?>"   onFocus="geolocate()" value="<?= $stop['stopaddress']; ?>"  style="padding-right: 30px;height: 30px !important;border-radius: 0px">
    </div>
    <div class="currentstoplocationbtn" onclick="getStopCurrentLocation(<?= $n ?>)"><img src="<?= base_url()?>assets/theme/default/images/target-a.svg"></div><div class="currentstoplocationdiv" id="stopercurrentlocationdiv_<?= $n ?>"><div class="form-group"><div class="inner-addon left-addon"><i class="glyphicon glyphicon-map-marker" style="top: -2px;"></i><input type="text" readonly id="stopercurrentlocationfield_<?= $n ?>" class="currentstoppoiinput" onclick="setStopCurrentLocation(<?= $n ?>)" style="height:30px !important;"></div></div></div><div class="currentstoplocationloader" id="stoperlocationloader_<?= $n ?>"><img src="<?= base_url()?>assets/theme/default/images/loader_2.gif" ></div> </div></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding:0px;width: 28%;padding-left: 5px;"><div class="col-lg-7 col-md-7 col-sm-7 col-xs-12" style="padding:0px;width:58%;"><label class="pickup_time pickup_time_label" style="margin:0px !important;padding-top: 9px !important;font-size:13px !important;"><?php echo 'Waiting Time'; ?></label></div><div class="col-lg-5 col-md-5 col-sm-5 col-xs-12" style="padding:0px;width:42%;"><input class="stopwaitingtimepicker" id="stopwaitingtimepicker_<?= $n ?>"  name="stopwaittime[]" type="text" value="<?= $stop['stopwaittime']; ?>" style="width:100%;height: 30px !important;border-radius: 0px" /></div></div><div class="col-lg-1 col-md-1 col-sm-1" style="width: 5%;padding: 0px;padding-left: 5px;"><button type="button"  onclick="removestopboxes(this,<?= $n ?>);" class="minusstopicon"><i class="fa fa-minus" style="margin:0px;"></i></button></div></div>
  <?php
   $n++;
   endforeach;
    ?>
     </div>
  
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;" >
         <div id="return_chairwheel" >
                    <div  style="float:left; margin-right:20px;">
                      <div class="squaredFour">
                        <span>Reccurent </span><input class="regular_check" id="regularcheckfieldid" type="checkbox" name="regularcheckfield" <?= ($booking['regularcheck']=='1')?"checked":""; ?> value="1" style="margin:0px 10px;float:none;">
                          <label for="regularcheckfieldid"></label>
                      </div>
                    </div>

                    <div style="float:left; margin-right:20px;">
                         <div class="squaredFour">
                        <span>Return </span><input class="return_car"  type="checkbox" id="returncheckfieldid" name="returncheckfield" value="1" 
                        <?= ($booking['returncheck']=='1')?"checked":""; ?>  style="margin:0px 10px;float:none;">
                           <label for="returncheckfieldid"></label>
                      </div>
                    </div>
                     <div style="float:left; margin-right:20px;">
                         <div class="squaredFour">
                        <span>Wheelchair </span><input class="wheelchair_carssh" type="checkbox" id="wheelcarcheckfieldid" name="wheelcarcheckfield" value="1" <?= ($booking['wheelchaircheck']=='1')?"checked":""; ?>  style="margin:0px 10px;float:none;">
                           <label for="wheelcarcheckfieldid"></label>
                      </div>
                    </div>
                      <div style="float:right;">
                        
                        <span>Add Extra Stop </span> <button type="button" onclick="addstopboxes();" class="plusgreeniconconfig" style="margin: 0px 0px 0px 10px!important;"><i class="fa fa-plus" style="margin:0px !important;"></i></button>
                           
                     
                    </div>
                    
                    <div class="clearfix"></div>
             </div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px;padding-top:20px;">
        <div class="pickupreturn_datetime_div">
              <div id="pickup_datetime_div" >
                <div class="pickdt-bkg" style="text-align:center;"> 
                    <label class="pickup_date pickup_dt_label" style="margin:0px !important;font-size:13px !important;"> <?php echo 'Pickup Date'; ?> </label>
                    
                    <input id="dtpicker" class="pickup-dt-bkg bdatepicker"  type="text" value="<?= $pick_date; ?>" name="pickdatefield" placeholder="<?php echo 'Pickup Date'; ?>"  />
                </div>
                <div class="picktime-bkg" style="text-align:center;"> 
                   <label class="pickup_time pickup_time_label" style="margin:0px !important;font-size:13px !important;"><?php echo 'Pickup Time'; ?></label>
                    
                    <input   id="timepicker1" name="picktimefield" type="text" value="<?= $pick_time; ?>" style="width:100%" />
                </div>
              </div>
          
              <div class="return_datetime_div" style="display: none;">

                <div class="picktime-bkg" style="text-align:center;"> 
                   <label class="pickup_time pickup_time_label" style="margin:0px !important;"><?php echo 'Waiting Time'; ?></label>
                  
                    
                    <input id="waitingtimepicker" name="waitingtimefield" type="text" value="<?= $waiting_time; ?>" style="width:100%" />
                </div>

                <div class="dropdt-bkg" style="text-align:center;">
                     <label style="margin:0px !important;" class="dropof_date dropof_dt_label"> <?php echo 'Return Date'; ?> </label>
                    

                   
                    <input id="dtpicker1" class="bdatepicker"  type="text" value="<?= $return_date; ?>" name="returndatefield" placeholder="<?php echo 'Drop Date'; ?>"  />
                </div>

                <div class="droptime-bkg" style="text-align:center;">
                    <label class="dropof_time dropof_time_label" style="margin:0px !important;"><?php echo 'Return Time'; ?></label>
                   
            
                    <input   id="timepicker2" name="returntimefield" type="text" value="<?= $return_time; ?>" style="width:100%;margin-top: 0px !important;" />
                </div>
              </div> 
        </div>
        
          <div class="startend_datetime_div" style="display: none;">

             <div class="pickdt-bkg" style="text-align:center;"> 
                    <label class="pickup_date start_dt_label" style="margin:0px !important;"> <?php echo 'Start Date'; ?> </label>
                 
                  <input  class="pickup-dt-bkg bdatepicker"  type="text" value="<?= $start_date; ?>" name="startdatefield" placeholder="<?php echo 'Start Date'; ?>"  />
              </div>
              <div class="picktime-bkg" style="text-align:center;"> 
               
                  <label class="pickup_time start_time_label" style="margin:0px !important;"><?php echo 'Start Time'; ?></label>
                  
                  <input   id="starttimepicker" name="starttimefield" type="text" value="<?= $start_time; ?>" style="width:100%" />
              </div>

              <div class="dropdt-bkg" style="text-align:center;">       
                  <label style="margin:0px !important;" class="dropof_date endof_dt_label"> <?php echo 'End Date'; ?> </label>
           
                  <input class="bdatepicker"  type="text" value="<?= $end_date; ?>" name="enddatefield" placeholder="<?php echo 'End Date'; ?>"  />
              </div>

              <div class="droptime-bkg" style="text-align:center;">
      
                  <label class="dropof_time endof_time_label" style="margin:0px !important;"><?php echo 'End Time'; ?></label>
          
                  <input   id="endtimepicker" name="endtimefield" type="text" value="<?= $end_time; ?>" style="width:100%;margin-top: 0px !important;" />
              </div>
          </div>
      </div>
    
     </div> 
     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 booking_comment_tab_size" style="width: 46%;margin-left: 8%;padding: 0px;">
          <div class="form-group">
            <label style="font-weight: 900;margin-bottom: 10px;">Comment :</label>
       <textarea class="form-control" rows="3" name="booking_comment" id="commentbox" placeholder="Write a comment here..."><?= $booking['booking_comment']; ?></textarea>

         </div>
     </div>    
  </div>
<!-- comment and checks section-->
<!-- Regular and Return Section -->
   <div  id="timeslotsrtn" style="display: none;" >
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
  
      <table cellpadding="0" cellspacing="0" 
      style="width:100%;" align="center" id="timesspantable">
        <tr style="text-align:left;" id="returnpicktimetable">
          <td>&nbsp;</td>
          <td><?php echo 'Monday'; ?></td>
          <td><?php echo 'Tuesday'; ?></td>
          <td><?php echo 'Wednesday'; ?></td>
          <td><?php echo 'Thursday'; ?></td>
          <td><?php echo 'Friday'; ?></td>
          <td><?php echo 'Saturday'; ?></td>
          <td><?php echo 'Sunday'; ?></td>
        </tr>
        <tr class="go-table" style="text-align:left;">
          <td><?php echo 'Pickup Time'; ?></td>
          <td class="weekdays_none_floating">
            <input name="pickmondaycheckfield" class="weekdays_go" id='pickdaycheck_1' type="checkbox" <?= ($ispickmonday=='1')?"checked":""; ?> value="1" >
            <input  class="wkdays_time_pick " id="go_time_1" name="pickmondayfield" type="text"  value="<?= $pickmondaytime ?>" style="float:none !important;" />
          </td>
          <td class="weekdays_none_floating"><input name="picktuesdaycheckfield" class="weekdays_go" id='pickdaycheck_2' type="checkbox" <?= ($ispicktuesday=='1')?"checked":""; ?> value="2"  >
            <input  class="wkdays_time_pick " id="go_time_2" name="picktuesdayfield" type="text" value="<?= $picktuesdaytime ?>" style="float:none !important;"/>
          </td>
          <td class="weekdays_none_floating"><input name="pickwednesdaycheckfield" class="weekdays_go" id='pickdaycheck_3' type="checkbox" <?= ($ispickwednesday=='1')?"checked":""; ?> value="3" >
            <input  class="wkdays_time_pick " id="go_time_3" name="pickwednesdayfield" type="text" value="<?= $pickwednesdaytime ?>" style="float:none !important;" />
          </td class="weekdays_none_floating">
          <td class="weekdays_none_floating"><input name="pickthursdaycheckfield" class="weekdays_go" id='pickdaycheck_4' type="checkbox" <?= ($ispickthursday=='1')?"checked":""; ?> value="4" >
            <input  class="wkdays_time_pick " id="go_time_4" name="pickthursdayfield" type="text" value="<?= $pickthursdaytime ?>"style="float:none !important;" />
          </td>
          <td class="weekdays_none_floating"><input name="pickfridaycheckfield" class="weekdays_go" id='pickdaycheck_5' type="checkbox" <?= ($ispickfriday=='1')?"checked":""; ?> value="5" >
            <input  class="wkdays_time_pick " id="go_time_5" name="pickfridayfield" type="text" value="<?= $pickfridaytime ?>" style="float:none !important;" />
          </td>
          <td class="weekdays_none_floating"><input name="picksaturdaycheckfield" class="weekdays_go" id='pickdaycheck_6' type="checkbox" <?= ($ispicksaturday=='1')?"checked":""; ?> value="6" >
            <input  class="wkdays_time_pick " id="go_time_6" name="picksaturdayfield" type="text" value="<?= $picksaturdaytime ?>"style="float:none !important;" />
          </td>
          <td class="weekdays_none_floating"><input name="picksundaycheckfield" class="weekdays_go" id='pickdaycheck_7' type="checkbox" <?= ($ispicksunday=='1')?"checked":""; ?> value="7" >
            <input  class="wkdays_time_pick " id="go_time_7" name="picksundayfield" type="text" value="<?= $picksundaytime ?>"style="float:none !important;" />
          </td>
        </tr>
        <tr class="back-table" style="text-align:left;">
          <td style="width:10%;text-align: left;"><?php echo 'Return Time'; ?></td>
          <td class="weekdays_none_floating">
            <input name="returnmondaycheckfield" class="weekdays_back" id='returndaycheck_1' type="checkbox" <?= ($isreturnmonday=='1')?"checked":""; ?> value="1" >
            <input  class="wkdays_time_pick " id="back_time_1" name="returnmondayfield" type="text" value="<?= $returnmondaytime ?>" style="float: none !important; "/>
          </td>
          <td class="weekdays_none_floating">
            <input name="returntuesdaycheckfield" class="weekdays_back" id='returndaycheck_2' type="checkbox" <?= ($isreturntuesday=='1')?"checked":""; ?> value="2" >
            <input  class="wkdays_time_pick " id="back_time_2" name="returntuesdayfield" type="text" value="<?= $returntuesdaytime ?>" style="float:none !important;" />
          </td>
          <td class="weekdays_none_floating">
            <input name="returnwednesdaycheckfield" class="weekdays_back" id='returndaycheck_3' type="checkbox" <?= ($isreturnwednesday=='1')?"checked":""; ?> value="3" >
            <input  class="wkdays_time_pick " id="back_time_3" name="returnwednesdayfield" type="text" value="<?= $returnwednesdaytime ?>" style="float:none !important;"/>
          </td>
          <td class="weekdays_none_floating">
            <input name="returnthursdaycheckfield" class="weekdays_back" id='returndaycheck_4' type="checkbox" <?= ($isreturnthursday=='1')?"checked":""; ?> value="4" >
            <input  class="wkdays_time_pick " id="back_time_4" name="returnthursdayfield" type="text" value="<?= $returnthursdaytime ?>" style="float:none !important;"/>
          </td>
          <td class="weekdays_none_floating">
            <input name="returnfridaycheckfield" class="weekdays_back" id='returndaycheck_5' type="checkbox" <?= ($isreturnfriday=='1')?"checked":""; ?> value="5" >
            <input  class="wkdays_time_pick " id="back_time_5" name="returnfridayfield" type="text" value="<?= $returnfridaytime ?>" style="float:none !important;"/>
          </td>
          <td class="weekdays_none_floating">
            <input name="returnsaturdaycheckfield" class="weekdays_back" id='returndaycheck_6' type="checkbox" <?= ($isreturnsaturday=='1')?"checked":""; ?> value="6" >
            <input  class="wkdays_time_pick " id="back_time_6" name="returnsaturdayfield" type="text" value="<?= $returnsaturdaytime ?>" style="float:none !important;"/>
          </td>
          <td class="weekdays_none_floating">
            <input name="returnsundaycheckfield" class="weekdays_back" id='returndaycheck_7' type="checkbox" <?= ($isreturnsunday=='1')?"checked":""; ?>  value="7" >
            <input  class="wkdays_time_pick " id="back_time_7" name="returnsundayfield" type="text" value="<?= $returnsundaytime ?>" style="float:none !important;"/>
          </td>
        </tr>
      </table>
    </div>
  </div>
<!-- Regular and Return Section -->
  



     <?php  include 'wheelchairdiv.php';?> 
              

          <div class="row submit-row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <button type="submit"  id="confirmbookingbtn" class="btn btn-primary btn-xs right btn-booking-next" style="margin-right: -13px !important;font-weight:normal;text-transform:none !important;">Identification <i class="fa fa-arrow-circle-o-right"></i> 
               </button>
            </div>
        </div>
     <?php echo form_close(); ?>
    
 <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo  $googleapikey->api_key; ?>&libraries=places&callback=initAutocomplete"
        async defer></script>
    <script type="text/javascript">
               const addform = document.getElementById('bookingform');
                //pickup fields
                const addpickupCategory=document.getElementById('pickupcategory');
                const addpickupAddress=document.getElementById('pickupautocomplete');
                const addpickupAddressCity=document.getElementById('pickup_city');
                const addpickupAddressCode=document.getElementById('pickup_code');
                const addpickupPackage=document.getElementById('pickpackagefield');
                const addpickupAirport=document.getElementById('airport_pick');
                const addpickupAirportFlightNo=document.getElementById('pickup_flight_no');
                const addpickupTrain=document.getElementById('train_pick');
                const addpickupTrainNo=document.getElementById('pickup_train_no');
                const addpickupHotel=document.getElementById('hotel_pick');
                const addpickupPark=document.getElementById('park_pick');

                //dropoff fields
                const adddropoffCategory=document.getElementById('dropoffcategory');
                const adddropoffAddress=document.getElementById('dropoffautocomplete');
                const adddropoffAddressCity=document.getElementById('dropoff_city');
                const adddropoffAddressCode=document.getElementById('dropoff_code');
                const adddropoffPackage=document.getElementById('droppackagefield');
                const adddropoffAirport=document.getElementById('airport_drop');
                const adddropoffAirportFlightNo=document.getElementById('dropof_flight_no');
                const adddropoffTrain=document.getElementById('train_drop');
                const adddropoffTrainNo=document.getElementById('dropoff_train_no');
                const adddropoffHotel=document.getElementById('hotel_drop');
                const adddropoffPark=document.getElementById('park_drop');

                //service fields
                const servicecategory=document.getElementById('pickservicecategoryinput');
                const service=document.getElementById('pickserviceinput');

$("form#bookingform").on('submit', function(event){
    event.preventDefault();
            
                
                  //alert("hello");
                  var pickvalid=true;
                  var dropvalid=true;
                  var servicevalid=true;

                 
                  const addpickupCategoryValue = addpickupCategory.value.trim();
                  const adddropoffCategoryValue = adddropoffCategory.value.trim();
                  //validation for pickup category fields
                  if(addpickupCategoryValue=='address'){
                  pickvalid=checkPickupAddressCategory(addpickupAddress,addpickupAddressCity,addpickupAddressCode);
                  }
                  else if(addpickupCategoryValue=='package'){
                  pickvalid=checkPickupPackageCategory(addpickupPackage);
                  }
                  else if(addpickupCategoryValue=='airport'){
                  pickvalid=checkPickupAirportCategory(addpickupAirport,addpickupAirportFlightNo);
                  }
                  else if(addpickupCategoryValue=='train'){
                  pickvalid=checkPickupTrainCategory(addpickupTrain,addpickupTrainNo);
                  }
                  else if(addpickupCategoryValue=='hotel'){
                  pickvalid=checkPickupHotelCategory(addpickupHotel);
                  }
                  else if(addpickupCategoryValue=='park'){
                  pickvalid=checkPickupParkCategory(addpickupPark);
                  }
                  //validation for dropoff category fields
                   if(adddropoffCategoryValue=='address'){
                 dropvalid=checkDropoffAddressCategory(adddropoffAddress,adddropoffAddressCity,adddropoffAddressCode);
                  }
                  else if(adddropoffCategoryValue=='package'){
                 dropvalid=checkDropoffPackageCategory(adddropoffPackage);
                  }
                  else if(adddropoffCategoryValue=='airport'){
                 dropvalid=checkDropoffAirportCategory(adddropoffAirport,adddropoffAirportFlightNo);
                  }
                  else if(adddropoffCategoryValue=='train'){
                 dropvalid=checkDropoffTrainCategory(adddropoffTrain,adddropoffTrainNo);
                  }
                  else if(adddropoffCategoryValue=='hotel'){
                 dropvalid=checkDropoffHotelCategory(adddropoffHotel);
                  }
                  else if(adddropoffCategoryValue=='park'){
                 dropvalid=checkDropoffParkCategory(adddropoffPark);
                  }
                servicevalid=checkServiceCategory(servicecategory,service)
                if(pickvalid==true && dropvalid==true && servicevalid==true){
                   event.currentTarget.submit();
                }
              });


            //pickup validation
function checkPickupAddressCategory(pickupAddress,pickupAddressCity,pickupAddressCode){
  let valid=true;
  const pickupAddressValue     = pickupAddress.value.trim();
  const pickupAddressCityValue = pickupAddressCity.value.trim();
  const pickupAddressCodeValue = pickupAddressCode.value.trim();
  if(pickupAddressValue === '') {
      setErrorFor(pickupAddress, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(pickupAddress);
  }
  if(pickupAddressCityValue === '') {
      setErrorFor(pickupAddressCity, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(pickupAddressCity);
  }
  if(pickupAddressCodeValue === '') {
      setErrorFor(pickupAddressCode, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(pickupAddressCode);
  }
  return valid;
}
function checkPickupAirportCategory(pickupAirport,pickupAirportFlightNo){
  let valid=true;
  const pickupAirportValue     = pickupAirport.value.trim();
  const pickupAirportFlightNoValue = pickupAirportFlightNo.value.trim();
  
  if(pickupAirportValue === '') {
      setErrorFor(pickupAirport, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(pickupAirport);
  }
  if(pickupAirportFlightNoValue === '') {
      setErrorFor(pickupAirportFlightNo, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(pickupAirportFlightNo);
  }
  
  return valid;
}
function checkPickupTrainCategory(pickupTrain,pickupTrainNo){
  let valid=true;
  const pickupTrainValue   = pickupTrain.value.trim();
  const pickupTrainNoValue = pickupTrainNo.value.trim();
  
  if(pickupTrainValue === '') {
      setErrorFor(pickupTrain, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(pickupTrain);
  }
  if(pickupTrainNoValue === '') {
      setErrorFor(pickupTrainNo, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(pickupTrainNo);
  }
  
  return valid;
}
function checkPickupPackageCategory(pickupPackage){
  let valid=true;
  const pickupPackageValue = pickupPackage.value.trim();
  if(pickupPackageValue === '') {
      setErrorFor(pickupPackage, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(pickupPackage);
  }
  return valid;
}
function checkPickupHotelCategory(pickupHotel){
  let valid=true;
  const pickupHotelValue = pickupHotel.value.trim();
  if(pickupHotelValue === '') {
      setErrorFor(pickupHotel, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(pickupHotel);
  }
  return valid;
}
function checkPickupParkCategory(pickupPark){
  let valid=true;
  const pickupParkValue = pickupPark.value.trim();
  if(pickupParkValue === '') {
      setErrorFor(pickupPark, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(pickupPark);
  }
  return valid;
}
//pickup validation
//dropoff validation
function checkDropoffAddressCategory(dropoffAddress,dropoffAddressCity,dropoffAddressCode){
  let valid=true;
  const dropoffAddressValue     = dropoffAddress.value.trim();
  const dropoffAddressCityValue = dropoffAddressCity.value.trim();
  const dropoffAddressCodeValue = dropoffAddressCode.value.trim();
  if(dropoffAddressValue === '') {
      setErrorFor(dropoffAddress, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(dropoffAddress);
  }
  if(dropoffAddressCityValue === '') {
      setErrorFor(dropoffAddressCity, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(dropoffAddressCity);
  }
  if(dropoffAddressCodeValue === '') {
      setErrorFor(dropoffAddressCode, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(dropoffAddressCode);
  }
  return valid;
}
function checkDropoffAirportCategory(dropoffAirport,dropoffAirportFlightNo){
  let valid=true;
  const dropoffAirportValue     = dropoffAirport.value.trim();
  const dropoffAirportFlightNoValue = dropoffAirportFlightNo.value.trim();
  
  if(dropoffAirportValue === '') {
      setErrorFor(dropoffAirport, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(dropoffAirport);
  }
  if(dropoffAirportFlightNoValue === '') {
      setErrorFor(dropoffAirportFlightNo, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(dropoffAirportFlightNo);
  }
  
  return valid;
}
function checkDropoffTrainCategory(dropoffTrain,dropoffTrainNo){
  let valid=true;
  const dropoffTrainValue   = dropoffTrain.value.trim();
  const dropoffTrainNoValue = dropoffTrainNo.value.trim();
  
  if(dropoffTrainValue === '') {
      setErrorFor(dropoffTrain, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(dropoffTrain);
  }
  if(dropoffTrainNoValue === '') {
      setErrorFor(dropoffTrainNo, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(dropoffTrainNo);
  }
  
  return valid;
}
function checkDropoffPackageCategory(dropoffPackage){
  let valid=true;
  const dropoffPackageValue = dropoffPackage.value.trim();
  if(dropoffPackageValue === '') {
      setErrorFor(dropoffPackage, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(dropoffPackage);
  }
  return valid;
}
function checkDropoffHotelCategory(dropoffHotel){
  let valid=true;
  const dropoffHotelValue = dropoffHotel.value.trim();
  if(dropoffHotelValue === '') {
      setErrorFor(dropoffHotel, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(dropoffHotel);
  }
  return valid;
}
function checkDropoffParkCategory(dropoffPark){
  let valid=true;
  const dropoffParkValue = dropoffPark.value.trim();
  if(dropoffParkValue === '') {
      setErrorFor(dropoffPark, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(dropoffPark);
  }
  return valid;
}
//dropoff validation
//service category
function checkServiceCategory(servicecategory,service){
  let valid=true;
  const servicecategoryValue     = servicecategory.value.trim();
  const serviceValue = service.value.trim();

  if(servicecategoryValue === '') {
      setErrorFor(servicecategory, 'REQUIRED');
      valid=false;
  }else{
      setSuccessFor(servicecategory); 
    }
  if(serviceValue === '') {
      setErrorFor(service, 'REQUIRED');
      valid=false;
  }else{
    setSuccessFor(service);
  }
  return valid;
}
//service category
function setErrorFor(input, message) {
 let formControl=$(input).parents('.edit-cardform-control');
 let small= formControl.children('small');
 $(formControl).addClass("errors");
 $(small).html(message);
}
function setSuccessFor(input){
  let formControl=$(input).parents('.edit-cardform-control');
  let small= formControl.children('small');
 if($(formControl).hasClass("errors")){
   $(formControl).removeClass("errors");
 } 
}
</script>
 <script type="text/javascript">
  
               var currentpickaddress='';
                var currentpickzipcode='';
                var currentpickcity='';

                 var currentdropaddress='';
                var currentdropzipcode='';
                var currentdropcity='';

           function  setPickCurrentLocation(){
                     if(currentpickcity!=''){
                         
                             document.getElementById("pickup_city").value='';
                             document.getElementById("pickup_city").value = currentpickcity;
                        }
                        if(currentpickzipcode!=''){
                             document.getElementById("pickup_code").value='';
                             document.getElementById("pickup_code").value = currentpickzipcode;
                        }

                      
                      //set address
                      var address=$('#currentuserpoiinput').val();
                      $('#pickupaddress').val(address);
                      $('#pickupautocomplete').val('');

                      var city=currentpickcity+',';
                      city = new RegExp(city,"g");
                      var newaddress = address.replace(city, "");
                      
                      var country="france";
                      country=new RegExp(country,"i");
                       newaddress=newaddress.replace(country,"");

                      var zipcode=currentpickzipcode;
                      zipcode = new RegExp(zipcode,"g");
                      newaddress = newaddress.replace(zipcode, "");

                      newaddress = newaddress.replace(/, ,/g, ",");

                      if(newaddress.search(" ")==0){
                          newaddress = newaddress.replace(" " , "");
                      }
                        newaddress = newaddress.replace(/^,\s*/, "");
                        newaddress = newaddress.replace(/,\s*$/, "");
                         newaddress=newaddress.replace(/  +/g, ' ');
                      $('#pickupautocomplete').val(newaddress);

                      //set address

                    $("#currentuserpoidiv").hide();
           }
                    
            async function getCurrentLocation(){
                     if(currentpickaddress==''){
                         document.getElementById("locationloaderdiv").style.display = "block";
                         let m1= await getPickCurrentLocation();
                     
                         document.getElementById("currentuserpoiinput").value='';
                         document.getElementById("currentuserpoiinput").value =m1.address;
                          document.getElementById("locationloaderdiv").style.display = "none";
                     }
                   
                        $("#currentuserpoidiv").toggle();
   
                   }

     function getPickCurrentLocation(){

 return  new Promise((resolve,reject)=>{
 setTimeout(()=>{
 if(navigator.geolocation){
              
                navigator.geolocation.getCurrentPosition(function(position){
                
                   var lat = position.coords.latitude;
                   var lang = position.coords.longitude;
           
            var latlng =   new  google.maps.LatLng(lat, lang);
           
            var geocoder  = new google.maps.Geocoder();
     geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    if (results[0]) {
                         
                      //get postal code
                      var i;
                      var typeIdx;

                         for (i = 0; i < results[0].address_components.length; i++) {
                            var types = results[0].address_components[i].types;

                            for (typeIdx = 0; typeIdx < types.length; typeIdx++) {
                                if (types[typeIdx] == 'postal_code') {
                                   // console.log("postal code:"+results[0].address_components[i].short_name);
                                     currentpickzipcode=results[0].address_components[i].short_name;
                                }
                                 //get city
                                   if (types[typeIdx] == 'locality') {
                                    
                                    //console.log("city:"+results[0].address_components[i].short_name);
                                     currentpickcity=results[0].address_components[i].short_name;
                                }
                        
                            }
                        }

                         currentpickaddress=results[0].formatted_address;

                        
                        


                      //  console.log(results);
                      //  console.log(results[0]);
                      var addressinfo = {address:currentpickaddress, city:currentpickcity, zipcode:currentpickzipcode};
                       resolve(addressinfo);
                             
                    }
                }
                });
               });
            } else{
               alert("Sorry, browser does not support geolocation!");
            }
          //  var addressinfo = {address:currentpickaddress, city:currentpickcity, zipcode:currentpickzipcode};
     
           // return addressinfo;
           
 },2000);
 });
             
 
         
         }

         // drop address field
          function setDropCurrentLocation(){
                     if(currentdropcity!=''){
                         
                             document.getElementById("dropoff_city").value='';
                             document.getElementById("dropoff_city").value = currentdropcity;
                        }
                        if(currentdropzipcode!=''){
                             document.getElementById("dropoff_code").value='';
                             document.getElementById("dropoff_code").value = currentdropzipcode;
                        }

                      
                      //set address
                      var address=$('#dropcurrentuserpoiinput').val();
                      $('#dropoffaddress').val(address);
                      $('#dropoffautocomplete').val('');

                      var city=currentdropcity+',';
                      city = new RegExp(city,"g");
                      var newaddress = address.replace(city, "");
                      
                      var country="france";
                      country=new RegExp(country,"i");
                       newaddress=newaddress.replace(country,"");

                      var zipcode=currentdropzipcode;
                      zipcode = new RegExp(zipcode,"g");
                      newaddress = newaddress.replace(zipcode, "");

                      newaddress = newaddress.replace(/, ,/g, ",");

                      if(newaddress.search(" ")==0){
                          newaddress = newaddress.replace(" " , "");
                      }
                        newaddress = newaddress.replace(/^,\s*/, "");
                        newaddress = newaddress.replace(/,\s*$/, "");
                         newaddress=newaddress.replace(/  +/g, ' ');
                      $('#dropoffautocomplete').val(newaddress);

                      //set address

                    $("#dropcurrentuserpoidiv").hide();
           }
                    
            async function getDropCurrentLocation(){
              
                     if(currentdropaddress==''){
                     
                         document.getElementById("droplocationloaderdiv").style.display = "block";
                         let m1= await getDropFullCurrentLocation();
                     
                         document.getElementById("dropcurrentuserpoiinput").value='';
                         document.getElementById("dropcurrentuserpoiinput").value =m1.address;
                          document.getElementById("droplocationloaderdiv").style.display = "none";
                     }
                   
                        $("#dropcurrentuserpoidiv").toggle();
   
                   }

     function getDropFullCurrentLocation(){

 return  new Promise((resolve,reject)=>{
 setTimeout(()=>{
 if(navigator.geolocation){
              
                navigator.geolocation.getCurrentPosition(function(position){
                
                   var lat = position.coords.latitude;
                   var lang = position.coords.longitude;
           
            var latlng =   new  google.maps.LatLng(lat, lang);
           
            var geocoder  = new google.maps.Geocoder();
     geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    if (results[0]) {
                         
                      //get postal code
                      var i;
                      var typeIdx;

                         for (i = 0; i < results[0].address_components.length; i++) {
                            var types = results[0].address_components[i].types;

                            for (typeIdx = 0; typeIdx < types.length; typeIdx++) {
                                if (types[typeIdx] == 'postal_code') {
                                   // console.log("postal code:"+results[0].address_components[i].short_name);
                                     currentdropzipcode=results[0].address_components[i].short_name;
                                }
                                 //get city
                                   if (types[typeIdx] == 'locality') {
                                    
                                    //console.log("city:"+results[0].address_components[i].short_name);
                                     currentdropcity=results[0].address_components[i].short_name;
                                }
                        
                            }
                        }

                         currentdropaddress=results[0].formatted_address;

                        
                        


                      //  console.log(results);
                      //  console.log(results[0]);
                      var addressinfo = {address:currentdropaddress, city:currentdropcity, zipcode:currentdropzipcode};
                       resolve(addressinfo);
                             
                    }
                }
                });
               });
            } else{
               alert("Sorry, browser does not support geolocation!");
            }
        
           
 },5000);
 });
             
 
         
         }

 </script>       
 <script type="text/javascript">
           $(document).ready(function(){
                    

                $('.go-table').hide();
                $('.back-table').hide();
                $('.tmpr_txt').hide();
                $('#timeslotsrtn').hide();
               if($('.wheelchair_carssh').is(":checked")) {
                $('#wheelchairdiv').show();
                     $('#nonwheelchairdiv').hide();
                    document.getElementById("carstypes").value='';
                    document.getElementById("carstypes").value='1';
                }else{
                    $('#nonwheelchairdiv').show();
                     $('#wheelchairdiv').hide();
                      document.getElementById("carstypes").value='';
                      document.getElementById("carstypes").value='0';
                 
                }
                if($('.regular_check').is(":checked")) {
                    $('#timeslotsrtn').show();
                    $('.go-table').show();
                    $(".pickupreturn_datetime_div").hide();    
                    $(".startend_datetime_div").show();   
                    if($('.return_car').is(":checked")){
                          $(".back-table").show();
                      }  
               
                }else{
                   
                    $('.go-table').hide();
                    $('#timeslotsrtn').hide();
                    $('.back-table').hide();
                    $(".pickupreturn_datetime_div").show();  
                    $(".startend_datetime_div").hide();        
                }

                if($('.return_car').is(":checked")) {
                   
                    $(".return_datetime_div").show();
                   
                    if($('.regular_check').is(":checked")){
                         $('#timeslotsrtn').show();
                         $('.back-table').show();
                      }
                  
                }else{
                    
                     $(".return_datetime_div").hide();
                     if($('.regular_check').is(":checked")){
                         $('.back-table').hide();
                      }
                }
         

             $('.regular_check').change(function() {
                if($(this).is(":checked")) {
                    $('#timeslotsrtn').show();
                    $('.go-table').show();
                    $(".pickupreturn_datetime_div").hide();    
                    $(".startend_datetime_div").show();   
                    if($('.return_car').is(":checked")){
                     //alert("check");
                          $(".back-table").show();
                      }  
               
                }else{
                   
                    $('.go-table').hide();
                    $('#timeslotsrtn').hide();
                    $('.back-table').hide();
                    $(".pickupreturn_datetime_div").show();  
                    $(".startend_datetime_div").hide();        
                }
             });
             $('.return_car').change(function() {
               
                 if($(this).is(":checked")) {
                   
                    $(".return_datetime_div").show();
                   
                    if($('.regular_check').is(":checked")){
                         $('#timeslotsrtn').show();
                         $('.back-table').show();
                      }
                  
                }else{
                    
                     $(".return_datetime_div").hide();
                     if($('.regular_check').is(":checked")){
                         $('.back-table').hide();
                      }
                }
                 
              
               
             });
              $('.wheelchair_carssh').change(function() {
                if($(this).is(":checked")) {
                    $('#wheelchairdiv').show();
                     $('#nonwheelchairdiv').hide();
                    document.getElementById("carstypes").value='';
                    document.getElementById("carstypes").value='1';
                }else{
                    $('#nonwheelchairdiv').show();
                     $('#wheelchairdiv').hide();
                      document.getElementById("carstypes").value='';
                      document.getElementById("carstypes").value='0';
                 
                }
             });
            
             
 

       
            });    
        </script>
    
     <script>
// This sample uses the Autocomplete widget to help the user select a
// place, then it retrieves the address components associated with that
// place, and then it populates the form fields with those details.
// This sample requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script
// src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
  var n=0;
  var stopper=new Array();
var placeSearch, autocomplete;

var componentForm = {

  locality: 'long_name',
  postal_code: 'short_name'
};

function initAutocomplete() {
  // Create the autocomplete object, restricting the search predictions to
  // geographical location types.
   var input = document.getElementById('pickupautocomplete');
  autocomplete = new google.maps.places.Autocomplete(
      document.getElementById('pickupautocomplete'), {types: ['geocode'],componentRestrictions: {country: "fr"}});
  autodropcomplete = new google.maps.places.Autocomplete(
      document.getElementById('dropoffautocomplete'), {types: ['geocode'],componentRestrictions: {country: "fr"}});
  // Avoid paying for data that you don't need by restricting the set of
  // place fields that are returned to just the address components.
  autocomplete.setFields(['address_component','geometry']);
  autocomplete.addListener('place_changed', fillInAddress);

  autodropcomplete.setFields(['address_component','geometry']);
  autodropcomplete.addListener('place_changed', fillInDropAddress);

 //stoper address
if(n==0){
  if($('#stopcountfield').length){
    n=document.getElementById('stopcountfield').value;
    for(var s=1;s <= n;s++){
       stopper.push(s);
    }
 }
}
 if(n > 0){
   for(var i=1;i <= n;i++){
   
     if(stopper.includes(i)){
  stopaddr = new google.maps.places.Autocomplete(
      document.getElementById('stopaddr_'+i), {types: ['geocode'],componentRestrictions: {country: "fr"}});
  stopaddr.setFields(['address_component','geometry']);
  stopaddr.addListener('place_changed', fillInStopAddress);
    }
   }
 }
 
 //stoper address

}
function fillInStopAddress(){
   // Get the place details from the autocomplete object.
  var place = stopaddr.getPlace();
  if(place.address_components){
   
     for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
  }

  }
}

function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  
      document.getElementById("pickup_city").value = '';
      document.getElementById("pickup_code").value = '';
      //document.getElementById("pickup_city").disabled = false;
      //document.getElementById("pickup_code").disabled = false;
      document.getElementById('pickloc_lat').value='';
      document.getElementById('pickloc_long').value='';

  // Get each component of the address from the place details,
  // and then fill-in the corresponding field on the form.

  if(place.address_components){
   
     for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];

    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
       if(addressType=="locality"){
          document.getElementById("pickup_city").value = val;
         //document.getElementById("pickup_city").disabled = true;
          
       }
       else if(addressType=="postal_code"){
           document.getElementById("pickup_code").value = val;
           //document.getElementById("pickup_code").disabled = true;
       }
     
    }
  }

          document.getElementById('pickloc_lat').value = place.geometry.location.lat();
          document.getElementById('pickloc_long').value = place.geometry.location.lng();
          console.log('latitude'+place.geometry.location.lat());
           console.log('longitude'+place.geometry.location.lng());
  }

var address=$('#pickupautocomplete').val();
 $('#pickupaddress').val(address);
$('#pickupautocomplete').val('');

var city=($('#pickup_city').val())+',';
city = new RegExp(city,"g");
 var newaddress = address.replace(city, "");

var country="france";
country=new RegExp(country,"i");
 newaddress=newaddress.replace(country,"");

 var zipcode=($('#pickup_code').val());
 zipcode = new RegExp(zipcode,"g");
 newaddress = newaddress.replace(zipcode, "");
 newaddress = newaddress.replace(/, ,/g, ",");

 if(newaddress.search(" ")==0){
    newaddress = newaddress.replace(" " , "");
  }
 newaddress = newaddress.replace(/^,\s*/, "");
 newaddress = newaddress.replace(/,\s*$/, "");
 newaddress=newaddress.replace(/  +/g, ' ');

 $('#pickupautocomplete').val(newaddress);


 



}

function fillInDropAddress() {
   // Get the place details from the autocomplete object.
  var place = autodropcomplete.getPlace();
console.log("place"+place);
  
      document.getElementById("dropoff_city").value = '';
      document.getElementById("dropoff_code").value = '';
      //document.getElementById("dropoff_city").disabled = false;
      //document.getElementById("dropoff_code").disabled = false;
      document.getElementById('droploc_lat').value='';
      document.getElementById('droploc_long').value='';

  // Get each component of the address from the place details,
  // and then fill-in the corresponding field on the form.
  if(place.address_components){
   
     for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];

    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
       if(addressType=="locality"){
          document.getElementById("dropoff_city").value = val;
         //document.getElementById("dropoff_city").disabled = true;
          
       }
       else if(addressType=="postal_code"){
           document.getElementById("dropoff_code").value = val;
          // document.getElementById("dropoff_code").disabled = true;
       }
     
    }
  }
  console.log(place);
          document.getElementById('droploc_lat').value = place.geometry.location.lat();
          document.getElementById('droploc_long').value = place.geometry.location.lng();
  }
 var address=$('#dropoffautocomplete').val();
 $('#dropoffaddress').val(address);
 $('#dropoffautocomplete').val('');

var city=($('#dropoff_city').val())+',';
city = new RegExp(city,"g");
var newaddress = address.replace(city, "");

var country="france";
country=new RegExp(country,"i");
newaddress=newaddress.replace(country,"");

 var zipcode=$('#dropoff_code').val();
 zipcode = new RegExp(zipcode,"g");
 newaddress = newaddress.replace(zipcode, "");
 newaddress = newaddress.replace(/, ,/g, ",");
 if(newaddress.search(" ")==0){
    newaddress = newaddress.replace(" " , "");
  }
 newaddress = newaddress.replace(/^,\s*/, "");
 newaddress = newaddress.replace(/,\s*$/, "");
 newaddress=newaddress.replace(/  +/g, ' ');
 $('#dropoffautocomplete').val(newaddress);
}
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      console.log("geolocation"+geolocation);
      var circle = new google.maps.Circle(
          {center: geolocation, radius: position.coords.accuracy});
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
    </script>
<script type="text/javascript">
  $(document).ready(function(){
 calwaitingtime();

 

  function calwaitingtime() {
  var time1= document.getElementById("timepicker1").value;
  var time2= document.getElementById("waitingtimepicker").value
  var a=time1.split(":");
  var b=time2.split(":");
  var h=parseInt(a[0])+parseInt(b[0]);
  var m=parseInt(a[1])+parseInt(b[1]);
  if(m>=60){
    m=m-60;
    if(m<10){
      m="0"+m; 
    }
    h=h+1;
  }else{
     if(m<10){
      m="0"+m; 
    }
  }

  if(h >= 24){
  
    h=h-24;
    if(h<10){
      h="0"+h;
     }
  }else{
     if(h<10){
      h="0"+h;
     }
  }
  document.getElementById("timepicker2").value = h.toString()+" : "+m.toString();
}
  });
</script>
<script type="text/javascript">

         $(document).ready(function(){
         $(".weekdays_go").click(function(){
            var day = $(this).val();
            var weekdays = $(this);
            console.log(day);
            console.log($(weekdays).is(":checked"));
            if ($(weekdays).is(":checked")) {
                $("#go_time_"+day).show();
            }
            else {
                $("#go_time_"+day).hide();
            }
        });
        
        $(".weekdays_back").click(function(){
            var day = $(this).val();
            var weekdays = $(this);
            console.log(day);
            console.log($(weekdays).is(":checked"));
            if ($(weekdays).is(":checked")) {
                $("#back_time_"+day).show();
            }
            else {
                $("#back_time_"+day).hide();
            }
        });
    });
    </script>
    <script type="text/javascript">
      $(document).ready(function(){

        $("#pickaddresscategorybtn").click(function(){
          document.getElementById("pickupcategory").value='';
          document.getElementById("pickupcategory").value='address';
        });
        $("#pickpackagecategorybtn").click(function(){
          document.getElementById("pickupcategory").value='';
          document.getElementById("pickupcategory").value='package';
        });
        $("#pickairportcategorybtn").click(function(){
          document.getElementById("pickupcategory").value='';
          document.getElementById("pickupcategory").value='airport';
        });
        $("#picktraincategorybtn").click(function(){
          document.getElementById("pickupcategory").value='';
          document.getElementById("pickupcategory").value='train';
        });
        $("#pickhotelcategorybtn").click(function(){
          document.getElementById("pickupcategory").value='';
          document.getElementById("pickupcategory").value='hotel';
        });
        $("#pickparkcategorybtn").click(function(){
          document.getElementById("pickupcategory").value='';
          document.getElementById("pickupcategory").value='park';
        });

          $("#dropaddresscategorybtn").click(function(){
          document.getElementById("dropoffcategory").value='';
          document.getElementById("dropoffcategory").value='address';
        });
        $("#droppackagecategorybtn").click(function(){
          document.getElementById("dropoffcategory").value='';
          document.getElementById("dropoffcategory").value='package';
        });
        $("#dropairportcategorybtn").click(function(){
          document.getElementById("dropoffcategory").value='';
          document.getElementById("dropoffcategory").value='airport';
        });
        $("#droptraincategorybtn").click(function(){
          document.getElementById("dropoffcategory").value='';
          document.getElementById("dropoffcategory").value='train';
        });
        $("#drophotelcategorybtn").click(function(){
          document.getElementById("dropoffcategory").value='';
          document.getElementById("dropoffcategory").value='hotel';
        });
        $("#dropparkcategorybtn").click(function(){
          document.getElementById("dropoffcategory").value='';
          document.getElementById("dropoffcategory").value='park';
        });
      });
    </script>
    <script>
      $( window ).load(function() {
       var stopcount=document.getElementById('stopcountfield').value;
        for(var i=1;i<=stopcount;i++){
                 $("#stopwaitingtimepicker_"+i).timepicki({
                  type_of_timezone:"zero",
                  step_size_minutes:'5',
                  show_meridian:false,
                  min_hour_value:0,
                  max_hour_value:23,
                  overflow_minutes:true,
                  increase_direction:'up',
                  disable_keyboard_mobile: true});
        }
     });
     
  function addservice(){
    var val=$("#pickservicecategoryinput").val();
    var selectedtext=$("#pickservicecategoryinput").find(":selected").text();
    selectedtext=selectedtext.trim();
    if(selectedtext.toLowerCase()=="reccurent"){
      if($(".regular_check").is(":checked")) {
          
      }else{
        $(".regular_check").trigger("click");
      }         
    }
    else{
       if($(".regular_check").is(":checked")) {
           $(".regular_check").trigger("click");
      }
    }
   
    if (val=='')
    {

      val=0;
    }
         $.ajax({
                type: "GET",
                url: '<?php echo base_url().'getbookingservices'; ?>',
                data: {'category_id': val},
                success: function (result) {
                
                  $("#pickserviceinput").empty();
                  document.getElementById("pickserviceinput").innerHTML =result;
                }
              });
   
   
  }
    function addpoidata(){
    var val=$("#pickpackagefield").val();
    
     $("#droppackagecategorybtn").trigger("click");
     $('#droppackagefield').val(val);
 
    if (val=='')
    {
          
      val=0;
     } 
    
         $.ajax({
                type: "GET",
                url: '<?php echo base_url().'getpoidatabypackage'; ?>',
                data: {'package_id': val},
                success: function (data) {
                if(data.result=='200'){

                  $('#packagedepartaddress').val(data.poiaddress);
                  $('#packagedepartcategoryname').val(data.poicategoryname);
                  $('#packagedepartname').val(data.poiname);
                  $('#packagedepartzipcode').val(data.zipcode);
                  $('#packagedepartcity').val(data.city);
               
                  $('#packagedestinationaddress').val(data.destinationpoiaddress);
                  $('#packagedestinationcategoryname').val(data.destinationpoicategoryname);
                  $('#packagedestinationname').val(data.destinationpoiname);
                   $('#packagedestinationzipcode').val(data.destinationzipcode);
                  $('#packagedestinationcity').val(data.destinationcity);


                }
                else{
                
               
                  $('#packagedepartaddress').val('');
                  $('#packagedepartcategoryname').val('');
                  $('#packagedepartname').val('');
                  $('#packagedepartzipcode').val('');
                  $('#packagedepartcity').val('');


                   $('#packagedestinationaddress').val('');
                  $('#packagedestinationcategoryname').val('');
                  $('#packagedestinationname').val('');
                   $('#packagedestinationzipcode').val('');
                  $('#packagedestinationcity').val('');

                  }

                }
              });
   
   }
   function adddroppoidata(){
    var val=$("#droppackagefield").val();
 
    if (val=='')
    {
          
      val=0;
     } 
    
         $.ajax({
                type: "GET",
                url: '<?php echo base_url().'getdroppoidatabypackage'; ?>',
                data: {'package_id': val},
                success: function (data) {
                if(data.result=='200'){

                  $('#packagedestinationaddress').val(data.poiaddress);
                  $('#packagedestinationcategoryname').val(data.poicategoryname);
                  $('#packagedestinationname').val(data.poiname);
                  $('#packagedestinationzipcode').val(data.zipcode);
                  $('#packagedestinationcity').val(data.city);
  

                }
                else{
                
               
                  $('#packagedestinationaddress').val('');
                  $('#packagedestinationcategoryname').val('');
                  $('#packagedestinationname').val('');
                  $('#packagedestinationzipcode').val('');
                  $('#packagedestinationcity').val('');


                  }

                }
              });
   
   }
  
     function closeErrorDiv(e){
     
      e.closest('div').remove();
     }
      function getpickcategorydata(e){
       $pickcategory= document.getElementById("pickupcategory").value;
      var val=$(e).val();
   
 
   if (val=='')
    {
          
      val=0;
     } 
    
         $.ajax({
                type: "GET",
                url: '<?php echo base_url().'getpoidatabycategory'; ?>',
                data: {'poi_id': val},
                success: function (data) {
                if(data.result=='200'){
                 if($pickcategory=="airport"){
                   $('#pickairportaddress').val(data.address);
                   $('#pickairportzipcode').val(data.zipcode);
                   $('#pickairportcity').val(data.city);
                 }
                 else if($pickcategory=="train"){
                   $('#picktrainaddress').val(data.address);
                   $('#picktrainzipcode').val(data.zipcode);
                   $('#picktraincity').val(data.city);
                 }

                  else if($pickcategory=="hotel"){
                   $('#pickhoteladdress').val(data.address);
                   $('#pickhotelzipcode').val(data.zipcode);
                   $('#pickhotelcity').val(data.city);
                 }

                  else if($pickcategory=="park"){
                   $('#pickparkaddress').val(data.address);
                   $('#pickparkzipcode').val(data.zipcode);
                   $('#pickparkcity').val(data.city);
                 }
                

                }
                else{
                
               
                   if($pickcategory=="airport"){
                   $('#pickairportaddress').val('');
                   $('#pickairportzipcode').val('');
                   $('#pickairportcity').val('');
                 }
                 else if($pickcategory=="train"){
                   $('#picktrainaddress').val('');
                   $('#picktrainzipcode').val('');
                   $('#picktraincity').val('');
                 }

                  else if($pickcategory=="hotel"){
                   $('#pickhoteladdress').val('');
                   $('#pickhotelzipcode').val('');
                   $('#pickhotelcity').val('');
                 }

                  else if($pickcategory=="park"){
                   $('#pickparkaddress').val('');
                   $('#pickparkzipcode').val('');
                   $('#pickparkcity').val('');
                 }
                
                  }

                }
              });
   
   }
  function getdropcategorydata(e){
       $dropcategory= document.getElementById("dropoffcategory").value;
      var val=$(e).val();
   
 
   if (val=='')
    {
          
      val=0;
     } 
    
         $.ajax({
                type: "GET",
                url: '<?php echo base_url().'getpoidatabycategory'; ?>',
                data: {'poi_id': val},
                success: function (data) {
                if(data.result=='200'){
                 if($dropcategory=="airport"){
                   $('#dropairportaddress').val(data.address);
                   $('#dropairportzipcode').val(data.zipcode);
                   $('#dropairportcity').val(data.city);
                 }
                 else if($dropcategory=="train"){
                   $('#droptrainaddress').val(data.address);
                   $('#droptrainzipcode').val(data.zipcode);
                   $('#droptraincity').val(data.city);
                 }

                  else if($dropcategory=="hotel"){
                   $('#drophoteladdress').val(data.address);
                   $('#drophotelzipcode').val(data.zipcode);
                   $('#drophotelcity').val(data.city);
                 }

                  else if($dropcategory=="park"){
                   $('#dropparkaddress').val(data.address);
                   $('#dropparkzipcode').val(data.zipcode);
                   $('#dropparkcity').val(data.city);
                 }
                

                }
                else{
                
               
                   if($dropcategory=="airport"){
                   $('#dropairportaddress').val('');
                   $('#dropairportzipcode').val('');
                   $('#dropairportcity').val('');
                 }
                 else if($dropcategory=="train"){
                   $('#droptrainaddress').val('');
                   $('#droptrainzipcode').val('');
                   $('#droptraincity').val('');
                 }

                  else if($dropcategory=="hotel"){
                   $('#drophoteladdress').val('');
                   $('#drophotelzipcode').val('');
                   $('#drophotelcity').val('');
                 }

                  else if($dropcategory=="park"){
                   $('#dropparkaddress').val('');
                   $('#dropparkzipcode').val('');
                   $('#dropparkcity').val('');
                 }
                
                  }

                }
              });
   
   }
   function myFunction(){
    alert("hello");
   }
 
   function addstopboxes(){
    n++;
    $("#stopaddressdiv").append('<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 stopinnerdiv" style="padding:0px;"><div class="col-lg-7 col-md-7 col-sm-7 col-xs-12" style="padding:0px;width: 67%;"><div class="form-group"><div class="input-group" style="position: relative;"><span class="input-group-addon"><i class="fa fa-map-marker"></i></span><input type="text" name="stopaddress[]" placeholder="Addresse line"  class="pick_up_txtbox_bkg" id="stopaddr_'+n+'"   onFocus="geolocate()" value=""  style="padding-right: 30px;height: 30px !important;border-radius: 0px"></div><div class="currentstoplocationbtn" onclick="getStopCurrentLocation('+n+')"><img src="<?= base_url()?>assets/theme/default/images/target-a.svg"></div><div class="currentstoplocationdiv" id="stopercurrentlocationdiv_'+n+'"><div class="form-group"><div class="inner-addon left-addon"><i class="glyphicon glyphicon-map-marker" style="top: -2px;"></i><input type="text" readonly id="stopercurrentlocationfield_'+n+'" class="currentstoppoiinput" onclick="setStopCurrentLocation('+n+')" style="height:30px !important;"></div></div></div><div class="currentstoplocationloader" id="stoperlocationloader_'+n+'"><img src="<?= base_url()?>assets/theme/default/images/loader_2.gif" ></div> </div></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding:0px;width: 28%;padding-left: 5px;"><div class="col-lg-7 col-md-7 col-sm-7 col-xs-12" style="padding:0px;width:58%;"><label class="pickup_time pickup_time_label" style="margin:0px !important;padding-top: 9px !important;font-size:13px !important;"><?php echo 'Waiting Time'; ?></label></div><div class="col-lg-5 col-md-5 col-sm-5 col-xs-12" style="padding:0px;width:42%;"><input class="stopwaitingtimepicker" id="stopwaitingtimepicker_'+n+'"  name="stopwaittime[]" type="text" value="<?php echo date('00 : 00') ?>" style="width:100%;height: 30px !important;border-radius: 0px" /></div></div><div class="col-lg-1 col-md-1 col-sm-1" style="width: 5%;padding: 0px;padding-left: 5px;"><button type="button"  onclick="removestopboxes(this,'+n+');" class="minusstopicon"><i class="fa fa-minus" style="margin:0px;"></i></button></div></div>');
 
    $("#stopwaitingtimepicker_"+n).timepicki({
        type_of_timezone:"zero",
        step_size_minutes:'5',
        show_meridian:false,
        min_hour_value:0,
        max_hour_value:23,
        overflow_minutes:true,
        increase_direction:'up',
        disable_keyboard_mobile: true});
        stopper.push(n);
        initAutocomplete();
     
   }
   function removestopboxes(e,current){
   
     $(e).closest(".stopinnerdiv").remove();
      const index = stopper.indexOf(current);
      if (index > -1) {
        stopper.splice(index, 1);
      }
}
      var currentstopaddress='';
      async function getStopCurrentLocation(currentstoper){
              
                     if(currentstopaddress==''){
                         document.getElementById("stoperlocationloader_"+currentstoper).style.display = "block";
                         let m1= await getStopperCurrentLocation();
                     
                         document.getElementById("stopercurrentlocationfield_"+currentstoper).value='';
                         document.getElementById("stopercurrentlocationfield_"+currentstoper).value =m1.address;
                         document.getElementById("stoperlocationloader_"+currentstoper).style.display = "none";
                     }else{
                      document.getElementById("stopercurrentlocationfield_"+currentstoper).value =currentstopaddress;
                     }

                   
                        $("#stopercurrentlocationdiv_"+currentstoper).toggle();
   
                   }

     function getStopperCurrentLocation(){

       return  new Promise((resolve,reject)=>{
       setTimeout(()=>{
       if(navigator.geolocation){
                    
                      navigator.geolocation.getCurrentPosition(function(position){
                      
                         var lat = position.coords.latitude;
                         var lang = position.coords.longitude;
                 
                  var latlng =   new  google.maps.LatLng(lat, lang);
                 
                  var geocoder  = new google.maps.Geocoder();
           geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                      if (status == google.maps.GeocoderStatus.OK) {

                          if (results[0]) {
                               
                            var i;
                            var typeIdx;
                            currentstopaddress=results[0].formatted_address;
                            var addressinfo = {address:currentstopaddress};
                            resolve(addressinfo);
                                   
                          }
                      }
                      });
                     });
                  } else{
                     alert("Sorry, browser does not support geolocation!");
                  }
              
       },2000);
       });
}

 function  setStopCurrentLocation(currentstoper){                                           
                      //set address
                      var address=$('#stopercurrentlocationfield_'+currentstoper).val();
                      $('#stopaddr_'+currentstoper).val(address);
                      $("#stopercurrentlocationdiv_"+currentstoper).hide();
           }
   function weekdays_go(e){
     
            var day = $(e).val();
            var weekdays = $(e);
           
            if ($(weekdays).is(":checked")) {
                $("#go_time_"+day).show();
            }
            else {
                $("#go_time_"+day).hide();
            }
        }
        
       
        function weekdays_back(e){
            var day = $(e).val();
            var weekdays = $(e);

            if ($(weekdays).is(":checked")) {
                $("#back_time_"+day).show();
            }
            else {
                $("#back_time_"+day).hide();
            }
        }
    const pickdaycheck_1=document.getElementById('pickdaycheck_1');
                    const pickdaycheck_2=document.getElementById('pickdaycheck_2');
                    const pickdaycheck_3=document.getElementById('pickdaycheck_3');
                    const pickdaycheck_4=document.getElementById('pickdaycheck_4');
                    const pickdaycheck_5=document.getElementById('pickdaycheck_5');
                    const pickdaycheck_6=document.getElementById('pickdaycheck_6');
                    const pickdaycheck_7=document.getElementById('pickdaycheck_7');


                    weekdays_go(pickdaycheck_1);
                    weekdays_go(pickdaycheck_2);
                    weekdays_go(pickdaycheck_3);
                    weekdays_go(pickdaycheck_4);
                    weekdays_go(pickdaycheck_5);
                    weekdays_go(pickdaycheck_6);
                    weekdays_go(pickdaycheck_7);

                    const returndaycheck_1=document.getElementById('returndaycheck_1');
                    const returndaycheck_2=document.getElementById('returndaycheck_2');
                    const returndaycheck_3=document.getElementById('returndaycheck_3');
                    const returndaycheck_4=document.getElementById('returndaycheck_4');
                    const returndaycheck_5=document.getElementById('returndaycheck_5');
                    const returndaycheck_6=document.getElementById('returndaycheck_6');
                    const returndaycheck_7=document.getElementById('returndaycheck_7');


                    weekdays_back(returndaycheck_1);
                    weekdays_back(returndaycheck_2);
                    weekdays_back(returndaycheck_3);
                    weekdays_back(returndaycheck_4);
                    weekdays_back(returndaycheck_5);
                    weekdays_back(returndaycheck_6);
                    weekdays_back(returndaycheck_7);
    </script>
