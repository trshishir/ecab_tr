           <div class="card">              
                <div class="card-body bg-light">
                   <input type="hidden" id="creditcardbtnnum" value="<?= $paymentmethodcount ?>">
                    <?php if (validation_errors()): ?>
                        <div class="alert alert-danger" role="alert">
                            <strong>Oops!</strong>
                            <?php echo validation_errors() ;?> 
                        </div>  
                    <?php endif ?>
                    <div id="payment-errors"></div>  
                       <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                        <div class="form-group cardform-control <?php if(isset($payment_method_form['credit_card_name']['error'])){
                                     echo (!empty($payment_method_form['credit_card_name']['error']))?'errors':'success';
                                     } ?>" >
                          <input type="text" list="cardnames" placeholder="Name on card"  name="name_<?= $cardtype ?>" id="name_<?= $cardtype ?>" autocomplete="off" value="<?php  if(isset($payment_method_form['credit_card_name']['value']) && !empty($payment_method_form['credit_card_name']['value'])){echo $payment_method_form['credit_card_name']['value'];} ?>">
                            <datalist id="cardnames">
                              <option value="Visa">
                              <option value="Mastercard">
                              <option value="Amex">
                              <option value="Unionpay">
                              <option value="Jcb">
                              <option value="Discover">
                              <option value="Diners">
                            </datalist>
                           
                              <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                              <?php
                     if(isset($payment_method_form['credit_card_name']['error']) && !empty($payment_method_form['credit_card_name']['error'])){     echo strip_tags($payment_method_form['credit_card_name']['error']);
                            }
                      ?>
                             </small>
                        </div>  
                      </div>
                    </div>

                      
                      <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                         <div class="form-group cardform-control <?php if(isset($payment_method_form['credit_card_number']['error'])){
                                     echo (!empty($payment_method_form['credit_card_number']['error']))?'errors':'success';
                                     } ?>">
                         
                            <input type="text" name="card_num_<?= $cardtype ?>" id="card_num_<?= $cardtype ?>" class="form-control" placeholder="Card Number" autocomplete="off" value="<?php  if(isset($payment_method_form['credit_card_number']['value']) && !empty($payment_method_form['credit_card_number']['value'])){echo $payment_method_form['credit_card_number']['value'];} ?>" >
                             <i class="fa fa-check-circle"></i>
                              <i class="fa fa-exclamation-circle"></i>
                              <small>
                       <?php
                     if(isset($payment_method_form['credit_card_number']['error']) && !empty($payment_method_form['credit_card_number']['error'])){     echo strip_tags($payment_method_form['credit_card_number']['error']);
                            }
                      ?>
                              </small>
                        </div>
                      </div>
                    </div>
                       
                        
                        <div class="row" style="padding-bottom: 20px;">

                            <div class="col-sm-8">
                                 <div class="row">
                                    <div class="col-sm-6">
                                  <div class="form-group cardform-control <?php if(isset($payment_method_form['credit_card_month']['error'])){
                                     echo (!empty($payment_method_form['credit_card_month']['error']))?'errors':'success';
                                     }?>" >
                                        <select name="exp_month_<?= $cardtype ?>" id="exp_month_<?= $cardtype ?>">
                                          <option value="">Month</option>
                                          <?php for($i=1;$i<13;$i++){ ?>
                                             <option <?php  if(isset($payment_method_form['credit_card_month']['value']) && !empty($payment_method_form['credit_card_month']['value'])){ echo ($payment_method_form['credit_card_month']['value']==$i)?"selected":"";} ?> value="<?= $i ?>"><?= ($i<10)?'0'.$i:$i; ?></option>
                                          <?php } ?>
                                        </select>
                                          
                                             <i class="fa fa-check-circle"></i>
                                              <i class="fa fa-exclamation-circle"></i>
                                              <small>
                                                <?php
                                               if(isset($payment_method_form['credit_card_month']['error']) && !empty($payment_method_form['credit_card_month']['error'])){ echo strip_tags($payment_method_form['credit_card_month']['error']);
                                                      }
                                                ?>
                                              </small>
                                          
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                    <div class="form-group cardform-control <?php if(isset($payment_method_form['credit_card_year']['error'])){
                                     echo (!empty($payment_method_form['credit_card_year']['error']))?'errors':'success';
                                     }?>">
                                          <select name="exp_year_<?= $cardtype ?>" id="exp_year_<?= $cardtype ?>">
                                          <option value="">Year</option>
                                          <?php for($j=20;$j<36;$j++){ ?>
                                            <option <?php  if(isset($payment_method_form['credit_card_year']['value']) && !empty($payment_method_form['credit_card_year']['value'])){ echo ($payment_method_form['credit_card_year']['value']==('20'.$j))?"selected":"";} ?> value="<?= '20'.$j ?>"><?= '20'.$j ?></option>
                                          <?php } ?>
                                          
                                        
                                        </select>
                                          
                                             <i class="fa fa-check-circle"></i>
                                              <i class="fa fa-exclamation-circle"></i>
                                              <small>
                                                <?php
                                               if(isset($payment_method_form['credit_card_year']['error']) && !empty($payment_method_form['credit_card_year']['error'])){ echo strip_tags($payment_method_form['credit_card_year']['error']);
                                                      }
                                                ?>
                                              </small>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group cardform-control <?php if(isset($payment_method_form['credit_card_cvc']['error'])){
                                  echo (!empty($payment_method_form['credit_card_cvc']['error']))?'errors':'success';
                                }?>">
                                    <input type="text" name="cvc_<?= $cardtype ?>" id="cvc_<?= $cardtype ?>" maxlength="4" class="form-control" autocomplete="off" placeholder="CVC" value="<?php  if(isset($payment_method_form['credit_card_cvc']['value']) && !empty($payment_method_form['credit_card_cvc']['value'])){echo $payment_method_form['credit_card_cvc']['value'];} ?>" >
                                     <i class="fa fa-check-circle"></i>
                                      <i class="fa fa-exclamation-circle"></i>
                                      <small>
                                         <?php
                                               if(isset($payment_method_form['credit_card_cvc']['error']) && !empty($payment_method_form['credit_card_cvc']['error'])){ echo strip_tags($payment_method_form['credit_card_cvc']['error']);
                                                      }
                                                ?>
                                      </small>
                                </div>
                            </div>
                        </div>                   
                   </div>
               </div>