<div class="col-md-12" style="padding:0px;">

  <div class="col-md-5" style="padding:0px;">
    <ul class="ulbdnone" >
    <li class="text-left" ><strong>Service Category : </strong>
      <?php  
     $servicecategory= $this->bookings_model->getservicesbyid('vbs_u_category_service', $booking['servicecategory']);
       echo $servicecategory->category_name;
       ?></li>
      </ul>
    <ul class="ulbdnone bts-1">
    <li style="border-bottom:0px;">
        <strong>Pick Up Category : </strong><?php echo ucwords($booking['categorytype']);?>
        <ul class="ulbdnone  clearfix" >             
        
          <?php if($booking['categorytype']=="address"): ?>
            <?php
               $pickshortaddress='';
               $pickshortaddress=$booking['address'];
               $pickshortaddress= str_replace(($booking['city'].','), "", $pickshortaddress);
               $pickshortaddress= str_replace(($booking['zipcode']), "",  $pickshortaddress);
               $pickshortaddress=str_ireplace("france","",$pickshortaddress);
               $pickshortaddress=rtrim($pickshortaddress,' ');
               $pickshortaddress=rtrim($pickshortaddress,',');
               $pickshortaddress = preg_replace('!\s+!', ' ', $pickshortaddress);
             ?>
         
        <li class="text-left"><strong>Complément d'adresse : </strong><?php echo $booking['otheraddress'];?></li>
        
         <li class="text-left"><strong>Addresse : </strong><?php echo $pickshortaddress;?></li>
         
        
         <?php elseif($booking['categorytype']=="package"): ?>
         <li class="text-left"><strong>Name : </strong><?php echo $booking['pickpackagename'];?></li>
         <li class="text-left"><strong>Addresse : </strong><?php echo $pickupaddress1;?></li>
         <?php elseif($booking['categorytype']=="airport"): ?>
         <li class="text-left"><strong>Name : </strong><?php echo $booking['pickairportname'];?></li>
          <li class="text-left"><strong>Flight Number : </strong><?php echo $booking['flightnumber'];?>
           <strong style="margin-left: 5px;">Arrival Time : </strong><?php echo $booking['flightarrivaltime'];?></li>
         <li class="text-left"><strong>Addresse : </strong><?php echo $pickupaddress1;?></li>
         <?php elseif($booking['categorytype']=="train"): ?>
         <li class="text-left"><strong>Name : </strong><?php echo $booking['picktrainname'];?></li>
         <li class="text-left"><strong>Train Number : </strong><?php echo $booking['trainnumber'];?>
         <strong style="margin-left: 5px;">Arrival Time : </strong><?php echo $booking['trainarrivalnumber'];?></li>
         <li class="text-left"><strong>Addresse : </strong><?php echo $pickupaddress1;?></li>
          <?php elseif($booking['categorytype']=="hotel"): ?>
         <li class="text-left"><strong>Name : </strong><?php echo $booking['pickhotelname'];?></li>
         <li class="text-left"><strong>Addresse : </strong><?php echo $pickupaddress1;?></li>
          <?php elseif($booking['categorytype']=="park"): ?>
         <li class="text-left"><strong>Name : </strong><?php echo $booking['pickparkname'];?></li>
         <li class="text-left"><strong>Addresse : </strong><?php echo $pickupaddress1;?></li>
        <?php endif; ?>
          <li>
          <ul class="ulbdnone clearfix" >
         <li class="text-left" style="float:left;border:0px;padding:0px;"><strong>Zip Code : </strong><?php echo $booking['zipcode'];?> </li>
         <li class="text-left" style="float:left;border:0px;padding:0px 10px;"><strong>City : </strong><?php echo $booking['city'];?></li>
         </ul>
       </li>
        
       </ul>
    </li>
    </ul>
      
    <?php
     $car_configuration_data= $this->bookings_model->booking_Configuration('vbs_car_configuration',['car_id' => $booking['carfield']]);
     ?>
   
    
        
   </div>
   <div class="col-md-7" style="padding:0px;">
    <ul class="ulbdnone" >
       <li class="text-left"><strong>Service : </strong>
      <?php
     $service= $this->bookings_model->getservicesbyid('vbs_u_service', $booking['service']);
     echo $service->service_name;
       ?>
      </li>
    </ul>
  <ul  class="ulbdnone" >
    <li style="border-bottom:0px;">
        <strong >Drop Off Category : </strong><?php echo ucwords($booking['dropcategorytype']);?>
        <ul  class="ulbdnone clearfix">             
       
          <?php if($booking['dropcategorytype']=="address"): ?>
          <?php 
             $dropshortaddress='';
             $dropshortaddress=$booking['dropaddress'];
             $dropshortaddress= str_replace(($booking['dropcity'].','), "", $dropshortaddress);
             $dropshortaddress= str_replace(($booking['dropzipcode']), "",  $dropshortaddress);
              $dropshortaddress=str_ireplace("france","",$dropshortaddress);
               $dropshortaddress=rtrim($dropshortaddress,' ');
               $dropshortaddress=rtrim($dropshortaddress,',');
             $dropshortaddress = preg_replace('!\s+!', ' ', $dropshortaddress);


          ?>
                <li class="text-left"><strong>Complément d'adresse : </strong><?php echo $booking['dropotheraddress'];?></li>
          
         <li class="text-left"><strong>Addresse : </strong><?php echo $dropshortaddress;?></li>
       
         <?php elseif($booking['dropcategorytype']=="package"): ?>
         <li class="text-left"><strong>Name : </strong><?php echo $booking['droppackagename'];?></li>
         <li class="text-left"><strong>Addresse : </strong><?php echo $dropoffaddress1;?></li>
         <?php elseif($booking['dropcategorytype']=="airport"): ?>
         <li class="text-left"><strong>Name : </strong><?php echo $booking['dropairportname'];?></li>
         <li class="text-left"><strong>Flight Number : </strong><?php echo $booking['dropflightnumber'];?>
         <strong style="margin-left: 5px;">Arrival Time : </strong><?php echo $booking['dropflightarrivaltime'];?></li>
         <li class="text-left"><strong>Addresse : </strong><?php echo $dropoffaddress1;?></li>
         <?php elseif($booking['dropcategorytype']=="train"): ?>
         <li class="text-left"><strong>Name : </strong><?php echo $booking['droptrainname'];?></li>
           <li class="text-left"><strong>Train Number : </strong><?php echo $booking['droptrainnumber'];?>
           <strong style="margin-left: 5px;">Arrival Time : </strong><?php echo $booking['droptrainarrivalnumber'];?></li>
         <li class="text-left"><strong>Addresse : </strong><?php echo $dropoffaddress1;?></li>
          <?php elseif($booking['dropcategorytype']=="hotel"): ?>
         <li class="text-left"><strong>Name : </strong><?php echo $booking['drophotelname'];?></li>
         <li class="text-left"><strong>Addresse : </strong><?php echo $dropoffaddress1;?></li>
          <?php elseif($booking['dropcategorytype']=="park"): ?>
         <li class="text-left"><strong>Name : </strong><?php echo $booking['dropparkname'];?></li>
         <li class="text-left"><strong>Addresse : </strong><?php echo $dropoffaddress1;?></li>
        <?php endif; ?>
        <li>
          <ul  class="ulbdnone clearfix" >
        <li class="text-left" style="float:left;border:0px;padding:0px;"><strong>Zip Code : </strong><?php echo $booking['dropzipcode'];?></li>
        <li class="text-left" style="float:left;border:0px;padding:0px 10px;"><strong>City : </strong><?php echo $booking['dropcity'];?></li>
         </ul>
       </li>
       </ul>
    </li>
    </ul>
           
   </div>
 </div>

   <?php if(count($stopsdata)>0): ?>
  <div class="col-md-12" style="padding: 0px;">
   
      <ul class="ulbdnone bts-1">             
         <li class="text-left" style="border-bottom:0px;"><strong>Stops : </strong>
          <?php foreach ($stopsdata as $stop): ?>
          <ul class="ulbdnone clearfix">
             <li class="text-left" style="width:100%;"><span style="display: inline-block;width:50%;"><strong>Addresse : </strong><?php echo  $stop['stopaddress'];?></span><span style="display: inline-block;width:50%;"><strong>Waiting Time : </strong><?php 
             $waitarray = explode(":", $stop['stopwaittime']);
             if($waitarray[0] > 1 && $waitarray[1] > 1){
               echo $waitarray[0]." hours ".$waitarray[1]." minutes";
             }
             elseif($waitarray[0] < 2 && $waitarray[1] < 2){
               echo $waitarray[0]." hour ".$waitarray[1]." minute";
             }
             elseif($waitarray[0] > 1 && $waitarray[1] < 2){
               echo $waitarray[0]." hours ".$waitarray[1]." minute";
             }
             elseif($waitarray[0] < 2 && $waitarray[1] > 1){
               echo $waitarray[0]." hour ".$waitarray[1]." minutes";
             }
            
              ?></span></li>
          </ul>
        <?php endforeach; ?>
      
         </li>
       </ul> 
  
 </div>   
  <?php endif; ?>

     <div class="col-md-12" style="padding: 0px;border-bottom:1px solid #ececec;">
      <div class="col-md-5" style="padding:0px;">
    <?php if($booking['regularcheck']!=1):?>
      
        <ul class="ulbdnone clearfix" >             
        <li class="text-left" style="float: left;padding:0px !important;border:0px;"><strong >PickUp Date : </strong><?php echo  $booking['pickupdate'];?></li>
        <li class="text-left"  style="float: left;padding: 0px !important;margin-left: 2px;border:0px;"><strong>PickUp Time : </strong><?php echo  $booking['pickuptime'];?></li>
       </ul> 
    
     <?php endif;?>
      </div>
              <div class="col-md-7" style="padding:0px;">
                    <div  style="float:left; margin-right:5px;">
                    
                        <span><strong>Reccurent : </strong></span><?php echo ($booking['regularcheck']==1)?"Yes":'No';  ?>,
                    
                    </div>

                    <div style="float:left; margin-right:5px;">
                       
                        <span><strong>Return : </strong></span> <?php echo ($booking['returncheck']==1)?"Yes":'No';  ?>,
                    
                    </div>
                     <div style="float:left; margin-right:5px;">
                     
                        <span><strong>Wheelchair : </strong> </span> <?php echo ($booking['wheelchaircheck']==1)?"Yes":'No';  ?>
                     
                    </div>
              </div>
        
   </div>


   <?php if($booking['returncheck']==1 && $booking['regularcheck']!=1 ):?>
     <div class="col-md-12" style="padding:0px;">
     <div class="col-md-5" style="padding:0px;">  

        <ul class="ulbdnone clearfix datetimequotediv">             
            <li class="text-left"  ><strong>Waiting Time : </strong>
              <?php
                $waitarray=explode(":", $booking['waitingtime']);
                if($waitarray[0] > 1 && $waitarray[1] > 1){
                   echo $waitarray[0]." hours and ".$waitarray[1]." minutes ";
                 }
                 elseif($waitarray[0] > 1 && $waitarray[1] <= 1){
                   echo $waitarray[0]." hours and ".$waitarray[1]." minute ";
                 }
                  elseif($waitarray[0] <= 1 && $waitarray[1] > 1){
                   echo $waitarray[0]." hour and ".$waitarray[1]." minutes ";
                 }
                 else{
                  echo $waitarray[0]." hour and ".$waitarray[1]." minute ";
                 }
               
                        ?></li>
            
   
       </ul>
     </div>
     <div class="col-md-7" style="padding:0px;">
        <ul class="ulbdnone clearfix datetimequotediv">             

            <li class="text-left"><strong>Return Date : </strong><?php echo  $booking['returndate'];?></li>
            <li class="text-left"><strong>Return Time : </strong><?php echo  $booking['returntime'];?></li>
   
       </ul>
     </div>
    </div>
  <?php endif; ?>
     <?php if($booking['regularcheck']==1):?>
             <div class="col-md-12 bbts-1">
              <div class="col-md-5" style="padding:0px;">
                  <span><strong>Start Date : </strong><?php echo  $booking['startdate'];?>, </span>
                    <span><strong>Start Time : </strong><?php echo  $booking['starttime'];?> </span>
               </div>     
                <div class="col-md-7" style="padding:0px;">
                    <span><strong>End Date : </strong><?php echo  $booking['enddate'];?>, </span>
                  <span><strong>End Time : </strong><?php echo  $booking['endtime'];?></span>
                </div>
                </div>


                    <div class="col-md-12" style="padding: 0px;">
      <table cellpadding="0" cellspacing="0" 
      style="width:100%;" align="center" id="timesspantable">
        <tr  class="quotepicktimecol fw-900">
          <td>&nbsp;</td>
        
         
                          
           
              <td>Monday</td>
           
              <td>Tuesday</td>
          
               <td>Wednesday</td>
           
       
              <td>Thursday</td>
        
         
               <td>Friday</td>
       
        
              <td>Saturday</td>
          
               <td>Sunday</td>
               
         
        </tr>
        <tr class="quotepicktimecol">
          <td style="font-size: 11px;font-weight: 900;">PickUp Time</td>
           <?php foreach($bookingregular as $regular): ?>
            <?php if(!empty($regular['time']) || $regular['time']!=''): ?>
              <td>
               <?php echo $regular['time'];?>
               </td>
             <?php else: ?>  
                 
                    <td>No Ride</td>
                 
            <?php endif; ?>
           <?php endforeach; ?>
        </tr>
          <?php if($booking['returncheck']==1):?>
        <tr class="quotepicktimecol">
          <td style="font-size: 11px;font-weight: 900;">Return Time</td>
           <?php foreach($bookingreturn as $return): ?>
             <?php if( !empty($return['time']) || $return['time']!=''): ?>
               <td>
              <?php echo $return['time'];?>
             </td>
                <?php else: ?>  
                  <td>No Ride</td>
             <?php endif; ?>
          <?php endforeach; ?>
        </tr>
       <?php endif; ?>
      </table>
    </div>
     <?php endif; ?>
        <div class="col-md-12" style="padding:0px;">
     <ul class="ulbdnone"  id="carquotestab">
    <li style="border-bottom:0px;">
        <strong>Car Category : </strong><?php echo $car_name  ?>
        <ul class="ulbdnone"  >   
          
          <?php
          $configno=0;
           foreach ($car_configuration_data as $config): 
            $configno++;
            ?>     
         <li class="text-left">
          <span><strong>Configuration <?= $configno ?> :  </strong></span>
          <span><?php echo $config->passengers;?><strong> Passengers,</strong></span>
          <span><?php echo $config->luggage;?><strong> Luggage,</strong></span>
          <span><?php echo $config->wheelchairs;?><strong> Wheelchairs,</strong></span>
          <span><?php echo $config->baby;?><strong> Babys,</strong></span>
          <span><?php echo $config->animals;?><strong> Animals</strong></span>
        </li>
        
           <?php endforeach; ?> 
       
       </ul>
    </li>
    </ul>
   </div> 

    <?php if(!empty($booking['booking_comment'])): ?>
   <div class="col-md-12" style="padding:0px;">
   <div class="form-group">
        <label style="font-weight: 900;margin-bottom: 10px;">Comment :</label>
   <textarea class="form-control" rows="4" name="booking_comment" id="commentbox" disabled style="background-color: #fff;resize: none;cursor: default;" ><?php echo $booking['booking_comment'];  ?></textarea>

     </div>
   </div>
   <?php endif; ?> 