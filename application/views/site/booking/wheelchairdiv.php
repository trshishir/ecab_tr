   <div class="non-wheelchair"  style="margin-top: 10px;margin-bottom:30px !important;">
                <input type="hidden" value="0" name="carstypes" id="carstypes">
                   <?php if($car_data): ?>
                    <!--none wheelchairs cars-->
                  <div class="row"id="nonwheelchairdiv">
                      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
                        <div class="row">
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-bottom: -4px;">                   
                              <?php
                              $item=0;
                               foreach ($car_data as $key => $car):
                                if($item<3):
                                ?> 
                                  
                                  
                                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" style="padding:5px;">
                                      <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="active car_labe_ds text-center" >
                                              <input type="radio" name="carfield" class="car_tpmr_veh" <?= ($item==0 && $booking['carfield']=='')?"checked":""; ?>  <?= ($booking['carfield']==$car->id)?"checked":""; ?> value="<?= $car->id ?>">
                                              <span class="smpl_txt" style="color:#0d4f78 !important;"><?= $car->car_cat_name ?><div><img src="<?php echo base_url($car->caricon);?>"></div></span>
                                            </div>
                                        </div>
                                      </div>
                                  <div class="row" style="margin-top: 1px;">
                                
                                 <?php
                                     $car_config=$this->bookings_model->getcarsconfigdata($car->id);
                                    foreach($car_config as $key => $config): 
                                 ?>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: -7px;">
                                            <div class="text-center btn-configs btn-config-ds">
                                                <div>
                                                    <i class="fa fa-male"></i><i class="fa fa-female"></i>
                                                    <input  class=" text-center body-bg" value="<?= $config->passengers ?>" style="width: 25px !important; border: none !important;" disabled>
                                                </div>
                                                <div>
                                                    <i class="fa fa-suitcase"></i>
                                                    <input class=" text-center body-bg" value="<?= $config->luggage ?>" style="width: 25px !important; border: none !important;" disabled>
                                                </div>
                                                <div class="wheelchair_config">
                                                    <i class="fa fa-wheelchair"></i>
                                                    <input class=" text-center body-bg" value="<?= $config->wheelchairs ?>" style="width: 25px !important; border: none !important;" disabled>
                                                </div>
                                                <div>
                                                   <i class="fa fa-child"></i>
                                                    <input  class=" text-center body-bg" value="<?= $config->baby ?>" style="width: 25px !important; border: none !important;" disabled>
                                                </div>
                                                <div>
                                                    <i class="fa fa-paw"></i>
                                                    <input class=" text-center body-bg" value="<?= $config->animals ?>" style="width: 25px !important; border: none !important;" disabled>
                                                </div>
                                              </div>
                                        </div>
                                 <?php endforeach;  ?>
                                </div>
                              </div>
                                 <?php 
                                 $item++;
                               endif;
                                  endforeach; 
                                  ?>   
                        </div>         
                      </div>
                    </div>     
                   </div>
                   
                    <!-- end seciton none wheelchairs cars -->
                   <?php endif; ?> 
              <?php if($wheelcar_data): ?>
                    <!--wheeelchair div -->
                 <div class="row" id="wheelchairdiv"style="display: none;">
                      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
                        <div class="row">
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: -4px;">                   
                              <?php
                                $item=0;
                               foreach ($wheelcar_data as $key => $car):
                                if($item<3):
                                ?> 
                                  
                                  
                                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" style="padding:5px;">
                                      <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="active car_labe_ds text-center" >
                                              <input type="radio" name="wheelcarfield" class="car_tpmr_veh" <?= ($item==0 && $booking['carfield']=='')?"checked":""; ?> 
                                              <?= ($booking['carfield']==$car->id)?"checked":""; ?>
                                              value="<?= $car->id ?>">
                                              <span class="smpl_txt" style="color:#0d4f78 !important;"><?= $car->car_cat_name ?><div><img src="<?php echo base_url($car->caricon);?>"></div></span>
                                            </div>
                                        </div>
                                      </div>
                                      <div class="row" style="margin-top: 1px;">
                                  <?php
                                     $car_config=$this->bookings_model->getcarsconfigdata($car->id);
                                     foreach($car_config as $key => $config): 
                                 ?>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: -7px;">
                                            <div class="text-center btn-configs btn-config-ds">
                                                <div>
                                                    <i class="fa fa-male"></i><i class="fa fa-female"></i>
                                                    <input  class=" text-center body-bg" value="<?= $config->passengers ?>" style="width: 25px !important; border: none !important;" disabled>
                                                </div>
                                                <div>
                                                    <i class="fa fa-suitcase"></i>
                                                    <input class=" text-center body-bg" value="<?= $config->luggage ?>" style="width: 25px !important; border: none !important;" disabled>
                                                </div>
                                                <div class="wheelchair_config">
                                                    <i class="fa fa-wheelchair"></i>
                                                    <input class=" text-center body-bg" value="<?= $config->wheelchairs ?>" style="width: 25px !important; border: none !important;" disabled>
                                                </div>
                                                <div>
                                                   <i class="fa fa-child"></i>
                                                    <input  class=" text-center body-bg" value="<?= $config->baby ?>" style="width: 25px !important; border: none !important;" disabled>
                                                </div>
                                                <div>
                                                    <i class="fa fa-paw"></i>
                                                    <input class=" text-center body-bg" value="<?= $config->animals ?>" style="width: 25px !important; border: none !important;" disabled>
                                                </div>
                                              </div>
                                        </div>
                               <?php endforeach;  ?>
                                </div>
                              </div>
                                 <?php 
                                 $item++;
                                  endif;
                                  endforeach; 
                                  ?>   
                            </div>         
                          </div>
                        </div>     
                      </div>
                   
                  
                      <!--end wheelchiar div --> 
                  <?php endif; ?>
                   

              </div>
