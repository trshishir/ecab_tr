</header>
<div class="slider_wrapper " id="sliderWrapper"> <!--slider_wrapper_1-->
    <div class="container">
        <div class="row">
            <ul class="homepageSlider">
                <?php foreach($slider as $item):?>

                    <li>
                        <?php if(!empty($item->left_icon)):?>
                            <div class="left_icon"><img src="<?php echo base_url(); ?>uploads/cms/slider/<?=$item->left_icon;?>"></div>
                        <?php endif;?>

                        <?php if(!empty($item->right_icon)):?>
                            <div class="right_icon pull-right"><img src="<?php echo base_url(); ?>uploads/cms/slider/<?=$item->right_icon;?>"></div>
                        <?php endif;?>

                        <img src="<?php echo base_url(); ?>uploads/cms/slider/<?=$item->img;?>" title="Visite Guidee Paris" alt="Visite Guidee Paris" style="height:360px" />
                        <div class="slider_footer_text col-md-12">
                            <span><?php echo $item->service_name;?></span>
                            <p><?php echo $item->service_short_description;?></p>
                        </div>

                        <div class="divCaption">
                            <div class="ncaption one white"><?=$item->service_name;?></div>
                            <div class="ncaption two white" style="font-size: 17px;"><?=$item->category_name;?></div>
                            <div class="ncaption three">
                                <?php if($item->category_link != "" && $item->service_link != ""):?>
                                    <a href="<?php echo base_url("services/{$item->category_link}/{$item->service_link}{$item->link}"); ?>" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a>
                                <?php endif;?>
                                <a href="<?php echo site_url("booking"); ?>" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>

                            </div>
                        </div>
                    </li>
                <?php endforeach;?>


                <!--
 <li style="width:204px;"><a href="<?php echo site_url(); ?>services/<?=$value->id;?>/<?=$value->title;?><?=$value->link;?>"><?=$value->title;?></a></li>

    			<li>
                    <img src="<?php echo base_url(); ?>assets/system_design/images/new_footer_header_images/slide1.jpg" title="Visite Guidee Paris" alt="Visite Guidee Paris" style="height:360px" />
                    <div class="divCaption">
                        <div class="ncaption one white">Handi Prive</div>
                        <div class="ncaption two white">TRANSPORT PMR A LA DEMANDE</div>
                        <div class="ncaption three">
                            <a href="<?php echo site_url(); ?>handi-prive.php" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a>
                            
                          <a href="#" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>
                        </div>
                    </div>
                </li>
    			<li>
                    <img src="<?php echo base_url(); ?>assets/system_design/images/new_footer_header_images/slide2.jpg" title="Gare De Lyon" alt="Gare De Lyon" style="height:360px" />
                    <div class="divCaption">
                        <div class="ncaption one white">Handi Pro</div>
                        <div class="ncaption two white">TRANSPORT PMR REGULIER</div>
                        <div class="ncaption three">
                            <a href="<?php echo site_url(); ?>handi-pro.php" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a>
                            <a href="#" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>
                        </div>
                    </div>
                </li>
    			<li>
                    <img src="<?php echo base_url(); ?>assets/system_design/images/new_footer_header_images/slide3.jpg" title="Gare Saint Lazare" alt="Gare Saint Lazare" style="height:360px" />
                    <div class="divCaption">
                        <div class="ncaption one white">Handi Medical</div>
                        <div class="ncaption two white">TRANSPORT PMR A LA DEMANDE </div>
                        <div class="ncaption three">
                            <a href="<?php echo site_url(); ?>handi-medical.php" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a>
                             <a href="#" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="<?php echo base_url(); ?>assets/system_design/images/new_footer_header_images/slide4.jpg" title="Aeroport D'Orly" alt="Aeroport D'Orly" style="height:360px" />
                    <div class="divCaption">
                        <div class="ncaption one white">Handi Business</div>
                        <div class="ncaption two white">TRANSPORT PMR REGULIER</div>
                        <div class="ncaption three">
                            <a href="<?php echo site_url(); ?>handi-business.php" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a>
                              <a href="#" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>
                            <br>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="<?php echo base_url(); ?>assets/system_design/images/new_footer_header_images/slide5.jpg" title="Aeroport De Roissy CDG" alt="Aeroport De Roissy CDG" style="height:360px" />
                    <div class="divCaption">
                        <div class="ncaption one white">Handi Senior</div>
                        <div class="ncaption two white">TRANSPORT PMR A LA DEMANDE</div>
                        <div class="ncaption three">
                            <a href="<?php echo site_url(); ?>handi-senior.php" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a>
                           <a href="#" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="<?php echo base_url(); ?>assets/system_design/images/new_footer_header_images/slide6.jpg" title="Aeroport Beauvais" alt="Aeroport Beauvais" style="height:360px" />
                    <div class="divCaption">
                        <div class="ncaption one white">Handi Event</div>
                        <div class="ncaption two white">TRANSPORT PMR A LA DEMANDE</div>
                        <div class="ncaption three">
                            <a href="<?php echo site_url(); ?>handi-event.php" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a>
                             <a href="#" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>
                        </div>
                    </div>
                </li>
              <li>
                    <img src="<?php echo base_url(); ?>assets/system_design/images/new_footer_header_images/slide7.jpg" title="Aeroport Beauvais" alt="Aeroport Beauvais" style="height:360px" />
                    <div class="divCaption">
                        <div class="ncaption one white">Handi Voyage</div>
                        <div class="ncaption two white">TRANSPORT PMR A LA DEMANDE</div>
                        <div class="ncaption three">
                            <a href="<?php echo site_url(); ?>handi-voyage.php" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a>
                            <a href="#" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>
                        </div>
                    </div>
                </li>
            -->

                <!-- 12-4-16
                <li>
                    <img src="<?php // echo base_url(); ?>assets/system_design/images/slider/chateaux.jpg" title="Chateaux" />
                    <div class="ncaption one white">Châteaux de Versailles</span>
                    <div class="ncaption two white">Visites Guidees</span>
                    <div class="ncaption three"><a href="<?php // echo site_url(); ?>/services/11" class="button light_orange_button"><?php // echo $this->lang->line('learn_more'); ?></a></span>
                    </li>-->
                <!-- 12-4-16
                <li>
                    <img src="<?php // echo base_url(); ?>assets/system_design/images/slider/fontainebleau.jpg" title="Fontainebleau" />
                    <div class="ncaption one white">Fontainebleau Tour</span>
                    <div class="ncaption two white">Visites Guidees</span>
                    <div class="ncaption three"><a href="<?php // echo site_url(); ?>/services/12" class="button light_orange_button"><?php // echo $this->lang->line('learn_more'); ?></a></span>
                    </li>-->
                <!-- 12-4-16
                <li>
                    <img src="<?php // echo base_url(); ?>assets/system_design/images/slider/chantilly.jpg" title="Chantilly" />
                    <div class="ncaption one white">Chantilly Tour</span>
                    <div class="ncaption two white">Visites Guidees</span>
                    <div class="ncaption three"><a href="<?php // echo site_url(); ?>/services/22" class="button light_orange_button"><?php // echo $this->lang->line('learn_more'); ?></a></span>
                    </li>-->
                <!-- 12-4-16<li>
                    <img src="<?php // echo base_url(); ?>assets/system_design/images/slider/honfleur-deauville.jpg" title="Gare De Lyon" />
                    <div class="ncaption one white">Honfleur et Deauville</span>
                    <div class="ncaption two white">Circuits Touristiques</span>
                    <div class="ncaption three"><a href="<?php // echo site_url(); ?>/services/13" class="button light_orange_button"><?php // echo $this->lang->line('learn_more'); ?></a></span>
                    </li>-->
                <!-- 12-4-16
                <li>
                    <img src="<?php // echo base_url(); ?>assets/system_design/images/slider/plages-du-debarquement.jpg" title="Plages des débarquements" />
                    <div class="ncaption one white">Plages des débarquements</span>
                    <div class="ncaption two white">Circuits Touristiques</span>
                    <div class="ncaption three"> <a href="<?php // echo site_url(); ?>/services/14" class="button light_orange_button"><?php // echo $this->lang->line('learn_more'); ?></a></span>
                    </li>-->
                <!-- 12-4-16
                <li>
                    <img src="<?php // echo base_url(); ?>assets/system_design/images/slider/mont-saint-michel.jpg" title="Saint Malo & Mont Saint Michel" />
                    <div class="ncaption one white">Saint Malo & Mont Saint Michel</span>
                    <div class="ncaption two white">Circuits Touristiques</span>
                    <div class="ncaption three"><a href="<?php // echo site_url(); ?>/services/15" class="button light_orange_button"><?php // echo $this->lang->line('learn_more'); ?></a></span>
                    </li>-->
                <!-- <li>
                    <img src="<?php // echo base_url(); ?>assets/system_design/images/slider/disneyland-paris.jpg" title="Disneyland Paris" />
                    <div class="ncaption one white">Disneyland Paris Tour</span>
                    <div class="ncaption two white">Visites Guidees</span>
                </li>
                
                
                <div class="ncaption one white">Honfleur & Deauville</span>
                        <div class="ncaption two white">Circuits Touristiques</span>
                <li>
                        <img src="<?php // echo base_url(); ?>assets/system_design/images/slider/parc-asterix.jpg" title="Parc Asterix" />
                        <div class="ncaption one white">Parc Asterix</span>
                        <div class="ncaption two white">Services at Parc Asterix</span>
                </li>
                
                <li><img src="<?php // echo base_url(); ?>assets/system_design/images/slider/parc-asterix-1.jpg" title="Parc Asterix" />
                        <div class="ncaption one white">Parc Asterix</span>
                        <div class="ncaption two white">Services at Parc Asterix</span>
                        </li>-->
            </ul>
            <!-- FORM WRAPPER -->
        </div>
    </div>
</div>

<!--<section id="quality_section">
            <div class="container">
                <div class="row">
                    <div class="quality_container"> 
                    <div class="col-md-4 title_bar">
                        <h2><?php // echo $this->lang->line("our_commitments") ?></h2>
                    </div>
                    <div class="col-md-2">
                        <h4><i class="fa fa-clock-o"></i> <?php // echo $this->lang->line("punctuality") ?></h4>
                    </div>
                    <div class="col-md-2">
                        <h4><i class="fa fa-lock"></i> <?php // echo $this->lang->line("security") ?></h4>
                    </div>
                    <div class="col-md-2">
                        <h4><i class="fa fa-thumbs-up"></i> <?php // echo $this->lang->line("quality") ?></h4>
                    </div>
                    <div class="col-md-2">
                        <h4><i class="fa fa-eur"></i> <?php // echo $this->lang->line("economy") ?></h4>
                    </div> 
                </div>
            </div>
        </div>
    </section> -->
<section id="quality_section">
    <div class="container">
        <div class="row">
            <div class="quality_container">
                <div class="col-md-3 title_bar">
                    <h2><?php echo $this->lang->line("our_commitments") ?></h2>
                </div>
                <div class="col-md-3">
                    <div class="quality_bar"><h4><img src="<?php echo base_url(); ?>assets/system_design/images/3-easy-steps.png" /> <span><?php echo 'Easy 3 Steps Booking' ?></span></h4></div>
                </div>
                <div class="col-md-3">
                    <div class="quality_bar"><h4><img src="<?php echo base_url(); ?>assets/system_design/images/qualified-drivers.png" /> <span><?php echo 'Qualified Drivers'; ?></span></h4></div>
                </div>
                <div class="col-md-3">
                    <div class="quality_bar"><h4><img src="<?php echo base_url(); ?>assets/system_design/images/affordable-prices.png" /> <span><?php echo 'Affordable Prices' ?></span></h4></div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="testimonial_section">
    <div class="container">
        <div class="row">
            <div class="testimonial_container">
                <div class="col-md-4">
                    <h2><?php echo $this->lang->line("testimonials_of_our_clients") ?></h2>
                </div>
                <div class="col-md-8">
                    <div class="testimonial-slider-wrap">
                        <?php if (count($testimonials) > 0) { ?>
                            <ul class="testy-bxslider">
                                <?php
                                // echo "<PRE>";var_dump($testimonials);echo "</PRE>";
                                foreach ($testimonials as $row):

                                    ?>
                                    <li>
                                        <div class="testy-img">
                                            <img height="60" width="60" alt="Profile" title="Profile" src="<?php echo base_url(); ?>/uploads/testimonials_images/<?php
                                            if ($row->gender == "male")
                                                echo '3_testimonial-image1.png';

                                            else  if ($row->gender == "female")
                                            {
                                                echo '2_testimonial-image2.png';

                                            }
                                            else
                                                echo "no-preview.jpg";
                                            ?>" class="img-responsive">
                                        </div>
                                        <div class="testy-content">
                                            <div class="testy-text"><?php echo substr($row->description, 0, 150); ?></div>
                                            <h3 class="testimonial_name"><?php echo $row->name; ?></h3>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php } else { ?>
                            <div class="col-md-9">
                                <div class="left-side-cont">
                                    <p align="center" style="margin:140px 0px">Coming Soon...</p>
                                </div>
                            </div>
                        <?php } ?>
                        <!--<div class="testy-more"><a href="<?php // echo site_url(); ?>testimonials.php" class="button blue_button"><?php // echo $this->lang->line('see_more'); ?></a></div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="package_section">
    <div class="container content-bg">


        <div class="row">

            <div style="margin-top:195px;margin-left: 15px;">

                <?php
                foreach($services as $item):

                ?>



                <div class="col-md-4 col-sm-12 col-xs-12" style="margin-top:0px;">
                    <div class="ribbon">
                        <?php if($item->rubens_new==1):?>

                            <div class="wrap">
                                <div class="ribbon6"><span class="promo-service">NEW</span></div>
                            </div>

                        <?php endif;?>
                        <?php if(($item->rubens_new!=1) && ($item->is_price_start_from==1)):?>
                            <div class="corner-promo-ribbon top-right-corner">

                                <span class="price"><?=$item->start_from_price;?> <i class="fa fa-<?php echo ($company_data['default_front_currency']=='usd')?'usd':'eur' ;?>"></i></span>
                                <span class="startfrom">Start from</span>
                            </div>

                        <?php endif;?>

                        <?php if($item->rubens_promo==1):?>
                            <!-- <div class="corner-promo-ribbon top-left-promo"><span class="promo-service">Promotion</span></div> -->
                            <div class="wrap1">
                                <div class="ribbon5"><span class="promo-service">Promotion</span></div>
                            </div>
                        <?php endif;?>


                        <?php if(($item->rubens_promo==1) || ($item->rubens_new==1)):?>
                        <div class="plans" style="width:98%; margin-top:3px;margin-bottom:3px; margin-left:3px;">


                            <?php else:?>
                            <div class="plans" style="width:100%; margin-top:0px;">

                                <?php endif;?>


                                <!-- <div class="plans"> -->

                                <div class="plans_head">

                                    <h3 style="margin-left:-11px;"><?=$item->title;?></h3>
                                    <div class="plans_img"><img src="<?php echo base_url();?>uploads/cms/<?php echo $item->image?>" alt="FIAT SCUDO TPMR - Fiat ScudoTpmr" title="FIAT SCUDO TPMR - Fiat ScudoTpmr"></div>

                                    <!--<div class="col-md-12 plan_title" id="plan_title_row">
                                        <p></p>
                                    </div>-->
                                    <div class="description">
                                        <?php

                                        $text= $item->description;
                                        $text= strip_tags($text);

                                        echo implode(' ', array_slice(explode(' ', $text), 0, 23)).' ...';


                                        ?>                                        </div>
                                    <div class="row"> <!--button-row-->

                                        <div class="learn_button">

                                            <!-- <a href="<?php echo base_url();?>handi-pro.php" class="button light_green_button">Learn More</a></div> -->
                                            <a href="<?php echo base_url("services/{$item->category_link}/{$item->link_url}{$item->link}"); ?>" class="button light_green_button">Know More</a></div>


                                        <div class="orderbtn"><a href="<?php echo site_url(); ?>booking.php" class="button light_orange_button">Book Now</a></div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>


                    <?php endforeach;?>


                </div>
            </div>






















            <!-- <div class="container-fluid body-border"> -->
            <?php
            /*
                                if (count($packages) > 0) : ?>
                                    <?php
                                    $sno = 1;
                                    foreach ($packages as $r) {
                                        if (($sno % 3) == 1 || $sno == 1) :
                                            ?>          <div class="row">
                                            <?php   endif;
                                            ?>
                                            <div class="col-md-4 col-sm-12 col-xs-12" style="margin-top:15px;">
                                                <?php if ($sno >= 1 && $sno <= 3) :?>
                                                <?php endif;?>
                                                <div class="plans">
                                                    <div class="plans_head">
                                                        <!--<div class="corner-left-ribbon top-left" ><span class="promo-txt">Promotion</span></div>-->
                                                        <h3 style="margin-left:-11px;"><?php echo $r->name; ?></h3>
                                                        <div class="plans_img"><img src="<?php echo base_url(); ?>uploads/service_images/<?php if ($r->image != "") echo $r->service_image; else echo "default-car.jpg"; ?>" alt="<?php echo $r->vehicle_name . " - " . $r->model; ?>" title="<?php echo $r->vehicle_name . " - " . $r->model; ?>"></div>
                                                        <!--<div class="corner-promo-ribbon top-right-promo"><span class="promo-service">Promotion</span></div>-->

                                                    <!--<div class="col-md-12 plan_title" id="plan_title_row">
                                                        <p><?php //echo $this->lang->line('package_details_caps'); ?></p>
                                                    </div>-->
                                                    <div class="description">
                                                        <?php   if ($this->lang->lang() == 'en') {
                                                                //  echo substr($r->terms_conditions, 0, 150);
                                                            echo $r->meta_description_en;
                                                        } else if ($this->lang->lang() == 'fr') {
                                                            echo $r->meta_description_fr;
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="row"> <!--button-row-->
                                                        <?php if ($r->service_id != NULL): ?>

                                                           <div class="learn_button">

                                                             <?php
                                                             $split = @explode(" ", $r->name);
                                                             $link = "services/" . $r->id;
                                                             if(count($split) == 2)
                                                                $link = "handi-" . strtolower($split[1]) . '.php';
                                                            ?>
                                                            <a href="<?php echo site_url() . $link; ?>" class="button light_green_button"><?php echo $this->lang->line('learn_more'); ?></a></div>

                                                        <?php endif; ?>
                                                        <div class="orderbtn"><a href="#" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a></div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <?php $sno++;
                                        if (($sno % 3) == 1) :
                                            ?>          </div>
                                        <?php   endif;
                                    } ?>
                                <?php endif;


            */
            ?>
            <!--<div class="span4">
                    <div class="plans">
                        <div class="plans_head"><h3>Transport AEROPORT</h3></div>
                       
                        <div class="corner-promo-ribbon top-right-promo"><span class="promo-service">Promotion</span></div>
                        <div class="col-md-12" id="plan_title_row">
                            <div class="col-md-6"><div class="plans_img"><img src="<?php echo base_url(); ?>assets/system_design/images/services/transfert-aeroport.jpg"></div></div>
                            <div class="col-md-6 plan_title">
                                    <p>VOUS CHERCHEZ UN TRANSPORT POUR VOUS RENDRE Ã L'AEROPORT ? ...</p>
                            </div>
                        </div>
                        <div class="description">Trans express vient vous chercher Ã  votre domicile ou Ã  la descente de vot ...</div>
                        <div>
                            <div class="learn_button"><a href="service.php?id=7" class="button blue_button">learn more</a></div>     
                            <div class="orderbtn"><a href="booking.php?Sid=7&amp;Cid=3" class="button green_button">Book now</a></div>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="plans">
                        <div class="plans_head"><h3>Transport Gare</h3></div>
                        <div class="corner-left-ribbon top-left" ><span class="head-txt1">Vente</span><span class="head-txt2">FLASH</span></div>
                        <div class="corner-right-ribbon top-right"><span class="price">$49</span><span class="startfrom">Start From</span></div>
                        <div class="corner-promo-ribbon top-right-promo"><span class="promo-service">Promotion</span></div>
                        <div class="col-md-12" id="plan_title_row">
                            <div class="col-md-6"><div class="plans_img"><img src="<?php echo base_url(); ?>assets/system_design/images/services/transfert-gare.jpg"></div></div>
                            <div class="col-md-6 plan_title">
                                <p>VOUS CHERCHEZ UN TRANSPORT POUR VOUS RENDRE Ã LA GARE ? OU ...</p>
                            </div>
                        </div>
                        <div class="description">Trans express ne vous fera jamais manquer votre train! Nous vous transportons rapidement dans toutes...</div>
                        <div>
                            <div class="learn_button"><a href="service.php?id=7" class="button blue_button">learn more</a></div>    
                            <div class="orderbtn"><a href="booking.php?Sid=7&amp;Cid=3" class="button green_button">Book now</a></div>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="plans">
                        <div class="plans_head"><h3>Transport Hotel</h3></div>
                        <div class="corner-left-ribbon top-left" ><span class="head-txt1">Vente</span><span class="head-txt2">FLASH</span></div>
                        <div class="corner-right-ribbon top-right"><span class="price">$49</span><span class="startfrom">Start From</span></div>
                        <div class="corner-promo-ribbon top-right-promo"><span class="promo-service">Promotion</span></div>
                        <div class="col-md-12" id="plan_title_row">
                            <div class="col-md-6"><div class="plans_img"><img src="<?php echo base_url(); ?>assets/system_design/images/services/transfert-hotel.jpg"></div></div>
                            <div class="col-md-6 plan_title">
                                <p>VOUS CHERCHEZ UN TRANSPORT POUR VOUS RENDRE Ã L'HOTEL ? OU ...</p>
                            </div>
                        </div>    
                        <div class="description">Trans express vous transporte jusqu'Ã  l'hÃ´tel parisien oÃ¹ vo ...</div>
                        <div>
                            <div class="learn_button"><a href="service.php?id=7" class="button blue_button">learn more</a></div>      
                            <div class="orderbtn"><a href="booking.php?Sid=7&amp;Cid=3" class="button green_button">Book now</a></div>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="plans">
                        <div class="plans_head"><h3>Transport Pro</h3></div>
                        <div class="corner-left-ribbon top-left" ><span class="head-txt1">Vente</span><span class="head-txt2">FLASH</span></div>
                        <div class="corner-right-ribbon top-right"><span class="price">$49</span><span class="startfrom">Start From</span></div>
                        <div class="corner-promo-ribbon top-right-promo"><span class="promo-service">Promotion</span></div>
                        <div class="col-md-12" id="plan_title_row">
                            <div class="col-md-6"><div class="plans_img"><img src="<?php echo base_url(); ?>assets/system_design/images/services/transfert-professionnel.jpg"></div></div>
                            <div class="col-md-6 plan_title">
                                <p>VOUS CHERCHEZ UN TRANSPORT POUR VOUS RENDRE Ã VOTRE TRAVAIL ...</p>
                            </div>
                        </div>    
                        <div class="description">C'est avec sÃ©rÃ©nitÃ© que vous pouvez faire appel Ã ...</div>
                        <div>
                            <div class="learn_button"><a href="service.php?id=7" class="button blue_button">learn more</a></div>      
                            <div class="orderbtn"><a href="booking.php?Sid=7&amp;Cid=3" class="button green_button">Book now</a></div>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="plans">
                        <div class="plans_head"><h3>Transport Institution</h3></div>
                        <div class="corner-left-ribbon top-left" ><span class="head-txt1">Vente</span><span class="head-txt2">FLASH</span></div>
                        <div class="corner-right-ribbon top-right"><span class="price">$49</span><span class="startfrom">Start From</span></div>
                        <div class="corner-promo-ribbon top-right-promo"><span class="promo-service">Promotion</span></div>
                        <div class="col-md-12" id="plan_title_row">
                            <div class="col-md-6"><div class="plans_img"><img src="<?php echo base_url(); ?>assets/system_design/images/services/transfert-institution.jpg"></div></div>
                            <div class="col-md-6 plan_title">
                                <p>VOUS CHERCHEZ UN TRANSPORT POUR LES MEMBRES DE VOTRE CENTER ...</p>
                            </div>
                        </div>    
                        <div class="description">EquipÃ© de vÃ©hicules qui permettent le transport de plusieurs personnes, ...</div>
                        <div>
                            <div class="learn_button"><a href="service.php?id=7" class="button blue_button">learn more</a></div>      
                            <div class="orderbtn"><a href="booking.php?Sid=7&amp;Cid=3" class="button green_button">Book now</a></div>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="plans">
                        <div class="plans_head"><h3>Transport Scolaire</h3></div>
                        <div class="corner-left-ribbon top-left" ><span class="head-txt1">Vente</span><span class="head-txt2">FLASH</span></div>
                        <div class="corner-right-ribbon top-right"><span class="price">$49</span><span class="startfrom">Start From</span></div>
                        <div class="corner-promo-ribbon top-right-promo"><span class="promo-service">Promotion</span></div>
                        <div class="col-md-12" id="plan_title_row">
                            <div class="col-md-6"><div class="plans_img"><img src="<?php echo base_url(); ?>assets/system_design/images/services/transfert-scolaire.jpg"></div></div>
                            <div class="col-md-6 plan_title">
                                <p>VOUS CHERCHEZ UN TRANSPORT POUR LES MEMBRES DE VOTRE CENTER ...</p>
                            </div>
                        </div>    
                        <div class="description">Trans express prend en charge vos enfants sur le trajet de l'Ã©cole. Ne vous inqui & Atild ...</div>
                        <div>
                            <div class="learn_button"><a href="service.php?id=7" class="button blue_button">learn more</a></div>      
                            <div class="orderbtn"><a href="booking.php?Sid=7&amp;Cid=3" class="button green_button">Book now</a></div>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="plans">
                        <div class="plans_head"><h3>Transport Private Tour</h3></div>
                        <div class="corner-left-ribbon top-left" ><span class="head-txt1">Vente</span><span class="head-txt2">FLASH</span></div>
                        <div class="corner-right-ribbon top-right"><span class="price">$49</span><span class="startfrom">Start From</span></div>
                        <div class="corner-promo-ribbon top-right-promo"><span class="promo-service">Promotion</span></div>
                        <div class="col-md-12" id="plan_title_row">
                            <div class="col-md-6"><div class="plans_img"><img src="<?php echo base_url(); ?>assets/system_design/images/services/transfert-private-tour-visite-guidee.jpg"></div></div>
                            <div class="col-md-6 plan_title">
                                <p>VOUS CHERCHEZ UN TRANSPORT POUR LES MEMBRES DE VOTRE CENTER ...</p>
                            </div>
                        </div>    
                        <div class="description">Trans express prend en charge vos enfants sur le trajet de l'Ã©cole. Ne vous inqui & Atild ...</div>
                        <div>
                            <div class="learn_button"><a href="service.php?id=7" class="button blue_button">learn more</a></div>      
                            <div class="orderbtn"><a href="booking.php?Sid=7&amp;Cid=3" class="button green_button">Book now</a></div>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="plans">
                        <div class="plans_head"><h3>Transport BUSINESS</h3></div>
                        <div class="corner-left-ribbon top-left" ><span class="head-txt1">Vente</span><span class="head-txt2">FLASH</span></div>
                        <div class="corner-right-ribbon top-right"><span class="price">$49</span><span class="startfrom">Start From</span></div>
                        <div class="corner-promo-ribbon top-right-promo"><span class="promo-service">Promotion</span></div>
                        <div class="col-md-12" id="plan_title_row">
                            <div class="col-md-6"><div class="plans_img"><img src="<?php echo base_url(); ?>assets/system_design/images/services/transfert-business.jpg"></div></div>
                            <div class="col-md-6 plan_title">
                                <p>VOUS CHERCHEZ UN TRANSPORT POUR LES MEMBRES DE VOTRE CENTER ...</p>
                            </div>
                        </div>    
                        <div class="description">Vous venez Ã  Paris en voyage d'affaires? Concentrez-vous sur le principal et confiez ...</div>
                        <div>
                            <div class="learn_button"><a href="service.php?id=7" class="button blue_button">learn more</a></div>      
                            <div class="orderbtn"><a href="booking.php?Sid=7&amp;Cid=3" class="button green_button">Book now</a></div>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="plans">
                        <div class="plans_head"><h3>Transport VOYAGE</h3></div>
                        <div class="corner-left-ribbon top-left" ><span class="head-txt1">Vente</span><span class="head-txt2">FLASH</span></div>
                        <div class="corner-right-ribbon top-right"><span class="price">$49</span><span class="startfrom">Start From</span></div>
                        <div class="corner-promo-ribbon top-right-promo"><span class="promo-service">Promotion</span></div>
                        <div class="col-md-12" id="plan_title_row">
                            <div class="col-md-6"><div class="plans_img"><img src="<?php echo base_url(); ?>assets/system_design/images/services/transfert-voyage.jpg"></div></div>
                            <div class="col-md-6 plan_title">
                                <p>VOUS CHERCHEZ UN TRANSPORT POUR LES MEMBRES DE VOTRE CENTER ...</p>
                            </div>
                        </div>    
                        <div class="description">Les pouvoirs de Trans express sont sans frontiÃ¨res! Nous partons avec vous en voyage, ...</div>
                        <div>
                            <div class="learn_button"><a href="service.php?id=7" class="button blue_button">learn more</a></div>      
                            <div class="orderbtn"><a href="booking.php?Sid=7&amp;Cid=3" class="button green_button">Book now</a></div>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
</section>

<script src="<?php echo base_url(); ?>assets/system_design/scripts/bookings.js"></script>
<script type="text/javascript">
    (function ($, W, D)
    {
        var JQUERY4U = {};

        JQUERY4U.UTIL =
            {
                setupFormValidation: function ()
                {
                    //Additional Methods
                    //form validation rules
                    $("#online_booking_form").validate({
                        rules: {
                            pick_up: {
                                required: true
                            },
                            drop_of: {
                                required: true
                            },
                            pick_time: {
                                required: true
                            }
                        },
                        messages: {
                            pick_up: {
                                required: "<?php echo $this->lang->line('pick_location_valid'); ?>"
                            },
                            drop_of: {
                                required: "<?php echo $this->lang->line('drop_location_valid'); ?>"
                            },
                            pick_time: {
                                required: "<?php echo $this->lang->line('pick_time_valid'); ?>"
                            }
                        },
                        submitHandler: function (form) {
                            form.submit();
                        }
                    });
                }
            }

        //when the dom has loaded setup form validation rules
        $(D).ready(function ($) {
            JQUERY4U.UTIL.setupFormValidation();
        });

    })(jQuery, window, document);

    $(function () {
        $('.ilike').click(function () {
            var $this = $(this);
            var p1 = $this.data('value');
            var arry = $this.data('arry');
            changeField(p1,arry);
        });
        $('.homepageSlider').bxSlider({
            mode: 'fade',
            auto: true,
            pager: true,
            autoControl: false,
            adaptiveHeight: true,
            easing: 'swing',
            responsive: true,
            preloadImages: 'all'
        });
        $(".slider_wrapper .bx-viewport").css("height", '360px');
        $('.testy-bxslider').bxSlider({
            mode: 'fade',
            controls: false,
            auto: true
        });

        $("#dtpicker").datepicker({dateFormat: "<?php echo strtolower($date_elem1); ?>-<?php echo strtolower($date_elem2); ?>-yy"});
        $("#dtpicker1").datepicker({dateFormat: "<?php echo strtolower($date_elem1); ?>-<?php echo strtolower($date_elem2); ?>-yy"});
    });
    function changeField(ss, divID)
    {
        if (ss == "pick_airport") {
            /*$('#local_pick').hide();
             $('#airport_pick').show();
             $('#airport_pick').attr('disabled', false);
             $('#local_pick').attr('disabled', true);
             $('#pick_airport_link').hide();
             $('#pick_type').val('A');
             $('#pick_local_link').show();*/
            showAirportPick();
        }
        else if (ss == "drop_airport") {
            /*$('#local_drop').hide();
             $('#airport_drop').show();
             $('#airport_drop').attr('disabled', false);
             $('#local_drop').attr('disabled', true);
             $('#drop_type').val('A');
             $('#drop_airport_link').hide();
             $('#drop_local_link').show();*/
            showAirportDrop(divID);

        }
        else if (ss == "pick_local") {
            /*$('#local_pick').show();
             $('#airport_pick').hide();
             $('#airport_pick').attr('disabled', true);
             $('#local_pick').attr('disabled', false);
             $('#pick_type').val('L');
             $('#pick_airport_link').show();
             $('#pick_local_link').hide();*/
            showLocalPick();
        }
        else if (ss == 'pick_train') {
            showTrainPick();
        }
        else if (ss == 'pick_hotel') {
            showHotelPick();
        }
        else if (ss == 'pick_park') {
            showParkPick();
        }
        else if (ss == "drop_local") {
            showLocalDrop(divID);
        }
        else if (ss == 'drop_train') {
            showTrainDrop(divID);
        }
        else if (ss == 'drop_hotel') {
            showHotelDrop(divID);
        }
        else if (ss == 'drop_park') {
            showParkDrop(divID);
        }

    }
</script>