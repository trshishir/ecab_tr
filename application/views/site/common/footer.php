<?php 
extract($company_data);

?>
<section id="partners">
  <div class="container"> <!-- padding-p-0-->
    <div class="row">
      <div class="partners_inner">
        <ul class="partners">
          <?php foreach($partners as $item):?>
            <li>
              <img src="<?php echo base_url(); ?>uploads/cms/<?=$item->image;?>" title="" alt="<?=$item->name;?>" />
            </li>
          <?php endforeach;?>
                   <!--  <li><img src="<?php echo base_url(); ?>assets/system_design/images/partners/charte-diversite.png" alt="Charte Diversite" title="Charte Diversite" /></li>
                    <li><img src="<?php echo base_url(); ?>assets/system_design/images/partners/direction.png" alt="Direction" title="Direction" /></li>
                    <li><img src="<?php echo base_url(); ?>assets/system_design/images/partners/aeroports.png" alt="Aeroports" title="Aeroports" /></li>
                    <li><img src="<?php echo base_url(); ?>assets/system_design/images/partners/iledefrance.png" alt="iLe De France" title="iLe De France" /></li>
                    <li><img src="<?php echo base_url(); ?>assets/system_design/images/partners/qtp.png" alt="QTP" title="QTP" /></li>
                    <li><img src="<?php echo base_url(); ?>assets/system_design/images/partners/ratp.png" alt="RATP" title="RATP"  /></li>
                    <li><img src="<?php echo base_url(); ?>assets/system_design/images/partners/sncf.png" alt="SNCF" title="SNCF" /></li> -->
                    <!--<li><img src="<?php echo base_url(); ?>assets/system_design/images/partners/club-chauffeur.png" /></li>
                    <li><img src="<?php echo base_url(); ?>assets/system_design/images/partners/chauffeur-prive.png" /></li>
                    <li><img src="<?php echo base_url(); ?>assets/system_design/images/partners/lecab.png" /></li>
                    <li><img src="<?php echo base_url(); ?>assets/system_design/images/partners/snap-car.png" /></li>
                    <li><img src="<?php echo base_url(); ?>assets/system_design/images/partners/allo-cab.png" /></li>-->
                    <!--
                     <li><img src="<?php echo base_url(); ?>assets/system_design/images/partners/stif.png" /></li>
                     <li><img src="<?php echo base_url(); ?>assets/system_design/images/partners/afm.png" /></li>
                     <li><img src="<?php echo base_url(); ?>assets/system_design/images/partners/apf.png" /></li>-->
                   </ul>
                 </div>
               </div>
             </div>
           </section>
           <section class="footer-top">
            <div class="container">
              <div class="row">
                <div class="footer-top-inner">
                  <div class="col-lg-3 col-md-3 col-sm-6" style="width:33%;">&nbsp;</div>
                  <div class="col-lg-9 col-md-9 col-sm-6" style="width:67%;">
                    <div class="row">
                      <div class="col-lg-3 col-md-3 col-sm-4">
                        <a href="<?php echo site_url();?>contact.php"><span class="send-us-email"><i class="fa fa-envelope"></i> <?php echo $email?></span></a>
                      </div>
                      <div class="col-lg-3 col-md-3 col-sm-4">
                        <!--<span class="call-us"><i class="fa fa-phone"></i> Fax <?php // echo $fax;?></span>-->
                          <span class="call-us"><img src="<?php echo base_url(); ?>assets/system_design/images/fax.png"/> Fax <?php echo $fax;?></span>
                      </div>
                      <div class="col-lg-3 col-md-3 col-sm-4">
                          <a class="live-assistance" href="Javascript:;"><span class="live-support"><i class="fa fa-wechat"></i> Live Support</span></a>
                      </div>
                      <div class="col-lg-3 col-md-3 col-sm-4">
                        <a href="<?php echo site_url();?>contact.php"><span class="ticket-support"><i class="fa fa-headphones"></i> Ticket Support</span></a>
                      </div>
                    </div>
                  </div>
                </div> 
              </div>
            </div>
          </section>
          <section class="footer">
            <div class="container">
              <div class="row">
                <div class="footer-inner">
                  <div class="col-lg-3 col-md-3 col-sm-6" style="width:33%;">
                      <div class="logo-info-block">
                        <div class="logo2"><img   src="<?php echo base_url(); ?>uploads/company/<?=$footer_logo;?>"  alt="eCab Logo White" title="eCab Logo White" /></div>
                        <div class="copyright-left">
                          <?php //    if(isset($site_settings->rights_reserved_content)) { ?>
                            <p>Copyright © <?php echo date("Y"); ?> <?=$name;?>  <?=$type;?> All Rights Reserved.</p>
                            <?php //} ?>
                          </div>
                          <p class="credit-cards"><!--<label><?php echo $this->lang->line('cards_we_accept');?></label>--><img src="<?php echo base_url();?>/assets/system_design/images/creditcards.png" alt="All Cards we accept" title="All Cards we accept" /></p>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-6" style="width:67%;">


                      <?php


                      foreach ($services_category as $data) :
                        $item =$data->id;
                        $found = array_filter($services_category_data,function($v,$k) use ($item){
                          return $v->category_id == $item;
                        },ARRAY_FILTER_USE_BOTH);
                        $result=(array_values($found));
                        ?>
                        <div class="col-lg-3 col-md-3 col-sm-4">
                          <div class="footer_div">
                            <div class="footer_heading">
                              <h5><?=$data->name;?> <?php echo $this->lang->line('transport_shuttle');?></h5>
                            </div>
                            <!--./footer_heading-->
                            <div class="footer_links">
                              <ul>
                                <?php
                                foreach ($result as $value):
                                  ?>
                                  <li ><a href="<?php echo site_url(); ?>services/<?=$value->id;?>/<?php echo strtolower(trim(str_replace(' ', '-', preg_replace('/[^a-zA-Z0-9_ -]/s','',$value->title))));?><?=$value->link;?>"><?=$value->title;?></a></li>

                                  <?php
                                endforeach;

                                ?>
                              </ul>
                            </div>
                          </div>
                          <!--./footer_div-->
                        </div>

                      <?php endforeach;?>

                    <?php /* ?>
                      <div class="col-lg-3 col-md-3 col-sm-4">
                        <div class="footer_div">
                          <div class="footer_heading">
                            <h5>TRANSPORT PMR REGULIER  <?php echo $this->lang->line('transport_shuttle');?></h5>
                          </div>
                          <!--./footer_heading-->
                          <div class="footer_links">
                            <ul>
                              <li> <a href="<?php echo site_url();?>handi-pro.php">HANDI PRO</a></li>
                              <li><a href="<?php echo site_url();?>handi-business.php">HANDI BUSINESS</a></li>

                              <li><a href="<?php echo site_url();?>handi-soclaire.php">HANDI SCOLAIRE</a></li>


                            </ul>
                          </div>
                        </div>
                        <!--./footer_div-->
                      </div>

                      <div class="col-lg-3 col-md-3 col-sm-4">
                        <div class="footer_div">
                          <div class="footer_heading">
                            <h5>TRANSPORT PMR A LA DEMANDE</h5>
                          </div>
                          <!--./footer_heading-->
                          <div class="footer_links">
                            <ul>
                              <li><a href="<?php echo site_url();?>handi-prive.php">HANDI PRIVE</a></li>
                              <li><a href="<?php echo site_url();?>handi-shuttle.php">HANDI SHUTTLE</a></li>
                              <li><a href="<?php echo site_url();?>handi-voyage.php">HANDI VOYAGE</a></li>
                              <li><a href="<?php echo site_url();?>handi-senior.php">HANDI SENIOR</a></li>
                              <li><a href="<?php echo site_url();?>handi-medical.php"> HANDI MEDICAL</a></li>
                            </ul>
                          </div>
                        </div>
                        <!--./footer_div-->
                      </div>

                      <div class="col-lg-3 col-md-3 col-sm-4">
                        <div class="footer_div">
                          <div class="footer_heading">
                            <h5>INFORMATION<?php echo $this->lang->line('transport_gare');?></h5>
                          </div>
                          <!--./footer_heading-->
                          <div class="footer_links">
                            <ul>
                              <li> <a href="<?php echo site_url();?>nos-tarifs.php"><?php echo $this->lang->line('tarrifs');?></a></li>

                              <li> <a href="<?php echo site_url();?>contact.php">Contact us</a></li>

                            </ul>
                          </div>
                        </div>
                        <!--./footer_div-->
                      </div>

<?php */ ?>


                        <!--<div class="col-lg-3 col-md-3 col-sm-4">
                        <div class="footer_div">
                          <div class="footer_heading">
                            <h5><?php echo $this->lang->line('useful_links');?></h5>
                          </div>
                          <div class="footer_links">
                            <ul>
                              <li><a href="<?php echo site_url();?>/services/13">Honfleur & Deauville</a></li>
                              <li><a href="<?php echo site_url();?>/services/14">Plages Des Debarquements</a></li>
                             <li><a href="<?php echo site_url();?>/services/15">Saint Malo & Monto Saint-Michel</a></li>
                            </ul>
                          </div>
                        </div>
                      </div> -->
                      <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="footer_div">
                          <div class="footer_heading">
                            <!-- <h5><?php //echo $this->lang->line('usefullinks_page');?></h5> -->
                            <h5>INFORMATION<?php echo $this->lang->line('transport_gare');?></h5>
                          </div>
                          <div class="footer_links">
                            <ul>
                               <li> <a href="<?php echo site_url();?>nos-tarifs.php"><?php echo $this->lang->line('tarrifs');?></a></li>

                              <li> <a href="<?php echo site_url();?>contact.php">Contact us</a></li>
                              <!--<li><a href="<?php echo site_url(); ?>/welcome/prices"><?php echo $this->lang->line('ourprice_page');?></a></li> -->
                              <li><a href="<?php echo site_url(); ?>zones.php"><?php echo $this->lang->line('zones_page');?></a></li>
                              <li><a href="<?php echo site_url(); ?>vehicules.php"><?php echo $this->lang->line('page_fleet');?></a></li>
                              <li><a href="<?php echo site_url(); ?>knowledgebase.php"><?php echo $this->lang->line('FAQs');?></a></li>
                              <li><a href="<?php echo site_url(); ?>downloads.php"><?php echo $this->lang->line('downloads');?></a></li>
                              <li><a href="<?php echo site_url(); ?>testimonials.php"><?php echo $this->lang->line('testimonials');?></a></li>
                              <!-- <li><a href="<?php echo site_url(); ?>/welcome/contactUs"><?php echo $this->lang->line('contact_page');?></a></li>-->
                            </ul>
                          </div>
                        </div>
                      </div>
                        <!--<div class="col-lg-3 col-md-3 col-sm-4">
                        <div class="footer_div">
                          <div class="footer_heading">
                            <h5>Trans Gare<?php echo $this->lang->line('trans_gare');?></h5>
                          </div>
                          <div class="footer_links">
                            <ul>
                              <li> <a href="#">Trans Institution<?php echo $this->lang->line('trans_institution');?></a></li>
                            </ul>
                          </div>
                        </div>
                    </div>
                     <div class="col-lg-3 col-md-3 col-sm-4">
                        <div class="footer_div">
                          <div class="footer_heading">
                            <h5><?php echo $this->lang->line('our_company');?></h5>
                          </div>
    
                          <div class="footer_links">
                            <ul>
                              <li> <a href="<?php echo site_url();?>/welcome/contactUs"><?php echo $this->lang->line('contact_us');?></a></li>
                              <li><a href="<?php echo site_url();?>/welcome/faqs"><?php echo $this->lang->line('FAQs');?></a></li>
                             <li><a href="<?php echo site_url();?>/welcome/testimonials"> <?php echo $this->lang->line('testimonials');?></a></li>
                            </ul>
                          </div>
                        </div>
                      </div> -->
                      <!--./col-lg-3-->
                        <!--
                    <div class="col-lg-3 col-md-3 col-sm-4">
                      <div class="footer_div">
    
                        <div class="footer_heading"> 
                                    <h5><?php echo $this->lang->line('pages');?></h5>
                                    </div>
                        <div class="footer_links">
                          <ul>
                          <?php
                        $this->db->select('id,name,parent_id');
                        $sub_categories = $this->db->get_where('vbs_aboutus',array('is_bottom' => '1','status' => 'Active'))->result();
                        if(count($sub_categories) > 0)
                            foreach($sub_categories as $sub_row) {
    
                                ?>
                                <li><a href="<?php echo site_url(); ?>/page/index/<?php echo $sub_row->id; ?>/<?php echo $sub_row->name;?>"><?php echo $sub_row->name;?></a></li>
    
                                <?php } ?>
                          </ul>
                        </div>
                      </div>
                      <! -- ./footer_div-- > 
                    </div>-->
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!--./container-->
        </section>
        <section class="bottom_footer">
          <div class="container">
            <div class="row"> 
              <div class="bottom_footer_inner">
                <!--<div class="col-md-3 padding-lr">
                    <div class="copyright-left">
                        <?php if(isset($site_settings->rights_reserved_content)) { ?>
                            <p><?php echo $site_settings->rights_reserved_content;?></p>
                        <?php } ?>
                    </div>
                  </div>-->

                  <div class="col-md-4">
                    <div class="footer-quick-links">
                      <ul>
                        <li>Powered by <a href="https://cabsofts.com/ecab" target="_BLANK">eCab V 1.0</a></li>
                        <li>Developed by <a href="https://cabsofts.com/" target="_BLANK">CAB SOFTS</a></li>
                      </ul>
                    </div>
                  </div>

                  <div class="col-md-8">
                    <div class="footer-quick-links">
                      <ul class="pull-left">
                        <li><a href="<?php echo site_url();?>legals/terms-of-service.php"><?php echo $this->lang->line('terms_of_service');?></a></li>
                        <li><a href="<?php echo site_url();?>legals/privacy-policy.php"><?php echo $this->lang->line('privacy_policy');?></a></li>
                        <li><a href="<?php echo site_url();?>contact.php"><?php echo $this->lang->line('report_abuse');?></a></li>
                        <li><a href="<?php echo site_url();?>legals/legal-notice.php"><?php echo $this->lang->line('legal_notice');?></a></li>
                        <li><a href="<?php echo site_url();?>legals/driver-agreement.php"><?php echo $this->lang->line('driver_agreement');?></a></li>
                        <li><a href="<?php echo site_url();?>legals/partner-agreement.php"><?php echo $this->lang->line('partner_agreement');?></a></li>
                        <li><a href="<?php echo site_url();?>legals/refund-policy.php"><?php echo $this->lang->line('refund_policy');?></a></li>
                      </ul>
                    </div>
                  </div>

                <!--<div class="col-md-3 padding-lr">
                    <div class="copyright-left right-foo">
                        <?php //if(isset($site_settings->design_by)) { ?>
                         < ! - - <p><a href="http://digitalvidhya.com" target="_blank"> <?php echo $this->lang->line('design_by');?>  <?php echo  $site_settings->design_by;?></a></p>-- >
                         <?php //} ?>
                       </div>
                     </div>-->
                   </div>
                 </div>
               </div>
             </section>
             <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

             <!-- Include all compiled plugins (below), or include individual files as needed -->

             <script src="<?php echo base_url();?>assets/system_design/scripts/jquery.min.js"></script>


             <script>
              $(function() {
        // $(".chzn-select").chosen();
      });
    </script>

    <script src="<?php echo base_url();?>assets/system_design/scripts/bootstrap.min.js"></script>
    <!--<script src="<?php echo base_url();?>assets/system_design/scripts/BeatPicker.min.js"></script>-->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script src="<?php echo base_url();?>assets/system_design/scripts/timepicki.js"></script>
    <script src="<?php echo base_url();?>assets/system_design/scripts/bx-slider.js"></script>
    <script src="<?php echo base_url(); ?>assets/system_design/scripts/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/system_design/scripts/jquery.creditCardValidator.js"></script>
   <script>
     function addwaitingtime() {
  var time1= document.getElementById("timepicker1").value;
  var time2= document.getElementById("waitingtimepicker").value
  var a=time1.split(":");
  var b=time2.split(":");
  var h=parseInt(a[0])+parseInt(b[0]);
  var m=parseInt(a[1])+parseInt(b[1]);
  if(m>=60){
    m=m-60;
    if(m<10){
      m="0"+m; 
    }
    h=h+1;
  }else{
     if(m<10){
      m="0"+m; 
    }
  }

  if(h >= 24){
  
    h=h-24;
    if(h<10){
      h="0"+h;
     }
  }else{
     if(h<10){
      h="0"+h;
     }
  }
  document.getElementById("timepicker2").value = h.toString()+" : "+m.toString();
}
      $('input.bdatepicker').datepicker({
        format: "dd/mm/yyyy"
      });
        $('#starttimepicker').timepicki({
        step_size_minutes:'5',
        show_meridian:false,
        min_hour_value:0,
        max_hour_value:23,
        overflow_minutes:true,
        increase_direction:'up',
        disable_keyboard_mobile: true});
        $('#endtimepicker').timepicki({
        step_size_minutes:'5',
        show_meridian:false,
        min_hour_value:0,
        max_hour_value:23,
        overflow_minutes:true,
        increase_direction:'up',
        disable_keyboard_mobile: true});
      $('#timepicker1').timepicki({
        step_size_minutes:'5',
        on_change: addwaitingtime,
        show_meridian:false,
        min_hour_value:0,
        max_hour_value:23,
        overflow_minutes:true,
        increase_direction:'up',
        disable_keyboard_mobile: true});


      $('#timepicker2').timepicki({
         step_size_minutes:'5',
        show_meridian:false,
        min_hour_value:0,
        max_hour_value:23,
        overflow_minutes:true,
        increase_direction:'up',
        disable_keyboard_mobile: true});
       $('#waitingtimepicker').timepicki({
        type_of_timezone:"zero",
         step_size_minutes:'5',
        show_meridian:false,
        min_hour_value:0,
        max_hour_value:23,
        overflow_minutes:true,
        increase_direction:'up',
        disable_keyboard_mobile: true});
      $('#go_time_1, #back_time_1').timepicki({ step_size_minutes:'5',show_meridian:false,min_hour_value:0,max_hour_value:23,overflow_minutes:true,increase_direction:'up',disable_keyboard_mobile: true});
      $('#go_time_2, #back_time_2').timepicki({ step_size_minutes:'5',show_meridian:false,min_hour_value:0,max_hour_value:23,overflow_minutes:true,increase_direction:'up',disable_keyboard_mobile: true});
      $('#go_time_3, #back_time_3').timepicki({ step_size_minutes:'5',show_meridian:false,min_hour_value:0,max_hour_value:23,overflow_minutes:true,increase_direction:'up',disable_keyboard_mobile: true});
      $('#go_time_4, #back_time_4').timepicki({ step_size_minutes:'5',show_meridian:false,min_hour_value:0,max_hour_value:23,overflow_minutes:true,increase_direction:'up',disable_keyboard_mobile: true});
      $('#go_time_5, #back_time_5').timepicki({ step_size_minutes:'5',show_meridian:false,min_hour_value:0,max_hour_value:23,overflow_minutes:true,increase_direction:'up',disable_keyboard_mobile: true});
      $('#go_time_6, #back_time_6').timepicki({ step_size_minutes:'5',show_meridian:false,min_hour_value:0,max_hour_value:23,overflow_minutes:true,increase_direction:'up',disable_keyboard_mobile: true});
      $('#go_time_7, #back_time_7').timepicki({ step_size_minutes:'5',show_meridian:false,min_hour_value:0,max_hour_value:23,overflow_minutes:true,increase_direction:'up',disable_keyboard_mobile: true});

        $('#timepicker_pick_fly').timepicki({
        step_size_minutes:'5',
        show_meridian:false,
        min_hour_value:0,
        max_hour_value:23,
        overflow_minutes:true,
        increase_direction:'up',
        disable_keyboard_mobile: true});
        $('#timepicker_pick_fly1').timepicki({
         step_size_minutes:'5',
        show_meridian:false,
        min_hour_value:0,
        max_hour_value:23,
        overflow_minutes:true,
        increase_direction:'up',
        disable_keyboard_mobile: true});
        $('#timepicker_drop_fly_1').timepicki({
        step_size_minutes:'5',
        show_meridian:false,
        min_hour_value:0,
        max_hour_value:23,
        overflow_minutes:true,
        increase_direction:'up',
        disable_keyboard_mobile: true});
         $('#timepicker_pick_stn').timepicki({
           step_size_minutes:'5',
        show_meridian:false,
        min_hour_value:0,
        max_hour_value:23,
        overflow_minutes:true,
        increase_direction:'up',
        disable_keyboard_mobile: true});
          $('#timepicker_pick_stn1').timepicki({
         step_size_minutes:'5',
        show_meridian:false,
        min_hour_value:0,
        max_hour_value:23,
        overflow_minutes:true,
        increase_direction:'up',
        disable_keyboard_mobile: true});
           $('#timepicker_drop_stn').timepicki({
         step_size_minutes:'5',
        show_meridian:false,
        min_hour_value:0,
        max_hour_value:23,
        overflow_minutes:true,
        increase_direction:'up',
        disable_keyboard_mobile: true});
   
    </script>
    <?php if(in_array("homebooking",$css_type)) { ?>

      <?php echo $this->load->view('site/common/script'); ?>

    <?php } ?>

    <?php if(in_array("onlinebooking",$css_type) || in_array("passengerdetails",$css_type)) { ?>

      <?php echo $this->load->view('site/common/online_script'); ?>

    <?php } ?>
    <!--Date Table-->
    <?php if(in_array("datatable",$css_type)) { ?>
      <script type="text/javascript" language="javascript" src="<?php echo base_url();?>assets/system_design/scripts/jquery.dataTables.js"></script>
      <script type="text/javascript" language="javascript" class="init">

        $(document).ready(function() {
          $('#example').dataTable();
          $('.example').dataTable();
        } );

      </script>
    <?php } ?>
    <!--Date Table-->



    <script type="text/javascript" class="init">



      function setActiveOnlinePackage(id) {
        numid = id.split('_')[1];
        $('#cars_data_list div').removeClass('active');
        $('li').removeClass('active');
        $('#'+id).parent().closest('ul').addClass('active');
        $('#'+id).parent().parent().addClass('car-sel-bx active');

      }

    </script>
    <!--Slider-->
    <?php if(in_array("slider",$css_type)) { ?>
    <?php } ?>
    <!--Slider-->

    <script type="text/javascript">
      $('.partners').bxSlider({
        minSlides: 5,
        maxSlides: 8,
        slideWidth: 240,
        slideMargin: 10,
        infiniteLoop: true,
        auto:true
      });
      $url = "<?php echo site_url(); ?>";
      $lang = $url.split(".fr/");
      if($lang[1] == 'en') {
        $(".liContactUs").css("width","108px");
        $(".liContactUs").css("text-align","center");
        $(".top-links").css("margin-left","0px");
        //  $(".copyright-left").css("font-size","9px");
      }
      else if ($lang[1] == 'fr') {
        $(".liContactUs").css("width","147px");
        $(".liContactUs").css("text-align","center");
        $(".liContactUs a").css("font-size","12px");
        $(".top-links").css("margin-left","-50px");
        //  $(".copyright-left").css("font-size","9px");
      }
      $(".scroll-up > .bx-wrapper").css("max-width","570px");
      var promoAudio = document.getElementById('mp3-audio');
      promoAudio.controls = false;
      function togglePlayPause() {
        var playpause = document.getElementById("playpause");
        if (promoAudio.paused) {
         $('.player-icon').removeClass('fa-play-circle');
         $('.player-icon').addClass('fa-pause-circle');
         playpause.title = "pause";
           //playpause.innerHTML = "pause";
           promoAudio.play();
         }
         else {
          $('.player-icon').removeClass('fa-pause-circle');
          $('.player-icon').addClass('fa-play-circle');
          playpause.title = "play";
            //playpause.innerHTML = "play";
            promoAudio.pause();
          }
        }



      </script>

      <script type="text/javascript">
    
$(document).ready(function(){

var count_li =$('.navbar-nav.menu').children('li').length;
// var li_width =(1/count_li *100);

// $(".navbar-nav.menu ").children('li').css("width","calc(1/"+count_li+" *100%");

// $(".navbar-nav.menu ").css("text-align","center");



});

</script>

<!-- Google Analytics -->
<script>
window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
ga('create', '<?php echo $site_seo[0]->google_analytics;?>', 'auto');
ga('send', 'pageview');
</script>
<script async src='https://www.google-analytics.com/analytics.js'></script>
<!-- End Google Analytics -->

    <script type="text/javascript">
        function openpopup(url,title) {
            window.open(url, title, "width=450,height=300");
        }
      var adDiv = document.getElementById("myAd");
        /*var myAdControl = new MicrosoftNSJS.Advertising.AdControl(adDiv,
        {
            applicationId: "<?php echo  $site_seo[0]->microsoft_ads;?>",
            adUnitId: "test",
        });

        myAdControl.isAutoRefreshEnabled = false;
        myAdControl.onErrorOccurred = myAdError;
        myAdControl.onAdRefreshed = myAdRefreshed;
        myAdControl.onEngagedChanged = myAdEngagedChanged;
       */
    </script>


<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '<?php echo  $site_seo[0]->facebook_ads;?>',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v6.0'
    });
  };
</script>
<script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>

<?php // var_dump( $site_seo[0] );?>
    </body>
    </html>
