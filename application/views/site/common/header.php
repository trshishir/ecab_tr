<?php

$page_lang = 'lang="fr"';
$page_controller = $this->uri->segment(1);
$controller_app = $this->router->fetch_class();
$CI =& get_instance();
$CI->load->model('general_model');
// echo $controller_app;exit;
$method = $this->router->fetch_method();
$areas = array('driver','jobs','partner','affiliate');
$controller = (in_array($page_controller,$areas) ) ? $page_controller : 'client' ;
extract($company_data);
// extract($site_seo[0]);
// $languages = $this->db->where('status', 1)->order_by('title', 'ASC')->get('vbs_languages')->result();
$languages = $CI->general_model->get_record('vbs_languages','status', 1);  
$currencies =  $CI->general_model->get('vbs_currencies');

?>
<!DOCTYPE html>
<html <?php echo $page_lang; ?>>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php
    // echo '<pre>';
    //         var_dump($this->uri->segment(1));
    // echo $this->router->fetch_class();
    // echo $this->router->fetch_method();

    //         die();
    ?>

    <?php  if(!in_array($this->router->fetch_class(), array('blog','services'))): ?>


        <meta name="keywords" content="<?php if (isset($site_seo[0]->meta_tag)) echo $site_seo[0]->meta_tag; ?>"/>
        <meta name="description" content="<?php if (isset($site_seo[0]->meta_description)) echo $site_seo[0]->meta_description; ?>" />
        <!-- <title><?php //echo $site_seo[0]->title; ?></title> -->

    <?php endif;?>

    <?php  if($this->router->fetch_class()=='blog'): ?>


        <meta name="keywords" content="<?php echo implode(',', array_column($news, 'meta_tag')); ?>"/>
        <meta name="description" content="<?php echo implode(',', array_column($news, 'meta_description')); ?>" />
    <?php
    endif;?>



    <?php  if($this->router->fetch_class()=='services'): ?>


        <meta name="keywords" content="<?php echo implode(',', array_column($services, 'meta_tag')); ?>"/>
        <meta name="description" content="<?php echo implode(',', array_column($services, 'meta_description')); ?>" />
    <?php
    endif;?>


    <title><?php echo $site_seo[0]->title; ?></title>






    <!--        <meta name="keywords" content="<?php //if (isset($meta_keywords)) echo $meta_keywords; ?>"/>
        <meta name="description" content="<?php //if (isset($meta_description)) echo $meta_description; ?>" />
        <title><?php //echo $title; ?></title> -->


    <link rel='shortcut icon' href='<?php echo base_url(); ?>assets/system_design/images/favicon.ico' type='image/x-icon'/>
    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/system_design/css/styles.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/system_design/css/callback.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/system_design/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <?php if (isset($site_settings->site_theme) && $site_settings->site_theme == "Red") { ?>
        <link href="<?php echo base_url(); ?>assets/system_design/css/cab-2ntheame.css" rel="stylesheet">
    <?php } else { ?>
        <link href="<?php echo base_url(); ?>assets/system_design/css/cab.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/system_design/css/custom-style.css" rel="stylesheet">
    <?php } ?>
    <!-- add by sultan-->
   <?php
    if($content=="site/booking/booking" || $content=="site/booking/booking_quote" || $content=="site/booking/booking_payment"){?>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/default/css/style.css">
    <?php  }  ?>


    <!-- end by sultan-->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php $google_api_info = get_google_api();?>
    <?php if(!empty($google_api_info) && $google_api_info->status == 1){?>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?=$google_api_info->api_key?>&libraries=places"></script>
    <?php } ?>
</head>
<body>
<header>
    <div class="header-wrapper">
        <div class="navbar-fixed-top">
            <div class="top-section">
                <div class="container">
                    <!-- <div class="container-fluid"> -->
                    <div class="row">
                        <div class="top_section_inner">
                            <div class="col-md-6 col-sm-6 col-xs-12 section-cen">
                                <ul class="top-links">
                                    <?php if($controller_app !='driver'){ ?>
                                    <li>
                                        <a href="<?php echo base_url();?>driver.php"><img src="https://i.imgur.com/Aimkg8w.png"  /><span style="float:left;">Driver</span></a>
                                    </li>
                                    <?php } ?>
                                    <?php if($controller_app !='auth' && $controller_app !='client' && $controller_app !='book_now' && $controller_app !='services' && $controller_app !='welcome' && $controller_app !='blog' ){ ?>
                                    <li>
                                        <a href="<?php echo base_url();?>site.php"><img src="<?php echo base_url() . "assets/system_design/images/cliente.png";?>"  /><span style="float:left;">Client</span></a>
                                    </li>
                                    <?php }?>
                                    <?php if($controller_app !='jobs'){ ?>
                                    <li>
                                        <a href="<?php echo base_url();?>jobs.php"><img src="https://i.imgur.com/F4cXf41.png" style="border-radius:50%;"  /><span style="float:left;">Jobs</span></a>
                                    </li>
                                    <?php } ?>
                                    <?php if($controller_app !='partner'){ ?>
                                    <li>
                                        <a href="<?php echo base_url();?>partner.php"><img src='<?php echo base_url() . "assets/system_design/images/partner.png";?>'  /><span style="float:left;">Partner</span></a>
                                    </li>
                                    <?php } ?>
                                    <?php if($controller_app !='affiliate'){ ?>
                                    <li>
                                        <a href="<?php echo base_url();?>affiliate.php"><img src='<?php echo base_url() . "assets/system_design/images/money-icon.png" ;?>' /><span style="float:left;">Affiliate</span></a>
                                    </li>
                                    <?php } ?>
                                </ul>
                                <?php   if (!$this->ion_auth->logged_in()) {    ?>
                                    <!-- <div class="social-icons left" style="margin-top:4px; margin-right: 10px !important">
                                                    <a class="top-login" href="#" data-href="<?php // echo site_url(); ?>chauffeur.php"><?php // echo $this->lang->line('driver_login');
                                    ?>
                                                    </a>
                                                </div>
                                            -->
                                <?php } ?>
                                <div class="social-icons left"> <?php $current_url = current_url();?>
                                    <ul>
                                        <?php if(!empty($facebook_link)){?>
                                            <li>
                                                <a href="<?=$facebook_link?>" >
                                                    <!--<a href="javascript:void(0);" onclick="openpopup('//www.facebook.com/sharer.php?u=<?php /*echo $current_url;*/?>','Facebook Share')">-->
                                                    <img class="facebook" src='<?php echo base_url() . "assets/system_design/images/facebook-icon.png" ?>' alt="eCab Facebook" title="eCab Facebook" />
                                                </a>
                                            </li>
                                        <?php } ?>


                                        <?php if(!empty($twitter_link)){?>
                                            <li>
                                                <a href="<?=$twitter_link?>" >
                                                    <!--<a href="javascript:void(0);" onclick="openpopup('//twitter.com/share?url=<?php echo $current_url;?>','Twitter Share')">-->
                                                    <img class="twitter" src='<?php echo base_url() . "assets/system_design/images/twitter-icon.png" ?>' alt="eCab Twitter" title="eCab Twitter"/>
                                                </a>
                                            </li>
                                        <?php } ?>


                                        <?php if(!empty($linkedin_link)){?>
                                            <li>
                                                <a href="<?=$linkedin_link?>">
                                                    <!--<a href="javascript:void(0);" onclick="openpopup('//www.linkedin.com/shareArticle?mini=true&url=<?php /*echo $current_url;*/?>','LinkedIn Share')">-->
                                                    <img class="linkedin" src='<?php echo base_url() . "assets/system_design/images/linkedin-icon.png" ?>' alt="eCab LinkedIn" title="eCab LinkedIn"/>
                                                </a>
                                            </li>
                                        <?php } ?>
                                        <?php if(!empty($youtube_link)){?>
                                            <li>
                                                <a href="<?=$youtube_link?>">
                                                <!--<a href="javascript:void(0);" onclick="openpopup('//www.youtube.com','Youtube')">-->
                                                    <img src='<?php echo base_url() . "assets/system_design/images/youtube-icon.png" ?>' alt="eCab GooglePlus" title="eCab YouTube"/>
                                                </a>
                                            </li>
                                        <?php } ?>
                                        <?php if(!empty($instagram_link)){?>
                                            <li>
                                                <a href="<?=$instagram_link?>">
                                                <!--<a href="javascript:void(0);" onclick="openpopup('//www.instagram.com','Instagram')">-->
                                                    <img src='<?php echo base_url() . "assets/system_design/images/instagram-icon.png" ?>' alt="eCab Instagram" title="eCab Instagram"/>
                                                </a>
                                            </li>
                                        <?php } ?>
                                        <?php
                                        /*
                                                $social_networks = $this->base_model->run_query("SELECT * FROM vbs_social_networks");
                                                //  social_networks
                                                if (isset($social_networks[0]->facebook)) {
                                        ?>
                                                    <li>
                                                        <a href="<?php echo $social_networks[0]->facebook; ?>"   target="_blank">
                                                            <img class="facebook" src='<?php echo base_url() . "assets/system_design/images/facebook-white.png" ?>' alt="eCab Facebook" title="eCab Facebook" />
                                                    </li>
                                        <?php
                                                }
                                                if (isset($social_networks[0]->twitter)) {
                                        ?>
                                                    <li>
                                                        <a href="<?php echo $social_networks[0]->twitter; ?>"   target="_blank">
                                                            <img class="twitter" src='<?php echo base_url() . "assets/system_design/images/twitter-white.png" ?>' alt="eCab Twitter" title="eCab Twitter"/>
                                                        </a>
                                                    </li>
                                        <?php
                                                }
                                                if (isset($social_networks[0]->linkedin)) {
                                        ?>
                                                    <li>
                                                        <a href="<?php echo $social_networks[0]->linkedin; ?>"  target="_blank">
                                                            <img class="linkedin" src='<?php echo base_url() . "assets/system_design/images/linkedin-white.png" ?>' alt="eCab LinkedIn" title="eCab LinkedIn"/>
                                                        </a>
                                                    </li>
                                        <?php
                                                }
                                                if (isset($social_networks[0]->google_plus)) {
                                        ?>
                                                    <!--<li>
                                                        <a href="<?php // echo $social_networks[0]->google_plus;  ?>" target="_blank">
                                                            < --<i class="fa fa-google-plus"></i> -- >
                                                            <img src='<?php // echo base_url() . "assets/system_design/images/google-icon.png" ?>' alt="eCab GooglePlus" title="eCab GooglePlus"/>
                                                        </a>
                                                    </li> -->
                                        <?php   }

                                        */
                                        ?>
                                        <!--<li class="bookmark" ><a href="javascript:(function(){var a=window,b=document,c=encodeURIComponent,d=a.open('http://www.seocentro.com/cgi-bin/promotion/bookmark/bookmark.pl?u='+c( b.location )+'&amp;t='+c( b.title ),'bookmark_popup','left='+((a.screenX||a.screenLeft)+10)+',top='+((a.screenY||a.screenTop)+10)+',height=480px,width=720px,scrollbars=1,resizable=1,alwaysRaised=1');a.setTimeout(function(){ d.focus()},300)})();"><span class="bookmark-icon"><img src="<?php echo base_url(); ?>assets/images/bookmark-64x.png" alt="Add to Favourite" title="Add to Favourite" /></span></a></li>-->
                                        <?php // if ($site_settings->app_settings == "Enable") { ?>
                                        <?php if(!empty($android_client)){?>
                                            <li class="mobile-apps"> <div>Download App</div> <a href="<?=$android_client?>" > <img class="linkedin" src='<?php echo base_url() . "assets/system_design/images/android-os-24.png" ?>' alt="eCab Android App " title="eCab Android App"/> </a> </li>

                                        <?php } ?>
                                        <?php if(!empty($ios_client)){?>
                                            <li class="mobile-apps"> <a href="<?=$ios_client?>" > <img class="linkedin" src='<?php echo base_url() . "assets/system_design/images/apple-logo-24.png" ?>' alt="eCab iOS App " title="eCab iOS App"/> </a> </li>
                                        <?php } ?>
                                        <li class="mobile-apps">
                                            <!-- <img style="    transform: rotate(135deg); width: 36px; height: 22px; margin-left: -6px;" src="<?php echo base_url();?>assets/images/appicon-arrow.png"> -->

                                            <!--<div id="curvedarrow" style="top:15px; left:3px;"></div>-->


                                        </li>



                                        <?php // }  ?>
                                    </ul>
                                </div>
                                <!-- Add to Favorites
                                        <div class="top-pack-book-links <?php if ($this->lang->lang() == 'fr') { echo 'leftalign_fr'; } else { echo 'leftalign_en'; } ?>">
                                            <ul>
                                                <li><a class="bookmark" href="javascript:(function(){var a=window,b=document,c=encodeURIComponent,d=a.open('http://www.seocentro.com/cgi-bin/promotion/bookmark/bookmark.pl?u='+c( b.location )+'&amp;t='+c( b.title ),'bookmark_popup','left='+((a.screenX||a.screenLeft)+10)+',top='+((a.screenY||a.screenTop)+10)+',height=480px,width=720px,scrollbars=1,resizable=1,alwaysRaised=1');a.setTimeout(function(){ d.focus()},300)})();"><span class="bookmark-icon"><img src="<?php echo base_url(); ?>assets/images/bookmark-64x.png" alt="Add to Favourite" title="Add to Favourite" /></span></a></li>
                                            </ul>
                                        </div> -->
                            </div>

                            <div class="col-md-1 col-xs-1"></div>
                            <div class="col-md-5 col-sm-5 section-ce2">
                                 <div class="audio" style="left: 55px;">
                                    <audio <?php echo ($front_autoplay_repeat==1)?' loop ':'';?> class="mp3-audio" id="mp3-audio">
                                        <source src="<?php echo base_url(); ?>assets/system_design/audio/<?php echo $audio_file;?>" type="audio/mpeg">
                                        Your browser does not support the audio element.
                                    </audio>
                                    <div id="controls"><a class="" id="playpause" onclick="togglePlayPause()"><i class="fa fa-play-circle player-icon" style="font-size:20px;"></i></a></div>
                                    <!--<object class="audioObj"  width="90" height="14" data="http://flash-mp3-player.net/medias/player_mp3_mini.swf" type="application/x-shockwave-flash" title="Adobe Flash Player">
                                                <param value="http://flash-mp3-player.net/medias/player_mp3_mini.swf" name="movie">
                                                <param value="#5590C1" name="bgcolor">
                                                <?php //if ($this->lang->lang() == 'fr') { ?>
                                                        <param value="mp3=<?php echo base_url(); ?>assets/system_design/audio/navetteo_french.mp3&amp;bgcolor=0D528A&amp;slidercolor=fb833e&amp;autoplay=1&amp;autoload=0" name="FlashVars">
                                                <?php //} else {
                                    ?>          <param value="mp3=<?php echo base_url(); ?>assets/system_design/audio/navetteo_english.mp3&amp;bgcolor=0D528A&amp;slidercolor=fb833e&amp;autoplay=1&amp;autoload=0" name="FlashVars">
                                                <?php //} ?>
                                            </object>
                                        -->
                                </div>
                                <div class="currency" style="float: inherit;left: 55px;">
                                    <select class="grad-bg form-control remove-apperance" id="default_currency" onchange="change_default_currency();">
                                        <?php foreach($currencies as $key => $value){ ?>
                                           <option value="<?php echo $value['id']; ?>" <?php echo ($value['default_status'] == 'yes')?'selected':'' ;?> data-admin="<?php echo $value['allow_front_change'];?>"><?php echo $value['symbole']; ?>&nbsp;<?php echo ucfirst($value['currency_code']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="lang-icons" style="float: inherit;left: 56px;"><!--class="selec" id="uli"-->
                                   <li class="btn-group">
                                        <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false" style="color:#4a4a4a;"> 
                                            <?php
                                            foreach($languages as $lang){
                                                if($lang['defult'] =='yes'){
                                                    echo '<img src="'.base_url().'assets/system_design/images/'.$lang['flag'].'" />'; echo '&nbsp;';  echo ucwords($lang['title']);
                                                }
                                            }
                                            ?>
                                            <b class="caret"></b>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <?php
                                            foreach ($languages as $lang) :
                                            ?>
                                            <li onclick="change_default_language('<?php echo $lang['id'];?>','<?php echo $lang['fontchange'];?>');" style="cursor: pointer;">
                                                <a href="<?= base_url() ?>global_controller/set_language/<?= $lang['title'] ?>" title="<?=$lang->title?>"><img src="<?= base_url(); ?>assets/system_design/images/<?= $lang['flag'] ?>" / > <?= ucwords($lang['title']); ?> </a>
                                            </li>
                                            <?php
                                            endforeach;
                                            ?>
                                        </ul>
                                    </li> 
                                </div>
                                <?php   if($front_autoplay ==1): ?>

                                    <script type="text/javascript">

                                        window.onload = function() {

                                            setTimeout(function(){
                                                var context = new AudioContext();

                                                togglePlayPause();
                                            },5);
                                        }

                                    </script>
                                <?php endif;?>
                                <?php if ($site_settings->app_settings == "Enable") { ?>
                                    <div class="top-links">
                                        <a href="<?php echo site_url(); ?>/welcome/download_app/android"><img src="<?php echo base_url(); ?>assets/system_design/images/header/google-play-icon.png" style="width:48%" alt="eCab Google Apps" title="eCab Google Apps " /></a>
                                        <a href="<?php echo site_url(); ?>/welcome/download_app/ios"><img src="<?php echo base_url(); ?>assets/system_design/images/header/apple-icon.png" style="width:48%" alt="eCab iPhone Apps" title="eCab iPhone Apps"/></a>
                                    </div>
                                <?php } ?>
                                <?php   if (!$this->ion_auth->logged_in()) { ?>
                                    <div class="user-nav" style="float: right;">
                                        <div class="btn-group" role="group">
                                            <div class="btn-group" role="group">
                                                <a href="<?php echo site_url();?><?php  echo $controller;?>/signup.php">
                                                    <button type="button" class="btn btn-default loginout register"><i class="fa fa-user"></i> Register</button>
                                                </a>
                                            </div>
                                            <div class="btn-group" role="group">
                                                <a href="<?php echo site_url(); ?><?php  echo $controller;?>/login.php">
                                                    <button type="button" class="btn btn-default loginout login"><i class="fa fa-lock"></i> Login</button>
                                                </a>
                                            </div>
                                        </div>
                                   </div>
                                <?php }else{?>
                                    <div class="user-nav" style="float: right;">
                                        <div class="btn-group" role="group">
                                            <div class="btn-group" role="group">
                                            <a href="<?php echo site_url();?><?php  echo $controller;?>/identification.php">
                                                <button type="button" class="btn clientaccountbtn"><i class="fa fa-user"></i> Account</button>
                                            </a>
                                            </div>
                                             <div class="btn-group" role="group">
                                                <a href="<?php echo site_url(); ?>client/logout">
                                                    <button type="button" class="btn clientloginlogoutbtn"><i class="fa fa-lock"></i> Logout</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div><!-- end collumn-->
                        </div> <!--end row-->
                    </div>
                </div>
            </div>

            <div class="container secondary-header">
                <!-- <div class="container-fluid"> padding-p-0-->
                <div class="row">
                    <div class="col-md-1"> <!--padding-p-l-->
                        <a href="<?php echo site_url(); ?>index.php">
                            <div class="header-logo"><img  src="<?php echo base_url(); ?>uploads/company/<?=$logo;?>" alt="eCab Logo" title="eCab Logo" /></div>

                        </a>
                    </div>
                    <?php
                    $from_day=json_decode($from_day);
                    $to_day=json_decode($to_day);
                    $start_time_1=json_decode($start_time_1);
                    $end_time_1=json_decode($end_time_1);
                    $start_time_2=json_decode($start_time_2);
                    $end_time_2=json_decode($end_time_2);

                    $working_hour ="<p class='working-hours'> ";
                    foreach ($from_day as $key => $value) {
                        if( $key>0)
                        {
                            $working_hour .='<br>';
                        }
                        $working_hour .="From $value To $to_day[$key] <br> From $start_time_1[$key]h To $end_time_1[$key]h and  From $start_time_2[$key]h To $end_time_2[$key]h ";
                    }
                    $working_hour .='</p>';
                    ?>
                    <div class="col-md-10">
                        <?php if ($this->lang->lang() == 'fr') { ?>
                            <div class="col-md-4 right french-contact-number"><!--padding-p-r-->
                                <?php
                                //$phone = $site_settings->land_line;
                                // echo "01 48 13 09 34";
                                echo $phone;
                                ?>


                                <!-- <p class="working-hours" style=" left:35px;">Du Lundi au Vendredi <br/>de 9h à 12h et de 14h à 18h</p> -->
                                <!-- <p class="working-hours" style="left:40px;">From <?=$from_day;?> to <?=$to_day;?> <br/><?=$start_time_1;?>h to <?=$end_time_1;?>h and from <?=$start_time_2;?>h to <?=$end_time_2;?>h</p> -->

                                <?=$working_hour;?>

                            </div>
                        <?php } else { ?>
                            <div class="col-md-4 right english-contact-number">
                                <?php
                                //    $phone = $site_settings->phone;
                                // echo "01 48 13 09 34";
                                echo $phone;
                                ?>
                                <?=$working_hour;?>

                            </div>
                        <?php } ?>
                        <div class="col-md-1 right">


                            <img src="<?php echo base_url(); ?>assets/system_design/images/call-us-girl.png" class="call-us-girl" alt="Call Us" title="Call Us" /></div>
                        <div class="header-banner">
                            <!-- <img src="<?php echo base_url(); ?>assets/system_design/images/header-banner.png" alt="eCab Header Banner"  title="eCab Header Banner"/> -->
                            <img src="<?php echo base_url(); ?>uploads/cms/<?=$banner;?>" alt="eCab Header Banner"  title="eCab Header Banner"/>

                        </div>
                    </div>

                </div>
                <!--</div> -->
            </div> <!--/.container-fluid -->
            <div class="main-menu"> <!-- style="height: 70px;" -->
                <div class="container nav-border">
                    <div class="row">
                        <nav class="navbar navbar-navetteo menu-total">
                            <div class="navbar-header">
                                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed nav-bar-btn" type="button"> <span class="sr-only"><?php echo $this->lang->line('toggle_navigation'); ?></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                            </div>
                            <div class="collapse navbar-collapse res-menu" id="navbar">
                                <?php   if (!$this->ion_auth->logged_in()) { ?>
                                    <ul class="nav navbar-nav menu">


                                        <li <?php if (isset($active_class)) { if ($active_class == "home") echo 'class="active"'; } ?> >
                                            <a href="<?php echo site_url(); ?>index.php"> <i class="fa fa-home"></i> <?php echo $this->lang->line('home_page'); ?></a>
                                        </li>

                                        <!-- <?php echo site_url(); ?>/welcome/onlineReservation"> -->
                                        <li class="obook <?php if (isset ($active_class)) { if ($active_class == "onlinebooking") echo 'active'; } ?>" >
                                            <a href="<?php echo site_url(); ?>booking.php"><i class="fa fa-book"></i> <?php echo $this->lang->line('book_online'); ?></a>
                                        </li>
                                        <!--<li class="packages <?php // if (isset($active_class)) { if ($active_class == "packages") echo 'active'; } ?>"><a href="<?php // echo site_url(); ?>/packages"><?php // echo $this->lang->line('packages'); ?></a></li>-->



                                        <?php


                                        foreach ($services_category as $data) :
                                        $item =$data->id;
                                        $found = array_filter($services_category_data,function($v,$k) use ($item){
                                            return $v->category_id == $item;
                                        },ARRAY_FILTER_USE_BOTH);
                                        $result=(array_values($found));
                                        ?>

                                        <li class="parks drop-menu menu-drop<?php if (isset($active_class)) { if ($active_class == "aeroport") echo 'class="active"'; } ?>">
                                            <a href="#" aria-expanded="false" role="button" data-toggle="dropdown" class="dropdown-toggle aeroport-a"><i class="fa fa-car" ></i><?=$data->name;?><span class="caret"></span> </a>
                                            <ul  style="width:204px;" role="menu" class="dropdown-menu drop-menu">

                                                <?php
                                                foreach ($result as $value):
                                                    ?>
                                                    <li style="width:204px;"><a href="<?php echo base_url("services/{$value->category_link}/{$value->link_url}{$value->link}"); ?>"><?=$value->title;?></a></li>

                                                <?php
                                                endforeach;

                                                ?>

                                                </li>

                                            </ul>
                                            <?php
                                            // var_dump($result);

                                            endforeach;


                                            ?>

                                            <?php /*
                                                        <li class="parks drop-menu menu-drop<?php if (isset($active_class)) { if ($active_class == "aeroport") echo 'class="active"'; } ?>">
                                                            <a href="#" aria-expanded="false" role="button" data-toggle="dropdown" class="dropdown-toggle aeroport-a"><i class="fa fa-car" ></i>TRANSPORT PMR REGULIER<span class="caret"></span> </a>
                                                            <ul  style="width:204px;" role="menu" class="dropdown-menu drop-menu">
                                                                <li style="width:204px;"><a href="<?php echo site_url(); ?>handi-pro.php">HANDI PRO</a></li>
                                                                <li style="width:204px;"><a href="<?php echo site_url(); ?>handi-business.php">HANDI BUSINESS</a></li>
                                                                <li style="width:204px;"><a href="<?php echo site_url(); ?>handi-scolaire.php">HANDI SCOLAIRE</a></li>
                                                            </ul>
                                                        </li>

                                                        <li class="parks drop-menu menu-drop<?php if (isset($active_class)) { if ($active_class == "transport_parcs") echo 'class="active"'; } ?>">
                                                            <a href="#" aria-expanded="false" role="button" data-toggle="dropdown" class="dropdown-toggle parks-a"><i class="fa fa-car" ></i>TRANSPORT PMR A LA DEMANDE<span class="caret"></span> </a>
                                                            <ul style="width:240px;" role="menu" class="dropdown-menu drop-menu">

                                                                <li style="width:240px;" ><a href="<?php echo site_url(); ?>handi-prive.php">HANDI PRIVE</a></li>

                                                                <li style="width:240px;" ><a href="<?php echo site_url(); ?>handi-shuttle.php">HANDI SHUTTLE</a></li>

                                                                <li style="width:240px;"><a href="<?php echo site_url(); ?>handi-voyage.php">HANDI VOYAGE</a></li>

                                                                <li><a href="<?php echo site_url(); ?>handi-senior.php">HANDI SENIOR</a></li>

                                                                <li><a href="<?php echo site_url(); ?>handi-medical.php">HANDI MEDICAL</a></li>



                                                            </ul>
                                                        </li>

                                                        */ ?>

                                        <li <?php if (isset($active_class)) { if ($active_class == "prices") echo 'class="active"'; } ?> ><a href="<?php echo site_url(); ?>prices.php"><i class="fa fa-money" ></i><?php echo $this->lang->line('tarrifs'); ?></a></li>
                                        <li <?php if (isset($active_class)) { if ($active_class == "zones") echo 'class="active"'; } ?> ><a href="<?php echo site_url(); ?>zones.php"><i class="fa fa-area-chart" ></i><?php echo $this->lang->line('zones_page'); ?></a></li>
                                        <li <?php if (isset($active_class)) { if ($active_class == "fleet") echo 'class="active"'; } ?> ><a href="<?php echo site_url(); ?>fleet.php"><i class="fa fa-truck" ></i><?php echo $this->lang->line('page_fleet'); ?></a></li>
                                        <li <?php if (isset($active_class)) { if ($active_class == "faqs") echo 'class="active"'; } ?> ><a href="<?php echo site_url(); ?>faq.php"><i class="fa fa-question-circle" ></i><?php echo $this->lang->line('FAQs'); ?></a></li>
                                        <li <?php if (isset($active_class)) { if ($active_class == "downloads") echo 'class="active"'; } ?> ><a href="<?php echo site_url(); ?>downloads.php"><i class="fa fa-download" ></i><?php echo $this->lang->line('downloads'); ?></a></li>
                                        <li <?php if (isset($active_class)) { if ($active_class == "testimonials") echo 'class="active"'; } ?> ><a href="<?php echo site_url(); ?>testimonials.php"><i class="fa fa-comments-o" ></i><?php echo $this->lang->line('testimonial_page'); ?></a></li>
                                        <li <?php if (isset($active_class)) { if ($active_class == "news") echo 'class="active"'; } ?> ><div class="menu-new-icon">News</div><a href="<?php echo site_url(); ?>blog.php"><i class="fa fa-newspaper-o" ></i><?php echo 'Blog'; ?></a></li>
                                        <li style="width:113px" class="green <?php if (isset($active_class)) { if ($active_class == "contactus") echo 'active'; } ?>" ><a style="text-align: center;" href="<?php echo site_url('contact') ?>"><i class="fa fa-envelope" ></i> <?php echo $this->lang->line('contact_us'); ?></a></li>
                                    </ul>
                                    <?php
                                }
                                else if ($this->ion_auth->logged_in()) {
                                    ?>
                                    <ul class="nav navbar-nav menu">
                                        <li <?php if (isset($active_class)) { if ($active_class == "home") echo 'class="active"'; } ?> ><a href="<?php echo site_url(); ?>index.php"> <i class="fa fa-home"></i> <?php echo $this->lang->line('home_page'); ?></a></li>
                                        <li class="obook <?php if (isset($active_class)) { if ($active_class == "onlinebooking") echo 'active'; } ?>" ><a href="<?php echo site_url(); ?>reservation.php"><?php echo $this->lang->line('book_online'); ?></a></li>
                                        <li class="packages <?php if (isset($active_class)) { if ($active_class == "packages") echo 'active'; } ?>"><a href="<?php echo site_url(); ?>/packages"><?php echo $this->lang->line('packages'); ?></a></li>
                                        <li <?php if (isset($active_class)) { if ($active_class == "my_bookings") echo 'active'; } ?>><a href="<?php echo site_url(); ?>/users/myBookings"><?php echo $this->lang->line('my_bookings'); ?></a></li>
                                        <li <?php if (isset($active_class)) { if ($active_class == "my_quotes") echo 'active'; } ?>><a href="<?php echo site_url(); ?>/users/myQuotes"><?php echo $this->lang->line('my_quotes'); ?></a></li>
                                        <li <?php if (isset($active_class)) { if ($active_class == "my_invoices") echo 'active'; } ?>><a href="<?php echo site_url(); ?>/users/myInvoices"><?php echo $this->lang->line('my_invoices'); ?></a></li>
                                        <li <?php if (isset($active_class)) { if ($active_class == "my_support_tickets") echo 'active'; } ?>><a href="<?php echo site_url(); ?>/users/mySupportTickets"><?php echo $this->lang->line('my_support_tickets'); ?></a></li>
                                    </ul>
                                <?php   } ?>
                            </div>
                            <!--/.nav-collapse -->
                        </nav>



                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/system_design/scripts/jquery.js"></script>
    <script src="<?php echo base_url(); ?>/assets/system_design/form_validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>/assets/system_design/form_validation/js/additional-methods.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    function change_default_currency(){
        var id = $('#default_currency').val()
            status = $('#default_currency').find(':selected').attr('data-admin');
        if(status =='no'){
            alert('Sorry You are not Allow to change');
        }else{
            $.ajax({
                url:'<?=base_url()?>admin/change_default_currency',
                type: "post",
                data:{'currency_id':id},
                dataType:'json',
                success: function(res){
                }
            })

        }
        
    }
    function change_default_language(id,status){
        if(status =='no'){
            alert('Sorry You are not Allow to change');
        }else{
            $.ajax({
                url:'<?=base_url()?>admin/change_default_language',
                type: "post",
                data:{'id':id},
                dataType:'json',
                success: function(res){
                    alert('working');
                }
            })

        }
        
    }
    
    </script>
    <div class="announcementsbox">
        <div class="container" style="position:relative;">
            <div class="row">
                <div class="announcement_inner">
                    <div class="col-md-12 col-xs-6">
                        <h2>Flash News </h2>
                        <marquee class="annn-marque 2">
                            <!-- <a href="#">eCab is the most advanced cab booking script in the market</a> -->
                            <a href="#"><?=$flash_news;?></a>

                        </marquee>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('header_modals.php'); ?>
