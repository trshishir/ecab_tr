<style>
    .pop-error{
        color: red;
        display: none;
    }
    a.btn-successs {
        border: 1px solid;
        padding: 5px 15px;
    }
    .btn-con{
        color: #fff !important;
        font-size: 14px !important;
        font-weight: bold !important;
        padding: 5px 18px !important;
        height: auto;
        background-color: rgb(163, 0, 150);
        background-image: -o-linear-gradient(to bottom,#a10096 0%,#a30096 100%);
        background-image: -moz-linear-gradient(to bottom,#a10096 0%,#a30096 100%);
        background-image: -ms-linear-gradient(to bottom,#a10096 0%,#a30096 100%);
        background-image: -webkit-linear-gradient(to bottom,#a10096 0%,#a30096 100%);
        background: linear-gradient(180deg, rgb(241, 10, 219) 0%, rgba(139,0,163,1) 100%);
        border: 1px solid #FEFEFE !important;
        box-shadow: 0 1px 0 #00B22D inset !important;
    }
    #myModal .modal-backdrop {
        z-index: 0 !important;
    }
    #myModal{
        top: 100px;
    }
    .pac-container {
        z-index: 99999999999;
    }
</style>
<?php
error_reporting(0);
$data = $this->base_model->run_query("SELECT * FROM " . $this->db->dbprefix('popups_settings') . " where id = 1")[0];
?>
<?php if ($data->status1 == 1) { ?>
    <div class="<?php if ($data->position1 == 1) {
        echo "floating-form-2 ";
    } else {
        echo "floating-form ";
    } ?> <?= $data->auto_open1 == 1 ? "visiable" : "" ?>"  id="<?php if ($data->position1 == 1) {
        echo "contact_form_2";
    } else if ($data->position1 == 2) {
        echo "jobs_form";
    } else {
        echo "contact_form";
    } ?>">
        <div class="<?php if ($data->position1 == 1) {
            echo "contact-opener-2";
        } else if ($data->position1 == 2) {
            echo "contact-opener-3";
        } else {
            echo "contact-opener";
        } ?>">
            <?php echo $data->name1 ?>
        </div>
        <div id="contact_results"></div>
        <div id="contact_body">
            <a href="javascript:;" class="close-btn-2"><i class="fa fa-times-circle-o"></i></a>
            <?php echo form_open_multipart('auth/callMe', ['id' => 'the-contact-form-2', 'class' => 'form']); ?>
            <input type="hidden" name="success_message" value="<?= strip_tags($data->success_message1) ?>">
            <h4 style="text-align: center; margin: 0 0 8px">DEMANDE DE RAPPEL <?php // $data->name1 ?></h4>
            <?php
            $c_user = $this->session->userdata('user');
            if($this->session->userdata('usergroup')=='admin'){
                $c_user = '';
            }
            ?>
            <?php if(($this->session->userdata('usergroup')=='admin')||($this->session->userdata('usergroup')=='')){ ?>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-question"></i></span>
                            <select onchange="pop_login_user1(this.value)" class="form-control" style="height: 35px !important;color: #000;" name="pop_user_type1">
                                <option value="Other">Autre</option>
                                <option value="Client">Client</option>
                                <option value="Driver">Driver</option>
                                <option value="Partner">Partner</option>
                                <option value="Jobseeker">Jobseeker</option>
                            </select>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div id="supp_login1" style="display:none;">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input id="username1" type="text" class="form-control" value="" required name="username" placeholder="Username*">
                    </div>
                    <small class="pop-error" id="err-username1">Username is required*</small>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input id="password1" type="password" class="form-control password" value="" name="password" placeholder="Password*">
                        <span class="input-group-btn">
							<button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-close"></i></button>
						</span>
                    </div>
                    <small class="pop-error" id="err-password1">Password is required*</small>
                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-6 col-xs-6">
                            <div class="form-group" style="margin:0;">
                                <div class="checkbox" style="margin:0;">
                                    <label> <input type="checkbox" style="margin-right: 0; margin-top: 4px; margin-left: -20px; float: none;" value="1" name="remember_me1"> Remember me </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-6" style="text-align: right;font-size: 11px;">
                            <a href="#" data-toggle="modal" data-target="#myModal"> <?php echo $this->lang->line('login_forgot_password'); ?></a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12" style="text-align: center">
                        <a href="#" class="btn-successs" onclick="supp_login1()"><?php echo 'Login'; ?></a>
                    </div>
                </div>
            </div>
            <div id="supp_other1">
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <div class="input-group" style="width:29%;float:left">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <select class="form-control s_civility1" name="civility" <?php echo $c_user->civility!=''?'readonly':''; ?> required>
                                <?php foreach (config_model::$civility as $key => $civil): ?>
                                    <option value="<?= $civil ?>" <?php echo $c_user->civility==$civil?'selected':''; ?>><?= $civil ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="input-group" style="width:35%;float:left">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" maxlength="100" class="form-control s_firstname1" required name="name" placeholder="Nom*" value="<?php echo $c_user->first_name; ?>" <?php echo $c_user->first_name!=''?'readonly':''; ?>>
                        </div>
                        <div class="input-group" style="width:36%;float:left">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" maxlength="100" class="form-control s_lastname1" required name="prename" placeholder="Prenom*" value="<?php echo $c_user->last_name; ?>" <?php echo $c_user->last_name!=''?'readonly':''; ?>>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input id="enterprise" maxlength="50" type="text" class="form-control s_company1" name="company" placeholder="Entreprise ou Organisme (Optionnel)" value="<?php echo $c_user->company_name; ?>" <?php echo $c_user->company_name!=''?'readonly':''; ?>>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                <input id="num2" maxlength="50" type="text" class="form-control s_phone1" required name="num" placeholder="Telephone*" value="<?php echo $c_user->phone; ?>" <?php echo $c_user->phone!=''?'readonly':''; ?>>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <input id="phone-email" maxlength="100" type="email" class="form-control" required name="email" placeholder="Votre email*" value="<?php echo $c_user->email; ?>" <?php echo $c_user->email!=''?'readonly':''; ?>>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <select class="form-control" name="subject" required>
                                <option value="">Objet de l'appel*</option>
                                <?php foreach (config_model::$call_subjects as $key => $subject): ?>
                                    <option value="<?= $subject ?>"><?= $subject ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group" >
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <select class="form-control" style="height: 35px !important;" name="days" required>
                                <option value="">Jours de Rappel*</option>
                                <?php foreach (config_model::$call_days as $key => $day): ?>
                                    <option value="<?= $day ?>"><?= $day ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6 form-group" style="/*padding:0 4px; width: 30%*/">
                        <div class="input-group">
                            <span class="input-group-addon normal-addon">DE</span>
                            <select class="form-control" name="from_time" style="width: 90px !important;" required>
                                <?php for ($i = 0; $i <= 24; $i++):
                                    $time = $i > 9 ? $i : "0" . $i;
                                    ?>
                                    <option value="<?= $time ?>"><?= $time ?></option>
                                <?php endfor; ?>
                            </select>
                            <span class="input-group-addon normal-addon" style="border: 0; background: #f2f2f2; padding: 6px 0">H</span>
                        </div>
                    </div>
                    <div class="col-xs-6 form-group" style="/*padding:0 4px 0 0; width: 26%*/">
                        <div class="input-group">
                            <span class="input-group-addon normal-addon" >A</span>
                            <select class="form-control" name="to_time" style="width: 98px !important;" required>
                                <?php for ($i = 0; $i <= 24; $i++):
                                    $time = $i > 9 ? $i : "0" . $i;
                                    ?>
                                    <option value="<?= $time ?>"><?= $time ?></option>
                                <?php endfor; ?>
                            </select>
                            <span class="input-group-addon normal-addon" style="border: 0; background: #f2f2f2; padding: 6px 0">H</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <textarea rows="3" maxlength="3000" class="form-control" name="message" placeholder="Merci de nous communiquer le motif de votre appel"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12" style="text-align: center">
                        <button class="btn-successs" type="submit">Rappelez moi</button>
                    </div>
                </div>
            </div>
            <?= form_close(); ?>
        </div>
    </div>
    <!-- / End Call Back Form -->
<?php } ?>

<?php if ($data->status2 == 1 && $controller == 'jobs') { ?>
    <!-- Jobs Slide Form -->
    <div class="<?php if ($data->position2 == 1) {
        echo "floating-form-2 ";
    } else {
        echo "floating-form ";
    } ?> <?= $data->auto_open2 == 1 ? "visiable" : "" ?>"  id="<?php if ($data->position2 == 1) {
        echo "contact_form_2";
    } else if ($data->position2 == 2) {
        echo "jobs_form";
    } else {
        echo "contact_form";
    } ?>">
        <div class="<?php if ($data->position2 == 1) { echo "contact-opener-2"; } else if ($data->position2 == 2) { echo "contact-opener-3"; } else { echo "contact-opener"; } ?>">
            <?= $data->name2 ?>
        </div>
        <div id="contact_results"></div>
        <div id="contact_body">
            <a href="javascript:;" class="close-btn-3 closee-btn"><i class="fa fa-times-circle-o"></i></a>
            <?php echo form_open_multipart('auth/submitJobForm', ['id' => 'the-job-form', 'class' => 'form']); ?>
            <input type="hidden" name="success_message" value="<?= strip_tags($data->success_message2) ?>">
            <?php
            $c_user = $this->session->userdata('user');
            if($this->session->userdata('usergroup')!='jobseekers'){
                $c_user = '';
            }
            ?>
            <?php if($this->session->userdata('usergroup')!='jobseekers'){ ?>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-question"></i></span>
                            <select onchange="pop_job_user(this.value)" class="form-control" style="height: 35px !important;color: #000;" name="job_user">
                                <option value="New">Nouveau Demandeur d'emploi ?</option>
                                <option value="Registered">Déjà Demandeur d'emploi</option>
                            </select>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div id="job_login1" style="display:none;">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input id="j_username1" type="text" class="user form-control" name="username" placeholder="Username*">
                    </div>
                    <small class="pop-error" id="err-j_username1">Username is required*</small>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input id="j_password1" type="password" class="form-control password" name="password" placeholder="Password*">
                        <span class="input-group-btn">
							<button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-close"></i></button>
						</span>
                    </div>
                    <small class="pop-error" id="err-j_password1">Password is required*</small>
                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-6 col-xs-6">
                            <div class="form-group" style="margin:0;">
                                <div class="checkbox" style="margin:0;">
                                    <label> <input type="checkbox" style="margin-right: 0; margin-top: 4px; margin-left: -20px; float: none;" value="1" name="remember_me2"> Remember me </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-6" style="text-align: right;font-size: 11px;">
                            <a href="#" data-toggle="modal" data-target="#myModal"> <?php echo $this->lang->line('login_forgot_password'); ?></a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-12" style="text-align: center">
                        <a href="#" class="btn-successs" onclick="job_login1()"><?php echo 'Login'; ?></a>
                    </div>
                </div>
            </div>
            <div id="job_other1">
                <div class="row">
                    <div class="col-xs-12 form-group" >
                        <div class="input-group" style="width:29%;float:left">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <select class="form-control j_civility" style="" name="civility" <?php echo $c_user->civility!=''?'readonly':''; ?> required>
                                <?php foreach (config_model::$civility as $key => $civil): ?>
                                    <option value="<?= $civil ?>" <?php echo $c_user->civility==$civil?'selected':''; ?>><?= $civil ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="input-group" style="width:35%;float:left">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" maxlength="100" class="form-control j_firstname" required name="name" placeholder="Nom*" value="<?php echo $c_user->first_name; ?>" <?php echo $c_user->first_name!=''?'readonly':''; ?>>
                        </div>
                        <div class="input-group" style="width:36%;float:left">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" maxlength="100" class="form-control j_lastname" required name="prename" placeholder="Prenom*" value="<?php echo $c_user->last_name; ?>" <?php echo $c_user->last_name!=''?'readonly':''; ?>>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                <input id="tel" maxlength="50" type="text" class="form-control j_phone" required name="tel" placeholder="Telephone*" value="<?php echo $c_user->phone; ?>" <?php echo $c_user->phone!=''?'readonly':''; ?>>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <input id="email" maxlength="100" type="email" class="form-control j_email" required name="email" placeholder="Email*" value="<?php echo $c_user->email; ?>" <?php echo $c_user->email!=''?'readonly':''; ?>>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                <input id="job_autocomplete" data-parent="the-job-form" type="text" class="form-control j_address" required name="address1" placeholder="Addresse*" value="<?php echo $c_user->address; ?>" <?php echo $c_user->address!=''?'readonly':''; ?>>
                                <?php if(!empty($google_api_info) && $google_api_info->status == 1){?>
                                    <?php if($google_api_info->permission->front->status == 1 && $google_api_info->permission->front->position == 1){?>
                                        <i class="location-icon" onclick="geolocate_model('job')"></i>
                                    <?php } ?>
                                <?php } ?>
                                <input type="hidden" name="latitude" value="">
                                <input type="hidden" name="longitude" value="">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                <input id="address2" type="text" class="form-control"  name="address2" placeholder="Complément d'adresse">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-xs-12 form-group">
                        <div class="input-group" style="width:50%;float:left">
                            <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                            <input type="text" maxlength="20" class="form-control j_zipcode" required name="postal_code" placeholder="CodePostal*" value="<?php echo $c_user->zipcode; ?>" <?php echo $c_user->zipcode!=''?'readonly':''; ?>>
                        </div>

                        <div class="input-group" style="width:50%;float:left">
                            <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                            <input type="text" class="form-control j_city" required name="ville" placeholder="Ville*" value="<?php echo $c_user->city; ?>" <?php echo $c_user->city!=''?'readonly':''; ?>>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <div class="input-group" style="width:50%;float:left">
                            <span class="input-group-addon"><i class="fa fa-car"></i></span>
                            <select class="form-control" name="situation">
                                <option value="retirement">Retraite</option>
                                <option value="unemployment">Chomage</option>
                                <option value="rsa">RSA</option>
                                <option value="onpole">En poste</option>
                            </select>
                        </div>
                        <div class="input-group" style="width:50%;float:left">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" class="form-control bdatepicker j_dob" name="dob" required placeholder="Date de naissance" value="<?php echo $c_user->dob; ?>" <?php echo $c_user->dob!=''?'readonly':''; ?>>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-car"></i></span>
                            <select class="form-control" name="offer" required>
                                <option value="">Sélectionnez l'Offre d'emploi</option>
                                <?php   foreach ($jobs_listing as $key => $offer): ?>
                                    <?php       $selected = "";
                                    if ($job_detail) {
                                        if ($job_detail->row()->id == $offer->id) {
                                            $selected = 'selected';
                                        }
                                    }
                                    ?>
                                    <option <?= $selected ?> value="<?= $offer->id ?>"><?= $offer->job_title ?></option>
                                <?php   endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-12">
                        <label>CV*</label>
                        <input type="file" required name="cv" accept=".pdf, .doc, .docx">
                    </div>
                    <div class="form-group col-xs-12">
                        <label>Lettre de Motivation</label>
                        <input type="file" name="letter" accept=".pdf, .doc, .docx">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12" style="text-align: center">
                        <button class="btn-successs" type="submit">Postuler</button>
                        <img class="displayimageloader" style="float: right; margin-right: 10px;display: none;" src="<?php echo base_url() . 'assets/css/loading.gif' ?>" />

                    </div>
                </div>
            </div>
            <?= form_close(); ?>
        </div>
    </div>
<?php } ?>

<?php if ($data->status3 == 1 && $controller == 'client') { ?>
    <!-- Contact Slide Form -->
    <div class="<?php if ($data->position3 == 1) { echo "floating-form-2 "; } else { echo "floating-form "; } ?> <?= $data->auto_open3 == 1 ? "visiable" : "" ?>" id="<?php if ($data->position3 == 1) { echo "contact_form_2"; } else if ($data->position3 == 2) { echo "jobs_form"; } else { echo "contact_form"; } ?>">
        <div class="<?php if ($data->position3 == 1) { echo "contact-opener-2"; } else if ($data->position3 == 2) { echo "contact-opener-3"; } else { echo "contact-opener"; } ?>">
            <?= $data->name3 ?>
        </div>
        <div id="contact_results"></div>
        <div id="contact_body">
            <a href="javascript:;" class="close-btn closee-btn"><i class="fa fa-times-circle-o"></i></a>
            <?php echo form_open_multipart('auth/submitContactForm', ['id' => 'the-contact-form', 'class' => 'form']); ?>
            <input type="hidden" name="success_message" value="<?= strip_tags($data->success_message3) ?>">
            <?php
            $c_user = $this->session->userdata('user');
            if($this->session->userdata('usergroup')!='clients'){
                $c_user = '';
            }
            ?>
            <?php if($this->session->userdata('usergroup')!='clients'){ ?>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-question"></i></span>
                            <select onchange="pop_qoute_user(this.value)" class="form-control" style="height: 35px !important;color: #000;" name="quote_user">
                                <option value="New">Nouveau Client ?</option>
                                <option value="Client">Déjà Client</option>
                            </select>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div id="client_login1" style="display:none;">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input id="c_username1" type="text" class="form-control" value="" name="username" placeholder="Username*">
                    </div>
                    <small class="pop-error" id="err-c_username1">Username is required*</small>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input id="c_password1" type="password" class="form-control password" value="" name="password" placeholder="Password*">
                        <span class="input-group-btn">
							<button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-close"></i></button>
						</span>
                    </div>
                    <small class="pop-error" id="err-c_password1">Password is required*</small>
                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-6 col-xs-6">
                            <div class="form-group" style="margin:0;">
                                <div class="checkbox" style="margin:0;">
                                    <label> <input type="checkbox" style="margin-right: 0; margin-top: 4px; margin-left: -20px; float: none;" value="1" name="remember_me3"> Remember me </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-6" style="text-align: right;font-size: 11px;">
                            <a href="#" data-toggle="modal" data-target="#myModal"> <?php echo $this->lang->line('login_forgot_password'); ?></a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-12" style="text-align: center">
                        <a href="#" class="btn-successs" onclick="client_login1()"><?php echo 'Login'; ?></a>
                    </div>
                </div>
            </div>
            <div id="client_other1">
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <div class="input-group" style="width:29%;float:left">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <select class="form-control c_civility" style="color: #000;" name="civility" <?php echo $c_user->civility!=''?'readonly':''; ?> required>
                                <?php foreach (config_model::$civility as $key => $civil): ?>
                                    <option value="<?= $civil ?>" <?php echo $c_user->civility==$civil?'selected':''; ?>><?= $civil ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="input-group" style="width:35%;float:left">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" maxlength="100" class=" form-control c_firstname" required name="name" placeholder="Nom*" value="<?php echo $c_user->first_name; ?>" <?php echo $c_user->first_name!=''?'readonly':''; ?>>
                        </div>
                        <div class="input-group" style="width:36%;float:left">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" maxlength="100" class=" form-control c_lastname" required name="prename" placeholder="Prenom*" value="<?php echo $c_user->last_name; ?>" <?php echo $c_user->last_name!=''?'readonly':''; ?>>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <div class="input-group">
                                <span class="input-group-addon" style="padding:6px 10px;"><i class="fa fa-home"></i></span>
                                <input id="enterprise2" maxlength="50" type="text" class="form-control c_company" name="company" placeholder="Entreprise ou Organisme  (Optionnel)" value="<?php echo $c_user->company_name; ?>" <?php echo $c_user->company_name!=''?'readonly':''; ?>>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                <input id="tel" maxlength="50" type="text" class="form-control c_phone" required name="tel" placeholder="Telephone*" value="<?php echo $c_user->phone; ?>" <?php echo $c_user->phone!=''?'readonly':''; ?>>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <div class="input-group">
                                <span class="input-group-addon" style="padding:6px 10px;"><i class="fa fa-envelope"></i></span>
                                <input id="email" maxlength="100" type="email" class="form-control c_email" required name="email" placeholder="Votre email*" value="<?php echo $c_user->email; ?>" <?php echo $c_user->email!=''?'readonly':''; ?>>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                <input id="contact_autocomplete" data-parent="the-contact-form" type="text" class="form-control c_address" required name="address1" placeholder="Addresse du devis*" value="<?php echo $c_user->address; ?>" <?php echo $c_user->address!=''?'readonly':''; ?>>
                                <?php if(!empty($google_api_info) && $google_api_info->status == 1){?>
                                    <?php if($google_api_info->permission->front->status == 1 && $google_api_info->permission->front->position == 1){?>
                                        <i class="location-icon" onclick="geolocate_model('contact')"></i>
                                    <?php } ?>
                                <?php } ?>
                                <input type="hidden" name="latitude" value="">
                                <input type="hidden" name="longitude" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                <input id="address2" type="text" class="form-control"  name="address2" placeholder="Complément d'adresse">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <div class="input-group" style="width:50%;float:left">
                            <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                            <input type="text" maxlength="20" class="form-control c_zipcode" required name="postal_code" placeholder="CodePostal*" value="<?php echo $c_user->zipcode; ?>" <?php echo $c_user->zipcode!=''?'readonly':''; ?>>
                        </div>
                        <div class="input-group" style="width:50%;float:left">
                            <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                            <input type="text" class="form-control c_city" required name="ville" placeholder="Ville*" value="<?php echo $c_user->city; ?>" <?php echo $c_user->city!=''?'readonly':''; ?>>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 form-group">
                        <div class="input-group" style="width:50%;float:left">
                            <span class="input-group-addon" style="padding:6px 8px;"><i class="fa fa-car"></i></span>
                            <select class="form-control" id="the-contact-form-service-cat" name="service_category">
                                <option value="">Service Category</option>
                                <?php
                                $service_cat = $this->base_model->run_query("SELECT * FROM vbs_u_category_service");
                                if($service_cat != false)
                                    foreach($service_cat as $key => $item){
                                        ?>
                                        <option <?php  if(isset($service_form['service_category']['value']) && !empty($service_form['service_category']['value'])){ echo ($service_form['service_category']['value']==$item->id)?"selected":"";} ?> value="<?= $item->id ?>"><?= $item->category_name ?></option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="input-group" style="width:50%;float:left">
                            <span class="input-group-addon" style="padding:6px 8px;"><i class="fa fa-car"></i></span>
                            <select class="form-control" name="service_id" id="the-contact-form-service">
                                <option value="">Service</option>
                                <?php
                                $service = $this->base_model->run_query("SELECT * FROM vbs_u_service");
                                if($service != false)
                                    foreach($service as $key => $item){
                                        ?>
                                        <option cat-data="<?=$item->service_category?>" value="<?= $item->id; ?>"><?= $item->service_name; ?></option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class=" form-group">
                            <textarea rows="5" style="height: 140px" maxlength="3000" class="form-control" name="message" required placeholder="Merci de nous indiquez toutes les informations nécessaires afin de vous fournir un devis pour la prestation de transport souhaitée*"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12" style="text-align: center">
                        <button class="btn-successs" type="submit">ENVOYER</button>
                    </div>
                </div>
            </div>
            <?= form_close(); ?>
        </div>
    </div>
<?php } ?>

<?php if ($data->status4 == 1) { ?>
<!-- Jobs Slide Form -->
<div class="<?php if ($data->position4 == 1) {
    echo "contact-opener-2";
} else if ($data->position4 == 2) {
    echo "contact-opener-3";
} else {
    echo "contact-opener-4";
} ?>"><?= $data->name4 ?></div>
<div class="<?php if ($data->position4 == 1) {
    echo "floating-form-2 ";
} else {
    echo "assistance-form ";
} ?> <?= $data->auto_open4 == 1 ? "visiable" : "" ?>"  id="<?php if ($data->position4 == 1) {
    echo "contact_form_2";
} else if ($data->position4 == 2) {
    echo "jobs_form";
} else {
    echo "support_form";
} ?>">

    <?php
    $chat_settings = $this->base_model->run_query2("SELECT * FROM ".$this->db->dbprefix('header_settings'));
    if($chat_settings['chat_status']=="active") {
        ?>
        <div class="contact-header">
            <i class="fa fa-wechat"></i> Live Support
        </div>
        <div class="chat_loader text-center">
            <img src="assets/loader_image.gif">
        </div>
        <div class="basic-chat-detail" <?php if ($this->session->userdata('chatuserexist') == true) { ?> style="display: none;" <?php } ?>>
            <a href="javascript:;" class="close-btn-4"><i class="fa fa-times-circle-o"></i></a>
            <!--<div class="row">
                <div class="col-md-12"> -->
            <?php
            $c_user = $this->session->userdata('user');
            if($this->session->userdata('usergroup')=='admin'){
                $c_user = '';
            }
            ?>
            <?php if(($this->session->userdata('usergroup')=='admin')||($this->session->userdata('usergroup')=='')){ ?>
                <div class="row">
                    <div class="col-md-12 form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-question"></i></span>
                            <select onchange="pop_login_user2(this.value)" class="form-control" name="pop_user_type2">
                                <option value="Other">Other</option>
                                <option value="Client">Client</option>
                                <option value="Driver">Driver</option>
                                <option value="Partner">Partner</option>
                                <option value="Jobseeker">Jobseeker</option>
                            </select>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div id="supp_login2" style="display:none;">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input id="username2" type="text" class="form-control" value="" required name="username" placeholder="Username*">
                    </div>
                    <small class="pop-error" id="err-username2">Username is required*</small>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input id="password2" type="password" class="form-control" value="" name="password" placeholder="Password*">
                        <span class="input-group-btn">
                                                <button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-close"></i></button>
                                        </span>
                    </div>
                    <small class="pop-error" id="err-password2">Password is required*</small>
                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-6 col-xs-6">
                            <div class="form-group" style="margin:0;">
                                <div class="checkbox" style="margin:0;">
                                    <label> <input type="checkbox" style="margin-right: 0; margin-top: 4px; margin-left: -20px; float: none;" value="1" name="remember_me4"> Remember me </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-6" style="text-align: right;font-size: 11px;">
                            <a href="#" data-toggle="modal" data-target="#myModal"> <?php echo $this->lang->line('login_forgot_password'); ?></a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-12" style="text-align: center">
                        <a href="#" class="btn-successs btn-con" onclick="supp_login2()"><?php echo 'Login'; ?></a>
                    </div>
                </div>
            </div>
            <div id="supp_other2">

                <div class="row" >
                    <div class="col-xs-12 form-group">
                        <div class="input-group" style="width:28%;float:left">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <select class="form-control s_civility2"  name="civility" <?php echo $c_user->civility!=''?'readonly':''; ?> required>
                                <?php foreach (config_model::$civility as $key => $civil): ?>
                                    <option value="<?= $civil ?>" <?php echo $c_user->civility==$civil?'selected':''; ?>><?= $civil ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="input-group" style="width:36%;float:left">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input id="name" type="text" maxlength="100" class="form-control s_firstname2" required name="name" placeholder="Nom*" value="<?php echo $c_user->first_name; ?>" <?php echo $c_user->first_name!=''?'readonly':''; ?>>
                        </div>
                        <div class="input-group" style="width:36%;float:left">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" maxlength="100" class="form-control s_lastname2" required name="prename" placeholder="Prenom*" value="<?php echo $c_user->last_name; ?>" <?php echo $c_user->last_name!=''?'readonly':''; ?>>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-home"></i></span>
                            <input id="enterprise" maxlength="50" type="text" class="form-control s_company2" name="company" placeholder="Entreprise ou Organisme (Optionnel)" value="<?php echo $c_user->company_name; ?>" <?php echo $c_user->company_name!=''?'readonly':''; ?>>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <input id="email2" maxlength="100" type="email" class="form-control s_email2" value="<?php echo $c_user->email; ?>" required="" name="email" placeholder="Email*" <?php echo $c_user->email!=''?'readonly':''; ?>>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                        <input id="telephone" maxlength="50" type="text" class="form-control s_phone2" value="<?php echo $c_user->phone; ?>" required="required" name="telephone" placeholder="Telephone*" <?php echo $c_user->phone!=''?'readonly':''; ?>>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12" style="text-align: center">
                        <button class="btn-successs btn-con" type="button" onclick="validateinputdata()"><?php echo 'Continue'; ?></button>
                    </div>
                </div>
            </div>
            <!--    </div>
            </div> -->

        </div>
        <div class="start-chat-div" <?php if ($this->session->userdata('chatuserexist') == true) { } else { echo 'style="display: none;"'; } ?>>
            <div class="historychating" id="historychating">
                <?php if ($this->session->userdata('chatuserexist') == true) {
                    $message_history = $this->base_model->run_query3("SELECT * FROM " . $this->db->dbprefix('messages_history') . " where userid_from=" . $this->session->userdata('chatuserid') . " or userid_to=1"); ?>
                    <?php   foreach ($message_history as $val) { ?>
                        <div class="users-chat-div2">
                            <div class="usercontent-div">
                                <p><strong><?php if ($val->type == 1) { echo'Admin'; } else { echo 'Me'; } ?></strong></p>
                                <p><?php echo $val->message_text; ?></p>
                                <span class="real-chat-badge"><small><?php echo date('Y-m-d', strtotime($val->dateandtime)) . ' ' . date('H:i', strtotime($val->dateandtime)); ?></small> <i class="fa fa-check" aria-hidden="true"> </i><i class="fa fa-check" aria-hidden="true"></i></span>
                            </div>
                        </div>
                    <?php   } ?>
                <?php } else { } ?>

            </div>
            <?php //echo form_open_multipart('', ['id' => '', 'class' => 'form','action'=> '#']);  ?>
            <div id="attachfile_div" class="text-center" style="width:45px;float: right;display: none;">
                <div id="otherfieattacmentdiv"></div>
                <img src="" id="changeattachfileimage" style="width: 100%;border: 1px solid #ddd; border-radius: 4px; padding: 3px;">
                <!-- <div class="text-center" id="attachfilename" style="line-height: 1;"></div> -->
            </div>
            <!-- <form action="<?php echo base_url(); ?>Messages/insertchatmessagedata" enctype="multipart/form-data" id="the-chat-form" class="form"> -->
            <?php echo form_open_multipart('', ['id' => 'the-chat-form', 'class' => 'form']); ?>
            <div class="form-group input_area_emoji" style="clear: both;">
                <input type="text" name="messagetext" id="input-left-position" class="form-control" placeholder="Type message" style="height: inherit !important;" required="required">
                <input type="hidden" name="userid" id="chatuserid" <?php if (!$this->session->userdata('chatuserid')) { } else { echo "value='" . $this->session->userdata('chatuserid') . "'"; } ?>>
            </div>
            <div class="row" style="width: 100%; margin: 0px !important;">
                <div class="col-xs-6" style="padding-left: 10px;">
                    <input type="file" name="chatfile" id="chatfile" style="visibility: hidden;height: 0px;width: 0px;" onchange="addattachfileto_div()">
                    <i class="fa fa-paperclip" aria-hidden="true" onclick="addchatfile()" style="cursor: pointer; font-size: 20px;"></i> &nbsp;
                    <i class="fa fa-smile-o" aria-hidden="true" onclick="openemojis()" style="cursor: pointer; font-size: 20px;"></i>
                </div>
                <div class="col-xs-6" style="text-align: right;">
                    <button class="btn-successs btn-con" type="submit"><?php echo 'Send'; ?> <img src="assets/btn_loader.gif" style="display: none;width: 16px; position: relative; top: 3px; left: 3px;" id="btn_loader"></button>
                </div>
            </div>
            <?= form_close(); ?>
        </div>
    <?php   }
    else {
    ?>
    <div id="contact_results"></div>
    <div id="contact_body">
        <a href="javascript:;" class="close-btn-4"><i class="fa fa-times-circle-o"></i></a>
        <?php echo form_open_multipart('auth/submitContactFormPopup', ['id' => 'the-support-form', 'class' => 'form']); ?>
        <input type="hidden" name="success_message" value="<?= strip_tags($data->success_message4) ?>">
        <?php
        $c_user = $this->session->userdata('user');
        if($this->session->userdata('usergroup')=='admin'){
            $c_user = '';
        }
        ?>
        <?php if(($this->session->userdata('usergroup')=='admin')||($this->session->userdata('usergroup')=='')){ ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group col-md-12">
                            <select onchange="pop_login_user3(this.value)" class="form-control" style="color: #000;" name="pop_user_type3">
                                <option value="Other">Other</option>
                                <option value="Client">Client</option>
                                <option value="Driver">Driver</option>
                                <option value="Partner">Partner</option>
                                <option value="Jobseeker">Jobseeker</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div id="supp_login3" style="display:none;">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input id="username3" type="text" class="form-control" value="" required name="username" placeholder="Username*">
                </div>
                <small class="pop-error" id="err-username3">Username is required*</small>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input id="password3" type="password" class="form-control password" value="" name="password" placeholder="Password*">
                    <span class="input-group-btn">
										<button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-close"></i></button>
									</span>
                </div>
                <small class="pop-error" id="err-password3">Password is required*</small>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group" style="margin:0;">
                            <div class="checkbox" style="margin:0;">
                                <label> <input type="checkbox" style="margin-right: 0; margin-top: 4px; margin-left: -20px; float: none;" value="1" name="remember_me5"> Remember me </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6" style="text-align: right;font-size: 11px;">
                        <a href="#" data-toggle="modal" data-target="#myModal"> <?php echo $this->lang->line('login_forgot_password'); ?></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12" style="text-align: center">
                    <a href="#" class="btn-successs btn-con" onclick="supp_login3()"><?php echo 'Login'; ?></a>
                </div>
            </div>
        </div>
        <div id="supp_other3">
            <div class="row">
                <div class="col-xs-3 form-group" style="padding:0 10px">
                    <select class="form-control s_civility3" style="height: 35px !important;color: #000;" name="civility" <?php echo $c_user->civility!=''?'readonly':''; ?> required>
                        <?php foreach (config_model::$civility as $key => $civil): ?>
                            <option value="<?= $civil ?>" <?php echo $c_user->civility==$civil?'selected':''; ?>><?= $civil ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class=" form-group col-xs-4">
                    <input type="text" maxlength="100" class="form-control s_firstname3" required name="first_name" placeholder="Nom*" value="<?php echo $c_user->first_name; ?>" <?php echo $c_user->first_name!=''?'readonly':''; ?>>
                </div>
                <div class="form-group col-xs-4">
                    <input type="text" maxlength="100" class="form-control s_lastname3" required name="last_name" placeholder="Prenom*" value="<?php echo $c_user->last_name; ?>" <?php echo $c_user->last_name!=''?'readonly':''; ?>>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                            <input id="tel" maxlength="50" type="text" class="form-control s_phone3" required name="telephone" placeholder="Telephone*" value="<?php echo $c_user->phone; ?>" <?php echo $c_user->phone!=''?'readonly':''; ?>>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class=" form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input id="email" maxlength="100" type="email" class="form-control s_email3" required name="email" placeholder="Email*" value="<?php echo $c_user->email; ?>" <?php echo $c_user->email!=''?'readonly':''; ?>>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <input type="text" name="company_name" class="user-name form-control s_company3" placeholder="Entreprise" value="<?php echo $c_user->company_name; ?>" <?php echo $c_user->company_name!=''?'readonly':''; ?>>
                    </div>
                </div>
                <div class="col-xs-6">
                    <input type="text" class="phone1 form-control" name="mobile" maxlength="11" placeholder="Mobile" value="">
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <select class="form-control" style="height: 35px !important;color: #000;" name="department">
                            <option value="">-- Department* --</option>
                            <option value="10">Booking service</option>
                            <option value="11">Clients Service</option>
                            <option value="12">Driver Service</option>
                            <option value="13">Accounting Service</option>
                            <option value="14">Sales Department</option>
                            <option value="15">Technical Service</option>
                            <option value="16">Disclaimer Service</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <select class="form-control" style="height: 35px !important;color: #000;" name="priority">
                            <option value="">-- Priorité* --</option>
                            <option value="High">High</option>
                            <option value="Medium">Medium</option>
                            <option value="Low">Low</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="text" name="subject" class="user-name form-control" placeholder="Matière" value="" required>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <textarea class="message form-control" name="visit_message" placeholder="Votre message" rows="25" required></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label> Télécharger </label>
                        <input class="form-control" type="file" name="attachments[]" multiple placeholder="Télécharger" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12" style="text-align: center">
                    <button class="btn-successs" type="submit"><?php echo $this->lang->line('contact_us_button'); ?></button>
                </div>
            </div>
        </div>
        <?= form_close(); ?>
        <?php   } ?>
    </div>
    <?php }  ?>

    <?php if ($data->status5 == 1) { ?>
        <!-- Cookie Form -->
        <div class="cookie-form <?= $data->auto_open5 == 1 ? "visiable" : "" ?>"  id="cookie_form">
            <div class="contact-opener-5">
                <?php echo $data->name5 ?>
            </div>

            <div class="row">
                <!--<a href="javascript:;" class="close-btn-5"><i class="fa fa-times-circle-o"></i></a> -->
                <div class="col-md-12">
                    <div id="supp_other2">
                        <a href="javascript:;" class="close-btn-5">OK</a>
                        <?php if (trim($data->success_message5) != "") { ?>
                            <?php echo $data->success_message5; ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    <?php }  ?>
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?php echo $this->lang->line('login_forgot_password'); ?></h4>
                </div>
                <div class="modal-body" style="display: inline-flex;">
                    <div class="col-md-12"><!--col-md-offset-3-->
                        <div id="forgot-password">

                            <div class="col-md-12 col-xs-12" style="display:none;"  id="f_alert-box">
                                <div class="alert "  id="f_alert">
                                    <strong id="f_type"></strong> <span id="f_message"></span>
                                    <button type="button" class="close" style="padding: 0" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-10 col-xs-10">
                                <div class="input-group input-group-lg in-ty" style="margin-top: 10px;">
                                    <div class='input-group'>
							<span class="input-group-btn">
								<button class="btn btn-default pwd-grp" type="button"><i class="fa fa-envelope"></i></button>
							</span>
                                        <input type="text" name="username" value="" id="f_email" class="" placeholder="Email:" style="height: 34px !important;border-radius: 0px 4px 4px 0px;">
                                    </div>
                                    <span class="error" id="f_email-err"></span>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-2" style="padding:10px 0px 0px 10px;">
                                <input type="submit" onclick="submitForgotForm()" class="button green_button" value="<?php echo $this->lang->line('submit'); ?>" style="height: 34px;padding: 0px 20px 0px 20px !important;"/>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <audio id="pop" style="visibility: hidden;height: 0px;width: 0px;">
        <source src="<?php echo base_url(); ?>assets/chat_audio_sound.mp3" type="audio/mpeg">
    </audio>

    <script type="text/javascript">
        function submitForgotForm()
        {
            var email = $('#f_email').val();

            if(email===''){
                $('#f_email-err').html('Email is required*');
            }else{
                if( !isValidEmailAddress( email ) ) {
                    $('#f_email-err').html('Incorrect email format.');
                }else{
                    $('#f_email-err').html('');
                    $.ajax({
                        url: '<?php echo base_url(); ?>auth/popup_forgot_submit',
                        method: 'get',
                        data: 'username='+email,
                        dataType: 'json',
                        success: function(res){
                            $('#f_email').val('');
                            $('#f_alert-box').show();
                            $('#f_alert').addClass(res.class);
                            $('#f_type').html(res.type);
                            $('#f_message').html(res.message);
                        }
                    });
                }
            }
        }

        function isValidEmailAddress(emailAddress) {
            var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
            return pattern.test(emailAddress);
        }

        function pop_job_user(user_type){
            if(user_type=='Registered'){
                $('#job_login1').slideDown();
                $('#job_other1').slideUp();
            }else{
                $('#job_login1').slideUp();
                $('#job_other1').slideDown();
            }
        }

        function pop_qoute_user(user_type){
            if(user_type=='Client'){
                $('#client_login1').slideDown();
                $('#client_other1').slideUp();
            }else{
                $('#client_login1').slideUp();
                $('#client_other1').slideDown();
            }
        }

        function pop_login_user1(user_type){
            if((user_type=='Jobseeker')||(user_type=='Partner')||(user_type=='Client')||(user_type=='Driver')){
                $('#supp_login1').slideDown();
                $('#supp_other1').slideUp();
            }
            if(user_type=='Other'){
                $('#supp_login1').slideUp();
                $('#supp_other1').slideDown();
            }
        }

        function pop_login_user2(user_type){
            if((user_type=='Jobseeker')||(user_type=='Partner')||(user_type=='Client')||(user_type=='Driver')){
                $('#supp_login2').slideDown();
                $('#supp_other2').slideUp();
            }
            if(user_type=='Other'){
                $('#supp_login2').slideUp();
                $('#supp_other2').slideDown();
            }
        }

        function pop_login_user3(user_type){
            if((user_type=='Jobseeker')||(user_type=='Partner')||(user_type=='Client')||(user_type=='Driver')){
                $('#supp_login3').slideDown();
                $('#supp_other3').slideUp();
            }
            if(user_type=='Other'){
                $('#supp_login3').slideUp();
                $('#supp_other3').slideDown();
            }
        }

        function supp_login1(){
            var username = $('#username1').val();
            var password = $('#password1').val();
            var remember_me = $('input[name="remember_me1"]').val();

            if(username==''){
                $('#err-username1').show();
            }else{
                $('#err-username1').hide();
            }
            if(password==''){
                $('#err-password1').show();
            }else{
                $('#err-password1').hide();
            }

            if((username!='')&&(password!='')){
                var usertype = $('select[name="pop_user_type1"]').val();
                var ajaxurl = '';
                if(usertype=='Client'){
                    ajaxurl = '<?php echo base_url(); ?>client/popup_login';
                }else if(usertype=='Driver'){
                    ajaxurl = '<?php echo base_url(); ?>driver/popup_login';
                }else if(usertype=='Partner'){
                    ajaxurl = '<?php echo base_url(); ?>partner/popup_login';
                }else if(usertype=='Jobseeker'){
                    ajaxurl = '<?php echo base_url(); ?>job/popup_login';
                }
                $.ajax({
                    url: ajaxurl,
                    method: 'get',
                    data: 'username='+username+'&password='+password+'&remember_me='+remember_me,
                    dataType: 'json',
                    success: function(res){
                        if(res.type=='Error'){
                            alert(res.message);
                        }else{
                            var civility = res.civility;
                            var fname = res.first_name;
                            var lname = res.last_name;
                            var company = res.company_name;
                            var email = res.email;
                            var phone = res.phone;

                            if(civility!=''){
                                $('.s_civility1').val(civility);
                                $('.s_civility1').attr('readonly',true);
                            }
                            if(fname!=''){
                                $('.s_firstname1').val(fname);
                                $('.s_firstname1').attr('readonly',true);
                            }
                            if(lname!=''){
                                $('.s_lastname1').val(lname);
                                $('.s_lastname1').attr('readonly',true);
                            }
                            if(company!=''){
                                $('.s_company1').val(company);
                                $('.s_company1').attr('readonly',true);
                            }
                            if(email!=''){
                                $('.s_email1').val(email);
                                $('.s_email1').attr('readonly',true);
                            }
                            if(phone!=''){
                                $('.s_phone1').val(phone);
                                $('.s_phone1').attr('readonly',true);
                            }
                            $('select[name="pop_user_type1"]').hide();
                            $('#supp_login1').slideUp();
                            $('#supp_other1').slideDown();
                        }
                    }
                });
            }
        }

        function supp_login2(){
            var username = $('#username2').val();
            var password = $('#password2').val();
            var remember_me = $('input[name="remember_me4"]').val();

            if(username==''){
                $('#err-username2').show();
            }else{
                $('#err-username2').hide();
            }
            if(password==''){
                $('#err-password2').show();
            }else{
                $('#err-password2').hide();
            }

            if((username!='')&&(password!='')){
                var usertype = $('select[name="pop_user_type2"]').val();
                var ajaxurl = '';
                if(usertype=='Client'){
                    ajaxurl = '<?php echo base_url(); ?>client/popup_login';
                }else if(usertype=='Driver'){
                    ajaxurl = '<?php echo base_url(); ?>driver/popup_login';
                }else if(usertype=='Partner'){
                    ajaxurl = '<?php echo base_url(); ?>partner/popup_login';
                }else if(usertype=='Jobseeker'){
                    ajaxurl = '<?php echo base_url(); ?>job/popup_login';
                }
                $.ajax({
                    url: ajaxurl,
                    method: 'get',
                    data: 'username='+username+'&password='+password+'&remember_me='+remember_me,
                    dataType: 'json',
                    success: function(res){
                        if(res.type=='Error'){
                            alert(res.message);
                        }else{
                            var civility = res.civility;
                            var fname = res.first_name;
                            var lname = res.last_name;
                            var company = res.company_name;
                            var email = res.email;
                            var phone = res.phone;

                            if(fname!=''){
                                $('.s_firstname2').val(fname);
                                $('.s_firstname2').attr('readonly',true);
                            }
                            if(lname!=''){
                                $('.s_lastname2').val(lname);
                                $('.s_lastname2').attr('readonly',true);
                            }
                            if(civility!=''){
                                $('.s_civility2').val(civility);
                                $('.s_civility2').attr('readonly',true);
                            }
                            if(company!=''){
                                $('.s_company2').val(company);
                                $('.s_company2').attr('readonly',true);
                            }
                            if(email!=''){
                                $('.s_email2').val(email);
                                $('.s_email2').attr('readonly',true);
                            }
                            if(phone!=''){
                                $('.s_phone2').val(phone);
                                $('.s_phone2').attr('readonly',true);
                            }
                            $('select[name="pop_user_type2"]').hide();
                            $('#supp_login2').slideUp();
                            $('#supp_other2').slideDown();
                        }
                    }
                });
            }
        }

        function supp_login3(){
            var username = $('#username3').val();
            var password = $('#password3').val();
            var remember_me = $('input[name="remember_me5"]').val();

            if(username==''){
                $('#err-username3').show();
            }else{
                $('#err-username3').hide();
            }
            if(password==''){
                $('#err-password3').show();
            }else{
                $('#err-password3').hide();
            }

            if((username!='')&&(password!='')){
                var usertype = $('select[name="pop_user_type3"]').val();
                var ajaxurl = '';
                if(usertype=='Client'){
                    ajaxurl = '<?php echo base_url(); ?>client/popup_login';
                }else if(usertype=='Driver'){
                    ajaxurl = '<?php echo base_url(); ?>driver/popup_login';
                }else if(usertype=='Partner'){
                    ajaxurl = '<?php echo base_url(); ?>partner/popup_login';
                }else if(usertype=='Jobseeker'){
                    ajaxurl = '<?php echo base_url(); ?>job/popup_login';
                }
                $.ajax({
                    url: ajaxurl,
                    method: 'get',
                    data: 'username='+username+'&password='+password+'&remember_me='+remember_me,
                    dataType: 'json',
                    success: function(res){
                        if(res.type=='Error'){
                            alert(res.message);
                        }else{
                            var civility = res.civility;
                            var fname = res.first_name;
                            var lname = res.last_name;
                            var company = res.company_name;
                            var email = res.email;
                            var phone = res.phone;

                            if(civility!=''){
                                $('.s_civility3').val(civility);
                                $('.s_civility3').attr('readonly',true);
                            }
                            if(fname!=''){
                                $('.s_firstname3').val(fname);
                                $('.s_firstname3').attr('readonly',true);
                            }
                            if(lname!=''){
                                $('.s_lastname3').val(lname);
                                $('.s_lastname3').attr('readonly',true);
                            }
                            if(company!=''){
                                $('.s_company3').val(company);
                                $('.s_company3').attr('readonly',true);
                            }
                            if(email!=''){
                                $('.s_email3').val(email);
                                $('.s_email3').attr('readonly',true);
                            }
                            if(phone!=''){
                                $('.s_phone3').val(phone);
                                $('.s_phone3').attr('readonly',true);
                            }
                            $('select[name="pop_user_type3"]').hide();
                            $('#supp_login3').slideUp();
                            $('#supp_other3').slideDown();
                        }
                    }
                });
            }
        }

        function job_login1(){
            var username = $('#j_username1').val();
            var password = $('#j_password1').val();
            var remember_me = $('input[name="remember_me2"]').val();

            if(username==''){
                $('#err-j_username1').show();
            }else{
                $('#err-j_username1').hide();
            }
            if(password==''){
                $('#err-j_password1').show();
            }else{
                $('#err-j_password1').hide();
            }

            if((username!='')&&(password!='')){
                $.ajax({
                    url: '<?php echo base_url(); ?>job/popup_login',
                    method: 'get',
                    data: 'username='+username+'&password='+password+'&remember_me='+remember_me,
                    dataType: 'json',
                    success: function(res){
                        if(res.type=='Error'){
                            alert(res.message);
                        }else{
                            var civility = res.civility;
                            var fname = res.first_name;
                            var lname = res.last_name;
                            var email = res.email;
                            var phone = res.phone;
                            var address = res.address;
                            var city = res.city;
                            var zipcode = res.zipcode;
                            var dob = res.dob;

                            if(address!=''){
                                $('.j_address').val(address);
                                $('.j_address').attr('readonly',true);
                            }
                            if(city!=''){
                                $('.j_city').val(city);
                                $('.j_city').attr('readonly',true);
                            }
                            if(zipcode!=''){
                                $('.j_zipcode').val(zipcode);
                                $('.j_zipcode').attr('readonly',true);
                            }
                            if(dob!=''){
                                $('.j_dob').val(dob);
                                $('.j_dob').attr('readonly',true);
                            }
                            if(civility!=''){
                                $('.j_civility').val(civility);
                                $('.j_civility').attr('readonly',true);
                            }
                            if(fname!=''){
                                $('.j_firstname').val(fname);
                                $('.j_firstname').attr('readonly',true);
                            }
                            if(lname!=''){
                                $('.j_lastname').val(lname);
                                $('.j_lastname').attr('readonly',true);
                            }
                            if(email!=''){
                                $('.j_email').val(email);
                                $('.j_email').attr('readonly',true);
                            }
                            if(phone!=''){
                                $('.j_phone').val(phone);
                                $('.j_phone').attr('readonly',true);
                            }
                            $('select[name="job_user"]').hide();
                            $('#job_login1').slideUp();
                            $('#job_other1').slideDown();
                        }
                    }
                });
            }
        }

        function client_login1(){
            var username = $('#c_username1').val();
            var password = $('#c_password1').val();
            var remember_me = $('input[name="remember_me3"]').val();

            if(username==''){
                $('#err-c_username1').show();
            }else{
                $('#err-c_username1').hide();
            }
            if(password==''){
                $('#err-c_password1').show();
            }else{
                $('#err-c_password1').hide();
            }

            if((username!='')&&(password!='')){
                $.ajax({
                    url: '<?php echo base_url(); ?>client/popup_login',
                    method: 'get',
                    data: 'username='+username+'&password='+password+'&remember_me='+remember_me,
                    dataType: 'json',
                    success: function(res){
                        if(res.type=='Error'){
                            alert(res.message);
                        }else{
                            var civility = res.civility;
                            var fname = res.first_name;
                            var lname = res.last_name;
                            var company = res.company_name;
                            var email = res.email;
                            var phone = res.phone;
                            var address = res.address;
                            var city = res.city;
                            var zipcode = res.zipcode;
                            var dob = res.dob;

                            if(address!=''){
                                $('.c_address').val(address);
                                $('.c_address').attr('readonly',true);
                            }
                            if(city!=''){
                                $('.c_city').val(city);
                                $('.c_city').attr('readonly',true);
                            }
                            if(zipcode!=''){
                                $('.c_zipcode').val(zipcode);
                                $('.c_zipcode').attr('readonly',true);
                            }
                            if(dob!=''){
                                $('.c_dob').val(dob);
                                $('.c_dob').attr('readonly',true);
                            }
                            if(civility!=''){
                                $('.c_civility').val(civility);
                                $('.c_civility').attr('readonly',true);
                            }
                            if(fname!=''){
                                $('.c_firstname').val(fname);
                                $('.c_firstname').attr('readonly',true);
                            }
                            if(lname!=''){
                                $('.c_lastname').val(lname);
                                $('.c_lastname').attr('readonly',true);
                            }
                            if(company!=''){
                                $('.c_company').val(company);
                                $('.c_company').attr('readonly',true);
                            }
                            if(email!=''){
                                $('.c_email').val(email);
                                $('.c_email').attr('readonly',true);
                            }
                            if(phone!=''){
                                $('.c_phone').val(phone);
                                $('.c_phone').attr('readonly',true);
                            }
                            $('select[name="quote_user"]').hide();
                            $('#client_login1').slideUp();
                            $('#client_other1').slideDown();
                        }
                    }
                });
            }
        }

        function updatemessagesoundforuser(userid) {
            // var userid = $('#chatuserid').val();
            $.ajax({
                url: '<?php echo base_url(); ?>Messages/updatemessagesoundforuserdatadiv',
                method: 'get',
                data: 'userid=' + userid,
                success: function (result)
                {
                    console.log("Chat sound updated!");
                }
            });
        }
        function getvisitorsmessages() {
            var userid = $('#chatuserid').val();
            $.ajax({
                url: '<?php echo base_url(); ?>Messages/getvisitorschathistory',
                method: 'get',
                data: 'userid=' + userid,
                success: function (result)
                {
                    $('#historychating').html(result);
                    var elem = document.getElementById('historychating');
                    elem.scrollTop = elem.scrollHeight;
                    setTimeout(getvisitorsmessages.bind(null, userid), 4000);
                    updatemessagesoundforuser(userid);
                }
            });
        }
        var userid = $('#chatuserid').val();
        getvisitorsmessages(userid);
    </script>
    <script src="<?php echo base_url(); ?>/assets/system_design/scripts/modals_script.js" type="text/javascript"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script> -->

    <?php if(!empty($google_api_info) && $google_api_info->status == 1 && $google_api_info->permission->front->status == 1){?>
        <script>
            function setModalLocation(m_autocomplete, y) {

                var place = m_autocomplete.getPlace();
                //console.log("#" + y)
                //console.log($("#" + y))
                var parentID = $(document.body).find("#" + y).data('parent');
                //console.log("oye " + parentID);

                $("#" + parentID).find("input[name='ville']").val("");
                $("#" + parentID).find("input[name='postal_code']").val("");
                $("#" + parentID).find("input[name='latitude']").val("");
                $("#" + parentID).find("input[name='longitude']").val("");

                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    //console.log(addressType);
                    if (addressType == "locality") {
                        console.log(addressType + " locality");
                        val = place.address_components[i]['long_name'];
                        $("#" + parentID).find("input[name='ville']").val(val);

                    }

                    if(addressType=="postal_code"){
                        //console.log(addressType + " postal code");
                        val = place.address_components[i]['short_name'];
                        $("#" + parentID).find("input[name='postal_code']").val(val);
                    }
                }

                $("#" + parentID).find("input[name='latitude']").val(place.geometry.location.lat());
                $("#" + parentID).find("input[name='longitude']").val(place.geometry.location.lng());
            }

            var contact_autocomplete, job_autocomplete = false;
            function initAutocompleteModel() {
                if($("#contact_autocomplete").length > 0) {
                    //console.log("reached in contact", $("#contact_autocomplete").length);
                    contact_autocomplete = new google.maps.places.Autocomplete(document.getElementById("contact_autocomplete"), {
                        types: ['geocode']
                        <?php if(!empty($google_api_info->country_code)){?>
                        , componentRestrictions: {country: "<?=$google_api_info->country_code?>"}
                        <?php } ?>
                    });
                    contact_autocomplete.setFields(['address_component', 'geometry']);
                    contact_autocomplete.addListener('place_changed', function () {
                        setModalLocation(contact_autocomplete, "contact_autocomplete");
                    });
                }

                if($("#job_autocomplete").length > 0) {
                    //console.log("reached in job", $("#job_autocomplete").length);
                    job_autocomplete = new google.maps.places.Autocomplete(document.getElementById("job_autocomplete"), {
                        types: ['geocode']
                        <?php if(!empty($google_api_info->country_code)){?>
                        , componentRestrictions: {country: "<?=$google_api_info->country_code?>"}
                        <?php } ?>
                    });
                    job_autocomplete.setFields(['address_component', 'geometry']);
                    job_autocomplete.addListener('place_changed', function () {
                        setModalLocation(job_autocomplete, "job_autocomplete");
                    });
                }
            }

            google.maps.event.addDomListener(window, "load", initAutocompleteModel);

            function geolocate_model(type) {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        var geolocation = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        var circle = new google.maps.Circle({
                            center: geolocation,
                            radius: position.coords.accuracy
                        });

                        var GEOCODING = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + geolocation.lat + '%2C' + geolocation.lng + '&language=en&key=<?=$google_api_info->api_key?>';

                        console.log(GEOCODING);
                        if (type == "contact") {
                            $.getJSON(GEOCODING).done(function(location) {
                                console.log(location);
                                setModalCurrentLocation("contact_autocomplete",location);
                            });
                            contact_autocomplete.setBounds(circle.getBounds());
                        }

                        if (type == "job") {
                            $.getJSON(GEOCODING).done(function(location) {
                                setModalCurrentLocation("job_autocomplete",location);
                            });
                            job_autocomplete.setBounds(circle.getBounds());
                        }
                    });
                }
            }


            function setModalCurrentLocation(y, location) {

                $('#' + y).val(location.results[0].formatted_address);
                var parentID = $(document.body).find("#" + y).data('parent');

                $("#" + parentID).find("input[name='ville']").val("");
                $("#" + parentID).find("input[name='postal_code']").val("");
                $("#" + parentID).find("input[name='latitude']").val("");
                $("#" + parentID).find("input[name='longitude']").val("");

                for (var i = 0; i < location.results.length; i++) {
                    var addressType = location.results[i].types[0];
                    //console.log(addressType);
                    if (addressType == "locality") {
                        console.log(addressType + " locality");
                        val = location.results[i].address_components[0]['long_name'];
                        $("#" + parentID).find("input[name='ville']").val(val);

                    }

                    if(addressType=="postal_code"){
                        //console.log(addressType + " postal code");
                        val = location.results[i].address_components[0]['short_name'];
                        $("#" + parentID).find("input[name='postal_code']").val(val);
                    }
                }

                $("#" + parentID).find("input[name='latitude']").val(location.results[0].geometry.location.lat);
                $("#" + parentID).find("input[name='longitude']").val(location.results[0].geometry.location.lng);
            }

        </script>
    <?php } ?>
    <script src='<?php echo base_url(); ?>emojis_assets/front_end_emoji_assets/inputEmoji.js'></script>
    <script type="text/javascript">
        $('#input-left-position').emoji({place: 'after'});
    </script>
    <script type="text/javascript">

        function validateinputdata()
        {
            var ipinfo;
            var name = $('#name').val();
            var email = $('#email2').val();
            var telephone = $('#telephone').val();

            if (name == "" || name == " " || email == "" || email == " " || telephone == "" || telephone == " ")
            {
                $('#name').css('border', '2px solid red');
                $('#email2').css('border', '2px solid red');
                $('#telephone').css('border', '2px solid red');
            } else
            {
                $.ajax({
                    url: '<?php echo base_url(); ?>Messages/insertchatdata',
                    method: 'get',
                    async: false,
                    data: 'username=' + name + '&email=' + email + '&telephone=' + telephone,

                    beforeSend: function ()
                    {
                        $('.chat_loader').fadeIn('slow');
                    },
                    success: function (data)
                    {
                        var obj = JSON.parse(data);
                        console.log(obj);
                        if (obj.status == 'false')
                        {
                            $('#chatuserid').val(obj.userid);
                        } else
                        {
                            $('#chatuserid').val(obj.userid);
                            $('#historychating').append(obj.message_history);
                            var elem = document.getElementById('historychating');
                            elem.scrollTop = elem.scrollHeight;
                        }
                        $('.chat_loader').fadeOut('slow');
                        $('.basic-chat-detail').css('display', 'none');
                        $('.start-chat-div').fadeIn('slow');
                    },
                    error: function (data) {
                        alert('ERROR: ' + data.status + ' :: ' + data.statusText);
                    }
                });

            }
        }

        $('#the-chat-form').on('submit', function (e)
        {
            e.preventDefault();
            var messagetext = $('#input-left-position').val();
            var userid = $('#chatuserid').val();
            // if(messagetext=="" || messagetext==" ")
            // {
            //     $('#input-left-position').css('border','1px solid red');
            //     return;
            // }
            // else
            // {

            var formData = new FormData(this);
            $('#input-left-position').css('border', '');
            $.ajax({
                url: '<?php echo base_url(); ?>Messages/insertchatmessagedata',
                method: 'post',
                data: formData,

                cache: false,

                contentType: false,

                processData: false,
                beforeSend: function ()
                {
                    $('#btn_loader').fadeIn('slow');
                },
                success: function (data)
                {
                    // alert(data);return;
                    $('#btn_loader').fadeOut('slow');
                    $('#historychating').append(data);
                    $('#input-left-position').val('');
                    $('#changeattachfileimage').css('display', 'none');
                    $('#input-left-position').attr("required", true);
                    var elem = document.getElementById('historychating');
                    elem.scrollTop = elem.scrollHeight;
                },
                error: function (data) {
                    alert('ERROR: ' + data.status + ' :: ' + data.statusText);
                }
            });
            // }
        });



        function addchatfile()

        {

            $('#chatfile').click();

        }

        function addattachfileto_div() {
            $('#attachfile_div').css({'display': 'block'});
            var attachfilename_val = $('#chatfile').val();
            var extension = attachfilename_val.split('.').pop();

            // alert(extension);return;
            $('#changeattachfileimage').css('display', 'block');
            if (extension == "png" || extension == "PNG" || extension == "jpg" || extension == "jpeg" || extension == "gif") {

                var oFReader = new FileReader();

                oFReader.readAsDataURL(document.getElementById("chatfile").files[0]);

                oFReader.onload = function (oFREvent) {

                    document.getElementById("changeattachfileimage").src = oFREvent.target.result;

                }

            } else

            {

                $('#changeattachfileimage').attr('src', '<?php echo base_url(); ?>assets/chat_files/file_upload_image.png');

            }

            var attachfilename_vall = attachfilename_val.substring(attachfilename_val.lastIndexOf("\\") + 1, attachfilename_val.length);

            $('#attachfilename').text(attachfilename_vall);

            $('#input-left-position').attr("required", false);

        }

        function openemojis() {
            $('.input_area_emoji').find('div').show();
        }
        // window.setInterval(function() {
        var elem = document.getElementById('historychating');
        elem.scrollTop = elem.scrollHeight;
        // }, 2000);
    </script>