<link href="<?php echo base_url(); ?>assets/system_design/css/login.css" rel="stylesheet">
<script type="text/javascript">
    (function ($, W, D)
    {
        var JQUERY4U = {};

        JQUERY4U.UTIL =
                {
                    setupFormValidation: function ()
                    {
                        //Additional Methods            
                        $.validator.addMethod("lettersonly", function (a, b) {
                            return this.optional(b) || /^[a-z ]+$/i.test(a)
                        }, "<?php echo $this->lang->line('valid_name'); ?>");

                        $.validator.addMethod("phoneNumber", function (uid, element) {
                            return (this.optional(element) || uid.match(/^([0-9]*)$/));
                        }, "<?php echo $this->lang->line('valid_phone_number'); ?>");


                        $.validator.addMethod("pwdmatch", function (repwd, element) {
                            var pwd = $('#password').val();
                            return (this.optional(element) || repwd == pwd);
                        }, "<?php echo $this->lang->line('valid_passwords'); ?>");


                        //form validation rules
                        $("#driver_signupform").validate({
                            rules: {
                                statut: {
                                    required: true
                                },
                                company: {
                                    required: function () {
                                        return $("#statut").val() == "2";
                                    }
                                },
                                first_name: {
                                    required: true,
                                    lettersonly: true
                                },
                                email: {
                                    required: true,
                                    email: true
                                },
                                phone: {
                                    required: true,
                                    phoneNumber: true,
                                    rangelength: [10, 11]
                                },
                                password: {
                                    required: true,
                                    rangelength: [8, 30]
                                },
                                confirm_password: {
                                    required: true,
                                    pwdmatch: true
                                }
                            },
                            messages: {
                                statut: {
                                    required: "Please select the statut"
                                },
                                company: {
                                    required: "Please enter the company"
                                },
                                first_name: {
                                    required: "<?php echo $this->lang->line('first_name_valid'); ?>"
                                },
                                email: {
                                    required: "<?php echo $this->lang->line('email_valid'); ?>"
                                },
                                phone: {
                                    required: "<?php echo $this->lang->line('phone_valid'); ?>"
                                },
                                password: {
                                    required: "<?php echo $this->lang->line('password_valid'); ?>"
                                },
                                password_confirm: {
                                    required: "<?php echo $this->lang->line('confirm_password_valid'); ?>"
                                }
                            },
                            submitHandler: function (form) {
                                form.submit();
                            }
                        });
                    }
                }

        //when the dom has loaded setup form validation rules
        $(D).ready(function ($) {
            JQUERY4U.UTIL.setupFormValidation();
        });

    })(jQuery, window, document);
</script>
</header>
<style>
    select {margin-top:0px;}
    .slider_wrapper .bx-wrapper .bx-pager {top:inherit;bottom: 0px;}
    select.cust_width1 {width:177px !important;margin-left:-17px !important;}
    select.cust_width2 {width:162px !important;margin-left:-5px !important;}
    select.statut {padding: 6px 10px;width: 127px;}
    .register-box textarea {height:auto !important;}
    input.green_button {top:25px;position:relative;}
    .register-box label {display:block !important;margin-bottom:0px !important;}
    .register-box .form-group {margin-bottom:5px !important;}
    .register-box .civility {width: 65px !important;padding: 5px !important;}
</style>
<?php switch (strtolower($active_class)){
    case "affiliate":
        $link = "affiliate";
        break;
    case "partner":
        $link = "partner";
        break;
    case "driver":
        $link = "driver";
        break;
    default:
        $link = "blog";
        break;
}?>
<div class="container body-border newsdetail_page"> 
    <div class="breadcrumb">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <aside class="nav-links">
                    <ul>
                        <li><a href="<?php echo site_url(); ?>/"><i class="fa fa-home"></i>  <?php echo $this->lang->line('home_page'); ?> </a></li>
                        <li class="active"><a href="<?php echo site_url($link); ?>"><?php echo $title ?> </a></li>
                        <?php if(!empty($details->category_name)){?>
                        <li class="active"><a href="javascript:void(0)"><?php echo $details->category_name ?> </a></li>
                        <?php } ?>
                        <li class="active"><a href="<?php echo base_url("$link/{$details->category_link}/{$details->link_url}{$details->link}"); ?>"><?php echo $details->title ?> </a></li>
                    </ul>
                </aside>
            </div>
        </div> 
    </div>
    <div class="clearfix"></div> 
    <div id="services-section" class="body-bg">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-xs-12">
                <img src="<?php echo base_url(); ?>uploads/cms/<?php echo $details->image; ?>" class="news_image img-responsive">
            </div>
            <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="detail-block">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12"> <?php //echo "<PRE>";print_r($job);print_r($details);echo "</PRE>";?>
                            <div class="dttime" style="font-weight:700;font-size: 14px;float:left;">
                                <?php $addCategoryDots = strlen($details->category_name) > 12 ? "..." : ""; ?>
                                Category: <span class="mr15"><strong><?= substr($details->category_name, 0, 12) . $addCategoryDots ;?> </strong></span>
                            </div> 
                            <div class="dttime" style="float:right;">
                                <span class="lbl-text">Publish Date:</span> &nbsp;<?= ($details->created_date != "") ? date('d/m/Y H:i',strtotime($details->created_date)) : ''; ?>
                                <span class="lbl-text">Author:</span> &nbsp;<?= $author->civility . ' ' .$author->first_name . ' ' . $author->last_name; ?> 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <h2><?php echo $details->title; ?></h2>
                        </div>
                    </div>
                    <ul class="news_info">
                        <li class=""><div class="pull-left"></div></li>
                        <li class=""><div class="pull-left"></div></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class=" tab-content">
                    <?php echo $details->description; ?>
                </div>
            </div>
        </div>
    </div>
</div>
