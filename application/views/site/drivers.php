<link href="<?php echo base_url(); ?>assets/system_design/css/login.css" rel="stylesheet">
<script type="text/javascript"> 
 (function($,W,D)
 {
    var JQUERY4U = {};
 
    JQUERY4U.UTIL =
    {
        setupFormValidation: function()
        {
            //Additional Methods            
        $.validator.addMethod("lettersonly",function(a,b){return this.optional(b)||/^[a-z ]+$/i.test(a)},"<?php echo $this->lang->line('valid_name');?>");
 
        $.validator.addMethod("phoneNumber", function(uid, element) {
            return (this.optional(element) || uid.match(/^([0-9]*)$/));
        },"<?php echo $this->lang->line('valid_phone_number');?>");
        
 
        $.validator.addMethod("pwdmatch", function(repwd, element) {
            var pwd= $('#password').val();
            return (this.optional(element) || repwd==pwd);
        },"<?php echo $this->lang->line('valid_passwords');?>");
         
        
        //form validation rules
        $("#driver_signupform").validate({
                rules: {
                    statut: {
                        required:true
                    },
                    company: {
                        required:  function(){
                                        return $("#statut").val() == "2";
                                  }
                    },
                    first_name: {
                        required: true,
                        lettersonly: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    phone:{
                            required: true,
                            phoneNumber: true,
                            rangelength: [10, 11]
                    },
                    password:{
                            required: true,
                            rangelength: [8, 30]
                    },
                    confirm_password: {
                        required:true,
                        pwdmatch:true
                    }
                },
                messages: {
                    statut: {
                        required:"Please select the statut"
                    },
                    company: {
                        required:"Please enter the company"
                    },
                    first_name: {
                        required: "<?php echo $this->lang->line('first_name_valid');?>"
                    },
                    email:{
                        required: "<?php echo $this->lang->line('email_valid');?>"
                    },
                    phone:{
                            required: "<?php echo $this->lang->line('phone_valid');?>"
                    },
                    password: {
                        required: "<?php echo $this->lang->line('password_valid');?>"
                    },
                    password_confirm:{
                            required: "<?php echo $this->lang->line('confirm_password_valid');?>"
                    }
                }, 
                submitHandler: function(form) {
                    form.submit();
                }
            });
        }
    }
 
    //when the dom has loaded setup form validation rules
    $(D).ready(function($) {
        JQUERY4U.UTIL.setupFormValidation();
    });
     
 })(jQuery, window, document);
</script>
</header>
<style>
    select {margin-top:0px;}
    .slider_wrapper .bx-wrapper .bx-pager {top:inherit;bottom: 0px;}
    select.cust_width1 {width:177px !important;margin-left:-17px !important;}
    select.cust_width2 {width:162px !important;margin-left:-5px !important;}
    select.statut {padding: 6px 10px;width: 127px;}
    .register-box textarea {height:auto !important;}
    input.green_button {top:25px;position:relative;}
    .register-box label {display:block !important;margin-bottom:0px !important;}
    .register-box .form-group {margin-bottom:5px !important;}
    .register-box .civility {width: 65px !important;padding: 5px !important;}
</style>
<div class="slider_wrapper " id="sliderWrapper"> <!--slider_wrapper_1-->
    <div class="container content-border">
        <div class="row"> 
            <ul class="homepageSlider">


     
                <?php foreach($slider as $item):?>

                    <li>

                        <?php if(!empty($item->left_icon)):?>
                            <div class="left_icon"><img src="<?php echo base_url(); ?>uploads/cms/slider/<?=$item->left_icon;?>"></div>
                        <?php endif;?>

                        <?php if(!empty($item->right_icon)):?>
                            <div class="right_icon pull-right"><img src="<?php echo base_url(); ?>uploads/cms/slider/<?=$item->right_icon;?>"></div>
                        <?php endif;?>
                        <img src="<?php echo base_url(); ?>uploads/cms/slider/<?=$item->img;?>" title="Visite Guidee Paris" alt="Visite Guidee Paris" style="height:360px" />
                        <div class="slider_footer_text col-md-12" style="margin-top: -10%;">
                            <span><?php echo $item->title;?></span>
                            <p><?php echo $item->subtitle;?></p>
                        </div>

                    </li>
                <?php endforeach;?>


<!-- 
                <li>
                    <img src="<?php echo base_url(); ?>assets/system_design/images/new_footer_header_images/slide1.jpg" title="Visite Guidee Paris" alt="Visite Guidee Paris" style="height:360px" />
                </li>
                <li>
                    <img src="<?php echo base_url(); ?>assets/system_design/images/new_footer_header_images/slide2.jpg" title="Gare De Lyon" alt="Gare De Lyon" style="height:360px" />
                </li>
                <li>
                    <img src="<?php echo base_url(); ?>assets/system_design/images/new_footer_header_images/slide3.jpg" title="Gare Saint Lazare" alt="Gare Saint Lazare" style="height:360px" />
                </li>
                <li>
                    <img src="<?php echo base_url(); ?>assets/system_design/images/new_footer_header_images/slide4.jpg" title="Aeroport D'Orly" alt="Aeroport D'Orly" style="height:360px" />
                </li>
                <li>
                    <img src="<?php echo base_url(); ?>assets/system_design/images/new_footer_header_images/slide5.jpg" title="Aeroport De Roissy CDG" alt="Aeroport De Roissy CDG" style="height:360px" />
                </li>
                <li>
                    <img src="<?php echo base_url(); ?>assets/system_design/images/new_footer_header_images/slide6.jpg" title="Aeroport Beauvais" alt="Aeroport Beauvais" style="height:360px" />
                </li>
                <li>
                    <img src="<?php echo base_url(); ?>assets/system_design/images/new_footer_header_images/slide7.jpg" title="Aeroport Beauvais" alt="Aeroport Beauvais" style="height:360px" />
                </li>  -->
            </ul> 
        </div> 
    </div>
</div>
<div class="container" style="position: relative;">
    <div class="register-box">
        <div class="booking-title booking-title-bg-red">Become Cab Driver Right now</div>
        <?php echo form_open('driver', array("id" => "driver_signupform")); ?>
        <div class=""> 
            <div class="col-md-6 form-group">
                <label>Statut</label>
                <select class="form-control" name="statut" id="statut">
                    <option value="1">Independent</option>
                    <option value="2">Company</option>
                </select>                                    
            </div>
            <div class="col-md-6 form-group"> 
                <div class="divCompany" style="display:none;">
                   <label>Company</label>
                    <?php echo form_input($company); ?>
                    <?php echo form_error('company'); ?>
                </div>                                
            </div>
        </div>
        <div class="clearfix"></div>
        <div class=""> 
            <div class="col-md-2 form-group">
                <label>Civility</label>
                <select name="civility" class="form-control civility">
                    <option value="0">Mr</option>
                    <option value="1">Miss</option>
                    <option value="2">Mrs</option>
                </select>                                   
            </div>
            <div class="col-md-5 form-group">
                <label>First Name</label>
                <?php echo form_input($first_name); ?>
                <?php echo form_error('first_name'); ?>                                    
            </div>
            <div class="col-md-5 form-group">
                <label>Last Name</label>
                <?php echo form_input($last_name); ?>
                <?php echo form_error('last_name'); ?>                                    
            </div>
        </div>
        <div class="clearfix"></div>
        <div>
            <div class="col-md-6 form-group">
                <label>Email</label>
                <?php echo form_input($email); ?>
                <?php echo form_error('email'); ?>                                
            </div>
            <div class="col-md-6 form-group">
                <label>Phone</label>
                <?php echo form_input($phone); ?>
                <?php echo form_error('phone'); ?>                               
            </div> 
        </div>
        <div class="clearfix"></div>
        <div>
            <div class="col-md-6 form-group">
                <label>Address</label>
                <?php echo form_textarea($address); ?>
                <?php echo form_error('address'); ?>                          
            </div> 
            <div class="col-md-6 form-group">
                <label>&nbsp;</label>
                <?php echo form_textarea($address1); ?>
                <?php echo form_error('address1'); ?>                                    
            </div> 
        </div>
        <div class="clearfix"></div>
        <div>
            <div class="col-md-6 ">
                <label>City</label>
                <?php echo form_input($city); ?>
                <?php echo form_error('city'); ?>                                     
            </div>
            <div class="col-md-6 ">
                <label>Zip Code</label>
                <?php echo form_input($zipcode); ?>
                <?php echo form_error('zipcode'); ?>                                   
            </div>
        </div>
        <div class="clearfix"></div>
        <div>
            <div class="col-md-6 form-group"> 
                <label>Password</label>
                <?php echo form_input($password); ?>
                <?php echo form_error('password'); ?> 
            </div> 
            <div class="col-md-6 form-group"> 
                <label>Confirm Password</label>
                <?php echo form_input($confirm_password); ?>
                <?php echo form_error('confirm_password'); ?> 
            </div>  
        </div>
        <div class="clearfix"></div> 
        <div class="bk-row">
            <div class="col-md-12">
                <div class="form-group" style="text-align:left;">
                    <span class="tos_conditions"><input type="checkbox" name="tos_conditions">Accept <a href="http://www.cab-booking-script.com/demo/termsServices" target="_blank"> Terms and Conditions</a></span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group" style="text-align: right;">

                    <input type="submit" class="button green_button" value="Registe Now">

                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<div class="booking-process">
    <div class="container">
        <div class="row">
            <div class="process-box">
                <div class="col-md-3 easy-steps">
                    <div class="img-holder">
                        <div class="img-container">
                            <div class="numbering">1</div>
                            <span class="process-text">Signup</span>
                            <i class="fa fa-sign-in"></i> 
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-3 easy-steps">
                    <div class="img-holder">
                        <div class="img-container">
                            <div class="numbering">2</div>
                            <span class="process-text">Upload Documents</span>
                            <i class="fa fa-upload"></i> 
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-3 easy-steps">
                    <div class="img-holder">
                        <div class="img-container">
                            <div class="numbering">3</div>
                            <span class="process-text">Get Approved</span>
                            <i class="fa fa-check"></i> 
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-3 easy-steps">
                    <div class="img-holder">
                        <div class="img-container">
                            <div class="numbering">4</div>
                            <span class="process-text">Start Driving</span>
                            <i class="fa fa-taxi"></i> 
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container body-border"> 
    <div class="breadcrumb">
        <div class="row">
            <aside class="nav-links">
                <ul>
                    <li><a href="<?php echo site_url(); ?>/"><i class="fa fa-home"></i>  <?php echo $this->lang->line('home_page'); ?> </a></li>
                    <li class="active"><a href="javascript:void(0)"><?php echo $title ?> </a></li>
                </ul>
            </aside>
        </div> 
    </div>
    <div class="clearfix"></div> 
    <div id="services-section" class="body-bg">
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box_head">
                        <h3>Transfert Aeroport D'Orly</h3>
                    </div> 
                    <div class="plans_img"><img src="http://navetteo.fr/uploads/service_images/3_aeroport-orly.png" alt="Peugeot 508 - Peugeot 508 " title="Peugeot 508 - Peugeot 508 "></div>
                    <div class="description">
                        La liaison entre Paris et l'aéroport d'Orly est souvent très difficile. Transports en commun surchargés, longues files d'attente pour prendre un taxi.                                         </div>
                    <div class="button-row">
                        <div class="read-more" ><a href="<?php echo base_url(); ?>services" class="button light_green_button">Know more</a></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box_head">
                        <h3>Transfert Aeroport CDG</h3></div>
                    <div class="plans_img"><img src="http://navetteo.fr/uploads/service_images/4_aeroport-de-roissy.png" alt="Peugeot 508 - Peugeot 508 " title="Peugeot 508 - Peugeot 508 "></div>
                    <div class="description">
                        Vous cherchez un transport pour vous rendre Ã L'aeroport? ou qu'on vous recupere de L'aeroport ?                                        </div>
                    <div class="button-row">
                        <div class="read-more" ><a href="<?php echo base_url(); ?>services" class="button light_green_button">Know more</a></div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box_head">
                        <h3>Transfert Aeroport Beauvais</h3>
                    </div>
                        <div class="plans_img"><img src="http://navetteo.fr/uploads/service_images/5_aeroport-beauvais.png" alt="Peugeot 508 - Peugeot 508 " title="Peugeot 508 - Peugeot 508 "></div>
                    <div class="description">
                        Navetteo est le meilleur site de transport de personne qui soit si vous avez besoin si vous avez besoin d'un transfert aéroport pour l'aéroport de Beauvais.                                        </div>
                    <div class="button-row">
                        <div class="read-more" ><a href="<?php echo base_url(); ?>services" class="button light_green_button">Know more</a></div>


                        <!-- Here Button Was Removed -->
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box_head">
                        <h3>Transfert Aeroport Le Bourget</h3>
                    </div>
                    <div class="box_img"><img src="http://navetteo.fr/uploads/service_images/28_le-bourget.png" alt="Peugeot 508 - Peugeot 508 " title="Peugeot 508 - Peugeot 508 "></div>
                    <div class="description">
                        La liaison entre Paris et l'aéroport Le Bourget est souvent très difficile. Transports en commun surchargés, longues files prendre un taxi.                                         </div>
                    <div class="button-row">
                        <div class="read-more" ><a href="<?php echo base_url(); ?>services" class="button light_green_button">Know more</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-6">
                <div class="box">
                    <div class="box_head">
                        <h3>Transfert Aeroport Paris Vatry</h3></div>
                    <div class="col-md-12 pkg_title_row">
                        <div class="plans_img"><img src="http://navetteo.fr/uploads/service_images/0_paris-vatry.png" alt="Peugeot 508 - Peugeot 508 " title="Peugeot 508 - Peugeot 508 "></div>
                    </div>
                    <div class="description">
                        La liaison entre Paris et l'aéroport Paris Vatry est souvent très difficile. Transports en commun surchargés, longues files prendre un taxi.                                         </div>
                    <div class="button-row">
                        <div class="read-more" ><a href="<?php echo base_url(); ?>services" class="button light_green_button">Know more</a></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box_head">
                        <h3>Transfert Gare Austerlitz</h3>
                    </div>
                    <div class="col-md-12 pkg_title_row">
                        <div class="plans_img"><img src="http://navetteo.fr/uploads/service_images/24_gare-d-austerlitz.png" alt="Peugeot 508 - Peugeot 508 " title="Peugeot 508 - Peugeot 508 "></div>
                    </div>
                    <div class="description">
                        Que ce soit sur les circuits touristiques de certaines régions, des visites guidées de grandes villes ou encore un transfert gare ou un transfert aéroport                                        </div>
                    <div class="button-row">
                        <div class="read-more" ><a href="<?php echo base_url(); ?>services" class="button light_green_button">Know more</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('.homepageSlider').bxSlider({
            mode: 'fade',
            auto: true,
            pager: true,
            autoControl: false,
            adaptiveHeight: true,
            easing: 'swing',
            responsive: true,
            preloadImages: 'all'
        });
        $(".slider_wrapper .bx-viewport").css("height", '530px');
        $("#statut").on('change',function() { 
           var statut_id = $(this).val();
           if (statut_id == 2) {
               $(".divCompany").show();
           }
           else { 
               $(".divCompany").hide();
           }
        });
    });
</script>