<link href="<?php echo base_url(); ?>assets/system_design/css/login.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/system_design/css/register.css" rel="stylesheet">
<style> 
select {margin-top:0px;}
#login_form{width: 100%;}</style>
<script type="text/javascript">
    (function ($, W, D)
    {
        var JQUERY4U = {};

        JQUERY4U.UTIL =
                {
                    setupFormValidation: function ()
                    {

                        //form validation rules
                        $("#login_form").validate({
                            rules: {
                                username: {
                                    required: true
                                },
                                password: {
                                    required: true
                                }
                            },
                            messages: {
                                username: {
                                    required: "<?php echo $this->lang->line('user_name_valid'); ?>"
                                },
                                password: {
                                    required: "<?php echo $this->lang->line('password_valid'); ?>"
                                }
                            },
                            errorPlacement: function (error, element) {
                                var name = $(element).attr("name");
                                error.appendTo($("#" + name + "_validate"));
                                error.appendTo( element.parents("div.input-group") );
                                 error.insertAfter(element);
                            },
                            submitHandler: function (form) {
                                form.submit();
                            }
                        });
                    }
                }

        //when the dom has loaded setup form validation rules
        $(D).ready(function ($) {
            JQUERY4U.UTIL.setupFormValidation();
        });

    })(jQuery, window, document);
</script>
<script type="text/javascript">
    (function ($, W, D)
    {
        var JQUERY4U = {};

        JQUERY4U.UTIL =
                {
                    setupFormValidation: function ()
                    {
                        //Additional Methods            
                        $.validator.addMethod("lettersonly", function (a, b) {
                            return this.optional(b) || /^[a-z ]+$/i.test(a)
                        }, "<?php echo $this->lang->line('valid_name'); ?>");

                        $.validator.addMethod("phoneNumber", function (uid, element) {
                            return (this.optional(element) || uid.match(/^([0-9]*)$/));
                        }, "<?php echo $this->lang->line('valid_phone_number'); ?>");


                        $.validator.addMethod("pwdmatch", function (repwd, element) {
                            var pwd = $('#password').val();
                            return (this.optional(element) || repwd == pwd);
                        }, "<?php echo $this->lang->line('valid_passwords'); ?>");

                        //form validation rules
                        $("#client_signupform").validate({
                            rules: {
                                statut: {
                                    required:true
                                },
                                company: {
                                    required:  function(){
                                                    return $("#statut").val() == "2";
                                              }
                                },
                                first_name: {
                                    required: true,
                                    lettersonly: true
                                },
                                email: {
                                    required: true,
                                    email: true
                                },
                                phone:{
                                        required: true,
                                        phoneNumber: true,
                                        rangelength: [10, 11]
                                },
                                password:{
                                        required: true,
                                        rangelength: [8, 30]
                                },
                                confirm_password: {
                                    required:true,
                                    pwdmatch:true
                                }
                            },
                            messages: {
                                statut: {
                                    required:"Please select the statut"
                                },
                                company: {
                                    required:"Please enter the company"
                                },
                                first_name: {
                                    required: "<?php echo $this->lang->line('first_name_valid');?>"
                                },
                                email:{
                                    required: "<?php echo $this->lang->line('email_valid');?>"
                                },
                                phone:{
                                        required: "<?php echo $this->lang->line('phone_valid');?>"
                                },
                                password: {
                                    required: "<?php echo $this->lang->line('password_valid');?>"
                                },
                                password_confirm:{
                                        required: "<?php echo $this->lang->line('confirm_password_valid');?>"
                                }
                            }, 
                            submitHandler: function(form) {
                                form.submit();
                            }
                        });
                    }
                }

        //when the dom has loaded setup form validation rules
        $(D).ready(function ($) {
            JQUERY4U.UTIL.setupFormValidation();
        });

    })(jQuery, window, document);
</script>
</header>
<?php error_reporting(0) ?>
<div class="container body-border">
    <div class="breadcrumb">
        <div class="row">
            <aside class="nav-links">
                <ul>
                    <li><a href="<?php echo site_url(); ?>/" style="color:#4ca6cf;"><i class="fa fa-home"></i>  <?php echo $this->lang->line('home_page');?> </a></li>
                    <li class="active"><a href="javascript:void(0)"><?php echo $title ?> </a></li>
                </ul>
            </aside>
        </div> 
    </div>
 <div class="col-md-12 col-xs-12">
              <?php include "alert.php"; ?>
 </div>
    <div class="row">
        <div class="col-md-5" style="padding:0px !important;">
            
            <div id="total-login" style="margin:0px !important;width: 100%">
                <?php
                    $attributes = array("name" => 'login_form', "id" => 'login_form');
                    echo form_open('client/login_validate', $attributes);
                ?>
                  <div class="row" style="margin-left:0px !important;">
                    <div class="col-md-10 col-xs-10 col-md-offset-1" style="margin-left:0px !important;padding-left:20px !important;padding-right:0px !important;width:95%;">
                        <div class="form-group text-center"> 
                            <label for="login_form" style="color: #555;font-weight:bold;font-size:15px;">Already client here ?</label>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left:0px !important;">
                    <div class="col-md-10 col-xs-10 col-md-offset-1" <?php if($forgot_form){ echo 'style="display:none;"'; } ?> style="margin-left:0px !important;padding-left:10px !important;padding-right:0px !important;width:95%;">
                       
                    </div>
                </div>
                <div class="row" style="margin-left:0px !important;padding-left: 10px;">
                    <div class="col-md-10 col-xs-10 col-md-offset-1" style="margin-left:0px !important;padding-left:10px !important;padding-right:0px !important;width:95%;">
                        <div class="form-group"> 
                            <label for="username">Username</label>
                            <div class='input-group'>
                                <span class="input-group-addon">
                                    <i class="fa fa-user"></i> 
                                </span>
                                <input type="text" name="username" value="" id="identity" class="user form-control" placeholder="Email/Username:">
                                <?php //echo form_input($username); ?>
                            </div>
                            <?php echo form_error('username'); ?>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left:0px !important;padding-left: 10px;">
                    <div class="col-md-10 col-xs-10 col-md-offset-1" style="margin-left:0px !important;padding-left:10px !important;padding-right:0px !important;width:95%;">
                        <div class="form-group">
                        <label for="password">Password</label>
                        <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-lock"></i>
                                </span>  
                            <?php echo form_input($passwords); ?>
                            <span class="input-group-btn">
                                <button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-close"></i></button>
                            </span>  
                            </div>
                            <?php echo form_error('password'); ?>
                            <input type="hidden" name="remember" value="1"/>
                            <input type="hidden" name="curl" value="<?php echo $_GET['curl'] ?>"/>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left:0px !important;">
                    <div class="col-md-10 col-xs-10 col-md-offset-1" style="margin-left:0px !important;padding-left:10px !important;padding-right:0px !important;width:95%;">
                        <div class="col-md-6 col-xs-6">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label> <input type="checkbox" style="margin-right: 0; margin-top: 4px; margin-left: -20px; float: none;" value="1" name="remember_me"> Remember me </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-6" style="text-align: right;top: 10px;">
                            <a href="javascript:ForgotPassword()"> <?php echo $this->lang->line('login_forgot_password'); ?></a>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left:0px !important;padding-left: 10px;">
                    <div class="col-md-10 col-xs-10 col-md-offset-1" style="padding:10px 0px 0px 10px; margin-left:0px !important;width:95%;">
                        <input type="submit" class="button green_button" value="<?php echo $this->lang->line('login'); ?>" style="float:right;"/>
                    </div>
                </div>
                
                <?php echo form_close(); ?>
            </div>
            
             <div class="col-md-12 forgot-password" <?php if($forgot_form){ echo 'style="display:block;"'; } ?>><!--col-md-offset-3-->
                <div id="forgot-password">
                   <?php
                   $attributes = array("name" => 'forgot_form', "id" => 'forgot_form');
                   echo form_open('forgot_password', $attributes);
                   ?>
                  
                   <div class="col-md-8 col-xs-8">
                      <div class="input-group input-group-lg in-ty" style="margin-top: 10px;">
                          <div class='input-group'>
                                <span class="input-group-btn">
                                    <button class="btn btn-default pwd-grp" type="button"><i class="fa fa-envelope"></i></button>
                                </span>
                                <input type="text" name="username" value="" id="identity" class="user" placeholder="Email/Username:">
                                <?php // echo form_input($username); ?>
                            </div>
                         <?php echo form_error('username'); ?>
                      </div>
                   </div>
                   <div class="col-md-4 col-xs-4" style="padding:10px 0px 0px 10px;">
                      <input type="submit" class="button green_button" value="<?php echo $this->lang->line('login'); ?>"/>
                   </div>
                   
                   <?php echo form_close(); ?>
                </div>
             </div>
        </div>
         <div class="login-page-divider" style="margin-left:-60px;">&nbsp;</div>
        <div class="col-md-6 col-md-offset-1">
           <?php echo form_open('client/signup_validate', array("id" => "client_signupform")); ?>
           <div class="row">
                <div class="col-md-12 form-group text-center">  
                      <label for="login_form" style="color: #555;font-weight:bold;font-size:15px;">New client here ?</label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group">  
                    <label> Statut </label>                                
                    <select class="form-control" name="statut" id="statut">
                        <option value="1">Independent</option>
                        <option value="2">Company</option>
                    </select>

                </div>
                <div class="col-md-6 form-group"> 
                    <div class="divCompany" style="display:none;">
                        <label>Company</label>
                        <?php echo form_input($company); ?>
                        <?php echo form_error('company'); ?>
                    </div> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 form-group"> 
                    <label> Civility </label>                                
                    <select class="form-control" name="civility">
                        <option value="Mr">Mr</option>
                        <option value="Mrs">Mrs</option>
                    </select> 
                </div>
                <div class="col-md-5 form-group"> 
                    <label>First Name</label>
                    <?php echo form_input($first_name); ?>
                    <?php echo form_error('first_name'); ?> 
                </div>
                <div class="col-md-5 form-group"> 
                    <label>Last Name</label>
                    <?php echo form_input($last_name); ?>
                    <?php echo form_error('last_name'); ?> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group"> 
                    <label>Email</label>
                    <?php echo form_input($email); ?>
                    <?php echo form_error('email'); ?> 
                </div>
                <div class="col-md-6 form-group"> 
                    <label>Phone</label>
                    <?php echo form_input($phone); ?>
                    <?php echo form_error('phone'); ?> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group"> 
                    <label>Address</label>
                    <?php echo form_textarea($address); ?>
                    <?php echo form_error('address'); ?> 
                </div>
                <div class="col-md-6 form-group"> 
                    <label>&nbsp;</label>
                    <?php echo form_textarea($address1); ?>
                    <?php echo form_error('address1'); ?> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group"> 
                    <label>City</label>
                    <?php echo form_input($city); ?>
                    <?php echo form_error('city'); ?> 
                </div>
                <div class="col-md-6 form-group"> 
                    <label>Zipcode</label>
                    <?php echo form_input($zipcode); ?>
                    <?php echo form_error('zipcode'); ?> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group"> 
                    <label>Mobile</label>
                    <?php echo form_input($mobile); ?>
                    <?php echo form_error('mobile'); ?> 
                </div> 
                <div class="col-md-6 form-group"> 
                    <label>Fax</label>
                    <?php echo form_input($fax); ?>
                    <?php echo form_error('fax'); ?> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group"> 
                    <label>Password</label>
                    <?php echo form_input($password); ?>
                    <?php echo form_error('password'); ?> 
                </div> 
                <div class="col-md-6 form-group"> 
                    <label>Confirm Password</label>
                    <?php echo form_input($confirm_password); ?>
                    <?php echo form_error('confirm_password'); ?> 
                </div>   
            </div> 
            <div class="row">
                <div class="col-md-12"> 
                    <input type="submit" class="button green_button" value="Register" style="margin-top: 30px;float:right;" />  
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div> 

<script type="text/javascript">
    $(document).ready(function () {
        // $.colorbox({inline: true, href: ".ajax"});
        $(".reveal").on('click',function() {
            var $pwd = $(".password");
            if ($pwd.attr('type') === 'password') {
                $pwd.attr('type', 'text');
                $('.reveal i').removeClass('glyphicon-eye-close');
                $('.reveal i').addClass('glyphicon-eye-open');
            } else {
                $pwd.attr('type', 'password');
                $('.reveal i').removeClass('glyphicon-eye-open');
                $('.reveal i').addClass('glyphicon-eye-close');
            }
        });
    });
    
    function ForgotPassword(){
        $('.forgot-password').toggle();
    }
</script> 
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url(); ?>assets/system_design/scripts/bootstrap.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places,geometry"></script>
<script src="<?php echo base_url(); ?>assets/system_design/scripts/jquery.geocomplete.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#statut").on('change', function () {
            var statut_id = $(this).val();
            if (statut_id == 2) {
                $(".divCompany").show();
            } else {
                $(".divCompany").hide();
            }
        });
        $(".location").geocomplete({
            country: '<?php echo $site_settings->site_country; ?>'
        }).bind("geocode:result", function (event, result) {
            // $(this).val(JSON.stringify(result.formatted_address));       
            //$form_address = JSON.stringify(result.formatted_address);
            //$address = $form_address.replace('\"','');
            //$(this).val($address);        
            $fieldName = $(this).attr('id');
            console.log("Field Name: " + $fieldName);
            $formatted_address = result.formatted_address;
            console.log("form address : " + $formatted_address);
            $("#" + $fieldName).val($formatted_address);
            console.log($fieldName + " : " + $("#" + $fieldName).val());
        });


        //  $.colorbox({inline:true, href:".ajax"});

    });
</script> 
</body>
</html>
