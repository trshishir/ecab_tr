</header>
<div class="container body-border">
    <div class="breadcrumb">
        <div class="row">
            <aside class="nav-links">
                <ul>
                    <li> <a href="<?php echo site_url(); ?>"> <?php echo $this->lang->line('home_page'); ?>  </a> </li>
                    <li class="active"><a href="javascript:void(0)">&nbsp;<?php if (isset($sub_heading)) echo $sub_heading; ?>s </a></li>
                </ul>
            </aside>
        </div>
    </div>
    <div class="row">
    <div class="col-md-12 legal-notice">

        <?php echo ($legals[0]->description); ?>







        <!-- 
        
        
<?php if ($this->lang->lang() == 'fr') { ?>
                                <p>Ce site est &eacute;dit&eacute; par la soci&eacute;t&eacute; <?php echo APP_NAME; ?> S.A.S</p>
            <p>Handi-Express SAS est une soci&eacute;t&eacute; agr&eacute;e par l'Etat: </p>
            <p>Agr&eacute;ment Transport routier public de personnes N&deg;: 2010/11/0004866</p>
            <p>Si&eacute;ge social : 48/50 Avenue d'Enghien, 93800 Epinay sur seine. </p>
            <p>RCS : Bobigny B 524 189 925</p>
            <p>SIREN : 524189925</p>
            <p>TVA (intracommunautaire) : FR 64 524189925 </p>
            <p>Capital Social : 100.000 euros</p>
            <p>La direction de la publication est assur&eacute;e par&nbsp;&nbsp;la soci&eacute;t&eacute; Handi-Express SAS.</p>
            <p>L'h&eacute;bergement de notre siteweb est assur&eacute; par la soci&eacute;t&eacute;&nbsp;OVH dont le si&egrave;ge social est situ&eacute; au <span style="font-family: ArialNarrow;">2 rue Kellermann 59100 Roubaix</span>&nbsp;Cedex, t&eacute;l&eacute;phone : <strong>0899 701 761</strong> 1,349 &euro; TTC par appel puis 0,337 &euro; TTC la minute .</p>
            <p>Conform&eacute;ment &agrave; la loi &laquo; Informatique et Libert&eacute;s &raquo; du 6 janvier 1978 modifi&eacute;e, ce site a fait l&rsquo;objet d&rsquo;une d&eacute;claration aupr&egrave;s de la CNIL (d&eacute;claration N&deg;<strong><span style="font-size: small;">1418754.</span></strong>).</p>
            <p>Les informations que nous sommes amen&eacute;s &agrave; recueillir proviennent de l&rsquo;enregistrement volontaire d&rsquo;une adresse courriel fournie par l&rsquo;internaute, lui permettant de recevoir une documentation et/ou de se renseigner sur un point quelconque. Ces informations ne sont pas communiqu&eacute;es &agrave; des tiers. Concernant les informations nominatives diffus&eacute;es sur le site, les personnes concern&eacute;es ont &eacute;t&eacute; inform&eacute;es de leurs droits.</p>
            <p>Conform&eacute;ment &agrave; la loi &laquo; Informatique et Libert&eacute;s &raquo; du 06/01/1978, vous disposez d'un droit d'acc&egrave;s, de rectification et d'opposition aux informations vous concernant qui peut s'exercer par courrier &agrave;&nbsp;l'adresse&nbsp;de&nbsp;Handi-Express&nbsp; SAS&nbsp; en indiquant vos nom, pr&eacute;nom, adresse postale compl&egrave;te et adresse email. L&rsquo;ensemble de ce site rel&egrave;ve de la l&eacute;gislation fran&ccedil;aise et internationale sur le droit d&rsquo;auteur et la propri&eacute;t&eacute; intellectuelle.</p>
            <p>Toute repr&eacute;sentation, reproduction, modification, utilisation commerciale, ainsi que tout transfert vers un autre site sont interdits (ce site &agrave; fait l'objet d'un d&eacute;p&ocirc;t aupr&eacute;s de Copyright France sous le N&deg;: <strong><span style="color: #000000;">C85D1A7</span><span style="font-size: xx-small;">).</span></strong>&nbsp;sauf autorisation explicite de notre part,&agrave; demander par courrier &agrave; l&rsquo;adresse indiqu&eacute;e plus haut. Les liens hypertextes pr&eacute;sents sur le site orientant les utilisateurs vers d&rsquo;autres sites Internet n&rsquo;engagent pas la responsabilit&eacute; de&nbsp;Handi-Express quant au contenu de ces sites.</p>
            <p>Toute personne physique ou morale souhaitant que soit &eacute;tabli un lien (adresse de son site) &agrave; partir de notre site, doit pr&eacute;alablement en demander l&rsquo;autorisation.&nbsp;&agrave; l'exception de sites diffusant des informations et/ou contenus ayant un caract&egrave;re ill&eacute;gal et/ou &agrave; caract&egrave;re religieux, politique, pornographique, x&eacute;nophobe, l&rsquo;&eacute;tablissement de lien vers notre site est autoris&eacute; sous r&eacute;serve qu'il s'agisse d'un lien vers la page d'accueil du site et que ce lien ouvre le site dans une nouvelle fen&ecirc;tre de navigation laissant appara&icirc;tre l'adresse www.ecab.app et www.ecab.app/recrutement, la soci&eacute;t&eacute; Handi-Express SAS&nbsp;se r&eacute;serve n&eacute;anmoins tout droit d&rsquo;opposition.</p>
            <p>
                    La loi applicable aux relations contractuelles entre Handi-Express SAS et ses utilisateurs  est la loi fran&ccedil;aise.<br />
                    A d&eacute;faut d&#39;accord amiable entre les parties concernant l&#39;interpr&eacute;tation ou l&#39;ex&eacute;cution des pr&eacute;sentes conditions g&eacute;n&eacute;rales d&#39;utilisation,seuls les tribunaux  de bobigny seront comp&eacute;tents pour juger d&#39;&eacute;ventuels litiges.<br />
                    La validation d&#39;informations ou d&#39;un service par &quot; simple ou double clic &quot; de l&#39;utilisateur constitue une signature &eacute;lectronique qui a, entre les parties, la m&ecirc;me valeur qu&#39;une signature manuscrite.<br />
                    &nbsp;</p>
            <p>Pour toute demande d&rsquo;informations suppl&eacute;mentaires, merci de nous contacter au 01 48 13 09 34. Nous sommes &agrave; votre disposition du lundi au Vendredi de 9h &agrave; 19h.</p>
                
    <?php
} else {
    ?>              <p>Navetteo.fr The site is edited by the company Navetteo SAS:</p>
                                        <p><b>Registered </b>office: 10 Rue de Penthièvre, 75008 Paris</p>
                                        <p><b>Siret </b>number: 81257428300014</p>
                                        <p><b>Registration number </b>RCS: 812 574 283 RCS Paris</p>
                                        <p><b>TVA intra</b> FR 50 812574283</p>
                                        <p><b>Share </b>capital: 2000 Euros</p>
                                        <p><b>Trainer registered in near the dirrect Bobigny under the </b>number:</p>
                                        <p><b>CNIL under the </b>number: 1885264</p>
                                        <p><b>Trademark with the INPI under the </b>number: 4194141</p>
                                        <p><b>Copyright France </b>No: 98BX1F9</p>
                                        <p><b>Publishing </b>Director: Navetteo the SAS</p>
                                        <p><b>Host: OVH - 2 rue Kellermann - 59100 Roubaix - France - 63 08203203</b></p>
                                        <p><b>No. </b>Phone: 01 85 09 94 91</p>
                                        <p><b>Fax </b>No.: 01 85 09 94 92</p>
                                        <p><b>Email: contact@Navetteo.fr</b><br>
                                    </p>
                                    <p>Management of the publication is provided by the company Navetteo SAS. 
                                    <p>The hosting of our WebSite is provided by OVH, whose registered office is at 2 rue Kellermann 59100 Roubaix Cedex, phone: 0899 701 761 € 1.349 per call and VAT . € 0.337 including tax per minute 
                                    accordance with the law "and Freedoms" of 6 January 1978, this site was the subject of a declaration to the CNIL (declaration N °: 1885264). </p>
                                    <p>The information we are led to collect from the voluntary registration of an email address provided by the user, allowing him to receive a brochure and / or to learn about any point. This information is not communicated to third parties. Regarding personal data published on the website, the persons concerned have been informed of their rights. </p>
                                    <p>According to the law "and Freedoms" of 06/01/1978, you have a right of access, correction and opposition to your information that can be exercised by mail to the SAS Navetteo address stating your full name, complete mailing address and email address. The whole of this site concerns the French and international legislation on copyright and intellectual property. </p>
                                    <p>Any representation, reproduction, modification, commercial use, and any transfer to another site are prohibited (this site is in the subject of a nearby deposit Copyright France under No.: 98BX1F9). unless explicitly authorized by us, to request by mail to the above address. The hyperlinks on the site directing users to other websites do not engage the responsibility of Handi-Express for the content of these sites. </p>
                                    <p>Any natural or legal person wishing to create a link (from its website address) from our site must first obtain permission. with the exception of sites distributing information and / or content with illegal and / or religious, political, pornographic, xenophobic.</p> 
                                    <p>Establishing link to our site is permitted provided that both of a link to the site home page that this link opens the site in a new browser window revealing the address www.navetteo.fr, the company Navetteo SAS nevertheless reserves any right of objection.</p>
<?php } ?> -->

    </div>
</div>
</div>