</header>
<div class="container body-border"> 
    <div class="breadcrumb">
        <div class="row">
            <aside class="nav-links">
                <ul>
                    <li> <a href="<?php echo site_url(); ?>"> <?php echo $this->lang->line('home_page'); ?>  </a> </li>
                    <li class="active"><a href="javascript:void(0)">&nbsp;<?php if (isset($sub_heading)) echo $sub_heading; ?> </a></li>
                </ul>
            </aside>
        </div>
    </div>
    <div class="col-md-12" style="text-align: center;">
        <?php foreach($zones as $data):?>
        <div class="col-md-6 admin_border" style="min-height: 483px; margin-bottom:10px;">
            <div class="col-md-12" style="text-align:left;">
                <?=($data->name)?"<label>$data->name  </label>":"";?>
                <?=($data->country)?"<label>$data->country  </label>":"";?>
                <?=($data->region)?"<label>$data->region  </label>":"";?>
                <?=($data->city)?"<label>$data->city  </label>":"";?>
                
            </div>
            <img src="<?php echo base_url(); ?>uploads/cms/<?=$data->image;?>" style=" width: 100%; min-width: 300px" alt="Trip Districts" title="<?=$data->name;?>" />
            
        </div>
    <?php endforeach;?>
    </div>

<!--     <div class="col-md-12" style="text-align: center;">
        <h2>Trips Available</h2>
        <div class="trip-districts">
            <img src="<?php echo base_url(); ?>assets/system_design/images/zones.png" style="width: 60%; min-width: 300px" alt="Trip Districts" title="Trip Districts" />
        </div>
    </div> -->
</div>