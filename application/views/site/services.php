</header>
<div class="container body-border service_page"> 
    <div class="breadcrumb">
        <div class="row">
            <aside class="nav-links">
                <ul>
                    <li> <a href="<?php echo site_url(); ?>"> <?php echo $this->lang->line('home_page'); ?>  </a> </li>
                    <li><a href="<?php echo base_url("services")?>">Services</a></li>
                    <?php if(!empty($service_info->category_name)){?>
                        <li class="active"><a href="javascript:void(0)"><?php echo $service_info->category_name ?> </a></li>
                    <?php } ?>
                    <li class="active"><a href="<?php echo base_url("services/{$service_info->category_link}/{$service_info->link_url}{$service_info->link}"); ?>">&nbsp;<?php if (isset($sub_heading)) echo $sub_heading; ?> </a></li>
                </ul>
            </aside>
        </div>
    </div>
    <div class="clearfix"></div> 
    <div id="services-section" class="body-bg">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-xs-12">
                <img src="<?php echo base_url(); ?>uploads/cms/<?php echo $service_info->image; ?>" class="service_image img-responsive" alt="<?php echo $service_info->name; ?>" title="<?php echo $service_info->name; ?>">
            </div>
            <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="detail-block">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12"> <?php //echo "<PRE>";print_r($job);print_r($details);echo "</PRE>";?>
                            <div class="dttime" style="float:left;">
                                <span class="lbl-text">Category: </span><strong><?= $service_info->category_name;?></strong>
                            </div> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <h2><?php echo $service_info->name; ?></h2>
                            <h5><?php echo $service_info->short_description; ?></h5><br/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="apply_now text-center">
                            <a href="#" class="button light_orange_button"><?php echo $this->lang->line('book_now'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class=" tab-content">
                    <?php echo $service_info->description; ?>
                </div>
            </div>
        </div>
    </div>
</div>