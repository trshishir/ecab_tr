</header>
<div class="container body-border"><!--padding-p-0 -->
	<div class="breadcrumb">
		<div class="row">
			<aside class="nav-links">
				<ul>
					<li> <a href="<?php echo site_url(); ?>"> <?php echo $this->lang->line('home_page'); ?>  </a> </li>
					<li class="active"><a href="javascript:void(0)">&nbsp;<?php if (isset($sub_heading)) echo $sub_heading; ?> </a></li>
				</ul>
			</aside>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="left-side-cont" style="margin:-15px 0px 200px -2px;">
				<article class="content">


                <?php 
                $category_faq = array_column($download, 'category_name');
                $category_faq = array_unique($category_faq);
                foreach ($category_faq as $key => $value):
                	?>
                	<div class="faqs">
                		<div class="faq-container">
                			<h3 class="faqs_category"><?=$value;?></h3>
                			<?php $i = 0;
                			foreach ($download as $row): $i++;
                				if($row->category_name==$value):
                					// var_dump($row);
                					?>
                					<section class="faq">
                						<h3 class=""><a target="_blank" href="<?php echo base_url();?>uploads/cms/<?php echo $row->image;?>" download> <i class="fa fa-download pull-right"></i> </a><?php echo $row->name; ?></h3>

                					</section>    
                					<?php
                				endif;
                			endforeach; ?>    
                		</div>
                	</div> 

                	<?php
                endforeach;
                ?>

            </article>
        </div>
    </div>
    <div class="col-md-6">
    	<?php //    $this->load->view('site/common/reasons_to_book');  ?>
    </div>
</div>
</div>
<script type="text/javascript" language="javascript">

</script>    