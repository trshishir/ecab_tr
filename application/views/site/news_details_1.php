<link href="<?php echo base_url(); ?>assets/system_design/css/login.css" rel="stylesheet">
<script type="text/javascript"> 
 (function($,W,D)
 {
    var JQUERY4U = {};

    JQUERY4U.UTIL =
    {
        setupFormValidation: function()
        {
            //Additional Methods            
            $.validator.addMethod("lettersonly",function(a,b){return this.optional(b)||/^[a-z ]+$/i.test(a)},"<?php echo $this->lang->line('valid_name');?>");

            $.validator.addMethod("phoneNumber", function(uid, element) {
                return (this.optional(element) || uid.match(/^([0-9]*)$/));
            },"<?php echo $this->lang->line('valid_phone_number');?>");


            $.validator.addMethod("pwdmatch", function(repwd, element) {
                var pwd= $('#password').val();
                return (this.optional(element) || repwd==pwd);
            },"<?php echo $this->lang->line('valid_passwords');?>");


        //form validation rules
        $("#driver_signupform").validate({
            rules: {
                statut: {
                    required:true
                },
                company: {
                    required:  function(){
                        return $("#statut").val() == "2";
                    }
                },
                first_name: {
                    required: true,
                    lettersonly: true
                },
                email: {
                    required: true,
                    email: true
                },
                phone:{
                    required: true,
                    phoneNumber: true,
                    rangelength: [10, 11]
                },
                password:{
                    required: true,
                    rangelength: [8, 30]
                },
                confirm_password: {
                    required:true,
                    pwdmatch:true
                }
            },
            messages: {
                statut: {
                    required:"Please select the statut"
                },
                company: {
                    required:"Please enter the company"
                },
                first_name: {
                    required: "<?php echo $this->lang->line('first_name_valid');?>"
                },
                email:{
                    required: "<?php echo $this->lang->line('email_valid');?>"
                },
                phone:{
                    required: "<?php echo $this->lang->line('phone_valid');?>"
                },
                password: {
                    required: "<?php echo $this->lang->line('password_valid');?>"
                },
                password_confirm:{
                    required: "<?php echo $this->lang->line('confirm_password_valid');?>"
                }
            }, 
            submitHandler: function(form) {
                form.submit();
            }
        });
    }
}

    //when the dom has loaded setup form validation rules
    $(D).ready(function($) {
        JQUERY4U.UTIL.setupFormValidation();
    });

})(jQuery, window, document);
</script>
</header>
<style>
    select {margin-top:0px;}
    .slider_wrapper .bx-wrapper .bx-pager {top:inherit;bottom: 0px;}
    select.cust_width1 {width:177px !important;margin-left:-17px !important;}
    select.cust_width2 {width:162px !important;margin-left:-5px !important;}
    select.statut {padding: 6px 10px;width: 127px;}
    .register-box textarea {height:auto !important;}
    input.green_button {top:25px;position:relative;}
    .register-box label {display:block !important;margin-bottom:0px !important;}
    .register-box .form-group {margin-bottom:5px !important;}
    .register-box .civility {width: 65px !important;padding: 5px !important;}
</style>

<div class="container body-border"> 
    <div class="breadcrumb">
        <div class="row">
            <aside class="nav-links">
                <ul>
                    <li><a href="<?php echo site_url(); ?>/"><i class="fa fa-home"></i>  <?php echo $this->lang->line('home_page'); ?> </a></li>
                    <li class="active"><a href="<?php echo site_url(); ?>blog"><?php echo $title ?> </a></li>
                    <li class=""><a ><?php echo $details->title ?> </a></li>
                </ul>
            </aside>
        </div> 
    </div>
    <div class="clearfix"></div> 
    <div id="services-section" class="body-bg">
        <div class="row">
<div class="col-md-12">
<img width="100%" src="<?php echo base_url();?>uploads/cms/<?php echo $details->image;?>">
</div>
<div class="col-md-12">
    <h1>
<?php echo $details->title;?>
</h1>
</div>

<div class="col-md-12">
<?php echo $details->description;?>
</div>




        </div>
    </div>
