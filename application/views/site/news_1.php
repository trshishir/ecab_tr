<link href="<?php echo base_url(); ?>assets/system_design/css/login.css" rel="stylesheet">
<style>
    .slider_wrapper .bx-wrapper .bx-pager {top:inherit;bottom: 0px;}
    select.cust_width1 {width:177px !important;margin-left:-17px !important;}
    select.cust_width2 {width:162px !important;margin-left:-5px !important;}
</style>
</header>
<div class="container body-border"><!--padding-p-0-->
    <div class="breadcrumb">
        <div class="row">
            <aside class="nav-links">
                <ul>
                    <li> <a href="<?php echo site_url(); ?>"> <?php echo $this->lang->line('home_page'); ?>  </a> </li>
                    <li class="active"><a href="javascript:void(0)">&nbsp;<?php if (isset($subtitle)) echo $subtitle; ?> </a></li>
                </ul>
            </aside>
        </div>
    </div>
    <div class="clearfix"></div> 
    <div id="services-section" class="body-bg">
        <div class="row">




            <?php
            foreach($news as $item):

                ?>

                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px;     padding:15px 0px 6px 0px;
                border: 1px solid lightgray;
                background:transparent linear-gradient(#fbfbfb, #ececec, #cecece) repeat scroll 0 0;
                margin-left: 19px;
                width: 97%;">

                <div class="col-md-4">
                    <!--       <?php if($item->rubens_new==1):?>

                            <div class="wrap">
                               <div class="ribbon6"><span class="promo-service">NEW</span></div>
                           </div>

                       <?php endif;?>

                       <?php if($item->rubens_promo==1):?>
                           <div class="wrap1">
                               <div class="ribbon5"><span class="promo-service">Promotion</span></div>
                           </div>
                           <?php endif;?> -->


                           <div class="plans_img" style="border: 1px solid lightgray;"><img src="<?php echo base_url();?>uploads/cms/<?php echo $item->image?>" alt="FIAT SCUDO TPMR - Fiat ScudoTpmr" title="<?php echo $item->title?>"></div>

                       </div>

                       <div class="col-md-8">
                        <div class="col-md-12">

                            <div class="col-md-12 " style="left:10px;
                            top: -17px;">
                            <p class="pull-right"><strong>Publish </strong> Date: <?php echo date('d-m-Y', strtotime($item->created_date));?>, 
                            <strong>Author</strong> <?php echo $item->civility.' '.$item->first_name.' '.$item->last_name;?></p>

                        </div> 
                        <div class="col-md-12" style="top: -32px;">
                            <p style="margin-left:-11px;    color: #0072bb;
                            margin-left: -11px;
                            font-size: 20px;
                            font-weight: bold;"><?=$item->title;?></p>

                        </div>                   
                    </div>

                    <div class="col-md-12" style="min-height:70px">
                        <?php

                        $text= $item->description;
                        $text= strip_tags($text); 

                        echo implode(' ', array_slice(explode(' ', $text), 0, 43)).' ...';


                        ?>                    

                    </div>
                    <div class="col-md-12 ">

                        <div class="button-row pull-right">
                            <div class="read_button" style=" margin-top:0px;"><a href="<?php echo base_url(); ?>blog/<?php echo $item->id;?>/<?php echo strtolower(trim(str_replace(' ', '-', preg_replace('/[^a-zA-Z0-9_ -]/s','',$item->title))));?><?=$item->link;?>" class="button light_green_button">Read more</a></div>



                        </div>  
                    </div>

                </div>


            </div>


        <?php endforeach;?>



        <?php
/*

        foreach($news as $item):

            ?>



            <div class="col-md-4 col-sm-12 col-xs-12" style="margin-top:15px;">
                <div class="ribbon">
                  <?php if($item->rubens_new==1):?>

                    <div class="wrap">
                     <div class="ribbon6"><span class="promo-service">NEW</span></div>
                 </div>

             <?php endif;?>

             <?php if($item->rubens_promo==1):?>
                 <!-- <div class="corner-promo-ribbon top-left-promo"><span class="promo-service">Promotion</span></div> -->
                 <div class="wrap1">
                     <div class="ribbon5"><span class="promo-service">Promotion</span></div>
                 </div>
             <?php endif;?>




             <div class="plans">

                <div class="plans_head">

                    <h3 style="margin-left:-11px;"><?=$item->title;?></h3>
                    <div class="plans_img"><img src="<?php echo base_url();?>uploads/cms/<?php echo $item->image?>" alt="FIAT SCUDO TPMR - Fiat ScudoTpmr" title="<?php echo $item->title?>"></div>

                                        <!--<div class="col-md-12 plan_title" id="plan_title_row">
                                            <p></p>
                                        </div>-->
                                        <div class="description">
                                            <?php

                                            $text= $item->description;
                                            $text= strip_tags($text); 

                                            echo implode(' ', array_slice(explode(' ', $text), 0, 25)).' ...';


                                            ?>                                        </div>

                                            <div class="button-row">
                                                <div class="learn_button" style="float:right;margin-bottom:20px;padding-left:11px;padding-right:11px;"><a href="<?php echo base_url(); ?>blog/<?php echo $item->id;?>/<?php echo strtolower(trim(str_replace(' ', '-', preg_replace('/[^a-zA-Z0-9_ -]/s','',$item->title))));?><?=$item->link;?>" class="button light_green_button">View more</a></div>



                                            </div>         
                                        </div>


                                    </div>
                                </div>
                            </div>


                        <?php endforeach;
*/
                        ?>   

<!-- 
            <div class="col-md-4">
                <div class="news">
                    <div class="news-header">
                        <h3>Transfert Aeroport D'Orly</h3>
                    </div>
                    <div class="col-md-12 pkg_title_row">
                        <div class="news_img"><img src="http://navetteo.fr/uploads/service_images/3_aeroport-orly.png" alt="Peugeot 508 - Peugeot 508 " title="Peugeot 508 - Peugeot 508 "></div>
                    </div>
                    <div class="description" >
                        La liaison entre Paris et l'aéroport d'Orly est souvent très difficile. Transports en commun surchargés, longues files d'attente pour prendre un taxi.                                         </div>
                    <div class="button-row">
                        <div class="view_more"><a href="<?php echo base_url(); ?>blog.php" class="button light_green_button">View more</a></div>



                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="news">
                    <div class="news-header">
                        <h3>TRANSFERT AEROPORT CDG</h3>
                    </div>
                    <div class="col-md-12 pkg_title_row">
                        <div class="news_img"><img src="http://navetteo.fr/uploads/service_images/4_aeroport-de-roissy.png" alt="Peugeot 508 - Peugeot 508 " title="Peugeot 508 - Peugeot 508 "></div>
                    </div>
                    <div class="description" >
                        Vous cherchez un transport pour vous rendre Ã L'aeroport? ou qu'on vous recupere de L'aeroport ?                                        </div>
                    <div class="button-row">
                        <div class="view_more"><a href="<?php echo base_url(); ?>blog.php" class="button light_green_button">View more</a></div>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="news">
                    <div class="news-header">
                        <h3>TRANSFERT AEROPORT BEAUVAIS</h3>
                    </div>
                    <div class="col-md-12 pkg_title_row">
                        <div class="news_img"><img src="http://navetteo.fr/uploads/service_images/5_aeroport-beauvais.png" alt="Peugeot 508 - Peugeot 508 " title="Peugeot 508 - Peugeot 508 "></div>
                    </div>
                    <div class="description" >
                        Navetteo est le meilleur site de transport de personne qui soit si vous avez besoin si vous avez besoin d'un transfert aéroport pour l'aéroport de Beauvais.                                        </div>
                    <div class="button-row">
                        <div class="view_more"><a href="<?php echo base_url(); ?>blog.php" class="button light_green_button">View more</a></div>


                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="news">
                    <div class="news-header">
                        <h3>TRANSFERT AEROPORT LE BOURGET</h3>
                    </div>
                    <div class="col-md-12 pkg_title_row">
                        <div class="news_img"><img src="http://navetteo.fr/uploads/service_images/28_le-bourget.png" alt="Peugeot 508 - Peugeot 508 " title="Peugeot 508 - Peugeot 508 "></div>
                    </div>
                    <div class="description" >
                        La liaison entre Paris et l'aéroport Le Bourget est souvent très difficile. Transports en commun surchargés, longues files prendre un taxi.                                         </div>
                    <div class="button-row">
                        <div class="view_more"><a href="<?php echo base_url(); ?>blog.php" class="button light_green_button">View more</a></div>
                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="news">
                    <div class="news-header">
                        <h3>TRANSFERT AEROPORT PARIS VATRY</h3>
                    </div>
                    <div class="col-md-12 pkg_title_row">
                        <div class="news_img"><img src="http://navetteo.fr/uploads/service_images/0_paris-vatry.png" alt="Peugeot 508 - Peugeot 508 " title="Peugeot 508 - Peugeot 508 "></div>
                    </div>
                    <div class="description" >
                        La liaison entre Paris et l'aéroport Paris Vatry est souvent très difficile. Transports en commun surchargés, longues files prendre un taxi.                                         </div>
                    <div class="button-row">
                        <div class="view_more" ><a href="<?php echo base_url(); ?>blog.php" class="button light_green_button">View more</a></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="news">
                    <div class="news-header">
                        <h3>TRANSFERT GARE AUSTERLITZ</h3>
                    </div>
                    <div class="col-md-12 pkg_title_row">
                        <div class="news_img"><img src="http://navetteo.fr/uploads/service_images/24_gare-d-austerlitz.png" alt="Peugeot 508 - Peugeot 508 " title="Peugeot 508 - Peugeot 508 "></div>
                    </div>
                    <div class="description" >
                        Que ce soit sur les circuits touristiques de certaines régions, des visites guidées de grandes villes ou encore un transfert gare ou un transfert aéroport                                        </div>
                    <div class="button-row">
                        <div class="view_more" ><a href="<?php echo base_url(); ?>services" class="button light_green_button">View more</a></div>
                    </div>
                </div>
            </div> -->






            
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('.homepageSlider').bxSlider({
            mode: 'fade',
            auto: true,
            pager: true,
            autoControl: false,
            adaptiveHeight: true,
            easing: 'swing',
            responsive: true,
            preloadImages: 'all'
        });
        $(".slider_wrapper .bx-viewport").css("height", '530px');
        $("#statut").on('change',function() { 
         var statut_id = $(this).val();
         if (statut_id == 2) {
             $(".divCompany").show();
         }
         else { 
             $(".divCompany").hide();
         }
     });
    });
</script>