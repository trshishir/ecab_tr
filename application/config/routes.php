<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, adhowever, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
| http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$default_controller = "welcome";
$controller_exceptions = array('welcome');
//  $route["^((?!\b".implode('\b|\b', $controller_exceptions)."\b).*)$"] = $route['default_controller'].'/$1';
/* foreach($controller_exceptions as $v) {
  $route[$v] = $default_controller . "/".$v;
  $route[$v."/(.*)"] = $default_controller . "/".$v.'/$1';
  } */

// $route['404_override'] = '';

$route['translate_uri_dashes'] = FALSE;

$route['default_controller'] = "auth";
$route['login'] = "auth/login";
$route['submit-contact-form'] = "auth/submitContactForm";
$route['call-me'] = "auth/callMe";
$route['submit-job-form'] = "auth/submitJobForm";
$route['chauffeur'] = "auth/driverlogin";
$route['404_override'] = 'welcome/page_404';
$route['index'] = "auth";

$route['admin'] = "auth/admin";
$route['forgot_password'] = "auth/forgot_password";
$route['reset_password'] = "auth/reset_password";
$route['drivers/registration'] = 'drivers/registration';
$route['driver/(:any)/(:any)'] = 'driver/index/$2';
$route['driver/register'] = 'driver/signup';
$route['driver/signup'] = 'driver/signup';
$route['driver/login'] = 'driver/login';
$route['driver/dashboard'] = 'driver/dashboard';
$route['driver/profile'] = 'driver/profile';
$route['driver/support'] = 'driver/support';

$route['book_now/(:any)'] = "book_now";

// Client ROUTES
$route['client'] = 'client/login';
$route['client/dashboard'] = 'client/dashboard';


// ADMIN AREA
$route['admin/dashboard'] = "dashboard";
$route['admin/change_default_currency'] = "dashboard/change_default_currency";
$route['admin/change_default_language'] = "dashboard/change_default_language";
$route['admin/logout'] = "dashboard/logout";

// Rectruitement_config ROUTES
$route['admin/recruitment_config'] = "Rectruitement_config";
$route['Rectruitement_config'] = "";
$route['admin/recruitment'] = "Rectruitement_config/add_jobs";

// Jobs
// $route['jobseeker/applications'] = "jobs/applications";
// $route['jobseeker/signup'] = "jobs/signup";
// $route['jobseeker/login'] = "jobs/login";
// $route['jobseeker/home'] = "jobs/home";
// $route['jobseeker/logout'] = "jobs/logout"; 
$route['jobs/(:any)'] = "jobs/view/$1";
$route['jobs/fetchJobseekersData'] = "jobs/fetchJobseekersData";
$route['jobs/jobseekerSearchData'] = "jobs/jobseekerSearchData";
$route['jobs/applications'] = "jobs/applications";
$route['jobs/signup'] = "jobs/signup";
$route['jobs/login'] = "jobs/login";
$route['jobs/logout'] = "jobs/logout"; 
$route['jobs/dashboard'] = "jobs/home";
$route['jobs/profile'] = "jobs/profile";
$route['jobs/document'] = "jobs/document";
$route['jobs/applications'] = "jobs/applications";
$route['jobs/commissions'] = "jobs/commissions";
$route['jobs/payments'] = "jobs/payments";
$route['jobs/messages'] = "jobs/messages";
$route['jobs/support'] = "jobs/support";

$route['jobseeker/profile'] = "jobs/profile";
$route['jobseeker/add'] = "jobs/add";
$route['jobseeker/store'] = "jobs/store";
$route['jobseeker/(:num)/edit'] = "jobs/edit/$1";
$route['jobseeker/(:num)/update'] = "jobs/update/$1";
$route['jobseeker/(:num)/reply'] = "jobs/reply/$1";
$route['jobseeker/(:num)/delete'] = "jobs/delete/$1";

// Partner Calls
$route['partner/(:any)/(:any)'] = "partner/index/$2";
$route['partner/signup'] = 'partner/signup';
$route['partner/login'] = 'partner/login';
$route['partner/home'] = 'partner/home';
$route['partner/profile'] = 'partner/profile';
$route['partner/support'] = 'partner/support';
$route['partner/logout'] = 'partner/logout';
$route['partner/support/add'] = "partner/add";
$route['partner/support/(:num)/edit'] = "partner/edit/$1";
$route['partner/support/(:num)/update'] = "partner/update/$1";
$route['partner/support/(:num)/reply'] = "partner/reply/$1";
$route['partner/support/(:num)/delete'] = "partner/delete/$1";

//route for affiliates

$route['affiliate/(:any)/(:any)']  = "affiliate/index/$2";
$route['affiliate/signup']  = "affiliate/signup";
$route['affiliate/login']   = "affiliate/login";
$route['affiliate/logout']  = "affiliate/logout";
$route['affiliate/home']    = "affiliate/home";
$route['affiliate/profile']    = "affiliate/profile";
$route['affiliate/support']    = "affiliate/support";
$route['admin/affiliates']  = "affiliates";
$route['admin/affiliates']  = "affiliates";
$route['admin/affiliates/add'] = "affiliates/add";
$route['admin/affiliates/fetchAffiliatesData'] = "affiliates/fetchAffiliatesData";
$route['admin/affiliates/affiliateSearchData'] = "affiliates/affiliateSearchData";

// Language routes
$route['admin/language'] = "language";
$route['admin/language/add'] = "language/add_language";
$route['admin/language/store_language'] = "language/store_language";
$route['admin/language/(:num)/edit'] = "language/edit_language/$1";
$route['admin/language/(:num)/view'] = "language/view_language/$1";

$route['admin/translations/(:any)/importcsv'] = "language/importcsv_translations/$1";
$route['admin/translations/(:any)/add'] = "language/add_translations/$1";
$route['admin/translations/(:any)/(:any)/edit'] = "language/edit_translation/$1/$2";
$route['admin/translations/(:any)/delete'] = "language/delete_translatio/$1n";

$route['admin/translations/(:any)'] = "language/translations/$1";
// routes for themes module by abid update_theme
$route['admin/themes'] = "themes";
$route['admin/themes/add'] = "themes/add_theme";
$route['admin/themes/store_theme'] = "themes/store_theme";
$route['admin/themes/(:num)/edit'] = "themes/edit_theme/$1";
$route['admin/themes/update_theme'] = "themes/update_theme";
$route['admin/themes/(:num)/view'] = "themes/view_theme/$1";
$route['admin/themes/(:num)/editTheme'] = "themes/edit/$1";
// routs for unitys module by abid
$route['admin/unitys'] = "unitys";
$route['admin/unitys/add'] = "unitys/add_unity";
$route['admin/unitys/store_unity'] = "unitys/store_unity";
$route['admin/unitys/(:num)/edit'] = "unitys/edit_unity/$1";
$route['admin/unitys/update_unity'] = "unitys/update_unity";
$route['admin/unitys/(:num)/view'] = "unitys/view_unity/$1";
// routes for currency module by abid
$route['admin/currency'] = "currency";
$route['admin/currency/add'] = "currency/add_currency";
$route['admin/currency/store_currency'] = "currency/store_currency";
$route['admin/currency/(:num)/edit'] = "currency/edit_currency/$1";
$route['admin/currency/update_currency'] = "currency/update_currency";
$route['admin/currency/(:num)/view'] = "currency/view_currency/$1";

// all routes for accounting config module 
$route['admin/accounting_config'] = "accounting_config";
$route['admin/accounting_config/accounting_statuts_add'] = "accounting_config/accounting_statuts_add";
$route['admin/accounting_config/accounting_statuts_update'] = "accounting_config/accounting_statuts_update";

// all routes for accounting config module 
$route['admin/accounting_config/accounting_vat_add'] = "accounting_config/accounting_vat_add";
$route['admin/accounting_config/accounting_vat_update'] = "accounting_config/accounting_vat_update";

// For payment method tab
$route['admin/accounting_config/accounting_payment_methode_add'] = "accounting_config/accounting_payment_methode_add";
$route['admin/accounting_config/accounting_payment_methode_update'] = "accounting_config/accounting_payment_methode_update";
// Payment Delay Tab
$route['admin/accounting_config/accounting_payment_delay_add'] = "accounting_config/accounting_payment_delay_add";
$route['admin/accounting_config/accounting_payment_delay_update'] = "accounting_config/accounting_payment_delay_update";
// For Bill Category Tab
$route['admin/accounting_config/accounting_bill_category_add'] = "accounting_config/accounting_bill_category_add";
$route['admin/accounting_config/accounting_bill_category_update'] = "accounting_config/accounting_bill_category_update";
// For Sales Category Tab
$route['admin/accounting_config/accounting_sales_category_add'] = "accounting_config/accounting_sales_category_add";
$route['admin/accounting_config/accounting_sales_category_update'] = "accounting_config/accounting_sales_category_update";

// For Client Category Tab
$route['admin/accounting_config/accounting_client_category_add'] = "accounting_config/accounting_client_category_add";
$route['admin/accounting_config/accounting_client_category_update'] = "accounting_config/accounting_client_category_update";

// For Supplier Tab
$route['admin/accounting_config/accounting_supplier_category_add'] = "accounting_config/accounting_supplier_category_add";
$route['admin/accounting_config/accounting_supplier_category_update'] = "accounting_config/accounting_supplier_category_update";

// For Supplier Tab
$route['admin/accounting_config/accounting_supplier_add'] = "accounting_config/accounting_supplier_add";
$route['admin/accounting_config/accounting_supplier_update'] = "accounting_config/accounting_supplier_update";

// For administration Tab
$route['admin/accounting_config/accounting_organization_category_add'] = "accounting_config/accounting_organization_category_add";
$route['admin/accounting_config/accounting_organization_category_update'] = "accounting_config/accounting_organization_category_update";

// For administration Tab
$route['admin/accounting_config/accounting_organization_add'] = "accounting_config/accounting_organization_add";
$route['admin/accounting_config/accounting_organization_update'] = "accounting_config/accounting_organization_update";

// For bank Tab
$route['admin/accounting_config/accounting_bank_add'] = "accounting_config/accounting_bank_add";
$route['admin/accounting_config/accounting_bank_update'] = "accounting_config/accounting_bank_update";

// For Factors Tab
$route['admin/accounting_config/accounting_factor_add'] = "accounting_config/accounting_factor_add";
$route['admin/accounting_config/accounting_factor_update'] = "accounting_config/accounting_factor_update";
$route['admin/accounting_config/delete_config_record'] = "accounting_config/delete_config_record";
// for client kind
$route['admin/accounting_config/accounting_client_kind_add'] = "accounting_config/accounting_client_kind_add";
$route['admin/accounting_config/accounting_client_kind_update'] = "accounting_config/accounting_client_kind_update";
// for Garantee Funds
$route['admin/accounting_config/accounting_garantee_fund_add'] = "accounting_config/accounting_garantee_fund_add";
$route['admin/accounting_config/accounting_garantee_fund_update'] = "accounting_config/accounting_garantee_fund_update";
// for Garantee Funds
$route['admin/accounting_config/accounting_financement_delay_add'] = "accounting_config/accounting_financement_delay_add";
$route['admin/accounting_config/accounting_financement_delay_update'] = "accounting_config/accounting_financement_delay_update";
// end of routes in config module

// routes for documents module by abid documents
$route['admin/documents'] = "documents";
$route['admin/documents/add'] = "documents/add_document";
$route['admin/documents/store_document'] = "documents/store_document";
$route['admin/documents/(:num)/edit'] = "documents/edit_document/$1";
$route['admin/documents/update_document'] = "documents/update_document";
$route['admin/documents/(:num)/view'] = "documents/view_document/$1";
$route['admin/documents/get_users_against_category'] = "documents/get_users_against_category";

// Routes for library module by abid 
$route['admin/library'] = "library";
$route['admin/library/add'] = "library/add_library";
$route['admin/library/store_library'] = "library/store_library";
$route['admin/library/(:num)/edit'] = "library/edit_library/$1";
$route['admin/library/update_library'] = "library/update_library";

// company routes
$route['admin/company'] = "company";
$route['admin/company/save'] = "company/save";

// department routes
$route['admin/departments'] = "departments";
$route['admin/departments/add'] = "departments/add";
$route['admin/departments/save'] = "departments/save";
$route['admin/departments/(:num)/edit'] = "departments/edit/$1";
$route['admin/departments/(:num)/update'] = "departments/update/$1";
$route['admin/departments/(:num)/delete'] = "departments/delete/$1";

// Roles routes
$route['admin/roles'] = "roles";
$route['admin/roles/add'] = "roles/add";
$route['admin/roles/save'] = "roles/save";
$route['admin/roles/(:num)/edit'] = "roles/edit/$1";
$route['admin/roles/(:num)/update'] = "roles/update/$1";
$route['admin/roles/(:num)/delete'] = "roles/delete/$1";
$route['admin/roles/set_search_session'] = "roles/set_search_session";

// Users routes
$route['admin/users'] = "users";
$route['admin/users/add'] = "users/add";
$route['admin/users/save'] = "users/save";
$route['admin/users/(:num)/edit'] = "users/edit/$1";
$route['admin/users/(:num)/update'] = "users/update/$1";
$route['admin/users/(:num)/delete'] = "users/delete/$1";
$route['admin/users/set_search_session'] = "users/set_search_session";
$route['admin/users/deleteusers'] = "users/delete_data";

//CMS PAGES
$route['admin/cms/addcms'] = "cms/addcms";
$route['admin/cms'] = "cms";
$route['admin/cms/add'] = "cms/add";
$route['admin/cms/save'] = "cms/save";
$route['admin/cms/(:num)/edit'] = "cms/edit/$1";
$route['admin/cms/(:num)/update'] = "cms/update/$1";
$route['admin/cms/(:num)/delete'] = "cms/delete/$1";


//Page Type
$route['admin/pagetype'] = "pagetype";
$route['admin/pagetype/add'] = "pagetype/add";
$route['admin/pagetype/save'] = "pagetype/save";
$route['admin/pagetype/(:num)/edit'] = "pagetype/edit/$1";
$route['admin/pagetype/(:num)/update'] = "pagetype/update/$1";
$route['admin/pagetype/(:num)/delete'] = "pagetype/delete/$1";

//Service PAGES
$route['admin/cmspages'] = "cmspages";
$route['admin/cmspages/add'] = "cmspages/add";
$route['admin/cmspages/save'] = "cmspages/save";
$route['admin/cmspages/(:num)/edit'] = "cmspages/edit/$1";
$route['admin/cmspages/(:num)/update'] = "cmspages/update/$1";
$route['admin/cmspages/(:num)/delete'] = "cmspages/delete/$1";


//Services Page
$route['admin/pageservices'] = "pageservices";
$route['admin/pageservices/add'] = "pageservices/add";
$route['admin/pageservices/save'] = "pageservices/save";
$route['admin/pageservices/(:num)/edit'] = "pageservices/edit/$1";
$route['admin/pageservices/(:num)/update'] = "pageservices/update/$1";
$route['admin/pageservices/(:num)/delete'] = "pageservices/delete/$1";


// supplier routes
$route['admin/supplier'] = "supplier";
$route['admin/supplier/add'] = "supplier/add";
$route['admin/supplier/save'] = "supplier/save";
$route['admin/supplier/(:num)/edit'] = "supplier/edit/$1";
$route['admin/supplier/(:num)/update'] = "supplier/update/$1";
$route['admin/supplier/(:num)/delete'] = "supplier/delete/$1";

$route['accounting'] = "Accounting";
$route['admin/accounting'] = "Accounting";


//Payments_Methods Routs

$route['admin/payment_methods'] = "payment_methods/index";
$route['admin/payment_methods/add'] = "payment_methods/add";
$route['admin/payment_methods/save'] = "payment_methods/save";
$route['admin/payment_methods/(:num)/edit'] = "payment_methods/edit/$1";
$route['admin/payment_methods/(:num)/update'] = "payment_methods/update/$1";
$route['admin/payment_methods/(:num)/delete'] = "payment_methods/delete/$1";


// bills routes
$route['admin/bills'] = "bills";
$route['admin/bills/add'] = "bills/add";
$route['admin/bills/add/(:num)'] = "bills/add/$1";
$route['admin/bills/save'] = "bills/save";
$route['admin/bills/(:num)/edit'] = "bills/edit/$1";
$route['admin/bills/(:num)/update'] = "bills/update/$1";
$route['admin/bills/(:num)/delete'] = "bills/delete/$1";

// bills routes 2
$route['admin/accounting/bills'] = "bills";
$route['admin/accounting/bills/add'] = "bills/add";
$route['admin/accounting/bills/save'] = "bills/save";
$route['admin/accounting/bills/(:num)/edit'] = "bills/edit/$1";
$route['admin/accounting/bills/(:num)/update'] = "bills/update/$1";
$route['admin/accounting/bills/(:num)/delete'] = "bills/delete/$1";

// sales routes
$route['admin/sales'] = "sales";
$route['admin/sales/add'] = "sales/add";
$route['admin/sales/add/(:num)'] = "sales/add/$1";
$route['admin/sales/save'] = "sales/save";
$route['admin/sales/(:num)/edit'] = "sales/edit/$1";
$route['admin/sales/(:num)/update'] = "sales/update/$1";
$route['admin/sales/(:num)/delete'] = "sales/delete/$1";

// sales routes 2
$route['admin/accounting/sales'] = "sales";
$route['admin/accounting/sales/add'] = "sales/add";
$route['admin/accounting/sales/save'] = "sales/save";
$route['admin/accounting/sales/(:num)/edit'] = "sales/edit/$1";
$route['admin/accounting/sales/(:num)/update'] = "sales/update/$1";
$route['admin/accounting/sales/(:num)/delete'] = "sales/delete/$1";




// factoring routes
$route['admin/factoring'] = "factoring";
$route['admin/factoring/add'] = "factoring/add";
$route['admin/factoring/add/(:num)'] = "factoring/add/$1";
$route['admin/factoring/save'] = "factoring/save";
$route['admin/factoring/(:num)/edit'] = "factoring/edit/$1";
$route['admin/factoring/(:num)/update'] = "factoring/update/$1";
$route['admin/factoring/(:num)/delete'] = "factoring/delete/$1";

// factoring routes 2
$route['admin/accounting/factoring'] = "factoring";
$route['admin/accounting/factoring/add'] = "factoring/add";
$route['admin/accounting/factoring/save'] = "factoring/save";
$route['admin/accounting/factoring/(:num)/edit'] = "factoring/edit/$1";
$route['admin/accounting/factoring/(:num)/update'] = "factoring/update/$1";
$route['admin/accounting/factoring/(:num)/delete'] = "factoring/delete/$1";




// bank routes
$route['admin/bank'] = "bank";
$route['admin/bank/add'] = "bank/add";
$route['admin/bank/add/(:num)'] = "bank/add/$1";
$route['admin/bank/save'] = "bank/save";
$route['admin/bank/(:num)/edit'] = "bank/edit/$1";
$route['admin/bank/(:num)/update'] = "bank/update/$1";
$route['admin/bank/(:num)/delete'] = "bank/delete/$1";

// bank routes 2
$route['admin/accounting/bank'] = "bank";
$route['admin/accounting/bank/add'] = "bank/add";
$route['admin/accounting/bank/save'] = "bank/save";
$route['admin/accounting/bank/(:num)/edit'] = "bank/edit/$1";
$route['admin/accounting/bank/(:num)/update'] = "bank/update/$1";
$route['admin/accounting/bank/(:num)/delete'] = "bank/delete/$1";




// reportss routes
$route['admin/reportss'] = "reportss";
$route['admin/reportss/add'] = "reportss/add";
$route['admin/reportss/add/(:num)'] = "reportss/add/$1";
$route['admin/reportss/save'] = "reportss/save";
$route['admin/reportss/(:num)/edit'] = "reportss/edit/$1";
$route['admin/reportss/(:num)/update'] = "reportss/update/$1";
$route['admin/reportss/(:num)/delete'] = "reportss/delete/$1";

// reportss routes 2
$route['admin/accounting/reportss'] = "reportss";
$route['admin/accounting/reportss/add'] = "reportss/add";
$route['admin/accounting/reportss/save'] = "reportss/save";
$route['admin/accounting/reportss/(:num)/edit'] = "reportss/edit/$1";
$route['admin/accounting/reportss/(:num)/update'] = "reportss/update/$1";
$route['admin/accounting/reportss/(:num)/delete'] = "reportss/delete/$1";





$route['admin/invoices'] = "invoices";

//admin bookings pages


// $route['admin/accounting'] = "accounting";
$route['admin/calls'] = "calls";
$route['admin/quote_requests'] = "quote_requests";
// $route['admin/jobs'] = "jobs";
$route['admin/newsletters'] = "newsletter";
$route['admin/files'] = "filemanagement";
$route['admin/tasks'] = "task";

$route['admin/calls/add'] = "calls/add";
$route['admin/quote_requests/add'] = "quote_requests/add";
// $route['admin/jobs/add'] = "jobs/add";
$route['admin/files/add'] = "filemanagement/add";
$route['admin/tasks/add'] = "task/add";
$route['admin/newsletters/add'] = "newsletter/add";

$route['insertadminchathistory'] = "Messages/insertadminreply";
// $route['admin/samplerequest/add'] = "samplerequest/add";
$route['admin/calls/store'] = "calls/store";
$route['admin/quote_requests/store'] = "quote_requests/store";

// $route['admin/samplerequest/store'] = "samplerequest/store";
// $route['admin/jobs/store'] = "jobs/store";
$route['admin/files/store'] = "filemanagement/store";
$route['admin/newsletters/store'] = "newsletter/store";
$route['admin/tasks/store'] = "task/store";

$route['admin/calls/(:num)/edit'] = "calls/edit/$1";
$route['admin/quote_requests/(:num)/edit'] = "quote_requests/edit/$1";
$route['admin/quote_requests/(:num)/view'] = "quote_requests/view/$1";
// $route['admin/jobs/(:num)/edit'] = "jobs/edit/$1";

$route['admin/filemanagement/fileSearchData'] = "filemanagement/fileSearchData";
$route['admin/filemanagement/delete'] = "filemanagement/delete";
$route['admin/filemanagement/delete_comment'] = "filemanagement/delete_comment";
$route['admin/filemanagement/add_sender'] = "filemanagement/add_sender";
$route['admin/filemanagement/delete_sender'] = "filemanagement/delete_sender";
$route['admin/filemanagement/get_user_by_destination'] = "filemanagement/get_user_by_destination";
$route['admin/filemanagement/delete_multiple']="filemanagement/delete_multiple";
$route['admin/files/(:num)/edit'] = "filemanagement/edit/$1";
$route['admin/files/search_user'] = "filemanagement/search_user/$1";
$route['admin/newsletters/(:num)/edit'] = "newsletter/edit/$1";
$route['admin/tasks/(:num)/edit'] = "task/edit/$1";

$route['admin/calls/(:num)/update'] = "calls/update/$1";
$route['admin/quote_requests/(:num)/update'] = "quote_requests/update/$1";
// $route['admin/jobs/(:num)/update'] = "jobs/update/$1";
$route['admin/files/(:num)/update'] = "filemanagement/update/$1";
$route['admin/newsletters/(:num)/update'] = "newsletter/update/$1";
$route['admin/tasks/(:num)/update'] = "task/update/$1";

$route['admin/calls/(:num)/reply'] = "calls/reply/$1";
$route['admin/quote_requests/(:num)/reply'] = "quote_requests/reply/$1";
// $route['admin/jobs/(:num)/reply'] = "jobs/reply/$1";
$route['admin/newsletter/(:num)/reply'] = "newsletter/reply/$1";

$route['admin/calls/(:num)/delete'] = "calls/delete/$1";
$route['admin/quote_requests/(:num)/delete'] = "quote_requests/delete/$1";
// $route['admin/jobs/(:num)/delete'] = "jobs/delete/$1";
$route['admin/files/(:num)/delete'] = "filemanagement/delete/$1";
$route['admin/newsletters/(:num)/delete'] = "newsletter/delete/$1";
$route['admin/tasks/(:num)/delete'] = "task/delete/$1";


//Supports
$route['admin/supports'] = "supports";
$route['admin/supports/add'] = "supports/add";
$route['admin/supports/store'] = "supports/store";
$route['admin/supports/(:num)/edit'] = "supports/edit/$1";
$route['admin/supports/closeTicket/(:num)'] = "supports/closeTicket/$1";
$route['admin/supports/(:num)/update'] = "supports/update/$1";
$route['admin/supports/(:num)/reply'] = "supports/reply/$1";
$route['admin/supports/(:num)/delete'] = "supports/delete/$1";

$route['supports/login'] = "supports/login";
$route['supports/home'] = "supports/home";
$route['user/supports/ticket_add'] = "supports/ticket_add";
$route['user/supports/(:num)/ticket_edit'] = "supports/ticket_edit/$1";
$route['user/supports/closeTicket/(:num)'] = "supports/closeTicket/$1";
$route['user/supports/(:num)/ticket_update'] = "supports/ticket_update/$1";
$route['user/supports/(:num)/ticket_reply'] = "supports/ticket_reply/$1";
$route['user/supports/(:num)/ticket_delete'] = "supports/ticket_delete/$1";

// Newsletter
$route['admin/newsletters/config'] = "newsletter/config";
$route['admin/newsletters/config_board'] = "newsletter/config_board";
$route['admin/newsletters/fetchTemplateDescription'] = "newsletter/fetchTemplateDescription";
$route['admin/newsletters/newsletterConfigData'] = "newsletter/newsletterConfigData";
$route['admin/newsletters/fetchNewslettersData'] = "newsletter/fetchNewslettersData";
$route['admin/newsletters/searchNewslettersData'] = "newsletter/searchNewslettersData";



// SERVICES
/*$route['handi-pro'] = "services/3";
$route['handi-prive'] = "services/4";
$route['handi-shuttle'] = "services/5";
$route['handi-scolaire'] = "services/6";
$route['handi-medical'] = "services/7";
$route['handi-business'] = "services/8";
$route['handi-senior'] = "services/9";
$route['handi-event'] = "services/10";
$route['handi-voyage'] = "services/11";*/
$route['services/(:any)/(:any)'] = "services/$2";
$route['reservation'] = "welcome/onlineBooking";
$route['prices'] = "welcome/prices";
$route['faq'] = "welcome/faqs";
$route['fleet'] = "welcome/fleet";
$route['testimonials'] = "welcome/testimonials";
$route['zones'] = "welcome/zones";
$route['contact'] = "welcome/contactUs";
// $route['supporttickets'] = "welcome/contactUs";
// $route['conditions-d-utilisation'] = "welcome/termsServices";
$route['legals/terms-of-service'] = "welcome/termsService";
$route['legals/privacy-policy'] = "welcome/privacyPolicy";
// $route['politique-de-vie-privee'] = "welcome/privacyPolicy";
// $route['mentions-legales'] = "welcome/legalNotice";
$route['legals/legal-notice'] = "welcome/legalNotice";
$route['legals/driver-agreement'] = "welcome/driverAgreement";
$route['legals/partner-agreement'] = "welcome/partnerAgreement";
$route['legals/refund-policy'] = "welcome/refundPolicy";


//QUICK REPLIES

$route['admin/quick_replies'] = "quick_replies";
$route['admin/quick_replies/add'] = "quick_replies/add";
$route['admin/quick_replies/(:num)/edit'] = "quick_replies/edit/$1";
$route['admin/quick_replies/(:num)/update'] = "quick_replies/update/$1";
$route['admin/quick_replies/(:num)/delete'] = "quick_replies/delete/$1";

//CONFIGURATIONS

$route['admin/accounting'] = "accounting";
$route['admin/configurations/add'] = "configurations/add";
$route['admin/configurations/(:num)/edit'] = "configurations/edit/$1";
$route['admin/configurations/(:num)/update'] = "configurations/update/$1";
$route['admin/configurations/(:num)/delete'] = "configurations/delete/$1";

//PIPLINE

$route['admin/pipline'] = "pipline";
$route['admin/pipline/add'] = "pipline/add";
$route['admin/pipline/(:num)/edit'] = "pipline/edit/$1";
$route['admin/pipline/(:num)/update'] = "pipline/update/$1";
$route['admin/pipline/(:num)/delete'] = "pipline/delete/$1";
$route['admin/pipline/deletepiplines'] = "pipline/delete_data";

//accounting

// $route['admin/accounting'] = "Accounting";
// $route['admin/accounting/add'] = "configurations/add";
// $route['admin/accounting/(:num)/edit'] = "configurations/edit/$1";
// $route['admin/accounting/(:num)/update'] = "configurations/update/$1";
// $route['admin/accounting/(:num)/delete'] = "configurations/delete/$1";

//SMTP

$route['admin/smtp'] = "smtp";
$route['admin/smtp/(:num)/update'] = "smtp/update/$1";

//Notifications

$route['admin/notifications'] = "notifications";
$route['admin/notifications/add'] = "notifications/add";
$route['admin/notifications/(:num)/edit'] = "notifications/edit/$1";
$route['admin/notifications/(:num)/update'] = "notifications/update/$1";
$route['admin/notifications/delete'] = "notifications/delete";

//router for clients
$route['admin/clients'] = "clients";
$route['admin/clients/add'] = "clients/add";
$route['admin/clients/fetchClientsData'] = "clients/fetchClientsData";
$route['admin/clients/add'] = "clients/add";
$route['admin/clients/clientSearchData'] = "clients/clientSearchData";
$route['admin/clients/clientExcelDownload'] = "clients/clientExcelDownload";
// $route['admin/clients/edit/(:num)'] = "clients/edit/$1";
$route['admin/clients/(:num)/edit'] = "clients/edit/$1";
$route['admin/clients/(:num)/update'] = "clients/update/$1";

// Client Requests
$route['client/request/add'] = "client/add";
$route['client/request/(:num)/edit'] = "client/edit/$1";
$route['client/request/(:num)/update'] = "client/update/$1";
$route['client/request/(:num)/reply'] = "client/reply/$1";
$route['client/request/(:num)/delete'] = "client/delete/$1";

$route['admin/google_api'] = "google_api";
$route['admin/configclients'] = "clients/configurations";
$route['admin/getclienttype'] = "clients/getclienttypeproccess";
$route['admin/addclienttype'] = "clients/addclienttypeproccess";
$route['admin/updateclienttype'] = "clients/updateclienttypeproccess";
$route['admin/deleteclienttype'] = "clients/deleteclienttypeproccess";
$route['admin/getclientdisable'] = "clients/getclientdisableproccess";
$route['admin/addclientdisable'] = "clients/addclientdisableproccess";
$route['admin/updateclientdisable'] = "clients/updateclientdisableproccess";
$route['admin/deleteclientdisable'] = "clients/deleteclientdisableproccess";
$route['admin/getclientstatus'] = "clients/getclientstatusproccess";
$route['admin/addclientstatus'] = "clients/addclientstatusproccess";
$route['admin/updateclientstatus'] = "clients/updateclientstatusproccess";
$route['admin/deleteclientstatus'] = "clients/deleteclientstatusproccess";
$route['admin/getclientpayment'] = "clients/getclientpaymentproccess";
$route['admin/addclientpayment'] = "clients/addclientpaymentproccess";
$route['admin/updateclientpayment'] = "clients/updateclientpaymentproccess";
$route['admin/deleteclientpayment'] = "clients/deleteclientpaymentproccess";
$route['admin/getclientdelay'] = "clients/getclientdelayproccess";
$route['admin/addclientdelay'] = "clients/addclientdelayproccess";
$route['admin/updateclientdelay'] = "clients/updateclientdelayproccess";
$route['admin/deleteclientdelay'] = "clients/deleteclientdelayproccess";


//route for drivers
$route['admin/drivers'] = "drivers";
$route['admin/drivers/add'] = "drivers/add";
$route['admin/drivers/get_ajax_driver_profile'] = "drivers/get_ajax_driver_profile";
$route['admin/drivers/ajax_get_region_listing'] = "drivers/ajax_get_region_listing";
$route['admin/drivers/fetchDriversData'] = "drivers/fetchDriversData";
$route['admin/drivers/driverSearchData'] = "drivers/driverSearchData";
$route['admin/drivers/driverExcelDownload'] = "clients/driverExcelDownload";
$route['admin/drivers/edit'] = "drivers/edit";
$route['admin/drivers/(:num)/update'] = "drivers/update/$1";
$route['admin/drivers/delete'] = "drivers/delete";
$route['admin/drivers/ajax_get_cities_listing'] = "drivers/ajax_get_cities_listing";
$route['admin/drivers/driverdocumentAdd'] = "drivers/driverdocumentAdd";
$route['admin/drivers/driverdocumentedit'] = "drivers/driverdocumentEdit";
$route['admin/drivers/deletedriverdocument'] = "drivers/driverdocumentDelete";
$route['admin/drivers/get_ajax_driverdocument'] = "drivers/get_ajax_driverdocument";

$route['admin/configdrivers'] = "drivers/configurations";
$route['admin/getdriverstatus'] = "drivers/getstatusprocess";
$route['admin/adddriversstatus'] = "drivers/addstatusprocess";
$route['admin/updatedriversstatus'] = "drivers/updatestatusprocess";
$route['admin/deletedriversstatus'] = "drivers/deletestatusprocess";
$route['admin/getcivility'] = "drivers/getcivilityprocess";
$route['admin/addcivilite'] = "drivers/addciviliteprocess";
$route['admin/updatecivilite'] = "drivers/updateciviliteprocess";
$route['admin/deletecivilite'] = "drivers/deleteciviliteprocess";
$route['admin/adddriverspost'] = "drivers/addpostprocess";
$route['admin/getdriverspost'] = "drivers/getpostprocess";
$route['admin/updatedriverspost'] = "drivers/updatepostprocess";
$route['admin/deletedriverspost'] = "drivers/deletepostprocess";
$route['admin/adddriverspattern'] = "drivers/addpatternprocess";
$route['admin/getdriverspattern'] = "drivers/getpatternprocess";
$route['admin/updatedriverspattern'] = "drivers/updatepatternprocess";
$route['admin/deletedriverspattern'] = "drivers/deletepatternprocess";
$route['admin/adddrivercontract'] = "drivers/addageprocess";
$route['admin/getdriverscontract'] = "drivers/getageprocess";
$route['admin/updatedriverscontract'] = "drivers/updateageprocess";
$route['admin/deletedriverscontract'] = "drivers/deleteageprocess";
$route['admin/adddriversnature'] = "drivers/addseriesprocess";
$route['admin/getdriversnature'] = "drivers/getseriesprocess";
$route['admin/updatedriversnature'] = "drivers/updateseriesprocess";
$route['admin/deletedriversnature'] = "drivers/deleteseriesprocess";
$route['admin/adddrivershours'] = "drivers/addboiteprocess";
$route['admin/getdrivershours'] = "drivers/getboiteprocess";
$route['admin/updatedrivershous'] = "drivers/updateboiteprocess";
$route['admin/deletedrivershours'] = "drivers/deleteboiteprocess";
$route['admin/adddriverstype'] = "drivers/addfuelprocess";
$route['admin/getdriverstype'] = "drivers/getfuelprocess";
$route['admin/updatedriverstype'] = "drivers/updatefuelprocess";
$route['admin/deletedriverstype'] = "drivers/deletefuelprocess";


//route for partners
$route['admin/partners'] = "partners";
$route['admin/partners/add'] = "partners/add";
$route['admin/partners/fetchPartnersData'] = "partners/fetchPartnersData";
$route['admin/partners/partnerSearchData'] = "partners/partnerSearchData";

//route for jobseekers
// renamed jobs to applications for admin on 16-April-2020
// renamed applications to jobseekers for admin on 02-June-2020
$route['admin/jobseekers'] = "jobseekers";
$route['admin/jobseekers/add'] = "jobseekers/add";
$route['admin/jobseekers/store'] = "jobseekers/store";
$route['admin/jobseekers/fetchJobseekersData'] = "jobseekers/fetchJobseekersData";
$route['admin/jobseekers/jobseekerSearchData'] = "jobseekers/jobseekerSearchData";
$route['admin/jobseekers/(:num)/edit'] = "jobseekers/edit/$1";
$route['admin/jobseekers/(:num)/update'] = "jobseekers/update/$1";
$route['admin/jobseekers/(:num)/reply'] = "jobseekers/reply/$1";
$route['admin/jobseekers/(:num)/delete'] = "jobseekers/delete/$1";


$route['admin/applications'] = "applications";
$route['admin/applications/add'] = "applications/add";
$route['admin/applications/store'] = "applications/store";
$route['admin/applications/(:num)/edit'] = "applications/edit/$1";
$route['admin/applications/(:num)/update'] = "applications/update/$1";
$route['admin/applications/(:num)/reply'] = "applications/reply/$1";
$route['admin/applications/(:num)/delete'] = "applications/delete/$1";

//router for cars
$route['admin/cars'] = "cars";
$route['admin/configcars'] = "cars/configurations";
$route['admin/addstatus'] = "cars/addstatusprocess";
$route['admin/addpost'] = "cars/addpostprocess";
$route['admin/addpattern'] = "cars/addpatternprocess";
$route['admin/addage'] = "cars/addageprocess";
$route['admin/addseries'] = "cars/addseriesprocess";
$route['admin/addboite'] = "cars/addboiteprocess";
$route['admin/addfuel'] = "cars/addfuelprocess";
$route['admin/addmail'] = "cars/addmailprocess";
$route['admin/addcolor'] = "cars/addcolorprocess";
$route['admin/addnature'] = "cars/addnatureprocess";
$route['admin/getstaut'] = "cars/getstatusprocess";
$route['admin/getpost'] = "cars/getpostprocess";
$route['admin/getpattern'] = "cars/getpatternprocess";
$route['admin/getage'] = "cars/getageprocess";
$route['admin/getseries'] = "cars/getseriesprocess";
$route['admin/getboite'] = "cars/getboiteprocess";
$route['admin/getfuel'] = "cars/getfuelprocess";
$route['admin/getmail'] = "cars/getmailprocess";
$route['admin/getcolor'] = "cars/getcolorprocess";
$route['admin/getnature'] = "cars/getnatureprocess";
$route['admin/updatestatus'] = "cars/updatestatusprocess";
$route['admin/updatepost'] = "cars/updatepostprocess";
$route['admin/updatepattern'] = "cars/updatepatternprocess";
$route['admin/updateage'] = "cars/updateageprocess";
$route['admin/updateseries'] = "cars/updateseriesprocess";
$route['admin/updateboite'] = "cars/updateboiteprocess";
$route['admin/updatefuel'] = "cars/updatefuelprocess";
$route['admin/updatemail'] = "cars/updatemailprocess";
$route['admin/updatecolor'] = "cars/updatecolorprocess";
$route['admin/updatenature'] = "cars/updatenatureprocess";
$route['admin/deletestatus'] = "cars/deletestatusprocess";
$route['admin/deletepost'] = "cars/deletepostprocess";
$route['admin/deletepattern'] = "cars/deletepatternprocess";
$route['admin/deleteage'] = "cars/deleteageprocess";
$route['admin/deleteseries'] = "cars/deleteseriesprocess";
$route['admin/deleteboite'] = "cars/deleteboiteprocess";
$route['admin/deletefuel'] = "cars/deletefuelprocess";
$route['admin/deletemail'] = "cars/deletemailprocess";
$route['admin/deletecolor'] = "cars/deletecolorprocess";
$route['admin/deletenature'] = "cars/deletenatureprocess";
$route['admin/cars/fetchCarsData'] = "cars/fetchCarsData";


//POPUPS

$route['admin/popups'] = "popups/edit";
$route['admin/popups/update'] = "popups/update";

//CALLBACK

$route['admin/callback/(:num)/edit'] = "callback/edit/$1";
$route['admin/callback/(:num)/update'] = "callback/update/$1";

//CALLBACK

$route['admin/settings/(:num)'] = "settings1/edit/$1";
$route['admin/settings/(:num)/update'] = "settings1/update/$1";

//REMINDERS

$route['admin/reminders'] = "reminders";
$route['admin/reminders/add'] = "reminders/add";
$route['admin/reminders/(:num)/edit'] = "reminders/edit/$1";
$route['admin/reminders/(:num)/update'] = "reminders/update/$1";
$route['admin/reminders/delete'] = "reminders/delete";

// admin Drivers Request
$route['admin/drivers_requests'] = "drivers_request";
$route['admin/drivers_requests/add'] = "drivers_request/add";
$route['admin/drivers_requests/absence_request_store']   = "drivers_request/absence_request_store";
$route['admin/drivers_requests/vacation_request_store']  = "drivers_request/vacation_request_store";
$route['admin/drivers_requests/salary_request_store']    = "drivers_request/salary_request_store";
$route['admin/drivers_requests/notes_request_store']    = "drivers_request/notes_request_store";
$route['admin/drivers_requests/(:num)/edit'] = "drivers_request/edit/$1";
$route['admin/drivers_requests/(:num)/salary_notes_update'] = "drivers_request/salary_notes_update/$1";
$route['admin/drivers_requests/(:num)/absence_vacation_update'] = "drivers_request/absence_vacation_update/$1";
$route['admin/drivers_requests/search'] = "drivers_request/search";

$route['admin/weather/(:num)/update'] = "weather/update/$1";
$route['admin/weather'] = "weather";

// $route['admin/bookings/(:num)/update'] = "bookings/update/$1";
// $route['admin/bookings/(:num)/delete'] = "bookings/delete/$1";

//sample
//admin sample pages
$route['admin/sample'] = "sample";
$route['admin/sample/add'] = "sample/add";
// $route['admin/bookings/save'] = "bookings/save";
// $route['admin/bookings/(:num)/edit'] = "bookings/edit/$1";
// $route['admin/bookings/(:num)/update'] = "bookings/update/$1";
// $route['admin/bookings/(:num)/delete'] = "bookings/delete/$1";

// admin infractions
$route['admin/infraction'] = "infraction";
$route['admin/infraction/add'] = "infraction/add";
// $route['changechatboxstatus'] = "Messages/changechatboxstatus";

// example: '/en/about' -> use controller 'about'
$route['^fr/(.+)$'] = "$1";
$route['^en/(.+)$'] = "$1";
$route['^pt/(.+)$'] = "$1";
$route['^de/(.+)$'] = "$1";
 
// '/en' and '/fr' -> use default controller
$route['^fr$'] = $route['default_controller'];
$route['^en$'] = $route['default_controller'];
$route['^pt$'] = $route['default_controller'];
$route['^de$'] = $route['default_controller']; 
/* End of file routes.php */
/* Location: ./application/config/routes.php */

$route['insertchatdatadetails'] = 'Messages/insertchatdata';

//Admin Booking Config
$route['admin/booking_config'] = "booking_config";
//BOOKING POI
$route['admin/booking_config/poiadd'] = "booking_config/poiAdd";
$route['admin/booking_config/poiedit'] = "booking_config/poiEdit";
$route['admin/booking_config/poidel'] = "booking_config/poiDel";
//Ajax
$route['admin/booking_config/get_ajax_poi'] = "booking_config/get_ajax_poi";

//BOOKING Package
$route['admin/booking_config/packageadd'] = "booking_config/packageAdd";
$route['admin/booking_config/packageedit'] = "booking_config/packageEdit";
$route['admin/booking_config/packagedelete'] = "booking_config/packageDel";
//Ajax
$route['admin/booking_config/get_ajax_package'] = "booking_config/get_ajax_package";




$route['admin/booking_config/vatadd'] = "booking_config/vatadd";
$route['admin/booking_config/vatedit'] = "booking_config/vatEdit";
$route['admin/booking_config/deleteVat'] = "booking_config/deleteVat";
$route['admin/booking_config/get_ajax_vat'] = "booking_config/get_ajax_vat";

$route['admin/booking_config/discountadd'] = "booking_config/discountAdd";
$route['admin/booking_config/discountedit'] = "booking_config/discountEdit";
$route['admin/booking_config/deletediscount'] = "booking_config/discountDelete";
$route['admin/booking_config/get_ajax_discount'] = "booking_config/get_ajax_discount";

$route['admin/booking_config/servicecatadd'] = "booking_config/servicecatAdd";
$route['admin/booking_config/servicecatedit'] = "booking_config/servicecatEdit";
$route['admin/booking_config/deleteservicecat'] = "booking_config/servicecatDelete";
$route['admin/booking_config/get_ajax_service_cat'] = "booking_config/get_ajax_service_cat";

$route['admin/booking_config/serviceadd'] = "booking_config/serviceAdd";
$route['admin/booking_config/serviceedit'] = "booking_config/serviceEdit";
$route['admin/booking_config/deleteservice'] = "booking_config/serviceDelete";
$route['admin/booking_config/get_ajax_service'] = "booking_config/get_ajax_service";

$route['admin/booking_config/clientCatadd'] = "booking_config/clientCatAdd";
$route['admin/booking_config/clientCatedit'] = "booking_config/clientCatEdit";
$route['admin/booking_config/deleteclientCat'] = "booking_config/clientCatDelete";
$route['admin/booking_config/get_ajax_client_cat'] = "booking_config/get_ajax_client_cat";
$route['admin/booking_config/carCatadd'] = "booking_config/carCatAdd";
$route['admin/booking_config/carCatedit'] = "booking_config/carCatEdit";
$route['admin/booking_config/deletecarCat'] = "booking_config/carCatDelete";
$route['admin/booking_config/get_ajax_car_cat'] = "booking_config/get_ajax_car_cat";

$route['admin/booking_config/rideCatadd'] = "booking_config/rideCatAdd";
$route['admin/booking_config/rideCatedit'] = "booking_config/rideCatEdit";
$route['admin/booking_config/deleterideCat'] = "booking_config/rideCatDelete";
$route['admin/booking_config/get_ajax_ride_cat'] = "booking_config/get_ajax_ride_cat";


$route['admin/booking_config/carpriceedit'] = "booking_config/price_car_Edit";
$route['admin/booking_config/cartypepriceedit'] = "booking_config/price_car_type_Edit";
$route['admin/booking_config/vanriceedit'] = "booking_config/price_van_Edit";
$route['admin/booking_config/minibuspriceedit'] = "booking_config/price_minibus_Edit";
$route['admin/booking_config/cartpmrpriceedit'] = "booking_config/price_cartpmr_Edit";
$route['admin/booking_config/vantpmrpriceedit'] = "booking_config/price_vantpmr_Edit";
$route['admin/booking_config/minibustpmrpriceedit'] = "booking_config/price_minibustpmr_Edit";

$route['admin/booking_config/carpriceadd'] = "booking_config/price_car_Add";
$route['admin/booking_config/cartypepriceadd'] = "booking_config/price_car_type_Add";
$route['admin/booking_config/vanpriceadd'] = "booking_config/price_van_Add";
$route['admin/booking_config/minibuspriceadd'] = "booking_config/price_minibus_Add";
$route['admin/booking_config/cartpmrpriceadd'] = "booking_config/price_cartpmr_Add";
$route['admin/booking_config/vantpmrpriceadd'] = "booking_config/price_vantpmr_Add";
$route['admin/booking_config/minibustpmrpriceadd'] = "booking_config/price_minibustpmr_Add";

$route['admin/booking_config/carpricedelete'] = "booking_config/price_car_Delete";
$route['admin/booking_config/cartypepricedelete'] = "booking_config/price_car_type_Delete";
$route['admin/booking_config/vanpricedelete'] = "booking_config/price_van_Delete";
$route['admin/booking_config/minibuspricedelete'] = "booking_config/price_minibus_Delete";
$route['admin/booking_config/cartpmrpricedelete'] = "booking_config/price_cartpmr_Delete";
$route['admin/booking_config/vantpmrpricedelete'] = "booking_config/price_vantpmr_Delete";
$route['admin/booking_config/minibustpmrpricedelete'] = "booking_config/price_minibustpmr_Delete";
$route['admin/booking_config/get_ajax_price_car'] = "booking_config/get_ajax_price_car";
$route['admin/booking_config/get_ajax_price_car_type'] = "booking_config/get_ajax_price_car_type";
$route['admin/booking_config/get_ajax_price_van'] = "booking_config/get_ajax_price_van";
$route['admin/booking_config/get_ajax_price_minibus'] = "booking_config/get_ajax_price_minibus";
$route['admin/booking_config/get_ajax_price_cartpmr'] = "booking_config/get_ajax_price_cartpmr";
$route['admin/booking_config/get_ajax_price_vantpmr'] = "booking_config/get_ajax_price_vantpmr";
$route['admin/booking_config/get_ajax_price_minibustpmr'] = "booking_config/get_ajax_price_minibustpmr";

$route['admin/booking_config/paymentmethodadd'] = "booking_config/paymentmethodAdd";
$route['admin/booking_config/paymentmethodedit'] = "booking_config/paymentmethodEdit";
$route['admin/booking_config/deletepaymentmethod'] = "booking_config/paymentmethodDelete";
$route['admin/booking_config/get_ajax_payment_method'] = "booking_config/get_ajax_payment_method";

# Booking Now
$route['booking_details'] = "book_now";


#
#
#added by rahul689
#
#

$route['admin/cms/services'] = "cms/services";
$route['admin/cms/service_category'] = "cms/service_category";
$route['admin/cms/ajax_service_category'] = "cms/ajax_service_category";
$route['admin/cms/ajax_get_category'] = "cms/ajax_get_category";
$route['admin/cms/ajax_get_all_cms_listing'] = "cms/ajax_get_all_cms_listing";
$route['admin/cms/ajax_delete_cms_listing'] = "cms/ajax_delete_cms_listing";
$route['admin/cms/ajax_get_cms_listing'] = "cms/ajax_get_cms_listing";
$route['admin/cms/ajax_update_cms_listing'] = "cms/ajax_update_cms_listing";

$route['admin/cms/services_list_ajax_data_set'] = "cms/services_list_ajax_data_set";
$route['admin/cms/category_list_ajax_data_set'] = "cms/category_list_ajax_data_set";
$route['admin/cms/add_cms_banner'] = "cms/add_cms_banner";
$route['admin/cms/add_cms_flash_news'] = "cms/add_cms_flash_news";
$route['admin/cms/ajax_get_cms_sliders'] = "cms/ajax_get_cms_sliders";



$route['admin/cms/legals'] = "cms/legals";
$route['admin/cms/info_bulls'] = "cms/info_bulls";
$route['admin/cms/add_infobull'] = "cms/add_infobull";

$route['admin/cms/news'] = "cms/news";
$route['admin/cms/prices'] = "cms/prices";
$route['admin/cms/seo'] = "cms/seo";
$route['admin/cms/cms_config'] = "cms/cms_config";

$route['admin/cms/fleet'] = "cms/fleet";
$route['admin/cms/faq'] = "cms/faq";
$route['admin/cms/testimonials'] = "cms/testimonials";
$route['admin/cms/downloads'] = "cms/downloads";
$route['admin/cms/partners'] = "cms/partners";
$route['admin/cms/banners'] = "cms/banners";
$route['admin/cms/flash_info'] = "cms/flash_info";
$route['admin/cms/slide_show'] = "cms/slide_show";
$route['admin/cms/add_cms_slider'] = "cms/add_cms_slider";
$route['admin/cms/ajax_delete_cms_sliders'] = "cms/ajax_delete_cms_sliders";



$route['admin/cms/services_cat'] = "cms/services_cat";
$route['admin/cms/downloads_cat'] = "cms/downloads_cat";
$route['admin/cms/news_cat'] = "cms/news_cat";
$route['admin/cms/car_cat'] = "cms/car_cat";
$route['admin/cms/faq_cat'] = "cms/faq_cat";


$route['admin/cms/zones'] = "cms/zones";
$route['admin/cms/ajax_get_region_listing'] = "cms/ajax_get_region_listing";
$route['admin/cms/ajax_get_cities_listing'] = "cms/ajax_get_cities_listing";
$route['admin/cms/add_cms_seo'] = "cms/add_cms_seo";


//$route['legales'] = "welcome/legaldefault";
$route['legales/(:any)'] = "welcome/legaldefault";


$route['downloads'] = "welcome/downloads";
$route['blog/(:any)/(:any)'] = "blog/index/$2";

//added by sultan
$route['booking'] = 'book_now/booking';
$route['booking_save'] = 'book_now/booking_save';
$route['booking_calprice_save'] = 'book_now/booking_calprice_save';
$route['booking_quote'] = 'book_now/booking_quote';
$route['booking_payment'] = 'book_now/booking_payment';
$route['getbookingservices'] = 'book_now/getbookingservices';
$route['getpoidatabypackage']='book_now/getpoidatabypackage';
$route['getdroppoidatabypackage']='book_now/getdroppoidatabypackage';
$route['getpoidatabycategory']='book_now/getpoidatabycategory';
$route['admin/bookings']="bookings";
$route['client/identification']='client/identification';
$route['book_now/saveclientbooking']='book_now/saveclientbooking';
$route['savepassenger'] = 'book_now/savepassenger';
$route['removepassenger'] = 'book_now/removepassenger';
$route['getpromodiscount']='book_now/getpromodiscount';
$route['confirmbooking']='book_now/confirmbooking';
$route['admin/booking_config/add_notworking_days'] = "booking_config/add_notworking_days";
$route['admin/booking_config/remove_notworking_days'] = "booking_config/remove_notworking_days";
$route['admin/booking_config/add_discount_periods'] = "booking_config/add_discount_periods";
$route['admin/booking_config/remove_discount_periods'] = "booking_config/remove_discount_periods";
$route['admin/booking_config/delete_car_config'] = "booking_config/delete_car_config";
$route['admin/booking_config/statutadd'] = "booking_config/statutadd";
$route['admin/booking_config/get_ajax_statut'] = "booking_config/get_ajax_statut";
$route['admin/booking_config/statutedit'] = "booking_config/statutEdit";
$route['admin/booking_config/deleteStatut'] = "booking_config/deleteStatut";
$route['admin/bookings/delete_booking_record']="bookings/delete_booking_record";
$route['admin/bookings/get_bookings_bysearch']="bookings/get_bookings_bysearch";
$route['admin/bookings/booking_save']="bookings/booking_save";
$route['admin/discounts'] = "discount";
$route['admin/discounts/discountadd'] = "discount/discountAdd";
$route['admin/discounts/discountedit'] = "discount/discountEdit";
$route['admin/discounts/deletediscount'] = "discount/discountDelete";
$route['admin/discounts/get_ajax_discount'] = "discount/get_ajax_discount";
$route['admin/discounts/get_service'] = "discount/get_service";
$route['admin/booking_config/get_poidata'] = "booking_config/get_poidata";
$route['admin/booking_config/upload_editor_image'] = "booking_config/upload_editor_image";
$route['admin/booking_config/apiadd'] = "booking_config/apiadd";
$route['admin/booking_config/get_ajax_api'] = "booking_config/get_ajax_api";
$route['admin/booking_config/apiedit'] = "booking_config/apiEdit";
$route['admin/booking_config/deleteApi'] = "booking_config/deleteApi";
$route['client/passengers'] = "passenger";
$route['client/passengers/passengeradd'] = "passenger/passengerAdd";
$route['client/passengers/passengeredit'] = "passenger/passengerEdit";
$route['client/passengers/deletepassenger'] = "passenger/passengerDelete";
$route['client/passengers/get_ajax_passenger'] = "passenger/get_ajax_passenger";
$route['admin/booking_config/pricestatusadd'] = "booking_config/pricestatusadd";
$route['admin/booking_config/get_ajax_pricestatus'] = "booking_config/get_ajax_pricestatus";
$route['admin/booking_config/pricestatusedit'] = "booking_config/pricestatusEdit";
$route['admin/booking_config/deletepricestatus'] = "booking_config/deletepricestatus";
$route['admin/booking_config/disablecatadd'] = "booking_config/disablecatadd";
$route['admin/booking_config/get_ajax_disablecat'] = "booking_config/get_ajax_disablecat";
$route['admin/booking_config/disablecatedit'] = "booking_config/disablecatEdit";
$route['admin/booking_config/deleteDisablecat'] = "booking_config/deleteDisablecat";
$route['add_client_passenger'] = 'book_now/add_client_passenger';
$route['add_client_aspassenger'] = 'book_now/add_client_aspassenger';
$route['client/bookings'] = "clientbookings";
$route['admin/bookings/getpassengersbyclient']='bookings/getpassengersbyclient';
$route['admin/bookings/savebookingpassenger']='bookings/savebookingpassenger';
$route['admin/bookings/removebookingpassenger']='bookings/removebookingpassenger';
$route['admin/bookings/booking_save']='bookings/booking_save';
$route['admin/bookings/get_ajax_bookings']='bookings/get_ajax_bookings';
$route['admin/bookings/update_booking_data']='bookings/update_booking_data';
$route['admin/bookings/save_booking_confirm']='bookings/save_booking_confirm';
$route['admin/bookings/get_ajax_detail_bookings']='bookings/get_ajax_detail_bookings';
$route['admin/quotes']="quotes";
$route['admin/quotes/get_ajax_quotes']='quotes/get_ajax_quotes';
$route['admin/quotes/delete_quotes_record']="quotes/delete_quotes_record";
$route['admin/quotes/update_booking_data']='quotes/update_booking_data';
$route['admin/quotes/booking_save']="quotes/booking_save";
$route['admin/quotes/save_booking_confirm']='quotes/save_booking_confirm';
$route['admin/quotes/get_ajax_detail_bookings']='quotes/get_ajax_detail_bookings';
$route['admin/quotes/quotes_pdf/(:num)']='quotes/quotes_pdf/$1';
$route['admin/booking_config/quoteconfigadd'] = "booking_config/quoteconfigAdd";
$route['admin/booking_config/quoteconfigedit'] = "booking_config/quoteconfigEdit";
$route['admin/booking_config/deletequoteconfig'] = "booking_config/quoteconfigDelete";
$route['admin/booking_config/get_ajax_quoteconfig'] = "booking_config/get_ajax_quoteconfig";
$route['admin/quotes/get_bookings_bysearch']="quotes/get_bookings_bysearch";
$route['admin/booking_config/invoicestatusadd'] = "booking_config/invoicestatusadd";
$route['admin/booking_config/invoicestatusedit'] = "booking_config/invoicestatusEdit";
$route['admin/invoices/get_bookings_bysearch']="invoices/get_bookings_bysearch";
$route['admin/quotes/notification_pdf/(:num)/(:num)']='quotes/notification_pdf/$1/$2';
$route['admin/quotes/reminder_pdf/(:num)/(:num)/(:num)']='quotes/reminder_pdf/$1/$2/$3';
$route['admin/invoices/get_ajax_invoices']='invoices/get_ajax_invoices';
$route['admin/invoices/delete_invoices_record']="invoices/delete_invoices_record";
$route['admin/invoices/update_booking_data']='invoices/update_booking_data';
$route['admin/invoices/booking_save']="invoices/booking_save";
$route['admin/invoices/save_booking_confirm']='invoices/save_booking_confirm';
$route['admin/invoices/get_ajax_detail_bookings']='invoices/get_ajax_detail_bookings';
$route['admin/invoices/invoices_pdf/(:num)']='invoices/invoices_pdf/$1';
$route['admin/invoices/notification_pdf/(:num)/(:num)']='invoices/notification_pdf/$1/$2';
$route['admin/invoices/reminder_pdf/(:num)/(:num)/(:num)']='invoices/reminder_pdf/$1/$2/$3';
$route['admin/quotes/update_booking_customprice']='quotes/update_booking_customprice';
$route['admin/quotes/add_quote_notification']='quotes/add_quote_notification';
$route['admin/quotes/add_quote_reminder']='quotes/add_quote_reminder';
$route['admin/invoices/update_booking_customprice']='invoices/update_booking_customprice';
$route['admin/invoices/add_invoice_notification']='invoices/add_invoice_notification';
$route['admin/invoices/add_invoice_reminder']='invoices/add_invoice_reminder';
$route['admin/quotes/getbookingsbyclient']='quotes/getbookingsbyclient';
$route['admin/bookings/update_booking_customprice']='bookings/update_booking_customprice';
$route['admin/quotes/addcustomreminders']='quotes/addcustomreminders';
$route['admin/quotes/removecustomreminders']='quotes/removecustomreminders';
$route['admin/invoices/addcustomreminders']='invoices/addcustomreminders';
$route['admin/invoices/removecustomreminders']='invoices/removecustomreminders';
$route['admin/invoices/addcustompayment']='invoices/addcustompayment';
$route['admin/invoices/removecustompayment']='invoices/removecustompayment';
$route['admin/invoices/getbookingsbyclient']='invoices/getbookingsbyclient';
$route['admin/bookings/notification_pdf/(:num)/(:num)']='bookings/notification_pdf/$1/$2';
$route['admin/bookings/addcustomreminders']='bookings/addcustomreminders';
$route['admin/bookings/removecustomreminders']='bookings/removecustomreminders';
$route['admin/bookings/add_booking_notification']='bookings/add_booking_notification';
$route['admin/bookings/addbookingquoterecord']='bookings/addbookingquoterecord';
$route['admin/quotes/addquoteinvoicerecord']='quotes/addquoteinvoicerecord';
$route['admin/emailcronjob/sendcronemails']='emailcronjob/sendcronemails';
$route['admin/drivers/addcustomreminders']='drivers/addcustomreminders';
$route['admin/drivers/removecustomreminders']='drivers/removecustomreminders';
$route['admin/drivers/notification_pdf/(:num)/(:num)/(:num)']='drivers/notification_pdf/$1/$2/$3';
$route['admin/drivers/reminder_pdf/(:num)/(:num)/(:num)/(:num)']='drivers/reminder_pdf/$1/$2/$3/$4';
$route['admin/cars/addcustomreminders']='cars/addcustomreminders';
$route['admin/cars/removecustomreminders']='cars/removecustomreminders';
//car config routes
$route['admin/car_config'] = "car_config";
$route['admin/car_config/carStatusadd'] = "car_config/carStatusadd";
$route['admin/car_config/carStatusEdit'] = "car_config/carStatusEdit";
$route['admin/car_config/deleteCarStatus'] = "car_config/deleteCarStatus";
$route['admin/car_config/get_ajax_carStatus'] = "car_config/get_ajax_carStatus";

//car civilite
$route['admin/car_config/civiliteadd'] = "car_config/civiliteAdd";
$route['admin/car_config/civiliteedit'] = "car_config/civiliteEdit";
$route['admin/car_config/deletecivilite'] = "car_config/civiliteDelete";
$route['admin/car_config/get_ajax_civilite'] = "car_config/get_ajax_civilite";

//car marque
$route['admin/car_config/marqueadd'] = "car_config/marqueAdd";
$route['admin/car_config/marqueedit'] = "car_config/marqueEdit";
$route['admin/car_config/deletemarque'] = "car_config/marqueDelete";
$route['admin/car_config/get_ajax_marque'] = "car_config/get_ajax_marque";

//car modele
$route['admin/car_config/modeleadd'] = "car_config/modeleAdd";
$route['admin/car_config/modeleedit'] = "car_config/modeleEdit";
$route['admin/car_config/deletemodele'] = "car_config/modeleDelete";
$route['admin/car_config/get_ajax_modele'] = "car_config/get_ajax_modele";

//car age
$route['admin/car_config/ageadd'] = "car_config/ageAdd";
$route['admin/car_config/ageedit'] = "car_config/ageEdit";
$route['admin/car_config/deleteage'] = "car_config/ageDelete";
$route['admin/car_config/get_ajax_age'] = "car_config/get_ajax_age";



//car serie
$route['admin/car_config/serieadd'] = "car_config/serieAdd";
$route['admin/car_config/serieedit'] = "car_config/serieEdit";
$route['admin/car_config/deleteserie'] = "car_config/serieDelete";
$route['admin/car_config/get_ajax_serie'] = "car_config/get_ajax_serie";

//car burant
$route['admin/car_config/burantadd'] = "car_config/burantAdd";
$route['admin/car_config/burantedit'] = "car_config/burantEdit";
$route['admin/car_config/deleteburant'] = "car_config/burantDelete";
$route['admin/car_config/get_ajax_burant'] = "car_config/get_ajax_burant";


//car courroie
$route['admin/car_config/courroieadd'] = "car_config/courroieAdd";
$route['admin/car_config/courroieedit'] = "car_config/courroieEdit";
$route['admin/car_config/deletecourroie'] = "car_config/courroieDelete";
$route['admin/car_config/get_ajax_courroie'] = "car_config/get_ajax_courroie";


//car couleur
$route['admin/car_config/couleuradd'] = "car_config/couleurAdd";
$route['admin/car_config/couleuredit'] = "car_config/couleurEdit";
$route['admin/car_config/deletecouleur'] = "car_config/couleurDelete";
$route['admin/car_config/get_ajax_couleur'] = "car_config/get_ajax_couleur";


//car nature
$route['admin/car_config/natureadd'] = "car_config/natureAdd";
$route['admin/car_config/natureedit'] = "car_config/natureEdit";
$route['admin/car_config/deletenature'] = "car_config/natureDelete";
$route['admin/car_config/get_ajax_nature'] = "car_config/get_ajax_nature";


//car type
$route['admin/car_config/typeadd'] = "car_config/typeAdd";
$route['admin/car_config/typeedit'] = "car_config/typeEdit";
$route['admin/car_config/deletetype'] = "car_config/typeDelete";
$route['admin/car_config/get_ajax_type'] = "car_config/get_ajax_type";



//car boite
$route['admin/car_config/boiteadd'] = "car_config/boiteAdd";
$route['admin/car_config/boiteedit'] = "car_config/boiteEdit";
$route['admin/car_config/deleteboite'] = "car_config/boiteDelete";
$route['admin/car_config/get_ajax_boite'] = "car_config/get_ajax_boite";


//car config routes
$route['admin/cars/add'] = "cars/add";
$route['admin/cars/edit'] = "cars/edit";
$route['admin/cars/delete'] = "cars/delete";
$route['admin/cars/get_ajax_data'] = "cars/get_ajax_data";

//12/12/2020
$route['admin/client_config'] = "client_config";

//client status
$route['admin/client_config/statusAdd'] = "client_config/statusAdd";
$route['admin/client_config/statusEdit'] = "client_config/statusEdit";
$route['admin/client_config/statusDelete'] = "client_config/statusDelete";
$route['admin/client_config/get_ajax_status_data'] = "client_config/get_ajax_status_data";

//client civilite
$route['admin/client_config/civiliteAdd'] = "client_config/civiliteAdd";
$route['admin/client_config/civiliteEdit'] = "client_config/civiliteEdit";
$route['admin/client_config/civiliteDelete'] = "client_config/civiliteDelete";
$route['admin/client_config/get_ajax_civilite_data'] = "client_config/get_ajax_civilite_data";


//client delay
$route['admin/client_config/delayAdd'] = "client_config/delayAdd";
$route['admin/client_config/delayEdit'] = "client_config/delayEdit";
$route['admin/client_config/delayDelete'] = "client_config/delayDelete";
$route['admin/client_config/get_ajax_delay_data'] = "client_config/get_ajax_delay_data";



//client payment
$route['admin/client_config/paymentAdd'] = "client_config/paymentAdd";
$route['admin/client_config/paymentEdit'] = "client_config/paymentEdit";
$route['admin/client_config/paymentDelete'] = "client_config/paymentDelete";
$route['admin/client_config/get_ajax_payment_data'] = "client_config/get_ajax_payment_data";


//client type
$route['admin/client_config/typeAdd'] = "client_config/typeAdd";
$route['admin/client_config/typeEdit'] = "client_config/typeEdit";
$route['admin/client_config/typeDelete'] = "client_config/typeDelete";
$route['admin/client_config/get_ajax_type_data'] = "client_config/get_ajax_type_data";




//client profile
$route['admin/clients/profileAdd'] = "clients/profileAdd";
$route['admin/clients/profileEdit'] = "clients/profileEdit";
$route['admin/clients/profileDelete'] = "clients/profileDelete";
$route['admin/clients/get_ajax_profile_data'] = "clients/get_ajax_profile_data";


//client contact
$route['admin/clients/contactAdd'] = "clients/contactAdd";
$route['admin/clients/contactEdit'] = "clients/contactEdit";
$route['admin/clients/contactDelete'] = "clients/contactDelete";
$route['admin/clients/get_ajax_contact_data'] = "clients/get_ajax_contact_data";


//client method
$route['admin/clients/methodAdd'] = "clients/methodAdd";
$route['admin/clients/methodEdit'] = "clients/methodEdit";
$route['admin/clients/methodDelete'] = "clients/methodDelete";
$route['admin/clients/get_ajax_method_data'] = "clients/get_ajax_method_data";


//client passenger
$route['admin/clients/passengerAdd'] = "clients/passengerAdd";
$route['admin/clients/passengerEdit'] = "clients/passengerEdit";
$route['admin/clients/passengerDelete'] = "clients/passengerDelete";
$route['admin/clients/get_ajax_passenger_data'] = "clients/get_ajax_passenger_data";


//client ride
$route['admin/clients/rideAdd'] = "clients/rideAdd";
$route['admin/clients/rideEdit'] = "clients/rideEdit";
$route['admin/clients/rideDelete'] = "clients/rideDelete";
$route['admin/clients/get_ajax_ride_data'] = "clients/get_ajax_ride_data";


//client booking
$route['admin/clients/bookingAdd'] = "clients/bookingAdd";
$route['admin/clients/bookingEdit'] = "clients/bookingEdit";
$route['admin/clients/bookingDelete'] = "clients/bookingDelete";
$route['admin/clients/get_ajax_booking_data'] = "clients/get_ajax_booking_data";


//client quote
$route['admin/clients/quoteAdd'] = "clients/quoteAdd";
$route['admin/clients/quoteEdit'] = "clients/quoteEdit";
$route['admin/clients/quoteDelete'] = "clients/quoteDelete";
$route['admin/clients/get_ajax_quote_data'] = "clients/get_ajax_quote_data";


//client contract
$route['admin/clients/contractAdd'] = "clients/contractAdd";
$route['admin/clients/contractEdit'] = "clients/contractEdit";
$route['admin/clients/contractDelete'] = "clients/contractDelete";
$route['admin/clients/get_ajax_contract_data'] = "clients/get_ajax_contract_data";


//client invoice
$route['admin/clients/invoiceAdd'] = "clients/invoiceAdd";
$route['admin/clients/invoiceEdit'] = "clients/invoiceEdit";
$route['admin/clients/invoiceDelete'] = "clients/invoiceDelete";
$route['admin/clients/get_ajax_invoice_data'] = "clients/get_ajax_invoice_data";


//client payment
$route['admin/clients/paymentAdd'] = "clients/paymentAdd";
$route['admin/clients/paymentEdit'] = "clients/paymentEdit";
$route['admin/clients/paymentDelete'] = "clients/paymentDelete";
$route['admin/clients/get_ajax_payment_data'] = "clients/get_ajax_payment_data";


//client support
$route['admin/clients/supportAdd'] = "clients/supportAdd";
$route['admin/clients/supportEdit'] = "clients/supportEdit";
$route['admin/clients/supportDelete'] = "clients/supportDelete";
$route['admin/clients/get_ajax_support_data'] = "clients/get_ajax_support_data";

//client message
$route['admin/clients/messageAdd'] = "clients/messageAdd";
$route['admin/clients/messageEdit'] = "clients/messageEdit";
$route['admin/clients/messageDelete'] = "clients/messageDelete";
$route['admin/clients/get_ajax_message_data'] = "clients/get_ajax_message_data";

$route['admin/dispatch_planning'] = "dispatch";
$route['admin/dispatch_timesheet'] = "dispatch/dispatch_timesheet";
$route['admin/dispatch_reports'] = "dispatch/dispatch_reports";
$route['admin/gross_bills'] = "dispatch/gross_bills";
$route['admin/factoring'] = "dispatch/factoring";

//driver profile
$route['admin/drivers/profileAdd'] = "drivers/profileAdd";
$route['admin/drivers/profileEdit'] = "drivers/profileEdit";
$route['admin/drivers/profileDelete'] = "drivers/profileDelete";
$route['admin/drivers/get_ajax_profile_data'] = "drivers/get_ajax_profile_data";
$route['admin/drivers/get_drivers_bysearch'] = "drivers/get_drivers_bysearch";

//car profile
$route['admin/cars/profileAdd'] = "cars/profileAdd";
$route['admin/cars/profileEdit'] = "cars/profileEdit";
$route['admin/cars/profileDelete'] = "cars/profileDelete";
$route['admin/cars/get_ajax_profile_data'] = "cars/get_ajax_profile_data";

//export excel
$route['admin/bookings/export_excel_file'] = "bookings/export_excel_file";
$route['admin/quotes/export_excel_file'] = "quotes/export_excel_file";
$route['admin/invoices/export_excel_file'] = "invoices/export_excel_file";

$route['admin/cars/add_car_image']    = "cars/add_car_image";
$route['admin/cars/notification_pdf'] = "cars/notification_pdf";
$route['admin/cars/reminder_pdf']     = "cars/reminder_pdf";

$route['admin/drivers/sendaccessdetail'] = "drivers/sendaccessdetail";
$route['admin/drivers/adddrivernotification']     = "drivers/adddrivernotification";
$route['admin/drivers/adddriverreminder']     = "drivers/adddriverreminder";

$route['admin/cars/addcarnotification']     = "cars/addcarnotification";
$route['admin/cars/addcarreminder']     = "cars/addcarreminder";
$route['admin/cars/get_cars_bysearch']     = "cars/get_cars_bysearch";

//car controller technique
$route['admin/cars/techniqueAdd']     = "cars/techniqueAdd";
$route['admin/cars/techniqueEdit']     = "cars/techniqueEdit";
$route['admin/cars/techniqueDelete']     = "cars/techniqueDelete";
$route['admin/cars/techniqueAjax']     = "cars/techniqueAjax";

//car assurance
$route['admin/cars/assuranceAdd']     = "cars/assuranceAdd";
$route['admin/cars/assuranceEdit']     = "cars/assuranceEdit";
$route['admin/cars/assuranceDelete']     = "cars/assuranceDelete";
$route['admin/cars/assuranceAjax']     = "cars/assuranceAjax";


//car entretien
$route['admin/cars/entretienAdd']     = "cars/entretienAdd";
$route['admin/cars/entretienEdit']     = "cars/entretienEdit";
$route['admin/cars/entretienDelete']     = "cars/entretienDelete";
$route['admin/cars/entretienAjax']     = "cars/entretienAjax";

//car reparation
$route['admin/cars/reparationAdd']     = "cars/reparationAdd";
$route['admin/cars/reparationEdit']     = "cars/reparationEdit";
$route['admin/cars/reparationDelete']     = "cars/reparationDelete";
$route['admin/cars/reparationAjax']     = "cars/reparationAjax";

//car sinistre
$route['admin/cars/sinistreAdd']     = "cars/sinistreAdd";
$route['admin/cars/sinistreEdit']     = "cars/sinistreEdit";
$route['admin/cars/sinistreDelete']     = "cars/sinistreDelete";
$route['admin/cars/sinistreAjax']     = "cars/sinistreAjax";

//car cout
$route['admin/cars/coutAdd']     = "cars/coutAdd";
$route['admin/cars/coutEdit']     = "cars/coutEdit";
$route['admin/cars/coutDelete']     = "cars/coutDelete";
$route['admin/cars/coutAjax']     = "cars/coutAjax";

//partner profile
$route['admin/partners/profileAdd'] = "partners/profileAdd";
$route['admin/partners/profileEdit'] = "partners/profileEdit";
$route['admin/partners/profileDelete'] = "partners/profileDelete";
$route['admin/partners/get_ajax_profile_data'] = "partners/get_ajax_profile_data";
$route['admin/partners/get_partners_bysearch'] = "partners/get_partners_bysearch";

//partner config 
$route['admin/partner_config'] = "partner_config";

//partner status
$route['admin/partner_config/partnerStatusadd'] = "partner_config/partnerStatusadd";
$route['admin/partner_config/partnerStatusEdit'] = "partner_config/partnerStatusEdit";
$route['admin/partner_config/deletePartnerStatus'] = "partner_config/deletePartnerStatus";
$route['admin/partner_config/get_ajax_partnerStatus'] = "partner_config/get_ajax_partnerStatus";

//partner civilite
$route['admin/partner_config/civiliteadd'] = "partner_config/civiliteAdd";
$route['admin/partner_config/civiliteedit'] = "partner_config/civiliteEdit";
$route['admin/partner_config/deletecivilite'] = "partner_config/civiliteDelete";
$route['admin/partner_config/get_ajax_civilite'] = "partner_config/get_ajax_civilite";

$route['admin/partners/ajax_get_region_listing'] = "partners/ajax_get_region_listing";
$route['admin/partners/ajax_get_cities_listing'] = "partners/ajax_get_cities_listing";
$route['admin/partners/addcustomreminders']='partners/addcustomreminders';
$route['admin/partners/removecustomreminders']='partners/removecustomreminders';
$route['admin/partners/notification_pdf/(:num)/(:num)/(:num)']='partners/notification_pdf/$1/$2/$3';
$route['admin/partners/reminder_pdf/(:num)/(:num)/(:num)/(:num)']='partners/reminder_pdf/$1/$2/$3/$4';

$route['admin/partners/sendaccessdetail']     = "partners/sendaccessdetail";
$route['admin/partners/addpartnernotification']     = "partners/addpartnernotification";
$route['admin/partners/addpartnerreminder']     = "partners/addpartnerreminder";

//partner type
$route['admin/partner_config/typeadd'] = "partner_config/typeAdd";
$route['admin/partner_config/typeedit'] = "partner_config/typeEdit";
$route['admin/partner_config/deletetype'] = "partner_config/typeDelete";
$route['admin/partner_config/get_ajax_type'] = "partner_config/get_ajax_type";

//partners document
$route['admin/partners/documentAdd']     = "partners/documentAdd";
$route['admin/partners/documentEdit']     = "partners/documentEdit";
$route['admin/partners/documentDelete']     = "partners/documentDelete";
$route['admin/partners/documentAjax']     = "partners/documentAjax";
$route['admin/partners/addpartnernote']     = "partners/addpartnernote";
$route['admin/partners/deletepartnernote']     = "partners/deletepartnernote";
//end by sultan

//driver config 
$route['admin/driver_config'] = "driver_config";
//BOOKING driver status


$route['admin/driver_config/driverStatusadd'] = "driver_config/driverStatusadd";
$route['admin/driver_config/driverStatusEdit'] = "driver_config/driverStatusEdit";
$route['admin/driver_config/deleteDriverStatus'] = "driver_config/deleteDriverStatus";
$route['admin/driver_config/get_ajax_driverStatus'] = "driver_config/get_ajax_driverStatus";

//driver civilite
$route['admin/driver_config/civiliteadd'] = "driver_config/civiliteAdd";
$route['admin/driver_config/civiliteedit'] = "driver_config/civiliteEdit";
$route['admin/driver_config/deletecivilite'] = "driver_config/civiliteDelete";
$route['admin/driver_config/get_ajax_civilite'] = "driver_config/get_ajax_civilite";

//driver post
$route['admin/driver_config/postadd'] = "driver_config/postAdd";
$route['admin/driver_config/postedit'] = "driver_config/postEdit";
$route['admin/driver_config/deletepost'] = "driver_config/postDelete";
$route['admin/driver_config/get_ajax_post'] = "driver_config/get_ajax_post";

//driver pattern
$route['admin/driver_config/patternadd'] = "driver_config/patternAdd";
$route['admin/driver_config/patternedit'] = "driver_config/patternEdit";
$route['admin/driver_config/deletepattern'] = "driver_config/patternDelete";
$route['admin/driver_config/get_ajax_pattern'] = "driver_config/get_ajax_pattern";


//driver contract
$route['admin/driver_config/contractadd'] = "driver_config/contractAdd";
$route['admin/driver_config/contractedit'] = "driver_config/contractEdit";
$route['admin/driver_config/deletecontract'] = "driver_config/contractDelete";
$route['admin/driver_config/get_ajax_contract'] = "driver_config/get_ajax_contract";

//driver nature
$route['admin/driver_config/natureadd'] = "driver_config/natureAdd";
$route['admin/driver_config/natureedit'] = "driver_config/natureEdit";
$route['admin/driver_config/deletenature'] = "driver_config/natureDelete";
$route['admin/driver_config/get_ajax_nature'] = "driver_config/get_ajax_nature";

//driver hours
$route['admin/driver_config/hoursadd'] = "driver_config/hoursAdd";
$route['admin/driver_config/hoursedit'] = "driver_config/hoursEdit";
$route['admin/driver_config/deletehours'] = "driver_config/hoursDelete";
$route['admin/driver_config/get_ajax_hours'] = "driver_config/get_ajax_hours";

//driver type
$route['admin/driver_config/typeadd'] = "driver_config/typeAdd";
$route['admin/driver_config/typeedit'] = "driver_config/typeEdit";
$route['admin/driver_config/deletetype'] = "driver_config/typeDelete";
$route['admin/driver_config/get_ajax_type'] = "driver_config/get_ajax_type";


$route['admin/cms/tutorials'] = "cms/tutorials";
$route['admin/cms/documentations'] = "cms/documentations";
$route['admin/cms/udapts'] = "cms/updates";

$route['admin/cms/tutorials_cat'] = "cms/tutorials_cat";
$route['admin/cms/documentations_cat'] = "cms/documentations_cat";
$route['admin/cms/udapts_cat'] = "cms/udapts_cat";

$route['admin/tutorials'] = "auth/tutorials";
$route['admin/documentations'] = "auth/documentations";
$route['admin/updats'] = "auth/updats";
$route['admin/cms/tutorials'] = "cms/tutorials";
$route['admin/cms/tutorials/(:num)/view'] = "cms/single_tutorials/$1";
$route['admin/cms/documentations/(:num)/view'] = "cms/single_documentations/$1";

