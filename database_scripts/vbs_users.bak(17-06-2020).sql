-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 17, 2020 at 03:11 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_fi_ecab`
--

-- --------------------------------------------------------

--
-- Table structure for table `vbs_users`
--

CREATE TABLE `vbs_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `civility` text NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company_name` text NOT NULL,
  `city` text NOT NULL,
  `zipcode` text NOT NULL,
  `address` text NOT NULL,
  `dob` date DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `image` varchar(512) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `link` text,
  `department_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `address1` text,
  `country` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vbs_users`
--

INSERT INTO `vbs_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `last_login`, `active`, `civility`, `first_name`, `last_name`, `company_name`, `city`, `zipcode`, `address`, `dob`, `phone`, `image`, `role_id`, `gender`, `link`, `department_id`, `created_at`, `created_on`, `address1`, `country`, `fax`, `region`) VALUES
(1, '127.0.0.1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '', 'admin@ecab.app', '', NULL, 1450673723, '', 1480964484, 1, 'Mr', 'Super', 'Admin', '', '', '', '', NULL, '7386896258', '', 4, 'Mr', 'adsfadf', 2, '2019-10-22 07:39:04', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL),
(204, '103.216.236.151', 'vaibhav radadiya', '5744efa36450560ada6fdba2fe204895', NULL, 'vaibhav@gmail.com', NULL, NULL, NULL, '', 1582973848, 1, 'Mr', 'vaibhav', 'radadiya', '', '', '', '', NULL, '', NULL, 6, NULL, NULL, 0, '2020-02-29 10:57:28', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL),
(205, '122.164.245.167', 'Client eCab', '25d55ad283aa400af464c76d713c07ad', NULL, 'client@ecab.app', NULL, NULL, NULL, '', 1583349576, 1, 'Mr', 'Client', 'eCab', 'Client Company', 'city1', '110011', 'address1', '2020-02-03', '1234567890', NULL, 6, NULL, NULL, 0, '2020-03-04 19:19:36', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL),
(206, '122.164.245.167', 'Driver eCab', '25d55ad283aa400af464c76d713c07ad', NULL, 'driver@ecab.app', NULL, NULL, NULL, '', 1583350682, 1, 'Mr', 'Driver', 'eCab', 'Driver Company', 'city1', '110099', 'address1', '2019-09-17', '1234567890', NULL, 7, NULL, NULL, 0, '2020-03-04 19:38:02', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL),
(207, '122.164.245.167', 'Partner Demo', '25d55ad283aa400af464c76d713c07ad', NULL, 'partner@ecab.app', NULL, NULL, NULL, '', 1583434157, 1, 'Mr', 'Partner', 'Demo', 'Partner Company', 'city1', '110022', 'address1', '2019-03-18', '1234567890', NULL, 9, NULL, NULL, 0, '2020-03-05 18:49:17', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL),
(210, '122.164.245.167', 'Jobseeker Demo', '25d55ad283aa400af464c76d713c07ad', NULL, 'jobseeker@ecab.app', NULL, NULL, NULL, '', 1583437901, 1, 'Mr', 'Jobseeker', 'Demo', 'Jobs Company', 'city1', '110033', 'address1', '2019-08-05', '1234567890', NULL, 8, NULL, NULL, 0, '2020-03-05 19:51:41', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL),
(211, '127.0.0.1', 'Support', '25d55ad283aa400af464c76d713c07ad', NULL, 'support@ecab.app', NULL, NULL, NULL, NULL, NULL, 1, 'Mr', 'Support', 'Demo', 'Support Company', '', '', '', NULL, '12345667890', NULL, 10, NULL, NULL, 0, '2020-03-13 14:29:31', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL),
(213, '112.135.11.113', 'Test tetstsfgshg', '25f9e794323b453885f5181f1b624d0b', NULL, 'koralage@gmail.com', NULL, NULL, NULL, NULL, 1585721282, 1, 'Mr', 'Test', 'tetstsfgshg', '', 'colombo', '15000', '94866 & ghdgahg\ncolombo', NULL, '0716329219', NULL, 6, NULL, NULL, NULL, '2020-04-01 06:08:02', '2020-04-01 06:08:02', NULL, NULL, NULL, NULL),
(214, '23.108.45.130', 'eric@talkwithwebvisitor.com', '5fbc4bce472045a2a4c70d94aaabc103', NULL, 'eric@talkwithwebvisitor.com', NULL, NULL, NULL, NULL, NULL, NULL, 'Mrs', 'Eric', 'Eric', 'talkwithwebvisitor.com', '', '', '', NULL, '416-385-3200', NULL, 10, NULL, NULL, 2, '2020-04-01 10:33:52', '2020-04-01 10:33:52', NULL, NULL, NULL, NULL),
(215, '172.83.40.52', 'amy@leggingshut.co', '0d8d7e9a02887595081380cf3a31c98b', NULL, 'amy@leggingshut.co', NULL, NULL, NULL, NULL, NULL, NULL, 'Mrs', 'Amy', 'Amy', 'Soila Viles', '', '', '', NULL, '(07) 5696 9487', NULL, 10, NULL, NULL, 2, '2020-04-06 22:35:16', '2020-04-06 22:35:16', NULL, NULL, NULL, NULL),
(216, '223.225.76.78', 'rajat.bhatia.7150@gmail.com', 'f002cb8062606dd75193dd4d69a65282', NULL, 'rajat.bhatia.7150@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, 'Mr', 'Rajat', 'Bhatia', '', '', '', '', NULL, '8800549424', NULL, 10, NULL, NULL, 2, '2020-04-15 13:26:21', '2020-04-15 13:26:21', NULL, NULL, NULL, NULL),
(219, '39.46.48.243', 'aamir ali1', '25d55ad283aa400af464c76d713c07ad', NULL, 'aamirali777@gmail.com', NULL, NULL, NULL, NULL, 1589973280, 1, '', 'aamir', 'ali', '', '', '', '', NULL, '0300123456', NULL, 6, NULL, NULL, NULL, '2020-05-20 11:14:40', '2020-05-20 11:14:40', NULL, NULL, NULL, NULL),
(220, '39.46.110.228', 'bilal khan', '25d55ad283aa400af464c76d713c07ad', NULL, 'bilalkhan786@gmail.com', NULL, NULL, NULL, NULL, 1590381124, 1, '', 'bilal', 'khan', '', '', '', '', NULL, '1234567899', NULL, 6, NULL, NULL, NULL, '2020-05-25 04:32:04', '2020-05-25 04:32:04', NULL, NULL, NULL, NULL),
(221, '185.128.25.158', 'hacker@shuttleinc.com', '7ccbf97dab9a5139f041d591d026917b', NULL, 'hacker@shuttleinc.com', NULL, NULL, NULL, NULL, NULL, NULL, 'Mrs', 'Laurel', 'Laurel', 'Laurel Gurney', '', '', '', NULL, '(08) 9279 4359', NULL, 10, NULL, NULL, 2, '2020-06-11 04:16:40', '2020-06-11 04:16:40', NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `vbs_users`
--
ALTER TABLE `vbs_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `vbs_users`
--
ALTER TABLE `vbs_users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=222;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
