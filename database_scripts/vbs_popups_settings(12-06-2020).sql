-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2020 at 01:05 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_fi_ecab`
--

-- --------------------------------------------------------

--
-- Table structure for table `vbs_popups_settings`
--

CREATE TABLE `vbs_popups_settings` (
  `id` int(11) NOT NULL,
  `status1` tinyint(1) NOT NULL DEFAULT '1',
  `status2` tinyint(1) NOT NULL DEFAULT '1',
  `status3` tinyint(1) NOT NULL DEFAULT '1',
  `status4` tinyint(4) NOT NULL DEFAULT '1',
  `status5` tinyint(4) NOT NULL DEFAULT '1',
  `name1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `name2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `name3` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `name4` varchar(255) DEFAULT 'NULL',
  `name5` varchar(255) DEFAULT NULL,
  `auto_open1` tinyint(1) NOT NULL DEFAULT '0',
  `auto_open2` tinyint(1) NOT NULL DEFAULT '0',
  `auto_open3` tinyint(1) NOT NULL DEFAULT '0',
  `auto_open4` tinyint(4) NOT NULL DEFAULT '0',
  `auto_open5` tinyint(4) NOT NULL DEFAULT '0',
  `position1` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Top, 2=Left, 3=Right',
  `position2` tinyint(1) NOT NULL DEFAULT '2' COMMENT '1=Top, 2=Left, 3=Right',
  `position3` tinyint(1) NOT NULL DEFAULT '3' COMMENT '1=Top, 2=Left, 3=Right',
  `position4` tinyint(4) NOT NULL DEFAULT '4' COMMENT '1=Top, 2=Left, 3=Right, 4=Bottom Right,',
  `position5` tinyint(4) NOT NULL DEFAULT '5' COMMENT '1=Top, 2=Left, 3=Right, 4=Bottom, 5=Bottom Left',
  `success_message1` text CHARACTER SET utf8,
  `success_message2` text CHARACTER SET utf8,
  `success_message3` text CHARACTER SET utf8,
  `success_message4` text CHARACTER SET utf8,
  `success_message1_french` text CHARACTER SET utf8,
  `success_message2_french` text CHARACTER SET utf8,
  `success_message3_french` text CHARACTER SET utf8,
  `success_message4_french` text CHARACTER SET utf8,
  `success_message5` text CHARACTER SET utf8,
  `success_message5_french` text CHARACTER SET utf8,
  `request_closing_days_1` tinyint(1) NOT NULL DEFAULT '7',
  `request_closing_days_2` tinyint(1) NOT NULL DEFAULT '7',
  `request_closing_days_3` tinyint(1) NOT NULL DEFAULT '7',
  `request_closing_days_4` tinyint(4) NOT NULL DEFAULT '7',
  `opening_delay` tinyint(4) NOT NULL DEFAULT '7'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vbs_popups_settings`
--

INSERT INTO `vbs_popups_settings` (`id`, `status1`, `status2`, `status3`, `status4`, `status5`, `name1`, `name2`, `name3`, `name4`, `name5`, `auto_open1`, `auto_open2`, `auto_open3`, `auto_open4`, `auto_open5`, `position1`, `position2`, `position3`, `position4`, `position5`, `success_message1`, `success_message2`, `success_message3`, `success_message4`, `success_message1_french`, `success_message2_french`, `success_message3_french`, `success_message4_french`, `success_message5`, `success_message5_french`, `request_closing_days_1`, `request_closing_days_2`, `request_closing_days_3`, `request_closing_days_4`, `opening_delay`) VALUES
(1, 1, 1, 1, 1, 1, 'DEMANDE DE RAPPEL (CLIQUEZ ICI!)', 'CANDIDATURE CHAUFFEUR (CLIQUEZ ICI!)', 'DEMANDE DE DEVIS (CLIQUEZ ICI!)', '<i class=\'fa fa-question-circle\'></i> Assistance', '! COOKIES POLICY', 0, 0, 1, 0, 0, 1, 2, 3, 5, 0, '<p>Merci&nbsp;de&nbsp;patienter...un&nbsp;operateur&nbsp;va&nbsp;prendre&nbsp;votre&nbsp;appel...en cas d&#39;&eacute;chec d&#39;appel, Sachez que nous avons&nbsp;enregistr&eacute; vos coordonn&eacute;es et un operateur&nbsp;vous rapellera d&eacute;s que possible.</p>\r\n', '<p>Nous&nbsp;avons&nbsp;bien&nbsp;re&ccedil;u&nbsp;votre&nbsp;candidature,&nbsp;et&nbsp;nous&nbsp;vous&nbsp;en&nbsp;remercions,&nbsp;sachez&nbsp;que&nbsp;si&nbsp;votre&nbsp;profil&nbsp;nous&nbsp;convient,&nbsp;nous&nbsp;allons&nbsp;vous&nbsp;convoquer&nbsp;par&nbsp;email&nbsp;&agrave;&nbsp;un&nbsp;entretien&nbsp;d&#39;embauche.&nbsp;Cordialement</p>\r\n', '<p>Nous&nbsp;avons&nbsp;bien&nbsp;re&ccedil;u&nbsp;votre&nbsp;message&nbsp;et&nbsp;nous vous en&nbsp;remercions,&nbsp;un&nbsp;de&nbsp;nos&nbsp;op&eacute;rateurs&nbsp;reviendra&nbsp;vers&nbsp;vous&nbsp;d&eacute;s&nbsp;que&nbsp;possible.</p>\r\n', '<p>Message sent successfully!!</p>\r\n', '<p>Merci&nbsp;de&nbsp;patienter...un&nbsp;operateur&nbsp;va&nbsp;prendre&nbsp;votre&nbsp;appel...en cas d&#39;&eacute;chec d&#39;appel, Sachez que nous avons&nbsp;enregistr&eacute; vos coordonn&eacute;es et un operateur&nbsp;vous rapellera d&eacute;s que possible.</p>\r\n', '<p>Nous&nbsp;avons&nbsp;bien&nbsp;re&ccedil;u&nbsp;votre&nbsp;candidature,&nbsp;et&nbsp;nous&nbsp;vous&nbsp;en&nbsp;remercions,&nbsp;sachez&nbsp;que&nbsp;si&nbsp;votre&nbsp;profil&nbsp;nous&nbsp;convient,&nbsp;nous&nbsp;allons&nbsp;vous&nbsp;convoquer&nbsp;par&nbsp;email&nbsp;&agrave;&nbsp;un&nbsp;entretien&nbsp;d&#39;embauche.&nbsp;Cordialement</p>\r\n', '<p>Nous&nbsp;avons&nbsp;bien&nbsp;re&ccedil;u&nbsp;votre&nbsp;message&nbsp;et&nbsp;nous vous en&nbsp;remercions,&nbsp;un&nbsp;de&nbsp;nos&nbsp;op&eacute;rateurs&nbsp;reviendra&nbsp;vers&nbsp;vous&nbsp;d&eacute;s&nbsp;que&nbsp;possible.</p>\r\n', '<p>Message sent successfully!!</p>\r\n', '<p style=\"text-align:justify\"><span style=\"background-color:rgb(239, 239, 239); color:rgb(64, 64, 64); font-family:open sans,verdana,tahoma,serif; font-size:13px\">We use cookies to ensure we give you the best experience on our website. By continuing on this site, we&#39;ll assume you are ok with it.&nbsp;</span><span style=\"background-color:rgb(242, 242, 242); color:rgb(51, 51, 51); font-family:open sans,verdana,tahoma,serif; font-size:13px\">Please see our <a href=\"https://ecab.app/legals/privacy-policy.php\" onclick=\"window.open(this.href, \'PrivacyPolicy\', \'resizable=no,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=no,dependent=no\'); return false;\">Privacy Policy</a></span><span style=\"background-color:rgb(242, 242, 242); color:rgb(51, 51, 51); font-family:open sans,verdana,tahoma,serif; font-size:13px\">&nbsp;for details.</span></p>\r\n', '<p style=\"text-align:justify\"><span style=\"background-color:rgb(239, 239, 239); color:rgb(64, 64, 64); font-family:open sans,verdana,tahoma,serif; font-size:13px\">We use cookies to ensure we give you the best experience on our website. By continuing on this site, we&#39;ll assume you are ok with it.&nbsp;</span><span style=\"background-color:rgb(242, 242, 242); color:rgb(51, 51, 51); font-family:open sans,verdana,tahoma,serif; font-size:13px\">Please see our <a href=\"https://ecab.app/legals/privacy-policy.php\" onclick=\"window.open(this.href, \'PrivacyPolicy\', \'resizable=no,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=no,dependent=no\'); return false;\">Privacy Policy</a></span><span style=\"background-color:rgb(242, 242, 242); color:rgb(51, 51, 51); font-family:open sans,verdana,tahoma,serif; font-size:13px\">&nbsp;for details.</span></p>\r\n', 0, 0, 0, 7, 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `vbs_popups_settings`
--
ALTER TABLE `vbs_popups_settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `vbs_popups_settings`
--
ALTER TABLE `vbs_popups_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
