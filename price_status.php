
                            <div class="row">
                            <div class="Listpricestatus">
                                <input type="hidden" class="chk-Addpricestatus-btn" value="">
                   
                            <div class="col-md-12">
                            <div class="module-body table-responsive">
                                <table class="configTable table table-bordered table-striped  table-hover dataTable no-footer" cellspacing="0" width="100%" data-selected_id="">
                                    <thead>
                                    <tr>
                                        <th class="no-sort text-center"style="width:2% !important;">#</th>
                                        <th class="text-center" style="width:3% !important;"><?php echo $this->lang->line('id'); ?></th>
                                        <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('date'); ?></th>
                                        <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('time'); ?></th>
                                        <th class="text-center" style="width:6% !important;"><?php echo $this->lang->line('added_by'); ?></th>
                                        <th class="column-first_name text-center" style="width:8% !important;white-space: nowrap;overflow: hidden !important;text-overflow: ellipsis;max-width:100px;">Message</th>
                                        <th class="text-center"style="width:8%;">Statut</th>
                                        <th class="text-center"style="width:10%;">Since</th>
                                        
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php $cnt=1; ?>
                                        <?php if (!empty($pricestatus_data)): ?>
                                        <?php foreach($pricestatus_data as $key => $item):?>
                                            <tr>
                                                <td class="text-center">
                                                    <input type="checkbox" class="chk-mainpricestatus-template" data-input="<?=$item->id?>"> 
                                                </td>
                                                <td class="text-center"><a href="javascript:void()" onclick="pricestatusidEdit('<?=$item->id?>')"><?=date("dmY", strtotime($item->created_at)) .'0000'. $item->id;?></a></td>
                                                <td class="text-center"><?=from_unix_date($item->created_at)?></td>
                                                <td class="text-center"><?=date("H:i:s", strtotime($item->since))?></td>
                                               <td class="text-center"> 
                                             <?php 
                                            $user=$this->bookings_config_model->getuser($item->user_id);
                                            $user=str_replace(".", " ", $user);
                                            echo $user;
                                            ?></td>
                                                <td class="text-center"><?php echo ($item->statut=='0')?$item->message:'';?></td>
                                                <td class="text-center"><?=$item->statut=='1'?'<span class="label label-success">Show</span>':'<span class="label label-danger">Hide</span>';?></td>
                                                <td class="text-center"><?= timeDiff($item->since) ?></td>

                                            </tr>
                                            <?php $cnt++; ?>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                                <br>
                                </div>
                            </div>
                                                    </div>
                            <?=form_open("admin/booking_config/pricestatusadd")?>
                            <!-- <form action="<?=base_url();?>admin/booking_config/statutadd" class="" method="post"  enctype="multipart/form-data"> -->
                            <div class="pricestatusadd" style="display: none;" >


                             <div class="col-md-12">   
                            <div class="col-md-2" style="margin-top: 5px;">
                                <div class="form-group">
                                <span>Statut</span>
                                <select class="form-control" id="pricestatus" name="statut" required onchange="showpricemessage()">
                                    
                                    <option value="1">Show</option>
                                    <option value="0">Hide</option>
                                </select>
                            </div>
                              </div>  
                            <div class="col-md-3" style="margin-top: 5px;display: none;" id="pricemessage">
                                <div class="form-group">
                                <span>Message</span>
                                    <input type="text" class="form-control"  name="message" placeholder="" value="">
                                </div>
                            </div>
                            
                           
                              </div>
                              <div class="col-md-12">   
                                                       
                                <button  class="btn"style="float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                                <button  type="button" class="btn" style="float:right; margin-left:7px;" onclick="cancelpricestatus()"><span class="fa fa-close"> Cancel </span></button>
                           
                               </div> 
                                
                            <?php echo form_close(); ?>
                          
                            </div>
                            <div class="pricestatusEdit" style="display:none">
                            <?=form_open("admin/booking_config/pricestatusedit")?>
                            <div class="pricestatusEditajax">
                            </div>
                             <div class="col-md-12">   
                            
                            <button  class="btn"style=" float:right; margin-left:7px;"><span class="fa fa-save"></span> Save </button>
                                <button type="button" class="btn" style="float:right; margin-left:7px;" onclick="cancelpricestatus()"><span class="fa fa-close"> Cancel </span></button>
                    
                           
                        </div>
                            <?php echo form_close(); ?>
                            </div>
                            <div class="col-md-12 pricestatusDelete" style="padding:20px  0px; margin-left: 15px;margin-top: 15px; display: none;">
                                        <?=form_open("admin/booking_config/deletepricestatus")?>
                                           
                                            <input type="hidden" id="pricestatusdeletid" name="delet_pricestatus_id" value="">
                                            <div style="display: inline-block;float:left;margin-right: 10px; margin-top:10px;"> Are you sure you want to delete selected?</div>
                                            <!-- <input class="btn" type="submit" id="search_by" value="Yes" style="float:left;margin-top: -11px;margin-right: 10px;" /> -->
                                <button  class="btn"style=" float:left;margin-right: 10px;"><span class="save-icon"></span> Yes </button>

                                            <a class="btn" style="cursor: pointer;" onclick="cancelpricestatus()">No</a>
                                        <?php echo form_close(); ?>
                                    </div>
                        </div>
           


<script type="text/javascript">

   function showpricemessage(){
  
        var status=document.getElementById('pricestatus').value;
      
        if(status==0){
            
            document.getElementById('pricemessage').style.display='block';
        }else{
            document.getElementById('pricemessage').style.display='none';
        }
    }
     function editshowpricemessage(){
      
        var status=document.getElementById('editpricestatus').value;
      
        if(status==0){
            
            document.getElementById('editpricemessage').style.display='block';
        }else{
            document.getElementById('editpricemessage').style.display='none';
        }
    }
                 
    function pricestatusadd()
    {
        setupDivpricestatus();
        $(".pricestatusadd").show();
    }

    function cancelpricestatus()
    {
        setupDivpricestatus();
        $(".Listpricestatus").show();
    }
    function pricestatusDelete()
    {
        var val = $('.chk-Addpricestatus-btn').val();
        if(val != "")
        {
            setupDivpricestatus();
            $(".pricestatusDelete").show();
        }else{
            alert('Please select a row to delete.');
        }
    }
    function pricestatusEdit()
    {
        var val = $('.chk-Addpricestatus-btn').val();
        if (val=='')
        {
            alert("Please select record!");
            return false;
        }
        setupDivpricestatus();
        $(".pricestatusEdit").show();
        $.ajax({
            type: "GET",
            url: '<?php echo base_url().'admin/booking_config/get_ajax_pricestatus'; ?>',
            data: {'pricestatus_id': val},
            success: function (result) {
                // alert(result);
                document.getElementsByClassName("pricestatusEditajax")[0].innerHTML = result;
                  var status=document.getElementById('editpricestatus').value;
      
                    if(status==0){
                        
                        document.getElementById('editpricemessage').style.display='block';
                    }else{
                        document.getElementById('editpricemessage').style.display='none';
                    }
            }
        });
    }
    function pricestatusidEdit(id){
        var val = id;
        if (val=='')
        {
            alert("Please select record!");
            return false;
        }
        setupDivpricestatus();
        $(".pricestatusEdit").show();
        $.ajax({
            type: "GET",
            url: '<?php echo base_url().'admin/booking_config/get_ajax_pricestatus'; ?>',
            data: {'pricestatus_id': val},
            success: function (result) {
                // alert(result);
                document.getElementsByClassName("pricestatusEditajax")[0].innerHTML = result;
                  var status=document.getElementById('editpricestatus').value;
      
                    if(status==0){
                        
                        document.getElementById('editpricemessage').style.display='block';
                    }else{
                        document.getElementById('editpricemessage').style.display='none';
                    }
            }
        });
    }
    function setupDivpricestatus()
    {
        $(".pricestatusadd").hide();
        $(".pricestatusEdit").hide();
        $(".Listpricestatus").hide();
        $(".pricestatusDelete").hide();

    }
     function setupDivpricestatusConfig()
    {
        $(".pricestatusadd").hide();
        $(".pricestatusEdit").hide();
         $(".Listpricestatus").show();
        $(".pricestatusDelete").hide();

    }
    // $('input.chk-mainpricestatus-template').on('change', function() {
    //     $('input.chk-mainpricestatus-template').not(this).prop('checked', false);
    //     var id = $(this).attr('data-input');
    //     console.log(id)
    //     $('.chk-Addpricestatus-btn').val(id);
    //     $('#pricestatusdeletid').val(id);
    // });
    $(document).on('change', 'input.chk-mainpricestatus-template', function() {
        $('input.chk-mainpricestatus-template').not(this).prop('checked', false);
        var id = $(this).attr('data-input');
        console.log(id)
        $('.chk-Addpricestatus-btn').val(id);
        $('#pricestatusdeletid').val(id);
    });

             </script>